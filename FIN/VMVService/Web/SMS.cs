
using System;
using System.Collections;
using System.Configuration;
using System.Data;

using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.IO;
using System.Net;
using System.Threading;

using VMVServices.Services.Configuration.ConfigurationSectionHandlers;
using VMVServices.Services.Business;




namespace VMVServices.Web
{
    public class SMS
    {
        const string ApiIdIndicator = "";
        const string ApiUsernameIndicator = "";
        const string ApiPasswordIndicator = "";
        const string ApiToIndicator = "";
        const string ApiTextIndicator = "";

        string _msg = "";
        string _cellNos = "";
        string _sendUrl = "";
        string _api_id = "";
        //string _api_username = gatewayPrms[UsernameKey].ParameterValue;
        //string _api_password = gatewayPrms[PasswordKey].ParameterValue;
        string _requestQueryString = "No request query string set";
        string _responseHtmlString = "No response html string set";
        HttpWebRequest _httpRequest;
        HttpWebResponse _httpResponse;

        public void SendSMS()
        {
            try { _httpRequest = (HttpWebRequest)WebRequest.Create(_sendUrl); }
            catch (Exception ex) { throw new Exception(string.Concat("Problem connecting to Url: ", _sendUrl, Environment.NewLine, Environment.NewLine, ex.Message)); }

            _httpRequest.Method = "POST";
            _httpRequest.ContentType = "application/x-www-form-urlencoded";

            //Setup the query string with the requitred data to login and send a sn SMS message
           
            //_requestQueryString = string.Concat(ApiIdIndicator, "=", _api_id, "&",
            //    ApiToIndicator, "=", _cellNos, "&",
            //    ApiTextIndicator, "=", _msg);

         
            _requestQueryString = ApiIdIndicator;
            _requestQueryString +="&Username=" + ApiUsernameIndicator;
            _requestQueryString +="&Password=" + ApiPasswordIndicator;
            _requestQueryString +="&To=" + ApiToIndicator;
            _requestQueryString +="&From=" + _api_id;
            _requestQueryString +="&Message=" + _msg;

            StreamWriter sw = new StreamWriter(_httpRequest.GetRequestStream());
            try
            { sw.Write(_requestQueryString); }
            catch (Exception ex) { throw new Exception(string.Concat("Problem posting data: ", Environment.NewLine, Environment.NewLine, _requestQueryString, Environment.NewLine, Environment.NewLine, "to Url: ", Environment.NewLine, Environment.NewLine, _sendUrl, Environment.NewLine, Environment.NewLine, ex.Message)); }
            finally { sw.Close(); }

            _httpResponse = (HttpWebResponse)_httpRequest.GetResponse();

            StreamReader sr = new StreamReader(_httpResponse.GetResponseStream());
            try
            { _responseHtmlString = sr.ReadToEnd(); }
            catch (Exception ex) { throw new Exception(string.Concat("Problem reading response to positing of data to Url: ", Environment.NewLine, Environment.NewLine, _sendUrl, Environment.NewLine, Environment.NewLine, ex.Message)); }
            finally { sr.Close(); }
            //Response.Write(_responseHtmlString);
             

            _httpResponse.Close();
        }
        
    }
}
