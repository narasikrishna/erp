using System;
using System.Web.UI.WebControls;
using VMVServices.Helpers;

namespace VMVServices.Web.Controls
{
	/// <summary>
	/// Summary description for iThinkDataGrid.
	/// </summary>
	public class VMVServicesDataGrid : DataGrid
	{
        public VMVServicesDataGrid()
		{

			//'SET THE DEFAULT PAGING PROPERTIES
            try { this.PagerStyle.PageButtonCount = Configuration.GetInt("VMVServices.Web.Controls.VMVServicesDataGrid.PagerStyle.PageButtonCount", 10); }
            catch { /*do nothing*/ }
            try { this.PagerStyle.Mode = (PagerMode)Configuration.GetInt("VMVServices.Web.Controls.VMVServicesDataGrid.PagerStyle.Mode", (int)PagerMode.NumericPages); }
            catch { /*do nothing*/ }
            try { this.PagerStyle.Position = (PagerPosition)Configuration.GetInt("VMVServices.Web.Controls.VMVServicesDataGrid.PagerStyle.Position", (int)PagerPosition.Bottom); }
            catch { /*do nothing*/ }

            try { this.PageSize = Configuration.GetInt("VMVServices.Web.Controls.VMVServicesDataGrid.PageSize", 10); }
            catch { /*do nothing*/ }

			//'DEFAULT SHOW NUMBER OF ROWS TO TRUE
            try { this.ShowNumberOfRows = Configuration.GetBool(Configuration.GetString("VMVServices.Web.Controls.VMVServicesDataGrid.ShowNoRows"), false); }
            catch { /*do nothing*/ }
            try { this.ShowNumberOfRowsColumn = Configuration.GetInt("VMVServices.Web.Controls.VMVServicesDataGrid.ShowNoRowsColumn", 1); }
            catch { /*do nothing*/ } //'1 ASSUMES IN MOST CASES THE FIRST COLUMN IS A HIDDEN COLUMN

			//'SET UP DEFAULT FORMATTING HERE

            try { this.CellPadding = Configuration.GetInt("VMVServices.Web.Controls.VMVServicesDataGrid.CellPadding", 2); }
            catch { /*do nothing */ }
            try { this.CellSpacing = Configuration.GetInt("VMVServices.Web.Controls.VMVServicesDataGrid.CellSpacing", 2); }
            catch { /*do nothing */ }

            try { this.BorderStyle = (BorderStyle)Configuration.GetInt("VMVServices.Web.Controls.VMVServicesDataGrid.BorderStyle", (int)BorderStyle.Solid); }
            catch { /*do nothing */ }
            try { this.BorderWidth = Unit.Pixel(Configuration.GetInt("VMVServices.Web.Controls.VMVServicesDataGrid.BorderWidth", 1)); }
            catch { /*do nothing */ }
            try { this.GridLines = (GridLines)Configuration.GetInt("VMVServices.Web.Controls.VMVServicesDataGrid.GridLines", (int)GridLines.None); }
            catch { /*do nothing */ }

            try { this.CssClass = Configuration.GetString("VMVServices.Web.Controls.VMVServicesDataGrid.CssClass", "VMVServicesDataGridCSSClass"); }
            catch { /*do nothing */ }
            try { this.EditItemStyle.CssClass = Configuration.GetString("VMVServices.Web.Controls.VMVServicesDataGrid.EditItemStyle.CssClass", "VMVServicesDataGridDataGridEditItemStyleCSSClass"); }
            catch { /*do nothing */ }
            try { this.ItemStyle.CssClass = Configuration.GetString("VMVServices.Web.Controls.VMVServicesDataGrid.ItemStyle.CssClass", "VMVServicesDataGridItemStyleCSSClass"); }
            catch { /*do nothing */ }
            try { this.AlternatingItemStyle.CssClass = Configuration.GetString("VMVServices.Web.Controls.VMVServicesDataGrid.AlternatingItemStyle.CssClass", "VMVServicesDataGridAlternatingItemStyleCSSClass"); }
            catch { /*do nothing */ }
            try { this.HeaderStyle.CssClass = Configuration.GetString("VMVServices.Web.Controls.VMVServicesDataGrid.HeaderStyle.CssClass", "VMVServicesDataGridHeaderStyleCSSClass"); }
            catch { /*do nothing */ }
            try { this.FooterStyle.CssClass = Configuration.GetString("VMVServices.Web.Controls.VMVServicesDataGrid.FooterStyle.CssClass", "VMVServicesDataGridFooterStyleCSSClass"); }
            catch { /*do nothing */ }
            try { this.PagerStyle.CssClass = Configuration.GetString("VMVServices.Web.Controls.VMVServicesDataGrid.PagerStyle.CssClass", "VMVServicesDataGridPagerStyleCSSClass"); }
            catch { /*do nothing */ }

            try { this.RowHoverStyle = Configuration.GetString("VMVServices.Web.Controls.VMVServicesDataGrid.RowHoverStyle", "VMVServicesDataGridRowHoverStyleCSSClass"); }
            catch { /*do nothing */ }

		}

		#region Custom Properties
		string _rowHoverStyle;
		public string RowHoverStyle{get{return _rowHoverStyle;}set{_rowHoverStyle = value;}}

		bool _showNumberOfRows = false;
		public bool ShowNumberOfRows{get{return _showNumberOfRows;}set{_showNumberOfRows = value;}}

		int _showNumberOfRowsColumn = 0;
		public int ShowNumberOfRowsColumn{get{return _showNumberOfRowsColumn;}set{_showNumberOfRowsColumn = value;}}
		#endregion

		#region Custom Methods

		void SetRowHover(System.Web.UI.WebControls.DataGridItemEventArgs e )
		{
			string className = string.Empty;

			if (e.Item.ItemType == ListItemType.Item ||
				e.Item.ItemType == ListItemType.AlternatingItem ||
				e.Item.ItemType == ListItemType.SelectedItem)
			{
				switch(e.Item.ItemType)
				{
					case ListItemType.AlternatingItem :
						className = this.AlternatingItemStyle.CssClass;
						break;
					case ListItemType.Item :
						className = this.ItemStyle.CssClass;
						break;
					case ListItemType.EditItem:
						className = this.EditItemStyle.CssClass;
						break;
				}

				e.Item.Attributes.Add("onmouseover", String.Concat("this.className='", this.RowHoverStyle, "';"));
				e.Item.Attributes.Add("onmouseout", String.Concat("this.className='", className, "';"));
			}
		}

		#endregion

		#region Event handlers

		protected override void OnItemCreated(DataGridItemEventArgs e)
		{
			this.SetRowHover(e);

			base.OnItemCreated (e);
		}


		#endregion


	}
}
