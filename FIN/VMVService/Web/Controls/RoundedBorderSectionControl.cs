using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VMVServices.Web.Controls
{
	/// <summary>
	/// Summary description for RoundedBorderSectionControl.
	/// </summary>
	[ParseChildren(false)]public class RoundedBorderSectionControl : WebControl
	{
		public enum CornerEnum{TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight}
        
		string _TopLeftImageClass = "bg_content_topleft";
		string _TopImageClass = "bg_content_middle";
		string _TopRightImageClass = "bg_content_topright";
		string _LeftImageClass = "bg_content_middle";
		string _RightImageClass = "bg_content_middle";
		string _BottomLeftImageClass = "bg_content_bottomleft";
		string _BottomImageClass = "bg_content_middle";
		string _BottomRightImageClass = "bg_content_bottomright";

		string _TopLeftCornerHeight = "";
		string _TopRightCornerHeight = "";
		string _BottomLeftCornerHeight = "";
		string _BottomRightCornerHeight = "";

		string _TopLeftCornerWidth = "";
		string _TopRightCornerWidth = "";
		string _BottomLeftCornerWidth = "";
		string _BottomRightCornerWidth = "";

		public string TopLeftImageClass { get { return _TopLeftImageClass; } set { _TopLeftImageClass = value; } }
		public string TopImageClass { get { return _TopImageClass; } set { _TopImageClass = value; } }
		public string TopRightImageClass { get { return _TopRightImageClass; } set { _TopRightImageClass = value; } }
		public string LeftImageClass { get { return _LeftImageClass; } set { _LeftImageClass = value; } }
		public string RightImageClass { get { return _RightImageClass; } set { _RightImageClass = value; } }
		public string BottomLeftImageClass { get { return _BottomLeftImageClass; } set { _BottomLeftImageClass = value; } }
		public string BottomImageClass { get { return _BottomImageClass; } set { _BottomImageClass = value; } }
		public string BottomRightImageClass { get { return _BottomRightImageClass; } set { _BottomRightImageClass = value; } }

		string _HorizontalAlign = "Left";
		string _VerticalAlign = "Top";
		string _BackgroundColor = "#ededed";

		public string HorizontalAlign {get{return _HorizontalAlign;}set{_HorizontalAlign = value;}}
		public string VerticalAlign {get{return _VerticalAlign;}set{_VerticalAlign = value;}}
		public string BackgroundColor {get{return _BackgroundColor;}set{_BackgroundColor = value;}}

		protected string GetTableCellRendering(string imageClass, CornerEnum corner)
		{
			string height = string.Empty;
			string width = string.Empty;
			switch(corner)
			{
				case CornerEnum.TopLeft:
					height=GetHeightString(_TopLeftCornerHeight);
					width=GetWidthString(_TopLeftCornerWidth);
					break;
				case CornerEnum.TopRight:
					height=GetHeightString(_TopRightCornerHeight);
					width=GetWidthString(_TopRightCornerWidth);
					break;
				case CornerEnum.BottomLeft:
					height=GetHeightString(_BottomLeftCornerHeight);
					width=GetWidthString(_BottomLeftCornerWidth);
					break;
				case CornerEnum.BottomRight:
					height=GetHeightString(_BottomRightCornerHeight);
					width=GetWidthString(_BottomRightCornerWidth);
					break;
			}
			return string.Concat("<td class='", imageClass, "' ", height, " ", width, "></td>");
		}

		string GetHeightString(string height)
		{
			if (height == string.Empty) 
				return string.Empty; 
			else 
				return string.Concat("height='", height, "px' "); 
		}
		string GetWidthString(string width)
		{
			if (width == string.Empty)
				return string.Empty;
			else
				return string.Concat("width='", width, "px' ");
		}

		public override void RenderBeginTag(HtmlTextWriter writer)
		{

			string beginTag = string.Concat("<table cellpadding='0' cellspacing='0' border='0' align='center' ><tr>", 
				GetTableCellRendering(this.TopLeftImageClass, CornerEnum.TopLeft), 
				GetTableCellRendering(this.TopImageClass, CornerEnum.Top), 
				GetTableCellRendering(this.TopRightImageClass, CornerEnum.TopRight), 
				"</tr><tr>", 
				GetTableCellRendering(this.LeftImageClass, CornerEnum.Left), 
				"<td><DIV style='width:", this.Width.ToString(), 
								";height:", this.Height.ToString(), 
								";Text-Align:", this.HorizontalAlign, 
								";Vertical-Align:", this.VerticalAlign, 
								";Background-Color:", this.BackgroundColor, ";Overflow:auto;Scrollbars:horizontal;'>");

			writer.Write(beginTag);
		}

		public override void RenderEndTag(HtmlTextWriter writer)
		{
			string endTag = string.Concat("</DIV></td>", 
				GetTableCellRendering(this.RightImageClass, CornerEnum.Right), 
				"</tr><tr>", 
				GetTableCellRendering(this.BottomLeftImageClass, CornerEnum.BottomLeft),
				GetTableCellRendering(this.BottomImageClass, CornerEnum.Bottom), 
				GetTableCellRendering(this.BottomRightImageClass, CornerEnum.BottomRight), 
				"</tr></table>");

			writer.Write(endTag);
		}


	}
}
