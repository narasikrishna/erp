using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VMVServices.Web.Controls
{
	/// <summary>
	/// Summary description for SectionControl.
	/// </summary>
	[ParseChildren(false)]public class SectionControl : WebControl
	{
		public override void RenderBeginTag(System.Web.UI.HtmlTextWriter writer)
		{
			string beginTag = string.Concat("<table><tr><td></td>", this.HeaderText, "</tr><tr><td>");
			writer.Write(beginTag);
		}

		public override void RenderEndTag(System.Web.UI.HtmlTextWriter writer)
		{
			string endTag = string.Concat("</td></tr><tr><td>", this.FooterText, "</td></tr></table>");
			writer.Write(endTag);
		}

		string _HeaderText;
		public string HeaderText{get{return _HeaderText;}set{_HeaderText = value;}}

		string _FooterText;
		public string FooterText{get{return _FooterText;}set{_FooterText = value;}}

	}
}
