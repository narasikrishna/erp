using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;

namespace VMVServices.Web.Controls
{
	/// <summary>
	/// Summary description for BackButton.
	/// </summary>
	[DefaultProperty("Text"), 
	ToolboxData("<{0}:BackButton runat=server></{0}:BackButton>")] 
	public class BackButton : Button
	{

		public BackButton()
		{
			this.Click += new System.EventHandler(this.onBackButtonClick);
			this.CausesValidation = false;
		}

		string _NavigateUrl = string.Empty;
		public string NavigateUrl{get{return _NavigateUrl;}set{_NavigateUrl = value;}}

		bool _UseJsHistoryBack = true;
		public bool UseJsHistoryBack {
			get{return _UseJsHistoryBack;}
			set{
				_UseJsHistoryBack = value;
				_UseJsDocumentHrefLocation = !value;
				_UseAspNetServerClickEvent = !value;
			}
		}

		bool _UseJsDocumentHrefLocation = false;
		public bool UseJsDocumentHrefLocation {
			get{return _UseJsDocumentHrefLocation;}
			set{
				_UseJsDocumentHrefLocation = value;
				_UseJsHistoryBack = !value;
				_UseAspNetServerClickEvent = !value;
			}
		}

		bool _UseAspNetServerClickEvent = false;
		public bool UseAspNetServerClickEvent {
			get{return _UseAspNetServerClickEvent;}
			set{
				_UseAspNetServerClickEvent = value;
				_UseJsHistoryBack = !value;
				_UseJsDocumentHrefLocation = !value;
			}
		}

		void onBackButtonClick(object sender, System.EventArgs e)
		{
			this.Page.Response.Redirect(this.ResolveUrl(this.NavigateUrl), false);
		}

		protected override void Render(System.Web.UI.HtmlTextWriter writer)
		{
            this.UseJsHistoryBack = true;
			if (this.UseJsHistoryBack)
				writer.Write(string.Concat("<INPUT type='button' onclick='history.back();' class='", this.CssClass, "' value='", this.Text, "' />"));
			else if (this.UseJsDocumentHrefLocation)
				writer.Write(string.Concat("<INPUT type='button' onclick=\"document.location.href='", this.NavigateUrl, "';\" class='", this.CssClass, "' value='", this.Text, "' />"));
			else
				base.Render (writer);
		}

	}
}
