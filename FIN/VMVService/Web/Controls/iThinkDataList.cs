using System;
using VMVServices.Helpers;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;

namespace VMVServices.Web.Controls
{
	/// <summary>
	/// Summary description for iThinkDataList.
	/// </summary>
	[DefaultProperty("DataSource"),
    ToolboxData("<{0}:VMVServicesDataList runat=server></{0}:VMVServicesDataList>")]
    public class VMVServicesDataList : System.Web.UI.WebControls.DataList
	{

		//Default Constructor
		public VMVServicesDataList()
		{
			this.SetupDefaultStyles();
            
		}

		void SetupDefaultStyles()
		{
			try {_CellPadding = Configuration.GetInt(string.Concat(this.GetType().ToString(), ".CellPadding"));}catch{}
			try {_CellSpacing = Configuration.GetInt(string.Concat(this.GetType().ToString(), ".CellSpacing"));}catch{}
			try {_CssClass = Configuration.GetString(string.Concat(this.GetType().ToString(), ".CssClass"));}catch{}
//			try {_HeaderItemStyleCssClass = Configuration.GetString(string.Concat(this.GetType().ToString(), ".HeaderStyle.CssClass"));}catch{}
//			try {_ItemStyleCssClass = Configuration.GetString(string.Concat(this.GetType().ToString(), ".ItemStyle.CssClass"));}catch{}
//			try {_AlternatingItemStyleCssClass = Configuration.GetString(string.Concat(this.GetType().ToString(), ".AlternatingItemStyle.CssClass"));}catch{}
//			try {_SelectedItemStyleCssClass = Configuration.GetString(string.Concat(this.GetType().ToString(), ".SelectedItemStyle.CssClass"));}catch{}
//			try {_EditItemStyleCssClass = Configuration.GetString(string.Concat(this.GetType().ToString(), ".EditItem.CssClass"));}catch{}
//			try {_FooterItemStyleCssClass = Configuration.GetString(string.Concat(this.GetType().ToString(), ".FooterStyle.CssClass"));}catch{}
//			try {_ItemHoverStyleCssClass = Configuration.GetString(string.Concat(this.GetType().ToString(), ".ItemHoverStyle.CssClass"));}catch{}

			this.CellPadding = _CellPadding;
			this.CellSpacing = _CellSpacing;
			//this.CssClass = _CssClass;
			this.Width = Unit.Percentage(100);
//			this.HeaderStyle.CssClass = _HeaderItemStyleCssClass;
//			this.ItemStyle.CssClass = _ItemStyleCssClass;
//			this.AlternatingItemStyle.CssClass = _AlternatingItemStyleCssClass;
//			this.SelectedItemStyle.CssClass = _SelectedItemStyleCssClass;
//			this.EditItemStyle.CssClass = _EditItemStyleCssClass;
//			this.FooterStyle.CssClass = _FooterItemStyleCssClass;
		}


		#region Properties

		string _CssClass = string.Empty;
		int _CellPadding = 0;
		int _CellSpacing = 0;
		string _HeaderItemStyleCssClass = string.Empty;
		string _ItemStyleCssClass = string.Empty;
		string _AlternatingItemStyleCssClass = string.Empty;
		string _SelectedItemStyleCssClass = string.Empty;
		string _EditItemStyleCssClass = string.Empty;
		string _FooterItemStyleCssClass = string.Empty;
		string _ItemHoverStyleCssClass = string.Empty;

		bool _IsScrollable = true;
		public bool IsScrollable {get{return _IsScrollable;}set{_IsScrollable = value;}}

		string _FixedHeight = "";
		public string FixedHeight {get{return _FixedHeight;}set{_FixedHeight = value;}}

		string _FixedWidth = "100%";
		public string FixedWidth {get{return _FixedWidth;}set{_FixedWidth = value;}}

		string _NoDataText = "";
		public string NoDataText {get{return _NoDataText;}set{_NoDataText = value;}}

		bool _DisplayNoDataText = true;
		public bool DisplayNoDataText{get{return _DisplayNoDataText;}set{_DisplayNoDataText = value;}}

		public bool NoDataPresent{get{return false;}}

		#endregion

		protected override void Render(System.Web.UI.HtmlTextWriter writer)
		{

			string widthDivOpenTag = string.Concat("<Table cellpadding='0' cellspacing='0' border='0'><tr><td>", 
				"<DIV style='vertical-align:top;width:", this.FixedWidth, ";table-layout:auto;'>");
			string heightDivOpenTag = string.Concat("<Table cellpadding='0' cellspacing='0' border='0'><tr><td>", 
				"<DIV class='", _CssClass, "' style='vertical-align:top;overflow:auto;height:", this.FixedHeight, ";width:", this.FixedWidth, ";table-layout:auto;'>");
			string widthDivCloseTag = "</div></td></tr></table>";
			string heightDivCloseTag = "</div></td></tr></table>";


			if (this.NoDataPresent && this.DisplayNoDataText)
				writer.Write(this.NoDataText);
			else
			{
				PlaceHolder headerPlaceHolder = new PlaceHolder();
				if (this.ShowHeader) this.HeaderTemplate.InstantiateIn(headerPlaceHolder);
				this.ShowHeader = false;
				
				writer.Write(widthDivOpenTag);
				headerPlaceHolder.RenderControl(writer);
				if (this.IsScrollable)writer.Write(heightDivOpenTag);
				base.Render (writer);
				if (this.IsScrollable)writer.Write(heightDivCloseTag);
				writer.Write(widthDivCloseTag);
			}
		}

		protected override void OnItemCommand(DataListCommandEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item ||
				e.Item.ItemType == ListItemType.AlternatingItem ||
				e.Item.ItemType == ListItemType.SelectedItem)
			{
				this.SelectedIndex = e.Item.ItemIndex;
			}
			else
			{
				this.SelectedIndex = -1;
			}

			base.OnItemCommand (e);
		}



	}
}

