using System;
using System.Web.UI.WebControls;

namespace VMVServices.Web.Controls
{
	/// <summary>
	/// Summary description for YearDropDownList.
	/// </summary>
	public class YearDropDownList : DropDownList
	{
		public YearDropDownList()
		{
			this.Items.Clear();
			if (this.StartYear < this.EndYear)
				for (int x = this.StartYear; x <= this.EndYear; x++)
					this.Items.Add(new ListItem(x.ToString(), x.ToString()));
			else
				for (int x = this.StartYear; x >= this.EndYear; x--)
					this.Items.Add(new ListItem(x.ToString(), x.ToString()));
		}

		int _startYear = DateTime.Now.Year;
		public int StartYear{get{return _startYear;}set{_startYear = value;}}

		int _endYear = DateTime.Now.Year-5;
		public int EndYear{get{return _endYear;}set{_startYear = value;}}
	}
}
