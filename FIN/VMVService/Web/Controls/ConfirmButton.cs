using System;
using System.Web.UI;
using System.ComponentModel;

namespace VMVServices.Web.Controls
{
	/// <summary>
	/// Summary description for ConfirmButton.
	/// </summary>
	[DefaultProperty("Text"), 
	ToolboxData("<{0}:ConfirmButton runat=server></{0}:ConfirmButton>")] 
	public class ConfirmButton : System.Web.UI.WebControls.Button
	{
		private string _ConfirmText="Are you sure?"; 

		[Bindable(true), 
		Category("Appearance")] 
		public string ConfirmText 
		{ 
			get   {return _ConfirmText;} 
			set   {_ConfirmText = value;} 
		} 
  
		protected override void OnPreRender(EventArgs e) 
		{ 
			base.Attributes.Add("OnClick", 
				"return confirm('"+ _ConfirmText + "');"); 
		} 

	}
}
