using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;


namespace VMVServices.Web.Controls
{
	/// <summary>
	/// Summary description for NumericTextBox.
	/// </summary>
	public class NumericTextBox : System.Web.UI.WebControls.TextBox
	{

        private const string ValidateNumericScriptKey = "VMVServices.Web.Controls.NumericTextBox.ValidateNumericScript";
		private const string ValidateNumericScript = "<script language='javascript'>" +
														"function ValidateNumeric(){" +
															"var keyCode = window.event.keyCode;" +
															"if(keyCode > 57 || keyCode < 48)" +
																"window.event.returnValue = false;" +
														"}</script>";

		protected override void OnPreRender(EventArgs e)
		{
			if (!this.Page.IsClientScriptBlockRegistered(ValidateNumericScriptKey))
				this.Page.RegisterClientScriptBlock(ValidateNumericScriptKey, 
						ValidateNumericScript);
			
			Attributes.Add("onKeyPress", "ValidateNumeric();");
			base.OnPreRender (e);
		}

		public override string Text
		{
			get { return base.Text; }
			set
			{
				try
				{
					base.Text = Convert.ToInt64(value).ToString();
				}
				catch{};
			}
		}


	}
}
