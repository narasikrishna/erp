using System;
using System.Web.UI.WebControls;

namespace VMVServices.Web.Controls
{
	/// <summary>
	/// Summary description for EmailTextBox.
	/// </summary>
	public class EmailTextBox : TextBox
	{
		const string emailRegularExpression = @"^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$";

		protected RequiredFieldValidator EmailRequiredValidator = new RequiredFieldValidator();
		protected RegularExpressionValidator EmailRegularExpressionValidator = new RegularExpressionValidator();

		public EmailTextBox()
		{
			//init validators
			EmailRequiredValidator.ErrorMessage = string.Concat(this.FriendyName, " is a required field.");
			EmailRequiredValidator.Text = EmailRequiredValidator.ErrorMessage;
			if (this.UseValidationSummary) EmailRequiredValidator.Text = "*";
			EmailRequiredValidator.ControlToValidate = this.ID;

			EmailRegularExpressionValidator.ErrorMessage = string.Concat(this.FriendyName, " must be a valid email address [someone@somewhere.com]");
			EmailRegularExpressionValidator.Text = EmailRegularExpressionValidator.ErrorMessage;
			if (this.UseValidationSummary) EmailRegularExpressionValidator.Text = "*";
			EmailRegularExpressionValidator.ControlToValidate = this.ID;
			EmailRegularExpressionValidator.ValidationExpression = emailRegularExpression;
		}

		bool _Required = false;
		public bool Required {get{return _Required;}set{_Required = value;}}

		bool _UseValidationSummary = false;
		public bool UseValidationSummary{get{return _UseValidationSummary;}set{_UseValidationSummary = value;}}

		string _FriendlyName = "Email address";
		public string FriendyName {get{return _FriendlyName;}set{_FriendlyName = value;}}

		protected override void Render(System.Web.UI.HtmlTextWriter writer)
		{
			string tableOpenTag = "<table cellpadding=0 cellspacing=0 border=0><tr><td>";

			writer.Write(tableOpenTag);
			base.Render (writer);
			writer.Write("</td><td>");
			if (this.Required) EmailRequiredValidator.RenderControl(writer);
			EmailRegularExpressionValidator.RenderControl(writer);
			writer.Write("</td></tr></table>");
		}

	}
}
