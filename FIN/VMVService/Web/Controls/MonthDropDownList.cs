using System;
using System.Web.UI.WebControls;

namespace VMVServices.Web.Controls
{
	/// <summary>
	/// Summary description for MonthDropDownList.
	/// </summary>
	public class MonthDropDownList : DropDownList
	{
		public MonthDropDownList()
		{
			this.Items.Clear();
			this.Items.Add(new ListItem("Jan", "1"));
			this.Items.Add(new ListItem("Feb", "2"));
			this.Items.Add(new ListItem("Mar", "3"));
			this.Items.Add(new ListItem("Apr", "4"));
			this.Items.Add(new ListItem("May", "5"));
			this.Items.Add(new ListItem("Jun", "6"));
			this.Items.Add(new ListItem("Jul", "7"));
			this.Items.Add(new ListItem("Aug", "8"));
			this.Items.Add(new ListItem("Sep", "9"));
			this.Items.Add(new ListItem("Oct", "10"));
			this.Items.Add(new ListItem("Nov", "11"));
			this.Items.Add(new ListItem("Dec", "12"));

		}

	}
}
