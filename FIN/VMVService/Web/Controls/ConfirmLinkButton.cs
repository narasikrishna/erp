using System;
using System.ComponentModel;
using System.Web.UI;

namespace VMVServices.Web.Controls
{
	/// <summary>
	/// Summary description for ConfirmLinkButton.
	/// </summary>
	[DefaultProperty("Text"), 
	ToolboxData("<{0}:ConfirmLinkButton runat=server></{0}:ConfirmLinkButton>")] 
	public class ConfirmLinkButton : System.Web.UI.WebControls.LinkButton
	{
		private string _ConfirmText="Are you sure?"; 

		[Bindable(true), 
		Category("Appearance")] 
		public string ConfirmText 
		{ 
			get   {return _ConfirmText;} 
			set   {_ConfirmText = value;} 
		} 
  
		protected override void OnPreRender(EventArgs e) 
		{ 
			base.Attributes.Add("OnClick", 
				"return confirm('"+ _ConfirmText + "');"); 
		} 
	}
}
