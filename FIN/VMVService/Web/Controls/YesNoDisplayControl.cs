using System;
using System.Web.UI.WebControls;
using VMVServices.Helpers;

namespace VMVServices.Web.Controls
{
	/// <summary>
	/// Summary description for YesNoDisplayControl.
	/// </summary>
	public class YesNoDisplayControl : WebControl
	{
		public YesNoDisplayControl()
		{
            try { this._yesImgSrc = Configuration.GetString("VMVServices.Web.Controls.YesNoDisplayControl.YesImgSrc", "~/images/yes.jpg"); }
			catch{}
            try { this._noImgSrc = Configuration.GetString("VMVServices.Web.Controls.YesNoDisplayControl.NoImgSrc", "~/images/no.jpg"); }
			catch{}
		}

		private string _yesImgSrc = string.Empty;
		private string _noImgSrc = string.Empty;
		private bool _value = false;

		public bool Value{get{return _value;}set{_value = value;}}

		protected override void Render(System.Web.UI.HtmlTextWriter writer)
		{
			string renderText = string.Empty;
			if (this.Value)
				renderText = string.Concat("<img src='", this.ResolveUrl(this._yesImgSrc), "' />");
			else
				renderText = string.Concat("<img src='", this.ResolveUrl(this._noImgSrc), "' />");

			writer.Write(renderText);
		}


	}
}
