using System;
using System.Web.UI.WebControls;

using VMVServices.Web;

namespace VMVServices.Web.Controls
{
	/// <summary>
	/// Summary description for ServerDisplayControl.
	/// </summary>
	public class ServerDisplayControl : WebControl
	{
		protected override void Render(System.Web.UI.HtmlTextWriter writer)
		{
			//determine whether we are viewing development, beta or live version of the website
			//and let the user know accordingly
			string serverType = ((PageBase)this.Page).ServerConnectionString.Name;
			string webAppVersion = ((PageBase)this.Page).WebApplicationVersion;
			
			if (serverType.ToUpper() != "LIVE" && serverType.ToUpper() != "PRODUCTION" )
			{
				string serverTypeDiv = string.Concat("<DIV style='PADDING-RIGHT:5px;PADDING-LEFT:5px;FONT-WEIGHT:normal;FONT-SIZE:10pt;LEFT:5px;PADDING-BOTTOM:5px;COLOR:white;PADDING-TOP:5px;FONT-FAMILY:verdana;POSITION:absolute;TOP:5px;BACKGROUND-COLOR:black'>", serverType, " site ", webAppVersion, "</DIV>");
				writer.Write(serverTypeDiv);
			}
		}
	}
}
