using System;
using System.Web.UI.WebControls;

namespace VMVServices.Web.Controls
{
	/// <summary>
	/// Summary description for CSSLinkTagControl.
	/// </summary>
	public class CSSLinkTagControl : WebControl
	{
		string _href;
		public string Href 
		{
			get{return _href;}
			set{_href = value;}
		}

		protected override void Render(System.Web.UI.HtmlTextWriter writer)
		{
			writer.Write(string.Concat("<LINK href=\"", this.ResolveUrl(this.Href), "\" type=\"text/css\" rel=\"stylesheet\">"));
		}

	}
}
