using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Collections;
using System.Collections.Specialized;
using System.Globalization;

namespace VMVServices.Web.Charts
{
	public class StackedColumnChart : Chart
	{
		// Methods
		public StackedColumnChart(){}
		public StackedColumnChart(ChartPointCollection data) : base(data)
		{
		}
 
		public StackedColumnChart(ChartPointCollection data, Color color) : base(data, color)
		{
		}
 
		public override void GetMinMaxMeanValue(out float min, out float max, out float mean)
		{
			ChartEngine engine1 = base.Engine;
			min = 0f;
			max = 0f;
			mean = 0f;
			int num1 = this.GetStackedCount(engine1.Charts);
			if ((num1 > 0) && (engine1.MaxPoints > 0))
			{
				Chart[] chartArray1 = new Chart[num1];
				int num2 = 0;
				foreach (Chart chart1 in ((IEnumerable) engine1.Charts))
				{
					if (chart1 is StackedColumnChart)
					{
						chartArray1[num2++] = chart1;
					}
				}
				float single1 = 0f;
				min = 0f;
				max = float.MinValue;
				for (int num3 = 0; num3 < engine1.MaxPoints; num3++)
				{
					float single3 = 0f;
					float single4 = 0f;
					for (int num4 = 0; num4 < num1; num4++)
					{
						ChartPointCollection collection1 = chartArray1[num4].Data;
						if (collection1.Count > num3)
						{
							float single2 = collection1[num3].yvalue;
							if (single2 >= 0f)
							{
								single3 += single2;
							}
							else
							{
								single4 += single2;
							}
						}
					}
					if (single4 < min)
					{
						min = single4;
					}
					if (single3 > max)
					{
						max = single3;
					}
					single1 += (single3 + single4);
				}
				mean = single1 / ((float) engine1.MaxPoints);
				if (base.dataLabels.visible)
				{
					float single5 = (max - min) / 10f;
					max += single5;
				}
			}
		}
 
		private int GetStackedCount(ChartCollection charts)
		{
			int num1 = 0;
			bool flag1 = true;
			foreach (Chart chart1 in ((IEnumerable) charts))
			{
				if (!(chart1 is StackedColumnChart))
				{
					continue;
				}
				if (flag1)
				{
					if (chart1 != this)
					{
						return 0;
					}
					flag1 = false;
				}
				num1++;
			}
			return num1;
		}
 
		public override void Render(Graphics graphics, int width, int height)
		{
			ChartEngine engine1 = base.Engine;
			int num1 = this.GetStackedCount(engine1.Charts);
			if (num1 != 0)
			{
				StackedColumnChart[] chartArray1 = new StackedColumnChart[num1];
				Pen[] penArray1 = new Pen[num1];
				Brush[] brushArray1 = new Brush[num1];
				int num2 = 0;
				bool flag1 = (base.shadow != null) && base.Shadow.Visible;
				Brush brush1 = null;
				if (flag1)
				{
					brush1 = new SolidBrush(base.Shadow.Color);
				}
				foreach (Chart chart1 in ((IEnumerable) engine1.Charts))
				{
					chartArray1[num2] = chart1 as StackedColumnChart;
					if (chartArray1[num2] != null)
					{
						penArray1[num2] = chart1.Line.GetPen(graphics);
						brushArray1[num2] = chart1.Fill.GetBrush(graphics);
						num2++;
						if (num2 >= num1)
						{
							break;
						}
					}
				}
				float single1 = engine1.scaleX;
				float single2 = engine1.scaleY;
				int num3 = 0;
				int num4 = (int) single1;
				if ((this.maxColumnWidth > 0) && (this.maxColumnWidth < single1))
				{
					num3 = (((int) single1) - this.maxColumnWidth) / 2;
					num4 = this.maxColumnWidth;
				}
				for (int num5 = 0; num5 < engine1.MaxPoints; num5++)
				{
					float single4 = 0f;
					float single5 = 0f;
					for (int num6 = 0; num6 < num1; num6++)
					{
						ChartPointCollection collection1 = chartArray1[num6].Data;
						if (collection1.Count > num5)
						{
							float single8;
							float single9;
							float single3 = collection1[num5].yvalue;
							float single6 = (num5 * single1) + num3;
							float single7 = num4 - 1f;
							if (single3 > 0f)
							{
								single4 += single3;
								single8 = single4 * single2;
								single9 = -single3 * single2;
								if (flag1)
								{
									graphics.FillRectangle(brush1, (float) (single6 + base.shadow.OffsetX), (float) (single8 + base.shadow.OffsetY), single7, single9);
								}
								graphics.FillRectangle(brushArray1[num6], single6, single8, single7, single9);
								graphics.DrawRectangle(penArray1[num6], single6, single8, single7, single9);
							}
							else if (single3 < 0f)
							{
								single8 = single5 * single2;
								single9 = single3 * single2;
								single5 += single3;
								if (flag1)
								{
									graphics.FillRectangle(brush1, (float) (single6 + base.shadow.OffsetX), (float) (single8 + base.shadow.OffsetY), single7, single9);
								}
								graphics.FillRectangle(brushArray1[num6], single6, single8, single7, single9);
								graphics.DrawRectangle(penArray1[num6], single6, single8, single7, single9);
							}
						}
					}
					if (base.dataLabels.visible)
					{
						this.RenderDataLabels(graphics);
					}
				}
			}
		}
 
		protected override void RenderDataLabels(Graphics graphics)
		{
			if (base.dataLabels.font == null)
			{
				base.dataLabels.font = ChartText.DefaultFont;
			}
			ChartEngine engine1 = base.Engine;
			int num1 = base.DataLabelMaxWidth;
			StringFormat format1 = new StringFormat();
			format1.Alignment = StringAlignment.Center;
			format1.LineAlignment = StringAlignment.Near;
			int num2 = this.GetStackedCount(engine1.Charts);
			if (num2 != 0)
			{
				StackedColumnChart[] chartArray1 = new StackedColumnChart[num2];
				Pen[] penArray1 = new Pen[num2];
				Brush[] brushArray1 = new Brush[num2];
				Brush[] brushArray2 = new Brush[num2];
				int num3 = 0;
				foreach (Chart chart1 in ((IEnumerable) engine1.Charts))
				{
					chartArray1[num3] = chart1 as StackedColumnChart;
					if (chartArray1[num3] != null)
					{
						penArray1[num3] = chart1.dataLabels.border.GetPen(graphics);
						brushArray1[num3] = chart1.dataLabels.background.GetBrush(graphics);
						brushArray2[num3] = new SolidBrush(chart1.dataLabels.foreColor);
						num3++;
						if (num3 >= num2)
						{
							break;
						}
					}
				}
				float single4 = engine1.scaleX;
				float single5 = engine1.scaleY;
				for (int num4 = 0; num4 < engine1.MaxPoints; num4++)
				{
					float single2 = 0f;
					float single3 = 0f;
					for (int num5 = 0; num5 < num2; num5++)
					{
						ChartPointCollection collection1 = chartArray1[num5].Data;
						if (collection1.Count > num4)
						{
							RectangleF ef1;
							ChartPoint point1 = collection1[num4];
							float single1 = point1.yvalue;
							string text1 = base.GetDataLabelText(point1);
							if (single1 > 0f)
							{
								single2 += single1;
								ef1 = base.GetDataLabelRectangle(graphics, single2, text1, num1, num4);
								base.DrawDataLabel(graphics, text1, ef1, penArray1[num5], brushArray1[num5], format1, brushArray2[num5]);
							}
							else if (single1 < 0f)
							{
								ef1 = base.GetDataLabelRectangle(graphics, single3, text1, num1, num4);
								single3 += single1;
								base.DrawDataLabel(graphics, text1, ef1, penArray1[num5], brushArray1[num5], format1, brushArray2[num5]);
							}
						}
					}
				}
			}
		}
 

		// Properties
		[DefaultValue(0)]
		public int MaxColumnWidth
		{
			get
			{
				return this.maxColumnWidth;
			}
			set
			{
				this.maxColumnWidth = value;
			}
		}
 

		// Fields
		private int maxColumnWidth;
	}
 
}
