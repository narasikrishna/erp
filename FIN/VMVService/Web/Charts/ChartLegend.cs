using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Collections;
using System.Collections.Specialized;
using System.Globalization;

namespace VMVServices.Web.Charts
{
	[TypeConverter(typeof(ExpandableObjectConverter)), PersistenceMode(PersistenceMode.InnerProperty)]
	public class ChartLegend
	{
		// Methods
		public ChartLegend()
		{
			this.width = 100;
			this.background = new ChartInterior();
			this.font = ChartText.DefaultFont;
			this.border = new ChartLine(Color.Black);
		}
 
		public override string ToString()
		{
			object[] objArray1 = new object[] { this.background.ToString(), this.border.ToString() } ;
			return string.Format(CultureInfo.InvariantCulture, "{0}:{1}", objArray1);
		}
 

		// Properties
		[NotifyParentProperty(true), DefaultValue((string) null), Category("Appearance")]
		public ChartInterior Background
		{
			get
			{
				return this.background;
			}
			set
			{
				this.background = value;
			}
		}
 
		[NotifyParentProperty(true), DefaultValue((string) null), Category("Appearance")]
		public ChartLine Border
		{
			get
			{
				return this.border;
			}
		}
 
		[Category("Appearance"), NotifyParentProperty(true), DefaultValue(typeof(Font), "Tahoma, 8pt")]
		public Font Font
		{
			get
			{
				return this.font;
			}
			set
			{
				this.font = value;
			}
		}
 
		[DefaultValue(0), NotifyParentProperty(true), Category("Appearance")]
		public LegendPosition Position
		{
			get
			{
				return this.position;
			}
			set
			{
				this.position = value;
			}
		}
 
		[DefaultValue(typeof(short), "100"), Category("Appearance"), NotifyParentProperty(true)]
		public short Width
		{
			get
			{
				return this.width;
			}
			set
			{
				this.width = value;
			}
		}
 

		// Fields
		private ChartInterior background;
		private ChartLine border;
		private Font font;
		private LegendPosition position;
		private short width;
	}
 
}
