using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Collections;
using System.Collections.Specialized;
using System.Globalization;

namespace VMVServices.Web.Charts
{
	public class ScatterChart : Chart
	{
		// Methods
		public ScatterChart()
		{
			this.internalDateRenderMode = ScatterChart.DateRenderModeOption.Automatic;
			this.dateRenderMode = ScatterChart.DateRenderModeOption.Automatic;
			this.xdataType = ScatterChart.XValueDataType.Float;
			this.scatterLines = ScatterChart.ScatterLinesOption.Lines;
		}
 
		public ScatterChart(ChartPointCollection data) : base(data)
		{
			this.internalDateRenderMode = ScatterChart.DateRenderModeOption.Automatic;
			this.dateRenderMode = ScatterChart.DateRenderModeOption.Automatic;
			this.xdataType = ScatterChart.XValueDataType.Float;
			this.scatterLines = ScatterChart.ScatterLinesOption.Lines;
		}
 
		public ScatterChart(ChartPointCollection data, Color color) : base(data, color)
		{
			this.internalDateRenderMode = ScatterChart.DateRenderModeOption.Automatic;
			this.dateRenderMode = ScatterChart.DateRenderModeOption.Automatic;
			this.xdataType = ScatterChart.XValueDataType.Float;
			this.scatterLines = ScatterChart.ScatterLinesOption.Lines;
		}
 
		private int GetMaxValue(ScatterChart.DateRenderModeOption option, TimeSpan diff)
		{
			switch (option)
			{
				case ScatterChart.DateRenderModeOption.Years:
				{
					return (int) (diff.TotalDays / 365.25);
				}
				case ScatterChart.DateRenderModeOption.Months:
				{
					return (int) (diff.TotalDays / 30.4375);
				}
				case ScatterChart.DateRenderModeOption.Weeks:
				{
					return (int) (diff.TotalDays / 7);
				}
				case ScatterChart.DateRenderModeOption.Days:
				{
					return (int) diff.TotalDays;
				}
				case ScatterChart.DateRenderModeOption.Hours:
				{
					return (int) diff.TotalHours;
				}
			}
			return (int) diff.TotalMinutes;
		}
 
		public override void GetMinMaxXValue(out int min, out int max)
		{
			ChartPointCollection collection1 = base.Data;
			if ((collection1 == null) || (collection1.Count == 0))
			{
				min = 0;
				max = 0;
			}
			else if (this.xdataType == ScatterChart.XValueDataType.DateTime)
			{
				DateTime time1 = DateTime.MaxValue;
				DateTime time2 = DateTime.MinValue;
				try
				{
					foreach (ChartPoint point1 in ((IEnumerable) base.Data))
					{
						if (point1.xvalue != null)
						{
							DateTime time3 = DateTime.Parse(point1.xvalue);
							if (time3 > time2)
							{
								time2 = time3;
							}
							if (time3 < time1)
							{
								time1 = time3;
							}
						}
					}
				}
				catch (FormatException)
				{
				}
				if (time1 < base.Engine.scatterMinDateTime)
				{
					base.Engine.scatterMinDateTime = time1;
				}
				else
				{
					time1 = base.Engine.scatterMinDateTime;
				}
				if (time2 > base.Engine.scatterMaxDateTime)
				{
					base.Engine.scatterMaxDateTime = time2;
				}
				else
				{
					time2 = base.Engine.scatterMaxDateTime;
				}
				TimeSpan span1 = time2.Subtract(time1);
				if (this.dateRenderMode == ScatterChart.DateRenderModeOption.Automatic)
				{
					if (span1.TotalDays > 1460)
					{
						this.internalDateRenderMode = ScatterChart.DateRenderModeOption.Years;
					}
					else if (span1.TotalDays > 180)
					{
						this.internalDateRenderMode = ScatterChart.DateRenderModeOption.Months;
					}
					else if (span1.TotalDays > 50)
					{
						this.internalDateRenderMode = ScatterChart.DateRenderModeOption.Weeks;
					}
					else if (span1.TotalDays >= 4)
					{
						this.internalDateRenderMode = ScatterChart.DateRenderModeOption.Days;
					}
					else if (span1.TotalHours > 2)
					{
						this.internalDateRenderMode = ScatterChart.DateRenderModeOption.Hours;
					}
					else
					{
						this.internalDateRenderMode = ScatterChart.DateRenderModeOption.Minutes;
					}
				}
				else
				{
					this.internalDateRenderMode = this.dateRenderMode;
				}
				max = this.GetMaxValue(this.internalDateRenderMode, span1) + 1;
				min = 0;
			}
			else
			{
				min = 0x7fffffff;
				max = -2147483648;
				float single1 = float.MaxValue;
				float single2 = float.MinValue;
				foreach (ChartPoint point2 in ((IEnumerable) base.Data))
				{
					float single3 = (int) this.GetXValueForData(point2.xvalue);
					if (single3 > single2)
					{
						single2 = single3;
					}
					if (single3 < single1)
					{
						single1 = single3;
					}
				}
				min = (int) single1;
				max = (int) single2;
				if (single1 < base.Engine.scatterMinNumber)
				{
					base.Engine.scatterMinNumber = (float) min;
				}
				max -= ((int) base.Engine.scatterMinNumber);
			}
		}
 
		public virtual float GetXValueForData(string data)
		{
			float single1 = 0f;
			try
			{
				try
				{
					if (this.xdataType == ScatterChart.XValueDataType.Integer)
					{
						single1 = int.Parse(data);
						return (single1 - ((int) base.Engine.scatterMinNumber));
					}
					if (this.xdataType == ScatterChart.XValueDataType.Float)
					{
						single1 = (int) Math.Min(float.Parse(data), (float) 2.147484E+09f);
						return (single1 - base.Engine.scatterMinNumber);
					}
					if (this.xdataType != ScatterChart.XValueDataType.DateTime)
					{
						return single1;
					}
					DateTime time1 = DateTime.Parse(data);
					DateTime time2 = base.Engine.scatterMinDateTime;
					if (time1 == time2)
					{
						return 1f;
					}
					TimeSpan span1 = time1.Subtract(time2);
					if (this.internalDateRenderMode == ScatterChart.DateRenderModeOption.Years)
					{
						return (((float) (span1.TotalDays / 365.25)) + 1f);
					}
					if (this.internalDateRenderMode == ScatterChart.DateRenderModeOption.Months)
					{
						return (((float) (span1.TotalDays / 30.4375)) + 1f);
					}
					if (this.internalDateRenderMode == ScatterChart.DateRenderModeOption.Weeks)
					{
						return (((float) (span1.TotalDays / 7)) + 1f);
					}
					if (this.internalDateRenderMode == ScatterChart.DateRenderModeOption.Days)
					{
						return (((float) span1.TotalDays) + 1f);
					}
					if (this.internalDateRenderMode == ScatterChart.DateRenderModeOption.Hours)
					{
						return (((float) span1.TotalHours) + 1f);
					}
					return (((float) span1.TotalMinutes) + 1f);
				}
				catch (Exception exception1)
				{
					throw new ApplicationException("Error while parsing the x value", exception1);
				}
			}
			catch (ApplicationException)
			{
			}
			return single1;
		}
 
		public override void Render(Graphics graphics, int width, int height)
		{
			ChartEngine engine1 = base.Engine;
			float single1 = engine1.scaleX;
			float single2 = engine1.scaleY;
			Pen pen1 = base.Line.GetPen(graphics);
			Point point1 = Point.Empty;
			float single3 = 1f;
			float single4 = single1 / 2f;
			Point[] pointArray1 = new Point[base.Data.Count];
			int num1 = 1;
			bool flag1 = (base.shadow != null) && base.Shadow.Visible;
			Pen pen2 = null;
			Point[] pointArray2 = null;
			if (flag1)
			{
				pointArray2 = new Point[base.Data.Count];
				pen2 = new Pen(base.Shadow.Color);
			}
			foreach (ChartPoint point3 in ((IEnumerable) base.Data))
			{
				int num2 = (int) (point3.yvalue * single2);
				single3 = this.GetXValueForData(point3.xvalue);
				int num3 = (int) ((single3 * single1) - single4);
				Point point2 = new Point(num3, num2);
				if (base.showLineMarkers)
				{
					base.lineMarker.Render(graphics, num3, num2);
				}
				if (this.ScatterLines == ScatterChart.ScatterLinesOption.Lines)
				{
					if (num1 > 1)
					{
						if (flag1)
						{
							graphics.DrawLine(pen2, (int) (point1.X + base.shadow.OffsetX), (int) (point1.Y + base.shadow.OffsetY), (int) (point2.X + base.shadow.OffsetX), (int) (point2.Y + base.shadow.OffsetY));
						}
						graphics.DrawLine(pen1, point1, point2);
					}
					point1 = point2;
				}
				else if (this.ScatterLines == ScatterChart.ScatterLinesOption.SmoothLines)
				{
					if (flag1)
					{
						pointArray2[num1 - 1] = new Point(num3 + base.shadow.OffsetX, num2 + base.shadow.OffsetY);
					}
					pointArray1[num1 - 1] = new Point(num3, num2);
				}
				num1++;
				single3 += 1f;
			}
			if (this.scatterLines == ScatterChart.ScatterLinesOption.SmoothLines)
			{
				if (flag1)
				{
					graphics.DrawCurve(pen2, pointArray2);
				}
				graphics.DrawCurve(pen1, pointArray1);
			}
			if (base.dataLabels.visible)
			{
				base.RenderDataLabels(graphics);
			}
		}
 

		// Properties
		[DefaultValue(0)]
		public ScatterChart.DateRenderModeOption DateRenderMode
		{
			get
			{
				return this.dateRenderMode;
			}
			set
			{
				this.dateRenderMode = value;
			}
		}
 
		[DefaultValue(1)]
		public ScatterChart.ScatterLinesOption ScatterLines
		{
			get
			{
				return this.scatterLines;
			}
			set
			{
				this.scatterLines = value;
			}
		}
 
		[DefaultValue(1)]
		public ScatterChart.XValueDataType XDataType
		{
			get
			{
				return this.xdataType;
			}
			set
			{
				this.xdataType = value;
			}
		}
 

		// Fields
		private DateRenderModeOption dateRenderMode;
		private DateRenderModeOption internalDateRenderMode;
		private ScatterLinesOption scatterLines;
		private XValueDataType xdataType;

		// Nested Types
		public enum DateRenderModeOption
		{
			Automatic,
			Years,
			Months,
			Weeks,
			Days,
			Hours,
			Minutes
		}

		public enum ScatterLinesOption
		{
			NoLines,
			Lines,
			SmoothLines
		}

		public enum XValueDataType
		{
			Integer,
			Float,
			DateTime
		}
	}
 
}
