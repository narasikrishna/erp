using System;

namespace VMVServices.Web.Charts
{
	public enum ChartImageFormat
	{
		Png,
		Bmp,
		Jpg,
		Gif,
		Emf,
		Tif,
		Wmf
	}

	public enum DataLabelPosition
	{
		Top,
		Center,
		Bottom
	}
 
	public enum GridLines
	{
		Both,
		Horizontal,
		None,
		Vertical
	}
 
	public enum InteriorType
	{
		Solid,
		Hatch,
		LinearGradient,
		PathGradient,
		Texture
	}
 
	public enum LegendPosition
	{
		Right,
		Bottom,
		Left,
		Top
	}
 

}
