using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Collections;
using System.Collections.Specialized;
using System.Globalization;

namespace VMVServices.Web.Charts
{
	public class SmoothLineChart : LineChart
	{
		// Methods
		public SmoothLineChart(){}
		public SmoothLineChart(ChartPointCollection data) : base(data) { }
 		public SmoothLineChart(ChartPointCollection data, Color color) : base(data, color) { }
		public SmoothLineChart(string xValueField, string yValueField, Color color, bool showLegend, string legend, bool showDataLabels, object dataSource) : base(xValueField, yValueField, color, showLegend, legend, showDataLabels, dataSource){}
		
 
			public override void Render(Graphics graphics, int width, int height)
		{
			ChartPointCollection chartPoints = base.Data;
			if ((chartPoints == null) || (chartPoints.Count < 2))
			{
				base.Render(graphics, width, height);
			}
			else
			{
				//custom smooth line rendering
				ChartEngine chartEngine = base.Engine;
				float scaleX = chartEngine.scaleX;
				float scaleY = chartEngine.scaleY;
				Pen smoothLinePen = base.Line.GetPen(graphics);
				Point[] smoothLinePoints = new Point[chartPoints.Count];
				int pointsCntr = 1;
				float halfScaleX = scaleX / 2f;

				bool showShadow = (base.shadow != null) && base.Shadow.Visible;
				Pen shadowPen = null;
				Point[] shadowPoints = null;
				if (showShadow)
				{
					shadowPoints = new Point[chartPoints.Count];
					shadowPen = new Pen(base.Shadow.Color);
				}

				foreach (ChartPoint point in ((IEnumerable) chartPoints))
				{
					int y = (int) (point.yvalue * scaleY);
					int x = (int) ((pointsCntr * scaleX) - halfScaleX);
					if (base.showLineMarkers)
					{
						base.lineMarker.Render(graphics, x, y);
					}
					smoothLinePoints[pointsCntr - 1] = new Point(x, y);
					if (showShadow)
					{
						shadowPoints[pointsCntr - 1] = new Point(x + base.Shadow.OffsetX, y + base.Shadow.OffsetY);
					}
					pointsCntr++;
				}

				if (showShadow)
				{
					graphics.DrawCurve(shadowPen, shadowPoints);
					shadowPen.Dispose();
				}

				graphics.DrawCurve(smoothLinePen, smoothLinePoints);
				smoothLinePen.Dispose();
				if (base.dataLabels.visible)
				{
					base.RenderDataLabels(graphics);
				}
			}
		}
	}
}
