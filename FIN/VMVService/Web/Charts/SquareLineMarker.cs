using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Collections;
using System.Collections.Specialized;
using System.Globalization;

namespace VMVServices.Web.Charts
{
	public class SquareLineMarker : LineMarker
	{
		// Methods
		public SquareLineMarker()
		{
			this.brush = null;
			this.pen = null;
		}
 
		public SquareLineMarker(int size, Color color, Color borderColor) : base(size, color, borderColor)
		{
			this.brush = null;
			this.pen = null;
		}
 
		public override void Render(Graphics graphics, int x, int y)
		{
			int num1 = base.Size;
			graphics.FillRectangle(this.Brush, (int) (x - (num1 / 2)), (int) (y - (num1 / 2)), num1, num1);
			graphics.DrawRectangle(this.Pen, (int) (x - (num1 / 2)), (int) (y - (num1 / 2)), num1, num1);
		}
 

		// Properties
		private Brush Brush
		{
			get
			{
				if (this.brush == null)
				{
					this.brush = new SolidBrush(base.Color);
				}
				return this.brush;
			}
		}
 
		private Pen Pen
		{
			get
			{
				if (this.pen == null)
				{
					this.pen = new Pen(base.BorderColor, 0f);
				}
				return this.pen;
			}
		}
 

		// Fields
		private Brush brush;
		private Pen pen;
	}
 
}
