using System;

namespace VMVServices.Web.Charts
{
	/// <summary>
	/// Summary description for XYDataSeriesValueField.
	/// </summary>
	public class XYDataSeriesValueField
	{
		public readonly string XValueField;
		public readonly string YValueField;

		public XYDataSeriesValueField(string xValueField, string yValueField)
		{
			if (xValueField == null || xValueField.Length == 0)
				throw new ArgumentNullException("X Value Field", "X Value Field must have a value.");

			if (yValueField == null || yValueField.Length == 0)
				throw new ArgumentNullException("Y Value Field", "Y Value Field must have a value");

			XValueField = xValueField;
			YValueField = yValueField;
		}
	}
}
