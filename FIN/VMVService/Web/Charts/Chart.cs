using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Collections;
using System.Collections.Specialized;
using System.Globalization;

namespace VMVServices.Web.Charts
{
	public abstract class Chart
	{
		// Methods
		protected Chart() : this(new ChartPointCollection()) { }
 		protected Chart(ChartPointCollection data) : this(data, Color.Black) { }
 		protected Chart(ChartPointCollection data, Color lineColor)
		{
			this.name = "";
			this.line = new ChartLine();
			this.engine = null;
			this.legend = "";
			this.dataXValueField = "";
			this.dataYValueField = "";
			this.showLegend = true;
			this.lineMarker = LineMarker.EmptyLineMarker;
			this.showLineMarkers = true;
			this.data = data;
			this.Line.Color = lineColor;
			this.fill = new ChartInterior();
			this.dataLabels = new ChartDataLabels();
		}
		protected Chart(string xValueField, string yValueField, Color color, bool showLegend, string legend, bool showDataLabels, object dataSource)
		{
			this.DataXValueField = xValueField;
			this.DataYValueField = yValueField;

			this.line = new ChartLine();
			this.Line.Color = color;
			this.fill = new ChartInterior(color);
			this.lineMarker = LineMarker.EmptyLineMarker;
			this.LineMarker.Color = color;
			this.LineMarker.BorderColor = color;
			
			this.ShowLegend = showLegend;
			this.Legend = legend;

			this.dataLabels = new ChartDataLabels();
			this.DataLabels.Visible = showDataLabels;
			this.DataLabels.ShowZeroValues = false;

			this.DataSource = dataSource;

		}
 

		public void DataBind()
		{
			if (this.dataSource != null)
			{
				if (!(this.dataSource is IEnumerable))
				{
					throw new ApplicationException("Data Source has not been established");
				}
				IEnumerable enumerable1 = (IEnumerable) this.dataSource;
				IEnumerator dataSourceEnumerator = enumerable1.GetEnumerator();

				//setup the single data collection
				if (this.data == null)
					this.data = new ChartPointCollection(); 
				else
					this.data.Clear(); 

				if ((this.dataXValueField == null) || (this.dataXValueField.Length == 0))
					throw new ArgumentNullException("DataXValueField", "The property DataXValueField needs to be set before calling DataBind");

				if ((this.dataYValueField == null) || (this.dataYValueField.Length == 0))
					throw new ArgumentNullException("DataYValueField", "The property DataYValueField needs to be set before calling DataBind"); 

				while (dataSourceEnumerator.MoveNext())
				{
					string XValue;
					float YValue;
					object obj1 = dataSourceEnumerator.Current;
					object obj2 = null;
					try
					{
						if (obj1 is DataRow)
							obj2 = ((DataRow)obj1)[this.dataXValueField];
						else
							obj2 = DataBinder.GetPropertyValue(obj1, this.dataXValueField);

						XValue = obj2 as string;
						if ((XValue == null) && (obj2 != null))
						{
							XValue = obj2.ToString();
						}
					}
					catch (InvalidCastException exception1)
					{
						throw new InvalidCastException(string.Format("Couldn't convert x-value. to a string from {0}", obj2), exception1);
					}
					try
					{
						if (obj1 is DataRow)
							obj2 = ((DataRow)obj1)[this.dataYValueField];
						else
							obj2 = DataBinder.GetPropertyValue(obj1, this.dataYValueField);

						if (obj2 is DBNull)
						{
							YValue = 0f;
						}
						else
						{
							YValue = Convert.ToSingle(obj2, CultureInfo.InvariantCulture);
						}
					}
					catch (InvalidCastException exception2)
					{
						if (obj2 == null)
						{
							throw new InvalidCastException("Unable to read y-value", exception2);
						}
						throw new InvalidCastException("Unable to convert y-value, from " + obj1.GetType().ToString() + " to single", exception2);
					}
					this.Data.Add(new ChartPoint(XValue, YValue));
				}			
			}
		}
 
		protected virtual void DrawDataLabel(Graphics graphics, string text, RectangleF rectangle, Pen pen, Brush backgroundBrush, StringFormat format, Brush textBrush)
		{
			graphics.FillRectangle(backgroundBrush, rectangle);
			graphics.DrawRectangle(pen, rectangle.Left, rectangle.Top, rectangle.Width, rectangle.Height);
			graphics.DrawString(text, this.dataLabels.font, textBrush, rectangle, format);
		}
 
		protected virtual RectangleF GetDataLabelRectangle(Graphics graphics, float value, string text, int maxWidth, int pointIndex)
		{
			SizeF ef1 = graphics.MeasureString(text, this.dataLabels.font, maxWidth);
			float single1 = ((pointIndex + 0.5f) * this.engine.scaleX) - (ef1.Width / 2f);
			float single2 = value * this.engine.scaleY;
			if (this.dataLabels.position == DataLabelPosition.Top)
			{
				single2 -= ef1.Height;
			}
			else if (this.dataLabels.Position == DataLabelPosition.Center)
			{
				single2 -= (ef1.Height / 2f);
			}
			return new RectangleF(single1, single2, ef1.Width + 1f, ef1.Height);
		}
 
		protected virtual string GetDataLabelText(ChartPoint point)
		{
			string text1 = " ";
			if (this.dataLabels.showLegend)
			{
				text1 = text1 + this.legend + this.dataLabels.separator;
			}
			if (this.dataLabels.showXTitle)
			{
				text1 = text1 + point.xvalue + this.dataLabels.separator;
			}
			if (this.dataLabels.showValue)
			{
				text1 = text1 + point.yvalue.ToString(this.dataLabels.numberFormat, CultureInfo.InvariantCulture);
			}
			return text1;
		}
 
		public virtual void GetMinMaxMeanValue(out float min, out float max, out float mean)
		{
			this.data.GetMinMaxMeanValue(out min, out max, out mean);
			if (this.dataLabels.visible)
			{
				float single1 = (max - min) / 10f;
				max += single1;
			}
		}
 
		public virtual void GetMinMaxXValue(out int min, out int max)
		{
			min = 0;
			if (this.Data != null)
			{
				max = this.Data.Count;
			}
			else
			{
				max = 0;
			}
		}
 
		public virtual void Render(Graphics graphics, int width, int height)
		{
			ChartEngine engine1 = this.Engine;
			float single1 = engine1.scaleX;
			float single2 = engine1.scaleY;
			Pen pen1 = this.Line.GetPen(graphics);
			Point[] pointArray1 = new Point[this.Data.Count];
			int num1 = 1;
			foreach (ChartPoint point1 in ((IEnumerable) this.Data))
			{
				if (this.showLineMarkers)
				{
					this.lineMarker.Render(graphics, (int) ((num1 * single1) - (single1 / 2f)), (int) (point1.yvalue * single2));
				}
				pointArray1[num1 - 1] = new Point((int) ((num1 * single1) - (single1 / 2f)), (int) (point1.yvalue * single2));
				if (num1 > 1)
				{
					graphics.DrawLine(pen1, pointArray1[num1 - 2], pointArray1[num1 - 1]);
				}
				num1++;
			}
			if (this.dataLabels.visible)
			{
				this.RenderDataLabels(graphics);
			}
		}
 
		protected virtual void RenderDataLabels(Graphics graphics)
		{
			if (this.dataLabels.font == null)
			{
				this.dataLabels.font = ChartText.DefaultFont;
			}
			int dataLabelMaxWidth = this.DataLabelMaxWidth;
			int cntr = 0;
			StringFormat stringFormat = new StringFormat();
			stringFormat.Alignment = StringAlignment.Center;
			stringFormat.LineAlignment = StringAlignment.Near;
			Brush dataLabelBackgroundBrush = this.dataLabels.background.GetBrush(graphics);
			Pen dataLabelPen = this.dataLabels.border.GetPen(graphics);
			Brush dataLabelForeColorBrush = new SolidBrush(this.dataLabels.foreColor);
			foreach (ChartPoint point1 in ((IEnumerable) this.data))
			{
				string dataLabelText = this.GetDataLabelText(point1);
				RectangleF ef1 = this.GetDataLabelRectangle(graphics, point1.yvalue, dataLabelText, dataLabelMaxWidth, cntr);
				cntr++;
				if ((point1.yvalue != 0f) || this.dataLabels.showZeroValues)
				{
					this.DrawDataLabel(graphics, dataLabelText, ef1, dataLabelPen, dataLabelBackgroundBrush, stringFormat, dataLabelForeColorBrush);
				}
			}
			dataLabelForeColorBrush.Dispose();
			dataLabelPen.Dispose();
			dataLabelBackgroundBrush.Dispose();
		}
 

		// Properties
		#region public properties
		[Category("Appearance"), PersistenceMode(PersistenceMode.InnerProperty), NotifyParentProperty(true), DefaultValue((string) null), DesignerSerializationVisibility(DesignerSerializationVisibility.Content), Bindable(true)]
		public ChartPointCollection Data
		{
			get
			{
				return this.data;
			}
		}

		protected virtual int DataLabelMaxWidth
		{
			get
			{
				if (this.dataLabels.maxPointsWidth <= 0)
				{
					return this.engine.Size.Width;
				}
				return (int) (this.engine.scaleX * this.dataLabels.maxPointsWidth);
			}
		}
 
		[Bindable(true), NotifyParentProperty(true), DesignerSerializationVisibility(DesignerSerializationVisibility.Content), PersistenceMode(PersistenceMode.InnerProperty), Category("Appearance")]
		public ChartDataLabels DataLabels
		{
			get
			{
				if (this.dataLabels == null)
				{
					this.dataLabels = new ChartDataLabels();
				}
				return this.dataLabels;
			}
		}
 
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), NotifyParentProperty(true), Browsable(false)]
		public object DataSource
		{
			get
			{
				return this.dataSource;
			}
			set
			{
				if ((value != null) && !(value is IEnumerable))
				{
					throw new ArgumentException("Invalid data source type");
				}
				this.dataSource = value;
			}
		}
 
		[DefaultValue(""), NotifyParentProperty(true)]
		public string DataXValueField
		{
			get
			{
				return this.dataXValueField;
			}
			set
			{
				this.dataXValueField = value;
			}
		}

		[DefaultValue(""), NotifyParentProperty(true)]
		public string DataYValueField
		{
			get
			{
				return this.dataYValueField;
			}
			set
			{
				this.dataYValueField = value;
			}
		}

		[NotifyParentProperty(true), Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public ChartEngine Engine
		{
			get
			{
				return this.engine;
			}
			set
			{
				this.engine = value;
			}
		}
 
		[PersistenceMode(PersistenceMode.InnerProperty), DesignerSerializationVisibility(DesignerSerializationVisibility.Content), DefaultValue((string) null), Bindable(true), NotifyParentProperty(true), Category("Appearance")]
		public ChartInterior Fill
		{
			get
			{
				return this.fill;
			}
			set
			{
				this.fill = value;
			}
		}
 
		[NotifyParentProperty(true), DefaultValue("")]
		public string Legend
		{
			get
			{
				return this.legend;
			}
			set
			{
				this.legend = value;
			}
		}
 
		[NotifyParentProperty(true), Category("Appearance"), DefaultValue((string) null), DesignerSerializationVisibility(DesignerSerializationVisibility.Content), PersistenceMode(PersistenceMode.InnerProperty), Bindable(true)]
		public ChartLine Line
		{
			get
			{
				return this.line;
			}
		}
 
		[Browsable(false), NotifyParentProperty(true), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public LineMarker LineMarker
		{
			get
			{
				return this.lineMarker;
			}
			set
			{
				this.lineMarker = value;
			}
		}
 
		[Browsable(true), NotifyParentProperty(true), DefaultValue("")]
		public string Name
		{
			get
			{
				return this.name;
			}
			set
			{
				this.name = value;
			}
		}
 
		[Bindable(true), PersistenceMode(PersistenceMode.InnerProperty), Category("Appearance"), NotifyParentProperty(true), DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public ChartShadow Shadow
		{
			get
			{
				if (this.shadow == null)
				{
					this.shadow = new ChartShadow();
				}
				return this.shadow;
			}
		}
 
		[NotifyParentProperty(true), DefaultValue(true)]
		public bool ShowLegend
		{
			get
			{
				return this.showLegend;
			}
			set
			{
				this.showLegend = value;
			}
		}
 
		[DefaultValue(true), NotifyParentProperty(true)]
		public bool ShowLineMarkers
		{
			get
			{
				return this.showLineMarkers;
			}
			set
			{
				this.showLineMarkers = value;
			}
		}

		#endregion

		// Fields
		#region private fields
		private ChartPointCollection data;
		internal ChartDataLabels dataLabels;
		private object dataSource;
		private string dataXValueField;
		private string dataYValueField;
		private ChartEngine engine;
		private ChartInterior fill;
		private string legend;
		private ChartLine line;
		internal LineMarker lineMarker;
		private string name;
		internal ChartShadow shadow;
		internal bool showLegend;
		internal bool showLineMarkers;
		#endregion

	}
 
}
