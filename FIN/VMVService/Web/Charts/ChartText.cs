using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Collections;
using System.Collections.Specialized;
using System.Globalization;

using VMVServices.Web.Charts.Design;

namespace VMVServices.Web.Charts
{
	[PersistenceMode(PersistenceMode.InnerProperty), TypeConverter(typeof(ExpandableObjectConverter))]
	public class ChartText
	{
		// Methods
		static ChartText()
		{
			ChartText.defaultFont = new Font("Tahoma", 8f);
		}
 
		public ChartText()
		{
			this.text = string.Empty;
			this.font = ChartText.defaultFont;
			this.foreColor = Color.Black;
			this.stringFormat = new StringFormat();
			this.stringFormat.Alignment = StringAlignment.Center;
			this.stringFormat.FormatFlags = StringFormatFlags.LineLimit;
		}
 
		public ChartText(StringFormat format)
		{
			this.text = string.Empty;
			this.font = ChartText.defaultFont;
			this.foreColor = Color.Black;
			this.stringFormat = format;
		}
 
		public override string ToString()
		{
			return this.text;
		}
 

		// Properties
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
		public static Font DefaultFont
		{
			get
			{
				return ChartText.defaultFont;
			}
			set
			{
				ChartText.defaultFont = value;
			}
		}
 
		[Category("Appearance"), DefaultValue(typeof(Font), "Tahoma, 8pt"), NotifyParentProperty(true)]
		public Font Font
		{
			get
			{
				return this.font;
			}
			set
			{
				this.font = value;
			}
		}
 
		[NotifyParentProperty(true), DefaultValue(typeof(Color), "Black"), Category("Appearance")]
		public Color ForeColor
		{
			get
			{
				return this.foreColor;
			}
			set
			{
				this.foreColor = value;
			}
		}
 
		[Browsable(true), DefaultValue((string) null), PersistenceMode(PersistenceMode.Attribute), TypeConverter(typeof(StringFormatConverter)), NotifyParentProperty(true)]
		public StringFormat StringFormat
		{
			get
			{
				return this.stringFormat;
			}
			set
			{
				this.stringFormat = value;
			}
		}
 
		[Category("Appearance"), NotifyParentProperty(true), DefaultValue("")]
		public string Text
		{
			get
			{
				return this.text;
			}
			set
			{
				this.text = value;
			}
		}
 

		// Fields
		private static Font defaultFont;
		private Font font;
		private Color foreColor;
		private StringFormat stringFormat;
		private string text;
	}
 
}
