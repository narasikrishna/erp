using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Collections;
using System.Collections.Specialized;
using System.Globalization;

namespace VMVServices.Web.Charts
{
	public class PieChart : Chart
	{
		// Methods
		public PieChart(){}
		public PieChart(ChartPointCollection data) : base(data)
		{
		}
 
		public PieChart(ChartPointCollection data, Color color) : base(data, color)
		{
		}
 
		private void DrawDataLabels(Graphics graphics, float angleScale, float x, float y, int width)
		{
			float single1 = 0f;
			float single2 = 270f;
			float single3 = x;
			float single4 = y;
			float single5 = ((float) width) / 2f;
			float single6 = single5;
			StringFormat format1 = null;
			format1 = new StringFormat();
			format1.Alignment = StringAlignment.Center;
			format1.LineAlignment = StringAlignment.Near;
			Pen pen1 = base.dataLabels.border.GetPen(graphics);
			Brush brush1 = base.dataLabels.background.GetBrush(graphics);
			Brush brush2 = new SolidBrush(base.dataLabels.foreColor);
			if (base.dataLabels.Position == DataLabelPosition.Center)
			{
				single6 *= 0.6f;
			}
			int num1 = this.DataLabelMaxWidth;
			single1 = 0f;
			single2 = 270f;
			foreach (ChartPoint point1 in ((IEnumerable) base.Data))
			{
				PointF tf1;
				single1 = angleScale * point1.yvalue;
				float single7 = single1 / 2f;
				if (this.explosion != 0)
				{
					float single8 = ((single2 + single7) * 3.141593f) / 180f;
					float single9 = ((float) Math.Cos((double) single8)) * this.explosion;
					float single10 = ((float) Math.Sin((double) single8)) * this.explosion;
					x = single3 + single9;
					y = single4 + single10;
				}
				string text1 = this.GetDataLabelText(point1);
				SizeF ef1 = graphics.MeasureString(text1, base.dataLabels.font, num1, format1);
				float single11 = single2 * 0.01745329f;
				float single12 = single7 * 0.01745329f;
				float single13 = (x + single5) + (single6 * ((float) Math.Cos((double) (single11 + single12))));
				float single14 = (y + single5) + (single6 * ((float) Math.Sin((double) (single11 + single12))));
				if (base.dataLabels.Position == DataLabelPosition.Center)
				{
					tf1 = new PointF(single13 - (ef1.Width / 2f), single14 - (ef1.Height / 2f));
				}
				else
				{
					float single15 = single2 + single7;
					if (single15 < 360f)
					{
						tf1 = new PointF(single13 + 1f, single14 - ef1.Height);
					}
					else if (single15 < 450f)
					{
						tf1 = new PointF(single13 + 1f, single14);
					}
					else if (single15 < 540f)
					{
						tf1 = new PointF(single13 - ef1.Width, single14);
					}
					else
					{
						tf1 = new PointF(single13 - ef1.Width, single14 - ef1.Height);
					}
				}
				RectangleF ef2 = new RectangleF(tf1, ef1);
				if ((point1.yvalue != 0f) || base.dataLabels.showZeroValues)
				{
					this.DrawDataLabel(graphics, text1, ef2, pen1, brush1, format1, brush2);
				}
				single2 += single1;
			}
		}
 
		private void DrawPie(Graphics graphics, float angleScale, float x, float y, int width, int height)
		{
			bool flag1 = (base.shadow != null) && base.Shadow.Visible;
			Brush brush1 = null;
			if (flag1)
			{
				brush1 = new SolidBrush(base.Shadow.Color);
			}
			Pen pen1 = base.Line.GetPen(graphics);
			float single1 = x;
			float single2 = y;
			float single3 = 0f;
			float single4 = 270f;
			int num1 = 0;
			int num2 = this.colors.Length - 1;
			foreach (ChartPoint point1 in ((IEnumerable) base.Data))
			{
				if (num1 > num2)
				{
					num1 = 0;
				}
				Color color1 = this.colors[num1++];
				single3 = angleScale * point1.yvalue;
				if (this.explosion != 0)
				{
					float single5 = ((single4 + (single3 / 2f)) * 3.141593f) / 180f;
					float single6 = ((float) Math.Cos((double) single5)) * this.explosion;
					float single7 = ((float) Math.Sin((double) single5)) * this.explosion;
					x = single1 + single6;
					y = single2 + single7;
				}
				if (flag1)
				{
					graphics.FillPie(brush1, (float) (x + base.Shadow.OffsetX), (float) (y + base.Shadow.OffsetY), (float) width, (float) height, single4, single3);
				}
				graphics.FillPie(new SolidBrush(color1), x, y, (float) width, (float) height, single4, single3);
				graphics.DrawPie(pen1, x, y, (float) width, (float) height, single4, single3);
				single4 += single3;
			}
		}
 
		public override void Render(Graphics graphics, int width, int height)
		{
			ChartEngine engine1 = base.Engine;
			engine1.GridLines = GridLines.None;
			engine1.ShowXValues = false;
			engine1.ShowYValues = false;
			float single1 = 0f;
			float single2 = -height;
			single2 += this.explosion;
			single1 += this.explosion;
			height -= (10 + (2 * this.explosion));
			width -= (10 + (2 * this.explosion));
			if (height >= width)
			{
				single2 += ((height - width) / 2);
				height = width;
			}
			else
			{
				single1 += ((width - height) / 2);
				width = height;
			}
			if (width > 0)
			{
				float single3 = 0f;
				foreach (ChartPoint point1 in ((IEnumerable) base.Data))
				{
					single3 += point1.yvalue;
				}
				float single4 = 360f / single3;
				if ((this.colors == null) || (this.colors.Length == 0))
				{
					Color[] colorArray1 = new Color[] { Color.SteelBlue, Color.DarkSlateBlue, Color.LightSteelBlue, Color.LemonChiffon, Color.MediumSlateBlue, Color.Lavender, Color.DarkKhaki, Color.Blue, Color.Cyan, Color.AntiqueWhite } ;
					this.colors = colorArray1;
				}
				int num1 = this.DataLabelMaxWidth;
				if (base.dataLabels.Visible)
				{
					if (base.dataLabels.font == null)
					{
						base.dataLabels.font = ChartText.DefaultFont;
					}
					float single5 = 0f;
					foreach (ChartPoint point2 in ((IEnumerable) base.Data))
					{
						string text1 = this.GetDataLabelText(point2);
						SizeF ef1 = graphics.MeasureString(text1, base.dataLabels.font, num1);
						if (ef1.Height > single5)
						{
							single5 = ef1.Height;
						}
						if (ef1.Width > single5)
						{
							single5 = ef1.Width;
						}
					}
					single1 += single5;
					single2 += single5;
					height -= ((int) (single5 * 2f));
					width = height;
				}
				if (width > 0)
				{
					this.DrawPie(graphics, single4, single1, single2, width, height);
					if (base.dataLabels.visible)
					{
						this.DrawDataLabels(graphics, single4, single1, single2, width);
					}
				}
			}
		}
 

		// Properties
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
		public Color[] Colors
		{
			get
			{
				return this.colors;
			}
			set
			{
				this.colors = value;
			}
		}
 
		[DefaultValue(0), Browsable(true)]
		public int Explosion
		{
			get
			{
				return this.explosion;
			}
			set
			{
				this.explosion = value;
			}
		}
 

		// Fields
		private Color[] colors;
		private int explosion;
	}
 
}
