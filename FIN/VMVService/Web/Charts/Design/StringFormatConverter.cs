using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.Design;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Drawing;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.ComponentModel.Design.Serialization;
using System.Collections;
using System.Collections.Specialized;
using System.Windows.Forms;

namespace VMVServices.Web.Charts.Design
{
	public class StringFormatConverter : ExpandableObjectConverter
	{
		// Methods
		public StringFormatConverter(){}
		public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
		{
			if (sourceType == typeof(string))
			{
				return true;
			}
			return base.CanConvertFrom(context, sourceType);
		}
 
		public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
		{
			if ((destinationType != typeof(string)) && (destinationType != typeof(InstanceDescriptor)))
			{
				return base.CanConvertTo(context, destinationType);
			}
			return true;
		}
 
		public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
		{
			if (value == null)
			{
				return new StringFormat();
			}
			if (!(value is string))
			{
				return base.ConvertFrom(context, culture, value);
			}
			string text1 = (string) value;
			if ((text1 == null) || (text1.Length == 0))
			{
				return new StringFormat();
			}
			StringFormat format1 = new StringFormat();
			string[] textArray1 = null;
			if (culture != null)
			{
				textArray1 = text1.Split(culture.TextInfo.ListSeparator.ToCharArray());
			}
			else
			{
				textArray1 = text1.Split(CultureInfo.InvariantCulture.TextInfo.ListSeparator.ToCharArray());
			}
			if ((textArray1 == null) || (textArray1.Length != 4))
			{
				return new StringFormat();
			}
			EnumConverter converter1 = new EnumConverter(typeof(StringAlignment));
			format1.Alignment = (StringAlignment) converter1.ConvertFromString(textArray1[0]);
			format1.LineAlignment = (StringAlignment) converter1.ConvertFromString(textArray1[1]);
			EnumConverter converter2 = new EnumConverter(typeof(StringTrimming));
			format1.Trimming = (StringTrimming) converter2.ConvertFromString(textArray1[2]);
			EnumConverter converter3 = new EnumConverter(typeof(StringFormatFlags));
			format1.FormatFlags = (StringFormatFlags) converter3.ConvertFromString(textArray1[3]);
			return format1;
		}
 
		public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
		{
			if ((value != null) && !(value is StringFormat))
			{
				throw new ArgumentException("Invalid StringFormat", "value");
			}
			if (destinationType == typeof(string))
			{
				string[] textArray1;
				if (value == null)
				{
					return string.Empty;
				}
				StringFormat format1 = (StringFormat) value;
				if (culture != null)
				{
					textArray1 = new string[] { format1.Alignment.ToString(), format1.LineAlignment.ToString(), format1.Trimming.ToString(), format1.FormatFlags.ToString() } ;
					return string.Join(culture.TextInfo.ListSeparator, textArray1);
				}
				textArray1 = new string[] { format1.Alignment.ToString(), format1.LineAlignment.ToString(), format1.Trimming.ToString(), format1.FormatFlags.ToString() } ;
				return string.Join(CultureInfo.InvariantCulture.TextInfo.ListSeparator, textArray1);
			}
			if (destinationType != typeof(InstanceDescriptor))
			{
				return base.ConvertTo(context, culture, value, destinationType);
			}
			if (value != null)
			{
				System.Reflection.MemberInfo info1 = null;
				object[] objArray1 = null;
				StringFormat format2 = (StringFormat) value;
				Type[] typeArray1 = new Type[] { typeof(StringAlignment), typeof(StringAlignment), typeof(StringTrimming), typeof(StringFormatFlags) } ;
				info1 = typeof(StringFormatConverter).GetMethod("CreateStringFormat", typeArray1);
				object[] objArray2 = new object[] { format2.Alignment, format2.LineAlignment, format2.Trimming, format2.FormatFlags } ;
				objArray1 = objArray2;
				if (info1 != null)
				{
					return new InstanceDescriptor(info1, objArray1);
				}
			}
			return null;
		}
 
		public static StringFormat CreateStringFormat(StringAlignment alignment, StringAlignment lineAlignment, StringTrimming trimming, StringFormatFlags formatFlags)
		{
			StringFormat format1 = new StringFormat(formatFlags);
			format1.Alignment = alignment;
			format1.LineAlignment = lineAlignment;
			format1.Trimming = trimming;
			return format1;
		}
 
	}
 
}
