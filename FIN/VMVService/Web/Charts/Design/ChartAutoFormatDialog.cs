using System;
using System.Data;
using System.Web;
using System.IO;
using System.Web.UI;
using System.Web.UI.Design;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Collections;
using System.Collections.Specialized;
using System.Windows.Forms;
using System.Globalization;
using System.Xml;

using VMVServices.Web.Charts;

namespace VMVServices.Web.Charts.Design
{
	public class ChartAutoFormatDialog : Form
	{
		// Methods
		static ChartAutoFormatDialog()
		{
			ChartAutoFormatDialog.interiorTypeConverter = new EnumConverter(typeof(InteriorType));
			ChartAutoFormatDialog.hatchStyleConverter = new EnumConverter(typeof(HatchStyle));
			ChartAutoFormatDialog.lineCapConverter = new EnumConverter(typeof(LineCap));
			ChartAutoFormatDialog.dashStyleConverter = new EnumConverter(typeof(DashStyle));
			ChartAutoFormatDialog.lineJoinConverter = new EnumConverter(typeof(LineJoin));
			ChartAutoFormatDialog.gridLinesConverter = new EnumConverter(typeof(GridLines));
			ChartAutoFormatDialog.fontConverter = new FontConverter();
			ChartAutoFormatDialog.pointConverter = new PointConverter();
			ChartAutoFormatDialog.stringFormatConverter = new StringFormatConverter();
		}
 
		public ChartAutoFormatDialog(ControlDesigner controlDesigner)
		{
			this.components = null;
			this.schemes = null;
			this._controlDesigner = controlDesigner;
			this._chart = (ChartControl) controlDesigner.Component;
			this.InitializeComponent();
			this.ReadSchemes();
			foreach (DataRow row1 in this.schemes.Rows)
			{
				this.lstScheme.Items.Add(row1["SchemeName"].ToString());
			}
			if (this.lstScheme.Items.Count > 0)
			{
				this.lstScheme.SelectedIndex = 0;
			}
		}
 
		private void ApplyScheme(string name, ChartControl c)
		{
			DataRow row1 = this.schemes.Rows.Find(name);
			if (row1 != null)
			{
				string text1 = "Configuring the Chart";
				try
				{
					this.ConfigureChart(row1, c);
					text1 = "Configuring the Background";
					this.ConfigureInterior(row1, c.Background, "Background");
					text1 = "Configuring the PlotBackground";
					this.ConfigureInterior(row1, c.PlotBackground, "PlotBackground");
					text1 = "Configuring the Border";
					this.ConfigureLine(row1, c.Border, "Border");
					text1 = "Configuring the ChartTitle";
					this.ConfigureText(row1, c.ChartTitle.Text, "ChartTitle");
					text1 = "Configuring the XTitle";
					this.ConfigureText(row1, c.XTitle, "XTitle");
					text1 = "Configuring the YTitle";
					this.ConfigureText(row1, c.YTitle, "YTitle");
					text1 = "Configuring the XAxisFont";
					this.ConfigureText(row1, c.XAxisFont, "XAxisFont");
					text1 = "Configuring the YAxisFont";
					this.ConfigureText(row1, c.YAxisFont, "YAxisFont");
				}
				catch (Exception exception1)
				{
					throw new ApplicationException("Error while " + text1 + "\n" + exception1.ToString(), exception1);
				}
			}
		}
 
		private void ConfigureChart(DataRow scheme, ChartControl c)
		{
			string text1 = scheme["ShowTitlesOnBackground"].ToString();
			if (text1.Length > 0)
			{
				c.ShowTitlesOnBackground = bool.Parse(text1);
			}
			text1 = scheme["HasChartLegend"].ToString();
			if (text1.Length > 0)
			{
				c.HasChartLegend = bool.Parse(text1);
			}
			text1 = scheme["GridLines"].ToString();
			if (text1.Length > 0)
			{
				c.GridLines = (GridLines) ChartAutoFormatDialog.gridLinesConverter.ConvertFromString(null, CultureInfo.InvariantCulture, text1);
			}
			text1 = scheme["ChartPadding"].ToString();
			if (text1.Length > 0)
			{
				c.ChartPadding = short.Parse(text1, CultureInfo.InvariantCulture);
			}
			text1 = scheme["Padding"].ToString();
			if (text1.Length > 0)
			{
				c.Padding = short.Parse(text1, CultureInfo.InvariantCulture);
			}
			text1 = scheme["TopPadding"].ToString();
			if (text1.Length > 0)
			{
				c.TopPadding = short.Parse(text1, CultureInfo.InvariantCulture);
			}
		}
 
		private void ConfigureInterior(DataRow scheme, ChartInterior interior, string name)
		{
			string text1 = scheme[name + "-Color"].ToString();
			if (text1.Length > 0)
			{
				interior.Color = ColorTranslator.FromHtml(text1);
			}
			text1 = scheme[name + "-Type"].ToString();
			if (text1.Length > 0)
			{
				interior.Type = (InteriorType) ChartAutoFormatDialog.interiorTypeConverter.ConvertFromString(null, CultureInfo.InvariantCulture, text1);
			}
			text1 = scheme[name + "-HatchStyle"].ToString();
			if (text1.Length > 0)
			{
				interior.HatchStyle = (HatchStyle) ChartAutoFormatDialog.hatchStyleConverter.ConvertFromString(null, CultureInfo.InvariantCulture, text1);
			}
			text1 = scheme[name + "-ForeColor"].ToString();
			if (text1.Length > 0)
			{
				interior.ForeColor = ColorTranslator.FromHtml(text1);
			}
			text1 = scheme[name + "-Angle"].ToString();
			if (text1.Length > 0)
			{
				interior.Angle = float.Parse(text1, CultureInfo.InvariantCulture);
			}
			text1 = scheme[name + "-StartPoint"].ToString();
			if (text1.Length > 0)
			{
				interior.StartPoint = (Point) ChartAutoFormatDialog.pointConverter.ConvertFromString(null, CultureInfo.InvariantCulture, text1);
			}
			text1 = scheme[name + "-EndPoint"].ToString();
			if (text1.Length > 0)
			{
				interior.EndPoint = (Point) ChartAutoFormatDialog.pointConverter.ConvertFromString(null, CultureInfo.InvariantCulture, text1);
			}
		}
 
		private void ConfigureLine(DataRow scheme, ChartLine line, string name)
		{
			string text1 = scheme[name + "-EndCap"].ToString();
			if (text1.Length > 0)
			{
				line.EndCap = (LineCap) ChartAutoFormatDialog.lineCapConverter.ConvertFromString(null, CultureInfo.InvariantCulture, text1);
			}
			text1 = scheme[name + "-DashStyle"].ToString();
			if (text1.Length > 0)
			{
				line.DashStyle = (DashStyle) ChartAutoFormatDialog.dashStyleConverter.ConvertFromString(null, CultureInfo.InvariantCulture, text1);
			}
			text1 = scheme[name + "-StartCap"].ToString();
			if (text1.Length > 0)
			{
				line.StartCap = (LineCap) ChartAutoFormatDialog.lineCapConverter.ConvertFromString(null, CultureInfo.InvariantCulture, text1);
			}
			text1 = scheme[name + "-Color"].ToString();
			if (text1.Length > 0)
			{
				line.Color = ColorTranslator.FromHtml(text1);
			}
			text1 = scheme[name + "-Width"].ToString();
			if (text1.Length > 0)
			{
				line.Width = float.Parse(text1, CultureInfo.InvariantCulture);
			}
			text1 = scheme[name + "-LineJoin"].ToString();
			if (text1.Length > 0)
			{
				line.LineJoin = (LineJoin) ChartAutoFormatDialog.lineJoinConverter.ConvertFromString(null, CultureInfo.InvariantCulture, text1);
			}
		}
 
		private void ConfigureText(DataRow scheme, ChartText text, string name)
		{
			string text1 = scheme[name + "-ForeColor"].ToString();
			if (text1.Length > 0)
			{
				text.ForeColor = ColorTranslator.FromHtml(text1);
			}
			text1 = scheme[name + "-Font"].ToString();
			if (text1.Length > 0)
			{
				text.Font = (Font) ChartAutoFormatDialog.fontConverter.ConvertFromString(null, CultureInfo.InvariantCulture, text1);
			}
			text1 = scheme[name + "-StringFormat"].ToString();
			if (text1.Length > 0)
			{
				text.StringFormat = (StringFormat) ChartAutoFormatDialog.stringFormatConverter.ConvertFromString(null, CultureInfo.InvariantCulture, text1);
			}
		}
 
		protected override void Dispose(bool disposing)
		{
			if (disposing && (this.components != null))
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}
 
		private void InitializeComponent()
		{
			this.cmdOk = new System.Windows.Forms.Button();
			this.cmdCancel = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.lstScheme = new System.Windows.Forms.ListBox();
			this.pictureBoxPreview = new System.Windows.Forms.PictureBox();
			base.SuspendLayout();
			this.cmdOk.DialogResult = DialogResult.OK;
			this.cmdOk.FlatStyle = FlatStyle.System;
			this.cmdOk.Location = new Point(0x198, 0x150);
			this.cmdOk.Name = "cmdOk";
			this.cmdOk.TabIndex = 0;
			this.cmdOk.Text = "OK";
			this.cmdOk.Click += new EventHandler(this.OnCmdOk);
			this.cmdCancel.DialogResult = DialogResult.Cancel;
			this.cmdCancel.FlatStyle = FlatStyle.System;
			this.cmdCancel.Location = new Point(0x1f0, 0x150);
			this.cmdCancel.Name = "cmdCancel";
			this.cmdCancel.TabIndex = 1;
			this.cmdCancel.Text = "Cancel";
			this.cmdCancel.Click += new EventHandler(this.OnCmdCancel);
			this.label1.Location = new Point(8, 4);
			this.label1.Name = "label1";
			this.label1.TabIndex = 2;
			this.label1.Text = "Select a Scheme";
			this.lstScheme.Location = new Point(8, 0x18);
			this.lstScheme.Name = "lstScheme";
			this.lstScheme.Size = new Size(0x98, 0x12f);
			this.lstScheme.TabIndex = 3;
			this.lstScheme.SelectedIndexChanged += new EventHandler(this.OnSchemeSelected);
			this.pictureBoxPreview.BackColor = SystemColors.Window;
			this.pictureBoxPreview.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.pictureBoxPreview.Location = new Point(0xa8, 0x18);
			this.pictureBoxPreview.Name = "pictureBoxPreview";
			this.pictureBoxPreview.Size = new Size(0x1b0, 0x12f);
			this.pictureBoxPreview.TabIndex = 4;
			this.pictureBoxPreview.TabStop = false;
			this.AutoScaleBaseSize = new Size(5, 13);
			base.CancelButton = this.cmdCancel;
			base.ClientSize = new Size(610, 0x170);
			System.Windows.Forms.Control[] controlArray1 = new System.Windows.Forms.Control[] { this.pictureBoxPreview, this.lstScheme, this.label1, this.cmdCancel, this.cmdOk } ;
			base.Controls.AddRange(controlArray1);
			base.FormBorderStyle = FormBorderStyle.FixedDialog;
			base.MaximizeBox = false;
			base.MinimizeBox = false;
			base.Name = "ChartAutoFormatDialog";
			base.StartPosition = FormStartPosition.CenterScreen;
			this.Text = "Auto Format...";
			base.ResumeLayout(false);
		}
 
		private void OnCmdCancel(object sender, EventArgs e)
		{
			base.Close();
		}
 
		private void OnCmdOk(object sender, EventArgs e)
		{
			this.ApplyScheme(this.lstScheme.SelectedItem.ToString(), this._chart);
			base.Close();
		}
 
		private void OnSchemeSelected(object sender, EventArgs e)
		{
			ChartControl control1 = new ChartControl();
			LineChart chart1 = new LineChart();
			chart1.Data.Add(new ChartPoint("Data 1", 1f));
			chart1.Data.Add(new ChartPoint("Data 2", 2f));
			chart1.Data.Add(new ChartPoint("Data 3", 3f));
			chart1.Data.Add(new ChartPoint("Data 4", 4f));
			chart1.Data.Add(new ChartPoint("Data 5", 5f));
			chart1.Data.Add(new ChartPoint("Data 6", 6f));
			control1.Charts.Add(chart1);
			control1.XTitle.Text = "XTitle Text";
			control1.YTitle.Text = "YTitle Text";
			control1.Width = (Unit) this.pictureBoxPreview.Width;
			control1.ChartTitle.Text.Text = "Auto Format Preview";
			control1.Height = (Unit) this.pictureBoxPreview.Height;
			try
			{
				this.ApplyScheme(this.lstScheme.SelectedItem.ToString(), control1);
			}
			catch (ApplicationException exception1)
			{
				MessageBox.Show(exception1.ToString(), "WebChart Designer:", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			control1.RedrawChart();
			this.pictureBoxPreview.Image = System.Drawing.Image.FromFile(control1.FileName);
		}
 
		private void ReadSchemes()
		{
			DataSet set1 = new DataSet();
			set1.Locale = CultureInfo.InvariantCulture;
			set1.ReadXml(new XmlTextReader(new StringReader("still gotta figure this one out")));
			this.schemes = set1.Tables["Schemes"];
			DataColumn[] columnArray1 = new DataColumn[] { this.schemes.Columns["SchemeName"] } ;
			this.schemes.PrimaryKey = columnArray1;
		}
		 

		// Fields
		private ChartControl _chart;
		private ControlDesigner _controlDesigner;
		private System.Windows.Forms.Button cmdCancel;
		private System.Windows.Forms.Button cmdOk;
		private Container components;
		private static EnumConverter dashStyleConverter;
		private static FontConverter fontConverter;
		private static EnumConverter gridLinesConverter;
		private static EnumConverter hatchStyleConverter;
		private static EnumConverter interiorTypeConverter;
		private System.Windows.Forms.Label label1;
		private static EnumConverter lineCapConverter;
		private static EnumConverter lineJoinConverter;
		private System.Windows.Forms.ListBox lstScheme;
		private PictureBox pictureBoxPreview;
		private static PointConverter pointConverter;
		private DataTable schemes;
		private static StringFormatConverter stringFormatConverter;
	}
 
}
