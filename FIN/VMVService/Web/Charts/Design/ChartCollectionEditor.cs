using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.Design;
using System.Web.UI.WebControls;
using System.Drawing;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Collections;
using System.Collections.Specialized;
using System.Windows.Forms;

namespace VMVServices.Web.Charts.Design
{
	public class ChartCollectionEditor : CollectionEditor
	{
		// Methods
		public ChartCollectionEditor(Type type) : base(type)
		{
		}
 
		protected override Type[] CreateNewItemTypes()
		{
			return new Type[] { typeof(LineChart), typeof(ColumnChart), typeof(SmoothLineChart), typeof(StackedColumnChart), typeof(AreaChart), typeof(ScatterChart), typeof(StackedAreaChart), typeof(PieChart) } ;
		}
 
	}
 
}
