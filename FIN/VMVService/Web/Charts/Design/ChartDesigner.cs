using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.Design;
using System.Web.UI.WebControls;
using System.Drawing;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Collections;
using System.Collections.Specialized;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace VMVServices.Web.Charts.Design
{
	internal class ChartDesigner : System.Web.UI.Design.ControlDesigner
	{
		// Methods
		public ChartDesigner()
		{
			this.addedVerbs = false;
		}
 
		public override string GetDesignTimeHtml()
		{
			ChartControl control1 = base.Component as ChartControl;
			if (control1 == null)
			{
				return this.GetEmptyDesignTimeHtml();
			}
			string text1 = string.Empty;
			bool flag1 = false;
			try
			{
				try
				{
					if (control1.Charts.Count == 0)
					{
						flag1 = true;
						LineChart chart1 = new LineChart();
						chart1.Data.Add(new ChartPoint("Data 1", 1f));
						chart1.Data.Add(new ChartPoint("Data 2", 2f));
						chart1.Data.Add(new ChartPoint("Data 3", 3f));
						chart1.Data.Add(new ChartPoint("Data 4", 4f));
						chart1.Data.Add(new ChartPoint("Data 5", 5f));
						chart1.Data.Add(new ChartPoint("Data 6", 6f));
						control1.Charts.Add(chart1);
					}
					return base.GetDesignTimeHtml();
				}
				catch (Exception exception1)
				{
					throw new ApplicationException("Error while creating the Preview.", exception1);
				}
				finally
				{
					if (flag1)
					{
						control1.Charts.Clear();
					}
				}
			}
			catch (ApplicationException exception2)
			{
				text1 = this.GetErrorDesignTimeHtml(exception2);
			}
			return text1;
		}
 
		protected override string GetErrorDesignTimeHtml(Exception ex)
		{
			return base.CreatePlaceHolderDesignTimeHtml("There was an error rendering the Chart." + ex.ToString());
		}
 
		public void OnAutoFormat(object sender, EventArgs e)
		{
			ChartAutoFormatDialog dialog1 = new ChartAutoFormatDialog(this);
			if (this.ShowDialog(base.Component.Site, dialog1) == DialogResult.OK)
			{
				IComponentChangeService service1 = null;
				service1 = (IComponentChangeService) base.Component.Site.GetService(typeof(IComponentChangeService));
				try
				{
					if (service1 == null)
					{
						return;
					}
					try
					{
						service1.OnComponentChanging(base.Component, null);
					}
					catch (CheckoutException exception1)
					{
						if (exception1 != CheckoutException.Canceled)
						{
							throw;
						}
						return;
					}
				}
				finally
				{
					if (service1 != null)
					{
						service1.OnComponentChanged(base.Component, null, null, null);
					}
				}
			}
		}
 
		private DialogResult ShowDialog(IServiceProvider serviceProvider, Form form)
		{
			if (serviceProvider != null)
			{
				IUIService service1 = (IUIService) serviceProvider.GetService(typeof(IUIService));
				if (service1 != null)
				{
					return service1.ShowDialog(form);
				}
			}
			return form.ShowDialog();
		}
 

		// Properties
		public override DesignerVerbCollection Verbs
		{
			get
			{
				DesignerVerbCollection collection1 = base.Verbs;
				if (!this.addedVerbs)
				{
					DesignerVerb verb1 = new DesignerVerb("Auto Format...", new EventHandler(this.OnAutoFormat));
					collection1.Add(verb1);
					this.addedVerbs = true;
				}
				return collection1;
			}
		}
 

		// Fields
		private bool addedVerbs;
	}
 
}
