using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Collections;
using System.Collections.Specialized;
using System.Globalization;


namespace VMVServices.Web.Charts
{
	public class AreaChart : Chart
	{
		// Methods
		public AreaChart(){}
		public AreaChart(ChartPointCollection data) : base(data)
		{
		}
 
		public AreaChart(ChartPointCollection data, Color color) : base(data, color)
		{
		}
 
		public override void Render(Graphics graphics, int width, int height)
		{
			ChartEngine engine1 = base.Engine;
			float single1 = engine1.scaleX;
			float single2 = engine1.scaleY;
			Point[] pointArray1 = new Point[base.Data.Count + 2];
			GraphicsPath path1 = new GraphicsPath();
			bool flag1 = (base.shadow != null) && base.Shadow.Visible;
			GraphicsPath path2 = null;
			if (flag1)
			{
				path2 = new GraphicsPath();
			}
			int num1 = 0;
			float single3 = single1 / 2f;
			pointArray1[num1++] = new Point((int) single3, 0);
			foreach (ChartPoint point1 in ((IEnumerable) base.Data))
			{
				int num2 = (int) (point1.yvalue * single2);
				int num3 = (int) ((num1 * single1) - single3);
				if (base.showLineMarkers)
				{
					base.lineMarker.Render(graphics, num3, num2);
				}
				pointArray1[num1] = new Point(num3, num2);
				if (num1 > 0)
				{
					if (flag1)
					{
						path2.AddLine((int) (pointArray1[num1 - 1].X + base.Shadow.OffsetX), (int) (pointArray1[num1 - 1].Y + base.Shadow.OffsetY), (int) (pointArray1[num1].X + base.Shadow.OffsetX), (int) (pointArray1[num1].Y + base.Shadow.OffsetY));
					}
					path1.AddLine(pointArray1[num1 - 1], pointArray1[num1]);
				}
				num1++;
			}
			pointArray1[num1] = new Point(pointArray1[num1 - 1].X, 0);
			path1.AddLine(pointArray1[num1 - 1], pointArray1[num1]);
			if (flag1)
			{
				SolidBrush brush1 = new SolidBrush(base.Shadow.Color);
				path2.AddLine(pointArray1[num1 - 1].X, pointArray1[num1 - 1].Y, pointArray1[num1].X, pointArray1[num1].Y);
				graphics.FillPath(brush1, path2);
				brush1.Dispose();
			}
			Brush brush2 = base.Fill.GetBrush(graphics);
			graphics.FillPath(brush2, path1);
			brush2.Dispose();
			Pen pen1 = base.Line.GetPen(graphics);
			graphics.DrawPath(pen1, path1);
			pen1.Dispose();
			if (base.dataLabels.visible)
			{
				base.RenderDataLabels(graphics);
			}
		}
 
	}
 
}
