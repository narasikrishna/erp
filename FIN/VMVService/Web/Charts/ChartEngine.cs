using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Drawing.Imaging;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Collections;
using System.Collections.Specialized;
using System.Globalization;
using System.Diagnostics;
using System.IO;

namespace VMVServices.Web.Charts
{
	public class ChartEngine
	{
		// Methods
		public ChartEngine()
		{
			this.chartPadding = 10;
			this.titlePadding = 5;
			this.leftChartPadding = 10;
			this.topChartPadding = 0;
			this.bottomChartPadding = 0;
			this.rightChartPadding = 0;
			this.padding = 10;
			this.topPadding = 20;
			this.yvaluesInterval = 0f;
			this.xvaluesInterval = 0;
			this.xticksInterval = 0;
			this.ycustomStart = 0f;
			this.ycustomEnd = 0f;
			this.maxPoints = 0;
			this.border = new ChartLine(Color.Black);
			this.background = new ChartInterior();
			this.plotBackground = new ChartInterior();
			this.legend = new ChartLegend();
			this.chartTitle = new ChartTitle();
			StringFormat format1 = new StringFormat();
			format1.Alignment = StringAlignment.Far;
			format1.FormatFlags = StringFormatFlags.LineLimit;
			this.yaxisFont = new ChartText(format1);
			this.xaxisFont = new ChartText();
		}
 
		private void CalculateRects()
		{
			this.controlRect = new Rectangle(0, 0, this.size.Width, this.size.Height);
			int _doublePadding = this.padding * 2;
			int _paddedWidth = this.size.Width - _doublePadding;
			int _paddedHeight = (this.size.Height - _doublePadding) - this.topPadding;
			int _padding = this.padding;
			int _paddedTopPadding = this.topPadding + this.padding;
			int _doubleChartPadding = 2 * this.chartPadding;

			if (this.chartTitle.Text.Text != null && this.chartTitle.Text.Text.Length != 0)
			{
				//currently the Title is forced to be at the top.
				this.titleRect = new Rectangle(this.padding, this.padding, _paddedWidth, this.chartTitle.Heigth);
				_paddedTopPadding = this.titleRect.Height + this.padding*2;
				_paddedHeight = (this.size.Height - _doublePadding) - this.titleRect.Height - this.padding;
			}

			if (this.hasChartLegend)
			{
				int _paddedLegendWidth = this.legend.Width + this.padding;
				if (this.legend.Position == LegendPosition.Right)
				{
					_paddedWidth -= _paddedLegendWidth;
					this.legendRect = new Rectangle((this.padding + _paddedWidth) + this.padding, _paddedTopPadding, this.legend.Width, _paddedHeight);
				}
				else if (this.legend.Position == LegendPosition.Left)
				{
					_paddedWidth -= _paddedLegendWidth;
					_padding = _doublePadding + this.legend.Width;
					_paddedTopPadding = this.topPadding + this.padding;
					this.legendRect = new Rectangle(this.padding, _paddedTopPadding, this.legend.Width, _paddedHeight);
				}
				else if (this.legend.Position == LegendPosition.Top)
				{
					_paddedHeight -= _paddedLegendWidth;
					_padding = this.padding;
					this.legendRect = new Rectangle(_padding, _paddedTopPadding, _paddedWidth, this.legend.Width);
					_paddedTopPadding = this.legendRect.Bottom + this.padding;
				}
				else if (this.legend.Position == LegendPosition.Bottom)
				{
					_paddedHeight -= _paddedLegendWidth;
					this.legendRect = new Rectangle(_padding, (_paddedTopPadding + _paddedHeight) + this.padding, _paddedWidth, this.legend.Width);
				}
			}
			this.plotRect = new Rectangle(_padding, _paddedTopPadding, _paddedWidth, _paddedHeight);
			this.chartRect = new Rectangle((_padding + this.chartPadding) + this.leftChartPadding, (_paddedTopPadding + this.chartPadding) + this.topChartPadding, ((this.plotRect.Width - _doubleChartPadding) - this.leftChartPadding) - this.rightChartPadding, ((this.plotRect.Height - _doubleChartPadding) - this.topChartPadding) - this.bottomChartPadding);
		}
 
		private Bitmap CreateBitmap()
		{
			if (this.size.Width <= 0)
			{
				this.size.Width = 1;
			}
			if (this.size.Height <= 0)
			{
				this.size.Height = 1;
			}
			return new Bitmap(this.size.Width, this.size.Height, PixelFormat.Format24bppRgb);
		}
 
		private void DetermineScale(IEnumerator enumCharts, float width, float height, out Chart maxPointsChart, out float minPointValue, out float maxPointValue)
		{
			maxPointsChart = null;
			enumCharts.Reset();
			this.maxPoints = -2147483648;
			float _minPointValue = float.MaxValue;
			float _maxPointValue = float.MinValue;
			int minXValue = 0;
			int maxXValue = 0;

			while (enumCharts.MoveNext())
			{
				Chart chart1 = (Chart) enumCharts.Current;
				if (chart1.Data != null)
				{
					chart1.GetMinMaxXValue(out minXValue, out maxXValue);
					if (maxXValue > this.maxPoints)
					{
						maxPointsChart = chart1;
						this.maxPoints = maxXValue;
					}
					float dataMinValue;
					float dataMeanValue;
					float dataMaxValue = 0f;
					chart1.GetMinMaxMeanValue(out dataMinValue, out dataMaxValue, out dataMeanValue);
					if (dataMinValue < _minPointValue)
					{
						_minPointValue = dataMinValue;
					}
					if (dataMaxValue > _maxPointValue)
					{
						_maxPointValue = dataMaxValue;
					}
				}
			}
			if (maxPointsChart == null)
			{
				this.showXValues = false;
			}
			if (this.maxPoints <= 0)
			{
				this.maxPoints = 1;
			}
			this.scaleX = width / ((float) this.maxPoints);
			if (this.ycustomEnd != 0f)
			{
				_maxPointValue = this.YCustomEnd;
			}
			if (this.ycustomStart != 0f)
			{
				_minPointValue = this.ycustomStart;
			}
			else if (_minPointValue > 0f)
			{
				_minPointValue = 0f;
			}
			if (_maxPointValue == _minPointValue)
			{
				if (_maxPointValue > 0f)
				{
					_minPointValue = 0f;
				}
				if (_maxPointValue < 0f)
				{
					_maxPointValue = 0f;
				}
				if (_maxPointValue == 0f)
				{
					_maxPointValue = _minPointValue + 100f;
				}
			}
			if (_maxPointValue <= _minPointValue)
			{
				_maxPointValue = _minPointValue + 100f;
			}
			this.scaleY = -1f * (height / (_maxPointValue - _minPointValue));
			minPointValue = _minPointValue;
			maxPointValue = _maxPointValue;
		}
 
		private void DrawAxisTitles(Graphics graphics)
		{
			int num1 = this.padding * 2;
			if ((this.ytitle != null) && (this.ytitle.Text.Length > 0))
			{
				Rectangle rectangle1;
				Brush brush1 = new SolidBrush(this.ytitle.ForeColor);
				if (this.showTitlesOnBackground)
				{
					rectangle1 = new Rectangle(this.plotRect.Left - this.padding, this.plotRect.Top, num1, this.plotRect.Height - num1);
				}
				else
				{
					rectangle1 = new Rectangle(this.plotRect.Left, this.plotRect.Top, this.chartPadding + this.leftChartPadding, this.chartRect.Height);
				}
				graphics.DrawString(this.ytitle.Text, this.ytitle.Font, brush1, (RectangleF) rectangle1, this.ytitle.StringFormat);
				brush1.Dispose();
			}
			if ((this.xtitle != null) && (this.xtitle.Text.Length > 0))
			{
				Rectangle rectangle2;
				Brush brush2 = new SolidBrush(this.xtitle.ForeColor);
				if (this.showTitlesOnBackground)
				{
					rectangle2 = new Rectangle(this.plotRect.Left, this.plotRect.Bottom, this.plotRect.Width, num1);
				}
				else
				{
					rectangle2 = new Rectangle(this.chartRect.Left, this.chartRect.Bottom, this.chartRect.Width, this.chartPadding);
				}
				graphics.DrawString(this.xtitle.Text, this.xtitle.Font, brush2, (RectangleF) rectangle2, this.xtitle.StringFormat);
				brush2.Dispose();
			}
		}
 
		private void DrawBackground(Graphics graphics)
		{
			graphics.Clear(Color.White);
			Brush brush1 = this.background.GetBrush(graphics);
			graphics.FillRectangle(brush1, this.controlRect);
			brush1.Dispose();
		}
 
		private void DrawBorder(Graphics graphics, Pen pen, Rectangle rectChart)
		{
			graphics.DrawRectangle(pen, rectChart);
		}
 
		private void DrawCharts(Graphics graphics, IEnumerator enumCharts)
		{
			enumCharts.Reset();
			while (enumCharts.MoveNext())
			{
				((Chart) enumCharts.Current).Render(graphics, this.chartRect.Width, this.chartRect.Height);
			}
		}
 
		private void DrawHorizontalBorders(Graphics graphics, Pen penBorder, Chart maxPointsChart, int width, int height, float minPoint, float maxPoint)
		{
			graphics.DrawLine(penBorder, (float) 0f, (float) (minPoint * this.scaleY), (float) 0f, (float) (maxPoint * this.scaleY));
			Pen gridLinePen = new Pen(Color.LightGray, 1);

			if (this.gridLines != GridLines.None)
			{
				graphics.DrawLine(gridLinePen, (float) width, (float) (minPoint * this.scaleY), (float) width, (float) (maxPoint * this.scaleY));
				graphics.DrawLine(gridLinePen, (float) 0f, (float) (maxPoint * this.scaleY), (float) width, (float) (maxPoint * this.scaleY));
			}

			if (maxPointsChart != null)
			{
				int num1 = -2;
				if (minPoint > 0f)
				{
					num1 = ((int) (minPoint * this.scaleY)) - 2;
				}
				Brush blackBrush = Brushes.Black;
				int _recommendedInterval = this.RecommendedInterval;
				int num3 = (this.xticksInterval <= 0) ? 1 : this.xticksInterval;
				float single1 = minPoint * this.scaleY;
				int num4 = num3 - 1;
				for (int num5 = 0; num5 <= this.maxPoints; num5 += _recommendedInterval)
				{
					float xPosition = num5 * this.scaleX;
					if ((this.gridLines == GridLines.Both) || (this.gridLines == GridLines.Vertical))
					{
						if (xPosition != 0)
							graphics.DrawLine(gridLinePen, xPosition, single1, xPosition, (float) (single1 - height));
					}
					if (this.gridLines != GridLines.None && this.showXAxisNodes)
					{
						num4++;
						if (num4 >= num3)
						{
							graphics.FillRectangle(blackBrush, (float) (xPosition - 1f), (float) num1, (float) 1f, (float) 2f);
							num4 = 0;
						}
					}
				}
			}
		}
 
		private void DrawLegend(Graphics graphics, Brush brush, IEnumerator enumCharts)
		{
			graphics.ResetTransform();

			//paint the background
			Brush backgroundBrush = this.legend.Background.GetBrush(graphics);
			graphics.FillRectangle(backgroundBrush, this.legendRect);
			backgroundBrush.Dispose();

			//draw the border
			Pen borderPen = this.legend.Border.GetPen(graphics);
			graphics.DrawRectangle(borderPen, this.legendRect);
			borderPen.Dispose();

			graphics.SetClip(this.legendRect, CombineMode.Replace);
			graphics.TranslateTransform((float) this.legendRect.Left, (float) this.legendRect.Top);
			int num1 = 10;
			Font legendFont = this.legend.Font;
			SizeF ef1 = graphics.MeasureString("CAM", legendFont, 0);
			int num2 = (int) (ef1.Height * 0.85f);
			int num3 = 7;

			if ((this.legend.Position == LegendPosition.Right) || (this.legend.Position == LegendPosition.Left))
			{
				int num4 = (this.legend.Width - 0x19) - 10;
				enumCharts.Reset();
				while (enumCharts.MoveNext())
				{
					Chart chart1 = (Chart) enumCharts.Current;
					if (chart1.showLegend)
					{
						if (!(chart1 is PieChart))
						{
							string legendText = chart1.Legend;
							SizeF ef2 = graphics.MeasureString(legendText, legendFont, num4, StringFormat.GenericDefault);
							Brush brush2 = chart1.Fill.GetBrush(graphics);
							Pen pen2 = chart1.Line.GetPen(graphics);
							this.DrawLegendItem(graphics, legendText, brush2, pen2, 10, num2, legendFont, brush, num1, ef2, StringFormat.GenericDefault);
							num1 += (((ef2.Height > num2) ? ((int) ef2.Height) : num2) + num3);
							brush2.Dispose();
							pen2.Dispose();
							continue;
						}
						PieChart chart2 = (PieChart) chart1;
						Color[] colorArray1 = chart2.Colors;
						int num5 = 0;
						int num6 = colorArray1.Length - 1;
						Pen pen3 = chart1.Line.GetPen(graphics);
						foreach (ChartPoint point1 in ((IEnumerable) chart2.Data))
						{
							if (num5 > num6)
							{
								num5 = 0;
							}
							Color color1 = colorArray1[num5++];
							string text2 = point1.XValue;
							SizeF ef3 = graphics.MeasureString(text2, legendFont, num4, StringFormat.GenericDefault);
							Brush brush3 = new SolidBrush(color1);
							this.DrawLegendItem(graphics, text2, brush3, pen3, 10, num2, legendFont, brush, num1, ef3, StringFormat.GenericDefault);
							num1 += (((ef3.Height > num2) ? ((int) ef3.Height) : num2) + num3);
							brush3.Dispose();
						}
						pen3.Dispose();
					}
				}
			}
			else
			{
				int num7 = this.legend.Width / (num2 + 7);
				if (num7 <= 0)
				{
					num7 = 1;
				}
				int num8 = this.GetNumberOfLegends(enumCharts) + 1;
				int num9 = (int) Math.Ceiling((double) (((float) num8) / ((float) num7)));
				if (num9 <= 0)
				{
					num9 = 1;
				}
				int num10 = this.legendRect.Width / num9;
				int num11 = 0;
				int num12 = num3;
				enumCharts.Reset();
				StringFormat format1 = new StringFormat();
				format1.Trimming = StringTrimming.EllipsisCharacter;
				format1.LineAlignment = StringAlignment.Near;
				format1.Alignment = StringAlignment.Near;
				format1.FormatFlags = StringFormatFlags.NoWrap;
				while (enumCharts.MoveNext())
				{
					Chart chart3 = (Chart) enumCharts.Current;
					if (chart3.showLegend)
					{
						if (!(chart3 is PieChart))
						{
							if (num11 == num9)
							{
								num1 += (num2 + num12);
								num12 = num3;
								num11 = 0;
							}
							int num13 = (num11 * num10) + 10;
							string text3 = chart3.Legend;
							SizeF ef4 = graphics.MeasureString(text3, legendFont, (int) ((num10 - num2) - 5), format1);
							if (num12 < (ef4.Height - 5f))
							{
								num12 = ((int) ef4.Height) - 5;
							}
							Brush brush4 = chart3.Fill.GetBrush(graphics);
							Pen pen4 = chart3.Line.GetPen(graphics);
							this.DrawLegendItem(graphics, text3, brush4, pen4, num13, num2, legendFont, brush, num1, ef4, format1);
							brush4.Dispose();
							pen4.Dispose();
							num11++;
							continue;
						}
						PieChart chart4 = (PieChart) chart3;
						Color[] colorArray2 = chart4.Colors;
						int num14 = 0;
						int num15 = colorArray2.Length - 1;
						Pen pen5 = chart3.Line.GetPen(graphics);
						foreach (ChartPoint point2 in ((IEnumerable) chart4.Data))
						{
							if (num14 > num15)
							{
								num14 = 0;
							}
							Color color2 = colorArray2[num14++];
							if (num11 == num9)
							{
								num1 += (num2 + num12);
								num12 = num3;
								num11 = 0;
							}
							int num16 = (num11 * num10) + 10;
							string text4 = point2.XValue;
							SizeF ef5 = graphics.MeasureString(text4, legendFont, (int) ((num10 - num2) - 5), format1);
							if (num12 < (ef5.Height - 5f))
							{
								num12 = ((int) ef5.Height) - 5;
							}
							Brush brush5 = new SolidBrush(color2);
							this.DrawLegendItem(graphics, text4, brush5, pen5, num16, num2, legendFont, brush, num1, ef5, format1);
							brush5.Dispose();
							num11++;
						}
						pen5.Dispose();
					}
				}
			}
		}
 
		private void DrawLegendItem(Graphics graphics, string legend, Brush glyphBrush, Pen glyphPen, int glyphLeft, int glyphHeight, Font fontLegend, Brush brush, int startY, SizeF textSize, StringFormat format)
		{
			graphics.FillRectangle(glyphBrush, glyphLeft, (int) (startY + 2), glyphHeight, glyphHeight);
			graphics.DrawRectangle(glyphPen, glyphLeft, (int) (startY + 1), glyphHeight, glyphHeight);
			RectangleF ef1 = new RectangleF(glyphLeft + 15f, (float) startY, textSize.Width, textSize.Height);
			graphics.DrawString(legend, fontLegend, brush, ef1, format);
		}
 
		private void DrawPlotBackground(Graphics graphics, Rectangle rectChart)
		{
			Brush brush1 = this.plotBackground.GetBrush(graphics);
			if (brush1 == null)
			{
				brush1 = Brushes.Black;
			}
			graphics.FillRectangle(brush1, rectChart);
			brush1.Dispose();
		}
 
		
		private void DrawTitle(Graphics graphics)
		{
			graphics.ResetTransform();

			//paint the background
			Brush backgroundBrush = this.chartTitle.Background.GetBrush(graphics);
			graphics.FillRectangle(backgroundBrush, this.titleRect);
			backgroundBrush.Dispose();

			//draw the border
			Pen borderPen = this.chartTitle.Border.GetPen(graphics);
			graphics.DrawRectangle(borderPen, this.titleRect);
			borderPen.Dispose();

			//graphics.SetClip(this.titleRect, CombineMode.Replace);
			//graphics.TranslateTransform((float) this.legendRect.Left, (float) this.legendRect.Top);

			int doublePadding = this.padding * 2;
			Brush titleBrush = new SolidBrush(this.chartTitle.Text.ForeColor);
			
			graphics.DrawString(this.chartTitle.Text.Text, 
				this.chartTitle.Text.Font, 
				titleBrush, 
				(RectangleF) new Rectangle(this.titleRect.X + this.titlePadding, 
											this.titleRect.Y + this.titlePadding, 
											this.titleRect.Width - this.titlePadding*2, 
											this.titleRect.Height - this.titlePadding*2), 
				this.chartTitle.Text.StringFormat);

			titleBrush.Dispose();
		}
 
		private void DrawVerticalBorders(Graphics graphics, Pen penBorder, int width, float minPoint, float maxPoint, float interval)
		{
			float yPosition = minPoint * this.scaleY;
			Pen gridLinePen = new Pen(Color.LightGray, 1);
			
			graphics.DrawLine(penBorder, (float) 0f, yPosition, (float) width, yPosition);
			
			if ((this.gridLines == GridLines.Horizontal) || (this.gridLines == GridLines.Both))
			{
				if (minPoint < 0f)
				{
					for (float single2 = 0f; single2 <= maxPoint; single2 += interval)
					{
						yPosition = single2 * this.scaleY;
						if (yPosition != 0)
							graphics.DrawLine(gridLinePen, (float) 0f, yPosition, (float) width, yPosition);
					}
					for (float single3 = 0f; single3 >= minPoint; single3 -= interval)
					{
						yPosition = single3 * this.scaleY;
						if (yPosition != 0)
							graphics.DrawLine(gridLinePen, (float) 0f, yPosition, (float) width, yPosition);
					}
				}
				else
				{
					for (float single4 = minPoint; single4 <= maxPoint; single4 += interval)
					{
						yPosition = single4 * this.scaleY;
						if (yPosition != 0)
							graphics.DrawLine(gridLinePen, (float) 0f, yPosition, (float) width, yPosition);
					}
				}
			}
			else
			{
				//draw the little stubs on the Y axis indicating visually the X interval
				if (minPoint < 0f)
				{
					for (float single2 = 0f; single2 <= maxPoint; single2 += interval)
					{
						yPosition = single2 * this.scaleY;
						graphics.DrawLine(gridLinePen, (float) -1f, yPosition, (float) 0f, yPosition);
					}
					for (float single3 = 0f; single3 >= minPoint; single3 -= interval)
					{
						yPosition = single3 * this.scaleY;
						graphics.DrawLine(gridLinePen, (float) -1f, yPosition, (float) 0f, yPosition);
					}
				}
				else
				{
					for (float single4 = minPoint; single4 <= maxPoint; single4 += interval)
					{
						yPosition = single4 * this.scaleY;
						graphics.DrawLine(gridLinePen, (float) -1f, yPosition, (float) 0f, yPosition);
					}
				}
			}
		}
 
		private void DrawXValues(Graphics graphics, Brush brush, Chart maxPointsChart, float minPoint)
		{
			if (maxPointsChart != null)
			{
				Font xAxisFont = this.xaxisFont.Font;
				int xValuesInterval = (this.xvaluesInterval <= 0) ? 1 : this.xvaluesInterval;
				int xValuesIntervalMinusOne = xValuesInterval - 1;
				if (minPoint < 0f)
				{
					minPoint = 0f;
				}
				ChartPointCollection chartPoints = maxPointsChart.Data;
				float textYPosition = minPoint * this.scaleY + this.xAxisLabelPadding;
				float textWidth = this.scaleX * xValuesInterval;
				int chartPointsCount = chartPoints.Count;
				int _maxPoints = Math.Max(this.maxPoints, chartPointsCount);
				ScatterChart chart1 = maxPointsChart as ScatterChart;
				if (chart1 != null)
				{
					textWidth = Math.Min(textWidth, (float) ((this.Size.Width / (_maxPoints + 1)) * xValuesInterval));
				}
				for (int cntr = 0; cntr <= _maxPoints; cntr++)
				{
					if (chartPointsCount > cntr)
					{
						xValuesIntervalMinusOne++;
						if (xValuesIntervalMinusOne >= xValuesInterval)
						{
							float single3;
							RectangleF drawStringrect;
							if (chart1 != null)
							{
								single3 = this.scaleX * (chart1.GetXValueForData(chartPoints[cntr].xvalue) - 1f);
							}
							else
							{
								single3 = cntr * this.scaleX;
							}
							if (!this.renderHorizontally)
							{
								drawStringrect = new RectangleF(new PointF(single3 - 1f, textYPosition), new SizeF(textWidth, (float) (this.chartPadding + this.bottomChartPadding)));
							}
							else
							{
								drawStringrect = new RectangleF(new PointF(single3 - 1f, textYPosition), new SizeF(textWidth, (float) (this.chartPadding + this.leftChartPadding)));
							}
							graphics.DrawString(chartPoints[cntr].xvalue, xAxisFont, brush, drawStringrect, this.xaxisFont.StringFormat);
							xValuesIntervalMinusOne = 0;
						}
					}
				}
			}
		}
 
		private void DrawYValues(Graphics graphics, float minPoint, float maxPoint, float interval)
		{
			Font yAxisFont = this.yaxisFont.Font;
			Brush yAxisBrush = new SolidBrush(this.yaxisFont.ForeColor);
			string textFormat = (this.yvaluesFormat == null) ? "{0:0}" : this.yvaluesFormat;
			StringFormat yAxisStringFormat = this.yaxisFont.StringFormat;
			float textXPosition = (-1f + (-1*this.yAxisLabelPadding));
			if (minPoint < 0f)
			{
				for (float yVal1 = 0f; yVal1 <= maxPoint; yVal1 += interval)
				{
					graphics.DrawString(string.Format(textFormat, yVal1, CultureInfo.InvariantCulture), yAxisFont, yAxisBrush, textXPosition, yVal1 * this.scaleY, yAxisStringFormat);
				}
				for (float yVal2 = 0f; yVal2 >= minPoint; yVal2 -= interval)
				{
					graphics.DrawString(string.Format(textFormat, yVal2, CultureInfo.InvariantCulture), yAxisFont, yAxisBrush, textXPosition, yVal2 * this.scaleY, yAxisStringFormat);
				}
			}
			else
			{
				for (float yVal3 = minPoint; yVal3 <= maxPoint; yVal3 += interval)
				{
					graphics.DrawString(string.Format(textFormat, yVal3, CultureInfo.InvariantCulture), yAxisFont, yAxisBrush, textXPosition, yVal3 * this.scaleY, yAxisStringFormat);
				}
			}
			yAxisBrush.Dispose();
		}
 
		private Bitmap GenerateBitmap()
		{
			this.scatterMinDateTime = DateTime.MaxValue;
			this.scatterMaxDateTime = DateTime.MinValue;
			this.scatterMinNumber = float.MaxValue;
			this.CalculateRects();
			Bitmap theBitmap = this.CreateBitmap();
			Graphics bitmapGraphics = Graphics.FromImage(theBitmap);
			try
			{
				float minPointValue;
				float maxPointValue;

				bitmapGraphics.SmoothingMode = SmoothingMode.AntiAlias;
				bitmapGraphics.TextRenderingHint = TextRenderingHint.SystemDefault;

				this.DrawBackground(bitmapGraphics);

				if (this.chartTitle.Text != null)
					this.DrawTitle(bitmapGraphics);

				this.DrawPlotBackground(bitmapGraphics, this.plotRect);

				Pen borderPen = this.border.GetPen(bitmapGraphics);
				this.DrawBorder(bitmapGraphics, borderPen, this.plotRect);

				this.DrawAxisTitles(bitmapGraphics);

				IEnumerator chartEnumerator = ((IEnumerable) this.charts).GetEnumerator();
				Chart chart = null;

				if (!this.renderHorizontally)
					this.DetermineScale(chartEnumerator, (float) this.chartRect.Width, (float) this.chartRect.Height, out chart, out minPointValue, out maxPointValue);
				else
					this.DetermineScale(chartEnumerator, (float) this.chartRect.Height, (float) this.chartRect.Width, out chart, out minPointValue, out maxPointValue);
				
				Rectangle _chartRect = this.chartRect;
				_chartRect.Inflate(2, 2);
				bitmapGraphics.SetClip(_chartRect, CombineMode.Replace);
				float single3 = this.chartRect.Bottom - (minPointValue * this.scaleY);
				
				if (this.renderHorizontally)
				{
					single3 = this.chartRect.Top;
					bitmapGraphics.RotateTransform(90f);
				}
				
				bitmapGraphics.TranslateTransform((float) this.chartRect.X, single3, MatrixOrder.Append);
				float _yValuesInterval = this.yvaluesInterval;
				
				if (_yValuesInterval <= 0f)
				{
					_yValuesInterval = (maxPointValue - minPointValue) / 10f;
					if (_yValuesInterval == 0f)
						_yValuesInterval = 1f;
					
					if (_yValuesInterval < 1f)
						this.yvaluesFormat = "{0:n}";
				}

				if (!this.RenderHorizontally)
					this.DrawVerticalBorders(bitmapGraphics, borderPen, this.chartRect.Width, minPointValue, maxPointValue, _yValuesInterval);
				else
					this.DrawVerticalBorders(bitmapGraphics, borderPen, this.chartRect.Height, minPointValue, maxPointValue, _yValuesInterval);
                
				Brush _xAxisBrush = new SolidBrush(this.xaxisFont.ForeColor);
				
				if (!this.renderHorizontally)
					this.DrawHorizontalBorders(bitmapGraphics, borderPen, chart, this.chartRect.Width, this.chartRect.Height, minPointValue, maxPointValue);
				else
					this.DrawHorizontalBorders(bitmapGraphics, borderPen, chart, this.chartRect.Height, this.chartRect.Width, minPointValue, maxPointValue);

				this.DrawCharts(bitmapGraphics, chartEnumerator);
				
				bitmapGraphics.ResetClip();
				
				if (this.showXValues)
					this.DrawXValues(bitmapGraphics, _xAxisBrush, chart, minPointValue);

				if (this.showYValues)
					this.DrawYValues(bitmapGraphics, minPointValue, maxPointValue, _yValuesInterval);

				if (this.hasChartLegend)
					this.DrawLegend(bitmapGraphics, _xAxisBrush, chartEnumerator);

				_xAxisBrush.Dispose();
			}
			catch (Exception exception1)
			{
				EventLog.WriteEntry("Application", exception1.ToString() + "\n" + exception1.StackTrace);
				throw exception1;
			}
			finally
			{
				bitmapGraphics.Dispose();
			}
			return theBitmap;
		}
 
		public Bitmap GenerateErrorBitmap(string errorMessage)
		{
			Bitmap bitmap1 = this.CreateBitmap();
			Graphics graphics1 = Graphics.FromImage(bitmap1);
			Rectangle rectangle1 = new Rectangle(10, 10, this.size.Width - 20, this.size.Height - 20);
			graphics1.FillRectangle(Brushes.White, rectangle1);
			StringFormat format1 = new StringFormat();
			format1.Trimming = StringTrimming.Word;
			graphics1.DrawString(errorMessage, ChartText.DefaultFont, Brushes.Red, (RectangleF) rectangle1, format1);
			graphics1.Dispose();
			return bitmap1;
		}
 
		public Bitmap GetBitmap()
		{
			return this.GenerateBitmap();
		}
 
		private int GetNumberOfLegends(IEnumerator enumCharts)
		{
			int num1 = 0;
			enumCharts.Reset();
			while (enumCharts.MoveNext())
			{
				Chart chart1 = (Chart) enumCharts.Current;
				if (chart1.ShowLegend)
				{
					if (!(chart1 is PieChart))
					{
						num1++;
						continue;
					}
					num1 += chart1.Data.Count;
				}
			}
			return num1;
		}
 
		public void SaveToFile(string filename, ImageFormat format)
		{
			if (File.Exists(filename))
			{
				File.Delete(filename);
			}
			Bitmap bitmap1 = null;
			try
			{
				try
				{
					bitmap1 = this.GenerateBitmap();
					if (bitmap1 == null)
					{
						bitmap1 = this.GenerateErrorBitmap("The bitmap was not generated correctly, Call your administrator and tell them to verify the error in the event log.");
					}
					bitmap1.Save(filename, format);
				}
				catch (Exception exception1)
				{
					throw new ApplicationException("Error while saving the file:", exception1);
				}
				finally
				{
					if (bitmap1 != null)
					{
						bitmap1.Dispose();
					}
				}
			}
			catch (ApplicationException exception2)
			{
				Trace.Write("Exception Occurred:" + exception2.ToString());
				Trace.Write("StackTrace:" + exception2.StackTrace);
				bitmap1 = this.GenerateErrorBitmap(exception2.ToString());
				bitmap1.Save(filename, format);
			}
			bitmap1 = null;
		}
 

		// Properties
		#region public properties
		public ChartInterior Background
		{
			get
			{
				return this.background;
			}
			set
			{
				this.background = value;
			}
		}
 
		public ChartLine Border
		{
			get
			{
				return this.border;
			}
			set
			{
				this.border = value;
			}
		}
 
		public short BottomChartPadding
		{
			get
			{
				return this.bottomChartPadding;
			}
			set
			{
				this.bottomChartPadding = value;
			}
		}
 
		public short ChartPadding
		{
			get
			{
				return this.chartPadding;
			}
			set
			{
				this.chartPadding = value;
			}
		}
		public short TitlePadding
		{
			get
			{
				return this.titlePadding;
			}
			set
			{
				this.titlePadding = value;
			}
		}
 
		public ChartCollection Charts
		{
			get
			{
				return this.charts;
			}
			set
			{
				this.charts = value;
			}
		}

		public ChartTitle ChartTitle
		{
			get
			{
				return this.chartTitle;
			}
			set
			{
				this.chartTitle = value;
			}
		}

		public GridLines GridLines
		{
			get
			{
				return this.gridLines;
			}
			set
			{
				this.gridLines = value;
			}
		}
 
		public bool HasChartLegend
		{
			get
			{
				return this.hasChartLegend;
			}
			set
			{
				this.hasChartLegend = value;
			}
		}
 

		public short LeftChartPadding
		{
			get
			{
				return this.leftChartPadding;
			}
			set
			{
				this.leftChartPadding = value;
			}
		}
 
		public ChartLegend Legend
		{
			get
			{
				return this.legend;
			}
			set
			{
				this.legend = value;
			}
		}
 
		internal int MaxPoints
		{
			get
			{
				return this.maxPoints;
			}
		}
 
		public short Padding
		{
			get
			{
				return this.padding;
			}
			set
			{
				this.padding = value;
			}
		}
 
		public ChartInterior PlotBackground
		{
			get
			{
				return this.plotBackground;
			}
			set
			{
				this.plotBackground = value;
			}
		}
 
		internal int RecommendedInterval
		{
			get
			{
				int num1 = 1;
				if ((this.scaleX < 2f) && (this.scaleX > 0f))
				{
					num1 = (int) (2f / this.scaleX);
				}
				return num1;
			}
		}
 
		public bool RenderHorizontally
		{
			get
			{
				return this.renderHorizontally;
			}
			set
			{
				this.renderHorizontally = value;
			}
		}
 
		public short RightChartPadding
		{
			get
			{
				return this.rightChartPadding;
			}
			set
			{
				this.rightChartPadding = value;
			}
		}
 
		public float ScaleX
		{
			get
			{
				return this.scaleX;
			}
		}
 
		public float ScaleY
		{
			get
			{
				return this.scaleY;
			}
		}
 
		public bool ShowTitlesOnBackground
		{
			get
			{
				return this.showTitlesOnBackground;
			}
			set
			{
				this.showTitlesOnBackground = value;
			}
		}
 
		public bool ShowXValues
		{
			get
			{
				return this.showXValues;
			}
			set
			{
				this.showXValues = value;
			}
		}
 
		public bool ShowYValues
		{
			get
			{
				return this.showYValues;
			}
			set
			{
				this.showYValues = value;
			}
		}
 
		public Size Size
		{
			get
			{
				return this.size;
			}
			set
			{
				this.size = value;
			}
		}
 
		public int XAxisLablePadding
		{
			get{return this.xAxisLabelPadding;}
			set{this.xAxisLabelPadding = value;}
		}
		public int YAxisLAbelPadding
		{
			get{return this.yAxisLabelPadding;}
			set{this.yAxisLabelPadding = value;}
		}

		//Properties Added By Allen iThink
		public bool ShowXAxisNodes
		{
			get{return this.showXAxisNodes;}
			set{this.showXAxisNodes = value;}
		}
//		public ChartText Title
//		{
//			get
//			{
//				return this.title;
//			}
//			set
//			{
//				this.title = value;
//			}
//		}
 
		public short TopChartPadding
		{
			get
			{
				return this.topChartPadding;
			}
			set
			{
				this.topChartPadding = value;
			}
		}
 
		public short TopPadding
		{
			get
			{
				return this.topPadding;
			}
			set
			{
				this.topPadding = value;
			}
		}
 
		public ChartText XAxisFont
		{
			get
			{
				return this.xaxisFont;
			}
			set
			{
				this.xaxisFont = value;
			}
		}
 
		public int XTicksInterval
		{
			get
			{
				return this.xticksInterval;
			}
			set
			{
				this.xticksInterval = value;
			}
		}
 
		public ChartText XTitle
		{
			get
			{
				return this.xtitle;
			}
			set
			{
				this.xtitle = value;
			}
		}
 
		public int XValuesInterval
		{
			get
			{
				return this.xvaluesInterval;
			}
			set
			{
				this.xvaluesInterval = value;
			}
		}
 
		public ChartText YAxisFont
		{
			get
			{
				return this.yaxisFont;
			}
			set
			{
				this.yaxisFont = value;
			}
		}
 
		public float YCustomEnd
		{
			get
			{
				return this.ycustomEnd;
			}
			set
			{
				this.ycustomEnd = value;
			}
		}
 
		public float YCustomStart
		{
			get
			{
				return this.ycustomStart;
			}
			set
			{
				this.ycustomStart = value;
			}
		}
 
		public ChartText YTitle
		{
			get
			{
				return this.ytitle;
			}
			set
			{
				this.ytitle = value;
			}
		}
 
		public string YValuesFormat
		{
			get
			{
				return this.yvaluesFormat;
			}
			set
			{
				this.yvaluesFormat = value;
			}
		}
 
		public float YValuesInterval
		{
			get
			{
				return this.yvaluesInterval;
			}
			set
			{
				this.yvaluesInterval = value;
			}
		}
 

		#endregion

		// Fields
		#region private fields
		
		private short padding;
		private short chartPadding;
		private short titlePadding;
		private short leftChartPadding;
		private short bottomChartPadding;
		private short rightChartPadding;
		private short topChartPadding;
		private short topPadding;

		internal Rectangle chartRect;
		internal Rectangle controlRect;
		internal Rectangle legendRect;
		internal Rectangle titleRect;

		private ChartInterior background;
		private ChartLine border;
		private ChartCollection charts;
		private GridLines gridLines;
		private bool hasChartLegend;
		private ChartLegend legend;
		private ChartTitle chartTitle;
		private int maxPoints;
		private ChartInterior plotBackground;
		internal Rectangle plotRect;
		private bool renderHorizontally;
		internal float scaleX;
		internal float scaleY;
		internal DateTime scatterMaxDateTime;
		internal DateTime scatterMinDateTime;
		internal float scatterMinNumber;
		private bool showTitlesOnBackground;
		private bool showXValues;
		private bool showYValues;
		private Size size;
		//private ChartText title;
		private ChartText xaxisFont;
		private int xticksInterval;
		private ChartText xtitle;
		private int xvaluesInterval;
		private ChartText yaxisFont;
		private float ycustomEnd;
		private float ycustomStart;
		private ChartText ytitle;
		private string yvaluesFormat;
		private float yvaluesInterval;

		//Fields added by Allen iThink
		private bool showXAxisNodes = false;
		private int xAxisLabelPadding = 8;
		private int yAxisLabelPadding = 8;


		#endregion
	}
 
}
