using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Collections;
using System.Collections.Specialized;
using System.Globalization;

namespace VMVServices.Web.Charts
{
	public class ChartPointCollection : IList, ICollection, IEnumerable
	{
		// Methods
		public ChartPointCollection()
		{
			this.listItems = new ArrayList();
		}
 
		public ChartPointCollection(ChartPoint[] points)
		{
			this.listItems = new ArrayList();
			this.listItems.AddRange(points);
		}
 
		public int Add(ChartPoint point)
		{
			return this.listItems.Add(point);
		}
 
		public void Clear()
		{
			this.listItems.Clear();
		}
 
		public bool Contains(ChartPoint point)
		{
			return this.listItems.Contains(point);
		}
 
		public void CopyTo(ChartPointCollection data, int index)
		{
			for (int num1 = index; num1 < this.listItems.Count; num1++)
			{
				data.listItems.Add(this.listItems[num1]);
			}
		}
 
		public void GetMinMaxMeanValue(out float min, out float max, out float mean)
		{
			min = 0f;
			max = 0f;
			mean = 0f;
			if (this.listItems.Count != 0)
			{
				float single1 = 0f;
				min = float.MaxValue;
				max = float.MinValue;
				foreach (ChartPoint point1 in ((IEnumerable) this))
				{
					float single2 = point1.yvalue;
					if (single2 < min)
					{
						min = single2;
					}
					if (single2 > max)
					{
						max = single2;
					}
					single1 += single2;
				}
				mean = single1 / ((float) this.listItems.Count);
			}
		}
 
		public int IndexOf(ChartPoint point)
		{
			return this.listItems.IndexOf(point);
		}
 
		public void Insert(int index, ChartPoint point)
		{
			this.listItems.Insert(index, point);
		}
 
		public void Remove(ChartPoint point)
		{
			this.listItems.Remove(point);
		}
 
		public void RemoveAt(int index)
		{
			this.listItems.RemoveAt(index);
		}
 
		void ICollection.CopyTo(Array array, int index)
		{
			this.listItems.CopyTo(array, index);
		}
 
		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.listItems.GetEnumerator();
		}
 
		int IList.Add(object value)
		{
			return this.Add((ChartPoint) value);
		}
 
		bool IList.Contains(object item)
		{
			return this.Contains((ChartPoint) item);
		}
 
		int IList.IndexOf(object item)
		{
			return this.IndexOf((ChartPoint) item);
		}
 
		void IList.Insert(int index, object item)
		{
			this.Insert(index, (ChartPoint) item);
		}
 
		void IList.Remove(object value)
		{
			this.Remove((ChartPoint) value);
		}
 

		// Properties
		public int Count
		{
			get
			{
				return this.listItems.Count;
			}
		}
 
		public ChartPoint this[int index]
		{
			get
			{
				return (ChartPoint) this.listItems[index];
			}
			set
			{
				this.listItems[index] = value;
			}
		}
 
		bool ICollection.IsSynchronized
		{
			get
			{
				return this.listItems.IsSynchronized;
			}
		}
 
		object ICollection.SyncRoot
		{
			get
			{
				return this.listItems.SyncRoot;
			}
		}
 
		bool IList.IsFixedSize
		{
			get
			{
				return this.listItems.IsFixedSize;
			}
		}
 
		bool IList.IsReadOnly
		{
			get
			{
				return this.listItems.IsReadOnly;
			}
		}
 
		object IList.this[int index]
		{
			get
			{
				return this[index];
			}
			set
			{
				this[index] = (ChartPoint) value;
			}
		}
 

		// Fields
		private ArrayList listItems;
	}
 
}
