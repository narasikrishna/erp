using System;
using System.Data;
using System.Diagnostics;
using System.Web;
using System.Web.UI;
using System.Web.UI.Design;
using System.Web.UI.WebControls;
using System.IO;
using System.Drawing;
using System.Drawing.Design;
using System.Drawing.Imaging;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Collections;
using System.Collections.Specialized;
using System.Globalization;
using System.Threading;
using System.Security;

using VMVServices.Web.Charts.Design;

namespace VMVServices.Web.Charts
{
	[ParseChildren(true), ToolboxBitmap(typeof(ChartControl), "WebChart.bmp"), DefaultProperty("Charts"), Designer(typeof(ChartDesigner)), ToolboxData("<{0}:ChartControl BorderStyle=\"Outset\" BorderWidth=\"5px\" runat=\"server\"></{0}:ChartControl>")]
	public class ChartControl : WebControl
	{
		// Methods
		static ChartControl()
		{
			ChartControl.lastCleanUpTime = DateTime.Now;
			ChartControl.autoCleanUpInterval = 30;
			string text1 = System.Configuration.ConfigurationSettings.AppSettings["AutoCleanUpInterval"];
			if ((text1 != null) && (text1.Length > 0))
			{
				try
				{
					try
					{
						ChartControl.autoCleanUpInterval = int.Parse(text1, CultureInfo.InvariantCulture);
					}
					catch (Exception exception1)
					{
						throw new ApplicationException("Ignoring AutoCleanUpInterval in config", exception1);
					}
				}
				catch (ApplicationException)
				{
					ChartControl.autoCleanUpInterval = 30;
				}
			}
			else
			{
				ChartControl.autoCleanUpInterval = 30;
			}
			ChartControl.lastCleanUpTime = DateTime.MinValue;
		}
 
		public ChartControl() : base(HtmlTextWriterTag.Img)
		{
			this.filename = string.Empty;
			try
			{
				this.engine = new ChartEngine();
				this.border = new ChartLine(Color.Black);
				this.title = new ChartTitle();
				this.xtitle = new ChartText();
				this.ytitle = new ChartText();
				this.charts = new ChartCollection(this.engine);
				this.background = new ChartInterior(Color.LightSteelBlue);
				this.plotBackground = new ChartInterior(Color.White);
				this.legend = new ChartLegend();
				StringFormat format1 = new StringFormat();
				format1.Alignment = StringAlignment.Far;
				format1.FormatFlags = StringFormatFlags.LineLimit;
				this.yaxisFont = new ChartText(format1);
				this.xaxisFont = new ChartText();
			}
			catch (ApplicationException exception1)
			{
				EventLog.WriteEntry("WebChart Control", exception1.ToString());
			}
		}
 
		protected override void AddAttributesToRender(HtmlTextWriter writer)
		{
			base.AddAttributesToRender(writer);
			if (this.AlternateText.Length > 0)
			{
				writer.AddAttribute(HtmlTextWriterAttribute.Alt, this.AlternateText);
			}
			if ((base.Site != null) && base.Site.DesignMode)
			{
				ChartControl.PhysicalPath = ChartControl.AddDirSeparator(Path.GetTempPath());
				ChartControl.VirtualPath = ChartControl.PhysicalPath;
				bool flag1 = false;
				if ((this.Charts.Count == 0) && this.Visible)
				{
					this.CreateFakeChart();
					flag1 = true;
				}
				this.RedrawChart();
				if (flag1)
				{
					this.Charts.Clear();
				}
			}

			if (this.SaveImageToFile)
				if ((this.filename.Length > 0) && File.Exists(this.filename))
				{
					writer.AddAttribute(HtmlTextWriterAttribute.Src, ChartControl.VirtualPath + this.ImageID + "." + this.ChartFormat.ToString(CultureInfo.InvariantCulture));
				}
				else
				{
					writer.AddAttribute(HtmlTextWriterAttribute.Src, this.DefaultImageUrl);
				}
			else
			{
				writer.AddAttribute(HtmlTextWriterAttribute.Src, string.Concat(this.ChartImageHandlerUrl, "?imgId=", this.sessionGuidId));
			}
		}
 
		private static string AddDirSeparator(string path)
		{
			string text1 = path;
			if (!text1.EndsWith(@"\"))
			{
				text1 = text1 + @"\";
			}
			return text1;
		}
 
		private static string AddPathSeparator(string path)
		{
			string text1 = path;
			if (!text1.EndsWith("/"))
			{
				text1 = text1 + "/";
			}
			return text1;
		}
 
		private void CreateFakeChart()
		{
			Chart chart1 = new ColumnChart();
			chart1.Legend = "WebChart";
			this.Charts.Add(chart1);
			this.InternalSet_ImageID("ChartDesigner" + this.UniqueID);
			if (this.Width.IsEmpty)
			{
				this.Width = (Unit) 400;
			}
			if (this.Height.IsEmpty)
			{
				this.Height = (Unit) 300;
			}
		}
 
		[EditorBrowsable(EditorBrowsableState.Never), Browsable(false)]
		public override void DataBind()
		{
			base.DataBind();
		}
 
		private ImageFormat GetImageFormat(ChartImageFormat format)
		{
			switch (format)
			{
				case ChartImageFormat.Png:
				{
					return ImageFormat.Png;
				}
				case ChartImageFormat.Bmp:
				{
					return ImageFormat.Bmp;
				}
				case ChartImageFormat.Jpg:
				{
					return ImageFormat.Jpeg;
				}
				case ChartImageFormat.Gif:
				{
					return ImageFormat.Gif;
				}
				case ChartImageFormat.Emf:
				{
					return ImageFormat.Emf;
				}
				case ChartImageFormat.Tif:
				{
					return ImageFormat.Tiff;
				}
				case ChartImageFormat.Wmf:
				{
					return ImageFormat.Wmf;
				}
			}
			return ImageFormat.Png;
		}
 
		private static string GetPhysicalPath()
		{
			string text1 = "";
			HttpContext context1 = HttpContext.Current;
			if (context1 != null)
			{
				string[] textArray1;
				text1 = context1.Server.MapPath(context1.Request.ApplicationPath);
				try
				{
					System.IO.DirectoryInfo info1 = new System.IO.DirectoryInfo(Path.Combine(text1, "WebCharts"));
					if (!info1.Exists)
					{
						info1.Create();
					}
					return ChartControl.AddDirSeparator(info1.FullName);
				}
				catch (SecurityException exception1)
				{
					string text2 = string.Empty;
					if (((Thread.CurrentPrincipal != null) && (Thread.CurrentPrincipal.Identity != null)) && (Thread.CurrentPrincipal.Identity.Name != null))
					{
						text2 = Thread.CurrentPrincipal.Identity.Name;
					}
					textArray1 = new string[] { "The chart control wasn't able to create the WebCharts folder at: ", text1, " please check for permissions of user: ", text2, "\r\n", exception1.ToString() } ;
					string text3 = string.Concat(textArray1);
					try
					{
						EventLog.WriteEntry("WebChart Control", text3);
					}
					catch (SecurityException)
					{
					}
					throw new SecurityException(text3, exception1);
				}
				catch (UnauthorizedAccessException exception2)
				{
					string text4 = string.Empty;
					textArray1 = new string[] { "The chart control wasn't able to create the TempCharts folder at: ", text1, " please create the folder and assign write permissions for user: ", text4, "\r\n", exception2.ToString() } ;
					string text5 = string.Concat(textArray1);
					if (((Thread.CurrentPrincipal != null) && (Thread.CurrentPrincipal.Identity != null)) && (Thread.CurrentPrincipal.Identity.Name != null))
					{
						text4 = Thread.CurrentPrincipal.Identity.Name;
					}
					try
					{
						EventLog.WriteEntry("WebChart Control", text5);
					}
					catch (SecurityException)
					{
					}
					throw new SecurityException(text5, exception2);
				}
			}
			return text1;
		}
 
		private static string GetVirtualPath()
		{
			string text1 = string.Empty;
			if (HttpContext.Current != null)
			{
				text1 = HttpContext.Current.Request.ApplicationPath;
			}
			return (ChartControl.AddPathSeparator(text1) + "WebCharts/");
		}
 
		private void InitializeDefaultValues()
		{
			if (this.Width.IsEmpty)
			{
				this.Width = (Unit) 400;
			}
			if (this.Height.IsEmpty)
			{
				this.Height = (Unit) 300;
			}
		}
 
		private void InternalSet_ImageID(string val)
		{
			this.ViewState["ImageID"] = val;
		}
 
		public static void PerformCleanUp()
		{
			try
			{
				if (ChartControl.PhysicalPath.Length > 0)
				{
					System.IO.DirectoryInfo info1 = new System.IO.DirectoryInfo(ChartControl.PhysicalPath);
					System.IO.FileInfo[] infoArray1 = info1.GetFiles();
					for (int num1 = 0; num1 < infoArray1.Length; num1++)
					{
						System.IO.FileInfo info2 = infoArray1[num1];
						try
						{
							string text1;
							if ((text1 = info2.Extension.ToUpper(CultureInfo.InvariantCulture)) != null)
							{
								text1 = string.IsInterned(text1);
								if (((((text1 == ".PNG") || (text1 == ".JPG")) || ((text1 == ".GIF") || (text1 == ".BMP"))) || (((text1 == ".WMF") || (text1 == ".EMF")) || (text1 == ".TIF"))) && (info2.CreationTime.AddMinutes((double) ChartControl.autoCleanUpInterval) <= ChartControl.lastCleanUpTime))
								{
									info2.Delete();
								}
							}
						}
						catch (UnauthorizedAccessException)
						{
						}
						catch (System.IO.IOException)
						{
						}
						catch (Exception)
						{
						}
					}
				}
				ChartControl.lastCleanUpTime = DateTime.Now;
			}
			catch (Exception)
			{
			}
		}
 
		public void RedrawChart()
		{
			ImageFormat format1 = this.GetImageFormat(this.ChartFormat);
			try
			{
				try
				{
					Guid guid1;
					if ((ChartControl.autoCleanUpInterval > 0) && (ChartControl.lastCleanUpTime.AddMinutes((double) ChartControl.autoCleanUpInterval) < DateTime.Now))
					{
						ChartControl.PerformCleanUp();
					}
					this.InitializeDefaultValues();
					this.engine.Size = new Size((int) this.Width.Value, (int) this.Height.Value);
					this.engine.ChartTitle = this.title;
					this.engine.XTitle = this.xtitle;
					this.engine.YTitle = this.ytitle;
					this.engine.ShowTitlesOnBackground = this.ShowTitlesOnBackground;
					this.engine.HasChartLegend = this.HasChartLegend;
					this.engine.ChartPadding = this.ChartPadding;
					this.engine.Legend = this.Legend;
					this.engine.Padding = this.Padding;
					this.engine.TopPadding = this.TopPadding;
					this.engine.LeftChartPadding = this.LeftChartPadding;
					this.engine.RightChartPadding = this.RightChartPadding;
					this.engine.TopChartPadding = this.TopChartPadding;
					this.engine.BottomChartPadding = this.BottomChartPadding;
					this.engine.ShowXValues = this.ShowXValues;
					this.engine.ShowYValues = this.ShowYValues;
					this.engine.RenderHorizontally = this.RenderHorizontally;
					this.engine.YValuesInterval = this.YValuesInterval;
					this.engine.YValuesFormat = this.YValuesFormat;
					this.engine.XValuesInterval = this.XValuesInterval;
					this.engine.XTicksInterval = this.XTicksInterval;
					this.engine.YCustomStart = this.YCustomStart;
					this.engine.YCustomEnd = this.YCustomEnd;
					this.engine.Border = this.Border;
					this.engine.Background = this.Background;
					this.engine.PlotBackground = this.PlotBackground;
					this.engine.ChartTitle = this.ChartTitle;
					this.engine.Legend = this.Legend;
					this.engine.Charts = this.Charts;
					this.engine.XAxisFont = this.XAxisFont;
					this.engine.YAxisFont = this.YAxisFont;
					this.engine.GridLines = this.GridLines;
					this.filename = this.FileName;
					if ((this.Page != null) && this.Page.IsPostBack)
					{
						HttpBrowserCapabilities capabilities1 = this.Context.Request.Browser;
						if (this.Page.SmartNavigation || ((capabilities1.MajorVersion >= 6) && (capabilities1.Browser.IndexOf("IE") >= 0)))
						{
							try
							{
								if (File.Exists(this.filename))
								{
									File.Delete(this.filename);
								}
							}
							catch (IOException)
							{
							}
							guid1 = Guid.NewGuid();
							this.InternalSet_ImageID(guid1.ToString());
							this.filename = this.FileName;
						}
					}
					if (this.SaveImageToFile) 
						try
						{
							this.engine.SaveToFile(this.filename, format1);
						}
						catch (IOException)
						{
							guid1 = Guid.NewGuid();
							this.InternalSet_ImageID(guid1.ToString());
							this.filename = this.FileName;
							this.engine.SaveToFile(this.filename, format1);
							return;
						}
					else
					{
						this.sessionGuidId = Guid.NewGuid().ToString();
						this.Page.Session[this.sessionGuidId] = this.engine.GetBitmap();
					}
				}
				catch (Exception exception1)
				{
					throw new ApplicationException("Error while rendering the chart.", exception1);
				}
			}
			catch (ApplicationException exception2)
			{
				if ((this.Context != null) && (this.Context.Trace != null))
				{
					this.Context.Trace.Warn("Exception", exception2.ToString());
					this.Context.Trace.Warn("StackTrace", exception2.StackTrace);
					this.Context.Trace.Warn("FileName", this.filename);
				}
				try
				{
					System.Drawing.Image image1 = this.engine.GenerateErrorBitmap(exception2.ToString());
					image1.Save(this.filename, format1);
					return;
				}
				catch (Exception)
				{
					this.AlternateText = exception2.ToString() + "\r\n" + exception2.StackTrace;
					return;
				}
			}
		}
 

		// Properties
		[DefaultValue(""), Browsable(true), EditorBrowsable(EditorBrowsableState.Advanced), Description("Sets or returns the alternate text to display when an image is not available for the device. The default value is an empty string.")]
		public string AlternateText
		{
			get
			{
				object obj1 = this.ViewState["AlternateText"];
				if (obj1 is string)
				{
					return (string) obj1;
				}
				return "";
			}
			set
			{
				this.ViewState["AlternateText"] = value;
			}
		}
 
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
		public static int AutoCleanUpInterval
		{
			get
			{
				return ChartControl.autoCleanUpInterval;
			}
			set
			{
				ChartControl.autoCleanUpInterval = value;
			}
		}
 
		[Browsable(false)]
		public override Color BackColor
		{
			get
			{
				return base.BackColor;
			}
			set
			{
				base.BackColor = value;
			}
		}
 
		[Category("Appearance"), DesignerSerializationVisibility(DesignerSerializationVisibility.Content), PersistenceMode(PersistenceMode.InnerProperty), Bindable(true), DefaultValue((string) null), NotifyParentProperty(true)]
		public ChartInterior Background
		{
			get
			{
				return this.background;
			}
			set
			{
				this.background = value;
			}
		}
 
		[Category("Appearance"), NotifyParentProperty(true), DesignerSerializationVisibility(DesignerSerializationVisibility.Content), PersistenceMode(PersistenceMode.InnerProperty), DefaultValue((string) null)]
		public ChartLine Border
		{
			get
			{
				return this.border;
			}
			set
			{
				this.border = value;
			}
		}
 
		[Category("Appearance"), Browsable(true), Description("Sets or returns the additional padding between the chart area and the bottom of the chart"), DefaultValue(typeof(short), "0")]
		public short BottomChartPadding
		{
			get
			{
				object obj1 = this.ViewState["BottomChartPadding"];
				if (obj1 != null)
				{
					return (short) obj1;
				}
				return 0;
			}
			set
			{
				this.ViewState["BottomChartPadding"] = value;
			}
		}
 
		[Description("Sets or returns the image format to use"), Browsable(true), Category("Appearance"), DefaultValue(0)]
		public ChartImageFormat ChartFormat
		{
			get
			{
				object obj1 = this.ViewState["ChartFormat"];
				if (obj1 != null)
				{
					return (ChartImageFormat) obj1;
				}
				return ChartImageFormat.Png;
			}
			set
			{
				this.ViewState["ChartFormat"] = value;
			}
		}
 
		[DefaultValue(typeof(short), "20"), Description("Sets or returns the padding between the chart area and the chart"), Browsable(true), Category("Appearance")]
		public short ChartPadding
		{
			get
			{
				object obj1 = this.ViewState["ChartPadding"];
				if (obj1 != null)
				{
					return (short) obj1;
				}
				return 20;
			}
			set
			{
				this.ViewState.SetItemDirty("ChartPadding", true);
				this.ViewState["ChartPadding"] = value;
			}
		}
 
		[Browsable(true), Category("Appearance"), DefaultValue((string) null), PersistenceMode(PersistenceMode.InnerProperty), NotifyParentProperty(true)]
		public ChartCollection Charts
		{
			get
			{
				return this.charts;
			}
		}
 
		[NotifyParentProperty(true), PersistenceMode(PersistenceMode.InnerProperty), DesignerSerializationVisibility(DesignerSerializationVisibility.Content), Category("Appearance"), DefaultValue((string) null)]
		public ChartTitle ChartTitle
		{
			get
			{
				return this.title;
			}
			set
			{
				this.title = value;
			}
		}
 
		[Editor(typeof(ImageUrlEditor), typeof(UITypeEditor)), Category("Appearance"), Browsable(true), Description("Sets or returns the default image to display when an image is not available for the device. The default value is an empty string."), DefaultValue("")]
		public string DefaultImageUrl
		{
			get
			{
				object obj1 = this.ViewState["DefaultImageUrl"];
				if (obj1 is string)
				{
					return (string) obj1;
				}
				return string.Empty;
			}
			set
			{
				this.ViewState["DefaultImageUrl"] = value;
			}
		}
 
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
		public string FileName
		{
			get
			{
				if (ChartControl.PhysicalPath.Length <= 0)
				{
					throw new SecurityException("The PhysicalPath property has not been set");
				}
				return (ChartControl.PhysicalPath + this.ImageID + "." + Enum.GetName(typeof(ChartImageFormat), this.ChartFormat));
			}
		}
 
		[Browsable(false)]
		public override FontInfo Font
		{
			get
			{
				return base.Font;
			}
		}
 
		[Browsable(false)]
		public override Color ForeColor
		{
			get
			{
				return base.ForeColor;
			}
			set
			{
				base.ForeColor = value;
			}
		}
 
		[Category("Appearance"), Browsable(true), DefaultValue(1), Description("Gets or sets a value that specifies the grid line style to display in the Chart")]
		public GridLines GridLines
		{
			get
			{
				object obj1 = this.ViewState["GridLines"];
				if (obj1 != null)
				{
					return (GridLines) obj1;
				}
				return GridLines.Horizontal;
			}
			set
			{
				this.ViewState["GridLines"] = value;
			}
		}
 
		[Description("Sets or returns if the legends box are shown"), DefaultValue(true), Browsable(true), Category("Appearance")]
		public bool HasChartLegend
		{
			get
			{
				object obj1 = this.ViewState["HasChartLegend"];
				if (obj1 != null)
				{
					return (bool) obj1;
				}
				return true;
			}
			set
			{
				this.ViewState["HasChartLegend"] = value;
			}
		}
 
		[DefaultValue(typeof(Unit), "300px")]
		public override Unit Height
		{
			get
			{
				return base.Height;
			}
			set
			{
				base.Height = value;
			}
		}
 
		[Browsable(false)]
		public string ImageID
		{
			get
			{
				object obj1 = this.ViewState["ImageID"];
				if (obj1 == null)
				{
					Guid guid1 = Guid.NewGuid();
					string text1 = guid1.ToString();
					this.InternalSet_ImageID(text1);
					return text1;
				}
				return (string) obj1;
			}
		}
 
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
		public static DateTime LastCleanupTime
		{
			get
			{
				return ChartControl.lastCleanUpTime;
			}
		}
 
		[Browsable(true), DefaultValue(typeof(short), "10"), Description("Sets or returns the additional padding between the left border and the chart"), Category("Appearance")]
		public short LeftChartPadding
		{
			get
			{
				object obj1 = this.ViewState["LeftChartPadding"];
				if (obj1 != null)
				{
					return (short) obj1;
				}
				return 10;
			}
			set
			{
				this.ViewState["LeftChartPadding"] = value;
			}
		}
 
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content), PersistenceMode(PersistenceMode.InnerProperty), Category("Appearance"), Description("Specifies the legend for the chart"), NotifyParentProperty(true)]
		public ChartLegend Legend
		{
			get
			{
				return this.legend;
			}
		}
 
		[Description("Sets or returns the padding between the borders and the chart area"), Category("Appearance"), DefaultValue(typeof(short), "10"), Browsable(true)]
		public short Padding
		{
			get
			{
				object obj1 = this.ViewState["Padding"];
				if (obj1 != null)
				{
					return (short) obj1;
				}
				return 10;
			}
			set
			{
				this.ViewState["Padding"] = value;
			}
		}
 
		[Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public static string PhysicalPath
		{
			get
			{
				if ((ChartControl._physicalPath == null) || (ChartControl._physicalPath.Length == 0))
				{
					ChartControl._physicalPath = ChartControl.GetPhysicalPath();
				}
				return ChartControl._physicalPath;
			}
			set
			{
				ChartControl._physicalPath = value;
			}
		}
 
		[PersistenceMode(PersistenceMode.InnerProperty), DefaultValue((string) null), Category("Appearance"), NotifyParentProperty(true), DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public ChartInterior PlotBackground
		{
			get
			{
				return this.plotBackground;
			}
			set
			{
				this.plotBackground = value;
			}
		}
 
		[Category("Appearance"), Description("Sets or returns if the chart will be rendered horizontally, By Default it will be false, so it will render vertically"), DefaultValue(false), Browsable(true)]
		public bool RenderHorizontally
		{
			get
			{
				object obj1 = this.ViewState["RenderHorizontally"];
				if (obj1 != null)
				{
					return (bool) obj1;
				}
				return false;
			}
			set
			{
				this.ViewState["RenderHorizontally"] = value;
			}
		}
 
		[Browsable(true), Category("Appearance"), Description("Sets or returns the additional padding between the chart area and the right of the chart"), DefaultValue(typeof(short), "0")]
		public short RightChartPadding
		{
			get
			{
				object obj1 = this.ViewState["RightChartPadding"];
				if (obj1 != null)
				{
					return (short) obj1;
				}
				return 0;
			}
			set
			{
				this.ViewState["RightChartPadding"] = value;
			}
		}
 
		[DefaultValue(true), Browsable(true), Category("Appearance"), Description("Sets or returns if the values in the X Axis are shown")]
		public bool ShowTitlesOnBackground
		{
			get
			{
				object obj1 = this.ViewState["ShowTitlesOnBackground"];
				if (obj1 != null)
				{
					return (bool) obj1;
				}
				return true;
			}
			set
			{
				this.ViewState["ShowTitlesOnBackground"] = value;
			}
		}
 
		[Browsable(true), Description("Sets or returns if the values in the X Axis are shown"), Category("Appearance"), DefaultValue(true)]
		public bool ShowXValues
		{
			get
			{
				object obj1 = this.ViewState["ShowXValues"];
				if (obj1 != null)
				{
					return (bool) obj1;
				}
				return true;
			}
			set
			{
				this.ViewState["ShowXValues"] = value;
			}
		}
 
		[Description("Sets or returns if the values in the Y Axis are shown"), Browsable(true), Category("Appearance"), DefaultValue(true)]
		public bool ShowYValues
		{
			get
			{
				object obj1 = this.ViewState["ShowYValues"];
				if (obj1 != null)
				{
					return (bool) obj1;
				}
				return true;
			}
			set
			{
				this.ViewState["ShowYValues"] = value;
			}
		}
		[Description("Sets or returns if Chart control save the image to disk and references this file from the IMG SRC attribute or is the Chart Control uses the ChartImage Handler to retreive the image from the Session object"), Browsable(true), Category("Appearance"), DefaultValue(false)]
		public bool SaveImageToFile
		{
			get
			{
				return this.saveImageToFile;
			}
			set
			{
				this.saveImageToFile = value;
			}
		}
 
		[Category("Appearance"), DefaultValue(typeof(short), "0"), Description("Sets or returns the additional padding between the chart area and the top of the chart"), Browsable(true)]
		public short TopChartPadding
		{
			get
			{
				object obj1 = this.ViewState["TopChartPadding"];
				if (obj1 != null)
				{
					return (short) obj1;
				}
				return 0;
			}
			set
			{
				this.ViewState["TopChartPadding"] = value;
			}
		}
 
		[Description("Sets or returns the padding between the top border and the chart"), DefaultValue(typeof(short), "15"), Browsable(true), Category("Appearance")]
		public short TopPadding
		{
			get
			{
				object obj1 = this.ViewState["TopPadding"];
				if (obj1 != null)
				{
					return (short) obj1;
				}
				return 15;
			}
			set
			{
				this.ViewState["TopPadding"] = value;
			}
		}
 
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
		public static string VirtualPath
		{
			get
			{
				if ((ChartControl._virtualPath == null) || (ChartControl._virtualPath.Length == 0))
				{
					ChartControl._virtualPath = ChartControl.GetVirtualPath();
				}
				return ChartControl._virtualPath;
			}
			set
			{
				ChartControl._virtualPath = value;
			}
		}
 
		[DefaultValue(typeof(Unit), "400px")]
		public override Unit Width
		{
			get
			{
				return base.Width;
			}
			set
			{
				base.Width = value;
			}
		}
 
		[Category("Appearance"), PersistenceMode(PersistenceMode.InnerProperty), Browsable(true), DefaultValue((string) null), NotifyParentProperty(true), DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public ChartText XAxisFont
		{
			get
			{
				return this.xaxisFont;
			}
		}
 
		[Description("Sets or returns the interval of the ticks to display in the X Axis. (Default is 0 which means every tick will have its legend shown in X axis)"), Category("Appearance"), Browsable(true), DefaultValue(0)]
		public int XTicksInterval
		{
			get
			{
				object obj1 = this.ViewState["XTicksInterval"];
				if (obj1 != null)
				{
					return (int) obj1;
				}
				return 0;
			}
			set
			{
				this.ViewState["XTicksInterval"] = value;
			}
		}
 
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content), DefaultValue((string) null), Category("Appearance"), PersistenceMode(PersistenceMode.InnerProperty), NotifyParentProperty(true)]
		public ChartText XTitle
		{
			get
			{
				return this.xtitle;
			}
			set
			{
				this.xtitle = value;
			}
		}
 
		[Browsable(true), DefaultValue(0), Category("Appearance"), Description("Sets or returns the interval of the points to display in the X Axis. (Default is 0 which means every point will have its legend shown in X axis)")]
		public int XValuesInterval
		{
			get
			{
				object obj1 = this.ViewState["XValuesInterval"];
				if (obj1 != null)
				{
					return (int) obj1;
				}
				return 0;
			}
			set
			{
				this.ViewState["XValuesInterval"] = value;
			}
		}
 
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content), NotifyParentProperty(true), PersistenceMode(PersistenceMode.InnerProperty), Category("Appearance"), Browsable(true), DefaultValue((string) null)]
		public ChartText YAxisFont
		{
			get
			{
				return this.yaxisFont;
			}
		}
 
		[Browsable(true), Description("Sets or returns the custom end point of the chart. (Default is 0 which means calculated by WebControl)"), Category("Appearance"), DefaultValue(0)]
		public float YCustomEnd
		{
			get
			{
				object obj1 = this.ViewState["YCustomEnd"];
				if (obj1 != null)
				{
					return (float) obj1;
				}
				return 0f;
			}
			set
			{
				this.ViewState["YCustomEnd"] = value;
			}
		}
 
		[Browsable(true), Category("Appearance"), DefaultValue(0), Description("Sets or returns the custom start point of the chart. (Default is 0 which means calculated by WebControl)")]
		public float YCustomStart
		{
			get
			{
				object obj1 = this.ViewState["YCustomStart"];
				if (obj1 != null)
				{
					return (float) obj1;
				}
				return 0f;
			}
			set
			{
				this.ViewState["YCustomStart"] = value;
			}
		}
 
		[Category("Appearance"), DesignerSerializationVisibility(DesignerSerializationVisibility.Content), PersistenceMode(PersistenceMode.InnerProperty), NotifyParentProperty(true), DefaultValue((string) null)]
		public ChartText YTitle
		{
			get
			{
				return this.ytitle;
			}
			set
			{
				this.ytitle = value;
			}
		}
 
		[Category("Appearance"), Browsable(true), Description("Sets or returns the format to use when drawing the Values in the Y Axis"), DefaultValue((string) null)]
		public string YValuesFormat
		{
			get
			{
				object obj1 = this.ViewState["YValuesFormat"];
				if (obj1 != null)
				{
					return (string) obj1;
				}
				return null;
			}
			set
			{
				this.ViewState["YValuesFormat"] = value;
			}
		}
 
		[Description("Sets or returns the interval of the points to display in the Y Axis. (Default is 0 which means calculated by WebControl)"), Browsable(true), Category("Appearance"), DefaultValue(0)]
		public float YValuesInterval
		{
			get
			{
				object obj1 = this.ViewState["YValuesInterval"];
				if (obj1 != null)
				{
					return (float) obj1;
				}
				return 0f;
			}
			set
			{
				this.ViewState["YValuesInterval"] = value;
			}
		}
 
		internal string ChartImageHandlerUrl{
			get
			{
                string url = (string)System.Configuration.ConfigurationSettings.AppSettings["VMVServices.Web.Charting.ChartControl.ChartImageHandlerUrl"];
				if (url == null || url.Length == 0)
					url = this.ResolveUrl("~/chartimage.ashx");
				return this.ResolveUrl(url);
			}
		}

		// Fields
		private static string _physicalPath;
		private static string _virtualPath;
		private static int autoCleanUpInterval;
		private ChartInterior background;
		private ChartLine border;
		private ChartCollection charts;
		private ChartEngine engine;
		private string filename;
		private static DateTime lastCleanUpTime;
		private ChartLegend legend;
		private ChartInterior plotBackground;
		private ChartTitle title;
		private ChartText xaxisFont;
		private ChartText xtitle;
		private ChartText yaxisFont;
		private ChartText ytitle;
		private bool saveImageToFile;
		private string sessionGuidId;
	}
 
}
