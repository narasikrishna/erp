using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Collections;
using System.Collections.Specialized;
using System.Globalization;

namespace VMVServices.Web.Charts
{
	public class LineChart : Chart
	{
		// Methods
		public LineChart() { }
		public LineChart(ChartPointCollection data) : base(data) { }
 		public LineChart(ChartPointCollection data, Color color) : base(data, color) { }
		public LineChart(string xValueField, string yValueField, Color color, bool showLegend, string legend, bool showDataLabels, object dataSource) : base(xValueField, yValueField, color, showLegend, legend, showDataLabels, dataSource)
		{
			this.ShowLineMarkers = true;
		}
 
		public override void Render(Graphics graphics, int width, int height)
		{
			ChartEngine eng = base.Engine;
			float scaleX = eng.scaleX;
			float scaleY = eng.scaleY;
			Pen linePen = base.Line.GetPen(graphics);
			Point lineFirstPoint = Point.Empty;
			int cntr = 1;
			float halfScaleX = scaleX / 2f;
			bool showShadow = (base.shadow != null) && base.Shadow.Visible;
			Pen shadowPen = null;
			if (showShadow)
			{
				shadowPen = new Pen(base.Shadow.Color);
			}
			Point shadowFirstPoint = Point.Empty;
			Point shadowSecondPoint = Point.Empty;
			foreach (ChartPoint chartPoint in ((IEnumerable) base.Data))
			{
				int lineSecondPointXPosition = (int) (chartPoint.yvalue * scaleY);
				int lineSecondPointYPosition = (int) ((cntr * scaleX) - halfScaleX);
				Point lineSecondPoint = new Point(lineSecondPointYPosition, lineSecondPointXPosition);

				//only draw the actual line, its shadow and its marker 
				//if it isn't laying against one of the axis
				if (!(lineFirstPoint.X == 0 && lineSecondPoint.X == 0) && !(lineFirstPoint.Y == 0 && lineSecondPoint.Y == 0))
				{
					if (showShadow)//draw the shadow if required
						shadowSecondPoint = new Point(lineSecondPoint.X + base.Shadow.OffsetX, lineSecondPoint.Y + base.Shadow.OffsetY);

					if (cntr > 1)//then draw the actual line
					{
						if (showShadow)
							graphics.DrawLine(shadowPen, shadowFirstPoint, shadowSecondPoint);

						graphics.DrawLine(linePen, lineFirstPoint, lineSecondPoint);
					}

					if (base.showLineMarkers)//then draw the line marker, so that it renders above the actual line, if required
						base.lineMarker.Render(graphics, lineSecondPointYPosition, lineSecondPointXPosition);
				}

				//reset variables to now move onto the next point
				lineFirstPoint = lineSecondPoint;
				if (showShadow)
					shadowFirstPoint = shadowSecondPoint;

				//increment the counter
				cntr++;
			}

			if (base.dataLabels.visible)//render the data labels if required
				base.RenderDataLabels(graphics);

			//clean up
			linePen.Dispose();
			if (showShadow)
				shadowPen.Dispose();

		}
 
	}
 
}
