using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Collections;
using System.Collections.Specialized;
using System.Globalization;

namespace VMVServices.Web.Charts
{
	[TypeConverter(typeof(ExpandableObjectConverter)), DefaultProperty("Visible"), PersistenceMode(PersistenceMode.InnerProperty)]
	public class ChartDataLabels
	{
		// Methods
		public ChartDataLabels()
		{
			this.showXTitle = false;
			this.showValue = true;
			this.showLegend = false;
			this.numberFormat = "";
			this.separator = "";
			this.position = DataLabelPosition.Top;
			this.visible = false;
			this.showZeroValues = true;
			this.foreColor = Color.Black;
			this.maxPointsWidth = 3;
			this.background = new ChartInterior(Color.Transparent);
			this.border = new ChartLine(Color.Transparent);
		}
 

		// Properties
		[DefaultValue((string) null), PersistenceMode(PersistenceMode.InnerProperty), Category("Appearance"), DesignerSerializationVisibility(DesignerSerializationVisibility.Content), Bindable(true), NotifyParentProperty(true)]
		public ChartInterior Background
		{
			get
			{
				return this.background;
			}
			set
			{
				this.background = value;
			}
		}
 
		[NotifyParentProperty(true), DefaultValue((string) null), DesignerSerializationVisibility(DesignerSerializationVisibility.Content), PersistenceMode(PersistenceMode.InnerProperty), Bindable(true), Category("Appearance")]
		public ChartLine Border
		{
			get
			{
				return this.border;
			}
			set
			{
				this.border = value;
			}
		}
 
		[NotifyParentProperty(true)]
		public Font Font
		{
			get
			{
				return this.font;
			}
			set
			{
				this.font = value;
			}
		}
 
		[NotifyParentProperty(true), DefaultValue(typeof(Color), "Black")]
		public Color ForeColor
		{
			get
			{
				return this.foreColor;
			}
			set
			{
				this.foreColor = value;
			}
		}
 
		[NotifyParentProperty(true), DefaultValue(3)]
		public int MaxPointsWidth
		{
			get
			{
				return this.maxPointsWidth;
			}
			set
			{
				this.maxPointsWidth = value;
			}
		}
 
		[DefaultValue(""), NotifyParentProperty(true)]
		public string NumberFormat
		{
			get
			{
				return this.numberFormat;
			}
			set
			{
				this.numberFormat = value;
			}
		}
 
		[NotifyParentProperty(true), DefaultValue(0)]
		public DataLabelPosition Position
		{
			get
			{
				return this.position;
			}
			set
			{
				this.position = value;
			}
		}
 
		[NotifyParentProperty(true), DefaultValue("")]
		public string Separator
		{
			get
			{
				return this.separator;
			}
			set
			{
				this.separator = value;
			}
		}
 
		[DefaultValue(false), NotifyParentProperty(true)]
		public bool ShowLegend
		{
			get
			{
				return this.showLegend;
			}
			set
			{
				this.showLegend = value;
			}
		}
 
		[DefaultValue(true), NotifyParentProperty(true)]
		public bool ShowValue
		{
			get
			{
				return this.showValue;
			}
			set
			{
				this.showValue = value;
			}
		}
 
		[NotifyParentProperty(true), DefaultValue(false)]
		public bool ShowXTitle
		{
			get
			{
				return this.showXTitle;
			}
			set
			{
				this.showXTitle = value;
			}
		}
 
		[DefaultValue(false), NotifyParentProperty(true)]
		public bool ShowZeroValues
		{
			get
			{
				return this.showZeroValues;
			}
			set
			{
				this.showZeroValues = value;
			}
		}
 
		[NotifyParentProperty(true), DefaultValue(false)]
		public bool Visible
		{
			get
			{
				return this.visible;
			}
			set
			{
				this.visible = value;
			}
		}
 

		// Fields
		internal ChartInterior background;
		internal ChartLine border;
		internal Font font;
		internal Color foreColor;
		internal int maxPointsWidth;
		internal string numberFormat;
		internal DataLabelPosition position;
		internal string separator;
		internal bool showLegend;
		internal bool showValue;
		internal bool showXTitle;
		internal bool showZeroValues;
		internal bool visible;
	}
 
}
