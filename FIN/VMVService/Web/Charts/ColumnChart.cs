using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Collections;
using System.Collections.Specialized;
using System.Globalization;

namespace VMVServices.Web.Charts
{
	public class ColumnChart : Chart
	{
		// Methods
		public ColumnChart()
		{
			this.maxColumnWidth = 0;
			this.dataLabelOffset = 0f;
			base.showLineMarkers = false;
		}
 
		public ColumnChart(ChartPointCollection data) : base(data)
		{
			this.maxColumnWidth = 0;
			this.dataLabelOffset = 0f;
			base.showLineMarkers = false;
		}
 
		public ColumnChart(ChartPointCollection data, Color color) : base(data, color)
		{
			this.maxColumnWidth = 0;
			this.dataLabelOffset = 0f;
			base.showLineMarkers = false;
		}

		public ColumnChart(string xValueField, string yValueField, Color color, bool showLegend, string legend, bool showDataLabels, object dataSource): base(xValueField, yValueField, color, showLegend, legend, showDataLabels, dataSource)
		{
			this.maxColumnWidth = 0;
			this.dataLabelOffset = 0f;
			base.showLineMarkers = false;
			this.Fill = new ChartInterior(color);
		}
 
		protected override RectangleF GetDataLabelRectangle(Graphics graphics, float value, string text, int maxWidth, int pointIndex)
		{
			RectangleF ef1 = base.GetDataLabelRectangle(graphics, value, text, maxWidth, pointIndex);
			ef1.Offset(this.dataLabelOffset, 0f);
			return ef1;
		}
 
		public override void Render(Graphics graphics, int width, int height)
		{
			float columnWidth;
			ChartEngine eng = base.Engine;
			float scaleX = eng.scaleX;
			float scaleY = eng.scaleY;
			Pen linePen = base.Line.GetPen(graphics);
			Brush columnBrush = base.Fill.GetBrush(graphics);
			bool showShadow = (base.shadow != null) && base.Shadow.Visible;
			
			Brush shadowBrush = null;
			if (showShadow)
				shadowBrush = new SolidBrush(base.Shadow.Color);

			float columnXPosition = 0f;
			int numberOfColumnCharts = 0;
			int currentChartIndex = 0;
			if (eng.Charts.Count > 1)
			{
				foreach (Chart chart1 in ((IEnumerable) eng.Charts))
				{
					if (chart1.Equals(this)) currentChartIndex = numberOfColumnCharts;

					if (chart1 is ColumnChart) numberOfColumnCharts++;
				}

				columnWidth = (scaleX - 1f) / ((float) numberOfColumnCharts);
				columnXPosition = (columnWidth * currentChartIndex) + 1f;
			}
			else
			{
				//there is only one chart
				columnXPosition = 1f;
				columnWidth = scaleX;
			}

			if ((this.maxColumnWidth > 0) && (this.maxColumnWidth < columnWidth))
			{
				int num3 = ((int) columnWidth) - this.maxColumnWidth;
				if (numberOfColumnCharts > 0)
				{
					num3 = (num3 / numberOfColumnCharts) / 2;
				}
				else
				{
					num3 /= 2;
				}
				columnWidth = this.maxColumnWidth;
				columnXPosition += (1 + num3);
			}

			float cntr = 0f;
			foreach (ChartPoint point1 in ((IEnumerable) base.Data))
			{
				float columnYPosition;
				float columnHeight;
				if ((point1.yvalue * scaleY) < 0f)
				{
					columnYPosition = point1.yvalue * scaleY;
					columnHeight = -(point1.yvalue * scaleY);
				}
				else
				{
					columnYPosition = 0f;
					columnHeight = point1.yvalue * scaleY;
				}
				
				int columnXPositionOffset = (int) (cntr * scaleX);
				if (showShadow)
					graphics.FillRectangle(shadowBrush, (float) ((columnXPositionOffset + columnXPosition) + base.Shadow.OffsetX), (float) (columnYPosition + base.Shadow.OffsetY), columnWidth, columnHeight);

				Rectangle columnRect = new Rectangle((int)(columnXPositionOffset + columnXPosition), (int)columnYPosition, (int)columnWidth, (int)columnHeight);
				graphics.FillRectangle(columnBrush, columnRect);
				graphics.DrawRectangle(linePen, columnRect);
				
				if (base.showLineMarkers)
					base.lineMarker.Render(graphics, (int) ((columnXPositionOffset + columnXPosition) + (columnWidth / 2f)), (columnYPosition < 0f) ? ((int) columnYPosition) : ((int) columnHeight));

				cntr += 1f;
			}

			if (showShadow)
				shadowBrush.Dispose();

			if (base.dataLabels.visible)
			{
				this.dataLabelOffset = columnXPosition - ((base.Engine.scaleX - columnWidth) / 2f);
				base.RenderDataLabels(graphics);
			}

			//clean up
			columnBrush.Dispose();
			linePen.Dispose();
		}
 

		// Properties
		[DefaultValue(0)]
		public int MaxColumnWidth
		{
			get
			{
				return this.maxColumnWidth;
			}
			set
			{
				this.maxColumnWidth = value;
			}
		}
		[DefaultValue(10)]
		public int ColumnPadding
		{
			get
			{
				return this.columnPadding;
			}
			set
			{
				this.columnPadding = value;
			}
		}

 

		// Fields
		private float dataLabelOffset;
		private int maxColumnWidth;
		private int columnPadding;
	}
 
}
