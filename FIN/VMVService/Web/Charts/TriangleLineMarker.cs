using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Collections;
using System.Collections.Specialized;
using System.Globalization;

namespace VMVServices.Web.Charts
{
	public class TriangleLineMarker : LineMarker
	{
		// Methods
		public TriangleLineMarker()
		{
			this.brush = null;
			this.pen = null;
		}
 
		public TriangleLineMarker(int size, Color color, Color borderColor) : base(size, color, borderColor)
		{
			this.brush = null;
			this.pen = null;
		}
 
		public override void Render(Graphics graphics, int x, int y)
		{
			int num1 = base.Size / 2;
			Point[] pointArray2 = new Point[] { new Point(x - num1, y + num1), new Point(x, y - num1), new Point(x + num1, y + num1) } ;
			Point[] pointArray1 = pointArray2;
			graphics.DrawPolygon(this.Pen, pointArray1);
			graphics.FillPolygon(this.Brush, pointArray1);
		}
 

		// Properties
		private Brush Brush
		{
			get
			{
				if (this.brush == null)
				{
					this.brush = new SolidBrush(base.Color);
				}
				return this.brush;
			}
		}
 
		private Pen Pen
		{
			get
			{
				if (this.pen == null)
				{
					this.pen = new Pen(base.BorderColor, 0f);
				}
				return this.pen;
			}
		}
 

		// Fields
		private Brush brush;
		private Pen pen;
	}
 
}
