using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Collections;
using System.Collections.Specialized;
using System.Globalization;

namespace VMVServices.Web.Charts
{
	public class CircleLineMarker : LineMarker
	{
		// Methods
		public CircleLineMarker(){}
		public CircleLineMarker(int size, Color color, Color borderColor) : base(size, color, borderColor)
		{
		}
 
		public override void Render(Graphics graphics, int x, int y)
		{
			graphics.FillEllipse(new SolidBrush(base.Color), (int) (x - (base.Size / 2)), (int) (y - (base.Size / 2)), base.Size, base.Size);
			graphics.DrawEllipse(new Pen(base.BorderColor, 0f), (int) (x - (base.Size / 2)), (int) (y - (base.Size / 2)), base.Size, base.Size);
		}
 
	}
 
}
