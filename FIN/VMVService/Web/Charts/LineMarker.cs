using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Collections;
using System.Collections.Specialized;
using System.Globalization;

namespace VMVServices.Web.Charts
{
	public abstract class LineMarker
	{
		// Methods
		static LineMarker()
		{
			LineMarker.EmptyLineMarker = new SquareLineMarker(4, Color.Red, Color.Red);
		}
 
		protected LineMarker() : this(1, Color.Red, Color.Red)
		{
		}
 
		protected LineMarker(int size, Color color, Color borderColor)
		{
			this.borderColor = Color.Black;
			this.color = Color.Red;
			this.size = 1;
			this.size = size;
			this.color = color;
			this.borderColor = borderColor;
		}
 
		public abstract void Render(Graphics graphics, int x, int y);
 

		// Properties
		public Color BorderColor
		{
			get
			{
				return this.borderColor;
			}
			set
			{
				this.borderColor = value;
			}
		}
 
		public Color Color
		{
			get
			{
				return this.color;
			}
			set
			{
				this.color = value;
			}
		}
 
		public int Size
		{
			get
			{
				return this.size;
			}
			set
			{
				this.size = value;
			}
		}
 

		// Fields
		private Color borderColor;
		private Color color;
		public static readonly LineMarker EmptyLineMarker;
		private int size;
	}
 
}
