using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Collections;
using System.Collections.Specialized;
using System.Globalization;

namespace VMVServices.Web.Charts
{
	public class StackedAreaChart : Chart
	{
		// Methods
		public StackedAreaChart(){}
		public StackedAreaChart(ChartPointCollection data) : base(data)
		{
		}
 
		public StackedAreaChart(ChartPointCollection data, Color color) : base(data, color)
		{
		}
 
		public override void GetMinMaxMeanValue(out float min, out float max, out float mean)
		{
			ChartEngine engine1 = base.Engine;
			min = 0f;
			max = 0f;
			mean = 0f;
			int num1 = this.GetStackedCount(engine1.Charts);
			if ((num1 > 0) && (engine1.MaxPoints > 0))
			{
				Chart[] chartArray1 = new Chart[num1];
				int num2 = 0;
				foreach (Chart chart1 in ((IEnumerable) engine1.Charts))
				{
					if (chart1 is StackedAreaChart)
					{
						chartArray1[num2++] = chart1;
					}
				}
				float single1 = 0f;
				min = 0f;
				max = float.MinValue;
				for (int num3 = 0; num3 < engine1.MaxPoints; num3++)
				{
					float single3 = 0f;
					float single4 = 0f;
					for (int num4 = 0; num4 < num1; num4++)
					{
						ChartPointCollection collection1 = chartArray1[num4].Data;
						if (collection1.Count > num3)
						{
							float single2 = collection1[num3].yvalue;
							if (single2 >= 0f)
							{
								single3 += single2;
							}
							else
							{
								single4 += single2;
							}
						}
					}
					if (single4 < min)
					{
						min = single4;
					}
					if (single3 > max)
					{
						max = single3;
					}
					single1 += (single3 + single4);
				}
				mean = single1 / ((float) engine1.MaxPoints);
				if (base.dataLabels.visible)
				{
					float single5 = (max - min) / 10f;
					max += single5;
				}
			}
		}
 
		private int GetStackedCount(ChartCollection charts)
		{
			int num1 = 0;
			bool flag1 = true;
			foreach (Chart chart1 in ((IEnumerable) charts))
			{
				if (!(chart1 is StackedAreaChart))
				{
					continue;
				}
				if (flag1)
				{
					if (chart1 != this)
					{
						return 0;
					}
					flag1 = false;
				}
				num1++;
			}
			return num1;
		}
 
		public override void Render(Graphics graphics, int width, int height)
		{
			ChartEngine engine1 = base.Engine;
			int num1 = this.GetStackedCount(engine1.Charts);
			if (num1 != 0)
			{
				float single1 = engine1.scaleX;
				float single2 = engine1.scaleY;
				float single4 = single1 / 2f;
				StackedAreaChart[] chartArray1 = new StackedAreaChart[num1];
				Pen[] penArray1 = new Pen[num1];
				Brush[] brushArray1 = new Brush[num1];
				Point[] pointArray1 = new Point[num1];
				GraphicsPath[] pathArray1 = new GraphicsPath[num1];
				bool flag1 = (base.shadow != null) && base.Shadow.Visible;
				Brush brush1 = null;
				GraphicsPath[] pathArray2 = null;
				if (flag1)
				{
					pathArray2 = new GraphicsPath[num1];
					brush1 = new SolidBrush(base.Shadow.Color);
				}
				int num2 = 0;
				foreach (Chart chart1 in ((IEnumerable) engine1.Charts))
				{
					chartArray1[num2] = chart1 as StackedAreaChart;
					if (chartArray1[num2] != null)
					{
						penArray1[num2] = chart1.Line.GetPen(graphics);
						brushArray1[num2] = chart1.Fill.GetBrush(graphics);
						pointArray1[num2] = new Point((int) single4, 0);
						pathArray1[num2] = new GraphicsPath();
						if (flag1)
						{
							pathArray2[num2] = new GraphicsPath();
						}
						num2++;
						if (num2 >= num1)
						{
							break;
						}
					}
				}
				for (int num3 = 0; num3 < engine1.MaxPoints; num3++)
				{
					float single5 = 0f;
					float single6 = 0f;
					for (int num4 = 0; num4 < num1; num4++)
					{
						ChartPointCollection collection1 = chartArray1[num4].Data;
						if (collection1.Count > num3)
						{
							float single9;
							float single10;
							float single3 = collection1[num3].yvalue;
							float single7 = num3 * single1;
							float single8 = single1 - 1f;
							if (single3 > 0f)
							{
								single5 += single3;
								single9 = single5 * single2;
								single10 = -single3 * single2;
								Point point1 = new Point((int) (single7 + single4), (int) single9);
								pathArray1[num4].AddLine(pointArray1[num4], point1);
								if (flag1)
								{
									pathArray2[num4].AddLine((int) (pointArray1[num4].X + base.shadow.OffsetX), (int) (pointArray1[num4].Y + base.shadow.OffsetY), (int) (point1.X + base.shadow.OffsetX), (int) (point1.Y + base.shadow.OffsetY));
								}
								pointArray1[num4] = point1;
							}
							else if (single3 < 0f)
							{
								single9 = single6 * single2;
								single10 = single3 * single2;
								single6 += single3;
								Point point2 = new Point((int) single8, (int) single10);
								pathArray1[num4].AddLine(pointArray1[num4], point2);
								if (flag1)
								{
									pathArray2[num4].AddLine((int) (pointArray1[num4].X + base.shadow.OffsetX), (int) (pointArray1[num4].Y + base.shadow.OffsetY), (int) (point2.X + base.shadow.OffsetX), (int) (point2.Y + base.shadow.OffsetY));
								}
								pointArray1[num4] = point2;
							}
						}
					}
					if (base.dataLabels.visible)
					{
						this.RenderDataLabels(graphics);
					}
				}
				for (int num5 = num1 - 1; num5 >= 0; num5--)
				{
					pathArray1[num5].AddLine(pointArray1[num5], new Point(pointArray1[num5].X, 0));
					pathArray1[num5].CloseAllFigures();
					if (flag1)
					{
						pathArray2[num5].AddLine((int) (pointArray1[num5].X + base.shadow.OffsetX), (int) (pointArray1[num5].Y + base.shadow.OffsetY), (int) (pointArray1[num5].X + base.shadow.OffsetX), 0);
						pathArray2[num5].CloseAllFigures();
						graphics.FillPath(brush1, pathArray2[num5]);
					}
					graphics.FillPath(brushArray1[num5], pathArray1[num5]);
					graphics.DrawPath(penArray1[num5], pathArray1[num5]);
					penArray1[num5].Dispose();
					brushArray1[num5].Dispose();
					pathArray1[num5].Dispose();
				}
			}
		}
 
		protected override void RenderDataLabels(Graphics graphics)
		{
			if (base.dataLabels.font == null)
			{
				base.dataLabels.font = ChartText.DefaultFont;
			}
			ChartEngine engine1 = base.Engine;
			int num1 = base.DataLabelMaxWidth;
			StringFormat format1 = new StringFormat();
			format1.Alignment = StringAlignment.Center;
			format1.LineAlignment = StringAlignment.Near;
			int num2 = this.GetStackedCount(engine1.Charts);
			if (num2 != 0)
			{
				StackedAreaChart[] chartArray1 = new StackedAreaChart[num2];
				Pen[] penArray1 = new Pen[num2];
				Brush[] brushArray1 = new Brush[num2];
				Brush[] brushArray2 = new Brush[num2];
				int num3 = 0;
				foreach (Chart chart1 in ((IEnumerable) engine1.Charts))
				{
					chartArray1[num3] = chart1 as StackedAreaChart;
					if (chartArray1[num3] != null)
					{
						penArray1[num3] = chart1.dataLabels.border.GetPen(graphics);
						brushArray1[num3] = chart1.dataLabels.background.GetBrush(graphics);
						brushArray2[num3] = new SolidBrush(chart1.dataLabels.foreColor);
						num3++;
						if (num3 >= num2)
						{
							break;
						}
					}
				}
				float single4 = engine1.scaleX;
				float single5 = engine1.scaleY;
				for (int num4 = 0; num4 < engine1.MaxPoints; num4++)
				{
					float single2 = 0f;
					float single3 = 0f;
					for (int num5 = 0; num5 < num2; num5++)
					{
						ChartPointCollection collection1 = chartArray1[num5].Data;
						if (collection1.Count > num4)
						{
							RectangleF ef1;
							ChartPoint point1 = collection1[num4];
							float single1 = point1.yvalue;
							string text1 = base.GetDataLabelText(point1);
							if (single1 > 0f)
							{
								single2 += single1;
								ef1 = base.GetDataLabelRectangle(graphics, single2, text1, num1, num4);
								base.DrawDataLabel(graphics, text1, ef1, penArray1[num5], brushArray1[num5], format1, brushArray2[num5]);
							}
							else if (single1 < 0f)
							{
								ef1 = base.GetDataLabelRectangle(graphics, single3, text1, num1, num4);
								single3 += single1;
								base.DrawDataLabel(graphics, text1, ef1, penArray1[num5], brushArray1[num5], format1, brushArray2[num5]);
							}
						}
					}
				}
			}
		}
 
	}
 
}
