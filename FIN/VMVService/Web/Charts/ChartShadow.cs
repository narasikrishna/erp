using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Collections;
using System.Collections.Specialized;
using System.Globalization;

namespace VMVServices.Web.Charts
{
	[PersistenceMode(PersistenceMode.InnerProperty), TypeConverter(typeof(ExpandableObjectConverter))]
	public class ChartShadow
	{
		// Methods
		public ChartShadow()
		{
			this._offsetX = 5;
			this._offsetY = 0;
			this._color = Color.DarkGray;
			this._visible = false;
		}
 

		// Properties
		[DefaultValue(typeof(Color), "DarkGray"), NotifyParentProperty(true)]
		public Color Color
		{
			get
			{
				return this._color;
			}
			set
			{
				this._color = value;
			}
		}
 
		[NotifyParentProperty(true), DefaultValue(5)]
		public int OffsetX
		{
			get
			{
				return this._offsetX;
			}
			set
			{
				this._offsetX = value;
			}
		}
 
		[DefaultValue(0), NotifyParentProperty(true)]
		public int OffsetY
		{
			get
			{
				return this._offsetY;
			}
			set
			{
				this._offsetY = value;
			}
		}
 
		[DefaultValue(false), NotifyParentProperty(true)]
		public bool Visible
		{
			get
			{
				return this._visible;
			}
			set
			{
				this._visible = value;
			}
		}
 

		// Fields
		private Color _color;
		private int _offsetX;
		private int _offsetY;
		private bool _visible;
	}
 
}
