using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Collections;
using System.Collections.Specialized;
using System.Globalization;

namespace VMVServices.Web.Charts
{
	[TypeConverter(typeof(ExpandableObjectConverter)), PersistenceMode(PersistenceMode.InnerProperty)]
	public class ChartLine
	{
		// Methods
		public ChartLine() : this(Color.Black)
		{
		}
 
		public ChartLine(Color color)
		{
			this.dashStyle = DashStyle.Solid;
			this.lineJoin = LineJoin.Miter;
			this.startCap = LineCap.Flat;
			this.endCap = LineCap.Flat;
			this.customStartCap = null;
			this.customEndCap = null;
			this.width = 0f;
			this.color = color;
		}
 
		public Pen GetPen(Graphics graphics)
		{
			Pen pen1 = new Pen(graphics.GetNearestColor(this.color));
			pen1.StartCap = this.startCap;
			pen1.EndCap = this.endCap;
			if (this.customStartCap != null)
			{
				pen1.CustomStartCap = this.customStartCap;
			}
			if (this.customEndCap != null)
			{
				pen1.CustomEndCap = this.customEndCap;
			}
			pen1.DashStyle = this.dashStyle;
			pen1.Width = this.width;
			return pen1;
		}
 
		public override string ToString()
		{
			object[] objArray1 = new object[] { this.dashStyle.ToString(CultureInfo.InvariantCulture), this.color.Name } ;
			return string.Format(CultureInfo.InvariantCulture, "{0}-{1}", objArray1);
		}
 

		// Properties
		[DefaultValue(typeof(Color), "Black"), Category("Appearance"), NotifyParentProperty(true)]
		public Color Color
		{
			get
			{
				return this.color;
			}
			set
			{
				this.color = value;
			}
		}
 
		[Category("Appearance"), NotifyParentProperty(true), DefaultValue((string) null)]
		public CustomLineCap CustomEndCap
		{
			get
			{
				return this.customEndCap;
			}
			set
			{
				this.customEndCap = value;
			}
		}
 
		[NotifyParentProperty(true), DefaultValue((string) null), Category("Appearance")]
		public CustomLineCap CustomStartCap
		{
			get
			{
				return this.customStartCap;
			}
			set
			{
				this.customStartCap = value;
			}
		}
 
		[Category("Appearance"), NotifyParentProperty(true), DefaultValue(0)]
		public DashStyle DashStyle
		{
			get
			{
				return this.dashStyle;
			}
			set
			{
				this.dashStyle = value;
			}
		}
 
		[DefaultValue(0), NotifyParentProperty(true), Category("Appearance")]
		public LineCap EndCap
		{
			get
			{
				return this.endCap;
			}
			set
			{
				this.endCap = value;
			}
		}
 
		[NotifyParentProperty(true), DefaultValue(0), Category("Appearance")]
		public LineJoin LineJoin
		{
			get
			{
				return this.lineJoin;
			}
			set
			{
				this.lineJoin = value;
			}
		}
 
		[DefaultValue(0), Category("Appearance"), NotifyParentProperty(true)]
		public LineCap StartCap
		{
			get
			{
				return this.startCap;
			}
			set
			{
				this.startCap = value;
			}
		}
 
		[DefaultValue((float) 0f), Category("Appearance"), NotifyParentProperty(true)]
		public float Width
		{
			get
			{
				return this.width;
			}
			set
			{
				this.width = value;
			}
		}
 

		// Fields
		private Color color;
		private CustomLineCap customEndCap;
		private CustomLineCap customStartCap;
		private DashStyle dashStyle;
		private LineCap endCap;
		private LineJoin lineJoin;
		private LineCap startCap;
		private float width;
	}
 
}
