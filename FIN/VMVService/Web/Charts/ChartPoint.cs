using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Collections;
using System.Collections.Specialized;
using System.Globalization;

namespace VMVServices.Web.Charts
{
	public class ChartPoint
	{
		// Methods
		public ChartPoint(){}
		public ChartPoint(string xvalue, float yvalue)
		{
			this.xvalue = xvalue;
			this.yvalue = yvalue;
		}
 
		public override string ToString()
		{
			object[] objArray1 = new object[] { this.xvalue, this.yvalue.ToString(CultureInfo.InvariantCulture) } ;
			return string.Format(CultureInfo.InvariantCulture, "{0}, {1}", objArray1);
		}
 

		// Properties
		[NotifyParentProperty(true)]
		public string XValue
		{
			get
			{
				return this.xvalue;
			}
			set
			{
				this.xvalue = value;
			}
		}
 
		[NotifyParentProperty(true)]
		public float YValue
		{
			get
			{
				return this.yvalue;
			}
			set
			{
				this.yvalue = value;
			}
		}
 

		// Fields
		internal string xvalue;
		internal float yvalue;
	}
 
}
