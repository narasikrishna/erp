using System;
using System.Web;
using System.Web.SessionState;
using System.Drawing;
using System.Drawing.Imaging;

namespace VMVServices.Web.Charts.HttpHandler
{
	/// <summary>
	/// Summary description for ChartImageHandler.
	/// </summary>
	public class ChartImageHandler : IHttpHandler, IRequiresSessionState
	{
		public ChartImageHandler() { }

		public void ProcessRequest(HttpContext context)
		{
			//
			//Extract input parameter from the querystring
			//
			string imgId = context.Request["imgid"].ToString();

			if (imgId == null || imgId == string.Empty)
				return;

			//Use the imgId to retreive the image in binary form 
			//from the Session object and return 
			//it to the requesting page
			Bitmap img = (Bitmap)context.Session[imgId];
			context.Response.ContentType = "image/jpeg";
			try 
			{
				img.Save(context.Response.OutputStream, ImageFormat.Jpeg);
			}
			catch (Exception e)
			{
                VMVServices.Services.ErrorManagement.Publish(e, -1, VMVServices.Settings.Instance.PublishErrorDetails);
			}

			//
			//Clean up and return
			//
			context.Session[imgId] = null;
			context.Session.Remove(imgId);
			img.Dispose();
		}

		public bool IsReusable { get { return true; } }
	}
}
