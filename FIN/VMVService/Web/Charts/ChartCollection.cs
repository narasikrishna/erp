using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Drawing.Design;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Collections;
using System.Collections.Specialized;
using System.Globalization;

using VMVServices.Web.Charts.Design;

namespace VMVServices.Web.Charts
{
	[Editor(typeof(ChartCollectionEditor), typeof(UITypeEditor)), PersistenceMode(PersistenceMode.InnerProperty)]
	public class ChartCollection : IList, ICollection, IEnumerable
	{
		// Methods
		public ChartCollection(ChartEngine engine)
		{
			this.engine = engine;
			this.listItems = new ArrayList();
		}
 
		public int Add(Chart chart)
		{
			chart.Engine = this.engine;
			return this.listItems.Add(chart);
		}
 
		public void Clear()
		{
			this.listItems.Clear();
		}
 
		public bool Contains(Chart chart)
		{
			return this.listItems.Contains(chart);
		}
 
		public void CopyTo(ChartCollection charts, int index)
		{
			for (int num1 = index; num1 < this.listItems.Count; num1++)
			{
				charts.listItems.Add(this.listItems[num1]);
			}
		}
 
		public Chart FindChart(string name)
		{
			foreach (Chart chart1 in this.listItems)
			{
				if (string.Compare(chart1.Name, name, true, CultureInfo.InvariantCulture) == 0)
				{
					return chart1;
				}
			}
			return null;
		}
 
		public int IndexOf(Chart chart)
		{
			return this.listItems.IndexOf(chart);
		}
 
		public void Insert(int index, Chart item)
		{
			this.listItems.Insert(index, item);
		}
 
		public void Remove(Chart chart)
		{
			this.listItems.Remove(chart);
		}
 
		public void RemoveAt(int index)
		{
			this.listItems.RemoveAt(index);
		}
 
		void ICollection.CopyTo(Array array, int index)
		{
			this.listItems.CopyTo(array, index);
		}
 
		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.listItems.GetEnumerator();
		}
 
		int IList.Add(object value)
		{
			return this.Add((Chart) value);
		}
 
		bool IList.Contains(object item)
		{
			return this.Contains((Chart) item);
		}
 
		int IList.IndexOf(object item)
		{
			return this.IndexOf((Chart) item);
		}
 
		void IList.Insert(int index, object item)
		{
			this.Insert(index, (Chart) item);
		}
 
		void IList.Remove(object value)
		{
			this.Remove((Chart) value);
		}
 

		// Properties
		public int Count
		{
			get
			{
				return this.listItems.Count;
			}
		}
 
		[NotifyParentProperty(true)]
		public Chart this[int index]
		{
			get
			{
				return (Chart) this.listItems[index];
			}
			set
			{
				this.listItems[index] = value;
			}
		}
 
		bool ICollection.IsSynchronized
		{
			get
			{
				return this.listItems.IsSynchronized;
			}
		}
 
		object ICollection.SyncRoot
		{
			get
			{
				return this.listItems.SyncRoot;
			}
		}
 
		bool IList.IsFixedSize
		{
			get
			{
				return this.listItems.IsFixedSize;
			}
		}
 
		bool IList.IsReadOnly
		{
			get
			{
				return this.listItems.IsReadOnly;
			}
		}
 
		object IList.this[int index]
		{
			get
			{
				return this[index];
			}
			set
			{
				this[index] = (Chart) value;
			}
		}
 

		// Fields
		private ChartEngine engine;
		private ArrayList listItems;
	}
 
}
