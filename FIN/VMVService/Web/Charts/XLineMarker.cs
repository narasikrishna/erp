using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Collections;
using System.Collections.Specialized;
using System.Globalization;

namespace VMVServices.Web.Charts
{
	public class XLineMarker : LineMarker
	{
		// Methods
		public XLineMarker()
		{
			this.brush = null;
			this.pen = null;
		}
 
		public XLineMarker(int size, Color color, Color borderColor) : base(size, color, borderColor)
		{
			this.brush = null;
			this.pen = null;
		}
 
		public override void Render(Graphics graphics, int x, int y)
		{
			int num1 = base.Size / 2;
			int num2 = x - num1;
			int num3 = x + num1;
			int num4 = y - num1;
			int num5 = y + num1;
			int num6 = base.Size / 5;
			int num7 = 2 * num6;
			if (num6 <= 0)
			{
				num6 = 1;
				base.Size = 5;
			}
			Point[] pointArray2 = new Point[] { new Point(num2, num4 + num6), new Point(num2 + num6, num4), new Point(x, num4 + num7), new Point(num3 - num6, num4), new Point(num3, num4 + num6), new Point(num3 - num7, y), new Point(num3, num5 - num6), new Point(num3 - num6, num5), new Point(x, num5 - num7), new Point(num2 + num6, num5), new Point(num2, num5 - num6), new Point(num2 + num7, y) } ;
			Point[] pointArray1 = pointArray2;
			graphics.FillPolygon(this.Brush, pointArray1);
			graphics.DrawPolygon(this.Pen, pointArray1);
		}
 

		// Properties
		private Brush Brush
		{
			get
			{
				if (this.brush == null)
				{
					this.brush = new SolidBrush(base.Color);
				}
				return this.brush;
			}
		}
 
		private Pen Pen
		{
			get
			{
				if (this.pen == null)
				{
					this.pen = new Pen(base.BorderColor, 0f);
				}
				return this.pen;
			}
		}
 

		// Fields
		private Brush brush;
		private Pen pen;
	}
 
}
