using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace VMVServices.Web
{
	/// <summary>
	/// Summary description for UserControlBase.
	/// </summary>
	public class UserControlBase : System.Web.UI.UserControl
	{

        public VMVServices.Web.PageBase PageBase { get { return (VMVServices.Web.PageBase)base.Page; } }
	
	}
}
