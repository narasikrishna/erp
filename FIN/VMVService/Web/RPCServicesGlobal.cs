using System;
using System.Collections;
using System.Configuration;
using System.ComponentModel;
using System.Web;
using System.Web.SessionState;
using System.Web.Security;
using System.Security.Principal;

using VMVServices.Services.Configuration.ConfigurationSectionHandlers;
using VMVServices.Services.Business;


namespace VMVServices.Web
{
	/// <summary>
	/// Summary description for iThinkGlobal.
	/// </summary>
    public class VMVServicesGlobal : System.Web.HttpApplication
	{
        public VMVServicesGlobal()
		{
			//
			// TODO: Add constructor logic here
			//
		}
        
        

		protected void Application_Start_Handler(Object sender, EventArgs e)
		{
			Application["MENUS"] = (MenuCollection)System.Configuration.ConfigurationSettings.GetConfig("menusConfigSection");
			Application["URLDETAILS"] = (UrlDetailCollection)System.Configuration.ConfigurationSettings.GetConfig("urlConfigSection");
			Application["SECURITYROLES"] = (RoleCollection)ConfigurationSettings.GetConfig("securityRolesConfigSection");
			Application["SERVERCONNECTIONSTRINGS"] = (ServerConnectionStringCollection)ConfigurationSettings.GetConfig("serverConnectionStringConfigSection");
        }

		protected void Application_AuthenticateRequest_Handler(Object sender, EventArgs e)
		{
			// Extract the forms authentication cookie
			string cookieName = FormsAuthentication.FormsCookieName;
			HttpCookie authCookie = Context.Request.Cookies[cookieName];

			if(null == authCookie)
			{
				// There is no authentication cookie.
				return;
			} 

			FormsAuthenticationTicket authTicket = null;
			try
			{
				authTicket = FormsAuthentication.Decrypt(authCookie.Value);
			}
			catch
			{
				// Log exception details (omitted for simplicity)
				return;
			}

			if (null == authTicket)
			{
				// Cookie failed to decrypt.
				return; 
			} 

			// When the ticket was created, the UserData property was assigned a
			// pipe delimited string of role names.
			string[] roles = authTicket.UserData.Split(new char[]{'|'});

			// Create an Identity object
			FormsIdentity id = new FormsIdentity( authTicket ); 

			// This principal will flow throughout the request.
			GenericPrincipal principal = new GenericPrincipal(id, roles);
			// Attach the new principal object to the current HttpContext object
			Context.User = principal;
		}
		protected void Application_Error_Handler(Object sender, EventArgs e)
		{
			//GET REFERENCE TO THE SOURCE OF THE EXCEPTION CHAIN
			Exception ex = Server.GetLastError();

			//REDIRECT TO ERROR MESSAGE PAGE
			string tmpstr = string.Concat("<strong>", "Unexpeceted problem occurred", "</strong><br><br>");
			Exception tmpex;

			long loggedOnUserID = 0;
			try {loggedOnUserID = ((BusinessObjectBase)Session[VMVServicesGlobal.Consts.LoggedOnUserKey]).ObjID;}
			catch{}
			//catch{}

			//LOG THE ERROR TO THE DATABASE, BUT IF THERE ARE ERRORS HERE THEN IGNORE THEM
			try
			{
				VMVServices.Services.ErrorManagement.Publish(ex, loggedOnUserID, VMVServices.Settings.Instance.PublishErrorDetails);
			}
			catch 
			{
				//do nothing cos this is error logging
			}

			tmpex = ex;
			while (tmpex != null)
			{
				tmpstr += string.Concat("<strong>Message:</strong> ", tmpex.Message, "<br><br>", 
					"<strong>Stack Trace:</strong> ", tmpex.StackTrace.Replace(Environment.NewLine, "<br><br>"), "<br><br><br><br>");

				tmpex = tmpex.InnerException;
			}

			//SEND EMAIL TO ADMINISTRATOR, BUT IF THERE ARE ERRORS HERE THEN IGNORE THEM
			bool sendEmailResult;
			try
			{
				string rootUrl = VMVServices.Web.Utils.GetRootUrl();

				sendEmailResult = VMVServices.Services.Business.Email.Send("allen@ansdata.co.za", //to email address
													string.Empty, //cc
													string.Empty, //bcc
													string.Concat(DateTime.Now.ToString("dd MMM yyyy HH:mm ss"), " - Error encountered on ", rootUrl), //subject
													tmpstr, //body / message text
													true, //send as html
													"smtp.saix.net", //smtp server
													"allen@ithink.co.za"); //from email address
			}
			catch
			{
				//do nothing cos this is error logging
				System.Diagnostics.Debug.WriteLine("Error sending email notification from Global Error Handler");
			}

			tmpstr = string.Concat("We apologise for the inconvenience but the system has encountered an unexpected problem.<BR><BR>", 
					"Don�t worry though, the problem has been appropriately logged and an administrator has been notified via email and will attend to it in order to ensure that this problem does not occur again.<BR><BR>", 
					"Please try your operation again and if the problem persists then allow some time for the administrator to respond to this problem.");

			//DISPLAY THE ERROR TO THE USER
			try{ Session[VMVServicesGlobal.Consts.AppMessageHeaderKey] = "Unexpected problem encountered." ;} catch{}
			try{ Session[VMVServicesGlobal.Consts.AppMessageContinueUrlKey] = VMVServicesGlobal.Urls.LogoffPageUrl;} catch{}
			try{ Session[VMVServicesGlobal.Consts.AppMessageImageKey] = VMVServicesGlobal.MessageImageEnum.Failed;} catch{}
			try{ Session[VMVServicesGlobal.Consts.AppMessageBodyKey] = tmpstr;} catch{}		
		}
		protected void Global_PreRequestHandlerExecute_Handler(object sender, System.EventArgs e)
		{
			string ID;

			if (Session[VMVServicesGlobal.Consts.LoggedOnUserKey] != null)
			{
				//'THIS IS AFTER AN INITIAL LOGIN

				//'GET THE UNIQUE IDENTIFIER FOR THE LOGGED ON USER
				ID = ((BusinessObjectBase)Session[VMVServicesGlobal.Consts.LoggedOnUserKey]).ObjID.ToString();

				//'simply accessing the item in the cache 
				//'resets the starting time for the sliding exiration
				//'
				//'the sliding expiration is defined in the web.config file
				//'in the "timeout" attribute of the <sessionState> item 
				try
				{
					if (ID != null) {string CacheItem = HttpContext.Current.Cache[ID].ToString();}
				}
				catch
				{
					//goto logoff page
					Server.Transfer(VMVServicesGlobal.Urls.LogoffPageUrl);
				}
				
			}
		}


		public class Consts
		{
			//Context object keys
            public const string ReportExportFormatList = "ReportExportFormatList";
			public const string AppMessageHeaderKey = "APPMESSAGEHEADERKEY";
			public const string AppMessageBodyKey = "APPMESSAGEBODYKEY";
			public const string AppMessageContinueUrlKey = "APPMESSAGECONTINUEURLKEY";
			public const string AppMessageImageKey = "APPMESSAGEIMAGEKEY";
		
			public const string LoggedOnUserKey = "LOGGEDONUSERKEY";
			public const string UseSessionExpireRedirectKey = "USESESSIONEXPIREREDIRECTKEY";
			public const string CurrentSysErrorKey = "CURRENTSYSERRORKEY";
            public const string LoggedSchemaName = "LoggedSchemaName";
            public const string LoggedSchemaConnectionString = "LoggedSchemaConnectionString";
            public const string CurrentUserConnection = "CurrentUserConnection";
            public const string TransactionNumber = "TransactionNumber";
            public const string ReportFile = "ReportFile";
            public const string ReportData = "ReportData";
            public const string SubReportData = "SubReportData";
            public const string ReportParameter = "ReportParameter";
            public const string ReportFilterParameter = "ReportFilterParameter";
            public const string ReportViewFilterParameter = "ReportViewFilterParameter";
            
            public const string ReportFormulaParameter = "ReportFormulaParameter";
            public const string ReportFileMissing = "ReportFileMissing";
            public const string ClaimEditFromClaimList = "ClaimEditFromClaimList";
            public const string PaymentRecoveryURL = "PaymentRecoveryURL";
            public const string PopUpPayeeObject = "PopUpPayeeObject";

            public const string AFAM ="AFAM";
            public const string BARCCL="BARCCL";
            public const string BARCM="BARCM";
            public const string GEBSM="GEBSM";
            public const string GSIFM="GSIFM";
            public const string LASIMF="LASIMF";
            public const string NGOVM="NGOVM";
            public const string STANCL="STANCL";
            public const string STANM = "STANM";
            public const string GSIFH = "GSIFH";
            public const string NGOVH = "NGOVH";
            public const string MINEM = "MINEM";
            public const string MININGMOTOR = "MININGMOTOR";
            
            
            public const string UserName = "UserName";
            public const string Machine_Name = "Machine_Name";

            public const string SuccessfullRows = "SuccessfullRows";
            public const string UnSuccessfullRows = "UnSuccessfullRows";
            public const string NoRecordsSuccessfull = "NoRecordsSuccessfull";

            public const string BulkLoadLogs = "BulkLoadLogs";

            public const string SuccessfullPostedCheques = "SuccessfullPostedCheques";

            public const string IsPmtverifcation = "IsPmtverifcation";

		}

		public class Urls
		{
			public static UrlDetailCollection UrlDetails
			{
				get
				{

                    return (UrlDetailCollection)HttpContext.Current.Application["URLDETAILS"];
				}
			}

			public static string MessagePageUrl{
                get
                {
                    return System.Configuration.ConfigurationSettings.AppSettings.Get("MessagePageUrl");//Urls.UrlDetails[UrlConsts.MessagePageUrlKey, true].Url;
                } 
            }
			public static string DefaultTemplateControlUrl{get{return Urls.UrlDetails[UrlConsts.DefaultTemplateControlUrlKey, true].Url; } }
			public static string LogoffPageUrl{get{return "login.aspx"; } }
			public static string ScreenSaverPageUrl{get{return Urls.UrlDetails[UrlConsts.ScreenSaverPageUrlKey, true].Url; } }
		}

		public class UrlConsts
		{
			public const string MessagePageUrlKey = "MessagePageUrl";
			public const string DefaultTemplateControlUrlKey = "PageTemplates.DefaultTemplateControlUrl";
			public const string LogoffPageUrlKey = "LogoffPageUrl";
			public const string ScreenSaverPageUrlKey = "ScreenSaverPageUrl";

            #region "URL handled by RPC" 
            public const string CompaniesListPageUrl = "~/CompaniesList.aspx";
            public const string MedicationsListPageUrl = "~/MedicationsList.aspx";
            public const string AccountsListPageUrl = "~/AccountsList.aspx";
            public const string CompanyUsersListPageUrl = "~/CompanyUsersList.aspx";
            public const string UsersListPageUrl = "~/UsersList.aspx";
            public const string LicenseRequestsListPageUrl = "~/LicenseRequestsList.aspx";
            public const string PatientsListPageUrl = "~/PatientsList.aspx";
            public const string HomePageUrl = "~/Home.aspx";
            public const string AccountAdminList = "~/AccountAdminList.aspx";
            public const string PatientAdminEntryPageUrl = "~/PatientAdminEntry.aspx";
            
            #endregion 
		}

		#region Enums
		public enum MessageImageEnum
		{
			Succeeded,
			Failed,
			Warning,
			Information,
		}

		#endregion

	}
}
