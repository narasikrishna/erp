using System;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections;

using VMVServices.Services.Configuration.ConfigurationSectionHandlers;
using VMVServices.Services.Business;


namespace VMVServices.Web
{
	/// <summary>
	/// Summary description for PageBase.
	/// </summary>
	public class PageBase : Page
	{

		protected HtmlGenericControl _bodyTag;
		public HtmlGenericControl BodyTag{get{return _bodyTag;}}

        static SortedList _errorCollection = new SortedList();
        public static  System.Collections.SortedList ErrorCollection
        {
            get { return _errorCollection; }
            set { _errorCollection = value; }
 
        }
        public string ValidationWindowProperties
        {
            get { return "toolbar=no,menubar=no,location=no,scrollbars=yes,resizable=no,status=no,width=300,height=250,left=300,Top=200"; }
        }

        private void Page_Init(object sender, System.EventArgs e)
        {
            if (Session.SessionID == null || Session.SessionID == string.Empty )
            {
                Response.Redirect("~/loginnew.aspx");
            }
 
        }

        static SortedList _emptyerrorCollection = new SortedList();
        public static System.Collections.SortedList EmptyErrorCollection
        {
            get { return _emptyerrorCollection; }
            set { _emptyerrorCollection = value; }

        }

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here

			//Code to call session expired redirect code injection, this causes the 
			//page to automatically logoff when the session expires, 
			//like banking systems
            
            

            VMVServices.Settings.CurrentSessionID = Session.SessionID;
            Page.MaintainScrollPositionOnPostBack = false;
			SetupSessionExpireRedirect();
			this.SetPageDetails();
		}

		protected int CurrentDataListPageIndex
		{
			get
			{
				//look for current page index in viewstate
				object o = ViewState["DataListCurrentPageIndex"];
				if (o == null)
					return 0;
				else
					return (int)o;
			}
			set{ ViewState["DataListCurrentPageIndex"] = value;}
		}


		#region Properties intended to be overriden in derived pages
		public virtual string RecordAlreadyExistsErrorMessage{get{return string.Empty;}}
		public virtual string DependantRecordDeletionErrorMessage{get{return string.Empty;}}
		#endregion

		#region Properties for referencing values from the session object
		//'Generic Message Display Variables
        public string AppMessageContinueUrl { get { return (string)Session[VMVServicesGlobal.Consts.AppMessageContinueUrlKey]; } set { Session[VMVServicesGlobal.Consts.AppMessageContinueUrlKey] = value; } }
        public string AppMessageHeader { get { return (string)Session[VMVServicesGlobal.Consts.AppMessageHeaderKey]; } set { Session[VMVServicesGlobal.Consts.AppMessageHeaderKey] = value; } }
        public string AppMessageBody { get { return (string)Session[VMVServicesGlobal.Consts.AppMessageBodyKey]; } set { Session[VMVServicesGlobal.Consts.AppMessageBodyKey] = value; } }
        public VMVServicesGlobal.MessageImageEnum AppMessageImage { get { return (VMVServicesGlobal.MessageImageEnum)Session[VMVServicesGlobal.Consts.AppMessageImageKey]; } set { Session[VMVServicesGlobal.Consts.AppMessageImageKey] = value; } }
		//'End of Generic Message Display Variables

        public SysError CurrentSysError { get { return (SysError)Session[VMVServicesGlobal.Consts.CurrentSysErrorKey]; } set { Session[VMVServicesGlobal.Consts.CurrentSysErrorKey] = value; } }

		private bool _useSessionExpireRedirect = true;
		public bool UseSessionExpireRedirect {get{return _useSessionExpireRedirect;}set{_useSessionExpireRedirect = value;}}


		public string WebApplicationVersion
		{
			get
			{
				string webAppVersion = ConfigurationSettings.AppSettings["Application.Version"];
				if (webAppVersion == null) webAppVersion = "-";
				return webAppVersion;
			}
		}

		public BusinessObjectBase LoggedOnUser
		{
            get { return (BusinessObjectBase)Session[VMVServicesGlobal.Consts.LoggedOnUserKey]; }
            set { Session[VMVServicesGlobal.Consts.LoggedOnUserKey] = value; }
		}

		public long LoggedOnUserID
		{
            get { return long.Parse(Session["userID"].ToString()); }
            set { Session["userID"] = value; }
		}

        public object EntityData
        {
            get { return Session["EntityData"]; }
            set { Session["EntityData"] = value; }
        }

        public string GlobalTransactionNumber
        {
            get {
                if (Session[VMVServicesGlobal.Consts.TransactionNumber] != null)
                {
                    return Session[VMVServicesGlobal.Consts.TransactionNumber].ToString();
                }
                else
                {
                     return  string.Empty;
                }
            }
            set { Session[VMVServicesGlobal.Consts.TransactionNumber] = value; }

        }
        public string LoggedSchema
        {
            get { return Session[VMVServicesGlobal.Consts.LoggedSchemaName].ToString().ToUpper(); }
            set { Session[VMVServicesGlobal.Consts.LoggedSchemaName] = value; }
 
        }

        public int IsPmtverifcation
        {
            get { return int.Parse(Session[VMVServicesGlobal.Consts.IsPmtverifcation].ToString()); }
            set { Session[VMVServicesGlobal.Consts.IsPmtverifcation] = value; }

        }

        public string LoggedSchemaConnection
        {
            get {
                if (Session[VMVServicesGlobal.Consts.LoggedSchemaConnectionString] != null)
                {
                    return Session[VMVServicesGlobal.Consts.LoggedSchemaConnectionString].ToString().ToUpper();
                }
                else
                {
                    return string.Empty;
                    
                }

                }
            set { Session[VMVServicesGlobal.Consts.LoggedSchemaConnectionString] = value; }

        }

        

        public string ReportFile
        {
            get { return Session[VMVServicesGlobal.Consts.ReportFile].ToString().ToUpper(); }
            set { Session[VMVServicesGlobal.Consts.ReportFile] = value; }

        }

        public DataSet ReportData
        {
            get {
                if (Session[VMVServicesGlobal.Consts.ReportData] != null)
                {
                    return  (DataSet)Session[VMVServicesGlobal.Consts.ReportData];
                }
                else
                {
                    return null; 
 
                }
            }
            set { Session[VMVServicesGlobal.Consts.ReportData] = value; }

        }
        public DataSet SubReportData
        {
            get
            {
                if (Session[VMVServicesGlobal.Consts.SubReportData] != null)
                {
                    return (DataSet)Session[VMVServicesGlobal.Consts.SubReportData];
                }
                else
                {
                    return null;

                }
            }
            set { Session[VMVServicesGlobal.Consts.SubReportData] = value; }

        }

        public Hashtable ReportFilterParameter
        {
            get {
                    if (Session[VMVServicesGlobal.Consts.ReportFilterParameter] != null)
                    {
                        return (Hashtable)Session[VMVServicesGlobal.Consts.ReportFilterParameter];
                    }
                    else
                    {
                        Hashtable htFilterParameter = new Hashtable();
                        return htFilterParameter;
                    }
                }
            set { Session[VMVServicesGlobal.Consts.ReportFilterParameter] = value; }

        }

        public Hashtable ReportFormulaParameter
        {
            get
            {
                if (Session[VMVServicesGlobal.Consts.ReportFormulaParameter] != null)
                {
                    return (Hashtable)Session[VMVServicesGlobal.Consts.ReportFormulaParameter];
                }
                else
                {
                    Hashtable htFormulaParameter = new Hashtable();
                    return htFormulaParameter;
                }
            }
            set { Session[VMVServicesGlobal.Consts.ReportFormulaParameter] = value; }

        }

       
        public Hashtable ReportParameter
        {
            get
            {
                if (Session[VMVServicesGlobal.Consts.ReportParameter] != null)
                {
                    return (Hashtable)Session[VMVServicesGlobal.Consts.ReportParameter];
                }
                else
                {
                    Hashtable htParameter = new Hashtable();
                    return htParameter;
                }
            }
            set { Session[VMVServicesGlobal.Consts.ReportParameter] = value; }

        }



		#endregion

		#region Properties for referencing properties stored in the Application object
		public RoleCollection SecurityRoles{get{return (RoleCollection)Application["SECURITYROLES"];}}
		
		public UrlDetail urlDetail
		{
			get
			{
				string pagePath = string.Empty;
				if (this.Request.ApplicationPath == "/")
					pagePath = string.Concat("~", this.Request.Path);
				else
					pagePath = this.Request.Path.Replace(this.Request.ApplicationPath, "~");
                return VMVServicesGlobal.Urls.UrlDetails[pagePath];
			}
		}

		public ServerConnectionStringCollection ServerConnectionStrings{get{return (ServerConnectionStringCollection)Application["SERVERCONNECTIONSTRINGS"];}}
		public ServerConnectionString ServerConnectionString
		{
			get
			{
                string server = VMVServices.Web.Utils.GetRootUrl();
				foreach (ServerConnectionString serverConStr in this.ServerConnectionStrings)
					if (serverConStr.Url == server)
						return serverConStr;
				return new ServerConnectionString();
			}
		}

        string _serverName = string.Empty;
        public string ServerName
        {

            get 
            {
                string connectionString = Settings.ConnectionString;
                _serverName = string.Empty;
                for (int iLoop = connectionString.ToUpper().IndexOf("HOST=") + 5; iLoop < connectionString.Length; iLoop++)
                {
                    if (connectionString.Substring(iLoop,1) != ")")
                    {
                        _serverName += connectionString.Substring(iLoop,1);

                    }
                    else
                    {
                        break;
                    }
                }
 
                
               return  _serverName;
            }
            
        }

        string _oraDBName = string.Empty;
        public string OraDBName
        {

            get
            {
                string connectionString = Settings.ConnectionString;
                _oraDBName = string.Empty;
                for (int iLoop = connectionString.ToUpper().IndexOf("SID=") + 4; iLoop < connectionString.Length; iLoop++)
                {
                    if (connectionString.Substring(iLoop, 1) != ")")
                    {
                        _oraDBName += connectionString.Substring(iLoop, 1);

                    }
                    else
                    {
                        break;
                    }
                }


                return _oraDBName;
            }

        }

        string _dbUser = string.Empty;
        public string DBUser
        {

            get
            {
                string connectionString = Settings.ConnectionString;
                _dbUser = string.Empty;
                for (int iLoop = connectionString.ToUpper().IndexOf("UID=") + 4; iLoop < connectionString.Length; iLoop++)
                {
                    if (connectionString.Substring(iLoop, 1) != ";")
                    {
                        _dbUser += connectionString.Substring(iLoop, 1);

                    }
                    else
                    {
                        break;
                    }
                }


                return _dbUser;
            }

        }



		#endregion

        #region Public Methods, Functions
		public void SetFocus(Control ctrl)
		{this.SetFocus(ctrl, true);}

		public void SetFocus(Control ctrl, bool registerWithBodyOnLoad)
		{
			//'<script language='Javascript'>
			//'<!--
			//'function SetFocus()
			//'   {
			//'       document.getElementById('ctrl.ClientID').focus();
			//'   }
			//'// -->
			//'</script>

			string ClientID = ctrl.ClientID;
			//if(ctrl.GetType() == typeof HPPWebControls.DatePicker Then ClientID += "_txtDate"

			string scrp ;
			scrp = String.Concat("<script language='Javascript'>", Environment.NewLine,
				"<!--", Environment.NewLine,
				"function SetFocus()", Environment.NewLine,
				"{", Environment.NewLine,
				"document.getElementById('", ClientID, "').focus();", Environment.NewLine,
				"}", Environment.NewLine,
				"// -->", Environment.NewLine,
				"</script>");

			ctrl.Page.RegisterClientScriptBlock("SetFocus", scrp);
			if (registerWithBodyOnLoad) AppendEventToBodyTag(_bodyTag, "SetFocus();");
		}

		protected void SetDefaultButtonScript()
		{
			string scrp = string.Empty;

			//'
			//'THE RESULTANT SCRIPT:
			//'

			//'<SCRIPT language=""javascript"">
			//'<!--
			//'    function fnTrapKD(btnID, event){
			//'        btn = findObj(btnID);
			//'        if (document.all){
			//'            if (event.keyCode == 13){
			//'                event.returnValue=false;
			//'                event.cancel = true;
			//'                btn.click();
			//'            }
			//'        }
			//'        else if (document.getElementById){
			//'            if (event.which == 13){
			//'                event.returnValue=false;
			//'                event.cancel = true;
			//'                btn.focus();
			//'                btn.click();
			//'            }
			//'        }
			//'        else if(document.layers){
			//'            if(event.which == 13){
			//'                event.returnValue=false;
			//'                event.cancel = true;
			//'                btn.focus();
			//'                btn.click();
			//'            }
			//'        }
			//'    }
			//
			//'    function findObj(n, d) { 
			//'     var p,i,x;  
			//'        if (!d) d=document; 
			//
			//'        if((p=n.indexOf(""?""))>0 && parent.frames.length) {
			//'      d=parent.frames[n.substring(p+1)].document; 
			//'      n=n.substring(0,p);
			//'     }
			//
			//'        if(!(x=d[n])&&d.all) x=d.all[n]; 
			//
			//'        for (i=0;!x&&i<d.forms.length;i++) 
			//'      x=d.forms[i][n];
			//
			//'        for(i=0;!x&&d.layers&&i<d.layers.length;i++) 
			//'      x=findObj(n,d.layers[i].document);
			//
			//'        if(!x && d.getElementById) x=d.getElementById(n); 
			//
			//'        return x;
			//'    }
			//'// -->
			//'</SCRIPT>

			scrp = string.Concat("", 
				"<SCRIPT language=\"javascript\">", Environment.NewLine, 
				"<!--", Environment.NewLine, 
				"    function fnTrapKD(btnID, event){", Environment.NewLine, 
				"        btn = findObj(btnID);", Environment.NewLine, 
				"        if (document.all){", Environment.NewLine, 
				"            if (event.keyCode == 13){", Environment.NewLine, 
				"                event.returnValue=false;", Environment.NewLine, 
				"                event.cancel = true;", Environment.NewLine, 
				"                btn.click();", Environment.NewLine, 
				"            }", Environment.NewLine, 
				"        }", Environment.NewLine, 
				"        else if (document.getElementById){", Environment.NewLine, 
				"            if (event.which == 13){", Environment.NewLine, 
				"                event.returnValue=false;", Environment.NewLine, 
				"                event.cancel = true;", Environment.NewLine, 
				"                btn.focus();", Environment.NewLine, 
				"                btn.click();", Environment.NewLine, 
				"            }", Environment.NewLine, 
				"        }", Environment.NewLine, 
				"        else if(document.layers){", Environment.NewLine, 
				"            if(event.which == 13){", Environment.NewLine, 
				"                event.returnValue=false;", Environment.NewLine, 
				"                event.cancel = true;", Environment.NewLine, 
				"                btn.focus();", Environment.NewLine, 
				"                btn.click();", Environment.NewLine, 
				"            }", Environment.NewLine, 
				"        }", Environment.NewLine, 
				"    }", Environment.NewLine, 
				"", Environment.NewLine, 
				"    function findObj(n, d) {", Environment.NewLine, 
				"     var p,i,x;", Environment.NewLine, 
				"        if (!d) d=document;", Environment.NewLine, 
				"", Environment.NewLine, 
				"        if((p=n.indexOf(\"?\"))>0 && parent.frames.length) {", Environment.NewLine, 
				"      d=parent.frames[n.substring(p+1)].document;", Environment.NewLine, 
				"      n=n.substring(0,p);", Environment.NewLine, 
				"     }", Environment.NewLine, 
				"", Environment.NewLine, 
				"        if(!(x=d[n])&&d.all) x=d.all[n]; ", Environment.NewLine, 
				"", Environment.NewLine, 
				"        for (i=0;!x&&i<d.forms.length;i++) ", Environment.NewLine, 
				"      x=d.forms[i][n];", Environment.NewLine, 
				"", Environment.NewLine, 
				"        for(i=0;!x&&d.layers&&i<d.layers.length;i++) ", Environment.NewLine, 
				"      x=findObj(n,d.layers[i].document);", Environment.NewLine, 
				"", Environment.NewLine, 
				"        if(!x && d.getElementById) x=d.getElementById(n); ", Environment.NewLine, 
				"", Environment.NewLine, 
				"        return x;", Environment.NewLine, 
				"    }", Environment.NewLine, 
				"// -->", Environment.NewLine, 
				"</SCRIPT>", Environment.NewLine);

			this.RegisterStartupScript("DefaultButtonScript", scrp);
		}

		public void SetDefaultButton(TextBox textBox, Control submitCtrl )
		{
			this.SetDefaultButtonScript();
			textBox.Attributes.Add("onkeydown", String.Concat("fnTrapKD('", submitCtrl.ClientID, "',event)"));
		}

		public void SetDefaultButton(TextBox[] textBoxes, Control submitCtrl)
		{
			foreach(TextBox textBox in textBoxes)
			{
				this.SetDefaultButton(textBox, submitCtrl);
			}
		}

        public void DisplayAppMessage(string MessageBody, string MessageHeader, string MessageContinueUrl, VMVServicesGlobal.MessageImageEnum MessageImage, bool endResponse)
		{
			if(MessageContinueUrl == string.Empty || MessageContinueUrl == null)
			{
				MessageContinueUrl = this.Request.RawUrl;
			}

			AppMessageHeader = MessageHeader;
			AppMessageBody = MessageBody;
			AppMessageContinueUrl = MessageContinueUrl;
			AppMessageImage = MessageImage;

            this.goTo("../Message/Message.aspx", endResponse);
		}

		public void DisplayDeleteCompleteMessage(string ContinueUrl)
		{
            this.DisplayAppMessage(string.Concat("The record was successfully deleted from the system."), "", ContinueUrl, VMVServicesGlobal.MessageImageEnum.Succeeded, false);
		}

		public void DisplaySaveCompleteMessage(string ContinueUrl)
		{
            this.DisplayAppMessage(string.Concat("The record was successfully saved to the system."), "", ContinueUrl, VMVServicesGlobal.MessageImageEnum.Succeeded, false);
		}

        public void DisplayErrorMessage(string errorDesc, string ContinueUrl, BusinessObjectBase o)
        {
            this.DisplayAppMessage(errorDesc,"", ContinueUrl, VMVServicesGlobal.MessageImageEnum.Warning, false);

            //if (ContinueUrl == string.Empty || ContinueUrl == null)
            //{
            //    ContinueUrl = this.Request.RawUrl;
            //}

            //AppMessageHeader = "";
            //ErrorCollection = ErrorCollections;
            //AppMessageContinueUrl = ContinueUrl;
            //AppMessageImage = VMVServicesGlobal.MessageImageEnum.Warning;

            //this.goTo(ContinueUrl, false);
        }

		public void HandleError(string IntroMessage , Exception Ex )
		{

            if (Ex.GetType() == typeof(VMVServices.Services.ErrorManagement.DatabaseConnectionException))
			{
                this.DisplayAppMessage(Ex.Message, "Trouble connecting to database.", null, VMVServicesGlobal.MessageImageEnum.Information, true);
				return;
			}
			//switch (typeof(Ex).ToString())
			//{
			//	case typeof(iThink.Services.ErrorManagement.DatabaseConnectionException).ToString():
			//	this.DisplayAppMessage(Ex.Message, "Database Connection Problem", null, Globals.MessageImageEnum.Information);
			//	return;
			//}

			string tmpstr = string.Concat("<strong>", IntroMessage, "</strong><br><br>");
			Exception tmpex;

			//'LOG THE ERROR TO THE DATABASE, BUT IF THERE ARE ERRORS HERE THEN IGNORE THEM
            try { VMVServices.Services.ErrorManagement.Publish(Ex, this.LoggedOnUserID, VMVServices.Settings.Instance.PublishErrorDetails); }
			catch{/* do nothing / ignore */}

			tmpex = Ex;
			while (tmpex != null)
			{
				tmpstr += string.Concat("<strong>Message:</strong> ", tmpex.Message, "<br><br>", 
					"<strong>Stack Trace:</strong> ", tmpex.StackTrace.Replace(Environment.NewLine, "<br><br>"), "<br><br><br><br>");

				tmpex = tmpex.InnerException;
			}

			//'DISPLAY THE ERROR TO THE USER
            this.DisplayAppMessage(tmpstr, "Error encountered.", null, VMVServicesGlobal.MessageImageEnum.Failed, true);

		}

		public void goTo(string Url,bool endResponse)
		{
            //if (Url == null || Url == string.Empty)
            //    this.DisplayAppMessage("A valid internet address (Url) was not supplied, please check with an administrator or notify the webmaster on <a href='mailto:allen@ansdata.co.za'>allen@ansdata.co.za</a>", "Invalid Address", null, VMVServicesGlobal.MessageImageEnum.Information, false);
            //else
            Response.Redirect(Url, endResponse); //endResponse true for Exceptions, false for success completion
		}
		public bool IsInRoles(string[] roles)
		{
			foreach (string role in roles)
			{
				if (this.Context.User.IsInRole(role))
					return true;
			}
			return false;
		}

		public void SaveBusinessObject(BusinessObjectBase obj)
		{
			this.SaveBusinessObject(obj, string.Empty);
		}
		public void SaveBusinessObject(BusinessObjectBase obj, string successUrl)
		{
			SaveBusinessObject(obj, successUrl, false);
		}
		public void SaveBusinessObject(BusinessObjectBase obj, string successUrl, bool showSuccessMessage)
		{
			try
			{
				obj.Save(this.LoggedOnUser.ObjID);

				if (showSuccessMessage) 
					this.DisplaySaveCompleteMessage(successUrl);
				else if (successUrl != string.Empty)
					this.goTo(successUrl,false);

			}
            catch (VMVServices.Services.ErrorManagement.RecordAlreadyExistsException)
			{
				if (this.RecordAlreadyExistsErrorMessage == string.Empty)
                    this.DisplayAppMessage(string.Concat("A record already exists in the database for the specified ", obj.ObjTypeName), string.Concat(obj.ObjTypeName, " already exists"), null, VMVServicesGlobal.MessageImageEnum.Warning, true);
				else
                    this.DisplayAppMessage(this.RecordAlreadyExistsErrorMessage, "Record already exists", null, VMVServicesGlobal.MessageImageEnum.Warning, true);
			}
            catch (VMVServices.Services.ErrorManagement.CanNotGetNextValueException)
			{
                this.DisplayAppMessage(string.Concat("Unable to add the data to the system for the specified ", obj.ObjTypeName, ": ", obj.ObjDescription), "Unable to add data.", null, VMVServicesGlobal.MessageImageEnum.Failed, true);
			}
            catch (VMVServices.Services.ErrorManagement.DatabaseConnectionException)
			{
                this.DisplayAppMessage("The system database is currently unavaliable, please try again at another time.", "System unavaliable", null, VMVServicesGlobal.MessageImageEnum.Information, true);
			}
			catch (Exception ex)
			{
				if (ex.Message.StartsWith("Timeout expired"))
                    this.DisplayAppMessage("The operation took too long to run (timed out). Please try again of notify a system <a href='mailto:allen@ansdata.co.za'>administrator</a> if the problem persists.", "Procedure took too long", null, VMVServicesGlobal.MessageImageEnum.Failed, true);
				else
                    this.HandleError("You encountered an unexpected problem.", ex);
			}
		}
		public void DeleteBusinessObject(BusinessObjectBase obj)
		{
			this.DeleteBusinessObject(obj, string.Empty, null);
		}
		public void DeleteBusinessObject(BusinessObjectBase obj, string successUrl)
		{
			this.DeleteBusinessObject(obj, successUrl, null);
		}
        public void DeleteBusinessObject(BusinessObjectBase obj, VMVServices.Helpers.Utils.VoidProcedure PostDeleteProcedures)
		{
			this.DeleteBusinessObject(obj, string.Empty, PostDeleteProcedures);
		}
        public void DeleteBusinessObject(BusinessObjectBase obj, string successUrl, VMVServices.Helpers.Utils.VoidProcedure PostDeleteProcedures)
		{
			try
			{
				obj.Delete(this.LoggedOnUser.ObjID);
				obj = null;
				if (PostDeleteProcedures != null) PostDeleteProcedures();
				if (successUrl != string.Empty) this.goTo(successUrl,false);
			}
            catch (VMVServices.Services.ErrorManagement.DatabaseConnectionException)
			{
                this.DisplayAppMessage("The system database is currently unavaliable, please try again at another time.", "System unavaliable", null, VMVServicesGlobal.MessageImageEnum.Information, true);
			}
            catch (VMVServices.Services.ErrorManagement.DependantRecordDeletionException)
			{
				if (this.DependantRecordDeletionErrorMessage == string.Empty)
                    this.DisplayAppMessage(string.Concat("You can not delete the ", obj.ObjTypeName, "<BR><BR>", obj.ObjDescription, "<BR><BR>because there is other data in the system that references this record."), "Can not Delete", successUrl, VMVServicesGlobal.MessageImageEnum.Warning, true);
				else
                    this.DisplayAppMessage(this.DependantRecordDeletionErrorMessage, "Can not delete record", successUrl, VMVServicesGlobal.MessageImageEnum.Warning, true);
			}
			catch (Exception ex)
			{
				this.HandleError("You encountered an unexpected problem.", ex);
			}
		}

        public bool GetIntToBoolean(int valueToChange)
        {
            if (valueToChange == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public string  GetNumberToString(int valueToChange)
        {
            if (valueToChange == 0)
            {
                return string.Empty;
            }
            else
            {
                return valueToChange.ToString();
            }
        }
        public string GetNumberToString(double  valueToChange)
        {
            if (valueToChange == 0)
            {
                return string.Empty;
            }
            else
            {
                return valueToChange.ToString();
            }
        }
        public string GetNumberToString(long valueToChange)
        {
            if (valueToChange == 0)
            {
                return string.Empty;
            }
            else
            {
                return valueToChange.ToString();
            }
        }

        
		protected virtual void SetPageDetails(){}

		protected void setDataListSelectedItemIndex(DataList dl, DataTable dt, string fieldName, int fieldValue)
		{
			for (int i = 0; i < dt.Rows.Count; i+=1)
			{
				if (int.Parse(dt.Rows[i][fieldName].ToString()) == fieldValue)
				{
					dl.SelectedIndex = i;
					break;
				}
			}
		}

		protected void setDataListSelectedItemIndex(DataList dl, BusinessObjectBase[] objs, int fieldValue)
		{
			for (int i = 0; i < objs.Length; i+=1)
			{
				if (objs[i].ObjID == fieldValue)
				{
					dl.SelectedIndex = i;
					break;
				}
			}
		}


		protected int GetIntFromQueryString(string key)
		{
			return GetIntFromQueryString(key, 0);
		}

		protected int GetIntFromQueryString(string key, int defaultValue)
		{
			string tmp = this.Request[key];
			if (tmp == null || tmp.Length == 0)
				return defaultValue;
			else
				return int.Parse(tmp);
		}
		protected string GetStringFromQueryString(string key)
		{
			return GetStringFromQueryString(key, string.Empty);
		}
		protected string GetStringFromQueryString(string key, string defaultValue)
		{
			string tmp = this.Request[key];
			if (tmp == null)
				return defaultValue;
			else
				return tmp;
		}

		#endregion

		#region Protected & Private Methods, Funtions
		public void AppendEventToBodyTag(HtmlGenericControl BodyTag, string EventText)
		{

			if (BodyTag.Attributes["onload"] == null)
				BodyTag.Attributes.Add("onload", EventText);
			else
				if (BodyTag.Attributes["onload"].IndexOf(EventText) == -1)
				BodyTag.Attributes["onload"] += EventText;
		}
		public void RemoveEventFromBodyTag(HtmlGenericControl BodyTag, string EventText)
		{
			if (BodyTag.Attributes["onload"] != null)
				BodyTag.Attributes["onload"] = BodyTag.Attributes["onload"].Replace(EventText, string.Empty);
		}

		public void SetupSessionExpireRedirect()
		{
			string redirectTimeout;
			string redirectUrl;

			if (this.UseSessionExpireRedirect)
			{
                if (VMVServices.Settings.Instance.UseScreenSaver)
				{
                    redirectUrl = string.Concat(Page.ResolveUrl(VMVServicesGlobal.Urls.ScreenSaverPageUrl), "?from=", Request.Url.ToString());
                    redirectTimeout = (VMVServices.Settings.Instance.ScreenSaverDisplayTime * 60 * 4000 - 6000).ToString();
				}
				else
				{
                    redirectUrl = Page.ResolveUrl(VMVServicesGlobal.Urls.LogoffPageUrl);
					redirectTimeout = (Session.Timeout * 60 * 4000 -60000).ToString(); //use session.timout, convert it to milliseconds,  less five second
				}
				SetupSessionExpireRedirect(redirectUrl, redirectTimeout);
			}

		}
		public void SetupSessionExpireRedirect(string redirectUrl, string redirectTimeout)
		{
            string str = VMVServices.Helpers.JavaScriptBuilder.ScriptTagWrapper(VMVServices.Helpers.JavaScriptBuilder.FunctionWrapper("SessionExpireRedirect()", String.Concat("window.setTimeout(\"window.location.href='", redirectUrl, "'\",", redirectTimeout, ");")), true);
			this.RegisterStartupScript("SessionExpireRedirect", str);
            //temp comment
			//AppendEventToBodyTag(_bodyTag, "SessionExpireRedirect();");
		}

		public void Logout(){Logout(true);}
		public void Logout(bool removeIdFromCache)
		{
			//sign out of the application
			FormsAuthentication.SignOut();

			//remove the counter from the context object
			try { if (removeIdFromCache) Cache.Remove(this.LoggedOnUser.ObjID.ToString()); } 
			catch{/*do nothing*/}
		}

		public void Login(SysUser user)
		{
			//'USER EXISTS IN THE SYSTEM
			//'NOW CHECK IF THE USER IS ALREADY LOGGED IN
			if (Cache[user.SysUserID.ToString()] == null)
			{
				//'THE AUTHENTICATED USERS ID IS NOT IN THE CACHE
				//'THEREFORE THEY ARE LOGGING IN FOR THE FIRST TIME

				//'INSERT THE CACHE ITEM THAT INDICATES THIS SPECIFIC USER IS LOGGED ON
				Cache.Insert(user.SysUserID.ToString(), 
					(object)user.SysUserID, 
					null, 
					DateTime.MaxValue, 
					new TimeSpan(0,Session.Timeout,0), 
					System.Web.Caching.CacheItemPriority.NotRemovable, 
					null);
			}

			//SETUP IDENTITY AND ROLES BASE DAUTHENTICATION
			FormsAuthenticationTicket authTicket = new 
				FormsAuthenticationTicket(1, //version
				user.Username, //logged on users name
				DateTime.Now, //creatoin
				DateTime.Now.AddMinutes(60), //expiration
				false, //persistent
				user.Roles);//roles

			//NOW ENCRYPT THE TICKET
			string encryptedTicket = FormsAuthentication.Encrypt(authTicket);
			// CREATE A COOKIE AND ADD THE ENCRYPTED TICKET TO THE
			// COOKIE AS DATA.
			HttpCookie authCookie = 
				new HttpCookie(FormsAuthentication.FormsCookieName,
				encryptedTicket);

			// ADD THE COOKIE TO THE OUTGOING COOKIES COLLECTION.
			Response.Cookies.Add(authCookie);
		}

		#endregion

		#region Page Framework Template Related Code

		PlaceHolder _container;
		//string _templatePath = string.Empty;
		
		//constructor
		public PageBase()
		{
			_container = new PlaceHolder();
			_container.ID = "_container";
            
			//try { _templatePath = iThinkGlobal.Urls.DefaultTemplateControlUrl; } 
			//catch {}
		}

		public string TemplatePath
		{
			get
			{
				string pageTemplatePath = string.Empty;
				try  { pageTemplatePath = this.urlDetail.TemplatePath; } 
				catch{/*do nothing*/}

                //commented by sudhakar on 240122010
                //if (pageTemplatePath == string.Empty)
                //    return iThinkGlobal.Urls.DefaultTemplateControlUrl;
                //else
                //    return pageTemplatePath;

                return pageTemplatePath;

			}
		}
		

		protected override void AddParsedSubObject(object obj)
		{
			// As controls are parsed on this page, add them to
			// our container control which we will inject into
			// out template later (in OnInit)
			//
			//if (_templatePath == String.Empty)
			if (TemplatePath == String.Empty || TemplatePath == "-")
				base.AddParsedSubObject(obj);
			else
				_container.Controls.Add((Control)obj);

			// don't chain to base page so 
			// that controls on this page are not added 
			// directly as children to the page

		}


		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			if (TemplatePath == String.Empty || TemplatePath == "-")
			{
				base.OnInit(e);
				return;
			}

			// load an .ascx file here and inject content into placeholder 
			// (currently hardcoded to template.ascx)
			// Here we a reloading the Template User Control into memory for use
			Control template = LoadControl(TemplatePath);
			if (template == null) throw new Exception("Error - missing template file: " + TemplatePath);

			//Locate control called _bodyTag, this represent the html body tag
			_bodyTag = (HtmlGenericControl)template.FindControl("_bodyTag");

			// Locate control called _placeHolder on the Template control, this placeholder control 
			// is where all the content of the page will be copied to.
			Control placeHolder = template.FindControl("_mainContentPlaceHolder");
			if (placeHolder == null) throw new Exception("Error - template is missing control with id of _mainContentPlaceHolder");

			// Earlier a control called _container was created and all the controls for the content
			// page were added to this control, we now ad dthis _container control to the placeholder control 
			// found on the template control. Confusing i know but stay with me!!!
			placeHolder.Controls.Add(_container);

			// Now we loop through all the controls in the template control (this will include all the control
			// from the content page that were previously added)
			// and copy them from the template control back onto the page, but now they conform to the structure
			// laid out in the template control
			// 
			while (template.Controls.Count > 0)
			{
				Control c = template.Controls[0]; //always get the control from the "bottom of the pile"
				template.Controls.Remove(c);
				Controls.Add(c);
			}
			InitializeComponent();
			base.OnInit(e);
		}
		
		#endregion


		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
        #region UserName and MachineName WithOut Trigger on 20/09/2011
        public string LoggedUserName
        {
            get { return Session[VMVServicesGlobal.Consts.UserName].ToString().ToUpper(); }
            set { Session[VMVServicesGlobal.Consts.UserName] = value; }


        }
        public string LoggedMachineName
        {
            get { return Session[VMVServicesGlobal.Consts.Machine_Name].ToString().ToUpper(); }
            set { Session[VMVServicesGlobal.Consts.Machine_Name] = value; }
        }
        #endregion UserName and MachineName WithOut Trigger

	}
}
