using System;
using System.Web;
using System.Web.UI.WebControls;

using System.Drawing;
using System.Collections;


namespace VMVServices.Web
{
    /// <summary>
    /// Summary description for Utils.
    /// </summary>
    public class Utils
    {
        public static void LoadDropDownList(DropDownList ddl, string dataTextField, string dataValueField, object dataSource, bool includeBlankItem, bool includeAllItem)
        {
            ddl.DataTextField = dataTextField;
            ddl.DataValueField = dataValueField;
            ddl.DataSource = dataSource;
            ddl.DataBind();
            if (includeBlankItem) ddl.Items.Insert(0, new ListItem("", "-1"));
            if (includeAllItem) ddl.Items.Insert(0, new ListItem(VMVServices.Settings.DropDownAllItemText, "-1"));
        }

       

        public static string GetRootUrl()
        {
            string url = HttpContext.Current.Request.Url.ToString();
            string rawurl = HttpContext.Current.Request.RawUrl;

            if (url.IndexOf("?") >= 0)
                url = url.Remove(url.IndexOf("?"), url.Length - url.IndexOf("?"));

            if (rawurl.IndexOf("?") >= 0)
                rawurl = rawurl.Remove(rawurl.IndexOf("?"), rawurl.Length - rawurl.IndexOf("?"));

            string server = url.Replace(rawurl, "");

            return server;
        }

        public static void DownLoadFile(string strFileWithPath, string strFileName)
        {
            try
            {
                System.IO.FileStream fs = null;


                fs = System.IO.File.Open(strFileWithPath, System.IO.FileMode.Open);
                byte[] btFile = new byte[fs.Length];
                fs.Read(btFile, 0, Convert.ToInt32(fs.Length));

                fs.Close();

                HttpContext.Current.Response.AddHeader("Content-disposition", "attachment; filename=" +
                                   strFileName);
                HttpContext.Current.Response.ContentType = "application/octet-stream";
                HttpContext.Current.Response.BinaryWrite(btFile);
                HttpContext.Current.Response.End();
            }
            catch
            {
            }

        }

        static Bitmap _bitmap;
        public static Bitmap BarcodeBitmap
        {
            get { return _bitmap; }
            set { _bitmap = value; }

        }
        public static string OrgLogo
        {
            get { return (HttpContext.Current.Session["OrgLogo"].ToString()); }
            set { HttpContext.Current.Session["OrgLogo"] = value; }
        }
        public static string FormCode
        {
            get { return (HttpContext.Current.Session["AppFormCode"].ToString()); }
            set { HttpContext.Current.Session["AppFormCode"] = value; }
        }
        public static string Mode
        {
            get { return (HttpContext.Current.Session["AppMode"].ToString()); }
            set { HttpContext.Current.Session["AppMode"] = value; }
        }
        public static int RecordId
        {
            get { return Convert.ToInt32(HttpContext.Current.Session["AppRecordId"].ToString()); }
            set { HttpContext.Current.Session["AppRecordId"] = value; }
        }
        public static string StrRecordId
        {
            get { return (HttpContext.Current.Session["AppStrRecordId"].ToString()); }
            set { HttpContext.Current.Session["AppStrRecordId"] = value; }
        }
        public static string EmpPhoto
        {
            get { return (HttpContext.Current.Session["EmpPhoto"].ToString()); }
            set { HttpContext.Current.Session["EmpPhoto"] = value; }
        }

        public static string UserId
        {
            get { return (HttpContext.Current.Session["UserCode"].ToString()); }
            set { HttpContext.Current.Session["UserCode"] = value; }
        }
        public static string UserName
        {
            get { 
                return (HttpContext.Current.Session["UserName"].ToString()); }
            set { HttpContext.Current.Session["UserName"] = value; }
        }

        public static string AddAllowed
        {
            get { return (HttpContext.Current.Session["AddAllowed"].ToString()); }
            set { HttpContext.Current.Session["AddAllowed"] = value; }
        }
        public static long GroupID
        {
            get { return Convert.ToInt16(HttpContext.Current.Session["GroupID"].ToString()); }
            set { HttpContext.Current.Session["GroupID"] = value; }
        }

        public static Boolean IsNew
        {
            get { return Convert.ToBoolean(HttpContext.Current.Session["IsNew"].ToString()); }
            set { HttpContext.Current.Session["IsNew"] = value; }
        }

        public static string OrganizationID
        {
            get
            {              
                return (HttpContext.Current.Session["OrganizationID"].ToString());
            }
            set { 
                
                HttpContext.Current.Session["OrganizationID"] = value; 
            
            }
        }
        public static string IsAlert
        {
            get
            {
                return (HttpContext.Current.Session["IsAlert"].ToString());
            }
            set
            {

                HttpContext.Current.Session["IsAlert"] = value;

            }
        }
        public static string RecordID
        {
            get
            {
                return (HttpContext.Current.Session["RecordID"].ToString());
            }
            set
            {

                HttpContext.Current.Session["RecordID"] = value;

            }
        }
        public static string DecimalPrecision
        {
            get
            {
                return (HttpContext.Current.Session["DecimalPrecision"].ToString());
            }
            set
            {

                HttpContext.Current.Session["DecimalPrecision"] = value;

            }
        }
        public static string CommaSeparation
        {
            get
            {
                return (HttpContext.Current.Session["CommaSeparation"].ToString());
            }
            set
            {

                HttpContext.Current.Session["CommaSeparation"] = value;

            }
        }
        public static string LanguageCode
        {
            get
            {
                return (HttpContext.Current.Session["LanguageCode"].ToString());
            }
            set
            {

                HttpContext.Current.Session["LanguageCode"] = value;

            }
        }
        public static string SavedRecordId
        {
            get
            {
                return (HttpContext.Current.Session["SavedRecordId"].ToString());
            }
            set
            {

                HttpContext.Current.Session["SavedRecordId"] = value;

            }
        }
        public static string RecordMsg
        {
            get
            {
                return (HttpContext.Current.Session["RecordMsg"].ToString());
            }
            set
            {

                HttpContext.Current.Session["RecordMsg"] = value;

            }
        }

        public static string Email
        {
            get { return (HttpContext.Current.Session["Email"].ToString()); }
            set { HttpContext.Current.Session["Email"] = value; }
        }
        public static string OrganizationName
        {
            get { return (HttpContext.Current.Session["OrganizationName"].ToString()); }
            set { HttpContext.Current.Session["OrganizationName"] = value; }
        }
        public static string Address1
        {
            get { return (HttpContext.Current.Session["Address1"].ToString()); }
            set { HttpContext.Current.Session["Address1"] = value; }
        }
        public static string Address2
        {
            get { return (HttpContext.Current.Session["Address2"].ToString()); }
            set { HttpContext.Current.Session["Address2"] = value; }
        }
        public static string Address3
        {
            get { return (HttpContext.Current.Session["Address3"].ToString()); }
            set { HttpContext.Current.Session["Address3"] = value; }
        }
        public static string Telephone
        {
            get { return (HttpContext.Current.Session["Telephone"].ToString()); }
            set { HttpContext.Current.Session["Telephone"] = value; }
        }
        public static string Mobile
        {
            get { return (HttpContext.Current.Session["Mobile"].ToString()); }
            set { HttpContext.Current.Session["Mobile"] = value; }
        }
        public static string Fax
        {
            get { return (HttpContext.Current.Session["Fax"].ToString()); }
            set { HttpContext.Current.Session["Fax"] = value; }
        }
        public static string SMSAllowed
        {
            get { return (HttpContext.Current.Session["SMSAllowed"].ToString()); }
            set { HttpContext.Current.Session["SMSAllowed"] = value; }
        }
        public static Boolean Multilanguage
        {
            get { return Convert.ToBoolean(HttpContext.Current.Session["Multilanguage"].ToString()); }
            set { HttpContext.Current.Session["Multilanguage"] = value; }
        }


        public static Hashtable ReportFilterParameter
        {
            get
            {
                if (HttpContext.Current.Session[VMVServicesGlobal.Consts.ReportFilterParameter] != null)
                {
                    return (Hashtable)HttpContext.Current.Session[VMVServicesGlobal.Consts.ReportFilterParameter];
                }
                else
                {
                    Hashtable htFilterParameter = new Hashtable();
                    return htFilterParameter;
                }
            }
            set { HttpContext.Current.Session[VMVServicesGlobal.Consts.ReportFilterParameter] = value; }

        }
        public static Hashtable ReportViewFilterParameter
        {
            get
            {
                if (HttpContext.Current.Session[VMVServicesGlobal.Consts.ReportViewFilterParameter] != null)
                {
                    return (Hashtable)HttpContext.Current.Session[VMVServicesGlobal.Consts.ReportViewFilterParameter];
                }
                else
                {
                    Hashtable htFilterParameter = new Hashtable();
                    return htFilterParameter;
                }
            }
            set { HttpContext.Current.Session[VMVServicesGlobal.Consts.ReportViewFilterParameter] = value; }

        }
    }
}
