using System;
using System.Configuration;
using VMVServices.Services.Configuration.ConfigurationSectionHandlers;
using VMVServices.Helpers;
using System.Globalization;
using System.Web;

namespace VMVServices
{
    /// <summary>
    /// Summary description for Settings.
    /// </summary>
    public class Settings
    {
        public Settings() { }

        static Settings _Instance;
        static  VMVServices.Web.PageBase PB = new VMVServices.Web.PageBase();
        
        public static Settings Instance
        {
            get
            {
                if (_Instance == null) _Instance = new Settings();
                return _Instance;
            }
        }
        static string _SchemaCon;
        public static string SchemaCon
        {
            get
            {
                //if (_SchemaCon == null) _SchemaCon = new Settings();
                return _SchemaCon;
            }
            set
            {
                _SchemaCon = value;
            }
        }
        static string _CurrentSessionID;
        public static string CurrentSessionID
        {
            get
            {
                //if (_SchemaCon == null) _SchemaCon = new Settings();
                return _CurrentSessionID;
            }
            set
            {
                _CurrentSessionID = value;
            }
        }

        
        //
        //Settings Properties
        //

        public string NextIDValueLockTableName { get { return VMVServices.Helpers.Configuration.GetString("AutoNumberGeneration.NextIDValueLockTableName", "tblSysNextValueLock"); } }
        public string NextIDValueTableName { get { return VMVServices.Helpers.Configuration.GetString("AutoNumberGeneration.NextIDValueTableName", "tblSysNextValue"); } }
        public string GetNextIDValueTimeOutSeconds { get { return VMVServices.Helpers.Configuration.GetString("AutoNumberGeneration.GetNextIDValueTimeOutSeconds", "5"); } }

        public  string DefaultDateFormatString
        {
            get
            {
                return VMVServices.Helpers.Configuration.GetString("DefaultDateFormatString", "dd/MM/yyyy");
            }
        }
        public CultureInfo DefaultDateCulture

        {
            get
            {
                return CultureInfo.CreateSpecificCulture("en-GB"); 
            }
        }
        public string DefaultDateTimeFormatString { get { return VMVServices.Helpers.Configuration.GetString("DefaultDateTimeFormatString", "dd/MM/yyyy HH:mm"); } }
        public string FriendlyDateFormatString { get { return VMVServices.Helpers.Configuration.GetString("FriendlyDateFormatString", "dd MMM yyyy"); } }
        public string FriendlyDateTimeFormatString { get { return VMVServices.Helpers.Configuration.GetString("FriendlyDateTimeFormatString", "dd MMM yyyy HH:mm ss"); } }
        public bool PublishErrorDetails
        {
            get { return VMVServices.Helpers.Configuration.GetBool("VMVServices.Settings.PublishErrorDetails", true); }
        }

        public bool ProgramDoesAuditting { get { return VMVServices.Helpers.Configuration.GetBool("VMVServices.Settings.ProgramDoesAuditting", false); } }

        public string SmtpServer { get { return VMVServices.Helpers.Configuration.GetString("VMVServices.Settings.SmtpServer", "local"); } }
        public string FromEmailAddress { get { return VMVServices.Helpers.Configuration.GetString("VMVServices.Settings.FromEmailAddress", "allen@ansdata.co.za"); } }

        public bool UseScreenSaver { get { return VMVServices.Helpers.Configuration.GetBool("VMVServices.Settings.UseScreenSaver", false); } }
        public int ScreenSaverTimeout { get { return VMVServices.Helpers.Configuration.GetInt("VMVServices.Settings.ScreenSaverTimeout", 8); } }
        public int ScreenSaverDisplayTime { get { return VMVServices.Helpers.Configuration.GetInt("VMVServices.Settings.ScreenSaverDisplayTime", 8); } }
        public int MaxEmailSendRetryNo { get { return VMVServices.Helpers.Configuration.GetInt("VMVServices.Settings.MaxEmailSendRetryNo", 5); } }


        public const string DropDownAllItemText = "[ All ]";

        static  string _connectionString;
        public static  string ConnectionString
        {
            get
            {

                ////if (_connectionString == string.Empty)
                ////    _connectionString = Configuration.GetString("connectionString", string.Empty);
                ////if (_connectionString == string.Empty)
                ////    _connectionString = System.Configuration.ConfigurationSettings.AppSettings.Get("serverConnectionString");//this.getServerConnectionString();

                //if (VMVServices.Settings.SchemaCon == string.Empty || VMVServices.Settings.SchemaCon == null)
                //{
                //    _connectionString = System.Configuration.ConfigurationSettings.AppSettings.Get("serverConnectionString");//this.getServerConnectionString();
                //}
                //else
                //{


                //    _connectionString = PB.LoggedSchemaConnection; 
                //}
                //string ss = HttpContext.Current.Session["ss"].ToString();

                
                    _connectionString = System.Configuration.ConfigurationSettings.AppSettings.Get("serverConnectionString");//this.getServerConnectionString();
                

                     

                return _connectionString;
            }
        }
        public string AonConnectionString
        {
            get
            {
                if (_connectionString == string.Empty)
                    _connectionString = Configuration.GetString("connectionString", string.Empty);
                if (_connectionString == string.Empty)
                    _connectionString = System.Configuration.ConfigurationSettings.AppSettings.Get("AonserverConnectionString");//this.getServerConnectionString();

                return _connectionString;
            }
        }
        string getServerConnectionString()
        {
            string url = System.Web.HttpContext.Current.Request.Url.ToString();
            //clean up url by removing everything after and including the ?
            if (url.IndexOf("?") > -1) url = url.Substring(0, url.IndexOf("?"));

            string rawurl = System.Web.HttpContext.Current.Request.RawUrl;
            //clean up the rawurl
            if (rawurl.IndexOf("?") > -1) rawurl = rawurl.Substring(0, rawurl.IndexOf("?"));

            string server = url.Replace(rawurl, "");
            //foreach (ServerConnectionString serverConStr in (ServerConnectionStringCollection)ConfigurationSettings.AppSettings.Get("serverConnectionString"))

            foreach (ServerConnectionString serverConStr in (ServerConnectionStringCollection)System.Web.HttpContext.Current.Application["serverConnectionString"])
                if (serverConStr.Url == server)
                    return serverConStr.ConnectionString;
            return "No connection string found";
        }
        string getAonServerConnectionString()
        {
            string url = System.Web.HttpContext.Current.Request.Url.ToString();
            //clean up url by removing everything after and including the ?
            if (url.IndexOf("?") > -1) url = url.Substring(0, url.IndexOf("?"));

            string rawurl = System.Web.HttpContext.Current.Request.RawUrl;
            //clean up the rawurl
            if (rawurl.IndexOf("?") > -1) rawurl = rawurl.Substring(0, rawurl.IndexOf("?"));

            string server = url.Replace(rawurl, "");
            //foreach (ServerConnectionString serverConStr in (ServerConnectionStringCollection)ConfigurationSettings.AppSettings.Get("serverConnectionString"))

            foreach (ServerConnectionString serverConStr in (ServerConnectionStringCollection)System.Web.HttpContext.Current.Application["AonserverConnectionString"])
                if (serverConStr.Url == server)
                    return serverConStr.ConnectionString;
            return "No connection string found";
        }


        public string DALAssembly { get { return VMVServices.Helpers.Configuration.GetString("DALAssembly", "System.Data.OracleClient"); } }
        public string DALConnection { get { return VMVServices.Helpers.Configuration.GetString("DALConnection", "System.Data.OracleClient.OracleConnection"); } }
        public string DALCommand { get { return VMVServices.Helpers.Configuration.GetString("DALCommand", "System.Data.OracleClient.OracleCommand"); } }
        public string DALParameter { get { return VMVServices.Helpers.Configuration.GetString("DALParameter", "System.Data.OracleClient.OracleParameter"); } }
        public string DALDataAdapter { get { return VMVServices.Helpers.Configuration.GetString("DALDataAdapter", "System.Data.OracleClient.OracleDataAdapter"); } }
        public string DbEngineType { get { return VMVServices.Helpers.Configuration.GetString("AxonDbEngineType", "VMVServices.Services.Data.MsSqlDbEngine"); } }
    }
}
