using System;
using System.Collections;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace VMVServices.Helpers
{
	/// <summary>
	/// Summary description for LogToXmlFile.
	/// </summary>
	[Serializable(), XmlInclude(typeof(LogToXmlFileItem))]
	public class LogToXmlFile
	{

		XmlSerializer mySerializer = new XmlSerializer(typeof(LogToXmlFile));

		public LogToXmlFile(){}
		public LogToXmlFile(string deserializeFilePath)
		{
			if (File.Exists(deserializeFilePath))
				using (StreamReader sr = new StreamReader(deserializeFilePath))
				{
					this.Items = ((LogToXmlFile)mySerializer.Deserialize(sr)).Items;
				}
		}

		ArrayList _Items = new ArrayList();
		[XmlArray("_Items"), XmlArrayItem("LogToXmlFileItem", typeof(LogToXmlFileItem))]
		public ArrayList Items
		{
			get{return _Items;}
			set{_Items = value;}
		}

		public void Serialize(string filePath)
		{
			if (File.Exists(filePath)) File.Delete(filePath);
			using (StreamWriter sw = new StreamWriter(filePath))
			{
				mySerializer.Serialize(sw, this);
			}
		}
	}

	public class LogToXmlFileItem
	{
		DateTime _LogDate;
		string _ActionEvent;
		string _ActionDescription;

		public DateTime LogDateTime{get{return _LogDate;}set{_LogDate = value;}}
		public string ActionEvent{get{return _ActionEvent;}set{_ActionEvent = value;}}
		public string ActionDescription{get{return _ActionDescription;}set{_ActionDescription = value;}}
	}
}
