using System;
using System.Web;

namespace VMVServices.Helpers
{
	/// <summary>
	/// Summary description for Urls.
	/// </summary>
	public class Urls
	{
		public static string ResolveUrl(string url)
		{
			if (url==null) throw new ArgumentNullException("url", "url can not be null");
			if (url.Length == 0) throw new ArgumentException("The url can not be an empty string", "url");
			
			// there is no ~ in the first character position, just return the url
			if (url[0] != '~') return url;        
			
			string applicationPath = HttpContext.Current.Request.ApplicationPath;
			
			// there is just the ~ in the URL, return the applicatonPath
			if (url.Length == 1)    return applicationPath;  
			
			// assume url looks like ~somePage
			int indexOfUrl=1;
			
			// determine the middle character
			string midPath = (applicationPath.Length >1 )? "/" : string.Empty;
			
			// if url looks like ~/ or ~\ change the indexOfUrl to 2
			if (url[1] == '/' || url[1] == '\\') indexOfUrl=2;
			
			return applicationPath + midPath + url.Substring(indexOfUrl);
		}// ResolveUrl

		public static string AppendItemToUrlQueryString(string url, string itemName, string itemValue)
		{
			string queryStringItem = string.Concat(itemName, "=", itemValue);
			
			if (url.IndexOf(string.Concat(itemName, "=")) != -1) url = StripItemFromUrlQueryString(url, itemName);

			if (url.IndexOf("?")==-1) url += "?"; 

			return url += queryStringItem;
		}

		public static string StripItemFromUrlQueryString(string url, string itemName)
		{
			//find the start poisition of the itemName
			int start = url.IndexOf(string.Concat(itemName, "="));
			int end = url.IndexOf("&", start);

			if (end == -1)
				return url.Remove(start, url.Length-start);
			else
				return url.Remove(start, end-start);
		}


	}
}
