using System;
using System.Data;
using System.Diagnostics;

namespace VMVServices.Helpers
{
	/// <summary>
	/// Summary description for Debugger.
	/// </summary>
	public class Debugger
	{
		public static void PrintCmdDetails(IDbCommand cmd)
		{
			Debug.WriteLine(cmd.CommandText);
			foreach (IDbDataParameter prm in cmd.Parameters)
				Debug.WriteLine(string.Concat(prm.ParameterName, ", ", prm.Value.ToString(), ", ", prm.DbType.ToString()));

		}

	}
}
