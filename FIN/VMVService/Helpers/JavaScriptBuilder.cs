using System;

namespace VMVServices.Helpers
{
	/// <summary>
	/// Summary description for JavaScriptBuilder.
	/// </summary>
	public class JavaScriptBuilder
	{
		public static string ScriptTagWrapper(string scr, bool UseJSComments)
		{
			if (UseJSComments)
				return string.Concat("<script language='javascript'><!-- ", Environment.NewLine, scr, Environment.NewLine, " --></script>");
			else
				return string.Concat("<script language='javascript'> ", Environment.NewLine, scr, Environment.NewLine, " </script>");
		}

		public static string FunctionWrapper(string FunctionName, string scr)
		{	
			return string.Concat("function ", FunctionName, "{", scr, "}");
		}

		public static string JSFileScriptTagWrapper(string filepath)
		{
			return string.Concat("<script language='javascript' src='", filepath, "'></script>");
		}
	}
}
