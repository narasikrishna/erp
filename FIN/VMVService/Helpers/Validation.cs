using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;


namespace VMVServices.Helpers
{
	/// <summary>
	/// Summary description for Validation.
	/// </summary>
	public class Validation
	{
		public static bool isStringNullOrEmpty(string str)
		{
			return (str == null || str == string.Empty);
		}
		public static bool isStringNullOrNotNumeric(string str)
		{
			if (str == null) return true;
			try
			{
				int.Parse(str);
				return false;
			}
			catch{return true;}
		}
        
        public static bool IsDate(string value)
        {
            try
            {
                DateTime datetimeVal;
                CultureInfo ci = new CultureInfo("en-GB");
                bool result = DateTime.TryParse(value, ci, DateTimeStyles.AssumeLocal, out datetimeVal);

                if (result)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                return false;
            }
        }
        public static bool IsValidDate(string checkDate)
        {
            try
            {
                DateTime datetimeVal;
                CultureInfo ci = new CultureInfo("en-GB");
                // checkDate = CommonFunctions.ChangeDateFormat(checkDate);
                bool result = DateTime.TryParse(checkDate, ci, DateTimeStyles.None, out datetimeVal);

                if (result)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                return false;
            }
        }


        public static string FormatMessage(string commonMessage, string messageLabels)
        {
            try
            {
                string[] labels;
                string formatedMessage;
                int labelCount = 0;
                labels = messageLabels.Split(new char[] { ',' });

                formatedMessage = string.Empty;
                for (int iLoop = 0; iLoop < commonMessage.Length - 1; iLoop++)
                {
                    if (commonMessage.Substring(iLoop, 1) != "~")
                    {
                        formatedMessage = formatedMessage + commonMessage.Substring(iLoop, 1);
                    }
                    else
                    {
                        //   messageLabels =  labels[labelCount];
                        formatedMessage = formatedMessage + labels[labelCount];
                        labelCount = labelCount + 1;
                    }
                }
                return formatedMessage;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

	}
}
