using System;
using Microsoft.VisualBasic;

namespace VMVServices.Helpers
{
	/// <summary>
	/// Summary description for DataProtection.
	/// </summary>
	public class DataProtection
	{
		//Contructor
		public DataProtection(){}

		#region RC4 Asymetric encryption and decryption
		protected static int[] sbox = new int[256];
		protected static int[] key = new int[256];
		public static string RC4Encrypt(string plainText, string password, string passwordSalt)
		{
			return RC4EncryptDecrypt(plainText, string.Concat(password, passwordSalt));
		}

		public static string RC4Decrypt(string encryptedText, string password, string passwordSalt)
		{
			return RC4EncryptDecrypt(encryptedText, string.Concat(password, passwordSalt));
		}

		protected static string RC4EncryptDecrypt(string textData, string password)
		{
			int i = 0;
			int j = 0;
			string cipher = string.Empty;

			// Call our method to initialize the arrays used here.
			RC4Initialize(password);

			// Set up a for loop.  Again, we use the Length property
			// of our String instead of the Len() function
			for (int a = 1; a <= textData.Length; a++)
			{
				// Initialize an integer variable we will use in this loop
				int itmp = 0;

				// Like the RC4Initialize method, we need to use the %
				// in place of Mod
				i = (i + 1) % 256;
				j = (j + sbox[i]) % 256;
				itmp = sbox[i];
				sbox[i] = sbox[j];
				sbox[j] = itmp;

				int k = sbox[(sbox[i] + sbox[j]) % 256];

				// Again, since the return type of String.Substring is a
				// string, we need to convert it to a char using
				// String.ToCharArray() and specifying that we want the
				// first value, [0].
				char ctmp = textData.Substring(a - 1, 1).ToCharArray()[0];

				// Use Asc() from the Microsoft.VisualBasic namespace
				itmp = Microsoft.VisualBasic.Strings.Asc(ctmp);

				// Here we need to use ^ operator that C# uses for Xor
				int cipherby = itmp ^ k;

				// Use Chr() from the Microsoft.VisualBasic namespace
				cipher += Microsoft.VisualBasic.Strings.Chr(cipherby);
			}

			// Return the value of cipher as the return value of our
			// method
			return cipher;
		}

		protected static void RC4Initialize(string password)
		{
			// Get the length of the password
			// Instead of Len(), we need to use the Length property
			// of the string
			int intLength = password.Length;


			// Set up our for loop.  In C#, we need to change our syntax.

			// The first argument is the initializer.  Here we declare a
			// as an integer and set it equal to zero.

			// The second argument is expression that is used to test
			// for the loop termination.  Since our arrays have 256
			// elements and are always zero based, we need to loop as long
			// as a is less than or equal to 255.

			// The third argument is an iterator used to increment the
			// value of a by one each time through the loop.  Note that
			// we can use the ++ increment notation instead of a = a + 1
			for (int a = 0; a <= 255; a++)
			{
				// Since we don't have Mid()  in C#, we use the C#
				// equivalent of Mid(), String.Substring, to get a
				// single character from strPwd.  We declare a character
				// variable, ctmp, to hold this value.

				// A couple things to note.  First, the Mod keyword we
				// used in VB need to be replaced with the %
				// operator C# uses.  Next, since the return type of
				// String.Substring is a string, we need to convert it to
				// a char using String.ToCharArray() and specifying that
				// we want the first value in the array, [0].
				char ctmp = (password.Substring((a % intLength),1).ToCharArray()[0]);

				// We now have our character and need to get the ASCII
				// code for it.  C# doesn't have the  VB Asc(), but that
				// doesn't mean we can't use it.  In the beginning of our
				// code, we imported the Microsoft.VisualBasic namespace.
				// This allows us to use many of the native VB functions
				// in C#
                
				// Note that we need to use [] instead of () for our
				// array members.
				key[a] = Microsoft.VisualBasic.Strings.Asc(ctmp);
				sbox[a] = a;
			}

			// Declare an integer x and initialize it to zero.
			int x = 0;

			// Again, create a for loop like the one above.  Note that we
			// need to use a different variable since we've already
			// declared a above.
			for (int b = 0; b <= 255; b++)
			{
				x = (x + sbox[b] + key[b]) % 256;
				int tempSwap = sbox[b];
				sbox[b] = sbox[x];
				sbox[x] = tempSwap;
			}

		}

		#endregion
	}
}
