using System;

namespace VMVServices.Helpers
{
	/// <summary>
	/// Summary description for Configuration.
	/// </summary>
	public class Configuration
	{

		public static string GetString(string key)
		{
			return GetConfigItem(key, string.Empty);
		}
		public static string GetString(string key, string defaultVal)
		{
			return GetConfigItem(key, defaultVal);
		}

		public static int GetInt(string key)
		{
			return GetInt(key, 0);
		}
		public static int GetInt(string key, int defaultVal)
		{
			return int.Parse(GetConfigItem(key, defaultVal.ToString()));
		}

		public static bool GetBool(string key)
		{
			return GetBool(key, false);
		}
		public static bool GetBool(string key, bool defaultVal)
		{
			string val = GetConfigItem(key, "false").ToUpper();
			if (defaultVal)
				val = GetConfigItem(key, "true").ToUpper();
			else
				val = GetConfigItem(key, "false").ToUpper();

			if (val == "TRUE" || val == "1" || val == "YES")
				return true;
			else
				return false;
		}

		static string GetConfigItem(string key, string defaultVal)
		{
			string val = string.Empty;
			try
			{val = System.Configuration.ConfigurationSettings.AppSettings[key];}
			catch
			{val = defaultVal;}

			if (val == null) val = defaultVal;
			return val;
		}
	}
}
