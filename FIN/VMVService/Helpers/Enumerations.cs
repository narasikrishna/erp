using System;
using System.Collections.Generic;
using System.Text;

namespace VMVServices.Helpers
{
    public class Enumerations
    {

        public static int RowNumber=14;
        public enum LookUpType
        {
            Yes_No = 1,
            Class_Name = 2,
            Payment_Mode=3
        }
        public enum LookUpValue
        {

        }
        public enum ApplicationsReports
        {
            CPONEW=24,
            ClaimCostRecoveries=45,
            ClaimsPaidOutstanding=46,
            PremiumsReceiptReg=47,
            ClaimRegistered=48,
            SettlementPostList=49,
            SettlementEditList = 50,
            RPL=51,
            BBL=52,
            MRIContributions=53,
            PPL=54,
            PRR=55,
            ChequeReconcilliation=56
        }
        public enum OracleDataType
        {
            CHAR,
            VARCHAR2,
            NUMBER,
            DATE
        }
    }
}
