using System;
using System.Data;
using System.Data.OracleClient;
using System.Reflection;
using VMVServices.Services.Business;
using VMVServices.Services.Data;
using System.Globalization;
using System.Threading;

namespace VMVServices.Helpers
{
    /// <summary>
    /// Summary description for Utils.
    /// </summary>
    public class Utils
    {
        public delegate void VoidProcedure();

        public static BusinessObjectBase[] MergeBusinessObjectArrays(BusinessObjectBase[][] jarrs)
        {
            int MergedArrCnt = 0;
            int cntr = 0;

            //'GET REQUIRED COUNT FOR NEW MERGED ARRAY
            foreach (BusinessObjectBase[] jarr in jarrs)
            {
                MergedArrCnt += jarr.Length;
            }

            BusinessObjectBase[] MergedArray = new BusinessObjectBase[MergedArrCnt];

            foreach (BusinessObjectBase[] jarr in jarrs)
            {
                foreach (BusinessObjectBase obj in jarr)
                {
                    MergedArray[cntr] = obj;
                    cntr += 1;
                }
            }

            return MergedArray;
        }
        public static SqlFieldAttributeCollection getSqlFieldAttributes(System.Type objType)
        {
            SqlFieldAttributeCollection sqlFields = new SqlFieldAttributeCollection();
            SqlFieldAttribute sqlField;
            object[] atts;
            foreach (PropertyInfo pInfo in objType.GetProperties(BindingFlags.Public | BindingFlags.Instance))
            {
                atts = pInfo.GetCustomAttributes(typeof(VMVServices.Services.Data.SqlFieldAttribute), true);
                if (atts.Length > 0)
                {
                    sqlField = (SqlFieldAttribute)atts[0];
                    sqlField.PropertyName = pInfo.Name;
                    if (sqlField.DbType == DbType.Object)
                        sqlField.DbType = SystemTypeToDbType(pInfo.PropertyType);
                    sqlFields.Add(sqlField);
                }
            }
            return sqlFields;
        }

        public static SqlFieldAttribute getSqlIDFieldAttributes(System.Type objType)
        {
            SqlFieldAttributeCollection sqlFields = new SqlFieldAttributeCollection();
            SqlFieldAttribute sqlField = null;
            object[] atts;
            foreach (PropertyInfo pInfo in objType.GetProperties(BindingFlags.Public | BindingFlags.Instance))
            {
                atts = pInfo.GetCustomAttributes(typeof(VMVServices.Services.Data.SqlFieldAttribute), true);
                if (atts.Length > 0)
                {
                    sqlField = (SqlFieldAttribute)atts[0];
                    sqlField.PropertyName = pInfo.Name;
                    if (sqlField.PrimaryKeyField)
                    {
                        return sqlField;

                    }

                }

            }
            return sqlField;

        }


        public static SqlTableAttribute getSqlTableAttribute(System.Type objType)
        {
            object[] atts = objType.GetCustomAttributes(typeof(VMVServices.Services.Data.SqlTableAttribute), true);//Get all custom attributes for this object
            if (atts.Length == 0)
                return new SqlTableAttribute();
            else
                return (SqlTableAttribute)atts[0];
        }



        public static bool getBoolFromString(string str)
        {
            if (str == null) str = string.Empty;
            return (str.ToUpper() == "TRUE" || str.ToUpper() == "YES" || str == "1");
        }

        public static object getPropertyValue(object obj, string propertyName)
        {
            return obj.GetType().GetProperty(propertyName).GetValue(obj, null);
        }

        public static string covertToTitleCase(string strWords)
        {
            CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
            TextInfo textInfo = cultureInfo.TextInfo;

            return textInfo.ToTitleCase(strWords);
        }

        public static string getDollarToWord(decimal nAmount)
        {
            decimal nDollar;
            decimal nCent;
            string dollarToString = string.Empty;

            nDollar = (int)nAmount;
            nCent = (System.Math.Abs(nAmount) * 100) % 100;

            //dollarToString = getNumberToWord((int)nDollar) + " pula";

            dollarToString = Num2Words((int)nDollar) + " pula ";

            //if (System.Math.Abs(nDollar) != 1)
            //{
            //    dollarToString = dollarToString; 
            //}

            //dollarToString += " and" + getNumberToWord((int)nCent) + " thebe";
            dollarToString += " and " + Num2Words((int)nCent) + " thebe";

            //if (System.Math.Abs(nCent) != 1)
            //{
            //    dollarToString = dollarToString;
            //}

            return dollarToString;
        }


        static string SINGLES = "One,Two,Three,Four,Five," + "Six,Seven,Eight,Nine";
        static string DECADES = "Ten,Eleven,Twelve,Thirteen,Fourteen," + "Fifteen,Sixteen,Seventeen,Eighteen,Nineteen";
        static string DECTYS = "Twenty,Thirty,Forty,Fifty," + "Sixty,Seventy,Eighty,Ninety";
        static string TIONS = ",Thousand,Million,Billion";

        public static string Num2Words(int iNum)
        {
            int i = iNum;
            bool b = false;
            string s = "";
            if (i < 0)
            {
                b = true;
                i = i * -1;
            }
            if (i == 0)
            {
                s = "Zero";
            }
            else if (i <= 2147483647)
            {
                string[] a = TIONS.Split(',');
                for (int j = 0; j <= 3; j++)
                {
                    int iii = i % 1000;
                    i = i / 1000;
                    if (iii > 0) s = nnn2words(iii) + " " + a[j] + " " + s;
                }
            }
            else
            {
                s = "out of range value";
            }
            if (b) s = "negative " + s;
            return s.Trim();
        }
        private static string nnn2words(int iNum)
        {
            string[] a = SINGLES.Split(',');
            string s = "";
            int i = iNum % 10;
            if (i > 0) s = a[i - 1];

            int ii = Convert.ToInt32(iNum % 100) / 10;
            if (ii == 1)
            {
                s = DECADES.Split(',')[i];
            }
            else if (((ii > 1) & (ii < 10)))
            {
                s = DECTYS.Split(',')[ii - 2] + " " + s;
            }
            i = (iNum / 100) % 10;
            if (i > 0) s = a[i - 1] + " Hundred " + s;
            return s;
        }
        public static string getNumberToWord(int currencyValue)
        {
            bool bNegative = false;
            bool bHundred = false;
            string NumberToWord = string.Empty;
            long nNumber;
            if (currencyValue < 0)
            {
                bNegative = true;
            }

            nNumber = long.Parse(System.Math.Abs(currencyValue).ToString());

            if (nNumber < 1000)
            {
                if ((nNumber / 100) > 0)
                {
                    NumberToWord = NumberToWord + getNumberToWord((int)((nNumber / 100))).ToString() + " hundred";
                    bHundred = true;

                }

                nNumber = nNumber - ((nNumber / 100) * 100);
                bool bNoFirstDigit = false;

                switch (nNumber / 10)
                {
                    case 0:
                        switch (nNumber % 10)
                        {
                            case 0:
                                if (bHundred != true)
                                {
                                    NumberToWord += " zero";
                                }
                                break;
                            case 1:
                                NumberToWord += " one";
                                break;
                            case 2:
                                NumberToWord += " two";
                                break;
                            case 3:
                                NumberToWord += " three";
                                break;
                            case 4:
                                NumberToWord += " four";
                                break;
                            case 5:
                                NumberToWord += " five";
                                break;
                            case 6:
                                NumberToWord += " six";
                                break;
                            case 7:
                                NumberToWord += " seven";
                                break;
                            case 8:
                                NumberToWord += " eight";
                                break;
                            case 9:
                                NumberToWord += " nine";
                                break;

                        }
                        bNoFirstDigit = true;
                        break;
                    case 1:
                        switch (nNumber % 10)
                        {
                            case 0:
                                NumberToWord += " ten";
                                break;
                            case 1:
                                NumberToWord += " eleven";
                                break;
                            case 2:
                                NumberToWord += " twelve";
                                break;
                            case 3:
                                NumberToWord += " thirteen";
                                break;
                            case 4:
                                NumberToWord += " fourteen";
                                break;
                            case 5:
                                NumberToWord += " fifteen";
                                break;
                            case 6:
                                NumberToWord += " sixteen";
                                break;
                            case 7:
                                NumberToWord += " seventeen";
                                break;
                            case 8:
                                NumberToWord += " eighteen";
                                break;
                            case 9:
                                NumberToWord += " nineteen";
                                break;

                        }
                        bNoFirstDigit = true;
                        break;
                    case 2:
                        NumberToWord += " twenty";
                        break;
                    case 3:
                        NumberToWord += " thirty";
                        break;
                    case 4:
                        NumberToWord += " forty";
                        break;
                    case 5:
                        NumberToWord += " fifty";
                        break;
                    case 6:
                        NumberToWord += " sixty";
                        break;
                    case 7:
                        NumberToWord += " seventy";
                        break;
                    case 8:
                        NumberToWord += " eighty";
                        break;
                    case 9:
                        NumberToWord += " ninety";
                        break;
                }
                if (bNoFirstDigit != true)
                {
                    if (nNumber % 10 != 0)
                    {
                        NumberToWord = NumberToWord + "-" + getNumberToWord((int)(nNumber % 10)).Substring(2);
                    }

                }


            }
            else
            {
                decimal nTemp;
                nTemp = 10 ^ 12;

                while (nTemp >= 1)
                {
                    if (nNumber >= nTemp)
                    {
                        NumberToWord = NumberToWord + getNumberToWord((int)(nNumber / nTemp));

                        switch ((int)(System.Math.Log((double)nTemp) / System.Math.Log((double)nTemp) + 0.5))
                        {
                            case 12:
                                NumberToWord += " trillion";
                                break;
                            case 9:
                                NumberToWord += " billion";
                                break;
                            case 6:
                                NumberToWord += " million";
                                break;
                            case 3:
                                NumberToWord += " thousand";
                                break;

                        }

                        nNumber = (long)(nNumber - ((nNumber / nTemp) * nTemp));

                    }

                    nTemp = nTemp / 1000;

                }
            }

            if (bNegative)
            {
                NumberToWord = " negative" + NumberToWord;
            }

            return NumberToWord;

        }

        public static object getDateFromString(string strDate, string dateFormat)
        {
            try
            {
                System.Globalization.DateTimeFormatInfo dtfi = new System.Globalization.DateTimeFormatInfo();
                dtfi.ShortDatePattern = dateFormat;
                return DateTime.ParseExact(strDate, "d", dtfi);
            }
            catch { return System.DBNull.Value; }
        }
        public static object getDateFromString(string strDate, string fromFormat, string toFormat)
        {
            try
            {
                DateTime theDate = DateTime.ParseExact(strDate, fromFormat, null);
                return theDate.ToString(toFormat);
            }
            catch
            {
                return string.Empty;
            }
        }
        public static bool isNumeric(string str)
        {
            return false;
        }

        private static long _TransactionNo;
        public static long TransactionNumber
        {
            get { return _TransactionNo; }
            set { _TransactionNo = value; }
        }
        private static long _MemberId;
        public static long MemberId
        {
            get { return _MemberId; }
            set { _MemberId = value; }
        }
        private static long _SchemeId;
        public static long SchemeId
        {
            get { return _SchemeId; }
            set { _SchemeId = value; }
        }
        private static long _MasterRiskId;
        public static long MasterRiskId
        {
            get { return _MasterRiskId; }
            set { _MasterRiskId = value; }
        }
        private static string _PolicyNumber;
        public static string PolicyNumber
        {
            get { return _PolicyNumber; }
            set { _PolicyNumber = value; }
        }
        private static long _CompanyId;
        public static long CompanyId
        {
            get { return _CompanyId; }
            set { _CompanyId = value; }
        }
        private static string _LastName;
        public static string LastName
        {
            get { return _LastName; }
            set { _LastName = value; }
        }
        public static DbType SystemTypeToDbType(Type typ)
        {
            if (typ == typeof(bool))
                return DbType.Boolean;
            else if (typ == typeof(DateTime))
                return DbType.DateTime;
            else if (typ == typeof(Int16))
                return DbType.Int64;
            else if (typ == typeof(Int32))
                return DbType.Int32;
            else if (typ == typeof(Int64))
                return DbType.Int64;
            else if (typ == typeof(double))
                return DbType.Double;
            else if (typ == typeof(Single))
                return DbType.Single;
            else if (typ == typeof(Byte[]))
                return DbType.Binary;
            else if (typ == typeof(Byte))
                return DbType.Byte;
            else
                return DbType.String;
        }

        public static string getMonthText(int month)
        {
            switch (month)
            {
                case 1: return "Jan";
                case 2: return "Feb";
                case 3: return "Mar";
                case 4: return "Apr";
                case 5: return "May";
                case 6: return "Jun";
                case 7: return "Jul";
                case 8: return "Aug";
                case 9: return "Sep";
                case 10: return "Oct";
                case 11: return "Nov";
                case 12: return "Dec";
                default: return string.Empty;
            }
        }


        public static int GetRandomNumber(int start, int end, int previousNumber)
        {
            byte[] randomBytes = new byte[] { new byte() };
            int randomNumber = 0;

            //check to see if the range for the randon numbers is greater 1, if not then return the start number
            if ((end - start) <= 1) return start;

            if (start == 0)
            {
                while (previousNumber == randomNumber)
                {
                    System.Security.Cryptography.RandomNumberGenerator.Create().GetBytes(randomBytes);
                    randomNumber = (int)randomBytes[0];
                    randomNumber = (randomNumber % end);
                }
            }
            else
            {
                while (previousNumber == randomNumber)
                {
                    System.Security.Cryptography.RandomNumberGenerator.Create().GetNonZeroBytes(randomBytes);
                    randomNumber = (int)randomBytes[0];
                    randomNumber = (randomNumber % end) + 1;
                }
            }
            return randomNumber;
        }
    }
}
