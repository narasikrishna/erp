using System;
using System.Configuration;
using System.Collections;
using System.Xml;

namespace VMVServices.Services.Configuration.ConfigurationSectionHandlers
{
	/// <summary>
	/// Summary description for ServerConnectionStringSectionHandler.
	/// </summary>
	public class ServerConnectionStringSectionHandler : IConfigurationSectionHandler
	{
		public object Create(object parent, object configContext, XmlNode section)
		{
			ServerConnectionStringCollection _serverConnectionStrings = new ServerConnectionStringCollection();
			
			if (section.Name != "serverConnectionStringConfigSection") throw new ConfigurationException();

			foreach (XmlElement node in section.ChildNodes)
			{
				if (node.Name != "server") throw new ConfigurationException();

				string name = node.GetAttribute("name");
				string url = node.GetAttribute("url");
				string connectionString = node.GetAttribute("connectionString");

				_serverConnectionStrings.Add(new ServerConnectionString(name, url, connectionString));
			}

			return _serverConnectionStrings;
		}
	}
	public class ServerConnectionString
	{
		public readonly string Name = string.Empty;
		public readonly string Url = string.Empty;
		public readonly string ConnectionString = string.Empty;

		public ServerConnectionString()
		{}
		public ServerConnectionString(string name, string url, string connectionString)
		{
			this.Name = name;
			this.Url = url;
			this.ConnectionString = connectionString;
		}
	}

	public class ServerConnectionStringCollection : CollectionBase
	{
		public virtual ServerConnectionString this[int Index] { get { return (ServerConnectionString)this.List[Index]; } }
		public virtual ServerConnectionString this[string Name] 
		{ 
			get 
			{ 
				foreach (ServerConnectionString serverConnectionString in this.List)
					if (serverConnectionString.Name == Name)
						return serverConnectionString;
				return new ServerConnectionString(string.Empty, string.Empty, string.Empty);
			}
		}

		public int Add(ServerConnectionString item) { return this.List.Add(item); }
	}

}
