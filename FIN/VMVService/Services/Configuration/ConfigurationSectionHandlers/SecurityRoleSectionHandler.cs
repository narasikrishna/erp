using System;
using System.Configuration;
using System.Collections;
using System.Xml;

namespace VMVServices.Services.Configuration.ConfigurationSectionHandlers
{
	/// <summary>
	/// Summary description for RoleSecuritySectionHandler.
	/// </summary>
	public class SecurityRoleSectionHandler : IConfigurationSectionHandler
	{
		public object Create(object parent, object configContext, XmlNode section)
		{
			RoleCollection _roles = new RoleCollection();
			
			if (section.Name != "securityRolesConfigSection") throw new ConfigurationException();

			foreach (XmlElement node in section.ChildNodes)
			{
				if (node.Name != "role") throw new ConfigurationException();

				string name = node.GetAttribute("name");
				string defaultLandingPage = node.GetAttribute("defaultLandingPage");

				_roles.Add(new Role(name, defaultLandingPage));
			}

			return _roles;
		}
	}

	public class RoleCollection : System.Collections.CollectionBase
	{
		public virtual Role this[int Index] { get { return (Role)this.List[Index]; } }

		public int Add(Role item) { return this.List.Add(item); }
	}
	public class Role
	{
		public readonly string Name;
		public readonly string DefaultLandingPage;
		public Role(string name, string defaultLandingPage){Name = name;DefaultLandingPage=defaultLandingPage;}
	}
}
