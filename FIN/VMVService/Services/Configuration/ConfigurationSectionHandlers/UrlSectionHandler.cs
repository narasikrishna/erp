using System;
using System.Configuration;
using System.Collections;
using System.Xml;

namespace VMVServices.Services.Configuration.ConfigurationSectionHandlers
{
	/// <summary>
	/// Summary description for PageHeadingClassSettingsHandler.
	/// </summary>
	public class UrlSectionHandler : IConfigurationSectionHandler
	{
		public object Create(object parent, object configContext, XmlNode section)
		{
			UrlDetailCollection _urls = new UrlDetailCollection();
			
			if (section.Name != "urlConfigSection") throw new ConfigurationException();

			foreach (XmlElement node in section.ChildNodes)
			{
				if (node.Name != "urlDetail") throw new ConfigurationException();

				string url = node.GetAttribute("url");
				string name = node.GetAttribute("name");
				string headerClass = node.GetAttribute("headerClass");
				string selectedMenu = node.GetAttribute("selectedMenu");
				string templatePath = node.GetAttribute("templatePath");

				if (headerClass == null) headerClass = string.Empty;
				if (selectedMenu == null) selectedMenu = string.Empty;
				if (templatePath == null) templatePath = string.Empty;
				
				_urls.Add(new UrlDetail(name, url, headerClass, selectedMenu, templatePath));
			}

			return _urls;
		}
	}
	public class UrlDetail
	{
		public readonly string Url = string.Empty;
		public readonly string Name = string.Empty;
		public readonly string HeaderClass = string.Empty;
		public readonly string SelectedMenu = string.Empty;
		public readonly string TemplatePath = string.Empty;

		public UrlDetail(){}
		public UrlDetail(string name, string url, string headerClass, string selectedMenu, string templatePath)
		{
			Url = url;
			Name = name;
			HeaderClass = headerClass;
			SelectedMenu = selectedMenu;
			TemplatePath = templatePath;
		}
	}
	public class UrlDetailCollection : CollectionBase
	{
		public virtual UrlDetail this[int Index] { get { return (UrlDetail)this.List[Index]; } }
		public virtual UrlDetail this[string theUrl] { 
			get 
			{ 
				foreach (UrlDetail url in this.List)
					if (url.Url.ToUpper() == theUrl.ToUpper())
						return url;
				return new UrlDetail();
			}
		}
		public virtual UrlDetail this[string item, bool isName]{
			get
			{
				if (isName)
				{
					foreach (UrlDetail url in this.List)
						if (url.Name == item)
							return url;
					return new UrlDetail();
				}
				else
					return this[item];

			}
		}

		public int Add(UrlDetail item) { return this.List.Add(item); }
	}

}
