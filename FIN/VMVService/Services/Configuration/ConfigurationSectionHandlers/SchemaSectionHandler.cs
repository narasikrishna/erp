﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Collections;
using System.Xml;

namespace VMVServices.Services.Configuration.ConfigurationSectionHandlers
{
    public class SchemaSectionHandler : IConfigurationSectionHandler
    {
        public object Create(object parent, object configContext, XmlNode section)
        {
            SchemaDetailCollection _sch = new SchemaDetailCollection();

            if (section.Name != "SchemaData") throw new ConfigurationException();

            foreach (XmlElement node in section.ChildNodes)
            {
                if (node.Name != "SchemaDetail") throw new ConfigurationException();
            
                string name = node.GetAttribute("name");               
                string description = node.GetAttribute("description");              
                _sch.Add(new SchemaDetail(name,description));
            }

            return _sch;
        }
    }
    public class SchemaDetail
    {       
        public readonly string Name = string.Empty;
        public readonly string Description = string.Empty;

        public SchemaDetail() { }
        public SchemaDetail(string name, string description)
        {
            Name = name;
            Description = description;            
        }
    }
    public class SchemaDetailCollection : CollectionBase
    {
        public virtual SchemaDetail this[int Index] { get { return (SchemaDetail)this.List[Index]; } }
        
        public virtual SchemaDetail this[string SchName]
        {
            get
            {
                foreach (SchemaDetail sch in this.List)
                    if (sch.Name.ToUpper() == SchName.ToUpper())
                        return sch;
                return new SchemaDetail();
            }
        }
        public virtual SchemaDetail this[string desc, bool isDesc]
        {
            get
            {
                if (isDesc)
                {
                    foreach (SchemaDetail sch in this.List)
                        if (sch.Description == desc)
                            return sch;
                    return new SchemaDetail();
                }
                else
                    return this[desc];

            }
        }
        public int Add(SchemaDetail sch) { return this.List.Add(sch); }
    }

}
