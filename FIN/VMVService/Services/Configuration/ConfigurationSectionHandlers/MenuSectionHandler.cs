using System;
using System.Configuration;
using System.Collections;
using System.Xml;

namespace VMVServices.Services.Configuration.ConfigurationSectionHandlers
{
	/// <summary>
	/// Summary description for MenuSectionHandler.
	/// </summary>
	public class MenuSectionHandler : IConfigurationSectionHandler
	{
		public object Create(object parent, object configContext, XmlNode section)
		{
			MenuCollection _menus = new MenuCollection();
			string role = string.Empty;
			string menuText = string.Empty;
			string menuLinkUrl = string.Empty;
			int menuTypeInstruction = 0;
			
			if (section.Name != "menusConfigSection") throw new ConfigurationException();

            foreach (XmlElement node in section.ChildNodes)
			{
				switch(node.Name)
				{
					case "role":
						role = node.GetAttribute("name");
						foreach (XmlElement rolenode in node.ChildNodes)
						{
							if (rolenode.Name != "menu") throw new ConfigurationException();

							menuText = rolenode.GetAttribute("text");
							menuLinkUrl = rolenode.GetAttribute("linkUrl");
							menuTypeInstruction = int.Parse(rolenode.GetAttribute("typeInstruction"));

							_menus.Add(new Menu(menuText, menuLinkUrl, menuTypeInstruction, role));

						}
						break;
					case "menu":
						menuText = node.GetAttribute("text");
						menuLinkUrl = node.GetAttribute("linkUrl");
						menuTypeInstruction = int.Parse(node.GetAttribute("typeInstruction"));

						_menus.Add(new Menu(menuText, menuLinkUrl, menuTypeInstruction, ""));
						break;
					default:throw new ConfigurationException();
				}
			}

			return _menus;
		}
	}

	public class Menu
	{
		public readonly string Text;
		public readonly string LinkUrl;
		public readonly int TypeInstruction;
		public readonly string Role;

		public Menu(string text, string linkUrl, int typeInstruction, string role)
		{
			Text = text;
			LinkUrl = linkUrl;
			TypeInstruction = typeInstruction;
			Role = role;
		}

	}
	public class MenuCollection : CollectionBase
	{ 
		public virtual Menu this[int Index] { get { return (Menu)this.List[Index]; } }

		public int Add(Menu item) { return this.List.Add(item); }
	}
}
