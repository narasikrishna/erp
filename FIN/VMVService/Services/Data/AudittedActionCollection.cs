using System;

namespace VMVServices.Services.Data
{
	/// <summary>
	/// Summary description for AudittedActionCollection.
	/// </summary>
	public class AudittedActionCollection : System.Collections.CollectionBase
	{
		public AudittedActionCollection(){}
		public AudittedActionCollection(AudittedActionCollection AACollection){this.AddRange(AACollection);}
		public AudittedActionCollection(AudittedAction[] AAArray){this.AddRange(AAArray);}
		public AudittedActionCollection(AudittedAction AA){this.Add(AA);}

		public int Add(AudittedAction Item)
		{ return this.List.Add(Item); }

		public void AddRange(AudittedActionCollection AACollection)
		{
			foreach (AudittedAction AA in AACollection) this.List.Add(AA);
		}

		public void AddRange(AudittedAction[] AAArray)
		{
			foreach (AudittedAction AA in AAArray) this.List.Add(AA);
		}
	}
}
