using System;

namespace VMVServices.Services.Data
{
	/// <summary>
	/// Summary description for FieldParameter.
	/// </summary>
	public class FieldParameter
	{
		public FieldParameter(){}

		string _fieldAssignmentList;
		public string FieldAssignmentList { get{return _fieldAssignmentList;} }

		string _insertFieldList;
		public string InsertFieldList { get{return _insertFieldList;} }

		string _insertParamFieldList;
		public string InsertParamFieldList { get{return _insertParamFieldList;} }

		IDataParameterCollection _params = new IDataParameterCollection();
		public IDataParameterCollection Params { get{return _params;} set{_params = value;} }

		public void AddFieldAssignment(string field, string assignmentOperator, string paramPrefix)
		{
			if (_fieldAssignmentList == null || _fieldAssignmentList == string.Empty)
				_fieldAssignmentList = string.Concat(field, assignmentOperator, paramPrefix, field);
			else
				_fieldAssignmentList = string.Concat(_fieldAssignmentList, ", ", field, assignmentOperator, paramPrefix, field);
		}

		public void AddInsertFieldList(string field)
		{
			if (_insertFieldList == null || _insertFieldList == string.Empty)
				_insertFieldList = field;
			else
				_insertFieldList = string.Concat(_insertFieldList, ", ", field);
		}

		public void AddInsertParamFieldList(string field, string paramPrefix)
		{
			if (_insertParamFieldList == null || _insertParamFieldList == string.Empty)
				_insertParamFieldList = string.Concat(paramPrefix, field);
			else
				_insertParamFieldList = string.Concat(_insertParamFieldList, ", ", paramPrefix, field);
		}
	}
}
