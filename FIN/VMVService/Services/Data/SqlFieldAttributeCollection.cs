using System;

namespace VMVServices.Services.Data
{
	/// <summary>
	/// Summary description for SqlGeneratorAttributeCollection.
	/// </summary>
	public class SqlFieldAttributeCollection : System.Collections.CollectionBase
	{
		public SqlFieldAttributeCollection(){}

		public SqlFieldAttributeCollection(object[] objs)
		{
			this.AddRange(objs);
		}

		public virtual SqlFieldAttribute this[int Index] { get{ return (SqlFieldAttribute)this.List[Index]; } }

		public int Add(SqlFieldAttribute Item)
			{ return this.List.Add(Item); }

		public void AddRange(object[] objs)
		{
			foreach (object obj in objs)
				if (obj.GetType() == typeof(SqlFieldAttribute))
					this.Add((SqlFieldAttribute)obj);
		}

	}
}
