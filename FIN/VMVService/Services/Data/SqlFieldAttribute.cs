using System;
using System.Data;
using System.Reflection;

using VMVServices.Helpers;

namespace VMVServices.Services.Data
{
	public enum SqlFieldEncryptionTypeEnum {RC4, AES}
	
	/// <summary>
	/// Summary description for SqlFieldAttribute.
	/// </summary>
	public class SqlFieldAttribute : Attribute
	{
		public SqlFieldAttribute(){}

		string _PropertyName;
		public string PropertyName {get{ return _PropertyName; } set{ _PropertyName = value; } }

		string _FieldName;
		public string FieldName {get{ if (_FieldName==string.Empty||_FieldName==null) return _PropertyName; else return _FieldName; } set{ _FieldName = value; } }

        bool _NotAutoIncrement;
        public bool NotAutoIncrement { get { return _NotAutoIncrement; } set { _NotAutoIncrement = value; } }

		bool _Updateable = true;
		public bool Updateable {get{ return _Updateable; } set{ _Updateable = value; } }

		bool _PrimaryKeyField;
		public bool PrimaryKeyField {get{ return _PrimaryKeyField; } set{ _PrimaryKeyField = value; } }

		bool _IsAuditProperty;
		public bool IsAuditProperty {get{ return _IsAuditProperty; } set{ _IsAuditProperty = value; } }

		System.Data.DbType _DbType = DbType.Object;//default value of object indicates unknown therefore retreive the actual value from the property data type.
		public System.Data.DbType DbType {get{ return _DbType; } set{ _DbType = value; } }

		object _DefaultValue;
		public object DefaultValue {get{ return _DefaultValue; } set{ _DefaultValue = value; } }

		bool _IsReplicationSiteID = false;
		public bool IsReplicationSiteID { get{ return _IsReplicationSiteID; } set{ _IsReplicationSiteID = value; } }

		bool _IsEncrypted = false;
		public bool IsEncrypted { get{ return _IsEncrypted; } set{ _IsEncrypted = value; } }

		SqlFieldEncryptionTypeEnum _SqlFieldEncryptionType = SqlFieldEncryptionTypeEnum.RC4;
		public SqlFieldEncryptionTypeEnum SqlFieldEncryptionType { get{ return _SqlFieldEncryptionType; } set{ _SqlFieldEncryptionType = value; } }

		protected object getDataValue(DataRow dr)
		{
			if (dr[this.FieldName] == DBNull.Value)
				return this.DefaultValue;
			else
				return dr[this.FieldName];
		}

		public void setPropertyValue(ref object obj, DataRow dr)
		{
			PropertyInfo pInfo = obj.GetType().GetProperty(this.PropertyName);
			object propertyValue = getDataValue(dr);

			if (propertyValue != null)
				if (propertyValue.GetType() == typeof(DateTime) && pInfo.PropertyType == typeof(string))
				{
					//SPECIAL CASE FOR CONVERSION FROM DATE TO STRING IN SPECIFIC DATE FORMAT
					//propertyValue = ((DateTime)propertyValue).ToString(Settings.Instance.DefaultDateFormatString);
					propertyValue = ((DateTime)propertyValue).ToString(Settings.Instance.DefaultDateTimeFormatString);
				}

			if (pInfo.PropertyType == typeof(Int16))
			{
				if (propertyValue == null) propertyValue = 0;
				if (propertyValue.ToString() != string.Empty)
					propertyValue = Int16.Parse(propertyValue.ToString());
				else
					propertyValue = null;
			}
			else if (pInfo.PropertyType == typeof(Int32))
			{
				if (propertyValue == null) propertyValue = 0;
				if (propertyValue.ToString() != string.Empty)
					propertyValue = Convert.ToInt32(propertyValue.ToString());
				else
					propertyValue = null;
			}
			else if (pInfo.PropertyType == typeof(Int64))
			{
				if (propertyValue == null) propertyValue = 0;
                if (propertyValue.ToString() != string.Empty)
                {
                    propertyValue = Int64.Parse(propertyValue.ToString());
                }
                else
                    propertyValue = null;
			}
			else if (pInfo.PropertyType == typeof(double))
			{
				if (propertyValue == null) propertyValue = 0;
				if (propertyValue.ToString() != string.Empty)
					propertyValue = double.Parse(propertyValue.ToString());
				else
					propertyValue = null;
			}
			else if (pInfo.PropertyType == typeof(decimal))
			{
				if (propertyValue == null) propertyValue = 0;
				if (propertyValue.ToString() != string.Empty)
					propertyValue = (decimal)propertyValue;
				else
					propertyValue = null;
			}
            else if (pInfo.PropertyType == typeof(Single  ))
            {
                if (propertyValue == null) propertyValue = 0;
                if (propertyValue.ToString() != string.Empty)
                    propertyValue = Single.Parse(propertyValue.ToString());
                else
                    propertyValue = null;
            }

			else if(pInfo.PropertyType == typeof(bool))
			{
				if (propertyValue == null) propertyValue = false;
				if (propertyValue.ToString() != string.Empty) propertyValue = (bool)propertyValue;
			}
			else if(pInfo.PropertyType == typeof(string))
			{
				if (propertyValue == null) propertyValue = string.Empty;
				propertyValue = propertyValue.ToString();
			}
		
			pInfo.SetValue(obj, propertyValue, null);
		}

		public IDataParameter getParameterFromObj(ref object obj)
		{
			IDataParameter prm = DbEngine.CreateParameter();
			prm.ParameterName = string.Concat(DbEngine.Instance.ParameterPrefix, this.FieldName);
			prm.DbType = this.DbType;
			prm.Value = Utils.getPropertyValue(obj, this.PropertyName);

            if ((prm.DbType == DbType.Date || prm.DbType == DbType.DateTime) )
			{
                if (prm.Value != null && prm.Value.GetType() == typeof(string) && prm.Value.ToString().Length > 10)
                {
                    //prm.Value = Utils.getDateFromString((string)prm.Value, Settings.Instance.DefaultDateTimeFormatString);
                    prm.Value = Utils.getDateFromString((string)prm.Value, Settings.Instance.DefaultDateTimeFormatString);
                }
                else if (prm.Value != null && prm.Value.GetType() == typeof(string) && prm.Value.ToString().Length >= 10)
                {

                    prm.Value = Utils.getDateFromString((string)prm.Value, Settings.Instance.DefaultDateFormatString);
                }
                else
                {
                    prm.Value = DBNull.Value;
 
                }
			}
            else  if ((prm.DbType == DbType.Date || prm.DbType == DbType.DateTime) && prm.Value.GetType() == typeof(string))
            {
                //prm.Value = Utils.getDateFromString((string)prm.Value, Settings.Instance.DefaultDateTimeFormatString);
                prm.Value = Utils.getDateFromString((string)prm.Value, Settings.Instance.DefaultDateFormatString);
            }
			else if ((prm.DbType == DbType.Int16 || prm.DbType == DbType.Int32 || prm.DbType == DbType.Int64) && prm.Value.GetType() == typeof(string))
				if ((string)prm.Value == string.Empty || !Utils.isNumeric((string)prm.Value))
					prm.Value = DBNull.Value;
				else
					prm.Value = (int)prm.Value;
			else if (prm.DbType == DbType.Decimal && prm.Value.GetType() == typeof(string))
				if ((string)prm.Value == string.Empty || !Utils.isNumeric((string)prm.Value))
					prm.Value = DBNull.Value;
				else
					prm.Value = (decimal)prm.Value;
			else if (prm.DbType == DbType.Double && prm.Value.GetType() == typeof(string))
				if ((string)prm.Value == string.Empty || !Utils.isNumeric((string)prm.Value))
					prm.Value = DBNull.Value;
				else
					prm.Value = (double)prm.Value;

            //else if(prm.DbType==DbType.String && prm.Value .GetType()==typeof(string))
            //    if ((string)prm.Value == string.Empty)
            //        prm.Value = DBNull.Value;
            //    else
            //        prm.Value = (string)prm.Value;

			return prm;
		}
	}
}
