using System;


namespace VMVServices.Services.Data
{
    /// <summary>
    /// Summary description for DbConsts.
    /// </summary>
    public class DbConsts
    {
        public DbConsts() { }

        public const string InsurerPopupProperties = "toolbar=no,menubar=no,location=no,scrollbars=yes,resizable=no,status=no,width=600,height=350,left=300,Top=200";

        public const string ReportProperties = "toolbar=no,menubar=no,location=no,scrollbars=yes,resizable=yes,status=no,width=800,height=600,left=100,Top=100";
        public const string CompanyList = "Company List";
        public const string Company = "Company";
        public const string ChequeBad = "Bad Cheques";
        public const string ChequeBadList = "Bad Cheques List";
        public const string MemberList = "Member List";
        public const string RiskList = "Risk List";
        public const string SinglePremiumList = "Single Premium List";
        public const string CFGList = "CFG List";
        public const string CFG = "CFG";
        public const string ClassToSchemeList = "ClassToSchemeList";
        public const string ClassDetails = "Class Details";
        public const string GroupList = "Group List";


        public const string EXLLABELCAPTION = "LABEL CAPTION";
        public const string EXLFIELDNAME = "FIELD NAME";
        public const string EXLDATATYPE = "DATA TYPE";
        public const string EXLLENGTH = "DATA LENGTH";
        public const string EXLDEFAULTVALUE = "DEFAULT";

        public const string F1 = "F1";
        public const string F2 = "F2";
        public const string F3 = "F3";
        public const string F4 = "F4";
        public const string F5 = "F5";

        //Assessor
        public const string Assessor_ID = "assessor_id";
        public const string First_Name = "first_name";
        public const string Last_Name = "Last_Name";
        public const string Phone = "phone_number";
        public const string Fax_number = "fax_number";
        public const string Email_ID = "email_id";
        public const string Address = "address";

        //Ssm Code Master
        public const string CODE_MST_ID = "CODE_MST_ID";
        public const string CODE = "CODE";

        //Location Master
        public const string LOCATION_MASTER_ID = "LOCATION_MASTER_ID";
        public const string LOCATION_MASTER_NUMBER = "LOCATION_MASTER_NUMBER";
        public const string ENTITY_TYPE_ID = "ENTITY_TYPE_ID";
        public const string ENTITY_NAME = "ENTITY_NAME";

        //Location Structure Details
        public const string LOCATION_STRUCT_DTLS_ID = "LOCATION_STRUCT_DTLS_ID";
        //public const string LOCATION_STRUCTURE_ID = "LOCATION_STRUCTURE_ID";
        public const string PARENT_ID = "PARENT_ID";
        public const string LAST_LEVEL = "LAST_LEVEL";


        //Location Structure
        public const string LOCATION_STRUCTURE_ID = "LOCATION_STRUCTURE_ID";
        public const string LOCATION_STRUCTURE_NUMBER = "LOCATION_STRUCTURE_NUMBER";
        public const string START_DATE = "START_DATE";
        public const string END_DATE = "END_DATE";

        //A

        public const string REPORT_NAME = "REPORT_NAME";
        public const string MENU_DESCRIPTION = "MENU_DESCRIPTION";
        public const string ACTION_DATE = "ACTION_DATE";

        public const string PageTitle = "FIN";
        public const string MachineName = "MachineName";
        public const string SingleReceipts = "Manual Premium - Single Receipts";
        public const string ClaimRegister = "Claim Register";
        public const string RiskClassDetails = "Risk Class Details";
        public const string ChequePrint = "Cheque Printing";
        public const string ManualCheque = "Manual Cheque";
        public const string ChequeCancellation = "Cheque Cancellation";
        public const string ChequeExport = "Cheque Export";
        public const string ChequeRelease = "Release Cheque Number";
        public const string CPO = "CPO";

        public const string UsernameColumnName = "Username";
        public const string PasswordColumnName = "Password";
        public const string DescriptionColumnName = "Description";
        public const string RoleDescriptionColumnName = "RoleDescription";
        public const string SurnameColumnName = "Surname";
        public const string FirstnameColumnName = "Firstname";
        public const string SentColumnName = "Sent";
        public const string FailedSendAttemptsColumnName = "FailedSendAttempts";
        public const string SysDateCreatedColumnName = "SysDateCreated";
        public const string SysDateLastUpdatedColumnName = "SysDateLastUpdated";
        public const string FULLNAME = "FULLNAME";
        public const string PASSWORD = "PASSWORD";
        public const string EMAIL = "EMAIL";
        public const string Roles = "Roles";


        //Bank
        public const string Bank_Code = "Bank_Code";
        public const string BANK_NAME = "BANK_NAME";
        public const string BANK_ADDRESS = "BANK_ADDRESS";
        public const string BankKeyID = "BankKeyID";
        public const string Branch_ID = "branch_id";
        public const string Branch_Name = "branch_name";
        public const string Bank_Key_ID = "bank_key_id";
        public const string BANK_BRANCH = "BANK_BRANCH";
        public const string BANK_LOAN_CODE = "BANK_LOAN_CODE";


        //Table_Seq_Name
        public const string AON_Bank_MST_SEQ = "AON_Bank_MST_SEQ";
        public const string AON_Bank_MST = "AON_Bank_MST";

        //INSURER
        public const string INSURERID = "INSURER_ID";
        public const string INSURERNAME = "INSURER_NAME";
        public const string BRANCH_DETAIL = "branch_detail";
        public const string INSURERADDRESS = "INSURER_ADDRESS";
        public const string INSURERPHONE = "INSURER_PHONE";
        public const string INSURERFAX = "INSURER_FAX";
        public const string INSUREREMAIL = "INSURER_EMAIL";
        public const string STATUS = "STATUS";
        public const string RATING = "RATING";
        public const string CFGID = "CFG_ID";
        public const string BUILDDATE = "BUILD_DATE";
        public const string UNAME = "U_NAME";
        public const string MACHINENAME = "MACHINE_NAME";
        public const string INSURERKEYID = "INSURERKEYID";
        public const string SYSDATECREATED = "SYSDATECREATED";
        public const string SYSDATELASTUPDATED = "SYSDATELASTUPDATED";

        //TAX
        public const string VATCOUNTRYCODE = "VAT_COUNTRY_CODE";
        public const string VATCOUNTRY = "VAT_COUNTRY";
        public const string TAXRATE = "TAX_RATE";
        public const string TAXDIVISOR = "TAX_DIVISOR";
        public const string TAXBROKERAGE = "TAX_BROKERAGE";
        public const string TAXPREMIUM = "TAX_PREMIUM";
        public const string TAXPAYEE = "TAX_PAYEE";
        public const string TAXEXCESS = "TAX_EXCESS";
        public const string TAXFEES = "TAX_FEES";
        public const string TAXRATE1 = "TAX_RATE1";
        public const string TAXDIVISOR1 = "TAX_DIVISOR1";

        //Currency
        public const string CURRENCYCODE = "CURRENCY_CODE";
        public const string CURRENCYNAME = "CURRENCY_NAME";
        public const string COUNTRY = "COUNTRY";

        //Payee
        public const string PAYEEID = "PAYEE_ID";
        public const string PAYEENAME = "PAYEE_NAME";
        public const string PAYEECLASSID = "PAYEE_CLASS_ID";
        public const string PAYEEADDRESS = "PAYEE_ADDRESS";
        public const string PAYEEPHONE = "PAYEE_PHONE";
        public const string PAYEEFAX = "PAYEE_FAX";
        public const string PAYEEEMAIL = "PAYEE_EMAIL";
        public const string PARTY = "PARTY";
        public const string ADDRESSEE = "ADDRESSEE";
        public const string CLAIMACTIONSAUTOID = "CLAIMACTIONSAUTOID";
        public const string PAYEEKEYID = "PAYEEKEYID";
        public const string VISIBLE = "VISIBLE";

        //ClaimStatus
        public const string CLAIMSTATUSKEYID = "CLAIMSTATUSKEYID";
        public const string STATUSID = "STATUS_ID";

        //CFG         
        public const string CFGNAME = "CFG_NAME";
        public const string CFGPOINTS = "CFG_POINTS";
        public const string CFGPERCENT = "CFG_PERCENT";
        public const string CFGDROP = "CFG_DROP";
        public const string CFGMAXPERCENT = "CFG_MAX_PERCENT";
        public const string CFG_VALUE = "CFG_VALUE";

        //Incident 
        public const string INCIDENTKEYID = "INCIDENT_KEYID";
        public const string INCIDENTID = "INCIDENT_ID";
        public const string INCIDENTDESCRIPTION = "INCIDENT_DESCRIPTION";
        public const string ACCIDENTCODE = "ACCIDENTCODE";
        public const string ABGILCODE = "ABGILCODE";

        //Company
        public const string COMPANYID = "COMPANY_ID";
        public const string COMPANYNAME = "COMPANY_NAME";
        public const string COMPANYBUSINESS = "COMPANY_BUSINESS";
        public const string COMPANYADDRESS = "COMPANY_ADDRESS";
        public const string COMPANYPHONE = "COMPANY_PHONE";
        public const string COMPANYFAX = "COMPANY_FAX";
        public const string CONTACTPERSON = "CONTACT_PERSON";
        public const string DEPARTMENT = "DEPARTMENT";
        public const string CITY = "CITY";
        public const string COMPANYKEYID = "COMPANY_KEY_ID";

        public const string CFGKEYID = "CFGKEYID";

        public const string RISKID = "RISK_ID";
        public const string MEMBERID = "MEMBER_ID";
        public const string LABELCAPTION = "LABEL_CAPTION";
        public const string FIELDNAME = "FIELD_NAME";
        public const string DATATYPE = "DATA_TYPE";
        public const string NULLABLE = "NULLABLE";
        public const string LENGTH = "LENGTH";
        public const string DEFAULTVALUE = "DEFAULT_VALUE";
        public const string RISKDATA = "DETAILS";


        //GL Code
        public const string GL_CODE = "GL_CODE";
        public const string GL_DESCRIPTION = "GL_DESCRIPTION";
        public const string SCHEME_TYPE = "SCHEME_TYPE";

        //Area
        public const string AREAID = "AREA_ID";
        public const string AREALOCATION = "AREA_LOCATION";
        public const string AREACODE = "AREA_CODE";

        //Load Format
        public const string LOADNAME = "LOAD_NAME";
        public const string LOADDOCUMENT = "LOAD_DOCUMENT";
        public const string LOADFORMAT = "LOAD_FORMAT";
        public const string LOADDELIMITER = "LOAD_DELIMITER";
        public const string LOADFORMATID = "LOAD_FORMAT_ID";

        //Scheme
        public const string BRANCHID = "BRANCH_ID";
        public const string BRANCHNAME = "BRANCH_NAME";
        public const string BRANCHADDRESS = "BRANCH_ADDRESS";
        public const string BRANCHPHONE = "BRANCH_PHONE";
        public const string BRANCHFAX = "BRANCH_FAX";
        public const string COMMISSIONRATE = "COMMISSION_RATE";
        public const string VATONPREMIUM = "VAT_ON_PREMIUM";
        public const string GLVATCODE = "GL_VAT_CODE";
        public const string VATKEYID = "VAT_KEY_ID";
        public const string NUMBER_OF_PREMIUM = "NUMBER_OF_PREMIUM";

        //Class
        public const string memberid = "member_id";
        public const string TABLEID = "TABLE_ID";
        public const string TABLENAME = "TABLE_NAME";
        public const string PRIMARYCOLUMNS = "PRIMARY_COLUMNS";
        public const string PARENTTABLEID = "PARENT_TABLEID";

        public const string MASTER_RISK_ID = "MASTER_RISK_ID";
        public const string RISKNAME = "RISK_NAME";
        public const string RISKSTATE = "RISK_STATE";
        public const string RISKACTIVE = "RISK_ACTIVE";

        //RiskRule
        public const string RULEID = "RULE_ID";

        public const string RULEDEFTYPE = "RULE_DEF_TYPE";
        public const string PREMIUMVARIATION = "PREMIUM_VARIATION";
        public const string ASSETREF = "ASSET_REF";
        public const string UNPAIDPREMIUMS = "UNPAID_PREMIUMS";
        public const string ASSETOWNER = "ASSET_OWNER";
        public const string CLAIMLIMIT = "CLAIM_LIMIT";
        public const string CLAIMINSURED = "CLAIM_INSURED";

        public const string STARTCOL = "START_COL";
        public const string FIELDPOSITION = "FIELD_POSITION";
        public const string COLLENGTH = "COL_LENGTH";
        public const string EXCELLETTER = "EXCEL_LETTER";

        public const string StartDate = "Start_Date";
        public const string EndDate = "End_Date";
        public const string ClaimStatus = "Claim_Status";
        public const string CutOffDate = "CutOff_Date";
        public const string CLAIM_OWN_DETAILS = "CLAIM_OWN_DETAILS";
        public const string CLAIM_OTHER_DETAILS = "CLAIM_OTHER_DETAILS";
        
            

        //Members

        public const string FIRSTNAME = "FIRST_NAME";
        public const string LASTNAME = "LAST_NAME";
        public const string BIRTHDATE = "BIRTH_DATE";
        public const string EMPNUMBER = "EMP_NUMBER";
        public const string MARITALSTATUS = "MARITAL_STATUS";
        public const string SEX = "SEX";
        public const string IDNUMBER = "ID_NUMBER";
        public const string OMANGNUMBER = "OMANG_NUMBER";
        public const string POSTALADDRESS = "POSTAL_ADDRESS";
        public const string PHONEWORK = "PHONE_WORK";
        public const string PHONEHOME = "PHONE_HOME";
        public const string PHONEMOBILE = "PHONE_MOBILE";
        public const string BULKHNDL = "BULK_HNDL";
        public const string NATIONALITY = "NATIONALITY";
        public const string PHYSICALADDRESS = "PHYSICAL_ADDRESS";
        public const string SumInsured = "Sum_Insured";

        //Premiumheader
        public const string PH_ID = "PH_ID";
        public const string PH_RECEIPT_REF = "PH_RECEIPT_REF";
        public const string PH_AMOUNT = "PH_AMOUNT";
        public const string PH_FILE_REF = "PH_FILE_REF";
        public const string PH_MANUAL_REF = "PH_MANUAL_REF";
        public const string PH_SUB_TOTAL = "PH_SUB_TOTAL";
        public const string PH_REN_NEW = "PH_REN_NEW";
        public const string PH_PREMIUM_TYPE = "PH_PREMIUM_TYPE";
        public const string COMMISSION_RATE = "COMMISSION_RATE";


        //Premium Contribution
        public const string CONTRIBUTION_ID = "CONTRIBUTION_ID";
        public const string POLICY_ID = "POLICY_ID";
        public const string P_PERIOD = "P_PERIOD";
        public const string P_YEAR = "P_YEAR";
        public const string P_DATE = "P_DATE";
        public const string P_AMOUNT = "P_AMOUNT";
        public const string INSURER_ID = "INSURER_ID";
        public const string LOAD_METHOD = "LOAD_METHOD";
        public const string CHEQUE_REF = "CHEQUE_REF";
        public const string AMOUNT_LESS_TAX = "AMOUNT_LESS_TAX";
        public const string AMOUNT_TAX = "AMOUNT_TAX";
        public const string TRANSACTION_NUMBER = "TRANSACTION_NUMBER";

        //Cancel Cheque
        public const string CHEQUE_NUMBER = "CHEQUE_NUMBER";
        public const string CHEQUE_AMOUNT = "CHEQUE_AMOUNT";
        public const string DATE_CHEQUE_PAID = "DATE_CHEQUE_PAID";
        public const string Payment = "Payment";
        public const string Status = "Status";


        //Lookup
        public const string VALUE_KEY_ID = "VALUE_KEY_ID";
        public const string TYPE_KEY_ID = "TYPE_KEY_ID";
        public const string VALUE_NAME = "VALUE_NAME";
        public const string TYPE_NAME = "TYPE_NAME";

        //Claim Register
        public const string CLAIMNUMBER = "CLAIM_NUMBER";
        public const string POLICYID = "POLICY_ID";
        public const string TPOD = "TPOD";
        public const string CLAIMDESCRIPTION = "CLAIM_DESCRIPTION";
        public const string CLAIMREPORTDATE = "CLAIM_REPORT_DATE";
        public const string CLAIMALLDOCSDATE = "CLAIM_ALL_DOCS_DATE";
        public const string CLAIMDATELOSS = "CLAIM_DATE_LOSS";
        public const string CLAIMSTATUS = "CLAIM_STATUS";
        public const string CLAIMCOVEREVIDENCE = "CLAIM_COVER_EVIDENCE";
        public const string INSURERIDATCLAIM = "INSURER_ID_AT_CLAIM";
        public const string CLAIMEXCESS = "CLAIM_EXCESS";
        public const string CLAIMCLASS = "CLAIM_CLASS";
        public const string CLAIMCATEGORY = "CLAIM_CATEGORY";
        public const string CLAIMLOSSDETAILS = "CLAIM_LOSS_DETAILS";
        public const string CLAIMREFBOOK = "CLAIM_REF_BOOK";
        public const string CLAIMREFFILE = "CLAIM_REF_FILE";
        public const string CLAIMOWNDETAILS = "CLAIM_OWN_DETAILS";
        public const string CLAIMOTHERDETAILS = "CLAIM_OTHER_DETAILS";
        public const string CLAIMDATESTATUSCHANGE = "CLAIM_DATE_STATUS_CHANGE";
        public const string CLAIMDATEREGISTERED = "CLAIM_DATE_REGISTERED";
        public const string COVERSTARTDATEATCLAIM = "COVER_START_DATE_AT_CLAIM";
        public const string COVERENDDATEATCLAIM = "COVER_END_DATE_AT_CLAIM";
        public const string POLICYVERSIONATCLAIM = "POLICY_VERSION_AT_CLAIM";
        public const string DRIVERNAME = "DRIVER_NAME";
        public const string DRIVERBIRTHDATE = "DRIVER_BIRTH_DATE";
        public const string LICENCENUMBER = "LICENCE_NUMBER";
        public const string LICENCEISSUEDATE = "LICENCE_ISSUE_DATE";
        public const string TOTALLOSS = "TOTAL_LOSS";
        public const string CLAIMOWNDAMAGEFINAL = "CLAIM_OWN_DAMAGE_FINAL";
        public const string CLAIMTPOWNDAMAGEFINAL = "CLAIM_TP_OWN_DAMAGE_FINAL";
        public const string INITIALESTIMATEOD = "INITIAL_ESTIMATE_OD";
        public const string INITIALESTIMATETP = "INITIAL_ESTIMATE_TP";
        public const string CURRENTESTIMATEOD = "CURRENT_ESTIMATE_OD";
        public const string CURRENTESTIMATETP = "CURRENT_ESTIMATE_TP";

        //payee class
        public const string PAYEE_CLASS_ID = "PAYEE_CLASS_ID";
        public const string PAYEE_CLASS_NAME = "PAYEE_CLASS_NAME";
        public const string PAYEE_DESCRIPTION = "PAYEE_DESCRIPTION";


        //Group
        public const string GROUP_ID = "GROUP_ID";
        public const string GROUP_NAME = "GROUP_NAME";
        public const string GROUP_DESCRIPTION = "GROUP_DESCRIPTION";

        //User
        public const string U_ID = "U_ID";
        public const string U_NAME = "U_NAME";
        public const string U_FIRST_NAME = "U_FIRST_NAME";
        public const string U_LAST_NAME = "U_LAST_NAME";
        public const string U_PASSWD = "U_PASSWD";
        public const string LOCK_ACCOUNT = "LOCK_ACCOUNT";
        public const string LOGIN_STATE = "LOGIN_STATE";

        //Schema
        public const string SCHEMA_KEY_ID = "SCHEMA_KEY_ID";
        public const string SCHEMA_NAME = "SCHEMA_NAME";
        public const string SCHEMA_DESCRIPTION = "SCHEMA_DESCRIPTION";
        public const string IS_PMT_VERI = "IS_PMT_VERI";

        //Claim Cost
        public const string CLAIM_COST_ID = "CLAIM_COST_ID";
        public const string EDIT_CLAIM_COST_ID = "EDIT_CLAIM_COST_ID";
        public const string EDIT_TRANSACTION_NUMBER = "EDIT_TRANSACTION_NUMBER";
        
        public const string TAXABLE = "TAXABLE";
        public const string PAYMENT_AMOUNT = "PAYMENT_AMOUNT";
        public const string PAYMENT_TYPE = "PAYMENT_TYPE";

        //Table Constant
        public const string CLAIM_COST = "CLAIM_COST";
        public const string CLAIM_APPROVED_LIST = "CLAIM_APPROVED_LIST";
        public const string SUM_AMOUNT = "SUM_AMOUNT";

        //reports
        public const string CPONEW = "CPO NEW";
        public const string CCR = "Claim Cost & Recoveries";
        public const string Recoveries = "Recoveries";
        public const string InstructiontoAuthoriseorNOTAuthorise = "Instruction to Authorise or NOT Authorise";
        public const string ClaimPaidOutstanding = "Claim Paid Outstanding";
        public const string CHEQUE_BATCH_NUMBER = "CHEQUE_BATCH_NUMBER";
        public const string SettlementPostList = "Settlement Post List";
        public const string SettlementEditList = "Settlement Edit List";
        public const string BankBatchList = "Bank Batch List";
        public const string ClaimRegisteredReport = "Claim Registered Report";
        public const string MRI = "MRI Contributions";
        public const string RenewalLetter = "Renewal Letter";
        public const string NUM_PAYEE = "NUM_PAYEE";


        public const string PremiumReceiptsRegister = "Premium Receipts Register";
        public const string PremiumPostList = "Premium Post List";
        public const string Month = "Month";
        public const string Year = "Year";
        //public const string ClaimRegisteredReport = "Claim Registered";
        public const string ChequeReconcilliation = "Cheque Reconcilliation";


        public const string COMPUTED_PREMIUM = "COMPUTED_PREMIUM";
        public const string COVER_STATUS = "COVER_STATUS";
        public const string COVER_STATUS_DATE = "COVER_STATUS_DATE";
        public const string LOAN_AMOUNT = "LOAN_AMOUNT";
        public const string LOAN_DATE = "LOAN_DATE";
        public const string LOAN_RATE = "LOAN_RATE";
        public const string LOAN_TERM = "LOAN_TERM";
        public const string MARKET_VALUE = "MARKET_VALUE";
        public const string VEHICLE_MAKE = "VEHICLE_MAKE";
        public const string VEHICLE_MODEL = "VEHICLE_MODEL";
        public const string VEHICLE_YEAR = "VEHICLE_YEAR";
        public const string REGISTRATION_NUMBER = "REGISTRATION_NUMBER";
        public const string NEXT_CFG = "NEXT_CFG";

        public const string PolicyRenewalNotificationList = "Policy Renewal Notification List";
        public const string AON_LOOKUP_VALUE = "AON.AON_LOOKUP_VALUE";
        public const string COST_DESCRIPTION = "COST_DESCRIPTION";

        public const string MANUAL_CLAIM_COST_CLONE = "MANUAL_CLAIM_COST_CLONE";
        public const string MANUAL_CLAIM_COST = "MANUAL_CLAIM_COST";
        public const string MANUAL_CLAIM_COST_AMOUNT = "MANUAL_CLAIM_COST_AMOUNT";
        public const string MANUAL_CLAIM_COST_CHEQUENUMBER = "MANUAL_CLAIM_COST_CHEQUENUMBER";

        public const string RENEWAL_NOTIFICATION = "RENEWAL_NOTIFICATION";

        //Assessor
        public const string ASSESSOR_ID = "ASSESSOR_ID";
        public const string ASSESSOR_NAME = "ASSESSOR_NAME";
        
        //Grid command name
        public const string POLICY = "POLICY";

        public const string RISKCLASSDETAIL = "RISKCLASSDETAIL";

        public const string LASTUSEDCHEQUE = "LASTUSEDCHEQUE";
        
        public const string CURRENTCHEQUE = "CURRENTCHEQUE";

        public const string MEMBERSBULKTEMPLATE = "MEMBERSBULKTEMPLATE";
        public const string RISKCLASSBULKLOAD = "RISKCLASSBULKLOAD";
        public const string POLICYBULKLOAD = "POLICYBULKLOAD";
        public const string EXCELINPUTFILE = "EXCELINPUTFILE";

        public const string ACTIVE_FLAG = "ACTIVE_FLAG";

        public const string Additional_Excess = "Additional_Excess";
        public const string Vehicle_Location = "Vehicle_Location";
        public const string ASSESSOR_INSTRUCTION_ID = "ASSESSOR_INSTRUCTION_ID";
        public const string ISSUED_DATE = "ISSUED_DATE";
        public const string ASSESSOR_APPROVAL_ID = "ASSESSOR_APPROVAL_ID";

        public const string Tp_Owner_Flag = "Tp_Owner_Flag";

        public const string ClaimForEdit = "ClaimForEdit";
        public const string PaymentObject = "PaymentObject";
        public const string ChequeBatchObject = "ChequeBatchObject";
        public const string ChequePrintObject = "ChequePrintObject";
        
        

    }
}
