using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Reflection;
using System.Data.OracleClient;
using VMVServices.Helpers;
using VMVServices.Services;
using System.Globalization;


using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Collections;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;


namespace VMVServices.Services.Data
{
    /// <summary>
    /// Summary description for DbEngine.
    /// </summary>
    public abstract class DbEngine
    {
        //Constructor
        public DbEngine() { }
        

        //Instance Property (Singleton Pattern, only one instance running)
        static DbEngine _Instance;
        public static DbEngine Instance
        {
            get
            {
                if (_Instance == null) _Instance = (DbEngine)Activator.CreateInstance(Type.GetType(Settings.Instance.DbEngineType));
                return _Instance;
            }
        }
        static string _SchemaCon;
        public static string SchemaCon
        {
            get
            {
                // if (_SchemaCon == null) _SchemaCon = (DbEngine)Activator.CreateInstance(Type.GetType(Settings.SchemaCon.DbEngineType));
                return _SchemaCon;
            }
            set
            {
                _SchemaCon = value;
            }
        }
        public object RunAction(IDbCommand cmd, IDbTransaction tran)
        {
            try
            {
                return ExecSqlScalar(cmd, tran);
            }
            catch (Exception e)
            {
                if (e is System.Data.SqlClient.SqlException)
                {
                    //'ex.number = 2601 = microsoft sql server unique index violation
                    //'ex.number = 2627 = microsoft sql server unique constraint violation
                    //
                    //'ex.number = 547 = DELETE statement conflicted with COLUMN REFERENCE constraint '[CONSTRAINT]'. The conflict occurred in database '[DATABASE]', table '[TABLE]', column '[COLUMN]'.
                    //'                   This error indicates a foreign key delete violation, that is a record is being deleted from a table 
                    //'                   when that same record is referenced by another record from another table. The user must first delete
                    //'                   the second record, thus destroying the reference and allowing the delete of the first record.
                    if (((System.Data.SqlClient.SqlException)e).Number == 2601 ||
                        ((System.Data.SqlClient.SqlException)e).Number == 2627)
                        //Throw RecordAlreadyExistsException


                        throw new VMVServices.Services.ErrorManagement.RecordAlreadyExistsException(string.Empty);

                       //throw new iThink.Services.ErrorManagement.RecordAlreadyExistsException(e.Message);
                    else if (((System.Data.SqlClient.SqlException)e).Number == 547)
                        throw new VMVServices.Services.ErrorManagement.DependantRecordDeletionException(string.Empty);
                    else
                        throw e;
                }
                else if (e is System.Data.OracleClient.OracleException)
                {
                    if (((System.Data.OracleClient.OracleException)e).ErrorCode == 2601 ||
                        ((System.Data.OracleClient.OracleException)e).ErrorCode == 2627)
                        throw new VMVServices.Services.ErrorManagement.RecordAlreadyExistsException(string.Empty);
                    else if (((System.Data.OracleClient.OracleException)e).ErrorCode == 547)
                        throw new VMVServices.Services.ErrorManagement.DependantRecordDeletionException(string.Empty);
                    else
                        throw e;
                }
                else if (e is System.Data.OleDb.OleDbException)
                {
                    if (((System.Data.OleDb.OleDbException)e).ErrorCode == -2147467259)
                    {
                        //oledb unique index violation
                        //Throw RecordAlreadyExistsException
                        throw e;
                    }
                    else
                    { throw e; }
                }
                else
                {
                    throw e;
                }
            }
        }
        public long RunAudittedActionForInsert(AudittedAction AA, IDbTransaction tran)
        {
            string sql = string.Empty;
            //AA.ActionCmd.CommandText += ";SELECT SCOPE_IDENTITY() AS [NEWID]";
            //AA.ActionCmd.CommandText += ";SELECT max(" + TableIDColumn + ") from " + TableName ;
            //long newId = int.Parse(RunAction(AA.ActionCmd, tran).ToString());
            RunAction(AA.ActionCmd, tran);

            //if (Settings.Instance.ProgramDoesAuditting && AA.AuditCmd != null) RunAction(AA.AuditCmd, tran);
            //return newId;

            return 0;
        }
        public void RunAudittedActions(AudittedActionCollection AAs)
        {
            IDbTransaction tran = StartTransaction();
            try
            {
                RunAudittedActions(AAs, tran);
                CommitTransaction(ref tran);
            }
            catch (Exception e)
            {
                RollbackTransaction(ref tran);
                throw e;
            }
        }

        public void RunAudittedActions(AudittedActionCollection AAs, IDbTransaction tran)
        {
            foreach (AudittedAction AA in AAs)
            {
                //ExecSqlNonQuery(AA.ActionCmd, tran);
                RunAction(AA.ActionCmd, tran);
                if (Settings.Instance.ProgramDoesAuditting && AA.AuditCmd != null)
                {
                    //ExecSqlNonQuery(AA.AuditCmd, tran);
                    RunAction(AA.ActionCmd, tran);
                }
            }
        }


        #region T-Sql Statement Constants & Functions

        public virtual string AssignmentOperator { get { return " = "; } }
        public virtual string EqualToOperator { get { return " = "; } }
        public virtual string LessThanOperator { get { return " < "; } }
        public virtual string LessThanEqualToOperator { get { return " <= "; } }
        public virtual string GreaterThanOperator { get { return " > "; } }
        public virtual string GreaterThanEqualToOperator { get { return " >= "; } }
        public virtual string NotEqualToOperator { get { return " <> "; } }
        public virtual string LikeOperator { get { return " LIKE "; } }
        public virtual string AndOperator { get { return " AND "; } }
        public virtual string OrOperator { get { return " OR "; } }
        public virtual string InOperator { get { return " IN "; } }
        public virtual string InnerJoinOperator { get { return " INNER JOIN "; } }
        public virtual string GroupByOperator { get { return " GROUP BY "; } }
        public virtual string AllFieldsOperator { get { return " * "; } }
        public virtual string TrueWhereClause { get { return " 1=1 "; } }
        public virtual string FalseWhereClause { get { return " 1=0 "; } }
        public virtual string WildCardOperator { get { return "%"; } }
        public virtual string DistinctFieldsOperator { get { return " DISTINCT "; } }
        public virtual string TextQuoteOperator { get { return "'"; } }
        public virtual string TrueValue { get { return "1"; } }
        public virtual string FalseValue { get { return "0"; } }


        public virtual string FieldSeperator { get { return ", "; } }
        public virtual string ParameterPrefix { get { return ":"; } }
        public virtual string OpeningFieldQualifier { get { return " = "; } }
        public virtual string ClosingFieldQualifier { get { return " = "; } }
        public virtual string UseFieldQualifiers { get { return " = "; } }

        public virtual string SQLParameterPrefix { get { return "@"; } }


        public virtual string SelectSql(string objName, string fieldList, string whereClause, string orderByClause, bool selectDistinct)
        {
            return SelectSql(objName, fieldList, whereClause, null, orderByClause, selectDistinct);
        }
        public virtual string SelectSql(string objName, string fieldList, string whereClause, string groupByClause, string orderByClause)
        {
            return SelectSql(objName, fieldList, whereClause, groupByClause, orderByClause, false);
        }
        public virtual string SelectSql(string objName, string fieldList, string whereClause, string groupByClause, string orderByClause, bool selectDistinct)
        {
            if (fieldList == null || fieldList == string.Empty) fieldList = AllFieldsOperator;
            if (selectDistinct) fieldList = string.Concat(DistinctFieldsOperator, fieldList);
            if (whereClause == null) whereClause = string.Empty;
            if (groupByClause == null) groupByClause = string.Empty;
            if (orderByClause != null && orderByClause != string.Empty) orderByClause = string.Concat(" ORDER BY ", orderByClause);

            string sql = string.Concat("SELECT ", fieldList, " FROM ", objName);
            if (whereClause != string.Empty) sql += string.Concat(" WHERE ", whereClause);
            if (groupByClause != string.Empty) sql += string.Concat(" GROUP BY ", groupByClause);
            sql += orderByClause;
            return sql;
        }
        public virtual string GetMenuList(string groupId, string userId)
        {
            string sql = string.Empty;
            sql = "select *";
            sql += " from app_group g, app_user u, aon_menu_mst m, aon_module_mst mo";
            sql += " where u.group_id = g.group_id";
            sql += " and m.module_id = mo.modulekeyid";
            sql += " and g.group_id = m.group_id";
            sql += " and m.main_application_key_id is null";
            sql += " and u.u_id = '" + userId + "'";
            sql += " and g.group_id = '" + groupId + "' order by m.order_no asc";

            return sql;
        }
        public virtual string GetSubMenuList(string groupId, string userId, string moduleId)
        {
            string sql = string.Empty;
            sql = "select *";
            sql += " from group_app_dtl g,aon_menu_mst m, aon_module_mst mo";
            sql += " where ";
            sql += "  m.module_id = mo.modulekeyid";
            // sql += " and g.group_id = m.group_id";
            sql += " and g.menu_key_id=m.menu_key_id";
            //sql += " and m.main_application_key_id is not null";
            //sql += " and u.u_id = '" + userId + "'";
            sql += " and g.group_id = " + groupId;
            sql += " and m.module_id=" + moduleId;
            sql += " order by m.order_no asc";

            return sql;
        }
        public virtual string GetPremiumList(long contributionId)
        {
            string sql = string.Empty;

            sql = " select INSURER_ID as INSURERID,c.*,h.* from p_contribution c,premium_header h where c.ph_id=h.ph_id";
            sql += " and c.contribution_id=" + contributionId;
            return sql;
        }
        public virtual string GetPremiumList()
        {
            string sql = string.Empty;

            sql = " select (select i.insurer_name from insurer i where i.insurer_id = c.insurer_id) as insurername,INSURER_ID as INSURERID,c.*,h.* from p_contribution c,premium_header h where c.ph_id=h.ph_id";
            return sql;
        }
        public virtual string GetPolicyNumberList(long companyKeyId, long branchid, string memberId, string lastName)
        {
            string sql = string.Empty;

            sql = "select * from branch b,company c,members m,policy p";
            sql += " where c.company_id=b.company_id and m.branch_id=b.branch_id";
            sql += " and p.member_id=m.member_id and";
            sql += " c.company_id=" + companyKeyId + " and m.branch_id=" + branchid;
            
            if (memberId.Length > 0)
            {
                sql += " and m.id_number='" + memberId + "'";
            }

            if (lastName.Length   > 0)
            {
                sql += " and upper(m.last_name) like '" + lastName.ToUpper()   + "%'";
            }
                        

            return sql;
        }

        public virtual string GetMemberListByName(long companyKeyId, long branchid, string lastname)
        {
            string sql = string.Empty;

            sql = "select * from branch b,company c,members m,policy p";
            sql += " where c.company_id=b.company_id and m.branch_id=b.branch_id";
            sql += " and p.member_id=m.member_id and";
            sql += " c.company_id='" + companyKeyId + "' and m.branch_id='" + branchid + "' and m.last_name='" + lastname + "'";

            return sql;
        }
        public virtual void GenerateCustomPaging(GridView gvList)
        {
            if (gvList.Rows.Count > 0)
            {
                GridViewRow gvr = gvList.BottomPagerRow;
                Label lb1 = (Label)gvr.Cells[0].FindControl("lblCurrentPage");
                lb1.Text = Convert.ToString(gvList.PageIndex + 1);
                lb1.Font.Bold = true;
                int[] page = new int[7];
                int count = 0;
                for (int index = -2; index < 5; index++)
                {
                    page[count] = gvList.PageIndex + index;
                    count += 1;
                }
                for (int i = 0; i < 7; i++)
                {
                    if (i != 3)
                    {
                        if (page[i] < 1 || page[i] > gvList.PageCount)
                        {
                            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("lbtn" + Convert.ToString(i));
                            lb.Visible = false;
                        }
                        else
                        {
                            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("lbtn" + Convert.ToString(i));
                            lb.Text = Convert.ToString(page[i]);

                            lb.CommandName = "PageNo";
                            lb.CommandArgument = lb.Text;

                        }
                    }
                }
                if (gvList.PageIndex == 0)
                {
                    LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("lbtnFirst");
                    lb.Visible = false;
                    lb = (LinkButton)gvr.Cells[0].FindControl("lbtnPrev");
                    lb.Visible = false;

                }
                if (gvList.PageIndex == gvList.PageCount - 1)
                {
                    LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("lbtnNext");
                    lb.Visible = false;
                    lb = (LinkButton)gvr.Cells[0].FindControl("lbtnLast");
                    lb.Visible = false;

                }
                if (gvList.PageIndex > gvList.PageCount - 10)
                {
                    Label lbmore = (Label)gvr.Cells[0].FindControl("nmore");
                    lbmore.Visible = false;
                }
                if (gvList.PageIndex < 9)
                {
                    Label lbmore = (Label)gvr.Cells[0].FindControl("pmore");
                    lbmore.Visible = false;
                }
            }
        }
        public virtual string GetPolicyListSearch(long companyKeyId, long branchid, string lastname, string policyNumber)
        {
            string sql = string.Empty;

            sql = "select trunc(m.birth_date) AS birth_date,b.*,c.*,m.*,p.* from branch b,company c,members m,policy p";
            sql += " where c.company_id=b.company_id and m.branch_id=b.branch_id";
            sql += " and p.member_id=m.member_id";



            if (companyKeyId != 0)
            {
                sql += " and c.company_id=" + companyKeyId;
            }
            if (branchid != 0)
            {
                sql += " and m.branch_id=" + branchid;
            }
            if (lastname != string.Empty)
            {
                sql += " and upper(m.last_name) like '%" + lastname.ToUpper() + "%'";
            }
            if (policyNumber != string.Empty)
            {
                sql += " and   upper(m.id_number) = '" + policyNumber.ToUpper() + "'";
            }
            return sql;
        }


        public virtual string GetMemberListSearch(long companyKeyId, long branchid, string lastname, string idNumber)
        {
            string sql = string.Empty;

            sql = "select ((m.first_name)|| '  ' ||(m.last_name)) as MemberName ,b.*,c.*,m.* from branch b,company c,members m";
            sql += " where c.company_id=b.company_id and m.branch_id=b.branch_id";
            //sql += " and p.member_id=m.member_id";

            if (companyKeyId != 0)
            {
                sql += " and c.company_id=" + companyKeyId;
            }
            if (branchid != 0)
            {
                sql += " and m.branch_id=" + branchid;
            }
            if (lastname != string.Empty)
            {
                sql += " and upper(m.last_name) like '%" + lastname.ToUpper() + "%'";
            }
            if (idNumber != string.Empty)
            {
                sql += " and  upper(m.id_number) = '" + idNumber.ToUpper() + "'";
            }
            return sql;
        }



        public virtual string GetAllPolicyNumberList(long companyKeyId, long branchid)
        {
            string sql = string.Empty;

            sql = "select trunc(m.birth_date) AS birth_date,b.*,c.*,m.*,p.* from branch b,company c,members m,policy p";
            sql += " where c.company_id=b.company_id and m.branch_id=b.branch_id";
            sql += " and p.member_id=m.member_id and";
            sql += " c.company_id='" + companyKeyId + "'and m.branch_id='" + branchid + "'";

            return sql;
        }
        public virtual string GetClaimCostList(int claimNumber)
        {
            string sql = string.Empty;
            sql = "select * from payee p,claim_cost cc,claim_register cr where p.payee_id=cc.payee_id and cr.claim_number=cc.claim_number";
            sql += " and cr.claim_number='" + claimNumber + "' order by cr.claim_number asc";
            return sql;
        }
        public virtual string GetAllClaimCostList()
        {
            string sql = string.Empty;
            sql = "select * from payee p,claim_cost cc,claim_register cr where p.payee_id=cc.payee_id and cr.claim_number=cc.claim_number order by cr.claim_number asc";
            return sql;
        }
        public virtual string GetGLCodeList()
        {
            string sql = string.Empty;
            sql = "select * from GL_CODES order by GL_CODE_KEY_ID desc";
            return sql;
        }

        public virtual string IsTableExists(string tableName)
        {
            string sql = string.Empty;
            sql = "select count(1) as count from user_tables where UPPER(table_name)='" + tableName.ToString().ToUpper() + "'";
            return sql;
        }

        public virtual string IsChildValueExists(string tableName, string fieldName, long values)
        {
            string sql = string.Empty;
            sql = "select count(1) as count from " + tableName + " where UPPER(" + fieldName + ")='" + values.ToString().ToUpper() + "'";
            return sql;
        }

        public virtual string IsValueExists(string tableName,string IDColumn, string fieldName, string values, long IDValue)
        {
            string sql = string.Empty;
            sql = "select count(1) as count from " + tableName + " where " + IDColumn + "<>" + IDValue.ToString() +" and UPPER(" + fieldName + ")='" + values.ToString().ToUpper() + "'";
            return sql;
        }
        public virtual string IsBankNameExists(string tableName, string fieldName, string values,long bankkeyId)
        {
            string sql = string.Empty;
            sql = "select count(1) as count from " + tableName + " where BANK_KEY_ID<>'" + bankkeyId + "' and  UPPER(" + fieldName + ")='" + values.ToString().ToUpper() + "'";
            return sql;
        }
        public virtual string IsPayeeAddressExists(string tableName, string fieldName, string values, long payeeId, string address)
        {
            string sql = string.Empty;
            sql = "select count(1) as count from " + tableName + " where Payee_Id<>'" + payeeId + "' and  UPPER(" + fieldName + ")='" + values.ToString().ToUpper() + "' and  UPPER(payee_address)='" + address.ToString().ToUpper() + "'";
            return sql;
        }
        public virtual string IsUniqueRecord(string tableName, string keyField, long keyFieldValue, string fieldName1, string fieldName2, string value1, string value2)
        {
            string sql = string.Empty;
            sql = "select count(1) as count from " + tableName + " where " + keyField + " <>'" + keyFieldValue + "' and  UPPER(" + fieldName1 + ")='" + value1.ToString().ToUpper() + "' and  UPPER(" + fieldName2 + ")='" + value2.ToString().ToUpper() + "'";
            return sql;
        }

        public virtual string GetPayeeDetails(string payeeName)
        {
            string sql = "select * from payee p where p.payee_name='" + payeeName + "'";
            return sql;
        }
        public virtual string GetSchemeList()
        {
            string sql = "select * from branch b,company c where b.COMPANY_ID=c.COMPANY_ID";
            return sql;
        }
        public virtual string GetRiskStructure(long masterRiskId)
        {
            string sql = "select * from Master_Risk mr where mr.master_Risk_Id='" + masterRiskId + "'";
            return sql;
        }
        public virtual string GetRiskStructure()
        {
            string sql = "select * from Master_Risk mr";
            return sql;
        }
        public virtual string GetRiskData(long masterRiskId, long memberId)
        {
            string sql = "select * from Master_Risk mr";
            return sql;
        }
        public virtual string GetRiskData(long memberId)
        {
            string sql = "select * from Master_Risk mr";
            return sql;
        }
        public virtual void ExecuteSP(string sqlObjects)
        {
            IDbCommand cmd = DbEngine.CreateCommand();
            cmd.CommandText = sqlObjects;
            ExecSqlNonQuery(cmd);
        }
        public virtual string SelectSequenceValue(string sequenceName)
        {
            string sql = "select " + sequenceName + ".NEXTVAL from dual";
            return sql;
        }
        public virtual string SelectSql(object objName)
        {
            string sql = string.Concat("SELECT * FROM ", objName);
            return sql;
        }
        public virtual string SelectSql(object objName, string orderByClause, bool isDesc)
        {
            string sql;
            if (isDesc)
            {
                sql = string.Concat("SELECT * FROM ", objName, " order by ", orderByClause, " desc");
            }

            else
            {
                sql = string.Concat("SELECT * FROM ", objName, " order by ", orderByClause);
            }

            return sql;
        }
        public virtual string SelectLastFiveRecords(object objName, string id)
        {
            string sql = string.Concat("SELECT * FROM ", objName, " where " + id + " not in (select top ((select count(*) from " + objName + ")-5)" + id + " from " + objName + ")");
            return sql;
        }
        public virtual string SelectCount(object objName, string id)
        {
            string sql = string.Concat("SELECT count(*) as count FROM ", objName, " where " + id);
            return sql;
        }
        public virtual string SelectSql(object objName, string ColumnName)
        {
            string sql = string.Concat("SELECT nvl(max(" + ColumnName + "),0) AS " + ColumnName + " FROM ", objName);
            return sql;
        }
        public virtual string SelectSqlMAX(string objName, string ColumnName)
        {
            string sql = string.Concat("SELECT nvl(max(" + ColumnName + "),0) AS " + ColumnName + " FROM ", objName);
            return sql;
        }

        public virtual string SelectSql(string objName, string whereClause)
        { return SelectSql(objName, null, whereClause, null, false); }

        public virtual string SelectIdentity()
        { return "SELECT @@IDENTITY"; }


        public virtual string UpdateSql(string objName, string updateFieldList, string whereFieldList)
        {
            if (whereFieldList == null || whereFieldList == string.Empty)
                whereFieldList = TrueWhereClause;
            return string.Concat("UPDATE ", objName, " SET ", updateFieldList, " WHERE ", whereFieldList);
        }
        public virtual string InsertSql(string objName, string insertFieldList, string insertParamList)
        {
            return string.Concat("INSERT INTO ", objName, " (", insertFieldList, ") VALUES (", insertParamList, ")");
        }
        public virtual string DeleteSql(string objName, string whereClause)
        {
            return string.Concat("DELETE ", objName, " WHERE ", whereClause);
        }

        public virtual string OrderByClause(string[] orderByFields)
        {
            string tmp = string.Empty;
            bool doneFirstField = false;
            foreach (string field in orderByFields)
                if (doneFirstField)
                    tmp += string.Concat(", ", field);
                else
                {
                    tmp += field;
                    doneFirstField = true;
                }
            return tmp;
        }
        #endregion

        #region Static functions to instantiate actual provider specific objects

        public static IDbConnection CreateConnection()
        {
            IDbConnection cn;

            if (HttpContext.Current.Session[VMVServices.Web.VMVServicesGlobal.Consts.CurrentUserConnection] == null)
            {
                HttpContext.Current.Session[VMVServices.Web.VMVServicesGlobal.Consts.CurrentUserConnection] = (IDbConnection)CreateDALObj(Settings.Instance.DALAssembly, Settings.Instance.DALConnection);
            }
            
            cn = (IDbConnection)HttpContext.Current.Session[VMVServices.Web.VMVServicesGlobal.Consts.CurrentUserConnection];
            
            

            if (cn.State != ConnectionState.Closed)
            {
                cn.Close();
            }

            cn.ConnectionString = Settings.ConnectionString;

            if (cn.ConnectionString == string.Empty) throw new Exception("No connection string provided. Check the config file.");
            try
            {
                         
                
                cn.Open();
                
            }
            catch (Exception e)
            {
                if (e is SqlException)
                    if (((SqlException)e).Number == 17 && ((SqlException)e).Message == "SQL Server does not exist or access denied.")
                        throw new ErrorManagement.DatabaseConnectionException(e.Message);
                    else
                        throw;
                else if (e is OracleException)
                    if (((OracleException)e).Code == 01017)
                        throw new ErrorManagement.DatabaseConnectionException(e.Message);
                    else
                        throw;
                else
                    throw;
            }           
            return cn;
        }

        public static IDbCommand CreateCommand()
        {
            return (IDbCommand)CreateDALObj(Settings.Instance.DALAssembly, Settings.Instance.DALCommand);
        }

        public static IDbDataParameter CreateParameter()
        {
            return (IDbDataParameter)CreateDALObj(Settings.Instance.DALAssembly, Settings.Instance.DALParameter);
        }

        public static IDbDataAdapter CreateDataAdapter()
        {
            return (IDbDataAdapter)CreateDALObj(Settings.Instance.DALAssembly, Settings.Instance.DALDataAdapter);
        }

        public static object CreateDALObj(string DALAssembly, string DALObj)
        {
            //Type t;
            foreach (AssemblyName an in Assembly.GetExecutingAssembly().GetReferencedAssemblies())
                if (an.Name == DALAssembly)
                {
                    Assembly a = Assembly.Load(an.FullName);
                    Type t = a.GetType(DALObj);
                    return Activator.CreateInstance(t);
                }
            return null;//Couldn't find the specifiedc assembly
        }


        #endregion

        #region Query Functions
        public DataSet getDataSet(IDbCommand cmd, IDbTransaction tran)
        {
            //
            //Here we use a transaction so we won't tear
            //down and destroy the connection object
            //
            DataSet ds = new DataSet();
            IDbDataAdapter da = DbEngine.CreateDataAdapter();
            da.SelectCommand = SetupCommand(cmd, tran);
            da.Fill(ds);
            return ds;
        }
        public DataSet getDataSet(IDbCommand cmd)
        {
            //
            //This is where the majority of call to return data from the 
            //Database will end up, this is the final resting spot
            //for database queries that return sets of data
            //
            //Because we're not working with a transaction we
            //tear down and destroy the connection and 
            //command objects after the query
            //
            DataSet ds = new DataSet();
            IDbDataAdapter da = DbEngine.CreateDataAdapter();
            da.SelectCommand = SetupCommand(cmd);
            try
            {
                da.Fill(ds);
            }
            catch
            {
                throw;//pass the erorr on
            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection.Dispose();
                cmd.Dispose();
            }
            return ds;
        }

        public DataSet UpdateDataSet(IDbCommand cmd)
        {
            //
            //This is where the majority of call to return data from the 
            //Database will end up, this is the final resting spot
            //for database queries that return sets of data
            //
            //Because we're not working with a transaction we
            //tear down and destroy the connection and 
            //command objects after the query
            //
            DataSet ds = new DataSet();
            IDbDataAdapter da = DbEngine.CreateDataAdapter();
            da.SelectCommand = SetupCommand(cmd);
            try
            {
                da.Fill(ds);
            }
            catch
            {
                throw;//pass the erorr on
            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection.Dispose();
                cmd.Dispose();
            }
            return ds;
        }//Changed on 12/09/2011 arasu

        public DataSet getDataSet(string sql)
        {
            return getDataSet(SetupCommand(sql));
        }
        //Changed on 12/09/2011 arasu
        public DataSet UpdateDataSet(string sql)
        {
            return UpdateDataSet(SetupCommand(sql));
        }

        public DataSet getDataSet(string sql, IDbTransaction tran)
        {
            return getDataSet(SetupCommand(sql, tran));
        }

        public DataTable getDataTable(IDbCommand cmd, IDbTransaction tran)
        { return getDataSet(cmd, tran).Tables[0]; }
        public DataTable getDataTable(IDbCommand cmd)
        { return getDataSet(cmd).Tables[0]; }

        public DataSet GetDataSet(IDbCommand cmd)
        {
            return getDataSet(cmd);
        }

        public DataTable getDataTable(string sql)
        { return getDataSet(sql).Tables[0]; }
        public DataTable getDataTable(string sql, IDbTransaction tran)
        { return getDataSet(sql, tran).Tables[0]; }
        public DataTable getDataTableQuery(string sql)
        { return getDataTable(sql); }

        public static IDataReader getDataReader(IDbCommand cmd)
        {
            cmd.Connection = DbEngine.CreateConnection();
            return cmd.ExecuteReader(CommandBehavior.CloseConnection);
        }
        public static IDataReader getDataReader(string sql)
        {
            return getDataReader(DbEngine.SetupCommand(sql));
        }

        public void ExecSqlNonQuery(IDbCommand cmd, IDbTransaction tran)
        {
            cmd = SetupCommand(cmd, tran);
            cmd.ExecuteNonQuery();
        }
        public void ExecSqlNonQuery(IDbCommand cmd)
        {
            try
            {
                cmd = SetupCommand(cmd);
                cmd.ExecuteNonQuery();

                // return ExecSqlScalar(cmd, tran);
            }
            catch (Exception e)
            {
                if (e is System.Data.SqlClient.SqlException)
                {
                    //'ex.number = 2601 = microsoft sql server unique index violation
                    //'ex.number = 2627 = microsoft sql server unique constraint violation
                    //
                    //'ex.number = 547 = DELETE statement conflicted with COLUMN REFERENCE constraint '[CONSTRAINT]'. The conflict occurred in database '[DATABASE]', table '[TABLE]', column '[COLUMN]'.
                    //'                   This error indicates a foreign key delete violation, that is a record is being deleted from a table 
                    //'                   when that same record is referenced by another record from another table. The user must first delete
                    //'                   the second record, thus destroying the reference and allowing the delete of the first record.
                    if (((System.Data.SqlClient.SqlException)e).Number == 2601 ||
                        ((System.Data.SqlClient.SqlException)e).Number == 2627)
                        //Throw RecordAlreadyExistsException
                        //throw new iThink.Services.ErrorManagement.RecordAlreadyExistsException(string.Empty);
                        throw new VMVServices.Services.ErrorManagement.RecordAlreadyExistsException(e.Message);
                    else if (((System.Data.SqlClient.SqlException)e).Number == 547)
                        throw new VMVServices.Services.ErrorManagement.DependantRecordDeletionException(string.Empty);
                    else
                        throw e;
                }
                else if (e is System.Data.OracleClient.OracleException)
                {
                    if (((System.Data.OracleClient.OracleException)e).ErrorCode == 2601 ||
                        ((System.Data.OracleClient.OracleException)e).ErrorCode == 2627)
                        throw new VMVServices.Services.ErrorManagement.RecordAlreadyExistsException(e.Message);
                    else if (((System.Data.OracleClient.OracleException)e).ErrorCode == 547)
                        throw new VMVServices.Services.ErrorManagement.DependantRecordDeletionException(string.Empty);
                    else
                        throw e;
                }
                else if (e is System.Data.OleDb.OleDbException)
                {
                    if (((System.Data.OleDb.OleDbException)e).ErrorCode == -2147467259)
                    {
                        //oledb unique index violation
                        //Throw RecordAlreadyExistsException
                        throw e;
                    }
                    else
                    { throw e; }
                }
                else
                {
                    throw e;
                }

            }



        }
        public void ExecSqlNonQuery(string sql)
        {
            IDbCommand cmd = DbEngine.CreateCommand();
            cmd.CommandText = sql;
            ExecSqlNonQuery(cmd);
        }

        public object ExecSqlScalar(IDbCommand cmd, IDbTransaction tran)
        {
            cmd = SetupCommand(cmd, tran);
            return cmd.ExecuteScalar();
        }
        public object ExecSqlScalar(IDbCommand cmd)
        {
            cmd = SetupCommand(cmd);
            return cmd.ExecuteScalar();
        }
        public object ExecSqlScalar(string sql)
        {
            IDbCommand cmd = DbEngine.CreateCommand();
            cmd.CommandText = sql;
            return ExecSqlScalar(cmd);
        }

        #endregion

        #region Transaction Support
        public IDbTransaction StartTransaction()
        {
            IDbConnection cn = DbEngine.CreateConnection();
            return cn.BeginTransaction();
        }
        public void CommitTransaction(ref IDbTransaction tran)
        {
            IDbConnection cn = tran.Connection;

            try {
                
                tran.Commit(); 

            }
            catch (Exception e) { tran.Rollback(); throw e; }
            finally { KillTransaction(ref tran, ref cn); }
        }
        public void RollbackTransaction(ref IDbTransaction tran)
        {
            IDbConnection cn = tran.Connection;
            try { tran.Rollback(); }
            catch (Exception e) { throw e; }
            finally { KillTransaction(ref tran,ref cn); }
        }
        protected void KillTransaction(ref IDbTransaction tran, ref IDbConnection cn)
        {
            //if (cn != null)
            //    try { cn.Close(); }
            //    finally { cn.Dispose(); }
            //tran.Dispose();

            //if (tran.Connection != null)
            //    try { tran.Connection.Close(); }
            //    finally { tran.Connection.Dispose(); }
            //tran.Dispose();
        }
        #endregion

        #region Sql Generator
        //
        //This section inspects the SqlGeneratorAttributes of an object, in order
        //to map object properties to database table fields and then using that 
        //mapping create insert, update and delete commands to manipulate the data
        //

        public static IDbCommand getInsertCmd(object obj)
        {
            return getInsertCmd(obj, false);
        }
        public static IDbCommand getInsertCmd(object obj, bool useAuditProperties)
        {
            string tableName = getTableNameFromObj(obj);
            IDbCommand cmd = DbEngine.CreateCommand();

            SqlFieldAttributeCollection sqlFields = Utils.getSqlFieldAttributes(obj.GetType());
            FieldParameter insertFieldParams = getInsertFieldParams(sqlFields, obj, useAuditProperties);

            cmd.CommandText = DbEngine.Instance.InsertSql(tableName, insertFieldParams.InsertFieldList, insertFieldParams.InsertParamFieldList);

            DbEngine.AttachParameters(ref cmd, insertFieldParams.Params);

            return cmd;
        }
        public static IDbCommand getUpdateCmd(object obj)
        {
            string tableName = getTableNameFromObj(obj);
            IDbCommand cmd = DbEngine.CreateCommand();

            SqlFieldAttributeCollection sqlFields = Utils.getSqlFieldAttributes(obj.GetType());
            FieldParameter updateFieldParams = getUpdateFieldParams(sqlFields, obj, false);
            FieldParameter whereFieldParams = getWhereFieldParams(sqlFields, obj);

            cmd.CommandText = DbEngine.Instance.UpdateSql(tableName, updateFieldParams.FieldAssignmentList, whereFieldParams.FieldAssignmentList);

            DbEngine.AttachParameters(ref cmd, updateFieldParams.Params);
            DbEngine.AttachParameters(ref cmd, whereFieldParams.Params);

            return cmd;
        }
        public static IDbCommand getDeleteCmd(object obj)
        {
            string tableName = getTableNameFromObj(obj);
            IDbCommand cmd = DbEngine.CreateCommand();

            SqlFieldAttributeCollection sqlFields = Utils.getSqlFieldAttributes(obj.GetType());
            FieldParameter whereFieldParams = getWhereFieldParams(sqlFields, obj);

            cmd.CommandText = DbEngine.Instance.DeleteSql(tableName, whereFieldParams.FieldAssignmentList);

            DbEngine.AttachParameters(ref cmd, whereFieldParams.Params);

            return cmd;
        }

        static string getTableNameFromObj(object obj)
        {
            SqlTableAttribute att = Utils.getSqlTableAttribute(obj.GetType());
            return att.TableName;
        }

        static FieldParameter getUpdateFieldParams(SqlFieldAttributeCollection sqlFields, object obj, bool useAuditProperties)
        {
            FieldParameter updateFieldParams = new FieldParameter();
            foreach (SqlFieldAttribute sqlField in sqlFields)
                if ((sqlField.Updateable && !sqlField.PrimaryKeyField) &&
                    ((!useAuditProperties && !sqlField.IsAuditProperty) || useAuditProperties))
                {
                    updateFieldParams.AddFieldAssignment(sqlField.FieldName, Instance.AssignmentOperator, Instance.ParameterPrefix);
                    updateFieldParams.Params.Add(sqlField.getParameterFromObj(ref obj));
                }
            return updateFieldParams;
        }

        static FieldParameter getInsertFieldParams(SqlFieldAttributeCollection sqlFields, object obj, bool useAuditProperties)
        {
            FieldParameter insertFieldParams = new FieldParameter();
            foreach (SqlFieldAttribute sqlField in sqlFields)
                if (sqlField.Updateable && ((!useAuditProperties && !sqlField.IsAuditProperty) || useAuditProperties) && !sqlField.PrimaryKeyField)
                {
                    insertFieldParams.AddInsertFieldList(sqlField.FieldName);
                    insertFieldParams.AddInsertParamFieldList(sqlField.FieldName, Instance.ParameterPrefix);
                    insertFieldParams.Params.Add(sqlField.getParameterFromObj(ref obj));
                }
                else if (sqlField.Updateable && ((!useAuditProperties && !sqlField.IsAuditProperty) || useAuditProperties) && sqlField.PrimaryKeyField && sqlField.NotAutoIncrement)
                {
                    insertFieldParams.AddInsertFieldList(sqlField.FieldName);
                    insertFieldParams.AddInsertParamFieldList(sqlField.FieldName, Instance.ParameterPrefix);
                    insertFieldParams.Params.Add(sqlField.getParameterFromObj(ref obj));
                }
            return insertFieldParams;
        }

        static FieldParameter getWhereFieldParams(SqlFieldAttributeCollection sqlFields, object obj)
        {
            FieldParameter whereFieldParams = new FieldParameter();
            foreach (SqlFieldAttribute sqlField in sqlFields)
                if (sqlField.PrimaryKeyField)
                {
                    whereFieldParams.AddFieldAssignment(sqlField.FieldName, Instance.AssignmentOperator, Instance.ParameterPrefix);
                    whereFieldParams.Params.Add(sqlField.getParameterFromObj(ref obj));
                }
            return whereFieldParams;
        }


        #endregion

        #region Helper Functions
        IDbCommand SetupCommand(IDbCommand cmd, IDbTransaction tran)
        {
            if (tran == null)
                return SetupCommand(cmd);
            else
            {
                cmd.Connection = tran.Connection;
                cmd.Transaction = tran;
                return cmd;
            }
        }
        IDbCommand SetupCommand(IDbCommand cmd)
        {
            cmd.Connection = DbEngine.CreateConnection();
            
            return cmd;
        }
        public static IDbCommand SetupCommand(string commandText)
        {
            IDbCommand cmd = DbEngine.CreateCommand();
            cmd.CommandText = commandText;

            return cmd;
        }
        public static IDbCommand SetupCommand(string commandText, IDbTransaction tran)
        {
            IDbCommand cmd = SetupCommand(commandText);
            cmd.Transaction = tran;
            return cmd;
        }
        public static IDbCommand SetupCommand(string commandText, IDbDataParameter[] prms)
        {
            IDbCommand cmd = SetupCommand(commandText);
            DbEngine.AttachParameters(ref cmd, prms);
            return cmd;
        }

        static void AttachParameters(ref IDbCommand cmd, IDataParameterCollection prms)
        {
            foreach (IDataParameter prm in prms)
                cmd.Parameters.Add(prm);
        }
        static void AttachParameters(ref IDbCommand cmd, IDbDataParameter[] prms)
        {
            foreach (IDbDataParameter prm in prms)
                cmd.Parameters.Add(prm);
        }

        public static IDbDataParameter SetupParameter(string prmName, object prmValue, DbType prmDbType)
        {
            IDbDataParameter prm = DbEngine.CreateParameter();
            prm.ParameterName = prmName;
            if (!prm.ParameterName.StartsWith(DbEngine.Instance.ParameterPrefix))
                prm.ParameterName = string.Concat(DbEngine.Instance.ParameterPrefix, prm.ParameterName);
            prm.Value = prmValue;
            prm.DbType = prmDbType;
            return prm;
        }

        public static string TextConnectToDb()
        {
            try
            {
                using (IDbConnection cn = DbEngine.CreateConnection())
                {
                    return string.Concat("Successfully connected to: ", cn.ConnectionString);
                }
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }
        public static IDbDataParameter GetStartDateParameter(DateTime date)
        {
            DateTime startDate = new DateTime(date.Year, date.Month, date.Day, 0, 0, 0);
            return SetupParameter("StartDate", startDate, DbType.DateTime);
        }
        public static IDbDataParameter GetEndDateParameter(DateTime date)
        {
            DateTime tomorrow = date.AddDays(1);
            DateTime endDate = new DateTime(tomorrow.Year, tomorrow.Month, tomorrow.Day, 0, 0, 0);
            return SetupParameter("EndDate", endDate, DbType.DateTime);
        }
        public static DateTime ConvertAsDate(string sourceDate)
        {
            string strNewFormat = string.Empty;

            DateTime datetimeVal;
            bool result = DateTime.TryParse(sourceDate, GetCultureInfo(), DateTimeStyles.AssumeLocal, out datetimeVal);
            return datetimeVal;

        }
        public static CultureInfo GetCultureInfo()
        {
            CultureInfo Culture = new CultureInfo("en-GB");
            return Culture;
        }
        #endregion

    }
}

