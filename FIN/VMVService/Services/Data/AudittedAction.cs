using System;
using System.Data;

namespace VMVServices.Services.Data
{
	/// <summary>
	/// Summary description for AudittedAction.
	/// </summary>
	public class AudittedAction
	{
		public AudittedAction(IDbCommand actionCmd, IDbCommand auditCmd)
		{
			ActionCmd = actionCmd;
			AuditCmd = auditCmd;
		}

		public readonly IDbCommand ActionCmd;
		public readonly IDbCommand AuditCmd;
	}
}
