using System;

namespace VMVServices.Services.Data
{
	/// <summary>
	/// Summary description for MsAccessDbEngine.
	/// </summary>
	public class MsAccessDbEngine : DbEngine
	{
		public MsAccessDbEngine(){}

		public override string DeleteSql(string objName, string whereClause)
		{
			return string.Concat("DELETE FROM ", objName, " WHERE ", whereClause);
		}
	}
}
