using System;

namespace VMVServices.Services.Data
{
	/// <summary>
	/// Summary description for SqlTableAttribute.
	/// </summary>
	public class SqlTableAttribute : Attribute
	{
		public SqlTableAttribute(){}

		string _TableName;
		public string TableName { get{return _TableName;} set{_TableName = value;} }
	}
}
