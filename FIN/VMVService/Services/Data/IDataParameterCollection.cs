using System;
using System.Data;

namespace VMVServices.Services.Data
{
	/// <summary>
	/// Summary description for IDataParameterCollection.
	/// </summary>
	public class IDataParameterCollection : System.Collections.CollectionBase
	{
		public IDataParameterCollection(){}

		public virtual IDataParameter this[int Index] { get{ return (IDataParameter)this.List[Index]; } }

		public int Add(IDataParameter Item)
		{ return this.List.Add(Item); }

	}
}
