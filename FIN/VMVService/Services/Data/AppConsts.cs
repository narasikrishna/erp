using System;


namespace VMVServices.Services.Data
{
    /// <summary>
    /// Summary description for DbConsts.
    /// </summary>
    public class AppConsts
    {
        public AppConsts() { }

        public const string PolicyDetailReports_Policy = "PolicyDetailReports_Policy.rpt";
        public const string PolicyDetailReports_Claim = "PolicyDetailReports_Claim.rpt";
        public const string PolicyDetailsReports_MotorAssesst = "PolicyDetailsReports_MotorAssesst.rpt";
        public const string PolicyDetailsReports_AutoAssesst = "PolicyDetailsReports_AutoAssesst.rpt";
        public const string PolicyDetailsReports_HouseAssesst = "PolicyDetailsReports_HouseAssesst.rpt";
        public const string PolicyDetailsReports_Premium = "PolicyDetailsReports_Premium.rpt";

        public const string MOTOR = "MOTOR";
        public const string AUTO = "AUTO";
        public const string CREDITLIFE = "CREDITLIFE";
        public const string HOUSEOWNERS = "HOUSEOWNERS";
        public const string HOUSE = "HOUSE";

        public const string EstimateChangedClaimNumber = "EstimateChangedClaimNumber";
        public const string ClaimsPaymentRecoveries = "Claim's Payment/Recoveries";
        public const string DisapproveClaimCostID = "DisapproveClaimCostID";
        

    }
}
