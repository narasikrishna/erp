using System;
using System.IO;
using VMVServices.Services.Business;

namespace VMVServices.Services
{
	/// <summary>
	/// Summary description for ErrorManagement.
	/// </summary>
	public class FileManagement
	{
        static string _fileName = string.Empty;
        static string _filePath = string.Empty;
        static string _fileExtention = string.Empty;
        static string _message = string.Empty;



        public static string  FileName 
        {
            get 
            {
                return _fileName;
            }
            set
            {
                _fileName = value;
 
            }
        }

        public static string FilePath
        {
            get
            {
                return _filePath;
            }
            set
            {
                _filePath = value;

            }
        }

        public static string FileExtention
        {
            get
            {
                return _fileExtention;
            }
            set
            {
                _fileExtention = value;

            }
        }

        public static string Message
        {
            get
            {
                return _message;
            }
            set
            {
                _message = value;

            }
        }

        public static void WriteText(bool useFileExtention)
        {
            string filenameWithPath;
            
            if (useFileExtention)
            {
                filenameWithPath = FilePath + FileName + FileExtention;
            }
            else
            {
                filenameWithPath = FilePath + FileName;
 
            }
            
            if (!File.Exists(@filenameWithPath))
            {

                FileStream fs = null;
                fs = File.Create(filenameWithPath);
                fs.Close();
                
            }

            StreamWriter sw = new StreamWriter(@filenameWithPath,true);

            sw.WriteLine(Message); 
            sw.Close();
            sw.Dispose();
            

 
       }

        public static void CreateFolder(string folderPathAndName)
        {
            
            if (Directory.Exists(folderPathAndName))
            {
                Directory.Delete(folderPathAndName,true); 
            }

            //System.Security.AccessControl.DirectorySecurity ds = new System.Security.AccessControl.DirectorySecurity();
            Directory.CreateDirectory(folderPathAndName);
        }

        

        
        
	}
}
