using System;
using VMVServices.Services.Business;

namespace VMVServices.Services
{
	/// <summary>
	/// Summary description for ErrorManagement.
	/// </summary>
	public class ErrorManagement
	{
		public static void Publish(Exception ex, long loggedOnUserID, bool publishErrorDetails)
		{
			try
			{
				
				//we better publish the error here or else!!!
				SysError err = new SysError();
				string errMessage;
				string detailedErrMessage;
				string lineBreak = "<br><br>";

				errMessage = string.Concat("<strong>Message: </strong>", ex.Message);

				detailedErrMessage = lineBreak;
				detailedErrMessage += "-----------------------------------";
				detailedErrMessage += lineBreak;
				detailedErrMessage += string.Concat("<strong>Message: </strong>", ex.Message);
				detailedErrMessage += lineBreak;
				detailedErrMessage += "-----------------------------------";
				detailedErrMessage += lineBreak;
				detailedErrMessage += string.Concat("<strong>Stacktrace: </strong>", ex.StackTrace.Replace(Environment.NewLine, "<br><br>"), "<br><br><br><br>");
				while (ex.InnerException != null)
				{
					detailedErrMessage += lineBreak;
					detailedErrMessage += "-----------------------------------";
					detailedErrMessage += lineBreak;
					detailedErrMessage += string.Concat("<strong>Message: </strong>", ex.InnerException.Message);
					detailedErrMessage += lineBreak;
					detailedErrMessage += "-----------------------------------";
					detailedErrMessage += lineBreak;
					detailedErrMessage += string.Concat("<strong>Stacktrace: </strong>", ex.InnerException.StackTrace.Replace(Environment.NewLine, "<br><br>"), "<br><br><br><br>");

					ex = ex.InnerException;
				}

				err.ErrorMessage = errMessage;
				if (publishErrorDetails) err.ErrorStackTrace = detailedErrMessage;

				err.SysAuditUserID = loggedOnUserID;
			
				err.Save(-1);
			}
			catch{/*do nothing, we're reporting an error here MAN!*/}
		}

		public class DatabaseConnectionException : Exception{public DatabaseConnectionException(string Message) : base(Message){}}
		public class CanNotGetNextValueException : Exception{public CanNotGetNextValueException(string Message) : base(Message){}}
		public class DependantRecordDeletionException : Exception{public DependantRecordDeletionException(string Message) : base(Message){}}
		public class RecordAlreadyExistsException : Exception{public RecordAlreadyExistsException(string Message):base(Message){}}

	}
}
