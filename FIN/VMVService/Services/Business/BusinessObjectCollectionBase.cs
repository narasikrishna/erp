using System;
using System.Collections;

namespace VMVServices.Services.Business
{
	/// <summary>
	/// Summary description for BusinessObjectCollectionBase.
	/// </summary>
	public class BusinessObjectCollectionBase : CollectionBase
	{
		public BusinessObjectBase[] ToBusinessObjectArray()
		{
			BusinessObjectBase[] arr = new BusinessObjectBase[this.List.Count];
			for (int cntr = 0;cntr < this.List.Count;cntr++)
                arr[cntr] = (BusinessObjectBase)this.List[cntr];
			return arr;
		}
	}
}
