using System;
using System.Data;
using System.Data.OracleClient;
using VMVServices.Services.Data;
using VMVServices.Helpers;
using System.Collections.Generic;
using System.Text;
using System.Globalization;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Collections;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace VMVServices.Services.Business
{
    /// <summary>
    /// Summary description for BusinessObjectBase.
    /// </summary>
    public abstract class BusinessObjectBase
    {
        //Constructor
        public BusinessObjectBase() { }

        public abstract string ObjDescription { get; }
        public abstract string ObjTypeName { get; }
        protected abstract void SetupChildObjectsForDelete();

        public System.Collections.SortedList ErrorCollection = new System.Collections.SortedList();

        public DataTable GetLastXRecordsByDateCreated(int x)
        {
            return Db.getDataTable(
                Db.SelectSql(
                    this.ViewName,
                    string.Concat("Top ", x.ToString(), " *"),
                    string.Empty,
                    "SysDateCreated DESC",
                    false));
        }


        #region DB, Table, View and Column Name properties

        protected virtual string OrderByFields { get { return string.Empty; } }

        long _ParentID;
        protected virtual long ParentID { get { return _ParentID; } set { _ParentID = value; } }

        long _ObjID;
        public virtual long ObjID { get { return _ObjID; } set { _ObjID = value; } }

        //string _StrObjID;
        //public virtual string StrObjID { get { return _StrObjID; } set { _StrObjID = value; } }

        //bool _AddMode;
        //public virtual bool AddMode { get { return _AddMode; } set { _AddMode = value; } }

        //bool _EditMode;
        //public virtual bool EditMode { get { return _EditMode; } set { _EditMode = value; } }

        //long _LngObjID;
        //public virtual long LngObjID { get { return _LngObjID; } set { _LngObjID = value; } }

        DateTime _auditDate;
        protected virtual DateTime auditDate { get { return _auditDate; } set { _auditDate = value; } }

        string _auditAction;
        protected virtual string auditAction { get { return _auditAction; } set { _auditAction = value; } }

        long _auditUserID;
        protected virtual long auditUserID { get { return _auditUserID; } set { _auditUserID = value; } }

        string _TableName;
        public string TableName
        {
            get
            {
                if (_TableName == null)
                {
                    SqlTableAttribute att = Utils.getSqlTableAttribute(this.GetType());
                    _TableName = att.TableName;
                }
                return _TableName;
            }
        }

        public virtual string ViewName { get { return TableName; } }
        string _TableIDColumn;
        public virtual string TableIDColumnName
        {
            get
            {
                
                SqlFieldAttribute  att = Utils.getSqlIDFieldAttributes(this.GetType());
                _TableIDColumn = att.PropertyName; 

                return _TableIDColumn;
            }
        }
        protected virtual string AuditTableName
        {
            //Remove the starting 'tbl' and replace it with 'tbl_Audit_'
            get { return string.Concat("tbl_Audit_", TableName.Remove(0, 3)); }
        }
        public virtual string ParentIDColumnName { get { return string.Empty; } }

        protected DbEngine Db
        { get { return VMVServices.Services.Data.DbEngine.Instance; } }

        #endregion

        #region Business Logic Layer Implementation

        #region Object Relationship Properties and Methods

        protected virtual BusinessObjectBase[] SiblingObjects { get { return new BusinessObjectBase[] { }; } }
        protected virtual BusinessObjectBase[] ChildObjects { get { return new BusinessObjectBase[] { }; } }
        protected virtual BusinessObjectBase[] RelatedObjects { get { return new BusinessObjectBase[] { }; } }

        protected BusinessObjectBase _PrimarySiblingObj;
        public virtual BusinessObjectBase PrimarySiblingObj { get { return _PrimarySiblingObj; } }

        protected BusinessObjectBase _ParentObj;
        public virtual BusinessObjectBase ParentObj { get { return _ParentObj; } }

        bool _MarkedForDelete = false;
        public bool MarkedForDelete { get { return _MarkedForDelete; } set { _MarkedForDelete = value; } }

        bool _MarkedForCollectionOnly = false;
        public bool MarkedForCollectionOnly { get { return _MarkedForCollectionOnly; } set { _MarkedForCollectionOnly = value; } }
        #endregion

        public virtual void Delete(long auditUserID)
        {
            Db.RunAudittedActions(this.getDeleteAAs(auditUserID));
        }

        public virtual AudittedActionCollection getDeleteAAs(long auditUserID)
        {
            AudittedActionCollection AAs = new AudittedActionCollection();

            this.SetupChildObjectsForDelete();

            foreach (BusinessObjectBase bob in ChildObjects)
                AAs.AddRange(bob.getDeleteAAs(auditUserID));

            foreach (BusinessObjectBase bob in RelatedObjects)
                if (bob.ObjID != 0)
                    AAs.AddRange(bob.getDeleteAAs(auditUserID));

            AAs.AddRange(this.getDeleteAA(auditUserID));

            foreach (BusinessObjectBase bob in SiblingObjects)
                AAs.AddRange(bob.getDeleteAAs(auditUserID));

            return AAs;
        }

        public virtual AudittedActionCollection getDeleteAA(long auditUserID)
        {

            this.PreDeleteAction(auditUserID);

            this.auditAction = "D";
            this.auditDate = DateTime.Now;
            this.auditUserID = auditUserID;

            AudittedActionCollection AAs = new AudittedActionCollection();

            AAs.AddRange(this.PreDeleteActionAAs);

            AAs.Add(
                new AudittedAction(
                    DbEngine.getDeleteCmd(this),
                    DbEngine.getInsertCmd(this, true)));

            this.PostDeleteAction(auditUserID);
            AAs.AddRange(this.PostDeleteActionAAs);

            return AAs;
        }

        public virtual void Save(long auditUserId)
        {
            //Db.RunAudittedActions(getSaveAA(AuditUserID)); 
            this.runSaveAAs(auditUserId, null);
        }

        public virtual void Save(long auditUserId, IDbTransaction tran)
        {
            this.runSaveAAs(auditUserId, tran);
        }

        public virtual void runSaveAAs(long auditUserId, IDbTransaction tran)
        {
            bool tranCreatedHere = false;//this variable is required because this function is used recursively and thus the transaction can only be committed or rolled back in the originating function
            if (tran == null)
            {
                tran = Db.StartTransaction();
                tranCreatedHere = true;
            }

            try
            {



                if (this.ObjID == 0)
                {
                    //
                    //Add Procedures
                    //

                    //this line insert the record into the DB and returns the new identity field value
                    foreach (BusinessObjectBase bob in SiblingObjects)
                        bob.runSaveAAs(auditUserId, tran);

                    if (MarkedForCollectionOnly != true)
                    {
                        this.ObjID = Db.RunAudittedActionForInsert(this.getAddAA(auditUserId), tran);
                    }

                    

                    foreach (BusinessObjectBase bob in ChildObjects)
                    {
                        bob.ParentID = this.ObjID;
                        bob.runSaveAAs(auditUserId, tran);
                    }
                }
                else
                {
                    //
                    //Update Procedures
                    //
                    Db.RunAudittedActions(this.getUpdateAA(auditUserID), tran);

                    foreach (BusinessObjectBase bob in SiblingObjects)
                        bob.runSaveAAs(auditUserId, tran);

                    foreach (BusinessObjectBase bob in ChildObjects)
                    {
                        bob.ParentID = this.ObjID;
                        if (bob.MarkedForDelete)
                            Db.RunAudittedActions(bob.getDeleteAAs(auditUserID), tran);
                        else
                            bob.runSaveAAs(auditUserId, tran);
                    }
                }
                


                if (tranCreatedHere) Db.CommitTransaction(ref tran);
            }
            catch
            {
                if (tranCreatedHere) Db.RollbackTransaction(ref tran);
                throw;
            }

        }

        protected virtual AudittedAction getAddAA(long auditUserId)
        {

            this.PreAddAction(auditUserId);

            //we no longer need this blessed piece of code for which
            //i spent hours working on
            //this.ObjID = AutoNumberGenerator.GetNextIDValue(auditUserID, TableName);
            this.auditAction = "A";
            this.auditDate = DateTime.Now;
            this.auditUserID = auditUserID;

            return new AudittedAction(
                DbEngine.getInsertCmd(this),
                DbEngine.getInsertCmd(this, true));

        }
        protected virtual AudittedActionCollection getUpdateAA(long auditUserID)
        {

            this.PreUpdateAction(auditUserID);

            this.auditAction = "U";
            this.auditDate = DateTime.Now;
            this.auditUserID = auditUserID;

            AudittedActionCollection AAs = new AudittedActionCollection();

            AAs.AddRange(this.PreUpdateActionAAs);

            AAs.Add(
                new AudittedAction(
                DbEngine.getUpdateCmd(this),
                DbEngine.getInsertCmd(this, true)));

            this.PostUpdateAction(auditUserID);
            AAs.AddRange(this.PostUpdateActionAAs);

            return AAs;

        }

        //		public virtual AudittedActionCollection getUpdateAAs(int auditUserId)
        //		{
        //			AudittedActionCollection AAs = new AudittedActionCollection();
        //
        //			//
        //			//Update Procedures
        //			//
        //			AAs.AddRange(this.getUpdateAA(auditUserId));
        //
        //			foreach (BusinessObjectBase bob in SiblingObjects)
        //				AAs.AddRange(bob.getUpdateAA(auditUserId));
        //
        //			foreach (BusinessObjectBase bob in ChildObjects)
        //			{
        //				if (bob.ObjID == 0)
        //				{
        //					bob.ParentID = this.ObjID;
        //					AAs.AddRange(bob.getAddAA(auditUserId));
        //				}
        //				else
        //					if (bob.MarkedForDelete)
        //					AAs.AddRange(bob.getDeleteAAs(auditUserId));
        //				else
        //					AAs.AddRange(bob.getUpdateAA(auditUserId));
        //			}
        //
        //			return AAs;
        //		}

        //		public virtual void runAddAAs(int auditUserId, IDbTransaction tran)
        //		{
        //			//
        //			//Add Procedures
        //			//
        //
        //			//this line insert the record into the DB and returns the new identity field value
        //			this.ObjID = Db.RunAudittedActionForInsert(this.getAddAA(auditUserID), tran);
        //
        //			foreach (BusinessObjectBase bob in SiblingObjects)
        //				Db.RunAudittedActionForInsert(bob.getAddAA(auditUserID), tran);
        //			
        //			foreach (BusinessObjectBase bob in ChildObjects)
        //			{
        //				bob.ParentID = this.ObjID;
        //				bob.runSaveAAs(auditUserID, tran);
        //			}
        //
        //		}
        //		public virtual AudittedActionCollection getSaveAA(int auditUserID)
        //		{
        //			//
        //			//This function is in the process of becoming obsolete!!!
        //			//
        //
        //			AudittedActionCollection AAs = new AudittedActionCollection();
        //
        //			if (ObjID == 0)
        //			{
        //				//
        //				//Add Procedures
        //				//
        //				foreach (BusinessObjectBase bob in SiblingObjects)
        //					AAs.AddRange(bob.getSaveAA(auditUserID));
        //					//AAs.AddRange(bob.getAddAA(auditUserID));
        //
        //				AAs.AddRange(this.getAddAA(auditUserID));
        //
        //				foreach (BusinessObjectBase bob in ChildObjects)
        //				{
        //					bob.ParentID = this.ObjID;
        //					//AAs.AddRange(bob.getAddAA(auditUserID));
        //					AAs.AddRange(bob.getSaveAA(auditUserID));
        //				}
        //			}
        //			else
        //			{
        //				//
        //				//Update Procedures
        //				//
        //				AAs.AddRange(this.getUpdateAA(auditUserID));
        //
        //				foreach (BusinessObjectBase bob in SiblingObjects)
        //					AAs.AddRange(bob.getUpdateAA(auditUserID));
        //
        //				foreach (BusinessObjectBase bob in ChildObjects)
        //				{
        //					if (bob.ObjID == 0)
        //					{
        //						bob.ParentID = this.ObjID;
        //						AAs.AddRange(bob.getAddAA(auditUserID));
        //					}
        //					else
        //						if (bob.MarkedForDelete)
        //							AAs.AddRange(bob.getDeleteAAs(auditUserID));
        //						else
        //							AAs.AddRange(bob.getUpdateAA(auditUserID));
        //				}
        //			}
        //
        //			return AAs;
        //		}
        //

        public virtual DataTable SelectAll()
        {
            return Db.getDataTable(Db.SelectSql(ViewName, null, null, this.OrderByFields, false));
        }
        public virtual DataTable SelectAll(string orderByFields, bool isDesc)
        {
            return Db.getDataTable(Db.SelectSql(ViewName,orderByFields,isDesc));
        }
        public DataTable SelectAll(int ParentID)
        {
            return Db.getDataTable(Db.SelectSql(ViewName,
                null,
                string.Concat(ParentIDColumnName, Db.EqualToOperator, ParentID),
                this.OrderByFields,
                false));
        }
        public DataTable SelectDatatableQuery(string sql)
        {
            return Db.getDataTableQuery(sql);
        }

        public DataTable SelectAll(object objName)
        {
            return Db.getDataTable(Db.SelectSql(objName));
        }
        public DataTable SelectAll(object objName, string orderByClause, bool isDesc)
        {
            return Db.getDataTable(Db.SelectSql(objName, orderByClause, isDesc));
        }
        public DataTable SelectLastFiveRecords(object objName, string id)
        {
            return Db.getDataTable(Db.SelectLastFiveRecords(objName, id));
        }
        public DataTable SelectCount(object objName, string id)
        {
            return Db.getDataTable(Db.SelectCount(objName, id));
        }
        public virtual DataTable SelectAllWithCondition(string objName, string whereClause)
        {
            return Db.getDataTable(Db.SelectSql(objName, whereClause));
        }
        public virtual DataTable GetPremiumList(long contributionId)
        {
            return Db.getDataTable(Db.GetPremiumList(contributionId));
        }
        public virtual DataTable GetPremiumList()
        {
            return Db.getDataTable(Db.GetPremiumList());
        }
        public virtual DataTable GetPolicyNumberList(long companyKeyId, long branchid, string  memberId, string lastName)
        {
            return Db.getDataTable(Db.GetPolicyNumberList(companyKeyId, branchid, memberId,lastName));
        }
        public virtual DataTable GetMemberListByName(long companyKeyId, long branchid, string lastname)
        {
            return Db.getDataTable(Db.GetMemberListByName(companyKeyId, branchid, lastname));
        }
        public virtual DataTable GetPolicyListSearch(long companyKeyId, long branchid, string lastname, string policyNumber)
        {
            return Db.getDataTable(Db.GetPolicyListSearch(companyKeyId, branchid, lastname, policyNumber));
        }

        public virtual DataTable GetMemberListSearch(long companyKeyId, long branchid, string lastname, string idNumber)
        {
            return Db.getDataTable(Db.GetMemberListSearch(companyKeyId, branchid, lastname, idNumber));
        }

        public virtual DataTable GetAllPolicyNumberList(long companyKeyId, long branchid)
        {
            return Db.getDataTable(Db.GetAllPolicyNumberList(companyKeyId, branchid));
        }
        public virtual DataTable GetClaimCostList(int claimNumber)
        {
            return Db.getDataTable(Db.GetClaimCostList(claimNumber));
        }
        public virtual DataTable GetAllClaimCostList()
        {
            return Db.getDataTable(Db.GetAllClaimCostList());
        }
        public virtual void GenerateCustomPaging(GridView gvList)
        {
           Db.GenerateCustomPaging(gvList);
        }

        public virtual bool IsTableExists(string tableName)
        {
            DataTable dtTableCount = new DataTable();
            int tableCount = 0;
            bool status = false;

            dtTableCount = Db.getDataTable(Db.IsTableExists(tableName));

            if (dtTableCount != null)
            {
                if (dtTableCount.TableName != null)
                {
                    if (dtTableCount.Rows.Count > 0)
                    {
                        tableCount = int.Parse(dtTableCount.Rows[0]["count"].ToString());
                    }
                }
            }

            return status = tableCount == 1 ? true : false;
        }

        public virtual bool IsValueExists(string fieldName, string values, long IDValue)
        {
            DataTable dtValueCount = new DataTable();
            int valueCount = 0;
            bool status = false;

            dtValueCount = Db.getDataTable(Db.IsValueExists(this.TableName,TableIDColumnName, fieldName, values, IDValue));

            if (dtValueCount != null)
            {
                if (dtValueCount.TableName != null)
                {
                    if (dtValueCount.Rows.Count > 0)
                    {
                        valueCount = int.Parse(dtValueCount.Rows[0]["count"].ToString());
                    }
                }
            }
            return status = valueCount >= 1 ? false : true;
        }
        public virtual bool IsChildValueExists(string fieldName, long values)
        {
            DataTable dtValueCount = new DataTable();
            int valueCount = 0;
            bool status = false;

            dtValueCount = Db.getDataTable(Db.IsChildValueExists(this.TableName, fieldName, values));

            if (dtValueCount != null)
            {
                if (dtValueCount.TableName != null)
                {
                    if (dtValueCount.Rows.Count > 0)
                    {
                        valueCount = int.Parse(dtValueCount.Rows[0]["count"].ToString());
                    }
                }
            }
            return status = valueCount >= 1 ? false : true;
        }
        public virtual bool IsBankNameExists(string tableName, string fieldName, string values, long bankkeyId)
        {
            DataTable dtValueCount = new DataTable();
            int valueCount = 0;
            bool status = false;

            dtValueCount = Db.getDataTable(Db.IsBankNameExists(tableName, fieldName, values, bankkeyId));

            if (dtValueCount != null)
            {
                if (dtValueCount.TableName != null)
                {
                    if (dtValueCount.Rows.Count > 0)
                    {
                        valueCount = int.Parse(dtValueCount.Rows[0]["count"].ToString());
                    }
                }
            }
            return status = valueCount >= 1 ? false : true;
        }

        public virtual bool IsPayeeAddressExists(string tableName, string fieldName, string values, long payeeId, string address)
        {
            DataTable dtValueCount = new DataTable();
            int valueCount = 0;
            bool status = false;

            dtValueCount = Db.getDataTable(Db.IsPayeeAddressExists(tableName, fieldName, values, payeeId,address));

            if (dtValueCount != null)
            {
                if (dtValueCount.TableName != null)
                {
                    if (dtValueCount.Rows.Count > 0)
                    {
                        valueCount = int.Parse(dtValueCount.Rows[0]["count"].ToString());
                    }
                }
            }
            return status = valueCount >= 1 ? false : true;
        }
        public virtual bool IsUniqueRecord(string tableName, string keyField, long keyFieldValue, string fieldName1, string fieldName2, string value1, string value2)
        {
            DataTable dtValueCount = new DataTable();
            int valueCount = 0;
            bool status = false;

            dtValueCount = Db.getDataTable(Db.IsUniqueRecord(tableName,keyField,keyFieldValue, fieldName1, fieldName2, value1, value2));

            if (dtValueCount != null)
            {
                if (dtValueCount.TableName != null)
                {
                    if (dtValueCount.Rows.Count > 0)
                    {
                        valueCount = int.Parse(dtValueCount.Rows[0]["count"].ToString());
                    }
                }
            }
            return status = valueCount >= 1 ? false : true;
        }

        public virtual void ExecuteSP(string sqlObjects)
        {
            Db.ExecuteSP(sqlObjects);
        }
        public virtual void ExecSqlNonQuery(string sql)
        {
            Db.ExecSqlNonQuery(sql);
        }
        public virtual DataTable GetGLCodeList()
        {
            return Db.getDataTable(Db.GetGLCodeList());
        }
        
        public virtual DataTable GetPayeeDetails(string payeeName)
        {
            return Db.getDataTable(Db.GetPayeeDetails(payeeName));
        }
        public virtual DataTable GetSchemeList()
        {
            return Db.getDataTable(Db.GetSchemeList());
        }
        public long SelectNextIdValue(object objName, string ColumnName)
        {
            DataTable dtID = new DataTable();
            long nextValue = 0;

            dtID = Db.getDataTable(Db.SelectSql(objName, ColumnName));

            if (dtID != null)
            {
                if (dtID.TableName != null)
                {
                    nextValue = int.Parse(dtID.Rows[0][ColumnName].ToString());
                }
            }
            return nextValue;
        }

        public long SelectNextIdValue()
        {
            DataTable dtID = new DataTable();
            long nextValue = 0;

            dtID = Db.getDataTable(Db.SelectSqlMAX(TableName, TableIDColumnName));

            if (dtID != null)
            {
                if (dtID.TableName != null)
                {
                    nextValue = long.Parse(dtID.Rows[0][TableIDColumnName].ToString());
                }
            }
            return nextValue;
        }
        
        public long  SelectSequenceValue(string SequenceName)
        {
            DataTable dtID = new DataTable();
            long  nextValue = 0;

            dtID = Db.getDataTable(Db.SelectSequenceValue(SequenceName));

            if (dtID != null)
            {
                if (dtID.TableName != null)
                {
                    nextValue = long.Parse(dtID.Rows[0]["NextVal"].ToString());
                }
            }
            return nextValue;
        }
        public DataSet SelectQuery(object objName)
        {
            return Db.getDataSet(Db.SelectSql(objName));
        }
        public DataSet SelectQuery(string sql)
        {
            return Db.getDataSet(sql);
        }
        //Changed on 12/09/2011 arasu
        public DataSet UpdateQueryLog(string sql)
        {
            return Db.UpdateDataSet(sql);
        }
        //Changed on 12/09/2011 arasu
        public DataSet UpdateQueryLogLock(string sql)
        {
            return Db.UpdateDataSet(sql);
        }
        
        public DataSet GetMenuList(string groupId, string userId)
        {
            return Db.getDataSet(Db.GetMenuList(groupId,userId));
        }
        public DataSet GetSubMenuList(string groupId, string userId, string moduleId)
        {
            return Db.getDataSet(Db.GetSubMenuList(groupId, userId, moduleId));
        }
       
        public DataTable SelectAll(string OtherIDColumnName, int OtherIDColumnValue)
        {
            return Db.getDataTable(Db.SelectSql(ViewName,
                null,
                string.Concat(OtherIDColumnName, Db.EqualToOperator, OtherIDColumnValue),
                this.OrderByFields,
                false));
        }
        public DataTable SelectAll(string OtherColumnName, string OtherColumnValue)
        {
            return Db.getDataTable(Db.SelectSql(ViewName,
                null,
                string.Concat(OtherColumnName, Db.EqualToOperator, Db.TextQuoteOperator, OtherColumnValue, Db.TextQuoteOperator),
                this.OrderByFields,
                false));
        }
        public DataTable SelectAll(string OtherColumnName, object OtherColumnValue, DbType dbType)
        {
            IDbDataParameter prm = DbEngine.SetupParameter(OtherColumnName, OtherColumnValue, dbType);
            string sql = Db.SelectSql(ViewName,
                null,
                string.Concat(OtherColumnName, Db.EqualToOperator, prm.ParameterName),
                this.OrderByFields,
                false);
            IDbCommand cmd = DbEngine.SetupCommand(sql, new IDbDataParameter[] { prm });
            return Db.getDataTable(cmd);
        }
        public virtual object SelectByID(int ID)
        {
            object obj = Activator.CreateInstance(this.GetType());
            getObj(ref obj, Db.getDataTable(Db.SelectSql(ViewName, string.Concat(TableIDColumnName, Db.EqualToOperator, ID))));
            return obj;
        }
        public virtual object SelectOne()
        {
            object obj = Activator.CreateInstance(this.GetType());
            getObj(ref obj, Db.getDataTable(Db.SelectSql(ViewName)));
            return obj;
        }

        public virtual object SelectByID(long ID)
        {
            object obj = Activator.CreateInstance(this.GetType());
            getObj(ref obj, Db.getDataTable(Db.SelectSql(ViewName, string.Concat(TableIDColumnName, Db.EqualToOperator, ID))));
            return obj;
        }
        public virtual object SelectBySearchCondition(string searchCondition)
        {
            object obj = Activator.CreateInstance(this.GetType());
            getObj(ref obj, Db.getDataTable(Db.SelectSql(ViewName, searchCondition)));
            return obj;
        }

        protected virtual void SetByID(int ID)
        {
            object obj = this;
            getObj(ref obj, Db.getDataTable(Db.SelectSql(ViewName, string.Concat(TableIDColumnName, Db.EqualToOperator, ID))));
        }

        protected virtual void SetByDR(DataRow dr)
        {
            object obj = this;
            getObj(ref obj, dr);
        }
        public static bool IsNumeric(string value)
        {
            Int64 int64Val;
            bool result = Int64.TryParse(value, NumberStyles.Integer, null, out int64Val);
            Decimal decValue;
            bool result1 = Decimal.TryParse(value, NumberStyles.Float, null, out decValue);
            if (result || result1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public static bool IsDate(string value)
        {
            try
            {
                DateTime datetimeVal;
                CultureInfo ci = new CultureInfo("en-GB");
                bool result = DateTime.TryParse(value, ci, DateTimeStyles.AssumeLocal, out datetimeVal);

                if (result)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                return false;
            }
        }
        public DataSet SearchData(DataSet sourceData, string sourceColumn, string searchValue)
        {
            try
            {
                DataTable dtSource = new DataTable();
                DataSet dsSource = new DataSet();
                if (sourceData != null && searchValue != "")
                {
                    if (sourceData.Tables.Count > 0)
                    {
                        if (sourceData.Tables[0].Rows.Count > 0)
                        {
                            if (sourceData.Tables[0].Columns[sourceColumn] != null)
                            {
                                if (sourceData.Tables[0].Columns[sourceColumn].DataType == typeof(int))
                                {
                                    if (!IsNumeric(searchValue))
                                    {
                                        return null;
                                    }
                                    else
                                    {
                                        sourceData.Tables[0].DefaultView.RowFilter = sourceColumn + "=" + searchValue;
                                    }
                                }
                                else if (sourceData.Tables[0].Columns[sourceColumn].DataType == typeof(string))
                                {
                                    sourceData.Tables[0].DefaultView.RowFilter = sourceColumn + " like '" + searchValue + "%'";
                                }
                                else if (sourceData.Tables[0].Columns[sourceColumn].DataType == typeof(decimal))
                                {
                                    if (!IsNumeric(searchValue))
                                    {
                                        return null;
                                    }
                                    else
                                    {
                                        sourceData.Tables[0].DefaultView.RowFilter = sourceColumn + "=" + searchValue;
                                    }
                                }
                                else if (sourceData.Tables[0].Columns[sourceColumn].DataType == typeof(DateTime))
                                {
                                    if (!IsDate(searchValue))
                                    {
                                        return null;
                                    }
                                    DateTime dtDate;
                                    bool check = DateTime.TryParse(searchValue, System.Globalization.CultureInfo.GetCultureInfo("fr-FR"), System.Globalization.DateTimeStyles.None, out dtDate);
                                    if (!check)
                                        return null;
                                    sourceData.Tables[0].DefaultView.RowFilter = sourceColumn + " = '" + dtDate.ToShortDateString() + "'";
                                }
                            }
                            dtSource = sourceData.Tables[0].DefaultView.ToTable();
                            dsSource.Tables.Add(dtSource);
                            return dsSource;

                        }
                        else
                        {
                            return null;
                        }
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //		protected virtual void SetupSiblingObjects()
        //		{
        //			foreach (BusinessObjectBase bob in SiblingObjects) LoadFromPrimarySibling(bob);
        //		}

        protected virtual void SetupChildObjects()
        {
            //This procedure must be overriden in derived objects to 
            //setup their child objects
        }


        private AudittedActionCollection _PreUpdateActionAAs = new AudittedActionCollection();
        protected AudittedActionCollection PreUpdateActionAAs { get { return _PreUpdateActionAAs; } set { _PreUpdateActionAAs = value; } }
        private AudittedActionCollection _PostUpdateActionAAs = new AudittedActionCollection();
        protected AudittedActionCollection PostUpdateActionAAs { get { return _PostUpdateActionAAs; } set { _PostUpdateActionAAs = value; } }

        private AudittedActionCollection _PreDeleteActionAAs = new AudittedActionCollection();
        protected AudittedActionCollection PreDeleteActionAAs { get { return _PreDeleteActionAAs; } set { _PreDeleteActionAAs = value; } }
        private AudittedActionCollection _PostDeleteActionAAs = new AudittedActionCollection();
        protected AudittedActionCollection PostDeleteActionAAs { get { return _PostDeleteActionAAs; } set { _PostDeleteActionAAs = value; } }

        protected virtual void PreAddAction(long auditUserID) { }
        protected virtual void PreUpdateAction(long auditUserID) { }
        protected virtual void PostUpdateAction(long auditUserID) { }
        protected virtual void PreDeleteAction(long auditUserID) { }
        protected virtual void PostDeleteAction(long auditUserID) { }

        #endregion

        #region Helper Funtions
        protected void getObj(ref object obj, DataTable dt)
        {
            if (dt.Rows.Count == 0) getObj(ref obj, dt.NewRow());
            else getObj(ref obj, dt.Rows[0]);
        }

        protected void getObj(ref object obj, DataRow dr)
        {
            //'
            //'*******************************************************************************
            //'THIS IS THE CENTRAL NERVOUS SYSTEM OF THE LOADING OF DATA FROM THE DATABASE
            //'INTO THE INFO OBJECT PROPERTIES
            //'
            //'EITHER USE REFLECTION (SLOWER, LESS CODE, MORE GENERIC, MORE MAINTAINABLE)
            //'
            //'OR
            //'
            //'USE CODE GENERATED FUNCTIONS TO MANUALLY RETREIVE THGE DATA FROM THE DATABASE
            //'AND POPULATE THE INFO OBJECT PROPERTIES (FASTER, MORE CODE, MORE MAINTENANCE REQUIRED)
            //'*******************************************************************************
            //'

            //object obj = Activator.CreateInstance(this.GetType());
            SqlFieldAttributeCollection sqlFields = Utils.getSqlFieldAttributes(this.GetType());

            try
            {
                if (dr != null)
                    foreach (SqlFieldAttribute sqlField in sqlFields)
                        sqlField.setPropertyValue(ref obj, dr);
                //if (!sqlField.IsAuditProperty)
                //	sqlField.setPropertyValue(ref obj, dr);
            }
            catch (Exception e)
            { throw e; }

            //return obj;
        }
        #endregion
    }
}
