using System;
using System.Web.Mail;
using System.Collections;
using System.IO;
using System.Net;

namespace VMVServices.Services.Business
{
	/// <summary>
	/// Summary description for Email.
	/// </summary>
	public class Email
	{
		public class HtmlTemplateTransform
		{
			public readonly string PlaceHolder = string.Empty;
			public readonly string PlaceHolderValue = string.Empty;
			public HtmlTemplateTransform(string placeHolder, string placeHolderValue)
			{
				PlaceHolder = placeHolder;
				PlaceHolderValue = placeHolderValue;
			}
			public void TransformHtml(ref string html)
			{
				html = html.Replace(PlaceHolder, PlaceHolderValue);
			}
		}
		public class HtmlTemplateTransformCollection : CollectionBase
		{
			public HtmlTemplateTransformCollection(){}

			public virtual HtmlTemplateTransform this[int Index] { get { return (HtmlTemplateTransform)this.List[Index]; } }

			public int Add(HtmlTemplateTransform item) { return this.List.Add(item); }
		}


		public static bool Send(string to, string subject, string message)
		{
			return Send(to, subject, message, true);
		}
		public static bool Send(string to, string subject, string message, bool sendAsHtml)
		{
			return Send(to, string.Empty, string.Empty, subject, message, sendAsHtml);
		}
		public static bool Send(string to, string cc, string bcc, string subject, string message, bool sendAsHtml)
		{
            return Send(to, cc, bcc, subject, message, sendAsHtml, VMVServices.Settings.Instance.SmtpServer, VMVServices.Settings.Instance.FromEmailAddress);
		}
		public static bool Send(string to, string cc, string bcc, string subject, string message, bool sendAsHtml, string smtpServer, string from)
		{

			//
			//To send attachements, use the following code:
			//
			// - MailMessage.Attachements.Add(new MailAttachements(@"c:\mailattachments.txt"));
            //

			MailMessage msg = new MailMessage();
			try
			{
				SmtpMail.SmtpServer = smtpServer;

				msg.Priority = MailPriority.Normal;
				msg.From = from;
				msg.To = to;
				msg.Cc = cc;
				msg.Bcc = bcc;
				msg.Subject = subject;
				msg.Body = message;

				//used when smtp requires authentication
				//msg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", "1"); //'basic authentication
				//msg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", "allen01"); //'set your username here
				//msg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", "password"); //'set your password here

				if (sendAsHtml)
					msg.BodyFormat = MailFormat.Html;
				else
					msg.BodyFormat = MailFormat.Text;

				SmtpMail.Send(msg);

				return true;
			}
			catch(Exception e)
			{
				string errMessage = string.Concat("Error Sending mail. Mail details are:", Environment.NewLine, 
													"SMTP Server: ", SmtpMail.SmtpServer, Environment.NewLine, 
													"From: ", msg.From, Environment.NewLine,  
													"To: ", msg.To, Environment.NewLine, 
													"Cc: ", msg.Cc, Environment.NewLine, 
													"Bcc: ",  msg.Bcc, Environment.NewLine, 
													"Subject: ", msg.Subject, Environment.NewLine);
                VMVServices.Services.ErrorManagement.Publish(e, -1, true);
                VMVServices.Services.ErrorManagement.Publish(new Exception(errMessage, e), -1, true);
				return false;
			}
		}

		public static string TransformHtmlTemplate(string templatePath, HtmlTemplateTransformCollection transforms)
		{
			string html = string.Empty;

			if (!File.Exists(templatePath))
				throw new Exception(string.Concat("File: ", templatePath, " not found."));

			StreamReader sr = new StreamReader(templatePath);
			try
				{ html = sr.ReadToEnd(); }
			finally
				{ sr.Close(); }

			return TransformHtml(html, transforms);
		}
		public static string TransformHtml(string html, HtmlTemplateTransformCollection transforms)
		{
			foreach (HtmlTemplateTransform transform in transforms)
				transform.TransformHtml(ref html);

			return html;
		}
		public static string GetHtmlFromUrl(string url)
		{
			//screen scraping, this is what it's called

			HttpWebRequest _httpRequest;
			HttpWebResponse _httpResponse;
			string _responseHtmlString;

			try { _httpRequest = (HttpWebRequest)WebRequest.Create(url); }
			catch (Exception e){throw new Exception(string.Concat("Problem connecting to Url: ", url, Environment.NewLine, Environment.NewLine, e.Message));}

			//setup the HttpRequesr object properties in prep for the HTTP Post
			_httpRequest.Method = "POST";
			//_httpRequest.ContentType = "application/x-www-form-urlencoded";

			//Get the Http Response to the Posting of our Http Request
			try
			{
				_httpResponse = (HttpWebResponse)_httpRequest.GetResponse();
			}
			catch(Exception e)
			{
				throw new Exception(string.Concat("Unable to retreive Html from specified Url due to the following problem whilst waiting for a response from the Url: ", e.Message));
			}

			StreamReader sr = new StreamReader(_httpResponse.GetResponseStream());
			try
			{ _responseHtmlString = sr.ReadToEnd(); }
			catch (Exception e) {throw new Exception(string.Concat("Problem reading response to request to Url: ", Environment.NewLine, Environment.NewLine, url, Environment.NewLine, Environment.NewLine, e.Message));}
			finally{sr.Close();}
			
			_httpResponse.Close();

			return _responseHtmlString;

		}
		public static string GetHtmlFromFile(string filePath)
		{
			using (StreamReader sr = File.OpenText(filePath))
			{
				try { return sr.ReadToEnd(); }
				finally { sr.Close(); }
			}
		}

		public static string GetFilePathFromUrl(string url)
		{
			System.Web.HttpContext context = System.Web.HttpContext.Current;
			return context.Server.MapPath(url);
		}

	}
}
