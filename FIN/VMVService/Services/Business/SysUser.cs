using System;
using System.Data;
using VMVServices.Services.Data;
using VMVServices.Services.Business;

namespace VMVServices.Services.Business
{
    [SqlTable(TableName = "AON_USER_MST")]
	public class SysUser : BusinessObjectBase
	{
		public SysUser(){}
		public SysUser(int ID){this.SetByID(ID);}
		public SysUser(DataRow dr){this.SetByDR(dr);}

		static SysUser _Instance;
		public static SysUser Instance { get { if (_Instance == null) _Instance = new SysUser(); return _Instance; } }

		public override string ObjDescription
		{
			get
			{
				return string.Concat(this.Fullname, ", Username: ", this.Username, ", Email: ", this.Email);
			}
		}
		public override string ObjTypeName
		{
			get
			{
				return "System User";
			}
		}

		protected override void SetupChildObjectsForDelete()
		{
			//calling the child objects properties will result in the private variables being instantiated to hold
			//the actual data if it exists, 
			//basically we a re forcing a load of the property which is using the lazy load pattern

			//No child objects, so do nothing
		}

        protected override void PreAddAction(long auditUserID)
		{
            this.SysDateLastUpdated = DateTime.Now.ToString(VMVServices.Settings.Instance.DefaultDateTimeFormatString);
			base.PreAddAction (auditUserID);
		}

        protected override void PreUpdateAction(long auditUserID)
		{
            this.SysDateLastUpdated = DateTime.Now.ToString(VMVServices.Settings.Instance.DefaultDateTimeFormatString);
			base.PreUpdateAction (auditUserID);
		}


		#region Table Field Properties
		//string _DefaultLandingPageUrl = "";
		//[SqlField()] public string DefaultLandingPageUrl {get{return _DefaultLandingPageUrl;}set{_DefaultLandingPageUrl = value;}}

		string _Email = "";
		[SqlField()] public string Email { get{ return _Email; } set{ _Email = value; } }

        string _Fullname = "";
        [SqlField()]
        public string Fullname { get { return _Fullname; } set { _Fullname = value; } }

		byte[] _Password = new byte[]{};
		[SqlField()] public byte[] Password { get{ return _Password; } set{ _Password = value; } }

		[SqlField(PrimaryKeyField=true)] 
        public long SysUserID { get{ return this.ObjID; } set{ this.ObjID = value; } }

		string _Username = "";
		[SqlField()] public string Username { get{ return _Username; } set{ _Username = value; } }

		string _Roles = "";
		[SqlField()] public string Roles { get{ return _Roles; } set{ _Roles = value; } }
              

		string _SysDateCreated = "";
		[SqlField(IsAuditProperty=true,DbType=System.Data.DbType.Date)]public virtual string SysDateCreated { get{return _SysDateCreated;} set{_SysDateCreated = value;} }

		string _SysDateLastUpdated = DateTime.Now.ToString();
		[SqlField(DbType=System.Data.DbType.Date)]public virtual string SysDateLastUpdated { get{return _SysDateLastUpdated;} set{_SysDateLastUpdated = value;} }

		[SqlField()]public virtual long SysAuditUserID { get{return this.auditUserID;} set{this.auditUserID = value;} }

		#endregion

		#region Object Relationship Properties
		//Object Relationship Properties go here

		#endregion

		#region Custom Business Logic
		public bool Authenticate(string username, string password)
		{
			this.SetByUsernamePassword(username, password);
			return (this.SysUserID != 0);
		}
		protected void SetByUsernamePassword(string username, string password)
		{
            Byte[] encryptedPassword = System.Text.Encoding.UTF8.GetBytes(VMVServices.Helpers.DataProtection.RC4Encrypt(password, "secretpassword", string.Empty));
			object obj = this;
			IDbCommand cmd = DbEngine.CreateCommand();
			cmd.CommandText = Db.SelectSql(ViewName, 
				string.Concat(DbConsts.UsernameColumnName, Db.EqualToOperator, Db.ParameterPrefix, DbConsts.UsernameColumnName, 
				Db.AndOperator, DbConsts.PasswordColumnName, Db.EqualToOperator, Db.ParameterPrefix, DbConsts.PasswordColumnName));
			
			IDbDataParameter usernamePrm = DbEngine.CreateParameter();
			usernamePrm.ParameterName = string.Concat(Db.ParameterPrefix, DbConsts.UsernameColumnName);
			usernamePrm.Value = username;
			usernamePrm.DbType = DbType.String;

			IDbDataParameter passwordPrm = DbEngine.CreateParameter();
			passwordPrm.ParameterName = string.Concat(Db.ParameterPrefix, DbConsts.PasswordColumnName);
			passwordPrm.Value = encryptedPassword;
			passwordPrm.DbType = DbType.Binary;

			cmd.Parameters.Add(usernamePrm);
			cmd.Parameters.Add(passwordPrm);

			getObj(ref obj, Db.getDataTable(cmd));
		}
		public string GetPasswordText
		{
			get
			{
                return VMVServices.Helpers.DataProtection.RC4Decrypt(System.Text.Encoding.UTF8.GetString(this.Password), "secretpassword", string.Empty);
			}
		}

		public void SetPasswordText(string strPassword)
		{
            Byte[] bytePassword = System.Text.Encoding.UTF8.GetBytes(VMVServices.Helpers.DataProtection.RC4Encrypt(strPassword, "secretpassword", string.Empty));
			this.Password = bytePassword;
		}

		public bool UsernameAlreadyInUse(string username)
		{
			IDbCommand cmd = DbEngine.CreateCommand();
			cmd.CommandText = Db.SelectSql(ViewName, 
				string.Concat(DbConsts.UsernameColumnName, Db.EqualToOperator, Db.ParameterPrefix, DbConsts.UsernameColumnName));
			
			IDbDataParameter usernamePrm = DbEngine.CreateParameter();
			usernamePrm.ParameterName = string.Concat(Db.ParameterPrefix, DbConsts.UsernameColumnName);
			usernamePrm.Value = username;
			usernamePrm.DbType = DbType.String;

			cmd.Parameters.Add(usernamePrm);

			DataTable dt = Db.getDataTable(cmd);

			return (dt.Rows.Count != 0);

		}


		#endregion
        public void CreateData(object obj)
        {
            VMVServices.Services.Data.DbEngine.Instance.StartTransaction();
            VMVServices.Services.Data.DbEngine.Instance.ExecSqlNonQuery(VMVServices.Services.Data.DbEngine.getInsertCmd(obj, true));
        }
        public void ModifyData(object obj)
        {
            VMVServices.Services.Data.DbEngine.Instance.StartTransaction();
            VMVServices.Services.Data.DbEngine.Instance.ExecSqlNonQuery(VMVServices.Services.Data.DbEngine.getUpdateCmd(obj));
        }
        public void DeleteData(object obj)
        {
            VMVServices.Services.Data.DbEngine.Instance.StartTransaction();
            VMVServices.Services.Data.DbEngine.Instance.ExecSqlNonQuery(VMVServices.Services.Data.DbEngine.getDeleteCmd(obj));
        }
	}
}
