using System;
using System.Data;
using VMVServices.Services.Data;

namespace VMVServices.Services.Business
{
	/// <summary>
	/// Summary description for Reporting.
	/// </summary>
	public class Reporting
	{
		//IDataReader functions
		public static DataTable Pivot(IDataReader dataValues, string[] keyColumns, string pivotNameColumn, string pivotValueColumn, string[] nonPivotColumns)
		{

			//Argument explanantion
			//
			// - dataValues			--> this is any open DataReader object, ready to be transformed and pivoted into a DataTable.  As mentioned, it should be fully grouped, aggregated, sorted and ready to go.  
			// - keyColumns[]		--> These are the columns in the DataReader which serves to identify each row.  In the previous example, this would be CustomerID.  Your DataReader's recordset should be grouped and sorted by this column as well. 
			// - pivotNameColumn	--> This is the column in the DataReader that contains the values you'd like to transform from rows into columns.   In the example, this would be ProductName. 
			// - pivotValueColumn	--> This is the column that in the DataReader that contains the values to pivot into the appropriate columns.  For our example, it would be Qty, which has been defined in the SELECT statement as SUM(Qty).
			// - nonPivotColumns[]	--> This is a string array of columns that are oin the datareader bu tthat should not be included in the pivot ooperation
			//

			DataTable tmp = new DataTable();
			DataRow r;
			string[] LastKeys = new string[keyColumns.Length];
			int i, x, y, pValIndex, pNameIndex;
			string s;
			bool FirstRow = true;

			// Add non-pivot columns to the data table:

			pValIndex = dataValues.GetOrdinal(pivotValueColumn);
			pNameIndex = dataValues.GetOrdinal(pivotNameColumn);

			//setup dummy first values for keyColumn values
			for (i=0; i < keyColumns.Length; i++)
				LastKeys[i] = "//dummy//";

			//then setup the non pivot columns for the datatable
			for (i=0; i <= dataValues.FieldCount - 1; i++)//loop through all the fields in the IDataReader
				for (x = 0; x < nonPivotColumns.Length; x++)//then loop through the non pivot columns
					if (dataValues.GetName(i) == nonPivotColumns[x] && i != pValIndex &&  i != pNameIndex )//if theres a match and they are not the pivot name column or the pivot value column
						tmp.Columns.Add(dataValues.GetName(i),dataValues.GetFieldType(i));//then add the column to the datatable

			r = tmp.NewRow();

			// now, fill up the table with the data:
			while (dataValues.Read())
			{
				// see if we need to start a new row
				for (x = 0; x < keyColumns.Length; x++)
					if (dataValues[keyColumns[x]].ToString() != LastKeys[x])
					{
						// if this isn't the very first row, we need to add the last one to the table
						if (!FirstRow) tmp.Rows.Add(r);
						r = tmp.NewRow();
						FirstRow = false;

						// Add all non-pivot column values to the new row:
						for (i = 0; i < nonPivotColumns.Length; i++)
							r[nonPivotColumns[i]] = dataValues[nonPivotColumns[i]];

						//update the LastKeys
						for (y = 0; y < keyColumns.Length; y++)
							LastKeys[y] = dataValues[keyColumns[y]].ToString();
					}

				// assign the pivot values to the proper column; add new columns if needed:
				s = dataValues[pNameIndex].ToString();
				if (!tmp.Columns.Contains(s))
					tmp.Columns.Add(s, dataValues.GetFieldType(pValIndex));
				r[s] = dataValues[pValIndex];
			}

			// add that final row to the datatable:
			if (!FirstRow) tmp.Rows.Add(r);

			// Close the DataReader
			dataValues.Close();                  

			// and that's it!
			return tmp;
		}

		public static DataTable Pivot(IDataReader dataValues, string keyColumn, string pivotNameColumn, string pivotValueColumn)
		{

			//Argument explanantion
			//
			// - dataValues			--> this is any open DataReader object, ready to be transformed and pivoted into a DataTable.  As mentioned, it should be fully grouped, aggregated, sorted and ready to go.  
			// - keyColumn			--> This is the column in the DataReader which serves to identify each row.  In the previous example, this would be CustomerID.  Your DataReader's recordset should be grouped and sorted by this column as well. 
			// - pivotNameColumn	--> This is the column in the DataReader that contains the values you'd like to transform from rows into columns.   In the example, this would be ProductName. 
			// - pivotValueColumn	--> This is the column that in the DataReader that contains the values to pivot into the appropriate columns.  For our example, it would be Qty, which has been defined in the SELECT statement as SUM(Qty).
			//

			DataTable tmp = new DataTable();
			DataRow r;
			string LastKey = "//dummy//";
			int i, pValIndex, pNameIndex;
			string s;
			bool FirstRow = true;

			// Add non-pivot columns to the data table:

			pValIndex = dataValues.GetOrdinal(pivotValueColumn);
			pNameIndex = dataValues.GetOrdinal(pivotNameColumn);

			for (i=0; i <= dataValues.FieldCount -1; i++)
				if (i != pValIndex &&  i != pNameIndex )
					tmp.Columns.Add(dataValues.GetName(i),dataValues.GetFieldType(i));

			r = tmp.NewRow();

			// now, fill up the table with the data:
			while (dataValues.Read())
			{
				// see if we need to start a new row
				if (dataValues[keyColumn].ToString() != LastKey)
				{
					// if this isn't the very first row, we need to add the last one to the table
					if (!FirstRow)
						tmp.Rows.Add(r);
					r = tmp.NewRow();
					FirstRow = false;
					// Add all non-pivot column values to the new row:
					for (i=0; i<= dataValues.FieldCount-3;i++)  
						r[i] = dataValues[tmp.Columns[i].ColumnName];
					LastKey = dataValues[keyColumn].ToString();
				}
				// assign the pivot values to the proper column; add new columns if needed:
				s = dataValues[pNameIndex].ToString();
				if (!tmp.Columns.Contains(s))
					tmp.Columns.Add(s, dataValues.GetFieldType(pValIndex));
				r[s] = dataValues[pValIndex];
			}

			// add that final row to the datatable:
			if (!FirstRow) tmp.Rows.Add(r);

			// Close the DataReader
			dataValues.Close();                  

			// and that's it!
			return tmp;
		}

		public static DataTable Pivot(string sql, string keyColumn, string pivotNameColumn, string pivotValueColumn)
		{
			return Pivot(DbEngine.getDataReader(sql), keyColumn, pivotNameColumn, pivotValueColumn);
		}
		public static DataTable Pivot(IDbCommand cmd, string keyColumn, string pivotNameColumn, string pivotValueColumn)
		{
			return Pivot(DbEngine.getDataReader(cmd), keyColumn, pivotNameColumn, pivotValueColumn);
		}


	}
}
