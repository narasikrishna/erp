using System;
using System.Data;
using VMVServices.Services.Data;
using VMVServices.Services.Business;

namespace VMVServices.Services.Business
{
	[SqlTable(TableName="tblSysError")]
	public class SysError : Business.BusinessObjectBase
	{
		public SysError(){}
		public SysError(int ID){this.SetByID(ID);}
		public SysError(DataRow dr){this.SetByDR(dr);}

		public override string ObjDescription
		{
			get
			{
				return this.ErrorMessage;
			}
		}
		public override string ObjTypeName
		{
			get
			{
				return "System Error";
			}
		}

		protected override void SetupChildObjectsForDelete()
		{
			//do nothing vbecause SysError does not habve childobjects
		}


		static SysError _Instance;
		public static SysError Instance { get { if (_Instance == null) _Instance = new SysError(); return _Instance; } }

        protected override void PreAddAction(long auditUserID)
		{
            this.SysDateLastUpdated = DateTime.Now.ToString(VMVServices.Settings.Instance.DefaultDateTimeFormatString);
			base.PreAddAction (auditUserID);
		}

        protected override void PreUpdateAction(long auditUserID)
		{
            this.SysDateLastUpdated = DateTime.Now.ToString(VMVServices.Settings.Instance.DefaultDateTimeFormatString);
			base.PreUpdateAction (auditUserID);
		}

		#region Table Field Properties
		string _ErrorMessage = "";
		[SqlField()] public string ErrorMessage { get{ return _ErrorMessage; } set{ _ErrorMessage = value; } }

		string _ErrorStackTrace = "";
		[SqlField()] public string ErrorStackTrace { get{ return _ErrorStackTrace; } set{ _ErrorStackTrace = value; } }

		[SqlField(PrimaryKeyField=true)] public long SysErrorID { get{ return this.ObjID; } set{ this.ObjID = value; } }

		string _SysDateCreated = "";
		[SqlField(IsAuditProperty=true,DbType=System.Data.DbType.Date)]public virtual string SysDateCreated { get{return _SysDateCreated;} set{_SysDateCreated = value;} }

		string _SysDateLastUpdated = DateTime.Now.ToString();
		[SqlField(DbType=System.Data.DbType.Date)]public virtual string SysDateLastUpdated { get{return _SysDateLastUpdated;} set{_SysDateLastUpdated = value;} }

		[SqlField()]public virtual long SysAuditUserID { get{return this.auditUserID;} set{this.auditUserID = value;} }

		#endregion

		#region Object Relationship Properties
		//Object Relationship Properties go here
		#endregion

		#region Custom Business Logic
		//Custom Business Logic goes here
		public DataTable SelectAllByDate(DateTime date)
		{
			IDbDataParameter startDate = DbEngine.GetStartDateParameter(date);
			IDbDataParameter endDate = DbEngine.GetEndDateParameter(date);
            string sql = Db.SelectSql(this.ViewName, string.Concat(VMVServices.Services.Data.DbConsts.SysDateCreatedColumnName, Db.GreaterThanEqualToOperator, startDate.ParameterName, Db.AndOperator, VMVServices.Services.Data.DbConsts.SysDateCreatedColumnName, Db.LessThanOperator, endDate.ParameterName));
			return Db.getDataTable(DbEngine.SetupCommand(sql, new IDbDataParameter[]{startDate, endDate}));
		}

		#endregion

	}

}
