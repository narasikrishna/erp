//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.Serialization;

namespace FIN.DAL
{
    [DataContract(IsReference = true)]
    [KnownType(typeof(GL_COMPANIES_HDR))]
    [KnownType(typeof(HR_EMPLOYEES))]
    [KnownType(typeof(HR_GRADES))]
    [KnownType(typeof(HR_JOBS))]
    [KnownType(typeof(HR_POSITIONS))]
    public partial class HR_EMP_ASSIGNMENT_DTLS: IObjectWithChangeTracker, INotifyPropertyChanged
    {
        #region Primitive Properties
    
        [DataMember]
        public Nullable<int> PK_ID
        {
            get { return _pK_ID; }
            set
            {
                if (_pK_ID != value)
                {
                    _pK_ID = value;
                    OnPropertyChanged("PK_ID");
                }
            }
        }
        private Nullable<int> _pK_ID;
    
        [DataMember]
        public Nullable<int> CHILD_ID
        {
            get { return _cHILD_ID; }
            set
            {
                if (_cHILD_ID != value)
                {
                    _cHILD_ID = value;
                    OnPropertyChanged("CHILD_ID");
                }
            }
        }
        private Nullable<int> _cHILD_ID;
    
        [DataMember]
        public string ASSIGNMENT_ID
        {
            get { return _aSSIGNMENT_ID; }
            set
            {
                if (_aSSIGNMENT_ID != value)
                {
                    if (ChangeTracker.ChangeTrackingEnabled && ChangeTracker.State != ObjectState.Added)
                    {
                        throw new InvalidOperationException("The property 'ASSIGNMENT_ID' is part of the object's key and cannot be changed. Changes to key properties can only be made when the object is not being tracked or is in the Added state.");
                    }
                    _aSSIGNMENT_ID = value;
                    OnPropertyChanged("ASSIGNMENT_ID");
                }
            }
        }
        private string _aSSIGNMENT_ID;
    
        [DataMember]
        public string ASSIGNMENT_EMP_ID
        {
            get { return _aSSIGNMENT_EMP_ID; }
            set
            {
                if (_aSSIGNMENT_EMP_ID != value)
                {
                    ChangeTracker.RecordOriginalValue("ASSIGNMENT_EMP_ID", _aSSIGNMENT_EMP_ID);
                    if (!IsDeserializing)
                    {
                        if (HR_EMPLOYEES != null && HR_EMPLOYEES.EMP_ID != value)
                        {
                            HR_EMPLOYEES = null;
                        }
                    }
                    _aSSIGNMENT_EMP_ID = value;
                    OnPropertyChanged("ASSIGNMENT_EMP_ID");
                }
            }
        }
        private string _aSSIGNMENT_EMP_ID;
    
        [DataMember]
        public System.DateTime ASSIGNMENT_FROM_DT
        {
            get { return _aSSIGNMENT_FROM_DT; }
            set
            {
                if (_aSSIGNMENT_FROM_DT != value)
                {
                    _aSSIGNMENT_FROM_DT = value;
                    OnPropertyChanged("ASSIGNMENT_FROM_DT");
                }
            }
        }
        private System.DateTime _aSSIGNMENT_FROM_DT;
    
        [DataMember]
        public Nullable<System.DateTime> ASSIGNMENT_TO_DT
        {
            get { return _aSSIGNMENT_TO_DT; }
            set
            {
                if (_aSSIGNMENT_TO_DT != value)
                {
                    _aSSIGNMENT_TO_DT = value;
                    OnPropertyChanged("ASSIGNMENT_TO_DT");
                }
            }
        }
        private Nullable<System.DateTime> _aSSIGNMENT_TO_DT;
    
        [DataMember]
        public string ASSIGNMENT_GRADE
        {
            get { return _aSSIGNMENT_GRADE; }
            set
            {
                if (_aSSIGNMENT_GRADE != value)
                {
                    ChangeTracker.RecordOriginalValue("ASSIGNMENT_GRADE", _aSSIGNMENT_GRADE);
                    if (!IsDeserializing)
                    {
                        if (HR_GRADES != null && HR_GRADES.GRADE_ID != value)
                        {
                            HR_GRADES = null;
                        }
                    }
                    _aSSIGNMENT_GRADE = value;
                    OnPropertyChanged("ASSIGNMENT_GRADE");
                }
            }
        }
        private string _aSSIGNMENT_GRADE;
    
        [DataMember]
        public string ASSIGNMENT_JOB
        {
            get { return _aSSIGNMENT_JOB; }
            set
            {
                if (_aSSIGNMENT_JOB != value)
                {
                    ChangeTracker.RecordOriginalValue("ASSIGNMENT_JOB", _aSSIGNMENT_JOB);
                    if (!IsDeserializing)
                    {
                        if (HR_JOBS != null && HR_JOBS.JOB_ID != value)
                        {
                            HR_JOBS = null;
                        }
                    }
                    _aSSIGNMENT_JOB = value;
                    OnPropertyChanged("ASSIGNMENT_JOB");
                }
            }
        }
        private string _aSSIGNMENT_JOB;
    
        [DataMember]
        public string ASSIGNMENT_POSITION
        {
            get { return _aSSIGNMENT_POSITION; }
            set
            {
                if (_aSSIGNMENT_POSITION != value)
                {
                    ChangeTracker.RecordOriginalValue("ASSIGNMENT_POSITION", _aSSIGNMENT_POSITION);
                    if (!IsDeserializing)
                    {
                        if (HR_POSITIONS != null && HR_POSITIONS.POSITION_ID != value)
                        {
                            HR_POSITIONS = null;
                        }
                    }
                    _aSSIGNMENT_POSITION = value;
                    OnPropertyChanged("ASSIGNMENT_POSITION");
                }
            }
        }
        private string _aSSIGNMENT_POSITION;
    
        [DataMember]
        public string ASSIGNMENT_REPORTING_TO
        {
            get { return _aSSIGNMENT_REPORTING_TO; }
            set
            {
                if (_aSSIGNMENT_REPORTING_TO != value)
                {
                    ChangeTracker.RecordOriginalValue("ASSIGNMENT_REPORTING_TO", _aSSIGNMENT_REPORTING_TO);
                    if (!IsDeserializing)
                    {
                        if (HR_EMPLOYEES1 != null && HR_EMPLOYEES1.EMP_ID != value)
                        {
                            HR_EMPLOYEES1 = null;
                        }
                    }
                    _aSSIGNMENT_REPORTING_TO = value;
                    OnPropertyChanged("ASSIGNMENT_REPORTING_TO");
                }
            }
        }
        private string _aSSIGNMENT_REPORTING_TO;
    
        [DataMember]
        public string ASSIGNMENT_ORG_ID
        {
            get { return _aSSIGNMENT_ORG_ID; }
            set
            {
                if (_aSSIGNMENT_ORG_ID != value)
                {
                    ChangeTracker.RecordOriginalValue("ASSIGNMENT_ORG_ID", _aSSIGNMENT_ORG_ID);
                    if (!IsDeserializing)
                    {
                        if (GL_COMPANIES_HDR != null && GL_COMPANIES_HDR.COMP_ID != value)
                        {
                            GL_COMPANIES_HDR = null;
                        }
                    }
                    _aSSIGNMENT_ORG_ID = value;
                    OnPropertyChanged("ASSIGNMENT_ORG_ID");
                }
            }
        }
        private string _aSSIGNMENT_ORG_ID;
    
        [DataMember]
        public string ENABLED_FLAG
        {
            get { return _eNABLED_FLAG; }
            set
            {
                if (_eNABLED_FLAG != value)
                {
                    _eNABLED_FLAG = value;
                    OnPropertyChanged("ENABLED_FLAG");
                }
            }
        }
        private string _eNABLED_FLAG;
    
        [DataMember]
        public string WORKFLOW_COMPLETION_STATUS
        {
            get { return _wORKFLOW_COMPLETION_STATUS; }
            set
            {
                if (_wORKFLOW_COMPLETION_STATUS != value)
                {
                    _wORKFLOW_COMPLETION_STATUS = value;
                    OnPropertyChanged("WORKFLOW_COMPLETION_STATUS");
                }
            }
        }
        private string _wORKFLOW_COMPLETION_STATUS;
    
        [DataMember]
        public string CREATED_BY
        {
            get { return _cREATED_BY; }
            set
            {
                if (_cREATED_BY != value)
                {
                    _cREATED_BY = value;
                    OnPropertyChanged("CREATED_BY");
                }
            }
        }
        private string _cREATED_BY;
    
        [DataMember]
        public System.DateTime CREATED_DATE
        {
            get { return _cREATED_DATE; }
            set
            {
                if (_cREATED_DATE != value)
                {
                    _cREATED_DATE = value;
                    OnPropertyChanged("CREATED_DATE");
                }
            }
        }
        private System.DateTime _cREATED_DATE;
    
        [DataMember]
        public string MODIFIED_BY
        {
            get { return _mODIFIED_BY; }
            set
            {
                if (_mODIFIED_BY != value)
                {
                    _mODIFIED_BY = value;
                    OnPropertyChanged("MODIFIED_BY");
                }
            }
        }
        private string _mODIFIED_BY;
    
        [DataMember]
        public Nullable<System.DateTime> MODIFIED_DATE
        {
            get { return _mODIFIED_DATE; }
            set
            {
                if (_mODIFIED_DATE != value)
                {
                    _mODIFIED_DATE = value;
                    OnPropertyChanged("MODIFIED_DATE");
                }
            }
        }
        private Nullable<System.DateTime> _mODIFIED_DATE;
    
        [DataMember]
        public string ATTRIBUTE1
        {
            get { return _aTTRIBUTE1; }
            set
            {
                if (_aTTRIBUTE1 != value)
                {
                    _aTTRIBUTE1 = value;
                    OnPropertyChanged("ATTRIBUTE1");
                }
            }
        }
        private string _aTTRIBUTE1;
    
        [DataMember]
        public string ATTRIBUTE2
        {
            get { return _aTTRIBUTE2; }
            set
            {
                if (_aTTRIBUTE2 != value)
                {
                    _aTTRIBUTE2 = value;
                    OnPropertyChanged("ATTRIBUTE2");
                }
            }
        }
        private string _aTTRIBUTE2;
    
        [DataMember]
        public string ATTRIBUTE3
        {
            get { return _aTTRIBUTE3; }
            set
            {
                if (_aTTRIBUTE3 != value)
                {
                    _aTTRIBUTE3 = value;
                    OnPropertyChanged("ATTRIBUTE3");
                }
            }
        }
        private string _aTTRIBUTE3;
    
        [DataMember]
        public string ATTRIBUTE4
        {
            get { return _aTTRIBUTE4; }
            set
            {
                if (_aTTRIBUTE4 != value)
                {
                    _aTTRIBUTE4 = value;
                    OnPropertyChanged("ATTRIBUTE4");
                }
            }
        }
        private string _aTTRIBUTE4;
    
        [DataMember]
        public string ATTRIBUTE5
        {
            get { return _aTTRIBUTE5; }
            set
            {
                if (_aTTRIBUTE5 != value)
                {
                    _aTTRIBUTE5 = value;
                    OnPropertyChanged("ATTRIBUTE5");
                }
            }
        }
        private string _aTTRIBUTE5;
    
        [DataMember]
        public string ATTRIBUTE6
        {
            get { return _aTTRIBUTE6; }
            set
            {
                if (_aTTRIBUTE6 != value)
                {
                    _aTTRIBUTE6 = value;
                    OnPropertyChanged("ATTRIBUTE6");
                }
            }
        }
        private string _aTTRIBUTE6;
    
        [DataMember]
        public string ATTRIBUTE7
        {
            get { return _aTTRIBUTE7; }
            set
            {
                if (_aTTRIBUTE7 != value)
                {
                    _aTTRIBUTE7 = value;
                    OnPropertyChanged("ATTRIBUTE7");
                }
            }
        }
        private string _aTTRIBUTE7;
    
        [DataMember]
        public string ATTRIBUTE8
        {
            get { return _aTTRIBUTE8; }
            set
            {
                if (_aTTRIBUTE8 != value)
                {
                    _aTTRIBUTE8 = value;
                    OnPropertyChanged("ATTRIBUTE8");
                }
            }
        }
        private string _aTTRIBUTE8;
    
        [DataMember]
        public string ATTRIBUTE9
        {
            get { return _aTTRIBUTE9; }
            set
            {
                if (_aTTRIBUTE9 != value)
                {
                    _aTTRIBUTE9 = value;
                    OnPropertyChanged("ATTRIBUTE9");
                }
            }
        }
        private string _aTTRIBUTE9;
    
        [DataMember]
        public string ATTRIBUTE10
        {
            get { return _aTTRIBUTE10; }
            set
            {
                if (_aTTRIBUTE10 != value)
                {
                    _aTTRIBUTE10 = value;
                    OnPropertyChanged("ATTRIBUTE10");
                }
            }
        }
        private string _aTTRIBUTE10;
    
        [DataMember]
        public string LOCATION_ID
        {
            get { return _lOCATION_ID; }
            set
            {
                if (_lOCATION_ID != value)
                {
                    _lOCATION_ID = value;
                    OnPropertyChanged("LOCATION_ID");
                }
            }
        }
        private string _lOCATION_ID;
    
        [DataMember]
        public string CATEGORY_ID
        {
            get { return _cATEGORY_ID; }
            set
            {
                if (_cATEGORY_ID != value)
                {
                    _cATEGORY_ID = value;
                    OnPropertyChanged("CATEGORY_ID");
                }
            }
        }
        private string _cATEGORY_ID;

        #endregion
        #region Navigation Properties
    
        [DataMember]
        public GL_COMPANIES_HDR GL_COMPANIES_HDR
        {
            get { return _gL_COMPANIES_HDR; }
            set
            {
                if (!ReferenceEquals(_gL_COMPANIES_HDR, value))
                {
                    var previousValue = _gL_COMPANIES_HDR;
                    _gL_COMPANIES_HDR = value;
                    FixupGL_COMPANIES_HDR(previousValue);
                    OnNavigationPropertyChanged("GL_COMPANIES_HDR");
                }
            }
        }
        private GL_COMPANIES_HDR _gL_COMPANIES_HDR;
    
        [DataMember]
        public HR_EMPLOYEES HR_EMPLOYEES
        {
            get { return _hR_EMPLOYEES; }
            set
            {
                if (!ReferenceEquals(_hR_EMPLOYEES, value))
                {
                    var previousValue = _hR_EMPLOYEES;
                    _hR_EMPLOYEES = value;
                    FixupHR_EMPLOYEES(previousValue);
                    OnNavigationPropertyChanged("HR_EMPLOYEES");
                }
            }
        }
        private HR_EMPLOYEES _hR_EMPLOYEES;
    
        [DataMember]
        public HR_GRADES HR_GRADES
        {
            get { return _hR_GRADES; }
            set
            {
                if (!ReferenceEquals(_hR_GRADES, value))
                {
                    var previousValue = _hR_GRADES;
                    _hR_GRADES = value;
                    FixupHR_GRADES(previousValue);
                    OnNavigationPropertyChanged("HR_GRADES");
                }
            }
        }
        private HR_GRADES _hR_GRADES;
    
        [DataMember]
        public HR_JOBS HR_JOBS
        {
            get { return _hR_JOBS; }
            set
            {
                if (!ReferenceEquals(_hR_JOBS, value))
                {
                    var previousValue = _hR_JOBS;
                    _hR_JOBS = value;
                    FixupHR_JOBS(previousValue);
                    OnNavigationPropertyChanged("HR_JOBS");
                }
            }
        }
        private HR_JOBS _hR_JOBS;
    
        [DataMember]
        public HR_POSITIONS HR_POSITIONS
        {
            get { return _hR_POSITIONS; }
            set
            {
                if (!ReferenceEquals(_hR_POSITIONS, value))
                {
                    var previousValue = _hR_POSITIONS;
                    _hR_POSITIONS = value;
                    FixupHR_POSITIONS(previousValue);
                    OnNavigationPropertyChanged("HR_POSITIONS");
                }
            }
        }
        private HR_POSITIONS _hR_POSITIONS;
    
        [DataMember]
        public HR_EMPLOYEES HR_EMPLOYEES1
        {
            get { return _hR_EMPLOYEES1; }
            set
            {
                if (!ReferenceEquals(_hR_EMPLOYEES1, value))
                {
                    var previousValue = _hR_EMPLOYEES1;
                    _hR_EMPLOYEES1 = value;
                    FixupHR_EMPLOYEES1(previousValue);
                    OnNavigationPropertyChanged("HR_EMPLOYEES1");
                }
            }
        }
        private HR_EMPLOYEES _hR_EMPLOYEES1;

        #endregion
        #region ChangeTracking
    
        protected virtual void OnPropertyChanged(String propertyName)
        {
            if (ChangeTracker.State != ObjectState.Added && ChangeTracker.State != ObjectState.Deleted)
            {
                ChangeTracker.State = ObjectState.Modified;
            }
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        protected virtual void OnNavigationPropertyChanged(String propertyName)
        {
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        event PropertyChangedEventHandler INotifyPropertyChanged.PropertyChanged{ add { _propertyChanged += value; } remove { _propertyChanged -= value; } }
        private event PropertyChangedEventHandler _propertyChanged;
        private ObjectChangeTracker _changeTracker;
    
        [DataMember]
        public ObjectChangeTracker ChangeTracker
        {
            get
            {
                if (_changeTracker == null)
                {
                    _changeTracker = new ObjectChangeTracker();
                    _changeTracker.ObjectStateChanging += HandleObjectStateChanging;
                }
                return _changeTracker;
            }
            set
            {
                if(_changeTracker != null)
                {
                    _changeTracker.ObjectStateChanging -= HandleObjectStateChanging;
                }
                _changeTracker = value;
                if(_changeTracker != null)
                {
                    _changeTracker.ObjectStateChanging += HandleObjectStateChanging;
                }
            }
        }
    
        private void HandleObjectStateChanging(object sender, ObjectStateChangingEventArgs e)
        {
            if (e.NewState == ObjectState.Deleted)
            {
                ClearNavigationProperties();
            }
        }
    
        protected bool IsDeserializing { get; private set; }
    
        [OnDeserializing]
        public void OnDeserializingMethod(StreamingContext context)
        {
            IsDeserializing = true;
        }
    
        [OnDeserialized]
        public void OnDeserializedMethod(StreamingContext context)
        {
            IsDeserializing = false;
            ChangeTracker.ChangeTrackingEnabled = true;
        }
    
        protected virtual void ClearNavigationProperties()
        {
            GL_COMPANIES_HDR = null;
            HR_EMPLOYEES = null;
            HR_GRADES = null;
            HR_JOBS = null;
            HR_POSITIONS = null;
            HR_EMPLOYEES1 = null;
        }

        #endregion
        #region Association Fixup
    
        private void FixupGL_COMPANIES_HDR(GL_COMPANIES_HDR previousValue, bool skipKeys = false)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (previousValue != null && previousValue.HR_EMP_ASSIGNMENT_DTLS.Contains(this))
            {
                previousValue.HR_EMP_ASSIGNMENT_DTLS.Remove(this);
            }
    
            if (GL_COMPANIES_HDR != null)
            {
                if (!GL_COMPANIES_HDR.HR_EMP_ASSIGNMENT_DTLS.Contains(this))
                {
                    GL_COMPANIES_HDR.HR_EMP_ASSIGNMENT_DTLS.Add(this);
                }
    
                ASSIGNMENT_ORG_ID = GL_COMPANIES_HDR.COMP_ID;
            }
            else if (!skipKeys)
            {
                ASSIGNMENT_ORG_ID = null;
            }
    
            if (ChangeTracker.ChangeTrackingEnabled)
            {
                if (ChangeTracker.OriginalValues.ContainsKey("GL_COMPANIES_HDR")
                    && (ChangeTracker.OriginalValues["GL_COMPANIES_HDR"] == GL_COMPANIES_HDR))
                {
                    ChangeTracker.OriginalValues.Remove("GL_COMPANIES_HDR");
                }
                else
                {
                    ChangeTracker.RecordOriginalValue("GL_COMPANIES_HDR", previousValue);
                }
                if (GL_COMPANIES_HDR != null && !GL_COMPANIES_HDR.ChangeTracker.ChangeTrackingEnabled)
                {
                    GL_COMPANIES_HDR.StartTracking();
                }
            }
        }
    
        private void FixupHR_EMPLOYEES(HR_EMPLOYEES previousValue, bool skipKeys = false)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (previousValue != null && previousValue.HR_EMP_ASSIGNMENT_DTLS.Contains(this))
            {
                previousValue.HR_EMP_ASSIGNMENT_DTLS.Remove(this);
            }
    
            if (HR_EMPLOYEES != null)
            {
                if (!HR_EMPLOYEES.HR_EMP_ASSIGNMENT_DTLS.Contains(this))
                {
                    HR_EMPLOYEES.HR_EMP_ASSIGNMENT_DTLS.Add(this);
                }
    
                ASSIGNMENT_EMP_ID = HR_EMPLOYEES.EMP_ID;
            }
            else if (!skipKeys)
            {
                ASSIGNMENT_EMP_ID = null;
            }
    
            if (ChangeTracker.ChangeTrackingEnabled)
            {
                if (ChangeTracker.OriginalValues.ContainsKey("HR_EMPLOYEES")
                    && (ChangeTracker.OriginalValues["HR_EMPLOYEES"] == HR_EMPLOYEES))
                {
                    ChangeTracker.OriginalValues.Remove("HR_EMPLOYEES");
                }
                else
                {
                    ChangeTracker.RecordOriginalValue("HR_EMPLOYEES", previousValue);
                }
                if (HR_EMPLOYEES != null && !HR_EMPLOYEES.ChangeTracker.ChangeTrackingEnabled)
                {
                    HR_EMPLOYEES.StartTracking();
                }
            }
        }
    
        private void FixupHR_GRADES(HR_GRADES previousValue, bool skipKeys = false)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (previousValue != null && previousValue.HR_EMP_ASSIGNMENT_DTLS.Contains(this))
            {
                previousValue.HR_EMP_ASSIGNMENT_DTLS.Remove(this);
            }
    
            if (HR_GRADES != null)
            {
                if (!HR_GRADES.HR_EMP_ASSIGNMENT_DTLS.Contains(this))
                {
                    HR_GRADES.HR_EMP_ASSIGNMENT_DTLS.Add(this);
                }
    
                ASSIGNMENT_GRADE = HR_GRADES.GRADE_ID;
            }
            else if (!skipKeys)
            {
                ASSIGNMENT_GRADE = null;
            }
    
            if (ChangeTracker.ChangeTrackingEnabled)
            {
                if (ChangeTracker.OriginalValues.ContainsKey("HR_GRADES")
                    && (ChangeTracker.OriginalValues["HR_GRADES"] == HR_GRADES))
                {
                    ChangeTracker.OriginalValues.Remove("HR_GRADES");
                }
                else
                {
                    ChangeTracker.RecordOriginalValue("HR_GRADES", previousValue);
                }
                if (HR_GRADES != null && !HR_GRADES.ChangeTracker.ChangeTrackingEnabled)
                {
                    HR_GRADES.StartTracking();
                }
            }
        }
    
        private void FixupHR_JOBS(HR_JOBS previousValue, bool skipKeys = false)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (previousValue != null && previousValue.HR_EMP_ASSIGNMENT_DTLS.Contains(this))
            {
                previousValue.HR_EMP_ASSIGNMENT_DTLS.Remove(this);
            }
    
            if (HR_JOBS != null)
            {
                if (!HR_JOBS.HR_EMP_ASSIGNMENT_DTLS.Contains(this))
                {
                    HR_JOBS.HR_EMP_ASSIGNMENT_DTLS.Add(this);
                }
    
                ASSIGNMENT_JOB = HR_JOBS.JOB_ID;
            }
            else if (!skipKeys)
            {
                ASSIGNMENT_JOB = null;
            }
    
            if (ChangeTracker.ChangeTrackingEnabled)
            {
                if (ChangeTracker.OriginalValues.ContainsKey("HR_JOBS")
                    && (ChangeTracker.OriginalValues["HR_JOBS"] == HR_JOBS))
                {
                    ChangeTracker.OriginalValues.Remove("HR_JOBS");
                }
                else
                {
                    ChangeTracker.RecordOriginalValue("HR_JOBS", previousValue);
                }
                if (HR_JOBS != null && !HR_JOBS.ChangeTracker.ChangeTrackingEnabled)
                {
                    HR_JOBS.StartTracking();
                }
            }
        }
    
        private void FixupHR_POSITIONS(HR_POSITIONS previousValue, bool skipKeys = false)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (previousValue != null && previousValue.HR_EMP_ASSIGNMENT_DTLS.Contains(this))
            {
                previousValue.HR_EMP_ASSIGNMENT_DTLS.Remove(this);
            }
    
            if (HR_POSITIONS != null)
            {
                if (!HR_POSITIONS.HR_EMP_ASSIGNMENT_DTLS.Contains(this))
                {
                    HR_POSITIONS.HR_EMP_ASSIGNMENT_DTLS.Add(this);
                }
    
                ASSIGNMENT_POSITION = HR_POSITIONS.POSITION_ID;
            }
            else if (!skipKeys)
            {
                ASSIGNMENT_POSITION = null;
            }
    
            if (ChangeTracker.ChangeTrackingEnabled)
            {
                if (ChangeTracker.OriginalValues.ContainsKey("HR_POSITIONS")
                    && (ChangeTracker.OriginalValues["HR_POSITIONS"] == HR_POSITIONS))
                {
                    ChangeTracker.OriginalValues.Remove("HR_POSITIONS");
                }
                else
                {
                    ChangeTracker.RecordOriginalValue("HR_POSITIONS", previousValue);
                }
                if (HR_POSITIONS != null && !HR_POSITIONS.ChangeTracker.ChangeTrackingEnabled)
                {
                    HR_POSITIONS.StartTracking();
                }
            }
        }
    
        private void FixupHR_EMPLOYEES1(HR_EMPLOYEES previousValue, bool skipKeys = false)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (previousValue != null && previousValue.HR_EMP_ASSIGNMENT_DTLS1.Contains(this))
            {
                previousValue.HR_EMP_ASSIGNMENT_DTLS1.Remove(this);
            }
    
            if (HR_EMPLOYEES1 != null)
            {
                if (!HR_EMPLOYEES1.HR_EMP_ASSIGNMENT_DTLS1.Contains(this))
                {
                    HR_EMPLOYEES1.HR_EMP_ASSIGNMENT_DTLS1.Add(this);
                }
    
                ASSIGNMENT_REPORTING_TO = HR_EMPLOYEES1.EMP_ID;
            }
            else if (!skipKeys)
            {
                ASSIGNMENT_REPORTING_TO = null;
            }
    
            if (ChangeTracker.ChangeTrackingEnabled)
            {
                if (ChangeTracker.OriginalValues.ContainsKey("HR_EMPLOYEES1")
                    && (ChangeTracker.OriginalValues["HR_EMPLOYEES1"] == HR_EMPLOYEES1))
                {
                    ChangeTracker.OriginalValues.Remove("HR_EMPLOYEES1");
                }
                else
                {
                    ChangeTracker.RecordOriginalValue("HR_EMPLOYEES1", previousValue);
                }
                if (HR_EMPLOYEES1 != null && !HR_EMPLOYEES1.ChangeTracker.ChangeTrackingEnabled)
                {
                    HR_EMPLOYEES1.StartTracking();
                }
            }
        }

        #endregion
    }
}
