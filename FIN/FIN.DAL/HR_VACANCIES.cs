//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.Serialization;

namespace FIN.DAL
{
    [DataContract(IsReference = true)]
    [KnownType(typeof(HR_APP_SHORT_LIST))]
    [KnownType(typeof(HR_DEPARTMENTS))]
    [KnownType(typeof(HR_DEPT_DESIGNATIONS))]
    [KnownType(typeof(HR_INTERVIEWS_HDR))]
    [KnownType(typeof(HR_INTERVIEWS_RESULTS))]
    [KnownType(typeof(HR_VAC_CRITERIA_MASTER))]
    [KnownType(typeof(HR_VAC_EVAL_CONDITION))]
    public partial class HR_VACANCIES: IObjectWithChangeTracker, INotifyPropertyChanged
    {
        #region Primitive Properties
    
        [DataMember]
        public Nullable<int> PK_ID
        {
            get { return _pK_ID; }
            set
            {
                if (_pK_ID != value)
                {
                    _pK_ID = value;
                    OnPropertyChanged("PK_ID");
                }
            }
        }
        private Nullable<int> _pK_ID;
    
        [DataMember]
        public Nullable<int> CHILD_ID
        {
            get { return _cHILD_ID; }
            set
            {
                if (_cHILD_ID != value)
                {
                    _cHILD_ID = value;
                    OnPropertyChanged("CHILD_ID");
                }
            }
        }
        private Nullable<int> _cHILD_ID;
    
        [DataMember]
        public string VAC_ID
        {
            get { return _vAC_ID; }
            set
            {
                if (_vAC_ID != value)
                {
                    if (ChangeTracker.ChangeTrackingEnabled && ChangeTracker.State != ObjectState.Added)
                    {
                        throw new InvalidOperationException("The property 'VAC_ID' is part of the object's key and cannot be changed. Changes to key properties can only be made when the object is not being tracked or is in the Added state.");
                    }
                    _vAC_ID = value;
                    OnPropertyChanged("VAC_ID");
                }
            }
        }
        private string _vAC_ID;
    
        [DataMember]
        public string VAC_DEPT_ID
        {
            get { return _vAC_DEPT_ID; }
            set
            {
                if (_vAC_DEPT_ID != value)
                {
                    ChangeTracker.RecordOriginalValue("VAC_DEPT_ID", _vAC_DEPT_ID);
                    if (!IsDeserializing)
                    {
                        if (HR_DEPARTMENTS != null && HR_DEPARTMENTS.DEPT_ID != value)
                        {
                            HR_DEPARTMENTS = null;
                        }
                    }
                    _vAC_DEPT_ID = value;
                    OnPropertyChanged("VAC_DEPT_ID");
                }
            }
        }
        private string _vAC_DEPT_ID;
    
        [DataMember]
        public string VAC_DESIG_ID
        {
            get { return _vAC_DESIG_ID; }
            set
            {
                if (_vAC_DESIG_ID != value)
                {
                    ChangeTracker.RecordOriginalValue("VAC_DESIG_ID", _vAC_DESIG_ID);
                    if (!IsDeserializing)
                    {
                        if (HR_DEPT_DESIGNATIONS != null && HR_DEPT_DESIGNATIONS.DEPT_DESIG_ID != value)
                        {
                            HR_DEPT_DESIGNATIONS = null;
                        }
                    }
                    _vAC_DESIG_ID = value;
                    OnPropertyChanged("VAC_DESIG_ID");
                }
            }
        }
        private string _vAC_DESIG_ID;
    
        [DataMember]
        public Nullable<int> VAC_NO_OF_PEOPLES
        {
            get { return _vAC_NO_OF_PEOPLES; }
            set
            {
                if (_vAC_NO_OF_PEOPLES != value)
                {
                    _vAC_NO_OF_PEOPLES = value;
                    OnPropertyChanged("VAC_NO_OF_PEOPLES");
                }
            }
        }
        private Nullable<int> _vAC_NO_OF_PEOPLES;
    
        [DataMember]
        public string VAC_EXP_YEARS
        {
            get { return _vAC_EXP_YEARS; }
            set
            {
                if (_vAC_EXP_YEARS != value)
                {
                    _vAC_EXP_YEARS = value;
                    OnPropertyChanged("VAC_EXP_YEARS");
                }
            }
        }
        private string _vAC_EXP_YEARS;
    
        [DataMember]
        public string VAC_RELOCATE_YN
        {
            get { return _vAC_RELOCATE_YN; }
            set
            {
                if (_vAC_RELOCATE_YN != value)
                {
                    _vAC_RELOCATE_YN = value;
                    OnPropertyChanged("VAC_RELOCATE_YN");
                }
            }
        }
        private string _vAC_RELOCATE_YN;
    
        [DataMember]
        public string VAC_TYPE
        {
            get { return _vAC_TYPE; }
            set
            {
                if (_vAC_TYPE != value)
                {
                    _vAC_TYPE = value;
                    OnPropertyChanged("VAC_TYPE");
                }
            }
        }
        private string _vAC_TYPE;
    
        [DataMember]
        public string VAC_COMM_LEVEL
        {
            get { return _vAC_COMM_LEVEL; }
            set
            {
                if (_vAC_COMM_LEVEL != value)
                {
                    _vAC_COMM_LEVEL = value;
                    OnPropertyChanged("VAC_COMM_LEVEL");
                }
            }
        }
        private string _vAC_COMM_LEVEL;
    
        [DataMember]
        public string VAC_REQ_TECHNOLOGY
        {
            get { return _vAC_REQ_TECHNOLOGY; }
            set
            {
                if (_vAC_REQ_TECHNOLOGY != value)
                {
                    _vAC_REQ_TECHNOLOGY = value;
                    OnPropertyChanged("VAC_REQ_TECHNOLOGY");
                }
            }
        }
        private string _vAC_REQ_TECHNOLOGY;
    
        [DataMember]
        public string VAC_BUDGET_ID
        {
            get { return _vAC_BUDGET_ID; }
            set
            {
                if (_vAC_BUDGET_ID != value)
                {
                    _vAC_BUDGET_ID = value;
                    OnPropertyChanged("VAC_BUDGET_ID");
                }
            }
        }
        private string _vAC_BUDGET_ID;
    
        [DataMember]
        public string VAC_CERT_REQ_YN
        {
            get { return _vAC_CERT_REQ_YN; }
            set
            {
                if (_vAC_CERT_REQ_YN != value)
                {
                    _vAC_CERT_REQ_YN = value;
                    OnPropertyChanged("VAC_CERT_REQ_YN");
                }
            }
        }
        private string _vAC_CERT_REQ_YN;
    
        [DataMember]
        public string VAC_INTER_EXTERNAL
        {
            get { return _vAC_INTER_EXTERNAL; }
            set
            {
                if (_vAC_INTER_EXTERNAL != value)
                {
                    _vAC_INTER_EXTERNAL = value;
                    OnPropertyChanged("VAC_INTER_EXTERNAL");
                }
            }
        }
        private string _vAC_INTER_EXTERNAL;
    
        [DataMember]
        public string VAC_AGENCY_NAME
        {
            get { return _vAC_AGENCY_NAME; }
            set
            {
                if (_vAC_AGENCY_NAME != value)
                {
                    _vAC_AGENCY_NAME = value;
                    OnPropertyChanged("VAC_AGENCY_NAME");
                }
            }
        }
        private string _vAC_AGENCY_NAME;
    
        [DataMember]
        public string VAC_AGENCY_PERSON
        {
            get { return _vAC_AGENCY_PERSON; }
            set
            {
                if (_vAC_AGENCY_PERSON != value)
                {
                    _vAC_AGENCY_PERSON = value;
                    OnPropertyChanged("VAC_AGENCY_PERSON");
                }
            }
        }
        private string _vAC_AGENCY_PERSON;
    
        [DataMember]
        public string VAC_AGENCY_CONTACT
        {
            get { return _vAC_AGENCY_CONTACT; }
            set
            {
                if (_vAC_AGENCY_CONTACT != value)
                {
                    _vAC_AGENCY_CONTACT = value;
                    OnPropertyChanged("VAC_AGENCY_CONTACT");
                }
            }
        }
        private string _vAC_AGENCY_CONTACT;
    
        [DataMember]
        public string ENABLED_FLAG
        {
            get { return _eNABLED_FLAG; }
            set
            {
                if (_eNABLED_FLAG != value)
                {
                    _eNABLED_FLAG = value;
                    OnPropertyChanged("ENABLED_FLAG");
                }
            }
        }
        private string _eNABLED_FLAG;
    
        [DataMember]
        public string WORKFLOW_COMPLETION_STATUS
        {
            get { return _wORKFLOW_COMPLETION_STATUS; }
            set
            {
                if (_wORKFLOW_COMPLETION_STATUS != value)
                {
                    _wORKFLOW_COMPLETION_STATUS = value;
                    OnPropertyChanged("WORKFLOW_COMPLETION_STATUS");
                }
            }
        }
        private string _wORKFLOW_COMPLETION_STATUS;
    
        [DataMember]
        public string CREATED_BY
        {
            get { return _cREATED_BY; }
            set
            {
                if (_cREATED_BY != value)
                {
                    _cREATED_BY = value;
                    OnPropertyChanged("CREATED_BY");
                }
            }
        }
        private string _cREATED_BY;
    
        [DataMember]
        public System.DateTime CREATED_DATE
        {
            get { return _cREATED_DATE; }
            set
            {
                if (_cREATED_DATE != value)
                {
                    _cREATED_DATE = value;
                    OnPropertyChanged("CREATED_DATE");
                }
            }
        }
        private System.DateTime _cREATED_DATE;
    
        [DataMember]
        public string MODIFIED_BY
        {
            get { return _mODIFIED_BY; }
            set
            {
                if (_mODIFIED_BY != value)
                {
                    _mODIFIED_BY = value;
                    OnPropertyChanged("MODIFIED_BY");
                }
            }
        }
        private string _mODIFIED_BY;
    
        [DataMember]
        public Nullable<System.DateTime> MODIFIED_DATE
        {
            get { return _mODIFIED_DATE; }
            set
            {
                if (_mODIFIED_DATE != value)
                {
                    _mODIFIED_DATE = value;
                    OnPropertyChanged("MODIFIED_DATE");
                }
            }
        }
        private Nullable<System.DateTime> _mODIFIED_DATE;
    
        [DataMember]
        public string ATTRIBUTE1
        {
            get { return _aTTRIBUTE1; }
            set
            {
                if (_aTTRIBUTE1 != value)
                {
                    _aTTRIBUTE1 = value;
                    OnPropertyChanged("ATTRIBUTE1");
                }
            }
        }
        private string _aTTRIBUTE1;
    
        [DataMember]
        public string ATTRIBUTE2
        {
            get { return _aTTRIBUTE2; }
            set
            {
                if (_aTTRIBUTE2 != value)
                {
                    _aTTRIBUTE2 = value;
                    OnPropertyChanged("ATTRIBUTE2");
                }
            }
        }
        private string _aTTRIBUTE2;
    
        [DataMember]
        public string ATTRIBUTE3
        {
            get { return _aTTRIBUTE3; }
            set
            {
                if (_aTTRIBUTE3 != value)
                {
                    _aTTRIBUTE3 = value;
                    OnPropertyChanged("ATTRIBUTE3");
                }
            }
        }
        private string _aTTRIBUTE3;
    
        [DataMember]
        public string ATTRIBUTE4
        {
            get { return _aTTRIBUTE4; }
            set
            {
                if (_aTTRIBUTE4 != value)
                {
                    _aTTRIBUTE4 = value;
                    OnPropertyChanged("ATTRIBUTE4");
                }
            }
        }
        private string _aTTRIBUTE4;
    
        [DataMember]
        public string ATTRIBUTE5
        {
            get { return _aTTRIBUTE5; }
            set
            {
                if (_aTTRIBUTE5 != value)
                {
                    _aTTRIBUTE5 = value;
                    OnPropertyChanged("ATTRIBUTE5");
                }
            }
        }
        private string _aTTRIBUTE5;
    
        [DataMember]
        public string ATTRIBUTE6
        {
            get { return _aTTRIBUTE6; }
            set
            {
                if (_aTTRIBUTE6 != value)
                {
                    _aTTRIBUTE6 = value;
                    OnPropertyChanged("ATTRIBUTE6");
                }
            }
        }
        private string _aTTRIBUTE6;
    
        [DataMember]
        public string ATTRIBUTE7
        {
            get { return _aTTRIBUTE7; }
            set
            {
                if (_aTTRIBUTE7 != value)
                {
                    _aTTRIBUTE7 = value;
                    OnPropertyChanged("ATTRIBUTE7");
                }
            }
        }
        private string _aTTRIBUTE7;
    
        [DataMember]
        public string ATTRIBUTE8
        {
            get { return _aTTRIBUTE8; }
            set
            {
                if (_aTTRIBUTE8 != value)
                {
                    _aTTRIBUTE8 = value;
                    OnPropertyChanged("ATTRIBUTE8");
                }
            }
        }
        private string _aTTRIBUTE8;
    
        [DataMember]
        public string ATTRIBUTE9
        {
            get { return _aTTRIBUTE9; }
            set
            {
                if (_aTTRIBUTE9 != value)
                {
                    _aTTRIBUTE9 = value;
                    OnPropertyChanged("ATTRIBUTE9");
                }
            }
        }
        private string _aTTRIBUTE9;
    
        [DataMember]
        public string ATTRIBUTE10
        {
            get { return _aTTRIBUTE10; }
            set
            {
                if (_aTTRIBUTE10 != value)
                {
                    _aTTRIBUTE10 = value;
                    OnPropertyChanged("ATTRIBUTE10");
                }
            }
        }
        private string _aTTRIBUTE10;
    
        [DataMember]
        public Nullable<int> VAC_MIN_EXP
        {
            get { return _vAC_MIN_EXP; }
            set
            {
                if (_vAC_MIN_EXP != value)
                {
                    _vAC_MIN_EXP = value;
                    OnPropertyChanged("VAC_MIN_EXP");
                }
            }
        }
        private Nullable<int> _vAC_MIN_EXP;
    
        [DataMember]
        public Nullable<int> VAC_MAX_EXP
        {
            get { return _vAC_MAX_EXP; }
            set
            {
                if (_vAC_MAX_EXP != value)
                {
                    _vAC_MAX_EXP = value;
                    OnPropertyChanged("VAC_MAX_EXP");
                }
            }
        }
        private Nullable<int> _vAC_MAX_EXP;
    
        [DataMember]
        public string VAC_NAME
        {
            get { return _vAC_NAME; }
            set
            {
                if (_vAC_NAME != value)
                {
                    _vAC_NAME = value;
                    OnPropertyChanged("VAC_NAME");
                }
            }
        }
        private string _vAC_NAME;
    
        [DataMember]
        public string ORG_ID
        {
            get { return _oRG_ID; }
            set
            {
                if (_oRG_ID != value)
                {
                    _oRG_ID = value;
                    OnPropertyChanged("ORG_ID");
                }
            }
        }
        private string _oRG_ID;
    
        [DataMember]
        public Nullable<decimal> BUDGET_FROM
        {
            get { return _bUDGET_FROM; }
            set
            {
                if (_bUDGET_FROM != value)
                {
                    _bUDGET_FROM = value;
                    OnPropertyChanged("BUDGET_FROM");
                }
            }
        }
        private Nullable<decimal> _bUDGET_FROM;
    
        [DataMember]
        public Nullable<decimal> BUDGET_TO
        {
            get { return _bUDGET_TO; }
            set
            {
                if (_bUDGET_TO != value)
                {
                    _bUDGET_TO = value;
                    OnPropertyChanged("BUDGET_TO");
                }
            }
        }
        private Nullable<decimal> _bUDGET_TO;
    
        [DataMember]
        public string STATUS
        {
            get { return _sTATUS; }
            set
            {
                if (_sTATUS != value)
                {
                    _sTATUS = value;
                    OnPropertyChanged("STATUS");
                }
            }
        }
        private string _sTATUS;
    
        [DataMember]
        public string PERIOD
        {
            get { return _pERIOD; }
            set
            {
                if (_pERIOD != value)
                {
                    _pERIOD = value;
                    OnPropertyChanged("PERIOD");
                }
            }
        }
        private string _pERIOD;

        #endregion
        #region Navigation Properties
    
        [DataMember]
        public TrackableCollection<HR_APP_SHORT_LIST> HR_APP_SHORT_LIST
        {
            get
            {
                if (_hR_APP_SHORT_LIST == null)
                {
                    _hR_APP_SHORT_LIST = new TrackableCollection<HR_APP_SHORT_LIST>();
                    _hR_APP_SHORT_LIST.CollectionChanged += FixupHR_APP_SHORT_LIST;
                }
                return _hR_APP_SHORT_LIST;
            }
            set
            {
                if (!ReferenceEquals(_hR_APP_SHORT_LIST, value))
                {
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        throw new InvalidOperationException("Cannot set the FixupChangeTrackingCollection when ChangeTracking is enabled");
                    }
                    if (_hR_APP_SHORT_LIST != null)
                    {
                        _hR_APP_SHORT_LIST.CollectionChanged -= FixupHR_APP_SHORT_LIST;
                    }
                    _hR_APP_SHORT_LIST = value;
                    if (_hR_APP_SHORT_LIST != null)
                    {
                        _hR_APP_SHORT_LIST.CollectionChanged += FixupHR_APP_SHORT_LIST;
                    }
                    OnNavigationPropertyChanged("HR_APP_SHORT_LIST");
                }
            }
        }
        private TrackableCollection<HR_APP_SHORT_LIST> _hR_APP_SHORT_LIST;
    
        [DataMember]
        public HR_DEPARTMENTS HR_DEPARTMENTS
        {
            get { return _hR_DEPARTMENTS; }
            set
            {
                if (!ReferenceEquals(_hR_DEPARTMENTS, value))
                {
                    var previousValue = _hR_DEPARTMENTS;
                    _hR_DEPARTMENTS = value;
                    FixupHR_DEPARTMENTS(previousValue);
                    OnNavigationPropertyChanged("HR_DEPARTMENTS");
                }
            }
        }
        private HR_DEPARTMENTS _hR_DEPARTMENTS;
    
        [DataMember]
        public HR_DEPT_DESIGNATIONS HR_DEPT_DESIGNATIONS
        {
            get { return _hR_DEPT_DESIGNATIONS; }
            set
            {
                if (!ReferenceEquals(_hR_DEPT_DESIGNATIONS, value))
                {
                    var previousValue = _hR_DEPT_DESIGNATIONS;
                    _hR_DEPT_DESIGNATIONS = value;
                    FixupHR_DEPT_DESIGNATIONS(previousValue);
                    OnNavigationPropertyChanged("HR_DEPT_DESIGNATIONS");
                }
            }
        }
        private HR_DEPT_DESIGNATIONS _hR_DEPT_DESIGNATIONS;
    
        [DataMember]
        public TrackableCollection<HR_INTERVIEWS_HDR> HR_INTERVIEWS_HDR
        {
            get
            {
                if (_hR_INTERVIEWS_HDR == null)
                {
                    _hR_INTERVIEWS_HDR = new TrackableCollection<HR_INTERVIEWS_HDR>();
                    _hR_INTERVIEWS_HDR.CollectionChanged += FixupHR_INTERVIEWS_HDR;
                }
                return _hR_INTERVIEWS_HDR;
            }
            set
            {
                if (!ReferenceEquals(_hR_INTERVIEWS_HDR, value))
                {
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        throw new InvalidOperationException("Cannot set the FixupChangeTrackingCollection when ChangeTracking is enabled");
                    }
                    if (_hR_INTERVIEWS_HDR != null)
                    {
                        _hR_INTERVIEWS_HDR.CollectionChanged -= FixupHR_INTERVIEWS_HDR;
                    }
                    _hR_INTERVIEWS_HDR = value;
                    if (_hR_INTERVIEWS_HDR != null)
                    {
                        _hR_INTERVIEWS_HDR.CollectionChanged += FixupHR_INTERVIEWS_HDR;
                    }
                    OnNavigationPropertyChanged("HR_INTERVIEWS_HDR");
                }
            }
        }
        private TrackableCollection<HR_INTERVIEWS_HDR> _hR_INTERVIEWS_HDR;
    
        [DataMember]
        public TrackableCollection<HR_INTERVIEWS_RESULTS> HR_INTERVIEWS_RESULTS
        {
            get
            {
                if (_hR_INTERVIEWS_RESULTS == null)
                {
                    _hR_INTERVIEWS_RESULTS = new TrackableCollection<HR_INTERVIEWS_RESULTS>();
                    _hR_INTERVIEWS_RESULTS.CollectionChanged += FixupHR_INTERVIEWS_RESULTS;
                }
                return _hR_INTERVIEWS_RESULTS;
            }
            set
            {
                if (!ReferenceEquals(_hR_INTERVIEWS_RESULTS, value))
                {
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        throw new InvalidOperationException("Cannot set the FixupChangeTrackingCollection when ChangeTracking is enabled");
                    }
                    if (_hR_INTERVIEWS_RESULTS != null)
                    {
                        _hR_INTERVIEWS_RESULTS.CollectionChanged -= FixupHR_INTERVIEWS_RESULTS;
                    }
                    _hR_INTERVIEWS_RESULTS = value;
                    if (_hR_INTERVIEWS_RESULTS != null)
                    {
                        _hR_INTERVIEWS_RESULTS.CollectionChanged += FixupHR_INTERVIEWS_RESULTS;
                    }
                    OnNavigationPropertyChanged("HR_INTERVIEWS_RESULTS");
                }
            }
        }
        private TrackableCollection<HR_INTERVIEWS_RESULTS> _hR_INTERVIEWS_RESULTS;
    
        [DataMember]
        public TrackableCollection<HR_VAC_CRITERIA_MASTER> HR_VAC_CRITERIA_MASTER
        {
            get
            {
                if (_hR_VAC_CRITERIA_MASTER == null)
                {
                    _hR_VAC_CRITERIA_MASTER = new TrackableCollection<HR_VAC_CRITERIA_MASTER>();
                    _hR_VAC_CRITERIA_MASTER.CollectionChanged += FixupHR_VAC_CRITERIA_MASTER;
                }
                return _hR_VAC_CRITERIA_MASTER;
            }
            set
            {
                if (!ReferenceEquals(_hR_VAC_CRITERIA_MASTER, value))
                {
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        throw new InvalidOperationException("Cannot set the FixupChangeTrackingCollection when ChangeTracking is enabled");
                    }
                    if (_hR_VAC_CRITERIA_MASTER != null)
                    {
                        _hR_VAC_CRITERIA_MASTER.CollectionChanged -= FixupHR_VAC_CRITERIA_MASTER;
                    }
                    _hR_VAC_CRITERIA_MASTER = value;
                    if (_hR_VAC_CRITERIA_MASTER != null)
                    {
                        _hR_VAC_CRITERIA_MASTER.CollectionChanged += FixupHR_VAC_CRITERIA_MASTER;
                    }
                    OnNavigationPropertyChanged("HR_VAC_CRITERIA_MASTER");
                }
            }
        }
        private TrackableCollection<HR_VAC_CRITERIA_MASTER> _hR_VAC_CRITERIA_MASTER;
    
        [DataMember]
        public TrackableCollection<HR_VAC_EVAL_CONDITION> HR_VAC_EVAL_CONDITION
        {
            get
            {
                if (_hR_VAC_EVAL_CONDITION == null)
                {
                    _hR_VAC_EVAL_CONDITION = new TrackableCollection<HR_VAC_EVAL_CONDITION>();
                    _hR_VAC_EVAL_CONDITION.CollectionChanged += FixupHR_VAC_EVAL_CONDITION;
                }
                return _hR_VAC_EVAL_CONDITION;
            }
            set
            {
                if (!ReferenceEquals(_hR_VAC_EVAL_CONDITION, value))
                {
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        throw new InvalidOperationException("Cannot set the FixupChangeTrackingCollection when ChangeTracking is enabled");
                    }
                    if (_hR_VAC_EVAL_CONDITION != null)
                    {
                        _hR_VAC_EVAL_CONDITION.CollectionChanged -= FixupHR_VAC_EVAL_CONDITION;
                    }
                    _hR_VAC_EVAL_CONDITION = value;
                    if (_hR_VAC_EVAL_CONDITION != null)
                    {
                        _hR_VAC_EVAL_CONDITION.CollectionChanged += FixupHR_VAC_EVAL_CONDITION;
                    }
                    OnNavigationPropertyChanged("HR_VAC_EVAL_CONDITION");
                }
            }
        }
        private TrackableCollection<HR_VAC_EVAL_CONDITION> _hR_VAC_EVAL_CONDITION;

        #endregion
        #region ChangeTracking
    
        protected virtual void OnPropertyChanged(String propertyName)
        {
            if (ChangeTracker.State != ObjectState.Added && ChangeTracker.State != ObjectState.Deleted)
            {
                ChangeTracker.State = ObjectState.Modified;
            }
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        protected virtual void OnNavigationPropertyChanged(String propertyName)
        {
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        event PropertyChangedEventHandler INotifyPropertyChanged.PropertyChanged{ add { _propertyChanged += value; } remove { _propertyChanged -= value; } }
        private event PropertyChangedEventHandler _propertyChanged;
        private ObjectChangeTracker _changeTracker;
    
        [DataMember]
        public ObjectChangeTracker ChangeTracker
        {
            get
            {
                if (_changeTracker == null)
                {
                    _changeTracker = new ObjectChangeTracker();
                    _changeTracker.ObjectStateChanging += HandleObjectStateChanging;
                }
                return _changeTracker;
            }
            set
            {
                if(_changeTracker != null)
                {
                    _changeTracker.ObjectStateChanging -= HandleObjectStateChanging;
                }
                _changeTracker = value;
                if(_changeTracker != null)
                {
                    _changeTracker.ObjectStateChanging += HandleObjectStateChanging;
                }
            }
        }
    
        private void HandleObjectStateChanging(object sender, ObjectStateChangingEventArgs e)
        {
            if (e.NewState == ObjectState.Deleted)
            {
                ClearNavigationProperties();
            }
        }
    
        protected bool IsDeserializing { get; private set; }
    
        [OnDeserializing]
        public void OnDeserializingMethod(StreamingContext context)
        {
            IsDeserializing = true;
        }
    
        [OnDeserialized]
        public void OnDeserializedMethod(StreamingContext context)
        {
            IsDeserializing = false;
            ChangeTracker.ChangeTrackingEnabled = true;
        }
    
        protected virtual void ClearNavigationProperties()
        {
            HR_APP_SHORT_LIST.Clear();
            HR_DEPARTMENTS = null;
            HR_DEPT_DESIGNATIONS = null;
            HR_INTERVIEWS_HDR.Clear();
            HR_INTERVIEWS_RESULTS.Clear();
            HR_VAC_CRITERIA_MASTER.Clear();
            HR_VAC_EVAL_CONDITION.Clear();
        }

        #endregion
        #region Association Fixup
    
        private void FixupHR_DEPARTMENTS(HR_DEPARTMENTS previousValue, bool skipKeys = false)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (previousValue != null && previousValue.HR_VACANCIES.Contains(this))
            {
                previousValue.HR_VACANCIES.Remove(this);
            }
    
            if (HR_DEPARTMENTS != null)
            {
                if (!HR_DEPARTMENTS.HR_VACANCIES.Contains(this))
                {
                    HR_DEPARTMENTS.HR_VACANCIES.Add(this);
                }
    
                VAC_DEPT_ID = HR_DEPARTMENTS.DEPT_ID;
            }
            else if (!skipKeys)
            {
                VAC_DEPT_ID = null;
            }
    
            if (ChangeTracker.ChangeTrackingEnabled)
            {
                if (ChangeTracker.OriginalValues.ContainsKey("HR_DEPARTMENTS")
                    && (ChangeTracker.OriginalValues["HR_DEPARTMENTS"] == HR_DEPARTMENTS))
                {
                    ChangeTracker.OriginalValues.Remove("HR_DEPARTMENTS");
                }
                else
                {
                    ChangeTracker.RecordOriginalValue("HR_DEPARTMENTS", previousValue);
                }
                if (HR_DEPARTMENTS != null && !HR_DEPARTMENTS.ChangeTracker.ChangeTrackingEnabled)
                {
                    HR_DEPARTMENTS.StartTracking();
                }
            }
        }
    
        private void FixupHR_DEPT_DESIGNATIONS(HR_DEPT_DESIGNATIONS previousValue, bool skipKeys = false)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (previousValue != null && previousValue.HR_VACANCIES.Contains(this))
            {
                previousValue.HR_VACANCIES.Remove(this);
            }
    
            if (HR_DEPT_DESIGNATIONS != null)
            {
                if (!HR_DEPT_DESIGNATIONS.HR_VACANCIES.Contains(this))
                {
                    HR_DEPT_DESIGNATIONS.HR_VACANCIES.Add(this);
                }
    
                VAC_DESIG_ID = HR_DEPT_DESIGNATIONS.DEPT_DESIG_ID;
            }
            else if (!skipKeys)
            {
                VAC_DESIG_ID = null;
            }
    
            if (ChangeTracker.ChangeTrackingEnabled)
            {
                if (ChangeTracker.OriginalValues.ContainsKey("HR_DEPT_DESIGNATIONS")
                    && (ChangeTracker.OriginalValues["HR_DEPT_DESIGNATIONS"] == HR_DEPT_DESIGNATIONS))
                {
                    ChangeTracker.OriginalValues.Remove("HR_DEPT_DESIGNATIONS");
                }
                else
                {
                    ChangeTracker.RecordOriginalValue("HR_DEPT_DESIGNATIONS", previousValue);
                }
                if (HR_DEPT_DESIGNATIONS != null && !HR_DEPT_DESIGNATIONS.ChangeTracker.ChangeTrackingEnabled)
                {
                    HR_DEPT_DESIGNATIONS.StartTracking();
                }
            }
        }
    
        private void FixupHR_APP_SHORT_LIST(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (e.NewItems != null)
            {
                foreach (HR_APP_SHORT_LIST item in e.NewItems)
                {
                    item.HR_VACANCIES = this;
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        if (!item.ChangeTracker.ChangeTrackingEnabled)
                        {
                            item.StartTracking();
                        }
                        ChangeTracker.RecordAdditionToCollectionProperties("HR_APP_SHORT_LIST", item);
                    }
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (HR_APP_SHORT_LIST item in e.OldItems)
                {
                    if (ReferenceEquals(item.HR_VACANCIES, this))
                    {
                        item.HR_VACANCIES = null;
                    }
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        ChangeTracker.RecordRemovalFromCollectionProperties("HR_APP_SHORT_LIST", item);
                    }
                }
            }
        }
    
        private void FixupHR_INTERVIEWS_HDR(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (e.NewItems != null)
            {
                foreach (HR_INTERVIEWS_HDR item in e.NewItems)
                {
                    item.HR_VACANCIES = this;
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        if (!item.ChangeTracker.ChangeTrackingEnabled)
                        {
                            item.StartTracking();
                        }
                        ChangeTracker.RecordAdditionToCollectionProperties("HR_INTERVIEWS_HDR", item);
                    }
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (HR_INTERVIEWS_HDR item in e.OldItems)
                {
                    if (ReferenceEquals(item.HR_VACANCIES, this))
                    {
                        item.HR_VACANCIES = null;
                    }
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        ChangeTracker.RecordRemovalFromCollectionProperties("HR_INTERVIEWS_HDR", item);
                    }
                }
            }
        }
    
        private void FixupHR_INTERVIEWS_RESULTS(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (e.NewItems != null)
            {
                foreach (HR_INTERVIEWS_RESULTS item in e.NewItems)
                {
                    item.HR_VACANCIES = this;
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        if (!item.ChangeTracker.ChangeTrackingEnabled)
                        {
                            item.StartTracking();
                        }
                        ChangeTracker.RecordAdditionToCollectionProperties("HR_INTERVIEWS_RESULTS", item);
                    }
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (HR_INTERVIEWS_RESULTS item in e.OldItems)
                {
                    if (ReferenceEquals(item.HR_VACANCIES, this))
                    {
                        item.HR_VACANCIES = null;
                    }
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        ChangeTracker.RecordRemovalFromCollectionProperties("HR_INTERVIEWS_RESULTS", item);
                    }
                }
            }
        }
    
        private void FixupHR_VAC_CRITERIA_MASTER(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (e.NewItems != null)
            {
                foreach (HR_VAC_CRITERIA_MASTER item in e.NewItems)
                {
                    item.HR_VACANCIES = this;
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        if (!item.ChangeTracker.ChangeTrackingEnabled)
                        {
                            item.StartTracking();
                        }
                        ChangeTracker.RecordAdditionToCollectionProperties("HR_VAC_CRITERIA_MASTER", item);
                    }
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (HR_VAC_CRITERIA_MASTER item in e.OldItems)
                {
                    if (ReferenceEquals(item.HR_VACANCIES, this))
                    {
                        item.HR_VACANCIES = null;
                    }
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        ChangeTracker.RecordRemovalFromCollectionProperties("HR_VAC_CRITERIA_MASTER", item);
                    }
                }
            }
        }
    
        private void FixupHR_VAC_EVAL_CONDITION(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (e.NewItems != null)
            {
                foreach (HR_VAC_EVAL_CONDITION item in e.NewItems)
                {
                    item.HR_VACANCIES = this;
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        if (!item.ChangeTracker.ChangeTrackingEnabled)
                        {
                            item.StartTracking();
                        }
                        ChangeTracker.RecordAdditionToCollectionProperties("HR_VAC_EVAL_CONDITION", item);
                    }
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (HR_VAC_EVAL_CONDITION item in e.OldItems)
                {
                    if (ReferenceEquals(item.HR_VACANCIES, this))
                    {
                        item.HR_VACANCIES = null;
                    }
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        ChangeTracker.RecordRemovalFromCollectionProperties("HR_VAC_EVAL_CONDITION", item);
                    }
                }
            }
        }

        #endregion
    }
}
