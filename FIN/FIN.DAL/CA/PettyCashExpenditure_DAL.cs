﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;

namespace FIN.DAL.CA
{
    public class PettyCashExpenditure_DAL
    {
        static string sqlQuery = "";
        public static string GetPettyCashExpendituredtls(string Petty_id)
        {
            sqlQuery = string.Empty;
            sqlQuery = "   SELECT to_char(PCD.GL_EXP_AMOUNT) as GL_EXP_AMOUNT,PCD.GL_ACCT_CODE_ID,PCD.GL_PCE_REMARKS,PCD.GL_PCE_DTL_ID, ";
            sqlQuery += "      (select AC.ACCT_CODE||' - '||AC.ACCT_CODE_DESC" + VMVServices.Web.Utils.LanguageCode + " AS ACCT_CODE";
            sqlQuery += "     from GL_ACCT_CODES AC";
            sqlQuery += "     where ac.acct_code_id = PCD.GL_ACCT_CODE_ID) as ACCT_CODE,";
            sqlQuery += "    (select AC.ACCT_CODE_ID";
            sqlQuery += "     from GL_ACCT_CODES AC";
            sqlQuery += "    where ac.acct_code_id = PCD.GL_ACCT_CODE_ID) as ACCT_CODE_ID,";
            sqlQuery += "   case PCD.GL_PCE_POSTED when '1' then 'TRUE' else 'FALSE' end as GL_PCE_POSTED, ";
            sqlQuery += "   case PCD.ENABLED_FLAG when '1' then 'TRUE' else 'FALSE' end as ENABLED_FLAG,'N' as DELETED,";
            sqlQuery += "   case NVL(PCD.POSTED_FLAG,0) when '1' then 'FALSE' else 'TRUE' end as POSTED_FLAG ";
            sqlQuery += "   ,PCD.JE_SEGMENT_ID_1,PCD.JE_SEGMENT_ID_2,PCD.JE_SEGMENT_ID_3,PCD.JE_SEGMENT_ID_4,PCD.JE_SEGMENT_ID_5,PCD.JE_SEGMENT_ID_6";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = PCD.JE_SEGMENT_ID_1 ) AS SEGMENT_1_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = PCD.JE_SEGMENT_ID_2 ) AS SEGMENT_2_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = PCD.JE_SEGMENT_ID_3 ) AS SEGMENT_3_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = PCD.JE_SEGMENT_ID_4 ) AS SEGMENT_4_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = PCD.JE_SEGMENT_ID_5 ) AS SEGMENT_5_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = PCD.JE_SEGMENT_ID_6 ) AS SEGMENT_6_TEXT";
            sqlQuery += "   FROM  GL_PETTY_CASH_EXPENDITURE_DTL PCD ";
            sqlQuery += "   where  ";
            sqlQuery += "   PCD.GL_PCE_HDR_ID = '" + Petty_id + "'";

            return sqlQuery;
        }
        public static string GetAccountdtls()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT AC.ACCT_CODE||' - '||AC.ACCT_CODE_DESC" + VMVServices.Web.Utils.LanguageCode + " AS ACCT_CODE,AC.ACCT_CODE_ID ";
            sqlQuery += " FROM GL_ACCT_CODES AC ";
            sqlQuery += " where AC.workflow_completion_status=1";
            sqlQuery += " and AC.enabled_flag=1";
            sqlQuery += " order by AC.ACCT_CODE asc";

            return sqlQuery;
        }

        public static string GetUserdtls()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT U.USER_CODE,U.FIRST_NAME||''||U.MIDDLE_NAME||''||U.LAST_NAME AS USER_NAME ";
            sqlQuery += " FROM SSM_USERS U ";
            sqlQuery += " where U.workflow_completion_status=1";
            sqlQuery += " and U.enabled_flag=1";
            sqlQuery += " order by U.USER_CODE asc";

            return sqlQuery;
        }

        public static string GetUserEmpDtl()
        {
            sqlQuery = string.Empty;
            sqlQuery += "  SELECT U.USER_CODE,u.user_code||' - '||e.emp_first_name||' '||e.emp_last_name as USER_NAME";
            sqlQuery += "  FROM SSM_USERS U,hr_employees e";
            sqlQuery += "  where U.workflow_completion_status=1";
            sqlQuery += "  and u.passport_number_or_emp_id = e.emp_id";
            sqlQuery += "  and U.enabled_flag=1";
            sqlQuery += "  order by U.USER_CODE asc";

            return sqlQuery;
        }

        public static string GetBalanceAmt()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT CAH.GL_PCA_BALANCE_AMOUNT ";
            sqlQuery += " FROM GL_PETTY_CASH_ALLOCATE_HDR CAH ";
            sqlQuery += " where CAH.workflow_completion_status=1";
            sqlQuery += " and CAH.enabled_flag=1";
            sqlQuery += " order by CAH.GL_PCA_BALANCE_AMOUNT asc";

            return sqlQuery;
        }

        public static string GetBalAmt(string gl_pca_user_id)
        {
            sqlQuery = string.Empty;

            //sqlQuery += " select nvl((pcd.gl_pca_amount)+(pcd.gl_pca_balance_amount),0) as gl_pca_balance_amount";
           
            sqlQuery += " select GL_PCA_DTL_ID, nvl((pcd.gl_pca_balance_amount),0) as gl_pca_balance_amount";
            sqlQuery += " ,nvl((select sum(pced.gl_exp_amount) from gl_petty_cash_expenditure_hdr pceh ";
            sqlQuery += " inner join gl_petty_cash_expenditure_dtl pced on pceh.gl_pce_hdr_id = pced.gl_pce_hdr_id ";
            sqlQuery += " where pceh.alloc_dtl_id =pcd.GL_PCA_DTL_ID ),0) as Exp_Amount ";
            sqlQuery += " from Gl_Petty_Cash_Allocate_Dtl pcd";
            sqlQuery += " where pcd.workflow_completion_status = 1";
            sqlQuery += " and pcd.gl_pca_user_id = '" + gl_pca_user_id + "'";
            sqlQuery += " and rownum=1 order by GL_PCA_DTL_ID desc";
            return sqlQuery;
        }


        public static string GetPettyCashDtls(string FromDate, string ToDate = "")
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT AC.ACCT_CODE, AC.ACCT_CODE_ID,PCD.GL_EXP_AMOUNT,PCD.GL_ACCT_CODE_ID,PCD.GL_PCE_REMARKS,PCD.GL_PCE_DTL_ID, ";
            sqlQuery += "case PCD.GL_PCE_POSTED when '1' then 'TRUE' else 'FALSE' end as GL_PCE_POSTED, ";
            sqlQuery += " case PCD.ENABLED_FLAG when '1' then 'TRUE' else 'FALSE' end as ENABLED_FLAG,'N' as DELETED";
            sqlQuery += " FROM GL_ACCT_CODES AC, GL_PETTY_CASH_EXPENDITURE_DTL PCD, GL_PETTY_CASH_EXPENDITURE_HDR PCH";
            sqlQuery += " where AC.ACCT_CODE_ID = PCD.GL_ACCT_CODE_ID ";
            sqlQuery += " and PCD.GL_PCE_HDR_ID = PCH.GL_PCE_HDR_ID";
            if (FromDate != null || ToDate != null)
            {
                sqlQuery += " and pch.gl_expd_date between to_date('" + FromDate + "','dd/MM/yyyy') and to_date('" + ToDate + "','dd/MM/yyyy')";
            }
            return sqlQuery;
        }
        public static string GetPettyBalAmount(string empId)
        {
            sqlQuery = string.Empty;

            sqlQuery += "  select sum(nvl(ad.gl_pca_amount,0))-sum(nvl(dd.gl_exp_amount,0)) as gl_exp_amount,ad.PREVIOUS_BAL_AMOUNT";
            sqlQuery += "  from gl_petty_cash_expenditure_hdr h,";
            sqlQuery += "   gl_petty_cash_expenditure_dtl dd,";
            sqlQuery += "    gl_petty_cash_allocate_hdr    a,";
            sqlQuery += "    gl_petty_cash_allocate_dtl    ad";
            sqlQuery += "  where h.gl_pce_hdr_id = dd.gl_pce_hdr_id";
            sqlQuery += "  and a.gl_pca_hdr_id = ad.gl_pca_hdr_id";
            sqlQuery += "  and ad.gl_pca_user_id = h.gl_user_id";
            sqlQuery += "  and ad.gl_pca_user_id ='" + empId + "'";
            sqlQuery += "   group by PREVIOUS_BAL_AMOUNT";
            return sqlQuery;
        }

        public static string GetPettyCashExpenditureReportDtls(string pcHdrId)
        {
            sqlQuery = string.Empty;

            sqlQuery = "select * from vw_petty_cash_expenditure V ";

            if (pcHdrId.Trim().Length > 0)
            {
                sqlQuery += " where V.gl_pce_hdr_id='" + pcHdrId + "'";
            }

            return sqlQuery;
        }

    }
}
