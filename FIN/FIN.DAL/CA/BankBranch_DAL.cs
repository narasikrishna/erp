﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data.EntityClient;
using System.Data;
namespace FIN.DAL.CA
{
    public class BankBranch_DAL
    {
        static string sqlQuery = "";

        public static string getBranchName(string Master_id)
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT CBB.BANK_BRANCH_ID AS BANK_BRANCH_ID, CBB.BANK_BRANCH_NAME" + VMVServices.Web.Utils.LanguageCode + " AS BANK_BRANCH_NAME ";
            sqlQuery += " FROM CA_BANK_BRANCH CBB ";
            sqlQuery += " WHERE CBB.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " AND CBB.ENABLED_FLAG = 1 ";
            sqlQuery += " AND CBB.BANK_ID = '" + Master_id + "'";
            sqlQuery += " ORDER BY BANK_BRANCH_NAME ";

            return sqlQuery;
        }
        public static string getBankAccountNumber(string Bank_id)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select bc.account_id,bc.bank_branch_id from   ca_bank cb,";
            sqlQuery += " ca_bank_branch bb ,";
            sqlQuery += " ca_bank_accounts bc";
            sqlQuery += " where bc.bank_branch_id=bb.bank_branch_id";
            //sqlQuery += " and bc.bank_id=bb.bank_id";
            sqlQuery += " and cb.bank_id=bb.bank_id";
            sqlQuery += " and bc.bank_id='" + Bank_id + "'";

            sqlQuery += " and bc.enabled_flag=1";
            sqlQuery += " and bc.workflow_completion_status=1";
            sqlQuery += " order by bc.account_id";



            return sqlQuery;
        }



        public static string GetSPFOR_ERR_MGR_BANK_BRANCH(string P_BANK_ID, string P_BANK_BRANCH_NAME, string P_RECORD_ID)
        {
            try
            {
                string retcode = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "PKG_VALIDATIONS.PROC_ERR_MGR_BANK_BRANCH";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add("@P_MODULE_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_SCREEN_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_BANK_ID", OracleDbType.Char).Value = P_BANK_ID;
                oraCmd.Parameters.Add("@P_BANK_BRANCH_NAME", OracleDbType.Char).Value = P_BANK_BRANCH_NAME;
                oraCmd.Parameters.Add("@P_RECORD_ID", OracleDbType.Char).Value = P_RECORD_ID;
                //oraCmd.Parameters.Add("@P_FROM_DATE", OracleDbType.Char).Value = P_FROM_DATE;
                //oraCmd.Parameters.Add("@P_TO_DATE", OracleDbType.Char).Value = P_TO_DATE;
                oraCmd.Parameters.Add("@P_RET", OracleDbType.Varchar2, 150);
                oraCmd.Parameters["@P_RET"].Direction = ParameterDirection.Output;
                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

                retcode = oraCmd.Parameters["@P_RET"].Value.ToString();


                return retcode;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


    }
}
