﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.CA
{
    public class BankCashRemittance_DAL
    {

        static string sqlQuery = "";


        public static string GetCashRemittancedtls(string Master_id)
        {
            sqlQuery = string.Empty;

            sqlQuery += " select cr.remit_dtl_id,cr.remit_trans_type AS LOOKUP_ID,cr.remit_trans_type AS LOOKUP_NAME,cr.remit_trans_date,cr.remit_trans_id,to_char(cr.remit_trans_amount) as remit_trans_amount,cr.attribute10,'N' AS DELETED";
            sqlQuery += " from CA_BANK_REMITTANCE_DTL cr";
            sqlQuery += " where cr.workflow_completion_status = 1";
            sqlQuery += " and cr.remit_id = '" + Master_id + "'";
            return sqlQuery;

        }

    }
}
