﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data.EntityClient;
using System.Data;

namespace FIN.DAL.CA
{
    public class BankAccounts_DAL
    {

        static string sqlQuery = "";


        public static string getBankBranchShortName(string StrID)
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT CBB.BANK_BRANCH_ID,CBB.ATTRIBUTE2,CBB.BANK_IFSC_CODE,CBB.BANK_SWIFT_CODE ";
            sqlQuery += " FROM CA_BANK_BRANCH CBB ";
            sqlQuery += " WHERE CBB.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " AND CBB.ENABLED_FLAG = 1 ";
            sqlQuery += " AND CBB.BANK_BRANCH_ID = '" + StrID + "'";



            return sqlQuery;
        }
        public static string getBankAccount(string bankId, string bankBranchId)
        {
            sqlQuery = string.Empty;

            sqlQuery += "       select cb.BANK_ID,";
            sqlQuery += "   cb.BANK_BRANCH_ID,";
            sqlQuery += "   cb.VENDOR_BANK_ACCOUNT_CODE,";
            sqlQuery += "   cb.VENDOR_BANK_ACCTOUNT_TYPE,";
            sqlQuery += "    ACCOUNT_ID";
            sqlQuery += "  from ca_bank_accounts cb";
            sqlQuery += "  where cb.workflow_completion_status='1'";
            sqlQuery += " and cb.enabled_flag='1'";
            sqlQuery += " and cb.BANK_ID='" + bankId + "'";
            sqlQuery += " and cb.BANK_BRANCH_ID='" + bankBranchId + "'";

            return sqlQuery;
        }


        public static string getBankFullDetails()
        {
            sqlQuery = string.Empty;
            sqlQuery += " SELECT CB.BANK_ID, CB.BANK_NAME,CB.BANK_SHORT_NAME  ";
            sqlQuery += " ,CBB.BANK_BRANCH_ID,CBB.BANK_BRANCH_NAME,CBB.BANK_IFSC_CODE,CBB.BANK_SWIFT_CODE ";
            sqlQuery += " ,CBA.ACCOUNT_ID,CBA.VENDOR_BANK_ACCOUNT_CODE ";
            sqlQuery += " ,CB.BANK_SHORT_NAME  || ' ' || ' ' || CBB.BANK_BRANCH_NAME || ' ' || CBA.VENDOR_BANK_ACCOUNT_CODE AS BANK_FULL_NAME ";
            sqlQuery += " FROM CA_BANK CB ";
            sqlQuery += " INNER JOIN CA_BANK_BRANCH CBB ON CB.BANK_ID= CBB.BANK_ID ";
            sqlQuery += " INNER JOIN CA_BANK_ACCOUNTS CBA ON CBA.BANK_BRANCH_ID= CBB.BANK_BRANCH_ID ";
            sqlQuery += " WHERE CBA.ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            return sqlQuery;
        }
        public static string GetSPFOR_ERR_MGR_BANK_ACCOUNTS(string P_BANK_ID, string P_BANK_BRANCH_NAME, String P_ACCOUNT_CODE, string P_RECORD_ID)
        {
            try
            {
                string retcode = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "PKG_VALIDATIONS.PROC_ERR_MGR_BANK_ACCOUNTS";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add("@P_MODULE_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_SCREEN_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_BANK_ID", OracleDbType.Char).Value = P_BANK_ID;
                oraCmd.Parameters.Add("@P_BANK_BRANCH_NAME", OracleDbType.Char).Value = P_BANK_BRANCH_NAME;
                oraCmd.Parameters.Add("@P_ACCOUNT_CODE", OracleDbType.Char).Value = P_ACCOUNT_CODE;
                oraCmd.Parameters.Add("@P_RECORD_ID", OracleDbType.Char).Value = P_RECORD_ID;
                //oraCmd.Parameters.Add("@P_FROM_DATE", OracleDbType.Char).Value = P_FROM_DATE;
                //oraCmd.Parameters.Add("@P_TO_DATE", OracleDbType.Char).Value = P_TO_DATE;
                oraCmd.Parameters.Add("@P_RET", OracleDbType.Varchar2, 150);
                oraCmd.Parameters["@P_RET"].Direction = ParameterDirection.Output;
                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

                retcode = oraCmd.Parameters["@P_RET"].Value.ToString();


                return retcode;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



    }
}
