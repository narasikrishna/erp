﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.CA
{
   public class PettyCashBook_DAL
    {
        static string sqlQuery = "";
        public static string getReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM GL_PETTY_CASH_BOOK V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["GL_PCA_USER_ID"] != null)
                {
                    sqlQuery += " AND V.GL_PCA_USER_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["GL_PCA_USER_ID"].ToString() + "'";
                }
            }
            return sqlQuery;
        }
        public static string getGlobalSegment()
        {
            sqlQuery = string.Empty;
            sqlQuery += " SELECT SV.SEGMENT_VALUE_ID,SV.SEGMENT_VALUE" + VMVServices.Web.Utils.LanguageCode;
            sqlQuery += " FROM GL_SEGMENT_VALUES SV ";
            sqlQuery += " WHERE SV.WORKFLOW_COMPLETION_STATUS='1' ";
            sqlQuery += " AND SV.ENABLED_FLAG='1'";


            return sqlQuery;
        }

        public static string getUser()
        {
            sqlQuery = string.Empty;
            sqlQuery += " SELECT PCAD.GL_PCA_USER_ID  ";
            sqlQuery += " FROM GL_PETTY_CASH_ALLOCATE_DTL PCAD ";
            sqlQuery += " WHERE PCAD.ENABLED_FLAG = 1 ";
            sqlQuery += " AND PCAD.WORKFLOW_COMPLETION_STATUS = 1 ";
            return sqlQuery;
        }
    }
}