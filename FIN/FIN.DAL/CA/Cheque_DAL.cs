﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.CA
{
    public class Cheque_DAL
    {
        static string sqlQuery = "";


        public static string GetChequeDetails(string Master_id)
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT CCD.CHECK_DTL_ID, CCD.CHECK_HDR_ID, CCD.CHECK_LINE_NUM, CCD.CHECK_NUMBER, CCD.CHECK_DT, CCD.CHECK_PAY_TO, to_char(CCD.CHECK_AMT) as CHECK_AMT, CCD.CHECK_STATUS AS LOOKUP_ID,CCD.CHECK_STATUS AS LOOKUP_NAME,CCD.CHECK_CANCEL_REMARKS, ";
            sqlQuery += " CASE CCD.CHECK_CROSSED_YN WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS CHECK_CROSSED_YN,";
            sqlQuery += " case CCD.ENABLED_FLAG when '1' then 'TRUE' else 'FALSE' end as ENABLED_FLAG,'N' as DELETED ";
            sqlQuery += " FROM CA_CHECK_DTL CCD ";
            sqlQuery += " WHERE CCD.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " AND CCD.ENABLED_FLAG = 1 ";
            sqlQuery += " AND CCD.CHECK_HDR_ID = '" + Master_id + "'";
            sqlQuery += " ORDER BY CHECK_DTL_ID ";

            return sqlQuery;

        }

        public static string GetChequeCreationDetails()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT CCCT.CHECK_LINE_NUM,ccct.check_number,CCCT.CHECK_PAY_TO,ccct.check_dt,to_char(ccct.check_amt) as check_amt,ccct.check_status as LOOKUP_ID,ccct.check_status AS LOOKUP_NAME,'' as CHECK_DTL_ID, '' as CHECK_HDR_ID, '' as CHECK_CANCEL_REMARKS, 'N' AS DELETED";
            sqlQuery += " , 'TRUE' AS CHECK_CROSSED_YN";
            sqlQuery += " FROM CA_CHEQUE_CREATION_TEMP_TABLE CCCT ";
            //  sqlQuery += " ORDER BY CCCT.CHECK_LINE_NUM ";

            return sqlQuery;

        }



        public static string GetAccountNumberDetails(string Master_id1, string Master_id2)
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT CBA.ACCOUNT_ID, CBA.VENDOR_BANK_ACCOUNT_CODE ";
            sqlQuery += " FROM CA_BANK_ACCOUNTS CBA ";
            sqlQuery += " WHERE CBA.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " AND CBA.ENABLED_FLAG = 1 ";
            sqlQuery += " AND CBA.BANK_ID = '" + Master_id1 + "'";
            sqlQuery += " AND CBA.BANK_BRANCH_ID = '" + Master_id2 + "'";
            sqlQuery += " ORDER BY ACCOUNT_ID ";

            return sqlQuery;

        }
        public static string GetCheckNumberDetails(string bankId = "", string bankBranchId = "", string accountId = "")
        {
            sqlQuery = string.Empty;

            sqlQuery = " select cd.check_number,cd.check_dtl_id,cd.check_dt from CA_CHECK_DTL cd, CA_CHECK_HDR ch";
            sqlQuery += " where cd.check_hdr_id=ch.check_hdr_id";
            sqlQuery += " and ch.check_bank_id='" + bankId + "'";
            sqlQuery += " and ch.check_branch_id='" + bankBranchId + "'";
            sqlQuery += " and ch.ACCOUNT_ID='" + accountId + "'";
            sqlQuery += " and  ch.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " AND ch.ENABLED_FLAG = 1 ";
            return sqlQuery;

        }

        public static string GetUnusedCheck(string bankId = "", string bankBranchId = "", string accountId = "", string str_Mode = "")
        {
            sqlQuery = string.Empty;
            sqlQuery = "  select cd.check_number,cd.check_dtl_id,cd.check_dt from CA_CHECK_DTL cd, CA_CHECK_HDR ch";
            sqlQuery += "   where cd.check_hdr_id=ch.check_hdr_id";
            sqlQuery += "   and ch.check_bank_id='" + bankId + "'";
            sqlQuery += "  and ch.check_branch_id='" + bankBranchId + "'";
            sqlQuery += "   and ch.ACCOUNT_ID='" + accountId + "'";
            sqlQuery += "   and  ch.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += "   AND ch.ENABLED_FLAG = 1 ";
            if (FINTableConstant.Add == str_Mode)
            {
                sqlQuery += " AND CD.CHECK_STATUS='UNUSED'";
            }
            sqlQuery += " ORDER BY CD.CHECK_DTL_ID";
            return sqlQuery;
        }
        public static string GetUnusedCheckNumberDetails(string bankId = "", string bankBranchId = "", string accountId = "", string str_Mode = "")
        {
            sqlQuery = string.Empty;

            sqlQuery = "  select cd.check_number,cd.check_dtl_id,cd.check_dt from CA_CHECK_DTL cd, CA_CHECK_HDR ch";
            sqlQuery += "   where cd.check_hdr_id=ch.check_hdr_id";
            sqlQuery += "   and ch.check_bank_id='" + bankId + "'";
            sqlQuery += "  and ch.check_branch_id='" + bankBranchId + "'";
            sqlQuery += "   and ch.ACCOUNT_ID='" + accountId + "'";
            sqlQuery += "   and  ch.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += "   AND ch.ENABLED_FLAG = 1 ";
            if (FINTableConstant.Add == str_Mode)
            {
                sqlQuery += "   and not exists";
                sqlQuery += "  (select pd.PAY_CHECK_NUMBER";
                sqlQuery += "    from inv_payments_hdr pd";
                sqlQuery += "   where pd.PAY_CHECK_NUMBER = cd.check_dtl_id and REV_FLAG is null)";
                sqlQuery += " AND CD.CHECK_STATUS='UNUSED'";
            }
            sqlQuery += " ORDER BY CD.CHECK_DTL_ID";
            return sqlQuery;

        }
        public static string GetCheckNumberDetails(string chequeNumber = "")
        {
            sqlQuery = string.Empty;

            sqlQuery = " select cd.check_number,cd.check_dtl_id,cd.check_dt from CA_CHECK_DTL cd, CA_CHECK_HDR ch";
            sqlQuery += " where cd.check_hdr_id=ch.check_hdr_id";
            sqlQuery += " and cd.check_dtl_id='" + chequeNumber + "'";

            sqlQuery += " and  ch.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " AND ch.ENABLED_FLAG = 1 ";
            return sqlQuery;

        }

        public static string getCheckNumberDetails(string Account_id)
        {
            {
                sqlQuery = string.Empty;

                sqlQuery = "select hd.pay_check_number from INV_PAYMENTS_HDR  HD";
                sqlQuery += " where hd.pay_account_id='" + Account_id + "'";
                sqlQuery += " and  hd.enabled_flag=1";
                sqlQuery += " and hd.workflow_completion_status=1";
                sqlQuery += " order by hd.pay_check_number";



                return sqlQuery;
            }
        }

        public static string getCheckViewData()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_CHECK_PRINT V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {

                if (VMVServices.Web.Utils.ReportViewFilterParameter["CHEQUEID"] != null)
                {
                    sqlQuery += " AND V.CHECK_DTL_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["CHEQUEID"] + "'";
                }



            }
            return sqlQuery;
        }

    }
}
