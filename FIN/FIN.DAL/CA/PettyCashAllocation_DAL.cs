﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.CA
{
    public class Pettycashallocation_DAL
    {
        static string sqlQuery = "";


        public static string GetPettycashallocationDetails(string Master_id)
        {
            sqlQuery = string.Empty;

            sqlQuery = "  SELECT PA.GL_PCA_DTL_ID,U.USER_CODE,u.user_code||' - '||e.emp_first_name||''||e.emp_last_name as USER_NAME,PA.GL_PCA_DATE,to_char(PA.GL_PCA_AMOUNT) as GL_PCA_AMOUNT,to_char(PA.GL_PCA_BALANCE_AMOUNT) as GL_PCA_BALANCE_AMOUNT,to_char(PA.previous_bal_amount) as  previous_bal_amount,PA.GL_PCA_REMARKS,";
            sqlQuery += "  CASE PA.ENABLED_FLAG WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS ENABLED_FLAG,'N' AS DELETED,";
            sqlQuery += "  CASE NVL(PA.POSTED_FLAG,0) WHEN '1' THEN 'FALSE' ELSE 'TRUE' END AS POSTED_FLAG ";
            sqlQuery += "  FROM GL_PETTY_CASH_ALLOCATE_DTL PA,SSM_USERS U,hr_employees e";
            sqlQuery += "  WHERE PA.GL_PCA_USER_ID = U.USER_CODE";
            sqlQuery += "  and u.passport_number_or_emp_id = e.emp_id";
            sqlQuery += "  AND PA.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += "  AND PA.GL_PCA_HDR_ID= '" + Master_id + "'";
            return sqlQuery;

        }

      
       
    }
}
