﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.CA
{
    public class Bank_DAL
    {
        static string sqlQuery = "";


        public static string getBankName()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT CB.BANK_ID AS BANK_ID, CB.BANK_NAME" + VMVServices.Web.Utils.LanguageCode + " AS BANK_NAME";
            sqlQuery += " FROM CA_BANK CB ";
            sqlQuery += " WHERE CB.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND CB.ENABLED_FLAG = 1 ";
            sqlQuery += " AND CB.ORG_ID = '" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " ORDER BY BANK_NAME";

            return sqlQuery;
        }

        public static string getBankShortName(string StrID)
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT CB.BANK_ID AS BANK_ID, CB.BANK_SHORT_NAME AS BANK_SHORT_NAME";
            sqlQuery += " FROM CA_BANK CB ";
            sqlQuery += " WHERE CB.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND CB.ENABLED_FLAG = 1 ";
            sqlQuery += " AND CB.ORG_ID = '" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " AND CB.BANK_ID = '" + StrID + "'";

            return sqlQuery;
        }
        public static string getBankBranchName()
        {
            sqlQuery = string.Empty;

            sqlQuery = "  SELECT bb.bank_branch_id,";
            sqlQuery += "  CB.BANK_NAME || ' - ' || bb.bank_branch_name AS BANK_NAME";
            sqlQuery += "  FROM CA_BANK CB, ca_bank_branch bb";
            sqlQuery += "  WHERE cb.bank_id = bb.bank_id";
            sqlQuery += "  and CB.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += "  AND CB.ENABLED_FLAG = 1";
            sqlQuery += "  AND CB.ORG_ID = '" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " ORDER BY BANK_NAME";
            return sqlQuery;
        }
        public static string getBankAccData(string bankBranchId)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select ba.* from ca_bank_accounts ba";
            sqlQuery += " where ba.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND ba.ENABLED_FLAG = 1";
            sqlQuery += " and ba.bank_branch_id='" + bankBranchId + "'";
            return sqlQuery;
        }
        public static string getOrgBankAccData(string bankBranchId)
        {
            sqlQuery = string.Empty;

            sqlQuery = "   SELECT CB.BANK_NAME AS BANK_NAME,";
            sqlQuery += "   bb.bank_ifsc_code,";
            sqlQuery += "   cbb.gl_account,";
            sqlQuery += "   cbb.vendor_bank_account_code";
            sqlQuery += "   FROM CA_BANK CB, ca_bank_branch bb, ca_bank_accounts cbb";
            sqlQuery += "   WHERE cb.bank_id = bb.bank_id";
            sqlQuery += "    and bb.bank_branch_id = cbb.bank_branch_id";
            sqlQuery += "   and cb.bank_id = cbb.bank_id";
            sqlQuery += "   and CB.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += "   AND CB.ENABLED_FLAG = 1";
            sqlQuery += "   and bb.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += "   AND bb.ENABLED_FLAG = 1";
            sqlQuery += "   AND CB.ORG_ID = '" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += "    and bb.bank_branch_id ='" + bankBranchId + "'";
            sqlQuery += "   ORDER BY BANK_NAME asc";
            return sqlQuery;
        }
        public static string getEmpAccData(string PayAdviceNo, string transDate)
        {
            sqlQuery = string.Empty;

            //sqlQuery = "    select e.emp_no,";
            //sqlQuery += "   e.emp_first_name || ' ' || e.emp_middle_name || ' ' ||";
            //sqlQuery += "    e.emp_last_name as emp_name,";
            //sqlQuery += "    eb.emp_iban_num,sum(pee.pay_amount) as salary,";
            //sqlQuery += "    eb.emp_bank_acct_code,'" + transDate + "' as tranDate";
            //sqlQuery += "   from hr_employees          e,";
            //sqlQuery += "    hR_EMP_BANK_DTLS      eb,";
            //sqlQuery += "    PAY_EMP_ELEMENT_VALUE pee,";
            //sqlQuery += "    PAY_ELEMENTS          pe,ca_bank_branch        bbb";
            //sqlQuery += "    where e.emp_id = eb.emp_id";
            //sqlQuery += "    and pee.pay_element_id = pe.pay_element_id";
            //sqlQuery += "   and pee.pay_emp_id = e.emp_id";
            //sqlQuery += "   and bbb.bank_id=eb.emp_bank_code";
            //sqlQuery += "   and bbb.bank_branch_id=eb.emp_bank_branch_code";
            //sqlQuery += "   and pee.ENABLED_FLAG = 1";
            //sqlQuery += "   and pee.WORKFLOW_COMPLETION_STATUS = 1";
            //sqlQuery += "    and bbb.bank_branch_id ='" + bankBranchId + "'";
            //sqlQuery += "    group by e.emp_no,";
            //sqlQuery += "   e.emp_first_name,e.emp_middle_name ,  e.emp_last_name,";
            //sqlQuery += "      eb.emp_iban_num,eb.emp_bank_acct_code";
            //sqlQuery += "   order by e.emp_no";

            sqlQuery += "   SELECT CBB.BANK_BRANCH_NAME,CBB.BANK_IFSC_CODE,CBA.VENDOR_BANK_ACCOUNT_CODE, FED.EMP_NO,FED.EMP_NAME" + VMVServices.Web.Utils.LanguageCode + " as EMP_NAME,EBD.EMP_IBAN_NUM, to_char(ROUND(FED.NET_DUE," + VMVServices.Web.Utils.DecimalPrecision + ")) as salary,EBD.EMP_BANK_ACCT_CODE ,'" + transDate + "' as tranDate ";
            sqlQuery += " ,nvl(PA.attribute6,0) as TRANSFER_STATUS ";
            sqlQuery += " ,'N' as DELETED ";
            sqlQuery += "   FROM PAY_ADVICE PA ";
            sqlQuery += " INNER JOIN PAY_FINAL_RUN_EMP_DTL FED ON FED.EMP_ID= PA.ATTRIBUTE1  and PA.PAYROLL_ID= FED.PAYROLL_ID ";
            sqlQuery += " INNER JOIN hr_emp_bank_dtls EBD ON EBD.PK_ID= EMP_BANK_ID ";
            sqlQuery += " INNER JOIN CA_BANK_ACCOUNTS CBA ON CBA.ACCOUNT_ID=PA.BANK_ID ";
            sqlQuery += " INNER JOIN CA_BANK_BRANCH CBB ON CBA.BANK_BRANCH_ID= CBB.BANK_BRANCH_ID ";
            sqlQuery += " INNER JOIN CA_BANK CB ON CB.BANK_ID= CBB.BANK_ID ";

            sqlQuery += " WHERE PA.ATTRIBUTE5='" + PayAdviceNo + "' ";
            sqlQuery += " ORDER BY TO_NUMBER(FED.EMP_NO) ";
            return sqlQuery;
        }
        public static string getExportData(string bankBranchId, string transDate)
        {
            sqlQuery = string.Empty;

            sqlQuery = "     SELECT ";
            sqlQuery += "   CB.BANK_NAME AS BANK_NAME,";
            sqlQuery += "   bb.bank_ifsc_code,";
            sqlQuery += "   cbb.gl_account,";
            sqlQuery += "   cbb.vendor_bank_account_code,";
            sqlQuery += "    '' as emp_no,";
            sqlQuery += "    '' as emp_name,";
            sqlQuery += "    '' emp_iban_num,null salary,";
            sqlQuery += "    '' as emp_bank_acct_code,'' as tranDate";
            sqlQuery += "   FROM CA_BANK CB, ca_bank_branch bb, ca_bank_accounts cbb";
            sqlQuery += "   WHERE cb.bank_id = bb.bank_id";
            sqlQuery += "    and bb.bank_branch_id = cbb.bank_branch_id";
            sqlQuery += "   and cb.bank_id = cbb.bank_id";
            sqlQuery += "   and CB.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += "   AND CB.ENABLED_FLAG = 1";
            sqlQuery += "   and bb.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += "   AND bb.ENABLED_FLAG = 1";
            sqlQuery += "    and bb.bank_branch_id ='" + bankBranchId + "'";

            sqlQuery += "  union ";

            sqlQuery += "  select ";
            sqlQuery += "   '' as BANK_NAME,";
            sqlQuery += "   '' as bank_ifsc_code,";
            sqlQuery += "   '' as gl_account,";
            sqlQuery += "   '' as vendor_bank_account_code,";
            sqlQuery += "   e.emp_no,";
            sqlQuery += "   e.emp_first_name || ' ' || e.emp_middle_name || ' ' ||";
            sqlQuery += "   e.emp_last_name as emp_name,";
            sqlQuery += "   eb.emp_iban_num,sum(pee.pay_amount) as salary,";
            sqlQuery += "   eb.emp_bank_acct_code,'" + transDate + "' as tranDate";
            sqlQuery += "   from hr_employees          e,";
            sqlQuery += "    ca_bank_branch        bbb,";
            sqlQuery += "    hR_EMP_BANK_DTLS      eb,";
            sqlQuery += "   PAY_EMP_ELEMENT_VALUE pee,";
            sqlQuery += "   PAY_ELEMENTS          pe";
            sqlQuery += "  where e.emp_id = eb.emp_id";
            sqlQuery += "   and pee.pay_element_id = pe.pay_element_id";
            sqlQuery += "   and pee.pay_emp_id = e.emp_id";
            sqlQuery += "  and bbb.bank_id = eb.emp_bank_code";
            sqlQuery += "  and bbb.bank_branch_id = eb.emp_bank_branch_code";
            sqlQuery += "  and pee.ENABLED_FLAG = 1";
            sqlQuery += "  and pee.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += "    and bbb.bank_branch_id ='" + bankBranchId + "'";
            sqlQuery += "    group by e.emp_no,";
            sqlQuery += "   e.emp_first_name,e.emp_middle_name ,  e.emp_last_name,";
            sqlQuery += "      eb.emp_iban_num,eb.emp_bank_acct_code";
            sqlQuery += "  order by BANK_NAME,emp_no";
            return sqlQuery;
        }

        public static string getBankBranchAcctNo(string bankID = "")
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT CB.BANK_ID AS BANK_ID, CB.BANK_NAME" + VMVServices.Web.Utils.LanguageCode + " || ' - ' || cbb.bank_branch_name" + VMVServices.Web.Utils.LanguageCode + " || ' - ' || cba.vendor_bank_account_code as bank_branch_acctno";
            sqlQuery += " ,CBB.BANK_BRANCH_ID AS BANK_BRANCH_ID";
            sqlQuery += " ,cba.VENDOR_BANK_ACCTOUNT_TYPE,cba.ACCOUNT_ID";
            sqlQuery += " FROM CA_BANK CB, CA_BANK_BRANCH CBB, ca_bank_accounts cba";
            sqlQuery += " WHERE cb.bank_id = cbb.bank_id";
            sqlQuery += " and cbb.bank_branch_id = cba.bank_branch_id";
            sqlQuery += " and cba.bank_id = cb.bank_id";
            sqlQuery += " AND CB.WORKFLOW_COMPLETION_STATUS = 1 AND CB.ENABLED_FLAG = 1 ";
            sqlQuery += " and cbb.workflow_completion_status = 1 and cbb.enabled_flag = 1";
            sqlQuery += " and cba.workflow_completion_status = 1 and cba.enabled_flag = 1";
            sqlQuery += " AND CB.ORG_ID = '" + VMVServices.Web.Utils.OrganizationID + "'";
            if (bankID != "")
            {
                sqlQuery += " and cba.account_id='" + bankID + "'";
            }
            sqlQuery += " ORDER BY BANK_NAME";

            return sqlQuery;
        }


        public static string getPaymentNumber(string  vendor_id)
        {
            sqlQuery = string.Empty;
            sqlQuery = "select t.pay_id from inv_payments_hdr t";
            sqlQuery += " where t.enabled_flag=1";
            sqlQuery += " and t.workflow_completion_status=1";
            sqlQuery += "  and t.PAY_VENDOR_ID='" + vendor_id + "'";
            sqlQuery += " order by t.pay_id";
            return sqlQuery;
        }
        public static string getInvoiceNumber()
        {
            sqlQuery = string.Empty;
            sqlQuery = " select ih.inv_id ,IH.INV_NUM FROM INV_PAYMENTS_DTL   IPD,";
            sqlQuery += "  inv_invoices_hdr   IH";
            sqlQuery += "  WHERE IH.INV_ID=IPD.PAY_INVOICE_ID";
            sqlQuery += "  AND IPD.ENABLED_FLAG=1";
            sqlQuery += "  AND IPD.WORKFLOW_COMPLETION_STATUS=1";
            return sqlQuery;
        }
        public static string getInvoiceNumberWithSupplier(string vend_id)
        {
            sqlQuery = string.Empty;
            sqlQuery = " select ih.inv_id ,IH.INV_NUM FROM INV_PAYMENTS_DTL   IPD,";
            sqlQuery += "  inv_invoices_hdr   IH";
            sqlQuery += "  WHERE IH.INV_ID=IPD.PAY_INVOICE_ID";
            sqlQuery += "  AND IPD.ENABLED_FLAG=1";
            sqlQuery += "  AND IPD.WORKFLOW_COMPLETION_STATUS=1";
            sqlQuery += " and ih.vendor_id = '" + vend_id + "'";
            return sqlQuery;
        }

        public static string getCurrency()
        {
            sqlQuery = string.Empty;
            sqlQuery = " select sc.currency_id,sc.currency_desc from ssm_currencies sc";
            sqlQuery += "  where sc.ENABLED_FLAG=1";
            sqlQuery += "  AND sc.WORKFLOW_COMPLETION_STATUS=1";
            return sqlQuery;
        }
    }
}
