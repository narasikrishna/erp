﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Linq;
using System.Data.Objects;
using System.Data.Entity;
using System.Runtime.Serialization;
using System.Text;
using System.Data.Metadata.Edm;
using System.Web;

using System.Data;
using FIN.DAL;
using System.Data.Common;

using System.Data.EntityClient;
using Oracle.DataAccess.Client;


using System.Data.Entity.Infrastructure;
using System.Linq.Expressions;

using System.Globalization;


namespace FIN.DAL
{
    public static class DBMethod
    {

        public static void SaveSingleEntity<T>(List<Tuple<object, string>> entity, bool modifyMode = false)
   where T : class
        {
            using (FINEntities context = new FINEntities())
            {
                
                DbTransaction transaction = null;
                try
                {
                    context.Connection.Open();
                    transaction = context.Connection.BeginTransaction();

                    string str_mode;
                    for (int iLoop = 0; iLoop < entity.Count; iLoop++)
                    {
                        str_mode = (string)entity[iLoop].Item2;
                        if (str_mode == "A")
                        {
                            context.CreateObjectSet<T>().AddObject((T)entity[iLoop].Item1);
                        }
                        else if (str_mode == "U")
                        {
                            context.CreateObjectSet<T>().Attach((T)entity[iLoop].Item1);
                            context.ObjectStateManager.ChangeObjectState((T)entity[iLoop].Item1, System.Data.EntityState.Modified);
                        }
                        if (str_mode == "D")
                        {
                            context.CreateObjectSet<T>().Attach((T)entity[iLoop].Item1);
                            context.CreateObjectSet<T>().DeleteObject((T)entity[iLoop].Item1);
                        }
                        context.SaveChanges();
                    }
                    transaction.Commit();

                    if (VMVServices.Web.Utils.StrRecordId != null && VMVServices.Web.Utils.Mode == "U")
                    {
                        if (VMVServices.Web.Utils.StrRecordId != "0")
                        {
                            FINSP.Get_SP_LOG_CHANGES_UPDATE(VMVServices.Web.Utils.FormCode, VMVServices.Web.Utils.UserName, VMVServices.Web.Utils.StrRecordId);
                        }
                    }
                    if (VMVServices.Web.Utils.RecordId != null && VMVServices.Web.Utils.Mode == "U")
                    {
                        if (VMVServices.Web.Utils.RecordId > 0)
                        {
                            FINSP.Get_SP_LOG_CHANGES_UPDATE(VMVServices.Web.Utils.FormCode, VMVServices.Web.Utils.UserName, VMVServices.Web.Utils.RecordId.ToString());
                        }
                    }
                    

                }
                catch (Exception ex)
                {

                    throw ex.InnerException;

                }
                finally
                {
                    context.Connection.Close();
                    //if (VMVServices.Web.Utils.IsAlert == "1")
                    //{
                    //    FINSQL.UpdateAlertUserLevel();
                    //}
                    transaction = null;
                }
            }
        }


        public static void SaveEntity<T>(T entity, bool modifyMode = false)
   where T : class
        {
            using (FINEntities context = new FINEntities())
            {
                DbTransaction transaction = null;
                try
                {
                    context.Connection.Open();
                    transaction = context.Connection.BeginTransaction();

                    if (modifyMode)
                    {
                        context.CreateObjectSet<T>().Attach(entity);
                        context.ObjectStateManager.ChangeObjectState(entity, System.Data.EntityState.Modified);
                    }
                    else
                    {

                        context.CreateObjectSet<T>().AddObject(entity);
                    }

                    //context.AddObject(entity);

                    context.SaveChanges();
                    transaction.Commit();
                  
                    
                    if (VMVServices.Web.Utils.StrRecordId != null && VMVServices.Web.Utils.Mode == "U")
                    {
                        if (VMVServices.Web.Utils.StrRecordId != "0")
                        {
                            FINSP.Get_SP_LOG_CHANGES_UPDATE(VMVServices.Web.Utils.FormCode, VMVServices.Web.Utils.UserName, VMVServices.Web.Utils.StrRecordId);
                        }
                    }
                    if (VMVServices.Web.Utils.RecordId != null && VMVServices.Web.Utils.Mode == "U")
                    {
                        if (VMVServices.Web.Utils.RecordId > 0)
                        {
                            FINSP.Get_SP_LOG_CHANGES_UPDATE(VMVServices.Web.Utils.FormCode, VMVServices.Web.Utils.UserName, VMVServices.Web.Utils.RecordId.ToString());
                        }
                    }
                    

                }
                catch (Exception ex)
                {

                    throw ex.InnerException;

                }
                finally
                {
                    context.Connection.Close();
                    transaction = null;
                }
            }
        }
        public static void SaveFourEntity<T, TC, T2, T3>(T entity, List<Tuple<object, string>> tmpChildEntity, TC ChildEntity, List<Tuple<object, string>> tmpSecondChildEntity, T2 SecondChildEntity, List<Tuple<object, string>> tmpThirdChildEntity, T3 ThirdChildEntity, bool modifyMode = false)
            where T : class
            where TC : class
            where T2 : class
            where T3 : class
        {
            using (FINEntities context = new FINEntities())
            {
                DbTransaction transaction = null;

                try
                {
                    context.Connection.Open();
                    transaction = context.Connection.BeginTransaction();

                    if (modifyMode)
                    {
                        context.CreateObjectSet<T>().Attach(entity);
                        context.ObjectStateManager.ChangeObjectState(entity, System.Data.EntityState.Modified);
                    }
                    else
                    {
                        context.CreateObjectSet<T>().AddObject(entity);
                    }

                    context.SaveChanges();
                    string str_mode;
                    for (int iLoop = 0; iLoop < tmpChildEntity.Count; iLoop++)
                    {
                        ChildEntity = (TC)tmpChildEntity[iLoop].Item1;
                        str_mode = (string)tmpChildEntity[iLoop].Item2;
                        if (str_mode == "A")
                        {
                            context.CreateObjectSet<TC>().AddObject(ChildEntity);
                        }
                        else if (str_mode == "U")
                        {
                            context.CreateObjectSet<TC>().Attach(ChildEntity);
                            context.ObjectStateManager.ChangeObjectState(ChildEntity, System.Data.EntityState.Modified);
                        }
                        if (str_mode == "D")
                        {
                            context.CreateObjectSet<TC>().Attach(ChildEntity);
                            context.CreateObjectSet<TC>().DeleteObject(ChildEntity);
                        }
                        context.SaveChanges();
                    }

                    for (int iLoop = 0; iLoop < tmpSecondChildEntity.Count; iLoop++)
                    {
                        SecondChildEntity = (T2)tmpSecondChildEntity[iLoop].Item1;
                        str_mode = (string)tmpSecondChildEntity[iLoop].Item2;
                        if (str_mode == "A")
                        {
                            context.CreateObjectSet<T2>().AddObject(SecondChildEntity);
                        }
                        else if (str_mode == "U")
                        {
                            context.CreateObjectSet<T2>().Attach(SecondChildEntity);
                            context.ObjectStateManager.ChangeObjectState(SecondChildEntity, System.Data.EntityState.Modified);
                        }
                        if (str_mode == "D")
                        {
                            context.CreateObjectSet<T2>().Attach(SecondChildEntity);
                            context.CreateObjectSet<T2>().DeleteObject(SecondChildEntity);
                        }
                        context.SaveChanges();
                    }

                    for (int iLoop = 0; iLoop < tmpThirdChildEntity.Count; iLoop++)
                    {
                        ThirdChildEntity = (T3)tmpThirdChildEntity[iLoop].Item1;
                        str_mode = (string)tmpThirdChildEntity[iLoop].Item2;
                        if (str_mode == "A")
                        {
                            context.CreateObjectSet<T3>().AddObject(ThirdChildEntity);
                        }
                        else if (str_mode == "U")
                        {
                            context.CreateObjectSet<T3>().Attach(ThirdChildEntity);
                            context.ObjectStateManager.ChangeObjectState(ThirdChildEntity, System.Data.EntityState.Modified);
                        }
                        if (str_mode == "D")
                        {
                            context.CreateObjectSet<T3>().Attach(ThirdChildEntity);
                            context.CreateObjectSet<T3>().DeleteObject(ThirdChildEntity);
                        }
                        context.SaveChanges();
                    }
                    transaction.Commit();

                    if (VMVServices.Web.Utils.StrRecordId != null && VMVServices.Web.Utils.Mode == "U")
                    {
                        if (VMVServices.Web.Utils.StrRecordId != "0")
                        {
                            FINSP.Get_SP_LOG_CHANGES_UPDATE(VMVServices.Web.Utils.FormCode, VMVServices.Web.Utils.UserName, VMVServices.Web.Utils.StrRecordId);
                        }
                    }

                    if (VMVServices.Web.Utils.RecordId != null && VMVServices.Web.Utils.Mode == "U")
                    {
                        if (VMVServices.Web.Utils.RecordId > 0)
                        {
                            FINSP.Get_SP_LOG_CHANGES_UPDATE(VMVServices.Web.Utils.FormCode, VMVServices.Web.Utils.UserName, VMVServices.Web.Utils.RecordId.ToString());
                        }
                    }
                   
                }
                catch (Exception ex)
                {
                    throw ex.InnerException;
                }
                finally
                {
                    context.Connection.Close();
                    //if (VMVServices.Web.Utils.IsAlert == "1")
                    //{
                    //    FINSQL.UpdateAlertUserLevel();
                    //}
                    transaction = null;
                }
            }

        }

        public static void SaveMultipleEntity<T, TC, T2>(T entity, List<Tuple<object, string>> tmpChildEntity, TC ChildEntity, List<Tuple<object, string>> tmpSecondChildEntity, T2 SecondChildEntity, bool modifyMode = false)
            where T : class
            where TC : class
            where T2 : class
        {
            using (FINEntities context = new FINEntities())
            {
                DbTransaction transaction = null;

                try
                {
                    context.Connection.Open();
                    transaction = context.Connection.BeginTransaction();

                    if (modifyMode)
                    {
                        context.CreateObjectSet<T>().Attach(entity);
                        context.ObjectStateManager.ChangeObjectState(entity, System.Data.EntityState.Modified);
                    }
                    else
                    {
                        context.CreateObjectSet<T>().AddObject(entity);
                    }

                    context.SaveChanges();
                    string str_mode;
                    for (int iLoop = 0; iLoop < tmpChildEntity.Count; iLoop++)
                    {
                        ChildEntity = (TC)tmpChildEntity[iLoop].Item1;
                        str_mode = (string)tmpChildEntity[iLoop].Item2;
                        if (str_mode == "A")
                        {
                            context.CreateObjectSet<TC>().AddObject(ChildEntity);
                        }
                        else if (str_mode == "U")
                        {
                            context.CreateObjectSet<TC>().Attach(ChildEntity);
                            context.ObjectStateManager.ChangeObjectState(ChildEntity, System.Data.EntityState.Modified);
                        }
                        if (str_mode == "D")
                        {
                            context.CreateObjectSet<TC>().Attach(ChildEntity);
                            context.CreateObjectSet<TC>().DeleteObject(ChildEntity);
                        }
                        context.SaveChanges();
                    }

                    for (int iLoop = 0; iLoop < tmpSecondChildEntity.Count; iLoop++)
                    {
                        SecondChildEntity = (T2)tmpSecondChildEntity[iLoop].Item1;
                        str_mode = (string)tmpSecondChildEntity[iLoop].Item2;
                        if (str_mode == "A")
                        {
                            context.CreateObjectSet<T2>().AddObject(SecondChildEntity);
                        }
                        else if (str_mode == "U")
                        {
                            context.CreateObjectSet<T2>().Attach(SecondChildEntity);
                            context.ObjectStateManager.ChangeObjectState(SecondChildEntity, System.Data.EntityState.Modified);
                        }
                        if (str_mode == "D")
                        {
                            context.CreateObjectSet<T2>().Attach(SecondChildEntity);
                            context.CreateObjectSet<T2>().DeleteObject(SecondChildEntity);
                        }
                        context.SaveChanges();
                    }


                    transaction.Commit();


                    if (VMVServices.Web.Utils.StrRecordId != null && VMVServices.Web.Utils.Mode == "U")
                    {
                        if (VMVServices.Web.Utils.StrRecordId != "0")
                        {
                            FINSP.Get_SP_LOG_CHANGES_UPDATE(VMVServices.Web.Utils.FormCode, VMVServices.Web.Utils.UserName, VMVServices.Web.Utils.StrRecordId);
                        }
                    }
                    if (VMVServices.Web.Utils.RecordId != null && VMVServices.Web.Utils.Mode == "U")
                    {
                        if (VMVServices.Web.Utils.RecordId > 0)
                        {
                            FINSP.Get_SP_LOG_CHANGES_UPDATE(VMVServices.Web.Utils.FormCode, VMVServices.Web.Utils.UserName, VMVServices.Web.Utils.RecordId.ToString());
                        }
                    }
                   

                }
                catch (Exception ex)
                {
                    throw ex.InnerException;
                }
                finally
                {
                    context.Connection.Close();
                    //if (VMVServices.Web.Utils.IsAlert == "1")
                    //{
                    //    FINSQL.UpdateAlertUserLevel();
                    //}
                    transaction = null;
                }
            }
        }

        public static void SaveMultipleRowsEntity<TC, T2>(List<Tuple<object, string>> tmpChildEntity, TC ChildEntity, List<Tuple<object, string>> tmpSecondChildEntity, T2 SecondChildEntity, bool modifyMode = false)
            where TC : class
            where T2 : class
        {
            using (FINEntities context = new FINEntities())
            {
                DbTransaction transaction = null;

                try
                {
                    context.Connection.Open();
                    transaction = context.Connection.BeginTransaction();

                    string str_mode;

                    for (int iLoop = 0; iLoop < tmpChildEntity.Count; iLoop++)
                    {
                        ChildEntity = (TC)tmpChildEntity[iLoop].Item1;
                        str_mode = (string)tmpChildEntity[iLoop].Item2;
                        if (str_mode == "A")
                        {
                            context.CreateObjectSet<TC>().AddObject(ChildEntity);
                        }
                        else if (str_mode == "U")
                        {
                            context.CreateObjectSet<TC>().Attach(ChildEntity);
                            context.ObjectStateManager.ChangeObjectState(ChildEntity, System.Data.EntityState.Modified);
                        }
                        if (str_mode == "D")
                        {
                            context.CreateObjectSet<TC>().Attach(ChildEntity);
                            context.CreateObjectSet<TC>().DeleteObject(ChildEntity);
                        }
                        context.SaveChanges();
                    }

                    for (int iLoop = 0; iLoop < tmpSecondChildEntity.Count; iLoop++)
                    {
                        SecondChildEntity = (T2)tmpSecondChildEntity[iLoop].Item1;
                        str_mode = (string)tmpSecondChildEntity[iLoop].Item2;
                        if (str_mode == "A")
                        {
                            context.CreateObjectSet<T2>().AddObject(SecondChildEntity);
                        }
                        else if (str_mode == "U")
                        {
                            context.CreateObjectSet<T2>().Attach(SecondChildEntity);
                            context.ObjectStateManager.ChangeObjectState(SecondChildEntity, System.Data.EntityState.Modified);
                        }
                        if (str_mode == "D")
                        {
                            context.CreateObjectSet<T2>().Attach(SecondChildEntity);
                            context.CreateObjectSet<T2>().DeleteObject(SecondChildEntity);
                        }
                        context.SaveChanges();
                    }


                    transaction.Commit();

                    if (VMVServices.Web.Utils.StrRecordId != null && VMVServices.Web.Utils.Mode == "U")
                    {
                        if (VMVServices.Web.Utils.StrRecordId != "0")
                        {
                            FINSP.Get_SP_LOG_CHANGES_UPDATE(VMVServices.Web.Utils.FormCode, VMVServices.Web.Utils.UserName, VMVServices.Web.Utils.StrRecordId);
                        }
                    }

                    if (VMVServices.Web.Utils.RecordId != null && VMVServices.Web.Utils.Mode == "U")
                    {
                        if (VMVServices.Web.Utils.RecordId > 0)
                        {
                            FINSP.Get_SP_LOG_CHANGES_UPDATE(VMVServices.Web.Utils.FormCode, VMVServices.Web.Utils.UserName, VMVServices.Web.Utils.RecordId.ToString());
                        }
                    }
                   

                }
                catch (Exception ex)
                {
                    throw ex.InnerException;
                }
                finally
                {
                    context.Connection.Close();
                    //if (VMVServices.Web.Utils.IsAlert == "1")
                    //{
                    //    FINSQL.UpdateAlertUserLevel();
                    //}
                    transaction = null;
                }
            }
        }
        public static void SavePCEntity<T, TC>(T entity, List<Tuple<object, string>> tmpChildEntity, TC ChildEntity, bool modifyMode = false)
            where T : class
            where TC : class
        {
            using (FINEntities context = new FINEntities())
            {
                DbTransaction transaction = null;

                try
                {
                    context.Connection.Open();
                    transaction = context.Connection.BeginTransaction();

                    if (modifyMode)
                    {
                        context.CreateObjectSet<T>().Attach(entity);
                        context.ObjectStateManager.ChangeObjectState(entity, System.Data.EntityState.Modified);
                    }
                    else
                    {
                        context.CreateObjectSet<T>().AddObject(entity);
                    }

                    context.SaveChanges();

                    string str_mode;

                    if (tmpChildEntity.Count > 0)
                    {
                        for (int iLoop = 0; iLoop < tmpChildEntity.Count; iLoop++)
                        {
                            ChildEntity = (TC)tmpChildEntity[iLoop].Item1;
                            str_mode = (string)tmpChildEntity[iLoop].Item2;
                            if (str_mode == "A")
                            {
                                context.CreateObjectSet<TC>().AddObject(ChildEntity);
                            }
                            else if (str_mode == "U")
                            {
                                context.CreateObjectSet<TC>().Attach(ChildEntity);
                                context.ObjectStateManager.ChangeObjectState(ChildEntity, System.Data.EntityState.Modified);
                            }
                            if (str_mode == "D")
                            {
                                context.CreateObjectSet<TC>().Attach(ChildEntity);
                                context.CreateObjectSet<TC>().DeleteObject(ChildEntity);
                            }
                            context.SaveChanges();
                        }
                    }

                    transaction.Commit();

                    if (VMVServices.Web.Utils.StrRecordId != null && VMVServices.Web.Utils.Mode == "U")
                    {
                        if (VMVServices.Web.Utils.StrRecordId != "0")
                        {
                            FINSP.Get_SP_LOG_CHANGES_UPDATE(VMVServices.Web.Utils.FormCode, VMVServices.Web.Utils.UserName, VMVServices.Web.Utils.StrRecordId);
                        }
                    }
                    if (VMVServices.Web.Utils.RecordId != null && VMVServices.Web.Utils.Mode == "U")
                    {
                        if (VMVServices.Web.Utils.RecordId > 0)
                        {
                            FINSP.Get_SP_LOG_CHANGES_UPDATE(VMVServices.Web.Utils.FormCode, VMVServices.Web.Utils.UserName, VMVServices.Web.Utils.RecordId.ToString());
                        }
                    }
                  

                }
                catch (Exception ex)
                {
                    throw ex.InnerException;
                }
                finally
                {
                    context.Connection.Close();
                    //if (VMVServices.Web.Utils.IsAlert == "1")
                    //{
                    //    FINSQL.UpdateAlertUserLevel();
                    //}
                    transaction = null;
                }
            }
        }

        public static void SavePCEntity1<T, TC>(T entity, List<Tuple<object, string>> tmpChildEntity, TC ChildEntity, FINEntities context, bool modifyMode = false)
            where T : class
            where TC : class
        {

            if (modifyMode)
            {
                context.CreateObjectSet<T>().Attach(entity);
                context.ObjectStateManager.ChangeObjectState(entity, System.Data.EntityState.Modified);
            }
            else
            {
                context.CreateObjectSet<T>().AddObject(entity);
            }

            context.SaveChanges();
            string str_mode;
            for (int iLoop = 0; iLoop < tmpChildEntity.Count; iLoop++)
            {
                ChildEntity = (TC)tmpChildEntity[iLoop].Item1;
                str_mode = (string)tmpChildEntity[iLoop].Item2;
                if (str_mode == "A")
                {
                    context.CreateObjectSet<TC>().AddObject(ChildEntity);
                }
                else if (str_mode == "U")
                {
                    context.CreateObjectSet<TC>().Attach(ChildEntity);
                    context.ObjectStateManager.ChangeObjectState(entity, System.Data.EntityState.Modified);
                }
                if (str_mode == "D")
                {
                    context.CreateObjectSet<TC>().Attach(ChildEntity);
                    context.CreateObjectSet<TC>().DeleteObject(ChildEntity);
                }
                context.SaveChanges();
            }



        }

        public static void DeleteEntity<T>(T entity)
    where T : class
        {
            using (FINEntities context = new FINEntities())
            {
                DbTransaction transaction = null;
                try
                {
                    context.Connection.Open();
                    transaction = context.Connection.BeginTransaction();

                    context.CreateObjectSet<T>().Attach(entity);
                    context.CreateObjectSet<T>().DeleteObject(entity);

                    context.SaveChanges();
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    context.Connection.Close();
                    transaction = null;
                }
            }
        }

        public static OracleCommand ExecuteStoredProcedure(OracleCommand oraCmd)
        {

            FINEntities context = new FINEntities();
            EntityConnection conn = (EntityConnection)context.Connection;
            OracleConnection oraConn = (OracleConnection)conn.StoreConnection;
            if (oraConn.State == ConnectionState.Closed)
                oraConn.Open();

            oraCmd.Connection = oraConn;
            oraCmd.ExecuteScalar();

            oraConn.Close();

            return oraCmd;
        }

        public static DataSet ExecuteStoredProcedure(OracleCommand oraCmd, string str_Query)
        {

            FINEntities context = new FINEntities();
            EntityConnection conn = (EntityConnection)context.Connection;
            OracleConnection oraConn = (OracleConnection)conn.StoreConnection;
            if (oraConn.State == ConnectionState.Closed)
                oraConn.Open();

            oraCmd.Connection = oraConn;
            oraCmd.ExecuteScalar();

            DataSet dtResult = new DataSet();
            OracleCommand oraCmd_Query = new OracleCommand(str_Query, oraConn);

            oraCmd.CommandType = CommandType.Text;

            OracleDataAdapter oraDA = new OracleDataAdapter(oraCmd_Query);

            oraDA.Fill(dtResult);
            oraConn.Close();

            return dtResult;
        }


        public static DataTable ExecuteStoredProcedure4CursorOutput(OracleCommand oraCmd)
        {

            FINEntities context = new FINEntities();
            EntityConnection conn = (EntityConnection)context.Connection;
            OracleConnection oraConn = (OracleConnection)conn.StoreConnection;
            if (oraConn.State == ConnectionState.Closed)
                oraConn.Open();

            oraCmd.Connection = oraConn;
            oraCmd.ExecuteNonQuery();



            OracleDataAdapter ad = new OracleDataAdapter(oraCmd);
            //OracleCommandBuilder cb = new OracleCommandBuilder(ad);

            DataTable dt = new DataTable();
            ad.Fill(dt);

            oraConn.Close();

            return dt;
        }

        public static OracleCommand ExecuteFunction(OracleCommand oraCmd)
        {

            FINEntities context = new FINEntities();
            EntityConnection conn = (EntityConnection)context.Connection;
            OracleConnection oraConn = (OracleConnection)conn.StoreConnection;
            if (oraConn.State == ConnectionState.Closed)
                oraConn.Open();

            oraCmd.Connection = oraConn;
            oraCmd.ExecuteNonQuery();
            return oraCmd;
        }
        public static ObjectContext GetContext()
        {
            using (FINEntities context = new FINEntities())
            {

                return context;
            }
        }

        public static object ExecuteScalarCommand<T>(string command)
        {
            try
            {

                //Entities.Entities context = new Entities.Entities();


                //DbConnection connection = context.Connection;

                //if (connection.State == ConnectionState.Closed)
                //    connection.Open();

                //DbCommand cmd = connection.CreateCommand();
                //cmd.CommandText = command;
                //cmd.CommandType = CommandType.Text


                FINEntities context = new FINEntities();

                EntityConnection conn = (EntityConnection)context.Connection;

                OracleConnection oraConn = (OracleConnection)conn.StoreConnection;

                if (oraConn.State == ConnectionState.Closed)
                    oraConn.Open();

                OracleCommand oraCmd = new OracleCommand(command, oraConn);

                oraCmd.CommandType = CommandType.Text;

                object strCmd = null;

                strCmd = oraCmd.ExecuteScalar();

                oraConn.Close();

                if (strCmd == null)
                {
                    strCmd = 0;
                }

                return strCmd;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static DataSet ExecuteQuery(string command, string tableName = "")
        {
            DataSet dtResult = new DataSet();

            try
            {

                FINEntities context = new FINEntities();

                EntityConnection conn = (EntityConnection)context.Connection;

                OracleConnection oraConn = (OracleConnection)conn.StoreConnection;

                OracleCommand oraCmd = new OracleCommand(command, oraConn);

                oraCmd.CommandType = CommandType.Text;

                OracleDataAdapter oraDA = new OracleDataAdapter(oraCmd);


                if (tableName != string.Empty)
                {
                    oraDA.Fill(dtResult, tableName);
                }
                else
                {
                    oraDA.Fill(dtResult);
                }

                oraConn.Close();

                //var factory = DbProviderFactories.GetFactory("Oracle.DataAccess.Client");


                //Entities.Entities context = new Entities.Entities();

                //DbConnection connection = context.Connection;
                //if (connection.State == ConnectionState.Closed)
                //    connection.Open();



                //DbCommand cmd = connection.CreateCommand();
                //cmd.CommandText = command;
                //cmd.CommandType = CommandType.Text;

                //DbDataAdapter da = factory.CreateDataAdapter();
                //da.SelectCommand = cmd;

                ////OracleDataAdapter da = new OracleDataAdapter(command, cmd.Connection.ConnectionString);

                //if (tableName != string.Empty)
                //{
                //    da.Fill(dtResult, tableName);
                //}
                //else
                //{
                //    da.Fill(dtResult);
                //}

                return dtResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }



        }


        public static void ExecuteNonQuery(string command, string tableName = "")
        {


            try
            {

                FINEntities context = new FINEntities();

                EntityConnection conn = (EntityConnection)context.Connection;
                conn.Open();
                OracleConnection oraConn = (OracleConnection)conn.StoreConnection;

                OracleCommand oraCmd = new OracleCommand(command, oraConn);

                oraCmd.CommandType = CommandType.Text;
                oraCmd.ExecuteNonQuery();
                conn.Close();

            }
            catch (Exception ex)
            {
                throw ex;
            }



        }
        public static void UpdateQuery(string command, string tableName = "")
        {
            FINEntities context = new FINEntities();
            DbTransaction beginTran = null;
            EntityConnection conn = (EntityConnection)context.Connection;

            try
            {
                conn.Open();
                beginTran = context.Connection.BeginTransaction();
                OracleConnection oraConn = (OracleConnection)conn.StoreConnection;

                OracleCommand oraCmd = new OracleCommand(command, oraConn);

                oraCmd.CommandType = CommandType.Text;
                oraCmd.ExecuteNonQuery();
                beginTran.Commit();
                conn.Close();
                beginTran = null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                conn.Close();
                beginTran = null;
            }
        }

        public static string GetStringValue(string sqlQuery)
        {
            string str_Value = string.Empty;

            str_Value = (ExecuteScalarCommand<string>(sqlQuery).ToString());

            return str_Value;
        }
        public static int GetIntValue(string sqlQuery)
        {
            int str_Value = 0;

            str_Value = int.Parse(ExecuteScalarCommand<string>(sqlQuery).ToString());

            return str_Value;
        }
        public static decimal GetDecimalValue(string sqlQuery)
        {
            decimal str_Value = 0;

            str_Value = decimal.Parse(ExecuteScalarCommand<string>(sqlQuery).ToString());

            return str_Value;
        }
        public static decimal GetAmountDecimalValue(string sqlQuery)
        {
            decimal str_Value = 0;

            str_Value = Math.Round(decimal.Parse(ExecuteScalarCommand<string>(sqlQuery).ToString()), int.Parse(VMVServices.Web.Utils.DecimalPrecision));

            return str_Value;
        }
        public static int GetPrimaryKeyValue(string sequenceName)
        {
            int keyID = 0;
            string sqlQuery;
            sqlQuery = "select " + sequenceName + ".nextval from dual";

            keyID = int.Parse(ExecuteScalarCommand<int>(sqlQuery).ToString());

            return keyID;
        }
        public static string GetAmtDecimalCommaSeparationValue(string amtStringValue)
        {
            string formattedValue = string.Empty;
            string separatedValue = string.Empty;

            int decimalPrecision = int.Parse(VMVServices.Web.Utils.DecimalPrecision.ToString());
            int commaSeparation = int.Parse(VMVServices.Web.Utils.CommaSeparation.ToString());

            formattedValue = "";
            if (amtStringValue != null)
            {
                if (amtStringValue.Length > 0)
                {
                    if (commaSeparation == 1)
                    {
                        separatedValue = "#,#,#,#,#,#,#,#,#,#,#,#,#";
                    }
                    else if (commaSeparation == 2)
                    {
                        separatedValue = "#,##,##,##,##,##,##,##,##,##,##,##,##";
                    }
                    else if (commaSeparation == 3)
                    {
                        separatedValue = "#,###,###,###,###,###,###,###,###,###,###,###,###";
                    }
                    else if (commaSeparation == 4)
                    {
                        separatedValue = "#,####,####,####,####,####,####,####,####,####,####,####,####";
                    }
                    else if (commaSeparation == 5)
                    {
                        separatedValue = "#,#####,#####,#####,#####,#####,#####,#####,#####,#####,#####,#####,#####";
                    }
                    else if (commaSeparation == 6)
                    {
                        separatedValue = "#,######,######,######,######,######,######,######,######,######,######,######,######";
                    }

                    decimal amtValue = decimal.Parse(amtStringValue.ToString());

                    if (decimalPrecision == 1)
                    {
                        formattedValue = amtValue.ToString(separatedValue + ".0", System.Globalization.CultureInfo.InvariantCulture);
                    }
                    else if (decimalPrecision == 2)
                    {
                        formattedValue = amtValue.ToString("#,###,###,###.00", System.Globalization.CultureInfo.InvariantCulture);
                    }
                    else if (decimalPrecision == 3)
                    {
                        formattedValue = amtValue.ToString(separatedValue + ".000", System.Globalization.CultureInfo.InvariantCulture);

                    }
                    else if (decimalPrecision == 4)
                    {
                        formattedValue = amtValue.ToString("#,###,###,###.0000", System.Globalization.CultureInfo.InvariantCulture);
                    }
                    else if (decimalPrecision == 5)
                    {
                        formattedValue = amtValue.ToString("#,###,###,###.00000", System.Globalization.CultureInfo.InvariantCulture);
                    }
                    else if (decimalPrecision == 6)
                    {
                        formattedValue = amtValue.ToString("#,###,###,###.000000", System.Globalization.CultureInfo.InvariantCulture);
                    }
                }
            }
            return formattedValue;
        }
        public static string GetAmtDecimalCommaSeparationValueWithNegative(string amtStringValue)
        {
            string formattedValue = string.Empty;
            string separatedValue = string.Empty;

            int decimalPrecision = int.Parse(VMVServices.Web.Utils.DecimalPrecision.ToString());
            int commaSeparation = int.Parse(VMVServices.Web.Utils.CommaSeparation.ToString());

            formattedValue = "";
            if (amtStringValue != null)
            {
                if (amtStringValue.Length > 0)
                {
                    if (commaSeparation == 1)
                    {
                        separatedValue = "#,#,#,#,#,#,#,#,#,#,#,#,#";
                    }
                    else if (commaSeparation == 2)
                    {
                        separatedValue = "#,##,##,##,##,##,##,##,##,##,##,##,##";
                    }
                    else if (commaSeparation == 3)
                    {
                        separatedValue = "#,###,###,###,###,###,###,###,###,###,###,###,###";
                    }
                    else if (commaSeparation == 4)
                    {
                        separatedValue = "#,####,####,####,####,####,####,####,####,####,####,####,####";
                    }
                    else if (commaSeparation == 5)
                    {
                        separatedValue = "#,#####,#####,#####,#####,#####,#####,#####,#####,#####,#####,#####,#####";
                    }
                    else if (commaSeparation == 6)
                    {
                        separatedValue = "#,######,######,######,######,######,######,######,######,######,######,######,######";
                    }

                    decimal amtValue = decimal.Parse(amtStringValue.ToString());

                    if (decimalPrecision == 1)
                    {
                        formattedValue = amtValue.ToString(separatedValue + ".0", System.Globalization.CultureInfo.InvariantCulture);
                    }
                    else if (decimalPrecision == 2)
                    {
                        formattedValue = amtValue.ToString("#,###,###,###.00", System.Globalization.CultureInfo.InvariantCulture);
                    }
                    else if (decimalPrecision == 3)
                    {
                        formattedValue = amtValue.ToString(separatedValue + ".000", System.Globalization.CultureInfo.InvariantCulture);
                        if (amtValue < 0)
                        {
                            formattedValue = String.Format("{0:#,##0.000;(#,##0.000);}", amtValue);
                        }
                    }
                    else if (decimalPrecision == 4)
                    {
                        formattedValue = amtValue.ToString("#,###,###,###.0000", System.Globalization.CultureInfo.InvariantCulture);
                    }
                    else if (decimalPrecision == 5)
                    {
                        formattedValue = amtValue.ToString("#,###,###,###.00000", System.Globalization.CultureInfo.InvariantCulture);
                    }
                    else if (decimalPrecision == 6)
                    {
                        formattedValue = amtValue.ToString("#,###,###,###.000000", System.Globalization.CultureInfo.InvariantCulture);
                    }
                }
            }
            return formattedValue;
        }
        public static string GetAmtDecimalSeparationValue(string amtStringValue)
        {
            string formattedValue = string.Empty;

            int decimalPrecision = int.Parse(VMVServices.Web.Utils.DecimalPrecision.ToString());
            string commaSeparation = (VMVServices.Web.Utils.OrganizationID.ToString());

            decimal amtValue = decimal.Parse(amtStringValue.ToString());


            if (decimalPrecision == 1)
            {
                formattedValue = amtValue.ToString("##########.#", System.Globalization.CultureInfo.InvariantCulture);
            }
            else if (decimalPrecision == 2)
            {
                formattedValue = amtValue.ToString("##########.##", System.Globalization.CultureInfo.InvariantCulture);
            }
            else if (decimalPrecision == 3)
            {
                formattedValue = amtValue.ToString("##########.###", System.Globalization.CultureInfo.InvariantCulture);
            }
            else if (decimalPrecision == 4)
            {
                formattedValue = amtValue.ToString("##########.####", System.Globalization.CultureInfo.InvariantCulture);
            }
            else if (decimalPrecision == 5)
            {
                formattedValue = amtValue.ToString("##########.#####", System.Globalization.CultureInfo.InvariantCulture);
            }
            else if (decimalPrecision == 6)
            {
                formattedValue = amtValue.ToString("##########.######", System.Globalization.CultureInfo.InvariantCulture);
            }
            return formattedValue;
        }
        static string value = DateTime.Now.ToString();
        public static string ConvertDateToString(string dateField)
        {
            if (dateField != string.Empty)
            {

                value = DateTime.Parse(dateField.ToString()).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.GetCultureInfo("en-US"));
            }
            return value;
        }
        public static DateTime ConvertStringToDate(string stringField)
        {
            DateTime datevalue = DateTime.Now;
            if (stringField != string.Empty)
            {
                datevalue = DateTime.ParseExact(stringField, "dd/MM/yyyy", System.Globalization.CultureInfo.GetCultureInfo("en-US"));
            }
            return datevalue;
        }

    }
}
