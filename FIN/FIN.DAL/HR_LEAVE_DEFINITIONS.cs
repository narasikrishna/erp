//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.Serialization;

namespace FIN.DAL
{
    [DataContract(IsReference = true)]
    [KnownType(typeof(HR_LEAVE_CONDITIONS_HDR))]
    public partial class HR_LEAVE_DEFINITIONS: IObjectWithChangeTracker, INotifyPropertyChanged
    {
        #region Primitive Properties
    
        [DataMember]
        public Nullable<int> PK_ID
        {
            get { return _pK_ID; }
            set
            {
                if (_pK_ID != value)
                {
                    _pK_ID = value;
                    OnPropertyChanged("PK_ID");
                }
            }
        }
        private Nullable<int> _pK_ID;
    
        [DataMember]
        public Nullable<int> CHILD_ID
        {
            get { return _cHILD_ID; }
            set
            {
                if (_cHILD_ID != value)
                {
                    _cHILD_ID = value;
                    OnPropertyChanged("CHILD_ID");
                }
            }
        }
        private Nullable<int> _cHILD_ID;
    
        [DataMember]
        public string LEAVE_ID
        {
            get { return _lEAVE_ID; }
            set
            {
                if (_lEAVE_ID != value)
                {
                    if (ChangeTracker.ChangeTrackingEnabled && ChangeTracker.State != ObjectState.Added)
                    {
                        throw new InvalidOperationException("The property 'LEAVE_ID' is part of the object's key and cannot be changed. Changes to key properties can only be made when the object is not being tracked or is in the Added state.");
                    }
                    _lEAVE_ID = value;
                    OnPropertyChanged("LEAVE_ID");
                }
            }
        }
        private string _lEAVE_ID;
    
        [DataMember]
        public string LEAVE_DESC
        {
            get { return _lEAVE_DESC; }
            set
            {
                if (_lEAVE_DESC != value)
                {
                    _lEAVE_DESC = value;
                    OnPropertyChanged("LEAVE_DESC");
                }
            }
        }
        private string _lEAVE_DESC;
    
        [DataMember]
        public string ORG_ID
        {
            get { return _oRG_ID; }
            set
            {
                if (_oRG_ID != value)
                {
                    if (ChangeTracker.ChangeTrackingEnabled && ChangeTracker.State != ObjectState.Added)
                    {
                        throw new InvalidOperationException("The property 'ORG_ID' is part of the object's key and cannot be changed. Changes to key properties can only be made when the object is not being tracked or is in the Added state.");
                    }
                    _oRG_ID = value;
                    OnPropertyChanged("ORG_ID");
                }
            }
        }
        private string _oRG_ID;
    
        [DataMember]
        public string FISCAL_YEAR
        {
            get { return _fISCAL_YEAR; }
            set
            {
                if (_fISCAL_YEAR != value)
                {
                    _fISCAL_YEAR = value;
                    OnPropertyChanged("FISCAL_YEAR");
                }
            }
        }
        private string _fISCAL_YEAR;
    
        [DataMember]
        public string LEAVE_TYPE
        {
            get { return _lEAVE_TYPE; }
            set
            {
                if (_lEAVE_TYPE != value)
                {
                    _lEAVE_TYPE = value;
                    OnPropertyChanged("LEAVE_TYPE");
                }
            }
        }
        private string _lEAVE_TYPE;
    
        [DataMember]
        public string WITH_PAY_YN
        {
            get { return _wITH_PAY_YN; }
            set
            {
                if (_wITH_PAY_YN != value)
                {
                    _wITH_PAY_YN = value;
                    OnPropertyChanged("WITH_PAY_YN");
                }
            }
        }
        private string _wITH_PAY_YN;
    
        [DataMember]
        public string CARRY_OVER_YN
        {
            get { return _cARRY_OVER_YN; }
            set
            {
                if (_cARRY_OVER_YN != value)
                {
                    _cARRY_OVER_YN = value;
                    OnPropertyChanged("CARRY_OVER_YN");
                }
            }
        }
        private string _cARRY_OVER_YN;
    
        [DataMember]
        public Nullable<decimal> NO_OF_DAYS
        {
            get { return _nO_OF_DAYS; }
            set
            {
                if (_nO_OF_DAYS != value)
                {
                    _nO_OF_DAYS = value;
                    OnPropertyChanged("NO_OF_DAYS");
                }
            }
        }
        private Nullable<decimal> _nO_OF_DAYS;
    
        [DataMember]
        public Nullable<System.DateTime> EFFECTIVE_FROM_DT
        {
            get { return _eFFECTIVE_FROM_DT; }
            set
            {
                if (_eFFECTIVE_FROM_DT != value)
                {
                    _eFFECTIVE_FROM_DT = value;
                    OnPropertyChanged("EFFECTIVE_FROM_DT");
                }
            }
        }
        private Nullable<System.DateTime> _eFFECTIVE_FROM_DT;
    
        [DataMember]
        public Nullable<System.DateTime> EFFECTIVE_TO_DT
        {
            get { return _eFFECTIVE_TO_DT; }
            set
            {
                if (_eFFECTIVE_TO_DT != value)
                {
                    _eFFECTIVE_TO_DT = value;
                    OnPropertyChanged("EFFECTIVE_TO_DT");
                }
            }
        }
        private Nullable<System.DateTime> _eFFECTIVE_TO_DT;
    
        [DataMember]
        public string ENABLED_FLAG
        {
            get { return _eNABLED_FLAG; }
            set
            {
                if (_eNABLED_FLAG != value)
                {
                    _eNABLED_FLAG = value;
                    OnPropertyChanged("ENABLED_FLAG");
                }
            }
        }
        private string _eNABLED_FLAG;
    
        [DataMember]
        public string WORKFLOW_COMPLETION_STATUS
        {
            get { return _wORKFLOW_COMPLETION_STATUS; }
            set
            {
                if (_wORKFLOW_COMPLETION_STATUS != value)
                {
                    _wORKFLOW_COMPLETION_STATUS = value;
                    OnPropertyChanged("WORKFLOW_COMPLETION_STATUS");
                }
            }
        }
        private string _wORKFLOW_COMPLETION_STATUS;
    
        [DataMember]
        public string CREATED_BY
        {
            get { return _cREATED_BY; }
            set
            {
                if (_cREATED_BY != value)
                {
                    _cREATED_BY = value;
                    OnPropertyChanged("CREATED_BY");
                }
            }
        }
        private string _cREATED_BY;
    
        [DataMember]
        public System.DateTime CREATED_DATE
        {
            get { return _cREATED_DATE; }
            set
            {
                if (_cREATED_DATE != value)
                {
                    _cREATED_DATE = value;
                    OnPropertyChanged("CREATED_DATE");
                }
            }
        }
        private System.DateTime _cREATED_DATE;
    
        [DataMember]
        public string MODIFIED_BY
        {
            get { return _mODIFIED_BY; }
            set
            {
                if (_mODIFIED_BY != value)
                {
                    _mODIFIED_BY = value;
                    OnPropertyChanged("MODIFIED_BY");
                }
            }
        }
        private string _mODIFIED_BY;
    
        [DataMember]
        public Nullable<System.DateTime> MODIFIED_DATE
        {
            get { return _mODIFIED_DATE; }
            set
            {
                if (_mODIFIED_DATE != value)
                {
                    _mODIFIED_DATE = value;
                    OnPropertyChanged("MODIFIED_DATE");
                }
            }
        }
        private Nullable<System.DateTime> _mODIFIED_DATE;
    
        [DataMember]
        public string ATTRIBUTE1
        {
            get { return _aTTRIBUTE1; }
            set
            {
                if (_aTTRIBUTE1 != value)
                {
                    _aTTRIBUTE1 = value;
                    OnPropertyChanged("ATTRIBUTE1");
                }
            }
        }
        private string _aTTRIBUTE1;
    
        [DataMember]
        public string ATTRIBUTE2
        {
            get { return _aTTRIBUTE2; }
            set
            {
                if (_aTTRIBUTE2 != value)
                {
                    _aTTRIBUTE2 = value;
                    OnPropertyChanged("ATTRIBUTE2");
                }
            }
        }
        private string _aTTRIBUTE2;
    
        [DataMember]
        public string ATTRIBUTE3
        {
            get { return _aTTRIBUTE3; }
            set
            {
                if (_aTTRIBUTE3 != value)
                {
                    _aTTRIBUTE3 = value;
                    OnPropertyChanged("ATTRIBUTE3");
                }
            }
        }
        private string _aTTRIBUTE3;
    
        [DataMember]
        public string ATTRIBUTE4
        {
            get { return _aTTRIBUTE4; }
            set
            {
                if (_aTTRIBUTE4 != value)
                {
                    _aTTRIBUTE4 = value;
                    OnPropertyChanged("ATTRIBUTE4");
                }
            }
        }
        private string _aTTRIBUTE4;
    
        [DataMember]
        public string ATTRIBUTE5
        {
            get { return _aTTRIBUTE5; }
            set
            {
                if (_aTTRIBUTE5 != value)
                {
                    _aTTRIBUTE5 = value;
                    OnPropertyChanged("ATTRIBUTE5");
                }
            }
        }
        private string _aTTRIBUTE5;
    
        [DataMember]
        public string ATTRIBUTE6
        {
            get { return _aTTRIBUTE6; }
            set
            {
                if (_aTTRIBUTE6 != value)
                {
                    _aTTRIBUTE6 = value;
                    OnPropertyChanged("ATTRIBUTE6");
                }
            }
        }
        private string _aTTRIBUTE6;
    
        [DataMember]
        public string ATTRIBUTE7
        {
            get { return _aTTRIBUTE7; }
            set
            {
                if (_aTTRIBUTE7 != value)
                {
                    _aTTRIBUTE7 = value;
                    OnPropertyChanged("ATTRIBUTE7");
                }
            }
        }
        private string _aTTRIBUTE7;
    
        [DataMember]
        public string ATTRIBUTE8
        {
            get { return _aTTRIBUTE8; }
            set
            {
                if (_aTTRIBUTE8 != value)
                {
                    _aTTRIBUTE8 = value;
                    OnPropertyChanged("ATTRIBUTE8");
                }
            }
        }
        private string _aTTRIBUTE8;
    
        [DataMember]
        public string ATTRIBUTE9
        {
            get { return _aTTRIBUTE9; }
            set
            {
                if (_aTTRIBUTE9 != value)
                {
                    _aTTRIBUTE9 = value;
                    OnPropertyChanged("ATTRIBUTE9");
                }
            }
        }
        private string _aTTRIBUTE9;
    
        [DataMember]
        public string ATTRIBUTE10
        {
            get { return _aTTRIBUTE10; }
            set
            {
                if (_aTTRIBUTE10 != value)
                {
                    _aTTRIBUTE10 = value;
                    OnPropertyChanged("ATTRIBUTE10");
                }
            }
        }
        private string _aTTRIBUTE10;
    
        [DataMember]
        public string CARRY_OVER_TYPE
        {
            get { return _cARRY_OVER_TYPE; }
            set
            {
                if (_cARRY_OVER_TYPE != value)
                {
                    _cARRY_OVER_TYPE = value;
                    OnPropertyChanged("CARRY_OVER_TYPE");
                }
            }
        }
        private string _cARRY_OVER_TYPE;
    
        [DataMember]
        public Nullable<decimal> MIN_SERVICE_PERIOD
        {
            get { return _mIN_SERVICE_PERIOD; }
            set
            {
                if (_mIN_SERVICE_PERIOD != value)
                {
                    _mIN_SERVICE_PERIOD = value;
                    OnPropertyChanged("MIN_SERVICE_PERIOD");
                }
            }
        }
        private Nullable<decimal> _mIN_SERVICE_PERIOD;
    
        [DataMember]
        public Nullable<decimal> MAX_NO_DAYS
        {
            get { return _mAX_NO_DAYS; }
            set
            {
                if (_mAX_NO_DAYS != value)
                {
                    _mAX_NO_DAYS = value;
                    OnPropertyChanged("MAX_NO_DAYS");
                }
            }
        }
        private Nullable<decimal> _mAX_NO_DAYS;
    
        [DataMember]
        public string APPLICABLE_FREQUENCY
        {
            get { return _aPPLICABLE_FREQUENCY; }
            set
            {
                if (_aPPLICABLE_FREQUENCY != value)
                {
                    _aPPLICABLE_FREQUENCY = value;
                    OnPropertyChanged("APPLICABLE_FREQUENCY");
                }
            }
        }
        private string _aPPLICABLE_FREQUENCY;
    
        [DataMember]
        public string HOLIDAY_CONSIDERATION
        {
            get { return _hOLIDAY_CONSIDERATION; }
            set
            {
                if (_hOLIDAY_CONSIDERATION != value)
                {
                    _hOLIDAY_CONSIDERATION = value;
                    OnPropertyChanged("HOLIDAY_CONSIDERATION");
                }
            }
        }
        private string _hOLIDAY_CONSIDERATION;
    
        [DataMember]
        public string JOIN_REPORT
        {
            get { return _jOIN_REPORT; }
            set
            {
                if (_jOIN_REPORT != value)
                {
                    _jOIN_REPORT = value;
                    OnPropertyChanged("JOIN_REPORT");
                }
            }
        }
        private string _jOIN_REPORT;
    
        [DataMember]
        public string LEAVE_ID_OL
        {
            get { return _lEAVE_ID_OL; }
            set
            {
                if (_lEAVE_ID_OL != value)
                {
                    _lEAVE_ID_OL = value;
                    OnPropertyChanged("LEAVE_ID_OL");
                }
            }
        }
        private string _lEAVE_ID_OL;

        #endregion
        #region Navigation Properties
    
        [DataMember]
        public TrackableCollection<HR_LEAVE_CONDITIONS_HDR> HR_LEAVE_CONDITIONS_HDR
        {
            get
            {
                if (_hR_LEAVE_CONDITIONS_HDR == null)
                {
                    _hR_LEAVE_CONDITIONS_HDR = new TrackableCollection<HR_LEAVE_CONDITIONS_HDR>();
                    _hR_LEAVE_CONDITIONS_HDR.CollectionChanged += FixupHR_LEAVE_CONDITIONS_HDR;
                }
                return _hR_LEAVE_CONDITIONS_HDR;
            }
            set
            {
                if (!ReferenceEquals(_hR_LEAVE_CONDITIONS_HDR, value))
                {
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        throw new InvalidOperationException("Cannot set the FixupChangeTrackingCollection when ChangeTracking is enabled");
                    }
                    if (_hR_LEAVE_CONDITIONS_HDR != null)
                    {
                        _hR_LEAVE_CONDITIONS_HDR.CollectionChanged -= FixupHR_LEAVE_CONDITIONS_HDR;
                    }
                    _hR_LEAVE_CONDITIONS_HDR = value;
                    if (_hR_LEAVE_CONDITIONS_HDR != null)
                    {
                        _hR_LEAVE_CONDITIONS_HDR.CollectionChanged += FixupHR_LEAVE_CONDITIONS_HDR;
                    }
                    OnNavigationPropertyChanged("HR_LEAVE_CONDITIONS_HDR");
                }
            }
        }
        private TrackableCollection<HR_LEAVE_CONDITIONS_HDR> _hR_LEAVE_CONDITIONS_HDR;

        #endregion
        #region ChangeTracking
    
        protected virtual void OnPropertyChanged(String propertyName)
        {
            if (ChangeTracker.State != ObjectState.Added && ChangeTracker.State != ObjectState.Deleted)
            {
                ChangeTracker.State = ObjectState.Modified;
            }
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        protected virtual void OnNavigationPropertyChanged(String propertyName)
        {
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        event PropertyChangedEventHandler INotifyPropertyChanged.PropertyChanged{ add { _propertyChanged += value; } remove { _propertyChanged -= value; } }
        private event PropertyChangedEventHandler _propertyChanged;
        private ObjectChangeTracker _changeTracker;
    
        [DataMember]
        public ObjectChangeTracker ChangeTracker
        {
            get
            {
                if (_changeTracker == null)
                {
                    _changeTracker = new ObjectChangeTracker();
                    _changeTracker.ObjectStateChanging += HandleObjectStateChanging;
                }
                return _changeTracker;
            }
            set
            {
                if(_changeTracker != null)
                {
                    _changeTracker.ObjectStateChanging -= HandleObjectStateChanging;
                }
                _changeTracker = value;
                if(_changeTracker != null)
                {
                    _changeTracker.ObjectStateChanging += HandleObjectStateChanging;
                }
            }
        }
    
        private void HandleObjectStateChanging(object sender, ObjectStateChangingEventArgs e)
        {
            if (e.NewState == ObjectState.Deleted)
            {
                ClearNavigationProperties();
            }
        }
    
        protected bool IsDeserializing { get; private set; }
    
        [OnDeserializing]
        public void OnDeserializingMethod(StreamingContext context)
        {
            IsDeserializing = true;
        }
    
        [OnDeserialized]
        public void OnDeserializedMethod(StreamingContext context)
        {
            IsDeserializing = false;
            ChangeTracker.ChangeTrackingEnabled = true;
        }
    
        protected virtual void ClearNavigationProperties()
        {
            HR_LEAVE_CONDITIONS_HDR.Clear();
        }

        #endregion
        #region Association Fixup
    
        private void FixupHR_LEAVE_CONDITIONS_HDR(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (e.NewItems != null)
            {
                foreach (HR_LEAVE_CONDITIONS_HDR item in e.NewItems)
                {
                    item.HR_LEAVE_DEFINITIONS = this;
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        if (!item.ChangeTracker.ChangeTrackingEnabled)
                        {
                            item.StartTracking();
                        }
                        ChangeTracker.RecordAdditionToCollectionProperties("HR_LEAVE_CONDITIONS_HDR", item);
                    }
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (HR_LEAVE_CONDITIONS_HDR item in e.OldItems)
                {
                    if (ReferenceEquals(item.HR_LEAVE_DEFINITIONS, this))
                    {
                        item.HR_LEAVE_DEFINITIONS = null;
                    }
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        ChangeTracker.RecordRemovalFromCollectionProperties("HR_LEAVE_CONDITIONS_HDR", item);
                    }
                }
            }
        }

        #endregion
    }
}
