//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.Serialization;

namespace FIN.DAL
{
    [DataContract(IsReference = true)]
    [KnownType(typeof(HR_DEPARTMENTS))]
    [KnownType(typeof(HR_EMP_MOB_LIMIT_DTL))]
    [KnownType(typeof(HR_EMPLOYEES))]
    public partial class HR_EMP_MOB_LIMIT_HDR: IObjectWithChangeTracker, INotifyPropertyChanged
    {
        #region Primitive Properties
    
        [DataMember]
        public Nullable<int> PK_ID
        {
            get { return _pK_ID; }
            set
            {
                if (_pK_ID != value)
                {
                    _pK_ID = value;
                    OnPropertyChanged("PK_ID");
                }
            }
        }
        private Nullable<int> _pK_ID;
    
        [DataMember]
        public Nullable<int> CHILD_ID
        {
            get { return _cHILD_ID; }
            set
            {
                if (_cHILD_ID != value)
                {
                    _cHILD_ID = value;
                    OnPropertyChanged("CHILD_ID");
                }
            }
        }
        private Nullable<int> _cHILD_ID;
    
        [DataMember]
        public string MOB_LIMIT_ID
        {
            get { return _mOB_LIMIT_ID; }
            set
            {
                if (_mOB_LIMIT_ID != value)
                {
                    if (ChangeTracker.ChangeTrackingEnabled && ChangeTracker.State != ObjectState.Added)
                    {
                        throw new InvalidOperationException("The property 'MOB_LIMIT_ID' is part of the object's key and cannot be changed. Changes to key properties can only be made when the object is not being tracked or is in the Added state.");
                    }
                    _mOB_LIMIT_ID = value;
                    OnPropertyChanged("MOB_LIMIT_ID");
                }
            }
        }
        private string _mOB_LIMIT_ID;
    
        [DataMember]
        public string ORG_ID
        {
            get { return _oRG_ID; }
            set
            {
                if (_oRG_ID != value)
                {
                    _oRG_ID = value;
                    OnPropertyChanged("ORG_ID");
                }
            }
        }
        private string _oRG_ID;
    
        [DataMember]
        public string DEPT_ID
        {
            get { return _dEPT_ID; }
            set
            {
                if (_dEPT_ID != value)
                {
                    ChangeTracker.RecordOriginalValue("DEPT_ID", _dEPT_ID);
                    if (!IsDeserializing)
                    {
                        if (HR_DEPARTMENTS != null && HR_DEPARTMENTS.DEPT_ID != value)
                        {
                            HR_DEPARTMENTS = null;
                        }
                    }
                    _dEPT_ID = value;
                    OnPropertyChanged("DEPT_ID");
                }
            }
        }
        private string _dEPT_ID;
    
        [DataMember]
        public string EMP_ID
        {
            get { return _eMP_ID; }
            set
            {
                if (_eMP_ID != value)
                {
                    ChangeTracker.RecordOriginalValue("EMP_ID", _eMP_ID);
                    if (!IsDeserializing)
                    {
                        if (HR_EMPLOYEES != null && HR_EMPLOYEES.EMP_ID != value)
                        {
                            HR_EMPLOYEES = null;
                        }
                    }
                    _eMP_ID = value;
                    OnPropertyChanged("EMP_ID");
                }
            }
        }
        private string _eMP_ID;
    
        [DataMember]
        public string MOB_NUMBER
        {
            get { return _mOB_NUMBER; }
            set
            {
                if (_mOB_NUMBER != value)
                {
                    _mOB_NUMBER = value;
                    OnPropertyChanged("MOB_NUMBER");
                }
            }
        }
        private string _mOB_NUMBER;
    
        [DataMember]
        public Nullable<decimal> MOB_LIMIT
        {
            get { return _mOB_LIMIT; }
            set
            {
                if (_mOB_LIMIT != value)
                {
                    _mOB_LIMIT = value;
                    OnPropertyChanged("MOB_LIMIT");
                }
            }
        }
        private Nullable<decimal> _mOB_LIMIT;
    
        [DataMember]
        public Nullable<System.DateTime> LIMIT_FROM
        {
            get { return _lIMIT_FROM; }
            set
            {
                if (_lIMIT_FROM != value)
                {
                    _lIMIT_FROM = value;
                    OnPropertyChanged("LIMIT_FROM");
                }
            }
        }
        private Nullable<System.DateTime> _lIMIT_FROM;
    
        [DataMember]
        public Nullable<System.DateTime> LIMIT_TO
        {
            get { return _lIMIT_TO; }
            set
            {
                if (_lIMIT_TO != value)
                {
                    _lIMIT_TO = value;
                    OnPropertyChanged("LIMIT_TO");
                }
            }
        }
        private Nullable<System.DateTime> _lIMIT_TO;
    
        [DataMember]
        public string ENABLED_FLAG
        {
            get { return _eNABLED_FLAG; }
            set
            {
                if (_eNABLED_FLAG != value)
                {
                    _eNABLED_FLAG = value;
                    OnPropertyChanged("ENABLED_FLAG");
                }
            }
        }
        private string _eNABLED_FLAG;
    
        [DataMember]
        public string WORKFLOW_COMPLETION_STATUS
        {
            get { return _wORKFLOW_COMPLETION_STATUS; }
            set
            {
                if (_wORKFLOW_COMPLETION_STATUS != value)
                {
                    _wORKFLOW_COMPLETION_STATUS = value;
                    OnPropertyChanged("WORKFLOW_COMPLETION_STATUS");
                }
            }
        }
        private string _wORKFLOW_COMPLETION_STATUS;
    
        [DataMember]
        public string CREATED_BY
        {
            get { return _cREATED_BY; }
            set
            {
                if (_cREATED_BY != value)
                {
                    _cREATED_BY = value;
                    OnPropertyChanged("CREATED_BY");
                }
            }
        }
        private string _cREATED_BY;
    
        [DataMember]
        public System.DateTime CREATED_DATE
        {
            get { return _cREATED_DATE; }
            set
            {
                if (_cREATED_DATE != value)
                {
                    _cREATED_DATE = value;
                    OnPropertyChanged("CREATED_DATE");
                }
            }
        }
        private System.DateTime _cREATED_DATE;
    
        [DataMember]
        public string MODIFIED_BY
        {
            get { return _mODIFIED_BY; }
            set
            {
                if (_mODIFIED_BY != value)
                {
                    _mODIFIED_BY = value;
                    OnPropertyChanged("MODIFIED_BY");
                }
            }
        }
        private string _mODIFIED_BY;
    
        [DataMember]
        public Nullable<System.DateTime> MODIFIED_DATE
        {
            get { return _mODIFIED_DATE; }
            set
            {
                if (_mODIFIED_DATE != value)
                {
                    _mODIFIED_DATE = value;
                    OnPropertyChanged("MODIFIED_DATE");
                }
            }
        }
        private Nullable<System.DateTime> _mODIFIED_DATE;
    
        [DataMember]
        public string ATTRIBUTE1
        {
            get { return _aTTRIBUTE1; }
            set
            {
                if (_aTTRIBUTE1 != value)
                {
                    _aTTRIBUTE1 = value;
                    OnPropertyChanged("ATTRIBUTE1");
                }
            }
        }
        private string _aTTRIBUTE1;
    
        [DataMember]
        public string ATTRIBUTE2
        {
            get { return _aTTRIBUTE2; }
            set
            {
                if (_aTTRIBUTE2 != value)
                {
                    _aTTRIBUTE2 = value;
                    OnPropertyChanged("ATTRIBUTE2");
                }
            }
        }
        private string _aTTRIBUTE2;
    
        [DataMember]
        public string ATTRIBUTE3
        {
            get { return _aTTRIBUTE3; }
            set
            {
                if (_aTTRIBUTE3 != value)
                {
                    _aTTRIBUTE3 = value;
                    OnPropertyChanged("ATTRIBUTE3");
                }
            }
        }
        private string _aTTRIBUTE3;
    
        [DataMember]
        public string ATTRIBUTE4
        {
            get { return _aTTRIBUTE4; }
            set
            {
                if (_aTTRIBUTE4 != value)
                {
                    _aTTRIBUTE4 = value;
                    OnPropertyChanged("ATTRIBUTE4");
                }
            }
        }
        private string _aTTRIBUTE4;
    
        [DataMember]
        public string ATTRIBUTE5
        {
            get { return _aTTRIBUTE5; }
            set
            {
                if (_aTTRIBUTE5 != value)
                {
                    _aTTRIBUTE5 = value;
                    OnPropertyChanged("ATTRIBUTE5");
                }
            }
        }
        private string _aTTRIBUTE5;
    
        [DataMember]
        public string ATTRIBUTE6
        {
            get { return _aTTRIBUTE6; }
            set
            {
                if (_aTTRIBUTE6 != value)
                {
                    _aTTRIBUTE6 = value;
                    OnPropertyChanged("ATTRIBUTE6");
                }
            }
        }
        private string _aTTRIBUTE6;
    
        [DataMember]
        public string ATTRIBUTE7
        {
            get { return _aTTRIBUTE7; }
            set
            {
                if (_aTTRIBUTE7 != value)
                {
                    _aTTRIBUTE7 = value;
                    OnPropertyChanged("ATTRIBUTE7");
                }
            }
        }
        private string _aTTRIBUTE7;
    
        [DataMember]
        public string ATTRIBUTE8
        {
            get { return _aTTRIBUTE8; }
            set
            {
                if (_aTTRIBUTE8 != value)
                {
                    _aTTRIBUTE8 = value;
                    OnPropertyChanged("ATTRIBUTE8");
                }
            }
        }
        private string _aTTRIBUTE8;
    
        [DataMember]
        public string ATTRIBUTE9
        {
            get { return _aTTRIBUTE9; }
            set
            {
                if (_aTTRIBUTE9 != value)
                {
                    _aTTRIBUTE9 = value;
                    OnPropertyChanged("ATTRIBUTE9");
                }
            }
        }
        private string _aTTRIBUTE9;
    
        [DataMember]
        public string ATTRIBUTE10
        {
            get { return _aTTRIBUTE10; }
            set
            {
                if (_aTTRIBUTE10 != value)
                {
                    _aTTRIBUTE10 = value;
                    OnPropertyChanged("ATTRIBUTE10");
                }
            }
        }
        private string _aTTRIBUTE10;

        #endregion
        #region Navigation Properties
    
        [DataMember]
        public HR_DEPARTMENTS HR_DEPARTMENTS
        {
            get { return _hR_DEPARTMENTS; }
            set
            {
                if (!ReferenceEquals(_hR_DEPARTMENTS, value))
                {
                    var previousValue = _hR_DEPARTMENTS;
                    _hR_DEPARTMENTS = value;
                    FixupHR_DEPARTMENTS(previousValue);
                    OnNavigationPropertyChanged("HR_DEPARTMENTS");
                }
            }
        }
        private HR_DEPARTMENTS _hR_DEPARTMENTS;
    
        [DataMember]
        public TrackableCollection<HR_EMP_MOB_LIMIT_DTL> HR_EMP_MOB_LIMIT_DTL
        {
            get
            {
                if (_hR_EMP_MOB_LIMIT_DTL == null)
                {
                    _hR_EMP_MOB_LIMIT_DTL = new TrackableCollection<HR_EMP_MOB_LIMIT_DTL>();
                    _hR_EMP_MOB_LIMIT_DTL.CollectionChanged += FixupHR_EMP_MOB_LIMIT_DTL;
                }
                return _hR_EMP_MOB_LIMIT_DTL;
            }
            set
            {
                if (!ReferenceEquals(_hR_EMP_MOB_LIMIT_DTL, value))
                {
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        throw new InvalidOperationException("Cannot set the FixupChangeTrackingCollection when ChangeTracking is enabled");
                    }
                    if (_hR_EMP_MOB_LIMIT_DTL != null)
                    {
                        _hR_EMP_MOB_LIMIT_DTL.CollectionChanged -= FixupHR_EMP_MOB_LIMIT_DTL;
                    }
                    _hR_EMP_MOB_LIMIT_DTL = value;
                    if (_hR_EMP_MOB_LIMIT_DTL != null)
                    {
                        _hR_EMP_MOB_LIMIT_DTL.CollectionChanged += FixupHR_EMP_MOB_LIMIT_DTL;
                    }
                    OnNavigationPropertyChanged("HR_EMP_MOB_LIMIT_DTL");
                }
            }
        }
        private TrackableCollection<HR_EMP_MOB_LIMIT_DTL> _hR_EMP_MOB_LIMIT_DTL;
    
        [DataMember]
        public HR_EMPLOYEES HR_EMPLOYEES
        {
            get { return _hR_EMPLOYEES; }
            set
            {
                if (!ReferenceEquals(_hR_EMPLOYEES, value))
                {
                    var previousValue = _hR_EMPLOYEES;
                    _hR_EMPLOYEES = value;
                    FixupHR_EMPLOYEES(previousValue);
                    OnNavigationPropertyChanged("HR_EMPLOYEES");
                }
            }
        }
        private HR_EMPLOYEES _hR_EMPLOYEES;

        #endregion
        #region ChangeTracking
    
        protected virtual void OnPropertyChanged(String propertyName)
        {
            if (ChangeTracker.State != ObjectState.Added && ChangeTracker.State != ObjectState.Deleted)
            {
                ChangeTracker.State = ObjectState.Modified;
            }
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        protected virtual void OnNavigationPropertyChanged(String propertyName)
        {
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        event PropertyChangedEventHandler INotifyPropertyChanged.PropertyChanged{ add { _propertyChanged += value; } remove { _propertyChanged -= value; } }
        private event PropertyChangedEventHandler _propertyChanged;
        private ObjectChangeTracker _changeTracker;
    
        [DataMember]
        public ObjectChangeTracker ChangeTracker
        {
            get
            {
                if (_changeTracker == null)
                {
                    _changeTracker = new ObjectChangeTracker();
                    _changeTracker.ObjectStateChanging += HandleObjectStateChanging;
                }
                return _changeTracker;
            }
            set
            {
                if(_changeTracker != null)
                {
                    _changeTracker.ObjectStateChanging -= HandleObjectStateChanging;
                }
                _changeTracker = value;
                if(_changeTracker != null)
                {
                    _changeTracker.ObjectStateChanging += HandleObjectStateChanging;
                }
            }
        }
    
        private void HandleObjectStateChanging(object sender, ObjectStateChangingEventArgs e)
        {
            if (e.NewState == ObjectState.Deleted)
            {
                ClearNavigationProperties();
            }
        }
    
        protected bool IsDeserializing { get; private set; }
    
        [OnDeserializing]
        public void OnDeserializingMethod(StreamingContext context)
        {
            IsDeserializing = true;
        }
    
        [OnDeserialized]
        public void OnDeserializedMethod(StreamingContext context)
        {
            IsDeserializing = false;
            ChangeTracker.ChangeTrackingEnabled = true;
        }
    
        protected virtual void ClearNavigationProperties()
        {
            HR_DEPARTMENTS = null;
            HR_EMP_MOB_LIMIT_DTL.Clear();
            HR_EMPLOYEES = null;
        }

        #endregion
        #region Association Fixup
    
        private void FixupHR_DEPARTMENTS(HR_DEPARTMENTS previousValue, bool skipKeys = false)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (previousValue != null && previousValue.HR_EMP_MOB_LIMIT_HDR.Contains(this))
            {
                previousValue.HR_EMP_MOB_LIMIT_HDR.Remove(this);
            }
    
            if (HR_DEPARTMENTS != null)
            {
                if (!HR_DEPARTMENTS.HR_EMP_MOB_LIMIT_HDR.Contains(this))
                {
                    HR_DEPARTMENTS.HR_EMP_MOB_LIMIT_HDR.Add(this);
                }
    
                DEPT_ID = HR_DEPARTMENTS.DEPT_ID;
            }
            else if (!skipKeys)
            {
                DEPT_ID = null;
            }
    
            if (ChangeTracker.ChangeTrackingEnabled)
            {
                if (ChangeTracker.OriginalValues.ContainsKey("HR_DEPARTMENTS")
                    && (ChangeTracker.OriginalValues["HR_DEPARTMENTS"] == HR_DEPARTMENTS))
                {
                    ChangeTracker.OriginalValues.Remove("HR_DEPARTMENTS");
                }
                else
                {
                    ChangeTracker.RecordOriginalValue("HR_DEPARTMENTS", previousValue);
                }
                if (HR_DEPARTMENTS != null && !HR_DEPARTMENTS.ChangeTracker.ChangeTrackingEnabled)
                {
                    HR_DEPARTMENTS.StartTracking();
                }
            }
        }
    
        private void FixupHR_EMPLOYEES(HR_EMPLOYEES previousValue, bool skipKeys = false)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (previousValue != null && previousValue.HR_EMP_MOB_LIMIT_HDR.Contains(this))
            {
                previousValue.HR_EMP_MOB_LIMIT_HDR.Remove(this);
            }
    
            if (HR_EMPLOYEES != null)
            {
                if (!HR_EMPLOYEES.HR_EMP_MOB_LIMIT_HDR.Contains(this))
                {
                    HR_EMPLOYEES.HR_EMP_MOB_LIMIT_HDR.Add(this);
                }
    
                EMP_ID = HR_EMPLOYEES.EMP_ID;
            }
            else if (!skipKeys)
            {
                EMP_ID = null;
            }
    
            if (ChangeTracker.ChangeTrackingEnabled)
            {
                if (ChangeTracker.OriginalValues.ContainsKey("HR_EMPLOYEES")
                    && (ChangeTracker.OriginalValues["HR_EMPLOYEES"] == HR_EMPLOYEES))
                {
                    ChangeTracker.OriginalValues.Remove("HR_EMPLOYEES");
                }
                else
                {
                    ChangeTracker.RecordOriginalValue("HR_EMPLOYEES", previousValue);
                }
                if (HR_EMPLOYEES != null && !HR_EMPLOYEES.ChangeTracker.ChangeTrackingEnabled)
                {
                    HR_EMPLOYEES.StartTracking();
                }
            }
        }
    
        private void FixupHR_EMP_MOB_LIMIT_DTL(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (e.NewItems != null)
            {
                foreach (HR_EMP_MOB_LIMIT_DTL item in e.NewItems)
                {
                    item.HR_EMP_MOB_LIMIT_HDR = this;
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        if (!item.ChangeTracker.ChangeTrackingEnabled)
                        {
                            item.StartTracking();
                        }
                        ChangeTracker.RecordAdditionToCollectionProperties("HR_EMP_MOB_LIMIT_DTL", item);
                    }
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (HR_EMP_MOB_LIMIT_DTL item in e.OldItems)
                {
                    if (ReferenceEquals(item.HR_EMP_MOB_LIMIT_HDR, this))
                    {
                        item.HR_EMP_MOB_LIMIT_HDR = null;
                    }
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        ChangeTracker.RecordRemovalFromCollectionProperties("HR_EMP_MOB_LIMIT_DTL", item);
                    }
                }
            }
        }

        #endregion
    }
}
