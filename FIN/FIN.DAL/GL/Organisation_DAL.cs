﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.GL
{
    public class Organisation_DAL
    {
        static string sqlQuery = "";


        public static string getStructName()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT S.ACCT_STRUCT_ID,S.ACCT_STRUCT_NAME" + VMVServices.Web.Utils.LanguageCode + " as ACCT_STRUCT_NAME";
            sqlQuery += " FROM GL_ACCT_STRUCTURE S";
            sqlQuery += " WHERE S.ENABLED_FLAG = 1";
            sqlQuery += " AND S.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " ORDER BY S.ACCT_STRUCT_NAME ";
            return sqlQuery;
        }

        public static string getCalendar()
        {
            sqlQuery = string.Empty;
            sqlQuery = "  SELECT c.cal_id,c.cal_desc" + VMVServices.Web.Utils.LanguageCode + " as cal_desc";
            sqlQuery += " FROM gl_acct_calendar_hdr c";
            sqlQuery += " WHERE c.ENABLED_FLAG = 1";
            sqlQuery += " AND c.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " ORDER BY c.cal_desc ";
            return sqlQuery;

        }


        public static string GetEligiblePlan()
        {
            sqlQuery = string.Empty;
            sqlQuery = "  Select PLAN_ID as ELIGIBLE_PLAN_ID , PLAN_LEVAL as ELIGIBLE_PLAN_LEVEL from INSURANCE_PLAN ";
            sqlQuery += "  where ORG_ID = '" + VMVServices.Web.Utils.OrganizationID + "'";
            return sqlQuery;
        }

        public static string GetSelectedPlan()
        {
            sqlQuery = string.Empty;
            sqlQuery = "  Select PLAN_ID as SELECTED_PLAN_ID , PLAN_LEVAL as SELECTED_PLAN_LEVEL from INSURANCE_PLAN ";
            sqlQuery += "  where ORG_ID = '" + VMVServices.Web.Utils.OrganizationID + "'";
            return sqlQuery;
        }

        public static string getCountry()
        {
            sqlQuery = string.Empty;
            sqlQuery = "  select C.COUNTRY_CODE,C.FULL_NAME";
            sqlQuery += " from SSM_COUNTRIES C";
            sqlQuery += " WHERE C.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND C.ENABLED_FLAG = 1";

            sqlQuery += " ORDER BY COUNTRY_CODE ";

            return sqlQuery;

        }
        public static string getOrganisationDetails()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT GCH.comp_logo,GCH.COMP_ID AS ORG_ID,(GCH.COMP_INTERNAL_NAME" + VMVServices.Web.Utils.LanguageCode + ") AS ORG_NAME ";
            //sqlQuery = " SELECT GCH.comp_logo,GCH.COMP_ID AS ORG_ID,(GCH.COMP_INTERNAL_NAME" + VMVServices.Web.Utils.LanguageCode + " || ' - ' || GCH.COMP_EXTERNAL_NAME|| ' - ' || GCH.COMP_SHORT_NAME) AS ORG_NAME ";
            sqlQuery += " FROM GL_COMPANIES_HDR GCH ";
            sqlQuery += " INNER JOIN  SSM_USER_ORG_INTERSECTS SUOI ON SUOI.ORG_ID= GCH.COMP_ID";
            sqlQuery += " WHERE GCH.ENABLED_FLAG = 1 ";
            sqlQuery += " AND GCH.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " AND SUOI.USER_CODE='" + VMVServices.Web.Utils.UserName + "'";
            sqlQuery += " ORDER BY ORG_ID ";

            return sqlQuery;
        }

        public static string getOrganisationData(string orgId = "")
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT GCH.comp_logo,GCH.COMP_ID AS ORG_ID,(GCH.COMP_INTERNAL_NAME" + VMVServices.Web.Utils.LanguageCode + ") AS ORG_NAME ";
            sqlQuery += " FROM GL_COMPANIES_HDR GCH ";
            sqlQuery += " INNER JOIN  SSM_USER_ORG_INTERSECTS SUOI ON SUOI.ORG_ID= GCH.COMP_ID";
            sqlQuery += " WHERE GCH.ENABLED_FLAG = 1 ";
            sqlQuery += " AND GCH.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " AND SUOI.USER_CODE='" + VMVServices.Web.Utils.UserName + "'";
            sqlQuery += " AND GCH.COMP_ID='" + orgId + "'";
            sqlQuery += " ORDER BY ORG_ID ";

            return sqlQuery;
        }

        public static string getCompanyNM()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT GCH.COMP_ID,(GCH.COMP_INTERNAL_NAME" + VMVServices.Web.Utils.LanguageCode + ") AS COMP_INTERNAL_NAME ";
            sqlQuery += " FROM GL_COMPANIES_HDR GCH ";
            sqlQuery += " WHERE GCH.ENABLED_FLAG = 1 ";
            sqlQuery += " AND GCH.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " ORDER BY COMP_ID ";

            return sqlQuery;
        }

        public static string GetCompanydtls(string Master_id)
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT SC.CURRENCY_ID,SC.CURRENCY_CODE||''||SC.CURRENCY_DESC AS CURRENCY_NAME,CD.PK_ID,CD.COMP_DTL_ID,CD.COMP_BASE_CURRENCY,AC.CAL_ID,AC.CAL_DESC" + VMVServices.Web.Utils.LanguageCode + " as CAL_DESC,S.ACCT_STRUCT_ID,S.ACCT_STRUCT_NAME" + VMVServices.Web.Utils.LanguageCode + " as ACCT_STRUCT_NAME,CD.EFFECTIVE_START_DT,CD.EFFECTIVE_END_DT,'N' AS DELETED, ";
            sqlQuery += " case cd.enabled_flag when '1' then 'TRUE' else 'FALSE' end as ENABLED_FLAG";
            sqlQuery += " FROM GL_COMPANIES_DTL CD,GL_ACCT_CALENDAR_HDR AC,GL_ACCT_STRUCTURE S,SSM_CURRENCIES SC";
            sqlQuery += " WHERE CD.COMP_ACCT_STRUCT_ID = S.ACCT_STRUCT_ID";
            sqlQuery += " AND  CD.COMP_ID = '" + Master_id + "'";
            sqlQuery += " AND CD.COMP_CAL_ID = AC.CAL_ID";
            sqlQuery += " AND CD.COMP_BASE_CURRENCY = SC.CURRENCY_ID";
            sqlQuery += " AND CD.WORKFLOW_COMPLETION_STATUS = 1";
            //            sqlQuery += " AND CD.ENABLED_FLAG = 1";   

            return sqlQuery;

        }

        public static string getCompanyCurrency(string str_Compid)
        {
            sqlQuery = string.Empty;
            sqlQuery = "SELECT C.CURRENCY_ID,C.CURRENCY_CODE,C.CURRENCY_DESC,C.CURRENCY_SYMBOL from GL_COMPANIES_DTL CD ";
            sqlQuery += " INNER JOIN SSM_CURRENCIES C ON C.CURRENCY_ID= CD.COMP_BASE_CURRENCY ";
            sqlQuery += " WHERE CD.ENABLED_FLAG=1 AND CD.COMP_ID ='" + str_Compid + "'";
            return sqlQuery;
        }
        public static string getCompGlobalSegment()
        {
            sqlQuery = string.Empty;

            sqlQuery = "    select to_char(nvl(c.COMP_GLOBAL_SEGMENT_ID, 0)) as COMP_GLOBAL_SEGMENT_ID";
            sqlQuery += "   from gl_companies_hdr c";
            sqlQuery += "  where c.comp_id = '" + VMVServices.Web.Utils.OrganizationID + "'";
            return sqlQuery;
        }

        public static string getOrgCalPeriodDetails(string strCompId, string CAL_DTL_ID)
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT PD.CAL_DTL_ID, pd.pk_id,pd.period_id,pd.period_name,pd.period_type,pd.period_from_dt,pd.period_to_dt,C.CODE AS LOOKUP_ID,C.CODE AS LOOKUP_NAME,'N' AS DELETED,CAPD.PERIOD_NUMBER";
            sqlQuery += " FROM GL_ACCT_PERIOD_DTL pd,ssm_code_masters C,gl_comp_acct_period_dtl capd";
            sqlQuery += " where pd.workflow_completion_status = 1";
            sqlQuery += " and capd.period_id = pd.period_id ";
            sqlQuery += " AND capd.PERIOD_STATUS =  C.CODE";
            sqlQuery += " and pd.enabled_flag =1";
            sqlQuery += " and pd.CAL_DTL_ID = '" + CAL_DTL_ID + "'";
            sqlQuery += " UNION ";
            sqlQuery += " SELECT PD.CAL_DTL_ID, 0 as pk_id,pd.period_id,pd.period_name,pd.period_type,pd.period_from_dt,pd.period_to_dt,C.CODE AS LOOKUP_ID,C.CODE AS LOOKUP_NAME,'N' AS DELETED,PERIOD_NUMBER";
            sqlQuery += " FROM GL_ACCT_PERIOD_DTL pd,ssm_code_masters C";
            sqlQuery += " where pd.workflow_completion_status = 1";
            sqlQuery += " AND PD.PERIOD_STATUS =  C.CODE";
            sqlQuery += " and pd.enabled_flag =1";
            sqlQuery += " and pd.CAL_DTL_ID = '" + CAL_DTL_ID + "'";
            //sqlQuery += " and pd.period_id not in (select period_id from gl_comp_acct_period_dtl where comp_id='" + strCompId + "' and cal_dtl_id='" + CAL_DTL_ID + "')";
            sqlQuery += " and   NOT exists  (select period_id from gl_comp_acct_period_dtl dd where comp_id='" + strCompId + "'  and dd.period_id=pd.period_id  and cal_dtl_id='" + CAL_DTL_ID + "')";
            sqlQuery += " order by PERIOD_NUMBER";
            return sqlQuery;
        }

        public static string getJournalGlobalSegment()
        {
            sqlQuery = string.Empty;

            sqlQuery = " select count(*)";
            sqlQuery += " from GL_JOURNAL_HDR j";
            sqlQuery += " where j.workflow_completion_status = 1";
            sqlQuery += " and j.enabled_flag = 1";

            return sqlQuery;
        }

        public static string getPeriodCalendarDtls()
        {
            sqlQuery = string.Empty;

            sqlQuery = " select acd.comp_id, acd.cal_dtl_id, acd.cal_id, acd.cal_acct_year";
            sqlQuery += " from GL_COMP_ACCT_CALENDAR_DTL acd";
            sqlQuery += " where acd.enabled_flag=1";
            sqlQuery += " and acd.WORKFLOW_COMPLETION_STATUS=1";
            sqlQuery += " order by acd.cal_acct_year asc";

            //sqlQuery = " SELECT distinct c.cal_id, c.cal_desc, d.cal_dtl_id, d.cal_acct_year";
            //sqlQuery += " FROM gl_acct_calendar_hdr c, gl_acct_calendar_dtl d";
            //sqlQuery += " WHERE c.ENABLED_FLAG = 1";
            //sqlQuery += " AND c.WORKFLOW_COMPLETION_STATUS = 1";
            //sqlQuery += " and d.cal_id = c.cal_id";
            //sqlQuery += " ORDER BY c.cal_desc";

            return sqlQuery;
        }

        public static string getPeriodStatus(string calDtlId)
        {
            sqlQuery = string.Empty;

            sqlQuery += " SELECT PD.CAL_DTL_ID, pd.pk_id,pd.period_id,pd.period_name,pd.period_type,pd.period_from_dt,pd.period_to_dt,C.CODE AS LOOKUP_ID,C.CODE AS LOOKUP_NAME,'N' AS DELETED,CAPD.PERIOD_NUMBER";
            sqlQuery += " FROM GL_ACCT_PERIOD_DTL pd,ssm_code_masters C,gl_comp_acct_period_dtl capd";
            sqlQuery += " where pd.workflow_completion_status = 1";
            sqlQuery += " and capd.period_id = pd.period_id ";
            sqlQuery += " AND capd.PERIOD_STATUS =  C.CODE";
            sqlQuery += " and pd.enabled_flag =1";
            sqlQuery += " and pd.CAL_DTL_ID = '" + calDtlId + "'";
            sqlQuery += " UNION ";
            sqlQuery += " SELECT PD.CAL_DTL_ID, 0 as pk_id,pd.period_id,pd.period_name,pd.period_type,pd.period_from_dt,pd.period_to_dt,C.CODE AS LOOKUP_ID,C.CODE AS LOOKUP_NAME,'N' AS DELETED,PERIOD_NUMBER";
            sqlQuery += " FROM GL_ACCT_PERIOD_DTL pd,ssm_code_masters C";
            sqlQuery += " where pd.workflow_completion_status = 1";
            sqlQuery += " AND PD.PERIOD_STATUS =  C.CODE";
            sqlQuery += " and pd.enabled_flag =1";
            sqlQuery += " and pd.CAL_DTL_ID = '" + calDtlId + "'";
            sqlQuery += " and   NOT exists  (select period_id from gl_comp_acct_period_dtl dd where dd.period_id=pd.period_id  and cal_dtl_id='" + calDtlId + "')";
            sqlQuery += " order by PERIOD_NUMBER";

            return sqlQuery;
        }

        public static string getAcctPeriodReportData()
        {
            sqlQuery = string.Empty;

            sqlQuery += " SELECT * FROM VW_GL_ACCT_PERIOD  V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["CalendarID"] != null)
                {
                    sqlQuery += " AND V.cal_dtl_id = '" + VMVServices.Web.Utils.ReportViewFilterParameter["CalendarID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["PeriodStatusID"] != null)
                {
                    sqlQuery += " AND V.period_status = '" + VMVServices.Web.Utils.ReportViewFilterParameter["PeriodStatusID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["CREATED_BY"] != null)
                {
                    sqlQuery += " AND UPPER(V.CREATED_BY) = UPPER('" + VMVServices.Web.Utils.ReportViewFilterParameter["CREATED_BY"].ToString() + "')";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["MODIFIED_BY"] != null)
                {
                    sqlQuery += " AND UPPER(V.MODIFIED_BY) = UPPER('" + VMVServices.Web.Utils.ReportViewFilterParameter["MODIFIED_BY"].ToString() + "')";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["CREATED_From_Date"] != null)
                {
                    sqlQuery += " AND V.CREATED_DATE >= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["CREATED_From_Date"].ToString() + "','dd/MM/yyyy') ";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["CREATED_To_Date"] != null)
                {
                    sqlQuery += " AND V.CREATED_DATE <= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["CREATED_To_Date"].ToString() + "','dd/MM/yyyy') ";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["MODIFIED_From_Date"] != null)
                {
                    sqlQuery += " AND V.MODIFIED_DATE >= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["MODIFIED_From_Date"].ToString() + "','dd/MM/yyyy') ";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["MODIFIED_To_Date"] != null)
                {
                    sqlQuery += " AND V.MODIFIED_DATE <= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["MODIFIED_To_Date"].ToString() + "','dd/MM/yyyy') ";
                }
            }

            return sqlQuery;
        }


    }
}
