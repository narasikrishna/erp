﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;

using System.Data.EntityClient;



using System.Data.Entity.Infrastructure;


namespace FIN.DAL.GL
{
    public class TrailBalance_DAL
    {
        static string spOutPut = string.Empty;
        public static System.Collections.SortedList ErrorCollection = new System.Collections.SortedList();

        static string sqlQuery = "";
        public static string getReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery += " SELECT * FROM GL_TRIAL_BALANCE_ACCOUNT_GROUP V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportFilterParameter["FromAcctGrp"] != null)
            {
                sqlQuery += " AND V.ACCOUNTING_GROUP >= '" + VMVServices.Web.Utils.ReportFilterParameter["FromAcctGrp"].ToString() + "'";
            }
            if (VMVServices.Web.Utils.ReportFilterParameter["ToAcctGrp"] != null)
            {
                sqlQuery += " AND V.ACCOUNTING_GROUP <= '" + VMVServices.Web.Utils.ReportFilterParameter["ToAcctGrp"].ToString() + "'";
            }
            if (VMVServices.Web.Utils.ReportFilterParameter["Debit_From_Amt"] != null || VMVServices.Web.Utils.ReportFilterParameter["Debit_To_Amt"] != null || VMVServices.Web.Utils.ReportFilterParameter["Credit_From_Amt"] != null || VMVServices.Web.Utils.ReportFilterParameter["Credit_To_Amt"] != null)
            {
                sqlQuery += " AND ( ";
                if (VMVServices.Web.Utils.ReportFilterParameter["Debit_From_Amt"] != null && VMVServices.Web.Utils.ReportFilterParameter["Debit_To_Amt"] != null)
                {
                    sqlQuery += " (V.ACCOUNTED_DR >= '" + (VMVServices.Web.Utils.ReportFilterParameter["Debit_From_Amt"] == null ? "0" : VMVServices.Web.Utils.ReportFilterParameter["Debit_From_Amt"].ToString()) + "')  ";
                    sqlQuery += " AND ( V.ACCOUNTED_DR <= '" + (VMVServices.Web.Utils.ReportFilterParameter["Debit_To_Amt"] == null ? "0" : VMVServices.Web.Utils.ReportFilterParameter["Debit_To_Amt"].ToString()) + "') ";

                }
                else if (VMVServices.Web.Utils.ReportFilterParameter["Debit_From_Amt"] != null)
                {
                    sqlQuery += " (V.ACCOUNTED_DR >= '" + (VMVServices.Web.Utils.ReportFilterParameter["Debit_From_Amt"] == null ? "0" : VMVServices.Web.Utils.ReportFilterParameter["Debit_From_Amt"].ToString()) + "')  ";


                }
                else if (VMVServices.Web.Utils.ReportFilterParameter["Debit_To_Amt"] != null)
                {
                    sqlQuery += "  (V.ACCOUNTED_DR <= '" + (VMVServices.Web.Utils.ReportFilterParameter["Debit_To_Amt"] == null ? "0" : VMVServices.Web.Utils.ReportFilterParameter["Debit_To_Amt"].ToString()) + "') ";

                }

                if ((VMVServices.Web.Utils.ReportFilterParameter["Debit_From_Amt"] != null || VMVServices.Web.Utils.ReportFilterParameter["Debit_To_Amt"] != null) && (VMVServices.Web.Utils.ReportFilterParameter["Credit_From_Amt"] != null || VMVServices.Web.Utils.ReportFilterParameter["Credit_To_Amt"] != null))
                { sqlQuery += " OR "; }


                if (VMVServices.Web.Utils.ReportFilterParameter["Credit_From_Amt"] != null && VMVServices.Web.Utils.ReportFilterParameter["Credit_To_Amt"] != null)
                {
                    sqlQuery += " (V.ACCOUNTED_CR >= '" + (VMVServices.Web.Utils.ReportFilterParameter["Credit_From_Amt"] == null ? "0" : VMVServices.Web.Utils.ReportFilterParameter["Credit_From_Amt"].ToString()) + "') ";
                    sqlQuery += " AND ( V.ACCOUNTED_CR <= '" + (VMVServices.Web.Utils.ReportFilterParameter["Credit_To_Amt"] == null ? "0" : VMVServices.Web.Utils.ReportFilterParameter["Credit_To_Amt"].ToString()) + "') ";
                }
                else if (VMVServices.Web.Utils.ReportFilterParameter["Credit_From_Amt"] != null)
                {
                    sqlQuery += " (V.ACCOUNTED_CR >= '" + (VMVServices.Web.Utils.ReportFilterParameter["Credit_From_Amt"] == null ? "0" : VMVServices.Web.Utils.ReportFilterParameter["Credit_From_Amt"].ToString()) + "') ";

                }
                else if (VMVServices.Web.Utils.ReportFilterParameter["Credit_To_Amt"] != null)
                {
                    sqlQuery += "  ( V.ACCOUNTED_CR <= '" + (VMVServices.Web.Utils.ReportFilterParameter["Credit_To_Amt"] == null ? "0" : VMVServices.Web.Utils.ReportFilterParameter["Credit_To_Amt"].ToString()) + "') ";
                }


                sqlQuery += " )";
            }
            //if (VMVServices.Web.Utils.ReportFilterParameter["Debit_From_Amt"] != null)
            //{
            //    sqlQuery += " and (v.ACCOUNTED_DR) >= " + VMVServices.Web.Utils.ReportFilterParameter["Debit_From_Amt"];
            //}
            //if (VMVServices.Web.Utils.ReportFilterParameter["Debit_To_Amt"] != null)
            //{
            //    sqlQuery += " and (v.ACCOUNTED_DR) <= " + VMVServices.Web.Utils.ReportFilterParameter["Debit_To_Amt"];
            //}
            //if (VMVServices.Web.Utils.ReportFilterParameter["Credit_From_Amt"] != null)
            //{
            //    sqlQuery += " and (v.ACCOUNTED_CR) >= " + VMVServices.Web.Utils.ReportFilterParameter["Credit_From_Amt"];
            //}
            //if (VMVServices.Web.Utils.ReportFilterParameter["Credit_To_Amt"] != null)
            //{
            //    sqlQuery += " and (v.ACCOUNTED_CR) <= " + VMVServices.Web.Utils.ReportFilterParameter["Credit_To_Amt"];
            //}
            if (VMVServices.Web.Utils.ReportFilterParameter["WITHZERO"] == "FALSE")
            {
                sqlQuery += " AND (v.accounted_dr > 0 or v.accounted_cr > 0)  ";
            }
            //if (VMVServices.Web.Utils.ReportFilterParameter != null || VMVServices.Web.Utils.ReportFilterParameter.Count != 0)
            //{
            //    if (VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.GROUP_ID] != null)
            //    {
            //        sqlQuery += " AND V.ACCT_GRP_ID = '" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.GROUP_ID].ToString() + "'";
            //    }

            //}

            return sqlQuery;
        }
        public static string getGlobalSegment()
        {
            sqlQuery = string.Empty;
            sqlQuery += " SELECT SV.SEGMENT_ID,SV.SEGMENT_VALUE" + VMVServices.Web.Utils.LanguageCode;
            sqlQuery += " FROM GL_SEGMENT_VALUES SV ";
            sqlQuery += " WHERE SV.WORKFLOW_COMPLETION_STATUS='1' ";
            sqlQuery += " AND SV.ENABLED_FLAG='1'";


            return sqlQuery;
        }
        public static DataSet GetSP_TrailBalanceDetail(string global_segment_id, DateTime tb_date, string str_unposted, string str_Query)
        {
            try
            {
                string strValue = string.Empty;
                string strNextCode = string.Empty;
                string strErrMsg = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "GL_PKG.GL_Trial_Balance_detail";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add(new OracleParameter("@p_org_id", OracleDbType.Varchar2, 250)).Value = VMVServices.Web.Utils.OrganizationID;
                oraCmd.Parameters.Add(new OracleParameter("@p_global_segment_id", OracleDbType.Varchar2, 250)).Value = global_segment_id;
                oraCmd.Parameters.Add(new OracleParameter("@p_date", OracleDbType.Date, 250)).Value = tb_date.ToString("dd/MMM/yyyy");



                return DBMethod.ExecuteStoredProcedure(oraCmd, str_Query);



            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static DataSet GetSP_GetAccountCostCenterSummary(DateTime tb_date, string str_unposted, string str_Query)
        {
            try
            {
                string strValue = string.Empty;
                string strNextCode = string.Empty;
                string strErrMsg = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "GL_PKG.GL_Account_CostCenter_Summay";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add(new OracleParameter("@p_org_id", OracleDbType.Varchar2, 250)).Value = VMVServices.Web.Utils.OrganizationID;
                oraCmd.Parameters.Add(new OracleParameter("@p_date", OracleDbType.Date, 250)).Value = tb_date.ToString("dd/MMM/yyyy");



                return DBMethod.ExecuteStoredProcedure(oraCmd, str_Query);



            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string getTrailBalanceDetailReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery += " SELECT * FROM GL_TRIAL_BALANCE_DETAILS V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportFilterParameter != null || VMVServices.Web.Utils.ReportFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportFilterParameter["FromAccount"] != null)
                {
                    sqlQuery += " AND V.ACCOUNT_CODE >=  '" + VMVServices.Web.Utils.ReportFilterParameter["FromAccount"].ToString() + "'";
                }

                if (VMVServices.Web.Utils.ReportFilterParameter["ToAccount"] != null)
                {
                    sqlQuery += " AND V.ACCOUNT_CODE <=  '" + VMVServices.Web.Utils.ReportFilterParameter["ToAccount"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["From_DebitAmt"] != null || VMVServices.Web.Utils.ReportFilterParameter["To_DebitAmt"] != null || VMVServices.Web.Utils.ReportFilterParameter["From_CreditAmt"] != null || VMVServices.Web.Utils.ReportFilterParameter["To_CreditAmt"] != null)
                {
                    sqlQuery += " AND ( ";
                    if (VMVServices.Web.Utils.ReportFilterParameter["From_DebitAmt"] != null && VMVServices.Web.Utils.ReportFilterParameter["To_DebitAmt"] != null)
                    {
                        sqlQuery += " (V.ACCOUNTED_DR >= '" + (VMVServices.Web.Utils.ReportFilterParameter["From_DebitAmt"] == null ? "0" : VMVServices.Web.Utils.ReportFilterParameter["From_DebitAmt"].ToString()) + "')  ";
                        sqlQuery += " AND ( V.ACCOUNTED_DR <= '" + (VMVServices.Web.Utils.ReportFilterParameter["To_DebitAmt"] == null ? "0" : VMVServices.Web.Utils.ReportFilterParameter["To_DebitAmt"].ToString()) + "') ";

                    }
                    else if (VMVServices.Web.Utils.ReportFilterParameter["From_DebitAmt"] != null)
                    {
                        sqlQuery += " (V.ACCOUNTED_DR >= '" + (VMVServices.Web.Utils.ReportFilterParameter["From_DebitAmt"] == null ? "0" : VMVServices.Web.Utils.ReportFilterParameter["From_DebitAmt"].ToString()) + "')  ";


                    }
                    else if (VMVServices.Web.Utils.ReportFilterParameter["To_DebitAmt"] != null)
                    {
                        sqlQuery += "  (V.ACCOUNTED_DR <= '" + (VMVServices.Web.Utils.ReportFilterParameter["To_DebitAmt"] == null ? "0" : VMVServices.Web.Utils.ReportFilterParameter["To_DebitAmt"].ToString()) + "') ";

                    }

                    if ((VMVServices.Web.Utils.ReportFilterParameter["From_DebitAmt"] != null || VMVServices.Web.Utils.ReportFilterParameter["To_DebitAmt"] != null) && (VMVServices.Web.Utils.ReportFilterParameter["From_CreditAmt"] != null || VMVServices.Web.Utils.ReportFilterParameter["To_CreditAmt"] != null))
                    { sqlQuery += " OR "; }


                    if (VMVServices.Web.Utils.ReportFilterParameter["From_CreditAmt"] != null && VMVServices.Web.Utils.ReportFilterParameter["To_CreditAmt"] != null)
                    {
                        sqlQuery += " (V.ACCOUNTED_CR >= '" + (VMVServices.Web.Utils.ReportFilterParameter["From_CreditAmt"] == null ? "0" : VMVServices.Web.Utils.ReportFilterParameter["From_CreditAmt"].ToString()) + "') ";
                        sqlQuery += " AND ( V.ACCOUNTED_CR <= '" + (VMVServices.Web.Utils.ReportFilterParameter["To_CreditAmt"] == null ? "0" : VMVServices.Web.Utils.ReportFilterParameter["To_CreditAmt"].ToString()) + "') ";
                    }
                    else if (VMVServices.Web.Utils.ReportFilterParameter["From_CreditAmt"] != null)
                    {
                        sqlQuery += " (V.ACCOUNTED_CR >= '" + (VMVServices.Web.Utils.ReportFilterParameter["From_CreditAmt"] == null ? "0" : VMVServices.Web.Utils.ReportFilterParameter["From_CreditAmt"].ToString()) + "') ";

                    }
                    else if (VMVServices.Web.Utils.ReportFilterParameter["To_CreditAmt"] != null)
                    {
                        sqlQuery += "  ( V.ACCOUNTED_CR <= '" + (VMVServices.Web.Utils.ReportFilterParameter["To_CreditAmt"] == null ? "0" : VMVServices.Web.Utils.ReportFilterParameter["To_CreditAmt"].ToString()) + "') ";
                    }


                    sqlQuery += " )";
                }


                //if (VMVServices.Web.Utils.ReportFilterParameter["From_DebitAmt"] != null)
                //{
                //    sqlQuery += " and V.ACCOUNTED_DR >= '" + VMVServices.Web.Utils.ReportFilterParameter["From_DebitAmt"] + "'";
                //}
                //if (VMVServices.Web.Utils.ReportFilterParameter["To_DebitAmt"] != null)
                //{
                //    sqlQuery += " and V.ACCOUNTED_DR <= '" + VMVServices.Web.Utils.ReportFilterParameter["To_DebitAmt"] + "'";
                //}
                //if (VMVServices.Web.Utils.ReportFilterParameter["From_CreditAmt"] != null)
                //{
                //    sqlQuery += " and V.ACCOUNTED_CR >= '" + VMVServices.Web.Utils.ReportFilterParameter["From_CreditAmt"] + "'";
                //}
                //if (VMVServices.Web.Utils.ReportFilterParameter["To_CreditAmt"] != null)
                //{
                //    sqlQuery += " and V.ACCOUNTED_CR <= '" + VMVServices.Web.Utils.ReportFilterParameter["To_CreditAmt"] + "'";
                //}
                if (VMVServices.Web.Utils.ReportFilterParameter["WITHZERO"] == "FALSE")
                {
                    sqlQuery += " AND (V.ACCOUNTED_DR >0 OR V.ACCOUNTED_CR >0)  ";
                }

            }

            return sqlQuery;
        }

        public static string getAccountCostCenterSummary()
        {
            sqlQuery = string.Empty;
            sqlQuery += " SELECT * FROM GL_Account_CostCenter_Summay V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportFilterParameter != null || VMVServices.Web.Utils.ReportFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportFilterParameter["WITHZERO"] == "FALSE")
                {
                    sqlQuery += " AND (V.ACCOUNTED_DR >0 OR V.ACCOUNTED_CR >0)  ";
                }

            }

            return sqlQuery;
        }
        public static string getDayBookCostCenterSummary()
        {
            sqlQuery = string.Empty;
            sqlQuery += " SELECT * FROM vw_account_costcenter_summay V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportFilterParameter["SEGMENT_ID"] != null)
                {
                    sqlQuery += " AND V.GLOBAL_SEGMENT_ID = '" + VMVServices.Web.Utils.ReportFilterParameter["SEGMENT_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["SEGMENT_VALUE_ID"] != null)
                {
                    sqlQuery += " AND V.segment_value_id = '" + VMVServices.Web.Utils.ReportFilterParameter["SEGMENT_VALUE_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
                {
                    sqlQuery += " AND V.je_date between to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy')";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
                {
                    sqlQuery += " AND to_date('" + VMVServices.Web.Utils.ReportFilterParameter["To_Date"].ToString() + "','dd/MM/yyyy')";
                }
                //if (VMVServices.Web.Utils.ReportFilterParameter["je_number"] != null)
                //{
                //    sqlQuery += " AND V.je_number = '" + VMVServices.Web.Utils.ReportFilterParameter["je_number"].ToString() + "'";

                //}

                if (VMVServices.Web.Utils.ReportFilterParameter["From_Value"] != null || VMVServices.Web.Utils.ReportFilterParameter["To_Value"] != null || VMVServices.Web.Utils.ReportFilterParameter["From_Credit_Value"] != null || VMVServices.Web.Utils.ReportFilterParameter["To_Credit_Value"] != null)
                {
                    sqlQuery += " AND ( ";
                    if (VMVServices.Web.Utils.ReportFilterParameter["From_Value"] != null && VMVServices.Web.Utils.ReportFilterParameter["To_Value"] != null)
                    {
                        sqlQuery += " (V.accounted_dr >= '" + (VMVServices.Web.Utils.ReportFilterParameter["From_Value"] == null ? "0" : VMVServices.Web.Utils.ReportFilterParameter["From_Value"].ToString()) + "')  ";
                        sqlQuery += " AND ( V.accounted_dr <= '" + (VMVServices.Web.Utils.ReportFilterParameter["To_Value"] == null ? "0" : VMVServices.Web.Utils.ReportFilterParameter["To_Value"].ToString()) + "') ";

                    }
                    else if (VMVServices.Web.Utils.ReportFilterParameter["From_Value"] != null)
                    {
                        sqlQuery += " (V.accounted_dr >= '" + (VMVServices.Web.Utils.ReportFilterParameter["From_Value"] == null ? "0" : VMVServices.Web.Utils.ReportFilterParameter["From_Value"].ToString()) + "')  ";


                    }
                    else if (VMVServices.Web.Utils.ReportFilterParameter["To_Value"] != null)
                    {
                        sqlQuery += "  (V.accounted_dr <= '" + (VMVServices.Web.Utils.ReportFilterParameter["To_Value"] == null ? "0" : VMVServices.Web.Utils.ReportFilterParameter["To_Value"].ToString()) + "') ";

                    }

                    if ((VMVServices.Web.Utils.ReportFilterParameter["From_Value"] != null || VMVServices.Web.Utils.ReportFilterParameter["To_Value"] != null) && (VMVServices.Web.Utils.ReportFilterParameter["From_Credit_Value"] != null || VMVServices.Web.Utils.ReportFilterParameter["To_Credit_Value"] != null))
                    { sqlQuery += " OR "; }


                    if (VMVServices.Web.Utils.ReportFilterParameter["From_Credit_Value"] != null && VMVServices.Web.Utils.ReportFilterParameter["To_Credit_Value"] != null)
                    {
                        sqlQuery += " (V.accounted_CR >= '" + (VMVServices.Web.Utils.ReportFilterParameter["From_Credit_Value"] == null ? "0" : VMVServices.Web.Utils.ReportFilterParameter["From_Credit_Value"].ToString()) + "') ";
                        sqlQuery += " AND ( V.accounted_CR <= '" + (VMVServices.Web.Utils.ReportFilterParameter["To_Credit_Value"] == null ? "0" : VMVServices.Web.Utils.ReportFilterParameter["To_Credit_Value"].ToString()) + "') ";
                    }
                    else if (VMVServices.Web.Utils.ReportFilterParameter["From_Credit_Value"] != null)
                    {
                        sqlQuery += " (V.accounted_CR >= '" + (VMVServices.Web.Utils.ReportFilterParameter["From_Credit_Value"] == null ? "0" : VMVServices.Web.Utils.ReportFilterParameter["From_Credit_Value"].ToString()) + "') ";

                    }
                    else if (VMVServices.Web.Utils.ReportFilterParameter["To_Credit_Value"] != null)
                    {
                        sqlQuery += "  ( V.accounted_CR <= '" + (VMVServices.Web.Utils.ReportFilterParameter["To_Credit_Value"] == null ? "0" : VMVServices.Web.Utils.ReportFilterParameter["To_Credit_Value"].ToString()) + "') ";
                    }


                    sqlQuery += " )";
                }
                /*   if (VMVServices.Web.Utils.ReportFilterParameter["From_Value"] != null)
                   {
                       sqlQuery += " AND abs(V.accounted_dr) >= '" + VMVServices.Web.Utils.ReportFilterParameter["From_Value"].ToString() + "'";
                   }
                   if (VMVServices.Web.Utils.ReportFilterParameter["To_Value"] != null)
                   {
                       sqlQuery += " AND  abs(V.accounted_dr) <= '" + VMVServices.Web.Utils.ReportFilterParameter["To_Value"].ToString() + "'";
                   }

                   if (VMVServices.Web.Utils.ReportFilterParameter["From_Credit_Value"] != null)
                   {
                       sqlQuery += " AND abs(V.accounted_CR) >= '" + VMVServices.Web.Utils.ReportFilterParameter["From_Credit_Value"].ToString() + "'";
                   }
                   if (VMVServices.Web.Utils.ReportFilterParameter["To_Credit_Value"] != null)
                   {
                       sqlQuery += " AND  abs(V.accounted_CR) <= '" + VMVServices.Web.Utils.ReportFilterParameter["To_Credit_Value"].ToString() + "'";
                   }
                  */
                //if (VMVServices.Web.Utils.ReportFilterParameter["WITHZERO"] == "FALSE")
                //{
                //    sqlQuery += " AND ABS(V.accounted_dr-accounted_cr) >0    ";
                //}
                
                ////Modified as RBList. Made Necessary changes below.
                //if (post != "1")
                //{
                //    sqlQuery += " and v.workflow_completion_status='1'";
                //}

                if (VMVServices.Web.Utils.ReportViewFilterParameter["UNPOST"] != null)
                {
                    sqlQuery += " AND V.WORKFLOW_COMPLETION_STATUS IN ( " + VMVServices.Web.Utils.ReportViewFilterParameter["UNPOST"].ToString() + " ) ";
                }
                
                if (VMVServices.Web.Utils.ReportFilterParameter["USER_CODE"] != null)
                {
                    sqlQuery += " AND V.created_by = '" + VMVServices.Web.Utils.ReportFilterParameter["USER_CODE"].ToString() + "'";
                }

                if (VMVServices.Web.Utils.ReportFilterParameter["FromAccount"] != null)
                {
                    sqlQuery += " AND V.ACCT_CODE >=  '" + VMVServices.Web.Utils.ReportFilterParameter["FromAccount"].ToString() + "'";
                }

                if (VMVServices.Web.Utils.ReportFilterParameter["ToAccount"] != null)
                {
                    sqlQuery += " AND V.ACCT_CODE <=  '" + VMVServices.Web.Utils.ReportFilterParameter["ToAccount"].ToString() + "'";
                }


                if (VMVServices.Web.Utils.ReportFilterParameter["FromJournal"] != null)
                {
                    sqlQuery += " AND V.je_number >=  '" + VMVServices.Web.Utils.ReportFilterParameter["FromJournal"].ToString() + "'";
                }

                if (VMVServices.Web.Utils.ReportFilterParameter["ToJournal"] != null)
                {
                    sqlQuery += " AND V.je_number <=  '" + VMVServices.Web.Utils.ReportFilterParameter["ToJournal"].ToString() + "'";
                }
            }

            return sqlQuery;
        }
        public static DataSet GetSP_TrailBalanceGroup(string global_segment_id, DateTime tb_date, string str_Query)
        {
            try
            {
                string strValue = string.Empty;
                string strNextCode = string.Empty;
                string strErrMsg = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "GL_PKG.GL_Trial_Balance_account_group";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add(new OracleParameter("@p_org_id", OracleDbType.Varchar2, 250)).Value = VMVServices.Web.Utils.OrganizationID;
                oraCmd.Parameters.Add(new OracleParameter("@p_global_segment_id", OracleDbType.Varchar2, 250)).Value = global_segment_id;
                oraCmd.Parameters.Add(new OracleParameter("@p_date", OracleDbType.Date, 250)).Value = tb_date.ToString("dd/MMM/yyyy");



                return DBMethod.ExecuteStoredProcedure(oraCmd, str_Query);



            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string getTrialBalanceQtrly(string qtrValue, string post)
        {
            sqlQuery = string.Empty;
            sqlQuery += " SELECT * FROM gl_je_summary_period V, gl_acct_period_dtl g  WHERE ROWNUM > 0 and v.period_id=g.period_id ";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportFilterParameter["SEGMENT_ID"] != null)
                {
                    sqlQuery += " AND V.GLOBAL_SEGMENT_ID = '" + VMVServices.Web.Utils.ReportFilterParameter["SEGMENT_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["CAL_DTL_ID"] != null)
                {
                    sqlQuery += " AND V.CAL_DTL_ID = '" + VMVServices.Web.Utils.ReportFilterParameter["CAL_DTL_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["GROUP_ID"] != null)
                {
                    sqlQuery += " AND V.GROUP_ID = '" + VMVServices.Web.Utils.ReportFilterParameter["GROUP_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["WITHZERO"] == "FALSE")
                {
                    sqlQuery += " AND ((abs(V.bb_amount_dr - V.bb_amount_cr)>0) or (abs(V.ACCOUNTED_DR-V.ACCOUNTED_DR) >0) or (abs(V.CB_AMOUNT_DR-V.CB_AMOUNT_CR) >0))";
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["FromAccount"] != null)
                {
                    sqlQuery += " AND V.ACCT_CODE >= '" + VMVServices.Web.Utils.ReportFilterParameter["FromAccount"].ToString() + "'";
                }

                if (VMVServices.Web.Utils.ReportFilterParameter["ToAccount"] != null)
                {
                    sqlQuery += " AND V.ACCT_CODE <= '" + VMVServices.Web.Utils.ReportFilterParameter["ToAccount"].ToString() + "'";
                }
                if (qtrValue == "Q1")
                {
                    sqlQuery += " and g.period_number >='1' and g.period_number <='3'";
                }
                if (qtrValue == "Q2")
                {
                    sqlQuery += " and g.period_number >='4' and g.period_number <='6'";
                }
                if (qtrValue == "Q3")
                {
                    sqlQuery += " and g.period_number >='7' and g.period_number <='9'";
                }
                if (qtrValue == "Q4")
                {
                    sqlQuery += " and g.period_number >='10' and g.period_number <='12'";
                }
                if (post != "1")
                {
                    sqlQuery += " and g.workflow_completion_status='1'";
                }
            }

            return sqlQuery;
        }


        public static string getTrialBalanceYearlyDtls(string globalSegmentId, string fromDate, string toDate, string post)
        {
            sqlQuery = string.Empty;
            //sqlQuery += " SELECT * FROM VW_TB_YRLY_DTLS V WHERE ROWNUM > 0 ";

            sqlQuery += " CREATE OR REPLACE VW_TB_VW_TB_YRLY_DTLS AS ";
            sqlQuery += "  SELECT DISTINCT Z.SEGMENT_ID,Z.ACCT_CODE_ID,Z.ACCT_CODE, Z.ACCT_NAME,";
            sqlQuery += "  SUM(Z.CUR_YR_BAL) CUR_YR_BAL,";
            sqlQuery += "  SUM(Z.PREV_YR_BAL) PREV_YR_BAL,";
            sqlQuery += "  SUM(Z.PREV_FIN_BAL) PREV_FIN_BAL";
            sqlQuery += "  FROM (SELECT JH.JE_GLOBAL_SEGMENT_ID AS SEGMENT_ID, AC.ACCT_CODE AS ACCT_CODE, AC.ACCT_CODE_ID AS ACCT_CODE_ID,";
            sqlQuery += "  AC.ACCT_CODE || ' - ' || AC.ACCT_CODE_DESC AS ACCT_NAME,";
            sqlQuery += "  SUM(NVL(JD.JE_AMOUNT_DR, 0)) - SUM(NVL(JD.JE_AMOUNT_CR, 0)) AS CUR_YR_BAL,";
            sqlQuery += "  0 AS PREV_YR_BAL,";
            sqlQuery += "  0 AS PREV_FIN_BAL";
            sqlQuery += "  FROM GL_JOURNAL_HDR JH";
            sqlQuery += "  INNER JOIN GL_JOURNAL_DTL JD ON JH.JE_HDR_ID = JD.JE_HDR_ID";
            if (VMVServices.Web.Utils.ReportFilterParameter["From_Date"] != null)
            {
                sqlQuery += " AND JH.JE_DATE >= TO_DATE('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "', 'dd/MM/yyyy')";
                sqlQuery += " AND JH.JE_DATE <= TO_DATE('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "', 'dd/MM/yyyy')";
            }
            //sqlQuery += "  AND JH.JE_DATE >= TO_DATE('31/12/2014', 'dd/MM/yyyy')";
            //sqlQuery += "  AND JH.JE_DATE <= TO_DATE('31/12/2014', 'dd/MM/yyyy')";

            sqlQuery += "  INNER JOIN GL_ACCT_CODES AC ON JD.JE_ACCT_CODE_ID = AC.ACCT_CODE_ID";
            if (VMVServices.Web.Utils.ReportFilterParameter["SEGMENT_ID"] != null)
            {
                sqlQuery += " WHERE JH.JE_GLOBAL_SEGMENT_ID='" + VMVServices.Web.Utils.ReportFilterParameter["SEGMENT_ID"].ToString() + "'";
            }
            //sqlQuery += "  WHERE JH.JE_GLOBAL_SEGMENT_ID='SEG_V_ID-0000001341'";

            sqlQuery += "  AND JH.JE_COMP_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            if (VMVServices.Web.Utils.ReportFilterParameter["FromAccount"] != null)
            {
                sqlQuery += "AND JD.JE_ACCT_CODE_ID >= NVL('" + VMVServices.Web.Utils.ReportFilterParameter["FromAccount"].ToString() + "',JD.JE_ACCT_CODE_ID)";
            }
            if (VMVServices.Web.Utils.ReportFilterParameter["ToAccount"] != null)
            {
                sqlQuery += " AND JD.JE_ACCT_CODE_ID <= NVL('" + VMVServices.Web.Utils.ReportFilterParameter["ToAccount"].ToString() + "',JD.JE_ACCT_CODE_ID)";
            }
            //sqlQuery += "  AND JD.JE_ACCT_CODE_ID=NVL('ACC-0000000189',JD.JE_ACCT_CODE_ID)";
            sqlQuery += "  GROUP BY AC.ACCT_CODE_ID,AC.ACCT_CODE, AC.ACCT_CODE_DESC,JH.JE_GLOBAL_SEGMENT_ID";
            sqlQuery += "  UNION";
            sqlQuery += "  SELECT JH.JE_GLOBAL_SEGMENT_ID AS SEGMENT_ID, AC.ACCT_CODE AS ACCT_CODE, AC.ACCT_CODE_ID AS ACCT_CODE_ID,";
            sqlQuery += "  AC.ACCT_CODE || ' - ' || AC.ACCT_CODE_DESC AS ACCT_NAME,";
            sqlQuery += "  0 AS CUR_YR_BAL,";
            sqlQuery += "  SUM(NVL(JD.JE_AMOUNT_DR, 0)) - SUM(NVL(JD.JE_AMOUNT_CR, 0)) AS PREV_YR_BAL,";
            sqlQuery += "  0 AS PREV_FIN_BAL";
            sqlQuery += "  FROM GL_JOURNAL_HDR JH";
            sqlQuery += "  INNER JOIN GL_JOURNAL_DTL JD ON JH.JE_HDR_ID = JD.JE_HDR_ID";

            if (VMVServices.Web.Utils.ReportFilterParameter["From_Date"] != null)
            {
                sqlQuery += "AND JH.JE_DATE >= ADD_MONTHS(TO_DATE('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "', 'dd/MM/yyyy'), -12)";
                sqlQuery += "AND JH.JE_DATE <= ADD_MONTHS(TO_DATE('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "', 'dd/MM/yyyy'),-12)";
            }
            //sqlQuery += "  AND JH.JE_DATE >= ADD_MONTHS(TO_DATE('31/12/2014','dd/MM/yyyy'), -12)";
            //sqlQuery += "  AND JH.JE_DATE <= ADD_MONTHS(TO_DATE('31/12/2014','dd/MM/yyyy'),-12)";

            sqlQuery += "  INNER JOIN GL_ACCT_CODES AC ON JD.JE_ACCT_CODE_ID = AC.ACCT_CODE_ID";
            if (VMVServices.Web.Utils.ReportFilterParameter["SEGMENT_ID"] != null)
            {
                sqlQuery += " WHERE JH.JE_GLOBAL_SEGMENT_ID='" + VMVServices.Web.Utils.ReportFilterParameter["SEGMENT_ID"].ToString() + "'";
            }

            //sqlQuery += "  WHERE JH.JE_GLOBAL_SEGMENT_ID='SEG_V_ID-0000001341'";

            sqlQuery += "  AND JH.JE_COMP_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            if (VMVServices.Web.Utils.ReportFilterParameter["FromAccount"] != null)
            {
                sqlQuery += "AND JD.JE_ACCT_CODE_ID >= NVL('" + VMVServices.Web.Utils.ReportFilterParameter["FromAccount"].ToString() + "',JD.JE_ACCT_CODE_ID)";
            }
            if (VMVServices.Web.Utils.ReportFilterParameter["ToAccount"] != null)
            {
                sqlQuery += " AND JD.JE_ACCT_CODE_ID <= NVL('" + VMVServices.Web.Utils.ReportFilterParameter["ToAccount"].ToString() + "',JD.JE_ACCT_CODE_ID)";
            }
            //sqlQuery += "  AND JD.JE_ACCT_CODE_ID=NVL('ACC-0000000189',JD.JE_ACCT_CODE_ID)";
            sqlQuery += "  GROUP BY AC.ACCT_CODE_ID,AC.ACCT_CODE, AC.ACCT_CODE_DESC,JH.JE_GLOBAL_SEGMENT_ID";
            sqlQuery += "  UNION";
            sqlQuery += "  SELECT JH.JE_GLOBAL_SEGMENT_ID AS SEGMENT_ID, AC.ACCT_CODE AS ACCT_CODE, AC.ACCT_CODE_ID AS ACCT_CODE_ID,";
            sqlQuery += "  AC.ACCT_CODE || ' - ' || AC.ACCT_CODE_DESC AS ACCT_NAME,";
            sqlQuery += "  0 AS CUR_YR_BAL,";
            sqlQuery += "  0 AS PREV_YR_BAL,";
            sqlQuery += "  SUM(NVL(JD.JE_AMOUNT_DR, 0)) - SUM(NVL(JD.JE_AMOUNT_CR, 0)) AS PREV_FIN_BAL";
            sqlQuery += "  FROM GL_JOURNAL_HDR JH";
            sqlQuery += "  INNER JOIN GL_JOURNAL_DTL JD ON JH.JE_HDR_ID = JD.JE_HDR_ID";
            if (VMVServices.Web.Utils.ReportFilterParameter["From_Date"] != null)
            {
                sqlQuery += "  AND JH.JE_DATE >= (SELECT CD.CAL_EFF_START_DT";
                sqlQuery += "  FROM gl_acct_calendar_dtl CD WHERE CD.CAL_EFF_START_DT <= ADD_MONTHS(TO_DATE('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy'), -12)";
                sqlQuery += "   AND CD.CALL_EFF_END_DT >= ADD_MONTHS(TO_DATE('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy'),-12))";
                sqlQuery += "  AND JH.JE_DATE <= (SELECT CD.CALL_EFF_END_DT";
                sqlQuery += "  FROM gl_acct_calendar_dtl CD WHERE CD.CAL_EFF_START_DT <= ADD_MONTHS(TO_DATE('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy'), -12)";
                sqlQuery += "  AND CD.CALL_EFF_END_DT >= ADD_MONTHS(TO_DATE('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy'),-12))";
            }
            sqlQuery += "  INNER JOIN GL_ACCT_CODES AC ON JD.JE_ACCT_CODE_ID = AC.ACCT_CODE_ID";
            if (VMVServices.Web.Utils.ReportFilterParameter["SEGMENT_ID"] != null)
            {
                sqlQuery += " WHERE JH.JE_GLOBAL_SEGMENT_ID='" + VMVServices.Web.Utils.ReportFilterParameter["SEGMENT_ID"].ToString() + "'";
            }
            //sqlQuery += "  WHERE JH.JE_GLOBAL_SEGMENT_ID='SEG_V_ID-0000001341'";
            sqlQuery += "  AND JH.JE_COMP_ID='" + VMVServices.Web.Utils.OrganizationID + "'";

            if (VMVServices.Web.Utils.ReportFilterParameter["FromAccount"] != null)
            {
                sqlQuery += "AND JD.JE_ACCT_CODE_ID >= NVL('" + VMVServices.Web.Utils.ReportFilterParameter["FromAccount"].ToString() + "',JD.JE_ACCT_CODE_ID)";
            }
            if (VMVServices.Web.Utils.ReportFilterParameter["ToAccount"] != null)
            {
                sqlQuery += " AND JD.JE_ACCT_CODE_ID <= NVL('" + VMVServices.Web.Utils.ReportFilterParameter["ToAccount"].ToString() + "',JD.JE_ACCT_CODE_ID)";
            }

            //sqlQuery += "  AND JD.JE_ACCT_CODE_ID=NVL('ACC-0000000189',JD.JE_ACCT_CODE_ID)";

            sqlQuery += "  GROUP BY AC.ACCT_CODE_ID, AC.ACCT_CODE, AC.ACCT_CODE_DESC, JH.JE_GLOBAL_SEGMENT_ID) Z";

            if (VMVServices.Web.Utils.ReportFilterParameter["WITHZERO"] == "FALSE")
            {
                sqlQuery += " WHERE ((abs(Z.cur_yr_bal)>0) or (abs(Z.prev_yr_bal) >0) or (abs(Z.prev_fin_bal) >0))";
            }

            sqlQuery += "  GROUP BY Z.SEGMENT_ID,Z.ACCT_CODE_ID,Z.ACCT_CODE,Z.ACCT_NAME";

            //if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            //{
            //    if (VMVServices.Web.Utils.ReportFilterParameter["SEGMENT_ID"] != null)
            //    {
            //        sqlQuery += " AND V.SEGMENT_ID = '" + VMVServices.Web.Utils.ReportFilterParameter["SEGMENT_ID"].ToString() + "'";
            //    }
            //    if (VMVServices.Web.Utils.ReportFilterParameter["WITHZERO"] == "FALSE")
            //    {
            //        sqlQuery += " AND ((abs(V.cur_yr_bal)>0) or (abs(V.prev_yr_bal) >0) or (abs(V.prev_fin_bal) >0))";
            //    }
            //    if (VMVServices.Web.Utils.ReportFilterParameter["FromAccount"] != null)
            //    {
            //        sqlQuery += " AND V.ACCT_CODE_ID >= '" + VMVServices.Web.Utils.ReportFilterParameter["FromAccount"].ToString() + "'";
            //    }

            //    if (VMVServices.Web.Utils.ReportFilterParameter["ToAccount"] != null)
            //    {
            //        sqlQuery += " AND V.ACCT_CODE_ID <= '" + VMVServices.Web.Utils.ReportFilterParameter["ToAccount"].ToString() + "'";
            //    }

            //    if (post != "1")
            //    {
            //        sqlQuery += " and V.workflow_completion_status='1'";
            //    }
            //}

            return sqlQuery;
        }


    }
}
