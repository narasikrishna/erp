﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.GL
{
    public class Budget_DAL
    {
        static string sqlQuery = "";

        public static string getBudgetType()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT SLVD.CODE AS JOURNAL_ID, SLVD.CODE AS JOURNAL_NAME ";
            sqlQuery += " FROM SSM_CODE_MASTERS SLVD ";
            sqlQuery += " WHERE SLVD.PARENT_CODE = 'BT' ";
            sqlQuery += " ORDER BY SLVD.CODE ";

            return sqlQuery;
        }

        public static string getActualAndProjectedBudgetDetails(string str_BudgetID, string accCodeId = "", string IsAccCode = "")
        {
            sqlQuery = string.Empty;

            sqlQuery = "  SELECT g.acct_grp_id as GROUP_ID,g.ACCT_GRP_DESC" + VMVServices.Web.Utils.LanguageCode + " as GROUP_NAME,to_char(gld.ACTUAL_PREVIOUS_AMOUNT) as ACTUAL_PREVIOUS_AMOUNT ,gld.PERCENTAGE,";
            sqlQuery += "  aC.ACCT_CODE_ID, GLD.ACCT_CODE,gld.ACCT_CODE_DESC,gld.BUDGET_AMOUNT,gld.PREV_YR_BUDGET_AMT,gld.PREV_REV_BUDGET_AMT,gld.PREV_YR_ACT_BUDGET_AMT,gld.PERCENTAGE_VAL,gld.IS_AMT_OR_PREV_YR,";
            sqlQuery += "  nvl(GLD.REVISED_BUDGET_AMOUNT,0) as REVISED_BUDGET_AMOUNT,nvl(GLD.FINAL_BUDGET_AMOUNT,0) as FINAL_BUDGET_AMOUNT,GLD.ENTIRE_OR_ADDITIONAL, ";
            sqlQuery += "  ac.acct_code_id as CODE_ID,ac.ACCT_CODE ||' - '|| ac.ACCT_CODE_DESC" + VMVServices.Web.Utils.LanguageCode + " as code_name,";
            sqlQuery += "  round((GLD.BUDGET_MONTH_AMT_01+GLD.BUDGET_MONTH_AMT_02+GLD.BUDGET_MONTH_AMT_03+GLD.BUDGET_MONTH_AMT_04+ ";
            sqlQuery += "  GLD.BUDGET_MONTH_AMT_05+GLD.BUDGET_MONTH_AMT_06+GLD.BUDGET_MONTH_AMT_07+GLD.BUDGET_MONTH_AMT_08+GLD.BUDGET_MONTH_AMT_09+GLD.BUDGET_MONTH_AMT_10+GLD.BUDGET_MONTH_AMT_11+GLD.BUDGET_MONTH_AMT_12))AS ANL_BUD_AMT,";
            sqlQuery += "  GLD.BUDGET_DTL_ID,GLD.ACTUAL_PREVIOUS_AMOUNT,GLD.PERCENTAGE, GLD.BUDGET_CODE, GLD.BUDGET_ACCT_GRP_ID as GROUP_ID, GLD.BUDGET_ACCT_CODE_ID as CODE_ID, to_char(GLD.BUDGET_MONTH_AMT_01) as BUDGET_MONTH_AMT_01,  to_char(GLD.BUDGET_MONTH_AMT_02) as BUDGET_MONTH_AMT_02, to_char(GLD.BUDGET_MONTH_AMT_03) as BUDGET_MONTH_AMT_03 , to_char(GLD.BUDGET_MONTH_AMT_04) as BUDGET_MONTH_AMT_04, ";
            sqlQuery += "  to_char(GLD.BUDGET_MONTH_AMT_05) as BUDGET_MONTH_AMT_05, to_char(GLD.BUDGET_MONTH_AMT_06) as BUDGET_MONTH_AMT_06, to_char(GLD.BUDGET_MONTH_AMT_07) as BUDGET_MONTH_AMT_07, to_char(GLD.BUDGET_MONTH_AMT_08) as BUDGET_MONTH_AMT_08, to_char(GLD.BUDGET_MONTH_AMT_09) as BUDGET_MONTH_AMT_09 ,to_char(GLD.BUDGET_MONTH_AMT_10) as BUDGET_MONTH_AMT_10 , to_char(GLD.BUDGET_MONTH_AMT_11) as BUDGET_MONTH_AMT_11 , to_char(GLD.BUDGET_MONTH_AMT_12) as BUDGET_MONTH_AMT_12 , ";
            sqlQuery += "  GLD.BUDGET_SEGMENT_ID_1, GLD.BUDGET_SEGMENT_ID_2, GLD.BUDGET_SEGMENT_ID_3, GLD.BUDGET_SEGMENT_ID_4, GLD.BUDGET_SEGMENT_ID_5, GLD.BUDGET_SEGMENT_ID_6, 'N' AS DELETED, ";

            sqlQuery += " TO_CHAR((SELECT ABS(SUM(JS.ACCOUNTED_DR - JS.ACCOUNTED_CR)) FROM GL_JE_SUMMARY_PERIOD JS WHERE JS.CAL_DTL_ID= BH.CAL_DTL_ID AND JS.ACCT_CODE_ID =  AC.ACCT_CODE_ID AND JS.PERIOD_NUMBER=1)) AS ACTUAL_BUDGET_AMT_01, ";
            sqlQuery += " TO_CHAR((SELECT ABS(SUM(JS.ACCOUNTED_DR - JS.ACCOUNTED_CR)) FROM GL_JE_SUMMARY_PERIOD JS WHERE JS.CAL_DTL_ID= BH.CAL_DTL_ID AND JS.ACCT_CODE_ID =  AC.ACCT_CODE_ID AND JS.PERIOD_NUMBER=2)) AS ACTUAL_BUDGET_AMT_02, ";
            sqlQuery += " TO_CHAR((SELECT ABS(SUM(JS.ACCOUNTED_DR - JS.ACCOUNTED_CR)) FROM GL_JE_SUMMARY_PERIOD JS WHERE JS.CAL_DTL_ID= BH.CAL_DTL_ID AND JS.ACCT_CODE_ID =  AC.ACCT_CODE_ID AND JS.PERIOD_NUMBER=3)) AS ACTUAL_BUDGET_AMT_03, ";
            sqlQuery += " TO_CHAR((SELECT ABS(SUM(JS.ACCOUNTED_DR - JS.ACCOUNTED_CR)) FROM GL_JE_SUMMARY_PERIOD JS WHERE JS.CAL_DTL_ID= BH.CAL_DTL_ID AND JS.ACCT_CODE_ID =  AC.ACCT_CODE_ID AND JS.PERIOD_NUMBER=4)) AS ACTUAL_BUDGET_AMT_04, ";
            sqlQuery += " TO_CHAR((SELECT ABS(SUM(JS.ACCOUNTED_DR - JS.ACCOUNTED_CR)) FROM GL_JE_SUMMARY_PERIOD JS WHERE JS.CAL_DTL_ID= BH.CAL_DTL_ID AND JS.ACCT_CODE_ID =  AC.ACCT_CODE_ID AND JS.PERIOD_NUMBER=5)) AS ACTUAL_BUDGET_AMT_05, ";
            sqlQuery += " TO_CHAR((SELECT ABS(SUM(JS.ACCOUNTED_DR - JS.ACCOUNTED_CR)) FROM GL_JE_SUMMARY_PERIOD JS WHERE JS.CAL_DTL_ID= BH.CAL_DTL_ID AND JS.ACCT_CODE_ID =  AC.ACCT_CODE_ID AND JS.PERIOD_NUMBER=6)) AS ACTUAL_BUDGET_AMT_06, ";
            sqlQuery += " TO_CHAR((SELECT ABS(SUM(JS.ACCOUNTED_DR - JS.ACCOUNTED_CR)) FROM GL_JE_SUMMARY_PERIOD JS WHERE JS.CAL_DTL_ID= BH.CAL_DTL_ID AND JS.ACCT_CODE_ID =  AC.ACCT_CODE_ID AND JS.PERIOD_NUMBER=7)) AS ACTUAL_BUDGET_AMT_07, ";
            sqlQuery += " TO_CHAR((SELECT ABS(SUM(JS.ACCOUNTED_DR - JS.ACCOUNTED_CR)) FROM GL_JE_SUMMARY_PERIOD JS WHERE JS.CAL_DTL_ID= BH.CAL_DTL_ID AND JS.ACCT_CODE_ID =  AC.ACCT_CODE_ID AND JS.PERIOD_NUMBER=8)) AS ACTUAL_BUDGET_AMT_08, ";
            sqlQuery += " TO_CHAR((SELECT ABS(SUM(JS.ACCOUNTED_DR - JS.ACCOUNTED_CR)) FROM GL_JE_SUMMARY_PERIOD JS WHERE JS.CAL_DTL_ID= BH.CAL_DTL_ID AND JS.ACCT_CODE_ID =  AC.ACCT_CODE_ID AND JS.PERIOD_NUMBER=9)) AS ACTUAL_BUDGET_AMT_09, ";
            sqlQuery += " TO_CHAR((SELECT ABS(SUM(JS.ACCOUNTED_DR - JS.ACCOUNTED_CR)) FROM GL_JE_SUMMARY_PERIOD JS WHERE JS.CAL_DTL_ID= BH.CAL_DTL_ID AND JS.ACCT_CODE_ID =  AC.ACCT_CODE_ID AND JS.PERIOD_NUMBER=10)) AS ACTUAL_BUDGET_AMT_10, ";
            sqlQuery += " TO_CHAR((SELECT ABS(SUM(JS.ACCOUNTED_DR - JS.ACCOUNTED_CR)) FROM GL_JE_SUMMARY_PERIOD JS WHERE JS.CAL_DTL_ID= BH.CAL_DTL_ID AND JS.ACCT_CODE_ID =  AC.ACCT_CODE_ID AND JS.PERIOD_NUMBER=11)) AS ACTUAL_BUDGET_AMT_11, ";
            sqlQuery += " TO_CHAR((SELECT ABS(SUM(JS.ACCOUNTED_DR - JS.ACCOUNTED_CR)) FROM GL_JE_SUMMARY_PERIOD JS WHERE JS.CAL_DTL_ID= BH.CAL_DTL_ID AND JS.ACCT_CODE_ID =  AC.ACCT_CODE_ID AND JS.PERIOD_NUMBER=12)) AS ACTUAL_BUDGET_AMT_12 ";


            sqlQuery += "  FROM GL_BUDGET_DTL GLD, GL_ACCT_GROUPS g, GL_ACCT_CODES ac,GL_BUDGET_HDR BH";
            sqlQuery += " WHERE GLD.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += "  and ac.acct_code_id = gld.budget_acct_code_id";
            sqlQuery += " and gld.budget_acct_code_id = ac.acct_code_id";
            sqlQuery += " and g.acct_grp_id = gld.budget_acct_grp_id";
            sqlQuery += " AND BH.BUDGET_CODE = GLD.BUDGET_CODE ";
            if (IsAccCode == "1" && accCodeId.Length > 0)
            {
                sqlQuery += " and ac.acct_code_id ='" + accCodeId + "'";
            }
            if (IsAccCode == "0" && accCodeId.Length > 0)
            {
                sqlQuery += " and g.acct_grp_id ='" + accCodeId + "'";
            }
            sqlQuery += " AND GLD.ENABLED_FLAG = 1";
            sqlQuery += "  AND GLD.BUDGET_CODE = '" + str_BudgetID + "'";

            sqlQuery += " AND g.acct_org_id ='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " ORDER BY GLD.BUDGET_DTL_ID";
            return sqlQuery;


        }

        public static string getBudgetDetails(string str_BudgetID, string accCodeId = "", string IsAccCode = "")
        {
            sqlQuery = string.Empty;

            sqlQuery = "  SELECT g.acct_grp_id as GROUP_ID,g.ACCT_GRP_DESC" + VMVServices.Web.Utils.LanguageCode + " as GROUP_NAME,to_char(gld.ACTUAL_PREVIOUS_AMOUNT) as ACTUAL_PREVIOUS_AMOUNT ,gld.PERCENTAGE,";
            sqlQuery += "  aC.ACCT_CODE_ID, GLD.ACCT_CODE,gld.ACCT_CODE_DESC,gld.BUDGET_AMOUNT,gld.PREV_YR_BUDGET_AMT,gld.PREV_REV_BUDGET_AMT,gld.PREV_YR_ACT_BUDGET_AMT,gld.PERCENTAGE_VAL,gld.IS_AMT_OR_PREV_YR,";
            sqlQuery += "  nvl(GLD.REVISED_BUDGET_AMOUNT,0) as REVISED_BUDGET_AMOUNT,nvl(GLD.FINAL_BUDGET_AMOUNT,0) as FINAL_BUDGET_AMOUNT,GLD.ENTIRE_OR_ADDITIONAL, ";
            sqlQuery += "  ac.acct_code_id as CODE_ID,ac.ACCT_CODE ||' - '|| ac.ACCT_CODE_DESC" + VMVServices.Web.Utils.LanguageCode + " as code_name,";
            sqlQuery += "  round((GLD.BUDGET_MONTH_AMT_01+GLD.BUDGET_MONTH_AMT_02+GLD.BUDGET_MONTH_AMT_03+GLD.BUDGET_MONTH_AMT_04+ ";
            sqlQuery += "  GLD.BUDGET_MONTH_AMT_05+GLD.BUDGET_MONTH_AMT_06+GLD.BUDGET_MONTH_AMT_07+GLD.BUDGET_MONTH_AMT_08+GLD.BUDGET_MONTH_AMT_09+GLD.BUDGET_MONTH_AMT_10+GLD.BUDGET_MONTH_AMT_11+GLD.BUDGET_MONTH_AMT_12))AS ANL_BUD_AMT,";
            sqlQuery += "  GLD.BUDGET_DTL_ID,GLD.ACTUAL_PREVIOUS_AMOUNT,GLD.PERCENTAGE, GLD.BUDGET_CODE, GLD.BUDGET_ACCT_GRP_ID as GROUP_ID, GLD.BUDGET_ACCT_CODE_ID as CODE_ID, to_char(GLD.BUDGET_MONTH_AMT_01) as BUDGET_MONTH_AMT_01,  to_char(GLD.BUDGET_MONTH_AMT_02) as BUDGET_MONTH_AMT_02, to_char(GLD.BUDGET_MONTH_AMT_03) as BUDGET_MONTH_AMT_03 , to_char(GLD.BUDGET_MONTH_AMT_04) as BUDGET_MONTH_AMT_04, ";
            sqlQuery += "  to_char(GLD.BUDGET_MONTH_AMT_05) as BUDGET_MONTH_AMT_05, to_char(GLD.BUDGET_MONTH_AMT_06) as BUDGET_MONTH_AMT_06, to_char(GLD.BUDGET_MONTH_AMT_07) as BUDGET_MONTH_AMT_07, to_char(GLD.BUDGET_MONTH_AMT_08) as BUDGET_MONTH_AMT_08, to_char(GLD.BUDGET_MONTH_AMT_09) as BUDGET_MONTH_AMT_09 ,to_char(GLD.BUDGET_MONTH_AMT_10) as BUDGET_MONTH_AMT_10 , to_char(GLD.BUDGET_MONTH_AMT_11) as BUDGET_MONTH_AMT_11 , to_char(GLD.BUDGET_MONTH_AMT_12) as BUDGET_MONTH_AMT_12 , ";
            sqlQuery += "  GLD.BUDGET_SEGMENT_ID_1, GLD.BUDGET_SEGMENT_ID_2, GLD.BUDGET_SEGMENT_ID_3, GLD.BUDGET_SEGMENT_ID_4, GLD.BUDGET_SEGMENT_ID_5, GLD.BUDGET_SEGMENT_ID_6, 'N' AS DELETED ";
            sqlQuery += "  FROM GL_BUDGET_DTL GLD, GL_ACCT_GROUPS g, GL_ACCT_CODES ac";
            sqlQuery += " WHERE GLD.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += "  and ac.acct_code_id = gld.budget_acct_code_id";
            sqlQuery += " and gld.budget_acct_code_id = ac.acct_code_id";
            sqlQuery += " and g.acct_grp_id = gld.budget_acct_grp_id";
            if (IsAccCode == "1" && accCodeId.Length > 0)
            {
                sqlQuery += " and ac.acct_code_id ='" + accCodeId + "'";
            }
            if (IsAccCode == "0" && accCodeId.Length > 0)
            {
                sqlQuery += " and g.acct_grp_id ='" + accCodeId + "'";
            }
            sqlQuery += " AND GLD.ENABLED_FLAG = 1";
            sqlQuery += "  AND GLD.BUDGET_CODE = '" + str_BudgetID + "'";

            sqlQuery += " AND g.acct_org_id ='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " ORDER BY GLD.BUDGET_DTL_ID";
            return sqlQuery;


        }
        public static string getGroupDetails()
        {
            sqlQuery = string.Empty;

            sqlQuery = "       SELECT g.acct_grp_id as GROUP_ID,";
            sqlQuery += "       g.ACCT_GRP_DESC" + VMVServices.Web.Utils.LanguageCode + " as GROUP_NAME,";
            sqlQuery += "       '' as CODE_ID,'' AS PERCENTAGE,";

            sqlQuery += "  (select (bd.budget_month_amt_01+bd.budget_month_amt_02+bd.budget_month_amt_03+";
            sqlQuery += " bd.budget_month_amt_04+bd.budget_month_amt_05+bd.budget_month_amt_06+bd.budget_month_amt_07+bd.budget_month_amt_08+";
            sqlQuery += " bd.budget_month_amt_09+bd.budget_month_amt_10+bd.budget_month_amt_11+bd.budget_month_amt_12) from GL_BUDGET_DTL bd,Gl_Budget_Hdr bh";
            sqlQuery += " where bd.budget_acct_grp_id = g.acct_grp_id";
            sqlQuery += " and bh.budget_code = bd.budget_code";
            sqlQuery += " and bh.budget_period_start_dt = ";
            sqlQuery += " (select max(sbh.budget_period_start_dt) from Gl_Budget_Hdr sbh,GL_BUDGET_DTL sbd where sbd.budget_code= sbh.budget_code and sbd.budget_acct_grp_id=g.acct_grp_id)";
            sqlQuery += " and rownum =1  )as ACTUAL_PREVIOUS_AMOUNT,";

            sqlQuery += "       '' as code_name,";
            sqlQuery += "       null AS ANL_BUD_AMT,";
            sqlQuery += "       null AS BUDGET_DTL_ID,";
            sqlQuery += "      null AS BUDGET_CODE,";
            sqlQuery += "        null as GROUP_ID,";
            sqlQuery += "        null AS CODE_ID,";
            sqlQuery += "        null AS BUDGET_MONTH_AMT_01,";
            sqlQuery += "        null AS BUDGET_MONTH_AMT_02,";
            sqlQuery += "        null AS BUDGET_MONTH_AMT_03,";
            sqlQuery += "         null AS BUDGET_MONTH_AMT_04,";
            sqlQuery += "         null AS BUDGET_MONTH_AMT_05,";
            sqlQuery += "        null AS BUDGET_MONTH_AMT_06,";
            sqlQuery += "        null AS BUDGET_MONTH_AMT_07,";
            sqlQuery += "        null AS BUDGET_MONTH_AMT_08,";
            sqlQuery += "       null AS BUDGET_MONTH_AMT_09,";
            sqlQuery += "       null AS BUDGET_MONTH_AMT_10,";
            sqlQuery += "       null AS BUDGET_MONTH_AMT_11,";
            sqlQuery += "        null AS BUDGET_MONTH_AMT_12,";
            sqlQuery += "        null AS BUDGET_SEGMENT_ID_1,";
            sqlQuery += "       null AS BUDGET_SEGMENT_ID_2,";
            sqlQuery += "       null AS BUDGET_SEGMENT_ID_3,";
            sqlQuery += "       null AS BUDGET_SEGMENT_ID_4,";
            sqlQuery += "      null AS BUDGET_SEGMENT_ID_5,";
            sqlQuery += "      null AS BUDGET_SEGMENT_ID_6,";
            sqlQuery += "      'N' AS DELETED";
            sqlQuery += "   FROM GL_ACCT_GROUPS g";
            sqlQuery += "  where g.acct_grp_id not in(select lh.acct_grp_id from GL_ACCT_GROUP_LINK_HDR lh)";
            sqlQuery += "  AND g.acct_org_id='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += "   ORDER BY g.ACCT_GRP_DESC";
            return sqlQuery;
        }
        public static string getGroupAcCodeDetailsBasedOrg(string orgId, string groupId)
        {
            sqlQuery = string.Empty;


            sqlQuery = "    SELECT g.acct_grp_id as GROUP_ID,";
            sqlQuery += "   g.ACCT_GRP_DESC" + VMVServices.Web.Utils.LanguageCode + " as GROUP_NAME,";
            sqlQuery += "   ac.acct_code_id as CODE_ID,";
            sqlQuery += "  ac.ACCT_CODE || ' - ' || ac.ACCT_CODE_DESC" + VMVServices.Web.Utils.LanguageCode + " as code_name,";
            sqlQuery += "  null AS ANL_BUD_AMT,null as ACTUAL_PREVIOUS_AMOUNT,null as PERCENTAGE,";
            sqlQuery += "  null AS BUDGET_DTL_ID,";
            sqlQuery += "    null AS BUDGET_CODE,";
            sqlQuery += "    null as GROUP_ID,";
            sqlQuery += "   null AS CODE_ID,";
            sqlQuery += "   null AS BUDGET_MONTH_AMT_01,";
            sqlQuery += "   null AS BUDGET_MONTH_AMT_02,";
            sqlQuery += "  null AS BUDGET_MONTH_AMT_03,";
            sqlQuery += "  null AS BUDGET_MONTH_AMT_04,";
            sqlQuery += "  null AS BUDGET_MONTH_AMT_05,";
            sqlQuery += "  null AS BUDGET_MONTH_AMT_06,";
            sqlQuery += "   null AS BUDGET_MONTH_AMT_07,";
            sqlQuery += "   null AS BUDGET_MONTH_AMT_08,";
            sqlQuery += "   null AS BUDGET_MONTH_AMT_09,";
            sqlQuery += "  null AS BUDGET_MONTH_AMT_10,";
            sqlQuery += "   null AS BUDGET_MONTH_AMT_11,";
            sqlQuery += "   null AS BUDGET_MONTH_AMT_12,";
            sqlQuery += "   null AS BUDGET_SEGMENT_ID_1,";
            sqlQuery += "   null AS BUDGET_SEGMENT_ID_2,";
            sqlQuery += "  null AS BUDGET_SEGMENT_ID_3,";
            sqlQuery += "  null AS BUDGET_SEGMENT_ID_4,";
            sqlQuery += "  null AS BUDGET_SEGMENT_ID_5,";
            sqlQuery += "  null AS BUDGET_SEGMENT_ID_6,";
            sqlQuery += " 'N' AS DELETED";
            sqlQuery += "  FROM GL_ACCT_GROUPS g, GL_ACCT_CODES ac,gl_acct_structure ga,gl_companies_dtl c";
            sqlQuery += "  WHERE g.acct_grp_id = ac.acct_grp_id";
            sqlQuery += "  and ac.acct_struct_id = ga.acct_struct_id";
            sqlQuery += "  and ga.acct_struct_id=c.comp_acct_struct_id";
            sqlQuery += "  and c.comp_id='" + orgId + "'";
            sqlQuery += "  and g.acct_grp_id = '" + groupId + "'";
            sqlQuery += "  AND g.acct_org_id='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += "  ORDER BY g.ACCT_GRP_DESC";

            return sqlQuery;
        }
        public static string getGroupAcCodeDetails()
        {
            sqlQuery = string.Empty;


            sqlQuery = "    SELECT g.acct_grp_id as GROUP_ID,";
            sqlQuery += "   g.ACCT_GRP_DESC" + VMVServices.Web.Utils.LanguageCode + " as GROUP_NAME,";
            sqlQuery += "   ac.acct_code_id as CODE_ID,";
            sqlQuery += "  ac.ACCT_CODE || ' - ' || ac.ACCT_CODE_DESC" + VMVServices.Web.Utils.LanguageCode + " as code_name,";
            sqlQuery += "  null AS ANL_BUD_AMT,'' AS PERCENTAGE,";

            sqlQuery += " (select (bd.budget_month_amt_01+bd.budget_month_amt_02+bd.budget_month_amt_03+";
            sqlQuery += " bd.budget_month_amt_04+bd.budget_month_amt_05+bd.budget_month_amt_06+bd.budget_month_amt_07+bd.budget_month_amt_08+";
            sqlQuery += " bd.budget_month_amt_09+bd.budget_month_amt_10+bd.budget_month_amt_11+bd.budget_month_amt_12) from GL_BUDGET_DTL bd,Gl_Budget_Hdr bh";
            sqlQuery += " where bd.budget_acct_grp_id = g.acct_grp_id";
            sqlQuery += " and bh.budget_code = bd.budget_code";
            sqlQuery += " and bh.budget_period_start_dt = ";
            sqlQuery += " (select max(sbh.budget_period_start_dt) from Gl_Budget_Hdr sbh,GL_BUDGET_DTL sbd where sbd.budget_code= sbh.budget_code and sbd.budget_acct_grp_id=g.acct_grp_id)";
            sqlQuery += " and rownum =1  )as ACTUAL_PREVIOUS_AMOUNT,";

            sqlQuery += "  null AS BUDGET_DTL_ID,";
            sqlQuery += "    null AS BUDGET_CODE,";
            sqlQuery += "    null as GROUP_ID,";
            sqlQuery += "   null AS CODE_ID,";
            sqlQuery += "   null AS BUDGET_MONTH_AMT_01,";
            sqlQuery += "   null AS BUDGET_MONTH_AMT_02,";
            sqlQuery += "  null AS BUDGET_MONTH_AMT_03,";
            sqlQuery += "  null AS BUDGET_MONTH_AMT_04,";
            sqlQuery += "  null AS BUDGET_MONTH_AMT_05,";
            sqlQuery += "  null AS BUDGET_MONTH_AMT_06,";
            sqlQuery += "   null AS BUDGET_MONTH_AMT_07,";
            sqlQuery += "   null AS BUDGET_MONTH_AMT_08,";
            sqlQuery += "   null AS BUDGET_MONTH_AMT_09,";
            sqlQuery += "  null AS BUDGET_MONTH_AMT_10,";
            sqlQuery += "   null AS BUDGET_MONTH_AMT_11,";
            sqlQuery += "   null AS BUDGET_MONTH_AMT_12,";
            sqlQuery += "   null AS BUDGET_SEGMENT_ID_1,";
            sqlQuery += "   null AS BUDGET_SEGMENT_ID_2,";
            sqlQuery += "  null AS BUDGET_SEGMENT_ID_3,";
            sqlQuery += "  null AS BUDGET_SEGMENT_ID_4,";
            sqlQuery += "  null AS BUDGET_SEGMENT_ID_5,";
            sqlQuery += "  null AS BUDGET_SEGMENT_ID_6,";
            sqlQuery += " 'N' AS DELETED";
            sqlQuery += " FROM GL_ACCT_GROUPS g, GL_ACCT_CODES ac";
            sqlQuery += " WHERE g.acct_grp_id = ac.acct_grp_id";
            sqlQuery += " AND g.acct_org_id='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " ORDER BY g.ACCT_GRP_DESC";

            return sqlQuery;
        }
        public static string getGroupAcCodeBASEDGroup(string grpId)
        {
            sqlQuery = string.Empty;


            sqlQuery = "    SELECT g.acct_grp_id as GROUP_ID,";
            sqlQuery += "   g.ACCT_GRP_DESC" + VMVServices.Web.Utils.LanguageCode + " as GROUP_NAME,";
            sqlQuery += "   ac.acct_code_id as CODE_ID,";
            sqlQuery += "   ac.ACCT_CODE || ' - ' || ac.ACCT_CODE_DESC" + VMVServices.Web.Utils.LanguageCode + " as code_name,";
            sqlQuery += "   null AS ANL_BUD_AMT,";
            sqlQuery += "   null AS BUDGET_DTL_ID,";
            sqlQuery += "    null AS BUDGET_CODE,";
            sqlQuery += "    null as GROUP_ID,";
            sqlQuery += "   null AS CODE_ID,";
            sqlQuery += "   null AS BUDGET_MONTH_AMT_01,";
            sqlQuery += "   null AS BUDGET_MONTH_AMT_02,";
            sqlQuery += "   null AS BUDGET_MONTH_AMT_03,";
            sqlQuery += "    null AS BUDGET_MONTH_AMT_04,";
            sqlQuery += "   null AS BUDGET_MONTH_AMT_05,";
            sqlQuery += "   null AS BUDGET_MONTH_AMT_06,";
            sqlQuery += "   null AS BUDGET_MONTH_AMT_07,";
            sqlQuery += "   null AS BUDGET_MONTH_AMT_08,";
            sqlQuery += "   null AS BUDGET_MONTH_AMT_09,";
            sqlQuery += "   null AS BUDGET_MONTH_AMT_10,";
            sqlQuery += "   null AS BUDGET_MONTH_AMT_11,";
            sqlQuery += "   null AS BUDGET_MONTH_AMT_12,";
            sqlQuery += "   null AS BUDGET_SEGMENT_ID_1,";
            sqlQuery += "   null AS BUDGET_SEGMENT_ID_2,";
            sqlQuery += "  null AS BUDGET_SEGMENT_ID_3,";
            sqlQuery += "  null AS BUDGET_SEGMENT_ID_4,";
            sqlQuery += "  null AS BUDGET_SEGMENT_ID_5,";
            sqlQuery += "  null AS BUDGET_SEGMENT_ID_6,";
            sqlQuery += "  'N' AS DELETED";
            sqlQuery += "  FROM GL_ACCT_GROUPS g, GL_ACCT_CODES ac";
            sqlQuery += "  WHERE g.acct_grp_id = ac.acct_grp_id";
            sqlQuery += "  and g.acct_grp_id = '" + grpId + "'";
            sqlQuery += "  AND g.acct_org_id='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += "  ORDER BY g.ACCT_GRP_DESC";

            return sqlQuery;
        }

        public static string GetGroupAcCodeAmt(string grpId, string prevYrId = "", string IsAccCode = "", string recordId = "")
        {
            sqlQuery = string.Empty;

            sqlQuery = "     select test.GROUP_ID,";
            sqlQuery += "      test.GROUP_NAME,";
            sqlQuery += "     test.CODE_ID,";
            sqlQuery += "     test.ACCT_CODE,";
            sqlQuery += "     test.ACCT_CODE_DESC,test.BUDGET_AMOUNT,";
            sqlQuery += "      sum(ACTUAL_PREVIOUS_AMOUNT) as ACTUAL_PREVIOUS_AMOUNT";
            sqlQuery += "   from (SELECT g.acct_grp_id as GROUP_ID,bd.BUDGET_AMOUNT,";
            sqlQuery += "   g.ACCT_GRP_DESC" + VMVServices.Web.Utils.LanguageCode + " as GROUP_NAME,";
            sqlQuery += "   ac.acct_code_id as CODE_ID,";
            sqlQuery += "   ac.ACCT_CODE,";
            sqlQuery += "   ac.ACCT_CODE_DESC" + VMVServices.Web.Utils.LanguageCode + ",";
            sqlQuery += "   (bd.budget_month_amt_01 + bd.budget_month_amt_02 +";
            sqlQuery += "   bd.budget_month_amt_03 + bd.budget_month_amt_04 +";
            sqlQuery += "   bd.budget_month_amt_05 + bd.budget_month_amt_06 +";
            sqlQuery += "    bd.budget_month_amt_07 + bd.budget_month_amt_08 +";
            sqlQuery += "    bd.budget_month_amt_09 + bd.budget_month_amt_10 +";
            sqlQuery += "    bd.budget_month_amt_11 + bd.budget_month_amt_12) as ACTUAL_PREVIOUS_AMOUNT      ";
            sqlQuery += "    FROM GL_ACCT_GROUPS g,";
            sqlQuery += "     GL_ACCT_CODES  ac,";
            sqlQuery += "     GL_BUDGET_DTL  bd,";
            sqlQuery += "     Gl_Budget_Hdr  bh";
            sqlQuery += "    WHERE g.acct_grp_id = ac.acct_grp_id";
            sqlQuery += "    and ac.acct_code_id=bd.acct_code ";
            sqlQuery += "   AND g.acct_org_id='" + VMVServices.Web.Utils.OrganizationID + "'";
            // sqlQuery += "      and bd.budget_acct_grp_id = g.acct_grp_id";
            sqlQuery += "      and bh.budget_code = bd.budget_code";
            sqlQuery += "    and (g.acct_grp_id = bd.attribute1 or ";
            sqlQuery += "       g.acct_grp_id = bd.attribute2 or ";
            sqlQuery += "       g.acct_grp_id = bd.attribute3 or ";
            sqlQuery += "      g.acct_grp_id = bd.attribute4 or ";
            sqlQuery += "      g.acct_grp_id = bd.attribute5 or ";
            sqlQuery += "      g.acct_grp_id = bd.attribute6 or ";
            sqlQuery += "     g.acct_grp_id = bd.attribute7 or ";
            sqlQuery += "     g.acct_grp_id = bd.attribute8 or ";
            sqlQuery += "     g.acct_grp_id = bd.attribute9 or ";
            sqlQuery += "     g.acct_grp_id = bd.attribute10) ";
            sqlQuery += "      and bh.CAL_DTL_ID = '" + prevYrId + "'";

            if (recordId.Length > 0)
            {
                sqlQuery += " and bh.BUDGET_CODE='" + recordId + "'";
            }
            if (IsAccCode == "1")
            {
                sqlQuery += "   and ac.acct_code_id= '" + grpId + "' ";
            }
            else
            {
                sqlQuery += "       and (bd.attribute1 = '" + grpId + "'  or  ";
                sqlQuery += "        bd.attribute2 = '" + grpId + "'  or  ";
                sqlQuery += "        bd.attribute3 = '" + grpId + "'  or  ";
                sqlQuery += "        bd.attribute4 = '" + grpId + "'  or  ";
                sqlQuery += "        bd.attribute5 = '" + grpId + "'  or  ";
                sqlQuery += "       bd.attribute6 = '" + grpId + "'  or  ";
                sqlQuery += "       bd.attribute7 = '" + grpId + "'  or  ";
                sqlQuery += "       bd.attribute8 = '" + grpId + "'  or  ";
                sqlQuery += "       bd.attribute9 = '" + grpId + "'  or  ";
                sqlQuery += "       bd.attribute10 = '" + grpId + "')";
            }

            //sqlQuery += "     and ac.acct_grp_id in ";
            //sqlQuery += "      (select distinct acc_id.*";
            //sqlQuery += "   from temp_gl_acc_id_name t4, TABLE(t4.ACC_VALUE) acc_id";
            //sqlQuery += "   where t4.level5_id in";
            //sqlQuery += "   (select distinct t3.level5_id";
            //sqlQuery += "   from temp_gl_acc_id_name t3";
            //sqlQuery += "   where t3.level5_id is not null";
            //sqlQuery += "   and t3.level4_id in";
            //sqlQuery += "   (select distinct t2.level4_id";
            //sqlQuery += "   from temp_gl_acc_id_name t2";
            //sqlQuery += "   where t2.level4_id is not null";
            //sqlQuery += "   and t2.level3_id in";
            //sqlQuery += "   (select distinct t1.level3_id";
            //sqlQuery += "   from temp_gl_acc_id_name t1";
            //sqlQuery += "   where t1.level3_id is not null";
            //sqlQuery += "   and t1.level2_id in";
            //sqlQuery += "   (select distinct t.level2_id";
            //sqlQuery += "   from temp_gl_acc_id_name t";
            //sqlQuery += "   where t.level2_id is not null";
            //sqlQuery += "   and (t.level1_id ='" + grpId + "'";
            //sqlQuery += "   ))))))";
            sqlQuery += "     ORDER BY g.ACCT_GRP_DESC) test";
            sqlQuery += "    group by test.GROUP_ID,";
            sqlQuery += "       test.GROUP_NAME,";
            sqlQuery += "      test.CODE_ID,";
            sqlQuery += "      test.ACCT_CODE,test.BUDGET_AMOUNT,";
            sqlQuery += "    test.ACCT_CODE_DESC";

            return sqlQuery;
        }
        public static string GetGroupAcCodeJournalAmt(string grpId = "", string prevYrId = "", string IsCodeORGroup = "")
        {
            sqlQuery = string.Empty;


            sqlQuery = "   select test.GROUP_ID,";
            sqlQuery += "     test.GROUP_NAME,";
            sqlQuery += "      test.CODE_ID,";
            sqlQuery += "     test.ACCT_CODE,";
            sqlQuery += "      test.ACCT_CODE_DESC,";
            sqlQuery += "      sum(ACTUAL_PREVIOUS_AMOUNT) as ACTUAL_PREVIOUS_AMOUNT";
            sqlQuery += "     from (SELECT bh.JE_PERIOD_ID,";
            sqlQuery += "       g.acct_grp_id as GROUP_ID,";
            sqlQuery += "       g.ACCT_GRP_DESC" + VMVServices.Web.Utils.LanguageCode + " as GROUP_NAME,";
            sqlQuery += "       ac.acct_code_id as CODE_ID,";
            sqlQuery += "       ac.ACCT_CODE,";
            sqlQuery += "       ac.ACCT_CODE_DESC" + VMVServices.Web.Utils.LanguageCode + ",";
            sqlQuery += "       bh.je_tot_cr_amount,";
            sqlQuery += "      (bh.je_tot_dr_amount) as ACTUAL_PREVIOUS_AMOUNT";
            sqlQuery += "     FROM GL_ACCT_GROUPS g,";
            sqlQuery += "      GL_ACCT_CODES  ac,";
            sqlQuery += "       gl_journal_dtl bd,";
            sqlQuery += "      gl_journal_hdr bh";
            sqlQuery += "    WHERE g.acct_grp_id = ac.acct_grp_id";
            sqlQuery += "   AND g.acct_org_id='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += "     and bd.je_acct_code_id = ac.acct_code_id";
            sqlQuery += "     and bh.je_hdr_id = bd.je_hdr_id";
            if (IsCodeORGroup == "1")
            {
                sqlQuery += "     and bd.je_acct_code_id='" + grpId + "'";
            }
            if (IsCodeORGroup == "0")
            {
                sqlQuery += "     and ac.acct_grp_id='" + grpId + "'";
            }
            sqlQuery += "     and bh.JE_PERIOD_ID in";
            sqlQuery += "      (select p.period_id";
            sqlQuery += "      from gl_acct_period_dtl p";
            if (prevYrId != string.Empty)
            {
                sqlQuery += "     where p.cal_dtl_id = '" + prevYrId + "'";
            }
            sqlQuery += "      )";
            sqlQuery += "    ORDER BY g.ACCT_GRP_DESC) test";
            sqlQuery += "     group by test.GROUP_ID,";
            sqlQuery += "           test.GROUP_NAME,";
            sqlQuery += "           test.CODE_ID,";
            sqlQuery += "        test.ACCT_CODE,";
            sqlQuery += "        test.ACCT_CODE_DESC";



            //sqlQuery = " select test.GROUP_ID,";
            //sqlQuery += " test.GROUP_NAME,";
            //sqlQuery += " test.CODE_ID,";
            //sqlQuery += " test.ACCT_CODE,";
            //sqlQuery += " test.ACCT_CODE_DESC,";
            //sqlQuery += " sum(ACTUAL_PREVIOUS_AMOUNT) as ACTUAL_PREVIOUS_AMOUNT";
            //sqlQuery += " from (SELECT g.acct_grp_id as GROUP_ID,";
            //sqlQuery += " g.ACCT_GRP_DESC as GROUP_NAME,";
            //sqlQuery += " ac.acct_code_id as CODE_ID,";
            //sqlQuery += " ac.ACCT_CODE,";
            //sqlQuery += " ac.ACCT_CODE_DESC,";
            //sqlQuery += " bh.je_tot_cr_amount,";
            //sqlQuery += " (bh.je_tot_dr_amount) as ACTUAL_PREVIOUS_AMOUNT";
            //sqlQuery += " FROM GL_ACCT_GROUPS g,";
            //sqlQuery += " GL_ACCT_CODES  ac,";
            //sqlQuery += "  gl_journal_dtl bd,";
            //sqlQuery += "       gl_journal_hdr bh";
            //sqlQuery += "  WHERE g.acct_grp_id = ac.acct_grp_id";
            //sqlQuery += "  and bd.je_acct_code_id = ac.acct_code_id";
            //sqlQuery += "  and bh.je_hdr_id = bd.je_hdr_id";
            ////sqlQuery += "      and bh.CAL_DTL_ID = '" + prevYrId + "'";
            //sqlQuery += "     and ac.acct_grp_id ='" + grpId + "'";
            //sqlQuery += " ORDER BY g.ACCT_GRP_DESC) test";
            //sqlQuery += " group by test.GROUP_ID,";
            //sqlQuery += " test.GROUP_NAME,";
            //sqlQuery += " test.CODE_ID,";
            //sqlQuery += " test.ACCT_CODE,";
            //sqlQuery += "   test.ACCT_CODE_DESC";
            return sqlQuery;
        }
        public static string getGlobalSegment()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT C.GLOBAL_SEGMENT_BALANCED";
            sqlQuery += " FROM GL_COMPANIES_HDR C";
            sqlQuery += " WHERE C.ENABLED_FLAG = 1";
            sqlQuery += " AND C.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND C.COMP_ID = '" + VMVServices.Web.Utils.OrganizationID + "'";
            return sqlQuery;
        }
        public static string GetBudgetVarianceDetails(string budYrId, string grpId = "")
        {
            sqlQuery = string.Empty;

            //sqlQuery = "     select test.budget_code,";
            //sqlQuery += "    test.budget_name,";
            //sqlQuery += "    test.budget_period_start_dt,";
            //sqlQuery += "    test.budget_period_end_dt,";
            sqlQuery = "  select  test.BUDGET_DTL_ID,";
            sqlQuery += " test.acct_code as Account_Code";
            sqlQuery += " ,test.acct_code_desc,";
            sqlQuery += "  to_char(ROUND(trunc(to_char(abs(test.budget_amount), '9999999999.999'))," + VMVServices.Web.Utils.DecimalPrecision + "),'9999999999.999') as Budget_Amount,";
            // sqlQuery += "    test.BUDGET_MONTH_AMT_01, test.BUDGET_MONTH_AMT_02, test.BUDGET_MONTH_AMT_03, test.BUDGET_MONTH_AMT_04,test.BUDGET_MONTH_AMT_05, test.BUDGET_MONTH_AMT_06, test.BUDGET_MONTH_AMT_07, test.BUDGET_MONTH_AMT_08, test.BUDGET_MONTH_AMT_09, test.BUDGET_MONTH_AMT_10, test.BUDGET_MONTH_AMT_11, test.BUDGET_MONTH_AMT_12, ";
            sqlQuery += "   to_char(ROUND(test.ACTUAL_PREVIOUS_AMOUNT," + VMVServices.Web.Utils.DecimalPrecision + ")) as Previous_Amount,";
            sqlQuery += "  to_char(ROUND((to_char((nvl(abs(to_number(test.budget_amount)),0) - nvl(test.ACTUAL_PREVIOUS_AMOUNT, 0))))," + VMVServices.Web.Utils.DecimalPrecision + "),'9999999999.999')  as Difference";
            sqlQuery += "   from (";
            sqlQuery += "   select d.BUDGET_DTL_ID,";
            sqlQuery += "    c.acct_code_desc" + VMVServices.Web.Utils.LanguageCode + " as acct_code_desc,";
            sqlQuery += "    c.acct_code,";
            sqlQuery += "     d.budget_amount,";
            //  sqlQuery += "    d.BUDGET_MONTH_AMT_01, d.BUDGET_MONTH_AMT_02, d.BUDGET_MONTH_AMT_03, d.BUDGET_MONTH_AMT_04,d.BUDGET_MONTH_AMT_05, d.BUDGET_MONTH_AMT_06, d.BUDGET_MONTH_AMT_07, d.BUDGET_MONTH_AMT_08, d.BUDGET_MONTH_AMT_09, d.BUDGET_MONTH_AMT_10, d.BUDGET_MONTH_AMT_11, d.BUDGET_MONTH_AMT_12, ";
            sqlQuery += "    (SELECT sum(bh.je_tot_dr_amount) as ACTUAL_PREVIOUS_AMOUNT";
            sqlQuery += "   FROM GL_ACCT_GROUPS g,";
            sqlQuery += "     GL_ACCT_CODES  ac,";
            sqlQuery += "    gl_journal_dtl bd,";
            sqlQuery += "   gl_journal_hdr bh";
            sqlQuery += "   WHERE g.acct_grp_id = ac.acct_grp_id";
            sqlQuery += "   AND g.acct_org_id='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += "    and bd.je_acct_code_id = ac.acct_code_id";
            sqlQuery += "    and bh.je_hdr_id = bd.je_hdr_id";
            sqlQuery += "    and bd.je_acct_code_id = c.acct_code_id";
            sqlQuery += "    and rownum = 1";
            sqlQuery += "    and bh.JE_PERIOD_ID in";
            sqlQuery += "   (select p.period_id";
            sqlQuery += "     from gl_acct_period_dtl p ";
            sqlQuery += "    where p.cal_dtl_id = b.CAL_DTL_ID";
            sqlQuery += "     )) ACTUAL_PREVIOUS_AMOUNT";
            sqlQuery += "   from gl_budget_hdr b, gl_budget_dtl d, gl_acct_codes c";
            sqlQuery += "   where b.budget_code = d.budget_code";
            sqlQuery += "    and d.budget_acct_code_id = c.acct_code_id";
            sqlQuery += "   and b.workflow_completion_status=1 and b.enabled_flag=1";

            sqlQuery += "   and b.CAL_DTL_ID='" + budYrId + "'";

            if (grpId != string.Empty)
            {
                sqlQuery += "       and (d.attribute1 = '" + grpId + "'  or  ";
                sqlQuery += "        d.attribute2 = '" + grpId + "'  or  ";
                sqlQuery += "        d.attribute3 = '" + grpId + "'  or  ";
                sqlQuery += "        d.attribute4 = '" + grpId + "'  or  ";
                sqlQuery += "        d.attribute5 = '" + grpId + "'  or  ";
                sqlQuery += "       d.attribute6 = '" + grpId + "'  or  ";
                sqlQuery += "       d.attribute7 = '" + grpId + "'  or  ";
                sqlQuery += "       d.attribute8 = '" + grpId + "'  or  ";
                sqlQuery += "       d.attribute9 = '" + grpId + "'  or  ";
                sqlQuery += "       d.attribute10 = '" + grpId + "')";
            }
            sqlQuery += "    ) test";
            sqlQuery += " ORDER BY acct_code ";

            return sqlQuery;
        }
        public static string GetBudgetCalYear()
        {
            sqlQuery = string.Empty;

            sqlQuery = " select distinct cd.cal_dtl_id,cd.cal_acct_year";
            sqlQuery += " from GL_ACCT_CALENDAR_DTL cd,gl_budget_hdr b";
            sqlQuery += " where cd.workflow_completion_status = 1";
            sqlQuery += " and cd.enabled_flag = 1 and cd.cal_dtl_id=b.cal_dtl_id and b.workflow_completion_status=1  order by cd.cal_acct_year asc";
            return sqlQuery;
        }

        public static string getBudgetGroupDetails(string budYrId = "")
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT distinct g.ACCT_GRP_ID AS GROUP_ID, g.ACCT_GRP_DESC" + VMVServices.Web.Utils.LanguageCode + " AS GROUP_NAME ";
            sqlQuery += "  FROM GL_ACCT_GROUPS g,gl_budget_hdr b,gl_budget_dtl bd ";
            sqlQuery += "  WHERE g.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += "  AND g.ENABLED_FLAG = 1 and b.budget_code = bd.budget_code";
            sqlQuery += "   AND g.acct_org_id='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += "    and (g.acct_grp_id = bd.attribute1 or ";
            sqlQuery += "       g.acct_grp_id = bd.attribute2 or ";
            sqlQuery += "       g.acct_grp_id = bd.attribute3 or ";
            sqlQuery += "      g.acct_grp_id = bd.attribute4 or ";
            sqlQuery += "      g.acct_grp_id = bd.attribute5 or ";
            sqlQuery += "      g.acct_grp_id = bd.attribute6 or ";
            sqlQuery += "     g.acct_grp_id = bd.attribute7 or ";
            sqlQuery += "     g.acct_grp_id = bd.attribute8 or ";
            sqlQuery += "     g.acct_grp_id = bd.attribute9 or ";
            sqlQuery += "     g.acct_grp_id = bd.attribute10) ";
            if (budYrId != string.Empty)
            {
                sqlQuery += "   and b.CAL_DTL_ID='" + budYrId + "'";
            }
            sqlQuery += " ORDER BY g.ACCT_GRP_DESC" + VMVServices.Web.Utils.LanguageCode + " asc ";

            return sqlQuery;
        }
        public static string getMonthlyPeriodAmt(string budDtlId = "")
        {
            sqlQuery = string.Empty;

            sqlQuery = "   select d.BUDGET_MONTH_AMT_01,";
            sqlQuery += "    d.BUDGET_MONTH_AMT_02,";
            sqlQuery += "      d.BUDGET_MONTH_AMT_03,";
            sqlQuery += "      d.BUDGET_MONTH_AMT_04,";
            sqlQuery += "    d.BUDGET_MONTH_AMT_05,";
            sqlQuery += "       d.BUDGET_MONTH_AMT_06,";
            sqlQuery += "      d.BUDGET_MONTH_AMT_07,";
            sqlQuery += "      d.BUDGET_MONTH_AMT_08,";
            sqlQuery += "      d.BUDGET_MONTH_AMT_09,";
            sqlQuery += "      d.BUDGET_MONTH_AMT_10,";
            sqlQuery += "      d.BUDGET_MONTH_AMT_11,";
            sqlQuery += "      d.BUDGET_MONTH_AMT_12";
            sqlQuery += "      from gl_budget_dtl d, gl_budget_hdr h";
            sqlQuery += "     where d.budget_code = h.budget_code";
            sqlQuery += "      and d.budget_dtl_id='" + budDtlId + "'";
            return sqlQuery;
        }
    }
}