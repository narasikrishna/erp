﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.GL
{
    public class AccountingGroupDefaultSegment_DAL
    {
        static string sqlQuery = "";


        public static string getGroupName(string mode)
        {
            sqlQuery = string.Empty;
            sqlQuery = "  SELECT AG.ACCT_GRP_ID,AG.ACCT_GRP_DESC" + VMVServices.Web.Utils.LanguageCode + " as GROUP_NAME ";
            sqlQuery += " FROM GL_ACCT_GROUPS AG ";
            sqlQuery += " WHERE AG.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND AG.ENABLED_FLAG = 1";
            ////Need to display all the groups as per NSN's feedback. Commenting the below code (09/07/2015)
            //if (FINTableConstant.Add == mode)
            //{
            //    sqlQuery += " AND (AG.EFFECTIVE_END_DT IS NULL OR";
            //    sqlQuery += "  (SYSDATE BETWEEN AG.EFFECTIVE_START_DT AND AG.EFFECTIVE_END_DT))";
            //    sqlQuery += " AND NOT exists  (select DS.ACCT_GRP_ID from GL_ACCT_GROUP_DEFAULT_SEGMENTS DS where ds.acct_grp_id = ag.acct_grp_id )";
            //    //sqlQuery += " AND AG.ACCT_GRP_ID NOT IN (select DS.ACCT_GRP_ID from GL_ACCT_GROUP_DEFAULT_SEGMENTS DS)";
            //}
            sqlQuery += " AND AG.acct_org_id='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " ORDER BY AG.ACCT_GRP_DESC ";

            return sqlQuery;
        }

        public static string getSegmentName()
        {
            sqlQuery = string.Empty;
            sqlQuery = "  SELECT S.SEGMENT_ID,S.SEGMENT_NAME" + VMVServices.Web.Utils.LanguageCode + " as SEGMENT_NAME ";
            sqlQuery += " FROM GL_SEGMENTS S";
            sqlQuery += " WHERE S.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND S.ENABLED_FLAG = 1";
            sqlQuery += " AND (s.EFFECTIVE_END_DT IS NULL OR";
            sqlQuery += "  (SYSDATE BETWEEN s.EFFECTIVE_START_DT AND s.EFFECTIVE_END_DT))";
            sqlQuery += " ORDER BY S.SEGMENT_NAME ";

            return sqlQuery;
        }



        public static string getAcctGrpDefSegDtl(string acct_def_id)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select ds.acct_def_dtl_id,s.segment_id,s.segment_name" + VMVServices.Web.Utils.LanguageCode + " as SEGMENT_NAME,";
            sqlQuery += " case ds.acct_is_mandatory when '1' then 'TRUE' else 'FALSE' END AS acct_is_mandatory,'N' AS DELETED ";
            sqlQuery += " from GL_ACCT_GRP_DEFAULT_SGMNTS_DTL ds,gl_segments s";
            sqlQuery += " where ds.workflow_completion_status = 1";
            sqlQuery += " and ds.acct_segment_id = s.segment_id";
            sqlQuery += " and ds.acct_def_id = '" + acct_def_id + "'";
            return sqlQuery;
        }

        public static string getchild_AcctGrp_frmAcctcode(string ACCT_GRP_ID)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select AC.ACCT_GRP_ID";
            sqlQuery += " from GL_ACCT_CODES AC";
            sqlQuery += " WHERE AC.ACCT_GRP_ID = '" + ACCT_GRP_ID + "'";
            return sqlQuery;
        }


    }
}
