﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.GL
{
   public class Segments
    {
       static string sqlQuery = "";

       public static string GetSegmentvalues(long Master_id)
       {
           sqlQuery = string.Empty;

           sqlQuery = " select sv.pk_id,sv.segment_value_id,sv.segment_value,sv.effective_start_dt,sv.effective_end_dt,'N' AS DELETED, ";
           sqlQuery += " case sv.enabled_flag when '1' then 'TRUE' else 'FALSE' end as enabled_flag";
           sqlQuery += " from GL_SEGMENT_VALUES sv";
           sqlQuery += " WHERE sv.PK_ID = " + Master_id;
           sqlQuery += " AND sv.WORKFLOW_COMPLETION_STATUS = 1";           
           return sqlQuery;

       }

    }
}
