﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;
using System.Data.EntityClient;
using System.Data.Entity.Infrastructure;

namespace FIN.DAL.GL
{
    public class AccountingCalendar_DAL
    {
        static string sqlQuery = "";


        public static string Getcalenderdatebyperiod(string periodid)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select h.pk_id,gp.PERIOD_ID,gp.PERIOD_NAME,gp.period_from_dt,gp.period_to_dt from  gl_acct_calendar_hdr h,gl_companies_dtl cd,GL_COMP_ACCT_PERIOD_DTL gp, gl_acct_calendar_dtl al ";
            sqlQuery += "  where h.enabled_flag='1'";
            sqlQuery += "  and h.workflow_completion_status='1' and h.CAL_ID=cd.COMP_CAL_ID and al.cal_id = h.cal_id and al.cal_dtl_id = gp.cal_dtl_id and gp.PERIOD_STATUS <>'NOP' ";
            sqlQuery += "  and cd.COMP_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += "  and gp.period_type='ACTPERIOD' ";
            sqlQuery += " and gp.PERIOD_ID = '" + periodid + "'";
            //sqlQuery += "  AND (gp.PERIOD_FROM_DT IS NULL OR";
            //sqlQuery += "  (SYSDATE BETWEEN gp.PERIOD_FROM_DT AND gp.PERIOD_TO_DT))";

            sqlQuery += "  order by gp.PERIOD_NAME asc";

            return sqlQuery;
        }

        public static string GetPeriodFromToDate(string periodId)
        {
            sqlQuery = string.Empty;
            sqlQuery += " select t.period_from_dt,t.period_to_dt from gl_acct_period_dtl t ";
            sqlQuery += " where t.period_type='ACTPERIOD' ";
            sqlQuery += " and t.period_id = '" + periodId + "'";
            return sqlQuery;
        }

        public static string getAcctcalendardtl(string Master_id)
        {
            sqlQuery = string.Empty;


            sqlQuery = " SELECT cd.pk_id,cd.cal_dtl_id,cd.cal_acct_year,cd.cal_eff_start_dt,cd.call_eff_end_dt,'N' AS DELETED";
            sqlQuery += " FROM GL_ACCT_CALENDAR_DTL cd";
            sqlQuery += " where cd.CAL_ID = '" + Master_id + "'";
            sqlQuery += " and cd.workflow_completion_status = 1";
            sqlQuery += " and cd.enabled_flag =1";
            return sqlQuery;
        }

        public static string getPerioddtl(string CAL_DTL_ID)
        {
            sqlQuery = string.Empty;


            sqlQuery = " SELECT pd.pk_id,pd.period_id,pd.period_name,pd.period_type,pd.period_from_dt,pd.period_to_dt,C.CODE AS LOOKUP_ID,C.CODE AS LOOKUP_NAME,'N' AS DELETED";
            sqlQuery += " FROM GL_ACCT_PERIOD_DTL pd,ssm_code_masters C";
            sqlQuery += " where pd.workflow_completion_status = 1";
            sqlQuery += " AND PD.PERIOD_STATUS =  C.CODE";
            sqlQuery += " and pd.enabled_flag =1";
            sqlQuery += " and pd.CAL_DTL_ID = '" + CAL_DTL_ID + "'";
            sqlQuery += " order by pd.period_from_dt";
            return sqlQuery;
        }

        public static string getPerioddtl4ActualPeriod(string CAL_DTL_ID)
        {
            sqlQuery = string.Empty;


            sqlQuery = " SELECT pd.pk_id,pd.period_id,pd.period_name,pd.period_type,pd.period_from_dt,pd.period_to_dt,C.CODE AS LOOKUP_ID,C.CODE AS LOOKUP_NAME,'N' AS DELETED";
            sqlQuery += " FROM GL_ACCT_PERIOD_DTL pd,ssm_code_masters C";
            sqlQuery += " where pd.workflow_completion_status = 1";
            sqlQuery += " AND PD.PERIOD_STATUS =  C.CODE";
            sqlQuery += " and pd.enabled_flag =1";
            sqlQuery += " and pd.CAL_DTL_ID = '" + CAL_DTL_ID + "'";
            sqlQuery += " and pd.period_type='ACTPERIOD' ";
            sqlQuery += " order by pd.period_from_dt";
            return sqlQuery;
        }

        public static string getfromLookup(string typeName)
        {
            sqlQuery = string.Empty;


            sqlQuery = "Select V.VALUE_KEY_ID,V.VALUE_NAME,m.TYPE_KEY_ID  From ssm_lookup_value_dtls V,ssm_lookup_type_master M";
            sqlQuery += " WHERE M.TYPE_NAME = '" + typeName + "'";
            sqlQuery += " AND V.TYPE_KEY_ID =  M.TYPE_KEY_ID";
            sqlQuery += " ORDER BY V.DISPLAY_ORDER";
            return sqlQuery;
        }
        public static string GetFinancialYear(string calId = "")
        {
            sqlQuery = string.Empty;

            sqlQuery = "select h.pk_id,h.cal_id,h.cal_desc" + VMVServices.Web.Utils.LanguageCode + " as cal_desc,h.cal_eff_start_dt,h.cal_eff_end_dt from  gl_acct_calendar_hdr h ";
            sqlQuery += " where h.enabled_flag='1'";
            sqlQuery += " and h.workflow_completion_status='1'";
            if (calId != string.Empty)
            {
                sqlQuery += " and h.cal_id='" + calId + "'";
            }
            //sqlQuery += " AND (h.cal_eff_start_dt IS NULL OR";
            //sqlQuery += " (SYSDATE BETWEEN h.cal_eff_start_dt AND h.cal_eff_end_dt))";
            sqlQuery += " order by h.cal_desc asc";
            return sqlQuery;
        }

        public static string GetCalAcctYear(string calId = "", string mode = "")
        {
            sqlQuery = string.Empty;

            sqlQuery = "select h.pk_id,cd.CAL_DTL_ID,CD.CAL_ACCT_YEAR from  gl_acct_calendar_hdr h,GL_ACCT_CALENDAR_DTL CD ";
            sqlQuery += " where H.CAL_ID= CD.CAL_ID";
            if (calId != string.Empty)
            {
                sqlQuery += " and h.cal_id='" + calId + "'";
            }
            if (mode != string.Empty && mode == FINTableConstant.Add)
            {
                sqlQuery += " AND cd.WORKFLOW_COMPLETION_STATUS='1' ";
                sqlQuery += " AND cd.ENABLED_FLAG='1'";

            }
            sqlQuery += " AND cd.CAL_DTL_ID IN (SELECT CAL_DTL_ID FROM GL_COMP_ACCT_CALENDAR_DTL WHERE COMP_ID='" + VMVServices.Web.Utils.OrganizationID + "')";
            sqlQuery += " order by cd.CAL_ACCT_YEAR asc";
            return sqlQuery;
        }
        public static string GetPreviousCalAcctYear(string calDtlId = "")
        {
            sqlQuery = string.Empty;

            sqlQuery = " select h.pk_id,cd.CAL_DTL_ID,CD.CAL_ACCT_YEAR from  gl_acct_calendar_hdr h,GL_ACCT_CALENDAR_DTL CD ";
            sqlQuery += " where H.CAL_ID= CD.CAL_ID";
            sqlQuery += " AND cd.WORKFLOW_COMPLETION_STATUS='1' ";
            sqlQuery += " AND cd.ENABLED_FLAG='1'";
            sqlQuery += " AND cd.CAL_DTL_ID IN (SELECT CAL_DTL_ID FROM GL_COMP_ACCT_CALENDAR_DTL WHERE COMP_ID='" + VMVServices.Web.Utils.OrganizationID + "')";

            sqlQuery += "    and cd.call_eff_end_dt < ";
            sqlQuery += "  (select ccd.cal_eff_start_dt ";
            sqlQuery += "     from GL_ACCT_CALENDAR_DTL ccd, GL_ACCT_CALENDAR_DTL CD ";
            sqlQuery += "   where ccd.CAL_DTL_ID = cd.CAL_DTL_ID ";
            sqlQuery += "   and ccd.cal_dtl_id = '" + calDtlId + "') ";

            sqlQuery += " order by cd.CAL_ACCT_YEAR asc";

            return sqlQuery;
        }

        public static string GetAccPeriodBasedOrg()
        {
            sqlQuery = string.Empty;

            sqlQuery = " select h.pk_id,gp.PERIOD_ID,gp.PERIOD_NAME,h.cal_eff_start_dt,h.cal_eff_end_dt from  gl_acct_calendar_hdr h,gl_companies_dtl cd,GL_COMP_ACCT_PERIOD_DTL gp, gl_acct_calendar_dtl al ";
            sqlQuery += "  where h.enabled_flag='1'";
            sqlQuery += "  and h.workflow_completion_status='1' and h.CAL_ID=cd.COMP_CAL_ID and al.cal_id = h.cal_id and al.cal_dtl_id = gp.cal_dtl_id and gp.PERIOD_STATUS='OPEN' ";
            sqlQuery += "  and cd.COMP_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            //sqlQuery += "  AND (gp.PERIOD_FROM_DT IS NULL OR";
            //sqlQuery += "  (SYSDATE BETWEEN gp.PERIOD_FROM_DT AND gp.PERIOD_TO_DT))";

            sqlQuery += "  order by gp.PERIOD_FROM_DT asc";

            return sqlQuery;
        }

        public static string GetAccPeriodBasedOrg4NotNOPPeriod()
        {
            sqlQuery = string.Empty;

            sqlQuery = " select h.pk_id,gp.PERIOD_ID,gp.PERIOD_NAME,h.cal_eff_start_dt,h.cal_eff_end_dt from  gl_acct_calendar_hdr h,gl_companies_dtl cd,GL_COMP_ACCT_PERIOD_DTL gp, gl_acct_calendar_dtl al ";
            sqlQuery += "  where h.enabled_flag='1'";
            sqlQuery += "  and h.workflow_completion_status='1' and h.CAL_ID=cd.COMP_CAL_ID and al.cal_id = h.cal_id and al.cal_dtl_id = gp.cal_dtl_id and gp.PERIOD_STATUS <>'NOP' ";
            sqlQuery += "  and cd.COMP_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += "  and gp.period_type='ACTPERIOD' ";
            
            //sqlQuery += "  AND (gp.PERIOD_FROM_DT IS NULL OR";
            //sqlQuery += "  (SYSDATE BETWEEN gp.PERIOD_FROM_DT AND gp.PERIOD_TO_DT))";

            sqlQuery += "  order by gp.period_from_dt desc";

            return sqlQuery;
        }
        public static string GetAccPeriodBasedOrg4NotNOPPeriodANDYear()
        {
            sqlQuery = string.Empty;

            sqlQuery = " select h.pk_id,gp.PERIOD_ID,gp.PERIOD_NAME,h.cal_eff_start_dt,h.cal_eff_end_dt from  gl_acct_calendar_hdr h,gl_companies_dtl cd,GL_COMP_ACCT_PERIOD_DTL gp, gl_acct_calendar_dtl al ";
            sqlQuery += "  where h.enabled_flag='1'";
            sqlQuery += "  and h.workflow_completion_status='1' and h.CAL_ID=cd.COMP_CAL_ID and al.cal_id = h.cal_id and al.cal_dtl_id = gp.cal_dtl_id and gp.PERIOD_STATUS <>'NOP' ";
            sqlQuery += "  and cd.COMP_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += "  and al.cal_eff_start_dt <= sysdate and al.call_eff_end_dt >= sysdate ";
            sqlQuery += "  and gp.period_type='ACTPERIOD' ";
            //sqlQuery += "  AND (gp.PERIOD_FROM_DT IS NULL OR";
            //sqlQuery += "  (SYSDATE BETWEEN gp.PERIOD_FROM_DT AND gp.PERIOD_TO_DT))";

            sqlQuery += "  order by gp.PERIOD_NAME asc";

            return sqlQuery;
        }
        public static string GetAccPeriodBasedOrgJDate(string journalDate)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select h.pk_id,gp.PERIOD_ID,gp.PERIOD_NAME,h.cal_eff_start_dt,h.cal_eff_end_dt from  gl_acct_calendar_hdr h,gl_companies_dtl cd,GL_ACCT_PERIOD_DTL gp, gl_acct_calendar_dtl al ";
            sqlQuery += "  where h.enabled_flag='1'";
            sqlQuery += "  and h.workflow_completion_status='1' and h.CAL_ID=cd.COMP_CAL_ID and al.cal_id = h.cal_id and al.cal_dtl_id = gp.cal_dtl_id and gp.PERIOD_STATUS='OPEN' ";
            sqlQuery += "  and cd.COMP_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += "  and to_date('" + journalDate + "','dd/MM/yyyy') between gp.period_from_dt and gp.period_to_dt";
            sqlQuery += "  order by gp.PERIOD_NAME asc";

            return sqlQuery;
        }
        public static string GetCompPeriodBasedOrgJDate(string journalDate)
        {
            sqlQuery = string.Empty;


            sqlQuery += "  select gg.pk_id,gg.comp_id,gg.period_id,gg.period_name,gg.period_from_dt as cal_eff_start_dt,gg.period_to_dt as cal_eff_end_dt ";
            sqlQuery += "  from  gl_comp_acct_period_dtl gg";
            sqlQuery += "  where gg.enabled_flag='1'";
            sqlQuery += "  and gg.workflow_completion_status='1' and gg.PERIOD_STATUS='OPEN'";
            sqlQuery += "  and gg.COMP_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += "  and to_date('" + journalDate + "','dd/MM/yyyy') between gg.period_from_dt and gg.period_to_dt";
            sqlQuery += "  order by gg.PERIOD_NAME asc";

            return sqlQuery;

        }
        public static string GetFinancialYearfordate(string cal_id)
        {
            sqlQuery = string.Empty;

            sqlQuery = "select h.pk_id,h.cal_id,h.cal_desc" + VMVServices.Web.Utils.LanguageCode + ",h.cal_eff_start_dt,h.cal_eff_end_dt from  gl_acct_calendar_hdr h ";
            sqlQuery += " where h.enabled_flag='1'";
            sqlQuery += " and h.workflow_completion_status='1'";

            sqlQuery += " and h.cal_id='" + cal_id + "'";

            sqlQuery += " order by h.cal_id asc";
            return sqlQuery;
        }


        public static string GetStartdtEnddtfrcalyear(string CAL_DTL_ID)
        {
            sqlQuery = string.Empty;
            sqlQuery += " SELECT CD.CAL_EFF_START_DT,CD.CALL_EFF_END_DT";
            sqlQuery += " FROM GL_ACCT_CALENDAR_DTL CD";
            sqlQuery += " WHERE CD.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND CD.ENABLED_FLAG = 1";
            sqlQuery += " AND CD.CAL_DTL_ID = '" + CAL_DTL_ID + "'";
            return sqlQuery;
        }

        public static string GetAccountYear(string comp_id, string cal_id)
        {
            sqlQuery = string.Empty;




            sqlQuery += " SELECT CACD.PK_ID,CACD.COMP_ID,CACD.CAL_DTL_ID,CACD.CAL_ID ";
            sqlQuery += " ,ACD.CAL_ACCT_YEAR,'TRUE' AS SELECTED ";
            sqlQuery += " FROM GL_COMP_ACCT_CALENDAR_DTL CACD ";
            sqlQuery += " INNER JOIN GL_ACCT_CALENDAR_DTL ACD ON ACD.CAL_DTL_ID = CACD.CAL_DTL_ID ";
            sqlQuery += " WHERE CACD.COMP_ID='" + comp_id + "' AND CACD.CAL_ID='" + cal_id + "'";
            sqlQuery += " UNION ";
            sqlQuery += " SELECT 0 AS PK_ID,'" + comp_id + "' AS COMP_ID,ACD.CAL_DTL_ID,'" + cal_id + "'";
            sqlQuery += " ,ACD.CAL_ACCT_YEAR,'FALSE' AS SELECTED ";
            sqlQuery += " FROM GL_ACCT_CALENDAR_DTL ACD  ";
            sqlQuery += " WHERE ACD.WORKFLOW_COMPLETION_STATUS =1 ";
            sqlQuery += " AND NOT exists (SELECT  CAL_DTL_id from  GL_COMP_ACCT_CALENDAR_DTL CACD WHERE CACD.COMP_ID='" + comp_id + "' AND CAL_ID='" + cal_id + "'  AND ACD.CAL_DTL_ID = CACD.CAL_DTL_ID)";
            //sqlQuery += " AND ACD.CAL_DTL_ID NOT IN (SELECT  CAL_DTL_id from  GL_COMP_ACCT_CALENDAR_DTL CACD WHERE CACD.COMP_ID='" + comp_id + "' AND CAL_ID='" + cal_id + "')";
            sqlQuery += " AND CAL_ID='" + cal_id + "'";
            sqlQuery += " ORDER BY CAL_ACCT_YEAR ";

            return sqlQuery;
        }

        public static string GetCalYear()
        {
            sqlQuery = string.Empty;

            sqlQuery = " select cd.cal_dtl_id,cd.cal_acct_year";
            sqlQuery += " from GL_ACCT_CALENDAR_DTL cd";
            sqlQuery += " where cd.workflow_completion_status = 1";
            sqlQuery += " and cd.enabled_flag = 1";
            sqlQuery += " AND cd.CAL_DTL_ID in (SELECT CAL_DTL_ID FROM GL_COMP_ACCT_CALENDAR_DTL where COMP_ID='" + VMVServices.Web.Utils.OrganizationID + "')";
            return sqlQuery;
        }
        public static string DeleteCalPeriodDtl(string calDtlId)
        {
            sqlQuery = string.Empty;

            sqlQuery = "DELETE FROM gl_acct_period_dtl WHERE cal_dtl_id = '" + calDtlId + "'";
            return sqlQuery;
        }

        public static string getFinanicalYear4Date(string str_date)
        {
            sqlQuery = string.Empty;
            sqlQuery = "SELECT ACD.CAL_DTL_ID,ACD.CAL_EFF_START_DT FROM GL_COMP_ACCT_CALENDAR_DTL CACD ";
            sqlQuery += " INNER JOIN GL_ACCT_CALENDAR_DTL ACD ON CACD.CAL_DTL_ID = ACD.CAL_DTL_ID ";
            sqlQuery += " WHERE TO_DATE('" + str_date + "','dd/MM/yyyy') BETWEEN ACD.CAL_EFF_START_DT AND ACD.CALL_EFF_END_DT ";
            sqlQuery += " AND CACD.COMP_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            return sqlQuery;
        }
        public static string IsPeriodFound(string calDtlId)
        {
            sqlQuery = string.Empty;
            sqlQuery = "  select count(1) as counts from gl_acct_period_dtl h where h.CAL_DTL_ID='" + calDtlId + "'";
            return sqlQuery;
        }

        public static string GetPeriodId4CurrentDate()
        {
            sqlQuery = string.Empty;
            sqlQuery = "  select * from gl_acct_period_dtl h where sysdate between period_from_dt and period_to_dt";
            return sqlQuery;
        }

        public static string GetAccPeriodBasedNotNOPPeriodSegmentBal(string calDtlId)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select h.pk_id,gp.PERIOD_ID,gp.PERIOD_NAME,h.cal_eff_start_dt,h.cal_eff_end_dt from  gl_acct_calendar_hdr h,gl_companies_dtl cd,GL_COMP_ACCT_PERIOD_DTL gp, gl_acct_calendar_dtl al ";
            sqlQuery += "  where h.enabled_flag='1'";
            sqlQuery += "  and h.workflow_completion_status='1' and h.CAL_ID=cd.COMP_CAL_ID and al.cal_id = h.cal_id and al.cal_dtl_id = gp.cal_dtl_id and gp.PERIOD_STATUS <>'NOP' ";
            sqlQuery += "  and cd.COMP_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += "  and gp.period_type='ACTPERIOD' ";
            sqlQuery += " and gp.cal_dtl_id='" + calDtlId + "'";
            sqlQuery += "  order by gp.period_from_dt desc";

            return sqlQuery;
        }

        public static void GetSPFOR_LeaveYrEnd_Process(string currentFYr, string previousFYr)
        {
            try
            {
                string strValue = string.Empty;
                string strNextCode = string.Empty;
                string strErrMsg = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "PKG_PAYROLL.Leave_Dept_Year_End";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add(new OracleParameter("@p_org_id", OracleDbType.Varchar2, 250)).Value = VMVServices.Web.Utils.OrganizationID;
                oraCmd.Parameters.Add(new OracleParameter("@p_curr_fin_year", OracleDbType.Varchar2, 250)).Value = currentFYr;
                oraCmd.Parameters.Add(new OracleParameter("@p_prev_fin_year", OracleDbType.Varchar2, 250)).Value = previousFYr;
             
                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

                OracleCommand oraCmd1 = new OracleCommand();
                oraCmd1.CommandText = "PKG_PAYROLL.Leave_Staff_YEAR_END";
                oraCmd1.CommandType = CommandType.StoredProcedure;
                oraCmd1.Parameters.Add(new OracleParameter("@p_org_id", OracleDbType.Varchar2, 250)).Value = VMVServices.Web.Utils.OrganizationID;
                oraCmd1.Parameters.Add(new OracleParameter("@p_curr_fin_year", OracleDbType.Varchar2, 250)).Value = currentFYr;
                oraCmd1.Parameters.Add(new OracleParameter("@p_prev_fin_year", OracleDbType.Varchar2, 250)).Value = previousFYr;
              
                oraCmd1 = DBMethod.ExecuteStoredProcedure(oraCmd1);


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
