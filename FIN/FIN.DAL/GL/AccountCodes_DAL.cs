﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.GL
{
    public class AccountCodes_DAL
    {
        static string sqlQuery = "";


        public static string getglAcctDet()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT * FROM vw_gl_acc_detail V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["ACCOUNT_GROUP"] != null)
                {
                    sqlQuery += " AND V.acct_grp_id = '" + VMVServices.Web.Utils.ReportViewFilterParameter["ACCOUNT_GROUP"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["ACCOUNT_TYPE"] != null)
                {
                    sqlQuery += " AND V.Account_Type = '" + VMVServices.Web.Utils.ReportViewFilterParameter["ACCOUNT_TYPE"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["ACCOUNT_START"] != null)
                {
                    sqlQuery += " AND V.acct_code like ( '" + VMVServices.Web.Utils.ReportViewFilterParameter["ACCOUNT_START"].ToString() + "%')";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Account"] != null)
                {
                    sqlQuery += " AND V.acct_code >= '" + VMVServices.Web.Utils.ReportViewFilterParameter["From_Account"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Account"] != null)
                {
                    sqlQuery += " AND V.acct_code <= '" + VMVServices.Web.Utils.ReportViewFilterParameter["To_Account"].ToString() + "'";
                }

                if (VMVServices.Web.Utils.ReportViewFilterParameter["CREATED_BY"] != null)
                {
                    sqlQuery += " AND UPPER(V.CREATED_BY) = UPPER('" + VMVServices.Web.Utils.ReportViewFilterParameter["CREATED_BY"].ToString() + "')";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["MODIFIED_BY"] != null)
                {
                    sqlQuery += " AND UPPER(V.MODIFIED_BY) = UPPER('" + VMVServices.Web.Utils.ReportViewFilterParameter["MODIFIED_BY"].ToString() + "')";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["CREATED_From_Date"] != null)
                {
                    sqlQuery += " AND V.CREATED_DATE >= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["CREATED_From_Date"].ToString() + "','dd/MM/yyyy') ";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["CREATED_To_Date"] != null)
                {
                    sqlQuery += " AND V.CREATED_DATE <= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["CREATED_To_Date"].ToString() + "','dd/MM/yyyy') ";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["MODIFIED_From_Date"] != null)
                {
                    sqlQuery += " AND V.MODIFIED_DATE >= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["MODIFIED_From_Date"].ToString() + "','dd/MM/yyyy') ";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["MODIFIED_To_Date"] != null)
                {
                    sqlQuery += " AND V.MODIFIED_DATE <= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["MODIFIED_To_Date"].ToString() + "','dd/MM/yyyy') ";
                }
            }

            return sqlQuery;
        }
        public static string GetPettyCashRpt()
        {
            sqlQuery = string.Empty;
            sqlQuery += " SELECT * FROM gl_petty_cash_book  V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["EMPLOYEE"] != null)
                {
                    sqlQuery += " AND V.gl_pca_user_id = '" + VMVServices.Web.Utils.ReportViewFilterParameter["EMPLOYEE"].ToString() + "'";
                }
            }

            return sqlQuery;
        }

        public static string getAccCodeswithOldAccCode()
        {
            sqlQuery = string.Empty;
            sqlQuery = "   select aa.acct_code_id as CODE_ID,";
            sqlQuery += "  aa.acct_code,";
            sqlQuery += "   aa.acct_code || ' - ' || aa.acct_code_desc || ' - ' || Attribute10 as CODE_NAME";
            sqlQuery += "   from gl_acct_codes aa";
            sqlQuery += "   where aa.enabled_flag = '1'";
            sqlQuery += "  and aa.workflow_completion_status = '1'  order by aa.acct_code asc ";
            return sqlQuery;
        }

        public static string getAccCodeswithR()
        {
            sqlQuery = string.Empty;
            sqlQuery = "   select aa.acct_code_id as CODE_ID,";
            sqlQuery += "  aa.acct_code,";
            sqlQuery += "   aa.acct_code || ' - ' || aa.acct_code_desc || ' - ' || Attribute10 as CODE_NAME";
            sqlQuery += "   from gl_acct_codes aa";
            sqlQuery += "   where aa.enabled_flag = '1'";
            sqlQuery += " and aa.acct_code like 'R%' ";
            sqlQuery += "  and aa.workflow_completion_status = '1'  order by aa.acct_code asc ";
            return sqlQuery;
        }

        public static string getTopLevelAcctGroup()
        {
            sqlQuery = string.Empty;
            sqlQuery += "SELECT DISTINCT LEVEL1,LEVEL1_ID FROM TEMP_GL_ACC_ID_NAME ";
            return sqlQuery;
        }

        public static string getStructName()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT S.ACCT_STRUCT_ID,S.ACCT_STRUCT_NAME" + VMVServices.Web.Utils.LanguageCode +" as ACCT_STRUCT_NAME";
            sqlQuery += " FROM GL_ACCT_STRUCTURE S, gl_companies_dtl g";
            sqlQuery += " WHERE S.ENABLED_FLAG = 1";
            sqlQuery += " AND S.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND (S.EFFECTIVE_END_DT IS NULL OR";
            sqlQuery += " (SYSDATE BETWEEN S.Effective_Start_Dt AND S.Effective_End_Dt))";
            sqlQuery += "  and g.comp_id  ='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " and s.ACCT_STRUCT_ID = g.comp_acct_struct_id  and g.workflow_completion_status=1";
            sqlQuery += " ORDER BY S.ACCT_STRUCT_NAME ";

            return sqlQuery;
        }


        public static string getAccountBasedGroup(string accGrpId)
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT GAC.ACCT_CODE_ID AS CODE_ID, GAC.ACCT_CODE ||' - '|| GAC.ACCT_CODE_DESC" + VMVServices.Web.Utils.LanguageCode + " AS CODE_NAME ";
            sqlQuery += " FROM GL_ACCT_CODES GAC , gl_companies_dtl g ";
            sqlQuery += " WHERE GAC.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " and  gac.ACCT_GRP_ID='" + accGrpId + "'";
            sqlQuery += " AND GAC.ENABLED_FLAG = 1 ";
            sqlQuery += "  and g.comp_id  ='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " and gac.acct_struct_id = g.comp_acct_struct_id  and g.workflow_completion_status=1";
            sqlQuery += " ORDER BY CODE_ID ";

            return sqlQuery;
        }
        public static string getAccount( string str_FromAcc="",string str_ToAcc="")
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT distinct GAC.ACCT_CODE_ID AS CODE_ID,GAC.ACCT_CODE ,GAC.ACCT_CODE ||' - '|| GAC.ACCT_CODE_DESC" + VMVServices.Web.Utils.LanguageCode + " AS CODE_NAME ";
            sqlQuery += " FROM GL_ACCT_CODES GAC, gl_companies_dtl g  ";
            sqlQuery += " WHERE GAC.WORKFLOW_COMPLETION_STATUS = 1  and g.workflow_completion_status=1";
            sqlQuery += " AND GAC.ENABLED_FLAG = 1 ";
            sqlQuery += "  and g.comp_id  ='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " and gac.acct_struct_id = g.comp_acct_struct_id";
            if (str_FromAcc.Length > 0)
            {
                sqlQuery += " AND GAC.ACCT_CODE>='" + str_FromAcc + "'";
            }
            if (str_ToAcc.Length > 0)
            {
                sqlQuery += " AND GAC.ACCT_CODE<='" + str_ToAcc + "'";
            }
            sqlQuery += " ORDER BY CODE_ID ";

            return sqlQuery;
        }

        public static string getAccount_BasedonAcctStruct()
        {
            sqlQuery = string.Empty;


            sqlQuery = " select ac.acct_code_id AS CODE_ID,ac.acct_code || ' - ' || ac.acct_code_desc" + VMVServices.Web.Utils.LanguageCode + " AS CODE_NAME";
            sqlQuery += " from GL_ACCT_CODES ac,gl_companies_dtl c";
            sqlQuery += " where ac.acct_struct_id = c.comp_acct_struct_id  and c.workflow_completion_status=1";
            sqlQuery += " and c.enabled_flag =1";
            sqlQuery += " and c.comp_id = '" + VMVServices.Web.Utils.OrganizationID + "'";
            return sqlQuery;
        }

        public static string getAccountWithoutControlAC()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT GAC.ACCT_CODE_ID AS CODE_ID, GAC.ACCT_CODE ||' - '|| GAC.ACCT_CODE_DESC" + VMVServices.Web.Utils.LanguageCode + " AS CODE_NAME ";
            sqlQuery += " FROM GL_ACCT_CODES GAC , gl_companies_dtl g ";
            sqlQuery += " WHERE GAC.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " AND GAC.ENABLED_FLAG = 1 and gac.IS_CONTROL_ACCT='N'  and g.workflow_completion_status=1";
            sqlQuery += "  and g.comp_id  ='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " and gac.acct_struct_id = g.comp_acct_struct_id";
            sqlQuery += " ORDER BY CODE_ID ";

            return sqlQuery;
        }

        public static string getAccountWithControlAC()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT DISTINCT GAC.ACCT_CODE_ID AS CODE_ID, GAC.ACCT_CODE ||' - '|| GAC.ACCT_CODE_DESC" + VMVServices.Web.Utils.LanguageCode + " AS CODE_NAME ";
            sqlQuery += " FROM GL_ACCT_CODES GAC, gl_companies_dtl g ";
            sqlQuery += " WHERE GAC.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " and gac.acct_struct_id = g.comp_acct_struct_id";
            sqlQuery += " AND GAC.ENABLED_FLAG = 1 and gac.IS_CONTROL_ACCT='Y'  and g.workflow_completion_status=1";
            sqlQuery += "  and g.comp_id  ='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " ORDER BY CODE_ID ";

            return sqlQuery;
        }
        public static string getGroupName()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT S.ACCT_GRP_ID,S.ACCT_GRP_DESC" + VMVServices.Web.Utils.LanguageCode + " as ACCT_GRP_DESC";
            sqlQuery += " FROM GL_ACCT_GROUPS S";
            sqlQuery += " WHERE S.ENABLED_FLAG = 1";
            sqlQuery += " AND S.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " AND (S.EFFECTIVE_END_DT IS NULL OR  (SYSDATE BETWEEN S.EFFECTIVE_START_DT AND S.EFFECTIVE_END_DT)) ";
            sqlQuery += " ORDER BY S.ACCT_GRP_DESC ";

            return sqlQuery;
        }

        public static string getSegment()
        {
            sqlQuery = string.Empty;

            sqlQuery = "SELECT S.SEGMENT_ID,S.SEGMENT_NAME" + VMVServices.Web.Utils.LanguageCode +" as SEGMENT_NAME ";
            sqlQuery += " FROM GL_SEGMENTS S";
            sqlQuery += " WHERE S.ENABLED_FLAG = 1";
            sqlQuery += " AND S.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND (S.EFFECTIVE_END_DT IS NULL OR (SYSDATE BETWEEN S.EFFECTIVE_START_DT AND S.EFFECTIVE_END_DT)) ";
            sqlQuery += " ORDER BY S.SEGMENT_NAME";

            return sqlQuery;
        }


        public static string getSegment_BasedonGroup(string acct_grp_id)
        {
            sqlQuery = string.Empty;


            sqlQuery = " SELECT s.segment_id,s.segment_name" + VMVServices.Web.Utils.LanguageCode + "  as segment_name,0 as ACCT_CODE_SEG_ID,AG.EFFECTIVE_START_DT as EFFECTIVE_START_DT,AG.EFFECTIVE_END_DT as EFFECTIVE_END_DT,'FALSE' AS ENABLED_FLAG";
            sqlQuery += " FROM  GL_ACCT_GROUP_DEFAULT_SEGMENTS ag,GL_SEGMENTS s,Gl_Acct_Grp_Default_Sgmnts_Dtl dsd";
            sqlQuery += " where dsd.acct_segment_id = s.segment_id";
            sqlQuery += " and ag.acct_def_id = dsd.acct_def_id";
            sqlQuery += " and ag.acct_grp_id = '" + acct_grp_id + "'";

            return sqlQuery;
        }


        public static string getSegmentDetails(string str_AcctCodeID)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select S.ACCT_CODE_ID,S.ACCT_CODE_SEG_ID,S.EFFECTIVE_START_DT,S.EFFECTIVE_END_DT,GS.SEGMENT_ID,GS.SEGMENT_NAME" + VMVServices.Web.Utils.LanguageCode + " as SEGMENT_NAME,'N' as DELETED,";
            sqlQuery += " CASE S.ENABLED_FLAG WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS ENABLED_FLAG";
            sqlQuery += " FROM GL_ACCT_CODE_SEGMENTS S,GL_SEGMENTS GS ";
            // sqlQuery += " WHERE S.ENABLED_FLAG = 1";
            sqlQuery += " WHERE GS.SEGMENT_ID =  S.ACCT_CODE_SEGMENT_ID   ";
            sqlQuery += " AND S.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND S.ACCT_CODE_ID ='" + str_AcctCodeID + "'";

            return sqlQuery;
        }
        public static string getAccCodeBasedSegment(string segmentValueId)
        {
            sqlQuery = string.Empty;

            sqlQuery = "   select a.acct_code_seg_id,";
            sqlQuery += "   a.acct_code_id as CODE_ID, aa.acct_code,";
            sqlQuery += "   aa.acct_code || ' - ' || aa.acct_code_desc" + VMVServices.Web.Utils.LanguageCode + " as CODE_NAME";
            sqlQuery += "  from gl_acct_codes aa, gl_acct_code_segments a";
            sqlQuery += "  where a.acct_code_id = aa.acct_code_id";
            sqlQuery += "   and aa.enabled_flag = '1'";
            sqlQuery += "   and aa.workflow_completion_status = '1'";
            //and aa.IS_CONTROL_ACCT='N' "; -- CHANGE LATER
            sqlQuery += "   and a.acct_code_segment_id =";
            sqlQuery += "   (select sv.segment_id";
            sqlQuery += "    from gl_segment_values sv";
            sqlQuery += "  where sv.segment_value_id = '" + segmentValueId + "')";


            return sqlQuery;
        }
        public static string getAccCodes()
        {
            sqlQuery = string.Empty;
            sqlQuery = "   select aa.acct_code_id as CODE_ID,";
            sqlQuery += "  aa.acct_code,";
            sqlQuery += "   aa.acct_code || ' - ' || aa.acct_code_desc as CODE_NAME";
            sqlQuery += "   from gl_acct_codes aa";
            sqlQuery += "   where aa.enabled_flag = '1'";
            sqlQuery += "  and aa.workflow_completion_status = '1'  order by aa.acct_code asc ";
            return sqlQuery;
        }
        public static string getAccCodeName(string accountId = "")
        {
            sqlQuery = string.Empty;


            sqlQuery = "   select aa.acct_code_id as CODE_ID,aa.ACCT_TYPE,";
            sqlQuery += "  (aa.acct_code||' -  '|| aa.acct_code_desc" + VMVServices.Web.Utils.LanguageCode + ") as CODE_NAME,aa.acct_code_desc" + VMVServices.Web.Utils.LanguageCode;
            sqlQuery += "   from gl_acct_codes aa, gl_companies_dtl g";
            sqlQuery += "   where";
            sqlQuery += "     aa.enabled_flag = '1'";
            sqlQuery += "    and aa.workflow_completion_status = '1'  and g.workflow_completion_status=1";
            if (accountId != string.Empty)
            {
                sqlQuery += "    and aa.acct_code_id='" + accountId + "'";
            }
            sqlQuery += "  and g.comp_id  ='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " and aa.acct_struct_id = g.comp_acct_struct_id";
            sqlQuery += "  order by aa.acct_code asc ";
            return sqlQuery;
        }


        public static string getAccCode4FinTempSegment(string Parent_id = "0")
        {
            sqlQuery = string.Empty;


            sqlQuery = "  select '0' as BS_TMPL_ID, aa.acct_code_id as CODE_ID,aa.ACCT_TYPE,";
            sqlQuery += " (aa.acct_code||' -  '|| aa.acct_code_desc" + VMVServices.Web.Utils.LanguageCode + ") as CODE_NAME,aa.acct_code_desc" + VMVServices.Web.Utils.LanguageCode+ " as acct_code_desc";
            sqlQuery += " ,'N' as SELECTED_REC ";
            sqlQuery += " from gl_acct_codes aa, gl_companies_dtl g";
            sqlQuery += " where";
            sqlQuery += " aa.enabled_flag = '1'";
            sqlQuery += " and aa.workflow_completion_status = '1'  and g.workflow_completion_status=1";
            sqlQuery += " and g.comp_id  ='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " and aa.acct_struct_id = g.comp_acct_struct_id";
            sqlQuery += " and  acct_code_id not in (select bs_acc_id from GL_COSTCENTER_TEMPLATE where BS_TMPL_PARENT_ID='" + Parent_id + "' and bs_acc_id is not null)";

            sqlQuery += " UNION ";

            sqlQuery += " select BST.BS_TMPL_ID, aa.acct_code_id as CODE_ID,aa.ACCT_TYPE,";
            sqlQuery += " (aa.acct_code||' -  '|| aa.acct_code_desc" + VMVServices.Web.Utils.LanguageCode + ") as CODE_NAME,aa.acct_code_desc" + VMVServices.Web.Utils.LanguageCode+ " as acct_code_desc ";
            sqlQuery += " ,'Y' as SELECTED_REC ";
            sqlQuery += " from gl_acct_codes aa, gl_companies_dtl g,GL_COSTCENTER_TEMPLATE bst";
            sqlQuery += " where";
            sqlQuery += " aa.enabled_flag = '1'";
            sqlQuery += " and aa.workflow_completion_status = '1'  and g.workflow_completion_status=1";
            sqlQuery += " and g.comp_id  ='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " and aa.acct_struct_id = g.comp_acct_struct_id";
            sqlQuery += " and  bst.BS_ACC_ID =  aa.acct_code_id";
            sqlQuery += " and BS_TMPL_PARENT_ID='" + Parent_id + "'";
            sqlQuery += "  order by CODE_NAME asc ";
            return sqlQuery;
        }
        public static string getAccCodeBasedOrg(string OrgId, string accountId = "")
        {
            sqlQuery = string.Empty;


            sqlQuery = "   select aa.acct_code_id as CODE_ID,aa.ACCT_TYPE,";
            sqlQuery += "  (aa.acct_code||' -  '|| aa.acct_code_desc" + VMVServices.Web.Utils.LanguageCode + ") as CODE_NAME,aa.acct_code_desc" + VMVServices.Web.Utils.LanguageCode+" as acct_code_desc";
            sqlQuery += "   from gl_acct_codes aa, gl_acct_structure a,gl_companies_dtl c";
            sqlQuery += "   where a.acct_struct_id = aa.acct_struct_id";
            sqlQuery += "    and c.comp_acct_struct_id=a.acct_struct_id";
            sqlQuery += "    and aa.enabled_flag = '1'";
            sqlQuery += "    and aa.workflow_completion_status = '1'  and c.workflow_completion_status=1";



            sqlQuery += "    and c.comp_id='" + OrgId + "'";

            if (accountId != string.Empty)
            {
                sqlQuery += "    and aa.acct_code_id='" + accountId + "'";
            }
            sqlQuery += "  order by aa.acct_code asc ";
            return sqlQuery;
        }
        public static string getNonCtrlAccCodeBasedOrg(string OrgId, string mode = "", bool fundTrans = false)
        {
            sqlQuery = string.Empty;


            sqlQuery = "   select aa.acct_code_id as CODE_ID,aa.ACCT_TYPE,";
            sqlQuery += "  (aa.acct_code||' -  '|| aa.acct_code_desc" + VMVServices.Web.Utils.LanguageCode + ") as CODE_NAME,aa.acct_code_desc" + VMVServices.Web.Utils.LanguageCode+ " as acct_code_desc";
            sqlQuery += "   from gl_acct_codes aa, gl_acct_structure a,gl_companies_dtl c,gl_acct_groups    ag"; // acc group added by jeyarani & nsn on 7/8/14
            sqlQuery += "   where a.acct_struct_id = aa.acct_struct_id";
            sqlQuery += "    and c.comp_acct_struct_id=a.acct_struct_id";
            sqlQuery += "    and ag.acct_grp_id = aa.acct_grp_id";
            sqlQuery += "    and aa.enabled_flag = '1'";
            sqlQuery += "    and aa.workflow_completion_status = '1'  and c.workflow_completion_status=1";
            sqlQuery += "    and c.enabled_flag='1'";

            if (mode != string.Empty && mode == "add")
            {
                sqlQuery += "    and aa.IS_CONTROL_ACCT='N'";
            }

            if (fundTrans == true)
            {
                sqlQuery += "  and ag.ACCT_FUND_TRANSFER='1'";
            }
            sqlQuery += "    and c.comp_id='" + OrgId + "'";

            sqlQuery += "  order by aa.acct_code asc ";
            return sqlQuery;
        }


        public static string getAccCodeBasedOrgRep()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT distinct GAC.ACCT_CODE_ID AS CODE_ID,GAC.ACCT_CODE ,GAC.ACCT_CODE||' -  '|| GAC.acct_code_desc" + VMVServices.Web.Utils.LanguageCode + " AS CODE_NAME ";
            sqlQuery += " FROM GL_ACCT_CODES GAC, gl_companies_dtl g  ";
            sqlQuery += " WHERE GAC.WORKFLOW_COMPLETION_STATUS = 1  and g.workflow_completion_status=1";
            sqlQuery += " AND GAC.ENABLED_FLAG = 1 ";
            sqlQuery += "  and g.comp_id  ='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " and gac.acct_struct_id = g.comp_acct_struct_id";
            sqlQuery += " ORDER BY CODE_ID ";

            return sqlQuery;
        }

        public static string getAccCodeBasedStartupLetter(string startWith)
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT distinct GAC.ACCT_CODE_ID AS CODE_ID,GAC.ACCT_CODE ,GAC.ACCT_CODE||' -  '|| GAC.acct_code_desc" + VMVServices.Web.Utils.LanguageCode + " AS CODE_NAME ";
            sqlQuery += " FROM GL_ACCT_CODES GAC, gl_companies_dtl g  ";
            sqlQuery += " WHERE GAC.WORKFLOW_COMPLETION_STATUS = 1  and g.workflow_completion_status=1";
            sqlQuery += " AND GAC.ENABLED_FLAG = 1 ";
            sqlQuery += "  and g.comp_id  ='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += "  and gac.acct_struct_id = g.comp_acct_struct_id";
            sqlQuery += "  and (gac.acct_code like '" + startWith + "%') ";
            sqlQuery += " ORDER BY CODE_ID ";

            return sqlQuery;
        }
         public static string getAccCodeBasedOrgRepForR()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT distinct GAC.ACCT_CODE_ID AS CODE_ID,GAC.ACCT_CODE ,GAC.ACCT_CODE||' -  '|| GAC.acct_code_desc" + VMVServices.Web.Utils.LanguageCode + " AS CODE_NAME ";
            sqlQuery += " FROM GL_ACCT_CODES GAC, gl_companies_dtl g  ";
            sqlQuery += " WHERE GAC.WORKFLOW_COMPLETION_STATUS = 1  and g.workflow_completion_status=1";
            sqlQuery += " AND GAC.ENABLED_FLAG = 1 ";
            sqlQuery += "  and (gac.acct_code like 'R%' OR GAC.ACCT_CODE LIKE 'E%') ";
            sqlQuery += "  and g.comp_id  ='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " and gac.acct_struct_id = g.comp_acct_struct_id";
            sqlQuery += " ORDER BY CODE_ID ";

            return sqlQuery;
        }

        public static string getExpAccCodeBasedOrgRep()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT distinct GAC.ACCT_CODE_ID AS CODE_ID,GAC.ACCT_CODE ,GAC.ACCT_CODE||' -  '|| GAC.acct_code_desc" + VMVServices.Web.Utils.LanguageCode + " AS CODE_NAME ";
            sqlQuery += " FROM GL_ACCT_CODES GAC, gl_companies_dtl g  ";
            sqlQuery += " WHERE GAC.WORKFLOW_COMPLETION_STATUS = 1  and g.workflow_completion_status=1";
            sqlQuery += " AND GAC.ENABLED_FLAG = 1 ";
            sqlQuery += "  and g.comp_id  ='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " and gac.acct_struct_id = g.comp_acct_struct_id";
            sqlQuery += " and gac.acct_code like 'E%'";
            sqlQuery += " ORDER BY CODE_ID ";

            return sqlQuery;
        }
        public static string getRevenueAccCodeBasedOrgRep()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT distinct GAC.ACCT_CODE_ID AS CODE_ID,GAC.ACCT_CODE ,GAC.ACCT_CODE||' -  '|| GAC.acct_code_desc" + VMVServices.Web.Utils.LanguageCode + " AS CODE_NAME ";
            sqlQuery += " FROM GL_ACCT_CODES GAC, gl_companies_dtl g  ";
            sqlQuery += " WHERE GAC.WORKFLOW_COMPLETION_STATUS = 1  and g.workflow_completion_status=1";
            sqlQuery += " AND GAC.ENABLED_FLAG = 1 ";
            sqlQuery += "  and g.comp_id  ='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " and gac.acct_struct_id = g.comp_acct_struct_id and gac.acct_code like 'R%'";
            sqlQuery += " ORDER BY CODE_ID ";

            return sqlQuery;
        }
        public static string getSegmentValues(string str_AccCodeId)
        {
            sqlQuery = string.Empty;
            sqlQuery += " SELECT SV.SEGMENT_VALUE_ID,  S.SEGMENT_NAME" + VMVServices.Web.Utils.LanguageCode + " || ' - ' || SV.SEGMENT_VALUE" + VMVServices.Web.Utils.LanguageCode + " as SEGMENT_VALUE ";
            sqlQuery += " FROM GL_ACCT_CODE_SEGMENTS ACS ";
            sqlQuery += " INNER JOIN GL_SEGMENTS S ON S.SEGMENT_ID = ACS.ACCT_CODE_SEGMENT_ID ";
            sqlQuery += " INNER JOIN GL_SEGMENT_VALUES SV ON SV.SEGMENT_ID = S.SEGMENT_ID ";
            sqlQuery += " WHERE  SV.ENABLED_FLAG=1 AND ACS.ACCT_CODE_SEG_ID='" + str_AccCodeId + "'";

            return sqlQuery;
        }

        public static string getUserdtls()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT SU.USER_CODE,SU.FIRST_NAME || ' ' || SU.MIDDLE_NAME || ' ' || SU.LAST_NAME AS USER_NAMES ";
            sqlQuery += " FROM SSM_USERS SU ";
            sqlQuery += " WHERE SU.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " AND SU.ENABLED_FLAG = 1 ";
            sqlQuery += " ORDER BY USER_CODE ";

            return sqlQuery;
        }
        public static string getUserNamedtls(string usr_code)
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT SU.USER_CODE,SU.FIRST_NAME || ' ' || SU.MIDDLE_NAME || ' ' || SU.LAST_NAME AS USER_NAMES ";
            sqlQuery += " FROM SSM_USERS SU ";
            sqlQuery += " WHERE SU.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " AND SU.ENABLED_FLAG = 1 ";
            sqlQuery += " AND SU.USER_CODE = '" + usr_code + "'";
            sqlQuery += " ORDER BY USER_CODE ";

            return sqlQuery;
        }
        public static string getAccountdtls(string Acct_code)
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT GAC.ACCT_CODE_ID AS CODE_ID,GAC.ACCT_CODE ,GAC.ACCT_CODE ||' - '|| GAC.ACCT_CODE_DESC" + VMVServices.Web.Utils.LanguageCode + " AS CODE_NAME ";
            sqlQuery += " FROM GL_ACCT_CODES GAC, gl_companies_dtl g ";
            sqlQuery += " WHERE GAC.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " AND GAC.ENABLED_FLAG = 1 ";
            sqlQuery += " AND GAC.ACCT_CODE_ID = '" + Acct_code + "'";
            sqlQuery += "  and g.comp_id ='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " and GAC.acct_struct_id = g.comp_acct_struct_id  and g.workflow_completion_status=1";
            sqlQuery += " ORDER BY CODE_ID ";

            return sqlQuery;
        }

        public static string getDetailedRevenue(string noyrs)
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT * FROM VW_DETAILED_REVENUE V WHERE ROWNUM > 0 ";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.SEGMENT_VALUE_ID] != null)
                {
                    sqlQuery += " AND V.GLOBAL_SEGMENT_ID='" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.SEGMENT_VALUE_ID].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["FromAccount"] != null)
                {
                    sqlQuery += " AND V.ACCOUNT_CODE>='" + VMVServices.Web.Utils.ReportFilterParameter["FromAccount"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["ToAccount"] != null)
                {
                    sqlQuery += " AND V.ACCOUNT_CODE<='" + VMVServices.Web.Utils.ReportFilterParameter["ToAccount"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["noyrs"] != null)
                {
                    if (noyrs == "0")
                    {
                        sqlQuery += " AND V.cal_acct_year <= to_CHAR(sysdate,'YYYY') ";
                        sqlQuery += " AND v.cal_acct_year >= TO_NUMBER(to_CHAR(sysdate,'YYYY'))";
                    }
                    else
                    {
                        int yrs = Convert.ToInt16(noyrs) - 1;
                        sqlQuery += " AND V.cal_acct_year <= to_CHAR(sysdate,'YYYY') ";
                        sqlQuery += " AND v.cal_acct_year >= TO_NUMBER(to_CHAR(sysdate,'YYYY')) - " + yrs + "";
                    }

                }

                if (VMVServices.Web.Utils.ReportViewFilterParameter["UnPost"].ToString() == "0")
                {
                    sqlQuery += " and v.workflow_completion_status='1'";
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["WITHZERO"] == "FALSE")
                {
                    sqlQuery += " AND abs(V.Begin_Balance_Acct_Dr)>0  ";
                }

                //if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.EFFECTIVE_FROM_DT] != null)
                //{
                //    sqlQuery += " AND to_date(V.CREATED_DATE,'dd/MM/yyyy') between to_date('" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.EFFECTIVE_FROM_DT].ToString() + "','dd/MM/yyyy')";
                //}
                //if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.EFFECTIVE_TO_DT] != null)
                //{
                //    sqlQuery += " AND to_date('" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.EFFECTIVE_TO_DT].ToString() + "','dd/MM/yyyy')";
                //}
            }

            return sqlQuery;

        }

        public static string getDetailedRevwithDiff()
        {
            sqlQuery = string.Empty;

            sqlQuery = "SELECT 'DIFF_YEAR','SEG_V_ID-0000001341',AC.ACCT_CODE,AC.ACCT_CODE_DESC,AC.ACCT_CODE_ID ";
            sqlQuery += " ,abs((SELECT BEGIN_BALANCE_ACCT_DR FROM VW_DETAILED_REVENUE VR WHERE CAL_ACCT_YEAR=2015 AND VR.acct_code_id= AC.ACCT_CODE_ID) - ";
            sqlQuery += " (SELECT NVL(BEGIN_BALANCE_ACCT_DR,0) FROM VW_DETAILED_REVENUE VR WHERE CAL_ACCT_YEAR=2014 AND VR.acct_code_id= AC.ACCT_CODE_ID)) BEGIN_BALANCE_ACCT_DR ";
            sqlQuery += " ,'Diff','','0',ac.acct_org_id,'1' ";
            sqlQuery += " FROM GL_ACCT_CODES AC WHERE AC.ACCT_CODE_ID IN (SELECT ACCT_CODE_ID FROM VW_DETAILED_REVENUE) ";
            return sqlQuery;
        }
        public static string GetBalanceSheetRPT()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT * FROM VW_BALANCE_SHEET V WHERE ROWNUM > 0 ";

            sqlQuery += " and v.je_comp_id='" + VMVServices.Web.Utils.OrganizationID + "'";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.FROM_DATE] != null)
                {
                    sqlQuery += " AND to_char(to_date(V.je_date,'dd/MM/yyyy')) between to_date('" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.FROM_DATE].ToString() + "','dd/MM/yyyy')";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.TO_DATE] != null)
                {
                    sqlQuery += " AND to_date('" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.TO_DATE].ToString() + "','dd/MM/yyyy')";
                }
            }

            return sqlQuery;
        }
        public static string GetIncomeRPT(string unpost, string acctCodeId, string segmentId)
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT * FROM VW_INCOME_REPORT V WHERE ROWNUM > 0 ";

            sqlQuery += " and v.je_comp_id='" + VMVServices.Web.Utils.OrganizationID + "'";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.FROM_DATE] != null)
                {
                    sqlQuery += " AND to_char(to_date(V.je_date,'dd/MM/yyyy')) between to_date('" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.FROM_DATE].ToString() + "','dd/MM/yyyy')";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.TO_DATE] != null)
                {
                    sqlQuery += " AND to_date('" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.TO_DATE].ToString() + "','dd/MM/yyyy')";
                }

                if (VMVServices.Web.Utils.ReportFilterParameter["Debit_From_Amt"] != null || VMVServices.Web.Utils.ReportFilterParameter["Debit_To_Amt"] != null || VMVServices.Web.Utils.ReportFilterParameter["Credit_From_Amt"] != null || VMVServices.Web.Utils.ReportFilterParameter["Credit_To_Amt"] != null)
                {
                    sqlQuery += " AND ( ";
                    if (VMVServices.Web.Utils.ReportFilterParameter["Debit_From_Amt"] != null && VMVServices.Web.Utils.ReportFilterParameter["Debit_To_Amt"] != null)
                    {
                        sqlQuery += " (V.debit_amt >= '" + (VMVServices.Web.Utils.ReportFilterParameter["Debit_From_Amt"] == null ? "0" : VMVServices.Web.Utils.ReportFilterParameter["Debit_From_Amt"].ToString()) + "')  ";
                        sqlQuery += " AND ( V.debit_amt <= '" + (VMVServices.Web.Utils.ReportFilterParameter["Debit_To_Amt"] == null ? "0" : VMVServices.Web.Utils.ReportFilterParameter["Debit_To_Amt"].ToString()) + "') ";

                    }
                    else if (VMVServices.Web.Utils.ReportFilterParameter["Debit_From_Amt"] != null)
                    {
                        sqlQuery += " (V.debit_amt >= '" + (VMVServices.Web.Utils.ReportFilterParameter["Debit_From_Amt"] == null ? "0" : VMVServices.Web.Utils.ReportFilterParameter["Debit_From_Amt"].ToString()) + "')  ";


                    }
                    else if (VMVServices.Web.Utils.ReportFilterParameter["Debit_To_Amt"] != null)
                    {
                        sqlQuery += "  (V.debit_amt <= '" + (VMVServices.Web.Utils.ReportFilterParameter["Debit_To_Amt"] == null ? "0" : VMVServices.Web.Utils.ReportFilterParameter["Debit_To_Amt"].ToString()) + "') ";

                    }

                    if ((VMVServices.Web.Utils.ReportFilterParameter["Debit_From_Amt"] != null || VMVServices.Web.Utils.ReportFilterParameter["Debit_To_Amt"] != null) && (VMVServices.Web.Utils.ReportFilterParameter["Credit_From_Amt"] != null || VMVServices.Web.Utils.ReportFilterParameter["Credit_To_Amt"] != null))
                    { sqlQuery += " OR "; }


                    if (VMVServices.Web.Utils.ReportFilterParameter["Credit_From_Amt"] != null && VMVServices.Web.Utils.ReportFilterParameter["Credit_To_Amt"] != null)
                    {
                        sqlQuery += " (V.credit_amt >= '" + (VMVServices.Web.Utils.ReportFilterParameter["Credit_From_Amt"] == null ? "0" : VMVServices.Web.Utils.ReportFilterParameter["Credit_From_Amt"].ToString()) + "') ";
                        sqlQuery += " AND ( V.credit_amt <= '" + (VMVServices.Web.Utils.ReportFilterParameter["Credit_To_Amt"] == null ? "0" : VMVServices.Web.Utils.ReportFilterParameter["Credit_To_Amt"].ToString()) + "') ";
                    }
                    else if (VMVServices.Web.Utils.ReportFilterParameter["Credit_From_Amt"] != null)
                    {
                        sqlQuery += " (V.credit_amt >= '" + (VMVServices.Web.Utils.ReportFilterParameter["Credit_From_Amt"] == null ? "0" : VMVServices.Web.Utils.ReportFilterParameter["Credit_From_Amt"].ToString()) + "') ";

                    }
                    else if (VMVServices.Web.Utils.ReportFilterParameter["Credit_To_Amt"] != null)
                    {
                        sqlQuery += "  ( V.credit_amt <= '" + (VMVServices.Web.Utils.ReportFilterParameter["Credit_To_Amt"] == null ? "0" : VMVServices.Web.Utils.ReportFilterParameter["Credit_To_Amt"].ToString()) + "') ";
                    }


                    sqlQuery += " )";
                }



                //if (VMVServices.Web.Utils.ReportFilterParameter["WITHZERO"] == "FALSE")
                //{
                //    sqlQuery += " AND (V.debit_amt >0 OR V.credit_amt >0)  ";
                //}
                //if (VMVServices.Web.Utils.ReportFilterParameter["Debit_From_Amt"] != null)
                //{
                //    sqlQuery += " and (v.debit_amt) >= " + VMVServices.Web.Utils.ReportFilterParameter["Debit_From_Amt"];
                //}
                //if (VMVServices.Web.Utils.ReportFilterParameter["Debit_To_Amt"] != null)
                //{
                //    sqlQuery += " and (v.debit_amt) <= " + VMVServices.Web.Utils.ReportFilterParameter["Debit_To_Amt"];
                //}
                //if (VMVServices.Web.Utils.ReportFilterParameter["Credit_From_Amt"] != null)
                //{
                //    sqlQuery += " and (v.credit_amt) >= " + VMVServices.Web.Utils.ReportFilterParameter["Credit_From_Amt"];
                //}
                //if (VMVServices.Web.Utils.ReportFilterParameter["Credit_To_Amt"] != null)
                //{
                //    sqlQuery += " and (v.credit_amt) <= " + VMVServices.Web.Utils.ReportFilterParameter["Credit_To_Amt"];
                //}
                if (VMVServices.Web.Utils.ReportFilterParameter["FromAccount"] != null)
                {
                    sqlQuery += " AND V.ACCT_CODE >=  '" + VMVServices.Web.Utils.ReportFilterParameter["FromAccount"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["ToAccount"] != null)
                {
                    sqlQuery += " AND V.ACCT_CODE <=  '" + VMVServices.Web.Utils.ReportFilterParameter["ToAccount"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["FromJournal"] != null)
                {
                    sqlQuery += " AND V.je_number >=  '" + VMVServices.Web.Utils.ReportFilterParameter["FromJournal"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["ToJournal"] != null)
                {
                    sqlQuery += " AND V.je_number <=  '" + VMVServices.Web.Utils.ReportFilterParameter["ToJournal"].ToString() + "'";
                }
                if (acctCodeId.ToUpper() != "ALL")
                {
                    sqlQuery += " and v.acct_code_id='" + acctCodeId + "'";
                }
                if (segmentId.ToUpper() != "ALL")
                {
                    sqlQuery += " and v.je_segment_id_1='" + segmentId + "'";
                }
                if (unpost != "1")
                {
                    sqlQuery += " AND V.workflow_completion_status=1";
                }

            }

            return sqlQuery;
        }
        public static string GetExpenseRPT(string unpost, string acctCodeId, string segmentId)
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT * FROM VW_EXPENSES_REPORT V WHERE ROWNUM > 0 ";

            sqlQuery += " and v.je_comp_id='" + VMVServices.Web.Utils.OrganizationID + "'";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.FROM_DATE] != null)
                {
                    sqlQuery += " AND to_char(to_date(V.je_date,'dd/MM/yyyy')) between to_date('" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.FROM_DATE].ToString() + "','dd/MM/yyyy')";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.TO_DATE] != null)
                {
                    sqlQuery += " AND to_date('" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.TO_DATE].ToString() + "','dd/MM/yyyy')";
                }
                //if (VMVServices.Web.Utils.ReportFilterParameter["WITHZERO"] == "FALSE")
                //{
                //    sqlQuery += " AND (V.debit_amt >0 OR V.credit_amt >0)  ";
                //}
                if (VMVServices.Web.Utils.ReportFilterParameter["Debit_From_Amt"] != null || VMVServices.Web.Utils.ReportFilterParameter["Debit_To_Amt"] != null || VMVServices.Web.Utils.ReportFilterParameter["Credit_From_Amt"] != null || VMVServices.Web.Utils.ReportFilterParameter["Credit_To_Amt"] != null)
                {
                    sqlQuery += " AND ( ";
                    if (VMVServices.Web.Utils.ReportFilterParameter["Debit_From_Amt"] != null && VMVServices.Web.Utils.ReportFilterParameter["Debit_To_Amt"] != null)
                    {
                        sqlQuery += " (V.debit_amt >= '" + (VMVServices.Web.Utils.ReportFilterParameter["Debit_From_Amt"] == null ? "0" : VMVServices.Web.Utils.ReportFilterParameter["Debit_From_Amt"].ToString()) + "')  ";
                        sqlQuery += " AND ( V.debit_amt <= '" + (VMVServices.Web.Utils.ReportFilterParameter["Debit_To_Amt"] == null ? "0" : VMVServices.Web.Utils.ReportFilterParameter["Debit_To_Amt"].ToString()) + "') ";

                    }
                    else if (VMVServices.Web.Utils.ReportFilterParameter["Debit_From_Amt"] != null)
                    {
                        sqlQuery += " (V.debit_amt >= '" + (VMVServices.Web.Utils.ReportFilterParameter["Debit_From_Amt"] == null ? "0" : VMVServices.Web.Utils.ReportFilterParameter["Debit_From_Amt"].ToString()) + "')  ";


                    }
                    else if (VMVServices.Web.Utils.ReportFilterParameter["Debit_To_Amt"] != null)
                    {
                        sqlQuery += "  (V.debit_amt <= '" + (VMVServices.Web.Utils.ReportFilterParameter["Debit_To_Amt"] == null ? "0" : VMVServices.Web.Utils.ReportFilterParameter["Debit_To_Amt"].ToString()) + "') ";

                    }

                    if ((VMVServices.Web.Utils.ReportFilterParameter["Debit_From_Amt"] != null || VMVServices.Web.Utils.ReportFilterParameter["Debit_To_Amt"] != null) && (VMVServices.Web.Utils.ReportFilterParameter["Credit_From_Amt"] != null || VMVServices.Web.Utils.ReportFilterParameter["Credit_To_Amt"] != null))
                    { sqlQuery += " OR "; }


                    if (VMVServices.Web.Utils.ReportFilterParameter["Credit_From_Amt"] != null && VMVServices.Web.Utils.ReportFilterParameter["Credit_To_Amt"] != null)
                    {
                        sqlQuery += " (V.credit_amt >= '" + (VMVServices.Web.Utils.ReportFilterParameter["Credit_From_Amt"] == null ? "0" : VMVServices.Web.Utils.ReportFilterParameter["Credit_From_Amt"].ToString()) + "') ";
                        sqlQuery += " AND ( V.credit_amt <= '" + (VMVServices.Web.Utils.ReportFilterParameter["Credit_To_Amt"] == null ? "0" : VMVServices.Web.Utils.ReportFilterParameter["Credit_To_Amt"].ToString()) + "') ";
                    }
                    else if (VMVServices.Web.Utils.ReportFilterParameter["Credit_From_Amt"] != null)
                    {
                        sqlQuery += " (V.credit_amt >= '" + (VMVServices.Web.Utils.ReportFilterParameter["Credit_From_Amt"] == null ? "0" : VMVServices.Web.Utils.ReportFilterParameter["Credit_From_Amt"].ToString()) + "') ";

                    }
                    else if (VMVServices.Web.Utils.ReportFilterParameter["Credit_To_Amt"] != null)
                    {
                        sqlQuery += "  ( V.credit_amt <= '" + (VMVServices.Web.Utils.ReportFilterParameter["Credit_To_Amt"] == null ? "0" : VMVServices.Web.Utils.ReportFilterParameter["Credit_To_Amt"].ToString()) + "') ";
                    }


                    sqlQuery += " )";
                }

                //if (VMVServices.Web.Utils.ReportFilterParameter["Debit_From_Amt"] != null)
                //{
                //    sqlQuery += " and (v.debit_amt) >= " + VMVServices.Web.Utils.ReportFilterParameter["Debit_From_Amt"];
                //}
                //if (VMVServices.Web.Utils.ReportFilterParameter["Debit_To_Amt"] != null)
                //{
                //    sqlQuery += " and (v.debit_amt) <= " + VMVServices.Web.Utils.ReportFilterParameter["Debit_To_Amt"];
                //}
                //if (VMVServices.Web.Utils.ReportFilterParameter["Credit_From_Amt"] != null)
                //{
                //    sqlQuery += " and (v.credit_amt) >= " + VMVServices.Web.Utils.ReportFilterParameter["Credit_From_Amt"];
                //}
                //if (VMVServices.Web.Utils.ReportFilterParameter["Credit_To_Amt"] != null)
                //{
                //    sqlQuery += " and (v.credit_amt) <= " + VMVServices.Web.Utils.ReportFilterParameter["Credit_To_Amt"];
                //}
                if (VMVServices.Web.Utils.ReportFilterParameter["FromAccount"] != null)
                {
                    sqlQuery += " AND V.ACCT_CODE >=  '" + VMVServices.Web.Utils.ReportFilterParameter["FromAccount"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["ToAccount"] != null)
                {
                    sqlQuery += " AND V.ACCT_CODE <=  '" + VMVServices.Web.Utils.ReportFilterParameter["ToAccount"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["FromJournal"] != null)
                {
                    sqlQuery += " AND V.je_number >=  '" + VMVServices.Web.Utils.ReportFilterParameter["FromJournal"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["ToJournal"] != null)
                {
                    sqlQuery += " AND V.je_number <=  '" + VMVServices.Web.Utils.ReportFilterParameter["ToJournal"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["From_Group"] != null)
                {
                    sqlQuery += " AND V.acct_grp_desc >=  '" + VMVServices.Web.Utils.ReportFilterParameter["From_Group"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["To_Group"] != null)
                {
                    sqlQuery += " AND V.acct_grp_desc  <=  '" + VMVServices.Web.Utils.ReportFilterParameter["To_Group"].ToString() + "'";
                }
                if (acctCodeId.ToUpper() != "ALL")
                {
                    sqlQuery += " and v.acct_code_id='" + acctCodeId + "'";
                }
                if (segmentId.ToUpper() != "ALL")
                {
                    sqlQuery += " and v.je_segment_id_1='" + segmentId + "'";
                }
                if (unpost != "1")
                {
                    sqlQuery += " AND V.workflow_completion_status=1";
                }
            }

            return sqlQuery;
        }
        public static string GetRevenueRPT(string unpost, string acctCodeId, string segmentId)
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT * FROM VW_REVENUE_EXPEN_INCOM_REPORT V WHERE ROWNUM > 0 ";

            sqlQuery += " and v.je_comp_id='" + VMVServices.Web.Utils.OrganizationID + "'";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.FROM_DATE] != null)
                {
                    sqlQuery += " AND to_char(to_date(V.je_date,'dd/MM/yyyy')) between to_date('" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.FROM_DATE].ToString() + "','dd/MM/yyyy')";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.TO_DATE] != null)
                {
                    sqlQuery += " AND to_date('" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.TO_DATE].ToString() + "','dd/MM/yyyy')";
                }

                if (VMVServices.Web.Utils.ReportFilterParameter["Debit_From_Amt"] != null || VMVServices.Web.Utils.ReportFilterParameter["Debit_To_Amt"] != null || VMVServices.Web.Utils.ReportFilterParameter["Credit_From_Amt"] != null || VMVServices.Web.Utils.ReportFilterParameter["Credit_To_Amt"] != null)
                {
                    sqlQuery += " AND ( ";
                    if (VMVServices.Web.Utils.ReportFilterParameter["Debit_From_Amt"] != null && VMVServices.Web.Utils.ReportFilterParameter["Debit_To_Amt"] != null)
                    {
                        sqlQuery += " (V.debit_amt >= '" + (VMVServices.Web.Utils.ReportFilterParameter["Debit_From_Amt"] == null ? "0" : VMVServices.Web.Utils.ReportFilterParameter["Debit_From_Amt"].ToString()) + "')  ";
                        sqlQuery += " AND ( V.debit_amt <= '" + (VMVServices.Web.Utils.ReportFilterParameter["Debit_To_Amt"] == null ? "0" : VMVServices.Web.Utils.ReportFilterParameter["Debit_To_Amt"].ToString()) + "') ";

                    }
                    else if (VMVServices.Web.Utils.ReportFilterParameter["Debit_From_Amt"] != null)
                    {
                        sqlQuery += " (V.debit_amt >= '" + (VMVServices.Web.Utils.ReportFilterParameter["Debit_From_Amt"] == null ? "0" : VMVServices.Web.Utils.ReportFilterParameter["Debit_From_Amt"].ToString()) + "')  ";


                    }
                    else if (VMVServices.Web.Utils.ReportFilterParameter["Debit_To_Amt"] != null)
                    {
                        sqlQuery += "  (V.debit_amt <= '" + (VMVServices.Web.Utils.ReportFilterParameter["Debit_To_Amt"] == null ? "0" : VMVServices.Web.Utils.ReportFilterParameter["Debit_To_Amt"].ToString()) + "') ";

                    }

                    if ((VMVServices.Web.Utils.ReportFilterParameter["Debit_From_Amt"] != null || VMVServices.Web.Utils.ReportFilterParameter["Debit_To_Amt"] != null) && (VMVServices.Web.Utils.ReportFilterParameter["Credit_From_Amt"] != null || VMVServices.Web.Utils.ReportFilterParameter["Credit_To_Amt"] != null))
                    { sqlQuery += " OR "; }


                    if (VMVServices.Web.Utils.ReportFilterParameter["Credit_From_Amt"] != null && VMVServices.Web.Utils.ReportFilterParameter["Credit_To_Amt"] != null)
                    {
                        sqlQuery += " (V.credit_amt >= '" + (VMVServices.Web.Utils.ReportFilterParameter["Credit_From_Amt"] == null ? "0" : VMVServices.Web.Utils.ReportFilterParameter["Credit_From_Amt"].ToString()) + "') ";
                        sqlQuery += " AND ( V.credit_amt <= '" + (VMVServices.Web.Utils.ReportFilterParameter["Credit_To_Amt"] == null ? "0" : VMVServices.Web.Utils.ReportFilterParameter["Credit_To_Amt"].ToString()) + "') ";
                    }
                    else if (VMVServices.Web.Utils.ReportFilterParameter["Credit_From_Amt"] != null)
                    {
                        sqlQuery += " (V.credit_amt >= '" + (VMVServices.Web.Utils.ReportFilterParameter["Credit_From_Amt"] == null ? "0" : VMVServices.Web.Utils.ReportFilterParameter["Credit_From_Amt"].ToString()) + "') ";

                    }
                    else if (VMVServices.Web.Utils.ReportFilterParameter["Credit_To_Amt"] != null)
                    {
                        sqlQuery += "  ( V.credit_amt <= '" + (VMVServices.Web.Utils.ReportFilterParameter["Credit_To_Amt"] == null ? "0" : VMVServices.Web.Utils.ReportFilterParameter["Credit_To_Amt"].ToString()) + "') ";
                    }


                    sqlQuery += " )";
                }
                //if (VMVServices.Web.Utils.ReportFilterParameter["WITHZERO"] == "FALSE")
                //{
                //    sqlQuery += " AND (V.debit_amt >0 OR V.credit_amt >0)  ";
                //}
                //if (VMVServices.Web.Utils.ReportFilterParameter["Debit_From_Amt"] != null)
                //{
                //    sqlQuery += " and (v.debit_amt) >= " + VMVServices.Web.Utils.ReportFilterParameter["Debit_From_Amt"];
                //}
                //if (VMVServices.Web.Utils.ReportFilterParameter["Debit_To_Amt"] != null)
                //{
                //    sqlQuery += " and (v.debit_amt) <= " + VMVServices.Web.Utils.ReportFilterParameter["Debit_To_Amt"];
                //}
                //if (VMVServices.Web.Utils.ReportFilterParameter["Credit_From_Amt"] != null)
                //{
                //    sqlQuery += " and (v.credit_amt) >= " + VMVServices.Web.Utils.ReportFilterParameter["Credit_From_Amt"];
                //}
                //if (VMVServices.Web.Utils.ReportFilterParameter["Credit_To_Amt"] != null)
                //{
                //    sqlQuery += " and (v.credit_amt) <= " + VMVServices.Web.Utils.ReportFilterParameter["Credit_To_Amt"];
                //}
                if (VMVServices.Web.Utils.ReportFilterParameter["FromAccount"] != null)
                {
                    sqlQuery += " AND V.ACCT_CODE >=  '" + VMVServices.Web.Utils.ReportFilterParameter["FromAccount"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["ToAccount"] != null)
                {
                    sqlQuery += " AND V.ACCT_CODE <=  '" + VMVServices.Web.Utils.ReportFilterParameter["ToAccount"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["FromJournal"] != null)
                {
                    sqlQuery += " AND V.je_number >=  '" + VMVServices.Web.Utils.ReportFilterParameter["FromJournal"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["ToJournal"] != null)
                {
                    sqlQuery += " AND V.je_number <=  '" + VMVServices.Web.Utils.ReportFilterParameter["ToJournal"].ToString() + "'";
                }
                if (acctCodeId.ToUpper() != "ALL")
                {
                    sqlQuery += " and v.acct_code_id='" + acctCodeId + "'";
                }
                if (segmentId.ToUpper() != "ALL")
                {
                    sqlQuery += " and v.je_segment_id_1='" + segmentId + "'";
                }
                if (unpost != "1")
                {
                    sqlQuery += " AND V.workflow_completion_status=1";
                }
            }

            return sqlQuery;
        }
        public static string GetProfitLossRPT()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT * FROM VW_PROFIT_LOSS V WHERE ROWNUM > 0 ";

            sqlQuery += " and v.je_comp_id='" + VMVServices.Web.Utils.OrganizationID + "'";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.FROM_DATE] != null)
                {
                    sqlQuery += " AND to_char(to_date(V.je_date,'dd/MM/yyyy')) between to_date('" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.FROM_DATE].ToString() + "','dd/MM/yyyy')";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.TO_DATE] != null)
                {
                    sqlQuery += " AND to_date('" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.TO_DATE].ToString() + "','dd/MM/yyyy')";
                }
            }

            return sqlQuery;
        }
        public static string getGroupnameinaccount()
        {
            sqlQuery = string.Empty;
            sqlQuery += " select Distinct ag.acct_grp_id,ag.acct_grp_desc" + VMVServices.Web.Utils.LanguageCode + " as Group_Name from gl_acct_codes ac ";
            sqlQuery += " inner join gl_acct_groups ag on ac.acct_grp_id=ag.acct_grp_id ";
            sqlQuery += " where ac.acct_org_id='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " order by Group_Name ";
            return sqlQuery;
        }

        public static string getAccountCodeStarts()
        {
            sqlQuery = string.Empty;
            sqlQuery += " select distinct substr(acct_code,0,1) acct_code_start from gl_acct_codes order by acct_code_start ";

            return sqlQuery;
        }
    }
}
