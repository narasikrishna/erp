﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.GL
{
    public class Currency_DAL
    {
        static string sqlQuery = "";

        public static string getCurrentyDesc()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT SC.CURRENCY_ID,  ";
            sqlQuery += " SC.CURRENCY_DESC AS CURRENCY_DESC ";
            sqlQuery += " from SSM_CURRENCIES SC ";
            sqlQuery += " WHERE SC.ENABLED_FLAG=1 ";
            sqlQuery += " AND SC.WORKFLOW_COMPLETION_STATUS=1 ";
            sqlQuery += " ORDER BY CURRENCY_DESC ";
            return sqlQuery;
        }


        public static string getCurrencyDetails()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT SC.CURRENCY_ID AS CURRENCY_ID,  TO_CHAR(SC.CURRENCY_CODE) || ' - ' || SC.CURRENCY_DESC AS CURRENCY_NAME  ";
            sqlQuery += " FROM SSM_CURRENCIES SC ";
            sqlQuery += " WHERE SC.ENABLED_FLAG = 1 ";
            sqlQuery += " AND SC.WORKFLOW_COMPLETION_STATUS = 1 ";
            //sqlQuery += " AND (Sc.EFFECTIVE_END_DT IS NULL OR";
            //sqlQuery += " (SYSDATE BETWEEN Sc.EFFECTIVE_START_DT AND Sc.EFFECTIVE_END_DT))";
            sqlQuery += " ORDER BY CURRENCY_NAME ";
            return sqlQuery;
        }

        public static string getCurrencyDetails(string currId)
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * ";
            sqlQuery += " FROM SSM_CURRENCIES SC ";
            sqlQuery += " WHERE SC.ENABLED_FLAG = 1 ";
            sqlQuery += " AND SC.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " and SC.currId'" + currId + "'";
            sqlQuery += " ORDER BY CURRENCY_ID ";
            return sqlQuery;
        }
        public static string getCurrencyBasedOrg(string orgId, string effectiveDate = "")
        {
            sqlQuery = string.Empty;

            sqlQuery = "  SELECT SC.CURRENCY_ID AS CURRENCY_ID,";
            sqlQuery += "     TO_CHAR(SC.CURRENCY_CODE) || ' - ' || SC.CURRENCY_DESC AS CURRENCY_NAME";
            sqlQuery += "   from gl_companies_dtl g, SSM_CURRENCIES sc";
            sqlQuery += "   where g.comp_base_currency = sc.currency_id";
            sqlQuery += "  AND SC.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += "  AND  SC.ENABLED_FLAG = 1 ";
            sqlQuery += "  and g.COMP_ID='" + orgId + "'";
            if (effectiveDate != string.Empty)
            {
                sqlQuery += "   and g.effective_start_dt <= to_date('" + effectiveDate + "', 'dd/MM/yyyy')";
                sqlQuery += "   and nvl(g.effective_end_dt, to_char(to_date(sysdate, 'dd/MON/yyyy'))) >=";
                sqlQuery += "  to_date('" + effectiveDate + "', 'dd/MM/yyyy')";
            }
            sqlQuery += "  ORDER BY sc.CURRENCY_ID ";
            return sqlQuery;
        }
        public static string GetPaymentCurrency(string effectiveDate = "")
        {
            sqlQuery = string.Empty;

            sqlQuery = "    select distinct c.currency_id,";
            sqlQuery += "    TO_CHAR(c.CURRENCY_CODE) || ' - ' || c.CURRENCY_DESC AS CURRENCY_NAME";
            sqlQuery += "   from inv_invoices_hdr i, ssm_currencies c";
            sqlQuery += "  where i.inv_pay_curr_code = c.currency_id";
            sqlQuery += "  and i.workflow_completion_status='1'";
            sqlQuery += "   and i.enabled_flag='1'";
            sqlQuery += "  and i.INV_ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += "  ORDER BY CURRENCY_NAME";
            return sqlQuery;
        }
        public static string GetARPaymentCurrency(string effectiveDate = "")
        {
            sqlQuery = string.Empty;

            sqlQuery = "   select distinct c.currency_id,";
            sqlQuery += "    TO_CHAR(c.CURRENCY_CODE) || ' - ' || c.CURRENCY_DESC AS CURRENCY_NAME";
            sqlQuery += "   from om_cust_invoice_hdr i, ssm_currencies c";
            sqlQuery += "   where i.om_inv_curr_code = c.currency_id";
            sqlQuery += "     and i.workflow_completion_status = '1'";
            sqlQuery += "     and i.enabled_flag = '1'";
            sqlQuery += "    and i.org_id ='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += "  ORDER BY CURRENCY_NAME";
            return sqlQuery;
        }

        public static string getCurrentyCode()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT SC.CURRENCY_ID,  ";
            sqlQuery += " SC.CURRENCY_CODE AS CURRENCY_CODE ";
            sqlQuery += " from SSM_CURRENCIES SC ";
            sqlQuery += " WHERE SC.ENABLED_FLAG=1 ";
            sqlQuery += " AND SC.WORKFLOW_COMPLETION_STATUS=1 ";
            sqlQuery += " ORDER BY CURRENCY_DESC ";
            return sqlQuery;
        }
    }
}
