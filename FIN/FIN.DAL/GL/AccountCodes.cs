﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.GL
{
    public class AccountCodes
    {
        static string sqlQuery = "";


        public static string getStructName()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT S.ACCT_STRUCT_ID,S.ACCT_STRUCT_NAME";
            sqlQuery += " FROM GL_ACCT_STRUCTURE S";
            sqlQuery += " WHERE S.ENABLED_FLAG = 1";
            sqlQuery += " AND S.WORKFLOW_COMPLETION_STATUS = 1";

            return sqlQuery;
        }

        public static string getGroupName()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT S.ACCT_GRP_ID,S.ACCT_GRP_DESC";
            sqlQuery += " FROM GL_ACCT_GROUPS S";
            sqlQuery += " WHERE S.ENABLED_FLAG = 1";
            sqlQuery += " AND S.WORKFLOW_COMPLETION_STATUS = 1";

            return sqlQuery;
        }

        public static string getSegment()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT S.SEGMENT_ID,S.SEGMENT_NAME";
            sqlQuery += " FROM GL_SEGMENTS S";
            sqlQuery += " WHERE S.ENABLED_FLAG = 1";
            sqlQuery += " AND S.WORKFLOW_COMPLETION_STATUS = 1";

            return sqlQuery;
        }


        public static string getSegmentDetails(string  str_AcctCodeID)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select S.ACCT_CODE_ID,S.ACCT_CODE_SEG_ID,S.ACCT_CODE_SEGMENT_ID,S.EFFECTIVE_START_DT,S.EFFECTIVE_END_DT,GS.SEGMENT_ID,'N' as DELETED";
            sqlQuery += " FROM GL_ACCT_CODE_SEGMENTS S,GL_SEGMENTS GS ";
            sqlQuery += " WHERE S.ENABLED_FLAG = 1";
            sqlQuery += " AND GS.SEGMENT_ID =  S.ACCT_CODE_SEGMENT_ID   ";
            sqlQuery += " AND S.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND S.ACCT_CODE_ID ='" + str_AcctCodeID +"'";

            return sqlQuery;
        }
        

    }
}
