﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;
using System.Data.EntityClient;
using System.Data.Entity.Infrastructure;


namespace FIN.DAL.GL
{
    public class JournalEntry_DAL
    {

        static string sqlQuery = "";


        public static string getStatementofAccountforgnCurr()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT * FROM vw_jr_ledger_system v where";
            sqlQuery += " v.JOURNAL_CURR_CODE not like 'KD%' ";


            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                //    //if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
                //    //{
                //    //    sqlQuery += " AND V.JE_DATE between to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy')";
                //    //}
                //    //if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
                //    //{
                //    //    sqlQuery += " AND to_date('" + VMVServices.Web.Utils.ReportFilterParameter["To_Date"].ToString() + "','dd/MM/yyyy')";
                //    //}

                //    if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.CODE_ID] != null)
                //    {
                //        sqlQuery += " AND je_acct_code_id ='" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.CODE_ID].ToString() + "'";
                //    }

                //    if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.SEGMENT_VALUE_ID] != null)
                //    {
                //        sqlQuery += " AND je_global_segment_id='" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.SEGMENT_VALUE_ID].ToString() + "'";
                //    }
                //    //if (VMVServices.Web.Utils.ReportViewFilterParameter["UNPOST"].ToString() == "0")
                //    //{
                //    //    sqlQuery += " AND workflow_completion_status=1 ";
                //    //}
                if (VMVServices.Web.Utils.ReportFilterParameter["FromAccount"] != null)
                {
                    sqlQuery += " AND V.ACCOUNT_CODE >=  '" + VMVServices.Web.Utils.ReportFilterParameter["FromAccount"].ToString() + "'";
                }

                if (VMVServices.Web.Utils.ReportFilterParameter["ToAccount"] != null)
                {
                    sqlQuery += " AND V.ACCOUNT_CODE <=  '" + VMVServices.Web.Utils.ReportFilterParameter["ToAccount"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["FromJournal"] != null)
                {
                    sqlQuery += " AND V.JOURNAL_NUMBER >=  '" + VMVServices.Web.Utils.ReportFilterParameter["FromJournal"].ToString() + "'";
                }

                if (VMVServices.Web.Utils.ReportFilterParameter["ToJournal"] != null)
                {
                    sqlQuery += " AND V.JOURNAL_NUMBER <=  '" + VMVServices.Web.Utils.ReportFilterParameter["ToJournal"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["From_Amount"] != null)
                {
                    sqlQuery += " AND V.JOURNAL_LINE_BALANCE >= '" + VMVServices.Web.Utils.ReportFilterParameter["From_Amount"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["To_Amount"] != null)
                {
                    sqlQuery += " AND V.JOURNAL_LINE_BALANCE <= '" + VMVServices.Web.Utils.ReportFilterParameter["To_Amount"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["WITHZERO"] == "FALSE")
                {
                    sqlQuery += " AND (V.JOURNAL_TRAN_CR >0 OR V.JOURNAL_TRAN_DR >0)  ";
                }
            }

            return sqlQuery;
        }

        public static string getOrganisationDetails()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT GCH.COMP_ID AS ORG_ID,(GCH.COMP_INTERNAL_NAME" + VMVServices.Web.Utils.LanguageCode + " || ' - ' || GCH.COMP_EXTERNAL_NAME|| ' - ' || GCH.COMP_SHORT_NAME) AS ORG_NAME ";
            sqlQuery += " FROM GL_COMPANIES_HDR GCH ";
            sqlQuery += " WHERE GCH.ENABLED_FLAG = 1 ";
            sqlQuery += " AND GCH.WORKFLOW_COMPLETION_STATUS = 1 ";
            // sqlQuery += " AND (GCH.EFFECTIVE_END_DT IS NULL OR";
            // sqlQuery += " (SYSDATE BETWEEN GCH.EFFECTIVE_START_DT AND GCH.EFFECTIVE_END_DT))";
            sqlQuery += " ORDER BY ORG_NAME ";

            return sqlQuery;
        }

        public static string getGlobalSegment()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT GS.SEGMENT_ID AS SEGMENT_ID, GS.SEGMENT_NAME" + VMVServices.Web.Utils.LanguageCode + " AS SEGMENT_NAME ";
            sqlQuery += " FROM GL_SEGMENTS GS ";
            sqlQuery += " WHERE GS.ENABLED_FLAG = 1 ";
            sqlQuery += " AND GS.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " ORDER BY SEGMENT_ID ";

            return sqlQuery;
        }


        public static string getJournalType()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT SLVD.CODE AS JOURNAL_ID, SLVD.CODE AS JOURNAL_NAME ";
            sqlQuery += " FROM ssm_code_masters SLVD ";
            sqlQuery += " WHERE SLVD.PARENT_CODE = 'JT'";
            sqlQuery += " ORDER BY SLVD.CHILD_ID ";

            return sqlQuery;
        }



        public static string getCurrencyDetails()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT SC.CURRENCY_ID AS CURRENCY_ID, TO_CHAR(SC.CURRENCY_CODE) || ' - ' || SC.CURRENCY_DESC AS CURRENCY_NAME ";
            sqlQuery += " FROM SSM_CURRENCIES SC ";
            sqlQuery += " WHERE SC.ENABLED_FLAG = 1 ";
            sqlQuery += " AND SC.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " ORDER BY SC.CURRENCY_CODE ";

            return sqlQuery;
        }

        public static string getSegmentDetails(string acct_code_id)
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT GSV.SEGMENT_VALUE_ID, GSV.SEGMENT_VALUE_ID ||' - '|| GSV.SEGMENT_VALUE" + VMVServices.Web.Utils.LanguageCode + " AS SEGMENT_VALUE ";
            sqlQuery += " from gl_segment_values gsv, gl_acct_code_segments gac";
            sqlQuery += " where gac.acct_code_segment_id = gsv.segment_id";
            sqlQuery += " and gac.acct_code_id ='" + acct_code_id + "'";
            sqlQuery += " AND gsv.enabled_flag = 1 ";
            sqlQuery += " ORDER BY gsv.segment_id ";

            return sqlQuery;
        }

        public static string getJEDetails(string Master_id)
        {
            sqlQuery = string.Empty;

            //sqlQuery = " select gjd.JE_SEGMENT_ID_1,JE_SEGMENT_ID_2,JE_SEGMENT_ID_3,JE_SEGMENT_ID_4,JE_SEGMENT_ID_5,JE_SEGMENT_ID_6, gjd.ATTRIBUTE1 as line_no,gac.acct_code_id,gac.acct_code, gac.acct_code as CODE_NAME, gac.acct_code_desc, gjd.pk_id, gjd.je_dtl_id, gjd.je_hdr_id, (case gjd.je_credit_debit when '1' then 'Cr' else 'Dr' end) as je_credit_debit,0 as JE_CONVERTED_AMT_dr_cr, gjd.je_amount as je_amount_dr,gjd.je_amount as je_amount_cr,gjd.JE_CONVERTED_AMT as JE_CONVERTED_AMT_cr,gjd.JE_CONVERTED_AMT as JE_CONVERTED_AMT_dr, 'N' as DELETED";
            //sqlQuery += " from gl_journal_dtl gjd, gl_acct_codes gac";
            //sqlQuery += " where gjd.je_acct_code_id = gac.acct_code_id";
            //sqlQuery += " and gjd.je_hdr_id = '" + Master_id + "'";
            //sqlQuery += " and gjd.workflow_completion_status = 1";
            //  sqlQuery += " and gjd.enabled_flag = 1";
            sqlQuery = "    select gjd.JE_SEGMENT_ID_1,";
            sqlQuery += "  gjd.JE_SEGMENT_ID_2,";
            sqlQuery += "  gjd.JE_SEGMENT_ID_3,";
            sqlQuery += "   gjd.JE_SEGMENT_ID_4,";
            sqlQuery += "   gjd.JE_SEGMENT_ID_5,";
            sqlQuery += "    gjd.JE_SEGMENT_ID_6,";
            sqlQuery += "    gjd.ATTRIBUTE1 as line_no,";
            sqlQuery += "    gac.acct_code_id,";
            sqlQuery += "    gac.acct_code,";
            sqlQuery += "    gac.acct_code as CODE_NAME,";
            sqlQuery += "   gac.acct_code ||' -' ||  gac.acct_code_desc" + VMVServices.Web.Utils.LanguageCode + " as acct_code_desc,";
            sqlQuery += "   gjd.pk_id,";
            sqlQuery += "   gjd.je_dtl_id,";
            sqlQuery += "   gjd.je_hdr_id,gjd.je_credit_debit,";
            sqlQuery += " gjd.JE_PARTICULARS,";

            //sqlQuery += "   (case gjd.je_credit_debit";
            //sqlQuery += "     when '1' then";
            //sqlQuery += "      'Cr'";
            //sqlQuery += "    else";
            //sqlQuery += "     'Dr'";
            //sqlQuery += "  end) as je_credit_debit,";

            sqlQuery += "   (case";
            sqlQuery += "   when nvl(JE_AMOUNT_DR, 0) = 0 then";
            sqlQuery += "    to_char(ROUND(JE_AMOUNT_CR," + VMVServices.Web.Utils.DecimalPrecision + "))";
            sqlQuery += "   else";
            sqlQuery += "    to_char(ROUND(JE_AMOUNT_DR," + VMVServices.Web.Utils.DecimalPrecision + ")) ";
            sqlQuery += "    end) as JE_AMOUNT_DR_CR, ";

            sqlQuery += "   (case";
            sqlQuery += "   when nvl(JE_ACCOUNTED_AMT_DR, 0) = 0 then";
            sqlQuery += "   to_char(ROUND(JE_ACCOUNTED_AMT_CR," + VMVServices.Web.Utils.DecimalPrecision + ")) ";
            sqlQuery += "   else";
            sqlQuery += "    to_char(ROUND(JE_ACCOUNTED_AMT_DR," + VMVServices.Web.Utils.DecimalPrecision + ")) ";
            sqlQuery += "   end) as JE_ACCOUNTED_AMT_CR_DR,";


            sqlQuery += "  to_char(ROUND(gjd.JE_AMOUNT_DR," + VMVServices.Web.Utils.DecimalPrecision + ")) as je_amount_dr,";
            sqlQuery += "  to_char(ROUND(gjd.JE_AMOUNT_CR," + VMVServices.Web.Utils.DecimalPrecision + ")) as je_amount_cr,";
            sqlQuery += "  to_char(ROUND(gjd.JE_ACCOUNTED_AMT_CR," + VMVServices.Web.Utils.DecimalPrecision + ")) as JE_ACCOUNTED_AMT_CR ,";
            sqlQuery += "  to_char(ROUND(gjd.JE_ACCOUNTED_AMT_DR," + VMVServices.Web.Utils.DecimalPrecision + ")) as JE_ACCOUNTED_AMT_DR,";
            sqlQuery += "  'N' as DELETED";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = gjd.JE_SEGMENT_ID_1 ) AS SEGMENT_1_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = gjd.JE_SEGMENT_ID_2 ) AS SEGMENT_2_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = gjd.JE_SEGMENT_ID_3 ) AS SEGMENT_3_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = gjd.JE_SEGMENT_ID_4 ) AS SEGMENT_4_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = gjd.JE_SEGMENT_ID_5 ) AS SEGMENT_5_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = gjd.JE_SEGMENT_ID_6 ) AS SEGMENT_6_TEXT";
            sqlQuery += "  from gl_journal_dtl gjd, gl_acct_codes gac";
            sqlQuery += "  where gjd.je_acct_code_id = gac.acct_code_id";
            sqlQuery += "   and gjd.je_hdr_id =  '" + Master_id + "'";
            sqlQuery += "  and gjd.workflow_completion_status = 1";
            sqlQuery += " order by gjd.JE_DTL_ID ";


            return sqlQuery;
        }

        public static string getSegmentValues()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT GSV.SEGMENT_VALUE_ID AS SEGMENT_VALUE_ID, GSV.SEGMENT_VALUE" + VMVServices.Web.Utils.LanguageCode + " AS SEGMENT_VALUE ";
            sqlQuery += " from gl_segment_values gsv, gl_acct_code_segments gac";
            sqlQuery += " where gac.acct_code_segment_id = gsv.segment_id";
            sqlQuery += " AND gsv.enabled_flag = 1 ";
            sqlQuery += " AND gsv.workflow_completion_status = 1 ";
            sqlQuery += " ORDER BY gsv.segment_id ";

            return sqlQuery;
        }
        public static string getReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery += " SELECT * FROM  VW_JOURNAL_VOUCHER1 V WHERE ROWNUM > 0 ";
            //if (VMVServices.Web.Utils.ReportFilterParameter != null || VMVServices.Web.Utils.ReportFilterParameter.Count != 0)
            //{
            //    if (VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.GROUP_ID] != null)
            //    {
            //        sqlQuery += " AND V.ACCT_GRP_ID = '" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.GROUP_ID].ToString() + "'";
            //    }

            //}

            return sqlQuery;
        }

        public static string UpdateJournal(string journalId)
        {
            sqlQuery = string.Empty;
            sqlQuery = " UPDATE gl_journal_hdr AC";
            sqlQuery += " SET AC.JOURNAL_REVERSED = 'Y'";
            sqlQuery += " WHERE AC.JE_HDR_ID ='" + journalId + "'";


            return sqlQuery;
        }
        public static string GetJournalVoucherDetailsRPT(string jeNumber, string str_JeHdrId = "")
        {
            sqlQuery = string.Empty;


            sqlQuery = "select * from GL_JOURNAL_VOUCHER V ";
            if (jeNumber.Trim().Length > 0)
            {
                sqlQuery += " where V.JE_NUMBER='" + jeNumber + "'";
            }
            else
            {
                sqlQuery += " where V.je_hdr_id='" + str_JeHdrId + "'";
            }
            return sqlQuery;
        }

        public static string getJournalLedgerSystem()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT * FROM vw_jr_ledger_system V WHERE ROWNUM >0 ";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
            //    if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
            //    {
            //        sqlQuery += " AND V.JE_DATE between to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy')";
                //    }
            //    if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
            //    {
            //        sqlQuery += " AND to_date('" + VMVServices.Web.Utils.ReportFilterParameter["To_Date"].ToString() + "','dd/MM/yyyy')";
            //    }

            //    if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.CODE_ID] != null)
            //    {
            //        sqlQuery += " AND je_acct_code_id ='" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.CODE_ID].ToString() + "'";
            //    }

            //    if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.SEGMENT_VALUE_ID] != null)
            //    {
            //        sqlQuery += " AND je_global_segment_id='" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.SEGMENT_VALUE_ID].ToString() + "'";
            //    }
            //    //if (VMVServices.Web.Utils.ReportViewFilterParameter["UNPOST"].ToString() == "0")
            //    //{
            //    //    sqlQuery += " AND workflow_completion_status=1 ";
            //    //}
                if (VMVServices.Web.Utils.ReportFilterParameter["FromAccount"] != null)
                {
                    sqlQuery += " AND V.ACCOUNT_CODE >=  '" + VMVServices.Web.Utils.ReportFilterParameter["FromAccount"].ToString() + "'";
                }

                if (VMVServices.Web.Utils.ReportFilterParameter["ToAccount"] != null)
                {
                    sqlQuery += " AND V.ACCOUNT_CODE <=  '" + VMVServices.Web.Utils.ReportFilterParameter["ToAccount"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["FromJournal"] != null)
                {
                    sqlQuery += " AND V.JOURNAL_NUMBER >=  '" + VMVServices.Web.Utils.ReportFilterParameter["FromJournal"].ToString() + "'";
                }

                if (VMVServices.Web.Utils.ReportFilterParameter["ToJournal"] != null)
                {
                    sqlQuery += " AND V.JOURNAL_NUMBER <=  '" + VMVServices.Web.Utils.ReportFilterParameter["ToJournal"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["From_Amount"] != null)
                {
                    sqlQuery += " AND V.JOURNAL_LINE_BALANCE >= '" + VMVServices.Web.Utils.ReportFilterParameter["From_Amount"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["To_Amount"] != null)
                {
                    sqlQuery += " AND V.JOURNAL_LINE_BALANCE <= '" + VMVServices.Web.Utils.ReportFilterParameter["To_Amount"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["Debit_From_Amt"] != null)
                {
                    sqlQuery += " and (v.JOURNAL_TRAN_DR) >= " + VMVServices.Web.Utils.ReportFilterParameter["Debit_From_Amt"];
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["Debit_To_Amt"] != null)
                {
                    sqlQuery += " and (v.JOURNAL_TRAN_DR) <= " + VMVServices.Web.Utils.ReportFilterParameter["Debit_To_Amt"];
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["Credit_From_Amt"] != null)
                {
                    sqlQuery += " and (v.JOURNAL_ACCT_CR) >= " + VMVServices.Web.Utils.ReportFilterParameter["Credit_From_Amt"];
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["Credit_To_Amt"] != null)
                {
                    sqlQuery += " and (v.JOURNAL_ACCT_CR) <= " + VMVServices.Web.Utils.ReportFilterParameter["Credit_To_Amt"];
                }

                //if (VMVServices.Web.Utils.ReportFilterParameter["WITHZERO"] == "FALSE")
                //{
                //    sqlQuery += " AND (V.JOURNAL_TRAN_CR >0 OR V.JOURNAL_TRAN_DR >0)  ";
                //}
            }


            return sqlQuery;
        }
        public static DataSet GetSP_Statement_of_accounts(string global_segment_id, DateTime from_Date, DateTime to_Date, string acctCode, string wfStatus, string str_Query)
        {
            try
            {
                string strValue = string.Empty;
                string strNextCode = string.Empty;
                string strErrMsg = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "GL_PKG.GL_Statement_of_accounts";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add(new OracleParameter("@p_org_id", OracleDbType.Varchar2, 250)).Value = VMVServices.Web.Utils.OrganizationID;
                oraCmd.Parameters.Add(new OracleParameter("@p_global_segment_id", OracleDbType.Varchar2, 250)).Value = global_segment_id;
                oraCmd.Parameters.Add(new OracleParameter("@p_from_date", OracleDbType.Date, 250)).Value = from_Date.ToString("dd/MMM/yyyy");
                oraCmd.Parameters.Add(new OracleParameter("@p_to_date", OracleDbType.Date, 250)).Value = to_Date.ToString("dd/MMM/yyyy");
                oraCmd.Parameters.Add(new OracleParameter("@p_acct_code", OracleDbType.Varchar2, 250)).Value = acctCode;
                oraCmd.Parameters.Add(new OracleParameter("@p_unposted", OracleDbType.Varchar2, 250)).Value = wfStatus;


                return DBMethod.ExecuteStoredProcedure(oraCmd, str_Query);



            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string getTempJournal(string str_userid)
        {
            sqlQuery = string.Empty;
            sqlQuery += " SELECT TJH.JE_HDR_ID,TJH.JE_DATE,TJH.JE_REFERENCE, PP.period_id,PP.period_name  FROM TMP_GL_JOURNAL_HDR TJH  ";
            sqlQuery += "  LEFT JOIN gl_comp_acct_period_dtl PP ON pp.period_id = TJH.JE_PERIOD_ID  ";
            sqlQuery += " WHERE TJH.CREATED_BY='" + str_userid + "'";
            return sqlQuery;

        }


        public static string get_TEMP_JEDetails(string JH_HDR_ID)
        {
            sqlQuery = string.Empty;


            sqlQuery = "    select gjd.JE_SEGMENT_ID_1,";
            sqlQuery += "  gjd.JE_SEGMENT_ID_2,";
            sqlQuery += "  gjd.JE_SEGMENT_ID_3,";
            sqlQuery += "   gjd.JE_SEGMENT_ID_4,";
            sqlQuery += "   gjd.JE_SEGMENT_ID_5,";
            sqlQuery += "    gjd.JE_SEGMENT_ID_6,";
            sqlQuery += "    gjd.ATTRIBUTE1 as line_no,";
            sqlQuery += "    gac.acct_code_id,";
            sqlQuery += "    gac.acct_code,";
            sqlQuery += "    gac.acct_code as CODE_NAME,";
            sqlQuery += "   gac.acct_code ||' -' ||  gac.acct_code_desc" + VMVServices.Web.Utils.LanguageCode + " as ACCT_CODE_DESC,";
            sqlQuery += "   gjd.pk_id,";
            sqlQuery += "   0 as je_dtl_id,";
            sqlQuery += "   gjd.je_hdr_id,gjd.je_credit_debit,";
            sqlQuery += " gjd.JE_PARTICULARS,";

            sqlQuery += "   (case";
            sqlQuery += "   when nvl(JE_AMOUNT_DR, 0) = 0 then";
            sqlQuery += "    to_char(ROUND(JE_AMOUNT_CR," + VMVServices.Web.Utils.DecimalPrecision + "))";
            sqlQuery += "   else";
            sqlQuery += "    to_char(ROUND(JE_AMOUNT_DR," + VMVServices.Web.Utils.DecimalPrecision + ")) ";
            sqlQuery += "    end) as JE_AMOUNT_DR_CR, ";

            sqlQuery += "   (case";
            sqlQuery += "   when nvl(JE_ACCOUNTED_AMT_DR, 0) = 0 then";
            sqlQuery += "   to_char(ROUND(JE_ACCOUNTED_AMT_CR," + VMVServices.Web.Utils.DecimalPrecision + ")) ";
            sqlQuery += "   else";
            sqlQuery += "    to_char(ROUND(JE_ACCOUNTED_AMT_DR," + VMVServices.Web.Utils.DecimalPrecision + ")) ";
            sqlQuery += "   end) as JE_ACCOUNTED_AMT_CR_DR,";


            sqlQuery += "  to_char(ROUND(gjd.JE_AMOUNT_DR," + VMVServices.Web.Utils.DecimalPrecision + ")) as je_amount_dr,";
            sqlQuery += "  to_char(ROUND(gjd.JE_AMOUNT_CR," + VMVServices.Web.Utils.DecimalPrecision + ")) as je_amount_cr,";
            sqlQuery += "  to_char(ROUND(gjd.JE_ACCOUNTED_AMT_CR," + VMVServices.Web.Utils.DecimalPrecision + ")) as JE_ACCOUNTED_AMT_CR ,";
            sqlQuery += "  to_char(ROUND(gjd.JE_ACCOUNTED_AMT_DR," + VMVServices.Web.Utils.DecimalPrecision + ")) as JE_ACCOUNTED_AMT_DR,";
            sqlQuery += "  'N' as DELETED";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = gjd.JE_SEGMENT_ID_1 ) AS SEGMENT_1_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = gjd.JE_SEGMENT_ID_2 ) AS SEGMENT_2_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = gjd.JE_SEGMENT_ID_3 ) AS SEGMENT_3_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = gjd.JE_SEGMENT_ID_4 ) AS SEGMENT_4_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = gjd.JE_SEGMENT_ID_5 ) AS SEGMENT_5_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = gjd.JE_SEGMENT_ID_6 ) AS SEGMENT_6_TEXT";
            sqlQuery += "  from tmp_gl_journal_dtl gjd, gl_acct_codes gac";
            sqlQuery += "  where gjd.je_acct_code_id = gac.acct_code_id";
            sqlQuery += "   and gjd.je_hdr_id =  '" + JH_HDR_ID + "'";
            sqlQuery += "  and gjd.workflow_completion_status = 1";

            return sqlQuery;
        }

        public static string getDistinctJEReference(string str_fromdate, string str_todate)
        {
            sqlQuery = string.Empty;
            sqlQuery = "select  distinct  JH.je_REFERENCE from gl_journal_hdr JH where JH.jE_COMP_ID='" + VMVServices.Web.Utils.OrganizationID + "' ";
            if (str_fromdate.Length > 0)
            {
                sqlQuery += " AND JH.JE_DATE >= to_date('" + str_fromdate + "','dd/MM/yyyy') ";

            }
            if (str_todate.Length > 0)
            {
                sqlQuery += " AND JH.JE_DATE <= to_date('" + str_todate + "','dd/MM/yyyy') ";
            }
            
            return sqlQuery;
        }
        public static string getJournalNumber(string str_fromdate, string str_todate)
        {
            sqlQuery = string.Empty;
            sqlQuery = "select  distinct  JH.je_number from gl_journal_hdr JH where JH.jE_COMP_ID='" + VMVServices.Web.Utils.OrganizationID + "' ";
            if (str_fromdate.Length > 0)
            {
                sqlQuery += " AND JH.JE_DATE >= to_date('" + str_fromdate + "','dd/MM/yyyy') ";

            }
            if (str_todate.Length > 0)
            {
                sqlQuery += " AND JH.JE_DATE <= to_date('" + str_todate + "','dd/MM/yyyy') ";
            }
            sqlQuery += " order by JH.je_number ";
            return sqlQuery;
        }

        public static string getDayBookDetails()
        {
            sqlQuery = string.Empty;
            sqlQuery += " SELECT * FROM  VW_DAYBOOK_DETAILS V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportFilterParameter != null || VMVServices.Web.Utils.ReportFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportFilterParameter["ACCOUNT_GROUP"] != null)
                {
                    sqlQuery += " AND V.ACCT_GRP_ID = '" + VMVServices.Web.Utils.ReportFilterParameter["ACCOUNT_GROUP"].ToString() + "'";
                }

                if (VMVServices.Web.Utils.ReportFilterParameter["ACCOUNT_CODE"] != null)
                {
                    sqlQuery += " AND V.acct_code_id = '" + VMVServices.Web.Utils.ReportFilterParameter["ACCOUNT_CODE"].ToString() + "'";
                }

                if (VMVServices.Web.Utils.ReportFilterParameter["JOURNAL_TYPE"] != null)
                {
                    sqlQuery += " AND V.JE_TYPE = '" + VMVServices.Web.Utils.ReportFilterParameter["JOURNAL_TYPE"].ToString() + "'";
                }

                if (VMVServices.Web.Utils.ReportFilterParameter["FromAccount"] != null)
                {
                    sqlQuery += " AND V.ACCT_CODE >= '" + VMVServices.Web.Utils.ReportFilterParameter["FromAccount"].ToString() + "'";
                }

                if (VMVServices.Web.Utils.ReportFilterParameter["ToAccount"] != null)
                {
                    sqlQuery += " AND V.ACCT_CODE <= '" + VMVServices.Web.Utils.ReportFilterParameter["ToAccount"].ToString() + "'";
                }


                if (VMVServices.Web.Utils.ReportFilterParameter["JOURNAL_REF"] != null)
                {
                    sqlQuery += " AND V.JE_REFERENCE = '" + VMVServices.Web.Utils.ReportFilterParameter["JOURNAL_REF"].ToString() + "'";
                }

                if (VMVServices.Web.Utils.ReportFilterParameter["SEGMENT"] != null)
                {
                    sqlQuery += " AND V.JE_SEGMENT_ID_1 = '" + VMVServices.Web.Utils.ReportFilterParameter["SEGMENT"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["FromJournal"] != null)
                {
                    sqlQuery += " AND V.JE_NUMBER >=  '" + VMVServices.Web.Utils.ReportFilterParameter["FromJournal"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["ToJournal"] != null)
                {
                    sqlQuery += " AND V.JE_NUMBER <=  '" + VMVServices.Web.Utils.ReportFilterParameter["ToJournal"].ToString() + "'";
                }

                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
                {
                    sqlQuery += " AND V.JE_DATE between to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy')";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
                {
                    sqlQuery += " AND to_date('" + VMVServices.Web.Utils.ReportFilterParameter["To_Date"].ToString() + "','dd/MM/yyyy')";
                }

                if (VMVServices.Web.Utils.ReportViewFilterParameter["UNPOST"]!= null )
                {
                    sqlQuery += " AND WORKFLOW_COMPLETION_STATUS IN ( " + VMVServices.Web.Utils.ReportViewFilterParameter["UNPOST"].ToString()+" ) ";
                }
                

                if (VMVServices.Web.Utils.ReportViewFilterParameter["FROMAMOUNT"] != null)
                {
                    sqlQuery += " AND (V.je_accounted_amt_dr+V.je_accounted_amt_cr) >=" + VMVServices.Web.Utils.ReportViewFilterParameter["FROMAMOUNT"].ToString();
                }

                if (VMVServices.Web.Utils.ReportViewFilterParameter["TOAMOUNT"] != null)
                {
                    sqlQuery += " AND (V.je_accounted_amt_dr+V.je_accounted_amt_cr) <=" + VMVServices.Web.Utils.ReportViewFilterParameter["TOAMOUNT"].ToString();
                }

                if (VMVServices.Web.Utils.ReportFilterParameter["Journal_Source"] != null)
                {
                    sqlQuery += " AND V.JE_SOURCE = '" + VMVServices.Web.Utils.ReportFilterParameter["Journal_Source"].ToString() + "'";
                }
                
            }

            return sqlQuery;
        }
    }
}
