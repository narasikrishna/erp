﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.GL
{
    public class Balances_DAL
    {
        static string sqlQuery = "";

        public static string getBIGlCashBalance()
        {
            sqlQuery = string.Empty;

            sqlQuery += "    select gac.acct_code_id,gag.ACCT_FUND_TRANSFER,";
            sqlQuery += "   gac.acct_code as ACCOUNT_NO,";
            sqlQuery += "   gac.acct_code_desc as ACCOUNT_NAME,";
            sqlQuery += "   gac.acct_code_desc_ol as ACCOUNT_NAME_OL,";
            sqlQuery += "   (round((nvl(cbd.amount_dr, 0)-nvl(cbd.amount_cr, 0)),3)) as BALANCE_AMT,";
            sqlQuery += "   gac.acct_grp_id,    ";
            sqlQuery += "   cbd.period_day,";
            sqlQuery += "    gac.acct_org_id as org_id,";
            sqlQuery += "    cbd.workflow_completion_status";
            sqlQuery += "   from gl_acct_groups gag, gl_acct_codes gac, gl_current_balances_day cbd,ssm_system_options ss";
            sqlQuery += "   where gag.acct_grp_id = gac.acct_grp_id";
            sqlQuery += "    and gac.acct_code = cbd.acct_code";
            sqlQuery += "   and ss.ap_default_cash_acct_code=gac.acct_code_id";
            // sqlQuery += "    and gag.ACCT_FUND_TRANSFER = 1";

            return sqlQuery;
        }
        public static string getPeriod()
        {
            sqlQuery = string.Empty;


            sqlQuery = " SELECT DISTINCT P.PERIOD_NAME,P.PERIOD_NAME";
            sqlQuery += " FROM GL_ACCT_PERIOD_DTL P";
            sqlQuery += " WHERE P.ENABLED_FLAG = 1";
            sqlQuery += " AND P.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " order by P.PERIOD_NAME asc";
            return sqlQuery;
        }


        public static string getSegment()
        {
            sqlQuery = string.Empty;


            sqlQuery = " SELECT S.SEGMENT_VALUE,S.SEGMENT_VALUE" + VMVServices.Web.Utils.LanguageCode;
            sqlQuery += " FROM GL_SEGMENT_VALUES S";
            sqlQuery += " WHERE S.ENABLED_FLAG = 1";
            sqlQuery += " AND S.WORKFLOW_COMPLETION_STATUS = 1";
            return sqlQuery;
        }

        public static string getBeginBalDtl_UP(string Period_Name, string segment_value, String Je_Comp_Id, String ACCT_CODE)
        {
            sqlQuery = string.Empty;


            sqlQuery = " SELECT B.Acct_Code,b.ACCT_CODE_DESC";
            sqlQuery += " ,to_char(ROUND(B.Begin_Balance_Acct_Cr," + VMVServices.Web.Utils.DecimalPrecision + ")) AS Begin_Balance_Acct_Cr ";
            sqlQuery += " ,to_char(ROUND(B.Begin_Balance_Acct_Dr," + VMVServices.Web.Utils.DecimalPrecision + ")) AS Begin_Balance_Acct_Dr ";
            sqlQuery += " ,to_char(ROUND(B.Accounted_Cr," + VMVServices.Web.Utils.DecimalPrecision + ")) AS Accounted_Cr ";
            sqlQuery += " ,to_char(ROUND(B.Accounted_Dr," + VMVServices.Web.Utils.DecimalPrecision + ")) AS Accounted_Dr ";
            sqlQuery += " ,to_char(ROUND(B.Closing_Balance_Acct_Cr," + VMVServices.Web.Utils.DecimalPrecision + ")) AS Closing_Balance_Acct_Cr ";
            sqlQuery += " ,to_char(ROUND(B.Closing_Balance_Acct_Dr," + VMVServices.Web.Utils.DecimalPrecision + ")) AS Closing_Balance_Acct_Dr ";
            //sqlQuery += " ,B.currency_code ";
            sqlQuery += " FROM GL_CURRENT_BALANCES_BC_UP B";
            sqlQuery += " WHERE B.Period_Name = '" + Period_Name + "'";
            sqlQuery += " AND B.global_segment_id = '" + segment_value + "'";
            sqlQuery += " AND B.Je_Comp_Id = '" + Je_Comp_Id + "'";
            if (ACCT_CODE != "")
            {
                sqlQuery += " AND B.ACCT_CODE_ID = '" + ACCT_CODE + "'";
            }
            return sqlQuery;
        }
        public static string getBeginBalDtl(string Period_Name, string segment_value, String Je_Comp_Id, String ACCT_CODE)
        {
            sqlQuery = string.Empty;


            sqlQuery = " SELECT B.Acct_Code,b.ACCT_CODE_DESC,";
            sqlQuery += "     to_char(sum(ROUND(nvl(B.Begin_Balance_Acct_Cr,0)," + VMVServices.Web.Utils.DecimalPrecision + "))) AS Begin_Balance_Acct_Cr,";
            sqlQuery += "   to_char(sum(ROUND(nvl(B.Begin_Balance_Acct_Dr,0), " + VMVServices.Web.Utils.DecimalPrecision + "))) AS Begin_Balance_Acct_Dr,";
            sqlQuery += "   to_char(sum(ROUND(nvl(B.Accounted_Cr,0), " + VMVServices.Web.Utils.DecimalPrecision + "))) AS Accounted_Cr,";
            sqlQuery += "   to_char(sum(ROUND(nvl(B.Accounted_Dr,0)," + VMVServices.Web.Utils.DecimalPrecision + "))) AS Accounted_Dr,";
            sqlQuery += "    to_char(sum(ROUND(nvl(B.Closing_Balance_Acct_Cr,0), " + VMVServices.Web.Utils.DecimalPrecision + "))) AS Closing_Balance_Acct_Cr,";
            sqlQuery += "   to_char(sum(ROUND(nvl(B.Closing_Balance_Acct_Dr,0), " + VMVServices.Web.Utils.DecimalPrecision + "))) AS Closing_Balance_Acct_Dr";


            //sqlQuery += " ,to_char(ROUND(B.Begin_Balance_Acct_Cr," + VMVServices.Web.Utils.DecimalPrecision + ")) AS Begin_Balance_Acct_Cr ";
            //sqlQuery += " ,to_char(ROUND(B.Begin_Balance_Acct_Dr," + VMVServices.Web.Utils.DecimalPrecision + ")) AS Begin_Balance_Acct_Dr ";
            //sqlQuery += " ,to_char(ROUND(B.Accounted_Cr," + VMVServices.Web.Utils.DecimalPrecision + ")) AS Accounted_Cr ";
            //sqlQuery += " ,to_char(ROUND(B.Accounted_Dr," + VMVServices.Web.Utils.DecimalPrecision + ")) AS Accounted_Dr ";
            //sqlQuery += " ,to_char(ROUND(B.Closing_Balance_Acct_Cr," + VMVServices.Web.Utils.DecimalPrecision + ")) AS Closing_Balance_Acct_Cr ";
            //sqlQuery += " ,to_char(ROUND(B.Closing_Balance_Acct_Dr," + VMVServices.Web.Utils.DecimalPrecision + ")) AS Closing_Balance_Acct_Dr ";
            //sqlQuery += " ,B.currency_code ";
            sqlQuery += " FROM GL_CURRENT_BALANCES_BC B";
            sqlQuery += " WHERE B.Period_Name = '" + Period_Name + "'";
            sqlQuery += " AND B.global_segment_id = '" + segment_value + "'";
            sqlQuery += " AND B.Je_Comp_Id = '" + Je_Comp_Id + "'";
            if (ACCT_CODE != "")
            {
                sqlQuery += " AND B.ACCT_CODE_ID = '" + ACCT_CODE + "'";
            }
            sqlQuery += "  group by B.Acct_Code,b.ACCT_CODE_DESC";
            return sqlQuery;
        }

        public static string getAcctBeginBal_UP(string Acct_Code, string JE_COMP_ID, string PERIOD_NAME, string SEGMENT_VALUE)
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT ab.Acct_Code,ab.currency_code";
            sqlQuery += " ,to_char(ROUND(AB.Begin_Balance_Cr," + VMVServices.Web.Utils.DecimalPrecision + ")) AS Begin_Balance_Cr ";
            sqlQuery += " ,to_char(ROUND(AB.Begin_Balance_Dr," + VMVServices.Web.Utils.DecimalPrecision + ")) AS Begin_Balance_Dr ";
            sqlQuery += " ,to_char(ROUND(AB.Amount_Cr," + VMVServices.Web.Utils.DecimalPrecision + ")) AS Amount_Cr ";
            sqlQuery += " ,to_char(ROUND(AB.Amount_Dr," + VMVServices.Web.Utils.DecimalPrecision + ")) AS Amount_Dr ";
            sqlQuery += " ,to_char(ROUND(AB.Closing_Balance_Cr," + VMVServices.Web.Utils.DecimalPrecision + ")) AS Closing_Balance_Cr ";
            sqlQuery += " ,to_char(ROUND(AB.Closing_Balance_Dr," + VMVServices.Web.Utils.DecimalPrecision + ")) AS Closing_Balance_Dr ";

            sqlQuery += " FROM GL_CURRENT_BALANCES_UP AB";
            sqlQuery += " WHERE AB.Acct_Code = '" + Acct_Code + "'";
            //sqlQuery += " AND AB.currency_code = '" + currency_code + "'";
            sqlQuery += " AND AB.JE_COMP_ID = '" + JE_COMP_ID + "'";
            sqlQuery += " AND AB.PERIOD_NAME = '" + PERIOD_NAME + "'";
            sqlQuery += " AND AB.GLOBAL_SEGMENT = '" + SEGMENT_VALUE + "'";
            return sqlQuery;
        }

        public static string getAcctBeginBal(string Acct_Code, string JE_COMP_ID, string PERIOD_NAME, string SEGMENT_VALUE)
        {
            sqlQuery = string.Empty;

            sqlQuery = "  SELECT ab.Acct_Code,ab.currency_code";

            sqlQuery += " ,to_char(ROUND(AB.Begin_Balance_Cr," + VMVServices.Web.Utils.DecimalPrecision + ")) AS Begin_Balance_Cr ";
            sqlQuery += " ,to_char(ROUND(AB.Begin_Balance_Dr," + VMVServices.Web.Utils.DecimalPrecision + ")) AS Begin_Balance_Dr ";
            sqlQuery += " ,to_char(ROUND(AB.Amount_Cr," + VMVServices.Web.Utils.DecimalPrecision + ")) AS Amount_Cr ";
            sqlQuery += " ,to_char(ROUND(AB.Amount_Dr," + VMVServices.Web.Utils.DecimalPrecision + ")) AS Amount_Dr ";
            sqlQuery += " ,to_char(ROUND(AB.Closing_Balance_Cr," + VMVServices.Web.Utils.DecimalPrecision + ")) AS Closing_Balance_Cr ";
            sqlQuery += " ,to_char(ROUND(AB.Closing_Balance_Dr," + VMVServices.Web.Utils.DecimalPrecision + ")) AS Closing_Balance_Dr ";

            sqlQuery += " FROM GL_CURRENT_BALANCES AB";
            sqlQuery += " WHERE AB.Acct_Code = '" + Acct_Code + "'";
            //sqlQuery += " AND AB.currency_code = '" + currency_code + "'";
            sqlQuery += " AND AB.JE_COMP_ID = '" + JE_COMP_ID + "'";
            sqlQuery += " AND AB.PERIOD_NAME = '" + PERIOD_NAME + "'";
            sqlQuery += " AND AB.GLOBAL_SEGMENT = '" + SEGMENT_VALUE + "'";
            //sqlQuery += "    group by b.ACCT_CODE_ID,b.ACCT_CODE_DESC";
            return sqlQuery;
        }


        public static string getAcctBal_Currencywise(string Acct_Code, string currency_code, string JE_COMP_ID, string PERIOD_NAME, string SEGMENT_VALUE, string str_unposted)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select bd.Acct_Code,trunc(bd.period_day) as period_day,bd.CURRENCY_CODE";
            sqlQuery += " ,to_char(ROUND(bd.amount_cr," + VMVServices.Web.Utils.DecimalPrecision + ")) AS amount_cr ";
            sqlQuery += " ,to_char(ROUND(bd.amount_dr," + VMVServices.Web.Utils.DecimalPrecision + ")) AS amount_dr ";
            sqlQuery += " ,to_char(ROUND(bd.accounted_cr," + VMVServices.Web.Utils.DecimalPrecision + ")) AS accounted_cr ";
            sqlQuery += " ,to_char(ROUND(bd.accounted_dr," + VMVServices.Web.Utils.DecimalPrecision + ")) AS accounted_dr ";
            sqlQuery += "  from gl_current_balances_day bd";
            sqlQuery += " where bd.acct_code = '" + Acct_Code + "'";
            sqlQuery += " and bd.currency_code = '" + currency_code + "'";
            sqlQuery += " and bd.je_comp_id = '" + JE_COMP_ID + "'";
            sqlQuery += " and bd.period_name = '" + PERIOD_NAME + "'";
            sqlQuery += " and bd.global_segment =  '" + SEGMENT_VALUE + "'";
            if (str_unposted == "1")
            {
                sqlQuery += " and bd.workflow_completion_status in ('0','1')";
            }
            else
            {
                sqlQuery += " and bd.workflow_completion_status in ('1')";
            }


            return sqlQuery;
        }

        public static string getAcctBal_Daywise(string date, string Acct_Code, string currency_code, string JE_COMP_ID, string PERIOD_NAME, string SEGMENT_VALUE, string str_Unposted)
        {
            sqlQuery = string.Empty;



            sqlQuery = " select jh.JE_HDR_ID, jh.je_number, jd.je_credit_debit";
            sqlQuery += " ,to_char(ROUND(jd.je_amount_cr," + VMVServices.Web.Utils.DecimalPrecision + ")) AS je_amount_cr ";
            sqlQuery += " ,to_char(ROUND(jd.je_amount_dr," + VMVServices.Web.Utils.DecimalPrecision + ")) AS je_amount_dr ";
            sqlQuery += " ,to_char(ROUND(jd.je_accounted_amt_cr," + VMVServices.Web.Utils.DecimalPrecision + ")) AS je_accounted_amt_cr ";
            sqlQuery += " ,to_char(ROUND(jd.je_accounted_amt_dr," + VMVServices.Web.Utils.DecimalPrecision + ")) AS je_accounted_amt_dr ";
            sqlQuery += " from gl_journal_hdr jh,gl_journal_dtl jd,SSM_CURRENCIES c,GL_ACCT_CODES ac";
            sqlQuery += " where jh.je_hdr_id = jd.je_hdr_id";
            sqlQuery += " and jh.je_currency_id = c.currency_id";
            sqlQuery += " and jd.je_acct_code_id = ac.acct_code_id";
            //sqlQuery += " and jh.je_date = to_date('" + date + "','dd/MM/yyyy')";
            sqlQuery += " and jh.je_date = (to_date('" + date + "','dd-MM-yyyy'))";
            sqlQuery += " and ac.acct_code = '" + Acct_Code + "'";
            sqlQuery += " and c.currency_code = '" + currency_code + "'";
            sqlQuery += " and jh.je_comp_id = '" + JE_COMP_ID + "'";
            sqlQuery += " and jh.je_period_id = '" + PERIOD_NAME + "'";
            sqlQuery += " and jh.je_global_segment_id = '" + SEGMENT_VALUE + "'";
            if (str_Unposted == "1")
            {
                sqlQuery += " and jh.workflow_completion_status in ('0','1')";
            }
            else
            {
                sqlQuery += " and jh.workflow_completion_status in ('1')";
            }


            return sqlQuery;
        }

        public static string getBankBalance()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT * FROM vw_ledger_bank_balances V WHERE ROWNUM > 0 ";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.PERIOD_ID] != null)
                {
                    sqlQuery += " AND V.PERIOD_ID='" + VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.PERIOD_ID].ToString() + "'";
                }

                if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.SEGMENT_VALUE_ID] != null)
                {
                    sqlQuery += " AND V.GLOBAL_SEGMENT_ID='" + VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.SEGMENT_VALUE_ID].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["BANK_NAME"] != null)
                {
                    sqlQuery += " AND V.BANK_NAME='" + VMVServices.Web.Utils.ReportViewFilterParameter["BANK_NAME"].ToString() + "'";
                }
                //if (VMVServices.Web.Utils.ReportViewFilterParameter["UNPOST"] != "1")
                //{
                //    sqlQuery += " AND workflow_completion_status='1'";
                //}
                //if (VMVServices.Web.Utils.ReportFilterParameter["WITHZERO"] == "FALSE")
                //{
                //    sqlQuery += " AND (V.BALANCE_AMT >0 OR V.BALANCE_AMT >0)  ";
                //}
            }

            return sqlQuery;
        }
        public static string getGlBankBalance()
        {
            sqlQuery = string.Empty;

            sqlQuery += "   select gac.acct_code_id,";
            sqlQuery += "  gac.acct_code as ACCOUNT_NO,";
            sqlQuery += "   gac.acct_code_desc as ACCOUNT_NAME,";
            sqlQuery += "   gac.acct_code_desc_ol as ACCOUNT_NAME_OL,";
            sqlQuery += "  round((nvl(cbd.amount_dr, 0)-nvl(cbd.amount_cr, 0)),3) as BALANCE_AMT,";
            sqlQuery += "  gac.acct_grp_id,";
            sqlQuery += "  bb.bank_name AS BANK_NAME,";
            sqlQuery += "  cbd.period_day,";
            sqlQuery += "   gac.acct_org_id as org_id,";
            sqlQuery += "   cbd.workflow_completion_status";
            sqlQuery += "  from gl_acct_groups gag, gl_acct_codes gac, gl_current_balances_day cbd,ca_bank_accounts ab,ca_bank bb";
            sqlQuery += "  where gag.acct_grp_id = gac.acct_grp_id";
            sqlQuery += "  and gac.acct_code = cbd.acct_code";
            sqlQuery += "  and ab.gl_account=gac.acct_code_id";
            sqlQuery += "  and bb.bank_id=ab.bank_id";
            // sqlQuery += " and gag.ACCT_FUND_TRANSFER = 1";
            return sqlQuery;
        }
        public static string getGlCashBalance()
        {
            sqlQuery = string.Empty;

            sqlQuery += "    select gac.acct_code_id,gag.ACCT_FUND_TRANSFER,";
            sqlQuery += "   gac.acct_code as ACCOUNT_NO,";
            sqlQuery += "   gac.acct_code_desc as ACCOUNT_NAME,";
            sqlQuery += "   gac.acct_code_desc_ol as ACCOUNT_NAME_OL,";
            sqlQuery += "   to_char(round((nvl(cbd.amount_dr, 0)-nvl(cbd.amount_cr, 0)),3)) as BALANCE_AMT,";
            sqlQuery += "   gac.acct_grp_id,    ";
            sqlQuery += "   cbd.period_day,";
            sqlQuery += "    gac.acct_org_id as org_id,";
            sqlQuery += "    cbd.workflow_completion_status";
            sqlQuery += "   from gl_acct_groups gag, gl_acct_codes gac, gl_current_balances_day cbd,ssm_system_options ss";
            sqlQuery += "   where gag.acct_grp_id = gac.acct_grp_id";
            sqlQuery += "    and gac.acct_code = cbd.acct_code";
            sqlQuery += "   and ss.ap_default_cash_acct_code=gac.acct_code_id";
            // sqlQuery += "    and gag.ACCT_FUND_TRANSFER = 1";

            return sqlQuery;
        }
    }
}
