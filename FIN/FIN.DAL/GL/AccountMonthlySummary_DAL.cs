﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;

using System.Data.EntityClient;



using System.Data.Entity.Infrastructure;



namespace FIN.DAL.GL
{
    public class AccountMonthlySummary_DAL
    {
        static string sqlQuery = "";


        public static DataTable getGLTrialBalanceYearlyDetails(string globalSegmentID, string fromDate, string toDate, string fromAcct, string toAcct, decimal withZero, string unPosted)
        {
            string strValue = string.Empty;
            string strNextCode = string.Empty;
            string strErrMsg = string.Empty;

            OracleCommand oraCmd = new OracleCommand();
            oraCmd.CommandText = "GL_REPORTS.TB_YEARLY_DTLS";
            oraCmd.CommandType = CommandType.StoredProcedure;
            oraCmd.Parameters.Add("@P_GLOBAL_SEGMENT", OracleDbType.Varchar2).Value = globalSegmentID;
            oraCmd.Parameters.Add("@P_FROM_DATE", OracleDbType.Varchar2).Value = fromDate;
            oraCmd.Parameters.Add("@P_TO_DATE", OracleDbType.Varchar2).Value = toDate;
            oraCmd.Parameters.Add("@P_ORG_ID", OracleDbType.Varchar2).Value = VMVServices.Web.Utils.OrganizationID;
            if (fromAcct.Trim().Length > 0)
            {
                oraCmd.Parameters.Add("@P_FROM_ACCT", OracleDbType.Varchar2).Value = fromAcct;
            }
            else
            {
                oraCmd.Parameters.Add(new OracleParameter("@P_FROM_ACCT", OracleDbType.Varchar2, 250)).Value = "NOACCT";
            }
            if (toAcct.Trim().Length > 0)
            {
                oraCmd.Parameters.Add("@P_TO_ACCT", OracleDbType.Varchar2).Value = toAcct;
            }
            else
            {
                oraCmd.Parameters.Add(new OracleParameter("@P_TO_ACCT", OracleDbType.Varchar2, 250)).Value = "NOACCT";
            }

            oraCmd.Parameters.Add(new OracleParameter("@P_WITH_ZERO", OracleDbType.Int32, 250)).Value = withZero;

            if (unPosted.Trim().Length > 0)
            {
                oraCmd.Parameters.Add(new OracleParameter("@P_UNPOSTED", OracleDbType.Varchar2, 100)).Value = unPosted;
            }
            else
            {
                oraCmd.Parameters.Add(new OracleParameter("@P_UNPOSTED", OracleDbType.Varchar2, 100)).Value = null;
            }
            
            oraCmd.Parameters.Add("@P_DATA", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            return DBMethod.ExecuteStoredProcedure4CursorOutput(oraCmd);
        }


        public static string getFinancialAcctGroupSummary(string qtrValue, string post)
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM gl_je_summary_period V, gl_acct_period_dtl g  WHERE ROWNUM > 0 and v.period_id=g.period_id ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportFilterParameter["SEGMENT_ID"] != null)
                {
                    sqlQuery += " AND V.GLOBAL_SEGMENT_ID = '" + VMVServices.Web.Utils.ReportFilterParameter["SEGMENT_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["CAL_DTL_ID"] != null)
                {
                    sqlQuery += " AND V.CAL_DTL_ID = '" + VMVServices.Web.Utils.ReportFilterParameter["CAL_DTL_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["ACCOUNTGROUP"] != null)
                {
                    sqlQuery += " AND V.GROUP_ID = '" + VMVServices.Web.Utils.ReportFilterParameter["ACCOUNTGROUP"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["ACCOUNT_START"] != null)
                {
                    sqlQuery += " AND V.ACCT_CODE like ( '" + VMVServices.Web.Utils.ReportViewFilterParameter["ACCOUNT_START"].ToString() + "%')";
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["FromAccount"] != null)
                {
                    sqlQuery += " AND V.ACCT_CODE >=  '" + VMVServices.Web.Utils.ReportFilterParameter["FromAccount"].ToString() + "'";
                }

                if (VMVServices.Web.Utils.ReportFilterParameter["ToAccount"] != null)
                {
                    sqlQuery += " AND V.ACCT_CODE <=  '" + VMVServices.Web.Utils.ReportFilterParameter["ToAccount"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["From_Amt"] != null)
                {
                    sqlQuery += " and (v.CB_AMOUNT_DR - v.CB_AMOUNT_CR) >= '" + VMVServices.Web.Utils.ReportFilterParameter["From_Amt"] + "'";
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["To_Amt"] != null)
                {
                    sqlQuery += " and (v.CB_AMOUNT_DR - v.CB_AMOUNT_CR) <= '" + VMVServices.Web.Utils.ReportFilterParameter["To_Amt"] + "'";
                }
                if (qtrValue == "Q1")
                {
                    sqlQuery += " and g.period_number >='1' and g.period_number <='6'";
                }
                if (qtrValue == "Q2")
                {
                    sqlQuery += " and g.period_number >='7' and g.period_number <='12'";
                }
                if (post != "1")
                {
                    sqlQuery += " and g.WORKFLOW_COMPLETION_STATUS='1'";
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["WITHZERO"] == "FALSE")
                {
                    sqlQuery += " AND abs(v.CB_AMOUNT_DR - v.CB_AMOUNT_CR) > 0  ";
                }


            }
            return sqlQuery;
        }

        public static string getReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM gl_account_monthly_summary_r V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportFilterParameter["WITHZERO"] == "FALSE")
            {
                sqlQuery += " AND (abs(V.transaction_debit - V.transaction_credit)>0 or abs(V.OPENING_BALANCE) >0)  ";
            }


            if (VMVServices.Web.Utils.ReportFilterParameter["From_Amt"] != null || VMVServices.Web.Utils.ReportFilterParameter["To_Amt"] != null || VMVServices.Web.Utils.ReportFilterParameter["Credit_From_Amt"] != null || VMVServices.Web.Utils.ReportFilterParameter["Credit_To_Amt"] != null)
            {
                sqlQuery += " AND ( ";
                if (VMVServices.Web.Utils.ReportFilterParameter["From_Amt"] != null && VMVServices.Web.Utils.ReportFilterParameter["To_Amt"] != null)
                {
                    sqlQuery += " (V.transaction_debit >= '" + (VMVServices.Web.Utils.ReportFilterParameter["From_Amt"] == null ? "0" : VMVServices.Web.Utils.ReportFilterParameter["From_Amt"].ToString()) + "')  ";
                    sqlQuery += " AND ( V.transaction_debit <= '" + (VMVServices.Web.Utils.ReportFilterParameter["To_Amt"] == null ? "0" : VMVServices.Web.Utils.ReportFilterParameter["To_Amt"].ToString()) + "') ";

                }
                else if (VMVServices.Web.Utils.ReportFilterParameter["From_Amt"] != null)
                {
                    sqlQuery += " (V.transaction_debit >= '" + (VMVServices.Web.Utils.ReportFilterParameter["From_Amt"] == null ? "0" : VMVServices.Web.Utils.ReportFilterParameter["From_Amt"].ToString()) + "')  ";


                }
                else if (VMVServices.Web.Utils.ReportFilterParameter["To_Amt"] != null)
                {
                    sqlQuery += "  (V.transaction_debit <= '" + (VMVServices.Web.Utils.ReportFilterParameter["To_Amt"] == null ? "0" : VMVServices.Web.Utils.ReportFilterParameter["To_Amt"].ToString()) + "') ";

                }

                if ((VMVServices.Web.Utils.ReportFilterParameter["From_Amt"] != null || VMVServices.Web.Utils.ReportFilterParameter["To_Amt"] != null) && (VMVServices.Web.Utils.ReportFilterParameter["Credit_From_Amt"] != null || VMVServices.Web.Utils.ReportFilterParameter["Credit_To_Amt"] != null))
                { sqlQuery += " OR "; }


                if (VMVServices.Web.Utils.ReportFilterParameter["Credit_From_Amt"] != null && VMVServices.Web.Utils.ReportFilterParameter["Credit_To_Amt"] != null)
                {
                    sqlQuery += " (V.transaction_credit >= '" + (VMVServices.Web.Utils.ReportFilterParameter["Credit_From_Amt"] == null ? "0" : VMVServices.Web.Utils.ReportFilterParameter["Credit_From_Amt"].ToString()) + "') ";
                    sqlQuery += " AND ( V.transaction_credit <= '" + (VMVServices.Web.Utils.ReportFilterParameter["Credit_To_Amt"] == null ? "0" : VMVServices.Web.Utils.ReportFilterParameter["Credit_To_Amt"].ToString()) + "') ";
                }
                else if (VMVServices.Web.Utils.ReportFilterParameter["Credit_From_Amt"] != null)
                {
                    sqlQuery += " (V.transaction_credit >= '" + (VMVServices.Web.Utils.ReportFilterParameter["Credit_From_Amt"] == null ? "0" : VMVServices.Web.Utils.ReportFilterParameter["Credit_From_Amt"].ToString()) + "') ";

                }
                else if (VMVServices.Web.Utils.ReportFilterParameter["Credit_To_Amt"] != null)
                {
                    sqlQuery += "  ( V.transaction_credit <= '" + (VMVServices.Web.Utils.ReportFilterParameter["Credit_To_Amt"] == null ? "0" : VMVServices.Web.Utils.ReportFilterParameter["Credit_To_Amt"].ToString()) + "') ";
                }


                sqlQuery += " )";
            }
            //if (VMVServices.Web.Utils.ReportFilterParameter["From_Amt"] != null)
            //{
            //    sqlQuery += " and v.transaction_debit  >= '" + VMVServices.Web.Utils.ReportFilterParameter["From_Amt"] + "'";
            //}

            //if (VMVServices.Web.Utils.ReportFilterParameter["To_Amt"] != null)
            //{
            //    sqlQuery += " and v.transaction_debit  <= '" + VMVServices.Web.Utils.ReportFilterParameter["To_Amt"] + "'";
            //}
            //if (VMVServices.Web.Utils.ReportFilterParameter["Credit_From_Amt"] != null)
            //{
            //    sqlQuery += " and v.transaction_credit >= '" + VMVServices.Web.Utils.ReportFilterParameter["Credit_From_Amt"] + "'";
            //}

            //if (VMVServices.Web.Utils.ReportFilterParameter["Credit_To_Amt"] != null)
            //{
            //    sqlQuery += " and  v.transaction_credit <= '" + VMVServices.Web.Utils.ReportFilterParameter["Credit_To_Amt"] + "'";
            //}
            if (VMVServices.Web.Utils.ReportFilterParameter["FromAccount"] != null)
            {
                sqlQuery += " AND V.account_code >=  '" + VMVServices.Web.Utils.ReportFilterParameter["FromAccount"].ToString() + "'";
            }
            if (VMVServices.Web.Utils.ReportFilterParameter["ToAccount"] != null)
            {
                sqlQuery += " AND V.account_code <=  '" + VMVServices.Web.Utils.ReportFilterParameter["ToAccount"].ToString() + "'";
            }
            return sqlQuery;
        }
        public static DataSet GetSP_AccountMonthlySummary(string global_segment_id, string str_Period, string str_acctcode, string str_unposted, string str_Query)
        {
            try
            {
                string strValue = string.Empty;
                string strNextCode = string.Empty;
                string strErrMsg = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "GL_PKG.GL_Account_Monthly_Summary";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add(new OracleParameter("@p_org_id", OracleDbType.Varchar2, 250)).Value = VMVServices.Web.Utils.OrganizationID;
                oraCmd.Parameters.Add(new OracleParameter("@p_global_segment_id", OracleDbType.Varchar2, 250)).Value = global_segment_id;
                oraCmd.Parameters.Add(new OracleParameter("@p_from_date", OracleDbType.Date, 250)).Value = null;
                oraCmd.Parameters.Add(new OracleParameter("@p_to_date", OracleDbType.Date, 250)).Value = null;              
                oraCmd.Parameters.Add(new OracleParameter("@p_acct_code", OracleDbType.Varchar2, 250)).Value = str_acctcode;
                oraCmd.Parameters.Add(new OracleParameter("@p_unpost", OracleDbType.Varchar2, 250)).Value = str_unposted;
                oraCmd.Parameters.Add(new OracleParameter("@p_period_id", OracleDbType.Varchar2, 250)).Value = str_Period;

                return DBMethod.ExecuteStoredProcedure(oraCmd, str_Query);



            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static string getGlobalSegment()
        {
            sqlQuery = string.Empty;
            sqlQuery += " SELECT SV.SEGMENT_ID,SV.SEGMENT_VALUE" + VMVServices.Web.Utils.LanguageCode;
            sqlQuery += " FROM GL_SEGMENT_VALUES SV ";
            sqlQuery += " WHERE SV.WORKFLOW_COMPLETION_STATUS='1' ";
            sqlQuery += " AND SV.ENABLED_FLAG='1'";


            return sqlQuery;
        }
        public static string getAccMonthSumparm()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM gl_je_summary_period V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportFilterParameter["SEGMENT_ID"] != null)
                {
                    sqlQuery += " AND V.GLOBAL_SEGMENT_ID = '" + VMVServices.Web.Utils.ReportFilterParameter["SEGMENT_ID"].ToString() + "'";
                }

                if (VMVServices.Web.Utils.ReportFilterParameter["AccountCode"] != null)
                {
                    sqlQuery += " AND V.ACCT_CODE_ID = '" + VMVServices.Web.Utils.ReportFilterParameter["AccountCode"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.PERIOD_ID] != null)
                {
                    sqlQuery += " AND V.PERIOD_ID = '" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.PERIOD_ID].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["AccFromGroup"] != null)
                {
                    sqlQuery += " AND V.GROUP_ID >= '" + VMVServices.Web.Utils.ReportFilterParameter["AccFromGroup"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["AccToGroup"] != null)
                {
                    sqlQuery += " AND V.GROUP_ID <= '" + VMVServices.Web.Utils.ReportFilterParameter["AccToGroup"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["WITHZERO"] == "FALSE")
                {
                    sqlQuery += " AND (V.BB_AMOUNT_CR >0 OR V.BB_AMOUNT_DR >0 or v.BB_ACCOUNTED_CR>0 or v.BB_ACCOUNTED_DR>0 or v.AMOUNT_CR>0 or v.AMOUNT_DR>0 or v.ACCOUNTED_CR>0 or v.ACCOUNTED_DR>0 or v.CB_AMOUNT_CR>0 or v.CB_AMOUNT_DR>0 or v.CB_ACCOUNTED_CR>0 or v.CB_ACCOUNTED_DR>0)  ";
                }
                //if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
                //{
                //    sqlQuery += " AND V.ACCT_CODE_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"].ToString() + "'";
                //}
                //if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
                //{
                //    sqlQuery += " AND V.ACCT_CODE_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"].ToString() + "'";
                //}
            }
            return sqlQuery;
        }


        public static DataSet getSP_GL_Period_Balance(string global_segment_id, DateTime dtpFromDate, DateTime dtp_ToDate, string str_unposted)
        {
            try
            {
                string strValue = string.Empty;
                string strNextCode = string.Empty;
                string strErrMsg = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "GL_PKG.gl_Period_Balance";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add(new OracleParameter("@p_org_id", OracleDbType.Varchar2, 250)).Value = VMVServices.Web.Utils.OrganizationID;
                oraCmd.Parameters.Add(new OracleParameter("@p_global_segment_id", OracleDbType.Varchar2, 250)).Value = global_segment_id;
                oraCmd.Parameters.Add(new OracleParameter("@p_from_date", OracleDbType.Date, 250)).Value = dtpFromDate.ToString("dd/MMM/yyyy");
                oraCmd.Parameters.Add(new OracleParameter("@p_to_date", OracleDbType.Date, 250)).Value = dtp_ToDate.ToString("dd/MMM/yyyy");
                oraCmd.Parameters.Add(new OracleParameter("@p_unposted", OracleDbType.Varchar2, 250)).Value = str_unposted;
                string str_dec = VMVServices.Web.Utils.DecimalPrecision;
                string str_Query = "  select A.ACCT_GRP_ID,to_char(ROUND(A.OPENING_BALANCE," + str_dec + ")) as OPENING_BALANCE ,to_char(ROUND(A.TRANSACTION_DEBIT," + str_dec + ")) as TRANSACTION_DEBIT ,to_char(ROUND(A.TRANSACTION_CREDIT," + str_dec + ")) as TRANSACTION_CREDIT,to_char(ROUND(A.CLOSING_BALANCE_DEBIT," + str_dec + ")) as CLOSING_BALANCE_DEBIT,to_char(ROUND(A.CLOSING_BALANCE_CREDIT," + str_dec + ")) as CLOSING_BALANCE_CREDIT ,to_char(ROUND(A.CLOSING_BALANCE," + str_dec + ")) as CLOSING_BALANCE,A.GROUP_LEVEL,A.PARENT_GROUP_ID,A.ACCT_GRP_NAME ";
                str_Query += "   ,LEVEL as num_or, lpad(' ',25-(to_number(a.group_level)* 4),' ') || a.acct_grp_name  as NEW_ACCT_GRP_NAME  from tmp_gl_period_balance a  ";
                str_Query += " where a.acct_grp_id is not null start with Group_level=5 ";
                str_Query += " connect by prior  a.acct_grp_id=a.parent_group_id ";

                return DBMethod.ExecuteStoredProcedure(oraCmd, str_Query);



            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GetExpensesBalance4Report()
        {
            sqlQuery = string.Empty;
            sqlQuery += " SELECT * FROM VW_TOP5EXPENCES_GROUP  V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["PERIOD"] != null)
                {
                    sqlQuery += " AND V.period_id = '" + VMVServices.Web.Utils.ReportViewFilterParameter["PERIOD"].ToString() + "'";
                }
            }

            return sqlQuery;
        }

        public static string Getcalenderdatebyperiod(string periodid)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select h.pk_id,gp.PERIOD_ID,gp.PERIOD_NAME,h.cal_eff_start_dt,h.cal_eff_end_dt from  gl_acct_calendar_hdr h,gl_companies_dtl cd,GL_COMP_ACCT_PERIOD_DTL gp, gl_acct_calendar_dtl al ";
            sqlQuery += "  where h.enabled_flag='1'";
            sqlQuery += "  and h.workflow_completion_status='1' and h.CAL_ID=cd.COMP_CAL_ID and al.cal_id = h.cal_id and al.cal_dtl_id = gp.cal_dtl_id and gp.PERIOD_STATUS <>'NOP' ";
            sqlQuery += "  and cd.COMP_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += "  and gp.period_type='ACTPERIOD' ";
            sqlQuery += " and gp.PERIOD_ID = '" + periodid + "'";
            //sqlQuery += "  AND (gp.PERIOD_FROM_DT IS NULL OR";
            //sqlQuery += "  (SYSDATE BETWEEN gp.PERIOD_FROM_DT AND gp.PERIOD_TO_DT))";

            sqlQuery += "  order by gp.PERIOD_NAME asc";

            return sqlQuery;
        }

        public static string getglAcctDet()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT * FROM vw_gl_acc_detail V WHERE ROWNUM > 0 ";
            return sqlQuery;
        }
        public static string GetPettyCashRpt()
        {
            sqlQuery = string.Empty;
            sqlQuery += " SELECT * FROM gl_petty_cash_book  V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["EMPLOYEE"] != null)
                {
                    sqlQuery += " AND V.gl_pca_user_id = '" + VMVServices.Web.Utils.ReportViewFilterParameter["EMPLOYEE"].ToString() + "'";
                }
            }

            return sqlQuery;
        }

    }
}
