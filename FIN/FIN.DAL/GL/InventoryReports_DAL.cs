﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.GL
{
   public class InventoryReports_DAL
    {
        static string sqlQuery = "";


        public static string getHalfYearlyDate(string calDtlID)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select cd.cal_dtl_id, cd.cal_id, cd.cal_acct_year,cd.cal_eff_start_dt";
            sqlQuery += " ,LAST_DAY(to_date(to_char(add_months(cal_eff_start_dt,5),'dd/MM/yyyy'),'dd/MM/yyyy')) as firstHalfYearly_to_date";
            sqlQuery += " ,to_char(add_months(cal_eff_start_dt,6),'dd/MM/yyyy') as secondHalfYearly_from_date";
            sqlQuery += " ,cd.call_eff_end_dt";
            sqlQuery += " from GL_ACCT_CALENDAR_DTL cd ";
            sqlQuery += " where cd.enabled_flag = 1 ";
            sqlQuery += " and cd.cal_dtl_id = '" + calDtlID + "'";

            return sqlQuery;
        }

        public static string getReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_INVENTORY_REPORT V  WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportFilterParameter != null || VMVServices.Web.Utils.ReportFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.INV_WH_ID] != null)
                {
                    sqlQuery += " AND V.INV_WH_ID = '" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.INV_WH_ID].ToString() + "'";
                }
            }
            return sqlQuery;
        }

        public static string getWareHouseName()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT DISTINCT IW.INV_WH_NAME,IW.INV_WH_ID  FROM INV_WAREHOUSES IW";
            sqlQuery += " WHERE IW.WORKFLOW_COMPLETION_STATUS = 1 ";

            sqlQuery += " AND IW.ENABLED_FLAG = 1 ";
            sqlQuery += " ORDER BY IW.INV_WH_NAME ";
            return sqlQuery;

        }
        public static string getInventoryCostReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_INVENTORY_REPORT V  WHERE ROWNUM > 0 ";
            //if (VMVServices.Web.Utils.ReportFilterParameter != null || VMVServices.Web.Utils.ReportFilterParameter.Count != 0)
            //{
            //    if (VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.INV_WH_ID] != null)
            //    {
            //        sqlQuery += " AND V.INV_WH_ID = '" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.INV_WH_ID].ToString() + "'";
            //    }
            //}
            return sqlQuery;
        }
        public static string getCalDate(string cal_id)
        {
            sqlQuery = string.Empty;
            sqlQuery = " select cd.cal_eff_start_dt,cd.call_eff_end_dt from gl_acct_calendar_dtl cd ";
            sqlQuery += " where cd.enabled_flag = 1 ";
            sqlQuery += " and cd.cal_dtl_id = '" + cal_id + "'";
            //if (VMVServices.Web.Utils.ReportFilterParameter != null || VMVServices.Web.Utils.ReportFilterParameter.Count != 0)
            //{
            //    if (VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.INV_WH_ID] != null)
            //    {
            //        sqlQuery += " AND V.INV_WH_ID = '" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.INV_WH_ID].ToString() + "'";
            //    }
            //}
            return sqlQuery;
        }
    }
}
