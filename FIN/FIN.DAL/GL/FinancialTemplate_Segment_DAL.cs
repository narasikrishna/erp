﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;

using System.Data.EntityClient;
using System.Data.Entity.Infrastructure;

namespace FIN.DAL.GL
{
    public class FinancialTemplate_Segment_DAL
    {

        public static string get_TemplateStructure(string str_name)
        {
            string sqlQuery = string.Empty;
            sqlQuery = " SELECT BST.BS_TMPL_ID,BST.BS_TMPL_NAME,BST.BS_TYPE,BST.BS_GROUP_NAME,NVL(BST.BS_GROUP_NAME,' ') || NVL(BST.BS_ACC_NAME,' ') || ' - ' || BST.BS_GROUP_NO as BS_DISPLAY_NAME,BST.BS_ACC_ID,BST.BS_ACC_NAME,BST.BS_TMPL_PARENT_ID,NVL(BST.BS_TMPL_PARENT_GROUP_NAME,'HEAD') AS BS_TMPL_PARENT_GROUP_NAME,BST.BS_GROUP_NO,BST.BS_TOT_REQ ";
            sqlQuery += "FROM GL_COSTCENTER_TEMPLATE BST ";
            sqlQuery += " where BS_TMPL_NAME='" + str_name + "'";
            sqlQuery += " ORDER BY BST.BS_GROUP_NO";
            return sqlQuery;
        }

        public static string get_Template4Group(string str_name, string groupNumber)
        {
            string sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM GL_COSTCENTER_TEMPLATE ";
            sqlQuery += " where BS_TMPL_NAME='" + str_name + "'";
            sqlQuery += " AND BS_GROUP_NO=" + groupNumber;
            return sqlQuery;
        }
        public static string get_TemplateGroupNumber(string str_name)
        {
            string sqlQuery = string.Empty;
            sqlQuery += " SELECT BS_GROUP_NO FROM GL_COSTCENTER_TEMPLATE  WHERE BS_TMPL_NAME='" + str_name + "' ";
            return sqlQuery;
        }
        public static string get_GroupTemplateStructure(string str_name)
        {
            string sqlQuery = string.Empty;
            sqlQuery = " SELECT BST.BS_TMPL_ID,BST.BS_TMPL_NAME,BST.BS_TYPE,BST.BS_GROUP_NAME,NVL(BST.BS_GROUP_NAME,' ') || NVL(BST.BS_ACC_NAME,' ')|| ' - ' || BST.NOTES as BS_DISPLAY_NAME,BST.BS_ACC_ID,BST.BS_ACC_NAME,BST.BS_TMPL_PARENT_ID,NVL(BST.BS_TMPL_PARENT_GROUP_NAME,'HEAD') AS BS_TMPL_PARENT_GROUP_NAME,BST.BS_GROUP_NO,BST.BS_TOT_REQ ";
            sqlQuery += "FROM GL_COSTCENTER_TEMPLATE BST ";
            sqlQuery += " where BS_TMPL_NAME='" + str_name + "'";
            sqlQuery += " AND UPPER(BST.BS_TYPE)='GROUP' ";
            sqlQuery += " ORDER BY BST.BS_GROUP_NO";
            return sqlQuery;
        }
        public static string get_TemplateData4TmplId(string str_TMPL_ID)
        {
            string sqlQuery = string.Empty;
            sqlQuery = "SELECT * FROM GL_COSTCENTER_TEMPLATE A WHERE A.BS_TMPL_PARENT_ID='" + str_TMPL_ID + "'";
            return sqlQuery;
        }

        public static DataTable get_TmpleateData(string str_TemplateName, string str_FromDate, string str_ToDate,string str_WithZero)
        {
            string strValue = string.Empty;
            string strNextCode = string.Empty;
            string strErrMsg = string.Empty;

            OracleCommand oraCmd = new OracleCommand();
            oraCmd.CommandText = "FINANICAL_TEMPLATE_SEGMENT";
            oraCmd.CommandType = CommandType.StoredProcedure;
            oraCmd.Parameters.Add("@P_TEMPLATE_NAME", OracleDbType.Varchar2).Value = str_TemplateName;
            oraCmd.Parameters.Add("@P_FROM_DATE", OracleDbType.Varchar2).Value = str_FromDate;
            oraCmd.Parameters.Add("@P_TO_DATE", OracleDbType.Varchar2).Value = str_ToDate;
            oraCmd.Parameters.Add("@P_ORG_ID", OracleDbType.Varchar2).Value = VMVServices.Web.Utils.OrganizationID;
            oraCmd.Parameters.Add("@P_WITH_ZERO", OracleDbType.Varchar2).Value = str_WithZero;
            oraCmd.Parameters.Add("@P_DATA", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            return DBMethod.ExecuteStoredProcedure4CursorOutput(oraCmd);
        }

        public static string getTemplateName()
        {
            string sqlQuery = string.Empty;
            sqlQuery = " SELECT DISTINCT BST.BS_TMPL_NAME FROM GL_COSTCENTER_TEMPLATE BST ";
            return sqlQuery;

        }
        public static string getSegmentValue(string Parent_id = "0")
        {
            string sqlQuery = string.Empty;

            sqlQuery += "  SELECT'0' as BS_TMPL_ID, SV.SEGMENT_VALUE_ID as SEGMENT_ID,SV.SEGMENT_VALUE as SEGMENT_NAME ";
            sqlQuery += " ,'N' as SELECTED_REC ";
            sqlQuery += " FROM GL_SEGMENTS S  ";
            sqlQuery += " INNER JOIN GL_SEGMENT_VALUES SV ON S.SEGMENT_ID= SV.SEGMENT_ID AND SV.SEGMENT_ID='SEG_ID-0000000101'";
            sqlQuery += " WHERE S.SEGMENT_ID NOT IN (SELECT NVL(COMP_GLOBAL_SEGMENT_ID,'') FROM gl_companies_hdr) ";
            sqlQuery += " and  SEGMENT_VALUE_ID not in (select bs_acc_id from GL_COSTCENTER_TEMPLATE where BS_TMPL_PARENT_ID='" + Parent_id + "' and bs_acc_id is not null)";

            sqlQuery += " UNION ";

            sqlQuery += "  SELECT BS_TMPL_ID, SV.SEGMENT_VALUE_ID as SEGMENT_ID,SV.SEGMENT_VALUE as SEGMENT_NAME ";
            sqlQuery += " ,'Y' as SELECTED_REC ";
            sqlQuery += " FROM GL_SEGMENTS S  ";
            sqlQuery += " INNER JOIN GL_SEGMENT_VALUES SV ON S.SEGMENT_ID= SV.SEGMENT_ID  AND SV.SEGMENT_ID='SEG_ID-0000000101' ";
            sqlQuery += " INNER JOIN GL_COSTCENTER_TEMPLATE BST on BST.BS_ACC_ID=SV.SEGMENT_VALUE_ID ";
            sqlQuery += " WHERE S.SEGMENT_ID NOT IN (SELECT NVL(COMP_GLOBAL_SEGMENT_ID,'') FROM gl_companies_hdr) ";
            sqlQuery += " and BS_TMPL_PARENT_ID='" + Parent_id + "'";

            sqlQuery += "  order by SEGMENT_NAME asc ";
            return sqlQuery;
        }

        public static string getTemplateSegment(string TempName)
        {
            string sqlQuery = string.Empty;
            /*sqlQuery += "  SELECT DISTINCT REPLACE(SV.SEGMENT_VALUE_ID,' - ','_') AS SEGMENT_VALUE_ID,SV.SEGMENT_VALUE ";
            sqlQuery += " from GL_COSTCENTER_TEMPLATE ST ";
            sqlQuery += "  INNER JOIN GL_SEGMENT_VALUES SV ON SV.SEGMENT_VALUE_ID = ST.BS_ACC_ID ";
            sqlQuery += "  where UPPER(ST.BS_TMPL_NAME) =UPPER('" + TempName + "') ";
            sqlQuery += " order by SV.SEGMENT_VALUE ";*/

            sqlQuery += "  SELECT DISTINCT REPLACE(SV.SEGMENT_VALUE_ID,' - ','_') AS SEGMENT_VALUE_ID,SV.SEGMENT_VALUE ";
            sqlQuery += " from GL_SEGMENT_VALUES SV ";
            return sqlQuery;
        }


        public static string getSubGroupDet(string str_ParentTmpl_id)
        {
            string sqlQuery = string.Empty;
            sqlQuery += " SELECT CT.BS_TMPL_ID,BS_GROUP_NAME,BS_GROUP_NO,BS_TOT_REQ,BS_FORMULA_GROUP,BS_FORMULA,NOTES,BS_TYPE ";
            sqlQuery += " ,'N' as DELETED ";
            sqlQuery += "  FROM GL_COSTCENTER_TEMPLATE CT ";
            sqlQuery += " WHERE CT.BS_TMPL_PARENT_ID='" + str_ParentTmpl_id + "'";
            return sqlQuery;
        }

        public static string getSubAccountDet(string str_ParentTmpl_id)
        {
            string sqlQuery = string.Empty;
            sqlQuery += " SELECT CT.BS_TMPL_ID,BS_GROUP_NAME,BS_ACC_ID,BS_TYPE ";
            sqlQuery += " ,'N' as DELETED ";
            sqlQuery += "  FROM GL_COSTCENTER_TEMPLATE CT ";
            sqlQuery += " WHERE CT.BS_TMPL_PARENT_ID='" + str_ParentTmpl_id + "'";
            return sqlQuery;
        }


        public static string getSubSegmentDet(string str_ParentTmpl_id)
        {
            string sqlQuery = string.Empty;
            sqlQuery += " SELECT CT.BS_TMPL_ID,BS_ACC_NAME,BS_ACC_ID,BS_TYPE ";
            sqlQuery += " ,'N' as DELETED ";
            sqlQuery += "  FROM GL_COSTCENTER_TEMPLATE CT ";
            sqlQuery += " WHERE CT.BS_TMPL_PARENT_ID='" + str_ParentTmpl_id + "'";
            return sqlQuery;
        }
    }
}
