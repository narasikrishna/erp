﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.GL
{
    public class Segments_DAL
    {
        static string sqlQuery = "";

        public static string GetSegmentvalues(string Master_id)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select sv.pk_id,sv.segment_value_id,sv.segment_value" + VMVServices.Web.Utils.LanguageCode + " as segment_value,sv.effective_start_dt,sv.effective_end_dt,'N' AS DELETED, sv.segment_value_ol,";
            sqlQuery += " case sv.enabled_flag when '1' then 'TRUE' else 'FALSE' end as enabled_flag";
            sqlQuery += " ,sv.parent_segment_value_id,psv.segment_value" + VMVServices.Web.Utils.LanguageCode + " as PARENT_SEGMENT_VALUE ";
            sqlQuery += " from GL_SEGMENT_VALUES SV";
            sqlQuery += " left join gl_segment_values psv on sv.parent_segment_value_id= psv.segment_value_id ";

            sqlQuery += " WHERE SV.SEGMENT_ID = '" + Master_id + "'";
            sqlQuery += " AND SV.WORKFLOW_COMPLETION_STATUS = 1";
            //sqlQuery += " AND SV.ENABLED_FLAG = 1";
            sqlQuery += " ORDER BY SEGMENT_VALUE_ID ";

            return sqlQuery;

        }
        public static string GetSegmentvalues4ParentSeg(string Master_id, string str_ParentSeg)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select sv.segment_value_id,sv.segment_value" + VMVServices.Web.Utils.LanguageCode + " as segment_value";
            sqlQuery += " from GL_SEGMENT_VALUES SV";
            sqlQuery += " WHERE SV.SEGMENT_ID = '" + Master_id + "'";
            if (str_ParentSeg != "0")
            {
                sqlQuery += " AND sv.parent_segment_value_id ='" + str_ParentSeg + "'";
            }
            sqlQuery += " AND SV.WORKFLOW_COMPLETION_STATUS = 1";
            //sqlQuery += " AND SV.ENABLED_FLAG = 1";
            sqlQuery += " ORDER BY SEGMENT_VALUE_ID ";

            return sqlQuery;

        }
        public static string GetGlobalSegmentvalues(string orgId, string segmentId = "")
        {
            sqlQuery = string.Empty;

            sqlQuery = "  select sv.segment_value_id , sv.segment_value" + VMVServices.Web.Utils.LanguageCode + " as segment_value,s.*";
            sqlQuery += "   from GL_SEGMENTS s, gl_segment_values sv,gl_companies_hdr  c";
            sqlQuery += "  where s.segment_id = sv.segment_id";
            sqlQuery += "  and c.comp_global_segment_id=s.segment_id";
            sqlQuery += "  and s.enabled_flag='1'";
            sqlQuery += "  and sv.workflow_completion_status='1'";
            sqlQuery += " and c.COMP_ID = '" + orgId + "'";

            if (segmentId != string.Empty)
            {
                sqlQuery += " and sv.segment_value_id='" + segmentId + "'";
            }

            sqlQuery += "  order by sv.segment_value asc";

            return sqlQuery;

        }
        public static string GetGlobalSegmentvaluesBasedOrg()
        {
            sqlQuery = string.Empty;

            sqlQuery = "  select sv.segment_value_id , sv.segment_value" + VMVServices.Web.Utils.LanguageCode + " as segment_value";
            sqlQuery += "   from GL_SEGMENTS s, gl_segment_values sv,gl_companies_hdr  c";
            sqlQuery += "  where s.segment_id = sv.segment_id";
            sqlQuery += "  and c.comp_global_segment_id=s.segment_id";
            sqlQuery += "  and s.enabled_flag='1'";
            sqlQuery += "  and sv.workflow_completion_status='1'";
            sqlQuery += " and c.COMP_ID = '" + VMVServices.Web.Utils.OrganizationID + "'";

            sqlQuery += "  order by sv.segment_value asc";

            return sqlQuery;

        }
        public static string GetGlobalSegmentvalues()
        {
            sqlQuery = string.Empty;

            sqlQuery = "  select sv.segment_value_id , sv.segment_value" + VMVServices.Web.Utils.LanguageCode + " as segment_value, s.*";
            sqlQuery += "   from GL_SEGMENTS s, gl_segment_values sv";
            sqlQuery += "  where s.segment_id = sv.segment_id";
            // sqlQuery += "  and c.comp_global_segment_id=s.segment_id";
            sqlQuery += "  and s.enabled_flag='1'";
            sqlQuery += "  and sv.workflow_completion_status='1'";

            sqlQuery += "  order by sv.segment_value asc";

            return sqlQuery;

        }
        public static string GetGlobalCostCentreSegmentvalues()
        {
            sqlQuery = string.Empty;

            sqlQuery = "  select sv.segment_value_id , sv.segment_value" + VMVServices.Web.Utils.LanguageCode + " as segment_value, s.*";
            sqlQuery += "   from GL_SEGMENTS s, gl_segment_values sv";
            sqlQuery += "  where s.segment_id = sv.segment_id";
            // sqlQuery += "  and c.comp_global_segment_id=s.segment_id";
            sqlQuery += "  and s.enabled_flag='1'";
            sqlQuery += "  and sv.workflow_completion_status='1'";
            sqlQuery += "  and upper(s.segment_name)=upper('Cost Centre')";
            sqlQuery += "  order by sv.segment_value asc";

            return sqlQuery;

        }
        public static string GetGlobalSegmentvaluesByOrgName()
        {
            sqlQuery = string.Empty;

            sqlQuery = "  select sv.segment_value_id , sv.segment_value" + VMVServices.Web.Utils.LanguageCode + " as segment_value, s.*";
            sqlQuery += "   from GL_SEGMENTS s inner join gl_segment_values sv on s.segment_id = sv.segment_id ";
            sqlQuery += "  right outer join gL_COMPANIES_HDR gh ";
            sqlQuery += "  on s.segment_id = gh.comp_global_segment_id ";
            sqlQuery += "  and s.enabled_flag='1'";
            sqlQuery += "  and sv.workflow_completion_status='1'";
            sqlQuery += " and gh.COMP_ID = '" + VMVServices.Web.Utils.OrganizationID + "'";

            sqlQuery += "  order by sv.segment_value asc";

            return sqlQuery;

        }

        public static string getSegmentValueNotInGlobalSegment(string segId="")
        {
            sqlQuery = string.Empty;

            sqlQuery += "  SELECT SV.SEGMENT_VALUE_ID,SV.SEGMENT_VALUE FROM GL_SEGMENTS S  ";
            sqlQuery += " INNER JOIN GL_SEGMENT_VALUES SV ON S.SEGMENT_ID= SV.SEGMENT_ID ";
            sqlQuery += " WHERE S.SEGMENT_ID NOT IN (SELECT NVL(COMP_GLOBAL_SEGMENT_ID,'') FROM gl_companies_hdr) ";
            if (segId.Length > 0)
            {
                sqlQuery += " and s.segment_id='" + segId + "'";
            }
            return sqlQuery;
        }
        public static string GetGlobalSegmentName()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT GS.SEGMENT_ID, GS.SEGMENT_NAME" + VMVServices.Web.Utils.LanguageCode + " as segment_name";
            sqlQuery += " FROM GL_SEGMENTS GS  ";
            sqlQuery += " WHERE GS.WORKFLOW_COMPLETION_STATUS = '1' ";
            sqlQuery += " AND GS.ENABLED_FLAG = '1' ";
            sqlQuery += "  ORDER BY SEGMENT_NAME ";

            return sqlQuery;

        }

        public static string getSegmentData(string str_SegmentID)
        {
            sqlQuery = string.Empty;
            sqlQuery += " SELECT * FROM GL_SEGMENTS GS WHERE GS.SEGMENT_ID='" + str_SegmentID + "'";
            return sqlQuery;
        }

        public static string GetSegmentvaluesBasedAccSegment(string accountCodeId, string segmentId)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select sv.segment_value_id,sv.segment_value" + VMVServices.Web.Utils.LanguageCode + " as segment_value,sv.segment_id from gl_acct_codes ac,GL_ACCT_CODE_SEGMENTS cs,gl_segment_values sv";
            sqlQuery += " where ac.acct_code_id=cs.acct_code_id";
            sqlQuery += " and sv.segment_id=cs.acct_code_segment_id";
            sqlQuery += " and ac.acct_code_id='" + accountCodeId + "'";
            sqlQuery += " and sv.segment_id='" + segmentId + "'";
            sqlQuery += " and ac.enabled_flag='1'";
            sqlQuery += " and cs.enabled_flag='1'";
            sqlQuery += " and sv.enabled_flag='1' ";
            sqlQuery += " and ac.workflow_completion_status='1'";
            sqlQuery += " and cs.workflow_completion_status='1'";
            sqlQuery += " and sv.workflow_completion_status='1'";
            sqlQuery += " order by sv.segment_value asc";

            return sqlQuery;
        }
        public static string GetSegmentvaluesBasedGroupSegment(string accountGroupId, string segmentId)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select sv.segment_value_id,sv.segment_value" + VMVServices.Web.Utils.LanguageCode + " as segment_value,sv.segment_id from gl_acct_codes ac,GL_ACCT_CODE_SEGMENTS cs,gl_segment_values sv";
            sqlQuery += " where ac.acct_code_id=cs.acct_code_id";
            sqlQuery += " and sv.segment_id=cs.acct_code_segment_id";
            sqlQuery += " and ac.acct_grp_id='" + accountGroupId + "'";
            sqlQuery += " and sv.segment_id='" + segmentId + "'";
            sqlQuery += " and ac.enabled_flag='1'";
            sqlQuery += " and cs.enabled_flag='1'";
            sqlQuery += " and sv.enabled_flag='1' ";
            sqlQuery += " and ac.workflow_completion_status='1'";
            sqlQuery += " and cs.workflow_completion_status='1'";
            sqlQuery += " and sv.workflow_completion_status='1'";
            sqlQuery += " order by sv.segment_value asc";

            return sqlQuery;
        }
        public static string GetSegmentBasedAccCode(string accountCodeId, string segmentValue = "")
        {
            sqlQuery = string.Empty;

            sqlQuery = " select  sv.segment_id,sv.segment_name" + VMVServices.Web.Utils.LanguageCode + " ,SV.IS_DEPENDENT ";
            sqlQuery += "  from gl_acct_codes ac, GL_ACCT_CODE_SEGMENTS cs, gl_segments sv";
            sqlQuery += "  where ac.acct_code_id = cs.acct_code_id";
            sqlQuery += "   and sv.segment_id = cs.acct_code_segment_id";
            sqlQuery += "   and ac.acct_code_id='" + accountCodeId + "'";
            sqlQuery += "   and ac.enabled_flag = '1'";
            sqlQuery += "   and cs.enabled_flag = '1'";
            sqlQuery += "   and sv.enabled_flag = '1'";
            sqlQuery += "   and ac.workflow_completion_status = '1'";
            sqlQuery += "   and cs.workflow_completion_status = '1'";
            sqlQuery += "   and sv.workflow_completion_status = '1'";

            if (segmentValue != string.Empty)
            {
                sqlQuery += "  and sv.segment_id not in";
                sqlQuery += "  (select svv.segment_id";
                sqlQuery += "     from gl_segment_values svv";
                sqlQuery += "   where svv.SEGMENT_VALUE_ID = '" + segmentValue + "' and rownum=1)";
            }

            sqlQuery += " order by CS.ACCT_CODE_SEG_ID asc";

            return sqlQuery;
        }
        public static string GetSegmentBasedAccGroup(string accountGroupId)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select  sv.segment_id,sv.segment_name" + VMVServices.Web.Utils.LanguageCode;
            sqlQuery += "  from gl_acct_codes ac, GL_ACCT_CODE_SEGMENTS cs, gl_segments sv";
            sqlQuery += "  where ac.acct_code_id = cs.acct_code_id";
            sqlQuery += "   and sv.segment_id = cs.acct_code_segment_id";
            sqlQuery += "  and ac.acct_grp_id='" + accountGroupId + "'";
            sqlQuery += "   and ac.enabled_flag = '1'";
            sqlQuery += "   and cs.enabled_flag = '1'";
            sqlQuery += "   and sv.enabled_flag = '1'";
            sqlQuery += "   and ac.workflow_completion_status = '1'";
            sqlQuery += "   and cs.workflow_completion_status = '1'";
            sqlQuery += "   and sv.workflow_completion_status = '1'";
            sqlQuery += " order by sv.segment_id asc";

            return sqlQuery;
        }

        public static string getSegmentvalue4Segment(string str_SegmentId,string str_FromValue="",string str_ToValue="" )
        {
            sqlQuery = string.Empty;
            sqlQuery = "  SELECT SV.SEGMENT_VALUE_ID,SV.SEGMENT_VALUE,SV.SEGMENT_VALUE_OL FROM GL_SEGMENT_VALUES SV ";
            sqlQuery += " WHERE SV.SEGMENT_ID='" + str_SegmentId + "'";
            if (str_FromValue.Length > 0)
            {
                sqlQuery += " AND SV.SEGMENT_VALUE>='" + str_FromValue + "'";
            }
            if (str_ToValue.Length > 0)
            {
                sqlQuery += " AND SV.SEGMENT_VALUE<='" + str_ToValue + "'";
            }
            sqlQuery += " ORDER BY SEGMENT_VALUE ";
            return sqlQuery;
        }

        public static string getSegmentName4Type(string str_DepSeg, string mode)
        {
            sqlQuery = string.Empty;
            sqlQuery += "SELECT SEGMENT_ID,SEGMENT_NAME" + VMVServices.Web.Utils.LanguageCode + " as segment_name FROM GL_SEGMENTS GS WHERE  GS.WORKFLOW_COMPLETION_STATUS = 1";

            sqlQuery += " AND GS.IS_DEPENDENT =" + str_DepSeg;

            if (FINTableConstant.Add == mode)
            {
                sqlQuery += " AND (GS.EFFECTIVE_END_DT IS NULL OR";
                sqlQuery += "  (SYSDATE BETWEEN GS.EFFECTIVE_START_DT AND GS.EFFECTIVE_END_DT))";
                sqlQuery += " AND GS.ENABLED_FLAG=1";


            }
            return sqlQuery;
        }

        public static string getCostCenterList()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT * FROM VW_COST_CENTERS_LIST V WHERE ROWNUM > 0 ";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.SEGMENT_ID] != null)
                {
                    sqlQuery += " AND V.SEGMENT_ID='" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.SEGMENT_ID].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["CREATED_BY"] != null)
                {
                    sqlQuery += " AND UPPER(V.CREATED_BY) = UPPER('" + VMVServices.Web.Utils.ReportViewFilterParameter["CREATED_BY"].ToString() + "')";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["MODIFIED_BY"] != null)
                {
                    sqlQuery += " AND UPPER(V.MODIFIED_BY) = UPPER('" + VMVServices.Web.Utils.ReportViewFilterParameter["MODIFIED_BY"].ToString() + "')";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["CREATED_From_Date"] != null)
                {
                    sqlQuery += " AND V.CREATED_DATE >= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["CREATED_From_Date"].ToString() + "','dd/MM/yyyy') ";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["CREATED_To_Date"] != null)
                {
                    sqlQuery += " AND V.CREATED_DATE <= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["CREATED_To_Date"].ToString() + "','dd/MM/yyyy') ";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["MODIFIED_From_Date"] != null)
                {
                    sqlQuery += " AND V.MODIFIED_DATE >= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["MODIFIED_From_Date"].ToString() + "','dd/MM/yyyy') ";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["MODIFIED_To_Date"] != null)
                {
                    sqlQuery += " AND V.MODIFIED_DATE <= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["MODIFIED_To_Date"].ToString() + "','dd/MM/yyyy') ";
                }
                //if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.EFFECTIVE_FROM_DT] != null)
                //{
                //    sqlQuery += " AND to_date(V.PERIOD_DAY,'dd/MM/yyyy') between to_date('" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.EFFECTIVE_FROM_DT].ToString() + "','dd/MM/yyyy')";
                //}
                //if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.EFFECTIVE_TO_DT] != null)
                //{
                //    sqlQuery += " AND to_date('" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.EFFECTIVE_TO_DT].ToString() + "','dd/MM/yyyy')";
                //}
            }

            return sqlQuery;
        }

        public static string getSegmentBalancesSummary(string post, string periodId)
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT * FROM VW_SEGMENT_BALANCES V  WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportFilterParameter["SEGMENT_VALUE_ID"] != null)
                {
                    sqlQuery += " AND V.je_segment_id_1 = '" + VMVServices.Web.Utils.ReportFilterParameter["SEGMENT_VALUE_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["CAL_DTL_ID"] != null)
                {
                    sqlQuery += " AND V.CAL_DTL_ID = '" + VMVServices.Web.Utils.ReportFilterParameter["CAL_DTL_ID"].ToString() + "'";
                }
                if (periodId.ToUpper() != "ALL")
                {
                    if (VMVServices.Web.Utils.ReportFilterParameter["PERIOD_ID"] != null)
                    {
                        sqlQuery += " AND V.PERIOD_ID = '" + VMVServices.Web.Utils.ReportFilterParameter["PERIOD_ID"].ToString() + "'";
                    }
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["WITHZERO"] == "FALSE")
                {
                    sqlQuery += " and (v.je_amount_dr-v.je_amount_cr) > 0";
                }
                if (post != "1")
                {
                    sqlQuery += " and v.WORKFLOW_COMPLETION_STATUS=1";
                }
             
                
            }
            return sqlQuery;
        }

        public static string getSegmentTransactionSummary(string segmentId, string post)
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT * FROM VW_SEGMENT_TRANSACTIONS V  WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportFilterParameter["From_Date"] != null)
                {
                    sqlQuery += " AND to_char(to_date(V.je_date,'dd/MM/yyyy')) between to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy')";
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["To_Date"] != null)
                {
                    sqlQuery += " AND to_date('" + VMVServices.Web.Utils.ReportFilterParameter["To_Date"].ToString() + "','dd/MM/yyyy')";
                }

                if (segmentId == "1")
                {
                    if (VMVServices.Web.Utils.ReportFilterParameter["SEGMENT_VALUE_ID"] != null)
                    {
                        sqlQuery += " AND V.je_segment_id_1 = '" + VMVServices.Web.Utils.ReportFilterParameter["SEGMENT_VALUE_ID"].ToString() + "'";
                    }
                }
                else if (segmentId == "2")
                {
                    if (VMVServices.Web.Utils.ReportFilterParameter["SEGMENT_VALUE_ID"] != null)
                    {
                        sqlQuery += " AND V.je_segment_id_2 = '" + VMVServices.Web.Utils.ReportFilterParameter["SEGMENT_VALUE_ID"].ToString() + "'";
                    }
                }
                else if (segmentId == "3")
                {
                    if (VMVServices.Web.Utils.ReportFilterParameter["SEGMENT_VALUE_ID"] != null)
                    {
                        sqlQuery += " AND V.je_segment_id_3 = '" + VMVServices.Web.Utils.ReportFilterParameter["SEGMENT_VALUE_ID"].ToString() + "'";
                    }
                }
                else if (segmentId == "4")
                {
                    if (VMVServices.Web.Utils.ReportFilterParameter["SEGMENT_VALUE_ID"] != null)
                    {
                        sqlQuery += " AND V.je_segment_id_4 = '" + VMVServices.Web.Utils.ReportFilterParameter["SEGMENT_VALUE_ID"].ToString() + "'";
                    }
                }
                else if (segmentId == "5")
                {
                    if (VMVServices.Web.Utils.ReportFilterParameter["SEGMENT_VALUE_ID"] != null)
                    {
                        sqlQuery += " AND V.je_segment_id_5 = '" + VMVServices.Web.Utils.ReportFilterParameter["SEGMENT_VALUE_ID"].ToString() + "'";
                    }
                }
                else if (segmentId == "6")
                {
                    if (VMVServices.Web.Utils.ReportFilterParameter["SEGMENT_VALUE_ID"] != null)
                    {
                        sqlQuery += " AND V.je_segment_id_6 = '" + VMVServices.Web.Utils.ReportFilterParameter["SEGMENT_VALUE_ID"].ToString() + "'";
                    }
                }
              
                
                if (post != "1")
                {
                    sqlQuery += " and v.WORKFLOW_COMPLETION_STATUS=1";
                }
                


            }
            return sqlQuery;
        }
    }
}
