﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;

using System.Data.EntityClient;



using System.Data.Entity.Infrastructure;
namespace FIN.DAL.GL
{
    public class AccountingGroups_DAL
    {
        static string sqlQuery = "";

        public static string getTopAccountGroupSummaryReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery += "  SELECT * FROM GL_ACCOUNT_GROUP_SUM ss WHERE ROWNUM > 0 ";
            sqlQuery += "  and ss.ACCOUNT_GROUP_ID in ";
            sqlQuery += "  (select distinct t.level1_id";
            sqlQuery += "    from temp_gl_acc_id_name t";
            sqlQuery += "    where t.level1_id is not null) ";
            if (VMVServices.Web.Utils.ReportFilterParameter != null || VMVServices.Web.Utils.ReportFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.PERIOD_NAME] != null)
                {
                    sqlQuery += " AND ss.PERIOD_NAME = '" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.PERIOD_NAME].ToString() + "'";
                }

            }
            sqlQuery += " ORDER BY ss.ACCOUNT_GROUP_DESCRIPTION,ss.PERIOD_FROM_DT ";
            return sqlQuery;
        }
        public static string getTopBalanceAccountGroupSummaryReportData()
        {
            sqlQuery = string.Empty;
            //sqlQuery += " SELECT * FROM gl_account_group_summary_r TTB WHERE ROWNUM > 0 ";
            //sqlQuery += " ORDER BY TTB.ACCOUNT_GROUP_ID ";

            sqlQuery += " SELECT * FROM VW_TOP_BALANCE V WHERE ROWNUM > 0";
            if (VMVServices.Web.Utils.ReportFilterParameter["FromGroupName"] != null)
            {
                sqlQuery += " AND V.ACCT_GRP_DESC >=  '" + VMVServices.Web.Utils.ReportFilterParameter["FromGroupName"].ToString() + "'";
            }

            if (VMVServices.Web.Utils.ReportFilterParameter["ToGroupName"] != null)
            {
                sqlQuery += " AND V.ACCT_GRP_DESC <=  '" + VMVServices.Web.Utils.ReportFilterParameter["ToGroupName"].ToString() + "'";
            }
            return sqlQuery;
        }
        public static string getTopBalanceAccountGroups()
        {
            sqlQuery = string.Empty;
            sqlQuery += "  select distinct t.acct_grp_id as GROUP_ID,g.acct_grp_desc as Group_Name from temp_top_bal t,gl_acct_groups g";
            sqlQuery += " where g.acct_grp_id=t.acct_grp_id";
            sqlQuery += " order by group_name";
            return sqlQuery;
        }
        public static string GetExpensesBalance4Report()
        {
            sqlQuery = string.Empty;
            sqlQuery += " SELECT * FROM VW_TOP5EXPENCES_GROUP  V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["PERIOD"] != null)
                {
                    sqlQuery += " AND V.period_id = '" + VMVServices.Web.Utils.ReportViewFilterParameter["PERIOD"].ToString() + "'";
                }
            }

            return sqlQuery;
        }


        public static DataSet getSP_GL_Period_Balance(string global_segment_id, DateTime dtpFromDate, DateTime dtp_ToDate, string str_unposted,string str_lang)
        {
            try
            {
                string strValue = string.Empty;
                string strNextCode = string.Empty;
                string strErrMsg = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "GL_PKG.gl_Period_Balance";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add(new OracleParameter("@p_org_id", OracleDbType.Varchar2, 250)).Value = VMVServices.Web.Utils.OrganizationID;
                oraCmd.Parameters.Add(new OracleParameter("@p_global_segment_id", OracleDbType.Varchar2, 250)).Value = global_segment_id;
                oraCmd.Parameters.Add(new OracleParameter("@p_from_date", OracleDbType.Date, 250)).Value = dtpFromDate.ToString("dd/MMM/yyyy");
                oraCmd.Parameters.Add(new OracleParameter("@p_to_date", OracleDbType.Date, 250)).Value = dtp_ToDate.ToString("dd/MMM/yyyy");
                oraCmd.Parameters.Add(new OracleParameter("@p_unposted", OracleDbType.Varchar2, 250)).Value = str_unposted;
                oraCmd.Parameters.Add(new OracleParameter("@p_lang", OracleDbType.Varchar2, 250)).Value = str_lang;
                string str_dec = VMVServices.Web.Utils.DecimalPrecision;
                string str_Query = "  select 'EXPAND' as B_TYPE,A.ACCT_GRP_ID,to_char(ROUND(A.OPENING_BALANCE," + str_dec + ")) as OPENING_BALANCE ,to_char(ROUND(A.TRANSACTION_DEBIT," + str_dec + ")) as TRANSACTION_DEBIT ,to_char(ROUND(A.TRANSACTION_CREDIT," + str_dec + ")) as TRANSACTION_CREDIT,to_char(ROUND(A.CLOSING_BALANCE_DEBIT," + str_dec + ")) as CLOSING_BALANCE_DEBIT,to_char(ROUND(A.CLOSING_BALANCE_CREDIT," + str_dec + ")) as CLOSING_BALANCE_CREDIT ,to_char(ROUND(A.CLOSING_BALANCE," + str_dec + ")) as CLOSING_BALANCE,A.GROUP_LEVEL,A.PARENT_GROUP_ID,A.ACCT_GRP_NAME ";
                str_Query += "   ,LEVEL as num_or, lpad(' ',25-(to_number(a.group_level)* 4),' ') || a.acct_grp_name  as NEW_ACCT_GRP_NAME  from tmp_gl_period_balance a  ";
                str_Query += " where a.acct_grp_id is not null start with Group_level=(select Max(Group_level) from tmp_gl_period_balance) ";
                str_Query += " connect by prior  a.acct_grp_id=a.parent_group_id ";

                return DBMethod.ExecuteStoredProcedure(oraCmd, str_Query);



            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string getAcctGroupCB4Period(string str_Period_id)
        {
            sqlQuery = string.Empty;
            return sqlQuery;
        }

        public static string getAccountGroupSummaryReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery += " SELECT * FROM gl_account_group_sum V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportFilterParameter != null || VMVServices.Web.Utils.ReportFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportFilterParameter["AccFromGroup"] != null)
                {
                    sqlQuery += " AND V.account_group_description >= '" + VMVServices.Web.Utils.ReportFilterParameter["AccFromGroup"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["AccToGroup"] != null)
                {
                    sqlQuery += " AND V.account_group_description <= '" + VMVServices.Web.Utils.ReportFilterParameter["AccToGroup"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["WITHZERO"] == "FALSE")
                {
                    sqlQuery += " AND (abs(V.opening_balance) >0 OR abs(V.transaction_debit) >0 or abs(v.transaction_credit)>0)  ";
                }
                sqlQuery += " order by account_group_id ";

            }

            return sqlQuery;
        }

        public static string getGroupName()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT GAG.ACCT_GRP_ID AS GROUP_ID, GAG.ACCT_GRP_ID ||' - '|| GAG.ACCT_GRP_DESC" + VMVServices.Web.Utils.LanguageCode + " AS GROUP_NAME";
            sqlQuery += " FROM GL_ACCT_GROUPS GAG ";
            sqlQuery += " WHERE GAG.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += "  and GAG.acct_org_id='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " AND GAG.ENABLED_FLAG = 1 ";
            sqlQuery += " ORDER BY GAG.ACCT_GRP_DESC asc ";

            return sqlQuery;
        }
        public static string getGroupName(string grpId = "")
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT GAG.ACCT_GRP_ID AS GROUP_ID,GAG.ACCT_GRP_DESC" + VMVServices.Web.Utils.LanguageCode + ", GAG.ACCT_GRP_ID ||' - '|| GAG.ACCT_GRP_DESC" + VMVServices.Web.Utils.LanguageCode + " AS GROUP_NAME ";
            sqlQuery += " FROM GL_ACCT_GROUPS GAG ";
            sqlQuery += " WHERE GAG.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " AND GAG.ENABLED_FLAG = 1 ";
            sqlQuery += "  and GAG.acct_org_id='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " AND GAG.ACCT_GRP_ID='" + grpId + "'";
            sqlQuery += " ORDER BY GAG.ACCT_GRP_DESC asc ";

            return sqlQuery;
        }
        public static string getGroupDetails()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT GAG.ACCT_GRP_ID AS GROUP_ID, GAG.ACCT_GRP_DESC" + VMVServices.Web.Utils.LanguageCode + " AS GROUP_NAME";
            sqlQuery += " FROM GL_ACCT_GROUPS GAG ";
            sqlQuery += " WHERE GAG.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " AND GAG.ENABLED_FLAG = 1 ";
            sqlQuery += "  and GAG.acct_org_id='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " ORDER BY GAG.ACCT_GRP_DESC asc ";

            return sqlQuery;
        }
        public static DataSet GetSP_AccountGroupSummary(string global_segment_id, DateTime dtpFromDate, DateTime dtp_ToDate, string str_groupid, string str_unposted, string str_Query)
        {
            try
            {
                string strValue = string.Empty;
                string strNextCode = string.Empty;
                string strErrMsg = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "GL_PKG.GL_Account_Group_Summary";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add(new OracleParameter("@p_org_id", OracleDbType.Varchar2, 250)).Value = VMVServices.Web.Utils.OrganizationID;
                oraCmd.Parameters.Add(new OracleParameter("@p_global_segment_id", OracleDbType.Varchar2, 250)).Value = global_segment_id;
                oraCmd.Parameters.Add(new OracleParameter("@p_from_date", OracleDbType.Date, 250)).Value = dtpFromDate.ToString("dd/MMM/yyyy");
                oraCmd.Parameters.Add(new OracleParameter("@p_to_date", OracleDbType.Date, 250)).Value = dtp_ToDate.ToString("dd/MMM/yyyy");
                if (str_groupid == "ALL")
                {

                    oraCmd.Parameters.Add(new OracleParameter("@p_group_id", OracleDbType.Varchar2, 250)).Value = null;
                }
                else
                {
                    oraCmd.Parameters.Add(new OracleParameter("@p_group_id", OracleDbType.Varchar2, 250)).Value = str_groupid;
                }
                oraCmd.Parameters.Add(new OracleParameter("@p_unposted", OracleDbType.Varchar2, 250)).Value = str_unposted;



                return DBMethod.ExecuteStoredProcedure(oraCmd, str_Query);



            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static DataSet GetSP_TopBalanceAccountGroupSummary(string str_Query)
        {
            try
            {
                string strValue = string.Empty;
                string strNextCode = string.Empty;
                string strErrMsg = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "GL_PKG.prc_top_bal";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add(new OracleParameter("@p_org_id", OracleDbType.Varchar2, 250)).Value = VMVServices.Web.Utils.OrganizationID;

                return DBMethod.ExecuteStoredProcedure(oraCmd, str_Query);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static void GetSP_Proc_Top_Group_Level_bal(string levelVal)
        {
            try
            {
                string strValue = string.Empty;
                string strNextCode = string.Empty;
                string strErrMsg = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "GL_PKG.Proc_Top_Group_Level_bal";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add(new OracleParameter("@p_org_id", OracleDbType.Varchar2, 250)).Value = VMVServices.Web.Utils.OrganizationID;
                oraCmd.Parameters.Add(new OracleParameter("@p_level", OracleDbType.Int32, 250)).Value = levelVal;
                DBMethod.ExecuteStoredProcedure(oraCmd);
              
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static DataSet GetSP_MonthlySalaryRevealed(string payroll_periods_id, string str_Query)
        {
            try
            {
                string strValue = string.Empty;
                string strNextCode = string.Empty;
                string strErrMsg = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "PKG_PAYROLL_REPORTS.PROC_FINAL_RUN_PAYROLL_REPORT";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add(new OracleParameter("@p_org_id", OracleDbType.Varchar2, 250)).Value = VMVServices.Web.Utils.OrganizationID;
                oraCmd.Parameters.Add(new OracleParameter("@p_payroll_period", OracleDbType.Varchar2, 250)).Value = payroll_periods_id;




                return DBMethod.ExecuteStoredProcedure(oraCmd, str_Query);



            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static DataSet GetSP_MonthlySalaryRevealedTrail(string payroll_periods_id, string str_Query)
        {
            try
            {
                string strValue = string.Empty;
                string strNextCode = string.Empty;
                string strErrMsg = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "PKG_PAYROLL_REPORTS.PROC_TRIAL_RUN_PAYROLL_REPORT";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add(new OracleParameter("@p_org_id", OracleDbType.Varchar2, 250)).Value = VMVServices.Web.Utils.OrganizationID;
                oraCmd.Parameters.Add(new OracleParameter("@p_payroll_period", OracleDbType.Varchar2, 250)).Value = payroll_periods_id;




                return DBMethod.ExecuteStoredProcedure(oraCmd, str_Query);



            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void GetSP_AccountGroupLinksCode(string grpId = "")
        {
            try
            {
                string strValue = string.Empty;
                string strNextCode = string.Empty;
                string strErrMsg = string.Empty;

                OracleCommand oraCmd = new OracleCommand();

                oraCmd.CommandText = "GL_PKG.GL_ACC_GRP_LINK";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add(new OracleParameter("@p_org_id", OracleDbType.Varchar2, 250)).Value = VMVServices.Web.Utils.OrganizationID;
                oraCmd.Parameters.Add(new OracleParameter("@p_acc_grp_id", OracleDbType.Varchar2, 250)).Value = DBNull.Value;

                DBMethod.ExecuteStoredProcedure(oraCmd);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static void GetSP_AccountGroupLinksCodeName(string grpId = "")
        {
            try
            {
                string strValue = string.Empty;
                string strNextCode = string.Empty;
                string strErrMsg = string.Empty;

                OracleCommand oraCmd = new OracleCommand();

                oraCmd.CommandText = "GL_ACC_GRP_CODE.GL_ACC_GRP_LINK_Id_Name";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add(new OracleParameter("@p_org_id", OracleDbType.Varchar2, 250)).Value = VMVServices.Web.Utils.OrganizationID;
                oraCmd.Parameters.Add(new OracleParameter("@p_acc_grp_id", OracleDbType.Varchar2, 250)).Value = DBNull.Value;

                DBMethod.ExecuteStoredProcedure(oraCmd);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GetAccountGroupLinkWithCode()
        {
            sqlQuery = string.Empty;

            sqlQuery = " select rownum,t.level1, t.level2, t.level3, t.level4, t.level5, acc.*";
            sqlQuery += " from temp_gl_acc_grp_link_hdr t, TABLE(t.tools) acc";
            sqlQuery += " order by t.level1, t.level2, t.level3, t.level4, t.level5 asc";

            return sqlQuery;
        }
        public static string GetAccountGroupLinkWithCodeName()
        {
            sqlQuery = string.Empty;

            sqlQuery = " select distinct rownum,t.level1, t.level2, t.level3, t.level4, t.level5,t.level1_id as level1_Id, rownum || t.level2_id as level2_Id, rownum || t.level3_id as level3_Id, rownum || t.level4_id as level4_Id, rownum || t.level5_id as level5_Id, acc.*,acc_id.*";
            //sqlQuery = " select rownum,t.level1, t.level2, t.level3, t.level4, t.level5,t.level1_Id, t.level2_Id, t.level3_Id, t.level4_Id, t.level5_Id, acc.*,acc_id.*";
            sqlQuery += " from TEMP_GL_ACC_Id_Name t, TABLE(t.tools) acc, TABLE(t.ACC_VALUE) acc_id";
            sqlQuery += " order by t.level1, t.level2, t.level3, t.level4, t.level5 asc";

            return sqlQuery;
        }
        public static string getLevel1()
        {
            sqlQuery = string.Empty;

            sqlQuery = "   select distinct t.level1,t.level1_id as level1_Id";//,(select acc_id.* from TABLE(t.tools) acc_id)  as acc_code_id";
            sqlQuery += "   from temp_gl_acc_id_name t"; ;
            sqlQuery += "   where t.level1_id is not null";
            //      sqlQuery += "  and t.level1_id = '" + grpId + "'";
            sqlQuery += " order by t.level1 asc";
            return sqlQuery;
        }
        public static string GetExpensesGroup()
        {
            sqlQuery = string.Empty;

            sqlQuery += "   select distinct t.level1, t.level1_id as level1_Id";
            sqlQuery += "   from temp_gl_acc_id_name t";
            sqlQuery += "   where t.level1_id is not null";
            sqlQuery += "   and upper(t.level1)=upper('Expenses')";
            sqlQuery += "   order by t.level1 asc";
            return sqlQuery;
        }
        public static string GetExpensesBalance(string str_PeriodId)
        {
            sqlQuery = string.Empty;

            sqlQuery += "  select * from (select  gj.group_id,gj.group_name, ";
            sqlQuery += "  gj.period_name, ";
            sqlQuery += "  gj.comp_id as org_id,gj.period_id,      abs(  sum(round((nvl(gj.cb_accounted_dr, 0) - nvl(gj.cb_accounted_cr, 0)), 3))) as BALANCE_AMT ";
            sqlQuery += "   from  ";
            sqlQuery += "    gl_je_summary_period  gj where substr(gj.acct_code,1,1)='E' ";
            sqlQuery += " AND gj.period_id='" + str_PeriodId + "'";
            sqlQuery += "   group by  gj.group_id,gj.group_name, ";
            sqlQuery += "  gj.period_name, ";
            sqlQuery += "  gj.comp_id,gj.period_id order by 5 desc)a where rownum <= 5 ";

            return sqlQuery;
        }
        public static string GetIncomeBalance(string str_Period_id)
        {
            sqlQuery = string.Empty;

            sqlQuery += "  select * from (select  gj.group_id,gj.group_name, ";
            sqlQuery += "  gj.period_name, ";
            sqlQuery += "  gj.comp_id as org_id,gj.period_id,       abs( sum(round((nvl(gj.cb_accounted_dr, 0) - nvl(gj.cb_accounted_cr, 0)), 3))) as BALANCE_AMT ";
            sqlQuery += "   from  ";
            sqlQuery += "    gl_je_summary_period  gj where substr(gj.acct_code,1,1)='R' ";
            sqlQuery += " AND gj.period_id='" + str_Period_id + "'";
            sqlQuery += "   group by  gj.group_id,gj.group_name, ";
            sqlQuery += "  gj.period_name, ";
            sqlQuery += "  gj.comp_id,gj.period_id order by 5 desc)a where rownum <= 5 ";

            return sqlQuery;
        }

        //public static string getLevel1Child(string grpId)
        //{
        //    sqlQuery = string.Empty;

        //    sqlQuery = "   select distinct t.level2,t.level2_id as level2_Id";
        //    sqlQuery += "   from temp_gl_acc_id_name t, TABLE(t.tools) acc, TABLE(t.ACC_VALUE) acc_id";
        //    sqlQuery += "   where t.level2_id is not null";
        //    sqlQuery += "  and t.level1_id = '" + grpId + "'";
        //    sqlQuery += " order by t.level2 asc";
        //    return sqlQuery;
        //}
        //public static string getLevel2Child(string grpId)
        //{
        //    sqlQuery = string.Empty;

        //    sqlQuery = "  select distinct t1.level3_id as level3_id,t1.level3";
        //    sqlQuery += "   from temp_gl_acc_id_name t1";
        //    sqlQuery += "  where t1.level3_id is not null";
        //    sqlQuery += "   and t1.level2_id =  '" + grpId + "'";
        //    sqlQuery += " order by t1.level3 asc";

        //    return sqlQuery;
        //}
        //public static string getLevel3Child(string grpId)
        //{
        //    sqlQuery = string.Empty;

        //    sqlQuery = "   select distinct t2.level4_id as level4_id,t2.level4";
        //    sqlQuery += "    from temp_gl_acc_id_name t2";
        //    sqlQuery += "   where t2.level4_id is not null";
        //    sqlQuery += "    and t2.level3_id =  '" + grpId + "'";
        //    sqlQuery += " order by t2.level4 asc";

        //    return sqlQuery;
        //}
        //public static string getLevel4Child(string grpId)
        //{
        //    sqlQuery = string.Empty;

        //    sqlQuery = "  select distinct  t3.level5_id as level5_id,t3.level5";
        //    sqlQuery += "  from temp_gl_acc_id_name t3";
        //    sqlQuery += "  where t3.level5_id is not null";
        //    sqlQuery += "  and t3.level4_id =  '" + grpId + "'";
        //    sqlQuery += " order by t3.level5 asc";

        //    return sqlQuery;
        //}

        public static string getLevel1Child(string grpId)
        {
            string sqlQuery1 = string.Empty;

            sqlQuery1 = "   select distinct t.level2_id as level2_Id,nvl(t.level2,' - ') as level2,level2 as levelvalue";//,(select acc_id.* from TABLE(t.tools) acc_id)  as acc_code_id";
            sqlQuery1 += "   from temp_gl_acc_id_name t";
            //       sqlQuery1 += "   where t.level2_id is not null";
            sqlQuery1 += "  where t.level1_id = '" + grpId + "'";
            sqlQuery1 += " order by levelvalue asc";
            return sqlQuery1;
        }
        public static string getLevel2Child(string grpId)
        {
            string sqlQuery2 = string.Empty;

            sqlQuery2 = "  select distinct t1.level3_id as level3_id,nvl(t1.level3,' - ') as level3,level3 as levelvalue";//,(select acc_id.* from TABLE(t1.tools) acc_id)  as acc_code_id";
            sqlQuery2 += "   from temp_gl_acc_id_name t1";
            // sqlQuery2 += "  where t1.level3_id is not null";
            sqlQuery2 += "   where t1.level2_id =  '" + grpId + "'";
            sqlQuery2 += " order by levelvalue asc";

            return sqlQuery2;
        }
        public static string getLevel3Child(string grpId)
        {
            string sqlQuery3 = string.Empty;

            sqlQuery3 = "   select distinct t2.level4_id as level4_id,nvl(t2.level4,' - ') as level4,level4 as levelvalue";//,(select acc_id.* from TABLE(t2.tools) acc_id)  as acc_code_id";
            sqlQuery3 += "    from temp_gl_acc_id_name t2";
            // sqlQuery3 += "   where t2.level4_id is not null";
            sqlQuery3 += "    where t2.level3_id =  '" + grpId + "'";
            sqlQuery3 += " order by levelvalue asc";

            return sqlQuery3;
        }
        public static string getLevel4Child(string grpId)
        {
            string sqlQuery4 = string.Empty;

            sqlQuery4 = "  select distinct  t3.level5_id as level5_id,nvl(t3.level5,' - ') as level5,level5 as levelvalue";//,(select acc_id.* from TABLE(t3.tools) acc_id)  as acc_code_id";
            sqlQuery4 += "  from temp_gl_acc_id_name t3";
            //   sqlQuery4 += "  where t3.level5_id is not null";
            sqlQuery4 += "  where t3.level4_id =  '" + grpId + "'";
            sqlQuery4 += " order by levelvalue asc";

            return sqlQuery4;
        }


        //with row num
        //public static string getLevel1Child(string grpId)
        //{
        //    string sqlQuery1 = string.Empty;

        //    sqlQuery1 = "   select distinct t.level2, rownum||t.level2_id as level2_Id";
        //    sqlQuery1 += "   from temp_gl_acc_id_name t, TABLE(t.tools) acc, TABLE(t.ACC_VALUE) acc_id";
        //    sqlQuery1 += "   where t.level2_id is not null";
        //    sqlQuery1 += "  and t.level1_id = '" + grpId + "'";
        //    sqlQuery1 += " order by t.level2 asc";
        //    return sqlQuery1;
        //}
        //public static string getLevel2Child(string grpId)
        //{
        //    string sqlQuery2 = string.Empty;

        //    sqlQuery2 = "  select distinct  rownum||t1.level3_id as level3_id,t1.level3";
        //    sqlQuery2 += "   from temp_gl_acc_id_name t1";
        //    sqlQuery2 += "  where t1.level3_id is not null";
        //    sqlQuery2 += "   and t1.level2_id =  '" + grpId + "'";
        //    sqlQuery2 += " order by t1.level3 asc";

        //    return sqlQuery2;
        //}
        //public static string getLevel3Child(string grpId)
        //{
        //    string sqlQuery3 = string.Empty;

        //    sqlQuery3 = "   select distinct  rownum||t2.level4_id as level4_id,t2.level4";
        //    sqlQuery3 += "    from temp_gl_acc_id_name t2";
        //    sqlQuery3 += "   where t2.level4_id is not null";
        //    sqlQuery3 += "    and t2.level3_id =  '" + grpId + "'";
        //    sqlQuery3 += " order by t2.level4 asc";

        //    return sqlQuery3;
        //}
        //public static string getLevel4Child(string grpId)
        //{
        //    string sqlQuery4 = string.Empty;

        //    sqlQuery4 = "  select distinct   rownum||t3.level5_id as level5_id,t3.level5";
        //    sqlQuery4 += "  from temp_gl_acc_id_name t3";
        //    sqlQuery4 += "  where t3.level5_id is not null";
        //    sqlQuery4 += "  and t3.level4_id =  '" + grpId + "'";
        //    sqlQuery4 += " order by t3.level5 asc";

        //    return sqlQuery4;
        //}

        // end


        public static string getAccCodes(string grpId, string level = "")
        {
            sqlQuery = string.Empty;


            string strValue = string.Empty;
            string strNextCode = string.Empty;
            string strErrMsg = string.Empty;

            if (level == "1")
            {
                sqlQuery = "    select   distinct t4.acct_code_desc" + VMVServices.Web.Utils.LanguageCode + " as acc_code_desc, t4.acct_code_id as acc_code_id, t4.acct_grp_id as acc_grp_id_value";
                sqlQuery += "   from gl_acct_codes t4 where t4.acct_code_id='" + grpId + "'";
                sqlQuery += "  order by acc_code_desc asc";
            }
            else
            {
                OracleCommand oraCmd = new OracleCommand();

                oraCmd.CommandText = "GL_ACC_GRP_CODE.Get_Acct_Code_Based_Group";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add(new OracleParameter("@p_org_id", OracleDbType.Varchar2, 250)).Value = VMVServices.Web.Utils.OrganizationID;
                oraCmd.Parameters.Add(new OracleParameter("@p_acc_grp_id", OracleDbType.Varchar2, 250)).Value = grpId;

                DBMethod.ExecuteStoredProcedure(oraCmd);


                sqlQuery = "   select  distinct t4.level10 as acc_code_desc, t4.level10_id as acc_code_id, t4.acc_grp_id_value";
                sqlQuery += "   from temp_gl_acc_grp_code_hier t4";
                sqlQuery += "  order by t4.level10 asc";
            }

            return sqlQuery;
        }

        public static string getAccGrpChildHierarchy(string nodeValue)
        {
            string sqlQuery4 = string.Empty;

            sqlQuery4 = "   select test.* from ( select distinct t.level6_id  as child1,t.level7_id as child2,t.level8_id as child3,t.level9_id as child4,t.level10_id as child5,'" + nodeValue + "' as child6";
            sqlQuery4 += "    from temp_gl_acc_id_name t";
            sqlQuery4 += "    where t.level5_id = '" + nodeValue + "'";
            sqlQuery4 += "    and t.level5_id is not null";
            sqlQuery4 += "    union";
            sqlQuery4 += "    select distinct t.level5_id  as child1,t.level6_id as child2,t.level7_id as child3,t.level8_id as child4,t.level9_id as child5,'" + nodeValue + "' as child6";
            sqlQuery4 += "    from temp_gl_acc_id_name t";
            sqlQuery4 += "    where t.level4_id ='" + nodeValue + "'";
            sqlQuery4 += "    and t.level4_id is not null";
            sqlQuery4 += "    union";
            sqlQuery4 += "    select distinct t.level4_id  as child1,t.level5_id as child2,t.level6_id as child3,t.level7_id as child4,t.level8_id as child5,'" + nodeValue + "' as child6";
            sqlQuery4 += "    from temp_gl_acc_id_name t";
            sqlQuery4 += "    where t.level3_id = '" + nodeValue + "'";
            sqlQuery4 += "    and t.level3_id is not null";
            sqlQuery4 += "    union";
            sqlQuery4 += "    select distinct t.level3_id  as child1,t.level4_id as child2,t.level5_id as child3,t.level6_id as child4,t.level7_id as child5,'" + nodeValue + "' as child6";
            sqlQuery4 += "    from temp_gl_acc_id_name t";
            sqlQuery4 += "    where t.level2_id = '" + nodeValue + "'";
            sqlQuery4 += "    and t.level2_id is not null";
            sqlQuery4 += "    union";
            sqlQuery4 += "    select distinct t.level2_id as child1,t.level3_id as child2,t.level4_id as child3,t.level5_id as child4,t.level6_id as child5,'" + nodeValue + "' as child6";
            sqlQuery4 += "    from temp_gl_acc_id_name t";
            sqlQuery4 += "    where t.level1_id ='" + nodeValue + "'";
            sqlQuery4 += "    and t.level1_id is not null";
            sqlQuery4 += "     ) test where (test.child1 is not null or test.child2 is not null or test.child3 is not null or test.child4 is not null or test.child5 is not null)";

            return sqlQuery4;
        }
        public static string getAccGrpHierarchy(string nodeValue)
        {
            string sqlQuery4 = string.Empty;

            sqlQuery4 = "   select t2.level1_id,t2.level2_id,t2.level3_id,t2.level4_id,t2.level5_id,t2.level6_id,t2.level7_id,t2.level8_id,t2.level9_id,t2.level10_id ";
            sqlQuery4 += "   from temp_gl_acc_id_name t2";
            sqlQuery4 += "   where (t2.level1_id = '" + nodeValue + "' or ";
            sqlQuery4 += "        t2.level2_id = '" + nodeValue + "' or ";
            sqlQuery4 += "        t2.level3_id = '" + nodeValue + "' or ";
            sqlQuery4 += "        t2.level4_id = '" + nodeValue + "' or ";
            sqlQuery4 += "       t2.level5_id = '" + nodeValue + "' or ";
            sqlQuery4 += "       t2.level6_id = '" + nodeValue + "' or ";
            sqlQuery4 += "       t2.level7_id = '" + nodeValue + "' or ";
            sqlQuery4 += "       t2.level8_id = '" + nodeValue + "' or ";
            sqlQuery4 += "       t2.level9_id = '" + nodeValue + "' or ";
            sqlQuery4 += "      t2.level10_id = '" + nodeValue + "')";
            return sqlQuery4;
        }
        public static string getAccCodeParent(string nodeValue)
        {
            string sqlQuery4 = string.Empty;

            sqlQuery4 = "   select t2.level1_id,t2.level2_id,t2.level3_id,t2.level4_id,t2.level5_id,t2.level6_id,t2.level7_id,t2.level8_id,t2.level9_id,t2.level10_id ";
            sqlQuery4 += "   from temp_gl_acc_id_name t2";
            sqlQuery4 += "   where (t2.level1_id in";
            sqlQuery4 += "        (select g.acct_grp_id";
            sqlQuery4 += "            from gl_acct_codes g";
            sqlQuery4 += "          where g.acct_code_id = '" + nodeValue + "') or";
            sqlQuery4 += "       t2.level2_id in";
            sqlQuery4 += "       (select g.acct_grp_id";
            sqlQuery4 += "          from gl_acct_codes g";
            sqlQuery4 += "         where g.acct_code_id =  '" + nodeValue + "') or ";
            sqlQuery4 += "     t2.level3_id in";
            sqlQuery4 += "     (select g.acct_grp_id";
            sqlQuery4 += "         from gl_acct_codes g";
            sqlQuery4 += "        where g.acct_code_id =  '" + nodeValue + "') or";
            sqlQuery4 += "     t2.level4_id in";
            sqlQuery4 += "    (select g.acct_grp_id";
            sqlQuery4 += "       from gl_acct_codes g";
            sqlQuery4 += "      where g.acct_code_id = '" + nodeValue + "') or";
            sqlQuery4 += "    t2.level5_id in";
            sqlQuery4 += "    (select g.acct_grp_id";
            sqlQuery4 += "       from gl_acct_codes g";
            sqlQuery4 += "      where g.acct_code_id = '" + nodeValue + "'))";

            return sqlQuery4;
        }

        public static string GetPreviousYear(string calDtlId)
        {
            string sqlQuery4 = string.Empty;


            sqlQuery4 = "      select k.cal_dtl_id";
            sqlQuery4 += "    from gl_acct_calendar_dtl k";
            sqlQuery4 += "   where k.cal_acct_year in";
            sqlQuery4 += "    (select to_char(j.cal_acct_year - 1) as cal_acct_year";
            sqlQuery4 += "      from gl_acct_calendar_dtl j";
            sqlQuery4 += "    where j.cal_dtl_id = '" + calDtlId + "')";
            sqlQuery4 += "    and rownum=1";
            return sqlQuery4;
        }
        public static string IsCodeORGroup(string nodeValue)
        {
            string sqlQuery4 = string.Empty;


            sqlQuery4 += "  select '1' as grpaccname from gl_acct_codes g";
            sqlQuery4 += "   where g.acct_code_id ='" + nodeValue + "'";
            sqlQuery4 += "  union ";
            sqlQuery4 += "  select '0' as grpaccname from gl_acct_groups g";
            sqlQuery4 += "  where g.acct_grp_id ='" + nodeValue + "'";

            return sqlQuery4;
        }

        //          public static string getAccCodeParent(string nodeValue)
        //        {
        //            string sqlQuery4 = string.Empty;

        //   sqlQuery4 = "        select distinct t2.level1_id,t2.level2_id,t2.level3_id,t2.level4_id,t2.level5_id,t2.level6_id,t2.level7_id,t2.level8_id,t2.level9_id,t2.level10_id";
        // sqlQuery4 += "    from temp_gl_acc_id_name t2";
        //sqlQuery4 += "    where (t2.level1_id = 'ACC_GRP-0000000161' or
        //      sqlQuery4 += "    t2.level2_id = 'ACC_GRP-0000000161' or
        //      sqlQuery4 += "    t2.level3_id = 'ACC_GRP-0000000161' or
        //      sqlQuery4 += "    t2.level4_id = 'ACC_GRP-0000000161' or
        //     sqlQuery4 += "     t2.level5_id = 'ACC_GRP-0000000161' or
        //     sqlQuery4 += "     t2.level6_id = 'ACC_GRP-0000000161' or
        //     sqlQuery4 += "     t2.level7_id = 'ACC_GRP-0000000161' or
        //     sqlQuery4 += "     t2.level8_id = 'ACC_GRP-0000000161' or
        //      sqlQuery4 += "    t2.level9_id = 'ACC_GRP-0000000161' or
        //      sqlQuery4 += "    t2.level10_id = 'ACC_GRP-0000000161');

        //                return sqlQuery4;
        //        }

        //public static string getLevel2Child(string grpId)
        //{
        //    sqlQuery = string.Empty;

        //    sqlQuery = "  select distinct t1.level3_id";
        //    sqlQuery += "   from temp_gl_acc_id_name t1";
        //    sqlQuery += "  where t1.level3_id is not null";
        //    sqlQuery += "   and t1.level2_id in";
        //    sqlQuery += "  (select distinct t.level2_id";
        //    sqlQuery += "   from temp_gl_acc_id_name t";
        //    sqlQuery += "  where t.level2_id is not null";
        //    sqlQuery += "  and (t.level1_id =  '" + grpId + "'";
        //    sqlQuery += "      ))";
        //    return sqlQuery;
        //}
        //public static string getLevel3Child(string grpId)
        //{
        //    sqlQuery = string.Empty;

        //    sqlQuery = "   select distinct t2.level4_id";
        //    sqlQuery += "    from temp_gl_acc_id_name t2";
        //    sqlQuery += "   where t2.level4_id is not null";
        //    sqlQuery += "    and t2.level3_id in";
        //    sqlQuery += "  (select distinct t1.level3_id";
        //    sqlQuery += "    from temp_gl_acc_id_name t1";
        //    sqlQuery += "   where t1.level3_id is not null";
        //    sqlQuery += "     and t1.level2_id in";
        //    sqlQuery += "   (select distinct t.level2_id";
        //    sqlQuery += "     from temp_gl_acc_id_name t";
        //    sqlQuery += "   where t.level2_id is not null";
        //    sqlQuery += "    and (t.level1_id =  '" + grpId + "'";
        //    sqlQuery += "   )))";

        //    return sqlQuery;
        //}
        //public static string getLevel4Child(string grpId)
        //{
        //    sqlQuery = string.Empty;

        //    sqlQuery = "  select distinct t3.level5_id";
        //    sqlQuery += "  from temp_gl_acc_id_name t3";
        //    sqlQuery += "  where t3.level5_id is not null";
        //    sqlQuery += "  and t3.level4_id in";
        //    sqlQuery += "  (select distinct t2.level4_id";
        //    sqlQuery += "  from temp_gl_acc_id_name t2";
        //    sqlQuery += "  where t2.level4_id is not null";
        //    sqlQuery += "  and t2.level3_id in";
        //    sqlQuery += "  (select distinct t1.level3_id";
        //    sqlQuery += "  from temp_gl_acc_id_name t1";
        //    sqlQuery += "  where t1.level3_id is not null";
        //    sqlQuery += "  and t1.level2_id in";
        //    sqlQuery += "  (select distinct t.level2_id";
        //    sqlQuery += "  from temp_gl_acc_id_name t";
        //    sqlQuery += "  where t.level2_id is not null";
        //    sqlQuery += "    and (t.level1_id =  '" + grpId + "'";
        //    sqlQuery += "  ) )))";

        //    return sqlQuery;
        //}
        //public static string getLevel4Child(string grpId)
        //{
        //    sqlQuery = string.Empty;

        //    sqlQuery = "  select distinct t3.level5_id";
        //    sqlQuery += "  from temp_gl_acc_id_name t3";
        //    sqlQuery += "  where t3.level5_id is not null";
        //    sqlQuery += "  and t3.level4_id in";
        //    sqlQuery += "  (select distinct t2.level4_id";
        //    sqlQuery += "  from temp_gl_acc_id_name t2";
        //    sqlQuery += "  where t2.level4_id is not null";
        //    sqlQuery += "  and t2.level3_id in";
        //    sqlQuery += "  (select distinct t1.level3_id";
        //    sqlQuery += "  from temp_gl_acc_id_name t1";
        //    sqlQuery += "  where t1.level3_id is not null";
        //    sqlQuery += "  and t1.level2_id in";
        //    sqlQuery += "  (select distinct t.level2_id";
        //    sqlQuery += "  from temp_gl_acc_id_name t";
        //    sqlQuery += "  where t.level2_id is not null";
        //    sqlQuery += "    and (t.level1_id =  '" + grpId + "'";
        //    sqlQuery += "  ) )))";

        //    return sqlQuery;
        //}
    }
}
