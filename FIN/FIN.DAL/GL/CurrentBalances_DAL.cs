﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;

using System.Data.EntityClient;



using System.Data.Entity.Infrastructure;

namespace FIN.DAL.GL
{
   public class CurrentBalances_DAL
    {
        static string sqlQuery = "";
        public static string getReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery += " SELECT * FROM GL_CURRENT_BALANCES  V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["PERIOD_NUMBER"] != null)
                {
                    sqlQuery += " AND V.PERIOD_NAME = '" + VMVServices.Web.Utils.ReportViewFilterParameter["PERIOD_NUMBER"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["SEGMENT_VALUE"] != null)
                {
                    sqlQuery += " AND V.GLOBAL_SEGMENT = '" + VMVServices.Web.Utils.ReportViewFilterParameter["SEGMENT_VALUE"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["FromAccount"] != null)
                {
                    sqlQuery += " AND V.Acct_Code >=  '" + VMVServices.Web.Utils.ReportFilterParameter["FromAccount"].ToString() + "'";
                }

                if (VMVServices.Web.Utils.ReportFilterParameter["ToAccount"] != null)
                {
                    sqlQuery += " AND V.Acct_Code <=  '" + VMVServices.Web.Utils.ReportFilterParameter["ToAccount"].ToString() + "'";
                }

                if (VMVServices.Web.Utils.ReportFilterParameter["From_DebitAmt"] != null || VMVServices.Web.Utils.ReportFilterParameter["To_DebitAmt"] != null || VMVServices.Web.Utils.ReportFilterParameter["From_CreditAmt"] != null || VMVServices.Web.Utils.ReportFilterParameter["To_CreditAmt"] != null)
                {
                    sqlQuery += " AND ( ";
                    if (VMVServices.Web.Utils.ReportFilterParameter["From_DebitAmt"] != null && VMVServices.Web.Utils.ReportFilterParameter["To_DebitAmt"] != null)
                    {
                        sqlQuery += " (V.Accounted_Dr >= '" + (VMVServices.Web.Utils.ReportFilterParameter["From_DebitAmt"] == null ? "0" : VMVServices.Web.Utils.ReportFilterParameter["From_DebitAmt"].ToString()) + "')  ";
                        sqlQuery += " AND ( V.Accounted_Dr <= '" + (VMVServices.Web.Utils.ReportFilterParameter["To_DebitAmt"] == null ? "0" : VMVServices.Web.Utils.ReportFilterParameter["To_DebitAmt"].ToString()) + "') ";

                    }
                    else if (VMVServices.Web.Utils.ReportFilterParameter["From_DebitAmt"] != null)
                    {
                        sqlQuery += " (V.Accounted_Dr >= '" + (VMVServices.Web.Utils.ReportFilterParameter["From_DebitAmt"] == null ? "0" : VMVServices.Web.Utils.ReportFilterParameter["From_DebitAmt"].ToString()) + "')  ";


                    }
                    else if (VMVServices.Web.Utils.ReportFilterParameter["To_DebitAmt"] != null)
                    {
                        sqlQuery += "  (V.Accounted_Dr <= '" + (VMVServices.Web.Utils.ReportFilterParameter["To_DebitAmt"] == null ? "0" : VMVServices.Web.Utils.ReportFilterParameter["To_DebitAmt"].ToString()) + "') ";

                    }

                    if ((VMVServices.Web.Utils.ReportFilterParameter["From_DebitAmt"] != null || VMVServices.Web.Utils.ReportFilterParameter["To_DebitAmt"] != null) && (VMVServices.Web.Utils.ReportFilterParameter["From_CreditAmt"] != null || VMVServices.Web.Utils.ReportFilterParameter["To_CreditAmt"] != null))
                    { sqlQuery += " OR "; }


                    if (VMVServices.Web.Utils.ReportFilterParameter["From_CreditAmt"] != null && VMVServices.Web.Utils.ReportFilterParameter["To_CreditAmt"] != null)
                    {
                        sqlQuery += " (V.Accounted_Cr >= '" + (VMVServices.Web.Utils.ReportFilterParameter["From_CreditAmt"] == null ? "0" : VMVServices.Web.Utils.ReportFilterParameter["From_CreditAmt"].ToString()) + "') ";
                        sqlQuery += " AND ( V.Accounted_Cr <= '" + (VMVServices.Web.Utils.ReportFilterParameter["To_CreditAmt"] == null ? "0" : VMVServices.Web.Utils.ReportFilterParameter["To_CreditAmt"].ToString()) + "') ";
                    }
                    else if (VMVServices.Web.Utils.ReportFilterParameter["From_CreditAmt"] != null)
                    {
                        sqlQuery += " (V.Accounted_Cr >= '" + (VMVServices.Web.Utils.ReportFilterParameter["From_CreditAmt"] == null ? "0" : VMVServices.Web.Utils.ReportFilterParameter["From_CreditAmt"].ToString()) + "') ";

                    }
                    else if (VMVServices.Web.Utils.ReportFilterParameter["To_CreditAmt"] != null)
                    {
                        sqlQuery += "  ( V.Accounted_Cr <= '" + (VMVServices.Web.Utils.ReportFilterParameter["To_CreditAmt"] == null ? "0" : VMVServices.Web.Utils.ReportFilterParameter["To_CreditAmt"].ToString()) + "') ";
                    }


                    sqlQuery += " )";
                }


                //if (VMVServices.Web.Utils.ReportFilterParameter["From_DebitAmt"] != null)
                //{
                //    sqlQuery += " and V.Amount_Dr >= '" + VMVServices.Web.Utils.ReportFilterParameter["From_DebitAmt"] + "'";
                //}
                //if (VMVServices.Web.Utils.ReportFilterParameter["To_DebitAmt"] != null)
                //{
                //    sqlQuery += " and V.Amount_Dr <= '" + VMVServices.Web.Utils.ReportFilterParameter["To_DebitAmt"] + "'";
                //}
                //if (VMVServices.Web.Utils.ReportFilterParameter["From_CreditAmt"] != null)
                //{
                //    sqlQuery += " and V.Amount_Cr >= '" + VMVServices.Web.Utils.ReportFilterParameter["From_CreditAmt"] + "'";
                //}
                //if (VMVServices.Web.Utils.ReportFilterParameter["To_CreditAmt"] != null)
                //{
                //    sqlQuery += " and V.Amount_Cr <= '" + VMVServices.Web.Utils.ReportFilterParameter["To_CreditAmt"] + "'";
                //}
                if (VMVServices.Web.Utils.ReportFilterParameter["WITHZERO"] == "FALSE")
                {
                    sqlQuery += " AND (V.OPENING_BAL >0 OR V.Amount_Dr >0 or V.Amount_Cr > 0)  ";
                }
               
            }

            return sqlQuery;
        }
        public static string getPeriodNumber()
        {
            sqlQuery = string.Empty;
            sqlQuery += " SELECT DISTINCT PERIOD_NUMBER,PERIOD_NAME  ";
            sqlQuery += " FROM gl_je_summary ";
            


            return sqlQuery;
        }
        public static string getGlobalSegment()
        {
            sqlQuery = string.Empty;
            sqlQuery += " SELECT SV.SEGMENT_ID,SV.SEGMENT_VALUE" + VMVServices.Web.Utils.LanguageCode;
            sqlQuery += " FROM GL_SEGMENT_VALUES SV ";
            sqlQuery += " WHERE SV.WORKFLOW_COMPLETION_STATUS='1' ";
            sqlQuery += " AND SV.ENABLED_FLAG='1'";


            return sqlQuery;
        }
    }
}
