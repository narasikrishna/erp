﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.GL
{
    public class AccountStructure_DAL
    {

        static string sqlQuery = "";
        public static string getStructureName(string structure_name)
        {
            sqlQuery = string.Empty;
            sqlQuery = " select count(*) as counts ";
            sqlQuery += " from GL_ACCT_STRUCTURE AST ";
            sqlQuery += " where  UPPER(AST.ACCT_STRUCT_NAME" + VMVServices.Web.Utils.LanguageCode + ")  = upper(' " + structure_name + "')";

            return sqlQuery;
        }


        public static string getAcctStructDetails(string acct_struct_id)
        {
            sqlQuery = string.Empty;



            sqlQuery = " select a.acct_struct_dtl_id,s.segment_id,s.segment_name" + VMVServices.Web.Utils.LanguageCode + " as segment_name ,'N' AS DELETED,";
            sqlQuery += " CASE a.enabled_flag when '1' then 'TRUE' ELSE 'FALSE' END AS enabled_flag";
            sqlQuery += " from GL_ACCT_STRUCTURE_DTL a,gl_segments s";
            sqlQuery += " where a.segment_id = s.segment_id";
            sqlQuery += " and a.workflow_completion_status = 1";
            sqlQuery += " and a.acct_struct_id = '" + acct_struct_id + "'";


            return sqlQuery;
        }

        public static string getStructureNamefrm_Acctcode(string acct_struct_id)
        {
            sqlQuery = string.Empty;


            sqlQuery = " select AC.ACCT_STRUCT_ID";
            sqlQuery += " from  GL_ACCT_CODES AC";
            sqlQuery += " WHERE AC.ACCT_STRUCT_ID = '" + acct_struct_id + "'";
            return sqlQuery;
        }

    }
}
