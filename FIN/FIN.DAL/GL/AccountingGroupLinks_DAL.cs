﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.GL
{
    public class AccountingGroupLinks_DAL
    {
        static string sqlQuery = "";


        public static string getAccountingGroupLinksDetails(string str_GroupLinkID)
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT GAGLD.ACCT_GRP_LNK_DTL_ID, GAG.ACCT_GRP_ID,GAG.ACCT_GRP_DESC" + VMVServices.Web.Utils.LanguageCode + " AS GROUP_NAME, GAGLD.EFFECTIVE_START_DT, GAGLD.EFFECTIVE_END_DT";
            sqlQuery += " ,CASE GAGLD.ENABLED_FLAG WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS ENABLED_FLAG";
            sqlQuery += " ,'N' AS DELETED ";
            sqlQuery += " FROM GL_ACCT_GROUP_LINK_DTL GAGLD,GL_ACCT_GROUPS GAG ";
            sqlQuery += " WHERE GAGLD.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " AND GAGLD.ACCT_GRP_ID = gag.acct_grp_id";
            sqlQuery += " AND GAGLD.ACCT_GRP_LNK_ID = '" + str_GroupLinkID + "'";
            sqlQuery += " ORDER BY GAGLD.ACCT_GRP_LNK_DTL_ID ";

            return sqlQuery;
        }

        public static string getAccountingGroupDetails(string str_GroupID)
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT GAGLD.ACCT_GRP_LNK_DTL_ID, GAG.ACCT_GRP_ID,GAG.ACCT_GRP_DESC" + VMVServices.Web.Utils.LanguageCode + " AS GROUP_NAME, GAGLD.EFFECTIVE_START_DT, GAGLD.EFFECTIVE_END_DT";
            sqlQuery += " ,CASE GAGLD.ENABLED_FLAG WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS ENABLED_FLAG";
            sqlQuery += " ,'N' AS DELETED ";
            sqlQuery += " FROM GL_ACCT_GROUP_LINK_DTL GAGLD,GL_ACCT_GROUPS GAG ";
            sqlQuery += " WHERE GAGLD.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " AND GAGLD.ACCT_GRP_ID = gag.acct_grp_id";
            // sqlQuery += " AND GAG.ACCT_GRP_ID not in '" + str_GroupID + "'";
            sqlQuery += " AND NOT exists  '" + str_GroupID + "'";

            sqlQuery += "  and GAG.acct_org_id='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " ORDER BY GAGLD.ACCT_GRP_LNK_DTL_ID ";

            return sqlQuery;
        }
        public static string getAccountingGroupLinksReportDetails(string str_GroupLinkID)
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT * FROM VW_GROUP_LINK_DETAILS V WHERE ROWNUM > 0";

            if (VMVServices.Web.Utils.ReportFilterParameter != null || VMVServices.Web.Utils.ReportFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.SEGMENT_ID] != null)
                {
                    sqlQuery += " and v.SEGMENT_ID=" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.SEGMENT_ID].ToString();
                }
                if (VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.DATE] != null)
                {
                    sqlQuery += " and v.DATE =" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.DATE].ToString();
                }
            }



            return sqlQuery;
        }


        public static string getAccountGroupName()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT GAG.ACCT_GRP_ID AS GROUP_ID, GAG.ACCT_GRP_DESC" + VMVServices.Web.Utils.LanguageCode + " AS GROUP_NAME ";
            sqlQuery += " FROM GL_ACCT_GROUPS GAG ";
            sqlQuery += " WHERE GAG.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " AND GAG.ENABLED_FLAG = 1 ";
            sqlQuery += " AND (GAG.EFFECTIVE_END_DT IS NULL OR GAG.EFFECTIVE_END_DT > SYSDATE) ";
            sqlQuery += "  and GAG.acct_org_id='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " ORDER BY GROUP_NAME ";

            return sqlQuery;
        }

        public static string getAccountGroupName4SubGroup(string str_masterRecId)
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT GAG.ACCT_GRP_ID AS GROUP_ID, GAG.ACCT_GRP_DESC" + VMVServices.Web.Utils.LanguageCode + " AS GROUP_NAME ";
            sqlQuery += " FROM GL_ACCT_GROUPS GAG ";
            sqlQuery += " WHERE GAG.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " AND GAG.ENABLED_FLAG = 1 ";
            sqlQuery += " AND (GAG.EFFECTIVE_END_DT IS NULL OR GAG.EFFECTIVE_END_DT > SYSDATE) ";
            //sqlQuery += " AND GAG.ACCT_GRP_ID NOT IN (SELECT ACCT_GRP_ID FROM GL_ACCT_GROUP_LINK_HDR) ";
            // sqlQuery += " AND GAG.ACCT_GRP_ID NOT IN (SELECT ACCT_GRP_ID FROM GL_ACCT_GROUP_LINK._DTL WHERE ACCT_GRP_LNK_ID<>'" + str_masterRecId + "') ";
            sqlQuery += " AND NOT EXISTS (SELECT ACCT_GRP_ID FROM GL_ACCT_GROUP_LINK_DTL B WHERE B.ACCT_GRP_ID =GAG.ACCT_GRP_ID AND  ACCT_GRP_LNK_ID<>'" + str_masterRecId + "') ";
            sqlQuery += "  and GAG.acct_org_id='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " ORDER BY GROUP_NAME ";

            return sqlQuery;
        }
        public static string getReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery = "SELECT * FROM VW_GROUP_LINK_DETAILS V WHERE ROWNUM > 0 ";

            if (VMVServices.Web.Utils.ReportFilterParameter != null || VMVServices.Web.Utils.ReportFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.GROUP_ID] != null)
                {
                    sqlQuery += " AND V.ACCT_GRP_ID = '" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.GROUP_ID].ToString() + "'";
                }

            }


            return sqlQuery;
        }

        public static string getAccountGroupListsBasedLevel()
        {
            sqlQuery = string.Empty;

            //sqlQuery = "SELECT * FROM gl_group_acc_link t WHERE ROWNUM > 0 ";

            //if (VMVServices.Web.Utils.ReportFilterParameter != null || VMVServices.Web.Utils.ReportFilterParameter.Count != 0)
            //{
            //    if (VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.GROUP_ID] != null)
            //    {
            //        sqlQuery += "  and (t.level1_id = '" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.GROUP_ID].ToString() + "' or ";
            //        sqlQuery += "    t.level1_id = '" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.GROUP_ID].ToString() + "' or ";
            //        sqlQuery += "    t.level2_id = '" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.GROUP_ID].ToString() + "' or ";
            //        sqlQuery += "    t.level3_id = '" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.GROUP_ID].ToString() + "' or ";
            //        sqlQuery += "    t.level4_id = '" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.GROUP_ID].ToString() + "' or ";
            //        sqlQuery += "     t.level5_id = '" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.GROUP_ID].ToString() + "' )";
            //    }
            //}

            if (VMVServices.Web.Utils.ReportFilterParameter != null || VMVServices.Web.Utils.ReportFilterParameter.Count != 0)
            {

                sqlQuery += "     select t.level1_id,";
                sqlQuery += "       t.level2_id,";
                sqlQuery += "       t.level3_id,";
                sqlQuery += "      t.level4_id,";
                sqlQuery += "      t.level5_id,";
                sqlQuery += "     t.level1,";
                sqlQuery += "     t.level2,";
                sqlQuery += "    t.level3,";
                sqlQuery += "    t.level4,";
                sqlQuery += "    t.level5,t.acc_grp_id as acc_code, acc.*";
                sqlQuery += "    from TEMP_GL_ACC_Id_Name t, TABLE(t.tools) acc";
                if (VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.GROUP_ID] != null)
                {
                    sqlQuery += "   where (t.level1_id = '" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.GROUP_ID].ToString() + "')";
                }
                sqlQuery += "  union";
                sqlQuery += "  select null as level1_id,";
                sqlQuery += "      t.level2_id,";
                sqlQuery += "    t.level3_id,";
                sqlQuery += "   t.level4_id,";
                sqlQuery += "   t.level5_id,";
                sqlQuery += "   null as level1,";
                sqlQuery += "   t.level2,";
                sqlQuery += "   t.level3,";
                sqlQuery += "   t.level4,";
                sqlQuery += "   t.level5,t.acc_grp_id as acc_code, acc.*";
                sqlQuery += "   from TEMP_GL_ACC_Id_Name t, TABLE(t.tools) acc";
                if (VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.GROUP_ID] != null)
                {
                    sqlQuery += "   where (t.level2_id = '" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.GROUP_ID].ToString() + "')";
                }
                sqlQuery += "   union";
                sqlQuery += "   select null as level1_id,";
                sqlQuery += "   null as level2_id,";
                sqlQuery += "   t.level3_id,";
                sqlQuery += "   t.level4_id,";
                sqlQuery += "   t.level5_id,";
                sqlQuery += "   null as level1,";
                sqlQuery += "   null as level2,";
                sqlQuery += "   t.level3,";
                sqlQuery += "   t.level4,";
                sqlQuery += "   t.level5,t.acc_grp_id as acc_code, acc.*";
                sqlQuery += "  from TEMP_GL_ACC_Id_Name t, TABLE(t.tools) acc";
                if (VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.GROUP_ID] != null)
                {
                    sqlQuery += "   where (t.level3_id = '" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.GROUP_ID].ToString() + "')";
                }
                sqlQuery += "   union";
                sqlQuery += "  select null as level1_id,";
                sqlQuery += "   null as level2_id,";
                sqlQuery += "   null as level3_id,";
                sqlQuery += "   t.level4_id,";
                sqlQuery += "   t.level5_id,";
                sqlQuery += "   null as level1,";
                sqlQuery += "  null as level2,";
                sqlQuery += "   null as level3,";
                sqlQuery += "   t.level4,";
                sqlQuery += "   t.level5,t.acc_grp_id as acc_code, acc.*";
                sqlQuery += "   from TEMP_GL_ACC_Id_Name t, TABLE(t.tools) acc";
                if (VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.GROUP_ID] != null)
                {
                    sqlQuery += "   where (t.level4_id = '" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.GROUP_ID].ToString() + "')";
                }
                sqlQuery += " union";
                sqlQuery += "   select null as level1_id,";
                sqlQuery += "  null as level2_id,";
                sqlQuery += "   null as level3_id,";
                sqlQuery += "   null as level4_id,";
                sqlQuery += "   t.level5_id,";
                sqlQuery += "   null as level1,";
                sqlQuery += "  null as level2,";
                sqlQuery += "  null as level3,";
                sqlQuery += "  null as level4,";
                sqlQuery += "   t.level5,t.acc_grp_id as acc_code, acc.*";
                sqlQuery += "  from TEMP_GL_ACC_Id_Name t, TABLE(t.tools) acc";
                if (VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.GROUP_ID] != null)
                {
                    sqlQuery += "  where (t.level5_id = '" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.GROUP_ID].ToString() + "')";
                }
                sqlQuery += "  order by level1, level2, level3, level4, level5";

            }
            return sqlQuery;
        }

        public static string getGroupLinkDetails()
        {
            sqlQuery = string.Empty;
            sqlQuery += " SELECT G.ACCT_GRP_ID AS GROUP_ID, G.ACCT_GRP_DESC" + VMVServices.Web.Utils.LanguageCode + " AS GROUP_NAME ";
            sqlQuery += " FROM GL_ACCT_GROUPS G,GL_ACCT_GROUP_LINK_HDR GLH ";
            sqlQuery += " WHERE G.ACCT_GRP_ID=GLH.ACCT_GRP_ID ";
            sqlQuery += " AND G.WORKFLOW_COMPLETION_STATUS='1' ";
            sqlQuery += " AND G.ENABLED_FLAG='1'";
            sqlQuery += "  and g.acct_org_id='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " ORDER BY GROUP_NAME ";

            return sqlQuery;
        }

        public static string GetGroupName4Header(string str_Mode)
        {
            sqlQuery = string.Empty;
            sqlQuery += "SELECT G.ACCT_GRP_ID AS GROUP_ID, G.ACCT_GRP_DESC" + VMVServices.Web.Utils.LanguageCode + " AS GROUP_NAME ";
            sqlQuery += "FROM GL_ACCT_GROUPS G ";
            sqlQuery += "WHERE G.ACCT_GRP_ID<> '0' ";
            if (str_Mode == FINTableConstant.Add)
            {
                sqlQuery += " AND G.WORKFLOW_COMPLETION_STATUS='1' ";
                sqlQuery += " AND G.ENABLED_FLAG='1'";
                sqlQuery += " AND G.ACCT_GRP_ID NOT IN(SELECT ACCT_GRP_ID FROM GL_ACCT_GROUP_LINK_HDR)";
            }
            sqlQuery += "  and g.acct_org_id='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += "ORDER BY GROUP_NAME ";

            return sqlQuery;
        }


        public static string getAccountBalanceReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery = "SELECT * FROM vw_account_bal_ledger V WHERE ROWNUM > 0 ";

            if (VMVServices.Web.Utils.ReportFilterParameter != null || VMVServices.Web.Utils.ReportFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.PERIOD_ID] != null)
                {
                    sqlQuery += " and v.period_id='" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.PERIOD_ID].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.CODE_ID] != null)
                {
                    sqlQuery += " and v.acct_code_id='" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.CODE_ID].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["From_AccNo"] != null)
                {
                    sqlQuery += " and v.ACCOUNT_NO>='" + VMVServices.Web.Utils.ReportFilterParameter["From_AccNo"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["To_AccNo"] != null)
                {
                    sqlQuery += " and v.ACCOUNT_NO<='" + VMVServices.Web.Utils.ReportFilterParameter["To_AccNo"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["From_Value"] != null)
                {
                    sqlQuery += " and v.CURRENT_BALANCE>='" + VMVServices.Web.Utils.ReportFilterParameter["From_Value"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["To_Value"] != null)
                {
                    sqlQuery += " and v.CURRENT_BALANCE<='" + VMVServices.Web.Utils.ReportFilterParameter["To_Value"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["WITHZERO"] == "FALSE")
                {
                    sqlQuery += " AND abs(V.CURRENT_BALANCE) > 0  ";
                }

                //if (VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.DATE] != null)
                //{
                //    sqlQuery += " and v.DATE =" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.DATE].ToString();
                //}
            }



            return sqlQuery;
        }

    }
}
