﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.GL
{
    public class UserAccountCode_DAL
    {
        static string sqlQuery = "";


        public static string getUserAcctCodeDtl(string GL_PC_ID)
        {
            sqlQuery = string.Empty;


            sqlQuery = " SELECT PCD.GL_PC_ID,AC.ACCT_CODE_ID as CODE_ID,AC.ACCT_CODE_DESC" + VMVServices.Web.Utils.LanguageCode + " as CODE_NAME,'N' AS DELETED,";
            sqlQuery += " CASE PCD.ENABLED_FLAG WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS ENABLED_FLAG";
            sqlQuery += " FROM GL_PETTY_CASH_USER_ACCT_CODES PCD,GL_ACCT_CODES AC";
            sqlQuery += " WHERE PCD.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND PCD.GL_ACCT_CODE = AC.ACCT_CODE_ID";
            sqlQuery += " AND PCD.GL_PC_ID = '" + GL_PC_ID + "'";

            return sqlQuery;
        }

    }
}
