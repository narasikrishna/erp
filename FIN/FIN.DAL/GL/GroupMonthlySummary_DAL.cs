﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;

using System.Data.EntityClient;



using System.Data.Entity.Infrastructure;

namespace FIN.DAL.GL
{
   public class GroupMonthlySummary_DAL
    {
        static string sqlQuery = "";
        public static string getReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM gl_group_monthly_summary_r V WHERE ROWNUM > 0 ";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Group"] != null)
                {
                    sqlQuery += " AND V.ACCOUNT_GROUP_DESCRIPTION >= '" + VMVServices.Web.Utils.ReportViewFilterParameter["From_Group"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Group"] != null)
                {
                    sqlQuery += " AND V.ACCOUNT_GROUP_DESCRIPTION <= '" + VMVServices.Web.Utils.ReportViewFilterParameter["To_Group"].ToString() + "'";
                }
                //if (VMVServices.Web.Utils.ReportViewFilterParameter["FromPeriod"] != null)
                //{
                //    sqlQuery += " AND V.CUR_PERIOD_ID >= '" + VMVServices.Web.Utils.ReportViewFilterParameter["FromPeriod"].ToString() + "'";
                //}
                //if (VMVServices.Web.Utils.ReportViewFilterParameter["ToPeriod"] != null)
                //{
                //    sqlQuery += " AND V.CUR_PERIOD_ID <= '" + VMVServices.Web.Utils.ReportViewFilterParameter["ToPeriod"].ToString() + "'";
                //}
            }

            //if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            //{
            //    if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.INV_WH_ID] != null)
            //    {
            //        sqlQuery += " AND V.WH_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.INV_WH_ID].ToString() + "'";
            //    }
            //}
            return sqlQuery;
        }
        public static string getAccountGroupName()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT GAG.ACCT_GRP_ID AS GROUP_ID, GAG.ACCT_GRP_DESC" + VMVServices.Web.Utils.LanguageCode + " AS GROUP_NAME ";
            sqlQuery += " FROM GL_ACCT_GROUPS GAG ";
            sqlQuery += " WHERE GAG.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " AND GAG.ENABLED_FLAG = 1 ";
            sqlQuery += " AND (GAG.EFFECTIVE_END_DT IS NULL OR GAG.EFFECTIVE_END_DT > SYSDATE) ";
            sqlQuery += " ORDER BY GROUP_NAME ";

            return sqlQuery;
        }
        public static DataSet GetSP_GroupMonthlySummary(string global_segment_id, DateTime tb_Fromdate, DateTime tb_ToDate, string str_Query)
        {
            try
            {
                string strValue = string.Empty;
                string strNextCode = string.Empty;
                string strErrMsg = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "GL_PKG.GL_Account_Group_Monthly_Sum";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add(new OracleParameter("@p_org_id", OracleDbType.Varchar2, 250)).Value = VMVServices.Web.Utils.OrganizationID;
                oraCmd.Parameters.Add(new OracleParameter("@p_global_segment_id", OracleDbType.Varchar2, 250)).Value = global_segment_id;
                oraCmd.Parameters.Add(new OracleParameter("@p_from_date", OracleDbType.Date, 250)).Value = tb_Fromdate.ToString("dd/MMM/yyyy");
                oraCmd.Parameters.Add(new OracleParameter("@p_to_date", OracleDbType.Date, 250)).Value = tb_ToDate.ToString("dd/MMM/yyyy");



                return DBMethod.ExecuteStoredProcedure(oraCmd, str_Query);



            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static string getGlobalSegment()
        {
            sqlQuery = string.Empty;
            sqlQuery += " SELECT SV.SEGMENT_ID,SV.SEGMENT_VALUE" + VMVServices.Web.Utils.LanguageCode;
            sqlQuery += " FROM GL_SEGMENT_VALUES SV ";
            sqlQuery += " WHERE SV.WORKFLOW_COMPLETION_STATUS='1' ";
            sqlQuery += " AND SV.ENABLED_FLAG='1'";


            return sqlQuery;
        }
    }
}
