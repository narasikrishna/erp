﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.GL
{
    public class PaymentGL_DAL
    {
        static string sqlQuery = "";


        public static string getGLPaymentdtl(string PAY_HDR_ID)
        {
            sqlQuery = string.Empty;



            sqlQuery += "  SELECT PD.PAY_DTL_ID,PD.CHEQUE_DATE,TO_CHAR(ROUND(PD.AMOUNT," + VMVServices.Web.Utils.DecimalPrecision + ")) AS AMOUNT,PD.BENEFICIARY_NAME,PD.REASON_FOR_PAYMENT,";
            sqlQuery += "  AC.ACCT_CODE_ID,AC.ACCT_CODE||' - '||AC.ACCT_CODE_DESC AS ACCT_CODE,CH.CHECK_DTL_ID,CH.CHECK_NUMBER,'N' AS DELETED";
            sqlQuery += " ,JE_SEGMENT_ID_1,JE_SEGMENT_ID_2,JE_SEGMENT_ID_3,JE_SEGMENT_ID_4,JE_SEGMENT_ID_5,JE_SEGMENT_ID_6";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = PD.JE_SEGMENT_ID_1 ) AS SEGMENT_1_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = PD.JE_SEGMENT_ID_2 ) AS SEGMENT_2_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = PD.JE_SEGMENT_ID_3 ) AS SEGMENT_3_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = PD.JE_SEGMENT_ID_4 ) AS SEGMENT_4_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = PD.JE_SEGMENT_ID_5 ) AS SEGMENT_5_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = PD.JE_SEGMENT_ID_6 ) AS SEGMENT_6_TEXT";
            sqlQuery += "  FROM CA_CHECK_DTL CH,GL_ACCT_CODES AC,GL_PAYMENT_DTL PD";
            sqlQuery += "  WHERE CH.CHECK_DTL_ID (+) = PD.CHEQUE_NO";
            sqlQuery += "  AND AC.ACCT_CODE_ID = PD.ACCT_CODE";
            sqlQuery += "  AND PD.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += "  AND PD.PAY_HDR_ID = '" + PAY_HDR_ID + "'";

            return sqlQuery;
        }

        public static string GetPaymentDetailsRPT(string paymentNumber, string payHdrId = "")
        {
            sqlQuery = string.Empty;

            sqlQuery = "select * from VW_GL_PAYMENT_DETAILS V ";
            if (paymentNumber.Trim().Length > 0)
            {
                sqlQuery += " where V.payment_no='" + paymentNumber + "'";
            }
            else
            {
                sqlQuery += " where V.pay_hdr_id='" + payHdrId + "'";
            }

            return sqlQuery;
        }

        public static string getTransactionData(string fromDate)
        {
            sqlQuery = string.Empty;

            //sqlQuery = " select t.TransactionID, t.TransactionDate, t.ReferenceNumber,t.PersonNumber,t.PersonName";
            //sqlQuery += " ,(case t.Migrated when '1' then 'TRUE' ELSE 'FALSE' END) AS Migrated, ie.error_message";
            //sqlQuery += " from vwTransactions t, gl_intf_errors ie";
            //sqlQuery += " where (t.migrated is null or t.migrated = '0')";
            //sqlQuery += " and (t.transactionid = ie.error_in) ";
            //sqlQuery += " group by t.PersonNumber,t.PersonName,t.migrated,t.transactionid,t.TransactionDate, t.ReferenceNumber,ie.error_message";
            //sqlQuery += " union all";
            //sqlQuery += " select t.TransactionID, t.TransactionDate, t.ReferenceNumber,t.PersonNumber,t.PersonName";
            //sqlQuery += " ,(case t.Migrated when '1' then 'TRUE' ELSE 'FALSE' END) AS Migrated, '' as error_message";
            //sqlQuery += " from vwTransactions t , gl_intf_errors ie ";
            //sqlQuery += " where (t.migrated is null or t.migrated = '0') ";
            //sqlQuery += " and (t.transactionid != ie.error_in)";
            //sqlQuery += " group by t.PersonNumber,t.PersonName,t.migrated,t.transactionid,t.TransactionDate, t.ReferenceNumber";
            //sqlQuery += " order by TransactionID asc";


            sqlQuery += " select t.TransactionID, t.TransactionDate, t.ReferenceNumber,t.PersonNumber,t.PersonName";
            sqlQuery += " ,(case t.Migrated when '1' then 'TRUE' ELSE 'FALSE' END) AS Migrated";
            sqlQuery += "  , (select ie.error_message from gl_intf_errors ie where t.transactionid = ie.error_in and rownum=1)  as error_message";
            sqlQuery += " from vwTransactions t  ";
            sqlQuery += " where (t.migrated is null or t.migrated = '0') ";
            sqlQuery += " and t.TransactionDate <= to_date('" + fromDate + "','dd/MM/yyyy')";
            sqlQuery += " group by t.PersonNumber,t.PersonName,t.migrated,t.transactionid,t.TransactionDate, t.ReferenceNumber";
            sqlQuery += " order by t.TransactionID asc";


            //sqlQuery = " select t.TransactionID, t.TransactionDate, t.ReferenceNumber, t.PersonNumber, t.PersonName"; 
            ////sqlQuery += ", t.AccountId, t.AccountNum,t.AccountNameEn, to_char(t.Debit) as Debit,to_char(t.Credit) as Credit ";
            //sqlQuery += " ,(case t.Migrated when '1' then 'TRUE' ELSE 'FALSE' END) AS Migrated";
            //sqlQuery += " from vwTransactions t";
            //sqlQuery += " where (t.migrated is null or t.migrated = '0')";
            //sqlQuery += " group by t.PersonNumber, t.PersonName, t.migrated, t.transactionid, t.TransactionDate, t.ReferenceNumber";
            //sqlQuery += " order by t.TransactionID asc";

            return sqlQuery;
        }
    }
}
