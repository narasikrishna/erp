﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.GL
{
    public class AccountingGroupDefaultSegment
    {
        static string sqlQuery = "";


        public static string getGroupName()
        {
            sqlQuery = string.Empty;
            sqlQuery = "  SELECT AG.ACCT_GRP_ID,AG.ACCT_GRP_DESC ";
            sqlQuery += " FROM GL_ACCT_GROUPS AG ";
            sqlQuery += " WHERE AG.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND AG.ENABLED_FLAG = 1";

            return sqlQuery;
        }

        public static string getSegmentName()
        {
            sqlQuery = string.Empty;
            sqlQuery = "  SELECT S.SEGMENT_ID,S.SEGMENT_NAME";
            sqlQuery += " FROM GL_SEGMENTS S";
            sqlQuery += " WHERE S.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND S.ENABLED_FLAG = 1";

            return sqlQuery;
        }


    }
}
