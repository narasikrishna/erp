﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;

using System.Data.EntityClient;
using System.Data.Entity.Infrastructure;

namespace FIN.DAL.GL
{
    public class BalanceSheetTemplate_DAL
    {
        public static string get_TemplateStructure(string str_name)
        {
            string sqlQuery = string.Empty;
            sqlQuery = " SELECT BST.BS_TMPL_ID,BST.BS_TMPL_NAME,BST.BS_TYPE,BST.BS_GROUP_NAME,NVL(BST.BS_GROUP_NAME,' ') || NVL(BST.BS_ACC_NAME,' ') || ' - ' || BST.BS_GROUP_NO as BS_DISPLAY_NAME,BST.BS_ACC_ID,BST.BS_ACC_NAME,BST.BS_TMPL_PARENT_ID,NVL(BST.BS_TMPL_PARENT_GROUP_NAME,'HEAD') AS BS_TMPL_PARENT_GROUP_NAME,BST.BS_GROUP_NO,BST.BS_TOT_REQ ";
            sqlQuery += "FROM GL_BALANCE_SHEET_TEMPLATE BST ";
            sqlQuery += " where BS_TMPL_NAME='" + str_name + "'";
            sqlQuery += " ORDER BY BST.BS_GROUP_NO";
            return sqlQuery;
        }

        public static string get_Template4Group(string str_name,string groupNumber)
        {
             string sqlQuery = string.Empty;
             sqlQuery = " SELECT * FROM GL_BALANCE_SHEET_TEMPLATE ";
             sqlQuery += " where BS_TMPL_NAME='" + str_name + "'";
             sqlQuery += " AND BS_GROUP_NO=" + groupNumber;
             return sqlQuery;
        }
        public static string get_TemplateGroupNumber(string str_name)
        {
            string sqlQuery = string.Empty;
            sqlQuery += " SELECT BS_GROUP_NO FROM GL_BALANCE_SHEET_TEMPLATE  WHERE BS_TMPL_NAME='" + str_name + "' ";
            return sqlQuery;
        }
        public static string get_GroupTemplateStructure(string str_name)
        {
            string sqlQuery = string.Empty;
            sqlQuery = " SELECT BST.BS_TMPL_ID,BST.BS_TMPL_NAME,BST.BS_TYPE,BST.BS_GROUP_NAME,NVL(BST.BS_GROUP_NAME,' ') || NVL(BST.BS_ACC_NAME,' ')|| ' - ' || BST.NOTES as BS_DISPLAY_NAME,BST.BS_ACC_ID,BST.BS_ACC_NAME,BST.BS_TMPL_PARENT_ID,NVL(BST.BS_TMPL_PARENT_GROUP_NAME,'HEAD') AS BS_TMPL_PARENT_GROUP_NAME,BST.BS_GROUP_NO,BST.BS_TOT_REQ ";
            sqlQuery += "FROM GL_BALANCE_SHEET_TEMPLATE BST ";
            sqlQuery += " where BS_TMPL_NAME='" + str_name + "'";
            sqlQuery += " AND UPPER(BST.BS_TYPE)='GROUP' ";
            sqlQuery += " ORDER BY BST.BS_GROUP_NO";
            return sqlQuery;
        }
        public static string get_TemplateData4TmplId(string str_TMPL_ID)
        {
            string sqlQuery = string.Empty;
            sqlQuery = "SELECT * FROM GL_BALANCE_SHEET_TEMPLATE A WHERE A.BS_TMPL_PARENT_ID='" + str_TMPL_ID + "'";
            return sqlQuery;
        }

        public static DataTable get_TmpleateData(string str_TemplateName, int NoOfYear,string str_Type)
        {
            string strValue = string.Empty;
            string strNextCode = string.Empty;
            string strErrMsg = string.Empty;

            OracleCommand oraCmd = new OracleCommand();
            oraCmd.CommandText = "BS_TEMPLATE_DATA";
            oraCmd.CommandType = CommandType.StoredProcedure;
            oraCmd.Parameters.Add("@P_TEMPLATE_NAME", OracleDbType.Varchar2).Value = str_TemplateName;                  
            oraCmd.Parameters.Add("@P_NO_OF_YEARS", OracleDbType.Int16).Value = NoOfYear;
            oraCmd.Parameters.Add("@P_ORG_ID", OracleDbType.Varchar2).Value = VMVServices.Web.Utils.OrganizationID;
            oraCmd.Parameters.Add("@P_YEAR", OracleDbType.Varchar2).Value = DateTime.Now.Year.ToString();
            oraCmd.Parameters.Add("@P_TYPE", OracleDbType.Varchar2).Value = str_Type;
            oraCmd.Parameters.Add("@P_DATA", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            return DBMethod.ExecuteStoredProcedure4CursorOutput(oraCmd);
        }

        public static string getTemplateName()
        {
            string sqlQuery = string.Empty;
            sqlQuery = " SELECT DISTINCT BST.BS_TMPL_NAME FROM GL_BALANCE_SHEET_TEMPLATE BST ";
            return sqlQuery;

        }
        public static string getSegmentValue(string Parent_id = "0")
        {
            string sqlQuery = string.Empty;

            sqlQuery += "  SELECT'0' as BS_TMPL_ID, SV.SEGMENT_VALUE_ID as SEGMENT_ID,SV.SEGMENT_VALUE as SEGMENT_NAME ";
            sqlQuery += " ,'N' as SELECTED_REC ";
            sqlQuery += " FROM GL_SEGMENTS S  ";
            sqlQuery += " INNER JOIN GL_SEGMENT_VALUES SV ON S.SEGMENT_ID= SV.SEGMENT_ID ";
            sqlQuery += " WHERE S.SEGMENT_ID NOT IN (SELECT NVL(COMP_GLOBAL_SEGMENT_ID,'') FROM gl_companies_hdr) ";
            sqlQuery += " and  SEGMENT_VALUE_ID not in (select bs_acc_id from gL_BALANCE_SHEET_TEMPLATE where BS_TMPL_PARENT_ID='" + Parent_id + "' and bs_acc_id is not null)";
            sqlQuery += " and S.SEGMENT_ID='SEG_ID-0000000101'";

            sqlQuery += " UNION ";

            sqlQuery += "  SELECT BS_TMPL_ID, SV.SEGMENT_VALUE_ID as SEGMENT_ID,SV.SEGMENT_VALUE as SEGMENT_NAME ";
            sqlQuery += " ,'Y' as SELECTED_REC ";
            sqlQuery += " FROM GL_SEGMENTS S  ";
            sqlQuery += " INNER JOIN GL_SEGMENT_VALUES SV ON S.SEGMENT_ID= SV.SEGMENT_ID ";
            sqlQuery += " INNER JOIN GL_BALANCE_SHEET_TEMPLATE BST on BST.BS_ACC_ID=SV.SEGMENT_VALUE_ID ";
            sqlQuery += " WHERE S.SEGMENT_ID NOT IN (SELECT NVL(COMP_GLOBAL_SEGMENT_ID,'') FROM gl_companies_hdr) ";
            sqlQuery += " and BS_TMPL_PARENT_ID='" + Parent_id + "'";
            sqlQuery += " and S.SEGMENT_ID='SEG_ID-0000000101'";

            sqlQuery += "  order by SEGMENT_NAME asc ";
            return sqlQuery;
        }

        public static string getAccCode4FinTempSegment(string Parent_id = "0")
        {
            string sqlQuery = string.Empty;


            sqlQuery = "  select '0' as BS_TMPL_ID, aa.acct_code_id as CODE_ID,aa.ACCT_TYPE,";
            sqlQuery += " (aa.acct_code||' -  '|| aa.acct_code_desc" + VMVServices.Web.Utils.LanguageCode + ") as CODE_NAME,aa.acct_code_desc" + VMVServices.Web.Utils.LanguageCode;
            sqlQuery += " ,'N' as SELECTED_REC ";
            sqlQuery += " from gl_acct_codes aa, gl_companies_dtl g";
            sqlQuery += " where";
            sqlQuery += " aa.enabled_flag = '1'";
            sqlQuery += " and aa.workflow_completion_status = '1'  and g.workflow_completion_status=1";
            sqlQuery += " and g.comp_id  ='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " and aa.acct_struct_id = g.comp_acct_struct_id";
            sqlQuery += " and  acct_code_id not in (select bs_acc_id from GL_BALANCE_SHEET_TEMPLATE where BS_TMPL_PARENT_ID='" + Parent_id + "' and bs_acc_id is not null)";

            sqlQuery += " UNION ";

            sqlQuery += " select BST.BS_TMPL_ID, aa.acct_code_id as CODE_ID,aa.ACCT_TYPE,";
            sqlQuery += " (aa.acct_code||' -  '|| aa.acct_code_desc" + VMVServices.Web.Utils.LanguageCode + ") as CODE_NAME,aa.acct_code_desc" + VMVServices.Web.Utils.LanguageCode;
            sqlQuery += " ,'Y' as SELECTED_REC ";
            sqlQuery += " from gl_acct_codes aa, gl_companies_dtl g,GL_BALANCE_SHEET_TEMPLATE bst";
            sqlQuery += " where";
            sqlQuery += " aa.enabled_flag = '1'";
            sqlQuery += " and aa.workflow_completion_status = '1'  and g.workflow_completion_status=1";
            sqlQuery += " and g.comp_id  ='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " and aa.acct_struct_id = g.comp_acct_struct_id";
            sqlQuery += " and  bst.BS_ACC_ID =  aa.acct_code_id";
            sqlQuery += " and BS_TMPL_PARENT_ID='" + Parent_id + "'";
            sqlQuery += "  order by CODE_NAME asc ";
            return sqlQuery;
        }
    }
}
