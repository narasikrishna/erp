﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.GL
{
    public class ActualandProjectedBudget_DAL
    {
        static string sqlQuery = "";


        public static string getPreviousYrAmt(string budget_acct_grp_id)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select (bd.budget_month_amt_01+bd.budget_month_amt_02+bd.budget_month_amt_03+";
            sqlQuery += " bd.budget_month_amt_04+bd.budget_month_amt_05+bd.budget_month_amt_06+bd.budget_month_amt_07+bd.budget_month_amt_08+";
            sqlQuery += " bd.budget_month_amt_09+bd.budget_month_amt_10+bd.budget_month_amt_11+bd.budget_month_amt_12) as ACTUAL_PREVIOUS_AMOUNT from GL_BUDGET_DTL bd,Gl_Budget_Hdr bh";
            sqlQuery += " where bd.budget_acct_grp_id = '" + budget_acct_grp_id + "'";
            sqlQuery += " and bh.budget_code = bd.budget_code";
            sqlQuery += " and bh.budget_period_start_dt =";
            sqlQuery += " (select max(sbh.budget_period_start_dt) from Gl_Budget_Hdr sbh,GL_BUDGET_DTL sbd ";
            sqlQuery += " where sbd.budget_code= sbh.budget_code and sbd.budget_acct_grp_id= '" + budget_acct_grp_id + "')";
            sqlQuery += " and rownum =1";


            return sqlQuery;
        }



    }
}
