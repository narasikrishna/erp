﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.GL
{
    public class AccountingCalendar
    {
        static string sqlQuery = "";


        public static string getAcctcalendardtl(long Master_id)
        {
            sqlQuery = string.Empty;


            sqlQuery = " SELECT cd.pk_id,cd.cal_dtl_id,cd.cal_acct_year,cd.cal_eff_start_dt,cd.call_eff_end_dt,'N' AS DELETED";
            sqlQuery += " FROM GL_ACCT_CALENDAR_DTL cd";
            sqlQuery += " where cd.pk_id = " + Master_id;
            sqlQuery += " and cd.workflow_completion_status = 1";
            sqlQuery += " and cd.enabled_flag =1";
            return sqlQuery;
        }

        public static string getPerioddtl()
        {
            sqlQuery = string.Empty;


            sqlQuery = " SELECT pd.pk_id,pd.period_id,pd.period_name,pd.period_type,pd.period_from_dt,pd.period_to_dt,pd.period_status,'N' AS DELETED";
            sqlQuery += " FROM GL_ACCT_PERIOD_DTL pd";
            sqlQuery += " where pd.workflow_completion_status = 1";
            sqlQuery += " and pd.enabled_flag =1";
            
            return sqlQuery;
        }

        public static string getfromLookup(string typeName)
        {
            sqlQuery = string.Empty;


            sqlQuery = "Select V.VALUE_KEY_ID,V.VALUE_NAME,m.TYPE_KEY_ID  From ssm_lookup_value_dtls V,ssm_lookup_type_master M";
            sqlQuery += " WHERE M.TYPE_NAME = '" + typeName + "'";
            sqlQuery += " AND V.TYPE_KEY_ID =  M.TYPE_KEY_ID";
            sqlQuery += " ORDER BY V.DISPLAY_ORDER";
            return sqlQuery;
        }


    }
}
