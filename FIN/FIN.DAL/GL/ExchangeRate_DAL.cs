﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.GL
{
    public class ExchangeRate_DAL
    {

        static string sqlQuery = "";

        public static string getExchangeRateDetails(string str_CompID)
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT SCER.CURRENCY_RATE_ID, SCER.CURRENCY_RATE_DT,c.currency_id,(TO_CHAR(c.CURRENCY_CODE)  || ' - ' || C.CURRENCY_SYMBOL) as CURRENCY_NAME, SCER.CURRENCY_STD_RATE, SCER.CURRENCY_SEL_RATE, SCER.CURRENCY_BUY_RATE, 'N' AS DELETED ";
            sqlQuery += " FROM SSM_CURRENCY_EXCHANGE_RATES SCER,SSM_CURRENCIES c ";
            sqlQuery += " WHERE SCER.CURRENCY_RATE_ID = '" + str_CompID + "'";
            sqlQuery += " and scer.currency_id = c.currency_id";
            sqlQuery += " AND SCER.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " AND SCER.ENABLED_FLAG = 1 ";
            sqlQuery += " ORDER BY CURRENCY_RATE_ID ";

            return sqlQuery;
        }

        public static string getStandardRate(string curr_id, string rateDate = "")
        {
            sqlQuery = string.Empty;

            sqlQuery = " select cer.currency_std_rate ";
            sqlQuery += " from ssm_currency_exchange_rates cer ";
            sqlQuery += " where cer.currency_id = '" + curr_id + "'";
            if (rateDate != null)
            {
                sqlQuery += " and cer.currency_rate_dt = to_date('" + rateDate + "','dd/MM/yyyy')";
            }
            sqlQuery += " AND cer.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " AND cer.ENABLED_FLAG = 1 ";

            return sqlQuery;
        }

        public static string GetExchangeRate(string exchangeDate, string currencyId = "")
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT SCER.CURRENCY_STD_RATE, SCER.CURRENCY_RATE_ID, SCER.CURRENCY_RATE_DT,  SCER.CURRENCY_SEL_RATE, SCER.CURRENCY_BUY_RATE ";

            sqlQuery += "  FROM SSM_CURRENCY_EXCHANGE_RATES SCER ";
            sqlQuery += "  WHERE ";
            sqlQuery += "  SCER.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += "   AND SCER.ENABLED_FLAG = 1 ";
            sqlQuery += "   AND SCER.CURRENCY_ID='" + currencyId + "'";
            //sqlQuery += " AND SCER.CURRENCY_RATE_DT= to_date('" + exchangeDate + "','dd/MM/yyyy')";
            sqlQuery += "   AND SCER.CURRENCY_RATE_DT =  (nvl((select sc.currency_rate_dt";
            sqlQuery += "     FROM SSM_CURRENCY_EXCHANGE_RATES SC";
            sqlQuery += "    where SC.CURRENCY_RATE_DT =";
            sqlQuery += "         to_date('" + exchangeDate + "','dd/MM/yyyy') and rownum=1 ),";
            sqlQuery += "   (select max(scr.CURRENCY_RATE_DT)";
            sqlQuery += "    FROM SSM_CURRENCY_EXCHANGE_RATES SCr)))";
            //if (exRateId != string.Empty)
            //{
            //    sqlQuery += " and SCER.CURRENCY_RATE_ID='" + exRateId + "'";
            //}


            sqlQuery += "  order by SCER.CURRENCY_RATE_DT,SCER.CURRENCY_RATE_ID desc ";

            return sqlQuery;
        }


        public static string getBasecurrency(string comp_id)
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT  bc.currency_desc";
            sqlQuery += " FROM GL_COMPANIES_HDR c,Gl_Companies_Dtl cd,SSM_CURRENCIES bc";
            sqlQuery += " where c.comp_id = cd.comp_id";
            sqlQuery += " and cd.comp_base_currency = bc.currency_id";
            sqlQuery += " and c.enabled_flag = 1";
            sqlQuery += " and c.comp_id = '" + comp_id + "'";
            return sqlQuery;
        }


        public static string GetLastExchangeRate(string exchangeDate, string currencyId)
        {
            sqlQuery = string.Empty;
            sqlQuery += " select * from SSM_CURRENCY_EXCHANGE_RATES ";
            sqlQuery += " where CURRENCY_ID='" + currencyId + "' ";
            sqlQuery += "   and CURRENCY_RATE_DT <= to_date('" + exchangeDate + "','dd/MM/yyyy')";            
            sqlQuery += " and rownum =1 ";
            sqlQuery += " order by CURRENCY_RATE_DT desc ";
            return sqlQuery;
        }

        public static string getExchangeRateReportData()
        {
            sqlQuery = string.Empty;

            sqlQuery += " SELECT * FROM VW_GL_EXCHANGE_RATE V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["CurrencyID"] != null)
                {
                    sqlQuery += " AND V.CURRENCY_ID = '" + VMVServices.Web.Utils.ReportFilterParameter["CurrencyID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["FromDate"] != null)
                {
                    sqlQuery += " AND V.CURRENCY_RATE_DT >= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["FromDate"].ToString() + "','dd/MM/yyyy') ";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["ToDate"] != null)
                {
                    sqlQuery += " AND V.CURRENCY_RATE_DT <= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["ToDate"].ToString() + "','dd/MM/yyyy') ";
                }

                if (VMVServices.Web.Utils.ReportViewFilterParameter["CREATED_BY"] != null)
                {
                    sqlQuery += " AND UPPER(V.CREATED_BY) = UPPER('" + VMVServices.Web.Utils.ReportViewFilterParameter["CREATED_BY"].ToString() + "')";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["MODIFIED_BY"] != null)
                {
                    sqlQuery += " AND UPPER(V.MODIFIED_BY) = UPPER('" + VMVServices.Web.Utils.ReportViewFilterParameter["MODIFIED_BY"].ToString() + "')";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["CREATED_From_Date"] != null)
                {
                    sqlQuery += " AND V.CREATED_DATE >= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["CREATED_From_Date"].ToString() + "','dd/MM/yyyy') ";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["CREATED_To_Date"] != null)
                {
                    sqlQuery += " AND V.CREATED_DATE <= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["CREATED_To_Date"].ToString() + "','dd/MM/yyyy') ";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["MODIFIED_From_Date"] != null)
                {
                    sqlQuery += " AND V.MODIFIED_DATE >= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["MODIFIED_From_Date"].ToString() + "','dd/MM/yyyy') ";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["MODIFIED_To_Date"] != null)
                {
                    sqlQuery += " AND V.MODIFIED_DATE <= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["MODIFIED_To_Date"].ToString() + "','dd/MM/yyyy') ";
                }
            }

            return sqlQuery;
        }


    }
}