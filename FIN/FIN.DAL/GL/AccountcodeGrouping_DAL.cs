﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.GL
{
    public class AccountcodeGrouping_DAL
    {
        static string sqlQuery = "";

        public static string getAccountcodeGroupingReportData()
        {
            sqlQuery = string.Empty;
            //sqlQuery = "SELECT * FROM temp_gl_acc_id_name ";
            // sqlQuery = "SELECT * FROM GL_GROUP_ACC_LINK V  ";
            sqlQuery = "  select t.level1, t.level2, t.level3, t.level4, t.level5,t.acc_grp_id as acc_code, nvl(acc.COLUMN_VALUE,'') as COLUMN_VALUE";
            sqlQuery += " from temp_gl_acc_id_name t, TABLE(t.tools) acc";
            sqlQuery += " order by t.level1, t.level2, t.level3, t.level4,t.level5,substr(column_value,1,6)";
            //if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            //{
            //    if (VMVServices.Web.Utils.ReportViewFilterParameter["VENDOR_NAME"] != null)
            //    {
            //        sqlQuery += " AND V.VENDOR_NAME = '" + VMVServices.Web.Utils.ReportViewFilterParameter["VENDOR_NAME"].ToString() + "'";
            //    }
            //}

            return sqlQuery;
        }
    }
}
