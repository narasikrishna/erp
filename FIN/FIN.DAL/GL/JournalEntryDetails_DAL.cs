﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.GL
{
    public class JournalEntryDetails_DAL
    {
        static string sqlQuery = "";

        public static string getJournalTrans4Report()
        {
            sqlQuery = string.Empty;
            sqlQuery += " SELECT * FROM VW_GL_TRANSACTION  V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["PERIOD"] != null)
                {
                    sqlQuery += " AND V.period_id = '" + VMVServices.Web.Utils.ReportViewFilterParameter["PERIOD"].ToString() + "'";
                }
            }

            return sqlQuery;
        }


        public static string getJournalTrans(string str_period_id)
        {
            sqlQuery = string.Empty;
            //sqlQuery += "  select (select ROUND(sum(jd.je_accounted_amt_cr)," + VMVServices.Web.Utils.DecimalPrecision + ")";
            //sqlQuery += "    from gl_journal_hdr jh, gl_journal_dtl jd";
            //sqlQuery += "   where jh.je_hdr_id = jd.je_hdr_id";
            //sqlQuery += "     and jh.je_source = 'AP') as ap_amt, ";
            //sqlQuery += "  (select ROUND(sum(jd.je_accounted_amt_cr)," + VMVServices.Web.Utils.DecimalPrecision + ")";
            //sqlQuery += "  from gl_journal_hdr jh, gl_journal_dtl jd";
            //sqlQuery += "  where jh.je_hdr_id = jd.je_hdr_id";
            //sqlQuery += "  and jh.je_source = 'GL') as gl_amt,";
            //sqlQuery += "  (select ROUND(sum(jd.je_accounted_amt_cr)," + VMVServices.Web.Utils.DecimalPrecision + ")";
            //sqlQuery += "     from gl_journal_hdr jh, gl_journal_dtl jd";
            //sqlQuery += "    where jh.je_hdr_id = jd.je_hdr_id";
            //sqlQuery += "      and jh.je_source = 'PER') as per_amt,";
            //sqlQuery += "  (select ROUND(sum(jd.je_accounted_amt_cr)," + VMVServices.Web.Utils.DecimalPrecision + ")";
            //sqlQuery += "    from gl_journal_hdr jh, gl_journal_dtl jd";
            //sqlQuery += "   where jh.je_hdr_id = jd.je_hdr_id";
            //sqlQuery += "   and jh.je_source = 'HR') as hr_amt";
            //sqlQuery += "  from dual";

            sqlQuery += "    select 'AP' as source_code,Round(nvl(sum(jd.je_accounted_amt_cr),0)," + VMVServices.Web.Utils.DecimalPrecision + ") as tot_amt,null as gl_amt,null as per_amt,NULL AS hr_amt";
            sqlQuery += "   from gl_journal_hdr jh, gl_journal_dtl jd,gl_acct_period_dtl apd";
            sqlQuery += "   where jh.je_hdr_id = jd.je_hdr_id and apd.period_id= jh.JE_PERIOD_ID ";
            sqlQuery += "   and apd.period_id='" + str_period_id + "'";          
            sqlQuery += "   and jh.je_source='AP'";
            sqlQuery += "   union";
            sqlQuery += "   select 'GL' as source_code,Round(nvl(sum(jd.je_accounted_amt_cr),0)," + VMVServices.Web.Utils.DecimalPrecision + ") as tot_amt,sum(jd.je_accounted_amt_cr) as gl_amt,null as per_amt,NULL AS hr_amt";
            sqlQuery += "   from gl_journal_hdr jh, gl_journal_dtl jd,gl_acct_period_dtl apd";
            sqlQuery += "   where jh.je_hdr_id = jd.je_hdr_id  and apd.period_id= jh.JE_PERIOD_ID ";
            sqlQuery += "  and apd.period_id='" + str_period_id + "' ";
            sqlQuery += "   and jh.je_source='MANUAL'";
            sqlQuery += "   union";
            sqlQuery += "   select 'PER' as source_code,ROUND(nvl(sum(jd.je_accounted_amt_cr),0)," + VMVServices.Web.Utils.DecimalPrecision + ") as tot_amt,null as gl_amt,sum(jd.je_accounted_amt_cr) as per_amt,NULL AS hr_amt";
            sqlQuery += "   from gl_journal_hdr jh, gl_journal_dtl jd,gl_acct_period_dtl apd";
            sqlQuery += "   where jh.je_hdr_id = jd.je_hdr_id  and apd.period_id= jh.JE_PERIOD_ID ";
            sqlQuery += "  and apd.period_id='" + str_period_id + "' ";
            sqlQuery += "   and jh.je_source='PER'";
            sqlQuery += "    union";
            sqlQuery += "   select 'HR' as source_code,ROUND(nvl(sum(jd.je_accounted_amt_cr),0)," + VMVServices.Web.Utils.DecimalPrecision + ") as tot_amt,null as gl_amt,null as per_amt,sum(jd.je_accounted_amt_cr) as hr_amt";
            sqlQuery += "    from gl_journal_hdr jh, gl_journal_dtl jd,gl_acct_period_dtl apd";
            sqlQuery += "   where jh.je_hdr_id = jd.je_hdr_id  and apd.period_id= jh.JE_PERIOD_ID ";
            sqlQuery += "  and apd.period_id='" + str_period_id + "' ";
            sqlQuery += "   and jh.je_source='HR'";
            return sqlQuery;
        }
        public static string getAccCodeBal()
        {
            sqlQuery = string.Empty;
            sqlQuery += "    select nvl(sum(jd.je_accounted_amt_cr), 0), ac.acct_code_desc";
            sqlQuery += "   from gl_journal_hdr jh, gl_journal_dtl jd, gl_acct_codes ac";
            sqlQuery += "   where jh.je_hdr_id = jd.je_hdr_id";
            sqlQuery += "    and ac.acct_code_id = jd.je_acct_code_id";
            sqlQuery += "   group by jd.je_acct_code_id, ac.acct_code_desc";
            sqlQuery += "  order by ac.acct_code_desc asc";
            return sqlQuery;
        }
    }
}
