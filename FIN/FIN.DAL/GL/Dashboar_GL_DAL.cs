﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.GL
{
    public class Dashboar_GL_DAL
    {
        static string sqlQuery = "";

        public static string getBIGlCashBalance()
        {
            sqlQuery = string.Empty;

            sqlQuery += "    select gac.acct_code_id,gag.ACCT_FUND_TRANSFER,";
            sqlQuery += "   gac.acct_code as ACCOUNT_NO,";
            sqlQuery += "   gac.acct_code_desc as ACCOUNT_NAME,";
            sqlQuery += "   gac.acct_code_desc_ol as ACCOUNT_NAME_OL,";
            sqlQuery += "   (round((nvl(cbd.amount_dr, 0)-nvl(cbd.amount_cr, 0)),3)) as BALANCE_AMT,";
            sqlQuery += "   gac.acct_grp_id,    ";
            sqlQuery += "   cbd.period_day,";
            sqlQuery += "    gac.acct_org_id as org_id,";
            sqlQuery += "    cbd.workflow_completion_status";
            sqlQuery += "   from gl_acct_groups gag, gl_acct_codes gac, gl_current_balances_day cbd,ssm_system_options ss";
            sqlQuery += "   where gag.acct_grp_id = gac.acct_grp_id";
            sqlQuery += "    and gac.acct_code = cbd.acct_code";
            sqlQuery += "   and ss.ap_default_cash_acct_code=gac.acct_code_id";
            // sqlQuery += "    and gag.ACCT_FUND_TRANSFER = 1";

            return sqlQuery;
        }

        public static string getLeave1GrpBalance4Period(string str_period)
        {
            sqlQuery = "";
            sqlQuery += "    SELECT GAN.LEVEL1_ID as GB_ID1, GAN.LEVEL1 as GBName,ROUND(SUM(JSP.CB_ACCOUNTED_DR)-SUM( JSP.CB_ACCOUNTED_CR)," + VMVServices.Web.Utils.DecimalPrecision +") as GB";
            sqlQuery += " FROM GL_JE_SUMMARY_PERIOD JSP ";
            sqlQuery += " INNER JOIN GL_ACCT_CODES AC ON AC.ACCT_CODE_ID =  JSP.ACCT_CODE_ID ";
            sqlQuery += " INNER JOIN TEMP_GL_ACC_ID_NAME GAN ON GAN.LEVEL5_ID =  AC.ACCT_GRP_ID ";
            sqlQuery += " WHERE JSP.PERIOD_ID='" + str_period + "'";
            sqlQuery += " GROUP BY GAN.LEVEL1,GAN.LEVEL1_ID ";
            return sqlQuery;
        }
        public static string getLeave1GrpBalance4Period4Report()
        {
            sqlQuery = string.Empty;
            sqlQuery += " SELECT * FROM VW_GL_GroupBalance  V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["PERIOD_ID"] != null)
                {
                    sqlQuery += " AND V.PERIOD_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["PERIOD_ID"].ToString() + "'";
                }
            }

            return sqlQuery;
        }
        public static string getLeave2GrpBalance4Period(string str_period, string lvl1_id)
        {
            sqlQuery = "";
            sqlQuery += "    SELECT GAN.LEVEL2_ID as GB_ID2, GAN.LEVEL2 as GBName,ROUND(SUM(JSP.CB_ACCOUNTED_DR)-SUM( JSP.CB_ACCOUNTED_CR)," + VMVServices.Web.Utils.DecimalPrecision + ")  as GB";
            sqlQuery += " FROM GL_JE_SUMMARY_PERIOD JSP ";
            sqlQuery += " INNER JOIN GL_ACCT_CODES AC ON AC.ACCT_CODE_ID =  JSP.ACCT_CODE_ID ";
            sqlQuery += " INNER JOIN TEMP_GL_ACC_ID_NAME GAN ON GAN.LEVEL5_ID =  AC.ACCT_GRP_ID ";
            sqlQuery += " WHERE JSP.PERIOD_ID='" + str_period + "'";
            sqlQuery += " AND GAN.LEVEL1_ID='" + lvl1_id + "'";
            sqlQuery += " GROUP BY GAN.LEVEL2,GAN.LEVEL2_ID ";
            return sqlQuery;
        }

        public static string getLeave3GrpBalance4Period(string str_period, string lvl2_id)
        {
            sqlQuery = "";
            sqlQuery += "    SELECT GAN.LEVEL3_ID as GB_ID3, GAN.LEVEL3 as GBName,ROUND(SUM(JSP.CB_ACCOUNTED_DR)-SUM( JSP.CB_ACCOUNTED_CR)," + VMVServices.Web.Utils.DecimalPrecision + ")  as GB";
            sqlQuery += " FROM GL_JE_SUMMARY_PERIOD JSP ";
            sqlQuery += " INNER JOIN GL_ACCT_CODES AC ON AC.ACCT_CODE_ID =  JSP.ACCT_CODE_ID ";
            sqlQuery += " INNER JOIN TEMP_GL_ACC_ID_NAME GAN ON GAN.LEVEL5_ID =  AC.ACCT_GRP_ID ";
            sqlQuery += " WHERE JSP.PERIOD_ID='" + str_period + "'";

            sqlQuery += " AND GAN.LEVEL2_ID='" + lvl2_id + "'";
            sqlQuery += " GROUP BY GAN.LEVEL3_ID , GAN.LEVEL3";
            return sqlQuery;
        }

        public static string getLeave4GrpBalance4Period(string str_period, string lvl3_id)
        {
            sqlQuery = "";
            sqlQuery += "    SELECT GAN.LEVEL4_ID as GB_ID4, GAN.LEVEL4 as GBName,ROUND(SUM(JSP.CB_ACCOUNTED_DR)-SUM( JSP.CB_ACCOUNTED_CR)," + VMVServices.Web.Utils.DecimalPrecision + ")  as GB";
            sqlQuery += " FROM GL_JE_SUMMARY_PERIOD JSP ";
            sqlQuery += " INNER JOIN GL_ACCT_CODES AC ON AC.ACCT_CODE_ID =  JSP.ACCT_CODE_ID ";
            sqlQuery += " INNER JOIN TEMP_GL_ACC_ID_NAME GAN ON GAN.LEVEL5_ID =  AC.ACCT_GRP_ID ";
            sqlQuery += " WHERE JSP.PERIOD_ID='" + str_period + "'";

            sqlQuery += " AND GAN.LEVEL3_ID='" + lvl3_id + "'";
            sqlQuery += " GROUP BY GAN.LEVEL4_ID , GAN.LEVEL4";
            return sqlQuery;
        }

        public static string getLeave5GrpBalance4Period(string str_period, string lvl4_id)
        {
            sqlQuery = "";
            sqlQuery += "    SELECT GAN.LEVEL5_ID as GB_ID5, GAN.LEVEL5 as GBName,ROUND(SUM(JSP.CB_ACCOUNTED_DR)-SUM( JSP.CB_ACCOUNTED_CR)," + VMVServices.Web.Utils.DecimalPrecision +") as GB";
            sqlQuery += " FROM GL_JE_SUMMARY_PERIOD JSP ";
            sqlQuery += " INNER JOIN GL_ACCT_CODES AC ON AC.ACCT_CODE_ID =  JSP.ACCT_CODE_ID ";
            sqlQuery += " INNER JOIN TEMP_GL_ACC_ID_NAME GAN ON GAN.LEVEL5_ID =  AC.ACCT_GRP_ID ";
            sqlQuery += " WHERE JSP.PERIOD_ID='" + str_period + "'";

            sqlQuery += " AND GAN.LEVEL4_ID='" + lvl4_id + "'";
            sqlQuery += " GROUP BY GAN.LEVEL5_ID , GAN.LEVEL5";
            return sqlQuery;
        }


        public static string getTrailBalance4Period(string str_period)
        {
            sqlQuery = "";
            sqlQuery += "   SELECT * FROM ( ";
            sqlQuery += " select JSP.ACCT_CODE_ID,JSP.ACCT_CODE,JSP.ACCT_CODE_DESC,to_char(ROUND(SUM(JSP.BB_ACCOUNTED_DR)-SUM(JSP.BB_ACCOUNTED_CR)," + VMVServices.Web.Utils.DecimalPrecision + ")) AS OP_BAL,to_char(ROUND(SUM(ACCOUNTED_DR)," + VMVServices.Web.Utils.DecimalPrecision + ")) AS TRANS_DR,to_char(ROUND(SUM(ACCOUNTED_CR)," + VMVServices.Web.Utils.DecimalPrecision + ")) AS TRANS_CR ";
            sqlQuery += " , to_char(ROUND(SUM(CB_ACCOUNTED_DR)-SUM(CB_ACCOUNTED_CR)," + VMVServices.Web.Utils.DecimalPrecision + ")) AS CLOS_BAL ";
            sqlQuery += " from  GL_JE_SUMMARY_PERIOD JSP ";
            sqlQuery += " WHERE JSP.PERIOD_ID='" + str_period + "'";
            sqlQuery += " GROUP BY JSP.ACCT_CODE_ID,JSP.ACCT_CODE,JSP.ACCT_CODE_DESC ";
            sqlQuery += " ) z WHERE CLOS_BAL >0 ";
            return sqlQuery;
        }
    }
}
