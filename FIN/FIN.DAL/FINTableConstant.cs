﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL
{
    public class FINTableConstant
    {
        public const string Add = "A";

        public const string INV_PKG = "INV_PKG";
        public const string PKG_VALIDATIONS = "PKG_VALIDATIONS";


        public const string item_txn_ledger_entry = "item_txn_ledger_entry";
        public const string inv_txn_ledger = "inv_txn_ledger";
        public const string FUNC_DUPLICATE_MGR_CURR_CODE = "FUNC_DUPLICATE_MGR_CURR_CODE";
       
        public const string APP_GROUP = "APP_GROUP";
        public const string STUDENT = "STUDENT";
        public const string GUARDIAN = "GUARDIAN";
        public const string AST_MAJOR_CATEGORY_MST = "AST_MAJOR_CATEGORY_MST";
        public const string AST_MINOR_CATEGORY_DTL = "AST_MINOR_CATEGORY_DTL";
        public const string AST_DEPRECIATION_METHOD_MST = "AST_DEPRECIATION_METHOD_MST";
        public const string AST_ASSET_MST = "AST_ASSET_MST";
        public const string FIN_CURRENCY_MST = "FIN_CURRENCY_MST";
        public const string AST_LOCATION_MST = "AST_LOCATION_MST";

        public const string HR_APPLICANTS_QUALIFICATION = "HR_APPLICANTS_QUALIFICATION";
       
    }
}
