//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.Serialization;

namespace FIN.DAL
{
    [DataContract(IsReference = true)]
    [KnownType(typeof(INV_ITEM_MASTER))]
    public partial class INV_ITEM_RATES: IObjectWithChangeTracker, INotifyPropertyChanged
    {
        #region Primitive Properties
    
        [DataMember]
        public Nullable<int> PK_ID
        {
            get { return _pK_ID; }
            set
            {
                if (_pK_ID != value)
                {
                    _pK_ID = value;
                    OnPropertyChanged("PK_ID");
                }
            }
        }
        private Nullable<int> _pK_ID;
    
        [DataMember]
        public Nullable<int> CHILD_ID
        {
            get { return _cHILD_ID; }
            set
            {
                if (_cHILD_ID != value)
                {
                    _cHILD_ID = value;
                    OnPropertyChanged("CHILD_ID");
                }
            }
        }
        private Nullable<int> _cHILD_ID;
    
        [DataMember]
        public string RATE_ID
        {
            get { return _rATE_ID; }
            set
            {
                if (_rATE_ID != value)
                {
                    if (ChangeTracker.ChangeTrackingEnabled && ChangeTracker.State != ObjectState.Added)
                    {
                        throw new InvalidOperationException("The property 'RATE_ID' is part of the object's key and cannot be changed. Changes to key properties can only be made when the object is not being tracked or is in the Added state.");
                    }
                    _rATE_ID = value;
                    OnPropertyChanged("RATE_ID");
                }
            }
        }
        private string _rATE_ID;
    
        [DataMember]
        public System.DateTime RATE_DATE
        {
            get { return _rATE_DATE; }
            set
            {
                if (_rATE_DATE != value)
                {
                    _rATE_DATE = value;
                    OnPropertyChanged("RATE_DATE");
                }
            }
        }
        private System.DateTime _rATE_DATE;
    
        [DataMember]
        public string RATE_ITEM_ID
        {
            get { return _rATE_ITEM_ID; }
            set
            {
                if (_rATE_ITEM_ID != value)
                {
                    ChangeTracker.RecordOriginalValue("RATE_ITEM_ID", _rATE_ITEM_ID);
                    if (!IsDeserializing)
                    {
                        if (INV_ITEM_MASTER != null && INV_ITEM_MASTER.ITEM_ID != value)
                        {
                            INV_ITEM_MASTER = null;
                        }
                    }
                    _rATE_ITEM_ID = value;
                    OnPropertyChanged("RATE_ITEM_ID");
                }
            }
        }
        private string _rATE_ITEM_ID;
    
        [DataMember]
        public decimal RATE_PRICE
        {
            get { return _rATE_PRICE; }
            set
            {
                if (_rATE_PRICE != value)
                {
                    _rATE_PRICE = value;
                    OnPropertyChanged("RATE_PRICE");
                }
            }
        }
        private decimal _rATE_PRICE;
    
        [DataMember]
        public string RATE_CURRENCY
        {
            get { return _rATE_CURRENCY; }
            set
            {
                if (_rATE_CURRENCY != value)
                {
                    _rATE_CURRENCY = value;
                    OnPropertyChanged("RATE_CURRENCY");
                }
            }
        }
        private string _rATE_CURRENCY;
    
        [DataMember]
        public Nullable<decimal> RATE_ITEM_QTY
        {
            get { return _rATE_ITEM_QTY; }
            set
            {
                if (_rATE_ITEM_QTY != value)
                {
                    _rATE_ITEM_QTY = value;
                    OnPropertyChanged("RATE_ITEM_QTY");
                }
            }
        }
        private Nullable<decimal> _rATE_ITEM_QTY;
    
        [DataMember]
        public Nullable<decimal> RATE_ITEM_SOLD_QTY
        {
            get { return _rATE_ITEM_SOLD_QTY; }
            set
            {
                if (_rATE_ITEM_SOLD_QTY != value)
                {
                    _rATE_ITEM_SOLD_QTY = value;
                    OnPropertyChanged("RATE_ITEM_SOLD_QTY");
                }
            }
        }
        private Nullable<decimal> _rATE_ITEM_SOLD_QTY;
    
        [DataMember]
        public string ENABLED_FLAG
        {
            get { return _eNABLED_FLAG; }
            set
            {
                if (_eNABLED_FLAG != value)
                {
                    _eNABLED_FLAG = value;
                    OnPropertyChanged("ENABLED_FLAG");
                }
            }
        }
        private string _eNABLED_FLAG;
    
        [DataMember]
        public string WORKFLOW_COMPLETION_STATUS
        {
            get { return _wORKFLOW_COMPLETION_STATUS; }
            set
            {
                if (_wORKFLOW_COMPLETION_STATUS != value)
                {
                    _wORKFLOW_COMPLETION_STATUS = value;
                    OnPropertyChanged("WORKFLOW_COMPLETION_STATUS");
                }
            }
        }
        private string _wORKFLOW_COMPLETION_STATUS;
    
        [DataMember]
        public string CREATED_BY
        {
            get { return _cREATED_BY; }
            set
            {
                if (_cREATED_BY != value)
                {
                    _cREATED_BY = value;
                    OnPropertyChanged("CREATED_BY");
                }
            }
        }
        private string _cREATED_BY;
    
        [DataMember]
        public System.DateTime CREATED_DATE
        {
            get { return _cREATED_DATE; }
            set
            {
                if (_cREATED_DATE != value)
                {
                    _cREATED_DATE = value;
                    OnPropertyChanged("CREATED_DATE");
                }
            }
        }
        private System.DateTime _cREATED_DATE;
    
        [DataMember]
        public string MODIFIED_BY
        {
            get { return _mODIFIED_BY; }
            set
            {
                if (_mODIFIED_BY != value)
                {
                    _mODIFIED_BY = value;
                    OnPropertyChanged("MODIFIED_BY");
                }
            }
        }
        private string _mODIFIED_BY;
    
        [DataMember]
        public Nullable<System.DateTime> MODIFIED_DATE
        {
            get { return _mODIFIED_DATE; }
            set
            {
                if (_mODIFIED_DATE != value)
                {
                    _mODIFIED_DATE = value;
                    OnPropertyChanged("MODIFIED_DATE");
                }
            }
        }
        private Nullable<System.DateTime> _mODIFIED_DATE;
    
        [DataMember]
        public string ATTRIBUTE1
        {
            get { return _aTTRIBUTE1; }
            set
            {
                if (_aTTRIBUTE1 != value)
                {
                    _aTTRIBUTE1 = value;
                    OnPropertyChanged("ATTRIBUTE1");
                }
            }
        }
        private string _aTTRIBUTE1;
    
        [DataMember]
        public string ATTRIBUTE2
        {
            get { return _aTTRIBUTE2; }
            set
            {
                if (_aTTRIBUTE2 != value)
                {
                    _aTTRIBUTE2 = value;
                    OnPropertyChanged("ATTRIBUTE2");
                }
            }
        }
        private string _aTTRIBUTE2;
    
        [DataMember]
        public string ATTRIBUTE3
        {
            get { return _aTTRIBUTE3; }
            set
            {
                if (_aTTRIBUTE3 != value)
                {
                    _aTTRIBUTE3 = value;
                    OnPropertyChanged("ATTRIBUTE3");
                }
            }
        }
        private string _aTTRIBUTE3;
    
        [DataMember]
        public string ATTRIBUTE4
        {
            get { return _aTTRIBUTE4; }
            set
            {
                if (_aTTRIBUTE4 != value)
                {
                    _aTTRIBUTE4 = value;
                    OnPropertyChanged("ATTRIBUTE4");
                }
            }
        }
        private string _aTTRIBUTE4;
    
        [DataMember]
        public string ATTRIBUTE5
        {
            get { return _aTTRIBUTE5; }
            set
            {
                if (_aTTRIBUTE5 != value)
                {
                    _aTTRIBUTE5 = value;
                    OnPropertyChanged("ATTRIBUTE5");
                }
            }
        }
        private string _aTTRIBUTE5;
    
        [DataMember]
        public string ATTRIBUTE6
        {
            get { return _aTTRIBUTE6; }
            set
            {
                if (_aTTRIBUTE6 != value)
                {
                    _aTTRIBUTE6 = value;
                    OnPropertyChanged("ATTRIBUTE6");
                }
            }
        }
        private string _aTTRIBUTE6;
    
        [DataMember]
        public string ATTRIBUTE7
        {
            get { return _aTTRIBUTE7; }
            set
            {
                if (_aTTRIBUTE7 != value)
                {
                    _aTTRIBUTE7 = value;
                    OnPropertyChanged("ATTRIBUTE7");
                }
            }
        }
        private string _aTTRIBUTE7;
    
        [DataMember]
        public string ATTRIBUTE8
        {
            get { return _aTTRIBUTE8; }
            set
            {
                if (_aTTRIBUTE8 != value)
                {
                    _aTTRIBUTE8 = value;
                    OnPropertyChanged("ATTRIBUTE8");
                }
            }
        }
        private string _aTTRIBUTE8;
    
        [DataMember]
        public string ATTRIBUTE9
        {
            get { return _aTTRIBUTE9; }
            set
            {
                if (_aTTRIBUTE9 != value)
                {
                    _aTTRIBUTE9 = value;
                    OnPropertyChanged("ATTRIBUTE9");
                }
            }
        }
        private string _aTTRIBUTE9;
    
        [DataMember]
        public string ATTRIBUTE10
        {
            get { return _aTTRIBUTE10; }
            set
            {
                if (_aTTRIBUTE10 != value)
                {
                    _aTTRIBUTE10 = value;
                    OnPropertyChanged("ATTRIBUTE10");
                }
            }
        }
        private string _aTTRIBUTE10;

        #endregion
        #region Navigation Properties
    
        [DataMember]
        public INV_ITEM_MASTER INV_ITEM_MASTER
        {
            get { return _iNV_ITEM_MASTER; }
            set
            {
                if (!ReferenceEquals(_iNV_ITEM_MASTER, value))
                {
                    var previousValue = _iNV_ITEM_MASTER;
                    _iNV_ITEM_MASTER = value;
                    FixupINV_ITEM_MASTER(previousValue);
                    OnNavigationPropertyChanged("INV_ITEM_MASTER");
                }
            }
        }
        private INV_ITEM_MASTER _iNV_ITEM_MASTER;

        #endregion
        #region ChangeTracking
    
        protected virtual void OnPropertyChanged(String propertyName)
        {
            if (ChangeTracker.State != ObjectState.Added && ChangeTracker.State != ObjectState.Deleted)
            {
                ChangeTracker.State = ObjectState.Modified;
            }
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        protected virtual void OnNavigationPropertyChanged(String propertyName)
        {
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        event PropertyChangedEventHandler INotifyPropertyChanged.PropertyChanged{ add { _propertyChanged += value; } remove { _propertyChanged -= value; } }
        private event PropertyChangedEventHandler _propertyChanged;
        private ObjectChangeTracker _changeTracker;
    
        [DataMember]
        public ObjectChangeTracker ChangeTracker
        {
            get
            {
                if (_changeTracker == null)
                {
                    _changeTracker = new ObjectChangeTracker();
                    _changeTracker.ObjectStateChanging += HandleObjectStateChanging;
                }
                return _changeTracker;
            }
            set
            {
                if(_changeTracker != null)
                {
                    _changeTracker.ObjectStateChanging -= HandleObjectStateChanging;
                }
                _changeTracker = value;
                if(_changeTracker != null)
                {
                    _changeTracker.ObjectStateChanging += HandleObjectStateChanging;
                }
            }
        }
    
        private void HandleObjectStateChanging(object sender, ObjectStateChangingEventArgs e)
        {
            if (e.NewState == ObjectState.Deleted)
            {
                ClearNavigationProperties();
            }
        }
    
        protected bool IsDeserializing { get; private set; }
    
        [OnDeserializing]
        public void OnDeserializingMethod(StreamingContext context)
        {
            IsDeserializing = true;
        }
    
        [OnDeserialized]
        public void OnDeserializedMethod(StreamingContext context)
        {
            IsDeserializing = false;
            ChangeTracker.ChangeTrackingEnabled = true;
        }
    
        protected virtual void ClearNavigationProperties()
        {
            INV_ITEM_MASTER = null;
        }

        #endregion
        #region Association Fixup
    
        private void FixupINV_ITEM_MASTER(INV_ITEM_MASTER previousValue, bool skipKeys = false)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (previousValue != null && previousValue.INV_ITEM_RATES.Contains(this))
            {
                previousValue.INV_ITEM_RATES.Remove(this);
            }
    
            if (INV_ITEM_MASTER != null)
            {
                if (!INV_ITEM_MASTER.INV_ITEM_RATES.Contains(this))
                {
                    INV_ITEM_MASTER.INV_ITEM_RATES.Add(this);
                }
    
                RATE_ITEM_ID = INV_ITEM_MASTER.ITEM_ID;
            }
            else if (!skipKeys)
            {
                RATE_ITEM_ID = null;
            }
    
            if (ChangeTracker.ChangeTrackingEnabled)
            {
                if (ChangeTracker.OriginalValues.ContainsKey("INV_ITEM_MASTER")
                    && (ChangeTracker.OriginalValues["INV_ITEM_MASTER"] == INV_ITEM_MASTER))
                {
                    ChangeTracker.OriginalValues.Remove("INV_ITEM_MASTER");
                }
                else
                {
                    ChangeTracker.RecordOriginalValue("INV_ITEM_MASTER", previousValue);
                }
                if (INV_ITEM_MASTER != null && !INV_ITEM_MASTER.ChangeTracker.ChangeTrackingEnabled)
                {
                    INV_ITEM_MASTER.StartTracking();
                }
            }
        }

        #endregion
    }
}
