﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.AP
{
  public class Warehouse_DAL
    {
        static string sqlQuery = "";
        public static string getWareHouseName()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT DISTINCT IW.INV_WH_ID,IW.INV_WH_NAME" + VMVServices.Web.Utils.LanguageCode + " as INV_WH_NAME  FROM INV_WAREHOUSES IW";
            sqlQuery += " WHERE IW.WORKFLOW_COMPLETION_STATUS = 1 ";

            sqlQuery += " AND IW.ENABLED_FLAG = 1 ";
            if (VMVServices.Web.Utils.LanguageCode != string.Empty)
            {
                sqlQuery += " AND  IW.INV_WH_NAME" + VMVServices.Web.Utils.LanguageCode + "  is not null ";
            }
            sqlQuery += "    and IW.ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " ORDER BY INV_WH_NAME ";
            return sqlQuery;

        }


        public static string getLotNumber()
        {
            sqlQuery = string.Empty;

            sqlQuery = " select LH.LOT_NUMBER from INV_RECEIPT_LOTS_HDR LH";
           
            return sqlQuery;

        }
        public static string getReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM INV_WAREHOUSE_ITEM_R V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.INV_WH_ID] != null)
                {
                    sqlQuery += " AND V.WH_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.INV_WH_ID].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.ITEM_ID] != null)
                {
                    sqlQuery += " AND V.item_id = '" + VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.ITEM_ID].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["FROM_LOT_NUMBER"] != null)
                {
                    sqlQuery += " AND V.LOT_NUMBER >= '" + VMVServices.Web.Utils.ReportViewFilterParameter["FROM_LOT_NUMBER"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["TO_LOT_NUMBER"] != null)
                {
                    sqlQuery += " AND V.LOT_NUMBER <= '" + VMVServices.Web.Utils.ReportViewFilterParameter["TO_LOT_NUMBER"].ToString() + "'";
                }
            }
            return sqlQuery;
        }
        public static string getWarehouseReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery = "SELECT * FROM VW_AP_WAREHOUSE V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {

                if (VMVServices.Web.Utils.ReportViewFilterParameter["WAREHOUSE_NAME"] != null)
                {
                    sqlQuery += " AND V.INV_WH_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["WAREHOUSE_NAME"].ToString() + "'";
                }

                if (VMVServices.Web.Utils.ReportViewFilterParameter["WAREHOUSE_TYPE"] != null)
                {
                    sqlQuery += " AND V.INV_WH_TYPE = '" + VMVServices.Web.Utils.ReportViewFilterParameter["WAREHOUSE_TYPE"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["WAREHOUSE_STATUS"] != null)
                {
                    sqlQuery += " AND V.INV_WH_STATUS = '" + VMVServices.Web.Utils.ReportViewFilterParameter["WAREHOUSE_STATUS"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["CREATED_BY"] != null)
                {
                    sqlQuery += " AND UPPER(V.CREATED_BY) = UPPER('" + VMVServices.Web.Utils.ReportViewFilterParameter["CREATED_BY"].ToString() + "')";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["MODIFIED_BY"] != null)
                {
                    sqlQuery += " AND UPPER(V.MODIFIED_BY) = UPPER('" + VMVServices.Web.Utils.ReportViewFilterParameter["MODIFIED_BY"].ToString() + "')";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["CREATED_From_Date"] != null)
                {
                    sqlQuery += " AND V.CREATED_DATE >= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["CREATED_From_Date"].ToString() + "','dd/MM/yyyy') ";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["CREATED_To_Date"] != null)
                {
                    sqlQuery += " AND V.CREATED_DATE <= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["CREATED_To_Date"].ToString() + "','dd/MM/yyyy') ";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["MODIFIED_From_Date"] != null)
                {
                    sqlQuery += " AND V.MODIFIED_DATE >= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["MODIFIED_From_Date"].ToString() + "','dd/MM/yyyy') ";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["MODIFIED_To_Date"] != null)
                {
                    sqlQuery += " AND V.MODIFIED_DATE <= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["MODIFIED_To_Date"].ToString() + "','dd/MM/yyyy') ";
                }

            }


            return sqlQuery;
        }
    }

}
