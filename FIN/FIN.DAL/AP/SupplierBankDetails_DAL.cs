﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.AP
{
    public class SupplierBankDetails_DAL
    {
        static string sqlQuery = "";

        public static string GetSupplierbankdtls(long Master_id)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select scb.pk_id,cb.bank_id,cb.bank_name,bb.bank_branch_id,bb.bank_branch_name,v.code LOOKUP_ID,v.code LOOKUP_NAME,scb.vendor_bank_account_code,bb.bank_ifsc_code,bb.bank_swift_code,'N' AS DELETED,";
            // 
            sqlQuery += " CASE scb.ENABLED_FLAG WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS ENABLED_FLAG";
            sqlQuery += " from supplier_customer_bank scb,CA_BANK cb,CA_BANK_BRANCH bb,ssm_code_masters v";

            sqlQuery += " where scb.bank_id = cb.bank_id";
            sqlQuery += " and scb.bank_branch_id = bb.bank_branch_id";
            sqlQuery += " and scb.vendor_bank_acctount_type = v.code";
            sqlQuery += " and scb.pk_id = " + Master_id;
            // sqlQuery += " and scb.enabled_flag = 1";
            sqlQuery += " and scb.workflow_completion_status = 1";

            return sqlQuery;

        }

        public static string getSupplierName()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT SC.VENDOR_ID,sc.vendor_code as VENDOR_CODE";
            sqlQuery += " FROM SUPPLIER_CUSTOMERS SC ";
            sqlQuery += " WHERE SC.ENABLED_FLAG = 1";
            sqlQuery += " AND SC.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " and sc.ORG_ID ='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " ORDER BY SC.VENDOR_ID";
            return sqlQuery;
        }

        public static string getSuppNM(string VENDOR_ID)
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT sc.vendor_name" + VMVServices.Web.Utils.LanguageCode + " as vendor_name ";
            sqlQuery += " FROM SUPPLIER_CUSTOMERS SC ";
            sqlQuery += " WHERE SC.ENABLED_FLAG = 1";
            sqlQuery += " AND SC.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND SC.VENDOR_ID = '" + VENDOR_ID + "'";
            if (VMVServices.Web.Utils.LanguageCode != string.Empty)
            {
                sqlQuery += " AND  sc.vendor_name" + VMVServices.Web.Utils.LanguageCode + "  is not null ";
            }
            sqlQuery += " and sc.ORG_ID ='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " ORDER BY SC.VENDOR_ID";
            return sqlQuery;
        }

        public static string getBankName()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT CB.BANK_ID,CB.BANK_NAME" + VMVServices.Web.Utils.LanguageCode + " AS BANK_NAME";
            sqlQuery += " FROM CA_BANK CB";
            sqlQuery += " WHERE CB.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND CB.ENABLED_FLAG = 1";
            sqlQuery += " ORDER BY CB.BANK_ID";


            return sqlQuery;
        }

        public static string getBankBranchName()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT CBB.BANK_BRANCH_ID,CBB.BANK_BRANCH_NAME" + VMVServices.Web.Utils.LanguageCode + " AS BANK_BRANCH_NAME ";
            sqlQuery += " FROM CA_BANK_BRANCH CBB";
            sqlQuery += " WHERE CBB.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND CBB.ENABLED_FLAG = 1";
            sqlQuery += " ORDER BY CBB.BANK_BRANCH_ID";
            return sqlQuery;
        }

        public static string getBankBranchNameasper_bank(string BANK_ID)
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT CBB.BANK_BRANCH_ID,CBB.BANK_BRANCH_NAME";
            sqlQuery += " FROM CA_BANK_BRANCH CBB";
            sqlQuery += " WHERE CBB.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND CBB.BANK_ID = '" + BANK_ID + "'";
            sqlQuery += " AND CBB.ENABLED_FLAG = 1";
            sqlQuery += " ORDER BY CBB.BANK_BRANCH_ID";
            return sqlQuery;
        }

        public static string getAccounttype(string typeName)
        {
            sqlQuery = string.Empty;


            sqlQuery = "Select V.VALUE_KEY_ID,V.VALUE_NAME,m.TYPE_KEY_ID  From ssm_lookup_value_dtls V,ssm_lookup_type_master M";
            sqlQuery += " WHERE M.TYPE_NAME = '" + typeName + "'";
            sqlQuery += " AND V.TYPE_KEY_ID =  M.TYPE_KEY_ID";
            sqlQuery += " ORDER BY V.DISPLAY_ORDER";
            return sqlQuery;
        }

        public static string getSupplierBankDtlsReportData(bool customer)
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_AP_SUP_BANK_DTLS V WHERE ROWNUM > 0 ";
            if (customer)
            {
                sqlQuery += "  AND UPPER(V.VENDOR_TYPE) IN ('CUSTOMER','BOTH')";
            }
            else
            {
                sqlQuery += "  AND UPPER(V.VENDOR_TYPE) IN ('SUPPLIER','BOTH')";
            }

            if (VMVServices.Web.Utils.ReportViewFilterParameter["CustomerSiteID"] != null)
            {
                sqlQuery += " AND V.VENDOR_LOC_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["CustomerSiteID"].ToString() + "'";
            }
            if (VMVServices.Web.Utils.ReportViewFilterParameter["CustomerID"] != null)
            {
                sqlQuery += " AND V.VENDOR_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["CustomerID"].ToString() + "'";
            }
            if (VMVServices.Web.Utils.ReportViewFilterParameter["BankID"] != null)
            {
                sqlQuery += " AND V.BANK_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["BankID"].ToString() + "'";
            }
            if (VMVServices.Web.Utils.ReportViewFilterParameter["CREATED_BY"] != null)
            {
                sqlQuery += " AND UPPER(V.CREATED_BY) = UPPER('" + VMVServices.Web.Utils.ReportViewFilterParameter["CREATED_BY"].ToString() + "')";
            }
            if (VMVServices.Web.Utils.ReportViewFilterParameter["MODIFIED_BY"] != null)
            {
                sqlQuery += " AND UPPER(V.MODIFIED_BY) = UPPER('" + VMVServices.Web.Utils.ReportViewFilterParameter["MODIFIED_BY"].ToString() + "')";
            }
            if (VMVServices.Web.Utils.ReportViewFilterParameter["CREATED_From_Date"] != null)
            {
                sqlQuery += " AND V.CREATED_DATE >= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["CREATED_From_Date"].ToString() + "','dd/MM/yyyy') ";
            }
            if (VMVServices.Web.Utils.ReportViewFilterParameter["CREATED_To_Date"] != null)
            {
                sqlQuery += " AND V.CREATED_DATE <= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["CREATED_To_Date"].ToString() + "','dd/MM/yyyy') ";
            }
            if (VMVServices.Web.Utils.ReportViewFilterParameter["MODIFIED_From_Date"] != null)
            {
                sqlQuery += " AND V.MODIFIED_DATE >= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["MODIFIED_From_Date"].ToString() + "','dd/MM/yyyy') ";
            }
            if (VMVServices.Web.Utils.ReportViewFilterParameter["MODIFIED_To_Date"] != null)
            {
                sqlQuery += " AND V.MODIFIED_DATE <= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["MODIFIED_To_Date"].ToString() + "','dd/MM/yyyy') ";
            }

            return sqlQuery;
        }


    }
}
