﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.AP
{
    public class WarehouseFacility_DAL
    {
        static string sqlQuery = "";

        public static string GetWarehouseFacilitydtls(string Master_id)
        {
            sqlQuery = string.Empty;


            sqlQuery = " SELECT WF.PK_ID,WF.INV_WH_ID,WF.WH_FACILITY_ID,WF.WH_FACILITY_DESC,V.CODE VALUE_KEY_ID,V.CODE VALUE_NAME,WF.WH_EFF_START_DT,WF.WH_EFF_END_DT,'N' AS DELETED,";
            sqlQuery += " CASE WF.ENABLED_FLAG WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS ENABLED_FLAG";
            sqlQuery += " FROM INV_WAREHOUSE_FACILITIES WF,SSM_CODE_MASTERS V";
            sqlQuery += " WHERE WF.WH_FACILITY_CODE = V.CODE";
            sqlQuery += " AND WF.WH_FACILITY_ID = '" + Master_id + "'";
           // sqlQuery += " AND WF.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND WF.ENABLED_FLAG = 1";


            return sqlQuery;

        }

        public static string getWarehouseName()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT W.INV_WH_ID,W.INV_WH_ID ||' - '||W.INV_WH_NAME" + VMVServices.Web.Utils.LanguageCode + " as INV_WH_NAME ";
            sqlQuery += " FROM INV_WAREHOUSES W ";
            sqlQuery += " WHERE W.ENABLED_FLAG = 1";
            sqlQuery += " AND W.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " and w.ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " ORDER BY W.INV_WH_ID";
            return sqlQuery;
        }
        public static string getWHNm(string INV_WH_ID)
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT W.INV_WH_NAME" + VMVServices.Web.Utils.LanguageCode + " as INV_WH_NAME ";
            sqlQuery += " FROM INV_WAREHOUSES W";
            sqlQuery += " WHERE W.ENABLED_FLAG = 1";
            sqlQuery += " AND W.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND W.INV_WH_ID =  '" + INV_WH_ID + "'";
            sqlQuery += " and w.ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " ORDER BY W.INV_WH_ID";
            return sqlQuery;
        }
        public static string getWFDetails(string WH_ID)
        {
            sqlQuery = string.Empty;

            sqlQuery += " select distinct WF.PK_ID, wf.wh_facility_id, wf.inv_wh_id, wf.wh_facility_code, wf.wh_facility_desc,";
            sqlQuery += " wf.wh_eff_start_dt, wf.wh_eff_end_dt, wf.workflow_completion_status,";
            sqlQuery += " V.CODE VALUE_KEY_ID,V.CODE VALUE_NAME, 'N' AS DELETED ";
            sqlQuery += " ,CASE WF.ENABLED_FLAG WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS ENABLED_FLAG";
            sqlQuery += " from inv_warehouse_facilities wf ";
            sqlQuery += " left join SSM_CODE_MASTERS V on v.code = wf.wh_facility_code";
            sqlQuery += " where wf.inv_wh_id='" + WH_ID + "'";
            sqlQuery += " and wf.WH_ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " order by wf.wh_facility_id";

            return sqlQuery;
        }

        public static string getReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_AP_WAREHOUSE_FACILITY V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.INV_WH_ID] != null)
                {
                    sqlQuery += " AND V.INV_WH_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.INV_WH_ID].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["FACILITY_NAME"] != null)
                {
                    sqlQuery += " AND V.WH_FACILITY_CODE = '" + VMVServices.Web.Utils.ReportViewFilterParameter["FACILITY_NAME"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["CREATED_BY"] != null)
                {
                    sqlQuery += " AND UPPER(V.CREATED_BY) = UPPER('" + VMVServices.Web.Utils.ReportViewFilterParameter["CREATED_BY"].ToString() + "')";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["MODIFIED_BY"] != null)
                {
                    sqlQuery += " AND UPPER(V.MODIFIED_BY) = UPPER('" + VMVServices.Web.Utils.ReportViewFilterParameter["MODIFIED_BY"].ToString() + "')";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["CREATED_From_Date"] != null)
                {
                    sqlQuery += " AND V.CREATED_DATE >= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["CREATED_From_Date"].ToString() + "','dd/MM/yyyy') ";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["CREATED_To_Date"] != null)
                {
                    sqlQuery += " AND V.CREATED_DATE <= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["CREATED_To_Date"].ToString() + "','dd/MM/yyyy') ";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["MODIFIED_From_Date"] != null)
                {
                    sqlQuery += " AND V.MODIFIED_DATE >= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["MODIFIED_From_Date"].ToString() + "','dd/MM/yyyy') ";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["MODIFIED_To_Date"] != null)
                {
                    sqlQuery += " AND V.MODIFIED_DATE <= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["MODIFIED_To_Date"].ToString() + "','dd/MM/yyyy') ";
                }
            }
            return sqlQuery;
        }
    }
}
