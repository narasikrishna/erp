﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;
namespace FIN.DAL.AP
{
    public class InventoryPriceList_DAL
    {
        static string sqlQuery = "";
        public static string GetPricelistCurrency()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT SC.CURRENCY_DESC,SC.CURRENCY_ID ";
            sqlQuery += " FROM SSM_CURRENCIES SC ";
            sqlQuery += " where SC.workflow_completion_status=1";
            sqlQuery += " and SC.enabled_flag=1";
            sqlQuery += " order by SC.CURRENCY_DESC asc";

            return sqlQuery;
        }

        public static string GetItemCode()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT IM.ITEM_CODE,IM.ITEM_ID ";
            sqlQuery += " FROM INV_ITEM_MASTER IM ";
            sqlQuery += " where IM.workflow_completion_status=1";
            sqlQuery += " and IM.enabled_flag=1";
            sqlQuery += "    and IM.ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " order by IM.ITEM_CODE asc";

            return sqlQuery;
        }
        public static string GetItem_Dtls(String item_id)
        {
            sqlQuery = string.Empty;
            sqlQuery = "  SELECT IM.Item_Name" + VMVServices.Web.Utils.LanguageCode + " as Item_Name,IM.ITEM_UOM,to_char(IM.ITEM_UNIT_PRICE) as ITEM_UNIT_PRICE,um.uom_desc";
            sqlQuery += " FROM INV_ITEM_MASTER IM,inv_uom_master um ";


            sqlQuery += " WHERE im.item_uom = um.uom_id ";
            sqlQuery += " and IM.ENABLED_FLAG = '1' ";
            sqlQuery += " and IM.WORKFLOW_COMPLETION_STATUS = '1' ";
            sqlQuery += " and IM.ITEM_ID='" + item_id + "'";
            sqlQuery += " and IM.ORG_ID= '" + VMVServices.Web.Utils.OrganizationID + "'";
            //sqlQuery += " ORDER BY PROF_DTL_ID asc ";

            return sqlQuery;
        }


        public static string GetInventoryPriceListDtls(string PriceList_id = "", string itemId = "")
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT PD.PRICE_ITEM_ID,PD.PRICE_ITEM_UOM,to_char(PD.PRICE_ITEM_COST) as PRICE_ITEM_COST,PD.PRICE_DTL_ID,IM.ITEM_ID,IM.ITEM_CODE" + VMVServices.Web.Utils.LanguageCode + " as ITEM_CODE,IM.ITEM_NAME" + VMVServices.Web.Utils.LanguageCode + " as ITEM_NAME,IM.ITEM_UOM,IM.ITEM_UOM,um.uom_desc, ";
            sqlQuery += " to_char(nvl(PD.PRICE_ITEM_COST,0)) as PO_ITEM_UNIT_PRICE,case PD.ENABLED_FLAG when '1' then 'TRUE' else 'FALSE' end as ENABLED_FLAG,'N' as DELETED";
            sqlQuery += " FROM INV_PRICE_LIST_HDR PH,INV_PRICE_LIST_DTL PD,INV_ITEM_MASTER IM,inv_uom_master um";
            sqlQuery += " where PH.PRICE_HDR_ID = PD.PRICE_HDR_ID ";
            sqlQuery += " AND PD.PRICE_ITEM_ID = IM.ITEM_ID ";
            sqlQuery += " and im.item_uom = um.uom_id ";


            if (PriceList_id != string.Empty)
            {
                sqlQuery += " and PH.PRICE_HDR_ID = '" + PriceList_id + "'";
            }

            if (itemId != string.Empty)
            {
                sqlQuery += " and IM.ITEM_ID = '" + itemId + "'";
            }
            sqlQuery += "  and ph.price_org_id='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " order by PRICE_DTL_ID ";
            return sqlQuery;
        }
      
        public static string GetLoanRepayDtls4Contract(string strContractId)
        {
            sqlQuery = string.Empty;
            sqlQuery = " select rd.ln_prn_repay_dtl_id, rd.ln_prn_repay_id, rd.ln_installment_no,rd.ln_installment_dt,rd.ln_amount as ln_amount,rd.LN_PS_AMOUNT as LN_PS_AMOUNT,LN_REVISED_PS_AMT, ";
            sqlQuery += " case rd.ENABLED_FLAG when '1' then 'TRUE' else 'FALSE' end as ENABLED_FLAG,'N' as DELETED";
            sqlQuery += ",NVL(rd.LN_PAID_DT,rd.ln_installment_dt) as Paid_Date ";
            sqlQuery += ",NVL(rd.LN_PAID_AMT,ln_amount) as PAID_AMT ";
            sqlQuery += "  from ln_repay_hdr rh, ln_repay_dtl rd ";
            sqlQuery += " where rh.ln_prn_repay_id = rd.ln_prn_repay_id ";
            sqlQuery += " and rh.ln_contract_id=  '" + strContractId + "'";
            return sqlQuery;

        }
        public static string GetInventoryPriceListDtls_frPO(string PriceList_id, string itemId, DateTime Startdt)
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT PD.PRICE_ITEM_ID,PD.PRICE_ITEM_UOM,PD.PRICE_ITEM_COST,PD.PRICE_DTL_ID,IM.ITEM_ID,IM.ITEM_CODE" + VMVServices.Web.Utils.LanguageCode + " as ITEM_CODE,IM.ITEM_NAME" + VMVServices.Web.Utils.LanguageCode + " as ITEM_NAME,IM.ITEM_UOM,IM.ITEM_UOM, ";
            sqlQuery += " PD.PRICE_ITEM_COST as PO_ITEM_UNIT_PRICE,case PD.ENABLED_FLAG when '1' then 'TRUE' else 'FALSE' end as ENABLED_FLAG,'N' as DELETED";
            sqlQuery += " FROM INV_PRICE_LIST_HDR PH,INV_PRICE_LIST_DTL PD,INV_ITEM_MASTER IM";
            sqlQuery += " where PH.PRICE_HDR_ID = PD.PRICE_HDR_ID ";
            sqlQuery += " AND PD.PRICE_ITEM_ID = IM.ITEM_ID ";

            if (PriceList_id != string.Empty)
            {
                sqlQuery += " and PH.PRICE_HDR_ID = '" + PriceList_id + "'";
            }

            if (itemId != string.Empty)
            {
                sqlQuery += " and IM.ITEM_ID = '" + itemId + "'";
            }


            sqlQuery += " and ph.price_list_start_dt <='" + Startdt.ToString("dd/MMM/yyyy") + "'";

            sqlQuery += " and nvl(ph.price_list_end_dt,sysdate) >='" + Startdt.ToString("dd/MMM/yyyy") + "'";


            sqlQuery += "  and ph.price_org_id='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " order by PRICE_DTL_ID ";
            return sqlQuery;
        }



        public static string GetInventoryPriceList()
        {
            sqlQuery = string.Empty;
            sqlQuery = "  select ph.price_hdr_id,ph.price_list_name";
            sqlQuery += "  fROM INV_PRICE_LIST_HDR PH";
            sqlQuery += "  where ph.enabled_flag='1'";
            sqlQuery += "  and ph.workflow_completion_status='1'";
            sqlQuery += "  and ph.price_org_id='" + VMVServices.Web.Utils.OrganizationID + "'";
            return sqlQuery;
        }

        public static string getInventoryPriceListReport()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_AP_INVENTORY_PRICELIST V WHERE ROWNUM > 0 ";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["InvPriceListHdrID"] != null)
                {
                    sqlQuery += " AND V.PRICE_HDR_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["InvPriceListHdrID"].ToString() + "'";
                }
            }
            return sqlQuery;
        }
        
    }
}
