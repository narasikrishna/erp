﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.AP
{
    public class Invoice_DAL
    {
        static string sqlQuery = "";
        public static string getPrepaymentAmount4PO(string str_poNo)
        {
            sqlQuery = string.Empty;
            sqlQuery += " SELECT ID.RECEIPT_ID as PO_NUM,INV_ITEM_AMT ";
            sqlQuery += " FROM INV_INVOICES_DTLS ID ";
            sqlQuery += " INNER JOIN INV_INVOICES_HDR IH ON IH.INV_ID= ID.INV_ID  ";
            sqlQuery += " WHERE UPPER(IH.INV_TYPE)='PREPAYMENT' AND ID.INV_LINE_TYPE='PO' ";
            sqlQuery += " AND ID.RECEIPT_ID='" + str_poNo + "' ";
            return sqlQuery;
        }

        public static string getTempInvoice(string str_userid)
        {
            sqlQuery = string.Empty;
            sqlQuery += " SELECT IH.INV_ID,IH.INV_NUM,IH.INV_DATE,IH.VENDOR_ID FROM TMP_INV_INVOICES_HDR IH  ";
            //sqlQuery += "  LEFT JOIN gl_comp_acct_period_dtl PP ON pp.period_id = TJH.JE_PERIOD_ID  ";
            sqlQuery += " WHERE IH.CREATED_BY='" + str_userid + "'";
            return sqlQuery;

        }
        public static string getInvoiceDetails(string str_INVID)
        {
            sqlQuery = string.Empty;

            sqlQuery += " SELECT ID.INV_LINE_ID,ID.INV_LINE_NUM,ID.RECEIPT_ID,id.ATTRIBUTE1 as MIS_NO ";
            sqlQuery += " ,IRH.INV_NUM AS GRN_NUM  ";
            sqlQuery += " ,ID.INV_ITEM_ID as ITEM_ID";
            sqlQuery += " ,'' as ITEM_CODE,'' as ITEM_NAME ";
            sqlQuery += " ,to_char(ID.INV_ITEM_QTY_BILLED) as INV_ITEM_QTY_BILLED,0 as unbilled_quantity,to_char(ID.INV_ITEM_PRICE) as INV_ITEM_PRICE,to_char(ID.INV_ITEM_AMT) as INV_ITEM_AMT,ID.INV_ITEM_ACCT_CODE_ID as code_id";
            sqlQuery += "  ,(SELECT GAC.ACCT_CODE ||' - '|| GAC.ACCT_CODE_DESC" + VMVServices.Web.Utils.LanguageCode + " AS CODE_NAME";
            sqlQuery += "  FROM GL_ACCT_CODES GAC";
            sqlQuery += "   WHERE GAC.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += "    AND GAC.ENABLED_FLAG = 1 and id.INV_ITEM_ACCT_CODE_ID=GAC.ACCT_CODE_ID";
            sqlQuery += "  ) as code_name";
            sqlQuery += " ,INV_SEGMENT_ID1,INV_SEGMENT_ID2,INV_SEGMENT_ID3,INV_SEGMENT_ID4,INV_SEGMENT_ID5,INV_SEGMENT_ID6";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = INV_SEGMENT_ID1 ) AS SEGMENT_1_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = INV_SEGMENT_ID2 ) AS SEGMENT_2_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = INV_SEGMENT_ID3 ) AS SEGMENT_3_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = INV_SEGMENT_ID4 ) AS SEGMENT_4_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = INV_SEGMENT_ID5 ) AS SEGMENT_5_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = INV_SEGMENT_ID6 ) AS SEGMENT_6_TEXT";
            sqlQuery += " ,ID.INV_LINE_TYPE,SCM_LT.DESCRIPTION" + VMVServices.Web.Utils.LanguageCode + " AS INV_LINE_TYPE_DESC ";
            sqlQuery += " , INV_LINE_TAX_AMT ";
            sqlQuery += " ,'0' AS DELETED ";
            sqlQuery += " FROM INV_INVOICES_DTLS ID ";
            sqlQuery += " INNER JOIN INV_INVOICES_HDR IRH ON IRH.INV_ID= ID.RECEIPT_ID ";
            // sqlQuery += " INNER JOIN INV_ITEM_MASTER ITM ON ITM.ITEM_ID= ID.INV_ITEM_ID ";
            sqlQuery += " INNER JOIN SSM_CODE_MASTERS SCM_LT ON SCM_LT.CODE=ID.INV_LINE_TYPE AND upper(SCM_LT.PARENT_CODE)='LINETYPE' ";
            sqlQuery += " WHERE ID.INV_ID='" + str_INVID + "' AND lower(INV_LINE_TYPE)='invoice' ";


            sqlQuery += " UNION ";
            sqlQuery += " SELECT ID.INV_LINE_ID, ";
            sqlQuery += " ID.INV_LINE_NUM, ";
            sqlQuery += " ID.RECEIPT_ID, id.ATTRIBUTE1 as MIS_NO,";
            sqlQuery += " SCM_OT.CODE as GRN_NUM, ";
            sqlQuery += " ID.INV_ITEM_ID as ITEM_ID, ";
            sqlQuery += " SCM_OT.CODE as ITEM_CODE, ";
            sqlQuery += " SCM_OT.Description" + VMVServices.Web.Utils.LanguageCode + " as ITEM_NAME, ";
            sqlQuery += " to_char(ID.INV_ITEM_QTY_BILLED) as INV_ITEM_QTY_BILLED,0 as unbilled_quantity, ";
            sqlQuery += " to_char(ID.INV_ITEM_PRICE) as INV_ITEM_PRICE, ";
            sqlQuery += " to_char(ID.INV_ITEM_AMT) as INV_ITEM_AMT, ";
            sqlQuery += " ID.INV_ITEM_ACCT_CODE_ID as code_id, ";
            sqlQuery += " (SELECT GAC.ACCT_CODE || ' - ' || GAC.ACCT_CODE_DESC" + VMVServices.Web.Utils.LanguageCode + " AS CODE_NAME ";
            sqlQuery += " FROM GL_ACCT_CODES GAC ";
            sqlQuery += " WHERE GAC.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " AND GAC.ENABLED_FLAG = 1 ";
            sqlQuery += " and id.INV_ITEM_ACCT_CODE_ID = GAC.ACCT_CODE_ID) as code_name, ";
            sqlQuery += " INV_SEGMENT_ID1, ";
            sqlQuery += " INV_SEGMENT_ID2, ";
            sqlQuery += " INV_SEGMENT_ID3, ";
            sqlQuery += " INV_SEGMENT_ID4, ";
            sqlQuery += " INV_SEGMENT_ID5, ";
            sqlQuery += " INV_SEGMENT_ID6 ";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = INV_SEGMENT_ID1 ) AS SEGMENT_1_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = INV_SEGMENT_ID2 ) AS SEGMENT_2_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = INV_SEGMENT_ID3 ) AS SEGMENT_3_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = INV_SEGMENT_ID4 ) AS SEGMENT_4_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = INV_SEGMENT_ID5 ) AS SEGMENT_5_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = INV_SEGMENT_ID6 ) AS SEGMENT_6_TEXT";
            sqlQuery += " ,ID.INV_LINE_TYPE, ";
            sqlQuery += " SCM_LT.DESCRIPTION" + VMVServices.Web.Utils.LanguageCode + " AS INV_LINE_TYPE_DESC, ";
            sqlQuery += " INV_LINE_TAX_AMT, ";
            sqlQuery += " '0' AS DELETED ";
            sqlQuery += "  FROM INV_INVOICES_DTLS ID   ";
            sqlQuery += "  INNER JOIN SSM_CODE_MASTERS SCM_LT ON SCM_LT.CODE = ID.INV_LINE_TYPE AND SCM_LT.PARENT_CODE = 'LINETYPE' ";
            sqlQuery += " INNER JOIN SSM_CODE_MASTERS SCM_OT ON SCM_OT.CODE = ID.Receipt_Id AND upper(SCM_OT.PARENT_CODE) = 'OC' ";
            sqlQuery += " WHERE ID.INV_ID = '" + str_INVID + "' AND lower(INV_LINE_TYPE)='other_charges' ";

            sqlQuery += " UNION ";
            sqlQuery += "  SELECT ID.INV_LINE_ID, ";
            sqlQuery += " ID.INV_LINE_NUM, ";
            sqlQuery += " ID.RECEIPT_ID,id.ATTRIBUTE1 as MIS_NO, ";
            sqlQuery += " ITM.Item_Name as GRN_NUM, ";
            sqlQuery += "  ID.INV_ITEM_ID as ITEM_ID, ";
            sqlQuery += "  ITM.ITEM_CODE, ";
            sqlQuery += "  ITM.ITEM_NAME, ";
            sqlQuery += "  to_char(ID.INV_ITEM_QTY_BILLED) as INV_ITEM_QTY_BILLED, 0 as unbilled_quantity,";
            sqlQuery += "  to_char(ID.INV_ITEM_PRICE) as INV_ITEM_PRICE, ";
            sqlQuery += "  to_char(ID.INV_ITEM_AMT) as INV_ITEM_AMT, ";
            sqlQuery += " ID.INV_ITEM_ACCT_CODE_ID as code_id, ";
            sqlQuery += " (SELECT GAC.ACCT_CODE || ' - ' || GAC.ACCT_CODE_DESC" + VMVServices.Web.Utils.LanguageCode + " AS CODE_NAME ";
            sqlQuery += "    FROM GL_ACCT_CODES GAC ";
            sqlQuery += "  WHERE GAC.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += "   AND GAC.ENABLED_FLAG = 1 ";
            sqlQuery += "   and id.INV_ITEM_ACCT_CODE_ID = GAC.ACCT_CODE_ID) as code_name, ";
            sqlQuery += "  INV_SEGMENT_ID1, ";
            sqlQuery += "  INV_SEGMENT_ID2, ";
            sqlQuery += "  INV_SEGMENT_ID3, ";
            sqlQuery += "  INV_SEGMENT_ID4, ";
            sqlQuery += "  INV_SEGMENT_ID5, ";
            sqlQuery += "  INV_SEGMENT_ID6 ";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = INV_SEGMENT_ID1 ) AS SEGMENT_1_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = INV_SEGMENT_ID2 ) AS SEGMENT_2_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = INV_SEGMENT_ID3 ) AS SEGMENT_3_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = INV_SEGMENT_ID4 ) AS SEGMENT_4_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = INV_SEGMENT_ID5 ) AS SEGMENT_5_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = INV_SEGMENT_ID6 ) AS SEGMENT_6_TEXT";
            sqlQuery += "  ,ID.INV_LINE_TYPE, ";
            sqlQuery += "   SCM_LT.DESCRIPTION" + VMVServices.Web.Utils.LanguageCode + " AS INV_LINE_TYPE_DESC, ";
            sqlQuery += "   INV_LINE_TAX_AMT, ";
            sqlQuery += "   '0' AS DELETED ";
            sqlQuery += "  FROM INV_INVOICES_DTLS ID  ";
            sqlQuery += " INNER JOIN INV_ITEM_MASTER ITM ON ITM.ITEM_ID = ID.RECEIPT_ID ";
            sqlQuery += " INNER JOIN SSM_CODE_MASTERS SCM_LT ON SCM_LT.CODE = ID.INV_LINE_TYPE AND upper(SCM_LT.PARENT_CODE) = 'LINETYPE' ";
            sqlQuery += "  WHERE ID.INV_ID = '" + str_INVID + "' and lower(INV_LINE_TYPE) in ('item','service') ";


            sqlQuery += " UNION ";
            sqlQuery += " SELECT ID.INV_LINE_ID, ";
            sqlQuery += " ID.INV_LINE_NUM, ";
            sqlQuery += " ID.RECEIPT_ID,id.ATTRIBUTE1 as MIS_NO, ";
            sqlQuery += " PO.PO_NUM as GRN_NUM, ";
            sqlQuery += " ID.INV_ITEM_ID as ITEM_ID, ";
            sqlQuery += " po.po_header_id as ITEM_CODE, ";
            sqlQuery += " (select iim.item_name from inv_item_master iim where iim.item_id=id.inv_item_id) AS ITEM_NAME, ";
            sqlQuery += " to_char(ID.INV_ITEM_QTY_BILLED) as INV_ITEM_QTY_BILLED,0 as unbilled_quantity, ";
            sqlQuery += " to_char(ID.INV_ITEM_PRICE) as INV_ITEM_PRICE, ";
            sqlQuery += " to_char(ID.INV_ITEM_AMT) as INV_ITEM_AMT, ";
            sqlQuery += " ID.INV_ITEM_ACCT_CODE_ID as code_id, ";
            sqlQuery += " (SELECT GAC.ACCT_CODE || ' - ' || GAC.ACCT_CODE_DESC" + VMVServices.Web.Utils.LanguageCode + " AS CODE_NAME ";
            sqlQuery += "    FROM GL_ACCT_CODES GAC ";
            sqlQuery += "   WHERE GAC.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += "     AND GAC.ENABLED_FLAG = 1 ";
            sqlQuery += "     and id.INV_ITEM_ACCT_CODE_ID = GAC.ACCT_CODE_ID) as code_name, ";
            sqlQuery += " INV_SEGMENT_ID1, ";
            sqlQuery += "  INV_SEGMENT_ID2, ";
            sqlQuery += " INV_SEGMENT_ID3, ";
            sqlQuery += " INV_SEGMENT_ID4, ";
            sqlQuery += " INV_SEGMENT_ID5, ";
            sqlQuery += " INV_SEGMENT_ID6";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = INV_SEGMENT_ID1 ) AS SEGMENT_1_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = INV_SEGMENT_ID2 ) AS SEGMENT_2_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = INV_SEGMENT_ID3 ) AS SEGMENT_3_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = INV_SEGMENT_ID4 ) AS SEGMENT_4_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = INV_SEGMENT_ID5 ) AS SEGMENT_5_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = INV_SEGMENT_ID6 ) AS SEGMENT_6_TEXT";
            sqlQuery += " ,ID.INV_LINE_TYPE, ";
            sqlQuery += " SCM_LT.DESCRIPTION" + VMVServices.Web.Utils.LanguageCode + " AS INV_LINE_TYPE_DESC, ";
            sqlQuery += " INV_LINE_TAX_AMT, ";
            sqlQuery += " '0' AS DELETED ";
            sqlQuery += " FROM INV_INVOICES_DTLS ID  ";
            sqlQuery += " INNER JOIN PO_HEADERS PO ON PO.PO_HEADER_ID = ID.RECEIPT_ID ";
            sqlQuery += " INNER JOIN SSM_CODE_MASTERS SCM_LT ON SCM_LT.CODE = ID.INV_LINE_TYPE AND upper(SCM_LT.PARENT_CODE) = 'LINETYPE' ";
            sqlQuery += " WHERE ID.INV_ID = '" + str_INVID + "' and upper(INV_LINE_TYPE) in ('PO_ITEM','PO')";


            sqlQuery += " union";

            sqlQuery += "  SELECT ID.INV_LINE_ID,";
            sqlQuery += "   ID.INV_LINE_NUM,";
            sqlQuery += "   ID.RECEIPT_ID,";
            sqlQuery += "  id.ATTRIBUTE1 as MIS_NO,";
            sqlQuery += "  '' as GRN_NUM,";
            sqlQuery += "  ID.INV_ITEM_ID as ITEM_ID,";
            sqlQuery += "  '' as ITEM_CODE,";
            sqlQuery += " '' as ITEM_NAME,";
            sqlQuery += "  to_char(ID.INV_ITEM_QTY_BILLED) as INV_ITEM_QTY_BILLED,";
            sqlQuery += "  0 as unbilled_quantity,";
            sqlQuery += " to_char(ID.INV_ITEM_PRICE) as INV_ITEM_PRICE,";
            sqlQuery += " to_char(ID.INV_ITEM_AMT) as INV_ITEM_AMT,";
            sqlQuery += " ID.INV_ITEM_ACCT_CODE_ID as code_id,";
            sqlQuery += " (SELECT GAC.ACCT_CODE || ' - ' || GAC.ACCT_CODE_DESC AS CODE_NAME";
            sqlQuery += "  FROM GL_ACCT_CODES GAC";
            sqlQuery += " WHERE GAC.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND GAC.ENABLED_FLAG = 1";
            sqlQuery += "  and id.INV_ITEM_ACCT_CODE_ID = GAC.ACCT_CODE_ID) as code_name,";
            sqlQuery += "  INV_SEGMENT_ID1,";
            sqlQuery += " INV_SEGMENT_ID2,";
            sqlQuery += " INV_SEGMENT_ID3,";
            sqlQuery += " INV_SEGMENT_ID4,";
            sqlQuery += " INV_SEGMENT_ID5,";
            sqlQuery += " INV_SEGMENT_ID6";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = INV_SEGMENT_ID1 ) AS SEGMENT_1_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = INV_SEGMENT_ID2 ) AS SEGMENT_2_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = INV_SEGMENT_ID3 ) AS SEGMENT_3_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = INV_SEGMENT_ID4 ) AS SEGMENT_4_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = INV_SEGMENT_ID5 ) AS SEGMENT_5_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = INV_SEGMENT_ID6 ) AS SEGMENT_6_TEXT";
            sqlQuery += " ,ID.INV_LINE_TYPE,";
            sqlQuery += " SCM_LT.DESCRIPTION AS INV_LINE_TYPE_DESC,";
            sqlQuery += " INV_LINE_TAX_AMT,";
            sqlQuery += " '0' AS DELETED";
            sqlQuery += " FROM INV_INVOICES_DTLS ID, SSM_CODE_MASTERS SCM_LT";
            sqlQuery += " where SCM_LT.CODE = ID.INV_LINE_TYPE";
            sqlQuery += " AND SCM_LT.PARENT_CODE = 'LINETYPE'";
            sqlQuery += " and lower(INV_LINE_TYPE) = 'miscellaneous'";
            sqlQuery += "  and ID.INV_ID = '" + str_INVID + "'";


            return sqlQuery;
        }


        public static string getTempInvoiceDetails(string str_INVID)
        {
            sqlQuery = string.Empty;

            sqlQuery += " SELECT '0' as INV_LINE_ID,ID.INV_LINE_NUM,ID.RECEIPT_ID,id.ATTRIBUTE1 as MIS_NO ";
            sqlQuery += " ,IRH.INV_NUM AS GRN_NUM  ";
            sqlQuery += " ,ID.INV_ITEM_ID as ITEM_ID";
            sqlQuery += " ,'' as ITEM_CODE,'' as ITEM_NAME ";
            sqlQuery += " ,to_char(ID.INV_ITEM_QTY_BILLED) as INV_ITEM_QTY_BILLED,0 as unbilled_quantity,to_char(ID.INV_ITEM_PRICE) as INV_ITEM_PRICE,to_char(ID.INV_ITEM_AMT) as INV_ITEM_AMT,ID.INV_ITEM_ACCT_CODE_ID as code_id";
            sqlQuery += "  ,(SELECT GAC.ACCT_CODE ||' - '|| GAC.ACCT_CODE_DESC" + VMVServices.Web.Utils.LanguageCode + " AS CODE_NAME";
            sqlQuery += "  FROM GL_ACCT_CODES GAC";
            sqlQuery += "   WHERE GAC.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += "    AND GAC.ENABLED_FLAG = 1 and id.INV_ITEM_ACCT_CODE_ID=GAC.ACCT_CODE_ID";
            sqlQuery += "  ) as code_name";
            sqlQuery += " ,INV_SEGMENT_ID1,INV_SEGMENT_ID2,INV_SEGMENT_ID3,INV_SEGMENT_ID4,INV_SEGMENT_ID5,INV_SEGMENT_ID6";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = INV_SEGMENT_ID1 ) AS SEGMENT_1_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = INV_SEGMENT_ID2 ) AS SEGMENT_2_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = INV_SEGMENT_ID3 ) AS SEGMENT_3_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = INV_SEGMENT_ID4 ) AS SEGMENT_4_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = INV_SEGMENT_ID5 ) AS SEGMENT_5_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = INV_SEGMENT_ID6 ) AS SEGMENT_6_TEXT";
            sqlQuery += " ,ID.INV_LINE_TYPE,SCM_LT.DESCRIPTION" + VMVServices.Web.Utils.LanguageCode + " AS INV_LINE_TYPE_DESC ";
            sqlQuery += " , INV_LINE_TAX_AMT ";
            sqlQuery += " ,'0' AS DELETED ";
            sqlQuery += " FROM TMP_INV_INVOICES_DTLS ID ";
            sqlQuery += " INNER JOIN TMP_INV_INVOICES_HDR IRH ON IRH.INV_ID= ID.RECEIPT_ID ";
            // sqlQuery += " INNER JOIN INV_ITEM_MASTER ITM ON ITM.ITEM_ID= ID.INV_ITEM_ID ";
            sqlQuery += " INNER JOIN SSM_CODE_MASTERS SCM_LT ON SCM_LT.CODE=ID.INV_LINE_TYPE AND upper(SCM_LT.PARENT_CODE)='LINETYPE' ";
            sqlQuery += " WHERE ID.INV_ID='" + str_INVID + "' AND lower(INV_LINE_TYPE)='invoice' ";


            sqlQuery += " UNION ";
            sqlQuery += " SELECT '0' as INV_LINE_ID, ";
            sqlQuery += " ID.INV_LINE_NUM, ";
            sqlQuery += " ID.RECEIPT_ID, id.ATTRIBUTE1 as MIS_NO,";
            sqlQuery += " SCM_OT.CODE as GRN_NUM, ";
            sqlQuery += " ID.INV_ITEM_ID as ITEM_ID, ";
            sqlQuery += " SCM_OT.CODE as ITEM_CODE, ";
            sqlQuery += " SCM_OT.Description" + VMVServices.Web.Utils.LanguageCode + " as ITEM_NAME, ";
            sqlQuery += " to_char(ID.INV_ITEM_QTY_BILLED) as INV_ITEM_QTY_BILLED,0 as unbilled_quantity, ";
            sqlQuery += " to_char(ID.INV_ITEM_PRICE) as INV_ITEM_PRICE, ";
            sqlQuery += " to_char(ID.INV_ITEM_AMT) as INV_ITEM_AMT, ";
            sqlQuery += " ID.INV_ITEM_ACCT_CODE_ID as code_id, ";
            sqlQuery += " (SELECT GAC.ACCT_CODE || ' - ' || GAC.ACCT_CODE_DESC" + VMVServices.Web.Utils.LanguageCode + " AS CODE_NAME ";
            sqlQuery += " FROM GL_ACCT_CODES GAC ";
            sqlQuery += " WHERE GAC.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " AND GAC.ENABLED_FLAG = 1 ";
            sqlQuery += " and id.INV_ITEM_ACCT_CODE_ID = GAC.ACCT_CODE_ID) as code_name, ";
            sqlQuery += " INV_SEGMENT_ID1, ";
            sqlQuery += " INV_SEGMENT_ID2, ";
            sqlQuery += " INV_SEGMENT_ID3, ";
            sqlQuery += " INV_SEGMENT_ID4, ";
            sqlQuery += " INV_SEGMENT_ID5, ";
            sqlQuery += " INV_SEGMENT_ID6 ";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = INV_SEGMENT_ID1 ) AS SEGMENT_1_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = INV_SEGMENT_ID2 ) AS SEGMENT_2_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = INV_SEGMENT_ID3 ) AS SEGMENT_3_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = INV_SEGMENT_ID4 ) AS SEGMENT_4_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = INV_SEGMENT_ID5 ) AS SEGMENT_5_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = INV_SEGMENT_ID6 ) AS SEGMENT_6_TEXT";
            sqlQuery += " ,ID.INV_LINE_TYPE, ";
            sqlQuery += " SCM_LT.DESCRIPTION" + VMVServices.Web.Utils.LanguageCode + " AS INV_LINE_TYPE_DESC, ";
            sqlQuery += " INV_LINE_TAX_AMT, ";
            sqlQuery += " '0' AS DELETED ";
            sqlQuery += "  FROM TMP_INV_INVOICES_DTLS ID   ";
            sqlQuery += "  INNER JOIN SSM_CODE_MASTERS SCM_LT ON SCM_LT.CODE = ID.INV_LINE_TYPE AND SCM_LT.PARENT_CODE = 'LINETYPE' ";
            sqlQuery += " INNER JOIN SSM_CODE_MASTERS SCM_OT ON SCM_OT.CODE = ID.Receipt_Id AND upper(SCM_OT.PARENT_CODE) = 'OC' ";
            sqlQuery += " WHERE ID.INV_ID = '" + str_INVID + "' AND lower(INV_LINE_TYPE)='other_charges' ";

            sqlQuery += " UNION ";
            sqlQuery += "  SELECT '0' as INV_LINE_ID, ";
            sqlQuery += " ID.INV_LINE_NUM, ";
            sqlQuery += " ID.RECEIPT_ID,id.ATTRIBUTE1 as MIS_NO, ";
            sqlQuery += " ITM.Item_Id as GRN_NUM, ";
            sqlQuery += "  ID.INV_ITEM_ID as ITEM_ID, ";
            sqlQuery += "  ITM.ITEM_CODE, ";
            sqlQuery += "  ITM.ITEM_NAME, ";
            sqlQuery += "  to_char(ID.INV_ITEM_QTY_BILLED) as INV_ITEM_QTY_BILLED, 0 as unbilled_quantity,";
            sqlQuery += "  to_char(ID.INV_ITEM_PRICE) as INV_ITEM_PRICE, ";
            sqlQuery += "  to_char(ID.INV_ITEM_AMT) as INV_ITEM_AMT, ";
            sqlQuery += " ID.INV_ITEM_ACCT_CODE_ID as code_id, ";
            sqlQuery += " (SELECT GAC.ACCT_CODE || ' - ' || GAC.ACCT_CODE_DESC" + VMVServices.Web.Utils.LanguageCode + " AS CODE_NAME ";
            sqlQuery += "    FROM GL_ACCT_CODES GAC ";
            sqlQuery += "  WHERE GAC.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += "   AND GAC.ENABLED_FLAG = 1 ";
            sqlQuery += "   and id.INV_ITEM_ACCT_CODE_ID = GAC.ACCT_CODE_ID) as code_name, ";
            sqlQuery += "  INV_SEGMENT_ID1, ";
            sqlQuery += "  INV_SEGMENT_ID2, ";
            sqlQuery += "  INV_SEGMENT_ID3, ";
            sqlQuery += "  INV_SEGMENT_ID4, ";
            sqlQuery += "  INV_SEGMENT_ID5, ";
            sqlQuery += "  INV_SEGMENT_ID6 ";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = INV_SEGMENT_ID1 ) AS SEGMENT_1_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = INV_SEGMENT_ID2 ) AS SEGMENT_2_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = INV_SEGMENT_ID3 ) AS SEGMENT_3_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = INV_SEGMENT_ID4 ) AS SEGMENT_4_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = INV_SEGMENT_ID5 ) AS SEGMENT_5_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = INV_SEGMENT_ID6 ) AS SEGMENT_6_TEXT";
            sqlQuery += "  ,ID.INV_LINE_TYPE, ";
            sqlQuery += "   SCM_LT.DESCRIPTION" + VMVServices.Web.Utils.LanguageCode + " AS INV_LINE_TYPE_DESC, ";
            sqlQuery += "   INV_LINE_TAX_AMT, ";
            sqlQuery += "   '0' AS DELETED ";
            sqlQuery += "  FROM TMP_INV_INVOICES_DTLS ID  ";
            sqlQuery += " INNER JOIN INV_ITEM_MASTER ITM ON ITM.ITEM_ID = ID.RECEIPT_ID ";
            sqlQuery += " INNER JOIN SSM_CODE_MASTERS SCM_LT ON SCM_LT.CODE = ID.INV_LINE_TYPE AND upper(SCM_LT.PARENT_CODE) = 'LINETYPE' ";
            sqlQuery += "  WHERE ID.INV_ID = '" + str_INVID + "' and lower(INV_LINE_TYPE) in ('item','service') ";


            sqlQuery += " UNION ";
            sqlQuery += " SELECT '0' as INV_LINE_ID, ";
            sqlQuery += " ID.INV_LINE_NUM, ";
            sqlQuery += " ID.RECEIPT_ID,id.ATTRIBUTE1 as MIS_NO, ";
            sqlQuery += " PO.Po_Header_Id as GRN_NUM, ";
            sqlQuery += " ID.INV_ITEM_ID as ITEM_ID, ";
            sqlQuery += " po.po_header_id as ITEM_CODE, ";
            sqlQuery += " (select iim.item_name from inv_item_master iim where iim.item_id=id.inv_item_id) AS ITEM_NAME, ";
            sqlQuery += " to_char(ID.INV_ITEM_QTY_BILLED) as INV_ITEM_QTY_BILLED,0 as unbilled_quantity, ";
            sqlQuery += " to_char(ID.INV_ITEM_PRICE) as INV_ITEM_PRICE, ";
            sqlQuery += " to_char(ID.INV_ITEM_AMT) as INV_ITEM_AMT, ";
            sqlQuery += " ID.INV_ITEM_ACCT_CODE_ID as code_id, ";
            sqlQuery += " (SELECT GAC.ACCT_CODE || ' - ' || GAC.ACCT_CODE_DESC" + VMVServices.Web.Utils.LanguageCode + " AS CODE_NAME ";
            sqlQuery += "    FROM GL_ACCT_CODES GAC ";
            sqlQuery += "   WHERE GAC.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += "     AND GAC.ENABLED_FLAG = 1 ";
            sqlQuery += "     and id.INV_ITEM_ACCT_CODE_ID = GAC.ACCT_CODE_ID) as code_name, ";
            sqlQuery += " INV_SEGMENT_ID1, ";
            sqlQuery += "  INV_SEGMENT_ID2, ";
            sqlQuery += " INV_SEGMENT_ID3, ";
            sqlQuery += " INV_SEGMENT_ID4, ";
            sqlQuery += " INV_SEGMENT_ID5, ";
            sqlQuery += " INV_SEGMENT_ID6 ";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = INV_SEGMENT_ID1 ) AS SEGMENT_1_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = INV_SEGMENT_ID2 ) AS SEGMENT_2_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = INV_SEGMENT_ID3 ) AS SEGMENT_3_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = INV_SEGMENT_ID4 ) AS SEGMENT_4_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = INV_SEGMENT_ID5 ) AS SEGMENT_5_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = INV_SEGMENT_ID6 ) AS SEGMENT_6_TEXT";
            sqlQuery += " ,ID.INV_LINE_TYPE, ";
            sqlQuery += " SCM_LT.DESCRIPTION" + VMVServices.Web.Utils.LanguageCode + " AS INV_LINE_TYPE_DESC, ";
            sqlQuery += " INV_LINE_TAX_AMT, ";
            sqlQuery += " '0' AS DELETED ";
            sqlQuery += " FROM TMP_INV_INVOICES_DTLS ID  ";
            sqlQuery += " INNER JOIN PO_HEADERS PO ON PO.PO_HEADER_ID = ID.RECEIPT_ID ";
            sqlQuery += " INNER JOIN SSM_CODE_MASTERS SCM_LT ON SCM_LT.CODE = ID.INV_LINE_TYPE AND upper(SCM_LT.PARENT_CODE) = 'LINETYPE' ";
            sqlQuery += " WHERE ID.INV_ID = '" + str_INVID + "' and upper(INV_LINE_TYPE)='PO_ITEM'";


            sqlQuery += " union";

            sqlQuery += "  SELECT '0' as INV_LINE_ID,";
            sqlQuery += "   ID.INV_LINE_NUM,";
            sqlQuery += "   ID.RECEIPT_ID,";
            sqlQuery += "  id.ATTRIBUTE1 as MIS_NO,";
            sqlQuery += "  '' as GRN_NUM,";
            sqlQuery += "  ID.INV_ITEM_ID as ITEM_ID,";
            sqlQuery += "  '' as ITEM_CODE,";
            sqlQuery += " '' as ITEM_NAME,";
            sqlQuery += "  to_char(ID.INV_ITEM_QTY_BILLED) as INV_ITEM_QTY_BILLED,";
            sqlQuery += "  0 as unbilled_quantity,";
            sqlQuery += " to_char(ID.INV_ITEM_PRICE) as INV_ITEM_PRICE,";
            sqlQuery += " to_char(ID.INV_ITEM_AMT) as INV_ITEM_AMT,";
            sqlQuery += " ID.INV_ITEM_ACCT_CODE_ID as code_id,";
            sqlQuery += " (SELECT GAC.ACCT_CODE || ' - ' || GAC.ACCT_CODE_DESC AS CODE_NAME";
            sqlQuery += "  FROM GL_ACCT_CODES GAC";
            sqlQuery += " WHERE GAC.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND GAC.ENABLED_FLAG = 1";
            sqlQuery += "  and id.INV_ITEM_ACCT_CODE_ID = GAC.ACCT_CODE_ID) as code_name,";
            sqlQuery += "  INV_SEGMENT_ID1,";
            sqlQuery += " INV_SEGMENT_ID2,";
            sqlQuery += " INV_SEGMENT_ID3,";
            sqlQuery += " INV_SEGMENT_ID4,";
            sqlQuery += " INV_SEGMENT_ID5,";
            sqlQuery += " INV_SEGMENT_ID6";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = INV_SEGMENT_ID1 ) AS SEGMENT_1_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = INV_SEGMENT_ID2 ) AS SEGMENT_2_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = INV_SEGMENT_ID3 ) AS SEGMENT_3_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = INV_SEGMENT_ID4 ) AS SEGMENT_4_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = INV_SEGMENT_ID5 ) AS SEGMENT_5_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = INV_SEGMENT_ID6 ) AS SEGMENT_6_TEXT";
            sqlQuery += " ,ID.INV_LINE_TYPE,";
            sqlQuery += " SCM_LT.DESCRIPTION AS INV_LINE_TYPE_DESC,";
            sqlQuery += " INV_LINE_TAX_AMT,";
            sqlQuery += " '0' AS DELETED";
            sqlQuery += " FROM TMP_INV_INVOICES_DTLS ID, SSM_CODE_MASTERS SCM_LT";
            sqlQuery += " where SCM_LT.CODE = ID.INV_LINE_TYPE";
            sqlQuery += " AND SCM_LT.PARENT_CODE = 'LINETYPE'";
            sqlQuery += " and lower(INV_LINE_TYPE) = 'miscellaneous'";
            sqlQuery += "  and ID.INV_ID = '" + str_INVID + "'";


            return sqlQuery;
        }

        public static string GetAcctCode(string pOHeaderId, string str_ItemId)
        {
            sqlQuery = string.Empty;

            sqlQuery += " select IM.INV_MAT_ACCT_CODE";
            sqlQuery += " from po_headers h,po_lines l,inv_item_master im";
            sqlQuery += " where h.po_header_id=l.po_header_id";
            sqlQuery += " AND L.PO_ITEM_ID = IM.ITEM_ID";
            sqlQuery += " and h.po_header_id='" + pOHeaderId + "'";
            sqlQuery += " and l.PO_ITEM_ID='" + str_ItemId + "'";

            return sqlQuery;
        }
        public static string GetQuantityDetails(string itemId, string vendorId, string invType, string receiptId)
        {
            sqlQuery = string.Empty;

            sqlQuery += "  select (l.PO_QUANTITY) - sum(ind.inv_item_qty_billed) as unbilled_quantity,";
            sqlQuery += " sum(ind.inv_item_qty_billed) as inv_item_qty_billed,";
            sqlQuery += " l.PO_QUANTITY";
            sqlQuery += " from inv_invoices_dtls ind, po_headers h, po_lines l, inv_invoices_hdr ih";
            sqlQuery += "  where ind.inv_id = ih.inv_id";
            sqlQuery += "  and ind.inv_item_id = l.PO_ITEM_ID";
            sqlQuery += "  and h.po_header_id = l.po_header_id";
            sqlQuery += "  and ind.inv_item_id = '" + itemId + "'";
            sqlQuery += "  and ih.vendor_id='" + vendorId + "'";
            sqlQuery += "  and ih.inv_type='" + invType + "'";
            sqlQuery += "   and ind.RECEIPT_ID='" + receiptId + "'";
            sqlQuery += " and h.po_header_id= '" + receiptId + "'";
            sqlQuery += "     and ind.RECEIPT_ID =h.po_header_id";
            sqlQuery += " group by l.PO_QUANTITY";

            return sqlQuery;
        }
        public static string getInvoiceHeaderDetails(string str_INVID)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select * from inv_invoices_hdr i where i.inv_id='" + str_INVID + "'";
            return sqlQuery;
        }

        public static string getInvoiceTaxDetails(string str_Inv_Tax_Dtl_Id, string str_EntityNo, string str_ItemNo)
        {
            sqlQuery = string.Empty;


            sqlQuery += "Select TD.INV_TAX_ID, TD.TAX_ID,'" + str_EntityNo + "' as EntityNo,'" + str_ItemNo + "' as ItemNo,TT.TAX_NAME,TD.INV_TAX_AMT,INV_TAX_PER ";
            sqlQuery += " FROM TAX_TERMS TT INNER JOIN INV_TAX_DTLS TD ON TD.TAX_ID= TT.TAX_ID ";
            sqlQuery += " WHERE TT.ENABLED_FLAG=1 AND TT.WORKFLOW_COMPLETION_STATUS =1 ";
            sqlQuery += " AND TD.INV_LINE_ID='" + str_Inv_Tax_Dtl_Id + "'";
            sqlQuery += " UNION ";
            sqlQuery += " SELECT '0',TT.TAX_ID,'" + str_EntityNo + "' as EntityNo,'" + str_ItemNo + "' as ItemNo,TT.TAX_NAME,NULL as  INV_TAX_AMT ,NULL as INV_TAX_PER FROM TAX_TERMS TT ";
            sqlQuery += " WHERE TT.ENABLED_FLAG=1 AND TT.WORKFLOW_COMPLETION_STATUS =1 ";
            // sqlQuery += " AND TAX_ID NOT IN (SELECT TAX_ID FROM INV_TAX_DTLS WHERE INV_LINE_ID='" + str_Inv_Tax_Dtl_Id + "')";
            sqlQuery += " AND not exists (SELECT  cc.TAX_ID FROM INV_TAX_DTLS cc  WHERE cc.INV_LINE_ID='" + str_Inv_Tax_Dtl_Id + "' and tt.tax_id = cc.tax_id)";

            sqlQuery += " ORDER BY INV_TAX_AMT ";

            return sqlQuery;
        }

        public static string getInvoice4Perpayment()
        {
            sqlQuery = string.Empty;
            sqlQuery += " SELECT IH.INV_NUM,IH.INV_ID FROM INV_INVOICES_HDR IH WHERE IH.INV_TYPE in ('Standard','PO Invoice') ";
            sqlQuery += " AND IH.Enabled_Flag=1 and IH.WORKFLOW_COMPLETION_STATUS=1 ";
            sqlQuery += "    and Ih.ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            return sqlQuery;
        }

        public static string getInvoiceNumber()
        {
            sqlQuery = string.Empty;
            sqlQuery += " SELECT IH.INV_NUM,IH.INV_ID FROM INV_INVOICES_HDR IH ";
            sqlQuery += " WHERE IH.Enabled_Flag=1 and IH.WORKFLOW_COMPLETION_STATUS=1 ";
            sqlQuery += " ORDER BY IH.INV_NUM ASC";
            //sqlQuery += "    and Ih.ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            return sqlQuery;
        }

        public static string getInvPaymentAmt(string supplierID, string fromDate, string toDate)
        {
            sqlQuery = string.Empty;
            sqlQuery += " select iih.vendor_id, iih.inv_date, sum(iih.inv_amt)as invoice_amt, iph.pay_date, nvl(sum(iph.payment_amt),0) as payment_amt, sc.vendor_name ";
            sqlQuery += " from inv_invoices_hdr iih ";
            sqlQuery += " left outer join inv_payments_hdr iph ";
            sqlQuery += " on iih.vendor_id = iph.pay_vendor_id ";
            sqlQuery += " inner join supplier_customers sc ";
            sqlQuery += " on iih.vendor_id = sc.vendor_id ";
            sqlQuery += " where iih.inv_date between to_date('" + fromDate.ToString() + "','dd/MM/yyyy') and to_date('" + toDate.ToString() + "','dd/MM/yyyy')";
            sqlQuery += " and iih.vendor_id = '" + supplierID + "'";
            sqlQuery += "    and Iih.inv_org_id='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " group by iih.vendor_id, iih.inv_date, iih.inv_amt, iph.pay_date, iph.payment_amt, sc.vendor_name ";

            return sqlQuery;
        }


        public static string getInvoiceReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery = "SELECT * FROM INVOICE V WHERE ROWNUM > 0 ";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["VENDOR_NAME"] != null)
                {
                    sqlQuery += " AND V.VENDOR_NAME = '" + VMVServices.Web.Utils.ReportViewFilterParameter["VENDOR_NAME"].ToString() + "'";
                }
            }

            return sqlQuery;
        }
        public static string getCustomerName()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT SC.VENDOR_NAME,SC.VENDOR_ID FROM SUPPLIER_CUSTOMERS SC ";
            sqlQuery += "    where SC.ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            return sqlQuery;
        }

        public static string getInvoiceNumber4Supplier(string str_supplier)
        {
            sqlQuery = string.Empty;
            sqlQuery += "select INV_ID,INV_NUM  ";
            sqlQuery += "  FROM INV_INVOICES_HDR IH ";
            sqlQuery += "  WHERE IH.ENABLED_FLAG=1 AND IH.WORKFLOW_COMPLETION_STATUS=1 AND IH.VENDOR_LOC_ID='" + str_supplier + "'";
            sqlQuery += " AND INV_TYPE IN ('Standard','PO_Invoice')";
            return sqlQuery;
        }

        public static string getInvoiceNumberForSupplier(string str_supplier)
        {
            sqlQuery = string.Empty;
            sqlQuery += "select INV_ID,INV_NUM  ";
            sqlQuery += "  FROM INV_INVOICES_HDR IH ";
            sqlQuery += "  WHERE IH.ENABLED_FLAG=1 AND IH.WORKFLOW_COMPLETION_STATUS=1 AND IH.VENDOR_ID='" + str_supplier + "'";
            return sqlQuery;
        }


        public static string getInvoice4Payemnt(string str_supp_id, string str_mode, string currId = "")
        {
            sqlQuery = string.Empty;
            if (str_mode == FINTableConstant.Add)
            {
                sqlQuery += " select INV_ID,INV_NUM  from ( ";
                sqlQuery += " select nvl(iih.inv_amt,0) - ";
                sqlQuery += " (sum(nvl(ipd.pay_invoice_paid_amt,0)) + sum(nvl(ipd.PAY_RETENTIION_AMT,0)))  ";
                sqlQuery += " bal_amt,iih.inv_id,iih.inv_num ";
                sqlQuery += " from inv_invoices_hdr iih,inv_payments_hdr iph,inv_payments_dtl ipd ";
                sqlQuery += " where iih.vendor_id=iph.pay_vendor_id ";
                sqlQuery += " and iph.pay_id=ipd.pay_id ";
                sqlQuery += " and iih.inv_id=ipd.pay_invoice_id ";
                sqlQuery += " and iph.REV_FLAG is null ";
                sqlQuery += "  and iih.vendor_id='" + str_supp_id + "'";
                if (currId.Length > 0)
                {
                    sqlQuery += "  and iih.inv_pay_curr_code='" + currId + "'";
                }
                sqlQuery += " group by iih.inv_amt,iih.inv_id,iih.inv_num,iih.inv_retention_amt) inv_bal ";

                sqlQuery += " where inv_bal.bal_amt > 0 ";
                sqlQuery += "  UNION ";
                sqlQuery += " select  uih.INV_ID,uih.INV_NUM ";
                sqlQuery += " FROM inv_invoices_hdr uih where  not exists (select pay_invoice_id from inv_payments_dtl pd,inv_payments_hdr PH where PH.pay_id=pd.pay_id and ph.rev_flag is null  and pd.pay_invoice_id= uih.inv_id ) ";
                sqlQuery += " AND vendor_id ='" + str_supp_id + "'";
                sqlQuery += " AND POSTED_FLAG='1' AND REV_FLAG IS NULL ";
                if (currId.Length > 0)
                {
                    sqlQuery += "  and inv_pay_curr_code='" + currId + "'";
                }

            }
            else if (str_mode != FINTableConstant.Add)
            {
                sqlQuery += " select iih.inv_id,iih.inv_num from inv_invoices_hdr iih where iih.vendor_id='" + str_supp_id + "'";
            }

            return sqlQuery;
        }



        public static string GetLiab_AdvAccount(string vendor_id)
        {
            sqlQuery = string.Empty;

            sqlQuery += " select s.AP_LIABILITY_ACCOUNT,s.AP_ADVANCE_ACCOUNT,s.VENDOR_RETENTION_ACCT_ID";
            sqlQuery += "  from supplier_customers s";
            sqlQuery += "  where s.workflow_completion_status = 1";
            sqlQuery += "  and s.vendor_id = '" + vendor_id + "'";

            return sqlQuery;
        }

        public static string GetAcct_frmSysopt()
        {
            sqlQuery = string.Empty;

            sqlQuery += " select SS.AP_LIABILITY_ACCT,SS.AP_ADVANCE_ACCT,SS.AP_RETENTION_ACCT";
            sqlQuery += " from ssm_system_options ss";
            sqlQuery += " WHERE ss.workflow_completion_status =1";
            sqlQuery += " and ss.module_code = 'AP'";
            return sqlQuery;
        }
        public static bool IsPaymentRecordFound(string invId)
        {
            sqlQuery = string.Empty;
            bool payCount = false;
            int counts = 0;
            sqlQuery += "  select count(1) as counts from inv_payments_dtl i,inv_payments_hdr p";
            sqlQuery += " where i.pay_id=p.pay_id and p.rev_flag is null and p.rev_date is null and  i.pay_invoice_id ='" + invId + "'";
            counts = DBMethod.GetIntValue(sqlQuery);
            if (counts > 0)
            {
                payCount = true;
            }
            else
            {
                payCount = false;
            }
            return payCount;
        }

        public static string getSierraSupplierInvoice()
        {
            sqlQuery = string.Empty;

            sqlQuery = " select distinct v.Supplier_Name,v.Supplier_ID || ' - ' || v.Supplier_Name as Suplr_Name, sc.description";
            sqlQuery += " ,(case t.POSTED when '1' then 'TRUE' ELSE 'FALSE' END) AS POSTED";
            sqlQuery += " ,t.Invoice_ID, t.Invoice_Number, t.Invoice_Date, t.Supplier_ID, to_char(t.Invoice_Amount) as Invoice_Amount";
            sqlQuery += " ,(select ie.error_message from gl_intf_errors ie where ie.error_in = t.INVOICE_ID and rownum=1) as error_message ";
            sqlQuery += " from DM_SR_SUPPLIER_INVOICE@CRCINTERFACE t, DM_SR_SUPPLIER@CRCINTERFACE v, ssm_code_masters sc";
            sqlQuery += " where v.Supplier_ID = t.Supplier_ID";
            sqlQuery += " and (t.POSTED is null or t.POSTED = '0')";
            sqlQuery += " and sc.code='Miscellaneous' and sc.parent_code='LINETYPE'";
            sqlQuery += " order by t.Invoice_Number asc";

            return sqlQuery;
        }
    }
}
