﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.AP
{
    public class RFQ_DAL
    {
        static string sqlQuery = "";

        public static string GetRFQDtls(string rfqId)
        {
            sqlQuery = string.Empty;

            sqlQuery = "    select pd.rfq_dtl_id,";
            sqlQuery += "     pd.attribute1 as LINE_NUM,";
            sqlQuery += "      '0' AS DELETED,";
            sqlQuery += "      pd.RFQ_ITEM_ID as tmp_rfq_item_id,";
            sqlQuery += "     pd.RFQ_ITEM_QTY as TMP_RFQ_ITEM_QTY,to_char(pd.rfq_amount) as rfq_amount,to_char(pd.RFQ_ITEM_PRICE) as RFQ_ITEM_PRICE,";
            sqlQuery += "     (select im.INV_ITEM_TYPE";
            sqlQuery += "     From inv_item_master im";
            sqlQuery += "    where pd.RFQ_ITEM_ID = im.item_id) as INV_ITEM_TYPE,";
            sqlQuery += "      (select im.item_name" + VMVServices.Web.Utils.LanguageCode;
            sqlQuery += "     From inv_item_master im";
            sqlQuery += "    where pd.RFQ_ITEM_ID = im.item_id) as item_name,";
            sqlQuery += "     (select um.uom_id";
            sqlQuery += "     From inv_uom_master um";
            sqlQuery += "    where pd.RFQ_ITEM_UOM = um.uom_id) as uom_id,";
            sqlQuery += "     (select um.uom_code" + VMVServices.Web.Utils.LanguageCode + " ||' - '||um.uom_desc" + VMVServices.Web.Utils.LanguageCode + " ";
            sqlQuery += "     From inv_uom_master um";
            sqlQuery += "    where pd.RFQ_ITEM_UOM = um.uom_id) as uom_name,";
            sqlQuery += "     pd.RFQ_ITEM_UOM as tmp_rfq_item_uom,";
            sqlQuery += "     pd.RFQ_ITEM_PRICE as tmp_rfq_item_cost";
            sqlQuery += "    from po_rfq_hdr         ph,";
            sqlQuery += "     po_rfq_dtl         pd,";
            sqlQuery += "     po_tmp_rfq_hdr     h";
            //sqlQuery += "    po_tmp_rfq_dtl     l,";
            //sqlQuery += "     po_requisition_hdr rh,";
            //sqlQuery += "    po_tmp_rfq_req_mapping rm";
            sqlQuery += "   where ph.rfq_id = pd.rfq_id";
            sqlQuery += "    and ph.rfq_tmp_rfq_id = h.tmp_rfq_id";
            //sqlQuery += "    and h.tmp_rfq_id = l.tmp_rfq_id";
            //sqlQuery += "     and rm.tmp_rfq_id=h.tmp_rfq_id";
            //sqlQuery += "    and rh.po_req_id = rm.tmp_req_id";
            sqlQuery += "    and h.enabled_flag = '1'";
            //sqlQuery += "     and rh.enabled_flag = '1'";
            sqlQuery += "     and ph.enabled_flag = '1'";
            sqlQuery += "     and  ph.rfq_id='" + rfqId + "'";
            sqlQuery += " order by RFQ_DTL_ID ";

            return sqlQuery;
        }
        public static string GetRFQAdditionalData(string rfQId)
        {
            sqlQuery = string.Empty;
            sqlQuery = "   select ad.rfq_add_id,ad.rfq_id,ad.rfq_add_type,ad.rfq_add_desc,'N' as deleted,ad.attribute1 as LINE_NUM";
            sqlQuery += "  from po_rfq_additional_dtls ad";
            sqlQuery += "  where ad.enabled_flag='1'";
            sqlQuery += "  and ad.rfq_id='" + rfQId + "'";
            sqlQuery += "  order by ad.RFQ_ADD_ID asc";
            return sqlQuery;
        }
        public static string getQution4Supplier(string supplierId)
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT PRH.RFQ_ID,PRH.RFQ_NUMBER FROM PO_RFQ_HDR PRH ";
            sqlQuery += "  WHERE PRH.ENABLED_FLAG=1 AND PRH.WORKFLOW_COMPLETION_STATUS=1 AND PRH.RFQ_SUPPLIER ='" + supplierId + "'";
            sqlQuery += "  and upper(prh.rfq_status)='APPROVED'";
            return sqlQuery;
        }
        public static string GetRFQQuotationNo(string supplierId)
        {
            sqlQuery = string.Empty;
            sqlQuery = "   select ph.quote_number,ph.rfq_id from po_rfq_hdr  ph";
            sqlQuery += "  where PH.ENABLED_FLAG=1 AND PH.WORKFLOW_COMPLETION_STATUS=1 AND lower(ph.rfq_status)='approved'";
            sqlQuery += "  and ph.rfq_supplier ='" + supplierId + "'";
            return sqlQuery;
        }
        public static string getQuotationNumber(string supplierId = "")
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT PRH.RFQ_ID,PRH.RFQ_NUMBER FROM PO_RFQ_HDR PRH ";
            sqlQuery += "  WHERE PRH.ENABLED_FLAG=1 AND PRH.WORKFLOW_COMPLETION_STATUS=1";
            sqlQuery += "  and upper(prh.rfq_status)='APPROVED'";

            if (supplierId != string.Empty)
            {
                sqlQuery += "  and  PRH.RFQ_SUPPLIER='" + supplierId + "'";
            }
            return sqlQuery;
        }
        public static string getQuotationReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM vw_quotation_comparision V WHERE ROWNUM > 0 ";
            sqlQuery += " AND v.ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["rfq_id"] != null)
                {
                    sqlQuery += " AND V.tmp_rfq_id = '" + VMVServices.Web.Utils.ReportFilterParameter["rfq_id"].ToString() + "'";
                }
            }
            return sqlQuery;
        }
        public static string getDtlsBasedOnQuoteNo(string reqQuoteNo)
        {
            sqlQuery = string.Empty;

            //sqlQuery += " select prh.rfq_id, prh.rfq_supplier, prh.rfq_number, prh.rfq_dt, prh.rfq_due_dt,prh.rfq_remarks,prh.rfq_currency,prh.rfq_status,prh.rfq_tmp_rfq_id,prh.enabled_flag,";
            //sqlQuery += " prd.rfq_dtl_id,prd.rfq_item_id, prd.rfq_item_uom,prd.rfq_item_qty,to_char(prd.rfq_amount) as rfq_amount";
            //sqlQuery += " from po_rfq_hdr prh, po_rfq_dtl prd";
            //sqlQuery += " where prh.rfq_id=prd.rfq_id";
            //sqlQuery += " and prh.rfq_id='" + reqQuoteNo + "'";

            sqlQuery += " select prh.rfq_id, prh.rfq_supplier, prh.rfq_number, prh.rfq_dt, prh.rfq_due_dt,prh.rfq_remarks,prh.rfq_currency,prh.rfq_status,prh.rfq_tmp_rfq_id,prh.enabled_flag,";
            sqlQuery += " prd.rfq_dtl_id,prd.rfq_item_id as tmp_rfq_item_id, ";
            //--prd.rfq_item_uom as tmp_rfq_item_uom,
            sqlQuery += " '0' as  rfq_amount,";
            sqlQuery += " '0' as RFQ_ITEM_PRICE,";
            //sqlQuery += " '0' as to_char(prd.rfq_amount) as rfq_amount,";
            sqlQuery += " prd.attribute1 as LINE_NUM,'0' AS DELETED,";
            sqlQuery += " (select im.INV_ITEM_TYPE";
            sqlQuery += " From inv_item_master im";
            sqlQuery += " where prd.RFQ_ITEM_ID = im.item_id) as INV_ITEM_TYPE,";
            sqlQuery += " (select im.item_name";
            sqlQuery += " From inv_item_master im";
            sqlQuery += " where prd.RFQ_ITEM_ID = im.item_id) as item_name,";
            sqlQuery += " (select um.uom_id";
            sqlQuery += " From inv_uom_master um";
            sqlQuery += " where prd.RFQ_ITEM_UOM = um.uom_id) as uom_id,";
            sqlQuery += " (select um.uom_code || ' - ' || um.uom_desc";
            sqlQuery += " From inv_uom_master um";
            sqlQuery += " where prd.RFQ_ITEM_UOM = um.uom_id) as uom_name,";
            sqlQuery += " prd.RFQ_ITEM_UOM as tmp_rfq_item_uom,";
            sqlQuery += " prd.rfq_item_qty as TMP_RFQ_ITEM_QTY,";
            sqlQuery += " prd.RFQ_ITEM_PRICE as tmp_rfq_item_cost";
            sqlQuery += " from po_rfq_hdr prh, po_rfq_dtl prd";
            sqlQuery += " where prh.rfq_id=prd.rfq_id";
            sqlQuery += " and prh.rfq_id='" + reqQuoteNo + "'";

            return sqlQuery;
        }

        public static string getAddDtlsBasedOnQuoteNo(string reqQuoteNo)
        {
            sqlQuery = string.Empty;

            sqlQuery += " select prh.rfq_supplier, prh.rfq_number, prh.rfq_dt, prh.rfq_due_dt,prh.rfq_remarks,prh.rfq_currency,prh.rfq_status,prh.rfq_tmp_rfq_id,prh.enabled_flag,";
            sqlQuery += " prad.rfq_add_id, prad.rfq_id, prad.rfq_add_type, prad.rfq_add_desc, 'N' as deleted, prad.attribute1 as LINE_NUM";
            sqlQuery += " from po_rfq_hdr prh, po_rfq_additional_dtls prad";
            sqlQuery += " where prh.rfq_id=prad.rfq_id";
            sqlQuery += " and prh.rfq_id='" + reqQuoteNo + "'";

            return sqlQuery;
        }
    }
}
