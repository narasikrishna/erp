﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace FIN.DAL
{
    public class InvoiceBatch_DAL
    {
        static string sqlQuery = "";

        public static string getInvoiceBatchDetails()
        {
            sqlQuery = string.Empty;
            sqlQuery = "SELECT j.batch_id,j.batch_desc,j.batch_date";
            sqlQuery += " from inv_batches j";
            sqlQuery += " where j.workflow_completion_status = '1'";
            sqlQuery += "  and j.enabled_flag = '1'";
            sqlQuery += "   and j.batch_org_id='" + VMVServices.Web.Utils.OrganizationID  + "'";
            sqlQuery += "  order by j.batch_id";

        
            return sqlQuery;
        }
    }
}
