﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.AP
{
    public class PurchaseOrder_DAL
    {

        static string sqlQuery = "";
        public static string GetPODetails(string pOHeaderId)
        {
            sqlQuery = string.Empty;
            sqlQuery = " select h.po_header_id,l.*,'0' AS DELETED,(to_char(nvl(l.PO_QUANTITY, 0) * nvl(l.po_unit_price, 0)))  as PO_line_Amount,";
            // sqlQuery += " case when l.ATTRIBUTE1=0 then (to_char(nvl(l.PO_QUANTITY, 0) * nvl(l.po_unit_price, 0))) else to_char(l.ATTRIBUTE1) end  as PO_line_Amount,";
            sqlQuery += " l.po_line_num as PO_REQ_LINE_NUM,l.po_item_id as ITEM_ID,l.po_description,to_char(l.PO_QUANTITY) as PO_QUANTITY,";
            sqlQuery += "   (select im.item_name From inv_item_master im where l.po_item_id=im.item_id) as item_name,";
            sqlQuery += "   (select um.uom_code From inv_uom_master um where l.po_uom=um.uom_id) as uom_code,";
            sqlQuery += " l.po_uom as PO_REQ_UOM,to_char(l.po_unit_price) as PO_ITEM_UNIT_PRICE";
            sqlQuery += " from po_headers h,po_lines l";
            sqlQuery += " where h.po_header_id=l.po_header_id";
            sqlQuery += " and h.po_header_id='" + pOHeaderId + "'";
            sqlQuery += " order by PO_LINE_ID ";


            return sqlQuery;
        }
        public static string GetItemBasedVendor(string vendorId = "")
        {
            sqlQuery = string.Empty;
            sqlQuery += "  select distinct im.item_id,im.item_name from inv_receipt_dtls ir,inv_item_master im,inv_receipts_hdr rh";
            sqlQuery += "  where ir.item_id=im.item_id";
            sqlQuery += "  and rh.receipt_id=ir.receipt_id";
            if (vendorId.Length > 0)
            {
                sqlQuery += "    and rh.VENDOR_ID='" + vendorId + "'";
            }
            sqlQuery += "     order by im.item_name asc";
            return sqlQuery;
        }
        public static string GetPOScope(string PO_LINE_ID)
        {
            sqlQuery = string.Empty;

            sqlQuery = "    select ps.po_scope_id,ps.po_line_id,ps.po_scope_desc,'N' as deleted from PO_WORK_SCOPE ps";
            sqlQuery += "  where ps.PO_LINE_ID='" + PO_LINE_ID + "'";

            return sqlQuery;
        }
        public static string GetIssuedQuantity()
        {
            sqlQuery = string.Empty;

            sqlQuery += "   select im.item_name,count(1) as issued_quantity from inv_material_issue_dtl md,inv_item_master im";
            sqlQuery += "  where md.item_id=im.item_id";
            sqlQuery += "   and im.org_id='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += "  group by im.item_name";
            return sqlQuery;
        }
        public static string GetGRNDataChart(string vendorId = "", string itemId = "")
        {
            sqlQuery = string.Empty;
            sqlQuery += "   select sum(rd.qty_received) as qty_received,im.item_name,";
            sqlQuery += "     sum(rd.qty_approved) as qty_approved,";
            sqlQuery += "    sum(rd.qty_rejected) as qty_rejected";
            sqlQuery += "   from inv_receipt_dtls   rd,";
            sqlQuery += "    inv_receipts_hdr   rh,";
            sqlQuery += "   supplier_customers sc,";
            sqlQuery += "   inv_item_master    im";
            sqlQuery += "   where rd.receipt_id = rd.receipt_id";
            sqlQuery += "   and im.org_id='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += "    and sc.vendor_id = rh.vendor_id";
            if (itemId.Length > 0)
            {
                sqlQuery += "    and im.item_id ='" + itemId + "'";
            }
            if (vendorId.Length > 0)
            {
                sqlQuery += "    and sc.vendor_id ='" + vendorId + "'";
            }
            sqlQuery += "    and im.item_id = rd.item_id  group by rd.item_id, im.item_name";
            return sqlQuery;
        }
        public static string GetAvgInvoicePayment(string vendorId = "", string Fromdate = "", string Todate = "")
        {
            sqlQuery = string.Empty;
            sqlQuery += "  SELECT count(*) as Tot_Inv,  to_char(ih.inv_date, 'Mon') as Year_Month ";
            sqlQuery += "    FROM INV_INVOICES_HDR ih WHERE ROWNUM > 0 ";


            if (Fromdate.Length > 0)
            {
                sqlQuery += "   and ih.inv_date >=to_date('" + Fromdate + "','dd/MM/yyyy')";
            }
            if (Todate.Length > 0)
            {
                sqlQuery += "  and  ih.inv_date <=to_date('" + Todate + "','dd/MM/yyyy')";
            }
            if (vendorId.Length > 0)
            {
                sqlQuery += "    and ih.vendor_id ='" + vendorId + "'";
            }
            sqlQuery += "  GROUP BY to_char(ih.inv_date, 'Mon') ,to_char(ih.inv_date, 'MM') ";
            sqlQuery += " order by  to_char(ih.inv_date, 'MM') ";
            return sqlQuery;
        }




        public static string GetSupplyWisePoAwaitedChart()
        {
            sqlQuery = string.Empty;

            sqlQuery += "  select po.po_vendor_id,sc.vendor_name,count(1) as counts from po_headers po,supplier_customers sc";
            sqlQuery += "  where po.po_header_id not in (select ir.po_header_id from inv_receipt_dtls ir where ir.po_header_id is not null)";
            sqlQuery += "  and sc.vendor_id=po.po_vendor_id";
            sqlQuery += "   and po.org_id='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " group by po.po_vendor_id,sc.vendor_name";
            return sqlQuery;
        }
        public static string GetPODetails4ItemId(string pOHeaderId, string str_ItemId)
        {
            sqlQuery = string.Empty;
            sqlQuery = " select h.po_header_id,l.*,to_char((nvl(l.PO_QUANTITY,0) * nvl(l.po_unit_price,0))) as PO_line_Amount,'0' AS DELETED, ";
            sqlQuery += " l.po_line_num as PO_REQ_LINE_NUM,l.po_item_id as ITEM_ID,l.po_description, to_char( rw.wh_qty) as wh_qty, ";
            //to_char(l.PO_QUANTITY) as PO_QUANTITY,";
            sqlQuery += "   (select im.item_name From inv_item_master im where l.po_item_id=im.item_id) as item_name,";
            sqlQuery += "   (select um.uom_code From inv_uom_master um where l.po_uom=um.uom_id) as uom_code,";
            sqlQuery += " l.po_uom as PO_REQ_UOM,to_char(l.po_unit_price) as PO_ITEM_UNIT_PRICE";
            sqlQuery += " from po_headers h,po_lines l,iNV_RECEIPT_ITEM_WH_HDR rw,INV_RECEIPT_DTLS rd";
            sqlQuery += " where h.po_header_id=l.po_header_id";
            sqlQuery += " and rd.receipt_dtl_id = rw.receipt_dtl_id";
            sqlQuery += " and rd.po_header_id = h.po_header_id";
            sqlQuery += " and rd.po_line_id =l.po_line_id";
            sqlQuery += " and h.po_header_id='" + pOHeaderId + "'";
            sqlQuery += " and l.PO_ITEM_ID='" + str_ItemId + "'";

            return sqlQuery;
        }

        public static string GetPODetailsBasedReq(string pOReqId)
        {
            sqlQuery = string.Empty;
            sqlQuery = "  select h.po_header_id,l.*,0 as PO_line_Amount,'0' AS DELETED, ";
            sqlQuery += "  l.po_line_num as PO_REQ_LINE_NUM,l.po_item_id as ITEM_ID,l.po_description,l.PO_QUANTITY,";
            sqlQuery += "   (select im.item_name" + VMVServices.Web.Utils.LanguageCode + "  From inv_item_master im where l.po_item_id=im.item_id) as item_name,";
            sqlQuery += "   (select um.uom_code" + VMVServices.Web.Utils.LanguageCode + "  From inv_uom_master um where l.po_uom=um.uom_id) as uom_code,";
            sqlQuery += "  l.po_uom as PO_REQ_UOM,l.po_unit_price as PO_ITEM_UNIT_PRICE";
            sqlQuery += "  from po_headers h,po_lines l";
            sqlQuery += "  where h.po_header_id=l.po_header_id";
            sqlQuery += "  and h.po_req_id='" + pOReqId + "'";

            return sqlQuery;
        }


        public static string GetPODetails_BasedonReq(string pOReqId)
        {
            sqlQuery = string.Empty;
            sqlQuery = "  select 0 as PO_REQ_LINE_NUM, 0 as po_header_id,0 as PO_LINE_ID,(select im.item_name" + VMVServices.Web.Utils.LanguageCode + "  From inv_item_master im where hd.item_id = im.item_id) as item_name,hd.item_id,hd.po_description,hd.po_quantity, ";
            sqlQuery += "  (select um.uom_code" + VMVServices.Web.Utils.LanguageCode + "  From inv_uom_master um where hd.po_req_uom = um.uom_id) as uom_code,hd.po_req_uom, to_char(hd.po_item_unit_price) as po_item_unit_price,";
            sqlQuery += "  to_char(nvl(hd.po_quantity,0) * nvl(hd.po_item_unit_price,0)) as PO_line_Amount,'0' AS DELETED";
            sqlQuery += "   from po_requisition_hdr h ,po_requisition_dtl hd";
            sqlQuery += "  where h.po_req_id = hd.po_req_id ";
            sqlQuery += "  and h.po_req_id = '" + pOReqId + "'";

            return sqlQuery;
        }
        public static string GetPODetails_BasedonQuote(string rfqId)
        {
            sqlQuery = string.Empty;

            sqlQuery = "  select 0 as PO_REQ_LINE_NUM,";
            sqlQuery += "    0 as po_header_id,";
            sqlQuery += "    0 as PO_LINE_ID,";
            sqlQuery += "    (select im.item_name";
            sqlQuery += "    From inv_item_master im";
            sqlQuery += "    where hd.rfq_item_id = im.item_id) as item_name,";
            sqlQuery += "    hd.rfq_item_id as item_id,";
            sqlQuery += "    '' as po_description,";
            sqlQuery += "    to_char(hd.rfq_item_qty) as po_quantity,";
            sqlQuery += "    (select um.uom_code";
            sqlQuery += "      From inv_uom_master um";
            sqlQuery += "     where hd.rfq_item_uom = um.uom_id) as uom_code,";
            sqlQuery += "   hd.rfq_item_uom as po_req_uom,";
            sqlQuery += "   to_char(hd.rfq_item_price) as po_item_unit_price,";
            ///sqlQuery += "   to_char(nvl(hd.rfq_item_qty, 0) * nvl(hd.rfq_item_price, 0)) as PO_line_Amount,";
            sqlQuery += "   case when hd.RFQ_AMOUNT=0  then (to_char(nvl(hd.rfq_item_qty, 0) * nvl(hd.rfq_item_price, 0))) else to_char(hd.rfq_amount) end as PO_line_Amount,";
            sqlQuery += "   '0' AS DELETED";
            sqlQuery += "   from PO_RFQ_HDR h, PO_RFQ_DTL hd";
            sqlQuery += "   where h.rfq_id=hd.rfq_id";
            sqlQuery += "        and h.RFQ_ID='" + rfqId + "'";

            return sqlQuery;
        }
        public static string GetPODetails()
        {
            sqlQuery = string.Empty;
            sqlQuery = "  select h.po_header_id,l.PO_LINE_ID,";
            sqlQuery += "   l.po_line_num,";
            sqlQuery += "   l.po_item_id,";
            sqlQuery += "   (select im.item_name" + VMVServices.Web.Utils.LanguageCode + " ";
            sqlQuery += "      From inv_item_master im";
            sqlQuery += "     where l.po_item_id = im.item_id) as item_name,";
            sqlQuery += "     (h.PO_NUM || ' - ' || l.po_line_num || ' - ' ||";
            sqlQuery += "    (select im.item_name" + VMVServices.Web.Utils.LanguageCode + " ";
            sqlQuery += "     From inv_item_master im";
            sqlQuery += "    where l.po_item_id = im.item_id) || ' - ' || l.po_quantity) as po_data";
            sqlQuery += "   from po_headers h, po_lines l";
            sqlQuery += "  where h.po_header_id = l.po_header_id";
            sqlQuery += "   and h.workflow_completion_status=1";
            sqlQuery += "  and h.enabled_flag=1";
            sqlQuery += "   order by h.po_header_id asc";

            return sqlQuery;
        }
        public static string GetPODetailsBasedSupplier(string vendorId)
        {
            sqlQuery = string.Empty;
            //sqlQuery = "  select h.po_header_id,l.PO_LINE_ID,";
            //sqlQuery += "   l.po_line_num,";
            //sqlQuery += "   l.po_item_id,";
            //sqlQuery += "   (select im.item_name" + VMVServices.Web.Utils.LanguageCode + " ";
            //sqlQuery += "      From inv_item_master im";
            //sqlQuery += "     where l.po_item_id = im.item_id) as item_name,";
            //sqlQuery += "     (h.PO_NUM || ' - ' || l.po_line_num || ' - ' ||";
            //sqlQuery += "    (select im.item_name" + VMVServices.Web.Utils.LanguageCode + " ";
            //sqlQuery += "     From inv_item_master im";
            //sqlQuery += "    where l.po_item_id = im.item_id) || ' - ' || l.po_quantity) as po_data";
            //sqlQuery += "   from po_headers h, po_lines l";
            //sqlQuery += "  where h.po_header_id = l.po_header_id";
            //sqlQuery += "  and h.PO_VENDOR_ID='" + vendorId + "'";
            //sqlQuery += "   and h.workflow_completion_status=1";
            //sqlQuery += "  and h.enabled_flag=1";
            //sqlQuery += "   order by h.po_header_id asc";

            sqlQuery += " select im.inv_item_type,h.po_header_id,l.PO_LINE_ID,";
            sqlQuery += " l.po_line_num,l.po_item_id, im.item_name  as item_name,";
            sqlQuery += " (h.PO_NUM || ' - ' || l.po_line_num || ' - ' ||im.item_name";
            sqlQuery += " || ' - ' || l.po_quantity) as po_data";
            sqlQuery += " from po_headers h, po_lines l,inv_item_master im";
            sqlQuery += " where h.po_header_id = l.po_header_id";
            sqlQuery += " and h.PO_VENDOR_ID='" + vendorId + "'";
            sqlQuery += " and l.po_item_id = im.item_id";
            sqlQuery += " and upper(im.inv_item_type) = upper('Item')";
            sqlQuery += " and h.workflow_completion_status=1";
            sqlQuery += " and h.enabled_flag=1";
            sqlQuery += " order by h.PO_NUM,l.po_line_num asc";



            return sqlQuery;
        }
        public static string GetPOData(string pOHeaderId)
        {
            sqlQuery = string.Empty;
            sqlQuery = "  select h.po_header_id,";
            sqlQuery += "   l.po_line_num,l.PO_LINE_ID,";
            sqlQuery += "   l.po_item_id,l.po_quantity,";
            sqlQuery += "   (select im.item_name" + VMVServices.Web.Utils.LanguageCode + " ";
            sqlQuery += "      From inv_item_master im";
            sqlQuery += "     where l.po_item_id = im.item_id) as item_name,";
            sqlQuery += "     (h.PO_NUM || ' - ' || l.po_line_num || ' - ' ||";
            sqlQuery += "    (select im.item_name" + VMVServices.Web.Utils.LanguageCode + " ";
            sqlQuery += "     From inv_item_master im";
            sqlQuery += "    where l.po_item_id = im.item_id) || ' - ' || l.po_quantity) as po_data";
            sqlQuery += "   from po_headers h, po_lines l";
            sqlQuery += "  where h.po_header_id = l.po_header_id";
            sqlQuery += "   and h.workflow_completion_status=1";
            sqlQuery += "  and l.PO_LINE_ID='" + pOHeaderId + "'";
            sqlQuery += "  and h.enabled_flag=1";
            sqlQuery += "   order by h.po_header_id asc";
            return sqlQuery;
        }

        public static string GetReceivedQuantity(string pOHeaderId, string poLineId, string ItemId)
        {
            sqlQuery = string.Empty;

            sqlQuery = "    select nvl(sum(i.qty_approved),0)  as qty_received";
            sqlQuery += "   from inv_receipt_dtls i";
            sqlQuery += "  where i.po_header_id = '" + pOHeaderId + "'";
            sqlQuery += "   and i.po_line_id = '" + poLineId + "'";
            sqlQuery += "   and i.item_id = '" + ItemId + "'";

            return sqlQuery;
        }
        public static string GetItemUnitprice(string ITEM_ID)
        {
            sqlQuery = string.Empty;


            sqlQuery = "  SELECT to_char(IM.ITEM_UNIT_PRICE) as ITEM_UNIT_PRICE";
            sqlQuery += " FROM INV_ITEM_MASTER IM";
            sqlQuery += " WHERE IM.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND IM.ITEM_ID = '" + ITEM_ID + "'";
            return sqlQuery;
        }
        public static string GetItemUnitBasedSupplierprice(string ITEM_ID)
        {

            sqlQuery = string.Empty;

            sqlQuery = "  select p.price_item_cost as PO_ITEM_UNIT_PRICE from inv_price_list_dtl p where p.WORKFLOW_COMPLETION_STATUS = 1 and p.enabled_flag=1 and p.price_item_id= '" + ITEM_ID + "'";

            return sqlQuery;
        }

        public static string GetRequsitionDesc(string REQUESTION_NUM)
        {
            sqlQuery = string.Empty;


            sqlQuery = "  select PRH.PO_REQ_DESC ";
            sqlQuery += " from PO_REQUISITION_HDR PRH ";
            sqlQuery += " WHERE PRH.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " and PRH.PO_REQ_ID = '" + REQUESTION_NUM + "'";
            return sqlQuery;
        }
        public static string GetItemUOM(string ITEM_ID)
        {
            sqlQuery = string.Empty;


            sqlQuery = "  select IM.ITEM_UOM";
            sqlQuery += " FROM INV_ITEM_MASTER IM";
            sqlQuery += " WHERE IM.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND IM.ITEM_ID = '" + ITEM_ID + "'";
            return sqlQuery;
        }
        public static string getReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM INV_PURCHASE_ORDER_R V WHERE ROWNUM > 0 ";

            if (VMVServices.Web.Utils.ReportFilterParameter != null || VMVServices.Web.Utils.ReportFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["PO_NUMBER"] != null)
                {
                    sqlQuery += " AND V.PO_NUMBER = '" + VMVServices.Web.Utils.ReportFilterParameter["PO_NUMBER"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.VENDOR_ID] != null)
                {
                    sqlQuery += " AND V.VENDOR_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.VENDOR_ID].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Amt"] != null)
                {
                    sqlQuery += " AND V.PO_AMOUNT >= '" + VMVServices.Web.Utils.ReportFilterParameter["From_Amt"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Amt"] != null)
                {
                    sqlQuery += " AND V.PO_AMOUNT <= '" + VMVServices.Web.Utils.ReportViewFilterParameter["To_Amt"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
                {
                    sqlQuery += " AND V.PO_DATE between to_date('" + VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy') ";
                    
                }
                if(VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
                {
                    sqlQuery += " AND to_date('" + VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"].ToString() + "','dd/MM/yyyy') ";
                }
            }

            return sqlQuery;
        }
        public static string getPONumberReportData(string poNum = "")
        {
            sqlQuery = string.Empty;
            sqlQuery = "SELECT * FROM INV_PURCHASE_ORDER_NUMBER_R V WHERE ROWNUM > 0 ";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                //if (VMVServices.Web.Utils.ReportViewFilterParameter["PO_NUM"] != null)
                //{
                //    sqlQuery += " AND V.PO_NUM = '" + VMVServices.Web.Utils.ReportViewFilterParameter["PO_NUM"].ToString() + "'";
                //}
                if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.VENDOR_ID] != null)
                {
                    sqlQuery += " AND V.VENDOR_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.VENDOR_ID].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
                {
                    sqlQuery += " AND V.PO_DATE >=  to_date('" + VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"].ToString() + "','dd/mm/yyyy')";

                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
                {
                    sqlQuery += " AND V.PO_DATE <= to_date('" + VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"].ToString() + "','dd/mm/yyyy')";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Amt"] != null && VMVServices.Web.Utils.ReportViewFilterParameter["To_Amt"] != null)
                {
                    sqlQuery += " AND V.PO_AMOUNT >=to_number(trunc(" + VMVServices.Web.Utils.ReportViewFilterParameter["From_Amt"] + "))";
                    sqlQuery += " AND V.PO_AMOUNT <=to_number(trunc(" + VMVServices.Web.Utils.ReportViewFilterParameter["To_Amt"] + "))";
                }
                if (poNum.Length > 0)
                {
                    sqlQuery += " AND V.PO_NUM in (" + poNum + ")";
                }
            }

            return sqlQuery;
        }


        public static string getPOLineItem2ReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM vw_purchase_order_line_item V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {

                if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.ITEM_ID] != null)
                {
                    sqlQuery += " AND V.ITEM_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.ITEM_ID].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
                {
                    sqlQuery += " AND V.PO_DATE between to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy') ";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
                {
                    sqlQuery += " AND to_date('" + VMVServices.Web.Utils.ReportFilterParameter["To_Date"].ToString() + "','dd/MM/yyyy') ";
                }
            }
            return sqlQuery;
        }

        public static string getPOLineNumberReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery = "SELECT * FROM INV_POLINE_ITEM_R V WHERE ROWNUM > 0 ";

            if (VMVServices.Web.Utils.ReportFilterParameter != null || VMVServices.Web.Utils.ReportFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.PO_LINE_NUM] != null)
                {
                    sqlQuery += " AND V.PO_LINE_NUM = '" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.PO_LINE_NUM].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.VENDOR_ID] != null)
                {
                    sqlQuery += " AND V.VENDOR_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.VENDOR_ID].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.ITEM_ID] != null)
                {
                    sqlQuery += " AND V.item_id = '" + VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.ITEM_ID].ToString() + "'";
                }
             
                if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.PO_HEADER_ID] != null)
                {
                    sqlQuery += " AND V.PO_HEADER_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.PO_HEADER_ID].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null && VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
                {
                    sqlQuery += " AND V.PO_DATE between to_date('" + VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy') ";
                    sqlQuery += " AND to_date('" + VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"].ToString() + "','dd/MM/yyyy') ";
                }
            }

            return sqlQuery;
        }

        public static string IsServiceOrItem(string itemId)
        {
            sqlQuery = string.Empty;

            sqlQuery += "  select ii.inv_item_type from inv_item_master ii where ii.item_id='" + itemId + "'";

            return sqlQuery;
        }
        public static string getPONumber()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT PH.PO_HEADER_ID, PH.PO_NUM AS PO_NUMBER FROM PO_HEADERS PH ";
            sqlQuery += " WHERE PH.WORKFLOW_COMPLETION_STATUS='1' ";
            sqlQuery += " AND PH.ENABLED_FLAG='1' ";
            sqlQuery += " AND ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " ORDER BY PH.PO_NUM ";
            return sqlQuery;
        }
        public static string getPONumber4Supplier(string str_Supplier)
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT PH.PO_HEADER_ID, PH.PO_NUM AS PO_NUMBER FROM PO_HEADERS PH ";
            sqlQuery += " WHERE PH.WORKFLOW_COMPLETION_STATUS='1' ";
            sqlQuery += " AND PH.ENABLED_FLAG='1' ";
            sqlQuery += " AND ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " AND VENDOR_LOC_ID='" + str_Supplier + "'";
            sqlQuery += " ORDER BY PH.PO_NUM ";
            return sqlQuery;
        }
        public static string getPOLineNumber()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT DISTINCT PL.PO_LINE_NUM FROM PO_LINES PL ";
            sqlQuery += " WHERE PL.WORKFLOW_COMPLETION_STATUS='1' ";
            sqlQuery += " AND PL.ENABLED_FLAG='1' ";
            sqlQuery += " ORDER BY PL.PO_LINE_NUM ";
            return sqlQuery;
        }
        public static string ValidateGoodsReceivedOrNot(string poHeaderId = "")
        {
            sqlQuery = string.Empty;
            sqlQuery = "  select count(1) as counts from inv_receipts_hdr irh,inv_receipt_dtls ird";
            sqlQuery += "  where irh.receipt_id=ird.receipt_id";
            sqlQuery += "  and irh.workflow_completion_status='1'";
            sqlQuery += "  and irh.org_id='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += "  and ird.po_header_id='" + poHeaderId + "'";
            return sqlQuery;
        }
    }
}
