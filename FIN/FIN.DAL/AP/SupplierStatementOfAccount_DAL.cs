﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;

using System.Data.EntityClient;



using System.Data.Entity.Infrastructure;

namespace FIN.DAL.AP
{
   public class SupplierStatementOfAccount_DAL
    {
       static string sqlQuery = "";


       public static string getSupplierStatementOfAccount()
       {
           sqlQuery = string.Empty;
           sqlQuery += " SELECT * FROM AP_SUPLR_STMT_ACCOUNT V WHERE ROWNUM > 0 ";
          
           if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
           {
             
               if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.VENDOR_ID] != null)
               {
                   sqlQuery += " AND V.VENDOR_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.VENDOR_ID].ToString() + "'";
               }

           }

           return sqlQuery;
       }
       public static string getVendorNumber()
       {
           sqlQuery = string.Empty;

           sqlQuery = " SELECT T.VENDOR_ID ";
           sqlQuery += " FROM SUPPLIER_CUSTOMERS  T ";
           

           return sqlQuery;
       }


       public static string GetSP_SupplierStatementOfAccount(string vendor_id, string tb_fromdate, string tb_todate)
       {
           try
           {
               string strValue = string.Empty;
               string strNextCode = string.Empty;
               string strErrMsg = string.Empty;
               string retcode = string.Empty;

               //DateTime StartDate = new DateTime();
               //if (tb_fromdate != null && tb_fromdate.Length > 0)
               //    StartDate = DBMethod.ConvertStringToDate(tb_fromdate);
               //DateTime EndDate = new DateTime();
               //if (tb_todate != null && tb_todate.Length > 0)
               //    EndDate = DBMethod.ConvertStringToDate(tb_todate);

               OracleCommand oraCmd = new OracleCommand();

               oraCmd.CommandText = "PROC_SUPLR_STMT_ACCNT";

               oraCmd.CommandType = CommandType.StoredProcedure;

               oraCmd.Parameters.Add(new OracleParameter("@P_VENDOR_ID", OracleDbType.Varchar2, 250)).Value = vendor_id;
               oraCmd.Parameters.Add(new OracleParameter("@P_FROM_DATE", OracleDbType.Varchar2, 250)).Value = tb_fromdate;

               if (tb_todate != null && tb_todate.Length > 0)
               {
                   oraCmd.Parameters.Add(new OracleParameter("@P_TO_DATE", OracleDbType.Varchar2, 250)).Value = tb_todate;
               }
               oraCmd.Parameters.Add(new OracleParameter("@P_ORG_ID", OracleDbType.Varchar2, 250)).Value = VMVServices.Web.Utils.OrganizationID;
               oraCmd.Parameters.Add("@P_RET", OracleDbType.Varchar2, 250);
               oraCmd.Parameters["@P_RET"].Direction = ParameterDirection.Output;
            
               oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);
               retcode = oraCmd.Parameters["@P_RET"].Value.ToString();

               return retcode;
           }
           catch (Exception ex)
           {
               throw ex;
           }
       }
       //public static DataSet GetSP_SupplierStatementOfAccount(string vendor_id, DateTime tb_fromdate, DateTime tb_todate ,string str_Query)
       //{
       //    try
       //    {
       //        string strValue = string.Empty;
       //        string strNextCode = string.Empty;
       //        string strErrMsg = string.Empty;

       //        OracleCommand oraCmd = new OracleCommand();
       //        oraCmd.CommandText = "PROC_SUPLR_STMT_ACCNT";
       //        oraCmd.CommandType = CommandType.StoredProcedure;
       //        oraCmd.Parameters.Add(new OracleParameter("@p_org_id", OracleDbType.Varchar2, 250)).Value = VMVServices.Web.Utils.OrganizationID;
       //        oraCmd.Parameters.Add(new OracleParameter("@P_VENDOR_ID", OracleDbType.Varchar2, 250)).Value = vendor_id;
       //        oraCmd.Parameters.Add(new OracleParameter("@P_FROM_DATE", OracleDbType.Date, 250)).Value = tb_fromdate.ToString("dd/MMM/yyyy");
       //        oraCmd.Parameters.Add(new OracleParameter("@P_END_DATE", OracleDbType.Date, 250)).Value = tb_todate.ToString("dd/MMM/yyyy");



       //        return DBMethod.ExecuteStoredProcedure(oraCmd, str_Query);



       //    }
       //    catch (Exception ex)
       //    {
       //        throw ex;
       //    }
       //}
    }
}
