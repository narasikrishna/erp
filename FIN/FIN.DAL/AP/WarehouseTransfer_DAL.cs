﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace FIN.DAL.AP
{
    public class WarehouseTransfer_DAL
    {
        static string sqlQuery = "";

        public static string GetWHTransdtls(String INV_TRANS_ID)
        {
            sqlQuery = string.Empty;


            sqlQuery = " select TD.INV_TRANS_DTL_ID,AC.ACCT_CODE_ID AS CODE_ID,AC.ACCT_CODE_DESC" + VMVServices.Web.Utils.LanguageCode +" AS CODE_NAME,RL.LOT_ID,RL.LOT_NUMBER,TD.INV_ITEM_QTY,'N' AS DELETED,td.INV_ITEM_ID as item_id,";
            sqlQuery += " (SELECT (im.ITEM_NUMBER||' - ' || im.item_name||' - '||im.item_description)  FROM INV_ITEM_MASTER IM where im.ITEM_ID=td.INV_ITEM_ID) AS ITEM_NAME";
            sqlQuery += " from INV_TRANSFER_DTL TD,inv_receipt_lots_hdr rl,GL_ACCT_CODES ac";
            sqlQuery += " WHERE TD.INV_LOT_ID = RL.LOT_ID";
            sqlQuery += " AND TD.ACCT_CODE_ID = AC.ACCT_CODE_ID";
            sqlQuery += " AND TD.INV_TRANS_ID = '" + INV_TRANS_ID + "'";
            sqlQuery += " AND TD.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += "    and rl.LOT_ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " AND TD.ENABLED_FLAG = 1";
            sqlQuery += " order by INV_TRANS_DTL_ID ";
            return sqlQuery;

        }

        public static string getWarehouseFROM()
        {
            sqlQuery = string.Empty;

            sqlQuery = " select w.inv_wh_id,w.inv_wh_name";
            sqlQuery += " from inv_warehouses w";
            sqlQuery += " where w.workflow_completion_status = 1";
            sqlQuery += " and w.enabled_flag = 1";
            sqlQuery += "    and w.ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " order by w.inv_wh_id";


            return sqlQuery;
        }

        public static string getWarehouseTO()
        {
            sqlQuery = string.Empty;

            sqlQuery = " select w.inv_wh_id,w.inv_wh_name";
            sqlQuery += " from inv_warehouses w";
            sqlQuery += " where w.workflow_completion_status = 1";
            sqlQuery += " and w.enabled_flag = 1";
            sqlQuery += "    and w.ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " order by w.inv_wh_id";


            return sqlQuery;
        }


        public static string getWarehouseFROMBasedReceipt()
        {
            sqlQuery = string.Empty;

            sqlQuery = " select distinct w.inv_wh_id,w.inv_wh_name";
            sqlQuery += " from inv_warehouses w,inv_receipt_item_wh_hdr r";
            sqlQuery += " where w.workflow_completion_status = 1";
            sqlQuery += " and w.enabled_flag = 1 and  w.inv_wh_id=r.wh_id";
            sqlQuery += "    and w.ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " order by w.inv_wh_name";


            return sqlQuery;
        }

      


        //public static string getGRNNo()
        //{
        //    sqlQuery = string.Empty;

        //    sqlQuery = " select rh.receipt_id,rh.grn_num";
        //    sqlQuery += " from inv_receipts_hdr rh";
        //    sqlQuery += " where rh.enabled_flag = 1";
        //    sqlQuery += " and rh.workflow_completion_status = 1";
        //    sqlQuery += " order by rh.receipt_id";


        //    return sqlQuery;
        //}


        public static string getItem()
        {
            sqlQuery = string.Empty;


            sqlQuery = " select i.item_id,i.item_code";
            sqlQuery += " from INV_ITEM_MASTER i";
            sqlQuery += " where i.workflow_completion_status = 1";
            sqlQuery += " and i.enabled_flag = 1";
            sqlQuery += " order by i.item_id";
            
            return sqlQuery;
        }

        public static string getItemname(string item_id)
        {
            sqlQuery = string.Empty;


            sqlQuery = " select i.item_name";
            sqlQuery += " from INV_ITEM_MASTER i";
            sqlQuery += " where i.workflow_completion_status = 1";
            sqlQuery += "  and i.enabled_flag = 1";
            sqlQuery += " and i.item_id =  '" + item_id + "'";
            sqlQuery += " order by i.item_id";            

            return sqlQuery;
        }


        //public static string getpodata(string item_id)
        //{
        //    sqlQuery = string.Empty;



        //    sqlQuery = " select ph.po_num,im.item_description,pl.po_line_num,im.item_id";
        //    sqlQuery += " from INV_RECEIPT_DTLS rd,PO_HEADERS ph,Po_Lines pl,INV_ITEM_MASTER im";
        //    sqlQuery += " where rd.po_header_id = ph.po_header_id";
        //    sqlQuery += " and rd.po_line_id = pl.po_line_id";
        //    sqlQuery += " and rd.item_id = im.item_id";
        //    sqlQuery += " and rd.item_id = '" + item_id + "'";
        //    sqlQuery += " and rd.workflow_completion_status = 1";
        //    sqlQuery += " and rd.enabled_flag = 1";


        //    return sqlQuery;
        //}

        public static string getLotNo()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT RLD.LOT_ID,RLD.LOT_NUMBER";
            sqlQuery += " FROM INV_RECEIPT_LOTS_HDR RLD";
            sqlQuery += " WHERE RLD.WORKFLOW_COMPLETION_STATUS = 1";
            //sqlQuery += " AND RLD.LOT_ID NOT IN (SELECT RWD.LOT_ID FROM INV_TRANSFER_DTL RWD WHERE RWD.ENABLED_FLAG = 1 AND RWD.WORKFLOW_COMPLETION_STATUS = 1)";
            sqlQuery += " AND RLD.ENABLED_FLAG = 1";
            sqlQuery += " ORDER BY RLD.LOT_ID";

            return sqlQuery;
        }

        public static string getQuatity(string lot_id)
        {
            sqlQuery = string.Empty;


            sqlQuery = " SELECT RLD.LOT_QTY";
            sqlQuery += " FROM INV_RECEIPT_LOTS_HDR RLD";
            sqlQuery += " WHERE RLD.LOT_ID = '" + lot_id + "'";
            sqlQuery += " AND RLD.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND RLD.ENABLED_FLAG = 1";

            return sqlQuery;
        }

    }
}
