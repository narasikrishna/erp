﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VMVServices.Web;
namespace FIN.DAL.AP
{
    public class WarehouseReceiptItem_DAL
    {
        static string sqlQuery = "";

        public static string GetWHRecptitemdtls(String inv_receipt_item_wh_id)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select rwd.PK_ID,rwd.INV_RECEIPT_ITEM_WH_ID,rwd.lot_id as lot_id,rl.lot_number,rwd.lot_qty,'0' AS DELETED";
            sqlQuery += " from INV_RECEIPT_ITEM_WH_DTL rwd,inv_receipt_lots_hdr rl";
            sqlQuery += " where rwd.lot_id = rl.lot_id";
            sqlQuery += " and rwd.workflow_completion_status = 1";
            sqlQuery += " and rwd.enabled_flag = 1";
            sqlQuery += " and rwd.inv_receipt_item_wh_id =  '" + inv_receipt_item_wh_id + "'";
            sqlQuery += " order by PK_ID ";

            return sqlQuery;

        }

        public static string getWarehouse()
        {
            sqlQuery = string.Empty;

            sqlQuery = " select w.inv_wh_id,w.inv_wh_name" + VMVServices.Web.Utils.LanguageCode + " as inv_wh_name ";
            sqlQuery += " from inv_warehouses w";
            sqlQuery += " where w.workflow_completion_status = 1";
            sqlQuery += " and w.enabled_flag = 1";
            if (VMVServices.Web.Utils.LanguageCode != string.Empty)
            {
                sqlQuery += "   and w.inv_wh_name" + VMVServices.Web.Utils.LanguageCode + "  is not null";
            }
            sqlQuery += "    and w.ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " order by w.inv_wh_id";


            return sqlQuery;
        }

        public static string getGRNNo()
        {
            sqlQuery = string.Empty;

            sqlQuery = " select rh.receipt_id,rh.grn_num";
            sqlQuery += " from inv_receipts_hdr rh";
            sqlQuery += " where rh.enabled_flag = 1";
            sqlQuery += " and rh.workflow_completion_status = 1";
            sqlQuery += "    and rh.ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " order by rh.receipt_id";


            return sqlQuery;
        }


        public static string getLineNo()
        {
            sqlQuery = string.Empty;


            sqlQuery = " SELECT IM.ITEM_ID,RD.LINE_NUM";
            sqlQuery += " FROM inv_receipt_dtls RD,INV_ITEM_MASTER IM";
            sqlQuery += " WHERE RD.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND RD.ITEM_ID = IM.ITEM_ID";
            sqlQuery += " AND RD.ENABLED_FLAG = 1";
            sqlQuery += "    and im.ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " ORDER BY RD.RECEIPT_DTL_ID";

            return sqlQuery;
        }

        public static string getgrndata(string str_recpt_id)
        {
            sqlQuery = string.Empty;


            sqlQuery = " SELECT RH.RECEIPT_DATE AS GRN_DATE";
            sqlQuery += " FROM INV_RECEIPTS_HDR RH";
            sqlQuery += " WHERE RH.RECEIPT_ID = '" + str_recpt_id + "'";
            sqlQuery += " AND RH.ENABLED_FLAG = 1";
            sqlQuery += " AND RH.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += "  and rh.ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            return sqlQuery;
        }


        public static string getpodata(string item_id)
        {
            sqlQuery = string.Empty;



            sqlQuery = " select ph.po_num,im.item_description" + VMVServices.Web.Utils.LanguageCode + " as item_description,pl.po_line_num,im.item_id";
            sqlQuery += " from INV_RECEIPT_DTLS rd,PO_HEADERS ph,Po_Lines pl,INV_ITEM_MASTER im";
            sqlQuery += " where rd.po_header_id = ph.po_header_id";
            sqlQuery += " and rd.po_line_id = pl.po_line_id";
            sqlQuery += " and rd.item_id = im.item_id";
            sqlQuery += " and rd.item_id = '" + item_id + "'";
            sqlQuery += " and rd.workflow_completion_status = 1";
            sqlQuery += " and rd.enabled_flag = 1";
            if (VMVServices.Web.Utils.LanguageCode != string.Empty)
            {
                sqlQuery += "   and im.item_description" + VMVServices.Web.Utils.LanguageCode + "  is not null";
            }
            sqlQuery += "    and im.ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            return sqlQuery;
        }

        public static string getLotNo()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT RLD.LOT_ID,RLD.LOT_NUMBER";
            sqlQuery += " FROM INV_RECEIPT_LOTS_HDR RLD";
            sqlQuery += " WHERE RLD.WORKFLOW_COMPLETION_STATUS = 1";
            //sqlQuery += " AND RLD.LOT_ID NOT IN (SELECT RWD.LOT_ID FROM INV_RECEIPT_ITEM_WH_DTL RWD WHERE RWD.ENABLED_FLAG = 1 AND RWD.WORKFLOW_COMPLETION_STATUS = 1)";
            sqlQuery += " AND   not exists (SELECT RWD.LOT_ID FROM INV_RECEIPT_ITEM_WH_DTL RWD WHERE RWD.ENABLED_FLAG = 1 and RWD.LOT_ID=RLD.LOT_ID AND RWD.WORKFLOW_COMPLETION_STATUS = 1)";

            sqlQuery += " AND RLD.ENABLED_FLAG = 1";
            sqlQuery += "    and rld.LOT_ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " ORDER BY RLD.LOT_NUMBER";

            return sqlQuery;
        }

        public static string getLotNoBasedReceipt(string receiptDtlId, string mode, string pk_id)
        {
            sqlQuery = string.Empty;

            sqlQuery = "  SELECT RLD.LOT_ID,RLD.LOT_NUMBER";
            sqlQuery += "  FROM INV_RECEIPT_LOTS_HDR RLD";
            sqlQuery += "  WHERE RLD.WORKFLOW_COMPLETION_STATUS = 1";
            //    sqlQuery += "  AND RLD.LOT_ID NOT IN (SELECT RWD.LOT_ID FROM INV_RECEIPT_ITEM_WH_DTL RWD WHERE RWD.ENABLED_FLAG = 1 AND RWD.WORKFLOW_COMPLETION_STATUS = 1)";
            // sqlQuery += "  AND not exists (SELECT RWD.LOT_ID FROM INV_RECEIPT_ITEM_WH_DTL RWD WHERE RWD.ENABLED_FLAG = 1 and RWD.LOT_ID=RLD.LOT_ID  AND RWD.WORKFLOW_COMPLETION_STATUS = 1 having sum(rwd.lot_qty) = sum(rld.lot_qty)";
           // sqlQuery += "  AND not exists ";
            sqlQuery += "   AND rld.lot_id not in ";
            sqlQuery += "    (SELECT RWD.LOT_ID";
            sqlQuery += "    FROM INV_RECEIPT_ITEM_WH_DTL RWD, INV_RECEIPT_LOTS_HDR RLD";
            sqlQuery += "    WHERE RWD.ENABLED_FLAG = 1";
            sqlQuery += "    and RWD.LOT_ID = RLD.LOT_ID";
            sqlQuery += "   AND RWD.WORKFLOW_COMPLETION_STATUS = 1 ";

            sqlQuery += "   having sum(rwd.lot_qty) =";
            sqlQuery += "   (SELECT sum(RLD.Lot_Qty)";
            sqlQuery += "     FROM INV_RECEIPT_LOTS_HDR RLD";
            sqlQuery += "    WHERE RLD.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += "   and RWD.LOT_ID = RLD.LOT_ID";
            sqlQuery += "   AND RLD.ENABLED_FLAG = 1";
            sqlQuery += "   and rld.receipt_dtl_id ='" + receiptDtlId + "'";
            sqlQuery += "  )";


            if (FINTableConstant.Add != mode)
            {
                sqlQuery += " and rwd.inv_receipt_item_wh_id <> '" + pk_id + "' ";
                sqlQuery += "  group by RWD.LOT_ID,rwd.inv_receipt_item_wh_id )";
            }
            else
            {
                sqlQuery += "  group by RWD.LOT_ID )";
            }


            sqlQuery += "  AND RLD.ENABLED_FLAG = 1";
            sqlQuery += "   and rld.receipt_dtl_id='" + receiptDtlId + "'";
            sqlQuery += "    and rld.LOT_ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += "  ORDER BY RLD.LOT_NUMBER";

            return sqlQuery;
        }
        public static bool IsWHLotExists(string INV_RECEIPT_ITEM_WH_ID, string WH_ID, string RECEIPT_ID, string RECEIPT_DTL_ID, string LOT_ID)
        {
            sqlQuery = string.Empty;

            bool retValue = false;

            sqlQuery = "   select count(1) as counts";
            sqlQuery += "  from iNV_RECEIPT_ITEM_WH_HDR ww,iNV_RECEIPT_ITEM_WH_DTL wd";
            sqlQuery += "  where ww.inv_receipt_item_wh_id=wd.inv_receipt_item_wh_id";
            sqlQuery += "  and ww.WH_ID = '" + WH_ID + "'";
            sqlQuery += "  and ww.inv_receipt_item_wh_id <>'" + INV_RECEIPT_ITEM_WH_ID + "'";
            sqlQuery += "  and ww.receipt_id='" + RECEIPT_ID + "'";
            sqlQuery += "  and ww.RECEIPT_DTL_ID='" + RECEIPT_DTL_ID + "'";
            sqlQuery += "  and wd.LOT_ID='" + LOT_ID + "'";
          
            int retValues = 0;
            retValues = DBMethod.GetIntValue(sqlQuery);

            if (retValues > 0)
            {
                retValue = true;
            }
            else
            {
                retValue = false;
            }

            return retValue;
        }

        public static string getQuatity(string lot_id)
        {
            sqlQuery = string.Empty;


            sqlQuery = " SELECT RLD.LOT_QTY";
            sqlQuery += " FROM INV_RECEIPT_LOTS_HDR RLD";
            sqlQuery += " WHERE RLD.LOT_ID = '" + lot_id + "'";
            sqlQuery += " AND RLD.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += "    and rld.LOT_ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " AND RLD.ENABLED_FLAG = 1";

            return sqlQuery;
        }

        public static string getReceiptDetails4WareHouse(string str_wh_id)
        {
            sqlQuery = string.Empty;
            sqlQuery = "  select RH.RECEIPT_ID,RH.GRN_NUM ";
            sqlQuery += " FROM INV_RECEIPT_ITEM_WH_HDR RIWH ";
            sqlQuery += " inner join INV_RECEIPTS_HDR RH ON RH.RECEIPT_ID= RIWH.RECEIPT_ID ";
            sqlQuery += " WHERE RIWH.WH_ID='" + str_wh_id + "'";
            sqlQuery += "    and RIWH.INV_ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " ORDER BY GRN_NUM ASC ";

            return sqlQuery;
        }
        public static string getItem4Receipt(string str_rec_id = "")
        {
            sqlQuery = string.Empty;
            sqlQuery = "   SELECT Distinct IM.ITEM_ID, (im.ITEM_NUMBER" + VMVServices.Web.Utils.LanguageCode + " ||' - ' || im.item_name" + VMVServices.Web.Utils.LanguageCode + " ||' - '||im.item_description" + VMVServices.Web.Utils.LanguageCode + " ) AS ITEM_NAME  ";
            sqlQuery += " FROM INV_RECEIPT_ITEM_WH_HDR  RIWH ";
            sqlQuery += " INNER JOIN INV_ITEM_MASTER IM ON IM.ITEM_ID = RIWH.ITEM_ID ";
            if (str_rec_id != string.Empty)
            {
                sqlQuery += " WHERE RIWH.RECEIPT_ID='" + str_rec_id + "'";
            }
            if (VMVServices.Web.Utils.LanguageCode != string.Empty)
            {
                sqlQuery += "   and im.ITEM_NUMBER" + VMVServices.Web.Utils.LanguageCode + "  is not null";
            }
            sqlQuery += "    and RIWH.INV_ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " ORDER BY ITEM_NAME ASC ";
            return sqlQuery;
        }
        public static string getItemDetails4WareHouse(string str_wh_id)
        {
            sqlQuery = string.Empty;
            sqlQuery = "   SELECT Distinct IM.ITEM_ID, IM.ITEM_DESCRIPTION" + VMVServices.Web.Utils.LanguageCode + "  AS ITEM_NAME  ";
            sqlQuery += " FROM INV_RECEIPT_ITEM_WH_HDR  RIWH ";
            sqlQuery += " INNER JOIN INV_ITEM_MASTER IM ON IM.ITEM_ID = RIWH.ITEM_ID ";
            sqlQuery += " WHERE RIWH.WH_ID='" + str_wh_id + "'";
            if (VMVServices.Web.Utils.LanguageCode != string.Empty)
            {
                sqlQuery += "   and IM.ITEM_DESCRIPTION" + VMVServices.Web.Utils.LanguageCode + "  is not null";
            }
            sqlQuery += "    and RIWH.INV_ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " ORDER BY ITEM_NAME ASC ";

            return sqlQuery;
        }

        public static string getLotDetails4WHItemRecID(string str_Wh_ID, string str_item_id, string str_Rec_Id = "")
        {
            sqlQuery = string.Empty;
            sqlQuery = "SELECT RLH.LOT_ID,RLH.LOT_NUMBER  ";
            sqlQuery += " FROM INV_RECEIPT_ITEM_WH_DTL RIWD ";
            sqlQuery += " INNER JOIN INV_RECEIPT_ITEM_WH_HDR RIWH ON RIWH.INV_RECEIPT_ITEM_WH_ID= RIWD.INV_RECEIPT_ITEM_WH_ID ";
            sqlQuery += " INNER JOIN INV_RECEIPT_LOTS_HDR RLH ON RLH.LOT_ID = RIWD.LOT_ID ";
            sqlQuery += " WHERE RIWH.WH_ID='" + str_Wh_ID + "'";
            if (str_item_id != string.Empty)
            {
                sqlQuery += "  AND RIWH.ITEM_ID='" + str_item_id + "'";
            }
            if (str_Rec_Id != string.Empty)
            {
                sqlQuery += "  and RIWH.RECEIPT_ID='" + str_Rec_Id + "'";
            }
            sqlQuery += "    and RIWH.INV_ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            return sqlQuery;
        }

    }
}
