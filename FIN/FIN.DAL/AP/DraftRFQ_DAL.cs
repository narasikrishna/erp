﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.AP
{
    public class DraftRFQ_DAL
    {
        static string sqlQuery = "";

        public static string GetDraftRFQDtls(string draftrfqId)
        {
            sqlQuery = string.Empty;

            sqlQuery = "   select l.TMP_RFQ_DTL_ID,l.attribute1 as LINE_NUM,";
            sqlQuery += "   (nvl(l.TMP_RFQ_ITEM_QTY, 0) * nvl(l.tmp_rfq_item_cost, 0)) as line_Amount,";
            sqlQuery += "   '0' AS DELETED,";
            sqlQuery += "   l.tmp_rfq_item_id as tmp_rfq_item_id,";
            sqlQuery += "   l.TMP_RFQ_ITEM_QTY,";
            sqlQuery += "   (select im.INV_ITEM_TYPE";
            sqlQuery += "      From inv_item_master im";
            sqlQuery += "     where l.tmp_rfq_item_id = im.item_id) as INV_ITEM_TYPE,";
            sqlQuery += "   (select im.item_name";
            sqlQuery += "      From inv_item_master im";
            sqlQuery += "      where l.tmp_rfq_item_id = im.item_id) as item_name,";
            sqlQuery += "    (select um.uom_code||' - '||um.uom_desc";
            sqlQuery += "      From inv_uom_master um";
            sqlQuery += "     where l.tmp_rfq_item_uom = um.uom_id) as UOM_NAME,";
            sqlQuery += "    l.tmp_rfq_item_uom,l.tmp_rfq_item_uom as UOM_ID,";
            sqlQuery += "    l.tmp_rfq_item_cost,";
            sqlQuery += "    l.tmp_rfq_comments,'' as tmp_rfq_supplier_id,'' as VENDOR_ID,''  as VENDOR_NAME";
            //sqlQuery += "    rr.tmp_rfq_supplier_id,";
            //sqlQuery += "    (select sc.vendor_code || sc.vendor_name";
            //sqlQuery += "    from supplier_customers sc";
            //sqlQuery += "   where sc.vendor_id = rr.tmp_rfq_supplier_id";
            //sqlQuery += "   and lower(sc.vendor_type) = 'supplier') as VENDOR_NAME,rr.TMP_RFQ_SUPPLIER_ID as VENDOR_ID";
            sqlQuery += "   from po_tmp_rfq_hdr              h,";
            sqlQuery += "   po_tmp_rfq_dtl              l";
            //sqlQuery += "   po_tmp_rfq_supplier_mapping rr";
            //sqlQuery += "   po_requisition_hdr          rh,";
            //sqlQuery += "  po_requisition_dtl          rd";
            sqlQuery += "   where h.tmp_rfq_id = l.tmp_rfq_id";
            //sqlQuery += "    and rh.po_req_id = rd.po_req_id and rd.po_req_id = h.TMP_RFQ_NUM ";
            //sqlQuery += "      and l.tmp_rfq_dtl_id=rr.tmp_rfq_dtl_id";
            sqlQuery += "    and h.enabled_flag = '1'";
            //sqlQuery += "    and rh.enabled_flag = '1'";
            sqlQuery += "    and l.tmp_rfq_id='" + draftrfqId + "'";
            sqlQuery += " order by TMP_RFQ_DTL_ID ";


            return sqlQuery;
        }
        public static string GetRFQDetails(string poReqId, string itemId = "")
        {
            sqlQuery = string.Empty;

            sqlQuery = "  select distinct '' as TMP_RFQ_DTL_ID,'' as LINE_NUM,";
            sqlQuery += "    '0' AS DELETED,";
            sqlQuery += "    rd.item_id as tmp_rfq_item_id,";
            sqlQuery += "    sum(rd.po_quantity) as TMP_RFQ_ITEM_QTY,";
            sqlQuery += "    (select im.INV_ITEM_TYPE";
            sqlQuery += "      From inv_item_master im";
            sqlQuery += "     where rd.item_id = im.item_id) as INV_ITEM_TYPE,";
            sqlQuery += "    (select im.item_name";
            sqlQuery += "       From inv_item_master im";
            sqlQuery += "     where rd.item_id = im.item_id) as item_name,";
            sqlQuery += "   '' as UOM_ID,'' as UOM_NAME,";
            sqlQuery += "   '' as tmp_rfq_item_uom,";
            sqlQuery += "    sum(rd.po_item_unit_price) as tmp_rfq_item_cost,";
            sqlQuery += "    '' as tmp_rfq_comments,";
            sqlQuery += "    '' as tmp_rfq_supplier_id,";
            sqlQuery += "    '' as VENDOR_NAME,'' as VENDOR_ID";
            sqlQuery += "    from po_requisition_hdr rh, po_requisition_dtl rd";
            sqlQuery += "   where rh.po_req_id = rd.po_req_id";
            sqlQuery += "     and rh.workflow_completion_status=1";
            sqlQuery += "   and rh.enabled_flag=1";
            sqlQuery += "  and rh.org_id='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += "   and rh.po_req_id in (" + poReqId + ")";
            sqlQuery += "   group by rd.item_id ";
            sqlQuery += "   order by item_name asc ";

            return sqlQuery;
        }
        public static string GetDraftRFQSupplierDtls(string tmpRfqDtlId = "")
        {
            sqlQuery = string.Empty;
            sqlQuery = "  select distinct sc.vendor_id,sc.vendor_code||' - '|| sc.vendor_name as vendor_name";
            sqlQuery += "   from po_tmp_rfq_supplier_mapping sm,supplier_customers sc";

            sqlQuery += "   where sm.tmp_rfq_supplier_id=sc.vendor_id";

            if (tmpRfqDtlId != string.Empty)
            {
                sqlQuery += "  and  sm.TMP_RFQ_DTL_ID='" + tmpRfqDtlId + "'";
            }
            sqlQuery += "  and sc.org_id='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += "   order by vendor_name asc";
            return sqlQuery;
        }
        public static string GetDraftRFQBasedSupplier(string supplierId, string draftRfqId = "")
        {
            sqlQuery = string.Empty;

            sqlQuery = "  select distinct tr.tmp_rfq_dtl_id,rh.tmp_rfq_id,rh.tmp_rfq_num,rh.tmp_rfq_dt,rh.tmp_rfq_due_dt,rh.tmp_rfq_currency,rh.tmp_rfq_status";
            sqlQuery += "  from po_tmp_rfq_hdr rh,po_tmp_rfq_supplier_mapping sm,po_tmp_rfq_dtl tr";
            sqlQuery += "  where rh.enabled_flag=1";
            sqlQuery += "  and rh.workflow_completion_status=1";
            sqlQuery += "   and tr.tmp_rfq_dtl_id=sm.tmp_rfq_dtl_id";
            sqlQuery += "  and tr.tmp_rfq_id=rh.tmp_rfq_id";
            sqlQuery += "  and rh.TMP_RFQ_ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += "     and sm.tmp_rfq_supplier_id='" + supplierId + "'";

            if (draftRfqId != string.Empty)
            {
                sqlQuery += "  and rh.tmp_rfq_id='" + draftRfqId + "'";
            }
            sqlQuery += "  and upper(rh.TMP_RFQ_STATUS)='APPROVED'";
            sqlQuery += "   order by rh.tmp_rfq_num asc";

            return sqlQuery;
        }
        public static string GetDraftRFQNo(string supplierId)
        {
            sqlQuery = string.Empty;
            sqlQuery += "    select distinct rh.tmp_rfq_id, rh.tmp_rfq_num";
            sqlQuery += "    from po_tmp_rfq_hdr rh, po_tmp_rfq_supplier_mapping sm, po_tmp_rfq_dtl tr";
            sqlQuery += "   where rh.enabled_flag = 1";
            sqlQuery += "   and rh.workflow_completion_status = 1";
            sqlQuery += "   and tr.tmp_rfq_dtl_id = sm.tmp_rfq_dtl_id";
            sqlQuery += "   and tr.tmp_rfq_id = rh.tmp_rfq_id";
            sqlQuery += "   and rh.TMP_RFQ_ORG_ID ='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += "   and sm.tmp_rfq_supplier_id ='" + supplierId + "'";
            sqlQuery += "  and upper(rh.TMP_RFQ_STATUS)='APPROVED'";
            sqlQuery += "   order by rh.tmp_rfq_num asc";

            return sqlQuery;
        }
        public static string GetDraftRFQNoBasedSupplier(string supplierId, string draftRfqId = "", string mode = "")
        {
            sqlQuery = string.Empty;

            sqlQuery = "  select distinct rh.tmp_rfq_id,rh.tmp_rfq_num";
            sqlQuery += "  from po_tmp_rfq_hdr rh,po_tmp_rfq_supplier_mapping sm,po_tmp_rfq_dtl tr";
            sqlQuery += "  where rh.enabled_flag=1";
            sqlQuery += "  and rh.workflow_completion_status=1";
            sqlQuery += "   and tr.tmp_rfq_dtl_id=sm.tmp_rfq_dtl_id";
            sqlQuery += "  and tr.tmp_rfq_id=rh.tmp_rfq_id";
            sqlQuery += "    and rh.TMP_RFQ_ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += "     and sm.tmp_rfq_supplier_id='" + supplierId + "'";

            if (mode == string.Empty)
            {
                sqlQuery += "      and not exists (select pr.RFQ_TMP_RFQ_ID from po_rfq_hdr pr where pr.workflow_completion_status = 1 and pr.RFQ_TMP_RFQ_ID = rh.tmp_rfq_id ";
                // sqlQuery += "      and rh.tmp_rfq_id not in (select pr.RFQ_TMP_RFQ_ID from po_rfq_hdr pr where pr.workflow_completion_status = 1";
                sqlQuery += "      and pr.RFQ_TMP_RFQ_ID is not null and pr.RFQ_ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
                sqlQuery += "      and pr.rfq_supplier ='" + supplierId + "')";
            }

            if (draftRfqId != string.Empty)
            {
                sqlQuery += "  and rh.tmp_rfq_id='" + draftRfqId + "'";
            }
            sqlQuery += "  and upper(rh.TMP_RFQ_STATUS)='APPROVED'";
            sqlQuery += "   order by rh.tmp_rfq_num asc";

            return sqlQuery;
        }
        public static string GetDraftRFQData(string draftRfqId, string supplierId)
        {
            sqlQuery = string.Empty;

            sqlQuery = "     select distinct '' as rfq_dtl_id,";
            sqlQuery += "    l.TMP_RFQ_DTL_ID,";
            sqlQuery += "    l.attribute1 as LINE_NUM,";
            sqlQuery += "   (nvl(l.TMP_RFQ_ITEM_QTY, 0) * nvl(l.tmp_rfq_item_cost, 0)) as line_Amount,";
            sqlQuery += "    '0' AS DELETED,";
            sqlQuery += "    l.tmp_rfq_item_id,";
            sqlQuery += "   l.TMP_RFQ_ITEM_QTY,";
            sqlQuery += "     (select im.INV_ITEM_TYPE";
            sqlQuery += "   From inv_item_master im";
            sqlQuery += "   where l.tmp_rfq_item_id = im.item_id) as INV_ITEM_TYPE,";
            sqlQuery += "   (select im.ITEM_CODE||' - '||im.item_name";
            sqlQuery += "   From inv_item_master im";
            sqlQuery += "   where l.tmp_rfq_item_id = im.item_id) as item_name,";
            sqlQuery += "   (select um.uom_code||' - '||um.UOM_DESC";
            sqlQuery += "   From inv_uom_master um";
            sqlQuery += "  where l.tmp_rfq_item_uom = um.uom_id) as uom_name,";
            sqlQuery += "   (select um.uom_id";
            sqlQuery += "   From inv_uom_master um";
            sqlQuery += "  where l.tmp_rfq_item_uom = um.uom_id) as uom_id,";
            sqlQuery += "  l.tmp_rfq_item_uom as tmp_rfq_item_uom,";
            sqlQuery += "   l.tmp_rfq_item_cost as tmp_rfq_item_cost";
            sqlQuery += "   from po_tmp_rfq_hdr     h,";
            sqlQuery += "  po_tmp_rfq_dtl     l,";
            sqlQuery += "  po_requisition_hdr rh,po_tmp_rfq_req_mapping rm,PO_TMP_RFQ_SUPPLIER_MAPPING sm";
            sqlQuery += "   where h.tmp_rfq_id = l.tmp_rfq_id ";
            sqlQuery += "   and rm.tmp_rfq_id=h.tmp_rfq_id";
            sqlQuery += "   and rh.po_req_id = rm.tmp_req_id";
            sqlQuery += "   and sm.tmp_rfq_dtl_id = l.tmp_rfq_dtl_id";
            sqlQuery += "   and sm.tmp_rfq_supplier_id = '" + supplierId + "'";
            sqlQuery += "   and h.enabled_flag = '1'";
            sqlQuery += "   and rh.enabled_flag = '1'";
            sqlQuery += "  and rh.ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += "  and h.tmp_rfq_id='" + draftRfqId + "'";
            sqlQuery += "     order by l.tmp_rfq_dtl_id asc";

            return sqlQuery;
        }

        public static string GetREQData(string ReqId)
        {
            sqlQuery = string.Empty;

            sqlQuery = "      SELECT RH.PO_REQ_ID,";
            sqlQuery += "       RH.PO_REQ_ID || ' - ' || RH.PO_REQ_DESC || ' - ' || RH.PO_REQ_DT AS PO_REQ_DESC";
            sqlQuery += "      FROM PO_REQUISITION_HDR RH, po_tmp_rfq_req_mapping rr";
            sqlQuery += "      WHERE RH.WORKFLOW_COMPLETION_STATUS = '1'";
            sqlQuery += "      and rr.tmp_req_id = rh.po_req_id";
            sqlQuery += "      and rr.TMP_RFQ_ID = '" + ReqId + "'";
            sqlQuery += "      AND RH.ENABLED_FLAG = '1'";
            sqlQuery += "   and rh.ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += "     order by PO_REQ_DESC asc";

            return sqlQuery;
        }
        public static string GetExistingREQData(string PoReqId)
        {
            sqlQuery = string.Empty;

            sqlQuery = "   (select pl.po_req_id";
            sqlQuery += "    from po_requisition_dtl pl";
            sqlQuery += "   where pl.po_req_id = '" + PoReqId + "'";
            sqlQuery += "  having sum(pl.PO_QUANTITY) =";
            sqlQuery += "    (select sum(pd.TMP_RFQ_ITEM_QTY)";
            sqlQuery += "    from po_tmp_rfq_dtl pd";
            sqlQuery += "    where pd.tmp_rfq_id in";
            sqlQuery += "   (select vv.TMP_RFQ_ID from po_tmp_rfq_req_mapping vv where vv.tmp_req_id='" + PoReqId + "')";
            sqlQuery += "   )";
            sqlQuery += "   group by pl.po_req_id)";
            sqlQuery += "  order by PO_REQ_ID desc";
            return sqlQuery;
        }

    }
}
