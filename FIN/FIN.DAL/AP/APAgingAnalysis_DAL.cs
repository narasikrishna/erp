﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.AP
{
    public class APAgingAnalysis_DAL
    {
        static string sqlQuery = "";
        public static string getReportData()
        {
            sqlQuery = string.Empty;
            if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null && VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != string.Empty)
            {
                sqlQuery = " SELECT * FROM AP_AGING_ANALYSIS V WHERE ROWNUM > 0 ";
                if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
                {
                    //if (VMVServices.Web.Utils.ReportViewFilterParameter["SEGMENT_ID"] != null)
                    //{
                    //    sqlQuery += " AND V.GLOBAL_SEGMENT_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["SEGMENT_ID"].ToString() + "'";
                    //}

                    if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.VENDOR_ID] != null)
                    {
                        sqlQuery += " AND V.VENDOR_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.VENDOR_ID].ToString() + "'";
                    }
                    if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
                    {
                        sqlQuery += " AND V.INV_DATE >= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy')";
                    }

                }
            }
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {

                sqlQuery = "    select SBT_T.VENDOR_ID,SBT_T.VENDOR_NAME,SBT_T.OUTSTANDING_AMOUNT,SBT_T.INV_DATE,SBT_T.INV_NUM,";
                if (VMVServices.Web.Utils.ReportViewFilterParameter["firstCase"] != null)
                {
                    sqlQuery += "   (case";
                    sqlQuery += "   WHEN SBT_T.No_of_days BETWEEN 0 and '" + VMVServices.Web.Utils.ReportViewFilterParameter["firstCase"] + "' THEN SBT_T.OUTSTANDING_AMOUNT END) as BUGET1,";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["firstCase"] != null && VMVServices.Web.Utils.ReportViewFilterParameter["secondCase"] != null)
                {
                    sqlQuery += "   (case";
                    sqlQuery += "   WHEN SBT_T.No_of_days BETWEEN '" + (int.Parse(VMVServices.Web.Utils.ReportViewFilterParameter["firstCase"].ToString()) + 1) + "' and  '" + VMVServices.Web.Utils.ReportViewFilterParameter["secondCase"] + "' THEN SBT_T.OUTSTANDING_AMOUNT END) as BUGET2,";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["secondCase"] != null && VMVServices.Web.Utils.ReportViewFilterParameter["thirdCase"] != null)
                {
                    sqlQuery += "   (case";
                    sqlQuery += "   WHEN SBT_T.No_of_days BETWEEN '" + (int.Parse(VMVServices.Web.Utils.ReportViewFilterParameter["secondCase"].ToString()) + 1) + "' and  '" + VMVServices.Web.Utils.ReportViewFilterParameter["thirdCase"] + "' THEN SBT_T.OUTSTANDING_AMOUNT END) as BUGET3,";

              }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["thirdCase"] != null && VMVServices.Web.Utils.ReportViewFilterParameter["fourthCase"] != null)
                {
                    sqlQuery += "   (case";
                    sqlQuery += "   WHEN SBT_T.No_of_days BETWEEN '" + VMVServices.Web.Utils.ReportViewFilterParameter["thirdCase"] + "' and  '" + VMVServices.Web.Utils.ReportViewFilterParameter["fourthCase"] + "' THEN SBT_T.OUTSTANDING_AMOUNT END) as BUGET4,";
                }

                if (VMVServices.Web.Utils.ReportViewFilterParameter["fourthCase"] != null && VMVServices.Web.Utils.ReportViewFilterParameter["fifthCase"] != null)
                {
                    sqlQuery += "   (case";
                    sqlQuery += "   WHEN SBT_T.No_of_days BETWEEN '" + VMVServices.Web.Utils.ReportViewFilterParameter["fourthCase"] + "' and  '" + VMVServices.Web.Utils.ReportViewFilterParameter["fifthCase"] + "' THEN SBT_T.OUTSTANDING_AMOUNT END) as BUGET5,";

                    sqlQuery += "   (case";
                    sqlQuery += "   WHEN SBT_T.No_of_days > '" + VMVServices.Web.Utils.ReportViewFilterParameter["fifthCase"] + "' THEN SBT_T.OUTSTANDING_AMOUNT END) as BUGET6,";
               
                }                
                sqlQuery += "   SBT_T.GLOBAL_SEGMENT_ID,sbt_t.org_id";
                sqlQuery += "   from";
                sqlQuery += "   (select SBT.INV_DATE,sbt.org_id,";
                sqlQuery += "      SBT.INV_NUM,";
                sqlQuery += "     SBT.VENDOR_ID,";
                sqlQuery += "     SBT.VENDOR_NAME,";
                sqlQuery += "      SBT.INVOICE_AMOUNT,";
                sqlQuery += "    SBT.AMOUNT_PAID,";
                sqlQuery += "     (SBT.INVOICE_AMOUNT - SBT.AMOUNT_PAID) as OUTSTANDING_AMOUNT,";
                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
                {
                    sqlQuery += "    ((to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "', 'DD/MM/YYYY')) -";
                    sqlQuery += "   ((SBT.INV_DATE))) as No_of_days,";
                }
                sqlQuery += "   SBT.GLOBAL_SEGMENT_ID";
                sqlQuery += "    from (select IH.INV_DATE,ih.inv_org_id as org_id,";
                sqlQuery += "     IH.INV_NUM,";
                sqlQuery += "     IH.VENDOR_ID,";
                sqlQuery += "     SC.VENDOR_NAME,";
                sqlQuery += "    nvl(SUM(ID.INV_ITEM_AMT),0) as INVOICE_AMOUNT,";
                sqlQuery += "    (select nvl(sum(IPH.PAYMENT_AMT),0)";
                sqlQuery += "     from inv_payments_hdr IPH, inv_payments_dtl ppd";
                sqlQuery += "    where IPH.Pay_Vendor_Id = IH.VENDOR_ID and iph.pay_id = ppd.pay_id and ppd.pay_invoice_id = ih.inv_id) as AMOUNT_PAID,";
                sqlQuery += "     IH.GLOBAL_SEGMENT_ID";
                sqlQuery += "     from inv_invoices_hdr   IH,";
                sqlQuery += "     inv_invoices_dtls  ID,";
                sqlQuery += "     supplier_customers SC";
                sqlQuery += "     where IH.INV_ID = ID.INV_ID and ih.rev_flag is null and ih.posted_flag=1";
                sqlQuery += "      and IH.VENDOR_ID = SC.VENDOR_ID  and (ID.INV_ITEM_AMT is not null)";
                sqlQuery += "     group by IH.INV_DATE,ih.inv_id,";
                sqlQuery += "     IH.INV_NUM,";
                sqlQuery += "     IH.VENDOR_ID,";
                sqlQuery += "     SC.VENDOR_NAME,ih.inv_org_id,";
                sqlQuery += "     SC.VENDOR_ID,";
                sqlQuery += "                  IH.GLOBAL_SEGMENT_ID) SBT ) SBT_T ";

             
               

            }
            return sqlQuery;
        }
        public static string GetAgingAnalysisChart()
        {
            sqlQuery = string.Empty;

            //sqlQuery += "   select '0-30' as 30_AgeDiff,";           
            //sqlQuery += "  IH.VENDOR_ID,";
            //sqlQuery += "  SC.VENDOR_NAME,";
            //sqlQuery += "  round(nvl(SUM(ID.INV_ITEM_AMT),0),'" + VMVServices.Web.Utils.DecimalPrecision + "') -";
            //sqlQuery += "  (select round(nvl(sum(IPH.PAYMENT_AMT),0),'" + VMVServices.Web.Utils.DecimalPrecision + "')";
            //sqlQuery += "   from inv_payments_hdr IPH";
            //sqlQuery += "   where IPH.Pay_Vendor_Id = IH.VENDOR_ID) as AMOUNT_PAID";
            //sqlQuery += "  from inv_invoices_hdr IH, inv_invoices_dtls ID, supplier_customers SC";
            //sqlQuery += "  where IH.INV_ID = ID.INV_ID";
            //sqlQuery += "  and IH.VENDOR_ID = SC.VENDOR_ID";
            //sqlQuery += "  and  ih.inv_org_id='" + VMVServices.Web.Utils.OrganizationID + "'";
            //sqlQuery += "  and floor(trunc(sysdate) - to_date(ih.inv_date)) <= 30";
            //sqlQuery += "  group by IH.VENDOR_ID, SC.VENDOR_NAME, ih.inv_org_id, SC.VENDOR_ID";

            //sqlQuery += "  union";
            //sqlQuery += "  select '31-60' as AgeDiff,";         
            //sqlQuery += "  IH.VENDOR_ID,";
            //sqlQuery += "  SC.VENDOR_NAME,";
            //sqlQuery += "  round(nvl(SUM(ID.INV_ITEM_AMT),0),'" + VMVServices.Web.Utils.DecimalPrecision + "') -";
            //sqlQuery += "  (select round(nvl(sum(IPH.PAYMENT_AMT),0),'" + VMVServices.Web.Utils.DecimalPrecision + "')";
            //sqlQuery += "   from inv_payments_hdr IPH";
            //sqlQuery += "   where IPH.Pay_Vendor_Id = IH.VENDOR_ID) as AMOUNT_PAID";
            //sqlQuery += "  from inv_invoices_hdr IH, inv_invoices_dtls ID, supplier_customers SC";
            //sqlQuery += "  where IH.INV_ID = ID.INV_ID";
            //sqlQuery += "  and IH.VENDOR_ID = SC.VENDOR_ID";
            //sqlQuery += "  and  ih.inv_org_id='" + VMVServices.Web.Utils.OrganizationID + "'";
            //sqlQuery += "  and (floor(trunc(sysdate) - to_date(ih.inv_date))) <= 60";
            //sqlQuery += "   and (floor(trunc(sysdate) - to_date(ih.inv_date))) > 30";
            //sqlQuery += "  group by IH.VENDOR_ID, SC.VENDOR_NAME, ih.inv_org_id, SC.VENDOR_ID";

            //sqlQuery += "  union";
            //sqlQuery += "  select '61-90' as AgeDiff,";
            //sqlQuery += "  IH.VENDOR_ID,";
            //sqlQuery += "  SC.VENDOR_NAME,";
            //sqlQuery += "  round(nvl(SUM(ID.INV_ITEM_AMT),0),'"+VMVServices.Web.Utils.DecimalPrecision+"') -";
            //sqlQuery += "  (select round(nvl(sum(IPH.PAYMENT_AMT),0),'" + VMVServices.Web.Utils.DecimalPrecision + "')";
            //sqlQuery += "   from inv_payments_hdr IPH";
            //sqlQuery += "   where IPH.Pay_Vendor_Id = IH.VENDOR_ID) as AMOUNT_PAID";
            //sqlQuery += "  from inv_invoices_hdr IH, inv_invoices_dtls ID, supplier_customers SC";
            //sqlQuery += "  where IH.INV_ID = ID.INV_ID";
            //sqlQuery += "  and IH.VENDOR_ID = SC.VENDOR_ID";
            //sqlQuery += "  and  ih.inv_org_id='" + VMVServices.Web.Utils.OrganizationID + "'";
            //sqlQuery += "  and (floor(trunc(sysdate) - to_date(ih.inv_date))) <= 90";
            //sqlQuery += "  and (floor(trunc(sysdate) - to_date(ih.inv_date))) > 61";
            //sqlQuery += "  group by IH.VENDOR_ID, SC.VENDOR_NAME, ih.inv_org_id, SC.VENDOR_ID";

            //sqlQuery += "  union";
            //sqlQuery += "  select '>90' as AgeDiff,";          
            //sqlQuery += "   IH.VENDOR_ID,";
            //sqlQuery += "   SC.VENDOR_NAME,";
            //sqlQuery += "  round(nvl(SUM(ID.INV_ITEM_AMT),0),'" + VMVServices.Web.Utils.DecimalPrecision + "') -";
            //sqlQuery += "  (select round(nvl(sum(IPH.PAYMENT_AMT),0),'" + VMVServices.Web.Utils.DecimalPrecision + "')";
            //sqlQuery += "   from inv_payments_hdr IPH";
            //sqlQuery += "   where IPH.Pay_Vendor_Id = IH.VENDOR_ID) as AMOUNT_PAID";
            //sqlQuery += "   from inv_invoices_hdr IH, inv_invoices_dtls ID, supplier_customers SC";
            //sqlQuery += "   where IH.INV_ID = ID.INV_ID";
            //sqlQuery += "   and IH.VENDOR_ID = SC.VENDOR_ID";
            //sqlQuery += "   and  ih.inv_org_id='" + VMVServices.Web.Utils.OrganizationID + "'";
            //sqlQuery += "   and (floor(trunc(sysdate) - to_date(ih.inv_date))) > 90";
            //sqlQuery += "  group by IH.VENDOR_ID, SC.VENDOR_NAME, ih.inv_org_id, SC.VENDOR_ID";



            sqlQuery += "   select '0-30' as AgeDiff,";
            sqlQuery += "    round(nvl(SUM(ih.inv_amt), 0), '" + VMVServices.Web.Utils.DecimalPrecision + "') -";
            sqlQuery += "    round(nvl(SUM(pd.pay_invoice_paid_amt), 0), 3) as AMOUNT_PAID";
            sqlQuery += "   from inv_invoices_hdr   IH,";
            sqlQuery += "     inv_invoices_dtls  ID,";
            sqlQuery += "    supplier_customers SC,";
            sqlQuery += "   inv_payments_hdr   pp,";
            sqlQuery += "   inv_payments_dtl   pd";
            sqlQuery += "   where IH.INV_ID = ID.INV_ID";
            sqlQuery += "   and IH.VENDOR_ID = SC.VENDOR_ID";
            sqlQuery += "  and pd.pay_id = pp.pay_id";
            sqlQuery += "  and pd.pay_invoice_id = id.inv_id";
            sqlQuery += "   and  ih.inv_org_id='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += "  and floor(trunc(sysdate) - to_date(ih.inv_date)) <= 30";
            sqlQuery += "  union";
            sqlQuery += "  select '31-60' as AgeDiff,";
            sqlQuery += "   round(nvl(SUM(ih.inv_amt), 0), '" + VMVServices.Web.Utils.DecimalPrecision + "') -";
            sqlQuery += "   round(nvl(SUM(pd.pay_invoice_paid_amt), 0), 3) as AMOUNT_PAID";
            sqlQuery += "  from inv_invoices_hdr   IH,";
            sqlQuery += "   inv_invoices_dtls  ID,";
            sqlQuery += "  supplier_customers SC,";
            sqlQuery += "  inv_payments_hdr   pp,";
            sqlQuery += "   inv_payments_dtl   pd";
            sqlQuery += "   where IH.INV_ID = ID.INV_ID";
            sqlQuery += "    and IH.VENDOR_ID = SC.VENDOR_ID";
            sqlQuery += "   and pd.pay_id = pp.pay_id";
            sqlQuery += "   and pd.pay_invoice_id = id.inv_id";
            sqlQuery += "   and  ih.inv_org_id='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += "  and (floor(trunc(sysdate) - to_date(ih.inv_date))) <= 60";
            sqlQuery += "   and (floor(trunc(sysdate) - to_date(ih.inv_date))) > 30";
            sqlQuery += "  union";
            sqlQuery += "  select '61-90' as AgeDiff,";
            sqlQuery += "   round(nvl(SUM(ih.inv_amt), 0), '" + VMVServices.Web.Utils.DecimalPrecision + "') -";
            sqlQuery += "  round(nvl(SUM(pd.pay_invoice_paid_amt), 0), 3) as AMOUNT_PAID";
            sqlQuery += "  from inv_invoices_hdr   IH,";
            sqlQuery += "   inv_invoices_dtls  ID,";
            sqlQuery += "   supplier_customers SC,";
            sqlQuery += "   inv_payments_hdr   pp,";
            sqlQuery += "  inv_payments_dtl   pd";
            sqlQuery += "   where IH.INV_ID = ID.INV_ID";
            sqlQuery += "    and IH.VENDOR_ID = SC.VENDOR_ID";
            sqlQuery += "   and pd.pay_id = pp.pay_id";
            sqlQuery += "   and pd.pay_invoice_id = id.inv_id";
            sqlQuery += "   and  ih.inv_org_id='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += "   and (floor(trunc(sysdate) - to_date(ih.inv_date))) <= 90";
            sqlQuery += "    and (floor(trunc(sysdate) - to_date(ih.inv_date))) > 61";
            sqlQuery += "  union";
            sqlQuery += "  select '>90' as AgeDiff,";
            sqlQuery += "   round(nvl(SUM(ih.inv_amt), 0), '" + VMVServices.Web.Utils.DecimalPrecision + "') -";
            sqlQuery += "   round(nvl(SUM(pd.pay_invoice_paid_amt), 0), 3) as AMOUNT_PAID";
            sqlQuery += "  from inv_invoices_hdr   IH,";
            sqlQuery += "   inv_invoices_dtls  ID,";
            sqlQuery += "  supplier_customers SC,";
            sqlQuery += "  inv_payments_hdr   pp,";
            sqlQuery += "   inv_payments_dtl   pd";
            sqlQuery += "  where IH.INV_ID = ID.INV_ID";
            sqlQuery += "   and IH.VENDOR_ID = SC.VENDOR_ID";
            sqlQuery += "   and pd.pay_id = pp.pay_id";
            sqlQuery += "   and pd.pay_invoice_id = id.inv_id";
            sqlQuery += "   and  ih.inv_org_id='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += "   and (floor(trunc(sysdate) - to_date(ih.inv_date))) > 90";

            return sqlQuery;
        }
        public static string getReservedGuaranteeBalanceReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_AP_PAYMENT V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {


                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
                {
                    sqlQuery += " AND V.pay_date between to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy') ";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
                {
                    sqlQuery += " AND to_date('" + VMVServices.Web.Utils.ReportFilterParameter["To_Date"].ToString() + "','dd/MM/yyyy') ";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.VENDOR_ID] != null)
                {
                    sqlQuery += " AND V.VENDOR_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.VENDOR_ID].ToString() + "'";
                }
            }
            return sqlQuery;
        }

        public static string getSubmitPaymentDtlsReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_AP_PAYMENT V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {

                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
                {
                    sqlQuery += " AND V.pay_date between to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy') ";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
                {
                    sqlQuery += " AND to_date('" + VMVServices.Web.Utils.ReportFilterParameter["To_Date"].ToString() + "','dd/MM/yyyy') ";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Amt"] != null && VMVServices.Web.Utils.ReportViewFilterParameter["To_Amt"] != null)
                {
                    sqlQuery += " AND abs(V.balance) >=to_number(trunc(abs(" + VMVServices.Web.Utils.ReportViewFilterParameter["From_Amt"] + ")))";
                    sqlQuery += " AND abs(V.balance) <=to_number(trunc(abs(" + VMVServices.Web.Utils.ReportViewFilterParameter["To_Amt"] + ")))";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.VENDOR_ID] != null)
                {
                    sqlQuery += " AND V.VENDOR_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.VENDOR_ID].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["PAYMENT_NUMBER"] != null)
                {
                    sqlQuery += " AND V.PAYMENT_NO  = '" + VMVServices.Web.Utils.ReportViewFilterParameter["PAYMENT_NUMBER"].ToString() + "'";
                }
            }
            return sqlQuery;
        }

        public static string getInvoiceListReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM invoice V WHERE ROWNUM > 0 ";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                //if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
                //{
                //    sqlQuery += " AND V.INV_DATE = to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy') ";
                //}
                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null && VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
                {
                    sqlQuery += " AND V.INV_DATE between to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy') ";

                    if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
                    {
                        sqlQuery += " AND to_date('" + VMVServices.Web.Utils.ReportFilterParameter["To_Date"].ToString() + "','dd/MM/yyyy') ";
                    }
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.VENDOR_ID] != null)
                {
                    sqlQuery += " AND V.VENDOR_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.VENDOR_ID].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["FromAmount"] != null && VMVServices.Web.Utils.ReportViewFilterParameter["ToAmount"] != null)
                {
                    sqlQuery += " AND V.inv_item_amt >= " + VMVServices.Web.Utils.ReportViewFilterParameter["FromAmount"];
                    sqlQuery += " AND V.inv_item_amt <= " + VMVServices.Web.Utils.ReportViewFilterParameter["ToAmount"];

                    //sqlQuery += " AND V.inv_item_amt >=to_number(trunc(" + VMVServices.Web.Utils.ReportViewFilterParameter["From_Amt"] + "))";
                    //sqlQuery += " AND V.inv_item_amt <=to_number(trunc(" + VMVServices.Web.Utils.ReportViewFilterParameter["To_Amt"] + "))";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["inv_id"] != null)
                {
                    sqlQuery += " AND V.inv_id ='" + VMVServices.Web.Utils.ReportFilterParameter["inv_id"].ToString() + "'";
                }
            }
            return sqlQuery;
        }

        public static string getInvoiceRegisterReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_AP_INVOICE_LIST V WHERE ROWNUM > 0 ";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                //if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
                //{
                //    sqlQuery += " AND V.INV_DATE = to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy') ";
                //}
                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null && VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
                {
                    sqlQuery += " AND V.INV_DATE between to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy') ";

                    if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
                    {
                        sqlQuery += " AND to_date('" + VMVServices.Web.Utils.ReportFilterParameter["To_Date"].ToString() + "','dd/MM/yyyy') ";
                    }
                }

                if (VMVServices.Web.Utils.ReportViewFilterParameter["FromAmount"] != null && VMVServices.Web.Utils.ReportViewFilterParameter["ToAmount"] != null)
                {
                    sqlQuery += " AND V.INV_AMT >=" + VMVServices.Web.Utils.ReportViewFilterParameter["FromAmount"];
                    sqlQuery += " AND V.INV_AMT <=" + VMVServices.Web.Utils.ReportViewFilterParameter["ToAmount"];
                }

                if (VMVServices.Web.Utils.ReportViewFilterParameter["SUPPLIER_NAME"] != null)
                {
                    sqlQuery += " AND V.SUPPLIER_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["SUPPLIER_NAME"].ToString() + "'";
                }

                if (VMVServices.Web.Utils.ReportViewFilterParameter["InvType"] != null)
                {
                    sqlQuery += " AND V.INV_TYPE = '" + VMVServices.Web.Utils.ReportViewFilterParameter["InvType"].ToString() + "'";
                }

                if (VMVServices.Web.Utils.ReportFilterParameter["From_InvNum"] != null)
                {
                    sqlQuery += " AND V.INV_NUM >=  '" + VMVServices.Web.Utils.ReportFilterParameter["From_InvNum"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["To_InvNum"] != null)
                {
                    sqlQuery += " AND V.INV_NUM <=  '" + VMVServices.Web.Utils.ReportFilterParameter["To_InvNum"].ToString() + "'";
                }

                if (VMVServices.Web.Utils.ReportFilterParameter["POSTEDTYPE"] == "P")
                {
                    sqlQuery += " AND V.POSTED_FLAG =  '1' and v.rev_flag is null";
                }
                else if (VMVServices.Web.Utils.ReportFilterParameter["POSTEDTYPE"] == "U")
                {
                    sqlQuery += " AND (V.POSTED_FLAG IS NULL OR V.POSTED_FLAG='0') and v.rev_flag is null";
                }
                else if (VMVServices.Web.Utils.ReportFilterParameter["POSTEDTYPE"] == "C")
                {
                    sqlQuery += " AND (V.REV_FLAG='1' and v.POSTED_FLAG in ('1','5'))";
                }


            }
            return sqlQuery;
        }
        public static string getChequesReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_PAYABLE_SYSTEM V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {

                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
                {
                    sqlQuery += " AND V.PAY_DATE >= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy')";
                }

                if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
                {
                    sqlQuery += " AND V.PAY_DATE <= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["To_Date"].ToString() + "','dd/MM/yyyy')";
                }



                if (VMVServices.Web.Utils.ReportViewFilterParameter["PAY_ID"] != null)
                {
                    sqlQuery += " AND V.PAY_ID ='" + VMVServices.Web.Utils.ReportFilterParameter["PAY_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Amt"] != null && VMVServices.Web.Utils.ReportViewFilterParameter["To_Amt"] != null)
                {
                    sqlQuery += " AND V.AMOUNT_PAID >=to_number(trunc(" + VMVServices.Web.Utils.ReportViewFilterParameter["From_Amt"] + "))";
                    sqlQuery += " AND V.AMOUNT_PAID <=to_number(trunc(" + VMVServices.Web.Utils.ReportViewFilterParameter["To_Amt"] + "))";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["FROM_CHEQ"] != null && VMVServices.Web.Utils.ReportViewFilterParameter["TO_CHEQ"] != null)
                {
                    sqlQuery += " AND V.CHEQUE_NO >=((" + VMVServices.Web.Utils.ReportViewFilterParameter["FROM_CHEQ"] + "))";
                    sqlQuery += " AND V.CHEQUE_NO <=((" + VMVServices.Web.Utils.ReportViewFilterParameter["TO_CHEQ"] + "))";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["SUPPLIER_ID"] != null)
                {
                    sqlQuery += " AND V.vendor_id ='" + VMVServices.Web.Utils.ReportFilterParameter["SUPPLIER_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["BANK_NAME"] != null)
                {
                    sqlQuery += " AND V.BANK_NAME ='" + VMVServices.Web.Utils.ReportFilterParameter["BANK_NAME"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["CURRENCY"] != null)
                {
                    sqlQuery += " AND V.PAYMENT_CURRENCY ='" + VMVServices.Web.Utils.ReportFilterParameter["CURRENCY"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["ACCOUNT_ID"] != null)
                {
                    sqlQuery += " AND V.BANK_ACCOUNT_NUMBER ='" + VMVServices.Web.Utils.ReportFilterParameter["ACCOUNT_ID"].ToString() + "'";
                }

                if (VMVServices.Web.Utils.ReportViewFilterParameter["PAYMENT_TYPE"] != null)
                {
                    sqlQuery += " AND V.PAYMENT_MODE ='" + VMVServices.Web.Utils.ReportFilterParameter["PAYMENT_TYPE"].ToString() + "'";
                }

                if (VMVServices.Web.Utils.ReportViewFilterParameter["CURRENCY_ID"] != null)
                {
                    sqlQuery += " AND V.PAYMENT_CURRENCY ='" + VMVServices.Web.Utils.ReportFilterParameter["CURRENCY_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["PAYMENT_NUMBER"] != null)
                {
                    sqlQuery += " AND V.PAY_ID ='" + VMVServices.Web.Utils.ReportFilterParameter["PAYMENT_NUMBER"].ToString() + "'";
                }


            }
            return sqlQuery;
        }
        public static string getChequesReportDataPaymnetReg()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_PAYABLE_SYSTEM V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {

                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
                {
                    sqlQuery += " AND V.PAY_DATE >= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy')";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
                {
                    sqlQuery += " AND V.PAY_DATE <= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["To_Date"].ToString() + "','dd/MM/yyyy')";
                }

                if (VMVServices.Web.Utils.ReportViewFilterParameter["PAY_ID"] != null)
                {
                    sqlQuery += " AND V.PAY_ID ='" + VMVServices.Web.Utils.ReportFilterParameter["PAY_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Amt"] != null && VMVServices.Web.Utils.ReportViewFilterParameter["To_Amt"] != null)
                {
                    sqlQuery += " AND V.AMOUNT_PAID >=to_number(trunc(" + VMVServices.Web.Utils.ReportViewFilterParameter["From_Amt"] + "))";
                    sqlQuery += " AND V.AMOUNT_PAID <=to_number(trunc(" + VMVServices.Web.Utils.ReportViewFilterParameter["To_Amt"] + "))";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["FROM_CHEQ"] != null && VMVServices.Web.Utils.ReportViewFilterParameter["TO_CHEQ"] != null)
                {
                    sqlQuery += " AND V.CHEQUE_NO >=((" + VMVServices.Web.Utils.ReportViewFilterParameter["FROM_CHEQ"] + "))";
                    sqlQuery += " AND V.CHEQUE_NO <=((" + VMVServices.Web.Utils.ReportViewFilterParameter["TO_CHEQ"] + "))";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["SUPPLIER_ID"] != null)
                {
                    sqlQuery += " AND V.vendor_id ='" + VMVServices.Web.Utils.ReportFilterParameter["SUPPLIER_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["BANK_NAME"] != null)
                {
                    sqlQuery += " AND V.BANK_NAME ='" + VMVServices.Web.Utils.ReportFilterParameter["BANK_NAME"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["CURRENCY"] != null)
                {
                    sqlQuery += " AND V.PAYMENT_CURRENCY ='" + VMVServices.Web.Utils.ReportFilterParameter["CURRENCY"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["ACCOUNT_ID"] != null)
                {
                    sqlQuery += " AND V.BANK_ACCOUNT_NUMBER ='" + VMVServices.Web.Utils.ReportFilterParameter["ACCOUNT_ID"].ToString() + "'";
                }

                if (VMVServices.Web.Utils.ReportViewFilterParameter["PAYMENT_TYPE"] != null)
                {
                    sqlQuery += " AND V.PAYMENT_MODE ='" + VMVServices.Web.Utils.ReportFilterParameter["PAYMENT_TYPE"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["PAYMENT_NUMBER"] != null)
                {
                    sqlQuery += " AND V.PAY_ID ='" + VMVServices.Web.Utils.ReportFilterParameter["PAYMENT_NUMBER"].ToString() + "'";
                }

                if (VMVServices.Web.Utils.ReportViewFilterParameter["INVOICE_NUMBER"] != null)
                {
                    sqlQuery += " AND V.INV_NUM ='" + VMVServices.Web.Utils.ReportFilterParameter["INVOICE_NUMBER"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["POSTEDTYPE"] == "P")
                {
                    sqlQuery += " AND V.POSTED_FLAG =  '1'  and v.rev_flag is null";
                }
                else if (VMVServices.Web.Utils.ReportFilterParameter["POSTEDTYPE"] == "U")
                {
                    sqlQuery += " AND (V.POSTED_FLAG IS NULL OR V.POSTED_FLAG='0')  and v.rev_flag is null";
                }
                else if (VMVServices.Web.Utils.ReportFilterParameter["POSTEDTYPE"] == "C")
                {
                    sqlQuery += " AND (V.REV_FLAG='1' and v.POSTED_FLAG in ('1','5'))";
                }

            }
            return sqlQuery;
        }


        public static string getStatementofAccountReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT v.inv_pk_id as invpk_id,v.debit as debit_amt,v.* FROM VW_INVOICE_PAYMENT_SUMMARY V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["VENDOR_ID"] != null)
                {
                    sqlQuery += " AND V.VENDOR_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["VENDOR_ID"].ToString() + "'";
                }

                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
                {
                    sqlQuery += " AND V.inv_date between to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy') ";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
                {
                    sqlQuery += " AND to_date('" + VMVServices.Web.Utils.ReportFilterParameter["To_Date"].ToString() + "','dd/MM/yyyy') ";
                }
            }
            sqlQuery += "            union ";
            sqlQuery += "  SELECT  v.inv_pk_id as invpk_id,v.debit as debit_amt,v.* ";
            sqlQuery += "  FROM VW_INVOICE_PAYMENT_SUMMARY V";
            sqlQuery += "   WHERE ROWNUM > 0";
            sqlQuery += "   AND v.OPEN_BAL_FLAG is not null ";

            sqlQuery += "      order by invpk_id,debit_amt desc";
            return sqlQuery;
        }
        public static string getGlobalSegment()
        {
            sqlQuery = string.Empty;
            sqlQuery += " SELECT SV.SEGMENT_VALUE_ID,SV.SEGMENT_VALUE" + VMVServices.Web.Utils.LanguageCode;
            sqlQuery += " FROM GL_SEGMENT_VALUES SV ";
            sqlQuery += " WHERE SV.WORKFLOW_COMPLETION_STATUS='1' ";
            sqlQuery += " AND SV.ENABLED_FLAG='1'";


            return sqlQuery;
        }
    }
}
