﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.AP
{
    public static class PurchaseOrderReq_DAL
    {
        static string sqlQuery = "";

        public static string GetPurchaseReqDetails(string pOReqId)
        {
            sqlQuery = string.Empty;

            sqlQuery = "  select rh.PO_REQ_ID,       rh.ORG_ID,";
            sqlQuery += "  rh.DEPT_ID,       rh.PO_REQ_AMT,   ";
            sqlQuery += "  rh.PO_REQ_TYPE,       rh.PO_REQ_DESC,";
            sqlQuery += "  rh.PO_REQ_DT,       rh.ITEM_SERVICE,";
            sqlQuery += "  rh.ENABLED_FLAG,       rh.WORKFLOW_COMPLETION_STATUS,";
            sqlQuery += "   rd. PO_REQ_LINE_ID,       ";
            sqlQuery += "   rd.PO_REQ_LINE_NUM,       rd.ITEM_ID,";
            sqlQuery += "   (select im.item_name" + VMVServices.Web.Utils.LanguageCode + "  From inv_item_master im where rd.item_id=im.item_id) as item_name,";
            sqlQuery += "   (select um.uom_code" + VMVServices.Web.Utils.LanguageCode + "  From inv_uom_master um where rd.po_req_uom=um.uom_id) as uom_code,";
            sqlQuery += "   (select um.uom_id From inv_uom_master um where rd.po_req_uom=um.uom_id) as uom_id,";
            sqlQuery += "    rd.PO_REQ_UOM,       rd.PO_DESCRIPTION,";
            sqlQuery += "   to_char(rd.PO_QUANTITY) as PO_QUANTITY,to_char(rd.PO_ITEM_UNIT_PRICE) as PO_ITEM_UNIT_PRICE,to_char((nvl(rd.PO_QUANTITY,0) * nvl(rd.PO_ITEM_UNIT_PRICE,0))) as PO_Amount,'0' AS DELETED";
            sqlQuery += "  from po_requisition_hdr rh, po_requisition_dtl rd";
            sqlQuery += "  where rh.po_req_id = rd.po_req_id";
            sqlQuery += " AND rh.PO_REQ_ID ='" + pOReqId + "'";
            sqlQuery += " order by PO_REQ_LINE_ID ";
            return sqlQuery;
        }

        public static string getReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM INV_PO_REQUISITION_R V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["PO_REQ_ID"] != null)
                {
                    sqlQuery += " AND V.PO_REQ_NUMBER = '" + VMVServices.Web.Utils.ReportViewFilterParameter["PO_REQ_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["DEPT_ID"] != null)
                {
                    sqlQuery += " AND V.DEPT_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["DEPT_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["ITEM_ID"] != null)
                {
                    sqlQuery += " AND V.ITEM_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["ITEM_ID"].ToString() + "'";
                }
            }
            return sqlQuery;
        }
        public static string getRequisitionNumber()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT  RH.PO_REQ_ID  ";
            sqlQuery += " FROM PO_REQUISITION_HDR RH ";
            sqlQuery += " WHERE RH.WORKFLOW_COMPLETION_STATUS='1' ";
            sqlQuery += "  and rh.org_id='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " AND RH.ENABLED_FLAG='1' ";
            return sqlQuery;
        }

        public static string getPOReqNumber()
        {
            sqlQuery = string.Empty;
            sqlQuery = "  SELECT  RH.PO_REQ_ID,RH.PO_REQ_ID||' - '||RH.PO_REQ_DESC AS PO_REQ_DESC  ";
            sqlQuery += "  FROM PO_REQUISITION_HDR RH ";
            sqlQuery += "  WHERE RH.WORKFLOW_COMPLETION_STATUS='1' ";
            sqlQuery += "  and rh.org_id='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += "   and rh.po_req_id not in";
            sqlQuery += "   (select pr.tmp_req_id";
            sqlQuery += "    from po_tmp_rfq_req_mapping pr, po_tmp_rfq_hdr ph";
            sqlQuery += "   where pr.workflow_completion_status = 1";
            sqlQuery += "   and ph.tmp_rfq_org_id = '" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += "   and ph.tmp_rfq_id = pr.tmp_rfq_id and upper(ph.tmp_rfq_status)='APPROVED')";
            //sqlQuery += "   and rh.po_req_id not in";
            //sqlQuery += "     (select distinct pd.po_req_id";
            //sqlQuery += "     from inv_receipt_dtls ir, po_requisition_dtl pd";
            //sqlQuery += "   where ir.item_id = pd.item_id";          
            //sqlQuery += "  )";
            sqlQuery += "  AND RH.ENABLED_FLAG='1' ";
            return sqlQuery;
        }
        public static string getPOReqData()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT  RH.PO_REQ_ID,RH.PO_REQ_ID||' - '||RH.PO_REQ_DESC||' - '||RH.PO_REQ_DT AS PO_REQ_DESC  ";
            sqlQuery += " FROM PO_REQUISITION_HDR RH ";
            sqlQuery += " WHERE RH.WORKFLOW_COMPLETION_STATUS='1' ";
            sqlQuery += " AND RH.ENABLED_FLAG='1' ";
            sqlQuery += "  and rh.org_id='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += "   order by PO_REQ_ID desc";
            return sqlQuery;
        }
        public static string getPOReqItemData(string reqId, string itemId = "")
        {
            sqlQuery += "   select rd.item_id,";
            sqlQuery += "   (rd.po_quantity),";
            sqlQuery += "   (select im.INV_ITEM_TYPE";
            sqlQuery += "     From inv_item_master im";
            sqlQuery += "    where rd.item_id = im.item_id) as INV_ITEM_TYPE,";
            sqlQuery += "   (select im.item_name" + VMVServices.Web.Utils.LanguageCode + " ";
            sqlQuery += "   From inv_item_master im";
            sqlQuery += "  where rd.item_id = im.item_id) as item_name,";
            sqlQuery += "  rd.po_req_uom as uom_code,";
            sqlQuery += "   rd.po_item_unit_price";
            sqlQuery += "  from po_requisition_hdr rh, po_requisition_dtl rd";
            sqlQuery += "  where rh.po_req_id = rd.po_req_id";
            sqlQuery += "  and rh.workflow_completion_status = 1";
            sqlQuery += "  and rh.enabled_flag = 1";
            sqlQuery += " and rd.item_id = '" + itemId + "'";
            sqlQuery += "  and rh.po_req_id in ('" + reqId + "')";
            sqlQuery += "  and rh.org_id='" + VMVServices.Web.Utils.OrganizationID + "'";
            return sqlQuery;
        }
    }
}
