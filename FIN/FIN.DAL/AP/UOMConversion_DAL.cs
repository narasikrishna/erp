﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.AP
{
    public class UOMConversion_DAL
    {

        static string sqlQuery = "";
        public static string getUOMConversationFrom()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT IUM.UOM_CODE AS UOM_CODE, IUM.UOM_CODE ||' - '|| IUM.UOM_DESC AS UOM_NAME ";
            sqlQuery += " FROM INV_UOM_MASTER IUM ";
            sqlQuery += " WHERE IUM.ENABLED_FLAG = 1 ";
            sqlQuery += " AND IUM.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += "   and IUM.org_id='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " ORDER BY UOM_CODE ";

            return sqlQuery;
        }

        public static string getUOMConversationTo()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT IUM.UOM_CODE AS UOM_CODE, IUM.UOM_CODE ||' - '|| IUM.UOM_DESC AS UOM_NAME ";
            sqlQuery += " FROM INV_UOM_MASTER IUM ";
            sqlQuery += " WHERE IUM.ENABLED_FLAG = 1 ";
            sqlQuery += " AND IUM.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += "   and IUM.org_id='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " ORDER BY UOM_CODE ";

            return sqlQuery;
        }
        public static string getUOMConversionReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery = "SELECT * FROM vw_ap_uom_conversion V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {

                if (VMVServices.Web.Utils.ReportViewFilterParameter["UOM_FROM_ID"] != null)
                {
                    sqlQuery += " AND V.UOM_FROM = '" + VMVServices.Web.Utils.ReportViewFilterParameter["UOM_FROM_ID"].ToString() + "'";
                }

                if (VMVServices.Web.Utils.ReportViewFilterParameter["UOM_TO_ID"] != null)
                {
                    sqlQuery += " AND V.UOM_TO = '" + VMVServices.Web.Utils.ReportViewFilterParameter["UOM_TO_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["CREATED_BY"] != null)
                {
                    sqlQuery += " AND UPPER(V.CREATED_BY) = UPPER('" + VMVServices.Web.Utils.ReportViewFilterParameter["CREATED_BY"].ToString() + "')";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["MODIFIED_BY"] != null)
                {
                    sqlQuery += " AND UPPER(V.MODIFIED_BY) = UPPER('" + VMVServices.Web.Utils.ReportViewFilterParameter["MODIFIED_BY"].ToString() + "')";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["CREATED_From_Date"] != null)
                {
                    sqlQuery += " AND V.CREATED_DATE >= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["CREATED_From_Date"].ToString() + "','dd/MM/yyyy') ";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["CREATED_To_Date"] != null)
                {
                    sqlQuery += " AND V.CREATED_DATE <= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["CREATED_To_Date"].ToString() + "','dd/MM/yyyy') ";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["MODIFIED_From_Date"] != null)
                {
                    sqlQuery += " AND V.MODIFIED_DATE >= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["MODIFIED_From_Date"].ToString() + "','dd/MM/yyyy') ";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["MODIFIED_To_Date"] != null)
                {
                    sqlQuery += " AND V.MODIFIED_DATE <= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["MODIFIED_To_Date"].ToString() + "','dd/MM/yyyy') ";
                }
                
            }
            

            return sqlQuery;
        }


    }
}
