﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.AP
{
    public class SupplierBranch_DAL
    {
        static string sqlQuery = "";

        public static string getSupplierSite4SupplierName(string str_SupplierId="")
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT SCB.VENDOR_LOC_ID,SCB.VENDOR_BRANCH_CODE" + VMVServices.Web.Utils.LanguageCode + " as VENDOR_BRANCH_CODE  FROM SUPPLIER_CUSTOMER_BRANCH SCB ";
            sqlQuery += " WHERE SCB.ENABLED_FLAG=1 AND SCB.WORKFLOW_COMPLETION_STATUS=1 ";
            if (VMVServices.Web.Utils.LanguageCode != string.Empty)
            {
                sqlQuery += " AND  SCB.VENDOR_BRANCH_CODE" + VMVServices.Web.Utils.LanguageCode + "  is not null ";
            }
            if (str_SupplierId != "")
            {
                sqlQuery += " and SCB.VENDOR_ID='" + str_SupplierId + "'";
            }

            return sqlQuery;

        }

        public static string GetSupplierNameFromBranch()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT DISTINCT SC.VENDOR_ID, SC.VENDOR_CODE || ' - ' || SC.VENDOR_NAME AS VEND_NAME, SC.VENDOR_CODE";
            sqlQuery += " FROM SUPPLIER_CUSTOMER_BRANCH SCB, SUPPLIER_CUSTOMERS SC";
            sqlQuery += " WHERE SC.VENDOR_ID=SCB.VENDOR_ID";
            sqlQuery += " AND SCB.ENABLED_FLAG = 1";
            sqlQuery += " AND SCB.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND UPPER(SC.VENDOR_TYPE) IN ('SUPPLIER','BOTH')";

            return sqlQuery;
        }

        public static string getSupplierSiteReportData(bool customer)
        {
            sqlQuery = string.Empty;
            sqlQuery = "SELECT * FROM VW_AP_SUPPLIER_SITE V WHERE ROWNUM > 0 ";
            if (customer)
            {
                sqlQuery += "  AND UPPER(V.VENDOR_TYPE) IN ('CUSTOMER','BOTH')";
            }
            else
            {
                sqlQuery += "  AND UPPER(V.VENDOR_TYPE) IN ('SUPPLIER','BOTH')";
            }

            if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Customer_Id"] != null)
            {
                sqlQuery += " AND V.VENDOR_ID >= '" + VMVServices.Web.Utils.ReportViewFilterParameter["From_Customer_Id"].ToString() + "'";
            }
            if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Customer_Id"] != null)
            {
                sqlQuery += " AND V.VENDOR_ID <= '" + VMVServices.Web.Utils.ReportViewFilterParameter["To_Customer_Id"].ToString() + "'";
            }
            if (VMVServices.Web.Utils.ReportViewFilterParameter["VendorLOCBranchID"] != null)
            {
                sqlQuery += " AND V.VENDOR_LOC_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["VendorLOCBranchID"].ToString() + "'";
            }
            if (VMVServices.Web.Utils.ReportViewFilterParameter["CREATED_BY"] != null)
            {
                sqlQuery += " AND UPPER(V.CREATED_BY) = UPPER('" + VMVServices.Web.Utils.ReportViewFilterParameter["CREATED_BY"].ToString() + "')";
            }
            if (VMVServices.Web.Utils.ReportViewFilterParameter["MODIFIED_BY"] != null)
            {
                sqlQuery += " AND UPPER(V.MODIFIED_BY) = UPPER('" + VMVServices.Web.Utils.ReportViewFilterParameter["MODIFIED_BY"].ToString() + "')";
            }
            if (VMVServices.Web.Utils.ReportViewFilterParameter["CREATED_From_Date"] != null)
            {
                sqlQuery += " AND V.CREATED_DATE >= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["CREATED_From_Date"].ToString() + "','dd/MM/yyyy') ";
            }
            if (VMVServices.Web.Utils.ReportViewFilterParameter["CREATED_To_Date"] != null)
            {
                sqlQuery += " AND V.CREATED_DATE <= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["CREATED_To_Date"].ToString() + "','dd/MM/yyyy') ";
            }
            if (VMVServices.Web.Utils.ReportViewFilterParameter["MODIFIED_From_Date"] != null)
            {
                sqlQuery += " AND V.MODIFIED_DATE >= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["MODIFIED_From_Date"].ToString() + "','dd/MM/yyyy') ";
            }
            if (VMVServices.Web.Utils.ReportViewFilterParameter["MODIFIED_To_Date"] != null)
            {
                sqlQuery += " AND V.MODIFIED_DATE <= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["MODIFIED_To_Date"].ToString() + "','dd/MM/yyyy') ";
            }


            return sqlQuery;
        }
    }
}
