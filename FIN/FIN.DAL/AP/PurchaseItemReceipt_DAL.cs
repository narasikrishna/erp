﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.AP
{
    public class PurchaseItemReceipt_DAL
    {
        static string sqlQuery = "";

        public static string GetReceiptDetails(string receiptId)
        {
            sqlQuery = string.Empty;
            sqlQuery = "  select ih.GRN_NUM,ih.RECEIPT_DATE,ih.receipt_id,";
            sqlQuery += "  ir.receipt_dtl_id,";
            sqlQuery += "  ir.line_num, (select l.po_quantity";
            sqlQuery += "  From inv_item_master im, po_lines l";
            sqlQuery += "  where ir.item_id = im.item_id";
            sqlQuery += "  and l.po_header_id = ir.po_header_id";
            sqlQuery += "  and l.po_item_id = im.item_id) as po_quantity,";
            sqlQuery += "    (select im.item_name" + VMVServices.Web.Utils.LanguageCode + " ";
            sqlQuery += "    From inv_item_master im";
            sqlQuery += "     where ir.item_id = im.item_id) as item_name,";
            sqlQuery += "     (ir.po_header_id || ' - ' || ir.line_num || ' - ' ||";
            sqlQuery += "     (select im.item_name" + VMVServices.Web.Utils.LanguageCode + "  || ' - ' || l.po_quantity";
            sqlQuery += "     From inv_item_master im, po_lines l";
            sqlQuery += "     where ir.item_id = im.item_id and l.po_header_id=ir.po_header_id";
            sqlQuery += "     and l.po_item_id = im.item_id)) as po_data,";
            sqlQuery += "       (select l.po_line_num";
            sqlQuery += "     From inv_item_master im, po_lines l";
            sqlQuery += "     where ir.item_id = im.item_id and l.po_header_id=ir.po_header_id";
            sqlQuery += "    and l.po_item_id = im.item_id) as po_line_num,";
            sqlQuery += "     ir.po_header_id,";
            //ir.po_header_id as PO_NUM,";
            sqlQuery += "     ((select pp.po_num from po_headers pp where pp.po_header_id= ir.po_header_id) || ' - ' || ir.line_num || ' - ' ||";
            sqlQuery += "     (select im.item_name" + VMVServices.Web.Utils.LanguageCode + "  || ' - ' || l.po_quantity";
            sqlQuery += "     From inv_item_master im, po_lines l";
            sqlQuery += "     where ir.item_id = im.item_id and l.po_header_id=ir.po_header_id";
            sqlQuery += "     and l.po_item_id = im.item_id)) as PO_NUM,";
            sqlQuery += "     ir.po_line_id,";
            sqlQuery += "     ir.item_id ,";
            sqlQuery += "     ir.qty_received,";
            sqlQuery += "    ir.qty_rejected,ir.QTY_APPROVED,ir.QTY_APPROVED as QTY_ALREADY_APPROVED,";
            sqlQuery += "    ir.inv_description as REMARKS,'0' as DELETED";
            sqlQuery += "    from inv_receipt_dtls ir, inv_receipts_hdr ih";
            sqlQuery += "    where ir.receipt_id = ih.receipt_id";
            sqlQuery += "    and ir.receipt_id = '" + receiptId + "'";
            sqlQuery += " order by RECEIPT_DTL_ID ";
            return sqlQuery;
        }


        public static string getGRNNo()
        {
            sqlQuery = string.Empty;

            sqlQuery = " select rh.receipt_id,rh.grn_num";
            sqlQuery += " from inv_receipts_hdr rh";
            sqlQuery += " where rh.enabled_flag = 1";
            sqlQuery += " and rh.workflow_completion_status = 1";
            sqlQuery += " and rh.ORG_ID= '" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " order by rh.receipt_id";


            return sqlQuery;
        }
        public static string GetPOItemLineBasedLot(string receiptId)
        {
            sqlQuery = string.Empty;

            string recFlag = string.Empty;
            sqlQuery = " select rr.receipt_flag from inv_receipts_hdr rr where rr.receipt_id='" + receiptId + "'";

            recFlag = DBMethod.GetStringValue(sqlQuery);

            sqlQuery = string.Empty;

            if (recFlag == "Micellaneous")
            {
                sqlQuery += "    select distinct ir.receipt_dtl_id,";
                sqlQuery += "    ih.receipt_date,";
                sqlQuery += "    ir.inv_description,";
                sqlQuery += "    ir.item_id,";
                sqlQuery += "    0 as PO_QUANTITY,";
                sqlQuery += "    0 as po_unit_price,";
                sqlQuery += "    0 as po_amount,";
                sqlQuery += "    ir.line_num,";
                sqlQuery += "    '' as po_num,";
                sqlQuery += "    '' as po_line_num,";
                sqlQuery += "    (case";
                sqlQuery += "   when (select im.item_name" + VMVServices.Web.Utils.LanguageCode + " ";
                sqlQuery += "        From inv_item_master im";
                sqlQuery += "        where im.item_id = ir.item_id) is null then";
                sqlQuery += "    (select v.Code";
                sqlQuery += "       from ssm_code_masters v";
                sqlQuery += "       where v.Parent_Code = 'SRTYP'";
                sqlQuery += "         and v.Code = ir.item_id)";
                sqlQuery += "     else";
                sqlQuery += "      (select im.item_name" + VMVServices.Web.Utils.LanguageCode + " ";
                sqlQuery += "         From inv_item_master im";
                sqlQuery += "        where im.item_id = ir.item_id)";
                sqlQuery += "    end) as item_service,";
                sqlQuery += "   (ir.line_num || ' - ' || ir.receipt_dtl_id || ' - ' || (case";
                sqlQuery += "    when (select im.item_name" + VMVServices.Web.Utils.LanguageCode + " ";
                sqlQuery += "           From inv_item_master im";
                sqlQuery += "          where im.item_id = ir.item_id) is null then";
                sqlQuery += "    (select v.Code";
                sqlQuery += "      from ssm_code_masters v";
                sqlQuery += "     where v.Parent_Code = 'SRTYP'";
                sqlQuery += "        and v.Code = ir.item_id)";
                sqlQuery += "   else";
                sqlQuery += "    (select im.item_name" + VMVServices.Web.Utils.LanguageCode + " ";
                sqlQuery += "     From inv_item_master im";
                sqlQuery += "    where im.item_id = ir.item_id)";
                sqlQuery += "   end) || ' - ' || ir.qty_approved) as po_data";
                sqlQuery += "    from inv_receipt_dtls ir, inv_receipts_hdr ih, inv_receipt_lots_hdr rl";
                sqlQuery += "   where ih.receipt_id = ir.receipt_id";
                sqlQuery += "   and ir.item_id = rl.item_id";
                sqlQuery += "   and rl.receipt_id = ih.receipt_id";
                sqlQuery += "   and ir.receipt_id = rl.receipt_id";
                sqlQuery += "   and rl.receipt_dtl_id = ir.receipt_dtl_id";
                sqlQuery += "    and ir.receipt_id = '" + receiptId + "'";
                sqlQuery += "  and ir.enabled_flag = 1";
                sqlQuery += "  and ir.workflow_completion_status = 1";
                sqlQuery += "   and ih.ORG_ID = '" + VMVServices.Web.Utils.OrganizationID + "'";
                sqlQuery += "   order by ir.receipt_dtl_id";

            }
            else
            {

                sqlQuery = "  select distinct ir.receipt_dtl_id,ih.receipt_date,ir.inv_description,ir.item_id,l.PO_QUANTITY,l.po_unit_price,ph.po_amount,";
                sqlQuery += "   ir.line_num,";
                sqlQuery += "   ph.po_num,";
                sqlQuery += "   l.po_line_num,";
                sqlQuery += "   (case";
                sqlQuery += "   when (select im.item_name" + VMVServices.Web.Utils.LanguageCode + " ";
                sqlQuery += "   From inv_item_master im";
                sqlQuery += "   where im.item_id = ir.item_id) is null then";
                sqlQuery += "   (select v.Code";
                sqlQuery += "  from ssm_code_masters v";
                sqlQuery += "  where v.Parent_Code = 'SRTYP'";
                sqlQuery += "  and v.Code = ir.item_id)";
                sqlQuery += "      else";
                sqlQuery += "     (select im.item_name";
                sqlQuery += "       From inv_item_master im";
                sqlQuery += "      where im.item_id = ir.item_id)";
                sqlQuery += "    end) as item_service,";
                sqlQuery += "     (ir.line_num || ' - ' || ir.receipt_dtl_id || ' - ' || (case";
                sqlQuery += "      when (select im.item_name";
                sqlQuery += "         From inv_item_master im";
                sqlQuery += "         where im.item_id = ir.item_id) is null then";
                sqlQuery += "   (select v.Code";
                sqlQuery += "  from ssm_code_masters v";
                sqlQuery += "  where v.Parent_Code = 'SRTYP'";
                sqlQuery += "  and v.Code = ir.item_id)";
                sqlQuery += "     else";
                sqlQuery += "    (select im.item_name";
                sqlQuery += "      From inv_item_master im";
                sqlQuery += "     where im.item_id = ir.item_id )";
                sqlQuery += "     end) || ' - ' || l.po_quantity) as po_data";
                sqlQuery += "    from inv_receipt_dtls ir, po_headers ph, po_lines l,inv_receipts_hdr ih,inv_receipt_lots_hdr rl";
                sqlQuery += "    where ir.po_header_id = ph.po_header_id";
                sqlQuery += "     and l.po_header_id = ph.po_header_id and ih.receipt_id=ir.receipt_id  and ir.item_id=l.po_item_id ";
                sqlQuery += "        and ir.po_line_id = l.po_line_id  ";
                sqlQuery += "   and ir.item_id = rl.item_id";
                sqlQuery += "    and rl.receipt_id = ih.receipt_id";
                sqlQuery += "    and ir.receipt_id = rl.receipt_id";
                sqlQuery += "    and rl.receipt_dtl_id = ir.receipt_dtl_id";
                sqlQuery += "    and ir.receipt_id = '" + receiptId + "'";
                sqlQuery += "  and  ir.enabled_flag = 1";
                sqlQuery += "  and ir.workflow_completion_status = 1";
                sqlQuery += "  and ih.ORG_ID= '" + VMVServices.Web.Utils.OrganizationID + "'";
                sqlQuery += "  order by ir.receipt_dtl_id";

            }
            return sqlQuery;
        }
        public static string GetPOItemLine(string receiptId)
        {
            sqlQuery = string.Empty;

            string recFlag = string.Empty;
            sqlQuery = " select rr.receipt_flag from inv_receipts_hdr rr where rr.receipt_id='" + receiptId + "'";

            recFlag = DBMethod.GetStringValue(sqlQuery);

            sqlQuery = string.Empty;

            if (recFlag == "Micellaneous")
            {
                sqlQuery = "   select ih.receipt_flag,";
                sqlQuery += "  ir.receipt_dtl_id,";
                sqlQuery += "   ih.receipt_date,";
                sqlQuery += "   ir.inv_description,";
                sqlQuery += "   ir.item_id,";
                sqlQuery += "   ir.qty_approved as PO_QUANTITY,";
                sqlQuery += "   0 as po_unit_price,";
                sqlQuery += "   0 as po_amount,";
                sqlQuery += "   ir.line_num,";
                sqlQuery += "   '' as po_num,";
                sqlQuery += "   '' as po_line_num,";
                sqlQuery += "    (case";
                sqlQuery += "  when (select im.item_name" + VMVServices.Web.Utils.LanguageCode + " ";
                sqlQuery += "  From inv_item_master im";
                sqlQuery += "  where im.item_id = ir.item_id) is null then";
                sqlQuery += "  (select v.Code";
                sqlQuery += "  from ssm_code_masters v";
                sqlQuery += "  where v.Parent_Code = 'SRTYP'";
                sqlQuery += "  and v.Code = ir.item_id)";
                sqlQuery += "  else";
                sqlQuery += "  (select im.item_name" + VMVServices.Web.Utils.LanguageCode + " ";
                sqlQuery += "  From inv_item_master im";
                sqlQuery += "  where im.item_id = ir.item_id)";
                sqlQuery += "  end) as item_service,";
                sqlQuery += "  (ir.line_num || ' - ' || ir.receipt_dtl_id || ' - ' || (case";
                sqlQuery += "  when (select im.item_name" + VMVServices.Web.Utils.LanguageCode + " ";
                sqlQuery += "  From inv_item_master im";
                sqlQuery += "  where im.item_id = ir.item_id) is null then";
                sqlQuery += "  (select v.Code";
                sqlQuery += "  from ssm_code_masters v";
                sqlQuery += "  where v.Parent_Code = 'SRTYP'";
                sqlQuery += "  and v.Code = ir.item_id)";
                sqlQuery += "  else";
                sqlQuery += "  (select im.item_name";
                sqlQuery += "  From inv_item_master im";
                sqlQuery += "  where im.item_id = ir.item_id)";
                sqlQuery += "  end)|| ' - '||ir.qty_approved) as po_data";
                sqlQuery += "  from inv_receipt_dtls ir, inv_receipts_hdr ih";
                sqlQuery += "  where ih.receipt_id = ir.receipt_id";
                sqlQuery += "  and ir.receipt_id = '" + receiptId + "'";
                sqlQuery += "  and ir.enabled_flag = 1 and ir.qty_approved>0 ";
                sqlQuery += "  and ir.workflow_completion_status = 1";
                sqlQuery += "  and ih.ORG_ID = '" + VMVServices.Web.Utils.OrganizationID + "'";
                sqlQuery += "     and ih.receipt_flag='Micellaneous'";
                sqlQuery += "   order by ir.receipt_dtl_id";

            }
            else
            {

                sqlQuery = "  select ir.receipt_dtl_id,ih.receipt_date,ir.inv_description,ir.item_id,l.PO_QUANTITY,l.po_unit_price,ph.po_amount,";
                sqlQuery += "   ir.line_num,";
                sqlQuery += "   ph.po_num,";
                sqlQuery += "   l.po_line_num,";
                sqlQuery += "   (case";
                sqlQuery += "   when (select im.item_name" + VMVServices.Web.Utils.LanguageCode + " ";
                sqlQuery += "   From inv_item_master im";
                sqlQuery += "   where im.item_id = ir.item_id) is null then";
                sqlQuery += "   (select v.Code";
                sqlQuery += "  from ssm_code_masters v";
                sqlQuery += "  where v.Parent_Code = 'SRTYP'";
                sqlQuery += "  and v.Code = ir.item_id)";
                sqlQuery += "      else";
                sqlQuery += "     (select im.item_name" + VMVServices.Web.Utils.LanguageCode + " ";
                sqlQuery += "       From inv_item_master im";
                sqlQuery += "      where im.item_id = ir.item_id)";
                sqlQuery += "    end) as item_service,";
                sqlQuery += "     (ir.line_num || ' - ' || ir.receipt_dtl_id || ' - ' || (case";
                sqlQuery += "      when (select im.item_name";
                sqlQuery += "         From inv_item_master im";
                sqlQuery += "         where im.item_id = ir.item_id) is null then";
                sqlQuery += "   (select v.Code";
                sqlQuery += "  from ssm_code_masters v";
                sqlQuery += "  where v.Parent_Code = 'SRTYP'";
                sqlQuery += "  and v.Code = ir.item_id)";
                sqlQuery += "     else";
                sqlQuery += "    (select im.item_name" + VMVServices.Web.Utils.LanguageCode + " ";
                sqlQuery += "      From inv_item_master im";
                sqlQuery += "     where im.item_id = ir.item_id )";
                sqlQuery += "     end) || ' - ' || l.po_quantity) as po_data";
                sqlQuery += "    from inv_receipt_dtls ir, po_headers ph, po_lines l,inv_receipts_hdr ih";
                sqlQuery += "    where ir.po_header_id = ph.po_header_id";
                sqlQuery += "     and l.po_header_id = ph.po_header_id and ih.receipt_id=ir.receipt_id  and ir.item_id=l.po_item_id ";
                sqlQuery += "    and ir.receipt_id = '" + receiptId + "'";
                sqlQuery += "  and  ir.enabled_flag = 1 and ir.qty_approved>0 ";
                sqlQuery += "  and ir.workflow_completion_status = 1";
                sqlQuery += "  and ih.ORG_ID= '" + VMVServices.Web.Utils.OrganizationID + "'";
                sqlQuery += "  order by ir.receipt_dtl_id";
            }

            return sqlQuery;
        }
        public static string GetPOItemLineDetails(string receiptId, string receiptDtlId)
        {
            sqlQuery = string.Empty;

            string recFlag = string.Empty;

            sqlQuery = " select rr.receipt_flag from inv_receipts_hdr rr where rr.receipt_id='" + receiptId + "'";

            recFlag = DBMethod.GetStringValue(sqlQuery);

            sqlQuery = string.Empty;

            if (recFlag == "Micellaneous")
            {
                sqlQuery = "    select ir.receipt_dtl_id,";
                sqlQuery += "     ih.receipt_date,";
                sqlQuery += "     im.item_name  as inv_description,";
                sqlQuery += "     ir.item_id,";
                sqlQuery += "     ir.qty_received as PO_QUANTITY,";
                sqlQuery += "     0 as po_unit_price,";
                sqlQuery += "     0 as po_amount,";
                sqlQuery += "     ir.po_header_id,";
                sqlQuery += "    ir.line_num,";
                sqlQuery += "     '' as PO_LINE_ID,";
                sqlQuery += "     '' as po_num,";
                sqlQuery += "    '' as po_line_num,";
                sqlQuery += "      (case";
                sqlQuery += "     when (select im.item_name" + VMVServices.Web.Utils.LanguageCode + " ";
                sqlQuery += "        From inv_item_master im";
                sqlQuery += "      where im.item_id = ir.item_id) is null then";
                sqlQuery += "        (select v.code";
                sqlQuery += "          from ssm_code_masters v";
                sqlQuery += "        where v.parent_code = 'SRTYP'";
                sqlQuery += "          and v.code = ir.item_id)";
                sqlQuery += "     else";
                sqlQuery += "     (select im.item_name" + VMVServices.Web.Utils.LanguageCode + "";
                sqlQuery += "      From inv_item_master im";
                sqlQuery += "     where im.item_id = ir.item_id)";
                sqlQuery += "     end) as item_service,";
                sqlQuery += "     (ir.line_num || ' - ' || ir.receipt_dtl_id || ' - ' || (case ";
                sqlQuery += "     when (select im.item_name" + VMVServices.Web.Utils.LanguageCode + " ";
                sqlQuery += "            From inv_item_master im";
                sqlQuery += "           where im.item_id = ir.item_id) is null then";
                sqlQuery += "     (select v.code";
                sqlQuery += "        from ssm_code_masters v";
                sqlQuery += "       where v.parent_code = 'SRTYP'";
                sqlQuery += "         and v.code = ir.item_id)";
                sqlQuery += "    else";
                sqlQuery += "     (select im.item_name" + VMVServices.Web.Utils.LanguageCode + " ";
                sqlQuery += "      From inv_item_master im";
                sqlQuery += "       where im.item_id = ir.item_id)";
                sqlQuery += "       end) || ' - ' || ir.QTY_APPROVED) as po_data";
                sqlQuery += "     from inv_receipt_dtls ir, inv_receipts_hdr ih,inv_item_master im";
                sqlQuery += "    where ih.receipt_id = ir.receipt_id      ";
                sqlQuery += " and ir.item_id = im.item_id ";
                sqlQuery += "    and ir.receipt_id = '" + receiptId + "'";
                sqlQuery += "    and ir.receipt_dtl_id = '" + receiptDtlId + "'";
                sqlQuery += "   and  ir.enabled_flag = 1";
                sqlQuery += "   and ir.workflow_completion_status = 1";
                sqlQuery += "   and ih.ORG_ID= '" + VMVServices.Web.Utils.OrganizationID + "'";
                sqlQuery += "    order by ir.receipt_dtl_id";
            }
            else
            {
                sqlQuery = "  select ir.receipt_dtl_id,ih.receipt_date,im.item_name  as inv_description,ir.item_id,l.PO_QUANTITY,l.po_unit_price,ph.po_amount,ir.po_header_id,";
                sqlQuery += "    ir.line_num,l.PO_LINE_ID,";
                sqlQuery += "     ph.po_num,";
                sqlQuery += "     l.po_line_num,";
                sqlQuery += "     (case";
                sqlQuery += "     when (select im.item_name" + VMVServices.Web.Utils.LanguageCode + " ";
                sqlQuery += "     From inv_item_master im";
                sqlQuery += "     where im.item_id = ir.item_id) is null then";
                sqlQuery += "    (select v.code";
                sqlQuery += "    from ssm_code_masters v";
                sqlQuery += "    where v.parent_code = 'SRTYP'";
                sqlQuery += "    and v.code = ir.item_id)";
                sqlQuery += "      else";
                sqlQuery += "     (select im.item_name" + VMVServices.Web.Utils.LanguageCode + " as item_name ";
                sqlQuery += "       From inv_item_master im";
                sqlQuery += "      where im.item_id = ir.item_id)";
                sqlQuery += "    end) as item_service,";
                sqlQuery += "     (ir.line_num || ' - ' || ir.receipt_dtl_id || ' - ' || (case";
                sqlQuery += "      when (select im.item_name" + VMVServices.Web.Utils.LanguageCode + " ";
                sqlQuery += "         From inv_item_master im";
                sqlQuery += "         where im.item_id = ir.item_id) is null then";
                sqlQuery += "    (select v.code";
                sqlQuery += "    from ssm_code_masters v";
                sqlQuery += "    where v.parent_code = 'SRTYP'";
                sqlQuery += "    and v.code = ir.item_id)";
                sqlQuery += "     else";
                sqlQuery += "    (select im.item_name" + VMVServices.Web.Utils.LanguageCode + " as item_name ";
                sqlQuery += "      From inv_item_master im";
                sqlQuery += "     where im.item_id = ir.item_id)";
                sqlQuery += "     end) || ' - ' || l.po_quantity) as po_data";
                sqlQuery += "    from inv_receipt_dtls ir, po_headers ph, po_lines l,inv_receipts_hdr ih,inv_item_master im";
                sqlQuery += "    where ir.po_header_id = ph.po_header_id";
                sqlQuery += " and ir.item_id = im.item_id ";
                sqlQuery += "     and l.po_header_id = ph.po_header_id and ih.receipt_id=ir.receipt_id  and ir.item_id=l.po_item_id";
                sqlQuery += "    and ir.receipt_id = '" + receiptId + "'";
                sqlQuery += "    and ir.receipt_dtl_id = '" + receiptDtlId + "'";
                sqlQuery += "  and  ir.enabled_flag = 1";
                sqlQuery += "  and ir.workflow_completion_status = 1";
                sqlQuery += "  and ih.ORG_ID= '" + VMVServices.Web.Utils.OrganizationID + "'";
                sqlQuery += "  order by ir.receipt_dtl_id";
            }
            return sqlQuery;
        }
        public static string getReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery = "SELECT * FROM INV_PURCHASE_ITEM_RECEIPT_R V WHERE ROWNUM > 0 ";

            if (VMVServices.Web.Utils.ReportFilterParameter != null || VMVServices.Web.Utils.ReportFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.GRN_NUM] != null)
                {
                    sqlQuery += " AND V.GRN_NUM = '" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.GRN_NUM].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.VENDOR_ID] != null)
                {
                    sqlQuery += " AND V.VENDOR_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.VENDOR_ID].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["ITEM_ID"] != null)
                {
                    sqlQuery += " AND V.ITEM_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["ITEM_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["From_Qty"] != null)
                {
                    sqlQuery += " and (V.QTY_RECEIVED) >= " + VMVServices.Web.Utils.ReportFilterParameter["From_Qty"];
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["To_Qty"] != null)
                {
                    sqlQuery += " and (V.QTY_RECEIVED) <= " + VMVServices.Web.Utils.ReportFilterParameter["To_Qty"];
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
                {
                    sqlQuery += " AND V.RECEIPT_DATE between to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy') ";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
                {
                    sqlQuery += " AND to_date('" + VMVServices.Web.Utils.ReportFilterParameter["To_Date"].ToString() + "','dd/MM/yyyy') ";
                }
            }

            return sqlQuery;
        }
        public static string getPOwiseItemReceiptReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery = "SELECT * FROM INV_PURCHASE_ITEM_RECEIPT_R V WHERE ROWNUM > 0 ";

            if (VMVServices.Web.Utils.ReportFilterParameter != null || VMVServices.Web.Utils.ReportFilterParameter.Count != 0)
            {
                //if (VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.GRN_NUM] != null)
                //{
                //    sqlQuery += " AND V.GRN_NUM = '" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.GRN_NUM].ToString() + "'";
                //}
                if (VMVServices.Web.Utils.ReportViewFilterParameter["item_id"] != null)
                {
                    sqlQuery += " AND V.item_id = '" + VMVServices.Web.Utils.ReportViewFilterParameter["item_id"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
                {
                    sqlQuery += " AND V.RECEIPT_DATE between to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy') ";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
                {
                    sqlQuery += " AND to_date('" + VMVServices.Web.Utils.ReportFilterParameter["To_Date"].ToString() + "','dd/MM/yyyy') ";
                }
            }

            return sqlQuery;
        }
        public static string getGRNNumber()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT RH.GRN_NUM FROM INV_RECEIPTS_HDR RH ";
            sqlQuery += " WHERE RH.WORKFLOW_COMPLETION_STATUS='1' ";
            sqlQuery += "  and rh.ORG_ID= '" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " AND RH.ENABLED_FLAG='1' ";

            return sqlQuery;
        }

        public static string getGRNItemRecDet(string str_GRNID, string str_ItemId)
        {
            sqlQuery = string.Empty;
            sqlQuery += " SELECT IM.ITEM_NAME" + VMVServices.Web.Utils.LanguageCode + "  AS item_service,  (RD.QTY_RECEIVED-RD.Qty_Rejected) as QTY_RECEIVED,to_char(PL.PO_UNIT_PRICE) as PO_UNIT_PRICE,to_char((RD.QTY_RECEIVED-RD.Qty_Rejected) * PL.PO_UNIT_PRICE) AS PO_Amount,RD.ITEM_ID ";
            sqlQuery += " FROM INV_RECEIPT_DTLS RD ";
            sqlQuery += " INNER JOIN PO_LINES PL ON PL.PO_LINE_ID= RD.PO_LINE_ID ";
            sqlQuery += " INNER JOIN INV_ITEM_MASTER IM ON IM.ITEM_ID = RD.ITEM_ID";
            sqlQuery += " WHERE RD.RECEIPT_ID ='" + str_GRNID + "' AND RD.ITEM_ID='" + str_ItemId + "'";
            return sqlQuery;
        }

    }
}
