﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;


namespace FIN.DAL.AP
{
    public class Item_DAL
    {

        static string sqlQuery = "";


        public static string getItemNameDetails_frARDeleiverychallan(string om_order_id)
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT IM.ITEM_ID, (im.ITEM_NUMBER" + VMVServices.Web.Utils.LanguageCode + " ||' - ' || im.item_name" + VMVServices.Web.Utils.LanguageCode + " ||' - '||im.item_description" + VMVServices.Web.Utils.LanguageCode + " ) AS ITEM_NAME ";
            sqlQuery += " FROM OM_CUSTOMER_ORDER_HDR oc,om_customer_order_dtl od,inv_item_master im ";
            sqlQuery += " WHERE IM.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " AND IM.ENABLED_FLAG =1";
            sqlQuery += " AND oc.om_order_id = od.om_order_id";
            sqlQuery += " and im.item_id = od.om_item_id";
            if (VMVServices.Web.Utils.LanguageCode != string.Empty)
            {
                sqlQuery += "   and im.item_name_OL is not null";
            }
            sqlQuery += " and od.om_order_id = '" + om_order_id + "'";
            sqlQuery += "    and im.ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " ORDER BY ITEM_NAME ASC ";

            return sqlQuery;
        }



        public static string getItemCategory()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT IIC.ITEM_CATEGORY_ID AS CATEGORY_ID, IIC.ITEM_CATEGORY_ID ||' - '|| IIC.ITEM_CAT_NAME" + VMVServices.Web.Utils.LanguageCode + "  AS CATEGORY_NAME,IIC.ITEM_CAT_NAME" + VMVServices.Web.Utils.LanguageCode + "  AS ITEM_CATEGORY ";
            sqlQuery += " FROM INV_ITEM_CATEGORY IIC ";
            sqlQuery += " WHERE IIC.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " AND IIC.ENABLED_FLAG = 1 ";
            if (VMVServices.Web.Utils.LanguageCode != string.Empty)
            {
                sqlQuery += "   and iUM.UOM_CODE_OL is not null";
            }
            sqlQuery += "    and IIC.ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " ORDER BY CATEGORY_ID ";

            return sqlQuery;
        }
        public static string getUnitPrice(string itemId)
        {
            sqlQuery = string.Empty;
            sqlQuery = " select nvl(po.item_unit_price,0) as  item_unit_price  from inv_item_master po";
            sqlQuery += "  where po.item_id='" + itemId + "'";
            sqlQuery += "  and po.org_id='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += "   and po.workflow_completion_status='1'";
            sqlQuery += "   and po.enabled_flag='1'";
            return sqlQuery;
        }
        public static string getItemName(string item_type)
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT (im.ITEM_NUMBER" + VMVServices.Web.Utils.LanguageCode + " ||' - ' || im.item_name" + VMVServices.Web.Utils.LanguageCode + " ||' - '||im.item_description" + VMVServices.Web.Utils.LanguageCode + " ) AS ITEM_NAME, IM.ITEM_ID ,IM.ITEM_CODE" + VMVServices.Web.Utils.LanguageCode + " as ITEM_CODE ";
            sqlQuery += " FROM INV_ITEM_MASTER IM ";
            sqlQuery += " WHERE IM.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " AND IM.ENABLED_FLAG =1 and im.inv_item_type='" + item_type + "'";
            if (VMVServices.Web.Utils.LanguageCode != string.Empty)
            {
                sqlQuery += "   and im.item_name_OL is not null";
            }
            sqlQuery += "    and im.ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " ORDER BY ITEM_NAME" + VMVServices.Web.Utils.LanguageCode + "  ASC ";

            return sqlQuery;
        }
        public static string getItemList_Without_Service()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT IM.ITEM_ID, (im.ITEM_NUMBER" + VMVServices.Web.Utils.LanguageCode + " ||' - ' || im.item_name" + VMVServices.Web.Utils.LanguageCode + " ||' - '||im.item_description" + VMVServices.Web.Utils.LanguageCode + " ) AS ITEM_NAME ";
            sqlQuery += " FROM INV_ITEM_MASTER IM ";
            sqlQuery += " WHERE IM.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " AND IM.ENABLED_FLAG =1";
            sqlQuery += "  and UPPER(im.inv_item_type)='ITEM'";
            if (VMVServices.Web.Utils.LanguageCode != string.Empty)
            {
                sqlQuery += "   and im.item_name_OL is not null";
            }
            sqlQuery += "    and im.ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " ORDER BY ITEM_NAME ASC ";

            return sqlQuery;
        }

        public static string getTransactionType()
        {
            sqlQuery = string.Empty;
            sqlQuery = " select tl.inv_tran_id,tl.inv_trans_type from inv_transaction_ledger tl";

            sqlQuery += " where tl.workflow_completion_status = 1";
            sqlQuery += " and tl.enabled_flag = 1";
           
           

            return sqlQuery;
        }

        public static string getItem_RECinWh_notServce()
        {
            
            sqlQuery = string.Empty;
            sqlQuery = " SELECT distinct IM.ITEM_ID, (im.ITEM_NUMBER" + VMVServices.Web.Utils.LanguageCode + " ||' - ' || im.item_name" + VMVServices.Web.Utils.LanguageCode + " ||' - '||im.item_description" + VMVServices.Web.Utils.LanguageCode + " ) AS ITEM_NAME ";
            sqlQuery += " FROM INV_ITEM_MASTER IM,INV_RECEIPT_ITEM_WH_HDR wh ";
            sqlQuery += " WHERE IM.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " AND IM.ENABLED_FLAG =1";
            sqlQuery += "  and UPPER(im.inv_item_type)='ITEM'";
            sqlQuery += " AND im.item_id = wh.item_id";
            if (VMVServices.Web.Utils.LanguageCode != string.Empty)
            {
                sqlQuery += "   and im.item_name_OL is not null";
            }
            sqlQuery += "    and im.ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " ORDER BY ITEM_NAME ASC ";

            return sqlQuery;
        }

        public static string getItemNameDetails()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT IM.ITEM_ID, (im.ITEM_NUMBER" + VMVServices.Web.Utils.LanguageCode + " ||' - ' || im.item_name" + VMVServices.Web.Utils.LanguageCode + " ||' - '||im.item_description" + VMVServices.Web.Utils.LanguageCode + " ) AS ITEM_NAME ";
            sqlQuery += " FROM INV_ITEM_MASTER IM ";
            sqlQuery += " WHERE IM.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " AND IM.ENABLED_FLAG =1";
            if (VMVServices.Web.Utils.LanguageCode != string.Empty)
            {
                sqlQuery += "   and im.item_name_OL is not null";
            }
            sqlQuery += "    and im.ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " ORDER BY ITEM_NAME ASC ";

            return sqlQuery;
        }

        public static string getItemGroup()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT  IIG.ITEM_GROUP_ID AS GROUP_ID, IIG.ITEM_GROUP_NAME" + VMVServices.Web.Utils.LanguageCode + "  AS GROUP_NAME ";
            sqlQuery += " FROM INV_ITEM_GROUP IIG ";
            sqlQuery += " WHERE IIG.ENABLED_FLAG = 1 ";
            sqlQuery += " AND IIG.WORKFLOW_COMPLETION_STATUS = 1 ";
            if (VMVServices.Web.Utils.LanguageCode != string.Empty)
            {
                sqlQuery += "   and IIG.ITEM_GROUP_NAME" + VMVServices.Web.Utils.LanguageCode + " is not null";
            }
            sqlQuery += "    and IIG.ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " ORDER BY GROUP_ID ";

            return sqlQuery;
        }


        public static string getWeightUOM()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT IUM.UOM_CODE" + VMVServices.Web.Utils.LanguageCode + "  AS UOM_CODE, IUM.UOM_CODE" + VMVServices.Web.Utils.LanguageCode + "  ||' - '||IUM.UOM_DESC" + VMVServices.Web.Utils.LanguageCode + "  AS UOM_NAME ";
            sqlQuery += " FROM INV_UOM_MASTER IUM ";
            sqlQuery += " WHERE IUM.ENABLED_FLAG = 1 ";
            sqlQuery += " AND IUM.UOM_CLASS =  'Weight' ";
            sqlQuery += " AND IUM.WORKFLOW_COMPLETION_STATUS = 1 ";
            if (VMVServices.Web.Utils.LanguageCode != string.Empty)
            {
                sqlQuery += "   and IUM.UOM_CODE" + VMVServices.Web.Utils.LanguageCode + "  is not null";
            }
            sqlQuery += "    and IUM.ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " ORDER BY UOM_CODE ";

            return sqlQuery;
        }


        public static string getLengthUOM()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT IUM.UOM_CODE" + VMVServices.Web.Utils.LanguageCode + "  AS UOM_CODE, IUM.UOM_CODE" + VMVServices.Web.Utils.LanguageCode + "  ||' - '||IUM.UOM_DESC" + VMVServices.Web.Utils.LanguageCode + "  AS UOM_NAME ";
            sqlQuery += " FROM INV_UOM_MASTER IUM ";
            sqlQuery += " WHERE IUM.ENABLED_FLAG = 1 ";
            sqlQuery += " AND IUM.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " AND IUM.UOM_CLASS =  'Length' ";
            if (VMVServices.Web.Utils.LanguageCode != string.Empty)
            {
                sqlQuery += "   and IUM.UOM_CODE" + VMVServices.Web.Utils.LanguageCode + "  is not null";
            }
            sqlQuery += "    and IUM.ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";

            sqlQuery += " ORDER BY UOM_CODE ";

            return sqlQuery;
        }


        public static string getAreaUOM()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT IUM.UOM_CODE" + VMVServices.Web.Utils.LanguageCode + "  AS UOM_CODE, IUM.UOM_CODE" + VMVServices.Web.Utils.LanguageCode + "  ||' - '||IUM.UOM_DESC" + VMVServices.Web.Utils.LanguageCode + "  AS UOM_NAME,ium.UOM_ID ";
            sqlQuery += " FROM INV_UOM_MASTER IUM ";
            sqlQuery += " WHERE IUM.ENABLED_FLAG = 1 ";
            sqlQuery += " AND IUM.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " AND IUM.UOM_CLASS =  'Area' ";
            if (VMVServices.Web.Utils.LanguageCode != string.Empty)
            {
                sqlQuery += "   and IUM.UOM_CODE" + VMVServices.Web.Utils.LanguageCode + "  is not null";
            }
            sqlQuery += "    and IUM.ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " ORDER BY UOM_CODE ";

            return sqlQuery;
        }

        public static string getServiceUOM()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT IUM.UOM_CODE" + VMVServices.Web.Utils.LanguageCode + "  AS UOM_CODE, IUM.UOM_CODE" + VMVServices.Web.Utils.LanguageCode + "  ||' - '||IUM.UOM_DESC" + VMVServices.Web.Utils.LanguageCode + "  AS UOM_NAME,ium.UOM_ID ";
            sqlQuery += " FROM INV_UOM_MASTER IUM ";
            sqlQuery += " WHERE IUM.ENABLED_FLAG = 1 ";
            sqlQuery += " AND IUM.WORKFLOW_COMPLETION_STATUS = 1 ";
            //sqlQuery += " AND IUM.UOM_CLASS =  '20' ";
            if (VMVServices.Web.Utils.LanguageCode != string.Empty)
            {
                sqlQuery += "   and IUM.UOM_CODE" + VMVServices.Web.Utils.LanguageCode + "  is not null";
            }
            sqlQuery += "    and IUM.ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " ORDER BY UOM_CODE ";

            return sqlQuery;
        }

        public static string getVolumeUOM()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT IUM.UOM_CODE" + VMVServices.Web.Utils.LanguageCode + "  AS UOM_CODE, IUM.UOM_CODE" + VMVServices.Web.Utils.LanguageCode + "  ||' - '||IUM.UOM_DESC" + VMVServices.Web.Utils.LanguageCode + "  AS UOM_NAME ";
            sqlQuery += " FROM INV_UOM_MASTER IUM ";
            sqlQuery += " WHERE IUM.ENABLED_FLAG = 1 ";
            sqlQuery += " AND IUM.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " AND IUM.UOM_CLASS =  'Volume' ";
            if (VMVServices.Web.Utils.LanguageCode != string.Empty)
            {
                sqlQuery += "   and IUM.UOM_CODE" + VMVServices.Web.Utils.LanguageCode + "  is not null";
            }
            sqlQuery += "    and IUM.ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " ORDER BY UOM_CODE ";

            return sqlQuery;
        }


        public static string getUOM()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT IUM.UOM_CODE" + VMVServices.Web.Utils.LanguageCode + "  AS UOM_CODE, IUM.UOM_DESC" + VMVServices.Web.Utils.LanguageCode + "  AS UOM_NAME ";
            sqlQuery += " FROM INV_UOM_MASTER IUM ";
            sqlQuery += " WHERE IUM.ENABLED_FLAG = 1 ";
            sqlQuery += " AND IUM.WORKFLOW_COMPLETION_STATUS = 1 ";
            if (VMVServices.Web.Utils.LanguageCode != string.Empty)
            {
                sqlQuery += "   and IUM.UOM_CODE" + VMVServices.Web.Utils.LanguageCode + "  is not null";
            }
            sqlQuery += "    and IUM.ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " ORDER BY UOM_NAME ";

            return sqlQuery;
        }

        public static string getUOM_frSO()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT IUM.UOM_ID AS UOM_CODE, IUM.UOM_DESC" + VMVServices.Web.Utils.LanguageCode + "  AS UOM_NAME ";
            sqlQuery += " FROM INV_UOM_MASTER IUM ";
            sqlQuery += " WHERE IUM.ENABLED_FLAG = 1 ";
            sqlQuery += " AND IUM.WORKFLOW_COMPLETION_STATUS = 1 ";
            if (VMVServices.Web.Utils.LanguageCode != string.Empty)
            {
                sqlQuery += "   and IUM.UOM_DESC" + VMVServices.Web.Utils.LanguageCode + "  is not null";
            }
            sqlQuery += "    and IUM.ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " ORDER BY UOM_NAME ";

            return sqlQuery;
        }


        public static string getReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery = "SELECT * FROM VW_ITEM_PARAM V WHERE ROWNUM > 0 ";

            if (VMVServices.Web.Utils.ReportFilterParameter != null || VMVServices.Web.Utils.ReportFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.ITEM_TYPE] != null)
                {
                    sqlQuery += " AND V.ITEM_TYPE = '" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.ITEM_TYPE].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["ITEM_CAT_NAME"] != null)
                {
                    sqlQuery += " AND V.ITEM_CAT_NAME = '" + VMVServices.Web.Utils.ReportFilterParameter["ITEM_CAT_NAME"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["ITEM_UOM"] != null)
                {
                    sqlQuery += " AND V.ITEM_UOM = '" + VMVServices.Web.Utils.ReportFilterParameter["ITEM_UOM"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["ITEM_GROUP_NAME"] != null)
                {
                    sqlQuery += " AND V.ITEM_GROUP_NAME = '" + VMVServices.Web.Utils.ReportFilterParameter["ITEM_GROUP_NAME"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["CREATED_BY"] != null)
                {
                    sqlQuery += " AND UPPER(V.CREATED_BY) = UPPER('" + VMVServices.Web.Utils.ReportViewFilterParameter["CREATED_BY"].ToString() + "')";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["MODIFIED_BY"] != null)
                {
                    sqlQuery += " AND UPPER(V.MODIFIED_BY) = UPPER('" + VMVServices.Web.Utils.ReportViewFilterParameter["MODIFIED_BY"].ToString() + "')";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["CREATED_From_Date"] != null)
                {
                    sqlQuery += " AND V.CREATED_DATE >= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["CREATED_From_Date"].ToString() + "','dd/MM/yyyy') ";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["CREATED_To_Date"] != null)
                {
                    sqlQuery += " AND V.CREATED_DATE <= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["CREATED_To_Date"].ToString() + "','dd/MM/yyyy') ";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["MODIFIED_From_Date"] != null)
                {
                    sqlQuery += " AND V.MODIFIED_DATE >= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["MODIFIED_From_Date"].ToString() + "','dd/MM/yyyy') ";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["MODIFIED_To_Date"] != null)
                {
                    sqlQuery += " AND V.MODIFIED_DATE <= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["MODIFIED_To_Date"].ToString() + "','dd/MM/yyyy') ";
                }
            }


            return sqlQuery;
        }


        public static string getDayBookReportData()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT * FROM AP_DAY_BOOK_PAYMENT V WHERE ROWNUM > 0 ";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {


                if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.EFFECTIVE_FROM_DT] != null)
                {
                    sqlQuery += " AND to_date(V.PAY_DATE,'dd/MM/yyyy') between to_date('" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.EFFECTIVE_FROM_DT].ToString() + "','dd/MM/yyyy')";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.EFFECTIVE_TO_DT] != null)
                {
                    sqlQuery += " AND to_date('" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.EFFECTIVE_TO_DT].ToString() + "','dd/MM/yyyy')";
                }
            }

            return sqlQuery;
        }

        public static string getDayBook_Material_Trans()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT * FROM VW_DAY_BOOK_MATERIALTRAN V WHERE ROWNUM > 0 ";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportFilterParameter["Item_id"] != null)
                {
                    sqlQuery += " AND V.INV_ITEM_ID =  '" + VMVServices.Web.Utils.ReportFilterParameter["Item_id"].ToString() + "'";
                }

                if (VMVServices.Web.Utils.ReportFilterParameter["Transaction_Type_Id"] != null)
                {
                    sqlQuery += " AND V.TRANS_TYPE =  '" + VMVServices.Web.Utils.ReportFilterParameter["Transaction_Type_Id"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["Reference_Number"] != null)
                {
                    sqlQuery += " AND V.REF_NUM =  '" + VMVServices.Web.Utils.ReportFilterParameter["Reference_Number"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["From_Quantity"] != null)
                {
                    sqlQuery += " AND V.INV_QTY >=  '" + VMVServices.Web.Utils.ReportFilterParameter["From_Quantity"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["To_Quantity"] != null)
                {
                    sqlQuery += " AND V.INV_QTY <=  '" + VMVServices.Web.Utils.ReportFilterParameter["To_Quantity"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
                {
                    sqlQuery += " AND V.INV_TRANS_DATE between to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy')";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
                {
                    sqlQuery += " AND to_date('" + VMVServices.Web.Utils.ReportFilterParameter["To_Date"].ToString() + "','dd/MM/yyyy')";
                }
            }

            return sqlQuery;
        }

        public static string getItemType()
        {
            sqlQuery = string.Empty;
            sqlQuery += "SELECT DISTINCT IM.INV_ITEM_TYPE AS ITEM_TYPE ";
            sqlQuery += "FROM INV_ITEM_MASTER IM ";
            sqlQuery += "WHERE IM.WORKFLOW_COMPLETION_STATUS='1' ";
            sqlQuery += "AND IM.ENABLED_FLAG='1' ";
            sqlQuery += "    and IM.ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            return sqlQuery;
        }

        public static string getItemCategoryName()
        {
            sqlQuery = string.Empty;
            sqlQuery += "SELECT DISTINCT IC.ITEM_CAT_NAME" + VMVServices.Web.Utils.LanguageCode + "  AS ITEM_CAT_NAME";
            sqlQuery += " FROM INV_ITEM_CATEGORY IC ";
            sqlQuery += "WHERE IC.WORKFLOW_COMPLETION_STATUS='1' ";
            if (VMVServices.Web.Utils.LanguageCode != string.Empty)
            {
                sqlQuery += "   and IC.ITEM_CAT_NAME" + VMVServices.Web.Utils.LanguageCode + "  is not null";
            }
            sqlQuery += "    and ic.ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += "AND IC.ENABLED_FLAG='1' ";

            return sqlQuery;
        }
        public static string getItemUOM()
        {
            sqlQuery = string.Empty;
            sqlQuery += "SELECT DISTINCT IM.ITEM_UOM ";
            sqlQuery += "FROM INV_ITEM_MASTER IM ";
            sqlQuery += "WHERE IM.WORKFLOW_COMPLETION_STATUS = '1' ";
            sqlQuery += "AND IM.ENABLED_FLAG='1' ";
            sqlQuery += "    and IM.ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            return sqlQuery;
        }
        public static string getGroupName()
        {
            sqlQuery = string.Empty;
            sqlQuery += "SELECT DISTINCT IG.ITEM_GROUP_NAME" + VMVServices.Web.Utils.LanguageCode + "  AS ITEM_GROUP_NAME,IG.ITEM_GROUP_ID ";
            sqlQuery += " FROM  INV_ITEM_GROUP IG ";
            sqlQuery += " WHERE IG.WORKFLOW_COMPLETION_STATUS='1' ";
            if (VMVServices.Web.Utils.LanguageCode != string.Empty)
            {
                sqlQuery += "   and IG.ITEM_GROUP_NAME" + VMVServices.Web.Utils.LanguageCode + "  is not null";
            }
            sqlQuery += "AND IG.ENABLED_FLAG='1' ";
            sqlQuery += "    and Ig.ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            return sqlQuery;
        }



        static string spOutPut = string.Empty;
        public static System.Collections.SortedList ErrorCollection = new System.Collections.SortedList();


        public static string GetSPFOR_DUPLICATE_CHECK(string P_ITEM_CODE, string P_ITEM_NAME, string P_ORG_ID, string P_RECORD_ID)
        {
            try
            {
                string retcode = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "PKG_VALIDATIONS.PROC_ERR_MGR_ITEM_MASTER";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add("@P_MODULE_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_SCREEN_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_ITEM_CODE", OracleDbType.Char).Value = P_ITEM_CODE;
                oraCmd.Parameters.Add("@P_ITEM_NAME", OracleDbType.Char).Value = P_ITEM_NAME;
                oraCmd.Parameters.Add("@P_ORG_ID", OracleDbType.Char).Value = P_ORG_ID;
                oraCmd.Parameters.Add("@P_RECORD_ID", OracleDbType.Char).Value = P_RECORD_ID;
                oraCmd.Parameters.Add("@P_RET", OracleDbType.Varchar2, 150);
                oraCmd.Parameters["@P_RET"].Direction = ParameterDirection.Output;
                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

                retcode = oraCmd.Parameters["@P_RET"].Value.ToString();

                return retcode;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GetSPFOR_ERR_MGR_CATEGORY(string P_SCREEN_CODE, string P_RECORD_ID, string P_VALUE1)
        {
            try
            {
                string retcode = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "PKG_DUPLN_VALDN.PROC_DUPLN_VALIDATION_ONE";
                oraCmd.CommandType = CommandType.StoredProcedure;
                //  oraCmd.Parameters.Add("@P_MODULE_CODE", OracleDbType.Char).Value = DBNull.Value;
                //  oraCmd.Parameters.Add("@P_SCREEN_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_SCREEN_CODE", OracleDbType.Char).Value = P_SCREEN_CODE;
                oraCmd.Parameters.Add("@P_PK_ID", OracleDbType.Char).Value = P_RECORD_ID;
                oraCmd.Parameters.Add("@P_VALUE1", OracleDbType.Char).Value = P_VALUE1;
                oraCmd.Parameters.Add("@P_RET", OracleDbType.Varchar2, 150);
                oraCmd.Parameters["@P_RET"].Direction = ParameterDirection.Output;
                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

                retcode = oraCmd.Parameters["@P_RET"].Value.ToString();

                return retcode;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string getItemCategoryReportData()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT * FROM VW_AP_ITEM_CATEGORY V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {

                if (VMVServices.Web.Utils.ReportViewFilterParameter["PARENT_CAT_ID"] != null)
                {
                    sqlQuery += " AND V.ITEM_CATEGORY_PARENT_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["PARENT_CAT_ID"].ToString() + "'";
                }

                if (VMVServices.Web.Utils.ReportViewFilterParameter["CAT_ID"] != null)
                {
                    sqlQuery += " AND V.ITEM_CAT_NAME = '" + VMVServices.Web.Utils.ReportViewFilterParameter["CAT_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["CREATED_BY"] != null)
                {
                    sqlQuery += " AND UPPER(V.CREATED_BY) = UPPER('" + VMVServices.Web.Utils.ReportViewFilterParameter["CREATED_BY"].ToString() + "')";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["MODIFIED_BY"] != null)
                {
                    sqlQuery += " AND UPPER(V.MODIFIED_BY) = UPPER('" + VMVServices.Web.Utils.ReportViewFilterParameter["MODIFIED_BY"].ToString() + "')";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["CREATED_From_Date"] != null)
                {
                    sqlQuery += " AND V.CREATED_DATE >= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["CREATED_From_Date"].ToString() + "','dd/MM/yyyy') ";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["CREATED_To_Date"] != null)
                {
                    sqlQuery += " AND V.CREATED_DATE <= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["CREATED_To_Date"].ToString() + "','dd/MM/yyyy') ";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["MODIFIED_From_Date"] != null)
                {
                    sqlQuery += " AND V.MODIFIED_DATE >= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["MODIFIED_From_Date"].ToString() + "','dd/MM/yyyy') ";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["MODIFIED_To_Date"] != null)
                {
                    sqlQuery += " AND V.MODIFIED_DATE <= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["MODIFIED_To_Date"].ToString() + "','dd/MM/yyyy') ";
                }

            }
            

            return sqlQuery;
        }
    }

}
