﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.AP
{
    public class TaxTerms_DAL
    {

        static string sqlQuery = "";
        public static string getTaxName(string TAX_NAME)
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT T.TAX_NAME" + VMVServices.Web.Utils.LanguageCode;
            sqlQuery += " FROM tAX_TERMS T";
            sqlQuery += " WHERE UPPER(T.TAX_NAME) = upper('" + TAX_NAME + "')";

            return sqlQuery;
        }
        public static string getTaxTerm()
        {
            sqlQuery = string.Empty;
            sqlQuery = "Select t.tax_id,t.tax_name" + VMVServices.Web.Utils.LanguageCode + ",t.tax_rate,t.tax_country,t.tax_eff_start_dt,t.tax_eff_end_dt,t.enabled_flag,t.pk_id from Tax_TERMS T where T.ENABLED_FLAG=1 AND T.WORKFLOW_COMPLETION_STATUS=1 ";
            sqlQuery += " AND (S.EFFECTIVE_END_DT IS NULL OR";
            sqlQuery += " (SYSDATE BETWEEN S.Effective_Start_Dt AND S.Effective_End_Dt))";
            sqlQuery += " ORDER BY T.TAX_NAME" + VMVServices.Web.Utils.LanguageCode + "";
            return sqlQuery;
        }

        public static string getTaxDetails()
        {
            sqlQuery = string.Empty;
            sqlQuery = "Select TAX_ID,TAX_NAME" + VMVServices.Web.Utils.LanguageCode + " FROM TAX_TERMS TT WHERE TT.ENABLED_FLAG=1 AND TT.WORKFLOW_COMPLETION_STATUS =1 ";
            return sqlQuery;
        }


    }
}
