﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.AP
{
    public class ItemCategory_DAL
    {

        static string sqlQuery = "";
        public static string getCategoryParent()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT IIC.ITEM_CATEGORY_ID AS CAT_ID,IIC.ITEM_CAT_NAME" + VMVServices.Web.Utils.LanguageCode + " AS CAT_NAME ";
            sqlQuery += " FROM INV_ITEM_CATEGORY IIC ";
            sqlQuery += " WHERE IIC.ENABLED_FLAG = 1 ";
            sqlQuery += " AND IIC.WORKFLOW_COMPLETION_STATUS = 1 ";
            if (VMVServices.Web.Utils.LanguageCode != string.Empty)
            {
                sqlQuery += "   and IIC.ITEM_CAT_NAME" + VMVServices.Web.Utils.LanguageCode + "  is not null";
            }
            sqlQuery += " and IIC.ORG_ID ='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " ORDER BY CAT_ID ";

            return sqlQuery;
        }

        public static string getCategory()
        {
            sqlQuery = string.Empty;

            sqlQuery = "  SELECT IIC.Item_Category_Id as ITEM_CATEGORY_PARENT_ID,";
            sqlQuery += "  IIC.ITEM_CAT_NAME" + VMVServices.Web.Utils.LanguageCode + "     as ITEM_CAT_NAME";
            sqlQuery += " FROM INV_ITEM_CATEGORY IIC";
            sqlQuery += "  WHERE IIC.ITEM_CATEGORY_PARENT_ID = '0'";
            sqlQuery += "   AND IIC.ENABLED_FLAG = 1";
            if (VMVServices.Web.Utils.LanguageCode != string.Empty)
            {
                sqlQuery += "   and IIC.ITEM_CAT_NAME" + VMVServices.Web.Utils.LanguageCode + "  is not null";
            }
            sqlQuery += " and IIC.ORG_ID ='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += "   AND IIC.WORKFLOW_COMPLETION_STATUS = 1";


            sqlQuery += " union ";

            sqlQuery += "  SELECT IIC.Item_Category_Id as ITEM_CATEGORY_PARENT_ID,";
            sqlQuery += "       IIC.ITEM_CAT_NAME" + VMVServices.Web.Utils.LanguageCode + "     as ITEM_CAT_NAME";
            sqlQuery += "   FROM INV_ITEM_CATEGORY IIC";
            sqlQuery += "   WHERE (IIC.ITEM_CATEGORY_PARENT_ID = '0'";
            sqlQuery += "    or IIC.ITEM_CATEGORY_PARENT_ID is null)";
            sqlQuery += "   AND IIC.ENABLED_FLAG = 1";
            if (VMVServices.Web.Utils.LanguageCode != string.Empty)
            {
                sqlQuery += "   and IIC.ITEM_CAT_NAME" + VMVServices.Web.Utils.LanguageCode + "  is not null";
            }
            sqlQuery += " and IIC.ORG_ID ='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += "  AND IIC.WORKFLOW_COMPLETION_STATUS = 1";

            return sqlQuery;

        }
        public static string getCategoryChild()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT IIC.Item_Category_Id as ITEM_CATEGORY_PARENT_ID, IIC.ITEM_CAT_NAME" + VMVServices.Web.Utils.LanguageCode + " as ITEM_CAT_NAME";
            sqlQuery += " FROM INV_ITEM_CATEGORY IIC ";
            sqlQuery += " WHERE IIC.ITEM_CATEGORY_PARENT_ID = '0' and  ";
            sqlQuery += " IIC.ENABLED_FLAG = 1 ";
            if (VMVServices.Web.Utils.LanguageCode != string.Empty)
            {
                sqlQuery += "   and IIC.ITEM_CAT_NAME" + VMVServices.Web.Utils.LanguageCode + "  is not null";
            }
            sqlQuery += " and IIC.ORG_ID ='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " AND IIC.WORKFLOW_COMPLETION_STATUS = 1 ";

            return sqlQuery;

        }

    }
}
