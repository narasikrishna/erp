﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.AP
{
    public class ItemGroup_DAL
    {

        static string sqlQuery = "";
        public static string getGroupName(string grp_name)
        {
            sqlQuery = string.Empty;
            sqlQuery = " select g.item_group_name" + VMVServices.Web.Utils.LanguageCode + " as item_group_name";
            sqlQuery += " from INV_ITEM_GROUP G ";
            sqlQuery += " WHERE UPPER(G.ITEM_GROUP_NAME) = upper('" + grp_name + "')";
            sqlQuery += " and g.ORG_ID ='" + VMVServices.Web.Utils.OrganizationID + "'";
            return sqlQuery;
        }
        public static string getItemGroupReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery = "SELECT * FROM vw_ap_item_group V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {



                if (VMVServices.Web.Utils.ReportViewFilterParameter["ITEM_GROUP"] != null)
                {
                    sqlQuery += " AND V.ITEM_GROUP_NAME = '" + VMVServices.Web.Utils.ReportViewFilterParameter["ITEM_GROUP"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["CREATED_BY"] != null)
                {
                    sqlQuery += " AND UPPER(V.CREATED_BY) = UPPER('" + VMVServices.Web.Utils.ReportViewFilterParameter["CREATED_BY"].ToString() + "')";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["MODIFIED_BY"] != null)
                {
                    sqlQuery += " AND UPPER(V.MODIFIED_BY) = UPPER('" + VMVServices.Web.Utils.ReportViewFilterParameter["MODIFIED_BY"].ToString() + "')";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["CREATED_From_Date"] != null)
                {
                    sqlQuery += " AND V.CREATED_DATE >= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["CREATED_From_Date"].ToString() + "','dd/MM/yyyy') ";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["CREATED_To_Date"] != null)
                {
                    sqlQuery += " AND V.CREATED_DATE <= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["CREATED_To_Date"].ToString() + "','dd/MM/yyyy') ";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["MODIFIED_From_Date"] != null)
                {
                    sqlQuery += " AND V.MODIFIED_DATE >= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["MODIFIED_From_Date"].ToString() + "','dd/MM/yyyy') ";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["MODIFIED_To_Date"] != null)
                {
                    sqlQuery += " AND V.MODIFIED_DATE <= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["MODIFIED_To_Date"].ToString() + "','dd/MM/yyyy') ";
                }
            }


            return sqlQuery;
        }

    }
}
