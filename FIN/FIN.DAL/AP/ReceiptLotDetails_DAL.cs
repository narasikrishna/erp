﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.AP
{
    public class ReceiptLotDetails_DAL
    {
        static string sqlQuery = "";

        public static string GetRecptlotdtls(String str_lot_id)
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT RLD.PK_ID,RLD.LOT_ID,C.CODE,C.CODE,RLD.ATTRIBUTE_VALUE,'N' AS DELETED";
            sqlQuery += " FROM INV_RECEIPT_LOT_DTLS RLD,SSM_CODE_MASTERS C";
            sqlQuery += " WHERE RLD.LOT_ID = '" + str_lot_id + "'";
            sqlQuery += " AND RLD.ATTRIBUTE_TYPE = C.CODE";
            sqlQuery += " AND RLD.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND RLD.ENABLED_FLAG = 1";
            sqlQuery += " ORDER BY RLD.PK_ID ";

            return sqlQuery;

        }

        public static string getGRNNo()
        {
            sqlQuery = string.Empty;

            sqlQuery = " select rh.receipt_id,rh.grn_num";
            sqlQuery += " from inv_receipts_hdr rh";
            sqlQuery += " where rh.enabled_flag = 1";
            sqlQuery += " and rh.workflow_completion_status = 1 and rh.org_id='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " order by rh.receipt_id";


            return sqlQuery;
        }


        public static string getLineNo()
        {
            sqlQuery = string.Empty;


            sqlQuery = " SELECT RD.RECEIPT_DTL_ID,RD.LINE_NUM";
            sqlQuery += " FROM inv_receipt_dtls RD";
            sqlQuery += " WHERE RD.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND RD.ENABLED_FLAG = 1";
            sqlQuery += " ORDER BY RD.RECEIPT_DTL_ID";

            return sqlQuery;
        }

        public static string getgrndata(string str_recpt_id)
        {
            sqlQuery = string.Empty;


            sqlQuery = " SELECT RH.RECEIPT_DATE AS GRN_DATE";
            sqlQuery += " FROM INV_RECEIPTS_HDR RH";
            sqlQuery += " WHERE RH.RECEIPT_ID = '" + str_recpt_id + "'";
            sqlQuery += " AND RH.ENABLED_FLAG = 1";
            sqlQuery += " AND RH.WORKFLOW_COMPLETION_STATUS = 1 and rh.org_id='" + VMVServices.Web.Utils.OrganizationID + "'";

            return sqlQuery;
        }


        public static string getLotqty(string receipt_dtl_id)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select (rd.qty_received - rd.qty_rejected) as tot_qty";
            sqlQuery += " from INV_RECEIPT_DTLS rd";
            sqlQuery += " where rd.workflow_completion_status = 1";
            sqlQuery += " and rd.receipt_dtl_id = '" + receipt_dtl_id + "'";
            return sqlQuery;
        }
        public static string getReceivedLotqtyinWHI(string lotId)
        {
            sqlQuery = string.Empty;

            sqlQuery = "  SELECT sum(rwd.lot_qty) as tot_qty";
            sqlQuery += "   FROM INV_RECEIPT_ITEM_WH_DTL RWD";
            sqlQuery += "  WHERE RWD.ENABLED_FLAG = 1";
            sqlQuery += "  and RWD.LOT_ID ='" + lotId + "'";
            sqlQuery += "   AND RWD.WORKFLOW_COMPLETION_STATUS = 1";
            return sqlQuery;
        }
        public static string GetTotalReceivedLotqty(string receipt_id, string receipt_dtl_id, string item_id)
        {
            sqlQuery = string.Empty;

            sqlQuery += " select sum(lh.lot_qty) as lot_qty from inv_receipt_lots_hdr lh";
            sqlQuery += " where lh.workflow_completion_status = 1 and lh.RECEIPT_ID='" + receipt_id + "'";
            sqlQuery += " and lh.receipt_dtl_id='" + receipt_dtl_id + "'";
            sqlQuery += " and lh.item_id='" + item_id + "'";
            sqlQuery += "  and lh.LOT_ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            return sqlQuery;
        }
        public static string getpodata(string receipt_dtl_id)
        {
            sqlQuery = string.Empty;

            string recFlag = string.Empty;

            sqlQuery = " select rr.receipt_flag from inv_receipts_hdr rr,inv_receipt_dtls rd where rr.receipt_id=rd.receipt_id and rd.receipt_dtl_id='" + receipt_dtl_id + "'";

            recFlag = DBMethod.GetStringValue(sqlQuery);

            sqlQuery = string.Empty;

            if (recFlag == "Micellaneous")
            {
                sqlQuery = "   select '' as PO_NUM,";
                sqlQuery += "  im.item_description,";
                sqlQuery += "  '' as po_line_num,";
                sqlQuery += "  im.item_id,";
                sqlQuery += "  rd.qty_approved,";
                sqlQuery += "   rd.receipt_dtl_id";
                sqlQuery += "  from INV_RECEIPT_DTLS rd, INV_ITEM_MASTER im";
                sqlQuery += "  where rd.item_id = im.item_id";
                sqlQuery += "  and rd.receipt_dtl_id = '" + receipt_dtl_id + "'";
                sqlQuery += "  and rd.workflow_completion_status = 1";
                sqlQuery += "   and rd.enabled_flag = 1";
                sqlQuery += "   and im.org_id ='" + VMVServices.Web.Utils.OrganizationID + "'";
            }
            else
            {
                sqlQuery = " select PH.PO_NUM,im.item_description,pl.po_line_num,im.item_id,rd.qty_approved";
                sqlQuery += " from INV_RECEIPT_DTLS rd,PO_HEADERS ph,Po_Lines pl,INV_ITEM_MASTER im";
                sqlQuery += " where rd.po_header_id = ph.po_header_id";
                sqlQuery += " and rd.po_line_id = pl.po_line_id";
                sqlQuery += " and rd.item_id = im.item_id";
                sqlQuery += " and rd.receipt_dtl_id = '" + receipt_dtl_id + "'";
                sqlQuery += " and rd.workflow_completion_status = 1";
                sqlQuery += " and rd.enabled_flag = 1";
                sqlQuery += "  and ph.org_id='" + VMVServices.Web.Utils.OrganizationID + "'";
            }

            return sqlQuery;
        }
       
        public static string GetLotNumber()
        {
            sqlQuery = string.Empty;

            sqlQuery = " select (h.lot_number || ' - ' || r.grn_num) as lot_Number,h.lot_id from inv_receipt_lots_hdr h, inv_receipts_hdr r";
            sqlQuery += "  where r.org_id='" + VMVServices.Web.Utils.OrganizationID + "'";

            return sqlQuery;
        }
        public static string GetReceivedLotQty(string receiptId = "")
        {
            sqlQuery = string.Empty;

            sqlQuery = "  select nvl(sum(hh.lot_qty),0) as lot_qty from INV_RECEIPT_LOTS_HDR hh ";
            sqlQuery += " where hh.RECEIPT_DTL_ID='" + receiptId + "'";
            sqlQuery += " and hh.workflow_completion_status=1";
            sqlQuery += " and hh.enabled_flag='1'";
            sqlQuery += " and hh.lot_org_id='" + VMVServices.Web.Utils.OrganizationID + "'";

            return sqlQuery;
        }
    }
}
