﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.AP
{
    public class UOMMasters_DAL
    {

        static string sqlQuery = "";
        public static string getUOMMaster()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT SLVD.CODE AS LOOKUP_ID, SLVD.DESCRIPTION"+ VMVServices.Web.Utils.LanguageCode+" AS LOOKUP_NAME ";
            sqlQuery += " FROM SSM_CODE_MASTERS SLVD ";
            sqlQuery += " WHERE SLVD.PARENT_CODE = 'UC' ";
            sqlQuery += " ORDER BY SLVD.PK_ID ASC ";

            return sqlQuery;
        }
        public static string getUOMDetails()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT IUM.UOM_CODE" + VMVServices.Web.Utils.LanguageCode + " AS UOM_CODE, IUM.UOM_CODE" + VMVServices.Web.Utils.LanguageCode + " ||' - '|| IUM.UOM_DESC" + VMVServices.Web.Utils.LanguageCode + " AS UOM_NAME,IUM.UOM_ID ";
            sqlQuery += " FROM inv_uom_master IUM ";
            sqlQuery += " WHERE IUM.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " AND IUM.ENABLED_FLAG =1 ";
            if (VMVServices.Web.Utils.LanguageCode != string.Empty)
            {
                sqlQuery += "   and IUM.UOM_CODE" + VMVServices.Web.Utils.LanguageCode + "  is not null";
            }
            sqlQuery += " and ium.ORG_ID ='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " ORDER BY UOM_CODE" + VMVServices.Web.Utils.LanguageCode + " ";

            return sqlQuery;
        }
        public static string getUOMBasedItem(string itemId)
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT IUM.UOM_CODE" + VMVServices.Web.Utils.LanguageCode + " AS UOM_CODE, IUM.UOM_CODE" + VMVServices.Web.Utils.LanguageCode + " ||' - '|| IUM.UOM_DESC" + VMVServices.Web.Utils.LanguageCode + " AS UOM_NAME,IUM.UOM_ID ";
            sqlQuery += " FROM INV_UOM_MASTER IUM ,inv_item_master i";
            sqlQuery += " WHERE IUM.WORKFLOW_COMPLETION_STATUS = 1 and ium.uom_id = i.item_uom";
            sqlQuery += " AND IUM.ENABLED_FLAG =1 ";
            sqlQuery += " AND i.ITEM_ID='" + itemId + "'";
            if (VMVServices.Web.Utils.LanguageCode != string.Empty)
            {
                sqlQuery += "   and IUM.UOM_CODE" + VMVServices.Web.Utils.LanguageCode + "  is not null";
            }
            sqlQuery += " and ium.ORG_ID ='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " and i.ORG_ID ='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " ORDER BY UOM_CODE" + VMVServices.Web.Utils.LanguageCode + " ";

            return sqlQuery;
        }

        public static string IsBaseUnit(string uomClass)
        {
            sqlQuery = string.Empty;

            sqlQuery = "  select (nvl(u.base_unit,0)) as base_unit from inv_uom_master u where u.uom_class='" + uomClass + "' and u.ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";


            return sqlQuery;
        }

        public static string get_VW_UOMDetails()
        {
            sqlQuery = string.Empty;
            sqlQuery += " SELECT * FROM VW_UOM V WHERE ROWNUM>0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {

                if (VMVServices.Web.Utils.ReportViewFilterParameter["UOM_ID"] != null)
                {
                    sqlQuery += " AND V.UOM_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["UOM_ID"].ToString() + "'";
                }

                if (VMVServices.Web.Utils.ReportViewFilterParameter["UOM_CLASS_ID"] != null)
                {
                    sqlQuery += " AND V.UOM_CLASS = '" + VMVServices.Web.Utils.ReportViewFilterParameter["UOM_CLASS_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["CREATED_BY"] != null)
                {
                    sqlQuery += " AND UPPER(V.CREATED_BY) = UPPER('" + VMVServices.Web.Utils.ReportViewFilterParameter["CREATED_BY"].ToString() + "')";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["MODIFIED_BY"] != null)
                {
                    sqlQuery += " AND UPPER(V.MODIFIED_BY) = UPPER('" + VMVServices.Web.Utils.ReportViewFilterParameter["MODIFIED_BY"].ToString() + "')";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["CREATED_From_Date"] != null)
                {
                    sqlQuery += " AND V.CREATED_DATE >= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["CREATED_From_Date"].ToString() + "','dd/MM/yyyy') ";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["CREATED_To_Date"] != null)
                {
                    sqlQuery += " AND V.CREATED_DATE <= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["CREATED_To_Date"].ToString() + "','dd/MM/yyyy') ";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["MODIFIED_From_Date"] != null)
                {
                    sqlQuery += " AND V.MODIFIED_DATE >= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["MODIFIED_From_Date"].ToString() + "','dd/MM/yyyy') ";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["MODIFIED_To_Date"] != null)
                {
                    sqlQuery += " AND V.MODIFIED_DATE <= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["MODIFIED_To_Date"].ToString() + "','dd/MM/yyyy') ";
                }
            }
            return sqlQuery;
        }
    }
}
