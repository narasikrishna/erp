﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.AP
{
    public class InvoiceRegister_DAL
    {
        static string sqlQuery = "";
        public static string getReportData(string vendorId = "")
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM INV_REGISTER V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["SEGMENT_ID"] != null)
                {
                    sqlQuery += " AND V.GLOBAL_SEGMENT_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["SEGMENT_ID"].ToString() + "'";
                }
                if (vendorId.Length > 0)
                {
                    sqlQuery += " AND V.VENDOR_ID in (" + vendorId + ")";
                }
                //if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.VENDOR_ID] != null)
                //{
                //    sqlQuery += " AND V.VENDOR_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.VENDOR_ID].ToString() + "'";
                //}
                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
                {
                    sqlQuery += " AND V.INV_DATE between to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy') ";

                    if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
                    {
                        sqlQuery += " AND to_date('" + VMVServices.Web.Utils.ReportFilterParameter["To_Date"].ToString() + "','dd/MM/yyyy') ";
                    }

                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Amt"] != null && VMVServices.Web.Utils.ReportViewFilterParameter["To_Amt"] != null)
                {
                    sqlQuery += " AND V.Transaction_Amount >=" + VMVServices.Web.Utils.ReportViewFilterParameter["From_Amt"];
                    sqlQuery += " AND V.Transaction_Amount <=" + VMVServices.Web.Utils.ReportViewFilterParameter["To_Amt"];
                }
            }
            return sqlQuery;
        }
        public static string getSupplierwiseSummaryReportData()
        {
            sqlQuery = string.Empty;

            //sqlQuery = " select sc.vendor_id,sc.vendor_name,round(sum(nvl(pd.pay_invoice_paid_amt, 0))) as payment_amt,round(sum(nvl(ii.inv_amt, 0))) as invoice_amt, ";
            //sqlQuery += "  fn_Get_Supplier_Opening_Bal(to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy'), sc.vendor_id) as opening_balance from supplier_customers sc, inv_payments_dtl pd,inv_invoices_hdr ii ";
            //sqlQuery += "  where sc.vendor_id = ii.vendor_id ";
            //sqlQuery += " and ii.inv_id=pd.pay_invoice_id (+) ";


            //if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            //{

            //    if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
            //    {
            //        sqlQuery += " AND pd.inv_date >= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy') ";
            //    }
            //    if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
            //    {
            //        sqlQuery += " AND pd.inv_date <= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["To_Date"].ToString() + "','dd/MM/yyyy') ";
            //    }
            //}
            //sqlQuery += " group by sc.vendor_name, sc.vendor_id";
            //sqlQuery += " order by payment_amt desc ";

            sqlQuery += " SELECT * FROM ( ";
            sqlQuery += " SELECT SC.VENDOR_ID,SC.VENDOR_NAME,   NVL(fn_Get_Supplier_Opening_Bal(to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy'), sc.vendor_id),0) as opening_balance ";
            sqlQuery += " ,NVL((SELECT ROUND(SUM(NVL(IH.INV_AMT,0))) FROM INV_INVOICES_HDR IH WHERE IH.VENDOR_ID=SC.VENDOR_ID ";
            sqlQuery += "  AND IH.REV_FLAG IS NULL ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter["FromSupplierID"] != null)
            {
                sqlQuery += " AND SC.VENDOR_ID >= '" + VMVServices.Web.Utils.ReportViewFilterParameter["FromSupplierID"].ToString() + "'";
            }
            if (VMVServices.Web.Utils.ReportViewFilterParameter["ToSupplierID"] != null)
            {
                sqlQuery += " AND SC.VENDOR_ID <= '" + VMVServices.Web.Utils.ReportViewFilterParameter["ToSupplierID"].ToString() + "'";
            }
          
            if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
            {
                sqlQuery += " AND IH.inv_date >= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy') ";
            }
            if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
            {
                sqlQuery += " AND IH.inv_date <= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["To_Date"].ToString() + "','dd/MM/yyyy') ";
            }
            sqlQuery += " ),0) AS invoice_amt ";

            sqlQuery += " ,NVL((SELECT ROUND(SUM(NVL(PH.PAYMENT_AMT,0))) FROM inv_payments_HDR PH WHERE PH.PAY_VENDOR_ID=SC.VENDOR_ID ";
            sqlQuery += "  AND PH.REV_FLAG IS NULL ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter["FromSupplierID"] != null)
            {
                sqlQuery += " AND SC.VENDOR_ID >= '" + VMVServices.Web.Utils.ReportViewFilterParameter["FromSupplierID"].ToString() + "'";
            }
            if (VMVServices.Web.Utils.ReportViewFilterParameter["ToSupplierID"] != null)
            {
                sqlQuery += " AND SC.VENDOR_ID <= '" + VMVServices.Web.Utils.ReportViewFilterParameter["ToSupplierID"].ToString() + "'";
            }
           
            if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
            {
                sqlQuery += " AND PH.PAY_date >= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy') ";
            }
            if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
            {
                sqlQuery += " AND PH.PAY_date <= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["To_Date"].ToString() + "','dd/MM/yyyy') ";
            }
            sqlQuery += " ),0) AS payment_amt ";
            sqlQuery += " FROM SUPPLIER_CUSTOMERS SC ";
            sqlQuery += " ) Z WHERE Z.OPENING_BALANCE > 0 or INVOICE_AMT >0 or PAYMENT_AMT > 0 ";
            //if (VMVServices.Web.Utils.ReportFilterParameter["FromAmount"] != null)
            //{
            //    sqlQuery += " AND Z.payment_amt >=  '" + VMVServices.Web.Utils.ReportFilterParameter["FromAmount"].ToString() + "'";
            //}
            //if (VMVServices.Web.Utils.ReportFilterParameter["ToAmount"] != null)
            //{
            //    sqlQuery += " AND Z.payment_amt <=  '" + VMVServices.Web.Utils.ReportFilterParameter["ToAmount"].ToString() + "'";
            //}
           

            return sqlQuery;
        }

        public static string getInvoiceBalancesReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM INV_REGISTER V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {

                if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.VENDOR_ID] != null)
                {
                    sqlQuery += " AND V.VENDOR_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.VENDOR_ID].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
                {
                    sqlQuery += " AND V.INV_DATE between to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy') ";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
                {
                    sqlQuery += " AND to_date('" + VMVServices.Web.Utils.ReportFilterParameter["To_Date"].ToString() + "','dd/MM/yyyy') ";
                }
                //if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Amt"] != null && VMVServices.Web.Utils.ReportViewFilterParameter["To_Amt"] != null)
                //{
                //    sqlQuery += " AND V.INVOICE_AMOUNT >=" + VMVServices.Web.Utils.ReportViewFilterParameter["From_Amt"];
                //    sqlQuery += " AND V.INVOICE_AMOUNT <=" + VMVServices.Web.Utils.ReportViewFilterParameter["To_Amt"];
                //}
                if (VMVServices.Web.Utils.ReportViewFilterParameter["FROM_INV_NUM"] != null)
                {
                    sqlQuery += " AND V.INV_NUM >=" + VMVServices.Web.Utils.ReportViewFilterParameter["FROM_INV_NUM"];
                   
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["TO_INV_NUM"] != null)
                {
                    sqlQuery += " AND V.INV_NUM <=" + VMVServices.Web.Utils.ReportViewFilterParameter["TO_INV_NUM"];
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["FROM_PAY_AMT"] != null)
                {
                    sqlQuery += " AND V.PAID_AMOUNT >=" + VMVServices.Web.Utils.ReportViewFilterParameter["FROM_PAY_AMT"];

                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["TO_PAY_AMT"] != null)
                {
                    sqlQuery += " AND V.PAID_AMOUNT <=" + VMVServices.Web.Utils.ReportViewFilterParameter["TO_PAY_AMT"];
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["FROM_BAL_AMT"] != null)
                {
                    sqlQuery += " AND V.Balance_Amount >=" + VMVServices.Web.Utils.ReportViewFilterParameter["FROM_BAL_AMT"];

                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["TO_BAL_AMT"] != null)
                {
                    sqlQuery += " AND V.Balance_Amount <=" + VMVServices.Web.Utils.ReportViewFilterParameter["TO_BAL_AMT"];
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["INV_TYPE"] != null)
                {
                    sqlQuery += " AND V.TRANSACTION_TYPE = '" + VMVServices.Web.Utils.ReportViewFilterParameter["INV_TYPE"].ToString() + "'";
                }
            }
            return sqlQuery;
        }
        public static string getGlobalSegment()
        {
            sqlQuery = string.Empty;
            sqlQuery += " SELECT SV.SEGMENT_ID,SV.SEGMENT_VALUE" + VMVServices.Web.Utils.LanguageCode;
            sqlQuery += " FROM GL_SEGMENT_VALUES SV ";
            sqlQuery += " WHERE SV.WORKFLOW_COMPLETION_STATUS='1' ";
            sqlQuery += " AND SV.ENABLED_FLAG='1'";


            return sqlQuery;
        }

        public static string getSupplierwiseSummaryData()
        {
            sqlQuery = string.Empty;

            //sqlQuery = " select sc.vendor_id,sc.vendor_name,to_char(round(sum(nvl(ii.inv_amt, 0)))) as invoice_amt,to_char(sum(X.payment_amt)) as payment_amt , ";
            //sqlQuery += "  to_char((round(sum(nvl(ii.inv_amt, 0))) - round(sum(nvl(payment_amt, 0))))) as balance ";
            ////sqlQuery += " ,to_char(fn_Get_Supplier_Opening_Bal(to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy'), sc.vendor_id)) as opening_balance ";
            //sqlQuery += " from supplier_customers sc, inv_invoices_hdr ii,( select da.pay_invoice_id, sum(pay_invoice_paid_amt) as payment_amt ";
            //sqlQuery += "  from inv_payments_dtl da group by da.pay_invoice_id ) X ";
            //sqlQuery += " where sc.vendor_id = ii.vendor_id ";
            //sqlQuery += " and X.pay_invoice_id = ii.inv_id ";
            //sqlQuery += " and ii.rev_flag is null ";

            //if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            //{
            //    if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
            //    {
            //        sqlQuery += " AND ii.inv_date >= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy') ";
            //    }
            //    if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
            //    {
            //        sqlQuery += " AND ii.inv_date <= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["To_Date"].ToString() + "','dd/MM/yyyy') ";
            //    }
            //}

            //sqlQuery += " group by sc.vendor_name, sc.vendor_id";


            sqlQuery = " select sc.vendor_id,sc.vendor_name,to_char(round(sum(nvl(ii.inv_amt, 0))," + VMVServices.Web.Utils.DecimalPrecision + ")) as invoice_amt,to_char(round(sum(nvl(pd.pay_invoice_paid_amt, 0))," + VMVServices.Web.Utils.DecimalPrecision + ")) as payment_amt, ";
            sqlQuery += " to_char((round(sum(nvl(ii.inv_amt, 0)), " + VMVServices.Web.Utils.DecimalPrecision + ") - round(sum(nvl(pd.pay_invoice_paid_amt, 0))," + VMVServices.Web.Utils.DecimalPrecision + "))) as balance";
            //sqlQuery += " ,to_char(fn_Get_Supplier_Opening_Bal(to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy'), sc.vendor_id)) as opening_balance ";
            sqlQuery += " from supplier_customers sc, inv_payments_dtl pd,inv_invoices_hdr ii ";
            sqlQuery += " where sc.vendor_id = ii.vendor_id ";
            sqlQuery += " and ii.inv_id = pd.pay_invoice_id (+) ";
            sqlQuery += " and ii.rev_flag is null ";
            sqlQuery += " and pd.pay_id in (select pay_id from inv_payments_hdr ph where ph.rev_flag is null ) ";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
                {
                    sqlQuery += " AND ii.inv_date >= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy') ";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
                {
                    sqlQuery += " AND ii.inv_date <= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["To_Date"].ToString() + "','dd/MM/yyyy') ";
                }
            }

            sqlQuery += " group by sc.vendor_name, sc.vendor_id";
            sqlQuery += " order by payment_amt desc ";


            return sqlQuery;
        }

        public static string getSupplierSummaryData(string vendorID)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select sc.vendor_id,sc.vendor_name,";
            sqlQuery += " ii.inv_id,to_date(ii.inv_date) as inv_date,pd.pay_id,";
            sqlQuery += " to_char(round(nvl(ii.inv_amt, 0))) as invoice_amt,";
            sqlQuery += " to_char(round(nvl(pd.pay_invoice_paid_amt, 0))) as payment_amt";
            sqlQuery += " from supplier_customers sc, inv_payments_dtl pd,inv_invoices_hdr ii";
            sqlQuery += " where sc.vendor_id = ii.vendor_id";
            sqlQuery += " and ii.inv_id=pd.pay_invoice_id (+) ";
            sqlQuery += " and ii.rev_flag is null ";
            sqlQuery += " and pd.pay_id in (select pay_id from inv_payments_hdr ph where ph.rev_flag is null ) ";
            sqlQuery += " and sc.vendor_id='" + vendorID + "'";
            sqlQuery += " order by ii.inv_id ";


            /// Old Query
            //sqlQuery = " select sc.vendor_id,sc.vendor_name,";
            //sqlQuery += " ii.inv_id,to_date(ii.inv_date) as inv_date,pd.pay_id,";
            //sqlQuery += " to_char(round(nvl(ii.inv_amt, 0))) as invoice_amt,";
            //sqlQuery += " to_char(round(nvl(hh.payment_amt, 0))) as payment_amt";
            //sqlQuery += " from supplier_customers sc, inv_payments_hdr hh, inv_payments_dtl pd,inv_invoices_hdr ii";
            //sqlQuery += " where sc.vendor_id = hh.pay_vendor_id ";
            //sqlQuery += " and pd.pay_id = hh.pay_id ";
            //sqlQuery += " and ii.inv_id=pd.pay_invoice_id ";
            //sqlQuery += " and sc.vendor_id='" + vendorID + "'";
            //sqlQuery += " order by ii.inv_id ";

            return sqlQuery;
        }

        public static string getAccountPayableData()
        {
            sqlQuery = string.Empty;

            //sqlQuery = " select ACCOUNTPAYABLEID,TYPE,VENDORID,to_char(AMOUNT) as AMOUNT,CURRENCYID,DueDate,to_char(PAIDAMOUNT) as PAIDAMOUNT,STATUS,STATUSDATE";
            //sqlQuery += " ,REMARKS_EN,REMARKS_AR,COMPONENTID,PAYMENTMETHOD,BANKID,PAYMENTNO,CHECKID,PAYMENTDATE ,MERGEDAPID,SPLITTEDAPID,CUSERID,CDATE ";
            //sqlQuery += " ,MDATE,CASE DisablePartiallyPaid WHEN 'TRUE' THEN '1' ELSE '0' END as DisablePartiallyPaid,CANCELDATE,INVOICEDATE ,ACCOUNTPAYABLENUMBER";
            //sqlQuery += " from accountpayable";
            //sqlQuery += " order by ACCOUNTPAYABLEID asc ";

            //sqlQuery = " select v.vendornumber, v.name_en,  sc.description,  t.accountpayableid,t.invoicedate as invoicedate,t.DueDate as DueDate, to_char(t.amount) as amount";
            //sqlQuery += " ,(case t.MIGRATED when '1' then 'TRUE' ELSE 'FALSE' END) AS MIGRATED";
            //sqlQuery += " ,scp.InvoiceNumber";
            //sqlQuery += " from ACCOUNTPAYABLE t, ServiceContractPayment scp, dm_intf_vendor_mapping v, ssm_code_masters sc";
            //sqlQuery += " where v.personid = t.vendorid";
            //sqlQuery += " and sc.code = to_char(t.type)";
            //sqlQuery += " and sc.parent_code='INV_TY' ";
            //sqlQuery += " and (t.migrated is null or t.migrated = '0')";
            //sqlQuery += " and t.accountpayableid = scp.PaymentId";
            //sqlQuery += " union all";
            //sqlQuery += " select v.vendornumber, v.name_en,  sc.description,  t.accountpayableid,t.invoicedate as invoicedate,t.DueDate as DueDate, to_char(t.amount) as amount";
            //sqlQuery += " ,(case t.MIGRATED when '1' then 'TRUE' ELSE 'FALSE' END) AS MIGRATED";
            //sqlQuery += " ,scp.InvoiceNumber";
            //sqlQuery += " from ACCOUNTPAYABLE t, GenericServiceContractPayment scp, dm_intf_vendor_mapping v, ssm_code_masters sc";
            //sqlQuery += " where v.personid = t.vendorid";
            //sqlQuery += " and sc.code = to_char(t.type)";
            //sqlQuery += " and sc.parent_code='INV_TY' ";
            //sqlQuery += " and (t.migrated is null or t.migrated = '0')";
            //sqlQuery += " and t.accountpayableid = scp.PaymentId";
            //sqlQuery += " order by ACCOUNTPAYABLEID asc";


            sqlQuery = " select '' as ModifyURL, v.vendornumber, v.name_en,  sc.description,  t.accountpayableid,t.invoicedate as invoicedate,t.DueDate as DueDate, to_char(t.amount) as amount";
            sqlQuery += " ,(case t.MIGRATED when '1' then 'TRUE' ELSE 'FALSE' END) AS MIGRATED";
            sqlQuery += " ,t.accountpayableid as InvoiceNumber";
            sqlQuery += "  ,(select ie.error_message from gl_intf_errors ie where ie.error_in = t.accountpayableid and rownum=1) as error_message ";
            sqlQuery += " from ACCOUNTPAYABLE t, dm_intf_vendor_mapping v, ssm_code_masters sc";
            sqlQuery += " where v.personid = t.vendorid";
            sqlQuery += " and sc.code = to_char(t.type)";
            sqlQuery += " and sc.parent_code='INV_TY' ";
            sqlQuery += " and (t.migrated is null or t.migrated = '0')";

            return sqlQuery;
        }
    }
}
