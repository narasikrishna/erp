﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.AP
{
    public class ServiceContract_DAL
    {
        static string sqlQuery = "";
        public static string getServiceContractDetails(string str_RecId)
        {
            string str_Query = string.Empty;
            str_Query += " select serv_contr_dtl_id,serv_contr_id,serv_contr_dtl_from_date,serv_contr_dtl_to_date,serv_contr_dtl_days,serv_contr_inv_status,serv_contr_pay_status ";
            str_Query += " ,'N' as DELETED ";
            str_Query += " ,PERCENTAGE as SERV_CONTR_PERCENTAGE ";
            str_Query += " from ap_service_contract_dtl ";
            str_Query += " where serv_contr_id='" + str_RecId + "'";
            return str_Query;
        }

        public static string getServiceContractReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM vw_ap_service_contract V WHERE ROWNUM > 0 ";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                //if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
                //{
                //    sqlQuery += " AND V.INV_DATE = to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy') ";
                //}
                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null && VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
                {
                    sqlQuery += " AND V.serv_contr_date between to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy') ";

                    if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
                    {
                        sqlQuery += " AND to_date('" + VMVServices.Web.Utils.ReportFilterParameter["To_Date"].ToString() + "','dd/MM/yyyy') ";
                    }
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.VENDOR_ID] != null)
                {
                    sqlQuery += " AND V.VENDOR_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.VENDOR_ID].ToString() + "'";
                }
                //if (VMVServices.Web.Utils.ReportViewFilterParameter["FromAmount"] != null && VMVServices.Web.Utils.ReportViewFilterParameter["ToAmount"] != null)
                //{
                //    sqlQuery += " AND V.inv_item_amt >= " + VMVServices.Web.Utils.ReportViewFilterParameter["FromAmount"];
                //    sqlQuery += " AND V.inv_item_amt <= " + VMVServices.Web.Utils.ReportViewFilterParameter["ToAmount"];

                //    //sqlQuery += " AND V.inv_item_amt >=to_number(trunc(" + VMVServices.Web.Utils.ReportViewFilterParameter["From_Amt"] + "))";
                //    //sqlQuery += " AND V.inv_item_amt <=to_number(trunc(" + VMVServices.Web.Utils.ReportViewFilterParameter["To_Amt"] + "))";
                //}
                if (VMVServices.Web.Utils.ReportViewFilterParameter["serv_contr_id"] != null)
                {
                    sqlQuery += " AND V.serv_contr_id ='" + VMVServices.Web.Utils.ReportFilterParameter["serv_contr_id"].ToString() + "'";
                }
            }
            return sqlQuery;
        }

        public static string getServiceContractSummaryReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM vw_ap_service_contract V WHERE ROWNUM > 0 ";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                //if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
                //{
                //    sqlQuery += " AND V.INV_DATE = to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy') ";
                //}
                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null && VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
                {
                    sqlQuery += " AND V.serv_contr_date between to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy') ";

                    if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
                    {
                        sqlQuery += " AND to_date('" + VMVServices.Web.Utils.ReportFilterParameter["To_Date"].ToString() + "','dd/MM/yyyy') ";
                    }
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.VENDOR_ID] != null)
                {
                    sqlQuery += " AND V.VENDOR_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.VENDOR_ID].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["SITE_ID"] != null)
                {
                    sqlQuery += " AND V.vendor_site_id = '" + VMVServices.Web.Utils.ReportViewFilterParameter["SITE_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["CONTRACT_TYPE"] != null)
                {
                    sqlQuery += " AND V.serv_contr_type = '" + VMVServices.Web.Utils.ReportViewFilterParameter["CONTRACT_TYPE"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["PAYMENT_TYPE"] != null)
                {
                    sqlQuery += " AND V.serv_contr_pay_type = '" + VMVServices.Web.Utils.ReportViewFilterParameter["PAYMENT_TYPE"].ToString() + "'";
                }
                //if (VMVServices.Web.Utils.ReportViewFilterParameter["FromAmount"] != null && VMVServices.Web.Utils.ReportViewFilterParameter["ToAmount"] != null)
                //{
                //    sqlQuery += " AND V.inv_item_amt >= " + VMVServices.Web.Utils.ReportViewFilterParameter["FromAmount"];
                //    sqlQuery += " AND V.inv_item_amt <= " + VMVServices.Web.Utils.ReportViewFilterParameter["ToAmount"];

                //    //sqlQuery += " AND V.inv_item_amt >=to_number(trunc(" + VMVServices.Web.Utils.ReportViewFilterParameter["From_Amt"] + "))";
                //    //sqlQuery += " AND V.inv_item_amt <=to_number(trunc(" + VMVServices.Web.Utils.ReportViewFilterParameter["To_Amt"] + "))";
                //}
                if (VMVServices.Web.Utils.ReportViewFilterParameter["serv_contr_id"] != null)
                {
                    sqlQuery += " AND V.serv_contr_id ='" + VMVServices.Web.Utils.ReportFilterParameter["serv_contr_id"].ToString() + "'";
                }
            }
            return sqlQuery;
        }
        public static string getContractNumber()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT sc.serv_contr_id,sc.serv_contr_num ";
            sqlQuery += " FROM ap_service_contract SC";
            sqlQuery += " where sc.workflow_completion_status='1'";
           
            sqlQuery += " AND SC.ENABLED_FLAG='1'";
            return sqlQuery;
        }
    }
}
