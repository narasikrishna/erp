﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.AP
{
    public class transactionLedger_DAL
    {
        static string sqlQuery = "";
        public static string GetTransLedgerDtl(string inv_item_id, string inv_wh_id)
        {
            sqlQuery = string.Empty;

            //sqlQuery += " select distinct tl.inv_trans_type,im.item_name,tl.inv_qty,r.grn_num||r.receipt_date as receipt,tl.inv_trans_date,w.inv_wh_name,";
            //sqlQuery += " (select ph.po_num from PO_HEADERS ph where tl.inv_po_header_id = ph.po_header_id) as po_num,";
            //sqlQuery += " (select rlh.lot_number from inv_receipt_lots_hdr rlh where rlh.lot_id=tl.inv_lot_id) lot_number";
            //sqlQuery += " from inv_transaction_ledger tl,INV_ITEM_MASTER im,Inv_Warehouses w,";
            //sqlQuery += " INV_RECEIPTS_HDR r,PO_HEADERS ph";
            //sqlQuery += " where tl.inv_item_id = im.item_id";
            //sqlQuery += " and tl.inv_wh_id = w.inv_wh_id";
            //sqlQuery += " and tl.receipt_id = r.receipt_id";           
            //sqlQuery += " and tl.inv_trans_type = '" + inv_trans_type + "'";
            //if (inv_item_id != "")
            //{
            //    sqlQuery += " and tl.inv_item_id = '" + inv_item_id + "'";
            //}
            //if (inv_wh_id != "")
            //{
            //    sqlQuery += " and tl.inv_wh_id = '" + inv_wh_id + "'";
            //}
            sqlQuery += " select distinct tl.inv_trans_type,tl.inv_qty,tl.INV_QOH,";
            sqlQuery += " im.item_name,w.inv_wh_name,tl.inv_trans_date  ";
            sqlQuery += " from inv_transaction_ledger tl,Inv_Warehouses w,inv_item_master im";
            sqlQuery += " where tl.workflow_completion_status = 1";
            // sqlQuery += " and tl.org_id = '" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " and tl.inv_wh_id = w.inv_wh_id";
            sqlQuery += " and  tl.inv_item_id = im.item_id";
            if (inv_item_id != "")
            {
                sqlQuery += " and  tl.inv_item_id = '" + inv_item_id + "'";

            }
            if (inv_wh_id != "")
            {
                sqlQuery += " and tl.inv_wh_id = '" + inv_wh_id + "'";
            }
            sqlQuery += " order by im.item_name,tl.inv_trans_date,w.inv_wh_name";

            return sqlQuery;
        }



    }
}
