﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.AP
{
    public class ItemOtherCost_DAL
    {
        static string sqlQuery = "";

        public static string GetItemOtherCostDetails(string costId)
        {
            sqlQuery = string.Empty;

            sqlQuery = "  select ic.inv_item_cost_id,";
            sqlQuery += "     ic.inv_item_cost_type as LOOKUP_ID,";
            sqlQuery += "     ic.inv_item_id,";
            sqlQuery += "     ic.inv_receipt_hdr_id,";
            sqlQuery += "     ic.inv_receipt_line_id,";
            sqlQuery += "     to_char(ic.inv_charge_amt) as inv_charge_amt,'N' as DELETED,";
            sqlQuery += "    (select ss.code";
            sqlQuery += "    from ssm_code_masters ss";
            sqlQuery += "     where ss.parent_code = 'CT'";
            sqlQuery += "    and ss.code = ic.inv_item_cost_type) as LOOKUP_NAME";
            sqlQuery += "     from inv_item_costing ic where ic.inv_item_cost_id ='" + costId + "'";

            return sqlQuery;
        }


        public static string GetItemOtherCostDtl_frGrn_ln_no(string inv_item_id, string inv_receipt_line_id)
        {
            sqlQuery = string.Empty;

            sqlQuery += " select ic.inv_item_cost_id,";
            sqlQuery += " ic.inv_item_cost_type as LOOKUP_ID,";
            sqlQuery += " ic.inv_item_id,ic.inv_receipt_hdr_id,";
            sqlQuery += " ic.inv_receipt_line_id,to_char(ic.inv_charge_amt) as inv_charge_amt,'N' as DELETED,";
            sqlQuery += " (select ss.code from ssm_code_masters ss";
            sqlQuery += " where ss.parent_code = 'CT'";
            sqlQuery += " and ss.code = ic.inv_item_cost_type) as LOOKUP_NAME";
            sqlQuery += " from inv_item_costing ic ";
            sqlQuery += " where ic.INV_RECEIPT_HDR_ID = '" + inv_item_id + "'";
            sqlQuery += " and ic.inv_receipt_line_id = '" + inv_receipt_line_id + "'";
            return sqlQuery;
        }



    }
}
