﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.AP
{
    public class Terms_DAL
    {
        static string sqlQuery = "";
        public static string getTermName()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT TH.TERM_ID,TH.TERM_NAME" + VMVServices.Web.Utils.LanguageCode + " as  TERM_NAME";
            sqlQuery += " FROM inv_pay_terms_hdr TH ";
            sqlQuery += " WHERE TH.ENABLED_FLAG = 1 ";
            sqlQuery += " AND TH.WORKFLOW_COMPLETION_STATUS = 1 ";
            if (VMVServices.Web.Utils.LanguageCode != string.Empty)
            {
                sqlQuery += " AND  TH.TERM_NAME" + VMVServices.Web.Utils.LanguageCode + "  is not null ";
            }
            sqlQuery += "  and th.ORG_ID = '" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " ORDER BY TERM_NAME ";

            return sqlQuery;
        }


        public static string GetTermdtls(String term_id)
        {
            sqlQuery = string.Empty;


            sqlQuery = " select td.term_dtl_id,td.term_cr_days,C.CODE AS VALUE_KEY_ID,C.CODE AS VALUE_NAME,to_char(td.term_amt) as term_amt,td.term_discount_days,";
            sqlQuery += " td.term_discount_type,to_char(td.term_discount_amt) as term_discount_amt,'N' AS DELETED";
            sqlQuery += " from iNV_PAY_TERMS_DTL td,ssm_code_masters c";
            sqlQuery += " where td.term_id =  '" + term_id + "'";
            sqlQuery += " and td.term_type = c.code";
            sqlQuery += " and td.workflow_completion_status = 1";
            sqlQuery += " order by term_dtl_id ";


            return sqlQuery;

        }



    }
}
