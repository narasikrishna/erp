﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.AP
{
    public class Payment_DAL
    {
        static string sqlQuery = "";

        public static string getPaymentDetails(string StrID)
        {
            sqlQuery = string.Empty;

            sqlQuery = "  select d.attribute1 as line_no,";
            sqlQuery += "  d.pay_dtl_id,";
            sqlQuery += "     d.pay_id,iv.inv_id,";
            sqlQuery += "    d.pay_invoice_id,";
            sqlQuery += "    to_char(d.pay_invoice_amt) as pay_invoice_amt,";
            sqlQuery += "    to_char(d.pay_invoice_paid_amt) as pay_invoice_paid_amt,";
            sqlQuery += "    '0' as amount_to_pay,";
            sqlQuery += "    iv.inv_num,";
            sqlQuery += "   iv.inv_date,to_char(iv.INV_RETENTION_AMT) as INV_RETENTION_AMT,";
            sqlQuery += "   to_char(iv.inv_amt) as inv_amt,'N' as deleted";
            sqlQuery += " ,d.attribute4 as INV_PAID_AMT ";
            sqlQuery += " ,to_char(d.PAY_RETENTIION_AMT) as PAY_RETENTIION_AMT ";
            sqlQuery += "   from inv_payments_dtl d, inv_invoices_hdr iv";
            sqlQuery += "    where d.pay_invoice_id=iv.inv_id";
            //     sqlQuery += "     and d.enabled_flag='1'";
            sqlQuery += "  and d.workflow_completion_status='1'";
            sqlQuery += "  and d.pay_id='" + StrID + "'";
            sqlQuery += " order by PAY_DTL_ID ";


            return sqlQuery;

        }

        public static string getPaymentOtherCharges(string str_ID)
        {
            sqlQuery = string.Empty;
            sqlQuery += "SELECT POC_ID ,PAY_ID,POC.Attribute1 as ACCT_CODE_ID ,POC.Attribute2 as AddSub_value, to_char(ROUND(POC_AMOUNT," + VMVServices.Web.Utils.DecimalPrecision + ")) as POC_AMOUNT, REMARKS,GAC.ACCT_CODE ||' - '|| GAC.ACCT_CODE_DESC" + VMVServices.Web.Utils.LanguageCode + " as ACCT_CODE_DESC,'N' as DELETED ";
            sqlQuery += " FROM INV_PAYMENTS_OTHER_CHARGES POC";
            sqlQuery += " INNER JOIN GL_ACCT_CODES GAC ON GAC.ACCT_CODE_ID= POC.ATTRIBUTE1 ";
            sqlQuery += " where PAY_ID='" + str_ID + "'";
            return sqlQuery;
        }
        public static string getInvoiceDetails(string currId, string supplierId = "")
        {
            sqlQuery = string.Empty;

            sqlQuery += "     select h.inv_id,h.inv_num,";
            sqlQuery += "    h.vendor_id,";
            sqlQuery += "    h.inv_acct_code_id,";
            sqlQuery += "    h.inv_retention_amt,";
            sqlQuery += "   h.global_segment_id,";
            sqlQuery += "   h.inv_pay_curr_code";
            sqlQuery += "  from inv_invoices_hdr h";
            sqlQuery += "   where h.inv_pay_curr_code='" + currId + "'";
            sqlQuery += "  and  h.workflow_completion_status='1'";
            sqlQuery += "  and h.enabled_flag='1'";
            sqlQuery += "  and h.VENDOR_ID='" + supplierId + "'";
            return sqlQuery;

        }


        public static string getTotalPaymentAmt4Invoice(string str_inv_id)
        {
            sqlQuery = string.Empty;
            sqlQuery += "select NVL(sum(ipd.PAY_INVOICE_PAID_AMT),0) + NVL(sum(ipd.PAY_RETENTIION_AMT),0) as inv_paid_amt from INV_PAYMENTS_DTL ipd,inv_payments_hdr iph WHERE iph.pay_id=ipd.pay_id  and ipd.PAY_INVOICE_ID='" + str_inv_id + "' and REV_JE_HDR_ID is null";
            return sqlQuery;
        }

        public static string getPaymentRegisterReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_PAYABLE_SYSTEM V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {

                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
                {
                    sqlQuery += " AND V.PAY_DATE between to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy') ";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
                {
                    sqlQuery += " AND to_date('" + VMVServices.Web.Utils.ReportFilterParameter["To_Date"].ToString() + "','dd/MM/yyyy') ";
                }
            }
            return sqlQuery;
        }


        public static string getSupplierwisePaymentReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_PAYABLE_SYSTEM V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["VENDOR_NAME"] != null)
                {
                    sqlQuery += " AND V.vendor_id = '" + VMVServices.Web.Utils.ReportViewFilterParameter["VENDOR_NAME"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
                {
                    sqlQuery += " AND V.PAY_DATE between to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy') ";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
                {
                    sqlQuery += " AND to_date('" + VMVServices.Web.Utils.ReportFilterParameter["To_Date"].ToString() + "','dd/MM/yyyy') ";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["PAYMENT_NUMBER"] != null)
                {
                    sqlQuery += " AND V.PAY_ID ='" + VMVServices.Web.Utils.ReportFilterParameter["PAYMENT_NUMBER"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["BANK_ID"] != null)
                {
                    sqlQuery += " AND V.BANK_NAME ='" + VMVServices.Web.Utils.ReportFilterParameter["BANK_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Amt"] != null && VMVServices.Web.Utils.ReportViewFilterParameter["To_Amt"] != null)
                {
                    sqlQuery += " AND V.AMOUNT_PAID >=to_number(trunc(" + VMVServices.Web.Utils.ReportViewFilterParameter["From_Amt"] + "))";
                    sqlQuery += " AND V.AMOUNT_PAID <=to_number(trunc(" + VMVServices.Web.Utils.ReportViewFilterParameter["To_Amt"] + "))";
                }
            }
            return sqlQuery;
        }

        public static string GetTop5SupplierAmt()
        {
            sqlQuery = string.Empty;

            sqlQuery += "  select sc.vendor_name,round(sum(nvl(hh.payment_amt,0)),'" + VMVServices.Web.Utils.DecimalPrecision + "') as payment_amt,sc.vendor_id from supplier_customers sc,inv_payments_hdr hh";
            sqlQuery += "  where sc.vendor_id=hh.pay_vendor_id";
            sqlQuery += "  and rownum<6";
            sqlQuery += "  group by sc.vendor_name,sc.vendor_id";
            sqlQuery += "  order by payment_amt desc";

            return sqlQuery;
        }

        public static string getTop5SupplierAged(string str_FirstBuget, string str_SecondBuget, string str_ThirdBuget)
        {
            sqlQuery = string.Empty;

            sqlQuery += " SELECT PAY_VENDOR_ID,SC.VENDOR_NAME ";
            sqlQuery += " ,Round((SELECT NVL(SUM(PAYMENT_AMT),0) FROM INV_PAYMENTS_HDR IPH WHERE IPH.PAY_VENDOR_ID=Z.PAY_VENDOR_ID AND IPH.PAY_DATE > SYSDATE -" + str_FirstBuget + " and IPH.PAY_DATE <= SYSDATE )," + VMVServices.Web.Utils.DecimalPrecision + ") AS FIRST_BUGET ";
            sqlQuery += " ,ROUND((SELECT NVL(SUM(PAYMENT_AMT),0) FROM INV_PAYMENTS_HDR IPH WHERE IPH.PAY_VENDOR_ID=Z.PAY_VENDOR_ID AND IPH.PAY_DATE > SYSDATE -" + str_SecondBuget + " and IPH.PAY_DATE <= SYSDATE-" + str_FirstBuget + " )," + VMVServices.Web.Utils.DecimalPrecision + ")  AS SECOND_BUGET";
            sqlQuery += " ,ROUND((SELECT NVL(SUM(PAYMENT_AMT),0) FROM INV_PAYMENTS_HDR IPH WHERE IPH.PAY_VENDOR_ID=Z.PAY_VENDOR_ID AND IPH.PAY_DATE > SYSDATE -" + str_ThirdBuget + " and IPH.PAY_DATE <= SYSDATE -" + str_SecondBuget + " )," + VMVServices.Web.Utils.DecimalPrecision + ")  AS THIRD_BUGET ";
            sqlQuery += " ,ROUND((SELECT NVL(SUM(PAYMENT_AMT),0) FROM INV_PAYMENTS_HDR IPH WHERE IPH.PAY_VENDOR_ID=Z.PAY_VENDOR_ID AND  IPH.PAY_DATE <= SYSDATE-" + str_ThirdBuget + " )," + VMVServices.Web.Utils.DecimalPrecision + ")  AS LAST_BUGET ";
            sqlQuery += "  FROM ( ";
            sqlQuery += " SELECT   PAY_VENDOR_ID FROM INV_PAYMENTS_HDR PH    ";
            sqlQuery += " GROUP BY PAY_VENDOR_ID ORDER BY SUM(PAYMENT_AMT) DESC ";
            sqlQuery += " ) Z ";
            sqlQuery += " INNER JOIN SUPPLIER_CUSTOMERS SC ON SC.VENDOR_ID= Z.PAY_VENDOR_ID ";
            sqlQuery += "  WHERE ROWNUM<=5 ";
            return sqlQuery;
        }
        public static string GetInvoicePaidAmt()
        {
            sqlQuery = string.Empty;

            sqlQuery += "   select sc.vendor_id,";
            sqlQuery += "   sc.vendor_name,";
            sqlQuery += "    round(sum(nvl(hh.payment_amt, 0)),'" + VMVServices.Web.Utils.DecimalPrecision + "') as payment_amt,";
            sqlQuery += "    round(sum(nvl(ii.inv_amt, 0)), '" + VMVServices.Web.Utils.DecimalPrecision + "') as invoice_amt";
            sqlQuery += "  from supplier_customers sc, inv_payments_hdr hh, inv_payments_dtl pd,inv_invoices_hdr ii";
            sqlQuery += "   where sc.vendor_id = hh.pay_vendor_id";
            sqlQuery += "   and pd.pay_id = hh.pay_id";
            sqlQuery += "  and ii.inv_id=pd.pay_invoice_id";
            sqlQuery += "  group by sc.vendor_name, sc.vendor_id";
            sqlQuery += "  order by payment_amt desc";

            return sqlQuery;
        }

        public static string getSierraSupplierReceipt()
        {
            sqlQuery = string.Empty;

            sqlQuery = " select distinct v.Supplier_Name, v.Supplier_ID || ' - ' || v.Supplier_Name as Suplr_Name, sc.description";
            sqlQuery += " ,t.Payment_ID, t.Payment_Number, t.Payment_Date, t.Customer_ID, to_char(t.Payment_Amount) as Payment_Amount, t.Invoice_ID ";
            sqlQuery += " ,(case t.POSTED when '1' then 'TRUE' ELSE 'FALSE' END) AS POSTED";
            sqlQuery += " , si.Invoice_Number";
            sqlQuery += " from DM_SR_SUPPLIER_PAYMENT@CRCINTERFACE t, DM_SR_SUPPLIER@CRCINTERFACE v, DM_SR_SUPPLIER_INVOICE@CRCINTERFACE si, ssm_code_masters sc";
            sqlQuery += " where v.Supplier_ID = t.Customer_ID";
            sqlQuery += " and si.Invoice_ID = t.Invoice_ID";
            sqlQuery += " and (t.posted is null or t.posted = '0')";
            sqlQuery += " and sc.code='Miscellaneous' and sc.parent_code='LINETYPE'";
            sqlQuery += " order by t.Payment_Number asc";

            return sqlQuery;
        }


        public static string getPaymentReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_PAYMENT_REPORT V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {

              

                if (VMVServices.Web.Utils.ReportViewFilterParameter["PAY_ID"] != null)
                {
                    sqlQuery += " AND V.PAY_ID ='" + VMVServices.Web.Utils.ReportFilterParameter["PAY_ID"].ToString() + "'";
                }
                


            }
            return sqlQuery;
        }
    }
}
