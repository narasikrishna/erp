﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL
{
    public class Supplier_DAL
    {

        static string sqlQuery = "";


        public static string getSupplierCustomer4Id(string str_id)
        {
            sqlQuery = string.Empty;
            sqlQuery += " SELECT * FROM SUPPLIER_CUSTOMERS WHERE VENDOR_ID='" + str_id + "'";
            return sqlQuery;
        }
        public static string getSupplierType()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT SLVD.CODE AS LOOKUP_ID, SLVD.CODE AS LOOKUP_NAME ";
            sqlQuery += " FROM SSM_CODE_MASTERS SLVD ";
            sqlQuery += " WHERE SLVD.PARENT_CODE = 'ST'";
            sqlQuery += "    and lower(slvd.code)<>'customer'";
            sqlQuery += " ORDER BY CODE ";

            return sqlQuery;

        }

        public static string GetSupplierName_ForArInvoice(string supNo = "")
        {
            sqlQuery = string.Empty;

            sqlQuery = "  select t.vendor_id,T.VENDOR_CODE ||' - '||t.vendor_name" + VMVServices.Web.Utils.LanguageCode + "  as vendor_name,T.VENDOR_CODE,T.VENDOR_NAME from supplier_customers t";
            sqlQuery += " WHERE t.WORKFLOW_COMPLETION_STATUS = 1  and (upper(t.vendor_type)='CUSTOMER' or upper(t.vendor_type)='BOTH') ";
            if (supNo != string.Empty)
            {
                sqlQuery += " AND t.vendor_id ='" + supNo + "'";
            }
            if (VMVServices.Web.Utils.LanguageCode != string.Empty)
            {
                sqlQuery += " AND  t.vendor_name" + VMVServices.Web.Utils.LanguageCode + "  is not null ";
            }
            sqlQuery += " AND t.ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " AND t.ENABLED_FLAG = 1 ";
            sqlQuery += " ORDER BY t.vendor_id asc ";
            return sqlQuery;

        }
        public static string getCustomerType()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT SLVD.CODE AS LOOKUP_ID, SLVD.CODE AS LOOKUP_NAME ";
            sqlQuery += " FROM SSM_CODE_MASTERS SLVD ";
            sqlQuery += " WHERE SLVD.PARENT_CODE = 'ST'";
            sqlQuery += "  and lower(slvd.code)<>'supplier'";
            sqlQuery += " ORDER BY CODE ";

            return sqlQuery;

        }
        public static string getSupplierStatus()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT SLVD.CODE AS LOOKUP_ID, SLVD.CODE AS LOOKUP_NAME ";
            sqlQuery += " FROM SSM_CODE_MASTERS SLVD ";
            sqlQuery += " WHERE SLVD.PARENT_CODE = 'SS'";
            sqlQuery += " ORDER BY CODE ";

            return sqlQuery;

        }

        public static string getCountry()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT SCO.COUNTRY_CODE AS COUNTRY_CODE, SCO.COUNTRY_CODE ||' - '|| SCO.FULL_NAME" + VMVServices.Web.Utils.LanguageCode + " AS COUNTRY_NAME,sco.NATIONALITY";
            sqlQuery += " FROM SSM_COUNTRIES SCO ";
            sqlQuery += " WHERE SCO.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " AND SCO.ENABLED_FLAG = 1 ";
            sqlQuery += " ORDER BY COUNTRY_CODE ";

            return sqlQuery;

        }

        public static string getState(string COUNTRY_CODE)
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT SST.STATE_CODE AS STATE_CODE, SST.STATE_CODE ||' - '|| SST.FULL_NAME" + VMVServices.Web.Utils.LanguageCode + " AS STATE_NAME ";
            sqlQuery += " FROM SSM_STATES SST ";
            sqlQuery += " WHERE SST.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " AND SST.ENABLED_FLAG = 1 ";
            sqlQuery += " AND SST.COUNTRY_CODE = '" + COUNTRY_CODE + "'";
            sqlQuery += " ORDER BY STATE_CODE ";

            return sqlQuery;
        }

        public static string getCity(string STATE_CODE)
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT SCI.CITY_CODE AS CITY_CODE, SCI.CITY_CODE ||' - '|| SCI.FULL_NAME" + VMVServices.Web.Utils.LanguageCode + " AS CITY_NAME ";
            sqlQuery += " FROM SSM_CITIES SCI ";
            sqlQuery += " WHERE SCI.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " AND SCI.ENABLED_FLAG = 1 ";
            sqlQuery += " AND SCI.STATE_CODE = '" + STATE_CODE + "'";
            sqlQuery += " ORDER BY CITY_CODE ";

            return sqlQuery;

        }
        public static string getCit(string STATE_CODE, string COUNTRY_CODE)
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT SCI.CITY_CODE AS CITY_CODE, SCI.CITY_CODE ||' - '|| SCI.FULL_NAME" + VMVServices.Web.Utils.LanguageCode + " AS CITY_NAME ";
            sqlQuery += " FROM SSM_CITIES SCI ";
            sqlQuery += " WHERE SCI.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " AND SCI.ENABLED_FLAG = 1 ";
            sqlQuery += " AND SCI.STATE_CODE = '" + STATE_CODE + "'";
            sqlQuery += " AND SCI.COUNTRY_CODE = '" + COUNTRY_CODE + "'";
            sqlQuery += " ORDER BY CITY_CODE ";

            return sqlQuery;

        }

        public static string GetSupplierName(string supNo = "")
        {
            sqlQuery = string.Empty;

            sqlQuery = "  select t.vendor_id,T.VENDOR_CODE ||' - '||t.vendor_name" + VMVServices.Web.Utils.LanguageCode + "  as vendor_name,T.VENDOR_CODE from supplier_customers t";
            sqlQuery += " WHERE t.WORKFLOW_COMPLETION_STATUS = 1  and (lower(t.vendor_type)='supplier' or lower(t.vendor_type)='both') ";
            if (supNo != string.Empty)
            {
                sqlQuery += " AND t.vendor_id ='" + supNo + "'";
            }
            if (VMVServices.Web.Utils.LanguageCode != string.Empty)
            {
                sqlQuery += " AND  t.vendor_name" + VMVServices.Web.Utils.LanguageCode + "  is not null ";
            }
            sqlQuery += " AND t.ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " AND t.ENABLED_FLAG = 1 ";
            sqlQuery += " ORDER BY t.vendor_name asc ";
            return sqlQuery;

        }
        public static string GetCustomerName(string supNo = "")
        {
            sqlQuery = string.Empty;

            sqlQuery = "  select t.vendor_id,T.VENDOR_CODE ||' - '||t.vendor_name" + VMVServices.Web.Utils.LanguageCode + "  as vendor_name,T.VENDOR_CODE from supplier_customers t";
            sqlQuery += " WHERE t.WORKFLOW_COMPLETION_STATUS = 1  and (lower(t.vendor_type)='customer' or lower(t.vendor_type)='both') ";
            if (supNo != string.Empty)
            {
                sqlQuery += " AND t.vendor_id ='" + supNo + "'";
            }

            if (VMVServices.Web.Utils.LanguageCode != string.Empty)
            {
                sqlQuery += " AND  t.vendor_name" + VMVServices.Web.Utils.LanguageCode + "  is not null ";
            }

            sqlQuery += " AND t.ENABLED_FLAG = 1 ";

            sqlQuery += " ORDER BY t.vendor_name asc ";
            return sqlQuery;

        }
        public static string GetCustomerNameDetails()
        {
            sqlQuery = string.Empty;

            sqlQuery = "  select t.vendor_id,t.vendor_name" + VMVServices.Web.Utils.LanguageCode + " as vendor_name  from supplier_customers t";
            sqlQuery += " WHERE t.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " AND t.ENABLED_FLAG = 1 ";
            sqlQuery += " AND (lower(t.vendor_type)='customer' or lower(t.vendor_type)='both') ";

            if (VMVServices.Web.Utils.LanguageCode != string.Empty)
            {
                sqlQuery += " AND  t.vendor_name" + VMVServices.Web.Utils.LanguageCode + "  is not null ";
            }
            sqlQuery += " AND t.ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " ORDER BY t.vendor_name asc ";
            return sqlQuery;

        }
        public static string GetCustomerBasedQuote(string quteHdrId)
        {
            sqlQuery = string.Empty;

            sqlQuery = "     select t.vendor_id,t.vendor_name" + VMVServices.Web.Utils.LanguageCode + "  as vendor_name ";
            sqlQuery += "  from supplier_customers t, OM_SALE_QUOTE_HDR co";
            sqlQuery += "  where ";
            sqlQuery += "  co.quote_cust_id = t.vendor_id  ";
            sqlQuery += "  and co.quote_hdr_id = '" + quteHdrId + "'";
            sqlQuery += " and t.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " AND t.ENABLED_FLAG = 1 ";
            sqlQuery += " AND (lower(t.vendor_type)='customer' or lower(t.vendor_type)='both') ";
            if (VMVServices.Web.Utils.LanguageCode != string.Empty)
            {
                sqlQuery += " AND  t.vendor_name" + VMVServices.Web.Utils.LanguageCode + "  is not null ";
            }
            sqlQuery += " AND t.ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " ORDER BY t.vendor_name asc ";
            return sqlQuery;

        }
        public static string GetSupplierNameDetails()
        {
            sqlQuery = string.Empty;

            sqlQuery = "  select t.vendor_id,t.vendor_name" + VMVServices.Web.Utils.LanguageCode + " as vendor_name from supplier_customers t";
            sqlQuery += " WHERE t.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " AND t.ENABLED_FLAG = 1 ";
            sqlQuery += " AND (lower(t.vendor_type)='supplier' or lower(t.vendor_type)='both') ";
            if (VMVServices.Web.Utils.LanguageCode != string.Empty)
            {
                sqlQuery += " AND  t.vendor_name" + VMVServices.Web.Utils.LanguageCode + "  is not null ";
            }
            sqlQuery += " AND t.ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " ORDER BY t.vendor_name asc ";
            return sqlQuery;

        }



        public static string GetSupplier()
        {
            sqlQuery = string.Empty;

            sqlQuery = "  select DISTINCT t.vendor_id,T.VENDOR_CODE||' '||t.vendor_name" + VMVServices.Web.Utils.LanguageCode + " as vendor_name,T.VENDOR_CODE from supplier_customers t";
            sqlQuery += " WHERE t.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " AND (lower(t.vendor_type)='supplier' or lower(t.vendor_type)='both') ";
            sqlQuery += " AND t.ENABLED_FLAG = 1 ";
            if (VMVServices.Web.Utils.LanguageCode != string.Empty)
            {
                sqlQuery += " AND  t.vendor_name" + VMVServices.Web.Utils.LanguageCode + "  is not null ";
            }
            sqlQuery += " AND t.ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " ORDER BY t.VENDOR_CODE asc ";
            return sqlQuery;

        }
        public static string getSupplierNameType()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT DISTINCT sc.vendor_type AS vendor_type";
            sqlQuery += " FROM SUPPLIER_CUSTOMERS SC";
            sqlQuery += " where sc.workflow_completion_status='1'";
            sqlQuery += "  AND (lower(sc.vendor_type)<>'customer')";
            sqlQuery += " AND SC.ENABLED_FLAG='1'";
            return sqlQuery;
        }
        public static string getCustomerNameType()
        {
            sqlQuery = string.Empty;

            sqlQuery = "  SELECT DISTINCT sc.vendor_type AS vendor_type";
            sqlQuery += "  FROM SUPPLIER_CUSTOMERS SC";
            sqlQuery += "  where sc.workflow_completion_status='1'";
            sqlQuery += "  AND (lower(sc.vendor_type)<>'supplier')";
            sqlQuery += "  AND SC.ENABLED_FLAG='1'";
            return sqlQuery;
        }
        public static string getReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery = "SELECT * FROM INV_SUPPLIER_R V WHERE ROWNUM > 0 ";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Supplier_Id"] != null)
                {
                    sqlQuery += " AND V.VENDOR_ID >= '" + VMVServices.Web.Utils.ReportViewFilterParameter["From_Supplier_Id"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Supplier_Id"] != null)
                {
                    sqlQuery += " AND V.VENDOR_ID <= '" + VMVServices.Web.Utils.ReportViewFilterParameter["To_Supplier_Id"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.COUNTRY_CODE] != null)
                {
                    sqlQuery += " AND V.COUNTRY_CODE = '" + VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.COUNTRY_CODE].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.STATE_CODE] != null)
                {
                    sqlQuery += " AND V.STATE_CODE = '" + VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.STATE_CODE].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.CITY_CODE] != null)
                {
                    sqlQuery += " AND V.CITY_CODE = '" + VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.CITY_CODE].ToString() + "'";
                }


            }

            return sqlQuery;
        }
        public static string getPOReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM INV_VENDOR_PO_DETAILS_R V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.VENDOR_ID] != null)
                {
                    sqlQuery += " AND V.VENDOR_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.VENDOR_ID].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
                {
                    sqlQuery += " AND V.po_date between to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy') ";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
                {
                    sqlQuery += " AND to_date('" + VMVServices.Web.Utils.ReportFilterParameter["To_Date"].ToString() + "','dd/MM/yyyy') ";
                }
            }
            return sqlQuery;
        }


        public static string GetPaymentDtlsForSupplier(string supNo = "")
        {
            sqlQuery = string.Empty;

            sqlQuery = "  select ph.*";
            sqlQuery += " from supplier_customers t, inv_payments_hdr ph";
            sqlQuery += " WHERE t.WORKFLOW_COMPLETION_STATUS = 1  and (lower(t.vendor_type)='supplier' or lower(t.vendor_type)='both') ";
            if (supNo != string.Empty)
            {
                sqlQuery += " AND t.vendor_id ='" + supNo + "'";
            }
            sqlQuery += " AND t.ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " AND t.ENABLED_FLAG = 1 ";
            sqlQuery += " t.vendor_id = ph.pay_vendor_id";
            sqlQuery += " ORDER BY ph.pay_id asc ";

            return sqlQuery;
        }

        public static string GetPaymentDtls()
        {
            sqlQuery = string.Empty;

            sqlQuery = "  select iph.*";
            sqlQuery += " from inv_payments_hdr iph";
            sqlQuery += " where iph.ENABLED_FLAG=1 and iph.WORKFLOW_COMPLETION_STATUS=1";
            sqlQuery += " order by iph.pay_id";

            return sqlQuery;

        }

        public static string GetCustomerAndSupplier()
        {
            sqlQuery = string.Empty;

            sqlQuery = "  select DISTINCT t.vendor_id,T.VENDOR_CODE||' '||t.vendor_name" + VMVServices.Web.Utils.LanguageCode + " as vendor_name,T.VENDOR_CODE";
            sqlQuery += " from supplier_customers t";
            sqlQuery += " WHERE t.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " AND (lower(t.vendor_type)='both') ";
            sqlQuery += " AND t.ENABLED_FLAG = 1 ";
            if (VMVServices.Web.Utils.LanguageCode != string.Empty)
            {
                sqlQuery += " AND  t.vendor_name" + VMVServices.Web.Utils.LanguageCode + "  is not null ";
            }
            sqlQuery += " AND t.ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " ORDER BY t.VENDOR_CODE asc ";

            return sqlQuery;
        }

        public static string getVendorReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery = "SELECT * FROM vm_ap_supplier V WHERE ROWNUM > 0 ";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Supplier_Id"] != null)
                {
                    sqlQuery += " AND V.VENDOR_ID >= '" + VMVServices.Web.Utils.ReportViewFilterParameter["From_Supplier_Id"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Supplier_Id"] != null)
                {
                    sqlQuery += " AND V.VENDOR_ID <= '" + VMVServices.Web.Utils.ReportViewFilterParameter["To_Supplier_Id"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.COUNTRY_CODE] != null)
                {
                    sqlQuery += " AND V.COUNTRY_CODE = '" + VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.COUNTRY_CODE].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.STATE_CODE] != null)
                {
                    sqlQuery += " AND V.STATE_CODE = '" + VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.STATE_CODE].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.CITY_CODE] != null)
                {
                    sqlQuery += " AND V.CITY_CODE = '" + VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.CITY_CODE].ToString() + "'";
                }

                if (VMVServices.Web.Utils.ReportViewFilterParameter["CREATED_BY"] != null)
                {
                    sqlQuery += " AND UPPER(V.CREATED_BY) = UPPER('" + VMVServices.Web.Utils.ReportViewFilterParameter["CREATED_BY"].ToString() + "')";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["MODIFIED_BY"] != null)
                {
                    sqlQuery += " AND UPPER(V.MODIFIED_BY) = UPPER('" + VMVServices.Web.Utils.ReportViewFilterParameter["MODIFIED_BY"].ToString() + "')";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["CREATED_From_Date"] != null)
                {
                    sqlQuery += " AND V.CREATED_DATE >= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["CREATED_From_Date"].ToString() + "','dd/MM/yyyy') ";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["CREATED_To_Date"] != null)
                {
                    sqlQuery += " AND V.CREATED_DATE <= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["CREATED_To_Date"].ToString() + "','dd/MM/yyyy') ";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["MODIFIED_From_Date"] != null)
                {
                    sqlQuery += " AND V.MODIFIED_DATE >= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["MODIFIED_From_Date"].ToString() + "','dd/MM/yyyy') ";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["MODIFIED_To_Date"] != null)
                {
                    sqlQuery += " AND V.MODIFIED_DATE <= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["MODIFIED_To_Date"].ToString() + "','dd/MM/yyyy') ";
                }


                //sqlQuery += " AND lower(v.VENDOR_TYPE)=lower('Supplier') or lower(v.VENDOR_TYPE)=lower('Both')";
                sqlQuery += " AND UPPER(V.vendor_type) in('SUPPLIER','BOTH')";

            }

            return sqlQuery;
        }
    }


}
