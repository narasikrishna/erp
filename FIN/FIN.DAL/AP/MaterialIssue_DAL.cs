﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.AP
{
    public class MaterialIssue_DAL
    {
        static string sqlQuery = "";

        public static string GetItemUOM_fromUommaster(string ItemID)
        {
            sqlQuery = string.Empty;

            sqlQuery += " select  u.uom_code as item_uom";
            sqlQuery += " from  inv_item_master im, inv_uom_master u";
            sqlQuery += " where  im.item_uom = u.uom_id";
            sqlQuery += " and im.item_id = '" + ItemID + "'";
            return sqlQuery;
        }

        public static string GetIndentDetails(string indentId)
        {
            sqlQuery = string.Empty;
            sqlQuery = " select '' as ISSUE_WH_ID,'' as ISSUE_LOT_ID,'0' as ISSUE_LOT_QTY,";
            sqlQuery += " imih.indent_dept_id, imih.issuing_emp_id, imih.received_by, imih.indent_remarks, imih.indent_date, imih.indent_id, imid.indent_dtl_id, ";
            sqlQuery += " (select im.item_name" + VMVServices.Web.Utils.LanguageCode + " ";
            sqlQuery += " From inv_item_master im where imid.item_id = im.item_id) as item_name, ";
            sqlQuery += " imid.item_uom, imid.item_id, imid.item_req_qty, imid.item_issue_qty, imid.remarks as REMARKS, '0' as DELETED ";
            sqlQuery += " from inv_material_issue_dtl imid, inv_material_issue_hdr imih ";
            sqlQuery += " where imid.indent_id = imih.indent_id ";
            sqlQuery += " and imid.indent_id = '" + indentId + "'";
            sqlQuery += " order by INDENT_DTL_ID ";
            return sqlQuery;
        }

        public static string GetLotDetails()
        {
            sqlQuery = string.Empty;
            sqlQuery = " select lh.lot_id, lh.lot_number, lh.lot_qty, nvl(0,lh.lot_qty_issued) as lot_qty_issued ";
            sqlQuery += " from inv_receipt_lots_hdr lh,inv_item_master im ";
            sqlQuery += " where lh.item_id=im.item_id ";

            return sqlQuery;
        }

        public static string GetLotQty(string lotID)
        {
            sqlQuery = string.Empty;
            sqlQuery = " select lh.lot_id, lh.lot_number,lh.RECEIPT_ID,lh.lot_qty, nvl(0,lh.lot_qty_issued) as lot_qty_issued, im.item_uom ";
            sqlQuery += " from inv_receipt_lots_hdr lh,inv_item_master im ";
            sqlQuery += " where lh.item_id=im.item_id ";
            sqlQuery += " and lh.lot_id='" + lotID + "'";

            return sqlQuery;
        }

        public static string GetItemLotDtls(string str_Indent_id)
        {
            sqlQuery = string.Empty;

            //sqlQuery = " select ir.LOT_NUMBER,ir.lot_id,ir.LOT_QTY as ir_lot_qty,'N' AS DELETED,ir.ITEM_ID, wh.inv_wh_id, wh.inv_wh_name,";
            //sqlQuery += " imiw.issue_mat_wh_id, imiw.issue_lot_qty as lot_qty, imiw.issue_wh_id, imiw.issue_lot_id";
            //sqlQuery += " from  INV_RECEIPT_LOTS_HDR ir, inv_warehouses wh, inv_material_issue_wh imiw";
            //sqlQuery += " where ir.item_id='" + str_Indent_id + "'";
            //sqlQuery += " and ir.lot_id=imiw.issue_lot_id";
            //sqlQuery += " and wh.inv_wh_id=imiw.issue_wh_id";
            //sqlQuery += " and wh.org_id='" + VMVServices.Web.Utils.OrganizationID + "'";


            sqlQuery += " select MIH.ISSUE_MAT_WH_ID,MID.INDENT_DTL_ID,MIH.INDENT_ID,WH.INV_WH_ID,MID.ITEM_ID , wh.inv_wh_name ";
            sqlQuery += " ,RLH.LOT_NUMBER,RLH.LOT_ID,RLH.LOT_QTY as ir_lot_qty,'N' AS DELETED,MIH.issue_lot_qty as lot_qty ";
            sqlQuery += " from iNV_MATERIAL_ISSUE_WH MIH ";
            sqlQuery += " INNER JOIN iNV_MATERIAL_ISSUE_DTL MID ON MIH.INDENT_DTL_ID=MID.INDENT_DTL_ID ";
            sqlQuery += " INNER JOIN iNV_MATERIAL_ISSUE_HDR MIH ON MIH.INDENT_ID= MID.INDENT_ID ";
            sqlQuery += " INNER JOIN inv_warehouses WH ON WH.inv_wh_id = MIH.ISSUE_WH_ID ";
            sqlQuery += " INNER JOIN INV_RECEIPT_LOTS_HDR RLH ON RLH.lot_id = MIH.issue_lot_id ";
            sqlQuery += " WHERE MIH.INDENT_ID='" + str_Indent_id + "'";
            return sqlQuery;

        }


        //public static string GetItemLotDtls_Updatemode(string itemID)
        //{
        //    sqlQuery = string.Empty;

        //    sqlQuery += " select (select ir.LOT_NUMBER from INV_RECEIPT_LOTS_HDR ir where imiw.issue_lot_id=ir.lot_id)LOT_NUMBER,";
        //    sqlQuery += " imiw.issue_lot_id as lot_id,imiw.issue_lot_qty as ir_lot_qty,'N' AS DELETED,md.item_id,wh.inv_wh_id, wh.inv_wh_name,";
        //    sqlQuery += " imiw.issue_mat_wh_id,imiw.issue_lot_qty as lot_qty,imiw.issue_wh_id,imiw.issue_lot_id";
        //    sqlQuery += " from INV_MATERIAL_ISSUE_DTL  md,inv_warehouses wh, inv_material_issue_wh imiw";
        //    sqlQuery += " where md.item_id = '" + itemID + "'";
        //    sqlQuery += " and md.indent_dtl_id = imiw.indent_dtl_id";
        //    sqlQuery += " and wh.inv_wh_id = imiw.issue_wh_id";
        //    sqlQuery += " and wh.org_id = '" + VMVServices.Web.Utils.OrganizationID + "'";

        //    return sqlQuery;

        //}



        public static string GetLotDetails(string WHID, string ItemID)
        {
            sqlQuery = string.Empty;
            sqlQuery = " select lh.lot_id, lh.lot_number, lh.lot_qty, nvl(0,lh.lot_qty_issued) as lot_qty_issued ";
            sqlQuery += " from inv_receipt_lots_hdr lh,inv_item_master im, inv_receipt_item_wh_hdr riwh ";
            sqlQuery += " where lh.item_id=im.item_id ";
            sqlQuery += " and lh.receipt_id=riwh.receipt_id";
            sqlQuery += " and lh.receipt_dtl_id = riwh.receipt_dtl_id";
            sqlQuery += " and im.item_id = riwh.item_id";
            sqlQuery += " and riwh.wh_id='" + WHID + "'";
            sqlQuery += " and im.item_id = '" + ItemID + "'";

            return sqlQuery;
        }

        //public static string GetLotDetails(string WHID, string ItemID)
        //{
        //    sqlQuery = string.Empty;
        //    sqlQuery = " select lh.lot_id, lh.lot_number, lh.lot_qty, nvl(0,lh.lot_qty_issued) as lot_qty_issued ";
        //    sqlQuery += " from inv_receipt_lots_hdr lh";
        //    sqlQuery += " where lh.item_id = '" + ItemID + "'";            

        //    return sqlQuery;
        //}

        public static string getWHDetails(string ItemID)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select iw.inv_wh_id, iw.inv_wh_name, riwh.item_id, riwh.wh_qty ";
            sqlQuery += " from inv_warehouses iw, inv_receipt_item_wh_hdr riwh, inv_item_master im ";
            sqlQuery += " where riwh.wh_id = iw.inv_wh_id ";
            sqlQuery += " and im.item_id = riwh.item_id";
            sqlQuery += " and im.item_id = '" + ItemID + "'";

            return sqlQuery;
        }

        public static string getWHDetails_fritem(string ItemID)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select distinct iw.inv_wh_id, iw.inv_wh_name, riwh.item_id";
            sqlQuery += " from inv_warehouses iw, inv_receipt_item_wh_hdr riwh, inv_item_master im ";
            sqlQuery += " where riwh.wh_id = iw.inv_wh_id ";
            sqlQuery += " and im.item_id = riwh.item_id";
            sqlQuery += " and im.item_id = '" + ItemID + "'";

            return sqlQuery;
        }


        public static string GetItemUOM(string ItemID)
        {
            sqlQuery = string.Empty;
            sqlQuery = " select lh.lot_id, lh.lot_number, lh.lot_qty, nvl(0,lh.lot_qty_issued) as lot_qty_issued, im.item_uom ";
            sqlQuery += " from inv_receipt_lots_hdr lh,inv_item_master im ";
            sqlQuery += " where lh.item_id=im.item_id ";
            sqlQuery += " and lh.item_id='" + ItemID + "'";

            return sqlQuery;
        }

        public static string getLotqtyissed_fromRecLotHdr(string LOT_ID)
        {
            sqlQuery = string.Empty;

            sqlQuery = "  SELECT nvl(RL.LOT_QTY_ISSUED,0) as LOT_QTY_ISSUED,RL.RECEIPT_ID,RL.ITEM_ID";
            sqlQuery += " from INV_RECEIPT_LOTS_HDR rl";
            sqlQuery += " WHERE RL.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND RL.ENABLED_FLAG = 1";
            sqlQuery += " AND RL.LOT_ID = '" + LOT_ID + "'";
            return sqlQuery;
        }

        public static string getLotqty_fromMatIssu(string LOT_ID)
        {
            sqlQuery = string.Empty;

            sqlQuery += " select miw.issue_lot_qty";
            sqlQuery += "  from iNV_MATERIAL_ISSUE_WH miw";
            sqlQuery += "  where miw.issue_lot_id ='" + LOT_ID + "'";
            return sqlQuery;
        }


        public static void UPDATE_Receiptlot_dtl(string P_LOT_ID, decimal P_QTY)
        {
            try
            {

                string strQuery = string.Empty;


                strQuery = "  update INV_RECEIPT_LOTS_HDR ff set ff.MODIFIED_BY='" + VMVServices.Web.Utils.UserName + "',ff.MODIFIED_DATE='" + DateTime.Today.ToString("dd-MMM-yyyy") + "', ff.LOT_QTY_ISSUED='" + P_QTY + "'";
                strQuery += "  where ff.lot_id='" + P_LOT_ID + "'";

                DBMethod.ExecuteNonQuery(strQuery);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
