//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.Serialization;

namespace FIN.DAL
{
    [DataContract(IsReference = true)]
    [KnownType(typeof(LN_LOAN_REQUEST_DTL))]
    public partial class LN_LOAN_PROVISION: IObjectWithChangeTracker, INotifyPropertyChanged
    {
        #region Primitive Properties
    
        [DataMember]
        public Nullable<int> PK_ID
        {
            get { return _pK_ID; }
            set
            {
                if (_pK_ID != value)
                {
                    _pK_ID = value;
                    OnPropertyChanged("PK_ID");
                }
            }
        }
        private Nullable<int> _pK_ID;
    
        [DataMember]
        public Nullable<int> CHILD_ID
        {
            get { return _cHILD_ID; }
            set
            {
                if (_cHILD_ID != value)
                {
                    _cHILD_ID = value;
                    OnPropertyChanged("CHILD_ID");
                }
            }
        }
        private Nullable<int> _cHILD_ID;
    
        [DataMember]
        public string LN_PROVISION_ID
        {
            get { return _lN_PROVISION_ID; }
            set
            {
                if (_lN_PROVISION_ID != value)
                {
                    if (ChangeTracker.ChangeTrackingEnabled && ChangeTracker.State != ObjectState.Added)
                    {
                        throw new InvalidOperationException("The property 'LN_PROVISION_ID' is part of the object's key and cannot be changed. Changes to key properties can only be made when the object is not being tracked or is in the Added state.");
                    }
                    _lN_PROVISION_ID = value;
                    OnPropertyChanged("LN_PROVISION_ID");
                }
            }
        }
        private string _lN_PROVISION_ID;
    
        [DataMember]
        public string LN_CONTRACT_ID
        {
            get { return _lN_CONTRACT_ID; }
            set
            {
                if (_lN_CONTRACT_ID != value)
                {
                    ChangeTracker.RecordOriginalValue("LN_CONTRACT_ID", _lN_CONTRACT_ID);
                    if (!IsDeserializing)
                    {
                        if (LN_LOAN_REQUEST_DTL != null && LN_LOAN_REQUEST_DTL.LN_CONTRACT_ID != value)
                        {
                            LN_LOAN_REQUEST_DTL = null;
                        }
                    }
                    _lN_CONTRACT_ID = value;
                    OnPropertyChanged("LN_CONTRACT_ID");
                }
            }
        }
        private string _lN_CONTRACT_ID;
    
        [DataMember]
        public Nullable<System.DateTime> LN_PROVISION_DATE
        {
            get { return _lN_PROVISION_DATE; }
            set
            {
                if (_lN_PROVISION_DATE != value)
                {
                    _lN_PROVISION_DATE = value;
                    OnPropertyChanged("LN_PROVISION_DATE");
                }
            }
        }
        private Nullable<System.DateTime> _lN_PROVISION_DATE;
    
        [DataMember]
        public Nullable<decimal> LN_PRINCIPLE_AMT
        {
            get { return _lN_PRINCIPLE_AMT; }
            set
            {
                if (_lN_PRINCIPLE_AMT != value)
                {
                    _lN_PRINCIPLE_AMT = value;
                    OnPropertyChanged("LN_PRINCIPLE_AMT");
                }
            }
        }
        private Nullable<decimal> _lN_PRINCIPLE_AMT;
    
        [DataMember]
        public Nullable<decimal> LN_NO_OF_DAYS
        {
            get { return _lN_NO_OF_DAYS; }
            set
            {
                if (_lN_NO_OF_DAYS != value)
                {
                    _lN_NO_OF_DAYS = value;
                    OnPropertyChanged("LN_NO_OF_DAYS");
                }
            }
        }
        private Nullable<decimal> _lN_NO_OF_DAYS;
    
        [DataMember]
        public Nullable<decimal> LN_PROFIT_SHARE_AMT
        {
            get { return _lN_PROFIT_SHARE_AMT; }
            set
            {
                if (_lN_PROFIT_SHARE_AMT != value)
                {
                    _lN_PROFIT_SHARE_AMT = value;
                    OnPropertyChanged("LN_PROFIT_SHARE_AMT");
                }
            }
        }
        private Nullable<decimal> _lN_PROFIT_SHARE_AMT;
    
        [DataMember]
        public string LN_ORG_ID
        {
            get { return _lN_ORG_ID; }
            set
            {
                if (_lN_ORG_ID != value)
                {
                    _lN_ORG_ID = value;
                    OnPropertyChanged("LN_ORG_ID");
                }
            }
        }
        private string _lN_ORG_ID;
    
        [DataMember]
        public string ENABLED_FLAG
        {
            get { return _eNABLED_FLAG; }
            set
            {
                if (_eNABLED_FLAG != value)
                {
                    _eNABLED_FLAG = value;
                    OnPropertyChanged("ENABLED_FLAG");
                }
            }
        }
        private string _eNABLED_FLAG;
    
        [DataMember]
        public string WORKFLOW_COMPLETION_STATUS
        {
            get { return _wORKFLOW_COMPLETION_STATUS; }
            set
            {
                if (_wORKFLOW_COMPLETION_STATUS != value)
                {
                    _wORKFLOW_COMPLETION_STATUS = value;
                    OnPropertyChanged("WORKFLOW_COMPLETION_STATUS");
                }
            }
        }
        private string _wORKFLOW_COMPLETION_STATUS;
    
        [DataMember]
        public string CREATED_BY
        {
            get { return _cREATED_BY; }
            set
            {
                if (_cREATED_BY != value)
                {
                    _cREATED_BY = value;
                    OnPropertyChanged("CREATED_BY");
                }
            }
        }
        private string _cREATED_BY;
    
        [DataMember]
        public System.DateTime CREATED_DATE
        {
            get { return _cREATED_DATE; }
            set
            {
                if (_cREATED_DATE != value)
                {
                    _cREATED_DATE = value;
                    OnPropertyChanged("CREATED_DATE");
                }
            }
        }
        private System.DateTime _cREATED_DATE;
    
        [DataMember]
        public string MODIFIED_BY
        {
            get { return _mODIFIED_BY; }
            set
            {
                if (_mODIFIED_BY != value)
                {
                    _mODIFIED_BY = value;
                    OnPropertyChanged("MODIFIED_BY");
                }
            }
        }
        private string _mODIFIED_BY;
    
        [DataMember]
        public Nullable<System.DateTime> MODIFIED_DATE
        {
            get { return _mODIFIED_DATE; }
            set
            {
                if (_mODIFIED_DATE != value)
                {
                    _mODIFIED_DATE = value;
                    OnPropertyChanged("MODIFIED_DATE");
                }
            }
        }
        private Nullable<System.DateTime> _mODIFIED_DATE;
    
        [DataMember]
        public string ATTRIBUTE1
        {
            get { return _aTTRIBUTE1; }
            set
            {
                if (_aTTRIBUTE1 != value)
                {
                    _aTTRIBUTE1 = value;
                    OnPropertyChanged("ATTRIBUTE1");
                }
            }
        }
        private string _aTTRIBUTE1;
    
        [DataMember]
        public string ATTRIBUTE2
        {
            get { return _aTTRIBUTE2; }
            set
            {
                if (_aTTRIBUTE2 != value)
                {
                    _aTTRIBUTE2 = value;
                    OnPropertyChanged("ATTRIBUTE2");
                }
            }
        }
        private string _aTTRIBUTE2;
    
        [DataMember]
        public string ATTRIBUTE3
        {
            get { return _aTTRIBUTE3; }
            set
            {
                if (_aTTRIBUTE3 != value)
                {
                    _aTTRIBUTE3 = value;
                    OnPropertyChanged("ATTRIBUTE3");
                }
            }
        }
        private string _aTTRIBUTE3;
    
        [DataMember]
        public string ATTRIBUTE4
        {
            get { return _aTTRIBUTE4; }
            set
            {
                if (_aTTRIBUTE4 != value)
                {
                    _aTTRIBUTE4 = value;
                    OnPropertyChanged("ATTRIBUTE4");
                }
            }
        }
        private string _aTTRIBUTE4;
    
        [DataMember]
        public string ATTRIBUTE5
        {
            get { return _aTTRIBUTE5; }
            set
            {
                if (_aTTRIBUTE5 != value)
                {
                    _aTTRIBUTE5 = value;
                    OnPropertyChanged("ATTRIBUTE5");
                }
            }
        }
        private string _aTTRIBUTE5;
    
        [DataMember]
        public string ATTRIBUTE6
        {
            get { return _aTTRIBUTE6; }
            set
            {
                if (_aTTRIBUTE6 != value)
                {
                    _aTTRIBUTE6 = value;
                    OnPropertyChanged("ATTRIBUTE6");
                }
            }
        }
        private string _aTTRIBUTE6;
    
        [DataMember]
        public string ATTRIBUTE7
        {
            get { return _aTTRIBUTE7; }
            set
            {
                if (_aTTRIBUTE7 != value)
                {
                    _aTTRIBUTE7 = value;
                    OnPropertyChanged("ATTRIBUTE7");
                }
            }
        }
        private string _aTTRIBUTE7;
    
        [DataMember]
        public string ATTRIBUTE8
        {
            get { return _aTTRIBUTE8; }
            set
            {
                if (_aTTRIBUTE8 != value)
                {
                    _aTTRIBUTE8 = value;
                    OnPropertyChanged("ATTRIBUTE8");
                }
            }
        }
        private string _aTTRIBUTE8;
    
        [DataMember]
        public string ATTRIBUTE9
        {
            get { return _aTTRIBUTE9; }
            set
            {
                if (_aTTRIBUTE9 != value)
                {
                    _aTTRIBUTE9 = value;
                    OnPropertyChanged("ATTRIBUTE9");
                }
            }
        }
        private string _aTTRIBUTE9;
    
        [DataMember]
        public string ATTRIBUTE10
        {
            get { return _aTTRIBUTE10; }
            set
            {
                if (_aTTRIBUTE10 != value)
                {
                    _aTTRIBUTE10 = value;
                    OnPropertyChanged("ATTRIBUTE10");
                }
            }
        }
        private string _aTTRIBUTE10;
    
        [DataMember]
        public string POSTED_FLAG
        {
            get { return _pOSTED_FLAG; }
            set
            {
                if (_pOSTED_FLAG != value)
                {
                    _pOSTED_FLAG = value;
                    OnPropertyChanged("POSTED_FLAG");
                }
            }
        }
        private string _pOSTED_FLAG;
    
        [DataMember]
        public Nullable<System.DateTime> POSTED_DATE
        {
            get { return _pOSTED_DATE; }
            set
            {
                if (_pOSTED_DATE != value)
                {
                    _pOSTED_DATE = value;
                    OnPropertyChanged("POSTED_DATE");
                }
            }
        }
        private Nullable<System.DateTime> _pOSTED_DATE;
    
        [DataMember]
        public string JE_HDR_ID
        {
            get { return _jE_HDR_ID; }
            set
            {
                if (_jE_HDR_ID != value)
                {
                    _jE_HDR_ID = value;
                    OnPropertyChanged("JE_HDR_ID");
                }
            }
        }
        private string _jE_HDR_ID;

        #endregion
        #region Navigation Properties
    
        [DataMember]
        public LN_LOAN_REQUEST_DTL LN_LOAN_REQUEST_DTL
        {
            get { return _lN_LOAN_REQUEST_DTL; }
            set
            {
                if (!ReferenceEquals(_lN_LOAN_REQUEST_DTL, value))
                {
                    var previousValue = _lN_LOAN_REQUEST_DTL;
                    _lN_LOAN_REQUEST_DTL = value;
                    FixupLN_LOAN_REQUEST_DTL(previousValue);
                    OnNavigationPropertyChanged("LN_LOAN_REQUEST_DTL");
                }
            }
        }
        private LN_LOAN_REQUEST_DTL _lN_LOAN_REQUEST_DTL;

        #endregion
        #region ChangeTracking
    
        protected virtual void OnPropertyChanged(String propertyName)
        {
            if (ChangeTracker.State != ObjectState.Added && ChangeTracker.State != ObjectState.Deleted)
            {
                ChangeTracker.State = ObjectState.Modified;
            }
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        protected virtual void OnNavigationPropertyChanged(String propertyName)
        {
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        event PropertyChangedEventHandler INotifyPropertyChanged.PropertyChanged{ add { _propertyChanged += value; } remove { _propertyChanged -= value; } }
        private event PropertyChangedEventHandler _propertyChanged;
        private ObjectChangeTracker _changeTracker;
    
        [DataMember]
        public ObjectChangeTracker ChangeTracker
        {
            get
            {
                if (_changeTracker == null)
                {
                    _changeTracker = new ObjectChangeTracker();
                    _changeTracker.ObjectStateChanging += HandleObjectStateChanging;
                }
                return _changeTracker;
            }
            set
            {
                if(_changeTracker != null)
                {
                    _changeTracker.ObjectStateChanging -= HandleObjectStateChanging;
                }
                _changeTracker = value;
                if(_changeTracker != null)
                {
                    _changeTracker.ObjectStateChanging += HandleObjectStateChanging;
                }
            }
        }
    
        private void HandleObjectStateChanging(object sender, ObjectStateChangingEventArgs e)
        {
            if (e.NewState == ObjectState.Deleted)
            {
                ClearNavigationProperties();
            }
        }
    
        protected bool IsDeserializing { get; private set; }
    
        [OnDeserializing]
        public void OnDeserializingMethod(StreamingContext context)
        {
            IsDeserializing = true;
        }
    
        [OnDeserialized]
        public void OnDeserializedMethod(StreamingContext context)
        {
            IsDeserializing = false;
            ChangeTracker.ChangeTrackingEnabled = true;
        }
    
        protected virtual void ClearNavigationProperties()
        {
            LN_LOAN_REQUEST_DTL = null;
        }

        #endregion
        #region Association Fixup
    
        private void FixupLN_LOAN_REQUEST_DTL(LN_LOAN_REQUEST_DTL previousValue, bool skipKeys = false)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (previousValue != null && previousValue.LN_LOAN_PROVISION.Contains(this))
            {
                previousValue.LN_LOAN_PROVISION.Remove(this);
            }
    
            if (LN_LOAN_REQUEST_DTL != null)
            {
                if (!LN_LOAN_REQUEST_DTL.LN_LOAN_PROVISION.Contains(this))
                {
                    LN_LOAN_REQUEST_DTL.LN_LOAN_PROVISION.Add(this);
                }
    
                LN_CONTRACT_ID = LN_LOAN_REQUEST_DTL.LN_CONTRACT_ID;
            }
            else if (!skipKeys)
            {
                LN_CONTRACT_ID = null;
            }
    
            if (ChangeTracker.ChangeTrackingEnabled)
            {
                if (ChangeTracker.OriginalValues.ContainsKey("LN_LOAN_REQUEST_DTL")
                    && (ChangeTracker.OriginalValues["LN_LOAN_REQUEST_DTL"] == LN_LOAN_REQUEST_DTL))
                {
                    ChangeTracker.OriginalValues.Remove("LN_LOAN_REQUEST_DTL");
                }
                else
                {
                    ChangeTracker.RecordOriginalValue("LN_LOAN_REQUEST_DTL", previousValue);
                }
                if (LN_LOAN_REQUEST_DTL != null && !LN_LOAN_REQUEST_DTL.ChangeTracker.ChangeTrackingEnabled)
                {
                    LN_LOAN_REQUEST_DTL.StartTracking();
                }
            }
        }

        #endregion
    }
}
