//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.Serialization;

namespace FIN.DAL
{
    [DataContract(IsReference = true)]
    [KnownType(typeof(SSM_LANGUAGES))]
    public partial class SSM_CODE_MASTERS: IObjectWithChangeTracker, INotifyPropertyChanged
    {
        #region Primitive Properties
    
        [DataMember]
        public int PK_ID
        {
            get { return _pK_ID; }
            set
            {
                if (_pK_ID != value)
                {
                    if (ChangeTracker.ChangeTrackingEnabled && ChangeTracker.State != ObjectState.Added)
                    {
                        throw new InvalidOperationException("The property 'PK_ID' is part of the object's key and cannot be changed. Changes to key properties can only be made when the object is not being tracked or is in the Added state.");
                    }
                    _pK_ID = value;
                    OnPropertyChanged("PK_ID");
                }
            }
        }
        private int _pK_ID;
    
        [DataMember]
        public Nullable<int> CHILD_ID
        {
            get { return _cHILD_ID; }
            set
            {
                if (_cHILD_ID != value)
                {
                    _cHILD_ID = value;
                    OnPropertyChanged("CHILD_ID");
                }
            }
        }
        private Nullable<int> _cHILD_ID;
    
        [DataMember]
        public string MASTER_CODE
        {
            get { return _mASTER_CODE; }
            set
            {
                if (_mASTER_CODE != value)
                {
                    _mASTER_CODE = value;
                    OnPropertyChanged("MASTER_CODE");
                }
            }
        }
        private string _mASTER_CODE;
    
        [DataMember]
        public string CODE
        {
            get { return _cODE; }
            set
            {
                if (_cODE != value)
                {
                    _cODE = value;
                    OnPropertyChanged("CODE");
                }
            }
        }
        private string _cODE;
    
        [DataMember]
        public string DESCRIPTION
        {
            get { return _dESCRIPTION; }
            set
            {
                if (_dESCRIPTION != value)
                {
                    _dESCRIPTION = value;
                    OnPropertyChanged("DESCRIPTION");
                }
            }
        }
        private string _dESCRIPTION;
    
        [DataMember]
        public string PARENT_CODE
        {
            get { return _pARENT_CODE; }
            set
            {
                if (_pARENT_CODE != value)
                {
                    _pARENT_CODE = value;
                    OnPropertyChanged("PARENT_CODE");
                }
            }
        }
        private string _pARENT_CODE;
    
        [DataMember]
        public string PARENT_TYPE
        {
            get { return _pARENT_TYPE; }
            set
            {
                if (_pARENT_TYPE != value)
                {
                    _pARENT_TYPE = value;
                    OnPropertyChanged("PARENT_TYPE");
                }
            }
        }
        private string _pARENT_TYPE;
    
        [DataMember]
        public System.DateTime EFFECTIVE_DATE
        {
            get { return _eFFECTIVE_DATE; }
            set
            {
                if (_eFFECTIVE_DATE != value)
                {
                    _eFFECTIVE_DATE = value;
                    OnPropertyChanged("EFFECTIVE_DATE");
                }
            }
        }
        private System.DateTime _eFFECTIVE_DATE;
    
        [DataMember]
        public Nullable<System.DateTime> END_DATE
        {
            get { return _eND_DATE; }
            set
            {
                if (_eND_DATE != value)
                {
                    _eND_DATE = value;
                    OnPropertyChanged("END_DATE");
                }
            }
        }
        private Nullable<System.DateTime> _eND_DATE;
    
        [DataMember]
        public string LANGUAGE_CODE
        {
            get { return _lANGUAGE_CODE; }
            set
            {
                if (_lANGUAGE_CODE != value)
                {
                    ChangeTracker.RecordOriginalValue("LANGUAGE_CODE", _lANGUAGE_CODE);
                    if (!IsDeserializing)
                    {
                        if (SSM_LANGUAGES != null && SSM_LANGUAGES.LANGUAGE_CODE != value)
                        {
                            SSM_LANGUAGES = null;
                        }
                    }
                    _lANGUAGE_CODE = value;
                    OnPropertyChanged("LANGUAGE_CODE");
                }
            }
        }
        private string _lANGUAGE_CODE;
    
        [DataMember]
        public string ENABLED_FLAG
        {
            get { return _eNABLED_FLAG; }
            set
            {
                if (_eNABLED_FLAG != value)
                {
                    _eNABLED_FLAG = value;
                    OnPropertyChanged("ENABLED_FLAG");
                }
            }
        }
        private string _eNABLED_FLAG;
    
        [DataMember]
        public string WORKFLOW_COMPLETION_STATUS
        {
            get { return _wORKFLOW_COMPLETION_STATUS; }
            set
            {
                if (_wORKFLOW_COMPLETION_STATUS != value)
                {
                    _wORKFLOW_COMPLETION_STATUS = value;
                    OnPropertyChanged("WORKFLOW_COMPLETION_STATUS");
                }
            }
        }
        private string _wORKFLOW_COMPLETION_STATUS;
    
        [DataMember]
        public string CREATED_BY
        {
            get { return _cREATED_BY; }
            set
            {
                if (_cREATED_BY != value)
                {
                    _cREATED_BY = value;
                    OnPropertyChanged("CREATED_BY");
                }
            }
        }
        private string _cREATED_BY;
    
        [DataMember]
        public System.DateTime CREATED_DATE
        {
            get { return _cREATED_DATE; }
            set
            {
                if (_cREATED_DATE != value)
                {
                    _cREATED_DATE = value;
                    OnPropertyChanged("CREATED_DATE");
                }
            }
        }
        private System.DateTime _cREATED_DATE;
    
        [DataMember]
        public string MODIFIED_BY
        {
            get { return _mODIFIED_BY; }
            set
            {
                if (_mODIFIED_BY != value)
                {
                    _mODIFIED_BY = value;
                    OnPropertyChanged("MODIFIED_BY");
                }
            }
        }
        private string _mODIFIED_BY;
    
        [DataMember]
        public Nullable<System.DateTime> MODIFIED_DATE
        {
            get { return _mODIFIED_DATE; }
            set
            {
                if (_mODIFIED_DATE != value)
                {
                    _mODIFIED_DATE = value;
                    OnPropertyChanged("MODIFIED_DATE");
                }
            }
        }
        private Nullable<System.DateTime> _mODIFIED_DATE;
    
        [DataMember]
        public string CCID_CODE
        {
            get { return _cCID_CODE; }
            set
            {
                if (_cCID_CODE != value)
                {
                    _cCID_CODE = value;
                    OnPropertyChanged("CCID_CODE");
                }
            }
        }
        private string _cCID_CODE;
    
        [DataMember]
        public string ATTRIBUTE1
        {
            get { return _aTTRIBUTE1; }
            set
            {
                if (_aTTRIBUTE1 != value)
                {
                    _aTTRIBUTE1 = value;
                    OnPropertyChanged("ATTRIBUTE1");
                }
            }
        }
        private string _aTTRIBUTE1;
    
        [DataMember]
        public string ATTRIBUTE2
        {
            get { return _aTTRIBUTE2; }
            set
            {
                if (_aTTRIBUTE2 != value)
                {
                    _aTTRIBUTE2 = value;
                    OnPropertyChanged("ATTRIBUTE2");
                }
            }
        }
        private string _aTTRIBUTE2;
    
        [DataMember]
        public string ATTRIBUTE3
        {
            get { return _aTTRIBUTE3; }
            set
            {
                if (_aTTRIBUTE3 != value)
                {
                    _aTTRIBUTE3 = value;
                    OnPropertyChanged("ATTRIBUTE3");
                }
            }
        }
        private string _aTTRIBUTE3;
    
        [DataMember]
        public string ATTRIBUTE4
        {
            get { return _aTTRIBUTE4; }
            set
            {
                if (_aTTRIBUTE4 != value)
                {
                    _aTTRIBUTE4 = value;
                    OnPropertyChanged("ATTRIBUTE4");
                }
            }
        }
        private string _aTTRIBUTE4;
    
        [DataMember]
        public string ATTRIBUTE5
        {
            get { return _aTTRIBUTE5; }
            set
            {
                if (_aTTRIBUTE5 != value)
                {
                    _aTTRIBUTE5 = value;
                    OnPropertyChanged("ATTRIBUTE5");
                }
            }
        }
        private string _aTTRIBUTE5;
    
        [DataMember]
        public string ATTRIBUTE6
        {
            get { return _aTTRIBUTE6; }
            set
            {
                if (_aTTRIBUTE6 != value)
                {
                    _aTTRIBUTE6 = value;
                    OnPropertyChanged("ATTRIBUTE6");
                }
            }
        }
        private string _aTTRIBUTE6;
    
        [DataMember]
        public string ATTRIBUTE7
        {
            get { return _aTTRIBUTE7; }
            set
            {
                if (_aTTRIBUTE7 != value)
                {
                    _aTTRIBUTE7 = value;
                    OnPropertyChanged("ATTRIBUTE7");
                }
            }
        }
        private string _aTTRIBUTE7;
    
        [DataMember]
        public string ATTRIBUTE8
        {
            get { return _aTTRIBUTE8; }
            set
            {
                if (_aTTRIBUTE8 != value)
                {
                    _aTTRIBUTE8 = value;
                    OnPropertyChanged("ATTRIBUTE8");
                }
            }
        }
        private string _aTTRIBUTE8;
    
        [DataMember]
        public string ATTRIBUTE9
        {
            get { return _aTTRIBUTE9; }
            set
            {
                if (_aTTRIBUTE9 != value)
                {
                    _aTTRIBUTE9 = value;
                    OnPropertyChanged("ATTRIBUTE9");
                }
            }
        }
        private string _aTTRIBUTE9;
    
        [DataMember]
        public string ATTRIBUTE10
        {
            get { return _aTTRIBUTE10; }
            set
            {
                if (_aTTRIBUTE10 != value)
                {
                    _aTTRIBUTE10 = value;
                    OnPropertyChanged("ATTRIBUTE10");
                }
            }
        }
        private string _aTTRIBUTE10;
    
        [DataMember]
        public string DESCRIPTION_OL
        {
            get { return _dESCRIPTION_OL; }
            set
            {
                if (_dESCRIPTION_OL != value)
                {
                    _dESCRIPTION_OL = value;
                    OnPropertyChanged("DESCRIPTION_OL");
                }
            }
        }
        private string _dESCRIPTION_OL;
    
        [DataMember]
        public Nullable<decimal> ORDER_NO
        {
            get { return _oRDER_NO; }
            set
            {
                if (_oRDER_NO != value)
                {
                    _oRDER_NO = value;
                    OnPropertyChanged("ORDER_NO");
                }
            }
        }
        private Nullable<decimal> _oRDER_NO;

        #endregion
        #region Navigation Properties
    
        [DataMember]
        public SSM_LANGUAGES SSM_LANGUAGES
        {
            get { return _sSM_LANGUAGES; }
            set
            {
                if (!ReferenceEquals(_sSM_LANGUAGES, value))
                {
                    var previousValue = _sSM_LANGUAGES;
                    _sSM_LANGUAGES = value;
                    FixupSSM_LANGUAGES(previousValue);
                    OnNavigationPropertyChanged("SSM_LANGUAGES");
                }
            }
        }
        private SSM_LANGUAGES _sSM_LANGUAGES;

        #endregion
        #region ChangeTracking
    
        protected virtual void OnPropertyChanged(String propertyName)
        {
            if (ChangeTracker.State != ObjectState.Added && ChangeTracker.State != ObjectState.Deleted)
            {
                ChangeTracker.State = ObjectState.Modified;
            }
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        protected virtual void OnNavigationPropertyChanged(String propertyName)
        {
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        event PropertyChangedEventHandler INotifyPropertyChanged.PropertyChanged{ add { _propertyChanged += value; } remove { _propertyChanged -= value; } }
        private event PropertyChangedEventHandler _propertyChanged;
        private ObjectChangeTracker _changeTracker;
    
        [DataMember]
        public ObjectChangeTracker ChangeTracker
        {
            get
            {
                if (_changeTracker == null)
                {
                    _changeTracker = new ObjectChangeTracker();
                    _changeTracker.ObjectStateChanging += HandleObjectStateChanging;
                }
                return _changeTracker;
            }
            set
            {
                if(_changeTracker != null)
                {
                    _changeTracker.ObjectStateChanging -= HandleObjectStateChanging;
                }
                _changeTracker = value;
                if(_changeTracker != null)
                {
                    _changeTracker.ObjectStateChanging += HandleObjectStateChanging;
                }
            }
        }
    
        private void HandleObjectStateChanging(object sender, ObjectStateChangingEventArgs e)
        {
            if (e.NewState == ObjectState.Deleted)
            {
                ClearNavigationProperties();
            }
        }
    
        protected bool IsDeserializing { get; private set; }
    
        [OnDeserializing]
        public void OnDeserializingMethod(StreamingContext context)
        {
            IsDeserializing = true;
        }
    
        [OnDeserialized]
        public void OnDeserializedMethod(StreamingContext context)
        {
            IsDeserializing = false;
            ChangeTracker.ChangeTrackingEnabled = true;
        }
    
        protected virtual void ClearNavigationProperties()
        {
            SSM_LANGUAGES = null;
        }

        #endregion
        #region Association Fixup
    
        private void FixupSSM_LANGUAGES(SSM_LANGUAGES previousValue, bool skipKeys = false)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (previousValue != null && previousValue.SSM_CODE_MASTERS.Contains(this))
            {
                previousValue.SSM_CODE_MASTERS.Remove(this);
            }
    
            if (SSM_LANGUAGES != null)
            {
                if (!SSM_LANGUAGES.SSM_CODE_MASTERS.Contains(this))
                {
                    SSM_LANGUAGES.SSM_CODE_MASTERS.Add(this);
                }
    
                LANGUAGE_CODE = SSM_LANGUAGES.LANGUAGE_CODE;
            }
            else if (!skipKeys)
            {
                LANGUAGE_CODE = null;
            }
    
            if (ChangeTracker.ChangeTrackingEnabled)
            {
                if (ChangeTracker.OriginalValues.ContainsKey("SSM_LANGUAGES")
                    && (ChangeTracker.OriginalValues["SSM_LANGUAGES"] == SSM_LANGUAGES))
                {
                    ChangeTracker.OriginalValues.Remove("SSM_LANGUAGES");
                }
                else
                {
                    ChangeTracker.RecordOriginalValue("SSM_LANGUAGES", previousValue);
                }
                if (SSM_LANGUAGES != null && !SSM_LANGUAGES.ChangeTracker.ChangeTrackingEnabled)
                {
                    SSM_LANGUAGES.StartTracking();
                }
            }
        }

        #endregion
    }
}
