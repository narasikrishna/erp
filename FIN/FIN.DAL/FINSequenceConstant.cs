﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL
{
   public static   class FINSequenceConstant
    {
       public static string  SYS_LOOKUP_TYPE_MASTER_SEQ="SYS_LOOKUP_TYPE_MASTER_SEQ";
       public static string FIN_CATEGORIES_SEQ = "FIN_CATEGORIES_SEQ";
       public static string FIN_GRADES_SEQ = "FIN_GRADES_SEQ";
       public static string GL_ACCT_CALENDAR_HDR_SEQ = "GL_ACCT_CALENDAR_HDR_SEQ";
       public static string SSM_CURRENCIES_SEQ = "SSM_CURRENCIES_SEQ";
       public static string GL_ACCT_GROUPS_SEQ = "GL_ACCT_GROUPS_SEQ";
       public static string GL_ACCT_GROUP_DEFAULT_SEG_SEQ = "GL_ACCT_GROUP_DEFAULT_SEG_SEQ";
       public static string GL_ACCT_CODES_SEQ = "GL_ACCT_CODES_SEQ";
       public static string GL_COMPANIES_HDR_SEQ = "GL_COMPANIES_HDR_SEQ";
       public static string GL_ACCT_CODE_SEGMENTS = "GL_ACCT_CODE_SEGMENTS";
       public static string GL_COMPANIES_DTL_SEQ = "GL_COMPANIES_DTL_SEQ";
       public static string GL_ACCT_CALENDAR_DTL_SEQ = "GL_ACCT_CALENDAR_DTL_SEQ";
       public static string GL_SEGMENTS_SEQ = "GL_SEGMENTS_SEQ";
       public static string GL_JOURNAL_HDR_SEQ = "GL_JOURNAL_HDR_SEQ";
       public static string GL_JOURNAL_DTL_SEQ = "GL_JOURNAL_DTL_SEQ";
       public static string SUPPLIER_CUSTOMER_BANK_SEQ = "SUPPLIER_CUSTOMER_BANK_SEQ";
       public static string INV_RECEIPT_LOTS_HDR_SEQ = "INV_RECEIPT_LOTS_HDR_SEQ";
       public static string INV_RECEIPT_ITEM_WH_HDR_SEQ = "INV_RECEIPT_ITEM_WH_HDR_SEQ";
       public static string iNV_TRANSFER_HDR_SEQ = "iNV_TRANSFER_HDR_SEQ";
       public static string tM_COMP_GRACE_SEQ = "tM_COMP_GRACE_SEQ";
       public static string tM_DEPT_GRACE_SEQ = "tM_DEPT_GRACE_SEQ";
       public static string HR_GRADES_SEQ = "HR_GRADES_SEQ";
       public static string HR_JOBS_SEQ = "HR_JOBS_SEQ";
       public static string HR_CLEARANCE_SEQ = "HR_CLEARANCE_SEQ";
       public static string HR_JOB_RESPONSIBILITY_SEQ = "HR_JOB_RESPONSIBILITY_SEQ";
       public static string HR_POSITIONS_SEQ = "HR_POSITIONS_SEQ";
       public static string hR_COMPETENCY_HDR_SEQ = "hR_COMPETENCY_HDR_SEQ";
       public static string hR_COMPETANCY_LINKS_HDR_SEQ = "hR_COMPETANCY_LINKS_HDR_SEQ";
       public static string pay_distribution_hdr_seq = "pay_distribution_hdr_seq";
       public static string pay_distribution_dtl_seq = "pay_distribution_dtl_seq";
       
       public static string HR_DEPT_DESIGNATIONS_SEQ = "HR_DEPT_DESIGNATIONS_SEQ";
       public static string HR_LEAVE_APPLICATIONS_SEQ = "HR_LEAVE_APPLICATIONS_SEQ";
       public static string TM_EMP_GROUP_LINKS_SEQ = "TM_EMP_GROUP_LINKS_SEQ";
       public static string iNV_RECEIPT_LOT_DTLS_SEQ = "iNV_RECEIPT_LOT_DTLS_SEQ";
       public static string iNV_RECEIPT_ITEM_WH_DTL_SEQ = "iNV_RECEIPT_ITEM_WH_DTL_SEQ";
       public static string TM_COMP_SCH_SEQ = "TM_COMP_SCH_SEQ";
       public static string INV_RECEIPT_LOT_DTLS_SEQ = "INV_RECEIPT_LOT_DTLS_SEQ";

       public const string AST_MAJOR_CATEGORY_SEQ = "AST_MAJOR_CATEGORY_SEQ";
       public const string ORG_ACADEMIC_STRUC_YEAR_SEQ = "ORG_ACADEMIC_STRUC_YEAR_SEQ";
       public const string ORG_CALENDAR_MASTER_SEQ = "ORG_CALENDAR_MASTER_SEQ";
       public const string ORG_ORGZN_STRUC_FOR_YEAR_SEQ = "ORG_ORGZN_STRUC_FOR_YEAR_SEQ";
       public const string ORG_LOCATION_STRUCTURE_SEQ = "ORG_LOCATION_STRUCTURE_SEQ";
       public const string ORG_LOCATION_MASTER_SEQ = "ORG_LOCATION_MASTER_SEQ";
       public const string ACD_EXAM_MASTER_SEQ = "ACD_EXAM_MASTER_SEQ";
       public const string ACD_EXAM_DETAIL_SEQ = "ACD_EXAM_DETAIL_SEQ";
       public const string ACD_FACILITY_MASTER_SEQ = "ACD_FACILITY_MASTER_SEQ";
       public const string ACD_THIRD_PARTY_MASTER_SEQ = "ACD_THIRD_PARTY_MASTER_SEQ";
       public const string ACD_EXAM_EVALUATION_SEQ = "ACD_EXAM_EVALUATION_SEQ";
       public const string ACD_ENTRANCE_ASSESSMENT_SEQ = "ACD_ENTRANCE_ASSESSMENT_SEQ";
       public const string ACD_FEE_MASTER_SEQ = "ACD_FEE_MASTER_SEQ";
       public const string ACD_SUBJECT_MASTER_SEQ = "ACD_SUBJECT_MASTER_SEQ";
       public const string ACD_COURSE_SUBJECT_SEQ = "ACD_COURSE_SUBJECT_SEQ";
       public const string ACD_STUDENT_REGISTRATION = "ACD_STUDENT_REGISTRATION_SEQ";
       public static string SYS_LOOKUP_TYPE_MASTER = "SYS_LOOKUP_TYPE_MASTER_SEQ";
       public static string TM_TABLE_EVENT = "TM_TABLE_EVENT_SEQ";
       public static string BASE_CURRENCY_SEQ = "BASE_CURRENCY_SEQ";
       public static string AST_ASSET_LOCATION_DTL_SEQ = "AST_ASSET_LOCATION_DTL_SEQ";
       public static string AST_DPRN_CATEGORY_CONFIG_SEQ = "AST_DPRN_CATEGORY_CONFIG_SEQ";
       public static string ASSET_OUTWARD_SERVICE_DTL_SEQ = "ASSET_OUTWARD_SERVICE_DTL_SEQ";
       public static string AST_ASSET_MST_SEQ = "AST_ASSET_MST_SEQ";
       public static string AST_ASSET_CATEGORY_DTL_SEQ = "AST_ASSET_CATEGORY_DTL_SEQ";
       public static string AST_ASSET_DISCARD_DTL_SEQ = "AST_ASSET_DISCARD_DTL_SEQ";
       public static string aST_DEPRECIATION_METHOD_SEQ = "aST_DEPRECIATION_METHOD_SEQ";
       public static string aST_LOCATION_MSt_seq = "aST_LOCATION_MSt_seq";
       public static string AST_MINOR_CATEGORY_DTL_SEQ = "AST_MINOR_CATEGORY_DTL_SEQ";
       public static string SSM_MENU_ITEMS_seq = "SSM_MENU_ITEMS_seq";

       public static string SSM_SCREENS_SEQ = "SSM_SCREENS_SEQ";
       public static string SSM_CITIES_SEQ = "SSM_CITIES_SEQ";
       public static string CUSTOMER_BANK_SEQ = "CUSTOMER_BANK_SEQ";
       public static string SSM_SYSTEM_PARAMETERS_SEQ = "SSM_SYSTEM_PARAMETERS_SEQ";
       public static string SSM_ROLES_SEQ = "SSM_ROLES_SEQ";
       public static string SSM_ROLE_MODULE_INTERSECTS_SEQ = "SSM_ROLE_MODULE_INTERSECTS_SEQ";
       public static string SSM_ROLE_FORM_PERMISSIONS_SEQ = "SSM_ROLE_FORM_PERMISSIONS_SEQ";

       public static string SSM_ERROR_MESSAGES_SEQ = "SSM_ERROR_MESSAGES_SEQ";
       public static string SSM_USER_ROLE_INTERSECTS_SEQ = "SSM_USER_ROLE_INTERSECTS_SEQ";
       public static string SSM_STATES_SEQ = "SSM_STATES_SEQ";
       public static string SSM_USER_ORG_INTERSECTS_SEQ = "SSM_USER_ORG_INTERSECTS_SEQ";
       public static string WFM_WORKFLOW_CODE_MASTERS_SEQ = "WFM_WORKFLOW_CODE_MASTERS_SEQ";
       public static string SSM_ALERT_MASTERS_SEQ = "SSM_ALERT_MASTERS_SEQ";
       public static string SSM_CODE_MASTERS_SEQ = "SSM_CODE_MASTERS_SEQ";
       public static string SSM_COUNTRIES_SEQ = "SSM_COUNTRIES_SEQ";
       public static string SSM_SEQUENCES_SEQ = "SSM_SEQUENCES_SEQ";
       public static string SSM_DOCUMENTS_SEQ = "SSM_DOCUMENTS_SEQ";
       
       public static string WFM_WORKFLOW_LEVELS_SEQ = "WFM_WORKFLOW_LEVELS_SEQ";
       public static string TM_EMP_TRANS_SEQ = "TM_EMP_TRANS_SEQ";

       //For Upload
       public const string TMP_FILE_DET_SEQ = "TMP_FILE_DET_SEQ";

    }
}

