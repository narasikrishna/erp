//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.Serialization;

namespace FIN.DAL
{
    [DataContract(IsReference = true)]
    public partial class SSM_SYSTEM_OPTIONS: IObjectWithChangeTracker, INotifyPropertyChanged
    {
        #region Primitive Properties
    
        [DataMember]
        public Nullable<int> PK_ID
        {
            get { return _pK_ID; }
            set
            {
                if (_pK_ID != value)
                {
                    _pK_ID = value;
                    OnPropertyChanged("PK_ID");
                }
            }
        }
        private Nullable<int> _pK_ID;
    
        [DataMember]
        public Nullable<int> CHILD_ID
        {
            get { return _cHILD_ID; }
            set
            {
                if (_cHILD_ID != value)
                {
                    _cHILD_ID = value;
                    OnPropertyChanged("CHILD_ID");
                }
            }
        }
        private Nullable<int> _cHILD_ID;
    
        [DataMember]
        public string SYS_OPT_ID
        {
            get { return _sYS_OPT_ID; }
            set
            {
                if (_sYS_OPT_ID != value)
                {
                    if (ChangeTracker.ChangeTrackingEnabled && ChangeTracker.State != ObjectState.Added)
                    {
                        throw new InvalidOperationException("The property 'SYS_OPT_ID' is part of the object's key and cannot be changed. Changes to key properties can only be made when the object is not being tracked or is in the Added state.");
                    }
                    _sYS_OPT_ID = value;
                    OnPropertyChanged("SYS_OPT_ID");
                }
            }
        }
        private string _sYS_OPT_ID;
    
        [DataMember]
        public string AP_LIABILITY_ACCT
        {
            get { return _aP_LIABILITY_ACCT; }
            set
            {
                if (_aP_LIABILITY_ACCT != value)
                {
                    _aP_LIABILITY_ACCT = value;
                    OnPropertyChanged("AP_LIABILITY_ACCT");
                }
            }
        }
        private string _aP_LIABILITY_ACCT;
    
        [DataMember]
        public string AP_ADVANCE_ACCT
        {
            get { return _aP_ADVANCE_ACCT; }
            set
            {
                if (_aP_ADVANCE_ACCT != value)
                {
                    _aP_ADVANCE_ACCT = value;
                    OnPropertyChanged("AP_ADVANCE_ACCT");
                }
            }
        }
        private string _aP_ADVANCE_ACCT;
    
        [DataMember]
        public string AP_RETENTION_ACCT
        {
            get { return _aP_RETENTION_ACCT; }
            set
            {
                if (_aP_RETENTION_ACCT != value)
                {
                    _aP_RETENTION_ACCT = value;
                    OnPropertyChanged("AP_RETENTION_ACCT");
                }
            }
        }
        private string _aP_RETENTION_ACCT;
    
        [DataMember]
        public string AP_DISCOUNT_ACCT
        {
            get { return _aP_DISCOUNT_ACCT; }
            set
            {
                if (_aP_DISCOUNT_ACCT != value)
                {
                    _aP_DISCOUNT_ACCT = value;
                    OnPropertyChanged("AP_DISCOUNT_ACCT");
                }
            }
        }
        private string _aP_DISCOUNT_ACCT;
    
        [DataMember]
        public string AP_EXCHANGE_GAIN_ACCT
        {
            get { return _aP_EXCHANGE_GAIN_ACCT; }
            set
            {
                if (_aP_EXCHANGE_GAIN_ACCT != value)
                {
                    _aP_EXCHANGE_GAIN_ACCT = value;
                    OnPropertyChanged("AP_EXCHANGE_GAIN_ACCT");
                }
            }
        }
        private string _aP_EXCHANGE_GAIN_ACCT;
    
        [DataMember]
        public string AP_EXCHANGE_LOSS_ACCT
        {
            get { return _aP_EXCHANGE_LOSS_ACCT; }
            set
            {
                if (_aP_EXCHANGE_LOSS_ACCT != value)
                {
                    _aP_EXCHANGE_LOSS_ACCT = value;
                    OnPropertyChanged("AP_EXCHANGE_LOSS_ACCT");
                }
            }
        }
        private string _aP_EXCHANGE_LOSS_ACCT;
    
        [DataMember]
        public Nullable<int> AP_PAYMENT_TERMS
        {
            get { return _aP_PAYMENT_TERMS; }
            set
            {
                if (_aP_PAYMENT_TERMS != value)
                {
                    _aP_PAYMENT_TERMS = value;
                    OnPropertyChanged("AP_PAYMENT_TERMS");
                }
            }
        }
        private Nullable<int> _aP_PAYMENT_TERMS;
    
        [DataMember]
        public Nullable<System.DateTime> AP_START_DATE
        {
            get { return _aP_START_DATE; }
            set
            {
                if (_aP_START_DATE != value)
                {
                    _aP_START_DATE = value;
                    OnPropertyChanged("AP_START_DATE");
                }
            }
        }
        private Nullable<System.DateTime> _aP_START_DATE;
    
        [DataMember]
        public Nullable<System.DateTime> AP_END_DATE
        {
            get { return _aP_END_DATE; }
            set
            {
                if (_aP_END_DATE != value)
                {
                    _aP_END_DATE = value;
                    OnPropertyChanged("AP_END_DATE");
                }
            }
        }
        private Nullable<System.DateTime> _aP_END_DATE;
    
        [DataMember]
        public string AP_ACTIVE
        {
            get { return _aP_ACTIVE; }
            set
            {
                if (_aP_ACTIVE != value)
                {
                    _aP_ACTIVE = value;
                    OnPropertyChanged("AP_ACTIVE");
                }
            }
        }
        private string _aP_ACTIVE;
    
        [DataMember]
        public string AR_DEBITOR_ACCT
        {
            get { return _aR_DEBITOR_ACCT; }
            set
            {
                if (_aR_DEBITOR_ACCT != value)
                {
                    _aR_DEBITOR_ACCT = value;
                    OnPropertyChanged("AR_DEBITOR_ACCT");
                }
            }
        }
        private string _aR_DEBITOR_ACCT;
    
        [DataMember]
        public string AR_REVENUE_ACCT
        {
            get { return _aR_REVENUE_ACCT; }
            set
            {
                if (_aR_REVENUE_ACCT != value)
                {
                    _aR_REVENUE_ACCT = value;
                    OnPropertyChanged("AR_REVENUE_ACCT");
                }
            }
        }
        private string _aR_REVENUE_ACCT;
    
        [DataMember]
        public string AR_DISCOUNT_ACCT
        {
            get { return _aR_DISCOUNT_ACCT; }
            set
            {
                if (_aR_DISCOUNT_ACCT != value)
                {
                    _aR_DISCOUNT_ACCT = value;
                    OnPropertyChanged("AR_DISCOUNT_ACCT");
                }
            }
        }
        private string _aR_DISCOUNT_ACCT;
    
        [DataMember]
        public string AR_EXCHANGE_GAIN_ACCT
        {
            get { return _aR_EXCHANGE_GAIN_ACCT; }
            set
            {
                if (_aR_EXCHANGE_GAIN_ACCT != value)
                {
                    _aR_EXCHANGE_GAIN_ACCT = value;
                    OnPropertyChanged("AR_EXCHANGE_GAIN_ACCT");
                }
            }
        }
        private string _aR_EXCHANGE_GAIN_ACCT;
    
        [DataMember]
        public string AR_EXCHANGE_LOSS_ACCT
        {
            get { return _aR_EXCHANGE_LOSS_ACCT; }
            set
            {
                if (_aR_EXCHANGE_LOSS_ACCT != value)
                {
                    _aR_EXCHANGE_LOSS_ACCT = value;
                    OnPropertyChanged("AR_EXCHANGE_LOSS_ACCT");
                }
            }
        }
        private string _aR_EXCHANGE_LOSS_ACCT;
    
        [DataMember]
        public Nullable<int> AR_PAYMENT_TERMS
        {
            get { return _aR_PAYMENT_TERMS; }
            set
            {
                if (_aR_PAYMENT_TERMS != value)
                {
                    _aR_PAYMENT_TERMS = value;
                    OnPropertyChanged("AR_PAYMENT_TERMS");
                }
            }
        }
        private Nullable<int> _aR_PAYMENT_TERMS;
    
        [DataMember]
        public Nullable<System.DateTime> AR_START_DATE
        {
            get { return _aR_START_DATE; }
            set
            {
                if (_aR_START_DATE != value)
                {
                    _aR_START_DATE = value;
                    OnPropertyChanged("AR_START_DATE");
                }
            }
        }
        private Nullable<System.DateTime> _aR_START_DATE;
    
        [DataMember]
        public Nullable<System.DateTime> AR_END_DATE
        {
            get { return _aR_END_DATE; }
            set
            {
                if (_aR_END_DATE != value)
                {
                    _aR_END_DATE = value;
                    OnPropertyChanged("AR_END_DATE");
                }
            }
        }
        private Nullable<System.DateTime> _aR_END_DATE;
    
        [DataMember]
        public string AR_ACTIVE
        {
            get { return _aR_ACTIVE; }
            set
            {
                if (_aR_ACTIVE != value)
                {
                    _aR_ACTIVE = value;
                    OnPropertyChanged("AR_ACTIVE");
                }
            }
        }
        private string _aR_ACTIVE;
    
        [DataMember]
        public string WORKFLOW_COMPLETION_STATUS
        {
            get { return _wORKFLOW_COMPLETION_STATUS; }
            set
            {
                if (_wORKFLOW_COMPLETION_STATUS != value)
                {
                    _wORKFLOW_COMPLETION_STATUS = value;
                    OnPropertyChanged("WORKFLOW_COMPLETION_STATUS");
                }
            }
        }
        private string _wORKFLOW_COMPLETION_STATUS;
    
        [DataMember]
        public string CREATED_BY
        {
            get { return _cREATED_BY; }
            set
            {
                if (_cREATED_BY != value)
                {
                    _cREATED_BY = value;
                    OnPropertyChanged("CREATED_BY");
                }
            }
        }
        private string _cREATED_BY;
    
        [DataMember]
        public Nullable<System.DateTime> CREATED_DATE
        {
            get { return _cREATED_DATE; }
            set
            {
                if (_cREATED_DATE != value)
                {
                    _cREATED_DATE = value;
                    OnPropertyChanged("CREATED_DATE");
                }
            }
        }
        private Nullable<System.DateTime> _cREATED_DATE;
    
        [DataMember]
        public string MODIFIED_BY
        {
            get { return _mODIFIED_BY; }
            set
            {
                if (_mODIFIED_BY != value)
                {
                    _mODIFIED_BY = value;
                    OnPropertyChanged("MODIFIED_BY");
                }
            }
        }
        private string _mODIFIED_BY;
    
        [DataMember]
        public Nullable<System.DateTime> MODIFIED_DATE
        {
            get { return _mODIFIED_DATE; }
            set
            {
                if (_mODIFIED_DATE != value)
                {
                    _mODIFIED_DATE = value;
                    OnPropertyChanged("MODIFIED_DATE");
                }
            }
        }
        private Nullable<System.DateTime> _mODIFIED_DATE;
    
        [DataMember]
        public string MODULE_CODE
        {
            get { return _mODULE_CODE; }
            set
            {
                if (_mODULE_CODE != value)
                {
                    _mODULE_CODE = value;
                    OnPropertyChanged("MODULE_CODE");
                }
            }
        }
        private string _mODULE_CODE;
    
        [DataMember]
        public Nullable<decimal> SSDP_EMPLOYEE
        {
            get { return _sSDP_EMPLOYEE; }
            set
            {
                if (_sSDP_EMPLOYEE != value)
                {
                    _sSDP_EMPLOYEE = value;
                    OnPropertyChanged("SSDP_EMPLOYEE");
                }
            }
        }
        private Nullable<decimal> _sSDP_EMPLOYEE;
    
        [DataMember]
        public Nullable<decimal> SSDP_EMPLOYER
        {
            get { return _sSDP_EMPLOYER; }
            set
            {
                if (_sSDP_EMPLOYER != value)
                {
                    _sSDP_EMPLOYER = value;
                    OnPropertyChanged("SSDP_EMPLOYER");
                }
            }
        }
        private Nullable<decimal> _sSDP_EMPLOYER;
    
        [DataMember]
        public Nullable<decimal> LOAN_ELIGIBLE_YEAR
        {
            get { return _lOAN_ELIGIBLE_YEAR; }
            set
            {
                if (_lOAN_ELIGIBLE_YEAR != value)
                {
                    _lOAN_ELIGIBLE_YEAR = value;
                    OnPropertyChanged("LOAN_ELIGIBLE_YEAR");
                }
            }
        }
        private Nullable<decimal> _lOAN_ELIGIBLE_YEAR;
    
        [DataMember]
        public Nullable<decimal> LOAN_REELIGIBLE_YEAR
        {
            get { return _lOAN_REELIGIBLE_YEAR; }
            set
            {
                if (_lOAN_REELIGIBLE_YEAR != value)
                {
                    _lOAN_REELIGIBLE_YEAR = value;
                    OnPropertyChanged("LOAN_REELIGIBLE_YEAR");
                }
            }
        }
        private Nullable<decimal> _lOAN_REELIGIBLE_YEAR;
    
        [DataMember]
        public Nullable<decimal> PERMISSION_HOURS
        {
            get { return _pERMISSION_HOURS; }
            set
            {
                if (_pERMISSION_HOURS != value)
                {
                    _pERMISSION_HOURS = value;
                    OnPropertyChanged("PERMISSION_HOURS");
                }
            }
        }
        private Nullable<decimal> _pERMISSION_HOURS;
    
        [DataMember]
        public Nullable<System.DateTime> HR_EFFECTIVE_FROM_DATE
        {
            get { return _hR_EFFECTIVE_FROM_DATE; }
            set
            {
                if (_hR_EFFECTIVE_FROM_DATE != value)
                {
                    _hR_EFFECTIVE_FROM_DATE = value;
                    OnPropertyChanged("HR_EFFECTIVE_FROM_DATE");
                }
            }
        }
        private Nullable<System.DateTime> _hR_EFFECTIVE_FROM_DATE;
    
        [DataMember]
        public Nullable<System.DateTime> HR_EFFECTIVE_TO_DATE
        {
            get { return _hR_EFFECTIVE_TO_DATE; }
            set
            {
                if (_hR_EFFECTIVE_TO_DATE != value)
                {
                    _hR_EFFECTIVE_TO_DATE = value;
                    OnPropertyChanged("HR_EFFECTIVE_TO_DATE");
                }
            }
        }
        private Nullable<System.DateTime> _hR_EFFECTIVE_TO_DATE;
    
        [DataMember]
        public string ORG_ID
        {
            get { return _oRG_ID; }
            set
            {
                if (_oRG_ID != value)
                {
                    _oRG_ID = value;
                    OnPropertyChanged("ORG_ID");
                }
            }
        }
        private string _oRG_ID;
    
        [DataMember]
        public string HR_PAY_ELEMENT_CODE
        {
            get { return _hR_PAY_ELEMENT_CODE; }
            set
            {
                if (_hR_PAY_ELEMENT_CODE != value)
                {
                    _hR_PAY_ELEMENT_CODE = value;
                    OnPropertyChanged("HR_PAY_ELEMENT_CODE");
                }
            }
        }
        private string _hR_PAY_ELEMENT_CODE;
    
        [DataMember]
        public string HR_BASIC_ELEMENT_CODE
        {
            get { return _hR_BASIC_ELEMENT_CODE; }
            set
            {
                if (_hR_BASIC_ELEMENT_CODE != value)
                {
                    _hR_BASIC_ELEMENT_CODE = value;
                    OnPropertyChanged("HR_BASIC_ELEMENT_CODE");
                }
            }
        }
        private string _hR_BASIC_ELEMENT_CODE;
    
        [DataMember]
        public string HR_MGRP_EMP_ELEMENT_CODE
        {
            get { return _hR_MGRP_EMP_ELEMENT_CODE; }
            set
            {
                if (_hR_MGRP_EMP_ELEMENT_CODE != value)
                {
                    _hR_MGRP_EMP_ELEMENT_CODE = value;
                    OnPropertyChanged("HR_MGRP_EMP_ELEMENT_CODE");
                }
            }
        }
        private string _hR_MGRP_EMP_ELEMENT_CODE;
    
        [DataMember]
        public string HR_MGRP_COMP_ELEMENT_CODE
        {
            get { return _hR_MGRP_COMP_ELEMENT_CODE; }
            set
            {
                if (_hR_MGRP_COMP_ELEMENT_CODE != value)
                {
                    _hR_MGRP_COMP_ELEMENT_CODE = value;
                    OnPropertyChanged("HR_MGRP_COMP_ELEMENT_CODE");
                }
            }
        }
        private string _hR_MGRP_COMP_ELEMENT_CODE;
    
        [DataMember]
        public string HR_MOBI_INVOICE_ELEMENT_CODE
        {
            get { return _hR_MOBI_INVOICE_ELEMENT_CODE; }
            set
            {
                if (_hR_MOBI_INVOICE_ELEMENT_CODE != value)
                {
                    _hR_MOBI_INVOICE_ELEMENT_CODE = value;
                    OnPropertyChanged("HR_MOBI_INVOICE_ELEMENT_CODE");
                }
            }
        }
        private string _hR_MOBI_INVOICE_ELEMENT_CODE;
    
        [DataMember]
        public string HR_MOBI_DEDUCT_ELEMENT_CODE
        {
            get { return _hR_MOBI_DEDUCT_ELEMENT_CODE; }
            set
            {
                if (_hR_MOBI_DEDUCT_ELEMENT_CODE != value)
                {
                    _hR_MOBI_DEDUCT_ELEMENT_CODE = value;
                    OnPropertyChanged("HR_MOBI_DEDUCT_ELEMENT_CODE");
                }
            }
        }
        private string _hR_MOBI_DEDUCT_ELEMENT_CODE;
    
        [DataMember]
        public string HR_MEDICAL_DEDUCT_ELEMENT_CODE
        {
            get { return _hR_MEDICAL_DEDUCT_ELEMENT_CODE; }
            set
            {
                if (_hR_MEDICAL_DEDUCT_ELEMENT_CODE != value)
                {
                    _hR_MEDICAL_DEDUCT_ELEMENT_CODE = value;
                    OnPropertyChanged("HR_MEDICAL_DEDUCT_ELEMENT_CODE");
                }
            }
        }
        private string _hR_MEDICAL_DEDUCT_ELEMENT_CODE;
    
        [DataMember]
        public string HR_RENT_DEDUCT_ELEMENT_CODE
        {
            get { return _hR_RENT_DEDUCT_ELEMENT_CODE; }
            set
            {
                if (_hR_RENT_DEDUCT_ELEMENT_CODE != value)
                {
                    _hR_RENT_DEDUCT_ELEMENT_CODE = value;
                    OnPropertyChanged("HR_RENT_DEDUCT_ELEMENT_CODE");
                }
            }
        }
        private string _hR_RENT_DEDUCT_ELEMENT_CODE;
    
        [DataMember]
        public string AP_DEFAULT_CASH_ACCT_CODE
        {
            get { return _aP_DEFAULT_CASH_ACCT_CODE; }
            set
            {
                if (_aP_DEFAULT_CASH_ACCT_CODE != value)
                {
                    _aP_DEFAULT_CASH_ACCT_CODE = value;
                    OnPropertyChanged("AP_DEFAULT_CASH_ACCT_CODE");
                }
            }
        }
        private string _aP_DEFAULT_CASH_ACCT_CODE;
    
        [DataMember]
        public string HR_EARN_LEAVE
        {
            get { return _hR_EARN_LEAVE; }
            set
            {
                if (_hR_EARN_LEAVE != value)
                {
                    _hR_EARN_LEAVE = value;
                    OnPropertyChanged("HR_EARN_LEAVE");
                }
            }
        }
        private string _hR_EARN_LEAVE;
    
        [DataMember]
        public string AR_DEFAULT_CASH_ACCT_CODE
        {
            get { return _aR_DEFAULT_CASH_ACCT_CODE; }
            set
            {
                if (_aR_DEFAULT_CASH_ACCT_CODE != value)
                {
                    _aR_DEFAULT_CASH_ACCT_CODE = value;
                    OnPropertyChanged("AR_DEFAULT_CASH_ACCT_CODE");
                }
            }
        }
        private string _aR_DEFAULT_CASH_ACCT_CODE;
    
        [DataMember]
        public string HR_LEAVE_SAL
        {
            get { return _hR_LEAVE_SAL; }
            set
            {
                if (_hR_LEAVE_SAL != value)
                {
                    _hR_LEAVE_SAL = value;
                    OnPropertyChanged("HR_LEAVE_SAL");
                }
            }
        }
        private string _hR_LEAVE_SAL;
    
        [DataMember]
        public string HR_LEAVE_ENCASH
        {
            get { return _hR_LEAVE_ENCASH; }
            set
            {
                if (_hR_LEAVE_ENCASH != value)
                {
                    _hR_LEAVE_ENCASH = value;
                    OnPropertyChanged("HR_LEAVE_ENCASH");
                }
            }
        }
        private string _hR_LEAVE_ENCASH;
    
        [DataMember]
        public string FA_ASSET_CST_ACC
        {
            get { return _fA_ASSET_CST_ACC; }
            set
            {
                if (_fA_ASSET_CST_ACC != value)
                {
                    _fA_ASSET_CST_ACC = value;
                    OnPropertyChanged("FA_ASSET_CST_ACC");
                }
            }
        }
        private string _fA_ASSET_CST_ACC;
    
        [DataMember]
        public string FA_DEPRICIATION_ACC
        {
            get { return _fA_DEPRICIATION_ACC; }
            set
            {
                if (_fA_DEPRICIATION_ACC != value)
                {
                    _fA_DEPRICIATION_ACC = value;
                    OnPropertyChanged("FA_DEPRICIATION_ACC");
                }
            }
        }
        private string _fA_DEPRICIATION_ACC;
    
        [DataMember]
        public string FA_ACCUM_DEPRICIATION_ACC
        {
            get { return _fA_ACCUM_DEPRICIATION_ACC; }
            set
            {
                if (_fA_ACCUM_DEPRICIATION_ACC != value)
                {
                    _fA_ACCUM_DEPRICIATION_ACC = value;
                    OnPropertyChanged("FA_ACCUM_DEPRICIATION_ACC");
                }
            }
        }
        private string _fA_ACCUM_DEPRICIATION_ACC;
    
        [DataMember]
        public string FA_DEPRICIATION_RUN
        {
            get { return _fA_DEPRICIATION_RUN; }
            set
            {
                if (_fA_DEPRICIATION_RUN != value)
                {
                    _fA_DEPRICIATION_RUN = value;
                    OnPropertyChanged("FA_DEPRICIATION_RUN");
                }
            }
        }
        private string _fA_DEPRICIATION_RUN;
    
        [DataMember]
        public string LEAVE_ADJ_CODE
        {
            get { return _lEAVE_ADJ_CODE; }
            set
            {
                if (_lEAVE_ADJ_CODE != value)
                {
                    _lEAVE_ADJ_CODE = value;
                    OnPropertyChanged("LEAVE_ADJ_CODE");
                }
            }
        }
        private string _lEAVE_ADJ_CODE;
    
        [DataMember]
        public string LEAVE_ADJ_EARN
        {
            get { return _lEAVE_ADJ_EARN; }
            set
            {
                if (_lEAVE_ADJ_EARN != value)
                {
                    _lEAVE_ADJ_EARN = value;
                    OnPropertyChanged("LEAVE_ADJ_EARN");
                }
            }
        }
        private string _lEAVE_ADJ_EARN;
    
        [DataMember]
        public string HR_LOAN_PAYMENT
        {
            get { return _hR_LOAN_PAYMENT; }
            set
            {
                if (_hR_LOAN_PAYMENT != value)
                {
                    _hR_LOAN_PAYMENT = value;
                    OnPropertyChanged("HR_LOAN_PAYMENT");
                }
            }
        }
        private string _hR_LOAN_PAYMENT;
    
        [DataMember]
        public string HR_LEAVE_INDM_SUSP_ACC
        {
            get { return _hR_LEAVE_INDM_SUSP_ACC; }
            set
            {
                if (_hR_LEAVE_INDM_SUSP_ACC != value)
                {
                    _hR_LEAVE_INDM_SUSP_ACC = value;
                    OnPropertyChanged("HR_LEAVE_INDM_SUSP_ACC");
                }
            }
        }
        private string _hR_LEAVE_INDM_SUSP_ACC;
    
        [DataMember]
        public string HR_OTHER_SUSP_ACC
        {
            get { return _hR_OTHER_SUSP_ACC; }
            set
            {
                if (_hR_OTHER_SUSP_ACC != value)
                {
                    _hR_OTHER_SUSP_ACC = value;
                    OnPropertyChanged("HR_OTHER_SUSP_ACC");
                }
            }
        }
        private string _hR_OTHER_SUSP_ACC;
    
        [DataMember]
        public string HR_LEAVE_ADJUSTMENT_MONTH
        {
            get { return _hR_LEAVE_ADJUSTMENT_MONTH; }
            set
            {
                if (_hR_LEAVE_ADJUSTMENT_MONTH != value)
                {
                    _hR_LEAVE_ADJUSTMENT_MONTH = value;
                    OnPropertyChanged("HR_LEAVE_ADJUSTMENT_MONTH");
                }
            }
        }
        private string _hR_LEAVE_ADJUSTMENT_MONTH;
    
        [DataMember]
        public string HR_LEAVE_SUSPENS_AC
        {
            get { return _hR_LEAVE_SUSPENS_AC; }
            set
            {
                if (_hR_LEAVE_SUSPENS_AC != value)
                {
                    _hR_LEAVE_SUSPENS_AC = value;
                    OnPropertyChanged("HR_LEAVE_SUSPENS_AC");
                }
            }
        }
        private string _hR_LEAVE_SUSPENS_AC;
    
        [DataMember]
        public string HR_INDEM_EXPENSE_AC
        {
            get { return _hR_INDEM_EXPENSE_AC; }
            set
            {
                if (_hR_INDEM_EXPENSE_AC != value)
                {
                    _hR_INDEM_EXPENSE_AC = value;
                    OnPropertyChanged("HR_INDEM_EXPENSE_AC");
                }
            }
        }
        private string _hR_INDEM_EXPENSE_AC;
    
        [DataMember]
        public string AR_CLEARANCE_ACC
        {
            get { return _aR_CLEARANCE_ACC; }
            set
            {
                if (_aR_CLEARANCE_ACC != value)
                {
                    _aR_CLEARANCE_ACC = value;
                    OnPropertyChanged("AR_CLEARANCE_ACC");
                }
            }
        }
        private string _aR_CLEARANCE_ACC;

        #endregion
        #region ChangeTracking
    
        protected virtual void OnPropertyChanged(String propertyName)
        {
            if (ChangeTracker.State != ObjectState.Added && ChangeTracker.State != ObjectState.Deleted)
            {
                ChangeTracker.State = ObjectState.Modified;
            }
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        protected virtual void OnNavigationPropertyChanged(String propertyName)
        {
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        event PropertyChangedEventHandler INotifyPropertyChanged.PropertyChanged{ add { _propertyChanged += value; } remove { _propertyChanged -= value; } }
        private event PropertyChangedEventHandler _propertyChanged;
        private ObjectChangeTracker _changeTracker;
    
        [DataMember]
        public ObjectChangeTracker ChangeTracker
        {
            get
            {
                if (_changeTracker == null)
                {
                    _changeTracker = new ObjectChangeTracker();
                    _changeTracker.ObjectStateChanging += HandleObjectStateChanging;
                }
                return _changeTracker;
            }
            set
            {
                if(_changeTracker != null)
                {
                    _changeTracker.ObjectStateChanging -= HandleObjectStateChanging;
                }
                _changeTracker = value;
                if(_changeTracker != null)
                {
                    _changeTracker.ObjectStateChanging += HandleObjectStateChanging;
                }
            }
        }
    
        private void HandleObjectStateChanging(object sender, ObjectStateChangingEventArgs e)
        {
            if (e.NewState == ObjectState.Deleted)
            {
                ClearNavigationProperties();
            }
        }
    
        protected bool IsDeserializing { get; private set; }
    
        [OnDeserializing]
        public void OnDeserializingMethod(StreamingContext context)
        {
            IsDeserializing = true;
        }
    
        [OnDeserialized]
        public void OnDeserializedMethod(StreamingContext context)
        {
            IsDeserializing = false;
            ChangeTracker.ChangeTrackingEnabled = true;
        }
    
        protected virtual void ClearNavigationProperties()
        {
        }

        #endregion
    }
}
