//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.Serialization;

namespace FIN.DAL
{
    [DataContract(IsReference = true)]
    [KnownType(typeof(INV_PRICE_LIST_HDR))]
    [KnownType(typeof(OM_CUSTOMER_ORDER_HDR))]
    [KnownType(typeof(OM_SALE_QUOTE_DTL))]
    [KnownType(typeof(SUPPLIER_CUSTOMERS))]
    [KnownType(typeof(SSM_CURRENCIES))]
    public partial class OM_SALE_QUOTE_HDR: IObjectWithChangeTracker, INotifyPropertyChanged
    {
        #region Primitive Properties
    
        [DataMember]
        public Nullable<int> PK_ID
        {
            get { return _pK_ID; }
            set
            {
                if (_pK_ID != value)
                {
                    _pK_ID = value;
                    OnPropertyChanged("PK_ID");
                }
            }
        }
        private Nullable<int> _pK_ID;
    
        [DataMember]
        public Nullable<int> CHILD_ID
        {
            get { return _cHILD_ID; }
            set
            {
                if (_cHILD_ID != value)
                {
                    _cHILD_ID = value;
                    OnPropertyChanged("CHILD_ID");
                }
            }
        }
        private Nullable<int> _cHILD_ID;
    
        [DataMember]
        public string QUOTE_HDR_ID
        {
            get { return _qUOTE_HDR_ID; }
            set
            {
                if (_qUOTE_HDR_ID != value)
                {
                    if (ChangeTracker.ChangeTrackingEnabled && ChangeTracker.State != ObjectState.Added)
                    {
                        throw new InvalidOperationException("The property 'QUOTE_HDR_ID' is part of the object's key and cannot be changed. Changes to key properties can only be made when the object is not being tracked or is in the Added state.");
                    }
                    _qUOTE_HDR_ID = value;
                    OnPropertyChanged("QUOTE_HDR_ID");
                }
            }
        }
        private string _qUOTE_HDR_ID;
    
        [DataMember]
        public string QUOTE_NAME
        {
            get { return _qUOTE_NAME; }
            set
            {
                if (_qUOTE_NAME != value)
                {
                    _qUOTE_NAME = value;
                    OnPropertyChanged("QUOTE_NAME");
                }
            }
        }
        private string _qUOTE_NAME;
    
        [DataMember]
        public string QUOTE_CUST_ID
        {
            get { return _qUOTE_CUST_ID; }
            set
            {
                if (_qUOTE_CUST_ID != value)
                {
                    ChangeTracker.RecordOriginalValue("QUOTE_CUST_ID", _qUOTE_CUST_ID);
                    if (!IsDeserializing)
                    {
                        if (SUPPLIER_CUSTOMERS != null && SUPPLIER_CUSTOMERS.VENDOR_ID != value)
                        {
                            SUPPLIER_CUSTOMERS = null;
                        }
                    }
                    _qUOTE_CUST_ID = value;
                    OnPropertyChanged("QUOTE_CUST_ID");
                }
            }
        }
        private string _qUOTE_CUST_ID;
    
        [DataMember]
        public string QUOTE_STATUS
        {
            get { return _qUOTE_STATUS; }
            set
            {
                if (_qUOTE_STATUS != value)
                {
                    _qUOTE_STATUS = value;
                    OnPropertyChanged("QUOTE_STATUS");
                }
            }
        }
        private string _qUOTE_STATUS;
    
        [DataMember]
        public Nullable<System.DateTime> QUOTE_EXPIRE_DATE
        {
            get { return _qUOTE_EXPIRE_DATE; }
            set
            {
                if (_qUOTE_EXPIRE_DATE != value)
                {
                    _qUOTE_EXPIRE_DATE = value;
                    OnPropertyChanged("QUOTE_EXPIRE_DATE");
                }
            }
        }
        private Nullable<System.DateTime> _qUOTE_EXPIRE_DATE;
    
        [DataMember]
        public Nullable<decimal> QUOTE_AMOUNT
        {
            get { return _qUOTE_AMOUNT; }
            set
            {
                if (_qUOTE_AMOUNT != value)
                {
                    _qUOTE_AMOUNT = value;
                    OnPropertyChanged("QUOTE_AMOUNT");
                }
            }
        }
        private Nullable<decimal> _qUOTE_AMOUNT;
    
        [DataMember]
        public Nullable<System.DateTime> QUOTE_DATE
        {
            get { return _qUOTE_DATE; }
            set
            {
                if (_qUOTE_DATE != value)
                {
                    _qUOTE_DATE = value;
                    OnPropertyChanged("QUOTE_DATE");
                }
            }
        }
        private Nullable<System.DateTime> _qUOTE_DATE;
    
        [DataMember]
        public string QUOTE_CURRENCY
        {
            get { return _qUOTE_CURRENCY; }
            set
            {
                if (_qUOTE_CURRENCY != value)
                {
                    ChangeTracker.RecordOriginalValue("QUOTE_CURRENCY", _qUOTE_CURRENCY);
                    if (!IsDeserializing)
                    {
                        if (SSM_CURRENCIES != null && SSM_CURRENCIES.CURRENCY_ID != value)
                        {
                            SSM_CURRENCIES = null;
                        }
                    }
                    _qUOTE_CURRENCY = value;
                    OnPropertyChanged("QUOTE_CURRENCY");
                }
            }
        }
        private string _qUOTE_CURRENCY;
    
        [DataMember]
        public string QUOTE_PAY_TERM_ID
        {
            get { return _qUOTE_PAY_TERM_ID; }
            set
            {
                if (_qUOTE_PAY_TERM_ID != value)
                {
                    _qUOTE_PAY_TERM_ID = value;
                    OnPropertyChanged("QUOTE_PAY_TERM_ID");
                }
            }
        }
        private string _qUOTE_PAY_TERM_ID;
    
        [DataMember]
        public Nullable<System.DateTime> QUOTE_REQ_DATE
        {
            get { return _qUOTE_REQ_DATE; }
            set
            {
                if (_qUOTE_REQ_DATE != value)
                {
                    _qUOTE_REQ_DATE = value;
                    OnPropertyChanged("QUOTE_REQ_DATE");
                }
            }
        }
        private Nullable<System.DateTime> _qUOTE_REQ_DATE;
    
        [DataMember]
        public string QUOTE_CARRIER_SERVICE
        {
            get { return _qUOTE_CARRIER_SERVICE; }
            set
            {
                if (_qUOTE_CARRIER_SERVICE != value)
                {
                    _qUOTE_CARRIER_SERVICE = value;
                    OnPropertyChanged("QUOTE_CARRIER_SERVICE");
                }
            }
        }
        private string _qUOTE_CARRIER_SERVICE;
    
        [DataMember]
        public string QUOTE_SHIP_INSTRUCTION
        {
            get { return _qUOTE_SHIP_INSTRUCTION; }
            set
            {
                if (_qUOTE_SHIP_INSTRUCTION != value)
                {
                    _qUOTE_SHIP_INSTRUCTION = value;
                    OnPropertyChanged("QUOTE_SHIP_INSTRUCTION");
                }
            }
        }
        private string _qUOTE_SHIP_INSTRUCTION;
    
        [DataMember]
        public string QUOTE_NOTE
        {
            get { return _qUOTE_NOTE; }
            set
            {
                if (_qUOTE_NOTE != value)
                {
                    _qUOTE_NOTE = value;
                    OnPropertyChanged("QUOTE_NOTE");
                }
            }
        }
        private string _qUOTE_NOTE;
    
        [DataMember]
        public string ENABLED_FLAG
        {
            get { return _eNABLED_FLAG; }
            set
            {
                if (_eNABLED_FLAG != value)
                {
                    _eNABLED_FLAG = value;
                    OnPropertyChanged("ENABLED_FLAG");
                }
            }
        }
        private string _eNABLED_FLAG;
    
        [DataMember]
        public string WORKFLOW_COMPLETION_STATUS
        {
            get { return _wORKFLOW_COMPLETION_STATUS; }
            set
            {
                if (_wORKFLOW_COMPLETION_STATUS != value)
                {
                    _wORKFLOW_COMPLETION_STATUS = value;
                    OnPropertyChanged("WORKFLOW_COMPLETION_STATUS");
                }
            }
        }
        private string _wORKFLOW_COMPLETION_STATUS;
    
        [DataMember]
        public string CREATED_BY
        {
            get { return _cREATED_BY; }
            set
            {
                if (_cREATED_BY != value)
                {
                    _cREATED_BY = value;
                    OnPropertyChanged("CREATED_BY");
                }
            }
        }
        private string _cREATED_BY;
    
        [DataMember]
        public System.DateTime CREATED_DATE
        {
            get { return _cREATED_DATE; }
            set
            {
                if (_cREATED_DATE != value)
                {
                    _cREATED_DATE = value;
                    OnPropertyChanged("CREATED_DATE");
                }
            }
        }
        private System.DateTime _cREATED_DATE;
    
        [DataMember]
        public string MODIFIED_BY
        {
            get { return _mODIFIED_BY; }
            set
            {
                if (_mODIFIED_BY != value)
                {
                    _mODIFIED_BY = value;
                    OnPropertyChanged("MODIFIED_BY");
                }
            }
        }
        private string _mODIFIED_BY;
    
        [DataMember]
        public Nullable<System.DateTime> MODIFIED_DATE
        {
            get { return _mODIFIED_DATE; }
            set
            {
                if (_mODIFIED_DATE != value)
                {
                    _mODIFIED_DATE = value;
                    OnPropertyChanged("MODIFIED_DATE");
                }
            }
        }
        private Nullable<System.DateTime> _mODIFIED_DATE;
    
        [DataMember]
        public string ATTRIBUTE1
        {
            get { return _aTTRIBUTE1; }
            set
            {
                if (_aTTRIBUTE1 != value)
                {
                    _aTTRIBUTE1 = value;
                    OnPropertyChanged("ATTRIBUTE1");
                }
            }
        }
        private string _aTTRIBUTE1;
    
        [DataMember]
        public string ATTRIBUTE2
        {
            get { return _aTTRIBUTE2; }
            set
            {
                if (_aTTRIBUTE2 != value)
                {
                    _aTTRIBUTE2 = value;
                    OnPropertyChanged("ATTRIBUTE2");
                }
            }
        }
        private string _aTTRIBUTE2;
    
        [DataMember]
        public string ATTRIBUTE3
        {
            get { return _aTTRIBUTE3; }
            set
            {
                if (_aTTRIBUTE3 != value)
                {
                    _aTTRIBUTE3 = value;
                    OnPropertyChanged("ATTRIBUTE3");
                }
            }
        }
        private string _aTTRIBUTE3;
    
        [DataMember]
        public string ATTRIBUTE4
        {
            get { return _aTTRIBUTE4; }
            set
            {
                if (_aTTRIBUTE4 != value)
                {
                    _aTTRIBUTE4 = value;
                    OnPropertyChanged("ATTRIBUTE4");
                }
            }
        }
        private string _aTTRIBUTE4;
    
        [DataMember]
        public string ATTRIBUTE5
        {
            get { return _aTTRIBUTE5; }
            set
            {
                if (_aTTRIBUTE5 != value)
                {
                    _aTTRIBUTE5 = value;
                    OnPropertyChanged("ATTRIBUTE5");
                }
            }
        }
        private string _aTTRIBUTE5;
    
        [DataMember]
        public string ATTRIBUTE6
        {
            get { return _aTTRIBUTE6; }
            set
            {
                if (_aTTRIBUTE6 != value)
                {
                    _aTTRIBUTE6 = value;
                    OnPropertyChanged("ATTRIBUTE6");
                }
            }
        }
        private string _aTTRIBUTE6;
    
        [DataMember]
        public string ATTRIBUTE7
        {
            get { return _aTTRIBUTE7; }
            set
            {
                if (_aTTRIBUTE7 != value)
                {
                    _aTTRIBUTE7 = value;
                    OnPropertyChanged("ATTRIBUTE7");
                }
            }
        }
        private string _aTTRIBUTE7;
    
        [DataMember]
        public string ATTRIBUTE8
        {
            get { return _aTTRIBUTE8; }
            set
            {
                if (_aTTRIBUTE8 != value)
                {
                    _aTTRIBUTE8 = value;
                    OnPropertyChanged("ATTRIBUTE8");
                }
            }
        }
        private string _aTTRIBUTE8;
    
        [DataMember]
        public string ATTRIBUTE9
        {
            get { return _aTTRIBUTE9; }
            set
            {
                if (_aTTRIBUTE9 != value)
                {
                    _aTTRIBUTE9 = value;
                    OnPropertyChanged("ATTRIBUTE9");
                }
            }
        }
        private string _aTTRIBUTE9;
    
        [DataMember]
        public string ATTRIBUTE10
        {
            get { return _aTTRIBUTE10; }
            set
            {
                if (_aTTRIBUTE10 != value)
                {
                    _aTTRIBUTE10 = value;
                    OnPropertyChanged("ATTRIBUTE10");
                }
            }
        }
        private string _aTTRIBUTE10;
    
        [DataMember]
        public string QUOTE_TYPE
        {
            get { return _qUOTE_TYPE; }
            set
            {
                if (_qUOTE_TYPE != value)
                {
                    _qUOTE_TYPE = value;
                    OnPropertyChanged("QUOTE_TYPE");
                }
            }
        }
        private string _qUOTE_TYPE;
    
        [DataMember]
        public string OM_PRICE_HDR_ID
        {
            get { return _oM_PRICE_HDR_ID; }
            set
            {
                if (_oM_PRICE_HDR_ID != value)
                {
                    ChangeTracker.RecordOriginalValue("OM_PRICE_HDR_ID", _oM_PRICE_HDR_ID);
                    if (!IsDeserializing)
                    {
                        if (INV_PRICE_LIST_HDR != null && INV_PRICE_LIST_HDR.PRICE_HDR_ID != value)
                        {
                            INV_PRICE_LIST_HDR = null;
                        }
                    }
                    _oM_PRICE_HDR_ID = value;
                    OnPropertyChanged("OM_PRICE_HDR_ID");
                }
            }
        }
        private string _oM_PRICE_HDR_ID;
    
        [DataMember]
        public string OM_DELIVERY_MODE
        {
            get { return _oM_DELIVERY_MODE; }
            set
            {
                if (_oM_DELIVERY_MODE != value)
                {
                    _oM_DELIVERY_MODE = value;
                    OnPropertyChanged("OM_DELIVERY_MODE");
                }
            }
        }
        private string _oM_DELIVERY_MODE;
    
        [DataMember]
        public Nullable<System.DateTime> OM_EXPECTED_DELIVERY_DT
        {
            get { return _oM_EXPECTED_DELIVERY_DT; }
            set
            {
                if (_oM_EXPECTED_DELIVERY_DT != value)
                {
                    _oM_EXPECTED_DELIVERY_DT = value;
                    OnPropertyChanged("OM_EXPECTED_DELIVERY_DT");
                }
            }
        }
        private Nullable<System.DateTime> _oM_EXPECTED_DELIVERY_DT;
    
        [DataMember]
        public string QUOTE_ORG_ID
        {
            get { return _qUOTE_ORG_ID; }
            set
            {
                if (_qUOTE_ORG_ID != value)
                {
                    _qUOTE_ORG_ID = value;
                    OnPropertyChanged("QUOTE_ORG_ID");
                }
            }
        }
        private string _qUOTE_ORG_ID;

        #endregion
        #region Navigation Properties
    
        [DataMember]
        public INV_PRICE_LIST_HDR INV_PRICE_LIST_HDR
        {
            get { return _iNV_PRICE_LIST_HDR; }
            set
            {
                if (!ReferenceEquals(_iNV_PRICE_LIST_HDR, value))
                {
                    var previousValue = _iNV_PRICE_LIST_HDR;
                    _iNV_PRICE_LIST_HDR = value;
                    FixupINV_PRICE_LIST_HDR(previousValue);
                    OnNavigationPropertyChanged("INV_PRICE_LIST_HDR");
                }
            }
        }
        private INV_PRICE_LIST_HDR _iNV_PRICE_LIST_HDR;
    
        [DataMember]
        public TrackableCollection<OM_CUSTOMER_ORDER_HDR> OM_CUSTOMER_ORDER_HDR
        {
            get
            {
                if (_oM_CUSTOMER_ORDER_HDR == null)
                {
                    _oM_CUSTOMER_ORDER_HDR = new TrackableCollection<OM_CUSTOMER_ORDER_HDR>();
                    _oM_CUSTOMER_ORDER_HDR.CollectionChanged += FixupOM_CUSTOMER_ORDER_HDR;
                }
                return _oM_CUSTOMER_ORDER_HDR;
            }
            set
            {
                if (!ReferenceEquals(_oM_CUSTOMER_ORDER_HDR, value))
                {
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        throw new InvalidOperationException("Cannot set the FixupChangeTrackingCollection when ChangeTracking is enabled");
                    }
                    if (_oM_CUSTOMER_ORDER_HDR != null)
                    {
                        _oM_CUSTOMER_ORDER_HDR.CollectionChanged -= FixupOM_CUSTOMER_ORDER_HDR;
                    }
                    _oM_CUSTOMER_ORDER_HDR = value;
                    if (_oM_CUSTOMER_ORDER_HDR != null)
                    {
                        _oM_CUSTOMER_ORDER_HDR.CollectionChanged += FixupOM_CUSTOMER_ORDER_HDR;
                    }
                    OnNavigationPropertyChanged("OM_CUSTOMER_ORDER_HDR");
                }
            }
        }
        private TrackableCollection<OM_CUSTOMER_ORDER_HDR> _oM_CUSTOMER_ORDER_HDR;
    
        [DataMember]
        public TrackableCollection<OM_SALE_QUOTE_DTL> OM_SALE_QUOTE_DTL
        {
            get
            {
                if (_oM_SALE_QUOTE_DTL == null)
                {
                    _oM_SALE_QUOTE_DTL = new TrackableCollection<OM_SALE_QUOTE_DTL>();
                    _oM_SALE_QUOTE_DTL.CollectionChanged += FixupOM_SALE_QUOTE_DTL;
                }
                return _oM_SALE_QUOTE_DTL;
            }
            set
            {
                if (!ReferenceEquals(_oM_SALE_QUOTE_DTL, value))
                {
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        throw new InvalidOperationException("Cannot set the FixupChangeTrackingCollection when ChangeTracking is enabled");
                    }
                    if (_oM_SALE_QUOTE_DTL != null)
                    {
                        _oM_SALE_QUOTE_DTL.CollectionChanged -= FixupOM_SALE_QUOTE_DTL;
                    }
                    _oM_SALE_QUOTE_DTL = value;
                    if (_oM_SALE_QUOTE_DTL != null)
                    {
                        _oM_SALE_QUOTE_DTL.CollectionChanged += FixupOM_SALE_QUOTE_DTL;
                    }
                    OnNavigationPropertyChanged("OM_SALE_QUOTE_DTL");
                }
            }
        }
        private TrackableCollection<OM_SALE_QUOTE_DTL> _oM_SALE_QUOTE_DTL;
    
        [DataMember]
        public SUPPLIER_CUSTOMERS SUPPLIER_CUSTOMERS
        {
            get { return _sUPPLIER_CUSTOMERS; }
            set
            {
                if (!ReferenceEquals(_sUPPLIER_CUSTOMERS, value))
                {
                    var previousValue = _sUPPLIER_CUSTOMERS;
                    _sUPPLIER_CUSTOMERS = value;
                    FixupSUPPLIER_CUSTOMERS(previousValue);
                    OnNavigationPropertyChanged("SUPPLIER_CUSTOMERS");
                }
            }
        }
        private SUPPLIER_CUSTOMERS _sUPPLIER_CUSTOMERS;
    
        [DataMember]
        public SSM_CURRENCIES SSM_CURRENCIES
        {
            get { return _sSM_CURRENCIES; }
            set
            {
                if (!ReferenceEquals(_sSM_CURRENCIES, value))
                {
                    var previousValue = _sSM_CURRENCIES;
                    _sSM_CURRENCIES = value;
                    FixupSSM_CURRENCIES(previousValue);
                    OnNavigationPropertyChanged("SSM_CURRENCIES");
                }
            }
        }
        private SSM_CURRENCIES _sSM_CURRENCIES;

        #endregion
        #region ChangeTracking
    
        protected virtual void OnPropertyChanged(String propertyName)
        {
            if (ChangeTracker.State != ObjectState.Added && ChangeTracker.State != ObjectState.Deleted)
            {
                ChangeTracker.State = ObjectState.Modified;
            }
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        protected virtual void OnNavigationPropertyChanged(String propertyName)
        {
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        event PropertyChangedEventHandler INotifyPropertyChanged.PropertyChanged{ add { _propertyChanged += value; } remove { _propertyChanged -= value; } }
        private event PropertyChangedEventHandler _propertyChanged;
        private ObjectChangeTracker _changeTracker;
    
        [DataMember]
        public ObjectChangeTracker ChangeTracker
        {
            get
            {
                if (_changeTracker == null)
                {
                    _changeTracker = new ObjectChangeTracker();
                    _changeTracker.ObjectStateChanging += HandleObjectStateChanging;
                }
                return _changeTracker;
            }
            set
            {
                if(_changeTracker != null)
                {
                    _changeTracker.ObjectStateChanging -= HandleObjectStateChanging;
                }
                _changeTracker = value;
                if(_changeTracker != null)
                {
                    _changeTracker.ObjectStateChanging += HandleObjectStateChanging;
                }
            }
        }
    
        private void HandleObjectStateChanging(object sender, ObjectStateChangingEventArgs e)
        {
            if (e.NewState == ObjectState.Deleted)
            {
                ClearNavigationProperties();
            }
        }
    
        protected bool IsDeserializing { get; private set; }
    
        [OnDeserializing]
        public void OnDeserializingMethod(StreamingContext context)
        {
            IsDeserializing = true;
        }
    
        [OnDeserialized]
        public void OnDeserializedMethod(StreamingContext context)
        {
            IsDeserializing = false;
            ChangeTracker.ChangeTrackingEnabled = true;
        }
    
        protected virtual void ClearNavigationProperties()
        {
            INV_PRICE_LIST_HDR = null;
            OM_CUSTOMER_ORDER_HDR.Clear();
            OM_SALE_QUOTE_DTL.Clear();
            SUPPLIER_CUSTOMERS = null;
            SSM_CURRENCIES = null;
        }

        #endregion
        #region Association Fixup
    
        private void FixupINV_PRICE_LIST_HDR(INV_PRICE_LIST_HDR previousValue, bool skipKeys = false)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (previousValue != null && previousValue.OM_SALE_QUOTE_HDR.Contains(this))
            {
                previousValue.OM_SALE_QUOTE_HDR.Remove(this);
            }
    
            if (INV_PRICE_LIST_HDR != null)
            {
                if (!INV_PRICE_LIST_HDR.OM_SALE_QUOTE_HDR.Contains(this))
                {
                    INV_PRICE_LIST_HDR.OM_SALE_QUOTE_HDR.Add(this);
                }
    
                OM_PRICE_HDR_ID = INV_PRICE_LIST_HDR.PRICE_HDR_ID;
            }
            else if (!skipKeys)
            {
                OM_PRICE_HDR_ID = null;
            }
    
            if (ChangeTracker.ChangeTrackingEnabled)
            {
                if (ChangeTracker.OriginalValues.ContainsKey("INV_PRICE_LIST_HDR")
                    && (ChangeTracker.OriginalValues["INV_PRICE_LIST_HDR"] == INV_PRICE_LIST_HDR))
                {
                    ChangeTracker.OriginalValues.Remove("INV_PRICE_LIST_HDR");
                }
                else
                {
                    ChangeTracker.RecordOriginalValue("INV_PRICE_LIST_HDR", previousValue);
                }
                if (INV_PRICE_LIST_HDR != null && !INV_PRICE_LIST_HDR.ChangeTracker.ChangeTrackingEnabled)
                {
                    INV_PRICE_LIST_HDR.StartTracking();
                }
            }
        }
    
        private void FixupSUPPLIER_CUSTOMERS(SUPPLIER_CUSTOMERS previousValue, bool skipKeys = false)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (previousValue != null && previousValue.OM_SALE_QUOTE_HDR.Contains(this))
            {
                previousValue.OM_SALE_QUOTE_HDR.Remove(this);
            }
    
            if (SUPPLIER_CUSTOMERS != null)
            {
                if (!SUPPLIER_CUSTOMERS.OM_SALE_QUOTE_HDR.Contains(this))
                {
                    SUPPLIER_CUSTOMERS.OM_SALE_QUOTE_HDR.Add(this);
                }
    
                QUOTE_CUST_ID = SUPPLIER_CUSTOMERS.VENDOR_ID;
            }
            else if (!skipKeys)
            {
                QUOTE_CUST_ID = null;
            }
    
            if (ChangeTracker.ChangeTrackingEnabled)
            {
                if (ChangeTracker.OriginalValues.ContainsKey("SUPPLIER_CUSTOMERS")
                    && (ChangeTracker.OriginalValues["SUPPLIER_CUSTOMERS"] == SUPPLIER_CUSTOMERS))
                {
                    ChangeTracker.OriginalValues.Remove("SUPPLIER_CUSTOMERS");
                }
                else
                {
                    ChangeTracker.RecordOriginalValue("SUPPLIER_CUSTOMERS", previousValue);
                }
                if (SUPPLIER_CUSTOMERS != null && !SUPPLIER_CUSTOMERS.ChangeTracker.ChangeTrackingEnabled)
                {
                    SUPPLIER_CUSTOMERS.StartTracking();
                }
            }
        }
    
        private void FixupSSM_CURRENCIES(SSM_CURRENCIES previousValue, bool skipKeys = false)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (previousValue != null && previousValue.OM_SALE_QUOTE_HDR.Contains(this))
            {
                previousValue.OM_SALE_QUOTE_HDR.Remove(this);
            }
    
            if (SSM_CURRENCIES != null)
            {
                if (!SSM_CURRENCIES.OM_SALE_QUOTE_HDR.Contains(this))
                {
                    SSM_CURRENCIES.OM_SALE_QUOTE_HDR.Add(this);
                }
    
                QUOTE_CURRENCY = SSM_CURRENCIES.CURRENCY_ID;
            }
            else if (!skipKeys)
            {
                QUOTE_CURRENCY = null;
            }
    
            if (ChangeTracker.ChangeTrackingEnabled)
            {
                if (ChangeTracker.OriginalValues.ContainsKey("SSM_CURRENCIES")
                    && (ChangeTracker.OriginalValues["SSM_CURRENCIES"] == SSM_CURRENCIES))
                {
                    ChangeTracker.OriginalValues.Remove("SSM_CURRENCIES");
                }
                else
                {
                    ChangeTracker.RecordOriginalValue("SSM_CURRENCIES", previousValue);
                }
                if (SSM_CURRENCIES != null && !SSM_CURRENCIES.ChangeTracker.ChangeTrackingEnabled)
                {
                    SSM_CURRENCIES.StartTracking();
                }
            }
        }
    
        private void FixupOM_CUSTOMER_ORDER_HDR(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (e.NewItems != null)
            {
                foreach (OM_CUSTOMER_ORDER_HDR item in e.NewItems)
                {
                    item.OM_SALE_QUOTE_HDR = this;
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        if (!item.ChangeTracker.ChangeTrackingEnabled)
                        {
                            item.StartTracking();
                        }
                        ChangeTracker.RecordAdditionToCollectionProperties("OM_CUSTOMER_ORDER_HDR", item);
                    }
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (OM_CUSTOMER_ORDER_HDR item in e.OldItems)
                {
                    if (ReferenceEquals(item.OM_SALE_QUOTE_HDR, this))
                    {
                        item.OM_SALE_QUOTE_HDR = null;
                    }
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        ChangeTracker.RecordRemovalFromCollectionProperties("OM_CUSTOMER_ORDER_HDR", item);
                    }
                }
            }
        }
    
        private void FixupOM_SALE_QUOTE_DTL(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (e.NewItems != null)
            {
                foreach (OM_SALE_QUOTE_DTL item in e.NewItems)
                {
                    item.OM_SALE_QUOTE_HDR = this;
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        if (!item.ChangeTracker.ChangeTrackingEnabled)
                        {
                            item.StartTracking();
                        }
                        ChangeTracker.RecordAdditionToCollectionProperties("OM_SALE_QUOTE_DTL", item);
                    }
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (OM_SALE_QUOTE_DTL item in e.OldItems)
                {
                    if (ReferenceEquals(item.OM_SALE_QUOTE_HDR, this))
                    {
                        item.OM_SALE_QUOTE_HDR = null;
                    }
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        ChangeTracker.RecordRemovalFromCollectionProperties("OM_SALE_QUOTE_DTL", item);
                    }
                }
            }
        }

        #endregion
    }
}
