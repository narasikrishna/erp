//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.Serialization;

namespace FIN.DAL
{
    [DataContract(IsReference = true)]
    public partial class HR_TRAINING_BUDGET_PLAN: IObjectWithChangeTracker, INotifyPropertyChanged
    {
        #region Primitive Properties
    
        [DataMember]
        public Nullable<int> PK_ID
        {
            get { return _pK_ID; }
            set
            {
                if (_pK_ID != value)
                {
                    _pK_ID = value;
                    OnPropertyChanged("PK_ID");
                }
            }
        }
        private Nullable<int> _pK_ID;
    
        [DataMember]
        public Nullable<int> CHILD_ID
        {
            get { return _cHILD_ID; }
            set
            {
                if (_cHILD_ID != value)
                {
                    _cHILD_ID = value;
                    OnPropertyChanged("CHILD_ID");
                }
            }
        }
        private Nullable<int> _cHILD_ID;
    
        [DataMember]
        public string TRN_BUDGET_PLAN_ID
        {
            get { return _tRN_BUDGET_PLAN_ID; }
            set
            {
                if (_tRN_BUDGET_PLAN_ID != value)
                {
                    if (ChangeTracker.ChangeTrackingEnabled && ChangeTracker.State != ObjectState.Added)
                    {
                        throw new InvalidOperationException("The property 'TRN_BUDGET_PLAN_ID' is part of the object's key and cannot be changed. Changes to key properties can only be made when the object is not being tracked or is in the Added state.");
                    }
                    _tRN_BUDGET_PLAN_ID = value;
                    OnPropertyChanged("TRN_BUDGET_PLAN_ID");
                }
            }
        }
        private string _tRN_BUDGET_PLAN_ID;
    
        [DataMember]
        public string TRN_COURSE_ID
        {
            get { return _tRN_COURSE_ID; }
            set
            {
                if (_tRN_COURSE_ID != value)
                {
                    _tRN_COURSE_ID = value;
                    OnPropertyChanged("TRN_COURSE_ID");
                }
            }
        }
        private string _tRN_COURSE_ID;
    
        [DataMember]
        public string TRN_ORGANISER
        {
            get { return _tRN_ORGANISER; }
            set
            {
                if (_tRN_ORGANISER != value)
                {
                    _tRN_ORGANISER = value;
                    OnPropertyChanged("TRN_ORGANISER");
                }
            }
        }
        private string _tRN_ORGANISER;
    
        [DataMember]
        public string TRN_QUARTER
        {
            get { return _tRN_QUARTER; }
            set
            {
                if (_tRN_QUARTER != value)
                {
                    _tRN_QUARTER = value;
                    OnPropertyChanged("TRN_QUARTER");
                }
            }
        }
        private string _tRN_QUARTER;
    
        [DataMember]
        public Nullable<decimal> TRN_ESTIMATION_CST
        {
            get { return _tRN_ESTIMATION_CST; }
            set
            {
                if (_tRN_ESTIMATION_CST != value)
                {
                    _tRN_ESTIMATION_CST = value;
                    OnPropertyChanged("TRN_ESTIMATION_CST");
                }
            }
        }
        private Nullable<decimal> _tRN_ESTIMATION_CST;
    
        [DataMember]
        public Nullable<decimal> TRN_PERIODS
        {
            get { return _tRN_PERIODS; }
            set
            {
                if (_tRN_PERIODS != value)
                {
                    _tRN_PERIODS = value;
                    OnPropertyChanged("TRN_PERIODS");
                }
            }
        }
        private Nullable<decimal> _tRN_PERIODS;
    
        [DataMember]
        public Nullable<decimal> TRN_NO_OF_PEOPLES
        {
            get { return _tRN_NO_OF_PEOPLES; }
            set
            {
                if (_tRN_NO_OF_PEOPLES != value)
                {
                    _tRN_NO_OF_PEOPLES = value;
                    OnPropertyChanged("TRN_NO_OF_PEOPLES");
                }
            }
        }
        private Nullable<decimal> _tRN_NO_OF_PEOPLES;
    
        [DataMember]
        public string TRN_EMPLOYEE_ID
        {
            get { return _tRN_EMPLOYEE_ID; }
            set
            {
                if (_tRN_EMPLOYEE_ID != value)
                {
                    _tRN_EMPLOYEE_ID = value;
                    OnPropertyChanged("TRN_EMPLOYEE_ID");
                }
            }
        }
        private string _tRN_EMPLOYEE_ID;
    
        [DataMember]
        public Nullable<decimal> TRN_COVERED_BY_KFAS
        {
            get { return _tRN_COVERED_BY_KFAS; }
            set
            {
                if (_tRN_COVERED_BY_KFAS != value)
                {
                    _tRN_COVERED_BY_KFAS = value;
                    OnPropertyChanged("TRN_COVERED_BY_KFAS");
                }
            }
        }
        private Nullable<decimal> _tRN_COVERED_BY_KFAS;
    
        [DataMember]
        public string TRN_TRAINING_TYPE
        {
            get { return _tRN_TRAINING_TYPE; }
            set
            {
                if (_tRN_TRAINING_TYPE != value)
                {
                    _tRN_TRAINING_TYPE = value;
                    OnPropertyChanged("TRN_TRAINING_TYPE");
                }
            }
        }
        private string _tRN_TRAINING_TYPE;
    
        [DataMember]
        public string ENABLED_FLAG
        {
            get { return _eNABLED_FLAG; }
            set
            {
                if (_eNABLED_FLAG != value)
                {
                    _eNABLED_FLAG = value;
                    OnPropertyChanged("ENABLED_FLAG");
                }
            }
        }
        private string _eNABLED_FLAG;
    
        [DataMember]
        public string WORKFLOW_COMPLETION_STATUS
        {
            get { return _wORKFLOW_COMPLETION_STATUS; }
            set
            {
                if (_wORKFLOW_COMPLETION_STATUS != value)
                {
                    _wORKFLOW_COMPLETION_STATUS = value;
                    OnPropertyChanged("WORKFLOW_COMPLETION_STATUS");
                }
            }
        }
        private string _wORKFLOW_COMPLETION_STATUS;
    
        [DataMember]
        public string CREATED_BY
        {
            get { return _cREATED_BY; }
            set
            {
                if (_cREATED_BY != value)
                {
                    _cREATED_BY = value;
                    OnPropertyChanged("CREATED_BY");
                }
            }
        }
        private string _cREATED_BY;
    
        [DataMember]
        public System.DateTime CREATED_DATE
        {
            get { return _cREATED_DATE; }
            set
            {
                if (_cREATED_DATE != value)
                {
                    _cREATED_DATE = value;
                    OnPropertyChanged("CREATED_DATE");
                }
            }
        }
        private System.DateTime _cREATED_DATE;
    
        [DataMember]
        public string MODIFIED_BY
        {
            get { return _mODIFIED_BY; }
            set
            {
                if (_mODIFIED_BY != value)
                {
                    _mODIFIED_BY = value;
                    OnPropertyChanged("MODIFIED_BY");
                }
            }
        }
        private string _mODIFIED_BY;
    
        [DataMember]
        public Nullable<System.DateTime> MODIFIED_DATE
        {
            get { return _mODIFIED_DATE; }
            set
            {
                if (_mODIFIED_DATE != value)
                {
                    _mODIFIED_DATE = value;
                    OnPropertyChanged("MODIFIED_DATE");
                }
            }
        }
        private Nullable<System.DateTime> _mODIFIED_DATE;
    
        [DataMember]
        public string ATTRIBUTE1
        {
            get { return _aTTRIBUTE1; }
            set
            {
                if (_aTTRIBUTE1 != value)
                {
                    _aTTRIBUTE1 = value;
                    OnPropertyChanged("ATTRIBUTE1");
                }
            }
        }
        private string _aTTRIBUTE1;
    
        [DataMember]
        public string ATTRIBUTE2
        {
            get { return _aTTRIBUTE2; }
            set
            {
                if (_aTTRIBUTE2 != value)
                {
                    _aTTRIBUTE2 = value;
                    OnPropertyChanged("ATTRIBUTE2");
                }
            }
        }
        private string _aTTRIBUTE2;
    
        [DataMember]
        public string ATTRIBUTE3
        {
            get { return _aTTRIBUTE3; }
            set
            {
                if (_aTTRIBUTE3 != value)
                {
                    _aTTRIBUTE3 = value;
                    OnPropertyChanged("ATTRIBUTE3");
                }
            }
        }
        private string _aTTRIBUTE3;
    
        [DataMember]
        public string ATTRIBUTE4
        {
            get { return _aTTRIBUTE4; }
            set
            {
                if (_aTTRIBUTE4 != value)
                {
                    _aTTRIBUTE4 = value;
                    OnPropertyChanged("ATTRIBUTE4");
                }
            }
        }
        private string _aTTRIBUTE4;
    
        [DataMember]
        public string ATTRIBUTE5
        {
            get { return _aTTRIBUTE5; }
            set
            {
                if (_aTTRIBUTE5 != value)
                {
                    _aTTRIBUTE5 = value;
                    OnPropertyChanged("ATTRIBUTE5");
                }
            }
        }
        private string _aTTRIBUTE5;
    
        [DataMember]
        public string ATTRIBUTE6
        {
            get { return _aTTRIBUTE6; }
            set
            {
                if (_aTTRIBUTE6 != value)
                {
                    _aTTRIBUTE6 = value;
                    OnPropertyChanged("ATTRIBUTE6");
                }
            }
        }
        private string _aTTRIBUTE6;
    
        [DataMember]
        public string ATTRIBUTE7
        {
            get { return _aTTRIBUTE7; }
            set
            {
                if (_aTTRIBUTE7 != value)
                {
                    _aTTRIBUTE7 = value;
                    OnPropertyChanged("ATTRIBUTE7");
                }
            }
        }
        private string _aTTRIBUTE7;
    
        [DataMember]
        public string ATTRIBUTE8
        {
            get { return _aTTRIBUTE8; }
            set
            {
                if (_aTTRIBUTE8 != value)
                {
                    _aTTRIBUTE8 = value;
                    OnPropertyChanged("ATTRIBUTE8");
                }
            }
        }
        private string _aTTRIBUTE8;
    
        [DataMember]
        public string ATTRIBUTE9
        {
            get { return _aTTRIBUTE9; }
            set
            {
                if (_aTTRIBUTE9 != value)
                {
                    _aTTRIBUTE9 = value;
                    OnPropertyChanged("ATTRIBUTE9");
                }
            }
        }
        private string _aTTRIBUTE9;
    
        [DataMember]
        public string ATTRIBUTE10
        {
            get { return _aTTRIBUTE10; }
            set
            {
                if (_aTTRIBUTE10 != value)
                {
                    _aTTRIBUTE10 = value;
                    OnPropertyChanged("ATTRIBUTE10");
                }
            }
        }
        private string _aTTRIBUTE10;
    
        [DataMember]
        public string TRN_ORG_ID
        {
            get { return _tRN_ORG_ID; }
            set
            {
                if (_tRN_ORG_ID != value)
                {
                    _tRN_ORG_ID = value;
                    OnPropertyChanged("TRN_ORG_ID");
                }
            }
        }
        private string _tRN_ORG_ID;
    
        [DataMember]
        public string TRN_BUDGET_PLAN_HDR_ID
        {
            get { return _tRN_BUDGET_PLAN_HDR_ID; }
            set
            {
                if (_tRN_BUDGET_PLAN_HDR_ID != value)
                {
                    _tRN_BUDGET_PLAN_HDR_ID = value;
                    OnPropertyChanged("TRN_BUDGET_PLAN_HDR_ID");
                }
            }
        }
        private string _tRN_BUDGET_PLAN_HDR_ID;

        #endregion
        #region ChangeTracking
    
        protected virtual void OnPropertyChanged(String propertyName)
        {
            if (ChangeTracker.State != ObjectState.Added && ChangeTracker.State != ObjectState.Deleted)
            {
                ChangeTracker.State = ObjectState.Modified;
            }
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        protected virtual void OnNavigationPropertyChanged(String propertyName)
        {
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        event PropertyChangedEventHandler INotifyPropertyChanged.PropertyChanged{ add { _propertyChanged += value; } remove { _propertyChanged -= value; } }
        private event PropertyChangedEventHandler _propertyChanged;
        private ObjectChangeTracker _changeTracker;
    
        [DataMember]
        public ObjectChangeTracker ChangeTracker
        {
            get
            {
                if (_changeTracker == null)
                {
                    _changeTracker = new ObjectChangeTracker();
                    _changeTracker.ObjectStateChanging += HandleObjectStateChanging;
                }
                return _changeTracker;
            }
            set
            {
                if(_changeTracker != null)
                {
                    _changeTracker.ObjectStateChanging -= HandleObjectStateChanging;
                }
                _changeTracker = value;
                if(_changeTracker != null)
                {
                    _changeTracker.ObjectStateChanging += HandleObjectStateChanging;
                }
            }
        }
    
        private void HandleObjectStateChanging(object sender, ObjectStateChangingEventArgs e)
        {
            if (e.NewState == ObjectState.Deleted)
            {
                ClearNavigationProperties();
            }
        }
    
        protected bool IsDeserializing { get; private set; }
    
        [OnDeserializing]
        public void OnDeserializingMethod(StreamingContext context)
        {
            IsDeserializing = true;
        }
    
        [OnDeserialized]
        public void OnDeserializedMethod(StreamingContext context)
        {
            IsDeserializing = false;
            ChangeTracker.ChangeTrackingEnabled = true;
        }
    
        protected virtual void ClearNavigationProperties()
        {
        }

        #endregion
    }
}
