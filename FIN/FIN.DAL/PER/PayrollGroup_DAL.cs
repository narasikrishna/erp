﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.PER
{
    public class PayrollGroup_DAL
    {
        static string sqlQuery = "";

        public static string GetPayPerioddtls(String PAY_GROUP_ID)
        {
            sqlQuery = string.Empty;


            sqlQuery = " select PGD.PAY_GROUP_ID,PGD.PAY_GROUP_CODE,PGD.PAY_GROUP_CODE_OL,PGD.PAY_GROUP_DESC_OL,PGD.PAY_GROUP_DESC,PGD.PAY_PAYROLL_TYPE,PGD.EFFECTIVE_FROM_DT,PGD.EFFECTIVE_TO_DT,PGD.ROUND_OFF_DECIMALS,'N' AS DELETED,'KWD' as CURRENCY_CODE,'KUWAIT DINAR' AS CURRENCY,";
            sqlQuery += " CASE PGD.ENABLED_FLAG WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS ENABLED_FLAG ";
            sqlQuery += " from PAY_GROUP_DTLS PGD";
            //sqlQuery += " where PGD.ENABLED_FLAG = 1";
            sqlQuery += " WHERE  PGD.WORKFLOW_COMPLETION_STATUS = 1 ";
            //sqlQuery += "   ";
            sqlQuery += " and PGD.PAY_GROUP_ID = '" + PAY_GROUP_ID + "'";
            sqlQuery += " order by PAY_GROUP_ID ";

            return sqlQuery;

        }

        public static string GetCurrency()
        {
            sqlQuery = string.Empty;


            sqlQuery = "  select SC.CURRENCY_ID AS CURRENCY_CODE,SC.CURRENCY_CODE||' - '||SC.CURRENCY_DESC as CURRENCY";
            sqlQuery += " from SSM_CURRENCIES SC";
            sqlQuery += " where SC.ENABLED_FLAG = 1";
            sqlQuery += "  and SC.WORKFLOW_COMPLETION_STATUS = 1 ";

            return sqlQuery;

        }

        public static string GetPayGroup()
        {
            sqlQuery = string.Empty;


            sqlQuery = " SELECT G.PAY_GROUP_ID,G.PAY_GROUP_DESC" + VMVServices.Web.Utils.LanguageCode + " AS PAY_GROUP_DESC ";
            sqlQuery += " FROM PAY_GROUP_DTLS G";
            sqlQuery += " WHERE G.ENABLED_FLAG = 1";
            sqlQuery += " AND G.WORKFLOW_COMPLETION_STATUS = 1";

            return sqlQuery;

        }



    }
}
