﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.PER
{
    public class PayrollElements_DAL
    {
        static string sqlQuery = "";

        public static string getPayElementDetailsReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM vw_pay_element_dtls V WHERE ROWNUM > 0 ";

            return sqlQuery;
        }

        public static string getPayElements_Recuringonly()
        {
            sqlQuery = string.Empty;

            sqlQuery = " select  PE.PAY_ELEMENT_ID,PE.PAY_ELEMENT_CODE||' - '||PE.PAY_ELEMENT_DESC as PAY_ELEMENT ";
            sqlQuery += " from PAY_ELEMENTS PE ";
            sqlQuery += " where PE.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " and PE.ENABLED_FLAG = 1 ";
            sqlQuery += " and  PE.PAY_ELE_RECURRING = '1'";
            sqlQuery += " and pe.PAY_ORG_ID = '" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " ORDER BY PE.PAY_ELEMENT_CODE ";

            return sqlQuery;

        }
        

        public static string GetPayElementdtls(String PAY_ELEMENT_ID)
        {
            sqlQuery = string.Empty;


            sqlQuery = " SELECT PE.PAY_ELEMENT_ID,PE.PAY_ELEMENT_CODE,PE.PAY_ELEMENT_CODE_OL,PE.PAY_ELEMENT_DESC,PE.PAY_ELEMENT_DESC_OL,PE.PAY_ELE_ALIAS,";
            sqlQuery += " C.CODE AS VALUE_KEY_ID,C.CODE AS VALUE_NAME,PE.EFFECTIVE_FROM_DT,PE.EFFECTIVE_TO_DT,'N' AS DELETED,";
           // sqlQuery += " AC.ACCT_CODE_ID AS REC_CODE_ID,AC.ACCT_CODE||' - '||AC.ACCT_CODE_DESC AS REC_CODE_NAME,";
            sqlQuery += " (select AC.ACCT_CODE_ID  from GL_ACCT_CODES AC where PE.GL_ID = AC.ACCT_CODE_ID) AS CODE_ID,";
            sqlQuery += " (select AC.ACCT_CODE||' - '||AC.ACCT_CODE_DESC from GL_ACCT_CODES AC where PE.GL_ID = AC.ACCT_CODE_ID) AS CODE_NAME,";
            sqlQuery += " (select rAC.ACCT_CODE_ID  from GL_ACCT_CODES rac where PE.RECEIVABLE_ID = rac.ACCT_CODE_ID) AS REC_CODE_ID,";
            sqlQuery += " (select rAC.ACCT_CODE || ' - ' || rAC.ACCT_CODE_DESC  from GL_ACCT_CODES rac where PE.RECEIVABLE_ID = rac.ACCT_CODE_ID) AS REC_CODE_NAME,";
            sqlQuery += " CASE PE.PAY_ELE_RECURRING WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS PAY_ELE_RECURRING,";
            sqlQuery += " CASE PE.PAY_ELE_TAXABLE WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS PAY_ELE_TAXABLE,";
            sqlQuery += " CASE PE.APPERARS_IN_PAYSLIP WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS APPERARS_IN_PAYSLIP,";
            sqlQuery += " CASE PE.UNPAID_LEAVE WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS UNPAID_LEAVE,";
            sqlQuery += " CASE PE.ENABLED_FLAG WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS ENABLED_FLAG,";
            sqlQuery += " CASE PE.PAY_ELEMENT_PRORATE WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS PAY_ELEMENT_PRORATE,";
            sqlQuery += " CASE PE.PAID_FOR_ANNUAL_LEAVE WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS PAID_FOR_ANNUAL_LEAVE";
            sqlQuery += " FROM PAY_ELEMENTS PE,SSM_CODE_MASTERS C";
            sqlQuery += " WHERE PE.WORKFLOW_COMPLETION_STATUS = 1";
         //   sqlQuery += " AND PE.GL_ID = AC.ACCT_CODE_ID";
           // sqlQuery += " AND PE.RECEIVABLE_ID = rac.ACCT_CODE_ID";
            sqlQuery += " AND PE.PAY_ELE_CLASS = C.CODE";
            sqlQuery += " AND PE.PAY_ELEMENT_ID = '" + PAY_ELEMENT_ID + "'";
            sqlQuery += " ORDER BY PAY_ELEMENT_ID ";
            return sqlQuery;
        }


        public static string GetElementCode(string pay_element_code, String PAY_ELEMENT_ID, string EFFECTIVE_FROM_DT, string EFFECTIVE_TO_DT)
        {
            sqlQuery = string.Empty;



            sqlQuery = " SELECT TRIM(PE.PAY_ELEMENT_CODE)";
            sqlQuery += " FROM PAY_ELEMENTS PE";
            sqlQuery += " WHERE TRIM(PE.EFFECTIVE_FROM_DT) = to_date('" + EFFECTIVE_FROM_DT + "','dd/MM/yyyy')";
            if (EFFECTIVE_TO_DT != string.Empty)
            {
                sqlQuery += " AND TRIM(NVL(PE.EFFECTIVE_TO_DT,SYSDATE)) =  to_date('" + EFFECTIVE_TO_DT + "','dd/MM/yyyy')";
            }
            sqlQuery += " AND TRIM(pe.pay_element_code) = '" + pay_element_code + "'";
            if (PAY_ELEMENT_ID.ToString() != null)
            {

                sqlQuery += " AND PE.PAY_ELEMENT_ID <> '" + PAY_ELEMENT_ID + "'";
            }

            return sqlQuery;
        }

        public static string getPayElements()
        {
            sqlQuery = string.Empty;

            sqlQuery = " select  PE.PAY_ELEMENT_ID,PE.PAY_ELEMENT_CODE||' - '||PE.PAY_ELEMENT_DESC as PAY_ELEMENT ";
            sqlQuery += " from PAY_ELEMENTS PE ";
            sqlQuery += " where PE.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " and PE.ENABLED_FLAG = 1 ";            
            sqlQuery += " and pe.PAY_ORG_ID = '" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " ORDER BY PE.PAY_ELEMENT_CODE ";

            return sqlQuery;

        }

        public static string getPayElements4SalLeave()
        {
            sqlQuery = string.Empty;

            sqlQuery = " select  PE.PAY_ELEMENT_ID as LOOKUP_ID,PE.PAY_ELEMENT_CODE||' - '||PE.PAY_ELEMENT_DESC as LOOKUP_NAME ";
            sqlQuery += " from PAY_ELEMENTS PE ";
            sqlQuery += " where PE.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " and PE.ENABLED_FLAG = 1 ";
            sqlQuery += " and pe.PAY_ORG_ID = '" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " ORDER BY PE.PAY_ELEMENT_CODE ";

            return sqlQuery;

        }

        public static string getPayElements_deductiononly()
        {
            sqlQuery = string.Empty;

            sqlQuery = " select  PE.PAY_ELEMENT_ID,PE.PAY_ELEMENT_CODE||' - '||PE.PAY_ELEMENT_DESC as PAY_ELEMENT ";
            sqlQuery += " from PAY_ELEMENTS PE ";
            sqlQuery += " where PE.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " and PE.ENABLED_FLAG = 1 ";
            sqlQuery += " and  PE.PAY_ELE_CLASS = 'Deduction'";
            sqlQuery += " and pe.PAY_ORG_ID = '" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " ORDER BY PE.PAY_ELEMENT_CODE ";

            return sqlQuery;

        }
        public static string getPayElements_NonRecurring_Deductiononly()
        {
            sqlQuery = string.Empty;

            sqlQuery = " select  PE.PAY_ELEMENT_ID,trim(PE.PAY_ELEMENT_CODE) || ' - ' || trim(PE.PAY_ELEMENT_DESC) as PAY_ELEMENT ";
            sqlQuery += " from PAY_ELEMENTS PE ";
            sqlQuery += " where PE.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " and PE.ENABLED_FLAG = 1 ";
            sqlQuery += " and  PE.PAY_ELE_CLASS = 'Deduction'";
            sqlQuery += " and  PE.PAY_ELE_RECURRING = '0'";
            sqlQuery += " and pe.PAY_ORG_ID = '" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " ORDER BY PE.PAY_ELEMENT_CODE ";

            return sqlQuery;

        }
        public static string getPayElements_Earningonly()
        {
            sqlQuery = string.Empty;

            sqlQuery = " select  PE.PAY_ELEMENT_ID,PE.PAY_ELEMENT_CODE||' - '||PE.PAY_ELEMENT_DESC as PAY_ELEMENT ";
            sqlQuery += " from PAY_ELEMENTS PE ";
            sqlQuery += " where PE.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " and PE.ENABLED_FLAG = 1 ";
            sqlQuery += " and  PE.PAY_ELE_CLASS = 'Earning'";
            sqlQuery += " and pe.PAY_ORG_ID = '" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " ORDER BY PE.PAY_ELEMENT_CODE ";

            return sqlQuery;

        }
        public static string getPayElements_NONRecuringonly()
        {
            sqlQuery = string.Empty;

            sqlQuery = " select  PE.PAY_ELEMENT_ID,PE.PAY_ELEMENT_CODE||' - '||PE.PAY_ELEMENT_DESC as PAY_ELEMENT ";
            sqlQuery += " from PAY_ELEMENTS PE ";
            sqlQuery += " where PE.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " and PE.ENABLED_FLAG = 1 ";
            sqlQuery += " and  PE.PAY_ELE_RECURRING = '0'";
            sqlQuery += " and pe.PAY_ORG_ID = '" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " ORDER BY PE.PAY_ELEMENT_CODE ";

            return sqlQuery;

        }
        


    }
}