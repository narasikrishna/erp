﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL
{
    public class PayrollPayDeductionParty_DAL
    {
        static string sqlQuery = "";
       
        public static string getDedPartyNumber()
        {
            sqlQuery = string.Empty;

            sqlQuery = " select PDPE.DED_PARTY_CODE, PDPE.DED_PARTY_CODE ";
            sqlQuery += " from PAY_DED_PARTY_ENROLLMENT PDPE ";
            sqlQuery += " where PDPE.ENABLED_FLAG = 1  ";
            sqlQuery += " and PDPE.WORKFLOW_COMPLETION_STATUS=1 ";
            sqlQuery += " ORDER BY PDPE.DED_PARTY_CODE ";

            return sqlQuery;

        }

        public static string getBankName(string DED_PARTY_CODE)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select PBD.BANK_CODE,PBD.BANK_NAME" + VMVServices.Web.Utils.LanguageCode + " AS BANK_NAME ";
            sqlQuery += " from PAY_DED_PARTY_BANK_DTLS PBD ";
            sqlQuery += " where PBD.ENABLED_FLAG = 1 ";
            sqlQuery += "  and PBD.WORKFLOW_COMPLETION_STATUS=1 ";
            sqlQuery += " and PBD.DED_PARTY_CODE='"+ DED_PARTY_CODE + "'" ;

            return sqlQuery;

        }

        public static string getBranchName(string DED_PARTY_CODE,string BANK_CODE)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select PBD.BRANCH_CODE,PBD.BRANCH_NAME" + VMVServices.Web.Utils.LanguageCode + " AS BRANCH_NAME  ";
            sqlQuery += " from PAY_DED_PARTY_BANK_DTLS PBD ";
            sqlQuery += " where PBD.ENABLED_FLAG = 1 ";
            sqlQuery += "  and PBD.WORKFLOW_COMPLETION_STATUS=1 ";
            sqlQuery += " and PBD.DED_PARTY_CODE ='" + DED_PARTY_CODE + "'";
            sqlQuery += " and PBD.BANK_CODE ='" + BANK_CODE + "'";

            return sqlQuery;

        }

        public static string getAccountNumber(string DED_PARTY_CODE, string BANK_CODE, string BRANCH_CODE)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select PBD.ACCOUNT_NUMBER,PBD.ACCOUNT_NUMBER ";
            sqlQuery += " from PAY_DED_PARTY_BANK_DTLS PBD ";
            sqlQuery += " where PBD.ENABLED_FLAG = 1 ";
            sqlQuery += "  and PBD.WORKFLOW_COMPLETION_STATUS=1 ";
            sqlQuery += " and PBD.DED_PARTY_CODE ='" + DED_PARTY_CODE + "'";
            sqlQuery += " and PBD.BANK_CODE ='" + BANK_CODE + "'";
            sqlQuery += " and PBD.BRANCH_CODE = '" + BRANCH_CODE + "'";

            return sqlQuery;

        }

        public static string getChequeNumber(string DED_PARTY_CODE)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select CD.CHECK_NUMBER,CD.CHECK_DTL_ID ";
            sqlQuery += " from CA_CHECK_HDR CH,CA_CHECK_DTL CD,PAY_DED_PARTY_BANK_DTLS DPBD ";
            sqlQuery += " where CH.CHECK_HDR_ID = CD.CHECK_HDR_ID ";
            sqlQuery += " and CH.CHECK_BANK_ID = DPBD.BANK_CODE ";
            sqlQuery += " and CH.ENABLED_FLAG = 1 ";
            sqlQuery += "  and CH.WORKFLOW_COMPLETION_STATUS=1 ";
            sqlQuery += " and DPBD.DED_PARTY_CODE = '" + DED_PARTY_CODE + "'";

            return sqlQuery;

        }

    }
}
