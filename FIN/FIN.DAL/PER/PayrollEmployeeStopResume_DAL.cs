﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.PER
{
    public class PayrollEmployeeStopResume_DAL
    {
        static string sqlQuery = "";

        public static string GetEmployeeDetails()
        {
            sqlQuery = string.Empty;

            sqlQuery = " select t.emp_id,t.emp_id ";
            sqlQuery += " from hr_employees t";
            sqlQuery += " where t.workflow_completion_status=1";
            sqlQuery += " and t.enabled_flag=1";
            sqlQuery += " order by t.emp_id asc";

            return sqlQuery;
        }
        public static string getPayElements(string str_EmpId)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select  PE.PAY_ELEMENT_ID,PE.PAY_ELEMENT_CODE" + VMVServices.Web.Utils.LanguageCode + " AS PAY_ELEMENT ";
            sqlQuery += " from PAY_ELEMENTS PE ";
            sqlQuery += " INNER JOIN PAY_EMP_ELEMENT_VALUE PEEV ON PEEV.PAY_ELEMENT_ID=PE.PAY_ELEMENT_ID AND PEEV.EFFECTIVE_TO_DT IS NULL ";
            sqlQuery += " where PE.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " and PE.ENABLED_FLAG = 1 ";
            sqlQuery += " and PE.PAY_ORG_ID = '" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " AND PEEV.PAY_EMP_ID='" + str_EmpId + "'";
            sqlQuery += " ORDER BY PE.PAY_ELEMENT_CODE ";

            return sqlQuery;

        }

        public static string getstopresume(string str_EmpId)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select  STOP_PAYTOLL ";
            sqlQuery += " from HR_EMPLOYEES HE ";
            sqlQuery += " where HE.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " and HE.ENABLED_FLAG = 1 ";
            //sqlQuery += " and HE.PAY_ORG_ID = '" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " AND HE.EMP_ID='" + str_EmpId + "'";

            return sqlQuery;

        }

        public static string getEmployeeName(string emp_id)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select HE.EMP_FIRST_NAME" + VMVServices.Web.Utils.LanguageCode + "  AS EMP_FIRST_NAME from HR_EMPLOYEES HE ";
            sqlQuery += " where HE.EMP_ID = '" + emp_id + "'";
            sqlQuery += " and HE.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " and HE.ENABLED_FLAG = 1 ";

            return sqlQuery;

        }

        public static string getElements(string element_id)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select  PE.PAY_ELEMENT_DESC as PAY_ELEMENT from PAY_ELEMENTS PE ";
            sqlQuery += " where PE.PAY_ELEMENT_ID = '" + element_id + "'";
            sqlQuery += " and PE.Enabled_Flag = 1";
            sqlQuery += " and PE.WORKFLOW_COMPLETION_STATUS = 1 ";

            return sqlQuery;

        }
    }
}
