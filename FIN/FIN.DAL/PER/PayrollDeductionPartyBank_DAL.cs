﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.PER
{
    public class PayrollDeductionPartyBank_DAL
    {
        static string sqlQuery = "";

        public static string GetPayrolldeductionpartybankdtls(string Master_id)
        {
            sqlQuery = string.Empty;



            sqlQuery = " SELECT PBD.DED_PARTY_DTL_ID,PBD.ACCOUNT_NUMBER,PBD.EFFECTIVE_FROM_DT,PBD.EFFECTIVE_TO_DT,B.BANK_ID,B.BANK_NAME,BB.BANK_BRANCH_ID,BB.BANK_BRANCH_NAME,'N' AS DELETED,";
            sqlQuery += " CASE PBD.ENABLED_FLAG WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS ENABLED_FLAG";
            sqlQuery += " FROM PAY_DED_PARTY_BANK_DTLS PBD,CA_BANK B,CA_BANK_BRANCH BB";
            sqlQuery += " WHERE PBD.BANK_CODE = B.BANK_ID";
            sqlQuery += " AND PBD.BRANCH_CODE = BB.BANK_BRANCH_ID";
            sqlQuery += " AND PBD.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND PBD.Ded_Party_Code = '" + Master_id + "'";
            sqlQuery += " order by DED_PARTY_DTL_ID ";

            return sqlQuery;

        }


        public static string getDeductionPartyName()
        {
            sqlQuery = string.Empty;
            
            sqlQuery = " SELECT  E.DED_PARTY_CODE,E.DED_PARTY_NAME";
            sqlQuery += " FROM PAY_DED_PARTY_ENROLLMENT E";
            sqlQuery += " WHERE E.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND E.DED_ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " AND E.ENABLED_FLAG = 1";
            sqlQuery += " order by e.ded_party_code";
            return sqlQuery;

        }


        public static string GetDedpartyName(String DED_PARTY_CODE)
        {
            sqlQuery = string.Empty;


            sqlQuery = " SELECT  E.DED_PARTY_NAME";
            sqlQuery += " FROM PAY_DED_PARTY_ENROLLMENT E";
            sqlQuery += " WHERE E.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND E.ENABLED_FLAG = 1";
            sqlQuery += " AND E.DED_PARTY_CODE = '" + DED_PARTY_CODE + "'";


            return sqlQuery;

        }


    }
}
