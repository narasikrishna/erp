﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.PER
{
    public class PayrollDeductionDetails_DAL
    {
        static string sqlQuery = "";

        public static string GetPayPerioddtls(String DED_PARTY_CODE, string PAYROLL_ID)
        {
            sqlQuery = string.Empty;


            sqlQuery = " select  0 as DED_ID,PFDD.PAY_EMP_ELEMENT_ID,HE.EMP_ID,HE.EMP_FIRST_NAME" + VMVServices.Web.Utils.LanguageCode + " AS EMP_FIRST_NAME,PE.PAY_ELEMENT_ID,PE.PAY_ELEMENT_CODE,PE.PAY_ELEMENT_DESC,to_char(PFDD.PAY_AMOUNT) as PAY_AMOUNT,PDPE.ENABLED_FLAG,PP.PAY_FROM_DT,PP.PAY_TO_DT,'N' as DELETED";
            sqlQuery += "  from PAY_DED_PARTY_ENROLLMENT PDPE, ";
            sqlQuery += " PAY_FINAL_RUN_DTL_DTL    PFDD,    ";
            sqlQuery += " HR_EMPLOYEES             HE, ";
            sqlQuery += " PAY_ELEMENTS             PE, ";
            sqlQuery += " PAY_FINAL_RUN_DTL        PFD, ";
            sqlQuery += " PAY_FINAL_RUN_HDR        PFH, ";
            sqlQuery += " PAY_PERIODS              PP ";
            sqlQuery += "  where PDPE.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " and PDPE.ENABLED_FLAG = 1 ";
            sqlQuery += "  and PDPE.PAY_ELEMENT_ID= PFDD.PAY_EMP_ELEMENT_ID  ";
            sqlQuery += "  and HE.EMP_ID= PFDD.PAY_EMP_ID ";
            sqlQuery += " AND PE.PAY_ELEMENT_ID= PFDD.PAY_EMP_ELEMENT_ID ";
            sqlQuery += " AND PFD.PAYROLL_DTL_ID = PFDD.PAYROLL_DTL_ID ";
            sqlQuery += " AND PFH.PAYROLL_ID= PFD.PAYROLL_ID ";
            sqlQuery += "  AND PFH.PAYROLL_PERIOD= PP.PAY_PERIOD_ID ";
            sqlQuery += " and PDPE.DED_PARTY_CODE = '" + DED_PARTY_CODE + "'";
            sqlQuery += " and PFH.PAYROLL_ID = '" + PAYROLL_ID + "'";

            return sqlQuery;

        }


        public static string GetPayRollDetails()
        {
            sqlQuery = string.Empty;


            sqlQuery = " select PFR.PAYROLL_ID,PP.PAY_PERIOD_ID";
            sqlQuery += " from PAY_FINAL_RUN_HDR PFR,PAY_PERIODS PP";
            sqlQuery += " where PFR.PAYROLL_PERIOD = PP.PAY_PERIOD_ID";
            sqlQuery += "  and PFR.ENABLED_FLAG = 1 ";
            sqlQuery += " and PFR.WORKFLOW_COMPLETION_STATUS = 1 ";
            return sqlQuery;

        }


        public static string GetDedPartyDetails()
        {
            sqlQuery = string.Empty;


            sqlQuery = " select DPE.DED_PARTY_CODE,DPE.DED_PARTY_CODE ";
            sqlQuery += " from PAY_DED_PARTY_ENROLLMENT DPE ";
            sqlQuery += "  where DPE.ENABLED_FLAG = 1 ";
            sqlQuery += " and DPE.WORKFLOW_COMPLETION_STATUS = 1 ";
            return sqlQuery;

        }

        public static string GetPayRollDetails_Name(string PAYROLL_NUM)
        {
            sqlQuery = string.Empty;


            sqlQuery = " select PP.PAY_PERIOD_DESC";
            sqlQuery += " from PAY_FINAL_RUN_HDR PFR,PAY_PERIODS PP";
            sqlQuery += " where PFR.PAYROLL_PERIOD = PP.PAY_PERIOD_ID";
            sqlQuery += "  and PFR.ENABLED_FLAG = 1 ";
            sqlQuery += " and PFR.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " and PFR.PAYROLL_ID = '" + PAYROLL_NUM + "'";
            return sqlQuery;

        }

        public static string GetDedPartyDetails_Name(string DED_PARTY_NUM)
        {
            sqlQuery = string.Empty;


            sqlQuery = " select DPE.DED_PARTY_NAME ";
            sqlQuery += " from PAY_DED_PARTY_ENROLLMENT DPE ";
            sqlQuery += "  where DPE.ENABLED_FLAG = 1 ";
            sqlQuery += " and DPE.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " and DPE.DED_PARTY_CODE = '" + DED_PARTY_NUM + "'";
            return sqlQuery;

        }

    }
}
