﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL
{
    public class PayrollDeductionParty_DAL
    {
        static string sqlQuery = "";

        public static string getPayElements()
        {
            sqlQuery = string.Empty;

            sqlQuery = " select  PE.PAY_ELEMENT_ID,PE.PAY_ELEMENT_CODE||' - '||PE.PAY_ELEMENT_DESC as PAY_ELEMENT ";
            sqlQuery += " from PAY_ELEMENTS PE ";
            sqlQuery += " where PE.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " and PE.ENABLED_FLAG = 1 ";
            sqlQuery += " and PE.PAY_ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " ORDER BY PE.PAY_ELEMENT_CODE ";

            return sqlQuery;

        }

        public static string getPayElements_FR_DEDUCTION()
        {
            sqlQuery = string.Empty;

            sqlQuery = " select  PE.PAY_ELEMENT_ID,PE.PAY_ELEMENT_CODE||' - '||PE.PAY_ELEMENT_DESC" + VMVServices.Web.Utils.LanguageCode + " as PAY_ELEMENT ";
            sqlQuery += " from PAY_ELEMENTS PE ";
            sqlQuery += " where PE.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " and PE.ENABLED_FLAG = 1 ";
            sqlQuery += " and UPPER(PE.PAY_ELE_CLASS) = 'DEDUCTION'";
            sqlQuery += " and PE.PAY_ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " ORDER BY PE.PAY_ELEMENT_CODE ";

            return sqlQuery;

        }




    }
}
