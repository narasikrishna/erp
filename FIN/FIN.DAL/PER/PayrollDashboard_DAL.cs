﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.PER
{
    public class PayrollDashboard_DAL
    {
        public static string getSal4FinYear(string deptID, string calID, string str_emp_id="")
        {
            string sqlQuery = string.Empty;

            sqlQuery += "  select round(sum(pfre.net_due)," + VMVServices.Web.Utils.DecimalPrecision + ") as net_sal,";
            sqlQuery += " pp.pay_period_id, pp.pay_period_desc";
            sqlQuery += " from pay_final_run_emp_dtl pfre, pay_periods pp, gl_acct_calendar_dtl gacd";
            sqlQuery += " where pfre.payroll_period(+) = pp.pay_period_id";
            sqlQuery += " and pp.pay_from_dt between gacd.cal_eff_start_dt and gacd.call_eff_end_dt";

            if (deptID.ToString().Length > 0)
            {
                sqlQuery += " and pfre.emp_department_id='" + deptID + "'";
            }

            if (calID.ToString().Length > 0)
            {
                sqlQuery += " and gacd.cal_dtl_id='" + calID + "'";
            }
            if (str_emp_id.Trim().Length > 0)
            {
                sqlQuery += " and pfre.emp_id='" + str_emp_id + "'";
            }

            sqlQuery += " group by pp.pay_period_id, pp.pay_period_desc";
           
            return sqlQuery;

        }

        public static string getEmpMobileExp(string deptID, string empID, string calID, string payID)
        {
            

            //sqlQuery += "  select round(sum(mled.value_amount)," + VMVServices.Web.Utils.DecimalPrecision + ") as used_amt,";
            //sqlQuery += " pp.pay_period_id, pp.pay_period_desc";
            //sqlQuery += " from HR_EMP_MOB_LIMIT_ENTRY_DTL mled, HR_EMP_MOB_LIMIT_ENTRY_HDR mle, pay_periods pp, gl_acct_calendar_dtl gacd";
            //sqlQuery += " where mle.mob_limit_entry_id = mled.mob_limit_entry_id";
            //sqlQuery += " and pp.pay_period_id = mle.period_id";
            //sqlQuery += " and pp.pay_from_dt between gacd.cal_eff_start_dt and gacd.call_eff_end_dt";

            //if (deptID.ToString().Length > 0)
            //{
            //    sqlQuery += " and mle.dept_id='" + deptID + "'";
            //}

            //if (empID.ToString().Length > 0)
            //{
            //    sqlQuery += " and mle.emp_id='" + empID + "'";
            //}

            //if (calID.ToString().Length > 0)
            //{
            //    sqlQuery += " and gacd.cal_dtl_id='" + calID + "'";
            //}

            //sqlQuery += " group by mled.value_amount, pp.pay_period_id, pp.pay_period_desc";


            string sqlQuery = string.Empty;

            sqlQuery += "  select round(sum(frdd.pay_Amount)," + VMVServices.Web.Utils.DecimalPrecision + ") as used_amt,";
            sqlQuery += " pp.pay_period_id, pp.pay_period_desc";
            sqlQuery += " from pay_final_run_emp_dtl pfre, pay_periods pp, gl_acct_calendar_dtl gacd";
            sqlQuery += " ,pay_final_run_dtl_dtl frdd,pay_final_run_dtl frd,pay_final_run_hdr frh, pay_elements pe";

            sqlQuery += " where pfre.payroll_period = pp.pay_period_id";
            sqlQuery += " and pp.pay_from_dt between gacd.cal_eff_start_dt and gacd.call_eff_end_dt";
            sqlQuery += " and frdd.payroll_dtl_id = frd.payroll_dtl_id and frh.payroll_id= frd.payroll_id and frh.payroll_period= pp.pay_period_id and frdd.pay_emp_id= pfre.emp_id ";
            //sqlQuery += " and sso.hr_mobi_invoice_element_code = frdd.pay_emp_element_id ";
            sqlQuery += " and pe.pay_element_id = frdd.pay_emp_element_id";
            sqlQuery += " and pe.pay_ele_class = 'Deduction'";

            if (deptID.ToString().Length > 0)
            {
                sqlQuery += " and pfre.emp_department_id='" + deptID + "'";
            }
            if (calID.ToString().Length > 0)
            {
                sqlQuery += " and gacd.cal_dtl_id='" + calID + "'";
            }
            if (empID.Trim().Length > 0)
            {
                sqlQuery += " and pfre.emp_id='" + empID + "'";
            }
            if (payID.ToString().Length > 0)
            {
                sqlQuery += " and pe.pay_element_id='" + payID + "'";
            }

            sqlQuery += " group by pp.pay_period_id, pp.pay_period_desc,PP.PAY_FROM_DT";
            sqlQuery += " Order by PP.PAY_FROM_DT ";
           
            return sqlQuery;

        }

        public static string getAnnualLeavePaid(string str_Year,string str_period)
        {
            string sqlQuery = string.Empty;

            sqlQuery += " select DEPT.DEPT_NAME,SUM(LC_NO_OF_DAYS) AS TOT_DAYS, round(sum(le.lc_leave_salary)," + VMVServices.Web.Utils.DecimalPrecision + ") as tot_sal";
            sqlQuery += " from HR_LEAVE_ENCASHMENT LE , gl_acct_period_dtl APD,HR_DEPARTMENTS DEPT";
            sqlQuery += " WHERE LE.LC_DATE BETWEEN APD.PERIOD_FROM_DT AND APD.PERIOD_TO_DT ";
            sqlQuery += " AND LE.LC_DEPT_ID= DEPT.DEPT_ID AND APD.PERIOD_ID='" + str_period + "'";
            sqlQuery += " GROUP BY  DEPT.DEPT_NAME";
                       
            return sqlQuery;

        }


        public static string getIndemLeavePaid()
        {
            string sqlQuery = string.Empty;

            sqlQuery += " select DEPT.DEPT_NAME, round(sum(IE.Indm_Amount)," + VMVServices.Web.Utils.DecimalPrecision + ") as TOT_AMT";
            sqlQuery += " from hr_indemnity_encashment IE, GL_ACCT_CALENDAR_DTL ACD, HR_DEPARTMENTS DEPT, hr_emp_work_dtls ewd";
            sqlQuery += " WHERE IE.IND_EC_DATE BETWEEN ACD.CAL_EFF_START_DT AND ACD.CALL_EFF_END_DT";
            sqlQuery += " AND IE.IND_EC_EMP_ID = ewd.emp_id ";
            sqlQuery += " and dept.dept_id = ewd.emp_dept_id";
            sqlQuery += " and ewd.effective_to_dt is null";
            sqlQuery += " AND ACD.CAL_DTL_ID = PKG_PAYROLL.GET_FISCAL_YEAR('" + VMVServices.Web.Utils.OrganizationID + "',SYSDATE)";
            sqlQuery += " GROUP BY DEPT.DEPT_NAME";

            return sqlQuery;

        }


        public static string GetEmployeeDetails4Dashbord(string empID)
        {
            string sqlQuery = string.Empty;
            string strLngCode = VMVServices.Web.Utils.LanguageCode;

            sqlQuery += " SELECT DEPT.DEPT_ID, DEPT.DEPT_NAME" + strLngCode + " as DEPT_NAME ,DD.DEPT_DESIG_ID,DD.DESIG_NAME" + strLngCode + " as DESIG_NAME, EMP.EMP_ID,EMP.EMP_NO ";
            sqlQuery += " ,(EMP.emp_first_name" + strLngCode + "||' '||EMP.emp_middle_name" + strLngCode + "||' '||EMP.emp_last_name" + strLngCode + ") EMP_NAME ";
            sqlQuery += " ,emp.emp_doj as DOJ, confirmation_date as DOC,";
            sqlQuery += " (select sum(la.no_of_days) as tot_days from hr_leave_applications la, HR_LEAVE_DEFINITIONS ld where la.leave_id = ld.leave_id";
            sqlQuery += " and la.app_status='Approved' and la.staff_id =EMP.EMP_ID) as tot_leave_taken ";
            sqlQuery += " FROM HR_EMP_WORK_DTLS EWD ";
            sqlQuery += " INNER JOIN HR_EMPLOYEES EMP ON EMP.EMP_ID= EWD.EMP_ID ";
            sqlQuery += " INNER JOIN HR_DEPARTMENTS DEPT ON EWD.EMP_DEPT_ID = DEPT.DEPT_ID ";
            sqlQuery += " INNER JOIN HR_DEPT_DESIGNATIONS DD ON DD.DEPT_DESIG_ID = EWD.EMP_DESIG_ID ";
            sqlQuery += " WHERE EWD.EFFECTIVE_TO_DT IS NULL AND EMP.TERMINATION_DATE IS NULL AND EMP.ENABLED_FLAG= 1 ";
            sqlQuery += " AND EMP.EMP_ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " AND EMP.EMP_ID='" + empID + "'";
            sqlQuery += "  ORDER BY TO_NUMBER(DEPT.ATTRIBUTE1),DD.DESIG_NAME,TO_NUMBER(EMP.EMP_NO) ";

            return sqlQuery;
        }

        
        
        
     
    }
}
