﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.PER
{
    public class PayrollGroupElement_DAL
    {
        static string sqlQuery = "";

        public static string GetPayPerioddtls(String PG_ELE_ID)
        {
            sqlQuery = string.Empty;


            sqlQuery = " select PEM.PG_ELE_ID,PEM.PAY_GROUP_ID,PG.PAY_GROUP_ID,PG.PAY_GROUP_CODE || ' - ' || PG.PAY_GROUP_DESC as PAY_GROUP,PEM.PAY_ELEMENT_ID,PE.PAY_ELEMENT_ID,PE.PAY_ELEMENT_CODE ,PE.PAY_ELEMENT_DESC,PEM.EFFECTIVE_FROM_DT,PEM.EFFECTIVE_TO_DT,PEM.ENABLED_FLAG,'N' as DELETED";
            sqlQuery += " from PAY_GROUP_ELEMENT_MAPPING PEM, PAY_GROUP_DTLS PG, PAY_ELEMENTS PE";
            sqlQuery += " where PEM.PAY_GROUP_ID = PG.PAY_GROUP_ID";
            sqlQuery += " and PEM.PAY_ELEMENT_ID = PE.PAY_ELEMENT_ID ";
            sqlQuery += "  and PEM.ENABLED_FLAG = 1 ";
            sqlQuery += " and PEM.WORKFLOW_COMPLETION_STATUS = 1 ";
        //    sqlQuery += " and PEM.PG_ELE_ID = '" + PG_ELE_ID + "'";
            sqlQuery += " and PEM.PAY_GROUP_ID = '" + PG_ELE_ID + "'";
            sqlQuery += " order by PG_ELE_ID ";
            return sqlQuery;

        }


        public static string getPayElements()
        {
            sqlQuery = string.Empty;

            sqlQuery = " select  PE.PAY_ELEMENT_ID,PE.PAY_ELEMENT_CODE" + VMVServices.Web.Utils.LanguageCode + " as PAY_ELEMENT ";
            sqlQuery += " from PAY_ELEMENTS PE ";
            sqlQuery += " where PE.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " and PE.PAY_ORG_ID = '" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " and PE.ENABLED_FLAG = 1 ";
            sqlQuery += " ORDER BY PE.PAY_ELEMENT_CODE ";

            return sqlQuery;

        }

        public static string getPayGroup()
        {
            sqlQuery = string.Empty;

            sqlQuery = " select PG.PAY_GROUP_ID,PG.PAY_GROUP_CODE" + VMVServices.Web.Utils.LanguageCode + "  as PAY_GROUP ";
            sqlQuery += " from PAY_GROUP_DTLS PG  ";
            sqlQuery += " where PG.ENABLED_FLAG =1";
            sqlQuery += " and PG.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " ORDER BY PG.PAY_GROUP_CODE ";

            return sqlQuery;

        }

        public static string getPayGroup_Name(string PAY_GROUP_ID)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select PG.PAY_GROUP_DESC ";
            sqlQuery += " from PAY_GROUP_DTLS PG  ";
            sqlQuery += " where PG.ENABLED_FLAG =1";
            sqlQuery += " and PG.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " and PG.PAY_GROUP_ID = '" + PAY_GROUP_ID + "'";

            return sqlQuery;

        }

        public static string geTEmpelementdetails(string PAY_GROUP_ID)
        {
            sqlQuery = string.Empty;


            sqlQuery += " select pem.PG_ELE_ID,pem.pay_group_id,pe.pay_element_id,pe.pay_element_code,'' PAY_EMP_ELEMENT_ID,pe.PAY_ELEMENT_DESC,'' PAY_AMOUNT,";
            sqlQuery += " pem.effective_from_dt,pem.effective_to_dt,'FALSE' AS ENABLED_FLAG,'N' as DELETED";
            sqlQuery += " from PAY_GROUP_ELEMENT_MAPPING PEM, PAY_ELEMENTS PE";
            sqlQuery += " where PEM.PAY_ELEMENT_ID = PE.PAY_ELEMENT_ID ";
            sqlQuery += " and PEM.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " AND PEM.PAY_GROUP_ID = '" + PAY_GROUP_ID + "'";


            return sqlQuery;

        }

        public static string getElement_Name(string PAY_ELEMENT_ID)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select PE.PAY_ELEMENT_DESC ";
            sqlQuery += " from PAY_ELEMENTS PE  ";
            sqlQuery += " where PE.ENABLED_FLAG =1";
            sqlQuery += " and PE.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " and PE.PAY_ELEMENT_ID = '" + PAY_ELEMENT_ID + "'";

            return sqlQuery;

        }
    }
}
