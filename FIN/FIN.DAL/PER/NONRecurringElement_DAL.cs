﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.PER
{
    public class NONRecurringElement_DAL
    {
        public static string getNonRecEmplist(string elementid, string strPeriodId)
        {
            string str_Query = string.Empty;
            str_Query = "";
            str_Query += " SELECT * FROM ( ";
            str_Query += " SELECT PNED.PAY_NR_DTL_ID,EMP.EMP_NO,EMP.EMP_ID,EMP.EMP_FIRST_NAME AS EMP_NAME,to_char(ROUND(PNED.PAY_DED_AMOUNT," + VMVServices.Web.Utils.DecimalPrecision + ")) as PAY_DED_AMOUNT,PNED.PAY_DED_REMARKS  ";
            str_Query += " ,DEPT.DEPT_ID,DEPT.DEPT_NAME ";
            str_Query += " FROM PAY_NR_ELEMENTS_DTL PNED  ";
            str_Query += " INNER JOIN PAY_NR_ELEMENTS_HDR PNEH ON PNEH.PAY_NR_ID = PNED.PAY_NR_ID  ";
            str_Query += " INNER JOIN HR_EMPLOYEES EMP ON EMP.EMP_ID = PNED.PAY_EMP_ID  ";
            str_Query += " INNER JOIN HR_EMP_WORK_DTLS EWD ON EWD.EMP_ID = EMP.EMP_ID AND EWD.EFFECTIVE_TO_DT IS NULL ";
            str_Query += " INNER JOIN HR_DEPARTMENTS DEPT ON DEPT.DEPT_ID = EWD.EMP_DEPT_ID ";
            str_Query += " WHERE PNEH.PAY_PERIOD_ID='" + strPeriodId + "' AND PNEH.PAY_ELEMENT_ID='" + elementid + "'";
            str_Query += " UNION";
            str_Query += " SELECT '0' AS PAY_NR_DTL_ID,EMP.EMP_NO ,EMP.EMP_ID,EMP.EMP_FIRST_NAME AS EMP_NAME,'' AS PAY_DED_AMOUNT,'' PAY_DED_REMARKS  ";
            str_Query += " ,DEPT.DEPT_ID,DEPT.DEPT_NAME ";
            str_Query += " FROM PAY_EMP_ELEMENT_VALUE PEEV   ";
            str_Query += " INNER JOIN HR_EMPLOYEES EMP ON EMP.EMP_ID = PEEV.PAY_EMP_ID  ";
            str_Query += " INNER JOIN HR_EMP_WORK_DTLS EWD ON EWD.EMP_ID = EMP.EMP_ID AND EWD.EFFECTIVE_TO_DT IS NULL ";
            str_Query += " INNER JOIN HR_DEPARTMENTS DEPT ON DEPT.DEPT_ID = EWD.EMP_DEPT_ID ";
            str_Query += " WHERE PEEV.PAY_ELEMENT_ID='" + elementid + "'";
            str_Query += " AND PEEV.PAY_EMP_ID NOT IN (SELECT PNED.PAY_EMP_ID FROM PAY_NR_ELEMENTS_DTL PNED  ";
            str_Query += " INNER JOIN PAY_NR_ELEMENTS_HDR PNEH ON PNEH.PAY_NR_ID = PNED.PAY_NR_ID  ";
            str_Query += " WHERE PNEH.PAY_PERIOD_ID='" + strPeriodId + "' AND PNEH.PAY_ELEMENT_ID='" + elementid + "'";
            str_Query += " )  ";
            str_Query += " ) Z ORDER BY TO_NUMBER(EMP_NO) ";


            return str_Query;
        }

        public static string getEmpAllowanceList()
        {
            string sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_ALLOWANCE V WHERE ROWNUM > 0 ";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["PAY_PERIOD_ID"] != null)
                {
                    sqlQuery += " AND V.PAY_PERIOD_ID = '" + VMVServices.Web.Utils.ReportFilterParameter["PAY_PERIOD_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["EMP_ID"] != null)
                {
                    sqlQuery += " AND V.PAY_EMP_ID = '" + VMVServices.Web.Utils.ReportFilterParameter["EMP_ID"].ToString() + "'";
                }
            }

            return sqlQuery;
        }

        public static string GetEmpInsurancePlan()
        {
            string sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_EMP_INSURANCE_PLAN V WHERE ROWNUM > 0 ";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["CAL_ID"] != null)
                {
                    sqlQuery += " AND V.CAL_ID = '" + VMVServices.Web.Utils.ReportFilterParameter["CAL_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["DEPT_ID"] != null)
                {
                    sqlQuery += " AND V.DEPT_ID = '" + VMVServices.Web.Utils.ReportFilterParameter["DEPT_ID"].ToString() + "'";
                }
            }

            return sqlQuery;
        }
    }
}
