﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.PER
{
    public class PayrollPeriods_DAL
    {
        static string sqlQuery = "";

        public static string GetPayPerioddtls(String PAY_PERIOD_ID)
        {
            sqlQuery = string.Empty;


            sqlQuery = " SELECT P.PAY_PERIOD_ID,P.PAY_PERIOD_DESC,P.PAY_PERIOD_DESC_OL,P.PAY_FROM_DT,P.PAY_TO_DT,p.PAYROLL_CUT_OFF_DT,'N' AS DELETED,";
            sqlQuery += " case P.enabled_flag when '1' then 'TRUE' else 'FALSE' END AS enabled_flag";
            sqlQuery += " ,P.NO_FRIDAYS,P.NO_WORKING_DAYS,P.NO_HOLIDAYS ";
            sqlQuery += " FROM PAY_PERIODS P";
            sqlQuery += " WHERE P.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND P.PAY_PERIOD_ID = '" + PAY_PERIOD_ID + "'";
            sqlQuery += " ORDER BY PAY_PERIOD_ID ";

            return sqlQuery;

        }

        public static string GetPayrollPeriods4Holiday(string str_HolidayId)
        {
            sqlQuery = string.Empty;
            sqlQuery += " select a.* from pay_periods a, hr_holidays_master b where a.pay_from_dt <= b.holiday_date and a.pay_to_dt >= b.holiday_date ";
            sqlQuery += " and b.HOLIDAY_ID='" + str_HolidayId + "'";
            sqlQuery += " AND a.PAY_ORG_ID = '" + VMVServices.Web.Utils.OrganizationID + "'";
            return sqlQuery;
        }

        public static string GetPayrollPeriods()
        {
            sqlQuery = string.Empty;

            string strLngCode = VMVServices.Web.Utils.LanguageCode;
            sqlQuery = " SELECT P.PAY_PERIOD_ID,P.PAY_PERIOD_DESC" + strLngCode + " as PAY_PERIOD_DESC";
            sqlQuery += " FROM PAY_PERIODS P";
            sqlQuery += " WHERE P.ENABLED_FLAG = 1";
            sqlQuery += " AND P.PAY_ORG_ID = '" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " AND P.WORKFLOW_COMPLETION_STATUS = 1";
            

            return sqlQuery;

        }
        public static string getPayrollPeriodWithDate()
        {
            sqlQuery = string.Empty;


            sqlQuery = " SELECT P.PAY_PERIOD_ID,P.PAY_PERIOD_DESC,P.PAY_FROM_DT";
            sqlQuery += " FROM PAY_PERIODS P";
            sqlQuery += " WHERE P.ENABLED_FLAG = 1";
            sqlQuery += " AND P.PAY_ORG_ID = '" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " AND P.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " ORDER BY PAY_FROM_DT DESC ";
            return sqlQuery;
        }

        public static string getPayrollPeriods4PayAdvice(string str_Mode)
        {
            sqlQuery = string.Empty;


            sqlQuery = " SELECT distinct P.PAY_PERIOD_ID,P.PAY_PERIOD_DESC";
            sqlQuery += " FROM PAY_PERIODS P";
            sqlQuery += " INNER JOIN PAY_FINAL_RUN_HDR FRH ON FRH.PAYROLL_PERIOD = P.PAY_PERIOD_ID ";
            sqlQuery += " INNER JOIN PAY_FINAL_RUN_DTL FRD ON FRD.PAYROLL_ID= FRH.PAYROLL_ID";
            sqlQuery += " INNER JOIN PAY_FINAL_RUN_DTL_DTL FRDD ON FRDD.PAYROLL_DTL_ID = FRD.PAYROLL_DTL_ID ";
            sqlQuery += " WHERE P.ENABLED_FLAG = 1";
            sqlQuery += " AND P.PAY_ORG_ID = '" + VMVServices.Web.Utils.OrganizationID + "'";
            //if (str_Mode == FINTableConstant.Add)
            //{
            //    sqlQuery += " AND FRDD.PAY_EMP_ID NOT IN (SELECT ATTRIBUTE1 FROM pay_advice) ";
            //}
            sqlQuery += " AND P.WORKFLOW_COMPLETION_STATUS = 1";

            return sqlQuery;
        }


        public static string GetPayrollWithoutApprovPeriods()
        {
            sqlQuery = string.Empty;


            sqlQuery = "   SELECT P.PAY_PERIOD_ID, P.PAY_PERIOD_DESC";
            sqlQuery += "   FROM PAY_PERIODS P";
            sqlQuery += "   WHERE P.ENABLED_FLAG = 1";
            sqlQuery += "   AND P.PAY_ORG_ID = '" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += "   AND P.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += "   and P.PAY_PERIOD_ID not in";
            sqlQuery += "   (select pr.payroll_period";
            sqlQuery += "    from pay_final_run_hdr pr";
            sqlQuery += "   where upper(pr.payroll_status) = upper('approved'))";
            return sqlQuery;

        }

        public static string GetEmpBasedOnPayrollPeriod(string payrollID)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select pp.pay_period_id, pep.emp_period_id, pep.pay_emp_id, pp.pay_period_desc,";
            sqlQuery += " e.emp_first_name || ' ' || e.emp_middle_name || ' ' || e.emp_last_name as emp_name";
            sqlQuery += " from pay_emp_periods pep, pay_periods pp, hr_employees e";
            sqlQuery += " where pp.pay_period_id = pep.pay_period_id";
            sqlQuery += " and e.emp_id=pep.pay_emp_id";
            sqlQuery += " and pp.ENABLED_FLAG = 1";
            sqlQuery += " and pp.PAY_ORG_ID = '" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " and pp.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " and pp.pay_period_id='" + payrollID + "'";
            sqlQuery += " order by pep.pay_emp_id";

            return sqlQuery;

        }
    }
}
