﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;

using System.Data.EntityClient;



using System.Data.Entity.Infrastructure;

namespace FIN.DAL.PER
{
    public class PayrollTrailrunDetails_DAL
    {
        static string sqlQuery = "";

        public static string GetPayTrialRunHdr(string payroll_id)
        {
            sqlQuery = string.Empty;


            sqlQuery = "   select prd.payroll_dtl_id,prd.pay_dept_id,d.dept_name,pt.PAY_GROUP_ID,pt.payroll_id,";
            sqlQuery += "   pt.payroll_period, ";
            sqlQuery += "    pt.payroll_days_hrs, ";
            sqlQuery += "    pt.payroll_status, ";
            sqlQuery += "    pt.payroll_date, ";
            sqlQuery += "    pt.payroll_total_payment, ";
            sqlQuery += "    pt.payroll_total_deduction,to_char(prd.pay_amount) as pay_amount,to_char(prd.pay_ded_amount) as pay_ded_amount,";
            sqlQuery += "    pt.payroll_remarks ";
            sqlQuery += "   ,pt.WORKFLOW_COMPLETION_STATUS AS WF ,";
            sqlQuery += "   to_char(prd.pay_amount-prd.pay_ded_amount) as pay_net_amount";
            sqlQuery += "   from pay_trial_run_hdr pt,pay_trial_run_dtl prd,hr_departments d ";
            sqlQuery += "  where pt.payroll_id = prd.payroll_id";
            sqlQuery += " and d.dept_id = prd.pay_dept_id";
            sqlQuery += " and  pt.payroll_id ='" + payroll_id + "'";
            //sqlQuery += "   where pt.enabled_flag =1";
            //sqlQuery += "    and pt.workflow_completion_status = 1";

            //if (groupId != string.Empty && groupId != "0")
            //{
            //    sqlQuery += " and pt.PAY_GROUP_ID='" + groupId + "'";
            //}
            //      sqlQuery += "   order by pt.payroll_id;";

            return sqlQuery;

        }
        public static string GetPayTrialRunDtl4PayrollId(string str_PayId)
        {
            sqlQuery = string.Empty;


            sqlQuery += " SELECT PTRD.PAYROLL_DTL_ID,PTRH.PAYROLL_ID,PTRD.PAY_DEPT_ID,HD.DEPT_NAME" + VMVServices.Web.Utils.LanguageCode + " AS DEPT_NAME ";
            sqlQuery += " ,to_char(ROUND(PTRD.PAY_AMOUNT," + VMVServices.Web.Utils.DecimalPrecision + ")) AS PAY_AMOUNT,to_char(ROUND(PTRD.PAY_DED_AMOUNT," + VMVServices.Web.Utils.DecimalPrecision + ")) AS  PAY_DED_AMOUNT ";
            //deiva
            sqlQuery += " ,to_char(ROUND(PTRD.PAY_AMOUNT-PTRD.PAY_DED_AMOUNT ," + VMVServices.Web.Utils.DecimalPrecision + ")) as pay_net_amount";
            sqlQuery += " ,PTRH.WORKFLOW_COMPLETION_STATUS AS WF ";
            sqlQuery += " FROM PAY_TRIAL_RUN_HDR PTRH ";
            sqlQuery += " INNER JOIN PAY_TRIAL_RUN_DTL PTRD ON PTRH.PAYROLL_ID = PTRD.PAYROLL_ID ";
            sqlQuery += " INNER JOIN HR_DEPARTMENTS HD ON HD.DEPT_ID = PTRD.PAY_DEPT_ID ";
            sqlQuery += " WHERE PTRH.ENABLED_FLAG=1 ";
            sqlQuery += " AND PTRH.PAYROLL_ID ='" + str_PayId + "'";

            return sqlQuery;

        }
        public static string GetPayTrialRunDtl(string str_period_id, string payGroupId)
        {
            sqlQuery = string.Empty;


            sqlQuery += " SELECT PTRD.PAYROLL_DTL_ID,PTRH.PAYROLL_ID,PTRD.PAY_DEPT_ID,HD.DEPT_NAME" + VMVServices.Web.Utils.LanguageCode + " AS DEPT_NAME ";
            sqlQuery += " ,to_char(ROUND(PTRD.PAY_AMOUNT," + VMVServices.Web.Utils.DecimalPrecision + ")) AS PAY_AMOUNT,to_char(ROUND(PTRD.PAY_DED_AMOUNT," + VMVServices.Web.Utils.DecimalPrecision + ")) AS  PAY_DED_AMOUNT ";
            sqlQuery += " ,PTRH.WORKFLOW_COMPLETION_STATUS AS WF ";
            //deivamani
            sqlQuery += " ,to_char(PTRD.PAY_AMOUNT-PTRD.PAY_DED_AMOUNT) as pay_net_amount";
            sqlQuery += " FROM PAY_TRIAL_RUN_HDR PTRH ";
            sqlQuery += " INNER JOIN PAY_TRIAL_RUN_DTL PTRD ON PTRH.PAYROLL_ID = PTRD.PAYROLL_ID ";
            sqlQuery += " INNER JOIN HR_DEPARTMENTS HD ON HD.DEPT_ID = PTRD.PAY_DEPT_ID ";
            sqlQuery += " WHERE PTRH.ENABLED_FLAG=1 ";
            sqlQuery += " AND PTRH.PAYROLL_PERIOD ='" + str_period_id + "'";
            if (payGroupId.Length > 0)
            {
                sqlQuery += "  AND PTRH.PAY_GROUP_ID='" + payGroupId + "' ";
            }

            return sqlQuery;

        }
        public static string getPayTrailrunEMP4Dept(string payrollDtlId)
        {
            sqlQuery = string.Empty;


            sqlQuery += " SELECT PAYROLL_DTL_ID,EMP_ID,EMP_NO,EMP_NAME,to_char(SUM(PAY_AMOUNT)) AS PAY_AMOUNT,to_char(SUM(PAY_DED_AMOUNT)) AS PAY_DED_AMOUNT, to_char(SUM(PAY_AMOUNT)-SUM(PAY_DED_AMOUNT)) as pay_net_amount FROM ( ";
            sqlQuery += " SELECT TRDD.PAYROLL_DTL_ID, EMP.EMP_ID,EMP.EMP_NO,EMP.EMP_FIRST_NAME AS EMP_NAME, to_char(SUM(ROUND(TRDD.PAY_AMOUNT," + VMVServices.Web.Utils.DecimalPrecision + "))) AS PAY_AMOUNT,to_char('0') AS PAY_DED_AMOUNT ";

            sqlQuery += " FROM PAY_TRIAL_RUN_DTL_DTL TRDD ";
            sqlQuery += " INNER JOIN PAY_ELEMENTS PEE ON PEE.PAY_ELEMENT_ID = TRDD.PAY_EMP_ELEMENT_ID AND PEE.PAY_ELE_CLASS='Earning' ";
            sqlQuery += " INNER JOIN HR_EMPLOYEES EMP ON EMP.EMP_ID = TRDD.PAY_EMP_ID ";
            sqlQuery += " WHERE TRDD.Payroll_Dtl_Id='" + payrollDtlId + "'";
            sqlQuery += " GROUP BY TRDD.PAYROLL_DTL_ID,EMP.EMP_ID,EMP.EMP_NO,EMP.EMP_FIRST_NAME ";
            sqlQuery += " UNION ";
            sqlQuery += " SELECT TRDD.PAYROLL_DTL_ID,EMP.EMP_ID,EMP.EMP_NO,EMP.EMP_FIRST_NAME AS EMP_NAME,to_char('0') AS pay_amount, to_char(SUM(ROUND(TRDD.PAY_AMOUNT," + VMVServices.Web.Utils.DecimalPrecision + "))) AS pay_ded_amount ";
            sqlQuery += " FROM PAY_TRIAL_RUN_DTL_DTL TRDD ";
            sqlQuery += " INNER JOIN PAY_ELEMENTS PEE ON PEE.PAY_ELEMENT_ID = TRDD.PAY_EMP_ELEMENT_ID AND PEE.PAY_ELE_CLASS='Deduction' ";
            sqlQuery += " INNER JOIN HR_EMPLOYEES EMP ON EMP.EMP_ID = TRDD.PAY_EMP_ID ";
            sqlQuery += " WHERE TRDD.Payroll_Dtl_Id='" + payrollDtlId + "'";
            sqlQuery += " GROUP BY TRDD.PAYROLL_DTL_ID,EMP.EMP_ID,EMP.EMP_NO,EMP.EMP_FIRST_NAME ";
            sqlQuery += " ) Z GROUP BY PAYROLL_DTL_ID,EMP_ID,EMP_NO,EMP_NAME";
            sqlQuery += " ORDER BY EMP_NO ";
            return sqlQuery;
        }

        public static string GetPayTrialRunDtlDtl(string payrollDtlId, string strEmpId)
        {
            sqlQuery = string.Empty;

            string strLngCode = VMVServices.Web.Utils.LanguageCode;
            sqlQuery = "         select ptdd.payroll_dtl_dtl_id,";
            sqlQuery += "   pe.PAY_ELE_CLASS as pay_element_Type ,";
            sqlQuery += "       ptdd.payroll_dtl_id,";
            sqlQuery += "       to_char(round(ptdd.pay_amount," + VMVServices.Web.Utils.DecimalPrecision + ")) pay_amount,";
            //sqlQuery += " ,to_char(ptdd.pay_amount-ptrd.pay_ded_amount) as pay_net_amount";
            sqlQuery += "       ptdd.PAY_EMP_ID,";
            sqlQuery += "       ptdd.PAY_EMP_ELEMENT_ID,";
            sqlQuery += "       (pe.pay_element_code||' - '|| pe.pay_element_desc) as pay_element_desc,";
            sqlQuery += "      (select (e.emp_first_name" + strLngCode + " || ' - ' || e.emp_middle_name" + strLngCode + " || ' - ' ||";
            sqlQuery += "             e.emp_last_name" + strLngCode + ")";
            sqlQuery += "       from hr_employees e";
            sqlQuery += "      where e.emp_id = ptdd.PAY_EMP_ID) as emp_name";
            //deivamani
            sqlQuery += "      from pay_trial_run_dtl_dtl ptdd,PAY_ELEMENTS pe";
            sqlQuery += "     where ptdd.enabled_flag = '1'";
            sqlQuery += "       and ptdd.workflow_completion_status = '1'";
            sqlQuery += "     and pe.pay_element_id=ptdd.PAY_EMP_ELEMENT_ID";
            sqlQuery += "    and ptdd.payroll_dtl_id='" + payrollDtlId + "'";
            sqlQuery += " and ptdd.PAY_EMP_ID='" + strEmpId + "'";
            sqlQuery += "     order by pe.PAY_ELE_CLASS";



            return sqlQuery;

        }

        public static string GetDtlEmp(string strEmpId)
        {
            sqlQuery = string.Empty;

            sqlQuery += " select E.EMP_ID ,e.emp_no||' - '||e.emp_first_name as EMP_NAME,E.EMP_DOJ,D.DEPT_NAME,DD.DESIG_NAME,d.DEPT_ID from ";
            sqlQuery += "  hr_employees e,hr_emp_work_dtls wd,hr_departments d,hr_dept_designations dd";
            sqlQuery += "  where e.emp_id = wd.emp_id";
            sqlQuery += " and wd.emp_dept_id = d.dept_id";
            sqlQuery += " and wd.emp_desig_id = dd.dept_desig_id";
            sqlQuery += "  AND E.EMP_ID ='" + strEmpId + "'";


            return sqlQuery;

        }

        public static string GetDepartmentDetails4PayPeriodGroup(string strPeriod, string strGroup)
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT d.DEPT_NAME" + VMVServices.Web.Utils.LanguageCode + " AS DEPT_NAME,d.DEPT_ID ";
            sqlQuery += " FROM HR_DEPARTMENTS d ";
            sqlQuery += " WHERE d.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " and d.org_id = '" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " AND d.ENABLED_FLAG = 1 ";
            //sqlQuery += " AND D.DEPT_ID NOT IN ( ";
            //sqlQuery += "SELECT TRD.PAY_DEPT_ID FROM PAY_TRIAL_RUN_HDR TRH ";
            //sqlQuery += "INNER JOIN PAY_TRIAL_RUN_DTL TRD ON TRH.PAYROLL_ID = TRD.PAYROLL_ID ";
            //sqlQuery += "WHERE TRH.WORKFLOW_COMPLETION_STATUS =1  AND TRH.PAYROLL_PERIOD='" + strPeriod + "'";
            //sqlQuery += "AND TRH.PAY_GROUP_ID ='" + strGroup + "'";
            //sqlQuery += ")";
            sqlQuery += " ORDER BY DEPT_NAME asc ";

            return sqlQuery;
        }

        public static string getPayroll4GroupWise(string str_Period)
        {
            sqlQuery = string.Empty;
            sqlQuery += "  SELECT TRH.Payroll_Id, PGD.PAY_GROUP_DESC,TRH.PAYROLL_STATUS,NVL(TRH.ATTRIBUTE1,'TRUE') AS APPROVE_SEND ";
            sqlQuery += " FROM PAY_TRIAL_RUN_HDR TRH ";
            sqlQuery += " INNER JOIN PAY_GROUP_DTLS PGD ON PGD.PAY_GROUP_ID = TRH.PAY_GROUP_ID ";
            sqlQuery += " WHERE TRH.PAYROLL_PERIOD='" + str_Period + "'";
            return sqlQuery;
        }

        public static string getApprovedProllId()
        {
            sqlQuery = string.Empty;
            sqlQuery += "SELECT TRH.PAYROLL_ID,TRH.PAYROLL_ID || ' - ' || PER.PAY_PERIOD_DESC || ' - ' || GD.PAY_GROUP_DESC AS PAYROLL_DET ";
            sqlQuery += " FROM PAY_TRIAL_RUN_HDR TRH ";
            sqlQuery += " INNER JOIN PAY_PERIODS PER ON PER.PAY_PERIOD_ID = TRH.PAYROLL_PERIOD ";
            sqlQuery += " INNER JOIN PAY_GROUP_DTLS GD ON GD.PAY_GROUP_ID = TRH.PAY_GROUP_ID ";
            sqlQuery += " WHERE PER.PAY_ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " AND TRH.workflow_completion_status =1 ";
            sqlQuery += " AND TRH.PAYROLL_ID NOT IN (SELECT PAYROLL_ID FROM pay_final_run_hdr) ";
            return sqlQuery;
        }

        public static string getApprovedProllId4PayrollPeriod(string str_PayPeriod)
        {
            sqlQuery = string.Empty;
            sqlQuery += "SELECT TRH.PAYROLL_ID,TRH.PAYROLL_ID || ' - ' || PER.PAY_PERIOD_DESC || ' - ' || GD.PAY_GROUP_DESC AS PAYROLL_DET ";
            sqlQuery += " FROM PAY_TRIAL_RUN_HDR TRH ";
            sqlQuery += " INNER JOIN PAY_PERIODS PER ON PER.PAY_PERIOD_ID = TRH.PAYROLL_PERIOD ";
            sqlQuery += " INNER JOIN PAY_GROUP_DTLS GD ON GD.PAY_GROUP_ID = TRH.PAY_GROUP_ID ";
            sqlQuery += " WHERE PER.PAY_ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " AND TRH.workflow_completion_status =1 ";
            sqlQuery += " AND TRH.PAYROLL_ID NOT IN (SELECT PAYROLL_ID FROM pay_final_run_hdr) ";
            sqlQuery += " AND TRH.PAYROLL_PERIOD='" + str_PayPeriod + "'";
            return sqlQuery;
        }

        public static string getFinalProllId()
        {
            sqlQuery = string.Empty;
            sqlQuery += "SELECT TRH.PAYROLL_ID,TRH.PAYROLL_ID || ' - ' || PER.PAY_PERIOD_DESC || ' - ' || GD.PAY_GROUP_DESC AS PAYROLL_DET ";
            sqlQuery += " FROM PAY_FINAL_RUN_HDR TRH ";
            sqlQuery += " INNER JOIN PAY_PERIODS PER ON PER.PAY_PERIOD_ID = TRH.PAYROLL_PERIOD ";
            sqlQuery += " INNER JOIN PAY_GROUP_DTLS GD ON GD.PAY_GROUP_ID = TRH.PAY_GROUP_ID ";
            sqlQuery += " WHERE PER.PAY_ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";

            return sqlQuery;
        }


        public static string getFinalProllDtlId4Payadvice(string str_payrollId, string str_Mode)
        {
            sqlQuery = string.Empty;
            sqlQuery += " SELECT distinct TRH.PAYROLL_ID,TRH.PAYROLL_ID || ' - ' || PER.PAY_PERIOD_DESC || ' - ' || GD.PAY_GROUP_DESC AS PAYROLL_DET ";
            sqlQuery += " FROM PAY_FINAL_RUN_HDR TRH ";
            sqlQuery += " INNER JOIN PAY_PERIODS PER ON PER.PAY_PERIOD_ID = TRH.PAYROLL_PERIOD ";
            sqlQuery += " INNER JOIN PAY_GROUP_DTLS GD ON GD.PAY_GROUP_ID = TRH.PAY_GROUP_ID ";
            sqlQuery += " INNER JOIN PAY_FINAL_RUN_DTL FRD ON FRD.PAYROLL_ID= TRH.PAYROLL_ID";
            sqlQuery += " INNER JOIN PAY_FINAL_RUN_DTL_DTL FRDD ON FRDD.PAYROLL_DTL_ID= FRD.PAYROLL_DTL_ID ";
            sqlQuery += " WHERE PER.PAY_ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " AND TRH.PAYROLL_PERIOD='" + str_payrollId + "'";
            //if (str_Mode == FINTableConstant.Add)
            //{
            //    sqlQuery += " AND FRDD.PAY_EMP_ID NOT IN (SELECT ATTRIBUTE1 FROM pay_advice) ";
            //}

            return sqlQuery;
        }
        public static void Payroll_Trail_MainPosting(string payrollId)
        {
            try
            {

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.CommandText = "PKG_PAYROLL.payroll_finalrun_process";
                oraCmd.Parameters.Add(new OracleParameter("@p_org_id", OracleDbType.Varchar2, 250)).Value = VMVServices.Web.Utils.OrganizationID;
                oraCmd.Parameters.Add(new OracleParameter("@p_payroll_id", OracleDbType.Varchar2, 250)).Value = payrollId;

                DBMethod.ExecuteFunction(oraCmd);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string getPayDtlid4periodemp(string str_period_id, string str_emp_id)
        {
            string sqlQuery = "";

            sqlQuery += " SELECT  distinct TRDD.PAYROLL_DTL_ID,TRH.Workflow_Completion_Status,TRH.PAYROLL_ID FROM PAY_TRIAL_RUN_DTL_DTL TRDD ";
            sqlQuery += " INNER JOIN PAY_TRIAL_RUN_DTL TRD ON TRDD.PAYROLL_DTL_ID= TRD.PAYROLL_DTL_ID ";
            sqlQuery += " INNER JOIN PAY_TRIAL_RUN_HDR TRH ON TRH.PAYROLL_ID= TRD.PAYROLL_ID ";
            sqlQuery += " WHERE TRH.PAYROLL_PERIOD='" + str_period_id + "' AND TRDD.PAY_EMP_ID='" + str_emp_id + "'";

            return sqlQuery;
        }

        public static string getPayDtlFromTmp(string str_emp_id)
        {
            string sqlQuery = "";
            sqlQuery += "  select distinct '' as PAYROLL_DTL_ID,";
            sqlQuery += "      '' as PAYROLL_DTL_DTL_ID,";
            sqlQuery += "      pe.PAY_ELE_CLASS as pay_element_Type,     ";
            sqlQuery += "     to_char(round(ptdd.pay_amount, 3)) pay_amount,";
            sqlQuery += "   to_char(round(ptdd.pay_dept_ded_amt, 3)) pay_deduction_amount,";
            sqlQuery += "  to_char(round(ptdd.pay_amount - ptdd.pay_dept_ded_amt, 3)) as pay_net_amount,";
            sqlQuery += "   ptdd.PAY_EMP_ID,";
            sqlQuery += "  ptdd.PAY_EMP_ELEMENT_ID,";
            sqlQuery += "  (pe.pay_element_code || ' - ' || pe.pay_element_desc) as pay_element_desc,";
            sqlQuery += "  (select (e.emp_first_name || ' - ' || e.emp_middle_name ||";
            sqlQuery += "       ' - ' || e.emp_last_name)";
            sqlQuery += "   from hr_employees e";
            sqlQuery += "   where e.emp_id = ptdd.PAY_EMP_ID) as emp_name";
            sqlQuery += "   from TMP_PAYROLL ptdd, PAY_ELEMENTS pe";
            sqlQuery += "   where ptdd.pay_emp_element_id = pe.pay_element_id";
            sqlQuery += "  and pe.pay_element_id = ptdd.PAY_EMP_ELEMENT_ID";
            sqlQuery += "  and ptdd.PAY_EMP_ID = '" + str_emp_id + "'";
            sqlQuery += "  order by pe.PAY_ELE_CLASS";
            return sqlQuery;
        }

    }
}
