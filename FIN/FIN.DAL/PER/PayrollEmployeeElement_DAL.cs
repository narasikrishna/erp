﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.PER
{
    public class PayrollEmployeeElement_DAL
    {
        static string sqlQuery = "";

        public static string GetPayPerioddtls(String PAY_EMP_ELEM_ID)
        {
            sqlQuery = string.Empty;


            sqlQuery = " select PEEV.PAY_GROUP_ID,PE.PAY_ELEMENT_ID,PE.PAY_ELEMENT_CODE,PEEV.PAY_EMP_ELEMENT_ID,PE.PAY_ELEMENT_DESC,to_char(PEEV.PAY_AMOUNT) as PAY_AMOUNT,PEEV.EFFECTIVE_FROM_DT,PEEV.EFFECTIVE_TO_DT,PEEV.ENABLED_FLAG,'N' as DELETED,";
            sqlQuery += " CASE PEEV.PAY_ELEMENT_PRORATE WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS PAY_ELEMENT_PRORATE,";
            sqlQuery += " CASE PEEV.PAID_FOR_ANNUAL_LEAVE WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS PAID_FOR_ANNUAL_LEAVE";
            sqlQuery += " from PAY_EMP_ELEMENT_VALUE PEEV,PAY_ELEMENTS PE";
            sqlQuery += " WHERE PEEV.PAY_ELEMENT_ID = PE.PAY_ELEMENT_ID";
            sqlQuery += " and PEEV.ENABLED_FLAG = 1 ";
            sqlQuery += " and PEEV.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " and PEEV.PAY_EMP_ELEMENT_ID = '" + PAY_EMP_ELEM_ID + "'";
            sqlQuery += " order by PEEV.effective_from_dt desc";

            return sqlQuery;

        }

        public static string GetPayPerioddtls4EmpId(String str_EMPID)
        {
            sqlQuery = string.Empty;


            sqlQuery = " select PEEV.PAY_GROUP_ID,PE.PAY_ELEMENT_ID,PE.PAY_ELEMENT_CODE" + VMVServices.Web.Utils.LanguageCode + " AS PAY_ELEMENT_CODE,PEEV.PAY_EMP_ELEMENT_ID,PE.PAY_ELEMENT_DESC" + VMVServices.Web.Utils.LanguageCode + " AS PAY_ELEMENT_DESC,to_char(PEEV.PAY_AMOUNT) as PAY_AMOUNT,PEEV.EFFECTIVE_FROM_DT,PEEV.EFFECTIVE_TO_DT,PEEV.ENABLED_FLAG,'N' as DELETED,";
            sqlQuery += " CASE PEEV.PAY_ELEMENT_PRORATE WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS PAY_ELEMENT_PRORATE,";
            sqlQuery += " CASE PEEV.PAID_FOR_ANNUAL_LEAVE WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS PAID_FOR_ANNUAL_LEAVE";
            sqlQuery += " ,PE.PAY_ELE_CLASS ";
            sqlQuery += " from PAY_EMP_ELEMENT_VALUE PEEV,PAY_ELEMENTS PE";
            sqlQuery += " WHERE PEEV.PAY_ELEMENT_ID = PE.PAY_ELEMENT_ID";
            sqlQuery += " and PEEV.ENABLED_FLAG = 1 ";
            sqlQuery += " and PEEV.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " and PEEV.PAY_EMP_ID = '" + str_EMPID + "'";
            sqlQuery += " order by PEEV.effective_from_dt desc ";

            return sqlQuery;

        }

        public static string GetPayPerioddtls_Bsdon_Grpcode(String PAY_GROUP_ID)
        {
            sqlQuery = string.Empty;


            sqlQuery = " select peev.PAY_GROUP_ID,0 as PAY_EMP_ELEMENT_ID,PE.PAY_ELEMENT_ID,PE.PAY_ELEMENT_CODE,PE.PAY_ELEMENT_DESC,'' as PAY_AMOUNT,'' EFFECTIVE_FROM_DT,'' EFFECTIVE_TO_DT,'TRUE' AS ENABLED_FLAG,'N' as DELETED,";
            sqlQuery += " CASE PE.PAY_ELEMENT_PRORATE WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS PAY_ELEMENT_PRORATE,";
            sqlQuery += " CASE PE.PAID_FOR_ANNUAL_LEAVE WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS PAID_FOR_ANNUAL_LEAVE";
            sqlQuery += " from PAY_GROUP_ELEMENT_MAPPING PEEV,PAY_ELEMENTS PE";
            sqlQuery += " WHERE PEEV.PAY_ELEMENT_ID = PE.PAY_ELEMENT_ID";
            sqlQuery += " and PEEV.ENABLED_FLAG = 1 ";
            sqlQuery += " and PEEV.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " and PEEV.PAY_GROUP_ID = '" + PAY_GROUP_ID + "'";

            return sqlQuery;

        }


        public static string getPayGroup()
        {
            sqlQuery = string.Empty;

            sqlQuery = " select PG.PAY_GROUP_ID,PG.PAY_GROUP_CODE" + VMVServices.Web.Utils.LanguageCode + " as PAY_GROUP ";
            sqlQuery += " from PAY_GROUP_DTLS PG  ";
            sqlQuery += " where PG.ENABLED_FLAG =1";
            sqlQuery += " and PG.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " and PG.PAY_ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " ORDER BY PG.PAY_GROUP_CODE ";

            return sqlQuery;

        }


        public static string getEmployees()
        {
            sqlQuery = string.Empty;

            sqlQuery = " select HE.EMP_ID,HE.EMP_FIRST_NAME" + VMVServices.Web.Utils.LanguageCode + " AS EMP_FIRST_NAME from  HR_EMPLOYEES HE ";
            sqlQuery += " where HE.ENABLED_FLAG = 1  ";
            sqlQuery += " and HE.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " ORDER BY HE.EMP_FIRST_NAME ";

            return sqlQuery;

        }

        public static string getEmployeeName(string Emp_id)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select  HE.EMP_FIRST_NAME" + VMVServices.Web.Utils.LanguageCode + " AS EMP_FIRST_NAME from HR_EMPLOYEES HE ";
            sqlQuery += " where HE.ENABLED_FLAG = 1  ";
            sqlQuery += " and HE.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " and HE.EMP_ID ='" + Emp_id + "'";

            return sqlQuery;

        }

        public static string getGrade(string Emp_id)
        {
            sqlQuery = string.Empty;

            sqlQuery = "SELECT G.GRADE_DESC ";
            sqlQuery += " FROM HR_EMP_WORK_DTLS WD, HR_GRADES G ";
            sqlQuery += "WHERE WD.EMP_GRADE_ID = G.GRADE_ID ";
            sqlQuery += " and WD.ENABLED_FLAG = 1  ";
            sqlQuery += " and WD.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " and WD.EMP_ID ='" + Emp_id + "'";

            return sqlQuery;

        }
        public static string getElement(string Emp_id)
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT  E.PAY_ELEMENT_CODE,LD.LOW_LIMIT,LD.HIGH_LIMIT FROM HR_GRADES_LEVEL_DTLS LD, HR_EMP_WORK_DTLS WD,PAY_ELEMENTS E ";
            sqlQuery += " WHERE LD.GRADE_ID = WD.EMP_GRADE_ID ";
            sqlQuery += " AND LD.PAY_ELEMENT_ID = E.PAY_ELEMENT_ID ";
            sqlQuery += " and WD.EMP_ID ='" + Emp_id + "'";

            return sqlQuery;

        }

        public static string getPayGroup_Name(string PAY_GROUP_ID)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select PG.PAY_GROUP_DESC ";
            sqlQuery += " from PAY_GROUP_DTLS PG  ";
            sqlQuery += " where PG.ENABLED_FLAG =1";
            sqlQuery += " and PG.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " and PG.PAY_GROUP_ID = '" + PAY_GROUP_ID + "'";

            return sqlQuery;

        }

        public static string getPayElements()
        {
            sqlQuery = string.Empty;

            sqlQuery = " select  PE.PAY_ELEMENT_ID,PE.PAY_ELEMENT_CODE" + VMVServices.Web.Utils.LanguageCode + " as PAY_ELEMENT ";
            sqlQuery += " from PAY_ELEMENTS PE ";
            sqlQuery += " where PE.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " and PE.ENABLED_FLAG = 1 ";
            sqlQuery += " and PE.PAY_ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " ORDER BY PE.PAY_ELEMENT_CODE ";

            return sqlQuery;

        }

        public static string getPayElements_Name(string PAY_ELEMENT_ID)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select PE.PAY_ELEMENT_DESC, ";
            sqlQuery += " CASE PE.PAY_ELEMENT_PRORATE WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS PAY_ELEMENT_PRORATE,";
            sqlQuery += " CASE PE.PAID_FOR_ANNUAL_LEAVE WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS PAID_FOR_ANNUAL_LEAVE";
            sqlQuery += " from PAY_ELEMENTS PE ";
            sqlQuery += " where PE.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " and PE.ENABLED_FLAG = 1 ";
            sqlQuery += " AND PE.PAY_ELEMENT_ID = '" + PAY_ELEMENT_ID + "'";

            return sqlQuery;

        }
    }
}
