﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.PER
{
    public class PayrollRun_DAL
    {
        public static string UpdateFinalRunStatus(string str_PayrollId)
        {
            string sqlQuery = string.Empty;
            sqlQuery = " Update PAY_TRIAL_RUN_HDR set PAYROLL_STATUS='APPROVED' where PAYROLL_ID='" + str_PayrollId + "'";

            return sqlQuery;
        }

        public static string GetPayFinalRunDtl(string str_payrollID)
        {
            string sqlQuery = string.Empty;


            sqlQuery += " SELECT PTRD.PAYROLL_DTL_ID,PTRH.PAYROLL_ID,PTRD.PAY_DEPT_ID,HD.DEPT_NAME" + VMVServices.Web.Utils.LanguageCode + " AS DEPT_NAME ";
            sqlQuery += " ,to_char(ROUND(PTRD.PAY_AMOUNT," + VMVServices.Web.Utils.DecimalPrecision + ")) AS PAY_AMOUNT,to_char(ROUND(PTRD.PAY_DED_AMOUNT," + VMVServices.Web.Utils.DecimalPrecision + ")) AS  PAY_DED_AMOUNT ";
            sqlQuery += " ,PTRH.WORKFLOW_COMPLETION_STATUS AS WF ";
            sqlQuery += ",to_char(ROUND(PAY_AMOUNT-PAY_DED_AMOUNT)) as pay_net_amount";
            sqlQuery += " FROM PAY_FINAL_RUN_HDR PTRH ";
            sqlQuery += " INNER JOIN PAY_FINAL_RUN_DTL PTRD ON PTRH.PAYROLL_ID = PTRD.PAYROLL_ID ";
            sqlQuery += " INNER JOIN HR_DEPARTMENTS HD ON HD.DEPT_ID = PTRD.PAY_DEPT_ID ";
            sqlQuery += " WHERE PTRH.ENABLED_FLAG=1 ";
            sqlQuery += " AND PTRH.PAYROLL_ID ='" + str_payrollID + "'";


            return sqlQuery;

        }

        public static string GetPayFinalRunDtl4PayrollPeriod(string str_payPeriod)
        {
            string sqlQuery = string.Empty;


            sqlQuery += " SELECT PTRD.PAYROLL_DTL_ID,PTRH.PAYROLL_ID,PTRD.PAY_DEPT_ID,HD.DEPT_NAME" + VMVServices.Web.Utils.LanguageCode + " AS DEPT_NAME ";
            sqlQuery += " ,to_char(ROUND(PTRD.PAY_AMOUNT," + VMVServices.Web.Utils.DecimalPrecision + ")) AS PAY_AMOUNT,to_char(ROUND(PTRD.PAY_DED_AMOUNT," + VMVServices.Web.Utils.DecimalPrecision + ")) AS  PAY_DED_AMOUNT ";
            sqlQuery += " ,PTRH.WORKFLOW_COMPLETION_STATUS AS WF ";
            sqlQuery += ",to_char(ROUND(PAY_AMOUNT-PAY_DED_AMOUNT)) as pay_net_amount";
            sqlQuery += " FROM PAY_FINAL_RUN_HDR PTRH ";
            sqlQuery += " INNER JOIN PAY_FINAL_RUN_DTL PTRD ON PTRH.PAYROLL_ID = PTRD.PAYROLL_ID ";
            sqlQuery += " INNER JOIN HR_DEPARTMENTS HD ON HD.DEPT_ID = PTRD.PAY_DEPT_ID ";
            sqlQuery += " WHERE PTRH.ENABLED_FLAG=1 ";
            sqlQuery += " AND PTRH.PAYROLL_PERIOD ='" + str_payPeriod + "'";


            return sqlQuery;

        }

        public static string getPayFinalrunEMP4Dept(string payrollDtlId)
        {
            string sqlQuery = string.Empty;


            sqlQuery += " SELECT PAYROLL_DTL_ID,EMP_ID,EMP_NO,EMP_NAME,to_char(SUM(PAY_AMOUNT)) AS PAY_AMOUNT,to_char(SUM(PAY_DED_AMOUNT)) AS PAY_DED_AMOUNT,to_char(SUM(PAY_AMOUNT)-SUM(PAY_DED_AMOUNT)) as pay_net_amount  FROM ( ";
            sqlQuery += " SELECT TRDD.PAYROLL_DTL_ID, EMP.EMP_ID,EMP.EMP_NO,EMP.EMP_FIRST_NAME AS EMP_NAME, to_char(SUM(ROUND(TRDD.PAY_AMOUNT," + VMVServices.Web.Utils.DecimalPrecision + "))) AS PAY_AMOUNT,'0' AS PAY_DED_AMOUNT ";
            sqlQuery += " FROM PAY_FINAL_RUN_DTL_DTL TRDD ";
            sqlQuery += " INNER JOIN PAY_ELEMENTS PEE ON PEE.PAY_ELEMENT_ID = TRDD.PAY_EMP_ELEMENT_ID AND PEE.PAY_ELE_CLASS='Earning' ";
            sqlQuery += " INNER JOIN HR_EMPLOYEES EMP ON EMP.EMP_ID = TRDD.PAY_EMP_ID ";
            sqlQuery += " WHERE TRDD.Payroll_Dtl_Id='" + payrollDtlId + "'";
            sqlQuery += " GROUP BY TRDD.PAYROLL_DTL_ID,EMP.EMP_ID,EMP.EMP_NO,EMP.EMP_FIRST_NAME ";
            sqlQuery += " UNION ";
            sqlQuery += " SELECT TRDD.PAYROLL_DTL_ID,EMP.EMP_ID,EMP.EMP_NO,EMP.EMP_FIRST_NAME AS EMP_NAME,'0' AS pay_amount, to_char(SUM(ROUND(TRDD.PAY_AMOUNT," + VMVServices.Web.Utils.DecimalPrecision + "))) AS pay_ded_amount ";
            sqlQuery += " FROM PAY_FINAL_RUN_DTL_DTL TRDD ";
            sqlQuery += " INNER JOIN PAY_ELEMENTS PEE ON PEE.PAY_ELEMENT_ID = TRDD.PAY_EMP_ELEMENT_ID AND PEE.PAY_ELE_CLASS='Deduction' ";
            sqlQuery += " INNER JOIN HR_EMPLOYEES EMP ON EMP.EMP_ID = TRDD.PAY_EMP_ID ";
            sqlQuery += " WHERE TRDD.Payroll_Dtl_Id='" + payrollDtlId + "'";
            sqlQuery += " GROUP BY TRDD.PAYROLL_DTL_ID,EMP.EMP_ID,EMP.EMP_NO,EMP.EMP_FIRST_NAME ";
            sqlQuery += " ) Z GROUP BY PAYROLL_DTL_ID,EMP_ID,EMP_NO,EMP_NAME ";
            sqlQuery += " ORDER BY EMP_NO ";
            return sqlQuery;
        }


        public static string GetPayFinalRunDtlDtl(string payrollDtlId, string strEmpId)
        {
            string sqlQuery = string.Empty;

            string strLngCode = VMVServices.Web.Utils.LanguageCode;
            sqlQuery = "         select ptdd.payroll_dtl_dtl_id,";
            sqlQuery += "   pe.PAY_ELE_CLASS as pay_element_Type ,";
            sqlQuery += "       ptdd.payroll_dtl_id,";
            sqlQuery += "       to_char(round(ptdd.pay_amount," + VMVServices.Web.Utils.DecimalPrecision + ")) pay_amount,";
            sqlQuery += "       ptdd.PAY_EMP_ID,";
            sqlQuery += "       ptdd.PAY_EMP_ELEMENT_ID,";
            sqlQuery += "       (pe.pay_element_code||' - '|| pe.pay_element_desc) as pay_element_desc,";
            sqlQuery += "      (select (e.emp_first_name" + strLngCode + " || ' - ' || e.emp_middle_name" + strLngCode + " || ' - ' ||";
            sqlQuery += "             e.emp_last_name" + strLngCode + ")";
            sqlQuery += "       from hr_employees e";
            sqlQuery += "      where e.emp_id = ptdd.PAY_EMP_ID) as emp_name";
            sqlQuery += "      from pay_final_run_dtl_dtl ptdd,PAY_ELEMENTS pe";
            sqlQuery += "     where ptdd.enabled_flag = '1'";
            sqlQuery += "       and ptdd.workflow_completion_status = '1'";
            sqlQuery += "     and pe.pay_element_id=ptdd.PAY_EMP_ELEMENT_ID";
            sqlQuery += "    and ptdd.payroll_dtl_id='" + payrollDtlId + "'";
            sqlQuery += " and ptdd.PAY_EMP_ID='" + strEmpId + "'";
            sqlQuery += "     order by pe.PAY_ELE_CLASS";



            return sqlQuery;

        }

        public static string GetPayAdvice(string str_payAdvID)
        {
            string sqlQuery = string.Empty;

            sqlQuery += " SELECT PTRD.PAYROLL_DTL_ID,PTRH.PAYROLL_ID,PTRD.PAY_DEPT_ID,HD.DEPT_NAME" + VMVServices.Web.Utils.LanguageCode + " AS DEPT_NAME ";
            sqlQuery += " ,to_char(ROUND(PTRD.PAY_AMOUNT," + VMVServices.Web.Utils.DecimalPrecision + ")) AS PAY_AMOUNT,to_char(ROUND(PTRD.PAY_DED_AMOUNT," + VMVServices.Web.Utils.DecimalPrecision + ")) AS  PAY_DED_AMOUNT ";
            sqlQuery += " ,PTRH.WORKFLOW_COMPLETION_STATUS AS WF, PTRH.PAYROLL_PERIOD ";
            sqlQuery += " ,PA.PAY_ADV_ID,PA.PAY_DATE,PA.ENABLED_FLAG AS APPROVED,PA.PAY_PERIOD_ID,PA.BANK_ID,PA.PAY_ORG_ID";
            sqlQuery += " FROM PAY_ADVICE PA ";
            sqlQuery += " INNER JOIN PAY_FINAL_RUN_DTL PTRD ON PTRD.PAYROLL_DTL_ID=PA.PAYROLL_DTL_ID ";
            sqlQuery += " INNER JOIN PAY_FINAL_RUN_HDR PTRH ON PTRH.PAYROLL_ID = PTRD.PAYROLL_ID ";
            sqlQuery += " INNER JOIN HR_DEPARTMENTS HD ON HD.DEPT_ID = PTRD.PAY_DEPT_ID ";
            sqlQuery += " WHERE PA.ENABLED_FLAG=1 ";
            sqlQuery += " AND PA.PAY_ADV_ID='" + str_payAdvID + "'";

            return sqlQuery;
        }

        public static string GetPayFinalRunDtlPayAdvice(string str_bank_id, string str_PeriodId, string str_PayrollId)
        {
            string sqlQuery = string.Empty;

            //sqlQuery += "  SELECT PTRD.PAYROLL_DTL_ID,PTRH.PAYROLL_ID,PTRD.PAY_DEPT_ID,HD.DEPT_NAME" + VMVServices.Web.Utils.LanguageCode + " AS DEPT_NAME ";
            //sqlQuery += "  ,to_char(ROUND(PTRD.PAY_AMOUNT," + VMVServices.Web.Utils.DecimalPrecision + ")) AS PAY_AMOUNT,to_char(ROUND(PTRD.PAY_DED_AMOUNT," + VMVServices.Web.Utils.DecimalPrecision + ")) AS  PAY_DED_AMOUNT ";
            //sqlQuery += "  , to_char(nvl(ROUND(PTRD.PAY_AMOUNT, " + VMVServices.Web.Utils.DecimalPrecision + "),0)-nvl(round(ptrd.pay_ded_amount," + VMVServices.Web.Utils.DecimalPrecision + "),0)) as pay_net_amt";
            //sqlQuery += "  ,PTRH.WORKFLOW_COMPLETION_STATUS AS WF, PTRH.PAYROLL_PERIOD ";
            //sqlQuery += "  ,(ee.emp_no||' - '||ee.emp_first_name||' '||ee.emp_middle_name||' '||ee.emp_last_name) as emp_name,ee.emp_id,";
            //sqlQuery += "  eb.emp_bank_code ,cc.bank_name, eb.emp_bank_acct_code";

            //sqlQuery += "  FROM hr_emp_work_dtls wd,hr_employees ee,hr_emp_bank_dtls  eb,ca_bank cc,PAY_FINAL_RUN_HDR PTRH ";
            //sqlQuery += "  INNER JOIN PAY_FINAL_RUN_DTL PTRD ON PTRH.PAYROLL_ID = PTRD.PAYROLL_ID ";
            //sqlQuery += " INNER JOIN PAY_FINAL_RUN_DTL_DTL FRDD ON FRDD.PAYROLL_DTL_ID = PTRD.PAYROLL_DTL_ID ";
            //sqlQuery += "  INNER JOIN HR_DEPARTMENTS HD ON HD.DEPT_ID = PTRD.PAY_DEPT_ID ";
            //sqlQuery += "  WHERE PTRH.ENABLED_FLAG=1 ";
            ////sqlQuery += "  and ptrd.PAYROLL_DTL_ID not in (select pa.PAYROLL_DTL_ID from pay_advice pa where pa.PAY_PERIOD_ID=PTRH.PAYROLL_PERIOD)";
            //sqlQuery += " AND FRDD.PAY_EMP_ID NOT IN (SELECT ATTRIBUTE1 FROM PAY_ADVICE) ";
            //sqlQuery += "  and wd.emp_dept_id=hd.dept_id";
            //sqlQuery += "  and wd.emp_id=ee.emp_id";
            //sqlQuery += "  and eb.emp_id = ee.emp_id";
            //sqlQuery += "   and cc.bank_id=eb.emp_bank_code";
            sqlQuery += " SELECT * FROM ( ";
            sqlQuery += " SELECT FRED.EMP_NO, PP.PAY_ADV_ID, FRED.PAYROLL_ID,'' as PAYROLL_DTL_ID,FRH.PAYROLL_PERIOD,FRH.WORKFLOW_COMPLETION_STATUS as WF,FRED.EMP_DEPARTMENT_ID as PAY_DEPT_ID, FRED.EMP_ID,FRED.EMP_NAME" + VMVServices.Web.Utils.LanguageCode + " as EMP_NAME,FRED.EMP_BANK as bank_name,EBD.EMP_IBAN_NUM as emp_bank_acct_code,EBD.EMP_BANK_CODE, to_char(ROUND(FRED.NET_DUE," + VMVServices.Web.Utils.DecimalPrecision + ")) AS pay_net_amt ";
            sqlQuery += " ,'TRUE' AS APPROVED,PP.ATTRIBUTE5 as PAY_ADVICE_NUMBER ";
            sqlQuery += " FROM PAY_FINAL_RUN_EMP_DTL FRED ";
            sqlQuery += " INNER JOIN PAY_FINAL_RUN_HDR FRH on FRH.PAYROLL_ID= FRED.PAYROLL_ID ";
            sqlQuery += " INNER JOIN PAY_ADVICE PP ON PP.PAYROLL_ID= FRED.PAYROLL_ID AND PP.ATTRIBUTE1 = FRED.EMP_ID ";
            sqlQuery += " INNER JOIN HR_EMP_BANK_DTLS EBD ON EBD.PK_ID= FRED.EMP_BANK_ID ";
            sqlQuery += " WHERE PP.PAY_PERIOD_ID='" + str_PeriodId + "'";
            sqlQuery += " AND PP.BANK_ID='" + str_bank_id + "'";
            if (str_PayrollId.Length > 0)
            {
                sqlQuery += " AND PP.PAYROLL_ID ='" + str_PayrollId + "'";
            }
            sqlQuery += " UNION ";
            sqlQuery += " SELECT FRED.EMP_NO, '0' AS PAY_ADV_ID,FRED.PAYROLL_ID,'' as PAYROLL_DTL_ID,FRH.PAYROLL_PERIOD,FRH.WORKFLOW_COMPLETION_STATUS as WF,FRED.EMP_DEPARTMENT_ID as PAY_DEPT_ID, FRED.EMP_ID,FRED.EMP_NAME" + VMVServices.Web.Utils.LanguageCode + " as EMP_NAME,FRED.EMP_BANK as bank_name,EBD.EMP_IBAN_NUM as emp_bank_acct_code,EBD.EMP_BANK_CODE, to_char(ROUND(FRED.NET_DUE," + VMVServices.Web.Utils.DecimalPrecision + ")) AS pay_net_amt ";
            sqlQuery += " ,'FALSE' AS APPROVED,'' as  PAY_ADVICE_NUMBER";
            sqlQuery += " FROM PAY_FINAL_RUN_EMP_DTL FRED ";
            sqlQuery += " INNER JOIN PAY_FINAL_RUN_HDR FRH on FRH.PAYROLL_ID= FRED.PAYROLL_ID ";
            //sqlQuery += " INNER JOIN PAY_FINAL_RUN_DTL FRD ON FRED.PAYROLL_ID= FRED.PAYROLL_ID ";
            sqlQuery += " INNER JOIN HR_EMP_BANK_DTLS EBD ON EBD.PK_ID= FRED.EMP_BANK_ID ";
            sqlQuery += " where FRED.PAYROLL_PERIOD ='" + str_PeriodId + "'";
            sqlQuery += " AND FRED.EMP_ID NOT IN (SELECT ATTRIBUTE1 FROM PAY_ADVICE where PAY_PERIOD_ID='" + str_PeriodId + "') ";
            if (str_PayrollId.Length > 0)
            {
                sqlQuery += " AND FRH.PAYROLL_ID ='" + str_PayrollId + "'";
            }
            sqlQuery += " )  order by to_number(EMP_NO) ";
            return sqlQuery;
        }

        public static string getPayAdviceReportData()
        {
            string sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_PAY_ADVICE V WHERE ROWNUM > 0 ";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["Pay_Adv_Number"] != null)
                {
                    sqlQuery += " AND V.pay_adv_id='" + VMVServices.Web.Utils.ReportViewFilterParameter["Pay_Adv_Number"].ToString() + "'";
                }

            }
            return sqlQuery;
        }

        public static string GetEmpBasedPayrollId(string payrollId)
        {
            string sqlQuery = string.Empty;
            sqlQuery += "   select pp.emp_id,ee.emp_email from pay_final_run_emp_dtl pp,hr_employees ee where pp.emp_id=ee.emp_id and pp.PAYROLL_PERIOD='" + payrollId + "' order by pp.emp_id ";
            return sqlQuery;
        }


        public static string getEmpTot(string empId)
        {
            string sqlQuery = string.Empty;
            sqlQuery += "     select nvl(earn_amount - deduct_amt,0) as tot_amt";
            sqlQuery += "    from (SELECT        ";
            sqlQuery += "          sum(RDD.PAY_AMOUNT) as earn_amount        ";
            sqlQuery += "          FROM PAY_FINAL_RUN_HDR     RH,";
            sqlQuery += "               PAY_FINAL_RUN_DTL     RD,";
            sqlQuery += "               PAY_FINAL_RUN_DTL_DTL RDD,";
            sqlQuery += "              PAY_ELEMENTS          PE,";
            sqlQuery += "              PAY_PERIODS           PP";
            sqlQuery += "        WHERE RH.PAYROLL_ID = RD.PAYROLL_ID";
            sqlQuery += "      AND RD.PAYROLL_DTL_ID = RDD.PAYROLL_DTL_ID";
            sqlQuery += "      AND RDD.PAY_EMP_ELEMENT_ID = PE.PAY_ELEMENT_ID";
            sqlQuery += "      AND RH.PAYROLL_PERIOD = PP.PAY_PERIOD_ID";
            sqlQuery += "      and rdd.pay_emp_id = '" + empId + "'";
            sqlQuery += "      and lower(pe.pay_ele_class) = lower('Earning')),";
            sqlQuery += "    (SELECT        ";
            sqlQuery += "      sum(RDD.PAY_AMOUNT) as deduct_amt     ";
            sqlQuery += "      FROM PAY_FINAL_RUN_HDR     RH,";
            sqlQuery += "        PAY_FINAL_RUN_DTL     RD,";
            sqlQuery += "      PAY_FINAL_RUN_DTL_DTL RDD,";
            sqlQuery += "     PAY_ELEMENTS          PE,";
            sqlQuery += "      PAY_PERIODS           PP";
            sqlQuery += "     WHERE RH.PAYROLL_ID = RD.PAYROLL_ID";
            sqlQuery += "     AND RD.PAYROLL_DTL_ID = RDD.PAYROLL_DTL_ID";
            sqlQuery += "      AND RDD.PAY_EMP_ELEMENT_ID = PE.PAY_ELEMENT_ID";
            sqlQuery += "      AND RH.PAYROLL_PERIOD = PP.PAY_PERIOD_ID";
            sqlQuery += "     and rdd.pay_emp_id ='" + empId + "'";
            sqlQuery += "     and lower(pe.pay_ele_class) = lower('Deduction'))";
            return sqlQuery;
        }

    }
}
