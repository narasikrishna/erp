﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.PER
{
    public class PayrollElementCalculation_DAL
    {
        static string sqlQuery = "";
        public static string getPayGroup()
        {
            sqlQuery = string.Empty;

            sqlQuery = " select PG.PAY_GROUP_ID,PG.PAY_GROUP_CODE" + VMVServices.Web.Utils.LanguageCode + "|| ' - ' ||PAY_GROUP_DESC as PAY_GROUP,PG.PAY_GROUP_DESC ";
            sqlQuery += " from PAY_GROUP_DTLS PG  ";
            sqlQuery += " where PG.ENABLED_FLAG =1";
            sqlQuery += " and PG.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " ORDER BY PG.PAY_GROUP_CODE ";

            return sqlQuery;

        }

       
        public static string GetDesignationName(string deptID)
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT HDD.DEPT_DESIG_ID AS DESIGN_ID,HDD.DEPT_DESIG_ID AS DESIGN_ID  ";
            sqlQuery += " ,HDD.DEPT_DESIG_ID ||' - '|| HDD.DESIG_NAME AS DESIG_NAME";
            sqlQuery += " FROM HR_DEPT_DESIGNATIONS HDD ";
            sqlQuery += " WHERE HDD.ENABLED_FLAG = 1 ";
            sqlQuery += " AND HDD.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " AND HDD.DEPT_ID = '" + deptID + "'";
            // sqlQuery += " AND HDD.ORG_ID = '" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " ORDER BY HDD.DEPT_DESIG_ID ";

            return sqlQuery;
        }

        public static string GetElementName(string Group_id)
        {
            sqlQuery = string.Empty;
            sqlQuery = " select distinct PE.PAY_ELEMENT_ID,PE.PAY_ELEMENT_CODE ||' - '||  PAY_ELEMENT_DESC as PAY_ELEMENT ";
            sqlQuery += " from PAY_GROUP_ELEMENT_MAPPING PEM,PAY_ELEMENTS PE ";
            sqlQuery += " where PEM.PAY_ELEMENT_ID = PE.PAY_ELEMENT_ID ";
            sqlQuery += " and PEM.ENABLED_FLAG = 1 ";
            sqlQuery += " AND PEM.WORKFLOW_COMPLETION_STATUS = 1 ";
            // sqlQuery += " and INSTR(PE.PAY_ELEMENT_CODE,'$') = 1 ";
            sqlQuery += " and PEM.PAY_GROUP_ID ='" + Group_id + "'";
            // sqlQuery += " AND HDD.ORG_ID = '" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " ORDER BY PE.PAY_ELEMENT_ID ";

            return sqlQuery;
        }

        public static string GetElementName_notinelement(string Group_id, string PAY_ELEMENT_ID)
        {
            sqlQuery = string.Empty;
            sqlQuery = " select distinct PE.PAY_ELEMENT_ID,PE.PAY_ELEMENT_CODE ||' - '||  PE.PAY_ELEMENT_DESC as PAY_ELEMENT ";
            sqlQuery += " from PAY_GROUP_ELEMENT_MAPPING PEM,PAY_ELEMENTS PE ";
            sqlQuery += " where PEM.PAY_ELEMENT_ID = PE.PAY_ELEMENT_ID ";
            sqlQuery += " and PEM.ENABLED_FLAG = 1 ";
            sqlQuery += " AND PEM.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " AND PE.PAY_ELEMENT_ID <> '" + PAY_ELEMENT_ID + "'";
            // sqlQuery += " and INSTR(PE.PAY_ELEMENT_CODE,'$') = 1 ";
            sqlQuery += " and PEM.PAY_GROUP_ID ='" + Group_id + "'";
            // sqlQuery += " AND HDD.ORG_ID = '" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " ORDER BY PE.PAY_ELEMENT_ID ";

            return sqlQuery;
        }


        public static string GetElment_notinelment(string PAY_ELEMENT_ID)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select distinct PE.PAY_ELEMENT_ID,PE.PAY_ELEMENT_CODE as PAY_ELEMENT ";
            sqlQuery += " from PAY_GROUP_ELEMENT_MAPPING PEM,PAY_ELEMENTS PE ";
            sqlQuery += " where PEM.PAY_ELEMENT_ID = PE.PAY_ELEMENT_ID ";
            sqlQuery += " and PEM.ENABLED_FLAG = 1 ";
            sqlQuery += " AND PEM.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " AND PE.PAY_ELEMENT_ID <> '" + PAY_ELEMENT_ID + "'";
            // sqlQuery += " AND HDD.ORG_ID = '" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " ORDER BY PE.PAY_ELEMENT_ID ";

            return sqlQuery;
        }

        public static string GetGroupName(string Group_id)
        {
            sqlQuery = string.Empty;
            sqlQuery = " select PGD.PAY_GROUP_DESC from PAY_GROUP_DTLS PGD ";
            sqlQuery += " where PGD.PAY_GROUP_ID ='" + Group_id + "'";
            sqlQuery += " and PGD.ENABLED_FLAG = 1 ";
            sqlQuery += " AND PGD.WORKFLOW_COMPLETION_STATUS = 1 ";

            return sqlQuery;
        }

        public static string GetDeptName(string Dept_id)
        {
            sqlQuery = string.Empty;
            sqlQuery = " select HD.DEPT_NAME from HR_DEPARTMENTS HD ";
            sqlQuery += " where HD.DEPT_ID = '" + Dept_id + "'";
            sqlQuery += " and HD.ENABLED_FLAG = 1 ";
            sqlQuery += " AND HD.WORKFLOW_COMPLETION_STATUS = 1 ";

            return sqlQuery;
        }


        public static string GetDesigName(string Design_id)
        {
            sqlQuery = string.Empty;
            sqlQuery = " select HDD.DESIG_NAME from HR_DEPT_DESIGNATIONS HDD ";
            sqlQuery += " where HDD.DEPT_DESIG_ID='" + Design_id + "'";
            sqlQuery += " and HDD.ENABLED_FLAG = 1 ";
            sqlQuery += " AND HDD.WORKFLOW_COMPLETION_STATUS = 1 ";

            return sqlQuery;
        }

        public static string GetElemName(string Elemet_id)
        {
            sqlQuery = string.Empty;
            sqlQuery = " select PE.PAY_ELEMENT_DESC from PAY_ELEMENTS PE ";
            sqlQuery += " where PE.PAY_ELEMENT_ID='" + Elemet_id + "'";
            sqlQuery += " and PE.ENABLED_FLAG = 1 ";
            sqlQuery += " AND PE.WORKFLOW_COMPLETION_STATUS = 1 ";

            return sqlQuery;
        }
    }
}
