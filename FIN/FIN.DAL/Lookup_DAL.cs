﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL
{
    public class Lookup_DAL
    {
        static string sqlQuery = "";
        public static string GetLookUpValues(string typeKeyId)
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT SCM.CODE AS LOOKUP_ID, SCM.DESCRIPTION" + VMVServices.Web.Utils.LanguageCode + " AS LOOKUP_NAME ,nvl(scm.order_no,0) as order_nos ";
            sqlQuery += " FROM SSM_CODE_MASTERS SCM ";
            sqlQuery += " WHERE SCM.PARENT_CODE = '" + typeKeyId + "'";
            sqlQuery += " and scm.enabled_flag=1";
            sqlQuery += " ORDER BY order_nos,SCM.CODE  ";

            return sqlQuery;
        }
        public static string GetGLSourceLookUpValues(string typeKeyId)
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT SCM.CODE AS LOOKUP_ID, SCM.DESCRIPTION" + VMVServices.Web.Utils.LanguageCode + " AS LOOKUP_NAME ";
            sqlQuery += " FROM SSM_CODE_MASTERS SCM ";
            sqlQuery += " WHERE SCM.PARENT_CODE = '" + typeKeyId + "'";
            sqlQuery += " and upper(scm.code) in ('MANUAL', 'RECURRING')";
            sqlQuery += " and scm.enabled_flag=1";
            sqlQuery += " ORDER BY SCM.CODE  ";

            return sqlQuery;
        }
        public static string GetGLJTLookUpValues(string typeKeyId)
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT SCM.CODE AS LOOKUP_ID, SCM.DESCRIPTION" + VMVServices.Web.Utils.LanguageCode + " AS LOOKUP_NAME ";
            sqlQuery += " FROM SSM_CODE_MASTERS SCM ";
            sqlQuery += " WHERE SCM.PARENT_CODE = '" + typeKeyId + "'";
            sqlQuery += " and scm.code in ('Fund transfer', 'Adjustment', 'Accrual', 'Provision','General')";
            sqlQuery += " and scm.enabled_flag=1";
            sqlQuery += " ORDER BY SCM.CODE  ";

            return sqlQuery;
        }

        public static string GetReimbLookUpValues(string typeKeyId)
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT SCM.CODE AS LOOKUP_ID, SCM.DESCRIPTION" + VMVServices.Web.Utils.LanguageCode + " AS LOOKUP_NAME ";
            sqlQuery += " FROM SSM_CODE_MASTERS SCM ";
            sqlQuery += " WHERE SCM.PARENT_CODE = '" + typeKeyId + "'";
            sqlQuery += " and scm.enabled_flag=1";
            sqlQuery += " ORDER BY SCM.ORDER_NO  ";

            return sqlQuery;
        }
    }
}
