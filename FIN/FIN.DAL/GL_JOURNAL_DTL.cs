//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.Serialization;

namespace FIN.DAL
{
    [DataContract(IsReference = true)]
    [KnownType(typeof(GL_ACCT_CODES))]
    [KnownType(typeof(GL_SEGMENT_VALUES))]
    [KnownType(typeof(GL_JOURNAL_HDR))]
    public partial class GL_JOURNAL_DTL: IObjectWithChangeTracker, INotifyPropertyChanged
    {
        #region Primitive Properties
    
        [DataMember]
        public Nullable<int> PK_ID
        {
            get { return _pK_ID; }
            set
            {
                if (_pK_ID != value)
                {
                    _pK_ID = value;
                    OnPropertyChanged("PK_ID");
                }
            }
        }
        private Nullable<int> _pK_ID;
    
        [DataMember]
        public Nullable<int> CHILD_ID
        {
            get { return _cHILD_ID; }
            set
            {
                if (_cHILD_ID != value)
                {
                    _cHILD_ID = value;
                    OnPropertyChanged("CHILD_ID");
                }
            }
        }
        private Nullable<int> _cHILD_ID;
    
        [DataMember]
        public string JE_DTL_ID
        {
            get { return _jE_DTL_ID; }
            set
            {
                if (_jE_DTL_ID != value)
                {
                    if (ChangeTracker.ChangeTrackingEnabled && ChangeTracker.State != ObjectState.Added)
                    {
                        throw new InvalidOperationException("The property 'JE_DTL_ID' is part of the object's key and cannot be changed. Changes to key properties can only be made when the object is not being tracked or is in the Added state.");
                    }
                    _jE_DTL_ID = value;
                    OnPropertyChanged("JE_DTL_ID");
                }
            }
        }
        private string _jE_DTL_ID;
    
        [DataMember]
        public string JE_HDR_ID
        {
            get { return _jE_HDR_ID; }
            set
            {
                if (_jE_HDR_ID != value)
                {
                    ChangeTracker.RecordOriginalValue("JE_HDR_ID", _jE_HDR_ID);
                    if (!IsDeserializing)
                    {
                        if (GL_JOURNAL_HDR != null && GL_JOURNAL_HDR.JE_HDR_ID != value)
                        {
                            GL_JOURNAL_HDR = null;
                        }
                    }
                    _jE_HDR_ID = value;
                    OnPropertyChanged("JE_HDR_ID");
                }
            }
        }
        private string _jE_HDR_ID;
    
        [DataMember]
        public string JE_ACCT_CODE_ID
        {
            get { return _jE_ACCT_CODE_ID; }
            set
            {
                if (_jE_ACCT_CODE_ID != value)
                {
                    ChangeTracker.RecordOriginalValue("JE_ACCT_CODE_ID", _jE_ACCT_CODE_ID);
                    if (!IsDeserializing)
                    {
                        if (GL_ACCT_CODES != null && GL_ACCT_CODES.ACCT_CODE_ID != value)
                        {
                            GL_ACCT_CODES = null;
                        }
                    }
                    _jE_ACCT_CODE_ID = value;
                    OnPropertyChanged("JE_ACCT_CODE_ID");
                }
            }
        }
        private string _jE_ACCT_CODE_ID;
    
        [DataMember]
        public string JE_PARTICULARS
        {
            get { return _jE_PARTICULARS; }
            set
            {
                if (_jE_PARTICULARS != value)
                {
                    _jE_PARTICULARS = value;
                    OnPropertyChanged("JE_PARTICULARS");
                }
            }
        }
        private string _jE_PARTICULARS;
    
        [DataMember]
        public string JE_CREDIT_DEBIT
        {
            get { return _jE_CREDIT_DEBIT; }
            set
            {
                if (_jE_CREDIT_DEBIT != value)
                {
                    _jE_CREDIT_DEBIT = value;
                    OnPropertyChanged("JE_CREDIT_DEBIT");
                }
            }
        }
        private string _jE_CREDIT_DEBIT;
    
        [DataMember]
        public Nullable<decimal> JE_AMOUNT_CR
        {
            get { return _jE_AMOUNT_CR; }
            set
            {
                if (_jE_AMOUNT_CR != value)
                {
                    _jE_AMOUNT_CR = value;
                    OnPropertyChanged("JE_AMOUNT_CR");
                }
            }
        }
        private Nullable<decimal> _jE_AMOUNT_CR;
    
        [DataMember]
        public Nullable<decimal> JE_ACCOUNTED_AMT_CR
        {
            get { return _jE_ACCOUNTED_AMT_CR; }
            set
            {
                if (_jE_ACCOUNTED_AMT_CR != value)
                {
                    _jE_ACCOUNTED_AMT_CR = value;
                    OnPropertyChanged("JE_ACCOUNTED_AMT_CR");
                }
            }
        }
        private Nullable<decimal> _jE_ACCOUNTED_AMT_CR;
    
        [DataMember]
        public string JE_SEGMENT_ID_1
        {
            get { return _jE_SEGMENT_ID_1; }
            set
            {
                if (_jE_SEGMENT_ID_1 != value)
                {
                    ChangeTracker.RecordOriginalValue("JE_SEGMENT_ID_1", _jE_SEGMENT_ID_1);
                    if (!IsDeserializing)
                    {
                        if (GL_SEGMENT_VALUES != null && GL_SEGMENT_VALUES.SEGMENT_VALUE_ID != value)
                        {
                            GL_SEGMENT_VALUES = null;
                        }
                    }
                    _jE_SEGMENT_ID_1 = value;
                    OnPropertyChanged("JE_SEGMENT_ID_1");
                }
            }
        }
        private string _jE_SEGMENT_ID_1;
    
        [DataMember]
        public string JE_SEGMENT_ID_2
        {
            get { return _jE_SEGMENT_ID_2; }
            set
            {
                if (_jE_SEGMENT_ID_2 != value)
                {
                    ChangeTracker.RecordOriginalValue("JE_SEGMENT_ID_2", _jE_SEGMENT_ID_2);
                    if (!IsDeserializing)
                    {
                        if (GL_SEGMENT_VALUES1 != null && GL_SEGMENT_VALUES1.SEGMENT_VALUE_ID != value)
                        {
                            GL_SEGMENT_VALUES1 = null;
                        }
                    }
                    _jE_SEGMENT_ID_2 = value;
                    OnPropertyChanged("JE_SEGMENT_ID_2");
                }
            }
        }
        private string _jE_SEGMENT_ID_2;
    
        [DataMember]
        public string JE_SEGMENT_ID_3
        {
            get { return _jE_SEGMENT_ID_3; }
            set
            {
                if (_jE_SEGMENT_ID_3 != value)
                {
                    ChangeTracker.RecordOriginalValue("JE_SEGMENT_ID_3", _jE_SEGMENT_ID_3);
                    if (!IsDeserializing)
                    {
                        if (GL_SEGMENT_VALUES2 != null && GL_SEGMENT_VALUES2.SEGMENT_VALUE_ID != value)
                        {
                            GL_SEGMENT_VALUES2 = null;
                        }
                    }
                    _jE_SEGMENT_ID_3 = value;
                    OnPropertyChanged("JE_SEGMENT_ID_3");
                }
            }
        }
        private string _jE_SEGMENT_ID_3;
    
        [DataMember]
        public string JE_SEGMENT_ID_4
        {
            get { return _jE_SEGMENT_ID_4; }
            set
            {
                if (_jE_SEGMENT_ID_4 != value)
                {
                    ChangeTracker.RecordOriginalValue("JE_SEGMENT_ID_4", _jE_SEGMENT_ID_4);
                    if (!IsDeserializing)
                    {
                        if (GL_SEGMENT_VALUES3 != null && GL_SEGMENT_VALUES3.SEGMENT_VALUE_ID != value)
                        {
                            GL_SEGMENT_VALUES3 = null;
                        }
                    }
                    _jE_SEGMENT_ID_4 = value;
                    OnPropertyChanged("JE_SEGMENT_ID_4");
                }
            }
        }
        private string _jE_SEGMENT_ID_4;
    
        [DataMember]
        public string JE_SEGMENT_ID_5
        {
            get { return _jE_SEGMENT_ID_5; }
            set
            {
                if (_jE_SEGMENT_ID_5 != value)
                {
                    ChangeTracker.RecordOriginalValue("JE_SEGMENT_ID_5", _jE_SEGMENT_ID_5);
                    if (!IsDeserializing)
                    {
                        if (GL_SEGMENT_VALUES4 != null && GL_SEGMENT_VALUES4.SEGMENT_VALUE_ID != value)
                        {
                            GL_SEGMENT_VALUES4 = null;
                        }
                    }
                    _jE_SEGMENT_ID_5 = value;
                    OnPropertyChanged("JE_SEGMENT_ID_5");
                }
            }
        }
        private string _jE_SEGMENT_ID_5;
    
        [DataMember]
        public string JE_SEGMENT_ID_6
        {
            get { return _jE_SEGMENT_ID_6; }
            set
            {
                if (_jE_SEGMENT_ID_6 != value)
                {
                    ChangeTracker.RecordOriginalValue("JE_SEGMENT_ID_6", _jE_SEGMENT_ID_6);
                    if (!IsDeserializing)
                    {
                        if (GL_SEGMENT_VALUES5 != null && GL_SEGMENT_VALUES5.SEGMENT_VALUE_ID != value)
                        {
                            GL_SEGMENT_VALUES5 = null;
                        }
                    }
                    _jE_SEGMENT_ID_6 = value;
                    OnPropertyChanged("JE_SEGMENT_ID_6");
                }
            }
        }
        private string _jE_SEGMENT_ID_6;
    
        [DataMember]
        public string ENABLED_FLAG
        {
            get { return _eNABLED_FLAG; }
            set
            {
                if (_eNABLED_FLAG != value)
                {
                    _eNABLED_FLAG = value;
                    OnPropertyChanged("ENABLED_FLAG");
                }
            }
        }
        private string _eNABLED_FLAG;
    
        [DataMember]
        public string WORKFLOW_COMPLETION_STATUS
        {
            get { return _wORKFLOW_COMPLETION_STATUS; }
            set
            {
                if (_wORKFLOW_COMPLETION_STATUS != value)
                {
                    _wORKFLOW_COMPLETION_STATUS = value;
                    OnPropertyChanged("WORKFLOW_COMPLETION_STATUS");
                }
            }
        }
        private string _wORKFLOW_COMPLETION_STATUS;
    
        [DataMember]
        public string CREATED_BY
        {
            get { return _cREATED_BY; }
            set
            {
                if (_cREATED_BY != value)
                {
                    _cREATED_BY = value;
                    OnPropertyChanged("CREATED_BY");
                }
            }
        }
        private string _cREATED_BY;
    
        [DataMember]
        public System.DateTime CREATED_DATE
        {
            get { return _cREATED_DATE; }
            set
            {
                if (_cREATED_DATE != value)
                {
                    _cREATED_DATE = value;
                    OnPropertyChanged("CREATED_DATE");
                }
            }
        }
        private System.DateTime _cREATED_DATE;
    
        [DataMember]
        public string MODIFIED_BY
        {
            get { return _mODIFIED_BY; }
            set
            {
                if (_mODIFIED_BY != value)
                {
                    _mODIFIED_BY = value;
                    OnPropertyChanged("MODIFIED_BY");
                }
            }
        }
        private string _mODIFIED_BY;
    
        [DataMember]
        public Nullable<System.DateTime> MODIFIED_DATE
        {
            get { return _mODIFIED_DATE; }
            set
            {
                if (_mODIFIED_DATE != value)
                {
                    _mODIFIED_DATE = value;
                    OnPropertyChanged("MODIFIED_DATE");
                }
            }
        }
        private Nullable<System.DateTime> _mODIFIED_DATE;
    
        [DataMember]
        public string ATTRIBUTE1
        {
            get { return _aTTRIBUTE1; }
            set
            {
                if (_aTTRIBUTE1 != value)
                {
                    _aTTRIBUTE1 = value;
                    OnPropertyChanged("ATTRIBUTE1");
                }
            }
        }
        private string _aTTRIBUTE1;
    
        [DataMember]
        public string ATTRIBUTE2
        {
            get { return _aTTRIBUTE2; }
            set
            {
                if (_aTTRIBUTE2 != value)
                {
                    _aTTRIBUTE2 = value;
                    OnPropertyChanged("ATTRIBUTE2");
                }
            }
        }
        private string _aTTRIBUTE2;
    
        [DataMember]
        public string ATTRIBUTE3
        {
            get { return _aTTRIBUTE3; }
            set
            {
                if (_aTTRIBUTE3 != value)
                {
                    _aTTRIBUTE3 = value;
                    OnPropertyChanged("ATTRIBUTE3");
                }
            }
        }
        private string _aTTRIBUTE3;
    
        [DataMember]
        public string ATTRIBUTE4
        {
            get { return _aTTRIBUTE4; }
            set
            {
                if (_aTTRIBUTE4 != value)
                {
                    _aTTRIBUTE4 = value;
                    OnPropertyChanged("ATTRIBUTE4");
                }
            }
        }
        private string _aTTRIBUTE4;
    
        [DataMember]
        public string ATTRIBUTE5
        {
            get { return _aTTRIBUTE5; }
            set
            {
                if (_aTTRIBUTE5 != value)
                {
                    _aTTRIBUTE5 = value;
                    OnPropertyChanged("ATTRIBUTE5");
                }
            }
        }
        private string _aTTRIBUTE5;
    
        [DataMember]
        public string ATTRIBUTE6
        {
            get { return _aTTRIBUTE6; }
            set
            {
                if (_aTTRIBUTE6 != value)
                {
                    _aTTRIBUTE6 = value;
                    OnPropertyChanged("ATTRIBUTE6");
                }
            }
        }
        private string _aTTRIBUTE6;
    
        [DataMember]
        public string ATTRIBUTE7
        {
            get { return _aTTRIBUTE7; }
            set
            {
                if (_aTTRIBUTE7 != value)
                {
                    _aTTRIBUTE7 = value;
                    OnPropertyChanged("ATTRIBUTE7");
                }
            }
        }
        private string _aTTRIBUTE7;
    
        [DataMember]
        public string ATTRIBUTE8
        {
            get { return _aTTRIBUTE8; }
            set
            {
                if (_aTTRIBUTE8 != value)
                {
                    _aTTRIBUTE8 = value;
                    OnPropertyChanged("ATTRIBUTE8");
                }
            }
        }
        private string _aTTRIBUTE8;
    
        [DataMember]
        public string ATTRIBUTE9
        {
            get { return _aTTRIBUTE9; }
            set
            {
                if (_aTTRIBUTE9 != value)
                {
                    _aTTRIBUTE9 = value;
                    OnPropertyChanged("ATTRIBUTE9");
                }
            }
        }
        private string _aTTRIBUTE9;
    
        [DataMember]
        public string ATTRIBUTE10
        {
            get { return _aTTRIBUTE10; }
            set
            {
                if (_aTTRIBUTE10 != value)
                {
                    _aTTRIBUTE10 = value;
                    OnPropertyChanged("ATTRIBUTE10");
                }
            }
        }
        private string _aTTRIBUTE10;
    
        [DataMember]
        public Nullable<decimal> JE_AMOUNT_DR
        {
            get { return _jE_AMOUNT_DR; }
            set
            {
                if (_jE_AMOUNT_DR != value)
                {
                    _jE_AMOUNT_DR = value;
                    OnPropertyChanged("JE_AMOUNT_DR");
                }
            }
        }
        private Nullable<decimal> _jE_AMOUNT_DR;
    
        [DataMember]
        public Nullable<decimal> JE_ACCOUNTED_AMT_DR
        {
            get { return _jE_ACCOUNTED_AMT_DR; }
            set
            {
                if (_jE_ACCOUNTED_AMT_DR != value)
                {
                    _jE_ACCOUNTED_AMT_DR = value;
                    OnPropertyChanged("JE_ACCOUNTED_AMT_DR");
                }
            }
        }
        private Nullable<decimal> _jE_ACCOUNTED_AMT_DR;

        #endregion
        #region Navigation Properties
    
        [DataMember]
        public GL_ACCT_CODES GL_ACCT_CODES
        {
            get { return _gL_ACCT_CODES; }
            set
            {
                if (!ReferenceEquals(_gL_ACCT_CODES, value))
                {
                    var previousValue = _gL_ACCT_CODES;
                    _gL_ACCT_CODES = value;
                    FixupGL_ACCT_CODES(previousValue);
                    OnNavigationPropertyChanged("GL_ACCT_CODES");
                }
            }
        }
        private GL_ACCT_CODES _gL_ACCT_CODES;
    
        [DataMember]
        public GL_SEGMENT_VALUES GL_SEGMENT_VALUES
        {
            get { return _gL_SEGMENT_VALUES; }
            set
            {
                if (!ReferenceEquals(_gL_SEGMENT_VALUES, value))
                {
                    var previousValue = _gL_SEGMENT_VALUES;
                    _gL_SEGMENT_VALUES = value;
                    FixupGL_SEGMENT_VALUES(previousValue);
                    OnNavigationPropertyChanged("GL_SEGMENT_VALUES");
                }
            }
        }
        private GL_SEGMENT_VALUES _gL_SEGMENT_VALUES;
    
        [DataMember]
        public GL_SEGMENT_VALUES GL_SEGMENT_VALUES1
        {
            get { return _gL_SEGMENT_VALUES1; }
            set
            {
                if (!ReferenceEquals(_gL_SEGMENT_VALUES1, value))
                {
                    var previousValue = _gL_SEGMENT_VALUES1;
                    _gL_SEGMENT_VALUES1 = value;
                    FixupGL_SEGMENT_VALUES1(previousValue);
                    OnNavigationPropertyChanged("GL_SEGMENT_VALUES1");
                }
            }
        }
        private GL_SEGMENT_VALUES _gL_SEGMENT_VALUES1;
    
        [DataMember]
        public GL_SEGMENT_VALUES GL_SEGMENT_VALUES2
        {
            get { return _gL_SEGMENT_VALUES2; }
            set
            {
                if (!ReferenceEquals(_gL_SEGMENT_VALUES2, value))
                {
                    var previousValue = _gL_SEGMENT_VALUES2;
                    _gL_SEGMENT_VALUES2 = value;
                    FixupGL_SEGMENT_VALUES2(previousValue);
                    OnNavigationPropertyChanged("GL_SEGMENT_VALUES2");
                }
            }
        }
        private GL_SEGMENT_VALUES _gL_SEGMENT_VALUES2;
    
        [DataMember]
        public GL_JOURNAL_HDR GL_JOURNAL_HDR
        {
            get { return _gL_JOURNAL_HDR; }
            set
            {
                if (!ReferenceEquals(_gL_JOURNAL_HDR, value))
                {
                    var previousValue = _gL_JOURNAL_HDR;
                    _gL_JOURNAL_HDR = value;
                    FixupGL_JOURNAL_HDR(previousValue);
                    OnNavigationPropertyChanged("GL_JOURNAL_HDR");
                }
            }
        }
        private GL_JOURNAL_HDR _gL_JOURNAL_HDR;
    
        [DataMember]
        public GL_SEGMENT_VALUES GL_SEGMENT_VALUES3
        {
            get { return _gL_SEGMENT_VALUES3; }
            set
            {
                if (!ReferenceEquals(_gL_SEGMENT_VALUES3, value))
                {
                    var previousValue = _gL_SEGMENT_VALUES3;
                    _gL_SEGMENT_VALUES3 = value;
                    FixupGL_SEGMENT_VALUES3(previousValue);
                    OnNavigationPropertyChanged("GL_SEGMENT_VALUES3");
                }
            }
        }
        private GL_SEGMENT_VALUES _gL_SEGMENT_VALUES3;
    
        [DataMember]
        public GL_SEGMENT_VALUES GL_SEGMENT_VALUES4
        {
            get { return _gL_SEGMENT_VALUES4; }
            set
            {
                if (!ReferenceEquals(_gL_SEGMENT_VALUES4, value))
                {
                    var previousValue = _gL_SEGMENT_VALUES4;
                    _gL_SEGMENT_VALUES4 = value;
                    FixupGL_SEGMENT_VALUES4(previousValue);
                    OnNavigationPropertyChanged("GL_SEGMENT_VALUES4");
                }
            }
        }
        private GL_SEGMENT_VALUES _gL_SEGMENT_VALUES4;
    
        [DataMember]
        public GL_SEGMENT_VALUES GL_SEGMENT_VALUES5
        {
            get { return _gL_SEGMENT_VALUES5; }
            set
            {
                if (!ReferenceEquals(_gL_SEGMENT_VALUES5, value))
                {
                    var previousValue = _gL_SEGMENT_VALUES5;
                    _gL_SEGMENT_VALUES5 = value;
                    FixupGL_SEGMENT_VALUES5(previousValue);
                    OnNavigationPropertyChanged("GL_SEGMENT_VALUES5");
                }
            }
        }
        private GL_SEGMENT_VALUES _gL_SEGMENT_VALUES5;

        #endregion
        #region ChangeTracking
    
        protected virtual void OnPropertyChanged(String propertyName)
        {
            if (ChangeTracker.State != ObjectState.Added && ChangeTracker.State != ObjectState.Deleted)
            {
                ChangeTracker.State = ObjectState.Modified;
            }
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        protected virtual void OnNavigationPropertyChanged(String propertyName)
        {
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        event PropertyChangedEventHandler INotifyPropertyChanged.PropertyChanged{ add { _propertyChanged += value; } remove { _propertyChanged -= value; } }
        private event PropertyChangedEventHandler _propertyChanged;
        private ObjectChangeTracker _changeTracker;
    
        [DataMember]
        public ObjectChangeTracker ChangeTracker
        {
            get
            {
                if (_changeTracker == null)
                {
                    _changeTracker = new ObjectChangeTracker();
                    _changeTracker.ObjectStateChanging += HandleObjectStateChanging;
                }
                return _changeTracker;
            }
            set
            {
                if(_changeTracker != null)
                {
                    _changeTracker.ObjectStateChanging -= HandleObjectStateChanging;
                }
                _changeTracker = value;
                if(_changeTracker != null)
                {
                    _changeTracker.ObjectStateChanging += HandleObjectStateChanging;
                }
            }
        }
    
        private void HandleObjectStateChanging(object sender, ObjectStateChangingEventArgs e)
        {
            if (e.NewState == ObjectState.Deleted)
            {
                ClearNavigationProperties();
            }
        }
    
        protected bool IsDeserializing { get; private set; }
    
        [OnDeserializing]
        public void OnDeserializingMethod(StreamingContext context)
        {
            IsDeserializing = true;
        }
    
        [OnDeserialized]
        public void OnDeserializedMethod(StreamingContext context)
        {
            IsDeserializing = false;
            ChangeTracker.ChangeTrackingEnabled = true;
        }
    
        protected virtual void ClearNavigationProperties()
        {
            GL_ACCT_CODES = null;
            GL_SEGMENT_VALUES = null;
            GL_SEGMENT_VALUES1 = null;
            GL_SEGMENT_VALUES2 = null;
            GL_JOURNAL_HDR = null;
            GL_SEGMENT_VALUES3 = null;
            GL_SEGMENT_VALUES4 = null;
            GL_SEGMENT_VALUES5 = null;
        }

        #endregion
        #region Association Fixup
    
        private void FixupGL_ACCT_CODES(GL_ACCT_CODES previousValue, bool skipKeys = false)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (previousValue != null && previousValue.GL_JOURNAL_DTL.Contains(this))
            {
                previousValue.GL_JOURNAL_DTL.Remove(this);
            }
    
            if (GL_ACCT_CODES != null)
            {
                if (!GL_ACCT_CODES.GL_JOURNAL_DTL.Contains(this))
                {
                    GL_ACCT_CODES.GL_JOURNAL_DTL.Add(this);
                }
    
                JE_ACCT_CODE_ID = GL_ACCT_CODES.ACCT_CODE_ID;
            }
            else if (!skipKeys)
            {
                JE_ACCT_CODE_ID = null;
            }
    
            if (ChangeTracker.ChangeTrackingEnabled)
            {
                if (ChangeTracker.OriginalValues.ContainsKey("GL_ACCT_CODES")
                    && (ChangeTracker.OriginalValues["GL_ACCT_CODES"] == GL_ACCT_CODES))
                {
                    ChangeTracker.OriginalValues.Remove("GL_ACCT_CODES");
                }
                else
                {
                    ChangeTracker.RecordOriginalValue("GL_ACCT_CODES", previousValue);
                }
                if (GL_ACCT_CODES != null && !GL_ACCT_CODES.ChangeTracker.ChangeTrackingEnabled)
                {
                    GL_ACCT_CODES.StartTracking();
                }
            }
        }
    
        private void FixupGL_SEGMENT_VALUES(GL_SEGMENT_VALUES previousValue, bool skipKeys = false)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (previousValue != null && previousValue.GL_JOURNAL_DTL.Contains(this))
            {
                previousValue.GL_JOURNAL_DTL.Remove(this);
            }
    
            if (GL_SEGMENT_VALUES != null)
            {
                if (!GL_SEGMENT_VALUES.GL_JOURNAL_DTL.Contains(this))
                {
                    GL_SEGMENT_VALUES.GL_JOURNAL_DTL.Add(this);
                }
    
                JE_SEGMENT_ID_1 = GL_SEGMENT_VALUES.SEGMENT_VALUE_ID;
            }
            else if (!skipKeys)
            {
                JE_SEGMENT_ID_1 = null;
            }
    
            if (ChangeTracker.ChangeTrackingEnabled)
            {
                if (ChangeTracker.OriginalValues.ContainsKey("GL_SEGMENT_VALUES")
                    && (ChangeTracker.OriginalValues["GL_SEGMENT_VALUES"] == GL_SEGMENT_VALUES))
                {
                    ChangeTracker.OriginalValues.Remove("GL_SEGMENT_VALUES");
                }
                else
                {
                    ChangeTracker.RecordOriginalValue("GL_SEGMENT_VALUES", previousValue);
                }
                if (GL_SEGMENT_VALUES != null && !GL_SEGMENT_VALUES.ChangeTracker.ChangeTrackingEnabled)
                {
                    GL_SEGMENT_VALUES.StartTracking();
                }
            }
        }
    
        private void FixupGL_SEGMENT_VALUES1(GL_SEGMENT_VALUES previousValue, bool skipKeys = false)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (previousValue != null && previousValue.GL_JOURNAL_DTL1.Contains(this))
            {
                previousValue.GL_JOURNAL_DTL1.Remove(this);
            }
    
            if (GL_SEGMENT_VALUES1 != null)
            {
                if (!GL_SEGMENT_VALUES1.GL_JOURNAL_DTL1.Contains(this))
                {
                    GL_SEGMENT_VALUES1.GL_JOURNAL_DTL1.Add(this);
                }
    
                JE_SEGMENT_ID_2 = GL_SEGMENT_VALUES1.SEGMENT_VALUE_ID;
            }
            else if (!skipKeys)
            {
                JE_SEGMENT_ID_2 = null;
            }
    
            if (ChangeTracker.ChangeTrackingEnabled)
            {
                if (ChangeTracker.OriginalValues.ContainsKey("GL_SEGMENT_VALUES1")
                    && (ChangeTracker.OriginalValues["GL_SEGMENT_VALUES1"] == GL_SEGMENT_VALUES1))
                {
                    ChangeTracker.OriginalValues.Remove("GL_SEGMENT_VALUES1");
                }
                else
                {
                    ChangeTracker.RecordOriginalValue("GL_SEGMENT_VALUES1", previousValue);
                }
                if (GL_SEGMENT_VALUES1 != null && !GL_SEGMENT_VALUES1.ChangeTracker.ChangeTrackingEnabled)
                {
                    GL_SEGMENT_VALUES1.StartTracking();
                }
            }
        }
    
        private void FixupGL_SEGMENT_VALUES2(GL_SEGMENT_VALUES previousValue, bool skipKeys = false)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (previousValue != null && previousValue.GL_JOURNAL_DTL2.Contains(this))
            {
                previousValue.GL_JOURNAL_DTL2.Remove(this);
            }
    
            if (GL_SEGMENT_VALUES2 != null)
            {
                if (!GL_SEGMENT_VALUES2.GL_JOURNAL_DTL2.Contains(this))
                {
                    GL_SEGMENT_VALUES2.GL_JOURNAL_DTL2.Add(this);
                }
    
                JE_SEGMENT_ID_3 = GL_SEGMENT_VALUES2.SEGMENT_VALUE_ID;
            }
            else if (!skipKeys)
            {
                JE_SEGMENT_ID_3 = null;
            }
    
            if (ChangeTracker.ChangeTrackingEnabled)
            {
                if (ChangeTracker.OriginalValues.ContainsKey("GL_SEGMENT_VALUES2")
                    && (ChangeTracker.OriginalValues["GL_SEGMENT_VALUES2"] == GL_SEGMENT_VALUES2))
                {
                    ChangeTracker.OriginalValues.Remove("GL_SEGMENT_VALUES2");
                }
                else
                {
                    ChangeTracker.RecordOriginalValue("GL_SEGMENT_VALUES2", previousValue);
                }
                if (GL_SEGMENT_VALUES2 != null && !GL_SEGMENT_VALUES2.ChangeTracker.ChangeTrackingEnabled)
                {
                    GL_SEGMENT_VALUES2.StartTracking();
                }
            }
        }
    
        private void FixupGL_JOURNAL_HDR(GL_JOURNAL_HDR previousValue, bool skipKeys = false)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (previousValue != null && previousValue.GL_JOURNAL_DTL.Contains(this))
            {
                previousValue.GL_JOURNAL_DTL.Remove(this);
            }
    
            if (GL_JOURNAL_HDR != null)
            {
                if (!GL_JOURNAL_HDR.GL_JOURNAL_DTL.Contains(this))
                {
                    GL_JOURNAL_HDR.GL_JOURNAL_DTL.Add(this);
                }
    
                JE_HDR_ID = GL_JOURNAL_HDR.JE_HDR_ID;
            }
            else if (!skipKeys)
            {
                JE_HDR_ID = null;
            }
    
            if (ChangeTracker.ChangeTrackingEnabled)
            {
                if (ChangeTracker.OriginalValues.ContainsKey("GL_JOURNAL_HDR")
                    && (ChangeTracker.OriginalValues["GL_JOURNAL_HDR"] == GL_JOURNAL_HDR))
                {
                    ChangeTracker.OriginalValues.Remove("GL_JOURNAL_HDR");
                }
                else
                {
                    ChangeTracker.RecordOriginalValue("GL_JOURNAL_HDR", previousValue);
                }
                if (GL_JOURNAL_HDR != null && !GL_JOURNAL_HDR.ChangeTracker.ChangeTrackingEnabled)
                {
                    GL_JOURNAL_HDR.StartTracking();
                }
            }
        }
    
        private void FixupGL_SEGMENT_VALUES3(GL_SEGMENT_VALUES previousValue, bool skipKeys = false)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (previousValue != null && previousValue.GL_JOURNAL_DTL3.Contains(this))
            {
                previousValue.GL_JOURNAL_DTL3.Remove(this);
            }
    
            if (GL_SEGMENT_VALUES3 != null)
            {
                if (!GL_SEGMENT_VALUES3.GL_JOURNAL_DTL3.Contains(this))
                {
                    GL_SEGMENT_VALUES3.GL_JOURNAL_DTL3.Add(this);
                }
    
                JE_SEGMENT_ID_4 = GL_SEGMENT_VALUES3.SEGMENT_VALUE_ID;
            }
            else if (!skipKeys)
            {
                JE_SEGMENT_ID_4 = null;
            }
    
            if (ChangeTracker.ChangeTrackingEnabled)
            {
                if (ChangeTracker.OriginalValues.ContainsKey("GL_SEGMENT_VALUES3")
                    && (ChangeTracker.OriginalValues["GL_SEGMENT_VALUES3"] == GL_SEGMENT_VALUES3))
                {
                    ChangeTracker.OriginalValues.Remove("GL_SEGMENT_VALUES3");
                }
                else
                {
                    ChangeTracker.RecordOriginalValue("GL_SEGMENT_VALUES3", previousValue);
                }
                if (GL_SEGMENT_VALUES3 != null && !GL_SEGMENT_VALUES3.ChangeTracker.ChangeTrackingEnabled)
                {
                    GL_SEGMENT_VALUES3.StartTracking();
                }
            }
        }
    
        private void FixupGL_SEGMENT_VALUES4(GL_SEGMENT_VALUES previousValue, bool skipKeys = false)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (previousValue != null && previousValue.GL_JOURNAL_DTL4.Contains(this))
            {
                previousValue.GL_JOURNAL_DTL4.Remove(this);
            }
    
            if (GL_SEGMENT_VALUES4 != null)
            {
                if (!GL_SEGMENT_VALUES4.GL_JOURNAL_DTL4.Contains(this))
                {
                    GL_SEGMENT_VALUES4.GL_JOURNAL_DTL4.Add(this);
                }
    
                JE_SEGMENT_ID_5 = GL_SEGMENT_VALUES4.SEGMENT_VALUE_ID;
            }
            else if (!skipKeys)
            {
                JE_SEGMENT_ID_5 = null;
            }
    
            if (ChangeTracker.ChangeTrackingEnabled)
            {
                if (ChangeTracker.OriginalValues.ContainsKey("GL_SEGMENT_VALUES4")
                    && (ChangeTracker.OriginalValues["GL_SEGMENT_VALUES4"] == GL_SEGMENT_VALUES4))
                {
                    ChangeTracker.OriginalValues.Remove("GL_SEGMENT_VALUES4");
                }
                else
                {
                    ChangeTracker.RecordOriginalValue("GL_SEGMENT_VALUES4", previousValue);
                }
                if (GL_SEGMENT_VALUES4 != null && !GL_SEGMENT_VALUES4.ChangeTracker.ChangeTrackingEnabled)
                {
                    GL_SEGMENT_VALUES4.StartTracking();
                }
            }
        }
    
        private void FixupGL_SEGMENT_VALUES5(GL_SEGMENT_VALUES previousValue, bool skipKeys = false)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (previousValue != null && previousValue.GL_JOURNAL_DTL5.Contains(this))
            {
                previousValue.GL_JOURNAL_DTL5.Remove(this);
            }
    
            if (GL_SEGMENT_VALUES5 != null)
            {
                if (!GL_SEGMENT_VALUES5.GL_JOURNAL_DTL5.Contains(this))
                {
                    GL_SEGMENT_VALUES5.GL_JOURNAL_DTL5.Add(this);
                }
    
                JE_SEGMENT_ID_6 = GL_SEGMENT_VALUES5.SEGMENT_VALUE_ID;
            }
            else if (!skipKeys)
            {
                JE_SEGMENT_ID_6 = null;
            }
    
            if (ChangeTracker.ChangeTrackingEnabled)
            {
                if (ChangeTracker.OriginalValues.ContainsKey("GL_SEGMENT_VALUES5")
                    && (ChangeTracker.OriginalValues["GL_SEGMENT_VALUES5"] == GL_SEGMENT_VALUES5))
                {
                    ChangeTracker.OriginalValues.Remove("GL_SEGMENT_VALUES5");
                }
                else
                {
                    ChangeTracker.RecordOriginalValue("GL_SEGMENT_VALUES5", previousValue);
                }
                if (GL_SEGMENT_VALUES5 != null && !GL_SEGMENT_VALUES5.ChangeTracker.ChangeTrackingEnabled)
                {
                    GL_SEGMENT_VALUES5.StartTracking();
                }
            }
        }

        #endregion
    }
}
