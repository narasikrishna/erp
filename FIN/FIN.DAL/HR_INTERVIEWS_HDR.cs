//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.Serialization;

namespace FIN.DAL
{
    [DataContract(IsReference = true)]
    [KnownType(typeof(HR_APPLICANTS))]
    [KnownType(typeof(HR_INTERVIEWS_DTL))]
    [KnownType(typeof(HR_INTERVIEWS_DTL_CRIT))]
    [KnownType(typeof(HR_VACANCIES))]
    [KnownType(typeof(HR_INTERVIEWS_RESULTS))]
    public partial class HR_INTERVIEWS_HDR: IObjectWithChangeTracker, INotifyPropertyChanged
    {
        #region Primitive Properties
    
        [DataMember]
        public Nullable<int> PK_ID
        {
            get { return _pK_ID; }
            set
            {
                if (_pK_ID != value)
                {
                    _pK_ID = value;
                    OnPropertyChanged("PK_ID");
                }
            }
        }
        private Nullable<int> _pK_ID;
    
        [DataMember]
        public Nullable<int> CHILD_ID
        {
            get { return _cHILD_ID; }
            set
            {
                if (_cHILD_ID != value)
                {
                    _cHILD_ID = value;
                    OnPropertyChanged("CHILD_ID");
                }
            }
        }
        private Nullable<int> _cHILD_ID;
    
        [DataMember]
        public string INT_ID
        {
            get { return _iNT_ID; }
            set
            {
                if (_iNT_ID != value)
                {
                    if (ChangeTracker.ChangeTrackingEnabled && ChangeTracker.State != ObjectState.Added)
                    {
                        throw new InvalidOperationException("The property 'INT_ID' is part of the object's key and cannot be changed. Changes to key properties can only be made when the object is not being tracked or is in the Added state.");
                    }
                    _iNT_ID = value;
                    OnPropertyChanged("INT_ID");
                }
            }
        }
        private string _iNT_ID;
    
        [DataMember]
        public string VAC_ID
        {
            get { return _vAC_ID; }
            set
            {
                if (_vAC_ID != value)
                {
                    ChangeTracker.RecordOriginalValue("VAC_ID", _vAC_ID);
                    if (!IsDeserializing)
                    {
                        if (HR_VACANCIES != null && HR_VACANCIES.VAC_ID != value)
                        {
                            HR_VACANCIES = null;
                        }
                    }
                    _vAC_ID = value;
                    OnPropertyChanged("VAC_ID");
                }
            }
        }
        private string _vAC_ID;
    
        [DataMember]
        public string APP_ID
        {
            get { return _aPP_ID; }
            set
            {
                if (_aPP_ID != value)
                {
                    ChangeTracker.RecordOriginalValue("APP_ID", _aPP_ID);
                    if (!IsDeserializing)
                    {
                        if (HR_APPLICANTS != null && HR_APPLICANTS.APP_ID != value)
                        {
                            HR_APPLICANTS = null;
                        }
                    }
                    _aPP_ID = value;
                    OnPropertyChanged("APP_ID");
                }
            }
        }
        private string _aPP_ID;
    
        [DataMember]
        public string ENABLED_FLAG
        {
            get { return _eNABLED_FLAG; }
            set
            {
                if (_eNABLED_FLAG != value)
                {
                    _eNABLED_FLAG = value;
                    OnPropertyChanged("ENABLED_FLAG");
                }
            }
        }
        private string _eNABLED_FLAG;
    
        [DataMember]
        public string WORKFLOW_COMPLETION_STATUS
        {
            get { return _wORKFLOW_COMPLETION_STATUS; }
            set
            {
                if (_wORKFLOW_COMPLETION_STATUS != value)
                {
                    _wORKFLOW_COMPLETION_STATUS = value;
                    OnPropertyChanged("WORKFLOW_COMPLETION_STATUS");
                }
            }
        }
        private string _wORKFLOW_COMPLETION_STATUS;
    
        [DataMember]
        public string CREATED_BY
        {
            get { return _cREATED_BY; }
            set
            {
                if (_cREATED_BY != value)
                {
                    _cREATED_BY = value;
                    OnPropertyChanged("CREATED_BY");
                }
            }
        }
        private string _cREATED_BY;
    
        [DataMember]
        public System.DateTime CREATED_DATE
        {
            get { return _cREATED_DATE; }
            set
            {
                if (_cREATED_DATE != value)
                {
                    _cREATED_DATE = value;
                    OnPropertyChanged("CREATED_DATE");
                }
            }
        }
        private System.DateTime _cREATED_DATE;
    
        [DataMember]
        public string MODIFIED_BY
        {
            get { return _mODIFIED_BY; }
            set
            {
                if (_mODIFIED_BY != value)
                {
                    _mODIFIED_BY = value;
                    OnPropertyChanged("MODIFIED_BY");
                }
            }
        }
        private string _mODIFIED_BY;
    
        [DataMember]
        public Nullable<System.DateTime> MODIFIED_DATE
        {
            get { return _mODIFIED_DATE; }
            set
            {
                if (_mODIFIED_DATE != value)
                {
                    _mODIFIED_DATE = value;
                    OnPropertyChanged("MODIFIED_DATE");
                }
            }
        }
        private Nullable<System.DateTime> _mODIFIED_DATE;
    
        [DataMember]
        public string ATTRIBUTE1
        {
            get { return _aTTRIBUTE1; }
            set
            {
                if (_aTTRIBUTE1 != value)
                {
                    _aTTRIBUTE1 = value;
                    OnPropertyChanged("ATTRIBUTE1");
                }
            }
        }
        private string _aTTRIBUTE1;
    
        [DataMember]
        public string ATTRIBUTE2
        {
            get { return _aTTRIBUTE2; }
            set
            {
                if (_aTTRIBUTE2 != value)
                {
                    _aTTRIBUTE2 = value;
                    OnPropertyChanged("ATTRIBUTE2");
                }
            }
        }
        private string _aTTRIBUTE2;
    
        [DataMember]
        public string ATTRIBUTE3
        {
            get { return _aTTRIBUTE3; }
            set
            {
                if (_aTTRIBUTE3 != value)
                {
                    _aTTRIBUTE3 = value;
                    OnPropertyChanged("ATTRIBUTE3");
                }
            }
        }
        private string _aTTRIBUTE3;
    
        [DataMember]
        public string ATTRIBUTE4
        {
            get { return _aTTRIBUTE4; }
            set
            {
                if (_aTTRIBUTE4 != value)
                {
                    _aTTRIBUTE4 = value;
                    OnPropertyChanged("ATTRIBUTE4");
                }
            }
        }
        private string _aTTRIBUTE4;
    
        [DataMember]
        public string ATTRIBUTE5
        {
            get { return _aTTRIBUTE5; }
            set
            {
                if (_aTTRIBUTE5 != value)
                {
                    _aTTRIBUTE5 = value;
                    OnPropertyChanged("ATTRIBUTE5");
                }
            }
        }
        private string _aTTRIBUTE5;
    
        [DataMember]
        public string ATTRIBUTE6
        {
            get { return _aTTRIBUTE6; }
            set
            {
                if (_aTTRIBUTE6 != value)
                {
                    _aTTRIBUTE6 = value;
                    OnPropertyChanged("ATTRIBUTE6");
                }
            }
        }
        private string _aTTRIBUTE6;
    
        [DataMember]
        public string ATTRIBUTE7
        {
            get { return _aTTRIBUTE7; }
            set
            {
                if (_aTTRIBUTE7 != value)
                {
                    _aTTRIBUTE7 = value;
                    OnPropertyChanged("ATTRIBUTE7");
                }
            }
        }
        private string _aTTRIBUTE7;
    
        [DataMember]
        public string ATTRIBUTE8
        {
            get { return _aTTRIBUTE8; }
            set
            {
                if (_aTTRIBUTE8 != value)
                {
                    _aTTRIBUTE8 = value;
                    OnPropertyChanged("ATTRIBUTE8");
                }
            }
        }
        private string _aTTRIBUTE8;
    
        [DataMember]
        public string ATTRIBUTE9
        {
            get { return _aTTRIBUTE9; }
            set
            {
                if (_aTTRIBUTE9 != value)
                {
                    _aTTRIBUTE9 = value;
                    OnPropertyChanged("ATTRIBUTE9");
                }
            }
        }
        private string _aTTRIBUTE9;
    
        [DataMember]
        public string ATTRIBUTE10
        {
            get { return _aTTRIBUTE10; }
            set
            {
                if (_aTTRIBUTE10 != value)
                {
                    _aTTRIBUTE10 = value;
                    OnPropertyChanged("ATTRIBUTE10");
                }
            }
        }
        private string _aTTRIBUTE10;
    
        [DataMember]
        public string INT_DESC
        {
            get { return _iNT_DESC; }
            set
            {
                if (_iNT_DESC != value)
                {
                    _iNT_DESC = value;
                    OnPropertyChanged("INT_DESC");
                }
            }
        }
        private string _iNT_DESC;
    
        [DataMember]
        public string INT_ORG_ID
        {
            get { return _iNT_ORG_ID; }
            set
            {
                if (_iNT_ORG_ID != value)
                {
                    _iNT_ORG_ID = value;
                    OnPropertyChanged("INT_ORG_ID");
                }
            }
        }
        private string _iNT_ORG_ID;

        #endregion
        #region Navigation Properties
    
        [DataMember]
        public HR_APPLICANTS HR_APPLICANTS
        {
            get { return _hR_APPLICANTS; }
            set
            {
                if (!ReferenceEquals(_hR_APPLICANTS, value))
                {
                    var previousValue = _hR_APPLICANTS;
                    _hR_APPLICANTS = value;
                    FixupHR_APPLICANTS(previousValue);
                    OnNavigationPropertyChanged("HR_APPLICANTS");
                }
            }
        }
        private HR_APPLICANTS _hR_APPLICANTS;
    
        [DataMember]
        public TrackableCollection<HR_INTERVIEWS_DTL> HR_INTERVIEWS_DTL
        {
            get
            {
                if (_hR_INTERVIEWS_DTL == null)
                {
                    _hR_INTERVIEWS_DTL = new TrackableCollection<HR_INTERVIEWS_DTL>();
                    _hR_INTERVIEWS_DTL.CollectionChanged += FixupHR_INTERVIEWS_DTL;
                }
                return _hR_INTERVIEWS_DTL;
            }
            set
            {
                if (!ReferenceEquals(_hR_INTERVIEWS_DTL, value))
                {
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        throw new InvalidOperationException("Cannot set the FixupChangeTrackingCollection when ChangeTracking is enabled");
                    }
                    if (_hR_INTERVIEWS_DTL != null)
                    {
                        _hR_INTERVIEWS_DTL.CollectionChanged -= FixupHR_INTERVIEWS_DTL;
                    }
                    _hR_INTERVIEWS_DTL = value;
                    if (_hR_INTERVIEWS_DTL != null)
                    {
                        _hR_INTERVIEWS_DTL.CollectionChanged += FixupHR_INTERVIEWS_DTL;
                    }
                    OnNavigationPropertyChanged("HR_INTERVIEWS_DTL");
                }
            }
        }
        private TrackableCollection<HR_INTERVIEWS_DTL> _hR_INTERVIEWS_DTL;
    
        [DataMember]
        public TrackableCollection<HR_INTERVIEWS_DTL_CRIT> HR_INTERVIEWS_DTL_CRIT
        {
            get
            {
                if (_hR_INTERVIEWS_DTL_CRIT == null)
                {
                    _hR_INTERVIEWS_DTL_CRIT = new TrackableCollection<HR_INTERVIEWS_DTL_CRIT>();
                    _hR_INTERVIEWS_DTL_CRIT.CollectionChanged += FixupHR_INTERVIEWS_DTL_CRIT;
                }
                return _hR_INTERVIEWS_DTL_CRIT;
            }
            set
            {
                if (!ReferenceEquals(_hR_INTERVIEWS_DTL_CRIT, value))
                {
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        throw new InvalidOperationException("Cannot set the FixupChangeTrackingCollection when ChangeTracking is enabled");
                    }
                    if (_hR_INTERVIEWS_DTL_CRIT != null)
                    {
                        _hR_INTERVIEWS_DTL_CRIT.CollectionChanged -= FixupHR_INTERVIEWS_DTL_CRIT;
                    }
                    _hR_INTERVIEWS_DTL_CRIT = value;
                    if (_hR_INTERVIEWS_DTL_CRIT != null)
                    {
                        _hR_INTERVIEWS_DTL_CRIT.CollectionChanged += FixupHR_INTERVIEWS_DTL_CRIT;
                    }
                    OnNavigationPropertyChanged("HR_INTERVIEWS_DTL_CRIT");
                }
            }
        }
        private TrackableCollection<HR_INTERVIEWS_DTL_CRIT> _hR_INTERVIEWS_DTL_CRIT;
    
        [DataMember]
        public HR_VACANCIES HR_VACANCIES
        {
            get { return _hR_VACANCIES; }
            set
            {
                if (!ReferenceEquals(_hR_VACANCIES, value))
                {
                    var previousValue = _hR_VACANCIES;
                    _hR_VACANCIES = value;
                    FixupHR_VACANCIES(previousValue);
                    OnNavigationPropertyChanged("HR_VACANCIES");
                }
            }
        }
        private HR_VACANCIES _hR_VACANCIES;
    
        [DataMember]
        public TrackableCollection<HR_INTERVIEWS_RESULTS> HR_INTERVIEWS_RESULTS
        {
            get
            {
                if (_hR_INTERVIEWS_RESULTS == null)
                {
                    _hR_INTERVIEWS_RESULTS = new TrackableCollection<HR_INTERVIEWS_RESULTS>();
                    _hR_INTERVIEWS_RESULTS.CollectionChanged += FixupHR_INTERVIEWS_RESULTS;
                }
                return _hR_INTERVIEWS_RESULTS;
            }
            set
            {
                if (!ReferenceEquals(_hR_INTERVIEWS_RESULTS, value))
                {
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        throw new InvalidOperationException("Cannot set the FixupChangeTrackingCollection when ChangeTracking is enabled");
                    }
                    if (_hR_INTERVIEWS_RESULTS != null)
                    {
                        _hR_INTERVIEWS_RESULTS.CollectionChanged -= FixupHR_INTERVIEWS_RESULTS;
                    }
                    _hR_INTERVIEWS_RESULTS = value;
                    if (_hR_INTERVIEWS_RESULTS != null)
                    {
                        _hR_INTERVIEWS_RESULTS.CollectionChanged += FixupHR_INTERVIEWS_RESULTS;
                    }
                    OnNavigationPropertyChanged("HR_INTERVIEWS_RESULTS");
                }
            }
        }
        private TrackableCollection<HR_INTERVIEWS_RESULTS> _hR_INTERVIEWS_RESULTS;

        #endregion
        #region ChangeTracking
    
        protected virtual void OnPropertyChanged(String propertyName)
        {
            if (ChangeTracker.State != ObjectState.Added && ChangeTracker.State != ObjectState.Deleted)
            {
                ChangeTracker.State = ObjectState.Modified;
            }
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        protected virtual void OnNavigationPropertyChanged(String propertyName)
        {
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        event PropertyChangedEventHandler INotifyPropertyChanged.PropertyChanged{ add { _propertyChanged += value; } remove { _propertyChanged -= value; } }
        private event PropertyChangedEventHandler _propertyChanged;
        private ObjectChangeTracker _changeTracker;
    
        [DataMember]
        public ObjectChangeTracker ChangeTracker
        {
            get
            {
                if (_changeTracker == null)
                {
                    _changeTracker = new ObjectChangeTracker();
                    _changeTracker.ObjectStateChanging += HandleObjectStateChanging;
                }
                return _changeTracker;
            }
            set
            {
                if(_changeTracker != null)
                {
                    _changeTracker.ObjectStateChanging -= HandleObjectStateChanging;
                }
                _changeTracker = value;
                if(_changeTracker != null)
                {
                    _changeTracker.ObjectStateChanging += HandleObjectStateChanging;
                }
            }
        }
    
        private void HandleObjectStateChanging(object sender, ObjectStateChangingEventArgs e)
        {
            if (e.NewState == ObjectState.Deleted)
            {
                ClearNavigationProperties();
            }
        }
    
        protected bool IsDeserializing { get; private set; }
    
        [OnDeserializing]
        public void OnDeserializingMethod(StreamingContext context)
        {
            IsDeserializing = true;
        }
    
        [OnDeserialized]
        public void OnDeserializedMethod(StreamingContext context)
        {
            IsDeserializing = false;
            ChangeTracker.ChangeTrackingEnabled = true;
        }
    
        protected virtual void ClearNavigationProperties()
        {
            HR_APPLICANTS = null;
            HR_INTERVIEWS_DTL.Clear();
            HR_INTERVIEWS_DTL_CRIT.Clear();
            HR_VACANCIES = null;
            HR_INTERVIEWS_RESULTS.Clear();
        }

        #endregion
        #region Association Fixup
    
        private void FixupHR_APPLICANTS(HR_APPLICANTS previousValue, bool skipKeys = false)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (previousValue != null && previousValue.HR_INTERVIEWS_HDR.Contains(this))
            {
                previousValue.HR_INTERVIEWS_HDR.Remove(this);
            }
    
            if (HR_APPLICANTS != null)
            {
                if (!HR_APPLICANTS.HR_INTERVIEWS_HDR.Contains(this))
                {
                    HR_APPLICANTS.HR_INTERVIEWS_HDR.Add(this);
                }
    
                APP_ID = HR_APPLICANTS.APP_ID;
            }
            else if (!skipKeys)
            {
                APP_ID = null;
            }
    
            if (ChangeTracker.ChangeTrackingEnabled)
            {
                if (ChangeTracker.OriginalValues.ContainsKey("HR_APPLICANTS")
                    && (ChangeTracker.OriginalValues["HR_APPLICANTS"] == HR_APPLICANTS))
                {
                    ChangeTracker.OriginalValues.Remove("HR_APPLICANTS");
                }
                else
                {
                    ChangeTracker.RecordOriginalValue("HR_APPLICANTS", previousValue);
                }
                if (HR_APPLICANTS != null && !HR_APPLICANTS.ChangeTracker.ChangeTrackingEnabled)
                {
                    HR_APPLICANTS.StartTracking();
                }
            }
        }
    
        private void FixupHR_VACANCIES(HR_VACANCIES previousValue, bool skipKeys = false)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (previousValue != null && previousValue.HR_INTERVIEWS_HDR.Contains(this))
            {
                previousValue.HR_INTERVIEWS_HDR.Remove(this);
            }
    
            if (HR_VACANCIES != null)
            {
                if (!HR_VACANCIES.HR_INTERVIEWS_HDR.Contains(this))
                {
                    HR_VACANCIES.HR_INTERVIEWS_HDR.Add(this);
                }
    
                VAC_ID = HR_VACANCIES.VAC_ID;
            }
            else if (!skipKeys)
            {
                VAC_ID = null;
            }
    
            if (ChangeTracker.ChangeTrackingEnabled)
            {
                if (ChangeTracker.OriginalValues.ContainsKey("HR_VACANCIES")
                    && (ChangeTracker.OriginalValues["HR_VACANCIES"] == HR_VACANCIES))
                {
                    ChangeTracker.OriginalValues.Remove("HR_VACANCIES");
                }
                else
                {
                    ChangeTracker.RecordOriginalValue("HR_VACANCIES", previousValue);
                }
                if (HR_VACANCIES != null && !HR_VACANCIES.ChangeTracker.ChangeTrackingEnabled)
                {
                    HR_VACANCIES.StartTracking();
                }
            }
        }
    
        private void FixupHR_INTERVIEWS_DTL(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (e.NewItems != null)
            {
                foreach (HR_INTERVIEWS_DTL item in e.NewItems)
                {
                    item.HR_INTERVIEWS_HDR = this;
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        if (!item.ChangeTracker.ChangeTrackingEnabled)
                        {
                            item.StartTracking();
                        }
                        ChangeTracker.RecordAdditionToCollectionProperties("HR_INTERVIEWS_DTL", item);
                    }
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (HR_INTERVIEWS_DTL item in e.OldItems)
                {
                    if (ReferenceEquals(item.HR_INTERVIEWS_HDR, this))
                    {
                        item.HR_INTERVIEWS_HDR = null;
                    }
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        ChangeTracker.RecordRemovalFromCollectionProperties("HR_INTERVIEWS_DTL", item);
                    }
                }
            }
        }
    
        private void FixupHR_INTERVIEWS_DTL_CRIT(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (e.NewItems != null)
            {
                foreach (HR_INTERVIEWS_DTL_CRIT item in e.NewItems)
                {
                    item.HR_INTERVIEWS_HDR = this;
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        if (!item.ChangeTracker.ChangeTrackingEnabled)
                        {
                            item.StartTracking();
                        }
                        ChangeTracker.RecordAdditionToCollectionProperties("HR_INTERVIEWS_DTL_CRIT", item);
                    }
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (HR_INTERVIEWS_DTL_CRIT item in e.OldItems)
                {
                    if (ReferenceEquals(item.HR_INTERVIEWS_HDR, this))
                    {
                        item.HR_INTERVIEWS_HDR = null;
                    }
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        ChangeTracker.RecordRemovalFromCollectionProperties("HR_INTERVIEWS_DTL_CRIT", item);
                    }
                }
            }
        }
    
        private void FixupHR_INTERVIEWS_RESULTS(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (e.NewItems != null)
            {
                foreach (HR_INTERVIEWS_RESULTS item in e.NewItems)
                {
                    item.HR_INTERVIEWS_HDR = this;
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        if (!item.ChangeTracker.ChangeTrackingEnabled)
                        {
                            item.StartTracking();
                        }
                        ChangeTracker.RecordAdditionToCollectionProperties("HR_INTERVIEWS_RESULTS", item);
                    }
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (HR_INTERVIEWS_RESULTS item in e.OldItems)
                {
                    if (ReferenceEquals(item.HR_INTERVIEWS_HDR, this))
                    {
                        item.HR_INTERVIEWS_HDR = null;
                    }
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        ChangeTracker.RecordRemovalFromCollectionProperties("HR_INTERVIEWS_RESULTS", item);
                    }
                }
            }
        }

        #endregion
    }
}
