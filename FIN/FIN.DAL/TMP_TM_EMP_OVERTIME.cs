//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.Serialization;

namespace FIN.DAL
{
    [DataContract(IsReference = true)]
    public partial class TMP_TM_EMP_OVERTIME: IObjectWithChangeTracker, INotifyPropertyChanged
    {
        #region Primitive Properties
    
        [DataMember]
        public System.DateTime OVERTIME_DATE
        {
            get { return _oVERTIME_DATE; }
            set
            {
                if (_oVERTIME_DATE != value)
                {
                    if (ChangeTracker.ChangeTrackingEnabled && ChangeTracker.State != ObjectState.Added)
                    {
                        throw new InvalidOperationException("The property 'OVERTIME_DATE' is part of the object's key and cannot be changed. Changes to key properties can only be made when the object is not being tracked or is in the Added state.");
                    }
                    _oVERTIME_DATE = value;
                    OnPropertyChanged("OVERTIME_DATE");
                }
            }
        }
        private System.DateTime _oVERTIME_DATE;
    
        [DataMember]
        public string OVERTIME_EMP_ID
        {
            get { return _oVERTIME_EMP_ID; }
            set
            {
                if (_oVERTIME_EMP_ID != value)
                {
                    _oVERTIME_EMP_ID = value;
                    OnPropertyChanged("OVERTIME_EMP_ID");
                }
            }
        }
        private string _oVERTIME_EMP_ID;
    
        [DataMember]
        public Nullable<decimal> OVERTIME_HOURS
        {
            get { return _oVERTIME_HOURS; }
            set
            {
                if (_oVERTIME_HOURS != value)
                {
                    _oVERTIME_HOURS = value;
                    OnPropertyChanged("OVERTIME_HOURS");
                }
            }
        }
        private Nullable<decimal> _oVERTIME_HOURS;
    
        [DataMember]
        public string LOGIN_EMP_ID
        {
            get { return _lOGIN_EMP_ID; }
            set
            {
                if (_lOGIN_EMP_ID != value)
                {
                    _lOGIN_EMP_ID = value;
                    OnPropertyChanged("LOGIN_EMP_ID");
                }
            }
        }
        private string _lOGIN_EMP_ID;

        #endregion
        #region ChangeTracking
    
        protected virtual void OnPropertyChanged(String propertyName)
        {
            if (ChangeTracker.State != ObjectState.Added && ChangeTracker.State != ObjectState.Deleted)
            {
                ChangeTracker.State = ObjectState.Modified;
            }
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        protected virtual void OnNavigationPropertyChanged(String propertyName)
        {
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        event PropertyChangedEventHandler INotifyPropertyChanged.PropertyChanged{ add { _propertyChanged += value; } remove { _propertyChanged -= value; } }
        private event PropertyChangedEventHandler _propertyChanged;
        private ObjectChangeTracker _changeTracker;
    
        [DataMember]
        public ObjectChangeTracker ChangeTracker
        {
            get
            {
                if (_changeTracker == null)
                {
                    _changeTracker = new ObjectChangeTracker();
                    _changeTracker.ObjectStateChanging += HandleObjectStateChanging;
                }
                return _changeTracker;
            }
            set
            {
                if(_changeTracker != null)
                {
                    _changeTracker.ObjectStateChanging -= HandleObjectStateChanging;
                }
                _changeTracker = value;
                if(_changeTracker != null)
                {
                    _changeTracker.ObjectStateChanging += HandleObjectStateChanging;
                }
            }
        }
    
        private void HandleObjectStateChanging(object sender, ObjectStateChangingEventArgs e)
        {
            if (e.NewState == ObjectState.Deleted)
            {
                ClearNavigationProperties();
            }
        }
    
        protected bool IsDeserializing { get; private set; }
    
        [OnDeserializing]
        public void OnDeserializingMethod(StreamingContext context)
        {
            IsDeserializing = true;
        }
    
        [OnDeserialized]
        public void OnDeserializedMethod(StreamingContext context)
        {
            IsDeserializing = false;
            ChangeTracker.ChangeTrackingEnabled = true;
        }
    
        protected virtual void ClearNavigationProperties()
        {
        }

        #endregion
    }
}
