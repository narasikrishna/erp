//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.Serialization;

namespace FIN.DAL
{
    [DataContract(IsReference = true)]
    [KnownType(typeof(HR_EMPLOYEES))]
    [KnownType(typeof(HR_TRM_ATTEND_HDR))]
    public partial class HR_TRM_ATTEND_DTL: IObjectWithChangeTracker, INotifyPropertyChanged
    {
        #region Primitive Properties
    
        [DataMember]
        public Nullable<int> PK_ID
        {
            get { return _pK_ID; }
            set
            {
                if (_pK_ID != value)
                {
                    _pK_ID = value;
                    OnPropertyChanged("PK_ID");
                }
            }
        }
        private Nullable<int> _pK_ID;
    
        [DataMember]
        public Nullable<int> CHILD_ID
        {
            get { return _cHILD_ID; }
            set
            {
                if (_cHILD_ID != value)
                {
                    _cHILD_ID = value;
                    OnPropertyChanged("CHILD_ID");
                }
            }
        }
        private Nullable<int> _cHILD_ID;
    
        [DataMember]
        public string ATT_DTL_ID
        {
            get { return _aTT_DTL_ID; }
            set
            {
                if (_aTT_DTL_ID != value)
                {
                    if (ChangeTracker.ChangeTrackingEnabled && ChangeTracker.State != ObjectState.Added)
                    {
                        throw new InvalidOperationException("The property 'ATT_DTL_ID' is part of the object's key and cannot be changed. Changes to key properties can only be made when the object is not being tracked or is in the Added state.");
                    }
                    _aTT_DTL_ID = value;
                    OnPropertyChanged("ATT_DTL_ID");
                }
            }
        }
        private string _aTT_DTL_ID;
    
        [DataMember]
        public string ATT_EMP_ID
        {
            get { return _aTT_EMP_ID; }
            set
            {
                if (_aTT_EMP_ID != value)
                {
                    ChangeTracker.RecordOriginalValue("ATT_EMP_ID", _aTT_EMP_ID);
                    if (!IsDeserializing)
                    {
                        if (HR_EMPLOYEES != null && HR_EMPLOYEES.EMP_ID != value)
                        {
                            HR_EMPLOYEES = null;
                        }
                    }
                    _aTT_EMP_ID = value;
                    OnPropertyChanged("ATT_EMP_ID");
                }
            }
        }
        private string _aTT_EMP_ID;
    
        [DataMember]
        public string ATT_HDR_ID
        {
            get { return _aTT_HDR_ID; }
            set
            {
                if (_aTT_HDR_ID != value)
                {
                    ChangeTracker.RecordOriginalValue("ATT_HDR_ID", _aTT_HDR_ID);
                    if (!IsDeserializing)
                    {
                        if (HR_TRM_ATTEND_HDR != null && HR_TRM_ATTEND_HDR.ATT_HDR_ID != value)
                        {
                            HR_TRM_ATTEND_HDR = null;
                        }
                    }
                    _aTT_HDR_ID = value;
                    OnPropertyChanged("ATT_HDR_ID");
                }
            }
        }
        private string _aTT_HDR_ID;
    
        [DataMember]
        public string ENABLED_FLAG
        {
            get { return _eNABLED_FLAG; }
            set
            {
                if (_eNABLED_FLAG != value)
                {
                    _eNABLED_FLAG = value;
                    OnPropertyChanged("ENABLED_FLAG");
                }
            }
        }
        private string _eNABLED_FLAG;
    
        [DataMember]
        public string WORKFLOW_COMPLETION_STATUS
        {
            get { return _wORKFLOW_COMPLETION_STATUS; }
            set
            {
                if (_wORKFLOW_COMPLETION_STATUS != value)
                {
                    _wORKFLOW_COMPLETION_STATUS = value;
                    OnPropertyChanged("WORKFLOW_COMPLETION_STATUS");
                }
            }
        }
        private string _wORKFLOW_COMPLETION_STATUS;
    
        [DataMember]
        public string CREATED_BY
        {
            get { return _cREATED_BY; }
            set
            {
                if (_cREATED_BY != value)
                {
                    _cREATED_BY = value;
                    OnPropertyChanged("CREATED_BY");
                }
            }
        }
        private string _cREATED_BY;
    
        [DataMember]
        public System.DateTime CREATED_DATE
        {
            get { return _cREATED_DATE; }
            set
            {
                if (_cREATED_DATE != value)
                {
                    _cREATED_DATE = value;
                    OnPropertyChanged("CREATED_DATE");
                }
            }
        }
        private System.DateTime _cREATED_DATE;
    
        [DataMember]
        public string MODIFIED_BY
        {
            get { return _mODIFIED_BY; }
            set
            {
                if (_mODIFIED_BY != value)
                {
                    _mODIFIED_BY = value;
                    OnPropertyChanged("MODIFIED_BY");
                }
            }
        }
        private string _mODIFIED_BY;
    
        [DataMember]
        public Nullable<System.DateTime> MODIFIED_DATE
        {
            get { return _mODIFIED_DATE; }
            set
            {
                if (_mODIFIED_DATE != value)
                {
                    _mODIFIED_DATE = value;
                    OnPropertyChanged("MODIFIED_DATE");
                }
            }
        }
        private Nullable<System.DateTime> _mODIFIED_DATE;
    
        [DataMember]
        public string ATTRIBUTE1
        {
            get { return _aTTRIBUTE1; }
            set
            {
                if (_aTTRIBUTE1 != value)
                {
                    _aTTRIBUTE1 = value;
                    OnPropertyChanged("ATTRIBUTE1");
                }
            }
        }
        private string _aTTRIBUTE1;
    
        [DataMember]
        public string ATTRIBUTE2
        {
            get { return _aTTRIBUTE2; }
            set
            {
                if (_aTTRIBUTE2 != value)
                {
                    _aTTRIBUTE2 = value;
                    OnPropertyChanged("ATTRIBUTE2");
                }
            }
        }
        private string _aTTRIBUTE2;
    
        [DataMember]
        public string ATTRIBUTE3
        {
            get { return _aTTRIBUTE3; }
            set
            {
                if (_aTTRIBUTE3 != value)
                {
                    _aTTRIBUTE3 = value;
                    OnPropertyChanged("ATTRIBUTE3");
                }
            }
        }
        private string _aTTRIBUTE3;
    
        [DataMember]
        public string ATTRIBUTE4
        {
            get { return _aTTRIBUTE4; }
            set
            {
                if (_aTTRIBUTE4 != value)
                {
                    _aTTRIBUTE4 = value;
                    OnPropertyChanged("ATTRIBUTE4");
                }
            }
        }
        private string _aTTRIBUTE4;
    
        [DataMember]
        public string ATTRIBUTE5
        {
            get { return _aTTRIBUTE5; }
            set
            {
                if (_aTTRIBUTE5 != value)
                {
                    _aTTRIBUTE5 = value;
                    OnPropertyChanged("ATTRIBUTE5");
                }
            }
        }
        private string _aTTRIBUTE5;
    
        [DataMember]
        public string ATTRIBUTE6
        {
            get { return _aTTRIBUTE6; }
            set
            {
                if (_aTTRIBUTE6 != value)
                {
                    _aTTRIBUTE6 = value;
                    OnPropertyChanged("ATTRIBUTE6");
                }
            }
        }
        private string _aTTRIBUTE6;
    
        [DataMember]
        public string ATTRIBUTE7
        {
            get { return _aTTRIBUTE7; }
            set
            {
                if (_aTTRIBUTE7 != value)
                {
                    _aTTRIBUTE7 = value;
                    OnPropertyChanged("ATTRIBUTE7");
                }
            }
        }
        private string _aTTRIBUTE7;
    
        [DataMember]
        public string ATTRIBUTE8
        {
            get { return _aTTRIBUTE8; }
            set
            {
                if (_aTTRIBUTE8 != value)
                {
                    _aTTRIBUTE8 = value;
                    OnPropertyChanged("ATTRIBUTE8");
                }
            }
        }
        private string _aTTRIBUTE8;
    
        [DataMember]
        public string ATTRIBUTE9
        {
            get { return _aTTRIBUTE9; }
            set
            {
                if (_aTTRIBUTE9 != value)
                {
                    _aTTRIBUTE9 = value;
                    OnPropertyChanged("ATTRIBUTE9");
                }
            }
        }
        private string _aTTRIBUTE9;
    
        [DataMember]
        public string ATTRIBUTE10
        {
            get { return _aTTRIBUTE10; }
            set
            {
                if (_aTTRIBUTE10 != value)
                {
                    _aTTRIBUTE10 = value;
                    OnPropertyChanged("ATTRIBUTE10");
                }
            }
        }
        private string _aTTRIBUTE10;

        #endregion
        #region Navigation Properties
    
        [DataMember]
        public HR_EMPLOYEES HR_EMPLOYEES
        {
            get { return _hR_EMPLOYEES; }
            set
            {
                if (!ReferenceEquals(_hR_EMPLOYEES, value))
                {
                    var previousValue = _hR_EMPLOYEES;
                    _hR_EMPLOYEES = value;
                    FixupHR_EMPLOYEES(previousValue);
                    OnNavigationPropertyChanged("HR_EMPLOYEES");
                }
            }
        }
        private HR_EMPLOYEES _hR_EMPLOYEES;
    
        [DataMember]
        public HR_TRM_ATTEND_HDR HR_TRM_ATTEND_HDR
        {
            get { return _hR_TRM_ATTEND_HDR; }
            set
            {
                if (!ReferenceEquals(_hR_TRM_ATTEND_HDR, value))
                {
                    var previousValue = _hR_TRM_ATTEND_HDR;
                    _hR_TRM_ATTEND_HDR = value;
                    FixupHR_TRM_ATTEND_HDR(previousValue);
                    OnNavigationPropertyChanged("HR_TRM_ATTEND_HDR");
                }
            }
        }
        private HR_TRM_ATTEND_HDR _hR_TRM_ATTEND_HDR;

        #endregion
        #region ChangeTracking
    
        protected virtual void OnPropertyChanged(String propertyName)
        {
            if (ChangeTracker.State != ObjectState.Added && ChangeTracker.State != ObjectState.Deleted)
            {
                ChangeTracker.State = ObjectState.Modified;
            }
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        protected virtual void OnNavigationPropertyChanged(String propertyName)
        {
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        event PropertyChangedEventHandler INotifyPropertyChanged.PropertyChanged{ add { _propertyChanged += value; } remove { _propertyChanged -= value; } }
        private event PropertyChangedEventHandler _propertyChanged;
        private ObjectChangeTracker _changeTracker;
    
        [DataMember]
        public ObjectChangeTracker ChangeTracker
        {
            get
            {
                if (_changeTracker == null)
                {
                    _changeTracker = new ObjectChangeTracker();
                    _changeTracker.ObjectStateChanging += HandleObjectStateChanging;
                }
                return _changeTracker;
            }
            set
            {
                if(_changeTracker != null)
                {
                    _changeTracker.ObjectStateChanging -= HandleObjectStateChanging;
                }
                _changeTracker = value;
                if(_changeTracker != null)
                {
                    _changeTracker.ObjectStateChanging += HandleObjectStateChanging;
                }
            }
        }
    
        private void HandleObjectStateChanging(object sender, ObjectStateChangingEventArgs e)
        {
            if (e.NewState == ObjectState.Deleted)
            {
                ClearNavigationProperties();
            }
        }
    
        protected bool IsDeserializing { get; private set; }
    
        [OnDeserializing]
        public void OnDeserializingMethod(StreamingContext context)
        {
            IsDeserializing = true;
        }
    
        [OnDeserialized]
        public void OnDeserializedMethod(StreamingContext context)
        {
            IsDeserializing = false;
            ChangeTracker.ChangeTrackingEnabled = true;
        }
    
        protected virtual void ClearNavigationProperties()
        {
            HR_EMPLOYEES = null;
            HR_TRM_ATTEND_HDR = null;
        }

        #endregion
        #region Association Fixup
    
        private void FixupHR_EMPLOYEES(HR_EMPLOYEES previousValue, bool skipKeys = false)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (previousValue != null && previousValue.HR_TRM_ATTEND_DTL.Contains(this))
            {
                previousValue.HR_TRM_ATTEND_DTL.Remove(this);
            }
    
            if (HR_EMPLOYEES != null)
            {
                if (!HR_EMPLOYEES.HR_TRM_ATTEND_DTL.Contains(this))
                {
                    HR_EMPLOYEES.HR_TRM_ATTEND_DTL.Add(this);
                }
    
                ATT_EMP_ID = HR_EMPLOYEES.EMP_ID;
            }
            else if (!skipKeys)
            {
                ATT_EMP_ID = null;
            }
    
            if (ChangeTracker.ChangeTrackingEnabled)
            {
                if (ChangeTracker.OriginalValues.ContainsKey("HR_EMPLOYEES")
                    && (ChangeTracker.OriginalValues["HR_EMPLOYEES"] == HR_EMPLOYEES))
                {
                    ChangeTracker.OriginalValues.Remove("HR_EMPLOYEES");
                }
                else
                {
                    ChangeTracker.RecordOriginalValue("HR_EMPLOYEES", previousValue);
                }
                if (HR_EMPLOYEES != null && !HR_EMPLOYEES.ChangeTracker.ChangeTrackingEnabled)
                {
                    HR_EMPLOYEES.StartTracking();
                }
            }
        }
    
        private void FixupHR_TRM_ATTEND_HDR(HR_TRM_ATTEND_HDR previousValue, bool skipKeys = false)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (previousValue != null && previousValue.HR_TRM_ATTEND_DTL.Contains(this))
            {
                previousValue.HR_TRM_ATTEND_DTL.Remove(this);
            }
    
            if (HR_TRM_ATTEND_HDR != null)
            {
                if (!HR_TRM_ATTEND_HDR.HR_TRM_ATTEND_DTL.Contains(this))
                {
                    HR_TRM_ATTEND_HDR.HR_TRM_ATTEND_DTL.Add(this);
                }
    
                ATT_HDR_ID = HR_TRM_ATTEND_HDR.ATT_HDR_ID;
            }
            else if (!skipKeys)
            {
                ATT_HDR_ID = null;
            }
    
            if (ChangeTracker.ChangeTrackingEnabled)
            {
                if (ChangeTracker.OriginalValues.ContainsKey("HR_TRM_ATTEND_HDR")
                    && (ChangeTracker.OriginalValues["HR_TRM_ATTEND_HDR"] == HR_TRM_ATTEND_HDR))
                {
                    ChangeTracker.OriginalValues.Remove("HR_TRM_ATTEND_HDR");
                }
                else
                {
                    ChangeTracker.RecordOriginalValue("HR_TRM_ATTEND_HDR", previousValue);
                }
                if (HR_TRM_ATTEND_HDR != null && !HR_TRM_ATTEND_HDR.ChangeTracker.ChangeTrackingEnabled)
                {
                    HR_TRM_ATTEND_HDR.StartTracking();
                }
            }
        }

        #endregion
    }
}
