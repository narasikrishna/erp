//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.Serialization;

namespace FIN.DAL
{
    [DataContract(IsReference = true)]
    [KnownType(typeof(MD_CONNECTIONS))]
    public partial class MD_MIGR_PARAMETER: IObjectWithChangeTracker, INotifyPropertyChanged
    {
        #region Primitive Properties
    
        [DataMember]
        public decimal ID
        {
            get { return _iD; }
            set
            {
                if (_iD != value)
                {
                    if (ChangeTracker.ChangeTrackingEnabled && ChangeTracker.State != ObjectState.Added)
                    {
                        throw new InvalidOperationException("The property 'ID' is part of the object's key and cannot be changed. Changes to key properties can only be made when the object is not being tracked or is in the Added state.");
                    }
                    _iD = value;
                    OnPropertyChanged("ID");
                }
            }
        }
        private decimal _iD;
    
        [DataMember]
        public decimal CONNECTION_ID_FK
        {
            get { return _cONNECTION_ID_FK; }
            set
            {
                if (_cONNECTION_ID_FK != value)
                {
                    ChangeTracker.RecordOriginalValue("CONNECTION_ID_FK", _cONNECTION_ID_FK);
                    if (!IsDeserializing)
                    {
                        if (MD_CONNECTIONS != null && MD_CONNECTIONS.ID != value)
                        {
                            MD_CONNECTIONS = null;
                        }
                    }
                    _cONNECTION_ID_FK = value;
                    OnPropertyChanged("CONNECTION_ID_FK");
                }
            }
        }
        private decimal _cONNECTION_ID_FK;
    
        [DataMember]
        public decimal OBJECT_ID
        {
            get { return _oBJECT_ID; }
            set
            {
                if (_oBJECT_ID != value)
                {
                    _oBJECT_ID = value;
                    OnPropertyChanged("OBJECT_ID");
                }
            }
        }
        private decimal _oBJECT_ID;
    
        [DataMember]
        public string OBJECT_TYPE
        {
            get { return _oBJECT_TYPE; }
            set
            {
                if (_oBJECT_TYPE != value)
                {
                    _oBJECT_TYPE = value;
                    OnPropertyChanged("OBJECT_TYPE");
                }
            }
        }
        private string _oBJECT_TYPE;
    
        [DataMember]
        public decimal PARAM_EXISTING
        {
            get { return _pARAM_EXISTING; }
            set
            {
                if (_pARAM_EXISTING != value)
                {
                    _pARAM_EXISTING = value;
                    OnPropertyChanged("PARAM_EXISTING");
                }
            }
        }
        private decimal _pARAM_EXISTING;
    
        [DataMember]
        public decimal PARAM_ORDER
        {
            get { return _pARAM_ORDER; }
            set
            {
                if (_pARAM_ORDER != value)
                {
                    _pARAM_ORDER = value;
                    OnPropertyChanged("PARAM_ORDER");
                }
            }
        }
        private decimal _pARAM_ORDER;
    
        [DataMember]
        public string PARAM_NAME
        {
            get { return _pARAM_NAME; }
            set
            {
                if (_pARAM_NAME != value)
                {
                    _pARAM_NAME = value;
                    OnPropertyChanged("PARAM_NAME");
                }
            }
        }
        private string _pARAM_NAME;
    
        [DataMember]
        public string PARAM_TYPE
        {
            get { return _pARAM_TYPE; }
            set
            {
                if (_pARAM_TYPE != value)
                {
                    _pARAM_TYPE = value;
                    OnPropertyChanged("PARAM_TYPE");
                }
            }
        }
        private string _pARAM_TYPE;
    
        [DataMember]
        public string PARAM_DATA_TYPE
        {
            get { return _pARAM_DATA_TYPE; }
            set
            {
                if (_pARAM_DATA_TYPE != value)
                {
                    _pARAM_DATA_TYPE = value;
                    OnPropertyChanged("PARAM_DATA_TYPE");
                }
            }
        }
        private string _pARAM_DATA_TYPE;
    
        [DataMember]
        public Nullable<decimal> PERCISION
        {
            get { return _pERCISION; }
            set
            {
                if (_pERCISION != value)
                {
                    _pERCISION = value;
                    OnPropertyChanged("PERCISION");
                }
            }
        }
        private Nullable<decimal> _pERCISION;
    
        [DataMember]
        public Nullable<decimal> SCALE
        {
            get { return _sCALE; }
            set
            {
                if (_sCALE != value)
                {
                    _sCALE = value;
                    OnPropertyChanged("SCALE");
                }
            }
        }
        private Nullable<decimal> _sCALE;
    
        [DataMember]
        public string NULLABLE
        {
            get { return _nULLABLE; }
            set
            {
                if (_nULLABLE != value)
                {
                    _nULLABLE = value;
                    OnPropertyChanged("NULLABLE");
                }
            }
        }
        private string _nULLABLE;
    
        [DataMember]
        public string DEFAULT_VALUE
        {
            get { return _dEFAULT_VALUE; }
            set
            {
                if (_dEFAULT_VALUE != value)
                {
                    _dEFAULT_VALUE = value;
                    OnPropertyChanged("DEFAULT_VALUE");
                }
            }
        }
        private string _dEFAULT_VALUE;
    
        [DataMember]
        public decimal SECURITY_GROUP_ID
        {
            get { return _sECURITY_GROUP_ID; }
            set
            {
                if (_sECURITY_GROUP_ID != value)
                {
                    _sECURITY_GROUP_ID = value;
                    OnPropertyChanged("SECURITY_GROUP_ID");
                }
            }
        }
        private decimal _sECURITY_GROUP_ID;
    
        [DataMember]
        public System.DateTime CREATED_ON
        {
            get { return _cREATED_ON; }
            set
            {
                if (_cREATED_ON != value)
                {
                    _cREATED_ON = value;
                    OnPropertyChanged("CREATED_ON");
                }
            }
        }
        private System.DateTime _cREATED_ON;
    
        [DataMember]
        public string CREATED_BY
        {
            get { return _cREATED_BY; }
            set
            {
                if (_cREATED_BY != value)
                {
                    _cREATED_BY = value;
                    OnPropertyChanged("CREATED_BY");
                }
            }
        }
        private string _cREATED_BY;
    
        [DataMember]
        public Nullable<System.DateTime> LAST_UPDATED_ON
        {
            get { return _lAST_UPDATED_ON; }
            set
            {
                if (_lAST_UPDATED_ON != value)
                {
                    _lAST_UPDATED_ON = value;
                    OnPropertyChanged("LAST_UPDATED_ON");
                }
            }
        }
        private Nullable<System.DateTime> _lAST_UPDATED_ON;
    
        [DataMember]
        public string LAST_UPDATED_BY
        {
            get { return _lAST_UPDATED_BY; }
            set
            {
                if (_lAST_UPDATED_BY != value)
                {
                    _lAST_UPDATED_BY = value;
                    OnPropertyChanged("LAST_UPDATED_BY");
                }
            }
        }
        private string _lAST_UPDATED_BY;

        #endregion
        #region Navigation Properties
    
        [DataMember]
        public MD_CONNECTIONS MD_CONNECTIONS
        {
            get { return _mD_CONNECTIONS; }
            set
            {
                if (!ReferenceEquals(_mD_CONNECTIONS, value))
                {
                    var previousValue = _mD_CONNECTIONS;
                    _mD_CONNECTIONS = value;
                    FixupMD_CONNECTIONS(previousValue);
                    OnNavigationPropertyChanged("MD_CONNECTIONS");
                }
            }
        }
        private MD_CONNECTIONS _mD_CONNECTIONS;

        #endregion
        #region ChangeTracking
    
        protected virtual void OnPropertyChanged(String propertyName)
        {
            if (ChangeTracker.State != ObjectState.Added && ChangeTracker.State != ObjectState.Deleted)
            {
                ChangeTracker.State = ObjectState.Modified;
            }
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        protected virtual void OnNavigationPropertyChanged(String propertyName)
        {
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        event PropertyChangedEventHandler INotifyPropertyChanged.PropertyChanged{ add { _propertyChanged += value; } remove { _propertyChanged -= value; } }
        private event PropertyChangedEventHandler _propertyChanged;
        private ObjectChangeTracker _changeTracker;
    
        [DataMember]
        public ObjectChangeTracker ChangeTracker
        {
            get
            {
                if (_changeTracker == null)
                {
                    _changeTracker = new ObjectChangeTracker();
                    _changeTracker.ObjectStateChanging += HandleObjectStateChanging;
                }
                return _changeTracker;
            }
            set
            {
                if(_changeTracker != null)
                {
                    _changeTracker.ObjectStateChanging -= HandleObjectStateChanging;
                }
                _changeTracker = value;
                if(_changeTracker != null)
                {
                    _changeTracker.ObjectStateChanging += HandleObjectStateChanging;
                }
            }
        }
    
        private void HandleObjectStateChanging(object sender, ObjectStateChangingEventArgs e)
        {
            if (e.NewState == ObjectState.Deleted)
            {
                ClearNavigationProperties();
            }
        }
    
        protected bool IsDeserializing { get; private set; }
    
        [OnDeserializing]
        public void OnDeserializingMethod(StreamingContext context)
        {
            IsDeserializing = true;
        }
    
        [OnDeserialized]
        public void OnDeserializedMethod(StreamingContext context)
        {
            IsDeserializing = false;
            ChangeTracker.ChangeTrackingEnabled = true;
        }
    
        // This entity type is the dependent end in at least one association that performs cascade deletes.
        // This event handler will process notifications that occur when the principal end is deleted.
        internal void HandleCascadeDelete(object sender, ObjectStateChangingEventArgs e)
        {
            if (e.NewState == ObjectState.Deleted)
            {
                this.MarkAsDeleted();
            }
        }
    
        protected virtual void ClearNavigationProperties()
        {
            MD_CONNECTIONS = null;
        }

        #endregion
        #region Association Fixup
    
        private void FixupMD_CONNECTIONS(MD_CONNECTIONS previousValue)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (previousValue != null && previousValue.MD_MIGR_PARAMETER.Contains(this))
            {
                previousValue.MD_MIGR_PARAMETER.Remove(this);
            }
    
            if (MD_CONNECTIONS != null)
            {
                if (!MD_CONNECTIONS.MD_MIGR_PARAMETER.Contains(this))
                {
                    MD_CONNECTIONS.MD_MIGR_PARAMETER.Add(this);
                }
    
                CONNECTION_ID_FK = MD_CONNECTIONS.ID;
            }
            if (ChangeTracker.ChangeTrackingEnabled)
            {
                if (ChangeTracker.OriginalValues.ContainsKey("MD_CONNECTIONS")
                    && (ChangeTracker.OriginalValues["MD_CONNECTIONS"] == MD_CONNECTIONS))
                {
                    ChangeTracker.OriginalValues.Remove("MD_CONNECTIONS");
                }
                else
                {
                    ChangeTracker.RecordOriginalValue("MD_CONNECTIONS", previousValue);
                }
                if (MD_CONNECTIONS != null && !MD_CONNECTIONS.ChangeTracker.ChangeTrackingEnabled)
                {
                    MD_CONNECTIONS.StartTracking();
                }
            }
        }

        #endregion
    }
}
