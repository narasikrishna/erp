//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.Serialization;

namespace FIN.DAL
{
    [DataContract(IsReference = true)]
    [KnownType(typeof(HR_INTERVIEWS_DTL))]
    [KnownType(typeof(HR_INTERVIEWS_HDR))]
    [KnownType(typeof(HR_VAC_EVAL_CONDITION))]
    public partial class HR_INTERVIEWS_DTL_CRIT: IObjectWithChangeTracker, INotifyPropertyChanged
    {
        #region Primitive Properties
    
        [DataMember]
        public Nullable<int> PK_ID
        {
            get { return _pK_ID; }
            set
            {
                if (_pK_ID != value)
                {
                    _pK_ID = value;
                    OnPropertyChanged("PK_ID");
                }
            }
        }
        private Nullable<int> _pK_ID;
    
        [DataMember]
        public Nullable<int> CHILD_ID
        {
            get { return _cHILD_ID; }
            set
            {
                if (_cHILD_ID != value)
                {
                    _cHILD_ID = value;
                    OnPropertyChanged("CHILD_ID");
                }
            }
        }
        private Nullable<int> _cHILD_ID;
    
        [DataMember]
        public string INT_CRIT_ID
        {
            get { return _iNT_CRIT_ID; }
            set
            {
                if (_iNT_CRIT_ID != value)
                {
                    if (ChangeTracker.ChangeTrackingEnabled && ChangeTracker.State != ObjectState.Added)
                    {
                        throw new InvalidOperationException("The property 'INT_CRIT_ID' is part of the object's key and cannot be changed. Changes to key properties can only be made when the object is not being tracked or is in the Added state.");
                    }
                    _iNT_CRIT_ID = value;
                    OnPropertyChanged("INT_CRIT_ID");
                }
            }
        }
        private string _iNT_CRIT_ID;
    
        [DataMember]
        public string INT_DTL_ID
        {
            get { return _iNT_DTL_ID; }
            set
            {
                if (_iNT_DTL_ID != value)
                {
                    ChangeTracker.RecordOriginalValue("INT_DTL_ID", _iNT_DTL_ID);
                    if (!IsDeserializing)
                    {
                        if (HR_INTERVIEWS_DTL != null && HR_INTERVIEWS_DTL.INT_DTL_ID != value)
                        {
                            HR_INTERVIEWS_DTL = null;
                        }
                    }
                    _iNT_DTL_ID = value;
                    OnPropertyChanged("INT_DTL_ID");
                }
            }
        }
        private string _iNT_DTL_ID;
    
        [DataMember]
        public string INT_ID
        {
            get { return _iNT_ID; }
            set
            {
                if (_iNT_ID != value)
                {
                    ChangeTracker.RecordOriginalValue("INT_ID", _iNT_ID);
                    if (!IsDeserializing)
                    {
                        if (HR_INTERVIEWS_HDR != null && HR_INTERVIEWS_HDR.INT_ID != value)
                        {
                            HR_INTERVIEWS_HDR = null;
                        }
                    }
                    _iNT_ID = value;
                    OnPropertyChanged("INT_ID");
                }
            }
        }
        private string _iNT_ID;
    
        [DataMember]
        public string VAC_EVAL_ID
        {
            get { return _vAC_EVAL_ID; }
            set
            {
                if (_vAC_EVAL_ID != value)
                {
                    ChangeTracker.RecordOriginalValue("VAC_EVAL_ID", _vAC_EVAL_ID);
                    if (!IsDeserializing)
                    {
                        if (HR_VAC_EVAL_CONDITION != null && HR_VAC_EVAL_CONDITION.VAC_EVAL_ID != value)
                        {
                            HR_VAC_EVAL_CONDITION = null;
                        }
                    }
                    _vAC_EVAL_ID = value;
                    OnPropertyChanged("VAC_EVAL_ID");
                }
            }
        }
        private string _vAC_EVAL_ID;
    
        [DataMember]
        public Nullable<int> INT_LEVEL_SCORED
        {
            get { return _iNT_LEVEL_SCORED; }
            set
            {
                if (_iNT_LEVEL_SCORED != value)
                {
                    _iNT_LEVEL_SCORED = value;
                    OnPropertyChanged("INT_LEVEL_SCORED");
                }
            }
        }
        private Nullable<int> _iNT_LEVEL_SCORED;
    
        [DataMember]
        public string INT_COMMENTS
        {
            get { return _iNT_COMMENTS; }
            set
            {
                if (_iNT_COMMENTS != value)
                {
                    _iNT_COMMENTS = value;
                    OnPropertyChanged("INT_COMMENTS");
                }
            }
        }
        private string _iNT_COMMENTS;
    
        [DataMember]
        public string ENABLED_FLAG
        {
            get { return _eNABLED_FLAG; }
            set
            {
                if (_eNABLED_FLAG != value)
                {
                    _eNABLED_FLAG = value;
                    OnPropertyChanged("ENABLED_FLAG");
                }
            }
        }
        private string _eNABLED_FLAG;
    
        [DataMember]
        public string WORKFLOW_COMPLETION_STATUS
        {
            get { return _wORKFLOW_COMPLETION_STATUS; }
            set
            {
                if (_wORKFLOW_COMPLETION_STATUS != value)
                {
                    _wORKFLOW_COMPLETION_STATUS = value;
                    OnPropertyChanged("WORKFLOW_COMPLETION_STATUS");
                }
            }
        }
        private string _wORKFLOW_COMPLETION_STATUS;
    
        [DataMember]
        public string CREATED_BY
        {
            get { return _cREATED_BY; }
            set
            {
                if (_cREATED_BY != value)
                {
                    _cREATED_BY = value;
                    OnPropertyChanged("CREATED_BY");
                }
            }
        }
        private string _cREATED_BY;
    
        [DataMember]
        public System.DateTime CREATED_DATE
        {
            get { return _cREATED_DATE; }
            set
            {
                if (_cREATED_DATE != value)
                {
                    _cREATED_DATE = value;
                    OnPropertyChanged("CREATED_DATE");
                }
            }
        }
        private System.DateTime _cREATED_DATE;
    
        [DataMember]
        public string MODIFIED_BY
        {
            get { return _mODIFIED_BY; }
            set
            {
                if (_mODIFIED_BY != value)
                {
                    _mODIFIED_BY = value;
                    OnPropertyChanged("MODIFIED_BY");
                }
            }
        }
        private string _mODIFIED_BY;
    
        [DataMember]
        public Nullable<System.DateTime> MODIFIED_DATE
        {
            get { return _mODIFIED_DATE; }
            set
            {
                if (_mODIFIED_DATE != value)
                {
                    _mODIFIED_DATE = value;
                    OnPropertyChanged("MODIFIED_DATE");
                }
            }
        }
        private Nullable<System.DateTime> _mODIFIED_DATE;
    
        [DataMember]
        public string ATTRIBUTE1
        {
            get { return _aTTRIBUTE1; }
            set
            {
                if (_aTTRIBUTE1 != value)
                {
                    _aTTRIBUTE1 = value;
                    OnPropertyChanged("ATTRIBUTE1");
                }
            }
        }
        private string _aTTRIBUTE1;
    
        [DataMember]
        public string ATTRIBUTE2
        {
            get { return _aTTRIBUTE2; }
            set
            {
                if (_aTTRIBUTE2 != value)
                {
                    _aTTRIBUTE2 = value;
                    OnPropertyChanged("ATTRIBUTE2");
                }
            }
        }
        private string _aTTRIBUTE2;
    
        [DataMember]
        public string ATTRIBUTE3
        {
            get { return _aTTRIBUTE3; }
            set
            {
                if (_aTTRIBUTE3 != value)
                {
                    _aTTRIBUTE3 = value;
                    OnPropertyChanged("ATTRIBUTE3");
                }
            }
        }
        private string _aTTRIBUTE3;
    
        [DataMember]
        public string ATTRIBUTE4
        {
            get { return _aTTRIBUTE4; }
            set
            {
                if (_aTTRIBUTE4 != value)
                {
                    _aTTRIBUTE4 = value;
                    OnPropertyChanged("ATTRIBUTE4");
                }
            }
        }
        private string _aTTRIBUTE4;
    
        [DataMember]
        public string ATTRIBUTE5
        {
            get { return _aTTRIBUTE5; }
            set
            {
                if (_aTTRIBUTE5 != value)
                {
                    _aTTRIBUTE5 = value;
                    OnPropertyChanged("ATTRIBUTE5");
                }
            }
        }
        private string _aTTRIBUTE5;
    
        [DataMember]
        public string ATTRIBUTE6
        {
            get { return _aTTRIBUTE6; }
            set
            {
                if (_aTTRIBUTE6 != value)
                {
                    _aTTRIBUTE6 = value;
                    OnPropertyChanged("ATTRIBUTE6");
                }
            }
        }
        private string _aTTRIBUTE6;
    
        [DataMember]
        public string ATTRIBUTE7
        {
            get { return _aTTRIBUTE7; }
            set
            {
                if (_aTTRIBUTE7 != value)
                {
                    _aTTRIBUTE7 = value;
                    OnPropertyChanged("ATTRIBUTE7");
                }
            }
        }
        private string _aTTRIBUTE7;
    
        [DataMember]
        public string ATTRIBUTE8
        {
            get { return _aTTRIBUTE8; }
            set
            {
                if (_aTTRIBUTE8 != value)
                {
                    _aTTRIBUTE8 = value;
                    OnPropertyChanged("ATTRIBUTE8");
                }
            }
        }
        private string _aTTRIBUTE8;
    
        [DataMember]
        public string ATTRIBUTE9
        {
            get { return _aTTRIBUTE9; }
            set
            {
                if (_aTTRIBUTE9 != value)
                {
                    _aTTRIBUTE9 = value;
                    OnPropertyChanged("ATTRIBUTE9");
                }
            }
        }
        private string _aTTRIBUTE9;
    
        [DataMember]
        public string ATTRIBUTE10
        {
            get { return _aTTRIBUTE10; }
            set
            {
                if (_aTTRIBUTE10 != value)
                {
                    _aTTRIBUTE10 = value;
                    OnPropertyChanged("ATTRIBUTE10");
                }
            }
        }
        private string _aTTRIBUTE10;

        #endregion
        #region Navigation Properties
    
        [DataMember]
        public HR_INTERVIEWS_DTL HR_INTERVIEWS_DTL
        {
            get { return _hR_INTERVIEWS_DTL; }
            set
            {
                if (!ReferenceEquals(_hR_INTERVIEWS_DTL, value))
                {
                    var previousValue = _hR_INTERVIEWS_DTL;
                    _hR_INTERVIEWS_DTL = value;
                    FixupHR_INTERVIEWS_DTL(previousValue);
                    OnNavigationPropertyChanged("HR_INTERVIEWS_DTL");
                }
            }
        }
        private HR_INTERVIEWS_DTL _hR_INTERVIEWS_DTL;
    
        [DataMember]
        public HR_INTERVIEWS_HDR HR_INTERVIEWS_HDR
        {
            get { return _hR_INTERVIEWS_HDR; }
            set
            {
                if (!ReferenceEquals(_hR_INTERVIEWS_HDR, value))
                {
                    var previousValue = _hR_INTERVIEWS_HDR;
                    _hR_INTERVIEWS_HDR = value;
                    FixupHR_INTERVIEWS_HDR(previousValue);
                    OnNavigationPropertyChanged("HR_INTERVIEWS_HDR");
                }
            }
        }
        private HR_INTERVIEWS_HDR _hR_INTERVIEWS_HDR;
    
        [DataMember]
        public HR_VAC_EVAL_CONDITION HR_VAC_EVAL_CONDITION
        {
            get { return _hR_VAC_EVAL_CONDITION; }
            set
            {
                if (!ReferenceEquals(_hR_VAC_EVAL_CONDITION, value))
                {
                    var previousValue = _hR_VAC_EVAL_CONDITION;
                    _hR_VAC_EVAL_CONDITION = value;
                    FixupHR_VAC_EVAL_CONDITION(previousValue);
                    OnNavigationPropertyChanged("HR_VAC_EVAL_CONDITION");
                }
            }
        }
        private HR_VAC_EVAL_CONDITION _hR_VAC_EVAL_CONDITION;

        #endregion
        #region ChangeTracking
    
        protected virtual void OnPropertyChanged(String propertyName)
        {
            if (ChangeTracker.State != ObjectState.Added && ChangeTracker.State != ObjectState.Deleted)
            {
                ChangeTracker.State = ObjectState.Modified;
            }
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        protected virtual void OnNavigationPropertyChanged(String propertyName)
        {
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        event PropertyChangedEventHandler INotifyPropertyChanged.PropertyChanged{ add { _propertyChanged += value; } remove { _propertyChanged -= value; } }
        private event PropertyChangedEventHandler _propertyChanged;
        private ObjectChangeTracker _changeTracker;
    
        [DataMember]
        public ObjectChangeTracker ChangeTracker
        {
            get
            {
                if (_changeTracker == null)
                {
                    _changeTracker = new ObjectChangeTracker();
                    _changeTracker.ObjectStateChanging += HandleObjectStateChanging;
                }
                return _changeTracker;
            }
            set
            {
                if(_changeTracker != null)
                {
                    _changeTracker.ObjectStateChanging -= HandleObjectStateChanging;
                }
                _changeTracker = value;
                if(_changeTracker != null)
                {
                    _changeTracker.ObjectStateChanging += HandleObjectStateChanging;
                }
            }
        }
    
        private void HandleObjectStateChanging(object sender, ObjectStateChangingEventArgs e)
        {
            if (e.NewState == ObjectState.Deleted)
            {
                ClearNavigationProperties();
            }
        }
    
        protected bool IsDeserializing { get; private set; }
    
        [OnDeserializing]
        public void OnDeserializingMethod(StreamingContext context)
        {
            IsDeserializing = true;
        }
    
        [OnDeserialized]
        public void OnDeserializedMethod(StreamingContext context)
        {
            IsDeserializing = false;
            ChangeTracker.ChangeTrackingEnabled = true;
        }
    
        protected virtual void ClearNavigationProperties()
        {
            HR_INTERVIEWS_DTL = null;
            HR_INTERVIEWS_HDR = null;
            HR_VAC_EVAL_CONDITION = null;
        }

        #endregion
        #region Association Fixup
    
        private void FixupHR_INTERVIEWS_DTL(HR_INTERVIEWS_DTL previousValue, bool skipKeys = false)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (previousValue != null && previousValue.HR_INTERVIEWS_DTL_CRIT.Contains(this))
            {
                previousValue.HR_INTERVIEWS_DTL_CRIT.Remove(this);
            }
    
            if (HR_INTERVIEWS_DTL != null)
            {
                if (!HR_INTERVIEWS_DTL.HR_INTERVIEWS_DTL_CRIT.Contains(this))
                {
                    HR_INTERVIEWS_DTL.HR_INTERVIEWS_DTL_CRIT.Add(this);
                }
    
                INT_DTL_ID = HR_INTERVIEWS_DTL.INT_DTL_ID;
            }
            else if (!skipKeys)
            {
                INT_DTL_ID = null;
            }
    
            if (ChangeTracker.ChangeTrackingEnabled)
            {
                if (ChangeTracker.OriginalValues.ContainsKey("HR_INTERVIEWS_DTL")
                    && (ChangeTracker.OriginalValues["HR_INTERVIEWS_DTL"] == HR_INTERVIEWS_DTL))
                {
                    ChangeTracker.OriginalValues.Remove("HR_INTERVIEWS_DTL");
                }
                else
                {
                    ChangeTracker.RecordOriginalValue("HR_INTERVIEWS_DTL", previousValue);
                }
                if (HR_INTERVIEWS_DTL != null && !HR_INTERVIEWS_DTL.ChangeTracker.ChangeTrackingEnabled)
                {
                    HR_INTERVIEWS_DTL.StartTracking();
                }
            }
        }
    
        private void FixupHR_INTERVIEWS_HDR(HR_INTERVIEWS_HDR previousValue, bool skipKeys = false)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (previousValue != null && previousValue.HR_INTERVIEWS_DTL_CRIT.Contains(this))
            {
                previousValue.HR_INTERVIEWS_DTL_CRIT.Remove(this);
            }
    
            if (HR_INTERVIEWS_HDR != null)
            {
                if (!HR_INTERVIEWS_HDR.HR_INTERVIEWS_DTL_CRIT.Contains(this))
                {
                    HR_INTERVIEWS_HDR.HR_INTERVIEWS_DTL_CRIT.Add(this);
                }
    
                INT_ID = HR_INTERVIEWS_HDR.INT_ID;
            }
            else if (!skipKeys)
            {
                INT_ID = null;
            }
    
            if (ChangeTracker.ChangeTrackingEnabled)
            {
                if (ChangeTracker.OriginalValues.ContainsKey("HR_INTERVIEWS_HDR")
                    && (ChangeTracker.OriginalValues["HR_INTERVIEWS_HDR"] == HR_INTERVIEWS_HDR))
                {
                    ChangeTracker.OriginalValues.Remove("HR_INTERVIEWS_HDR");
                }
                else
                {
                    ChangeTracker.RecordOriginalValue("HR_INTERVIEWS_HDR", previousValue);
                }
                if (HR_INTERVIEWS_HDR != null && !HR_INTERVIEWS_HDR.ChangeTracker.ChangeTrackingEnabled)
                {
                    HR_INTERVIEWS_HDR.StartTracking();
                }
            }
        }
    
        private void FixupHR_VAC_EVAL_CONDITION(HR_VAC_EVAL_CONDITION previousValue, bool skipKeys = false)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (previousValue != null && previousValue.HR_INTERVIEWS_DTL_CRIT.Contains(this))
            {
                previousValue.HR_INTERVIEWS_DTL_CRIT.Remove(this);
            }
    
            if (HR_VAC_EVAL_CONDITION != null)
            {
                if (!HR_VAC_EVAL_CONDITION.HR_INTERVIEWS_DTL_CRIT.Contains(this))
                {
                    HR_VAC_EVAL_CONDITION.HR_INTERVIEWS_DTL_CRIT.Add(this);
                }
    
                VAC_EVAL_ID = HR_VAC_EVAL_CONDITION.VAC_EVAL_ID;
            }
            else if (!skipKeys)
            {
                VAC_EVAL_ID = null;
            }
    
            if (ChangeTracker.ChangeTrackingEnabled)
            {
                if (ChangeTracker.OriginalValues.ContainsKey("HR_VAC_EVAL_CONDITION")
                    && (ChangeTracker.OriginalValues["HR_VAC_EVAL_CONDITION"] == HR_VAC_EVAL_CONDITION))
                {
                    ChangeTracker.OriginalValues.Remove("HR_VAC_EVAL_CONDITION");
                }
                else
                {
                    ChangeTracker.RecordOriginalValue("HR_VAC_EVAL_CONDITION", previousValue);
                }
                if (HR_VAC_EVAL_CONDITION != null && !HR_VAC_EVAL_CONDITION.ChangeTracker.ChangeTrackingEnabled)
                {
                    HR_VAC_EVAL_CONDITION.StartTracking();
                }
            }
        }

        #endregion
    }
}
