//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.Serialization;

namespace FIN.DAL
{
    [DataContract(IsReference = true)]
    [KnownType(typeof(GL_COMPANIES_HDR))]
    public partial class HR_EMP_PERMIT_DTLS: IObjectWithChangeTracker, INotifyPropertyChanged
    {
        #region Primitive Properties
    
        [DataMember]
        public Nullable<int> PK_ID
        {
            get { return _pK_ID; }
            set
            {
                if (_pK_ID != value)
                {
                    _pK_ID = value;
                    OnPropertyChanged("PK_ID");
                }
            }
        }
        private Nullable<int> _pK_ID;
    
        [DataMember]
        public Nullable<int> CHILD_ID
        {
            get { return _cHILD_ID; }
            set
            {
                if (_cHILD_ID != value)
                {
                    _cHILD_ID = value;
                    OnPropertyChanged("CHILD_ID");
                }
            }
        }
        private Nullable<int> _cHILD_ID;
    
        [DataMember]
        public string PERMIT_ID
        {
            get { return _pERMIT_ID; }
            set
            {
                if (_pERMIT_ID != value)
                {
                    if (ChangeTracker.ChangeTrackingEnabled && ChangeTracker.State != ObjectState.Added)
                    {
                        throw new InvalidOperationException("The property 'PERMIT_ID' is part of the object's key and cannot be changed. Changes to key properties can only be made when the object is not being tracked or is in the Added state.");
                    }
                    _pERMIT_ID = value;
                    OnPropertyChanged("PERMIT_ID");
                }
            }
        }
        private string _pERMIT_ID;
    
        [DataMember]
        public string PERMIT_TYPE
        {
            get { return _pERMIT_TYPE; }
            set
            {
                if (_pERMIT_TYPE != value)
                {
                    _pERMIT_TYPE = value;
                    OnPropertyChanged("PERMIT_TYPE");
                }
            }
        }
        private string _pERMIT_TYPE;
    
        [DataMember]
        public string PERMIT_EMP_ID
        {
            get { return _pERMIT_EMP_ID; }
            set
            {
                if (_pERMIT_EMP_ID != value)
                {
                    _pERMIT_EMP_ID = value;
                    OnPropertyChanged("PERMIT_EMP_ID");
                }
            }
        }
        private string _pERMIT_EMP_ID;
    
        [DataMember]
        public string PERMIT_SPONSOR
        {
            get { return _pERMIT_SPONSOR; }
            set
            {
                if (_pERMIT_SPONSOR != value)
                {
                    _pERMIT_SPONSOR = value;
                    OnPropertyChanged("PERMIT_SPONSOR");
                }
            }
        }
        private string _pERMIT_SPONSOR;
    
        [DataMember]
        public string PERMIT_NUMBER
        {
            get { return _pERMIT_NUMBER; }
            set
            {
                if (_pERMIT_NUMBER != value)
                {
                    _pERMIT_NUMBER = value;
                    OnPropertyChanged("PERMIT_NUMBER");
                }
            }
        }
        private string _pERMIT_NUMBER;
    
        [DataMember]
        public string PERMIT_ISSUED_BY
        {
            get { return _pERMIT_ISSUED_BY; }
            set
            {
                if (_pERMIT_ISSUED_BY != value)
                {
                    _pERMIT_ISSUED_BY = value;
                    OnPropertyChanged("PERMIT_ISSUED_BY");
                }
            }
        }
        private string _pERMIT_ISSUED_BY;
    
        [DataMember]
        public System.DateTime PERMIT_ISSUE_DATE
        {
            get { return _pERMIT_ISSUE_DATE; }
            set
            {
                if (_pERMIT_ISSUE_DATE != value)
                {
                    _pERMIT_ISSUE_DATE = value;
                    OnPropertyChanged("PERMIT_ISSUE_DATE");
                }
            }
        }
        private System.DateTime _pERMIT_ISSUE_DATE;
    
        [DataMember]
        public Nullable<System.DateTime> PERMIT_EXPIRY_DATE
        {
            get { return _pERMIT_EXPIRY_DATE; }
            set
            {
                if (_pERMIT_EXPIRY_DATE != value)
                {
                    _pERMIT_EXPIRY_DATE = value;
                    OnPropertyChanged("PERMIT_EXPIRY_DATE");
                }
            }
        }
        private Nullable<System.DateTime> _pERMIT_EXPIRY_DATE;
    
        [DataMember]
        public string ENABLED_FLAG
        {
            get { return _eNABLED_FLAG; }
            set
            {
                if (_eNABLED_FLAG != value)
                {
                    _eNABLED_FLAG = value;
                    OnPropertyChanged("ENABLED_FLAG");
                }
            }
        }
        private string _eNABLED_FLAG;
    
        [DataMember]
        public string WORKFLOW_COMPLETION_STATUS
        {
            get { return _wORKFLOW_COMPLETION_STATUS; }
            set
            {
                if (_wORKFLOW_COMPLETION_STATUS != value)
                {
                    _wORKFLOW_COMPLETION_STATUS = value;
                    OnPropertyChanged("WORKFLOW_COMPLETION_STATUS");
                }
            }
        }
        private string _wORKFLOW_COMPLETION_STATUS;
    
        [DataMember]
        public string CREATED_BY
        {
            get { return _cREATED_BY; }
            set
            {
                if (_cREATED_BY != value)
                {
                    _cREATED_BY = value;
                    OnPropertyChanged("CREATED_BY");
                }
            }
        }
        private string _cREATED_BY;
    
        [DataMember]
        public System.DateTime CREATED_DATE
        {
            get { return _cREATED_DATE; }
            set
            {
                if (_cREATED_DATE != value)
                {
                    _cREATED_DATE = value;
                    OnPropertyChanged("CREATED_DATE");
                }
            }
        }
        private System.DateTime _cREATED_DATE;
    
        [DataMember]
        public string MODIFIED_BY
        {
            get { return _mODIFIED_BY; }
            set
            {
                if (_mODIFIED_BY != value)
                {
                    _mODIFIED_BY = value;
                    OnPropertyChanged("MODIFIED_BY");
                }
            }
        }
        private string _mODIFIED_BY;
    
        [DataMember]
        public Nullable<System.DateTime> MODIFIED_DATE
        {
            get { return _mODIFIED_DATE; }
            set
            {
                if (_mODIFIED_DATE != value)
                {
                    _mODIFIED_DATE = value;
                    OnPropertyChanged("MODIFIED_DATE");
                }
            }
        }
        private Nullable<System.DateTime> _mODIFIED_DATE;
    
        [DataMember]
        public string ATTRIBUTE1
        {
            get { return _aTTRIBUTE1; }
            set
            {
                if (_aTTRIBUTE1 != value)
                {
                    _aTTRIBUTE1 = value;
                    OnPropertyChanged("ATTRIBUTE1");
                }
            }
        }
        private string _aTTRIBUTE1;
    
        [DataMember]
        public string ATTRIBUTE2
        {
            get { return _aTTRIBUTE2; }
            set
            {
                if (_aTTRIBUTE2 != value)
                {
                    _aTTRIBUTE2 = value;
                    OnPropertyChanged("ATTRIBUTE2");
                }
            }
        }
        private string _aTTRIBUTE2;
    
        [DataMember]
        public string ATTRIBUTE3
        {
            get { return _aTTRIBUTE3; }
            set
            {
                if (_aTTRIBUTE3 != value)
                {
                    _aTTRIBUTE3 = value;
                    OnPropertyChanged("ATTRIBUTE3");
                }
            }
        }
        private string _aTTRIBUTE3;
    
        [DataMember]
        public string ATTRIBUTE4
        {
            get { return _aTTRIBUTE4; }
            set
            {
                if (_aTTRIBUTE4 != value)
                {
                    _aTTRIBUTE4 = value;
                    OnPropertyChanged("ATTRIBUTE4");
                }
            }
        }
        private string _aTTRIBUTE4;
    
        [DataMember]
        public string ATTRIBUTE5
        {
            get { return _aTTRIBUTE5; }
            set
            {
                if (_aTTRIBUTE5 != value)
                {
                    _aTTRIBUTE5 = value;
                    OnPropertyChanged("ATTRIBUTE5");
                }
            }
        }
        private string _aTTRIBUTE5;
    
        [DataMember]
        public string ATTRIBUTE6
        {
            get { return _aTTRIBUTE6; }
            set
            {
                if (_aTTRIBUTE6 != value)
                {
                    _aTTRIBUTE6 = value;
                    OnPropertyChanged("ATTRIBUTE6");
                }
            }
        }
        private string _aTTRIBUTE6;
    
        [DataMember]
        public string ATTRIBUTE7
        {
            get { return _aTTRIBUTE7; }
            set
            {
                if (_aTTRIBUTE7 != value)
                {
                    _aTTRIBUTE7 = value;
                    OnPropertyChanged("ATTRIBUTE7");
                }
            }
        }
        private string _aTTRIBUTE7;
    
        [DataMember]
        public string ATTRIBUTE8
        {
            get { return _aTTRIBUTE8; }
            set
            {
                if (_aTTRIBUTE8 != value)
                {
                    _aTTRIBUTE8 = value;
                    OnPropertyChanged("ATTRIBUTE8");
                }
            }
        }
        private string _aTTRIBUTE8;
    
        [DataMember]
        public string ATTRIBUTE9
        {
            get { return _aTTRIBUTE9; }
            set
            {
                if (_aTTRIBUTE9 != value)
                {
                    _aTTRIBUTE9 = value;
                    OnPropertyChanged("ATTRIBUTE9");
                }
            }
        }
        private string _aTTRIBUTE9;
    
        [DataMember]
        public string ATTRIBUTE10
        {
            get { return _aTTRIBUTE10; }
            set
            {
                if (_aTTRIBUTE10 != value)
                {
                    _aTTRIBUTE10 = value;
                    OnPropertyChanged("ATTRIBUTE10");
                }
            }
        }
        private string _aTTRIBUTE10;
    
        [DataMember]
        public string PERMIT_ORG_ID
        {
            get { return _pERMIT_ORG_ID; }
            set
            {
                if (_pERMIT_ORG_ID != value)
                {
                    ChangeTracker.RecordOriginalValue("PERMIT_ORG_ID", _pERMIT_ORG_ID);
                    if (!IsDeserializing)
                    {
                        if (GL_COMPANIES_HDR != null && GL_COMPANIES_HDR.COMP_ID != value)
                        {
                            GL_COMPANIES_HDR = null;
                        }
                    }
                    _pERMIT_ORG_ID = value;
                    OnPropertyChanged("PERMIT_ORG_ID");
                }
            }
        }
        private string _pERMIT_ORG_ID;

        #endregion
        #region Navigation Properties
    
        [DataMember]
        public GL_COMPANIES_HDR GL_COMPANIES_HDR
        {
            get { return _gL_COMPANIES_HDR; }
            set
            {
                if (!ReferenceEquals(_gL_COMPANIES_HDR, value))
                {
                    var previousValue = _gL_COMPANIES_HDR;
                    _gL_COMPANIES_HDR = value;
                    FixupGL_COMPANIES_HDR(previousValue);
                    OnNavigationPropertyChanged("GL_COMPANIES_HDR");
                }
            }
        }
        private GL_COMPANIES_HDR _gL_COMPANIES_HDR;

        #endregion
        #region ChangeTracking
    
        protected virtual void OnPropertyChanged(String propertyName)
        {
            if (ChangeTracker.State != ObjectState.Added && ChangeTracker.State != ObjectState.Deleted)
            {
                ChangeTracker.State = ObjectState.Modified;
            }
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        protected virtual void OnNavigationPropertyChanged(String propertyName)
        {
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        event PropertyChangedEventHandler INotifyPropertyChanged.PropertyChanged{ add { _propertyChanged += value; } remove { _propertyChanged -= value; } }
        private event PropertyChangedEventHandler _propertyChanged;
        private ObjectChangeTracker _changeTracker;
    
        [DataMember]
        public ObjectChangeTracker ChangeTracker
        {
            get
            {
                if (_changeTracker == null)
                {
                    _changeTracker = new ObjectChangeTracker();
                    _changeTracker.ObjectStateChanging += HandleObjectStateChanging;
                }
                return _changeTracker;
            }
            set
            {
                if(_changeTracker != null)
                {
                    _changeTracker.ObjectStateChanging -= HandleObjectStateChanging;
                }
                _changeTracker = value;
                if(_changeTracker != null)
                {
                    _changeTracker.ObjectStateChanging += HandleObjectStateChanging;
                }
            }
        }
    
        private void HandleObjectStateChanging(object sender, ObjectStateChangingEventArgs e)
        {
            if (e.NewState == ObjectState.Deleted)
            {
                ClearNavigationProperties();
            }
        }
    
        protected bool IsDeserializing { get; private set; }
    
        [OnDeserializing]
        public void OnDeserializingMethod(StreamingContext context)
        {
            IsDeserializing = true;
        }
    
        [OnDeserialized]
        public void OnDeserializedMethod(StreamingContext context)
        {
            IsDeserializing = false;
            ChangeTracker.ChangeTrackingEnabled = true;
        }
    
        protected virtual void ClearNavigationProperties()
        {
            GL_COMPANIES_HDR = null;
        }

        #endregion
        #region Association Fixup
    
        private void FixupGL_COMPANIES_HDR(GL_COMPANIES_HDR previousValue, bool skipKeys = false)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (previousValue != null && previousValue.HR_EMP_PERMIT_DTLS.Contains(this))
            {
                previousValue.HR_EMP_PERMIT_DTLS.Remove(this);
            }
    
            if (GL_COMPANIES_HDR != null)
            {
                if (!GL_COMPANIES_HDR.HR_EMP_PERMIT_DTLS.Contains(this))
                {
                    GL_COMPANIES_HDR.HR_EMP_PERMIT_DTLS.Add(this);
                }
    
                PERMIT_ORG_ID = GL_COMPANIES_HDR.COMP_ID;
            }
            else if (!skipKeys)
            {
                PERMIT_ORG_ID = null;
            }
    
            if (ChangeTracker.ChangeTrackingEnabled)
            {
                if (ChangeTracker.OriginalValues.ContainsKey("GL_COMPANIES_HDR")
                    && (ChangeTracker.OriginalValues["GL_COMPANIES_HDR"] == GL_COMPANIES_HDR))
                {
                    ChangeTracker.OriginalValues.Remove("GL_COMPANIES_HDR");
                }
                else
                {
                    ChangeTracker.RecordOriginalValue("GL_COMPANIES_HDR", previousValue);
                }
                if (GL_COMPANIES_HDR != null && !GL_COMPANIES_HDR.ChangeTracker.ChangeTrackingEnabled)
                {
                    GL_COMPANIES_HDR.StartTracking();
                }
            }
        }

        #endregion
    }
}
