//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.Serialization;

namespace FIN.DAL
{
    [DataContract(IsReference = true)]
    [KnownType(typeof(HR_TRAINING_COST_HDR))]
    [KnownType(typeof(SSM_CURRENCIES))]
    public partial class HR_TRAINING_COST_DTL: IObjectWithChangeTracker, INotifyPropertyChanged
    {
        #region Primitive Properties
    
        [DataMember]
        public Nullable<int> PK_ID
        {
            get { return _pK_ID; }
            set
            {
                if (_pK_ID != value)
                {
                    _pK_ID = value;
                    OnPropertyChanged("PK_ID");
                }
            }
        }
        private Nullable<int> _pK_ID;
    
        [DataMember]
        public Nullable<int> CHILD_ID
        {
            get { return _cHILD_ID; }
            set
            {
                if (_cHILD_ID != value)
                {
                    _cHILD_ID = value;
                    OnPropertyChanged("CHILD_ID");
                }
            }
        }
        private Nullable<int> _cHILD_ID;
    
        [DataMember]
        public string COST_DTL_ID
        {
            get { return _cOST_DTL_ID; }
            set
            {
                if (_cOST_DTL_ID != value)
                {
                    if (ChangeTracker.ChangeTrackingEnabled && ChangeTracker.State != ObjectState.Added)
                    {
                        throw new InvalidOperationException("The property 'COST_DTL_ID' is part of the object's key and cannot be changed. Changes to key properties can only be made when the object is not being tracked or is in the Added state.");
                    }
                    _cOST_DTL_ID = value;
                    OnPropertyChanged("COST_DTL_ID");
                }
            }
        }
        private string _cOST_DTL_ID;
    
        [DataMember]
        public string COST_HDR_ID
        {
            get { return _cOST_HDR_ID; }
            set
            {
                if (_cOST_HDR_ID != value)
                {
                    ChangeTracker.RecordOriginalValue("COST_HDR_ID", _cOST_HDR_ID);
                    if (!IsDeserializing)
                    {
                        if (HR_TRAINING_COST_HDR != null && HR_TRAINING_COST_HDR.COST_HDR_ID != value)
                        {
                            HR_TRAINING_COST_HDR = null;
                        }
                    }
                    _cOST_HDR_ID = value;
                    OnPropertyChanged("COST_HDR_ID");
                }
            }
        }
        private string _cOST_HDR_ID;
    
        [DataMember]
        public string COST_TYPE
        {
            get { return _cOST_TYPE; }
            set
            {
                if (_cOST_TYPE != value)
                {
                    _cOST_TYPE = value;
                    OnPropertyChanged("COST_TYPE");
                }
            }
        }
        private string _cOST_TYPE;
    
        [DataMember]
        public string COST_ITEM_DESC
        {
            get { return _cOST_ITEM_DESC; }
            set
            {
                if (_cOST_ITEM_DESC != value)
                {
                    _cOST_ITEM_DESC = value;
                    OnPropertyChanged("COST_ITEM_DESC");
                }
            }
        }
        private string _cOST_ITEM_DESC;
    
        [DataMember]
        public string COST_ITEM_REMARK
        {
            get { return _cOST_ITEM_REMARK; }
            set
            {
                if (_cOST_ITEM_REMARK != value)
                {
                    _cOST_ITEM_REMARK = value;
                    OnPropertyChanged("COST_ITEM_REMARK");
                }
            }
        }
        private string _cOST_ITEM_REMARK;
    
        [DataMember]
        public int COST_QTY
        {
            get { return _cOST_QTY; }
            set
            {
                if (_cOST_QTY != value)
                {
                    _cOST_QTY = value;
                    OnPropertyChanged("COST_QTY");
                }
            }
        }
        private int _cOST_QTY;
    
        [DataMember]
        public decimal COST_UNIT_PRICE
        {
            get { return _cOST_UNIT_PRICE; }
            set
            {
                if (_cOST_UNIT_PRICE != value)
                {
                    _cOST_UNIT_PRICE = value;
                    OnPropertyChanged("COST_UNIT_PRICE");
                }
            }
        }
        private decimal _cOST_UNIT_PRICE;
    
        [DataMember]
        public string COST_CURRENCY
        {
            get { return _cOST_CURRENCY; }
            set
            {
                if (_cOST_CURRENCY != value)
                {
                    ChangeTracker.RecordOriginalValue("COST_CURRENCY", _cOST_CURRENCY);
                    if (!IsDeserializing)
                    {
                        if (SSM_CURRENCIES != null && SSM_CURRENCIES.CURRENCY_ID != value)
                        {
                            SSM_CURRENCIES = null;
                        }
                    }
                    _cOST_CURRENCY = value;
                    OnPropertyChanged("COST_CURRENCY");
                }
            }
        }
        private string _cOST_CURRENCY;
    
        [DataMember]
        public Nullable<System.DateTime> COST_EXCHANGE_RATE_DT
        {
            get { return _cOST_EXCHANGE_RATE_DT; }
            set
            {
                if (_cOST_EXCHANGE_RATE_DT != value)
                {
                    _cOST_EXCHANGE_RATE_DT = value;
                    OnPropertyChanged("COST_EXCHANGE_RATE_DT");
                }
            }
        }
        private Nullable<System.DateTime> _cOST_EXCHANGE_RATE_DT;
    
        [DataMember]
        public Nullable<decimal> COST_EXCHANGE_RATE_AMT
        {
            get { return _cOST_EXCHANGE_RATE_AMT; }
            set
            {
                if (_cOST_EXCHANGE_RATE_AMT != value)
                {
                    _cOST_EXCHANGE_RATE_AMT = value;
                    OnPropertyChanged("COST_EXCHANGE_RATE_AMT");
                }
            }
        }
        private Nullable<decimal> _cOST_EXCHANGE_RATE_AMT;
    
        [DataMember]
        public Nullable<decimal> COST_AMOUNT
        {
            get { return _cOST_AMOUNT; }
            set
            {
                if (_cOST_AMOUNT != value)
                {
                    _cOST_AMOUNT = value;
                    OnPropertyChanged("COST_AMOUNT");
                }
            }
        }
        private Nullable<decimal> _cOST_AMOUNT;
    
        [DataMember]
        public string ENABLED_FLAG
        {
            get { return _eNABLED_FLAG; }
            set
            {
                if (_eNABLED_FLAG != value)
                {
                    _eNABLED_FLAG = value;
                    OnPropertyChanged("ENABLED_FLAG");
                }
            }
        }
        private string _eNABLED_FLAG;
    
        [DataMember]
        public string WORKFLOW_COMPLETION_STATUS
        {
            get { return _wORKFLOW_COMPLETION_STATUS; }
            set
            {
                if (_wORKFLOW_COMPLETION_STATUS != value)
                {
                    _wORKFLOW_COMPLETION_STATUS = value;
                    OnPropertyChanged("WORKFLOW_COMPLETION_STATUS");
                }
            }
        }
        private string _wORKFLOW_COMPLETION_STATUS;
    
        [DataMember]
        public string CREATED_BY
        {
            get { return _cREATED_BY; }
            set
            {
                if (_cREATED_BY != value)
                {
                    _cREATED_BY = value;
                    OnPropertyChanged("CREATED_BY");
                }
            }
        }
        private string _cREATED_BY;
    
        [DataMember]
        public System.DateTime CREATED_DATE
        {
            get { return _cREATED_DATE; }
            set
            {
                if (_cREATED_DATE != value)
                {
                    _cREATED_DATE = value;
                    OnPropertyChanged("CREATED_DATE");
                }
            }
        }
        private System.DateTime _cREATED_DATE;
    
        [DataMember]
        public string MODIFIED_BY
        {
            get { return _mODIFIED_BY; }
            set
            {
                if (_mODIFIED_BY != value)
                {
                    _mODIFIED_BY = value;
                    OnPropertyChanged("MODIFIED_BY");
                }
            }
        }
        private string _mODIFIED_BY;
    
        [DataMember]
        public Nullable<System.DateTime> MODIFIED_DATE
        {
            get { return _mODIFIED_DATE; }
            set
            {
                if (_mODIFIED_DATE != value)
                {
                    _mODIFIED_DATE = value;
                    OnPropertyChanged("MODIFIED_DATE");
                }
            }
        }
        private Nullable<System.DateTime> _mODIFIED_DATE;
    
        [DataMember]
        public string ATTRIBUTE1
        {
            get { return _aTTRIBUTE1; }
            set
            {
                if (_aTTRIBUTE1 != value)
                {
                    _aTTRIBUTE1 = value;
                    OnPropertyChanged("ATTRIBUTE1");
                }
            }
        }
        private string _aTTRIBUTE1;
    
        [DataMember]
        public string ATTRIBUTE2
        {
            get { return _aTTRIBUTE2; }
            set
            {
                if (_aTTRIBUTE2 != value)
                {
                    _aTTRIBUTE2 = value;
                    OnPropertyChanged("ATTRIBUTE2");
                }
            }
        }
        private string _aTTRIBUTE2;
    
        [DataMember]
        public string ATTRIBUTE3
        {
            get { return _aTTRIBUTE3; }
            set
            {
                if (_aTTRIBUTE3 != value)
                {
                    _aTTRIBUTE3 = value;
                    OnPropertyChanged("ATTRIBUTE3");
                }
            }
        }
        private string _aTTRIBUTE3;
    
        [DataMember]
        public string ATTRIBUTE4
        {
            get { return _aTTRIBUTE4; }
            set
            {
                if (_aTTRIBUTE4 != value)
                {
                    _aTTRIBUTE4 = value;
                    OnPropertyChanged("ATTRIBUTE4");
                }
            }
        }
        private string _aTTRIBUTE4;
    
        [DataMember]
        public string ATTRIBUTE5
        {
            get { return _aTTRIBUTE5; }
            set
            {
                if (_aTTRIBUTE5 != value)
                {
                    _aTTRIBUTE5 = value;
                    OnPropertyChanged("ATTRIBUTE5");
                }
            }
        }
        private string _aTTRIBUTE5;
    
        [DataMember]
        public string ATTRIBUTE6
        {
            get { return _aTTRIBUTE6; }
            set
            {
                if (_aTTRIBUTE6 != value)
                {
                    _aTTRIBUTE6 = value;
                    OnPropertyChanged("ATTRIBUTE6");
                }
            }
        }
        private string _aTTRIBUTE6;
    
        [DataMember]
        public string ATTRIBUTE7
        {
            get { return _aTTRIBUTE7; }
            set
            {
                if (_aTTRIBUTE7 != value)
                {
                    _aTTRIBUTE7 = value;
                    OnPropertyChanged("ATTRIBUTE7");
                }
            }
        }
        private string _aTTRIBUTE7;
    
        [DataMember]
        public string ATTRIBUTE8
        {
            get { return _aTTRIBUTE8; }
            set
            {
                if (_aTTRIBUTE8 != value)
                {
                    _aTTRIBUTE8 = value;
                    OnPropertyChanged("ATTRIBUTE8");
                }
            }
        }
        private string _aTTRIBUTE8;
    
        [DataMember]
        public string ATTRIBUTE9
        {
            get { return _aTTRIBUTE9; }
            set
            {
                if (_aTTRIBUTE9 != value)
                {
                    _aTTRIBUTE9 = value;
                    OnPropertyChanged("ATTRIBUTE9");
                }
            }
        }
        private string _aTTRIBUTE9;
    
        [DataMember]
        public string ATTRIBUTE10
        {
            get { return _aTTRIBUTE10; }
            set
            {
                if (_aTTRIBUTE10 != value)
                {
                    _aTTRIBUTE10 = value;
                    OnPropertyChanged("ATTRIBUTE10");
                }
            }
        }
        private string _aTTRIBUTE10;
    
        [DataMember]
        public Nullable<decimal> COVERED_KFAS
        {
            get { return _cOVERED_KFAS; }
            set
            {
                if (_cOVERED_KFAS != value)
                {
                    _cOVERED_KFAS = value;
                    OnPropertyChanged("COVERED_KFAS");
                }
            }
        }
        private Nullable<decimal> _cOVERED_KFAS;

        #endregion
        #region Navigation Properties
    
        [DataMember]
        public HR_TRAINING_COST_HDR HR_TRAINING_COST_HDR
        {
            get { return _hR_TRAINING_COST_HDR; }
            set
            {
                if (!ReferenceEquals(_hR_TRAINING_COST_HDR, value))
                {
                    var previousValue = _hR_TRAINING_COST_HDR;
                    _hR_TRAINING_COST_HDR = value;
                    FixupHR_TRAINING_COST_HDR(previousValue);
                    OnNavigationPropertyChanged("HR_TRAINING_COST_HDR");
                }
            }
        }
        private HR_TRAINING_COST_HDR _hR_TRAINING_COST_HDR;
    
        [DataMember]
        public SSM_CURRENCIES SSM_CURRENCIES
        {
            get { return _sSM_CURRENCIES; }
            set
            {
                if (!ReferenceEquals(_sSM_CURRENCIES, value))
                {
                    var previousValue = _sSM_CURRENCIES;
                    _sSM_CURRENCIES = value;
                    FixupSSM_CURRENCIES(previousValue);
                    OnNavigationPropertyChanged("SSM_CURRENCIES");
                }
            }
        }
        private SSM_CURRENCIES _sSM_CURRENCIES;

        #endregion
        #region ChangeTracking
    
        protected virtual void OnPropertyChanged(String propertyName)
        {
            if (ChangeTracker.State != ObjectState.Added && ChangeTracker.State != ObjectState.Deleted)
            {
                ChangeTracker.State = ObjectState.Modified;
            }
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        protected virtual void OnNavigationPropertyChanged(String propertyName)
        {
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        event PropertyChangedEventHandler INotifyPropertyChanged.PropertyChanged{ add { _propertyChanged += value; } remove { _propertyChanged -= value; } }
        private event PropertyChangedEventHandler _propertyChanged;
        private ObjectChangeTracker _changeTracker;
    
        [DataMember]
        public ObjectChangeTracker ChangeTracker
        {
            get
            {
                if (_changeTracker == null)
                {
                    _changeTracker = new ObjectChangeTracker();
                    _changeTracker.ObjectStateChanging += HandleObjectStateChanging;
                }
                return _changeTracker;
            }
            set
            {
                if(_changeTracker != null)
                {
                    _changeTracker.ObjectStateChanging -= HandleObjectStateChanging;
                }
                _changeTracker = value;
                if(_changeTracker != null)
                {
                    _changeTracker.ObjectStateChanging += HandleObjectStateChanging;
                }
            }
        }
    
        private void HandleObjectStateChanging(object sender, ObjectStateChangingEventArgs e)
        {
            if (e.NewState == ObjectState.Deleted)
            {
                ClearNavigationProperties();
            }
        }
    
        protected bool IsDeserializing { get; private set; }
    
        [OnDeserializing]
        public void OnDeserializingMethod(StreamingContext context)
        {
            IsDeserializing = true;
        }
    
        [OnDeserialized]
        public void OnDeserializedMethod(StreamingContext context)
        {
            IsDeserializing = false;
            ChangeTracker.ChangeTrackingEnabled = true;
        }
    
        protected virtual void ClearNavigationProperties()
        {
            HR_TRAINING_COST_HDR = null;
            SSM_CURRENCIES = null;
        }

        #endregion
        #region Association Fixup
    
        private void FixupHR_TRAINING_COST_HDR(HR_TRAINING_COST_HDR previousValue, bool skipKeys = false)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (previousValue != null && previousValue.HR_TRAINING_COST_DTL.Contains(this))
            {
                previousValue.HR_TRAINING_COST_DTL.Remove(this);
            }
    
            if (HR_TRAINING_COST_HDR != null)
            {
                if (!HR_TRAINING_COST_HDR.HR_TRAINING_COST_DTL.Contains(this))
                {
                    HR_TRAINING_COST_HDR.HR_TRAINING_COST_DTL.Add(this);
                }
    
                COST_HDR_ID = HR_TRAINING_COST_HDR.COST_HDR_ID;
            }
            else if (!skipKeys)
            {
                COST_HDR_ID = null;
            }
    
            if (ChangeTracker.ChangeTrackingEnabled)
            {
                if (ChangeTracker.OriginalValues.ContainsKey("HR_TRAINING_COST_HDR")
                    && (ChangeTracker.OriginalValues["HR_TRAINING_COST_HDR"] == HR_TRAINING_COST_HDR))
                {
                    ChangeTracker.OriginalValues.Remove("HR_TRAINING_COST_HDR");
                }
                else
                {
                    ChangeTracker.RecordOriginalValue("HR_TRAINING_COST_HDR", previousValue);
                }
                if (HR_TRAINING_COST_HDR != null && !HR_TRAINING_COST_HDR.ChangeTracker.ChangeTrackingEnabled)
                {
                    HR_TRAINING_COST_HDR.StartTracking();
                }
            }
        }
    
        private void FixupSSM_CURRENCIES(SSM_CURRENCIES previousValue, bool skipKeys = false)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (previousValue != null && previousValue.HR_TRAINING_COST_DTL.Contains(this))
            {
                previousValue.HR_TRAINING_COST_DTL.Remove(this);
            }
    
            if (SSM_CURRENCIES != null)
            {
                if (!SSM_CURRENCIES.HR_TRAINING_COST_DTL.Contains(this))
                {
                    SSM_CURRENCIES.HR_TRAINING_COST_DTL.Add(this);
                }
    
                COST_CURRENCY = SSM_CURRENCIES.CURRENCY_ID;
            }
            else if (!skipKeys)
            {
                COST_CURRENCY = null;
            }
    
            if (ChangeTracker.ChangeTrackingEnabled)
            {
                if (ChangeTracker.OriginalValues.ContainsKey("SSM_CURRENCIES")
                    && (ChangeTracker.OriginalValues["SSM_CURRENCIES"] == SSM_CURRENCIES))
                {
                    ChangeTracker.OriginalValues.Remove("SSM_CURRENCIES");
                }
                else
                {
                    ChangeTracker.RecordOriginalValue("SSM_CURRENCIES", previousValue);
                }
                if (SSM_CURRENCIES != null && !SSM_CURRENCIES.ChangeTracker.ChangeTrackingEnabled)
                {
                    SSM_CURRENCIES.StartTracking();
                }
            }
        }

        #endregion
    }
}
