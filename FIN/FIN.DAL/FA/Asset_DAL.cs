﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.FA
{
    public class Asset_DAL
    {
        static string sqlQuery = "";

        public static string getAssetReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_ASSET V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["ASSET_MST_ID"] != null)
                {
                    sqlQuery += " AND V.ASSET_MST_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["ASSET_MST_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["ASSET_LOCATION_ID"] != null)
                {
                    sqlQuery += " AND V.ASSET_LOCATION_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["ASSET_LOCATION_ID"].ToString() + "'";
                }
            }
            return sqlQuery;
        }

        public static string getAssetDiscardReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_ASSET_DISCARD V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["ASSET_DISCARD_ID"] != null)
                {
                    sqlQuery += " AND V.ASSET_DISCARD_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["ASSET_DISCARD_ID"].ToString() + "'";
                }
            }
            return sqlQuery;
        }

        public static string getAssetTransferReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_ASSET_TRANSFER V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["ASSET_MST_ID"] != null)
                {
                    sqlQuery += " AND V.ASSET_MST_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["ASSET_MST_ID"].ToString() + "'";
                }
            }
            return sqlQuery;
        }

        public static string getAssetDepreciationReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_ASSET_DEPRECIATION V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["PERIOD_ID"] != null)
                {
                    sqlQuery += " AND V.PERIOD_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["PERIOD_ID"].ToString() + "'";
                }
            }
            return sqlQuery;
        }

        public static string GetAssetNumberAssetName()
        {
            sqlQuery = string.Empty;

            sqlQuery = "  select AM.ASSET_MST_ID,AM.ASSET_NUMBER || ' - ' || AM.ASSET_NAME" + VMVServices.Web.Utils.LanguageCode + "  as ASSET_NAME";
            sqlQuery += " from ast_asset_mst am";
            sqlQuery += " where am.workflow_completion_status=1";
            sqlQuery += " ORDER BY am.asset_number asc ";

            return sqlQuery;
        }

        public static string GetAssetLocation()
        {
            sqlQuery = string.Empty;

            sqlQuery = " select ALM.LOCATION_NAME, ALM.ASSET_LOCATION_ID from ast_location_mst alm order by ALM.LOCATION_NAME";
            
            return sqlQuery;
        }

    }
}

