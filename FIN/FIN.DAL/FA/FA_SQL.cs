﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Collections;

using System.Data;
using System.Web;

namespace FIN.DAL.FA
{
    public class FA_SQL
    {
        static string sqlQuery = string.Empty;
        static string AllFieldsOperator { get { return " * "; } }
        static string DistinctFieldsOperator { get { return " DISTINCT "; } }


        #region "Common"
        /// <summary>
        /// common methods for entire modules
        /// </summary>
        /// <param name="condition"></param>
        /// <returns></returns>

        public static string ValidateEvaluationCondition(string condition)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select count(1) as counts from dual where " + condition;

            return sqlQuery;
        }
        public static string IsValueExists(string tableName, string IDColumn, int IDValue, string fieldName, string fieldvalue)
        {
            sqlQuery = string.Empty;
            sqlQuery = "select count(1) as count from " + tableName + " where " + IDColumn + "<>" + IDValue.ToString() + " and UPPER(" + fieldName + ")='" + fieldvalue.ToString().ToUpper() + "'";
            return sqlQuery;
        }
        public static string IsValueExistsBasedOrganization(string tableName, string IDColumn, int IDValue, string fieldName, string fieldvalue)
        {
            sqlQuery = string.Empty;
            sqlQuery = "select count(1) as count from " + tableName + " where organization_master_id ='" + VMVServices.Web.Utils.OrganizationID + "' and " + IDColumn + "<>" + IDValue.ToString() + " and UPPER(" + fieldName + ")='" + fieldvalue.ToString().ToUpper() + "'";
            return sqlQuery;
        }
        public static string SelectSql(string tableName, string fieldList, string whereClause, string groupByClause, string orderByClause, bool selectDistinct)
        {
            if (fieldList == null || fieldList == string.Empty) fieldList = AllFieldsOperator;
            if (selectDistinct) fieldList = string.Concat(DistinctFieldsOperator, fieldList);
            if (whereClause == null) whereClause = string.Empty;
            if (groupByClause == null) groupByClause = string.Empty;
            if (orderByClause != null && orderByClause != string.Empty) orderByClause = string.Concat(" ORDER BY ", orderByClause);

            string sql = string.Concat("SELECT ", fieldList, " FROM ", tableName);
            if (whereClause != string.Empty) sql += string.Concat(" WHERE ", whereClause);
            if (groupByClause != string.Empty) sql += string.Concat(" GROUP BY ", groupByClause);
            sql += orderByClause;
            return sql;
        }
        public static string GetMenuList(long groupid, long moduleId)
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT AM.MODULEKEYID,AM.MODULENAME ";
            sqlQuery += " ,AMM.MENU_KEY_ID,AMM.MENU_DESCRIPTION,AMM.MENU_URL ";
            sqlQuery += " ,GAD.GROUP_ID,GAD.ENABLED_FLAG,GAD.ADD_FLAG,GAD.EDIT_FLAG,GAD.DELETE_FLAG ";
            sqlQuery += " FROM GROUP_APP_DTL GAD ";
            sqlQuery += " INNER JOIN AON_MENU_MST AMM ON AMM.MENU_KEY_ID= GAD.MENU_KEY_ID ";
            sqlQuery += " INNER JOIN AON_MODULE_MST AM ON AM.MODULEKEYID = AMM.MODULE_ID ";
            sqlQuery += " WHERE GAD.GROUP_ID=" + groupid;
            sqlQuery += " and AM.MODULEKEYID=" + moduleId;
            sqlQuery += " ORDER BY AM.ORDERNO,AMM.ORDER_NO";
            return sqlQuery;
        }
        public static string GetModuleList(long groupId, long moduleId)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select mm.modulename as module_name,mm.modulekeyid as module_key_id";
            sqlQuery += " from aon_module_mst mm";
            sqlQuery += " where mm.modulekeyid in";
            sqlQuery += " (select m.module_id";
            sqlQuery += " from aon_menu_mst m";
            sqlQuery += " where m.menu_key_id in (select ga.menu_key_id";
            sqlQuery += "  from group_app_dtl ga";
            sqlQuery += " where ga.group_id = " + groupId + ")) and mM.MODULEKEYID=" + moduleId + " order by to_number(mm.orderno) asc";

            return sqlQuery;
        }
        public static string GetLookupData(string typeName)
        {
            sqlQuery = string.Empty;

            sqlQuery = "Select V.VALUE_KEY_ID,V.VALUE_NAME  From SYS_LOOKUP_VALUE_DTLS V,SYS_LOOKUP_TYPE_MASTER M";
            sqlQuery += " WHERE upper(M.TYPE_NAME) = upper('" + typeName + "')";
            sqlQuery += " AND V.TYPE_KEY_ID =  M.TYPE_KEY_ID";
            sqlQuery += " ORDER BY V.DISPLAY_ORDER";
            return sqlQuery;

        }
        #endregion

        #region "Fixed Assets"

        /// <summary>
        /// Get the asset major and minor categories
        /// </summary>
        /// <param name="condition"></param>
        /// <returns></returns>
        public static string GetAssetCategoriesList(Hashtable htParameters, string modifyURL, string deleteURL, int prgId)
        {
            sqlQuery = string.Empty;

            sqlQuery = "select ";
            sqlQuery += "  mc.category_name,";
            sqlQuery += " mc.category_desc,";
            sqlQuery += " md.minor_category_name,";
            sqlQuery += " md.minor_category_desc,";

            sqlQuery += "  'AssetCategoryEntry.aspx?ProgramID=" + prgId + "' || Chr(38) || 'Mode=U' ||";
            sqlQuery += " Chr(38) || 'ID=' || mc.MAJOR_CATEGORY_ID as ModifyURL,";
            sqlQuery += "  'AssetCategoryEntry.aspx?ProgramID=" + prgId + "' || Chr(38) || 'Mode=D' ||";
            sqlQuery += "  Chr(38) || 'ID=' || mc.MAJOR_CATEGORY_ID as DeleteURL";


            sqlQuery += " from ast_major_category_mst mc, ast_minor_category_dtl md";
            sqlQuery += " where md.major_category_id = mc.major_category_id and mc.ENABLED_FLAG = 'Y'";

            if (htParameters != null)
            {
                if (htParameters.Count > 0)
                {
                    if (htParameters[FINColumnConstants.MAJOR_CATEGORY_ID] != null && int.Parse(htParameters[FINColumnConstants.MAJOR_CATEGORY_ID].ToString()) > 0)
                    {
                        sqlQuery += " and mc.MAJOR_CATEGORY_ID =" + htParameters[FINColumnConstants.MAJOR_CATEGORY_ID];
                    }

                }
            }
            if (htParameters != null)
            {
                if (htParameters.Count > 0)
                {
                    if (htParameters[FINColumnConstants.MINOR_CATEGORY_ID] != null && int.Parse(htParameters[FINColumnConstants.MINOR_CATEGORY_ID].ToString()) > 0)
                    {
                        sqlQuery += " and md.MINOR_CATEGORY_ID =" + htParameters[FINColumnConstants.MINOR_CATEGORY_ID];
                    }
                }
            }

            sqlQuery += " order by mc.MAJOR_CATEGORY_ID desc";
            return sqlQuery;

        }
        /// <summary>
        /// Get the asset major and minor categories based on the major category id
        /// </summary>
        /// <param name="condition"></param>
        /// <returns></returns>
        public static string GetAssetCategoriesWithData(long major_category_id)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select m.major_category_id,";
            sqlQuery += "  m.category_name,";
            sqlQuery += "  m.category_desc,";
            sqlQuery += "  m.enabled_flag,";
            sqlQuery += "  md.MINOR_CATEGORY_ID,";
            sqlQuery += "  case md.enabled_flag when '1' then 'TRUE' ELSE 'FALSE' END AS MINOR_enabled_flag,";
            sqlQuery += "  md.minor_category_name,md.minor_category_name_ol,";
            sqlQuery += "  md.minor_category_desc,MD.CREATED_DATE,MD.CREATE_BY";
            sqlQuery += "  from ast_major_category_mst m, ast_minor_category_dtl md";
            sqlQuery += "  where m.major_category_id = md.major_category_id";
            sqlQuery += "  and m.major_category_id=" + major_category_id;

            return sqlQuery;
        }

        //public static string GetBaseCurrencyEffectiveDate(long currency_id,DateTime Eff_date)
        //{
        //    sqlQuery = string.Empty;

        //    sqlQuery = "select count(*)";
        //    sqlQuery += " from ast_asset_mst M";
        //    sqlQuery += " where M.base_currency_id IN";
        //    sqlQuery += " (select b.base_currency_id";
        //    sqlQuery += " from fin_base_currency b";
        //    sqlQuery += " where b.CURRENCY_ID = " + currency_id;
        //    sqlQuery += " and trunc(to_date(b.effective_date, 'dd-MM-yyyy')) > trunc(to_date('" + Eff_date.ToString("dd-MM-yyyy") + "','dd-MM-yyyy')))";
        //    sqlQuery += " and trunc(to_date(m.CREATED_DATE, 'dd-MM-yyyy'))  > trunc(to_date('" + Eff_date.ToString("dd-MM-yyyy") + "','dd-mm-yyyy'))";


        //    return sqlQuery;
        //}

        public static string GetBaseCurrencyEffectiveDate(long currency_id, DateTime Eff_date, long RecordID)
        {
            sqlQuery = string.Empty;

            sqlQuery = "select b.base_currency_id";
            sqlQuery += " from fin_base_currency b";
            sqlQuery += " where b.CURRENCY_ID = " + currency_id;
            sqlQuery += " and b.BASE_CURRENCY_ID <>" + RecordID;
            sqlQuery += " and b.effective_date > trunc(to_date('" + Eff_date.ToString("dd-MM-yyyy") + "','dd-MM-yyyy'))";

            return sqlQuery;
        }
        public static string GetAssetLocationBasedAsset(long assetId)
        {
            sqlQuery = string.Empty;
            sqlQuery = "    select d.* ";
            sqlQuery += "  from ast_asset_location_dtl d";
            sqlQuery += "  where d.asset_mst_id='" + assetId + "'";
            return sqlQuery;
        }
        public static string GetDepreciationAssetCategoriesEffectiveDate(long major_category_id, long dprn_method_mst_id, DateTime Eff_date)
        {
            sqlQuery = string.Empty;

            sqlQuery = "select dc.dprn_category_id";
            sqlQuery += " from AST_DPRN_CATEGORY_CONFIG dc";
            sqlQuery += " where dc.major_category_id =  " + major_category_id;
            sqlQuery += " and dc.dprn_method_mst_id =  " + dprn_method_mst_id;
            sqlQuery += " and dc.effect_from > trunc(to_date('" + Eff_date.ToString("dd-MM-yyyy") + "','dd-MM-yyyy'))";

            return sqlQuery;
        }

        public static string Assetoutwardtoserviceoutwarddate(long asset_mst_id, long RecordID, DateTime outwardDate, string mode)
        {
            sqlQuery = string.Empty;



            sqlQuery = "select count(1) AS COUNT";
            sqlQuery += " from ast_asset_outward_service_dtl od,ast_asset_inward_service i";
            sqlQuery += " where od.outward_service_id = i.outward_service_id";
            sqlQuery += " AND OD.ASSET_MST_ID=I.ASSET_MST_ID";
            sqlQuery += " and i.asset_mst_id =  " + asset_mst_id;
            if (mode == "A")
            {
                sqlQuery += " and (select max(inward_date)";
                sqlQuery += " from ast_asset_inward_service s";
                sqlQuery += "  where ASSET_MST_ID =  " + asset_mst_id + ")";
                sqlQuery += "  > trunc(to_date('" + outwardDate.ToString("dd-MM-yyyy") + "','dd-MM-yyyy'))";
            }
            else if (mode == "U")
            {
                sqlQuery += " and od.outward_service_id <>  " + RecordID;
                sqlQuery += " and (select max(outward_date) from ast_asset_outward_service_dtl o where o.asset_mst_id = " + asset_mst_id + ")";
                sqlQuery += " < (select max(inward_date) from ast_asset_inward_service s where ASSET_MST_ID = " + asset_mst_id + ")";
            }

            return sqlQuery;
        }

        public static string GetBaseCurrencyAssetcreateDate(DateTime Eff_date)
        {
            sqlQuery = string.Empty;

            sqlQuery = "select ASSET_MST_ID";
            sqlQuery += " from ast_asset_mst M";
            sqlQuery += " where m.CREATED_DATE > trunc(to_date('" + Eff_date.ToString("dd-MM-yyyy") + "','dd-MM-yyyy'))";

            return sqlQuery;
        }


        /// <summary>
        /// Get the asset major and minor categories based on the major category id
        /// </summary>
        /// <param name="condition"></param>
        /// <returns></returns>
        public static string GetAssetCategoriesWithData()
        {
            sqlQuery = string.Empty;

            sqlQuery = "select m.major_category_id,";
            sqlQuery += " m.category_name,";
            sqlQuery += "  m.category_desc,";
            sqlQuery += " m.enabled_flag,";
            sqlQuery += "  md.MINOR_CATEGORY_ID,";
            sqlQuery += "   mD.enabled_flag AS MINOR_enabled_flag,md.minor_category_name,";
            sqlQuery += " md.minor_category_desc,MD.CREATED_DATE,MD.CREATE_BY";
            sqlQuery += "  from ast_major_category_mst m, ast_minor_category_dtl md";
            sqlQuery += " where m.major_category_id = md.major_category_id";

            return sqlQuery;
        }
        public static string GetDepreciationMethodList(long RecordID)
        {
            sqlQuery = string.Empty;

            sqlQuery = "select DM.DPRN_METHOD_MST_ID,DM.DPRN_NAME,DM.DPRN_NAME_OL,DM.DPRN_DESC,VD.Code AS DPRN_METHOD_ID,VD.Code AS DPRN_METHOD_NAME,DM.DPRN_RATE,VD1.Code AS DPRN_TYPE_DURATION_ID,VD1.Code  AS DPRN_TYPE_DURATION_NAME,DM.DPRN_TYPE_DURATION";
            sqlQuery += " from AST_DEPRECIATION_METHOD_MST DM,ssm_code_masters VD,ssm_code_masters VD1";
            sqlQuery += " WHERE VD.Code = DM.DPRN_METHOD_ID";
            sqlQuery += " AND VD1.Code = DM.DPRN_TYPE_DURATION_ID";
            sqlQuery += " AND DM.DPRN_METHOD_MST_ID = " + RecordID;
            return sqlQuery;

        }

        public static string GetCurrencyList(long RecordID)
        {
            sqlQuery = string.Empty;

            sqlQuery = "select c.currency_id,c.currency_code,c.currency_desc,c.currency_symbol,l.value_key_id as initial_amt_sep_id,";
            sqlQuery += " l.value_name as initial_amt_sep_name,ld.value_key_id as subsequent_amt_sep_id,ld.value_name as";
            sqlQuery += " subsequent_amt_sep_name,c.decimal_precision";
            sqlQuery += " from FIN_CURRENCY_MST c,SYS_LOOKUP_VALUE_DTLS l,SYS_LOOKUP_VALUE_DTLS ld";
            sqlQuery += " where c.initial_amt_sep = l.value_key_id";
            sqlQuery += " and c.subsequent_amt_sep = ld.value_key_id";
            sqlQuery += " and currency_id = " + RecordID;
            return sqlQuery;

        }

        public static string GetCurrencyDataList(Hashtable htParameters, string modifyURL, string deleteURL)
        {
            sqlQuery = string.Empty;

            sqlQuery = "select c.currency_code,c.currency_desc,";
            sqlQuery += " l.value_name as initial_amt_sep_name,ld.value_name as subsequent_amt_sep_name,c.decimal_precision";
            sqlQuery += ", " + modifyURL + "," + deleteURL;
            sqlQuery += " from FIN_CURRENCY_MST c,SYS_LOOKUP_VALUE_DTLS l,SYS_LOOKUP_VALUE_DTLS ld";
            sqlQuery += " where c.initial_amt_sep = l.value_key_id";
            sqlQuery += " and c.subsequent_amt_sep = ld.value_key_id";
            if (htParameters != null)
            {
                if (htParameters.Count > 0)
                {
                    if (htParameters[FINColumnConstants.INITIAL_AMT_SEP_ID] != null)
                    {
                        sqlQuery += " and c.initial_amt_sep = " + htParameters[FINColumnConstants.INITIAL_AMT_SEP_ID];
                    }
                }
            }

            if (htParameters != null)
            {
                if (htParameters.Count > 0)
                {
                    if (htParameters[FINColumnConstants.SUBSEQUENT_AMT_SEP_ID] != null)
                    {
                        sqlQuery += " and c.subsequent_amt_sep = " + htParameters[FINColumnConstants.SUBSEQUENT_AMT_SEP_ID];
                    }
                }
            }

            sqlQuery += " order by c.currency_id desc";

            return sqlQuery;

        }

        public static string GetDepreciationAssetCategoriesDataList(Hashtable htParameters, string modifyURL, string deleteURL)
        {
            sqlQuery = string.Empty;

            sqlQuery = "select mc.category_name,dm.dprn_name,dc.effect_from";
            sqlQuery += ", " + modifyURL + "," + deleteURL;
            sqlQuery += " from AST_DPRN_CATEGORY_CONFIG dc,AST_MAJOR_CATEGORY_MST mc,AST_DEPRECIATION_METHOD_MST dm";
            sqlQuery += " where dc.major_category_id = mc.major_category_id";
            sqlQuery += " and dc.dprn_method_mst_id = dm.dprn_method_mst_id";
            if (htParameters != null)
            {
                if (htParameters.Count > 0)
                {
                    if (htParameters[FINColumnConstants.MAJOR_CATEGORY_ID] != null)
                    {
                        sqlQuery += " and dc.major_category_id = " + htParameters[FINColumnConstants.MAJOR_CATEGORY_ID];
                    }
                }
            }

            if (htParameters != null)
            {
                if (htParameters.Count > 0)
                {
                    if (htParameters[FINColumnConstants.DPRN_METHOD_MST_ID] != null)
                    {
                        sqlQuery += " and dc.dprn_method_mst_id = " + htParameters[FINColumnConstants.DPRN_METHOD_MST_ID];
                    }
                }
            }

            sqlQuery += " order by dc.dprn_category_id desc";
            return sqlQuery;

        }

        public static string GetAssetoutwardtoserviceDataList(Hashtable htParameters, string modifyURL, string deleteURL)
        {
            sqlQuery = string.Empty;

            sqlQuery = "select a.asset_name,o.outward_date,t.third_party_name,o.service_provider_ref,o.service_reference,o.exp_delivery_date,o.approx_cost";
            sqlQuery += ", " + modifyURL + "," + deleteURL;
            sqlQuery += " from AST_ASSET_OUTWARD_SERVICE_DTL o,AST_ASSET_MST a,iacademe.acd_third_party_master t";
            sqlQuery += " where a.asset_mst_id = o.asset_mst_id";
            sqlQuery += " and t.third_party_id = o.third_party_id";

            if (htParameters != null)
            {
                if (htParameters.Count > 0)
                {
                    if (htParameters[FINColumnConstants.ASSET_MST_ID] != null)
                    {
                        sqlQuery += " and a.asset_mst_id = " + htParameters[FINColumnConstants.ASSET_MST_ID];
                    }
                }
            }

            if (htParameters != null)
            {
                if (htParameters.Count > 0)
                {
                    if (htParameters[FINColumnConstants.DPRN_METHOD_MST_ID] != null)
                    {
                        sqlQuery += " and t.third_party_id = " + htParameters[FINColumnConstants.THIRD_PARTY_ID];
                    }
                }
            }

            sqlQuery += " order by o.outward_service_id desc";
            return sqlQuery;

        }


        public static string GetBaseCurrencyDataList(Hashtable htParameters, string modifyURL, string deleteURL)
        {
            sqlQuery = string.Empty;

            sqlQuery = "select c.currency_code,c.currency_desc,bc.effective_date";
            sqlQuery += ", " + modifyURL + "," + deleteURL;
            sqlQuery += " from FIN_BASE_CURRENCY bc,FIN_CURRENCY_MST c";
            sqlQuery += " where bc.currency_id = c.currency_id";

            if (htParameters != null)
            {
                if (htParameters.Count > 0)
                {
                    if (htParameters[FINColumnConstants.CURRENCY_ID] != null)
                    {
                        sqlQuery += " and bc.currency_id = " + htParameters[FINColumnConstants.CURRENCY_ID];
                    }
                }
            }

            sqlQuery += " order by bc.base_currency_id desc";

            return sqlQuery;

        }


        public static string GetCurrencyInitial()
        {
            sqlQuery = string.Empty;

            sqlQuery = "select l.value_key_id as initial_amt_sep_id,";
            sqlQuery += " l.value_name as initial_amt_sep_name";
            sqlQuery += " from FIN_CURRENCY_MST c,SYS_LOOKUP_VALUE_DTLS l";
            sqlQuery += " where c.initial_amt_sep = l.value_key_id";


            return sqlQuery;

        }

        public static string GetCurrencySubsequent()
        {
            sqlQuery = string.Empty;

            sqlQuery = "select ld.value_key_id as subsequent_amt_sep_id,ld.value_name as";
            sqlQuery += " subsequent_amt_sep_name";
            sqlQuery += " from FIN_CURRENCY_MST c,SYS_LOOKUP_VALUE_DTLS ld";
            sqlQuery += " where c.subsequent_amt_sep = ld.value_key_id";

            return sqlQuery;

        }

        public static string GetDepreciationDataList(Hashtable htParameters, string modifyURL, string deleteURL)
        {
            sqlQuery = string.Empty;

            sqlQuery = "select DM.DPRN_NAME,DM.DPRN_DESC,VD.VALUE_NAME AS DPRN_METHOD_NAME,DM.DPRN_RATE,";
            sqlQuery += " VD1.VALUE_NAME  AS DPRN_TYPE_DURATION_NAME,DM.DPRN_TYPE_DURATION";
            sqlQuery += ", " + modifyURL + "," + deleteURL;
            sqlQuery += " from AST_DEPRECIATION_METHOD_MST DM,IACADEME.SYS_LOOKUP_VALUE_DTLS VD,IACADEME.SYS_LOOKUP_VALUE_DTLS VD1";
            sqlQuery += " WHERE VD.VALUE_KEY_ID = DM.DPRN_METHOD_ID";
            sqlQuery += " AND VD1.VALUE_KEY_ID = DM.DPRN_TYPE_DURATION_ID";
            if (htParameters != null)
            {
                if (htParameters.Count > 0)
                {
                    if (htParameters[FINColumnConstants.DPRN_NAME] != null)
                    {
                        sqlQuery += " AND DM.DPRN_NAME = '" + htParameters[FINColumnConstants.DPRN_NAME] + "'";
                    }
                }
            }

            if (htParameters != null)
            {
                if (htParameters.Count > 0)
                {
                    if (htParameters[FINColumnConstants.DPRN_METHOD_ID] != null)
                    {
                        sqlQuery += " AND DM.DPRN_METHOD_ID = " + htParameters[FINColumnConstants.DPRN_METHOD_ID];
                    }
                }
            }

            sqlQuery += " ORDER BY DM.DPRN_NAME";

            return sqlQuery;

        }


        public static string GetAssetLocationDetails(long assetId)
        {
            sqlQuery = string.Empty;

            sqlQuery = "select al.ASSET_LOCATION_DTL_ID,";
            sqlQuery += " al.CURRENT_LOCATION_ID,al.PREVIOUS_LOCATION_ID,";
            sqlQuery += " al.EFFECT_FROM,al.LOCATION_INCHARGE,";
            sqlQuery += " al.DEPARTMENT_DTL,al.END_DATE,";
            sqlQuery += " (select lm.location_name from ast_location_mst lm where lm.ASSET_LOCATION_ID=al.PREVIOUS_LOCATION_ID) as location_name";
            sqlQuery += " from AST_ASSET_LOCATION_DTL al";
            sqlQuery += " where al.END_DATE is null and al.ASSET_MST_ID=" + assetId;

            return sqlQuery;

        }

        public static string GetBaseCurrencyDetails()
        {
            sqlQuery = string.Empty;

            sqlQuery = "select c.currency_id,c.currency_desc";
            sqlQuery += " from FIN_BASE_CURRENCY bc,FIN_CURRENCY_MST c";
            sqlQuery += " where bc.currency_id = c.currency_id";

            return sqlQuery;

        }

        public static string GetDepreciationAssetCategoriesMajorCatid()
        {
            sqlQuery = string.Empty;

            sqlQuery = "select distinct m.major_category_id,m.category_name";
            sqlQuery += " from AST_DPRN_CATEGORY_CONFIG d,AST_MAJOR_CATEGORY_MST m";
            sqlQuery += " where d.major_category_id = m.major_category_id";

            return sqlQuery;

        }

        public static string GetAssetoutwardtoserviceAssetmasterid()
        {
            sqlQuery = string.Empty;

            sqlQuery = "select distinct a.asset_mst_id,a.asset_name";
            sqlQuery += " from AST_ASSET_OUTWARD_SERVICE_DTL o,AST_ASSET_MST a";
            sqlQuery += " where a.asset_mst_id = o.asset_mst_id";
            sqlQuery += " and  o.enabled_flag = '1'";

            return sqlQuery;

        }

        public static string GetAssetoutwardtoservicethirdpartymasterid()
        {
            sqlQuery = string.Empty;

            sqlQuery = "select distinct t.THIRD_PARTY_ID,t.THIRD_PARTY_NAME ";
            sqlQuery += " from ast_asset_outward_service_dtl o,iacademe.acd_third_party_master t";
            sqlQuery += " where t.third_party_id = o.third_party_id";
            sqlQuery += " and  o.enabled_flag = '1'";

            return sqlQuery;

        }

        public static string GetDepreciationAssetCategoriesDeprnmethodmstid()
        {
            sqlQuery = string.Empty;

            sqlQuery = "select distinct dm.dprn_method_mst_id,dm.dprn_name";
            sqlQuery += " from AST_DPRN_CATEGORY_CONFIG d,AST_DEPRECIATION_METHOD_MST dm";
            sqlQuery += " where d.dprn_method_mst_id = dm.dprn_method_mst_id";

            return sqlQuery;

        }

        public static string GetAssetLocation(long RecordID)
        {
            sqlQuery = string.Empty;

            sqlQuery = "select LM.ASSET_LOCATION_ID,LM.LOCATION_NAME,LM.LOCATION_NAME_OL,LM.LOCATION_DESC";
            sqlQuery += " FROM AST_LOCATION_MST LM";
            sqlQuery += " WHERE LM.ASSET_LOCATION_ID = " + RecordID;
            sqlQuery += " order by LM.ASSET_LOCATION_ID desc ";
            return sqlQuery;

        }


        public static string GetAssetLocationList(Hashtable htParameters, string modifyURL, string deleteURL)
        {
            sqlQuery = string.Empty;

            sqlQuery = "SELECT  LM.LOCATION_NAME,LM.LOCATION_DESC";
            sqlQuery += ", " + modifyURL + "," + deleteURL;
            sqlQuery += " FROM AST_LOCATION_MST LM";
            sqlQuery += " WHERE 1=1";
            if (htParameters != null)
            {
                if (htParameters.Count > 0)
                {
                    if (htParameters[FINColumnConstants.LOCATION_NAME] != null)
                    {
                        sqlQuery += " AND LM.LOCATION_NAME = '" + htParameters[FINColumnConstants.LOCATION_NAME] + "'";
                    }
                }
            }

            if (htParameters != null)
            {
                if (htParameters.Count > 0)
                {
                    if (htParameters[FINColumnConstants.LOCATION_DESC] != null)
                    {
                        sqlQuery += " and LM.LOCATION_DESC = '" + htParameters[FINColumnConstants.LOCATION_DESC] + "'";
                    }
                }
            }

            sqlQuery += " ORDER BY ASSET_LOCATION_ID DESC";

            return sqlQuery;

        }

        //GetAssetCAtegory Details
        public static string GetAssetCategory(long RecordID)
        {
            sqlQuery = string.Empty;

            sqlQuery = "SELECT ac.EFFECT_FROM,MCM.MAJOR_CATEGORY_ID,MCD.MINOR_CATEGORY_ID,MCM.CATEGORY_NAME||' - '||MCD.MINOR_CATEGORY_NAME AS CURRENT_CATEGORY,AM.ASSET_MST_ID";
            sqlQuery += " FROM AST_ASSET_CATEGORY_DTL AC,AST_ASSET_MST AM,AST_MAJOR_CATEGORY_MST MCM,AST_MINOR_CATEGORY_DTL MCD";
            sqlQuery += " WHERE AC.ASSET_MST_ID=AM.ASSET_MST_ID";
            sqlQuery += " AND AC.MAJORE_CATEGORY_ID= MCM.MAJOR_CATEGORY_ID";
            sqlQuery += " AND AC.MINOR_CATEGORY_ID= MCD.MINOR_CATEGORY_ID";
            sqlQuery += " AND AC.ASSET_CATEGORY_ID = " + RecordID;
            return sqlQuery;

        }

        public static string GetAssetDetails(string mode = "")
        {
            sqlQuery = string.Empty;

            sqlQuery = "SELECT AM.ASSET_MST_ID,AM.ASSET_NUMBER||' - '||AM.ASSET_NAME AS ASSET_NAME";
            sqlQuery += " FROM AST_ASSET_MST AM";
            sqlQuery += " where am.organization_master_id='" + VMVServices.Web.Utils.OrganizationID + "'";
            if (mode == "A")
            {
                sqlQuery += " and Am.ENABLED_FLAG = '1'";
            }
            return sqlQuery;

        }


        public static string getAssetDiscardReportDtl()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_ASSET_DISCARD V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportFilterParameter["DISCARD_TYPE_ID"] != null)
                {
                    sqlQuery += " AND V.DISCARD_TYPE_ID = '" + VMVServices.Web.Utils.ReportFilterParameter["DISCARD_TYPE_ID"].ToString() + "'";
                }

                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
                {
                    sqlQuery += " AND V.DISCARD_DATE between to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy')";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
                {
                    sqlQuery += " AND to_date('" + VMVServices.Web.Utils.ReportFilterParameter["To_Date"].ToString() + "','dd/MM/yyyy')";
                }


            }
            return sqlQuery;
        }


        public static string GetAssetCategoryList(Hashtable htParameters, string modifyURL, string deleteURL)
        {
            sqlQuery = string.Empty;

            sqlQuery = "SELECT AM.ASSET_NUMBER||' - '||AM.ASSET_NAME AS ASSET_NAME,MCM.CATEGORY_NAME,MCD.MINOR_CATEGORY_NAME";
            sqlQuery += ", " + modifyURL + "," + deleteURL;
            sqlQuery += " FROM AST_ASSET_CATEGORY_DTL AC,AST_ASSET_MST AM,AST_MAJOR_CATEGORY_MST MCM,AST_MINOR_CATEGORY_DTL MCD";
            sqlQuery += " WHERE AC.ASSET_MST_ID=AM.ASSET_MST_ID";
            sqlQuery += " AND AC.MAJORE_CATEGORY_ID= MCM.MAJOR_CATEGORY_ID";
            sqlQuery += " AND AC.MINOR_CATEGORY_ID= MCD.MINOR_CATEGORY_ID";
            sqlQuery += " AND AC.ENABLED_FLAG = '1'";

            if (htParameters != null)
            {
                if (htParameters.Count > 0)
                {
                    if (htParameters[FINColumnConstants.MAJOR_CATEGORY_ID] != null)
                    {
                        sqlQuery += " AND MCM.MAJOR_CATEGORY_ID = " + htParameters[FINColumnConstants.MAJOR_CATEGORY_ID];
                    }
                }
            }

            if (htParameters != null)
            {
                if (htParameters.Count > 0)
                {
                    if (htParameters[FINColumnConstants.MINOR_CATEGORY_ID] != null)
                    {
                        sqlQuery += " AND MCD.MINOR_CATEGORY_ID = " + htParameters[FINColumnConstants.MINOR_CATEGORY_ID];
                    }
                }
            }

            sqlQuery += " ORDER BY ASSET_CATEGORY_ID DESC";

            return sqlQuery;

        }

        public static string GetAssetNameLabel(int Assetid)
        {
            sqlQuery = string.Empty;

            sqlQuery = "SELECT MCM.CATEGORY_NAME||' - '||MCD.MINOR_CATEGORY_NAME AS ASSET_NAME";
            sqlQuery += " FROM AST_ASSET_CATEGORY_DTL AC,AST_ASSET_MST AM,AST_MAJOR_CATEGORY_MST MCM,AST_MINOR_CATEGORY_DTL MCD";
            sqlQuery += " WHERE AC.ASSET_MST_ID=AM.ASSET_MST_ID";
            sqlQuery += " AND AC.MAJORE_CATEGORY_ID= MCM.MAJOR_CATEGORY_ID";
            sqlQuery += " AND AC.MINOR_CATEGORY_ID= MCD.MINOR_CATEGORY_ID";
            sqlQuery += " AND AC.Enabled_Flag = '1'";
            sqlQuery += " AND AM.ASSET_MST_ID = " + Assetid;

            return sqlQuery;

        }

        public static string UpdFlagAssetChange(int Masterid, DateTime dtenddate)
        {
            sqlQuery = string.Empty;
            sqlQuery = "UPDATE AST_ASSET_CATEGORY_DTL AC";
            sqlQuery += " SET AC.Enabled_Flag = 0,AC.END_DATE = trunc(to_date('" + dtenddate.ToString("dd-MM-yyyy") + "','dd-MM-yyyy')-1)";
            sqlQuery += " WHERE AC.ASSET_MST_ID =" + Masterid;
            sqlQuery += " AND AC.END_DATE IS NULL";

            return sqlQuery;
        }

        public static string GetBaseCurrency()
        {
            sqlQuery = string.Empty;
            sqlQuery = "select b.base_currency_id,c.currency_desc from fin_base_currency b,fin_currency_mst c";
            sqlQuery += " where b.currency_id=c.currency_id";
            sqlQuery += " and b.enabled_flag='1'";

            return sqlQuery;
        }

        public static string GetAssetDetail()
        {
            sqlQuery = string.Empty;

            sqlQuery = "SELECT AM.ASSET_MST_ID,AM.ASSET_NAME";
            sqlQuery += " FROM AST_ASSET_INWARD_SERVICE INS,AST_ASSET_MST AM";
            sqlQuery += " WHERE AM.ASSET_MST_ID = INS.ASSET_MST_ID";
            sqlQuery += " AND INS.ENABLED_FLAG = '1'";

            return sqlQuery;

        }

        public static string GetAssetInwardsFromService(Hashtable htParameters, string modifyURL, string deleteURL)
        {
            sqlQuery = string.Empty;

            sqlQuery = "SELECT OS.SERVICE_REFERENCE,AM.ASSET_NAME,INS.INWARD_DATE";
            sqlQuery += ", " + modifyURL + "," + deleteURL;
            sqlQuery += " FROM AST_ASSET_INWARD_SERVICE INS,AST_ASSET_OUTWARD_SERVICE_DTL OS,AST_ASSET_MST AM";
            sqlQuery += " WHERE INS.ASSET_MST_ID = AM.ASSET_MST_ID";
            sqlQuery += " AND INS.OUTWARD_SERVICE_ID = OS.OUTWARD_SERVICE_ID";
            sqlQuery += " AND INS.ENABLED_FLAG = '1'";

            if (htParameters != null)
            {
                if (htParameters.Count > 0)
                {
                    if (htParameters[FINColumnConstants.ASSET_MST_ID] != null)
                    {
                        sqlQuery += " and AM.ASSET_MST_ID = " + htParameters[FINColumnConstants.ASSET_MST_ID];
                    }
                }
            }

            if (htParameters != null)
            {
                if (htParameters.Count > 0)
                {
                    if (htParameters[FINColumnConstants.OUTWARD_SERVICE_ID] != null)
                    {
                        sqlQuery += " AND OS.OUTWARD_SERVICE_ID = " + htParameters[FINColumnConstants.OUTWARD_SERVICE_ID];
                    }
                }
            }

            if (htParameters != null)
            {
                if (htParameters.Count > 0)
                {
                    if (htParameters[FINColumnConstants.INWARD_DATE] != null)
                    {
                        sqlQuery += " AND OS.INWARD_DATE = " + htParameters[FINColumnConstants.INWARD_DATE];
                    }
                }
            }

            sqlQuery += "ORDER BY INWARD_SERVICE_ID DESC";

            return sqlQuery;

        }



        public static string GetAssetList(Hashtable htParameters, string modifyURL, string deleteURL)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select a.asset_mst_id,a.asset_number,a.asset_model,a.asset_name,a.manufacture_dtl,a.orginal_cost";
            sqlQuery += ", " + modifyURL + "," + deleteURL;
            sqlQuery += "  from ast_asset_mst a";
            sqlQuery += "  where a.enabled_flag='1'";
            if (htParameters != null)
            {
                if (htParameters.Count > 0)
                {
                    if (htParameters[FINColumnConstants.ASSET_NUMBER] != null)
                    {
                        sqlQuery += " and a.ASSET_NUMBER = '" + htParameters[FINColumnConstants.ASSET_NUMBER] + "'";
                    }
                }
            }

            if (htParameters != null)
            {
                if (htParameters.Count > 0)
                {
                    if (htParameters[FINColumnConstants.ASSET_MST_ID] != null)
                    {
                        sqlQuery += " AND a.asset_mst_id = '" + htParameters[FINColumnConstants.ASSET_MST_ID] + "'";
                    }
                }
            }
            if (htParameters != null)
            {
                if (htParameters.Count > 0)
                {
                    if (htParameters[FINColumnConstants.ASSET_MODEL] != null)
                    {
                        sqlQuery += " AND a.ASSET_MODEL = '" + htParameters[FINColumnConstants.ASSET_MODEL] + "'";
                    }
                }
            }

            return sqlQuery;

        }

        //Get Dicard details
        public static string GetDiscardList(Hashtable htParameters, string modifyURL, string deleteURL)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select a.*";
            sqlQuery += ", " + modifyURL + "," + deleteURL;
            sqlQuery += "  from ast_asset_depreciation_dtl a";
            sqlQuery += "  where a.enabled_flag='1'";

            if (htParameters != null)
            {
                if (htParameters.Count > 0)
                {
                    if (htParameters[FINColumnConstants.ASSET_MST_ID] != null)
                    {
                        sqlQuery += " and a.asset_mst_id = '" + htParameters[FINColumnConstants.ASSET_MST_ID] + "'";
                    }
                }
            }

            return sqlQuery;

        }

        public static string GetAssetDepDet(int AD_ID)
        {
            sqlQuery = string.Empty;
            sqlQuery += " SELECT * FROM AST_ASSET_DEPRECIATION_DTL ad where ad.asset_depreciation_id=" + AD_ID;
            return sqlQuery;
        }

        public static string GetAssetDepreciationList(string calDtlId, string PeriodId, string categID="")
        {
            sqlQuery = string.Empty;

            sqlQuery = "  select ad.ASSET_DEPRECIATION_ID,(select dm.dprn_name";
            sqlQuery += " from AST_DEPRECIATION_METHOD_MST dm";
            sqlQuery += " where ad.DPRN_METHOD_MST_ID = dm.DPRN_METHOD_MST_ID) as dprn_name,";
            sqlQuery += " ad.dprn_rate,";
            sqlQuery += " ad.processed_on,";
            sqlQuery += " ad.asset_cost,GL_AMOUNT,GL_ACCOUNT,GL_TRANSFER_DATE,";
            sqlQuery += " ad.ASSET_MST_ID,";
            sqlQuery += "  (select am.ASSET_NUMBER";
            sqlQuery += "   from ast_asset_mst am";
            sqlQuery += "   where am.asset_mst_id = ad.asset_mst_id) as ASSET_NUMBER,";
            sqlQuery += "  (select am.asset_name";
            sqlQuery += "   from ast_asset_mst am";
            sqlQuery += "   where am.asset_mst_id = ad.asset_mst_id) as asset_name,ad.GL_AMOUNT as depre_amt";
            sqlQuery += " ,ad.WORKFLOW_COMPLETION_STATUS,ad.POSTED_FLAG,ad.JE_HDR_ID,amcm.category_name ";
            sqlQuery += " from AST_ASSET_DEPRECIATION_DTL ad,ast_major_category_mst amcm";
            sqlQuery += " where ad.CAL_DTL_ID='" + calDtlId + "'";
            sqlQuery += " AND ad.PERIOD_ID='" + PeriodId + "'";
            sqlQuery += "  and ad.major_category_id = amcm.major_category_id";
            if (categID != "")
            {
                sqlQuery += " and ad.major_category_id = '" + categID + "'";
            }
            return sqlQuery;
        }

        public static string GetDprnName()
        {
            sqlQuery = string.Empty;
            sqlQuery = "select distinct DM.DPRN_NAME";
            sqlQuery += " from AST_DEPRECIATION_METHOD_MST DM";
            sqlQuery += "  where dm.organization_master_id =" + VMVServices.Web.Utils.OrganizationID;
            sqlQuery += " order by DM.DPRN_NAME";
            return sqlQuery;
        }

        public static string GetLocation()
        {
            sqlQuery = string.Empty;
            sqlQuery = "SELECT DISTINCT LM.LOCATION_NAME";
            sqlQuery += " FROM ast_location_mst LM";
            sqlQuery += " ORDER BY LM.LOCATION_NAME";
            return sqlQuery;
        }

        public static string GetLocationDesc()
        {
            sqlQuery = string.Empty;
            sqlQuery = "SELECT DISTINCT LM.LOCATION_DESC";
            sqlQuery += " FROM ast_location_mst LM";
            sqlQuery += " ORDER BY LM.LOCATION_DESC";
            return sqlQuery;
        }

        public static string GetAssetCAtegoryEffectiveDate(long asset_mst_id, DateTime Eff_date)
        {
            sqlQuery = string.Empty;
            sqlQuery = "select ACD.ASSET_MST_ID";
            sqlQuery += " from ast_asset_category_dtl ACD";
            sqlQuery += " where ACD.ASSET_MST_ID = " + asset_mst_id;
            sqlQuery += " and ACD.EFFECT_FROM > trunc(to_date('" + Eff_date.ToString("dd-MM-yyyy") + "','dd-MM-yyyy'))";
            return sqlQuery;
        }

        public static string ThirdPartyDetails()
        {
            sqlQuery = string.Empty;
            sqlQuery = "select distinct OD.THIRD_PARTY_ID,TM.THIRD_PARTY_NAME";
            sqlQuery += " from ast_asset_outward_service_dtl OD,IACADEME.Acd_Third_Party_Master TM";
            sqlQuery += " where OD.THIRD_PARTY_ID = TM.THIRD_PARTY_ID";
            return sqlQuery;
        }

        public static string GetAssetOutwardServiceDate(long outward_service_id, DateTime Eff_date)
        {
            sqlQuery = string.Empty;
            sqlQuery = "select D.OUTWARD_SERVICE_ID";
            sqlQuery += " from ast_asset_outward_service_dtl D";
            sqlQuery += " where D.OUTWARD_SERVICE_ID = " + outward_service_id;
            sqlQuery += " and D.Exp_Delivery_Date > trunc(to_date('" + Eff_date.ToString("dd-MM-yyyy") + "','dd-MM-yyyy'))";
            return sqlQuery;
        }

        public static string GetAssetMaximumOutwardDate(long outward_service_id, DateTime Eff_date)
        {
            sqlQuery = string.Empty;
            sqlQuery = "select MAX(D.OUTWARD_DATE)";
            sqlQuery += " from ast_asset_outward_service_dtl D";
            sqlQuery += " where ACD.ASSET_MST_ID = " + outward_service_id;
            sqlQuery += " having MAX(D.OUTWARD_DATE) > trunc(to_date('" + Eff_date.ToString("dd-MM-yyyy") + "','dd-MM-yyyy'))";
            return sqlQuery;
        }

        public static string BindAssetMaster()
        {
            sqlQuery = string.Empty;
            sqlQuery = "select AM.ASSET_MST_ID,AM.ASSET_NAME";
            sqlQuery += " FROM AST_ASSET_MST AM";
            return sqlQuery;
        }
        public static string BindOutWardAssets()
        {
            sqlQuery = string.Empty;

            sqlQuery = "   select AM.ASSET_MST_ID, AM.ASSET_NAME";
            sqlQuery += "  FROM AST_ASSET_MST AM, ast_asset_outward_service_dtl osd";
            sqlQuery += " where osd.asset_mst_id = am.asset_mst_id";
            return sqlQuery;
        }
        public static string BindCurrencyVal()
        {
            sqlQuery = string.Empty;

            sqlQuery = "select CM.CURRENCY_ID,CM.CURRENCY_DESC";
            sqlQuery += " FROM FIN_CURRENCY_MST CM";
            return sqlQuery;

        }

        public static string BindCurrencyVal4prefix(string str_cur)
        {
            sqlQuery = string.Empty;

            sqlQuery = "select CM.CURRENCY_ID,CM.CURRENCY_DESC";
            sqlQuery += " FROM FIN_CURRENCY_MST CM";
            sqlQuery += " WHERE CM.CURRENCY_DESC LIKE '" + str_cur + "%'";
            return sqlQuery;

        }


        public static string BindExchangeRateType()
        {
            sqlQuery = string.Empty;

            sqlQuery = "Select V.VALUE_KEY_ID,V.VALUE_NAME";
            sqlQuery += " From SYS_LOOKUP_VALUE_DTLS V,SYS_LOOKUP_TYPE_MASTER M";
            sqlQuery += " WHERE upper(M.TYPE_NAME) = 'EXCHANGE TYPE'";
            sqlQuery += " AND V.TYPE_KEY_ID =  M.TYPE_KEY_ID";
            sqlQuery += " ORDER BY V.DISPLAY_ORDER";
            return sqlQuery;

        }

        public static string BindMajorCategory()
        {
            sqlQuery = string.Empty;
            sqlQuery = "select MC.MAJOR_CATEGORY_ID,MC.CATEGORY_NAME";
            sqlQuery += " from AST_MAJOR_CATEGORY_MST MC";
            return sqlQuery;
        }

        public static string BindMinorCategory()
        {
            sqlQuery = string.Empty;
            sqlQuery = "select MD.MINOR_CATEGORY_ID,MD.MINOR_CATEGORY_NAME";
            sqlQuery += " from AST_MINOR_CATEGORY_DTL MD";
            return sqlQuery;
        }
        public static string BindCategoryDetail(long assetId)
        {
            sqlQuery = string.Empty;

            sqlQuery = "select * from AST_ASSET_CATEGORY_DTL h where h.asset_mst_id =" + assetId;
            return sqlQuery;
        }
        public static string BindThirdParty(string assetId)
        {
            sqlQuery = string.Empty;

            sqlQuery = "  select ao.outward_service_id as vendor_id, sc.vendor_name";
            sqlQuery += " from supplier_customers sc, ast_asset_outward_service_dtl ao";
            sqlQuery += " where sc.vendor_id = ao.third_party_id and ao.ASSET_MST_ID='" + assetId + "'";

            return sqlQuery;


        }
        public static string GetAssetDepreciatedDeatils(string Mode, string CalDtlId, string PeriodID, string catID="")
        {
            sqlQuery = string.Empty;
            decimal periodDays = 0;

            if (PeriodID != string.Empty)
            {
                sqlQuery = "   select nvl((ap.period_to_dt - ap.period_from_dt),0) as period_days";
                sqlQuery += "   from gl_acct_period_dtl ap";
                sqlQuery += "      where AP.cal_dtl_id='" + PeriodID + "'";
                periodDays = DBMethod.GetAmountDecimalValue(sqlQuery);
            }

            sqlQuery = string.Empty;

            //sqlQuery = "   select am.asset_mst_id,";
            //sqlQuery += "  am.asset_number,";
            //sqlQuery += "  am.asset_name,";
            //sqlQuery += "    nvl(am.orginal_cost, 0) as asset_cost,";
            //sqlQuery += "  nvl(am.orginal_cost, 0) * ";

            ////if (mode.ToUpper() == "MONTHLY")
            ////{
            ////    sqlQuery += periodDays + " * ";
            ////}
            ////else if (mode.ToUpper() == "YEARLY")
            ////{
            ////    sqlQuery += "  365 * ";
            ////}

            //sqlQuery += "  (select distinct sum(nvl(adm.dprn_rate, 0)) as dprn_rate";
            //sqlQuery += "   from ast_depreciation_method_mst adm,";
            //sqlQuery += "    AST_DPRN_CATEGORY_CONFIG    ac,";
            //sqlQuery += "  AST_ASSET_CATEGORY_DTL      aa,ast_asset_mst am";
            //sqlQuery += "   where adm.dprn_method_mst_id = (ac.dprn_method_mst_id)";
            //sqlQuery += "   and aa.asset_mst_id = am.asset_mst_id";
            //sqlQuery += "  and (aa.majore_category_id = ac.major_category_id or";
            //sqlQuery += "   aa.minor_category_id = ac.major_category_id)) /100  as depre_amt";
            //sqlQuery += "   from ast_asset_mst am";
            //sqlQuery += "   order by am.asset_number asc ";


            sqlQuery += "  SELECT '0' as ASSET_DEPRECIATION_ID,am.asset_mst_id, cm.category_name as category_name,";
            sqlQuery += "  am.asset_number, ";
            sqlQuery += "  am.asset_name, ";
            sqlQuery += "  NVL(am.orginal_cost, 0) AS asset_cost, ";
            sqlQuery += "  NVL(am.orginal_cost, 0) * ";
            sqlQuery += " (SELECT SUM(NVL(adm.dprn_rate, 0)) AS dprn_rate ";
            sqlQuery += "  FROM ast_depreciation_method_mst adm, ";
            sqlQuery += "   AST_DPRN_CATEGORY_CONFIG ac, ";
            sqlQuery += "   AST_ASSET_CATEGORY_DTL acd ";
            sqlQuery += "  WHERE adm.dprn_method_mst_id = ac.dprn_method_mst_id ";
            sqlQuery += "  and acd.majore_category_id = ac.major_category_id ";
            sqlQuery += "  and acd.asset_mst_id=am.asset_mst_id and adm.enabled_flag=1 ";
            sqlQuery += "   ) /100 AS depre_amt ";
            sqlQuery += "  FROM ast_asset_mst am, ast_asset_category_dtl cd, ast_major_category_mst cm ";
            sqlQuery += " where am.asset_mst_id=cd.asset_mst_id";
            sqlQuery += " and cd.majore_category_id=cm.major_category_id";
            if (catID != "")
            {
                sqlQuery += "  and cm.major_category_id='" + catID + "'";
            }
            sqlQuery += "  ORDER BY am.asset_number ASC ";

            return sqlQuery;
        }

        #endregion
    }
}
