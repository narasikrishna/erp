//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.Serialization;

namespace FIN.DAL
{
    [DataContract(IsReference = true)]
    [KnownType(typeof(SL_TRN_PROCESS_LOG_ERROR))]
    public partial class SL_TRN_PROCESS_LOG: IObjectWithChangeTracker, INotifyPropertyChanged
    {
        #region Primitive Properties
    
        [DataMember]
        public string PROCESS_ID
        {
            get { return _pROCESS_ID; }
            set
            {
                if (_pROCESS_ID != value)
                {
                    if (ChangeTracker.ChangeTrackingEnabled && ChangeTracker.State != ObjectState.Added)
                    {
                        throw new InvalidOperationException("The property 'PROCESS_ID' is part of the object's key and cannot be changed. Changes to key properties can only be made when the object is not being tracked or is in the Added state.");
                    }
                    _pROCESS_ID = value;
                    OnPropertyChanged("PROCESS_ID");
                }
            }
        }
        private string _pROCESS_ID;
    
        [DataMember]
        public string PROCESS_TYPE
        {
            get { return _pROCESS_TYPE; }
            set
            {
                if (_pROCESS_TYPE != value)
                {
                    _pROCESS_TYPE = value;
                    OnPropertyChanged("PROCESS_TYPE");
                }
            }
        }
        private string _pROCESS_TYPE;
    
        [DataMember]
        public string PROCESS_FILE_NAME
        {
            get { return _pROCESS_FILE_NAME; }
            set
            {
                if (_pROCESS_FILE_NAME != value)
                {
                    _pROCESS_FILE_NAME = value;
                    OnPropertyChanged("PROCESS_FILE_NAME");
                }
            }
        }
        private string _pROCESS_FILE_NAME;
    
        [DataMember]
        public string PROCESS_FILE_PATH
        {
            get { return _pROCESS_FILE_PATH; }
            set
            {
                if (_pROCESS_FILE_PATH != value)
                {
                    _pROCESS_FILE_PATH = value;
                    OnPropertyChanged("PROCESS_FILE_PATH");
                }
            }
        }
        private string _pROCESS_FILE_PATH;
    
        [DataMember]
        public Nullable<System.DateTime> PROCESS_DATE_TIME
        {
            get { return _pROCESS_DATE_TIME; }
            set
            {
                if (_pROCESS_DATE_TIME != value)
                {
                    _pROCESS_DATE_TIME = value;
                    OnPropertyChanged("PROCESS_DATE_TIME");
                }
            }
        }
        private Nullable<System.DateTime> _pROCESS_DATE_TIME;
    
        [DataMember]
        public string ENABLED_FLAG
        {
            get { return _eNABLED_FLAG; }
            set
            {
                if (_eNABLED_FLAG != value)
                {
                    _eNABLED_FLAG = value;
                    OnPropertyChanged("ENABLED_FLAG");
                }
            }
        }
        private string _eNABLED_FLAG;
    
        [DataMember]
        public string WORKFLOW_COMPLETION_STATUS
        {
            get { return _wORKFLOW_COMPLETION_STATUS; }
            set
            {
                if (_wORKFLOW_COMPLETION_STATUS != value)
                {
                    _wORKFLOW_COMPLETION_STATUS = value;
                    OnPropertyChanged("WORKFLOW_COMPLETION_STATUS");
                }
            }
        }
        private string _wORKFLOW_COMPLETION_STATUS;
    
        [DataMember]
        public string CREATED_BY
        {
            get { return _cREATED_BY; }
            set
            {
                if (_cREATED_BY != value)
                {
                    _cREATED_BY = value;
                    OnPropertyChanged("CREATED_BY");
                }
            }
        }
        private string _cREATED_BY;
    
        [DataMember]
        public System.DateTime CREATED_DATE
        {
            get { return _cREATED_DATE; }
            set
            {
                if (_cREATED_DATE != value)
                {
                    _cREATED_DATE = value;
                    OnPropertyChanged("CREATED_DATE");
                }
            }
        }
        private System.DateTime _cREATED_DATE;
    
        [DataMember]
        public string MODIFIED_BY
        {
            get { return _mODIFIED_BY; }
            set
            {
                if (_mODIFIED_BY != value)
                {
                    _mODIFIED_BY = value;
                    OnPropertyChanged("MODIFIED_BY");
                }
            }
        }
        private string _mODIFIED_BY;
    
        [DataMember]
        public Nullable<System.DateTime> MODIFIED_DATE
        {
            get { return _mODIFIED_DATE; }
            set
            {
                if (_mODIFIED_DATE != value)
                {
                    _mODIFIED_DATE = value;
                    OnPropertyChanged("MODIFIED_DATE");
                }
            }
        }
        private Nullable<System.DateTime> _mODIFIED_DATE;
    
        [DataMember]
        public string ATTRIBUTE1
        {
            get { return _aTTRIBUTE1; }
            set
            {
                if (_aTTRIBUTE1 != value)
                {
                    _aTTRIBUTE1 = value;
                    OnPropertyChanged("ATTRIBUTE1");
                }
            }
        }
        private string _aTTRIBUTE1;
    
        [DataMember]
        public string ATTRIBUTE2
        {
            get { return _aTTRIBUTE2; }
            set
            {
                if (_aTTRIBUTE2 != value)
                {
                    _aTTRIBUTE2 = value;
                    OnPropertyChanged("ATTRIBUTE2");
                }
            }
        }
        private string _aTTRIBUTE2;
    
        [DataMember]
        public string ATTRIBUTE3
        {
            get { return _aTTRIBUTE3; }
            set
            {
                if (_aTTRIBUTE3 != value)
                {
                    _aTTRIBUTE3 = value;
                    OnPropertyChanged("ATTRIBUTE3");
                }
            }
        }
        private string _aTTRIBUTE3;
    
        [DataMember]
        public string ATTRIBUTE4
        {
            get { return _aTTRIBUTE4; }
            set
            {
                if (_aTTRIBUTE4 != value)
                {
                    _aTTRIBUTE4 = value;
                    OnPropertyChanged("ATTRIBUTE4");
                }
            }
        }
        private string _aTTRIBUTE4;
    
        [DataMember]
        public string ATTRIBUTE5
        {
            get { return _aTTRIBUTE5; }
            set
            {
                if (_aTTRIBUTE5 != value)
                {
                    _aTTRIBUTE5 = value;
                    OnPropertyChanged("ATTRIBUTE5");
                }
            }
        }
        private string _aTTRIBUTE5;
    
        [DataMember]
        public string ATTRIBUTE6
        {
            get { return _aTTRIBUTE6; }
            set
            {
                if (_aTTRIBUTE6 != value)
                {
                    _aTTRIBUTE6 = value;
                    OnPropertyChanged("ATTRIBUTE6");
                }
            }
        }
        private string _aTTRIBUTE6;
    
        [DataMember]
        public string ATTRIBUTE7
        {
            get { return _aTTRIBUTE7; }
            set
            {
                if (_aTTRIBUTE7 != value)
                {
                    _aTTRIBUTE7 = value;
                    OnPropertyChanged("ATTRIBUTE7");
                }
            }
        }
        private string _aTTRIBUTE7;
    
        [DataMember]
        public string ATTRIBUTE8
        {
            get { return _aTTRIBUTE8; }
            set
            {
                if (_aTTRIBUTE8 != value)
                {
                    _aTTRIBUTE8 = value;
                    OnPropertyChanged("ATTRIBUTE8");
                }
            }
        }
        private string _aTTRIBUTE8;
    
        [DataMember]
        public string ATTRIBUTE9
        {
            get { return _aTTRIBUTE9; }
            set
            {
                if (_aTTRIBUTE9 != value)
                {
                    _aTTRIBUTE9 = value;
                    OnPropertyChanged("ATTRIBUTE9");
                }
            }
        }
        private string _aTTRIBUTE9;
    
        [DataMember]
        public string ATTRIBUTE10
        {
            get { return _aTTRIBUTE10; }
            set
            {
                if (_aTTRIBUTE10 != value)
                {
                    _aTTRIBUTE10 = value;
                    OnPropertyChanged("ATTRIBUTE10");
                }
            }
        }
        private string _aTTRIBUTE10;

        #endregion
        #region Navigation Properties
    
        [DataMember]
        public TrackableCollection<SL_TRN_PROCESS_LOG_ERROR> SL_TRN_PROCESS_LOG_ERROR
        {
            get
            {
                if (_sL_TRN_PROCESS_LOG_ERROR == null)
                {
                    _sL_TRN_PROCESS_LOG_ERROR = new TrackableCollection<SL_TRN_PROCESS_LOG_ERROR>();
                    _sL_TRN_PROCESS_LOG_ERROR.CollectionChanged += FixupSL_TRN_PROCESS_LOG_ERROR;
                }
                return _sL_TRN_PROCESS_LOG_ERROR;
            }
            set
            {
                if (!ReferenceEquals(_sL_TRN_PROCESS_LOG_ERROR, value))
                {
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        throw new InvalidOperationException("Cannot set the FixupChangeTrackingCollection when ChangeTracking is enabled");
                    }
                    if (_sL_TRN_PROCESS_LOG_ERROR != null)
                    {
                        _sL_TRN_PROCESS_LOG_ERROR.CollectionChanged -= FixupSL_TRN_PROCESS_LOG_ERROR;
                    }
                    _sL_TRN_PROCESS_LOG_ERROR = value;
                    if (_sL_TRN_PROCESS_LOG_ERROR != null)
                    {
                        _sL_TRN_PROCESS_LOG_ERROR.CollectionChanged += FixupSL_TRN_PROCESS_LOG_ERROR;
                    }
                    OnNavigationPropertyChanged("SL_TRN_PROCESS_LOG_ERROR");
                }
            }
        }
        private TrackableCollection<SL_TRN_PROCESS_LOG_ERROR> _sL_TRN_PROCESS_LOG_ERROR;

        #endregion
        #region ChangeTracking
    
        protected virtual void OnPropertyChanged(String propertyName)
        {
            if (ChangeTracker.State != ObjectState.Added && ChangeTracker.State != ObjectState.Deleted)
            {
                ChangeTracker.State = ObjectState.Modified;
            }
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        protected virtual void OnNavigationPropertyChanged(String propertyName)
        {
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        event PropertyChangedEventHandler INotifyPropertyChanged.PropertyChanged{ add { _propertyChanged += value; } remove { _propertyChanged -= value; } }
        private event PropertyChangedEventHandler _propertyChanged;
        private ObjectChangeTracker _changeTracker;
    
        [DataMember]
        public ObjectChangeTracker ChangeTracker
        {
            get
            {
                if (_changeTracker == null)
                {
                    _changeTracker = new ObjectChangeTracker();
                    _changeTracker.ObjectStateChanging += HandleObjectStateChanging;
                }
                return _changeTracker;
            }
            set
            {
                if(_changeTracker != null)
                {
                    _changeTracker.ObjectStateChanging -= HandleObjectStateChanging;
                }
                _changeTracker = value;
                if(_changeTracker != null)
                {
                    _changeTracker.ObjectStateChanging += HandleObjectStateChanging;
                }
            }
        }
    
        private void HandleObjectStateChanging(object sender, ObjectStateChangingEventArgs e)
        {
            if (e.NewState == ObjectState.Deleted)
            {
                ClearNavigationProperties();
            }
        }
    
        protected bool IsDeserializing { get; private set; }
    
        [OnDeserializing]
        public void OnDeserializingMethod(StreamingContext context)
        {
            IsDeserializing = true;
        }
    
        [OnDeserialized]
        public void OnDeserializedMethod(StreamingContext context)
        {
            IsDeserializing = false;
            ChangeTracker.ChangeTrackingEnabled = true;
        }
    
        protected virtual void ClearNavigationProperties()
        {
            SL_TRN_PROCESS_LOG_ERROR.Clear();
        }

        #endregion
        #region Association Fixup
    
        private void FixupSL_TRN_PROCESS_LOG_ERROR(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (e.NewItems != null)
            {
                foreach (SL_TRN_PROCESS_LOG_ERROR item in e.NewItems)
                {
                    item.SL_TRN_PROCESS_LOG = this;
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        if (!item.ChangeTracker.ChangeTrackingEnabled)
                        {
                            item.StartTracking();
                        }
                        ChangeTracker.RecordAdditionToCollectionProperties("SL_TRN_PROCESS_LOG_ERROR", item);
                    }
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (SL_TRN_PROCESS_LOG_ERROR item in e.OldItems)
                {
                    if (ReferenceEquals(item.SL_TRN_PROCESS_LOG, this))
                    {
                        item.SL_TRN_PROCESS_LOG = null;
                    }
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        ChangeTracker.RecordRemovalFromCollectionProperties("SL_TRN_PROCESS_LOG_ERROR", item);
                    }
                }
            }
        }

        #endregion
    }
}
