﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL
{

    public class FINSQL
    {
        static string sqlQuery = string.Empty;
        public static string GetMenuList(String User_Name, string str_ModuleName, string SelLang)
        {
            sqlQuery = string.Empty;
            //sqlQuery = " select SMI_P.FORM_LABEL as PARENT_MENU,smi.PARENT_MENU_CODE, SCM.MASTER_CODE as MODULECODE,SCM.DESCRIPTION as MODULENAME,SMI.PK_ID as MENU_KEY_ID,SS.SCREEN_CODE,SS.SCREEN_NAME ";
            //sqlQuery += " ,SRFP.INSERT_ALLOWED_FLAG,SRFP.UPDATE_ALLOWED_FLAG,SRFP.DELETE_ALLOWED_FLAG,SRFP.QUERY_ALLOWED_FLAG,SMI.REPORT_NAME,SMI.ENTRY_PAGE_OPEN,SMI.MENU_URL,SS.ENABLED_FLAG ";
            //sqlQuery += " from ssm_user_role_intersects SURI,ssm_role_module_intersects SRMI ";
            //sqlQuery += " ,ssm_role_form_permissions SRFP,ssm_screens SS,ssm_menu_items SMI,ssm_code_masters SCM ";
            //sqlQuery += " ,ssm_menu_items SMI_P ";
            //sqlQuery += " where SURI.ROLE_CODE = SRMI.ROLE_CODE";
            //sqlQuery += " and SRMI.MODULE_CODE = SRFP.MODULE_CODE ";
            //sqlQuery += " and SRFP.FORM_CODE = SS.SCREEN_CODE ";
            //sqlQuery += " and SS.SCREEN_CODE = SMI.FORM_CODE ";
            //sqlQuery += " and SRMI.MODULE_CODE = SCM.MASTER_CODE ";
            //sqlQuery += " and SRFP.FORM_ACCESS_FLAG = '1' ";
            //sqlQuery += " and SS.ENABLED_FLAG = '1' ";
            //sqlQuery += " and SURI.USER_CODE = '" + User_Name + "'";
            //sqlQuery += " and SCM.MASTER_CODE='" + str_ModuleName + "'";
            //sqlQuery += "  and SMI.Parent_Menu_Code = SMI_P.menu_code(+) ";
            //sqlQuery += " and SS.LANGUAGE_CODE='" + SelLang + "'";
            //sqlQuery += "  ORDER BY SMI.PARENT_MENU_CODE, SMI.ORDER_NO  asc";

            sqlQuery = " SELECT (SELECT NVL(SMI_PP.FORM_LABEL" + VMVServices.Web.Utils.LanguageCode + ", SMI_P.FORM_LABEL" + VMVServices.Web.Utils.LanguageCode + ")  FROM SSM_MENU_ITEMS SMI_P , SSM_MENU_ITEMS SMI_PP WHERE SMI_P.MENU_CODE = SMI.PARENT_MENU_CODE AND SMI_P.PARENT_MENU_CODE= SMI_PP.MENU_CODE (+) AND SMI_P.LANGUAGE_CODE = SMI.LANGUAGE_CODE) AS PARENT_MENU  ";
            sqlQuery += " ,(SELECT  FORM_LABEL" + VMVServices.Web.Utils.LanguageCode + "  FROM SSM_MENU_ITEMS SMI_P WHERE SMI_P.MENU_CODE= SMI.PARENT_MENU_CODE AND SMI_P.LANGUAGE_CODE= SMI.LANGUAGE_CODE) AS PARENT_PARENT_MENU ";
            sqlQuery += " ,SMI.ATTRIBUTE2,SMI.ATTRIBUTE3 ";
            sqlQuery+= "  ,(SELECT NVL(SMI_PMC.PARENT_MENU_CODE,SMI.PARENT_MENU_CODE) FROM SSM_MENU_ITEMS SMI_PMC WHERE SMI_PMC.FORM_CODE= SMI.PARENT_MENU_CODE) AS PARENT_MENU_CODE ";
            sqlQuery += ",SMI.PARENT_MENU_CODE as PARENT_PARENT_MENU_CODE ";
            sqlQuery += " , SCM.MASTER_CODE as MODULECODE,SCM.DESCRIPTION" + VMVServices.Web.Utils.LanguageCode + " as MODULENAME,SMI.PK_ID as MENU_KEY_ID,SS.SCREEN_CODE,SS.SCREEN_NAME" + VMVServices.Web.Utils.LanguageCode + " as SCREEN_NAME  ";
            sqlQuery += " ,SRFP.INSERT_ALLOWED_FLAG,SRFP.UPDATE_ALLOWED_FLAG,SRFP.DELETE_ALLOWED_FLAG,SRFP.QUERY_ALLOWED_FLAG,SMI.REPORT_NAME,SMI.ENTRY_PAGE_OPEN,SMI.MENU_URL,SS.ENABLED_FLAG   ";
            sqlQuery += " ,SS.SHOW_ICONSET ";
            sqlQuery += " FROM SSM_USER_ROLE_INTERSECTS SURI ";
            sqlQuery += " INNER JOIN SSM_ROLE_MODULE_INTERSECTS  SRMI ON SRMI.ROLE_CODE= SURI.ROLE_CODE ";
            sqlQuery += " INNER JOIN SSM_ROLE_FORM_PERMISSIONS SRFP ON SRFP.ROLE_CODE= SRMI.ROLE_CODE AND SRFP.MODULE_CODE= SRMI.MODULE_CODE ";
            sqlQuery += " INNER JOIN SSM_SCREENS SS ON SS.SCREEN_CODE = SRFP.FORM_CODE  ";
            sqlQuery += " INNER JOIN SSM_MENU_ITEMS SMI ON SMI.FORM_CODE= SS.SCREEN_CODE "; //--AND SMI.LANGUAGE_CODE =SS.LANGUAGE_CODE 
            sqlQuery += " INNER JOIN SSM_CODE_MASTERS SCM ON SCM.CODE=  SRMI.MODULE_CODE AND SCM.MASTER_CODE = SRMI.MODULE_CODE ";
            sqlQuery += " WHERE upper(SURI.USER_CODE) =upper('" + User_Name + "')";
            if(str_ModuleName.Length >0)
            {
                sqlQuery += " AND upper(SRMI.MODULE_CODE)=upper('" + str_ModuleName + "') ";
            }
            //sqlQuery += " AND SS.LANGUAGE_CODE='" + SelLang + "' ";
            sqlQuery += " AND SS.LANGUAGE_CODE='EN' ";
            sqlQuery += " AND SS.ENABLED_FLAG='1'";
            sqlQuery += " AND SRFP.FORM_ACCESS_FLAG='1' AND SRFP.ACCESS_FLAG='1' AND SRFP.ACTIVE_FLAG='1'";
            sqlQuery += "  and ss.tv_parent_menu is null";
            sqlQuery += "  ORDER BY MODULENAME,SMI.ATTRIBUTE1,SMI.ORDER_NO,SS.SCREEN_NAME";
            // SMI.PARENT_MENU_CODE, 
            return sqlQuery;
        }
        public static string GetTVMenuList(String User_Name, string SelLang, string programId)
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT (SELECT  FORM_LABEL FROM SSM_MENU_ITEMS SMI_P WHERE SMI_P.MENU_CODE= SMI.PARENT_MENU_CODE AND SMI_P.LANGUAGE_CODE= SMI.LANGUAGE_CODE) AS PARENT_MENU ";
            sqlQuery += " ,SMI.ATTRIBUTE2,SMI.ATTRIBUTE3,SMI.PARENT_MENU_CODE, SCM.MASTER_CODE as MODULECODE,SCM.DESCRIPTION" + VMVServices.Web.Utils.LanguageCode + " as MODULENAME,SMI.PK_ID as MENU_KEY_ID,SS.SCREEN_CODE,SS.SCREEN_NAME  ";
            sqlQuery += " ,SRFP.INSERT_ALLOWED_FLAG,SRFP.UPDATE_ALLOWED_FLAG,SRFP.DELETE_ALLOWED_FLAG,SRFP.QUERY_ALLOWED_FLAG,SMI.REPORT_NAME,SMI.ENTRY_PAGE_OPEN,SMI.MENU_URL,SS.ENABLED_FLAG   ";
            sqlQuery += " FROM SSM_USER_ROLE_INTERSECTS SURI ";
            sqlQuery += " INNER JOIN SSM_ROLE_MODULE_INTERSECTS  SRMI ON SRMI.ROLE_CODE= SURI.ROLE_CODE ";
            sqlQuery += " INNER JOIN SSM_ROLE_FORM_PERMISSIONS SRFP ON SRFP.ROLE_CODE= SRMI.ROLE_CODE AND SRFP.MODULE_CODE= SRMI.MODULE_CODE ";
            sqlQuery += " INNER JOIN SSM_SCREENS SS ON SS.SCREEN_CODE = SRFP.FORM_CODE  ";
            sqlQuery += " INNER JOIN SSM_MENU_ITEMS SMI ON SMI.FORM_CODE= SS.SCREEN_CODE "; 
            sqlQuery += " INNER JOIN SSM_CODE_MASTERS SCM ON SCM.CODE=  SRMI.MODULE_CODE AND SCM.MASTER_CODE = SRMI.MODULE_CODE ";
            sqlQuery += " WHERE upper(SURI.USER_CODE) =upper('" + User_Name + "') ";
           
            sqlQuery += " AND SS.LANGUAGE_CODE='EN' ";
            sqlQuery += " AND SS.ENABLED_FLAG='1'";
            sqlQuery += " AND SRFP.FORM_ACCESS_FLAG='1' AND SRFP.ACCESS_FLAG='1' AND SRFP.ACTIVE_FLAG='1'";
            sqlQuery += "     and ss.tv_parent_menu is not null";
            sqlQuery += "  and smi.parent_menu_code=ss.tv_parent_menu";
            sqlQuery += "  AND SMI.PARENT_MENU_CODE IN (SELECT MM.MENU_CODE FROM SSM_MENU_ITEMS MM WHERE MM.PK_ID='" + programId + "')";
            sqlQuery += "  ORDER BY SMI.ATTRIBUTE2,SMI.ORDER_NO";
         
            return sqlQuery;
        }

        public static string GetMenuListFull(string str_UserId)
        {
            sqlQuery = string.Empty;
            sqlQuery += " SELECT NVL((SELECT  FORM_LABEL FROM SSM_MENU_ITEMS SMI_P WHERE SMI_P.MENU_CODE= SMI.PARENT_MENU_CODE AND SMI_P.LANGUAGE_CODE= SMI.LANGUAGE_CODE),'MENU') AS PARENT_MENU ";
            sqlQuery += " ,(SELECT  MENU_IMAGE_URL FROM SSM_MENU_ITEMS SMI_P WHERE SMI_P.MENU_CODE= SMI.PARENT_MENU_CODE AND SMI_P.LANGUAGE_CODE= SMI.LANGUAGE_CODE) AS PARENT_MENU_IMG_URL  ";
            sqlQuery += " ,NVL(SMI.ATTRIBUTE1,500) as MENU_LIST_ORDER ,NVL(SMI.ORDER_NO,0) as ORDER_NO ,SMI.ATTRIBUTE2,SMI.ATTRIBUTE3,SMI.PARENT_MENU_CODE, SCM.MASTER_CODE as MODULECODE,SCM.DESCRIPTION as MODULENAME,SMI.PK_ID as MENU_KEY_ID,SS.SCREEN_CODE,SS.SCREEN_NAME  ";
            sqlQuery += " ,SRFP.ACCESS_FLAG,SRFP.INSERT_ALLOWED_FLAG,SRFP.UPDATE_ALLOWED_FLAG,SRFP.DELETE_ALLOWED_FLAG,SRFP.QUERY_ALLOWED_FLAG,SMI.REPORT_NAME,SMI.ENTRY_PAGE_OPEN,SMI.MENU_URL,SS.ENABLED_FLAG   ";
            sqlQuery += " , MENU_IMAGE_URL ";
            sqlQuery += " FROM SSM_USER_ROLE_INTERSECTS SURI ";
            sqlQuery += " INNER JOIN SSM_ROLE_MODULE_INTERSECTS  SRMI ON SRMI.ROLE_CODE= SURI.ROLE_CODE ";
            sqlQuery += " INNER JOIN SSM_ROLE_FORM_PERMISSIONS SRFP ON SRFP.ROLE_CODE= SRMI.ROLE_CODE AND SRFP.MODULE_CODE= SRMI.MODULE_CODE ";
            sqlQuery += " INNER JOIN SSM_SCREENS SS ON SS.SCREEN_CODE = SRFP.FORM_CODE  ";
            sqlQuery += " INNER JOIN SSM_MENU_ITEMS SMI ON SMI.FORM_CODE= SS.SCREEN_CODE  ";
            sqlQuery += " INNER JOIN SSM_CODE_MASTERS SCM ON SCM.CODE=  SRMI.MODULE_CODE AND SCM.MASTER_CODE = SRMI.MODULE_CODE ";
            sqlQuery += "  WHERE SS.LANGUAGE_CODE='EN' AND SS.ENABLED_FLAG='1' AND SRFP.FORM_ACCESS_FLAG='1' AND SRFP.ACCESS_FLAG='1' AND SRFP.ACTIVE_FLAG='1' ";
            sqlQuery += " AND upper(SURI.USER_CODE) =upper('" + str_UserId + "') ";

            sqlQuery += " UNION     ";
            sqlQuery += " SELECT NVL((SELECT  FORM_LABEL FROM SSM_MENU_ITEMS SMI_P WHERE SMI_P.MENU_CODE= SMI.PARENT_MENU_CODE AND SMI_P.LANGUAGE_CODE= SMI.LANGUAGE_CODE),'MENU') AS PARENT_MENU  ";
            sqlQuery += " ,(SELECT  MENU_IMAGE_URL FROM SSM_MENU_ITEMS SMI_P WHERE SMI_P.MENU_CODE= SMI.PARENT_MENU_CODE AND SMI_P.LANGUAGE_CODE= SMI.LANGUAGE_CODE) AS PARENT_MENU_IMG_URL  ";
            sqlQuery += " ,NVL(SMI.ATTRIBUTE1,500) as MENU_LIST_ORDER,NVL(SMI.ORDER_NO,0) as ORDER_NO,SMI.ATTRIBUTE2,SMI.ATTRIBUTE3,SMI.PARENT_MENU_CODE, SCM.MASTER_CODE as MODULECODE,SCM.DESCRIPTION as MODULENAME,SMI.PK_ID as MENU_KEY_ID,SS.SCREEN_CODE,SS.SCREEN_NAME   ";
            sqlQuery += " ,'0' as ACCESS_FLAG,'0' INSERT_ALLOWED_FLAG,'0' as UPDATE_ALLOWED_FLAG,'0'as DELETE_ALLOWED_FLAG,'0'as QUERY_ALLOWED_FLAG,SMI.REPORT_NAME,SMI.ENTRY_PAGE_OPEN,SMI.MENU_URL,SS.ENABLED_FLAG    ";
            sqlQuery += " , MENU_IMAGE_URL ";
            sqlQuery += " FROM SSM_MENU_ITEMS SMI ";
            sqlQuery += " INNER JOIN SSM_SCREENS SS ON SMI.FORM_CODE= SS.SCREEN_CODE ";
            sqlQuery += " INNER JOIN SSM_CODE_MASTERS SCM ON SCM.CODE=  SS.MODULE_CODE  ";
            sqlQuery += " WHERE SS.LANGUAGE_CODE='EN' AND SS.ENABLED_FLAG='1'  AND SCM.MASTER_CODE !='MC' ";
            sqlQuery += " AND SMI.FORM_CODE NOT IN ( ";
            sqlQuery += "             SELECT FORM_CODE FROM   SSM_USER_ROLE_INTERSECTS SURI  ";
            sqlQuery += "             INNER JOIN SSM_ROLE_MODULE_INTERSECTS  SRMI ON SRMI.ROLE_CODE= SURI.ROLE_CODE  ";
            sqlQuery += "             INNER JOIN SSM_ROLE_FORM_PERMISSIONS SRFP ON SRFP.ROLE_CODE= SRMI.ROLE_CODE AND SRFP.MODULE_CODE= SRMI.MODULE_CODE  ";
            sqlQuery += "             WHERE   SRFP.FORM_ACCESS_FLAG='1' AND SRFP.ACCESS_FLAG='1' AND SRFP.ACTIVE_FLAG='1'  ";
            sqlQuery += "             AND upper(SURI.USER_CODE) =upper('" + str_UserId + "')  ";
            sqlQuery += "               ) ";

            return sqlQuery;
        }
        public static string GetLookupData(string typeName)
        {
            sqlQuery = string.Empty;

            sqlQuery = "Select V.VALUE_KEY_ID,V.VALUE_NAME  From SYS_LOOKUP_VALUE_DTLS V,SYS_LOOKUP_TYPE_MASTER M";
            sqlQuery += " WHERE M.TYPE_NAME = '" + typeName + "'";
            sqlQuery += " AND V.TYPE_KEY_ID =  M.TYPE_KEY_ID";
            sqlQuery += " ORDER BY V.DISPLAY_ORDER";
            return sqlQuery;

        }


        public static string GetProjectMasterList(string modifyURL, string deleteURL)
        {
            sqlQuery = string.Empty;
            sqlQuery = "SELECT PROJECT_NAME,DESCRIPTION,ENABLED_FLAG,START_DATE,END_DATE," + modifyURL + "," + deleteURL + " FROM PROJECT_MASTER ";
            return sqlQuery;

        }
        public static string GetMenuDtl(long pkId)
        {
            sqlQuery = string.Empty;
            sqlQuery = " select  m.*,h.list_table_name ";
            sqlQuery += " from SSM_MENU_ITEMS m, ssm_list_hdr h";
            sqlQuery += "  where m.form_code = h.list_screen_code  ";
            sqlQuery += "   and m.pk_id = " + pkId;
            return sqlQuery;

        }


        public static string GetCategoriesList(string modifyURL, string deleteURL)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select c.category_code,c.category_desc,c.eff_from_date,c.eff_to_date";
            sqlQuery += " ," + modifyURL + "," + deleteURL;
            sqlQuery += "  from  FIN_CATEGORIES c";

            sqlQuery += "  order by c.category_id desc";

            return sqlQuery;

        }
        public static string GetWorkFlowMonitor1(string userCode)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select w.workflow_code,";
            sqlQuery += "   w.role_code,";
            sqlQuery += "   w.level_code,";
            sqlQuery += "   w.transaction_code,";
            sqlQuery += "   w.status,";
            sqlQuery += "   w.is_this_final,";
            sqlQuery += "   w.created_date,";
            sqlQuery += "   w.workflow_message,";
            sqlQuery += "  (select (sm.menu_url || '?ProgramID=' || sm.pk_id)";
            sqlQuery += "   from ssm_menu_items sm";
            sqlQuery += "     where sm.form_code = w.workflow_code) as menu_url,";
            sqlQuery += "    (select ss.module_code";
            sqlQuery += "     from ssm_screens ss";
            sqlQuery += "      where ss.screen_code = w.workflow_code";
            sqlQuery += "         and rownum = 1) as module_code";
            sqlQuery += "   from wfm_workflow_monitors w,ssm_user_role_intersects ur";
            sqlQuery += "  where w.role_code=ur.role_code";
            sqlQuery += "  and ur.user_code='" + userCode + "'";
            sqlQuery += "  and w.status='WFST02'";

            return sqlQuery;

        }
        public static string GetAlertUserLevel(string userCode)
        {
            sqlQuery = string.Empty;

            //sqlQuery = "      select al.alert_user_level_code,";
            //sqlQuery += "       al.alert_code,";
            //sqlQuery += "       al.user_code,";
            //sqlQuery += "       al.alert_message,";
            //sqlQuery += "       al.created_date,";
            //sqlQuery += "        al.transaction_code";
            //sqlQuery += "    from ssm_alert_user_levels al";
            //sqlQuery += "   WHERE al.user_code='" + userCode + "'";
            //sqlQuery += "   AND al.END_DATE is null";


            sqlQuery = "      SELECT d.RESPONSED_BY,d.RESPONSED_DATE,d.created_date,M.ALERT_DESC ALERT_TYPE,D.ALERT_USER_LEVEL_CODE,D.alert_message alert_message,D.ALERT_CODE,M.alert_type alertcheck,m.screen_code,d.transaction_code";
            sqlQuery += "         , D.org_id,";
            sqlQuery += "       (select (GCH.COMP_INTERNAL_NAME || ' - ' || GCH.COMP_EXTERNAL_NAME || ' - ' ||";
            sqlQuery += "              GCH.COMP_SHORT_NAME)";
            sqlQuery += "        from gl_companies_hdr GCH";
            sqlQuery += "       where GCH.comp_id = D.ORG_ID) as org_desc,";
            sqlQuery += "        (select (sm.menu_url || '?ProgramID=' || sm.pk_id)";
            sqlQuery += "          from ssm_menu_items sm";
            sqlQuery += "         where sm.form_code = M.Screen_Code) as menu_url,";
            sqlQuery += "       (select ss.module_code";
            sqlQuery += "          from ssm_screens ss";
            sqlQuery += "         where ss.screen_code = M.Screen_Code";
            sqlQuery += "          and rownum = 1) as module_code,";
            sqlQuery += "      (select ss.description";
            sqlQuery += "       from ssm_screens ss";
            sqlQuery += "      where ss.screen_code = M.Screen_Code";
            sqlQuery += "      and rownum = 1) as module_description";
            sqlQuery += "     FROM   SSM_ALERT_USER_LEVELS D ,SSM_ALERT_MASTERS M";
            sqlQuery += "     	 WHERE  UPPER(d.USER_CODE) = UPPER( '" + VMVServices.Web.Utils.UserName + "') AND D.end_date IS NULL";
            sqlQuery += "     	 AND    D.ALERT_CODE=M.ALERT_CODE";
            sqlQuery += "     	 AND    D.enabled_flag = '1' AND D.workflow_completion_status = '1'";
            sqlQuery += "      AND    M.workflow_completion_status = '1'";
            if (VMVServices.Web.Utils.LanguageCode != string.Empty)
            {
                sqlQuery += "      and m.language_code = '" + VMVServices.Web.Utils.LanguageCode + "'";
            }
            sqlQuery += "      AND  RESPONSED_BY is null and RESPONSED_DATE is null";
            sqlQuery += "     	 ORDER BY d.created_date desc";

            return sqlQuery;

        }
        public static string GetWorkFlowMonitor(string userCode)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select wm.workflow_code,wm.org_id,wm.REMARKS,";
            sqlQuery += "     (select (GCH.COMP_INTERNAL_NAME" + VMVServices.Web.Utils.LanguageCode + " || ' - ' || GCH.COMP_EXTERNAL_NAME || ' - ' ||";
            sqlQuery += "        GCH.COMP_SHORT_NAME)";
            sqlQuery += "   from gl_companies_hdr GCH";
            sqlQuery += "  where GCH.comp_id = wm.org_id) as org_desc,";
            sqlQuery += "   wm.role_code,";
            sqlQuery += "   wm.level_code,";
            sqlQuery += "   wm.transaction_code,";
            sqlQuery += "   wm.status,";
            sqlQuery += "   wm.is_this_final,";
            sqlQuery += "   wm.created_date,";
            sqlQuery += "   wm.workflow_message,";
            sqlQuery += "  (select (sm.menu_url || '?ProgramID=' || sm.pk_id)";
            sqlQuery += "   from ssm_menu_items sm";
            sqlQuery += "     where sm.form_code = wm.workflow_code) as menu_url,";
            sqlQuery += "    (select ss.module_code";
            sqlQuery += "     from ssm_screens ss";
            sqlQuery += "      where ss.screen_code = wm.workflow_code";
            sqlQuery += "         and rownum = 1) as module_code,";
            sqlQuery += "      (select ss.description";
            sqlQuery += "      from ssm_screens ss";
            sqlQuery += "      where ss.screen_code = wm.workflow_code";
            sqlQuery += "   and rownum = 1) as module_description,";
            sqlQuery += "      (select ss.screen_name";
            sqlQuery += "      from ssm_screens ss";
            sqlQuery += "      where ss.screen_code = wm.workflow_code";
            sqlQuery += "   and rownum = 1) as Screen_name";
            sqlQuery += "     FROM WFM_WORKFLOW_MONITORS WM";
            sqlQuery += "     WHERE WM.STATUS NOT IN ('WFST01', 'WFST02')";
            sqlQuery += "      AND APPROVED_ALREADY =";
            sqlQuery += "      (SELECT MAX(APPROVED_ALREADY)";
            sqlQuery += "         FROM WFM_WORKFLOW_MONITORS";
            sqlQuery += "         WHERE STATUS NOT IN ('WFST01', 'WFST02')";
            sqlQuery += "     AND WORKFLOW_CODE = WM.WORKFLOW_CODE";
            sqlQuery += "      AND TRANSACTION_CODE = WM.TRANSACTION_CODE)";
            sqlQuery += "        AND ORG_ID IN (SELECT org_id";
            sqlQuery += "            FROM ssm_user_org_intersects";
            sqlQuery += "          WHERE UPPER(USER_CODE) = UPPER( '" + VMVServices.Web.Utils.UserName + "')";
            sqlQuery += "         AND workflow_completion_status = '1'";
            sqlQuery += "         AND enabled_flag = '1')";
            sqlQuery += "       AND LEVEL_CODE =";
            sqlQuery += "      (SELECT MIN(LEVEL_CODE)";
            sqlQuery += "        FROM WFM_WORKFLOW_MONITORS ";
            sqlQuery += "       WHERE STATUS NOT IN ('WFST01', 'WFST02')";
            sqlQuery += "       AND WORKFLOW_CODE = WM.WORKFLOW_CODE";
            sqlQuery += "       AND TRANSACTION_CODE = WM.TRANSACTION_CODE)";
            sqlQuery += "       AND ROLE_CODE IN (SELECT ROLE_CODE";
            sqlQuery += "        FROM SSM_USER_ROLE_INTERSECTS";
            sqlQuery += "       WHERE UPPER(USER_CODE) = UPPER( '" + VMVServices.Web.Utils.UserName + "')";
            sqlQuery += "      AND WORKFLOW_COMPLETION_STATUS = '1'";
            sqlQuery += "      AND ENABLED_FLAG = '1')";
            sqlQuery += "     ORDER BY created_date desc";

            return sqlQuery;

        }

        public static string getModuelName(string str_lng)
        {
            sqlQuery = string.Empty;
            sqlQuery = "SELECT MASTER_CODE,CODE,DESCRIPTION" + VMVServices.Web.Utils.LanguageCode + " AS DESCRIPTION FROM SSM_CODE_MASTERS WHERE PARENT_CODE='MODC' AND ENABLED_FLAG=1 ";
            //sqlQuery += " AND LANGUAGE_CODE='" + str_lng + "'"; 
            sqlQuery += " AND CODE NOT IN('CA','GL','PR') ORDER BY CODE";
            return sqlQuery;
        }
        public static string getModuelNam(string str_lng)
        {
            sqlQuery = string.Empty;
            sqlQuery = "SELECT MASTER_CODE,CODE,DESCRIPTION" + VMVServices.Web.Utils.LanguageCode + " AS DESCRIPTION FROM SSM_CODE_MASTERS WHERE PARENT_CODE='MODC' AND ENABLED_FLAG=1 ";
           // sqlQuery += "  AND LANGUAGE_CODE='" + str_lng + "' ";
            sqlQuery += " ORDER BY CODE";
            return sqlQuery;
        }
        public static string getMenuName(string str_ModuelName, string str_lng)
        {
            sqlQuery = string.Empty;
            sqlQuery = "SELECT SCREEN_CODE,SCREEN_NAME" + VMVServices.Web.Utils.LanguageCode + " as SCREEN_NAME FROM SSM_SCREENS WHERE MODULE_CODE='" + str_ModuelName + "' AND ENABLED_FLAG=1 ";
            //sqlQuery += " AND LANGUAGE_CODE='" + str_lng + "'";
            return sqlQuery;
        }
        public static string IsValueExists(string tableName, string IDColumn, string IDValue, string fieldName, string fieldvalue)
        {
            sqlQuery = string.Empty;
            sqlQuery = "select count(1) as count from " + tableName + " where " + IDColumn + "<>'" + IDValue.ToString() + "' and UPPER(" + fieldName + ")='" + fieldvalue.ToString().ToUpper() + "'";
            return sqlQuery;
        }
        public static string IsMultipleValueExists(string tableName, string IDColumn, int IDValue, string fieldName1, string fieldvalue1, string fieldName2, string fieldvalue2, string fieldName3, string fieldvalue3)
        {
            sqlQuery = string.Empty;
            sqlQuery = "select count(1) as count from " + tableName + " where " + IDColumn + "<>" + IDValue.ToString() + " and UPPER(" + fieldName1 + ")='" + fieldvalue1.ToString().ToUpper() + "'";

            if (fieldName2 != null && fieldvalue2 != null)
            {
                if (fieldName2 != string.Empty && fieldvalue2 != string.Empty)
                {
                    sqlQuery += " and UPPER(" + fieldName2 + ")='" + fieldvalue2.ToString().ToUpper() + "'";
                }
            }
            if (fieldName3 != null && fieldvalue3 != null)
            {
                if (fieldName3 != string.Empty && fieldvalue3 != string.Empty)
                {
                    sqlQuery += " and UPPER(" + fieldName3 + ")='" + fieldvalue3.ToString().ToUpper() + "'";
                }
            }
            return sqlQuery;
        }
        public static string GetVideopath(string str_FormCode = "")
        {
            sqlQuery = string.Empty;
            sqlQuery += " select mm.HELP_FILE_PATH as FilePath,NVL(mm.ORG_ID,'') as ORG_ID from ssm_menu_items mm where mm.FORM_CODE='" + str_FormCode + "'";
            return sqlQuery;
        }
        public static string getModuleName4User(string str_UserId)
        {
            sqlQuery = string.Empty;
            sqlQuery += " SELECT RMI.MODULE_CODE FROM SSM_ROLE_MODULE_INTERSECTS RMI  ";
            sqlQuery += " INNER JOIN SSM_USER_ROLE_INTERSECTS URI ON RMI.ROLE_CODE = URI.ROLE_CODE ";
            sqlQuery += " WHERE UPPER(URI.USER_CODE)='" + str_UserId + "'";
            return sqlQuery;
        }
        public static void UpdateAlertUserLevel()
        {
            sqlQuery = string.Empty;

            if (VMVServices.Web.Utils.IsAlert == "1")
            {
                sqlQuery = "  update SSM_ALERT_USER_LEVELS set END_DATE=sysdate,RESPONSED_BY='" + VMVServices.Web.Utils.UserName + "', RESPONSED_DATE=sysdate";
                sqlQuery += " where TRANSACTION_CODE='" + VMVServices.Web.Utils.RecordID + "'";

                DBMethod.ExecuteNonQuery(sqlQuery);
            }

        }

        public static string GetUploadedDocList(string recID, string fileID = "")
        {
            sqlQuery = string.Empty;

            sqlQuery = " select v.FILE_NAME,v.TMP_FILE_ID ";
            sqlQuery += " ,v.tmp_file_id || '.' || v.file_extenstion as filepath ";
            sqlQuery += " ,v.created_by as UPLOADED_BY,v.CREATED_DATE as UPLOADED_ON ";
            sqlQuery += " from TMP_FILE_DET v ";
            sqlQuery += " where v.TRANSACTION_ID='" + recID + "'";
            //sqlQuery += " and v.SCREEN_CODE='" + screenCode + "'";
            if (fileID != "")
            {
                sqlQuery += " and v.tmp_file_id='" + fileID + "'";
            }
            sqlQuery += " order by v.TMP_FILE_ID desc";

            return sqlQuery;
        }

        public static string UpdateUploadedDocList(string fileId)
        {
            sqlQuery = string.Empty;

            sqlQuery = " update TMP_FILE_DET set deleted='0' where TMP_FILE_ID='" + fileId + "'";

            return sqlQuery;
        }

        public static string GetInsurePlanDtlswtCond(string leveltype, string str_rec_id)
        {
            sqlQuery = string.Empty;
            sqlQuery = " Select * from INSURANCE_PLAN where PLAN_LEVAL = '" + leveltype + "'";
            sqlQuery += "  and PLAN_TO is null and ORG_ID = '" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " and plan_id != '" + str_rec_id + "'";
            return sqlQuery;

        }



    }
}
