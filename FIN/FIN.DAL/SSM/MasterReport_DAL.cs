﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace FIN.DAL.SSM
{
    public class MasterReport_DAL
    {
        static string sqlQuery = "";

        public static string getMasterrepdtl(string Master_id, string str_table_name)
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT LD.LIST_DTL_ID,TC.COLUMN_NAME,TC.MASTER_DISP_COLUMN_NAME,TC.DISPLAY_NAME,TC.WIDTH,TC.COL_TYPE,";
            sqlQuery += " TC.MASTER_TABLE_NAME, to_char(ld.hdr_dtl_link) as hdr_dtl_link,TC.FLAG_MASTER,CASE LD.LIST_DISPLAY WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS LIST_DISPLAY,'N' AS DELETED";
            sqlQuery += " FROM SSM_REP_LIST_DTL LD,SSM_REP_LIST_HDR LH,TAB_COL_STRUCT_DTL TC";
            sqlQuery += " WHERE LD.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND LD.LIST_HDR_ID = LH.LIST_HDR_ID";
            sqlQuery += " AND LD.DISPLAY_COLUMN_NM = TC.DISPLAY_NAME";
            sqlQuery += " AND LH.LIST_TABLE_NAME = TC.TABLE_NAME";
            sqlQuery += " AND LH.LIST_HDR_ID = '" + Master_id + "'";
            sqlQuery += " UNION ";
            sqlQuery += " SELECT '0' LIST_DTL_ID ,TC.COLUMN_NAME,TC.MASTER_DISP_COLUMN_NAME,TC.DISPLAY_NAME,TC.WIDTH,TC.COL_TYPE,";
            sqlQuery += " TC.MASTER_TABLE_NAME,";
            sqlQuery += " case TC.FLAG_MASTER when '0' then '' else (tc.table_name|| '.' ||tc.column_name|| '=' ||tc.master_link_column_name) end as HDR_DTL_LINK,";
            sqlQuery += " TC.FLAG_MASTER,'FALSE' AS LIST_DISPLAY,'N' AS DELETED";
            sqlQuery += " from TAB_COL_STRUCT_DTL tc";
            sqlQuery += " where tc.table_name = '" + str_table_name + "'";
            sqlQuery += " and tc.column_name not in (select  list_column_name from SSM_REP_LIST_DTL where LIST_HDR_ID = '" + Master_id + "')";


            return sqlQuery;
        }


        public static string getScreenName(string screen_code)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select s.screen_name";
            sqlQuery += " from ssm_screens s";
            sqlQuery += " where s.screen_code = '" + screen_code + "'";
            return sqlQuery;
        }

        public static string getScreenCde(string screen_code)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select s.list_screen_code";
            sqlQuery += " from SSM_REP_LIST_HDR s";
            sqlQuery += " where s.list_screen_code = '" + screen_code + "'";
            return sqlQuery;
        }



        public static string getDisplaycolumn(string table_name)
        {
            sqlQuery = " SELECT TC.DISPLAY_NAME,'0' LIST_DTL_ID,TC.COLUMN_NAME,TC.MASTER_DISP_COLUMN_NAME,TC.WIDTH,TC.COL_TYPE,TC.FLAG_MASTER,";
            sqlQuery += " TC.MASTER_TABLE_NAME,";
            sqlQuery += " case TC.FLAG_MASTER when '0' then '' else (tc.table_name|| '.' ||tc.column_name|| '=' ||tc.master_link_column_name) end as HDR_DTL_LINK,";
            sqlQuery += " 'FALSE' AS LIST_DISPLAY,'N' AS DELETED";
            sqlQuery += " from TAB_COL_STRUCT_DTL tc";
            sqlQuery += " where tc.table_name = '" + table_name + "'";
            return sqlQuery;
        }
        public static string getReportListName()
        {
            sqlQuery = string.Empty;
            sqlQuery = " select s.list_hdr_id, s.attribute1 as REPORT_NAME";
            sqlQuery += " from SSM_REP_LIST_HDR s";
            return sqlQuery;

        }
    }
}
