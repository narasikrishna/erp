﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.SSM
{
    public class States_DAL
    {
        static string sqlQuery = "";


        public static string getStateDetails()
        {
            sqlQuery = string.Empty;

            sqlQuery = "  select t.STATE_CODE,";
            sqlQuery += "  t.COUNTRY_CODE,";
            sqlQuery += "   t.FULL_NAME,";
            sqlQuery += "   t.SHORT_NAME,t.FULL_NAME||t.SHORT_NAME as State_name,";
            sqlQuery += "    t.EFFECTIVE_DATE,";
            sqlQuery += "    t.END_DATE,";
            sqlQuery += "   t.ENABLED_FLAG";
            sqlQuery += "  from ssm_states t";
            sqlQuery += "  where t.WORKFLOW_COMPLETION_STATUS = '1' ";
            sqlQuery += "  AND t.ENABLED_FLAG = '1' ";
            return sqlQuery;
        }
        public static string getStateDetails(string STATE_CODE)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select t.STATE_CODE,";
            sqlQuery += "  t.COUNTRY_CODE,(t.FULL_NAME) as State_name,";
            sqlQuery += "   t.FULL_NAME,";
            sqlQuery += "   t.SHORT_NAME,";
            sqlQuery += "    t.EFFECTIVE_DATE,";
            sqlQuery += "    t.END_DATE,";
            sqlQuery += "   t.ENABLED_FLAG";
            sqlQuery += "  from ssm_states t";
            sqlQuery += "  where t.WORKFLOW_COMPLETION_STATUS = '1' ";
            sqlQuery += "  AND t.ENABLED_FLAG = '1' ";
            sqlQuery += "  AND t.STATE_CODE = '" + STATE_CODE + "'";
            return sqlQuery;
        }
        public static string getStateDetailsBasedCountry(string Country_CODE)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select t.STATE_CODE,";
            sqlQuery += "  t.COUNTRY_CODE,(t.FULL_NAME) as State_name,";
            sqlQuery += "   t.FULL_NAME,";
            sqlQuery += "   t.SHORT_NAME,";
            sqlQuery += "    t.EFFECTIVE_DATE,";
            sqlQuery += "    t.END_DATE,";
            sqlQuery += "   t.ENABLED_FLAG";
            sqlQuery += "  from ssm_states t";
            sqlQuery += "  where t.WORKFLOW_COMPLETION_STATUS = '1' ";
            sqlQuery += "  AND t.ENABLED_FLAG = '1' ";
            sqlQuery += "  AND t.COUNTRY_CODE = '" + Country_CODE + "'";
            return sqlQuery;
        }


        public static string GetCountryNM(string country_code)
        {
            sqlQuery = string.Empty;


            sqlQuery = " select c.full_name"+ VMVServices.Web.Utils.LanguageCode +"";
            sqlQuery += " from SSM_COUNTRIES c";
            sqlQuery += " where c.enabled_flag = 1";
            sqlQuery += " and c.workflow_completion_status = 1";
            sqlQuery += " and c.country_code = '" + country_code + "'";

            return sqlQuery;
        }

        public static string getSteDtl(long STATE_CODE)
        {
            sqlQuery = string.Empty;


            sqlQuery = " SELECT S.PK_ID,S.STATE_CODE,S.FULL_NAME,S.FULL_NAME_OL,S.SHORT_NAME,S.EFFECTIVE_DATE,S.END_DATE,'0' AS DELETED,";
            sqlQuery += " CASE S.ENABLED_FLAG WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS ENABLED_FLAG";
            sqlQuery += " FROM SSM_STATES S";
            sqlQuery += " WHERE S.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND S.PK_ID = " + STATE_CODE;

            return sqlQuery;
        }
    }
}
