﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.SSM
{
    public class Menu_DAL
    {
        static string sqlQuery = "";

        public static string GetMenuData(int pkId)
        {
            sqlQuery = string.Empty;


            sqlQuery = " select mi. PK_ID,";
            sqlQuery += "  mi. CHILD_ID,";
            sqlQuery += "  mi. MENU_CODE,";
            sqlQuery += "   mi.FORM_CODE,";
            sqlQuery += "   mi. FORM_LABEL,";
            sqlQuery += "   mi.PARENT_MENU_CODE,";
            sqlQuery += "    mi.LANGUAGE_CODE,";
            sqlQuery += "   mi.MENU_URL,";
            sqlQuery += "   mi.ORDER_NO,";
            sqlQuery += "  case mi.enabled_flag when '1' then 'TRUE' else 'FALSE' END AS enabled_flag,";
            sqlQuery += "   mi.REPORT_NAME,";
            sqlQuery += "  mi.PK_COLUMN_NAME,";
            sqlQuery += "  mi.ENTRY_PAGE_OPEN,";
            sqlQuery += "  mi.report_code,";
            sqlQuery += "   'N' as Deleted";
            sqlQuery += "  from ssm_menu_items mi";
            sqlQuery += "  where mi.pk_id='" + pkId + "'";

            return sqlQuery;
        }
    }
}
