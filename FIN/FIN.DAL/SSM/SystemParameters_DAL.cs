﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.SSM
{
    public class SystemParameters_DAL
    {
        static string sqlQuery = "";

        public static string GetSysParamData(int pkId)
        {
            sqlQuery = string.Empty;

            sqlQuery = "   select ss.pk_id,ss.param_code,ss.param_name,ss.param_desc,ss.param_value,";
            sqlQuery += "   (case ss.enabled_flag when '1' then 'TRUE' ELSE 'FALSE' END) AS enabled_flag,'n' as deleted from SSM_SYSTEM_PARAMETERS ss";
            sqlQuery += "   WHERE ";
            sqlQuery += "   SS.WORKFLOW_COMPLETION_STATUS=1";
            sqlQuery += "   and ss.PK_ID=" + pkId;

            return sqlQuery;
        }
        public static string GetSysEmailParam()
        {
            sqlQuery = string.Empty;

            sqlQuery = "   select ss.param_value from ssm_system_parameters ss where ss.param_code='EMAIL_Y_N'";
            return sqlQuery;
        }
        public static string GetWeekOffDay()
        {
            sqlQuery = string.Empty;
            sqlQuery = "   select ss.param_value from ssm_system_parameters ss where ss.param_code in ('WEEKOFF1','WEEKOFF2') and param_value!='NULL'";
            return sqlQuery;
        }
        public static string GetFixedDays()
        {
            sqlQuery = string.Empty;
            sqlQuery = "   select sum(ss.param_value) from ssm_system_parameters ss where ss.param_code='FIXEDDAYS'";
            return sqlQuery;
        }
    }
}
