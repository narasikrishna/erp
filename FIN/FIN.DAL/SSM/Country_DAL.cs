﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.SSM
{
    public class Country_DAL
    {

        static string sqlQuery = "";


        public static string getCountryDetails(string Master_id)
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT  SC.PK_ID,sc.FULL_NAME" + VMVServices.Web.Utils.LanguageCode + " as  country_name,SC.COUNTRY_CODE, SC.FULL_NAME, SC.FULL_NAME_OL, SC.SHORT_NAME, SC.CURR_ID, SCU.CURRENCY_DESC AS CURRENCY_NAME, SC.EFFECTIVE_DATE, SC.END_DATE, 'N' AS DELETED , case SC.ENABLED_FLAG when '1' then 'TRUE' else 'FALSE' end as ENABLED_FLAG ";
            sqlQuery += " FROM SSM_COUNTRIES SC, SSM_CURRENCIES SCU ";
            sqlQuery += " WHERE SC.CURR_ID = SCU.CURRENCY_ID ";
            sqlQuery += " AND SC.WORKFLOW_COMPLETION_STATUS = '1' ";
            sqlQuery += " AND SC.ENABLED_FLAG = '1' ";
            sqlQuery += " AND SC.COUNTRY_CODE = '" + Master_id + "'";

            return sqlQuery;
        }

        public static string getCurrency()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT SCU.CURRENCY_ID, SCU.CURRENCY_DESC" + VMVServices.Web.Utils.LanguageCode +" AS CURRENCY_NAME ";
            sqlQuery += " FROM SSM_CURRENCIES SCU ";
            sqlQuery += " WHERE SCU.WORKFLOW_COMPLETION_STATUS = '1' ";
            sqlQuery += " AND SCU.ENABLED_FLAG = '1' ";
            sqlQuery += " ORDER BY CURRENCY_DESC ";

            return sqlQuery;
        }
        public static string getCountryDetails()
        {
            sqlQuery = string.Empty;

            sqlQuery = "          Select sc.COUNTRY_CODE,";
            sqlQuery += "      sc.COUNTRY_CODE||' - '||sc.FULL_NAME" + VMVServices.Web.Utils.LanguageCode +"  as country_name,";
            sqlQuery += "      sc.SHORT_NAME,";
            sqlQuery += " sc.Nationality, ";
            sqlQuery += "      sc.CURR_ID,";
            sqlQuery += "      sc.EFFECTIVE_DATE,";
            sqlQuery += "      sc.END_DATE,";
            sqlQuery += "       sc.ENABLED_FLAG";
            sqlQuery += "   from SSM_COUNTRIES sc";
            sqlQuery += "   where sc.enabled_flag = '1'";
            sqlQuery += "  and sc.workflow_completion_status = '1'";

            return sqlQuery; 
        }
//        public static string getCountryName()
//        {
//            sqlQuery = string.Empty;
//            sqlQuery +="SELECT S.COUNTRY_CODE,";
//            sqlQuery +="S.COUNTRY_CODE||' - '||S.FULL_NAME AS COUNTRY_NAME  
//FROM SSM_COUNTRIES S
//WHERE S.WORKFLOW_COMPLETION_STATUS='1'
//AND S.ENABLED_FLAG='1';

        public static string getCurrencyDetails()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT SSM.CURRENCY_CODE,SSM.CURRENCY_ID,ssm.currency_desc";
            sqlQuery += " FROM SSM_CURRENCIES SSM";
           
            return sqlQuery;
        }

        public static string getCurrencyReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery = "SELECT * FROM vw_ssm_currencies V WHERE ROWNUM > 0 ";

            //if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            //{
            //    if (VMVServices.Web.Utils.ReportViewFilterParameter["CURRENCY_CODE"] != null)
            //    {
            //        sqlQuery += " AND V.CURRENCY_CODE = '" + VMVServices.Web.Utils.ReportViewFilterParameter["CURRENCY_CODE"].ToString() + "'";
            //    }
            //    if (VMVServices.Web.Utils.ReportViewFilterParameter["CURRENCY_ID"] != null)
            //    {
            //        sqlQuery += " AND V.CURRENCY_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["CURRENCY_ID"].ToString() + "'";
            //    }
            //    if (VMVServices.Web.Utils.ReportViewFilterParameter["CURR_SYMBL"] != null)
            //    {
            //        sqlQuery += " AND V.CURRENCY_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["CURR_SYMBL"].ToString() + "'";
            //    }
            //    if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.STATE_CODE] != null)
            //    {
            //        sqlQuery += " AND V.INIT_AMT_SEPERATOR = '" + VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.STATE_CODE].ToString() + "'";
            //    }
            //    if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.CITY_CODE] != null)
            //    {
            //        sqlQuery += " AND V.CITY_CODE = '" + VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.CITY_CODE].ToString() + "'";
            //    }


            //}

            return sqlQuery;
        }

    }
}
