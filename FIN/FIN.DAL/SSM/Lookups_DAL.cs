﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.SSM
{
    public class Lookups_DAL
    {

        static string sqlQuery = "";

        public static string GetLookUpData(int pkId)
        {
            sqlQuery = string.Empty;
            sqlQuery = " select sm.pk_id,sm.MASTER_CODE,sm.CODE,sm.DESCRIPTION,sm.DESCRIPTION_OL,sm.EFFECTIVE_DATE,sm.PARENT_CODE,sm.PARENT_TYPE,sm.END_DATE,sm.ORDER_NO, ";
            sqlQuery += "      (case sm.ENABLED_FLAG";
            sqlQuery += "      when '1' then";
            sqlQuery += "        'TRUE'";
            sqlQuery += "          ELSE";
            sqlQuery += "       'FALSE'";
            sqlQuery += "         END) AS ENABLED_FLAG,";
            sqlQuery += "    'N' as deleted";
            sqlQuery += "  from SSM_code_masters sm ";
            sqlQuery += "   where sm.workflow_completion_status = 1";
            sqlQuery += "  and sm.pk_id='" + pkId + "'";

            return sqlQuery;
        }
        public static string GetLookUpData4ParentCode(string Parent_Code)
        {
            sqlQuery = string.Empty;
            sqlQuery = " select sm.pk_id,sm.MASTER_CODE,sm.CODE,sm.DESCRIPTION,sm.DESCRIPTION_OL,sm.EFFECTIVE_DATE,sm.PARENT_CODE,sm.PARENT_TYPE,sm.END_DATE,sm.ORDER_NO, ";
            sqlQuery += "      (case sm.ENABLED_FLAG";
            sqlQuery += "      when '1' then";
            sqlQuery += "        'TRUE'";
            sqlQuery += "          ELSE";
            sqlQuery += "       'FALSE'";
            sqlQuery += "         END) AS ENABLED_FLAG,";
            sqlQuery += "    'N' as deleted";
            sqlQuery += "  from SSM_code_masters sm ";
            sqlQuery += "   where sm.workflow_completion_status = 1";
            sqlQuery += "  and sm.PARENT_CODE='" + Parent_Code + "'";
            sqlQuery += " ORDER bY SM.Master_Code";

            return sqlQuery;
        }

        public static string GetLookUp()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT distinct scm.parent_code ";
            sqlQuery += " FROM SSM_CODE_MASTERS SCM where scm.parent_code is not null";

            sqlQuery += " ORDER BY SCM.parent_code asc  ";

            return sqlQuery;
        }
    }
}
