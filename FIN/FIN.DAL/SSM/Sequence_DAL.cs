﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.SSM
{
    public class Sequence_DAL
    {
        static string sqlQuery = "";


        public static string getSequenceDetails(int Master_id)
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT SS.PK_ID,SS.SEQUENCE_CODE, SS.SCREEN_CODE, SC.SCREEN_NAME, SS.DESCRIPTION, SS.WIDTH, SS.SEQUENCE_FORMAT, SS.PAD_ZERO, SS.START_WITH, SS.CURRENT_DOCUMENT_NUMBER, SS.EFFECTIVE_DATE, SS.INCREMENT_BY, 'N' AS DELETED , case SS.ENABLED_FLAG when '1' then 'TRUE' else 'FALSE' end as ENABLED_FLAG ";
            sqlQuery += " FROM SSM_SEQUENCES SS, SSM_SCREENS SC ";
            sqlQuery += " WHERE SS.WORKFLOW_COMPLETION_STATUS = '1' ";
            sqlQuery += " AND SS.ENABLED_FLAG = '1' ";
            sqlQuery += " AND SS.SCREEN_CODE = SC.SCREEN_CODE(+) ";
            sqlQuery += " AND SS.PK_ID = '" + Master_id + "'";

            return sqlQuery;
        }

        public static string getScreenCode()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT SS.SCREEN_CODE, SS.SCREEN_NAME ";
            sqlQuery += " FROM SSM_SCREENS SS ";
            sqlQuery += " WHERE SS.WORKFLOW_COMPLETION_STATUS = '1' ";
            sqlQuery += " AND SS.ENABLED_FLAG = '1' ";
            sqlQuery += " ORDER BY SS.SCREEN_NAME ";

            return sqlQuery;
        }



    }
}
