﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.SSM
{
    public class RoleModules_DAL
    {

        static string sqlQuery = "";

        public static string GetRoleModuleData(int pkId)
        {
            sqlQuery = string.Empty;

            sqlQuery = "    select sr.pk_id,sr.role_code,sr.module_code,(select distinct s.description from ssm_screens s where s.module_code=sr.module_code and rownum=1) as MODULE_desc,";
            sqlQuery += "   (case sr.enabled_flag when '1' then 'TRUE' ELSE 'FALSE' END) AS enabled_flag,'n' as deleted,";
            sqlQuery += "   (case sr.MODULE_ACCESS_FLAG when '1' then 'TRUE' ELSE 'FALSE' END) AS MODULE_ACCESS_FLAG";
            sqlQuery += "  from ssm_role_module_intersects sr";
            sqlQuery += "  where sr.workflow_completion_status=1";
            sqlQuery += "  and sr.pk_id=" + pkId;

            return sqlQuery;

        }

        public static string GetRoleModuleData4RoleCode(string str_RoleCode)
        {
            sqlQuery = string.Empty;

            sqlQuery = "    select sr.pk_id,sr.role_code,sr.module_code,(select distinct s.description from ssm_screens s where s.module_code=sr.module_code and rownum=1) as MODULE_desc,";
            sqlQuery += "   (case sr.enabled_flag when '1' then 'TRUE' ELSE 'FALSE' END) AS enabled_flag,'n' as deleted,";
            sqlQuery += "   (case sr.MODULE_ACCESS_FLAG when '1' then 'TRUE' ELSE 'FALSE' END) AS MODULE_ACCESS_FLAG";
            sqlQuery += "  from ssm_role_module_intersects sr";
            sqlQuery += "  where sr.workflow_completion_status=1";
            sqlQuery += "  and sr.role_code='" + str_RoleCode + "'";

            return sqlQuery;

        }
    }
}
