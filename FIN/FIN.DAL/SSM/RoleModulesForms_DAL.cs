﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.SSM
{
    public class RoleModulesForms_DAL
    {
        static string sqlQuery = "";

        public static string GetRoleModuleFormsData(int pkId)
        {
            sqlQuery = string.Empty;

            sqlQuery = "    select sr.pk_id,";
            sqlQuery += "   sr.role_code,";
            sqlQuery += "   sr.module_code,";
            sqlQuery += "   sr.form_code, mi.screen_name as FORM_DESC,  ";
            sqlQuery += "   (case sr.insert_allowed_flag when '1' then 'TRUE' ELSE 'FALSE' END) AS insert_allowed_flag,";
            sqlQuery += "   (case sr.UPDATE_ALLOWED_FLAG when '1' then 'TRUE' ELSE 'FALSE' END) AS UPDATE_ALLOWED_FLAG,";
            sqlQuery += "   (case sr.DELETE_ALLOWED_FLAG when '1' then 'TRUE' ELSE 'FALSE' END) AS DELETE_ALLOWED_FLAG,";
            sqlQuery += "   (case sr.QUERY_ALLOWED_FLAG when '1' then 'TRUE' ELSE 'FALSE' END) AS QUERY_ALLOWED_FLAG,";
            sqlQuery += "   (case sr.FORM_ACCESS_FLAG when '1' then 'TRUE' ELSE 'FALSE' END) AS FORM_ACCESS_FLAG,   ";
            sqlQuery += "   (case sr.ENABLED_FLAG when '1' then 'TRUE' ELSE 'FALSE' END) AS ENABLED_FLAG,";
            sqlQuery += "      'n' as deleted";
            sqlQuery += "   from ssm_role_form_permissions sr, ssm_screens mi";
            sqlQuery += "   where sr.form_code = mi.SCREEN_CODE";
            //   sqlQuery += "   and sr.pk_id='" + pkId + "'";
            sqlQuery += "   and sr.module_code in (select rr.module_code from ssm_role_module_intersects rr where rr.pk_id='" + pkId + "')";
            sqlQuery += "   and sr.role_code in (select rr.role_code from ssm_role_module_intersects rr where rr.pk_id='" + pkId + "')";
            sqlQuery += "   order by sr.module_code, sr.form_code";

            return sqlQuery;

        }

        public static string getRoleDashBoardData(string str_Role_id, string module_code)
        {
            sqlQuery = string.Empty;
            sqlQuery += "  SELECT RDP.PK_ID, RDP.ROLL_CODE,DF.MODEL_CODE,DF.DB_FRAME_ID,DF.DB_FRAME_NAME,CASE WHEN RDP.VIEW_FLAG =1 THEN 'TRUE' ELSE 'FALSE' END AS VIEW_FLAG ";
            sqlQuery += " FROM ROLL_DASHBOARD_PERMISSION RDP ";
            sqlQuery += " INNER JOIN DASHBRORD_FRAMES DF ON DF.DB_FRAME_ID =RDP.DB_FRAME_ID ";
            sqlQuery += " WHERE RDP.MODEL_CODE='" + module_code + "' ";
            sqlQuery += " AND RDP.ROLL_CODE='" + str_Role_id + "' ";
            sqlQuery += " UNION ";
            sqlQuery += " SELECT 0 as PK_ID, '' AS ROLL_CODE,DF.MODEL_CODE,DF.DB_FRAME_ID,DF.DB_FRAME_NAME,'FALSE' AS VIEW_FLAG FROM DASHBRORD_FRAMES DF ";
            sqlQuery += " WHERE DF.DB_FRAME_ID NOT IN (SELECT SDF.DB_FRAME_ID  FROM ROLL_DASHBOARD_PERMISSION SDF WHERE SDF.MODEL_CODE='" + module_code + "' AND SDF.ROLL_CODE='" + str_Role_id + "' )";
            sqlQuery += " AND DF.MODEL_CODE='" + module_code + "'";
            return sqlQuery;
        }

        public static string getRoleDashBoardData4User(string str_userId, string module_code)
        {
            sqlQuery = string.Empty;
            sqlQuery += "  SELECT RDP.PK_ID, RDP.ROLL_CODE,DF.MODEL_CODE,DF.DB_FRAME_ID,DF.DB_FRAME_NAME,CASE WHEN RDP.VIEW_FLAG =1 THEN 'TRUE' ELSE 'FALSE' END AS VIEW_FLAG ";
            sqlQuery += " , DF.DB_FRAME_DIV_ID ";
            sqlQuery += " FROM ROLL_DASHBOARD_PERMISSION RDP ";
            sqlQuery += " INNER JOIN DASHBRORD_FRAMES DF ON DF.DB_FRAME_ID =RDP.DB_FRAME_ID ";
            sqlQuery += " WHERE RDP.MODEL_CODE='" + module_code + "' ";
            sqlQuery += " AND RDP.ROLL_CODE in ( select ROLE_CODE from ssm_user_role_intersects where user_code='" + str_userId + "')";

            return sqlQuery;
        }

        public static string GetRoleModuleForms(string moduleCode, string str_role_code)
        {
            sqlQuery = string.Empty;

            sqlQuery = "    select sr.pk_id,";
            sqlQuery += "   sr.role_code,";
            sqlQuery += "   sr.module_code,";
            sqlQuery += "   sr.form_code, mi.screen_name as FORM_DESC,  ";
            sqlQuery += "   (case sr.insert_allowed_flag when '1' then 'TRUE' ELSE 'FALSE' END) AS insert_allowed_flag,";
            sqlQuery += "   (case sr.UPDATE_ALLOWED_FLAG when '1' then 'TRUE' ELSE 'FALSE' END) AS UPDATE_ALLOWED_FLAG,";
            sqlQuery += "   (case sr.DELETE_ALLOWED_FLAG when '1' then 'TRUE' ELSE 'FALSE' END) AS DELETE_ALLOWED_FLAG,";
            sqlQuery += "   (case sr.QUERY_ALLOWED_FLAG when '1' then 'TRUE' ELSE 'FALSE' END) AS QUERY_ALLOWED_FLAG,";
            sqlQuery += "   (case sr.FORM_ACCESS_FLAG when '1' then 'TRUE' ELSE 'FALSE' END) AS FORM_ACCESS_FLAG,   ";
            sqlQuery += "   (case sr.ENABLED_FLAG when '1' then 'TRUE' ELSE 'FALSE' END) AS ENABLED_FLAG,";
            sqlQuery += "      'n' as deleted";
            sqlQuery += "   from ssm_role_form_permissions sr, ssm_screens mi";
            sqlQuery += "   where sr.form_code = mi.SCREEN_CODE";
            sqlQuery += "   and sr.module_code='" + moduleCode + "'";
            sqlQuery += "   and sr.role_code='" + str_role_code + "' ";
            sqlQuery += " and mi.enabled_flag=1 ";
            sqlQuery += " UNION ";
            sqlQuery += "    select 0 as pk_id,";
            sqlQuery += "   '" + str_role_code + "' as role_code,";
            sqlQuery += "   mi.module_code,";
            sqlQuery += "   mi.screen_code as form_code, mi.screen_name as FORM_DESC,  ";
            sqlQuery += "   'FALSE' AS insert_allowed_flag,";
            sqlQuery += "    'FALSE'  AS UPDATE_ALLOWED_FLAG,";
            sqlQuery += "    'FALSE'  AS DELETE_ALLOWED_FLAG,";
            sqlQuery += "    'FALSE'  AS QUERY_ALLOWED_FLAG,";
            sqlQuery += "    'FALSE'  AS FORM_ACCESS_FLAG,   ";
            sqlQuery += "    'FALSE' AS ENABLED_FLAG,";
            sqlQuery += "      'n' as deleted";
            sqlQuery += "   from  ssm_screens mi";
            sqlQuery += "   where   MI.module_code='" + moduleCode + "'";
            sqlQuery += " and mi.screen_code not in (select form_code from ssm_role_form_permissions where role_code='" + str_role_code + "')";
            sqlQuery += " and mi.enabled_flag=1 ";
            sqlQuery += "   order by insert_allowed_flag";

            return sqlQuery;

        }
    }
}
