﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.SSM
{
    public class User_DAL
    {
        static string sqlQuery = "";

        public static string GetUserData()
        {
            sqlQuery = string.Empty;

            sqlQuery = "select su.PK_ID,";
            sqlQuery += " su.CHILD_ID,";
            sqlQuery += " su.USER_CODE,";
            sqlQuery += " su.USER_PASSWORD,";
            sqlQuery += " su.TITLE,";
            sqlQuery += " su.FIRST_NAME,";
            sqlQuery += " su.MIDDLE_NAME,";
            sqlQuery += " su.LAST_NAME,";
            sqlQuery += " su.SURNAME,";
            sqlQuery += " su.GENDER,";
            sqlQuery += " su.DATE_OF_BIRTH,";
            sqlQuery += " su.NATIONALITY,";
            sqlQuery += " su.ADDRESS_LINE_1,";
            sqlQuery += " su.ADDRESS_LINE_2,";
            sqlQuery += " su.CITY,";
            sqlQuery += " su.STATE,";
            sqlQuery += " su.POSTAL_CODE,";
            sqlQuery += " su.COUNTRY,";
            sqlQuery += " su.PHONE,";
            sqlQuery += " su.MOBILE_NO,";
            sqlQuery += " su.FAX_NUMBER,";
            sqlQuery += " su.URL,";
            sqlQuery += " su.EMAIL_ID,";
            sqlQuery += " su.TELEX_NO,";
            sqlQuery += " su.EFFECTIVE_DATE,";
            sqlQuery += " su.END_DATE,";
            sqlQuery += " su.CHANGE_PASSWORD_ON_NEXT_LOGIN,";
            sqlQuery += " su.CHANGE_PASSWORD_INTERVAL,";
            sqlQuery += " su.ENABLED_FLAG,   ";
            sqlQuery += " su.SUPERVISOR_CODE,";
            sqlQuery += " su.PASSPORT_NUMBER_OR_EMP_ID,";
            sqlQuery += " su.PASSWORD_CHANGE_FREQUENCY,";
            sqlQuery += " su.FREQUENCY_OF_PASSWORD_CHANGE,";
            sqlQuery += " su.PASSWORD_CHANGE_DATE,";
            sqlQuery += " su.DEFAULT_LANGUAGE,";
            sqlQuery += " su.GROUP_ID,";
            sqlQuery += " su.PASSWORD_LOCK,";
            sqlQuery += " su.BRANCH_ZONE,";
            sqlQuery += " (su.first_name || ' ' || su.middle_name || ' ' || su.last_name) as user_name";
            sqlQuery += " from ssm_users su";
            sqlQuery += " where su.workflow_completion_status = '1'";
            sqlQuery += " and su.enabled_flag = '1'";

            return sqlQuery;

        }



        public static string GetUserOrgIntersectData(string USER_CODE)
        {
            sqlQuery = string.Empty;


            sqlQuery = " SELECT UO.PK_ID,UO.EFFECTIVE_DATE,UO.END_DATE,C.COMP_ID,C.COMP_INTERNAL_NAME" + VMVServices.Web.Utils.LanguageCode + " as COMP_INTERNAL_NAME,'0' AS DELETED,";
            sqlQuery += " CASE UO.ENABLED_FLAG WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS ENABLED_FLAG";
            sqlQuery += " FROM SSM_USER_ORG_INTERSECTS UO,GL_COMPANIES_HDR C";
            sqlQuery += " WHERE UO.ORG_ID = C.COMP_ID";
            sqlQuery += " AND UO.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND UO.USER_CODE = '" + USER_CODE + "'";
            return sqlQuery;
        }

        public static string GetUserRoleIntersectData(string USER_CODE)
        {
            sqlQuery = string.Empty;


            sqlQuery = " SELECT UR.PK_ID,UR.EFFECTIVE_DATE,UR.END_DATE,R.ROLE_CODE,R.ROLE_NAME,'N' AS DELETED,";
            sqlQuery += " CASE UR.ENABLED_FLAG WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS ENABLED_FLAG";
            sqlQuery += " FROM SSM_USER_ROLE_INTERSECTS UR,SSM_ROLES R";
            sqlQuery += " WHERE UR.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND UR.ROLE_CODE = R.ROLE_CODE";
            sqlQuery += " AND UR.USER_CODE = '" + USER_CODE + "'";

            return sqlQuery;
        }

        public static string getUserLoginDetails(string str_user_code)
        {
            sqlQuery = string.Empty;
            sqlQuery += " select su.pk_id,su.password_lock,su.user_password,su.user_code,su.first_name" + VMVServices.Web.Utils.LanguageCode + " || ' ' || su.middle_name" + "" + " || ' ' || su.last_name" + "" + "  as EMP_NAME ";
            sqlQuery += " ,su.change_password_on_next_login ";
            sqlQuery += " from ssm_users su ";
            sqlQuery += " where (su.ATTRIBUTE1 != 'EMPLOYEE' or su.ATTRIBUTE1 is null) and UPPER(su.user_code)='" + str_user_code + "'";
            return sqlQuery;
        }

        public static string getCreatedBy()
        {
            sqlQuery = string.Empty;
            sqlQuery += " SELECT DISTINCT SU.USER_CODE, SU.USER_CODE || ' - ' || SU.FIRST_NAME AS USER_NAME FROM SSM_USERS SU ";
            return sqlQuery;
        }
        
        public static string getReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_SSM_AUDIT_TRAIL V WHERE ROWNUM > 0 ";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["FORM_CODE"] != null)
                {
                    sqlQuery += " AND V.SCREEN_CODE = '" + VMVServices.Web.Utils.ReportViewFilterParameter["FORM_CODE"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["USER_CODE"] != null)
                {
                    sqlQuery += " AND V.USER_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["USER_CODE"].ToString() + "'";
                }
                //if (VMVServices.Web.Utils.ReportViewFilterParameter["Emp_id"] != null)
                //{
                //    sqlQuery += " AND V.emp_id = '" + VMVServices.Web.Utils.ReportViewFilterParameter["Emp_id"].ToString() + "'";
                //}
                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
                {
                    sqlQuery += " AND V.created_date >= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy')";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
                {
                    sqlQuery += " AND V.created_date <= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["To_Date"].ToString() + "','dd/MM/yyyy')";
                }

            }


            return sqlQuery;
        }



    }

}
