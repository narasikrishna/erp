﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.SSM
{
    public class Role_DAL
    {
        static string sqlQuery = "";

        public static string GetRoleData(int pkId)
        {
            sqlQuery = string.Empty;

            sqlQuery = "    select sr.PK_ID,sr.role_code,";
            sqlQuery += "    sr.role_name,sr.role_name_ol,";
            sqlQuery += "     sr.start_date,";
            sqlQuery += "     (case sr.enabled_flag";
            sqlQuery += "      when '1' then";
            sqlQuery += "     'TRUE'";
            sqlQuery += "    ELSE";
            sqlQuery += "     'FALSE'";
            sqlQuery += "     END) AS enabled_flag,";
            sqlQuery += "     'n' as deleted";
            sqlQuery += "    from ssm_roles sr";
            sqlQuery += "   where sr.workflow_completion_status='1'";
            sqlQuery += "     and sr.PK_ID=" + pkId;

            return sqlQuery;

        }
        public static string GetRole(string ROLE_CODE = "")
        {
            sqlQuery = string.Empty;

            sqlQuery = "    select sr.PK_ID,sr.role_code,";
            sqlQuery += "    sr.role_name,";
            sqlQuery += "     sr.start_date,";
            sqlQuery += "     (case sr.enabled_flag";
            sqlQuery += "      when '1' then";
            sqlQuery += "     'TRUE'";
            sqlQuery += "    ELSE";
            sqlQuery += "     'FALSE'";
            sqlQuery += "     END) AS enabled_flag,";
            sqlQuery += "     'n' as deleted";
            sqlQuery += "    from ssm_roles sr";
            sqlQuery += "   where sr.workflow_completion_status='1'";
            sqlQuery += "   and sr.enabled_flag='1'";

            if (ROLE_CODE != string.Empty)
            {
                sqlQuery += "     and sr.ROLE_CODE='" + ROLE_CODE + "'";
            }
            sqlQuery += "   order by sr.role_name";

            return sqlQuery;

        }

        public static string GetModuleList4Role(string str_Role)
        {
            sqlQuery = string.Empty;

            //sqlQuery = " select distinct s.module_code,s.description from ssm_screens s ";
            sqlQuery += " SELECT distinct RMI.MODULE_CODE,RMI.MODULE_CODE as description FROM SSM_ROLE_MODULE_INTERSECTS RMI  ";
            //sqlQuery += " INNER JOIN SSM_USER_ROLE_INTERSECTS URI ON RMI.ROLE_CODE = URI.ROLE_CODE ";
            sqlQuery += " WHERE  RMI.ROLE_CODE='" + str_Role + "'";
            sqlQuery += "   order by 1";
            return sqlQuery;
        }
    }
}
