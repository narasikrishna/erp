﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.SSM
{
    public class SystemOptions_DAL
    {
        static string sqlQuery = "";


        public static string getAnnualLeave()
        {
            sqlQuery = string.Empty;
            sqlQuery += " SELECT * FROM SSM_SYSTEM_OPTIONS SSO  ";
            sqlQuery += " WHERE SSO.ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " AND SSO.hr_earn_leave IS NOT NULL ";
            sqlQuery += "  AND ((sysdate BETWEEN SSO.HR_EFFECTIVE_FROM_DATE AND ";
            sqlQuery += "   SSO.HR_EFFECTIVE_TO_DATE) OR (SSO.HR_EFFECTIVE_FROM_DATE <= sysdate AND ";
            sqlQuery += " SSO.HR_EFFECTIVE_TO_DATE IS NULL)) ";
            return sqlQuery;
        }


        public static string getSysoptID()
        {
            sqlQuery = string.Empty;

            sqlQuery += " SELECT * ";
            sqlQuery += " FROM SSM_SYSTEM_OPTIONS SO";
            sqlQuery += " WHERE SO.WORKFLOW_COMPLETION_STATUS = 1";
            //sqlQuery += " AND SO.ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            //  sqlQuery += " AND SO.MODULE_CODE = '" + MODULE_CODE + "'";
            return sqlQuery;
        }

        public static string getSysoptIDData(string MODULE_CODE)
        {
            sqlQuery = string.Empty;

            sqlQuery += " SELECT * ";
            sqlQuery += " FROM SSM_SYSTEM_OPTIONS SO";
            sqlQuery += " WHERE SO.WORKFLOW_COMPLETION_STATUS = 1";
            //sqlQuery += " AND SO.ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " AND SO.MODULE_CODE = '" + MODULE_CODE + "'";
            return sqlQuery;
        }


    }
}
