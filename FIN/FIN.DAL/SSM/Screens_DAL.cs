﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.SSM
{
    public class Screens_DAL
    {
        static string sqlQuery = "";

        public static string GetScreenData(int pkId)
        {
            sqlQuery = string.Empty;

            sqlQuery = "      Select ss.PK_ID, (case when ss.audit_trail='1' then 'TRUE' else 'FALSE' end) as audit_trail,";
            sqlQuery += "     ss.CHILD_ID,";
            sqlQuery += "      ss.LANGUAGE_CODE,";
            sqlQuery += "     ss.SCREEN_CODE,";
            sqlQuery += "     ss.SCREEN_NAME,";
            sqlQuery += "    ss.MODULE_CODE,";
            sqlQuery += "     ss.DESCRIPTION,'N' as Deleted,";
            sqlQuery += "   ss.UNIT_TYPE";
            sqlQuery += "  from SSM_SCREENS ss";
            sqlQuery += "  where ss.pk_id='" + pkId + "'";

            return sqlQuery;
        }


        public static string GetScreenName()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT S.SCREEN_CODE,(S.SCREEN_CODE||' - '||S.SCREEN_NAME) aS SCREEN_NAME,S.SCREEN_NAME as screen_desc";
            sqlQuery += " FROM SSM_SCREENS S";
            sqlQuery += " WHERE S.ENABLED_FLAG = 1";
            sqlQuery += " AND S.WORKFLOW_COMPLETION_STATUS = 1";
            return sqlQuery;
        }

        public static string GetModuleData()
        {
            sqlQuery = string.Empty;

            sqlQuery = " select * from ssm_role_module_intersects";

            return sqlQuery;
        }
        public static string GetModuleData(string moduleCode)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select * from ssm_role_module_intersects k where k.MODULE_CODE='" + moduleCode + "'";

            return sqlQuery;
        }
        public static string GetModuleList(string module_code = "")
        {
            sqlQuery = string.Empty;

            sqlQuery = " select distinct s.module_code,s.description from ssm_screens s ";

            if (module_code != string.Empty)
            {
                sqlQuery += "  where s.module_code='" + module_code + "'";
            }

            sqlQuery += "   order by 1";
            return sqlQuery;
        }

        public static string GetScreenName(string moduleCode, string screecode = "")
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT  s.SCREEN_CODE,(S.SCREEN_CODE||' - '||S.SCREEN_NAME) aS SCREEN_NAME,S.SCREEN_NAME as screen_desc";
            sqlQuery += " FROM SSM_SCREENS S";
            sqlQuery += " WHERE S.ENABLED_FLAG = 1";
            sqlQuery += " AND S.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += "  and s.module_code='" + moduleCode + "'";
            if (screecode != string.Empty)
            {
                sqlQuery += "  and s.SCREEN_CODE='" + screecode + "'";
            }
            sqlQuery += "   order by 1";
            return sqlQuery;
        }
    }
}
