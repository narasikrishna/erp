﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.SSM
{
    public class WorkflowDAL
    {
        static string sqlQuery = "";

        public static string GetWorkFlowData(int pkId)
        {
            sqlQuery = string.Empty;

            sqlQuery = "              select wl.pk_id,wl.remarks,wl.role_code,(select r.role_name from ssm_roles r where r.role_code = wl.role_code) as ROLE_DESC,wl.workflow_code,cm.description,wl.effective_date,wl.end_date,";
            sqlQuery += "       (case when cm.enabled_flag='1' then 'TRUE' else 'FALSE' end) as enabled_flag,'N' as Deleted";
            sqlQuery += "       from wfm_workflow_code_masters cm, wfm_workflow_levels wl";
            sqlQuery += "      where cm.workflow_code = wl.workflow_code";
            sqlQuery += "  and cm.pk_id='" + pkId + "'";
            //sqlQuery = "        select cm.pk_id,cm.ROLE_DESC,cm.role_code,cm.remarks,";
            //sqlQuery += "       cm.workflow_code,";
            //sqlQuery += "        cm.description,";
            //sqlQuery += "       cm.effective_date,";
            //sqlQuery += "        cm.end_date,";
            //sqlQuery += "       (case when cm.enabled_flag='1' then 'TRUE' else 'FALSE' end) as enabled_flag,'N' as Deleted";
            //sqlQuery += "      from wfm_workflow_code_masters cm";
            //sqlQuery += " where cm.pk_id='" + pkId + "'";

            return sqlQuery;

        }

        public static string GetWorkFlowEmpData(int pkId)
        {
            sqlQuery = string.Empty;

            sqlQuery = "              select wl.pk_id,wl.remarks,wl.role_code,(select r.role_name from ssm_roles r where r.role_code = wl.role_code) as ROLE_DESC,wl.workflow_code,cm.description,wl.effective_date,wl.end_date,";
            sqlQuery += "    ss.emp_id, ss.emp_no || ' ' || ss.emp_first_name || ' ' || ss.emp_middle_name || ' ' ||ss.emp_last_name as emp_name,";
            sqlQuery += "       (case when wl.enabled_flag='1' then 'TRUE' else 'FALSE' end) as enabled_flag,'N' as Deleted,";
            sqlQuery += "     (case when wl.EXCEPTION_FLAG='1' then 'TRUE' else 'FALSE' end) as EXCEPTION_FLAG";
            sqlQuery += "       from wfm_workflow_code_masters cm, wfm_workflow_levels wl,hr_employees ss";
            sqlQuery += "      where cm.workflow_code = wl.workflow_code and ss.emp_id = wl.emp_id";
            sqlQuery += "  and cm.pk_id=wl.CHILD_ID";
            sqlQuery += "  and cm.pk_id='" + pkId + "'";


            return sqlQuery;

        }
    }
}
