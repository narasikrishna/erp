﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.SSM
{
    public class UserRoleDetails_DAL
    {
        static string sqlQuery = "";

        public static string GetUserRoleData(int pkId)
        {
            sqlQuery = string.Empty;

            sqlQuery = "           select em.pk_id,";
            sqlQuery += "        em.effective_date,";
            sqlQuery += "        em.end_date,";
            sqlQuery += "       em.role_code,(select sr.ROLE_NAME from ssm_roles sr where sr.role_code=em.role_code)as role_desc,";
            sqlQuery += "       em.user_code,";
            sqlQuery += "      (case em.ENABLED_FLAG";
            sqlQuery += "      when '1' then";
            sqlQuery += "        'TRUE'";
            sqlQuery += "          ELSE";
            sqlQuery += "       'FALSE'";
            sqlQuery += "         END) AS ENABLED_FLAG,";
            sqlQuery += "    'N' as deleted";
            sqlQuery += "    from ssm_user_role_intersects em";
            sqlQuery += "   where em.workflow_completion_status = 1";
            sqlQuery += "  and em.pk_id='" + pkId + "'";
            return sqlQuery;

        }
    }
}
