﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.SSM
{
    public class TableColumnStruct_DAL
    {
        static string sqlQuery = "";

        public static string GetTableName()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT UT.TABLE_NAME,UT.TABLE_NAME";
            sqlQuery += " FROM USER_TABLES UT";
            return sqlQuery;
        }

        public static string GetColName(string TABLE_NAME)
        {
            sqlQuery = string.Empty;


            sqlQuery = " SELECT uc.COLUMN_NAME,uc.COLUMN_NAME";
            sqlQuery += " FROM USER_TAB_COLUMNS uc";
            sqlQuery += " where uc.TABLE_NAME = '" + TABLE_NAME + "'";

            return sqlQuery;
        }

        public static string GetScreenName()
        {
            sqlQuery = string.Empty;


            sqlQuery = " SELECT S.SCREEN_CODE,S.SCREEN_NAME";
            sqlQuery += " FROM SSM_SCREENS S";

            return sqlQuery;
        }

        public static string getScreenNameinTSD()
        {
            sqlQuery = string.Empty;

            sqlQuery = "select distinct S.SCREEN_CODE,S.SCREEN_NAME from tab_col_struct_dtl sd ";
            sqlQuery += "inner join SSM_SCREENS s on s.screen_code= sd.screen_code ";

            return sqlQuery;
        }
        public static string GetTableName_basedscreencode(string SCREEN_CODE)
        {
            sqlQuery = string.Empty;

            //sqlQuery +=" select distinct table_name from TAB_COL_STRUCT_DTL where  screen_code='" + SCREEN_CODE +"'";
            //sqlQuery += " UNION ";
            sqlQuery = " SELECT S.TABLE_NAME,S.TABLE_NAME";
            sqlQuery += " FROM SSM_SCREENS S";
            sqlQuery += " WHERE S.SCREEN_CODE = '" + SCREEN_CODE + "'";
            // sqlQuery += " AND S.SCREEN_CODE NOT IN (SELECT SCREEN_CODE FROM TAB_COL_STRUCT_DTL)";
            return sqlQuery;
        }


        public static string GetTabStructdtl_tcsid(string tab_col_struct_id)
        {
            sqlQuery = string.Empty;



            sqlQuery = "  SELECT cs.tab_col_struct_id,cs.table_name,cs.column_name,cs.master_table_name,cs.master_disp_column_name,";
            sqlQuery += " cs.master_link_column_name,cs.display_name,cs.display_name_ol,cs.width";
            sqlQuery += " FROM TAB_COL_STRUCT_DTL  cs";
            sqlQuery += " where cs.tab_col_struct_id = '" + tab_col_struct_id + "'";
            return sqlQuery;
        }

        public static string GetTabStructdtl_Tabname(string table_name)
        {
            sqlQuery = string.Empty;



            sqlQuery = "  SELECT cs.tab_col_struct_id,cs.table_name,cs.column_name,cs.master_table_name,cs.master_disp_column_name,";
            sqlQuery += " cs.master_link_column_name,cs.display_name,cs.width,cs.DISPLAY_NAME_OL";
            sqlQuery += " FROM TAB_COL_STRUCT_DTL  cs";
            sqlQuery += " where cs.table_name = '" + table_name + "'";
            return sqlQuery;
        }

        public static string GetTabStructdtl_Screen(string str_screen_code)
        {
            sqlQuery = string.Empty;



            sqlQuery = "  SELECT cs.tab_col_struct_id,cs.table_name,cs.column_name,cs.master_table_name,cs.master_disp_column_name,";
            sqlQuery += " cs.master_link_column_name,cs.display_name,cs.width,cs.DISPLAY_NAME_OL";
            sqlQuery += " , cs.org_id_column_name ";
            sqlQuery += " FROM TAB_COL_STRUCT_DTL  cs";
            sqlQuery += " where cs.screen_code = '" + str_screen_code + "'";
            return sqlQuery;
        }

        public static string getColName4Screen(string str_screencode)
        {
            sqlQuery = string.Empty;
            sqlQuery += "select column_name,tab_col_struct_id from tab_col_struct_dtl where  flag_master=0 and screen_code='" + str_screencode + "'";

            return sqlQuery;
        }
    }
}
