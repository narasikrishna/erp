﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.SSM
{
    public class TownCity_DAL
    {
        static string sqlQuery = "";

        public static string GetTownCityData(int pkId)
        {
            sqlQuery = string.Empty;

            sqlQuery = "     select sc.PK_ID,'N' as DELETED,";
            sqlQuery += "     sc.CHILD_ID,";
            sqlQuery += "     sc.CITY_CODE,";
            sqlQuery += "     sc.STATE_CODE,";
            sqlQuery += "     sc.COUNTRY_CODE,";
            sqlQuery += "     sc.FULL_NAME,sc.FULL_NAME_OL,";
            sqlQuery += "     sc.SHORT_NAME,";
            sqlQuery += "     sc.EFFECTIVE_DATE,";
            sqlQuery += "     sc.END_DATE,";
            sqlQuery += "    (case sc.ENABLED_FLAG WHEN '1' then 'TRUE' ELSE 'FALSE' end) as ENABLED_FLAG,";
            sqlQuery += "    sc.BRANCH_DESIGNATION";
            sqlQuery += "    from ssm_cities sc";
            sqlQuery += "     where sc.workflow_completion_status=1";
            //   sqlQuery += "     and sc.enabled_flag=1";
            sqlQuery += "     and sc.PK_ID=" + pkId;
           
            return sqlQuery;
        }
        public static string GetTownCityAllData(string countryId)
        {
            sqlQuery = string.Empty;

            sqlQuery = "     select sc.PK_ID,'N' as DELETED,";
            sqlQuery += "     sc.CHILD_ID,";
            sqlQuery += "     sc.CITY_CODE,";
            sqlQuery += "     sc.STATE_CODE,";
            sqlQuery += "     sc.COUNTRY_CODE,";
            sqlQuery += "     sc.FULL_NAME,sc.FULL_NAME_OL,";
            sqlQuery += "     sc.SHORT_NAME,";
            sqlQuery += "     sc.EFFECTIVE_DATE,";
            sqlQuery += "     sc.END_DATE,";
            sqlQuery += "    (case sc.ENABLED_FLAG WHEN '1' then 'TRUE' ELSE 'FALSE' end) as ENABLED_FLAG,";
            sqlQuery += "    sc.BRANCH_DESIGNATION";
            sqlQuery += "    from ssm_cities sc";
            sqlQuery += "     where sc.workflow_completion_status=1";
            //   sqlQuery += "     and sc.enabled_flag=1";
            //  sqlQuery += "     and sc.PK_ID=" + pkId;
            sqlQuery += "     and sc.COUNTRY_CODE='" + countryId + "'";
            return sqlQuery;
        }


        public static string getCity()
        {
            sqlQuery = string.Empty;

            sqlQuery = " select c.city_code,c.full_name";
            sqlQuery += " from ssm_cities c";
            sqlQuery += " where c.workflow_completion_status = 1";
            sqlQuery += " and c.enabled_flag = 1";
            sqlQuery += " order by full_name";
            return sqlQuery;
        }

    }
}
