﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;

using System.Data.EntityClient;



using System.Data.Entity.Infrastructure;


namespace FIN.DAL.SSM
{
    public class Alerts_DAL
    {
        static string sqlQuery = "";

        public static string GetAlertNM()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT a.alert_code,a.alert_message";
            sqlQuery += " FROM ssm_alert_masters a";
            sqlQuery += " where a.enabled_flag = 1";
            sqlQuery += " and a.workflow_completion_status = 1";

            return sqlQuery;
        }

         public static void InsertProcessLog(string error, string value, string flag)
        {
            sqlQuery = string.Empty;

            int seq_number = 0;

            seq_number = DBMethod.GetPrimaryKeyValue("ssm_process_log_seq");

            sqlQuery += "    insert into ssm_process_log (PROCESS_ID,PROCESS_CODE,PROCESS_ERROR,PROCESS_DATE,PROCESS_ORG_ID,PROCESS_STATUS,";
            sqlQuery += "   ENABLED_FLAG, CREATED_BY,CREATED_DATE)";
            sqlQuery += "  values (" + seq_number + ",'" + seq_number + "_Process','" + error + "',sysdate,'" + VMVServices.Web.Utils.OrganizationID + "','" + value + "','" + flag + "','" + VMVServices.Web.Utils.UserName + "',sysdate)";

            DBMethod.ExecuteNonQuery(sqlQuery);
        }
       

        public static string GetEmailAlert()
        {
            sqlQuery = string.Empty;
            sqlQuery = "select * from ssm_system_parameters pp where pp.param_code='ALERT-EMAIL'";
            return sqlQuery;
        }
        public static string GetAlertDetails(string alertCode = "")
        {
            sqlQuery = string.Empty;
            sqlQuery = "  select am.USER_CODE,am.ALERT_MESSAGE from ssm_alert_user_levels am";
            sqlQuery += "   where am.alert_code = '" + alertCode + "'";
            sqlQuery += "   and am.created_date in";
            sqlQuery += "   (select max(aa.CREATED_DATE) from ssm_alert_user_levels aa where trunc(aa.CREATED_DATE)=trunc(sysdate))";
            return sqlQuery;
        }
        public static bool GetScheduleAlertForDocuments()
        {
            Boolean countBool = false;
            int reccount = 0;
            sqlQuery = string.Empty;

            sqlQuery = "  select COUNT(1) as counts";
            sqlQuery += "    from ssm_alert_masters ss";
            sqlQuery += "   where ss.attribute1 is not null";
            sqlQuery += "     and ss.attribute2 is not null";
            sqlQuery += "    and ss.alert_code = 'ALRT_CDE-0000000006'";
            sqlQuery += "    and ss.attribute2 =";
            sqlQuery += "    (SELECT trunc((sysdate - max(dd.last_ddl_time)), 0)";
            sqlQuery += "    FROM DBA_OBJECTS dd";
            sqlQuery += "    WHERE dd.OBJECT_TYPE = 'PROCEDURE'";
            sqlQuery += "   and dd.object_name = 'PROC_HR_ALERT_EMP_DOCUMENT')";

            reccount = DBMethod.GetIntValue(sqlQuery);

            if (reccount > 0)
            {
                countBool = true;
            }
            else
            {
                countBool = false;
            }

            return countBool;
        }
        public static bool GetScheduleAlertForEmpProbation()
        {
            Boolean countBool = false;
            int reccount = 0;
            sqlQuery = string.Empty;

            sqlQuery = "  select COUNT(1) as counts";
            sqlQuery += "    from ssm_alert_masters ss";
            sqlQuery += "   where ss.attribute1 is not null";
            sqlQuery += "     and ss.attribute2 is not null";
            sqlQuery += "    and ss.alert_code = 'ALRT_CDE-0000000040";
            sqlQuery += "    and ss.attribute2 =";
            sqlQuery += "    (SELECT trunc((sysdate - max(dd.last_ddl_time)), 0)";
            sqlQuery += "    FROM DBA_OBJECTS dd";
            sqlQuery += "    WHERE dd.OBJECT_TYPE = 'PROCEDURE'";
            sqlQuery += "   and dd.object_name = 'PROC_HR_ALERT_EMP_DOCUMENT')";

            reccount = DBMethod.GetIntValue(sqlQuery);

            if (reccount > 0)
            {
                countBool = true;
            }
            else
            {
                countBool = false;
            }

            return countBool;
        }
        public static void GetSP_FOR_Scheduled_Doc_Expired_Alert()
        {
            try
            {
                
                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "Alerts.SCHEDULED_DOC_EXPIRED_ALERT";
                oraCmd.CommandType = CommandType.StoredProcedure;

                oraCmd.Parameters.Add("@p_org_id", OracleDbType.NVarchar2).Value = VMVServices.Web.Utils.OrganizationID;
              
                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);            

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static void GetSP_FOR_Scheduled_Emp_Probation_Alert()
        {
            try
            {

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "Alerts.SCHEDULED_EMP_PROBATION_ALERT";
                oraCmd.CommandType = CommandType.StoredProcedure;

                oraCmd.Parameters.Add("@p_org_id", OracleDbType.NVarchar2).Value = VMVServices.Web.Utils.OrganizationID;

                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        
    }
}
