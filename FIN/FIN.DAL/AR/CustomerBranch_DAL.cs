﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.AR
{
    public class CustomerBranch_DAL
    {
        static string sqlQuery = "";
        public static string GetCustomerBranch(string cusNo = "")
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT CB.VENDOR_LOC_ADD1 ||' '|| CB.VENDOR_BRANCH_CODE" + VMVServices.Web.Utils.LanguageCode + " AS VENDOR_BRANCH_CODE,CB.VENDOR_LOC_ID";
            sqlQuery += " FROM SUPPLIER_CUSTOMER_BRANCH CB";
            sqlQuery += " WHERE CB.ENABLED_FLAG = 1";
            sqlQuery += " AND CB.WORKFLOW_COMPLETION_STATUS = 1";
            if (cusNo != "")
            {
                sqlQuery += " and cb.vendor_id='" + cusNo + "'";
            }

            return sqlQuery;

        }

        public static string GetCustomerNameFromBranch()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT DISTINCT SC.VENDOR_ID, SC.VENDOR_CODE || ' - ' || SC.VENDOR_NAME AS VEND_NAME, SC.VENDOR_CODE";
            sqlQuery += " FROM SUPPLIER_CUSTOMER_BRANCH SCB, SUPPLIER_CUSTOMERS SC";
            sqlQuery += " WHERE SC.VENDOR_ID=SCB.VENDOR_ID";
            sqlQuery += " AND SCB.ENABLED_FLAG = 1";
            sqlQuery += " AND SCB.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND UPPER(SC.VENDOR_TYPE) IN ('CUSTOMER','BOTH')";

            return sqlQuery;
        }

    }
}
