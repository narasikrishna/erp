﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.AR
{
    public class SalesOrder_DAL
    {
        static string sqlQuery = "";


        public static string getSalesOrderDetails(string str_OrderID)
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT COH.OM_ORDER_ID, COH.OM_ORDER_NUM,cod.OM_ORDER_DTL_ID, COD.OM_ORDER_LINE_NUM, cod.OM_ORDER_DESC,IM.ITEM_NAME, COD.OM_ITEM_ID as ITEM_ID, COD.OM_ORDER_QTY,COD.OM_ORDER_UOM as UOM_NAME, COD.OM_ORDER_UOM AS UOM_CODE, to_char(COD.OM_ORDER_PRICE) as OM_ORDER_PRICE , to_char(nvl(COD.OM_ORDER_QTY,0) * nvl(COD.OM_ORDER_PRICE,0)) as AMOUNT, (case when cod.enabled_flag='1' then 'TRUE' else 'false' end)  AS ENABLED_FLAG, '0' AS DELETED ";
            sqlQuery += " FROM OM_CUSTOMER_ORDER_HDR COH, OM_CUSTOMER_ORDER_DTL COD, INV_ITEM_MASTER IM ";
            sqlQuery += " WHERE COD.OM_ITEM_ID = IM.ITEM_ID ";
            sqlQuery += " AND COH.OM_ORDER_ID = COD.OM_ORDER_ID ";
            sqlQuery += " AND COH.OM_ORDER_ID = '" + str_OrderID + "'";
            //sqlQuery += " AND COH.WORKFLOW_COMPLETION_STATUS = '1'";
            //sqlQuery += " AND COH.ENABLED_FLAG = '1' ";
            sqlQuery += " AND COD.WORKFLOW_COMPLETION_STATUS = '1'";
            //sqlQuery += " AND COD.ENABLED_FLAG = '1' ";
            sqlQuery += " AND IM.WORKFLOW_COMPLETION_STATUS = '1'";
            sqlQuery += " AND IM.ENABLED_FLAG = '1' ";
            sqlQuery += " ORDER BY COD.OM_ORDER_LINE_NUM ";

            return sqlQuery;
        }

        public static string getItemUnitPriceDetails(string str_OrderID)
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT IM.ITEM_UNIT_PRICE ";
            sqlQuery += " FROM INV_ITEM_MASTER IM ";
            sqlQuery += " WHERE IM.ITEM_ID = '" + str_OrderID + "'";
            sqlQuery += " AND IM.WORKFLOW_COMPLETION_STATUS = '1' ";
            sqlQuery += " AND IM.ENABLED_FLAG = '1' ";

            return sqlQuery;
        }

        public static string getOrderNo(string str_OrderID)
        {
            sqlQuery = string.Empty;


            sqlQuery += " select  CO.OM_ORDER_ID,CO.OM_ORDER_NUM";
            sqlQuery += " from OM_CUSTOMER_ORDER_HDR co";
            sqlQuery += " where co.workflow_completion_status=1";
            sqlQuery += " and co.om_vendor_loc_id='" + str_OrderID + "'";
            sqlQuery += " order by co.om_order_num";

            return sqlQuery;
        }


        public static string getCurrency(string quote_hdr_id)
        {
            sqlQuery = string.Empty;


            sqlQuery = " select c.currency_id,c.currency_desc";
            sqlQuery += " from SSM_CURRENCIES c,OM_SALE_QUOTE_HDR sq";
            sqlQuery += " where c.currency_id = sq.quote_currency";
            sqlQuery += " and sq.quote_hdr_id = '" + quote_hdr_id + "'";

            return sqlQuery;
        }

        public static string getAccountGroupName()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT GAG.ACCT_GRP_ID AS GROUP_ID, GAG.ACCT_GRP_DESC" + VMVServices.Web.Utils.LanguageCode + " AS GROUP_NAME ";
            sqlQuery += " FROM GL_ACCT_GROUPS GAG ";
            sqlQuery += " WHERE GAG.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " AND GAG.ENABLED_FLAG = 1 ";
            sqlQuery += " AND (GAG.EFFECTIVE_END_DT IS NULL OR GAG.EFFECTIVE_END_DT > SYSDATE) ";
            sqlQuery += " ORDER BY GROUP_NAME ";

            return sqlQuery;
        }
        public static string GetSaleOrderDetails(string order_id = "")
        {
            sqlQuery = string.Empty;

            sqlQuery = "    select ";
            sqlQuery += "   od.om_order_line_num,";
            sqlQuery += "   od.om_order_dtl_id,";
            sqlQuery += "   oc.om_order_id,";
            sqlQuery += "   im.item_id,";
            sqlQuery += "   im.item_name,";
            sqlQuery += "   od.om_order_qty,";
            sqlQuery += "  (oc.om_order_num || ' - ' || od.om_order_line_num || ' - ' ||";
            sqlQuery += "  im.item_name) as om_order_num";
            sqlQuery += "   from OM_CUSTOMER_ORDER_HDR oc,";
            sqlQuery += "     om_customer_order_dtl od,";
            sqlQuery += "    inv_item_master       im";
            sqlQuery += "   where oc.om_order_id = od.om_order_id";
            sqlQuery += "   and im.item_id = od.om_item_id";
            sqlQuery += "   and oc.workflow_completion_status=1";
            sqlQuery += "   and im.workflow_completion_status=1";
            sqlQuery += "   and oc.enabled_flag=1";
            sqlQuery += "   and im.enabled_flag=1";

            if (order_id != string.Empty)
            {
                sqlQuery += "   and oc.om_order_id ='" + order_id + "'";
            }

            sqlQuery += "    order by oc.om_order_num";
            return sqlQuery;
        }


        public static string GetSaleOrderDts_forDeliverychallan(string order_id, string item_id)
        {
            sqlQuery = string.Empty;

            sqlQuery = "    select ";
            sqlQuery += "   od.om_order_line_num,";
            sqlQuery += "   od.om_order_dtl_id,";
            sqlQuery += "   oc.om_order_id,";
            sqlQuery += "   im.item_id,";
            sqlQuery += "   im.item_name,";
            sqlQuery += "   od.om_order_qty,";
            sqlQuery += "  (oc.om_order_num || ' - ' || od.om_order_line_num || ' - ' ||";
            sqlQuery += "  im.item_name) as om_order_num";
            sqlQuery += "   from OM_CUSTOMER_ORDER_HDR oc,";
            sqlQuery += "     om_customer_order_dtl od,";
            sqlQuery += "    inv_item_master       im";
            sqlQuery += "   where oc.om_order_id = od.om_order_id";
            sqlQuery += "   and im.item_id = od.om_item_id";
            sqlQuery += "   and oc.workflow_completion_status=1";
            sqlQuery += "   and im.workflow_completion_status=1";
            sqlQuery += "   and oc.enabled_flag=1";
            sqlQuery += "   and im.enabled_flag=1";

            if (order_id != string.Empty)
            {
                sqlQuery += "   and oc.om_order_id ='" + order_id + "'";
            }
            if (item_id != string.Empty)
            {
                sqlQuery += "   and im.item_id ='" + item_id + "'";
            }

            sqlQuery += "    order by oc.om_order_num";
            return sqlQuery;
        }


        public static string GetOrderNo()
        {
            sqlQuery = string.Empty;


            sqlQuery = " select oc.om_order_id,oc.om_order_num";
            sqlQuery += " from OM_CUSTOMER_ORDER_HDR oc";
            sqlQuery += " where oc.enabled_flag = 1";
            sqlQuery += " and oc.workflow_completion_status =1";
            sqlQuery += " order by oc.om_order_num";

            return sqlQuery;
        }

        public static string GetReceiptNo()
        {
            sqlQuery = string.Empty;


            sqlQuery = " SELECT CH.CUST_PAY_ID FROM OM_CUST_PAYMENT_HDR CH";

            sqlQuery += " where CH.enabled_flag = 1";
            sqlQuery += " and CH.workflow_completion_status =1";


            return sqlQuery;
        }

        public static string GetQuoteNumber()
        {
            sqlQuery = string.Empty;


            sqlQuery = " SELECT SH.QUOTE_HDR_ID  FROM OM_SALE_QUOTE_HDR SH";

            sqlQuery += " WHERE SH.ENABLED_FLAG = 1";
            sqlQuery += " AND SH.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " order by SH.QUOTE_HDR_ID ASC ";


            return sqlQuery;
        }

        public static string getReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_SALES_ORDER V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {


                if (VMVServices.Web.Utils.ReportViewFilterParameter["FROM_ORDER_ID"] != null)
                {
                    sqlQuery += " AND V.SALES_ORDER_NO >='" + VMVServices.Web.Utils.ReportFilterParameter["FROM_ORDER_ID"].ToString() + "'";
                }

                if (VMVServices.Web.Utils.ReportViewFilterParameter["TO_ORDER_ID"] != null)
                {

                    sqlQuery += " AND V.SALES_ORDER_NO <='" + VMVServices.Web.Utils.ReportFilterParameter["TO_ORDER_ID"].ToString() + "'";
                }



                if (VMVServices.Web.Utils.ReportViewFilterParameter["CUST_FROM"] != null)
                {
                    sqlQuery += " AND V.VENDOR_ID >='" + VMVServices.Web.Utils.ReportFilterParameter["CUST_FROM"].ToString() + "'";
                }

                if (VMVServices.Web.Utils.ReportViewFilterParameter["CUST_TO"] != null)
                {
                    sqlQuery += "AND  V.VENDOR_ID <='" + VMVServices.Web.Utils.ReportFilterParameter["CUST_TO"].ToString() + "'";
                }


                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
                {

                    sqlQuery += " AND V.ORDER_DATE >= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy') ";
                }

                if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
                {
                    sqlQuery += " AND ORDER_DATE <= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["To_Date"].ToString() + "','dd/MM/yyyy') ";
                }

                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Amt"] != null)
                {
                    sqlQuery += " AND V.AMOUNT >=to_number(trunc(" + VMVServices.Web.Utils.ReportViewFilterParameter["From_Amt"] + "))";

                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Amt"] != null)
                {
                    sqlQuery += " AND V.AMOUNT <=to_number(trunc(" + VMVServices.Web.Utils.ReportViewFilterParameter["To_Amt"] + "))";
                }
                sqlQuery += " ORDER BY V.ORDER_DATE";
            }
            return sqlQuery;
        }

        public static string getReceiptReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_RECEIPT V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["CUST_PAY_ID"] != null)
                {
                    sqlQuery += " AND V.receipt_number = '" + VMVServices.Web.Utils.ReportViewFilterParameter["CUST_PAY_ID"].ToString() + "'";
                }

                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
                {
                    sqlQuery += " AND V.PAY_DATE between to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy') ";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
                {
                    sqlQuery += " AND to_date('" + VMVServices.Web.Utils.ReportFilterParameter["To_Date"].ToString() + "','dd/MM/yyyy') ";
                }
            }
            return sqlQuery;
        }

        public static string GetCustomerSOAwaitedChart()
        {
            sqlQuery = string.Empty;

            sqlQuery += " select coh.om_cust_id ,sc.vendor_name,count(1) as counts ";
            sqlQuery += " from om_customer_order_hdr coh,om_customer_order_dtl cod,supplier_customers sc";
            sqlQuery += " where cod.om_item_id not in (select cid.om_item_id from om_cust_invoice_dtl cid where cid.om_item_id is not null)";
            sqlQuery += " and sc.vendor_id=coh.om_cust_id";
            sqlQuery += " and coh.om_order_id=cod.om_order_id";
            sqlQuery += " and (sc.vendor_type='Customer' or sc.vendor_type='Both')";
            sqlQuery += " and coh.om_order_org_id='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " group by coh.om_cust_id,sc.vendor_name";

            return sqlQuery;
        }
    }
}
