﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;

using System.Data.EntityClient;



using System.Data.Entity.Infrastructure;

namespace FIN.DAL.AR
{
   public class CustomerStatementofAccount_DAL
    {

        static string sqlQuery = "";


        public static string getCustomerStatementofAccounts()
        {
            sqlQuery = string.Empty;
            sqlQuery += " SELECT * FROM AR_CUST_STMT_ACCOUNT V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportFilterParameter != null || VMVServices.Web.Utils.ReportFilterParameter.Count != 0)
            {
                //if (VMVServices.Web.Utils.ReportViewFilterParameter["CUST_FROM_ID"] != null)
                //{
                //    sqlQuery += " AND V.vendor_id >= '" + VMVServices.Web.Utils.ReportViewFilterParameter["CUST_FROM_ID"].ToString() + "'";
                //}
                //if (VMVServices.Web.Utils.ReportViewFilterParameter["CUST_TO_ID"] != null)
                //{
                //    sqlQuery += " AND V.vendor_id <= '" + VMVServices.Web.Utils.ReportViewFilterParameter["CUST_TO_ID"].ToString() + "'";
                //}
                if (VMVServices.Web.Utils.ReportFilterParameter["FROM_AMT"] != null)
                {
                    sqlQuery += " AND V.inv_balance >= '" + VMVServices.Web.Utils.ReportFilterParameter["FROM_AMT"].ToString() + "'";
                } if (VMVServices.Web.Utils.ReportFilterParameter["TO_AMT"] != null)
                {
                    sqlQuery += " AND V.inv_balance <= '" + VMVServices.Web.Utils.ReportFilterParameter["TO_AMT"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["WITHZERO"] == "FALSE")
                {
                    sqlQuery += " AND (V.inv_balance >0)  ";
                }
            }

            return sqlQuery;
        }

        public static string GetSP_CustomerStatementOfAccount(string vendor_id, String tb_fromdate, String tb_todate)
        {
            try
            {
                string strValue = string.Empty;
                string strNextCode = string.Empty;
                string strErrMsg = string.Empty;
                string retcode = string.Empty;

                DateTime StartDate = new DateTime();
                if (tb_fromdate != null && tb_fromdate.Length > 0)
                    StartDate = Convert.ToDateTime(tb_fromdate);
                DateTime EndDate = new DateTime();
                if (tb_todate != null && tb_todate.Length > 0)
                    EndDate = Convert.ToDateTime(tb_todate);

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "PROC_CUST_STMT_ACCNT";
                oraCmd.CommandType = CommandType.StoredProcedure;
                //oraCmd.Parameters.Add(new OracleParameter("@p_org_id", OracleDbType.Varchar2, 250)).Value = VMVServices.Web.Utils.OrganizationID;
                oraCmd.Parameters.Add(new OracleParameter("@P_VENDOR_ID", OracleDbType.Varchar2, 250)).Value = vendor_id;
                oraCmd.Parameters.Add(new OracleParameter("@P_FROM_DATE", OracleDbType.Varchar2, 250)).Value = StartDate.ToString("dd/MMM/yyyy");
                if (tb_todate != null && tb_todate.Length > 0)
                {
                    oraCmd.Parameters.Add(new OracleParameter("@P_END_DATE", OracleDbType.Varchar2, 250)).Value = EndDate.ToString("dd/MMM/yyyy");
                }

                oraCmd.Parameters.Add("@P_RET", OracleDbType.Varchar2, 150);
                oraCmd.Parameters["@P_RET"].Direction = ParameterDirection.Output;
                //oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);



                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);
                retcode = oraCmd.Parameters["@P_RET"].Value.ToString();

                return retcode;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GetSP_CustomerStatementOfAccount_New(string From_vendor_id, string To_vendor_id, String tb_fromdate, String tb_todate)
        {
            try
            {
                string strValue = string.Empty;
                string strNextCode = string.Empty;
                string strErrMsg = string.Empty;
                string retcode = string.Empty;

                DateTime StartDate = new DateTime();
                if (tb_fromdate != null && tb_fromdate.Length > 0)
                    StartDate = Convert.ToDateTime(tb_fromdate);
                DateTime EndDate = new DateTime();
                if (tb_todate != null && tb_todate.Length > 0)
                    EndDate = Convert.ToDateTime(tb_todate);

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "PROC_HDR_CUST_STMT";
                oraCmd.CommandType = CommandType.StoredProcedure;
                //oraCmd.Parameters.Add(new OracleParameter("@p_org_id", OracleDbType.Varchar2, 250)).Value = VMVServices.Web.Utils.OrganizationID;
                oraCmd.Parameters.Add(new OracleParameter("@p_from_customer", OracleDbType.Varchar2, 250)).Value = From_vendor_id;
                oraCmd.Parameters.Add(new OracleParameter("@p_to_customer", OracleDbType.Varchar2, 250)).Value = To_vendor_id;
                oraCmd.Parameters.Add(new OracleParameter("@P_FROM_DATE", OracleDbType.Varchar2, 250)).Value = StartDate.ToString("dd/MMM/yyyy");
                if (tb_todate != null && tb_todate.Length > 0)
                {
                    oraCmd.Parameters.Add(new OracleParameter("@P_END_DATE", OracleDbType.Varchar2, 250)).Value = EndDate.ToString("dd/MMM/yyyy");
                }

                oraCmd.Parameters.Add("@P_RET", OracleDbType.Varchar2, 150);
                oraCmd.Parameters["@P_RET"].Direction = ParameterDirection.Output;
                //oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);



                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);
                retcode = oraCmd.Parameters["@P_RET"].Value.ToString();

                return retcode;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
