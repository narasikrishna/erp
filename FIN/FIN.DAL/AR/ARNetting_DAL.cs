﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.AR
{
    public class ARNetting_DAL
    {
        static string sqlQuery = "";
        public static string get_BAL_APInvoiceDet(string supplierId)
        {
            sqlQuery = string.Empty;

            sqlQuery += " SELECT '0' as NETTING_DTL_ID,'0' as NETTING_ID, IH.INV_ID, IH.VENDOR_ID,IH.INV_NUM, SC.VENDOR_CODE,SC.VENDOR_NAME,IH.INV_AMT AS AP_INV_AMT,NVL(SUM(PD.PAY_INVOICE_PAID_AMT),0) AS AP_PAYMENT_AMT ";
            sqlQuery += " ,TO_CHAR(IH.INV_AMT - NVL(SUM(PD.PAY_INVOICE_PAID_AMT),0)) as AP_BAL_INV_AMT ";
            sqlQuery += " ,'N' as DELETED ";
            sqlQuery += " FROM INV_INVOICES_HDR IH  ";
            sqlQuery += " INNER JOIN SUPPLIER_CUSTOMERS SC ON IH.VENDOR_ID= SC.VENDOR_ID ";
            sqlQuery += " LEFT JOIN INV_PAYMENTS_DTL PD ON PD.PAY_INVOICE_ID=IH.INV_ID ";
            sqlQuery += " WHERE SC.VENDOR_ID='" + supplierId + "'";
            sqlQuery += " and ih.posted_flag=1 and ih.rev_flag is null or ih.rev_flag=0";
            sqlQuery += " GROUP BY IH.INV_ID,IH.VENDOR_ID,IH.INV_NUM, SC.VENDOR_CODE,SC.VENDOR_NAME,IH.INV_AMT ";
            return sqlQuery;

        }


        public static string get_BAL_ARInvoiceDet(string supplierId)
        {
            sqlQuery = string.Empty;

            sqlQuery += "  SELECT '0' as NETTING_DTL_ID,'0' AS NETTING_ID, CIH.OM_INV_ID,CIH.OM_VENDOR_ID,CIH.OM_INV_NUM, SC.VENDOR_CODE,SC.VENDOR_NAME ";
            sqlQuery += " ,CIH.OM_INV_AMT AS AR_INV_AMT,NVL(SUM(CPD.CUST_INV_PAID_AMT),0) AS AR_PAYMENT_AMT ";
            sqlQuery += " ,TO_CHAR(CIH.OM_INV_AMT - NVL(SUM(CPD.CUST_INV_PAID_AMT),0)) AS AR_BAL_INV_AMT ";
            sqlQuery += " ,'N' as DELETED ";
            sqlQuery += " FROM OM_CUST_INVOICE_HDR CIH ";
            sqlQuery += " INNER JOIN SUPPLIER_CUSTOMERS SC ON CIH.OM_VENDOR_ID= SC.VENDOR_ID ";
            sqlQuery += " LEFT JOIN OM_CUST_PAYMENT_DTL CPD ON CPD.CUST_INV_ID=CIH.OM_INV_ID ";
            sqlQuery += " WHERE SC.VENDOR_ID='" + supplierId + "'";
            sqlQuery += " and cih.posted_flag=1 and cih.rev_flag is null or cih.rev_flag=0";
            sqlQuery += " GROUP BY CIH.OM_INV_ID,CIH.OM_VENDOR_ID,CIH.OM_INV_NUM, SC.VENDOR_CODE,SC.VENDOR_NAME,CIH.OM_INV_AMT ";
            return sqlQuery;

        }

        public static string get_NettingAP(string str_sup_id)
        {
            sqlQuery = string.Empty;
            sqlQuery += " SELECT ND.NETTING_DTL_ID,NH.NETTING_ID,IH.INV_ID,IH.INV_NUM,TO_CHAR(ND.INV_AMT) AS AP_BAL_INV_AMT ";
            sqlQuery += " ,'N' AS DELETED ";
            sqlQuery += "  FROM AR_NETTING_HDR NH ";
            sqlQuery += " INNER JOIN AR_NETTING_DTL ND ON NH.NETTING_ID= ND.NETTING_ID ";
            sqlQuery += " INNER JOIN INV_INVOICES_HDR IH ON IH.INV_ID = ND.INV_ID ";
            sqlQuery += " WHERE ND.INV_TYPE='AP' ";
            sqlQuery += " AND NH.VENDOR_ID='" + str_sup_id + "'";
            return sqlQuery;
        }

        public static string get_NettingAR(string str_sup_id)
        {
            sqlQuery = string.Empty;
            sqlQuery += " SELECT ND.NETTING_DTL_ID,NH.NETTING_ID,IH.OM_INV_ID,IH.OM_INV_NUM,TO_CHAR(ND.INV_AMT) AS AR_BAL_INV_AMT ";
            sqlQuery += " ,'N' AS DELETED ";
            sqlQuery += "  FROM AR_NETTING_HDR NH ";
            sqlQuery += " INNER JOIN AR_NETTING_DTL ND ON NH.NETTING_ID= ND.NETTING_ID ";
            sqlQuery += " INNER JOIN OM_CUST_INVOICE_HDR IH ON IH.OM_INV_ID = ND.INV_ID ";
            sqlQuery += " WHERE ND.INV_TYPE='AR' ";
            sqlQuery += " AND NH.VENDOR_ID='" + str_sup_id + "'";
            return sqlQuery;
        }


        public static string getAPARNettingDtls(string supplierID)
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT * FROM VW_AP_AR_NETTING V WHERE ROWNUM > 0 ";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.VENDOR_ID] != null)
                {
                    sqlQuery += " AND V.VENDOR_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.VENDOR_ID].ToString() + "'";
                }
            }
            return sqlQuery;
        }

    }
}
