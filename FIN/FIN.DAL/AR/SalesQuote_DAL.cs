﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.AR
{
    public class SalesQuote_DAL
    {
        static string sqlQuery = "";

        public static string getSalesQuota()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT QH.QUOTE_HDR_ID, (QH.QUOTE_HDR_ID || '  - '|| QH.QUOTE_NAME) as QUOTE_NAME";
            sqlQuery += " FROM OM_SALE_QUOTE_HDR QH ";
            sqlQuery += " WHERE QH.WORKFLOW_COMPLETION_STATUS = '1' ";
            sqlQuery += " AND QH.ENABLED_FLAG = '1' ";
            sqlQuery += " ORDER BY QH.QUOTE_HDR_ID ";

            return sqlQuery;
        }


        public static string getPayterm()
        {
            sqlQuery = string.Empty;
            sqlQuery = " select p.term_id,p.term_name" + VMVServices.Web.Utils.LanguageCode + " as term_name ";
            sqlQuery += " from inv_pay_terms_hdr p";
            sqlQuery += " where p.workflow_completion_status = 1";
            sqlQuery += " and p.enabled_flag = 1";
            if (VMVServices.Web.Utils.LanguageCode != string.Empty)
            {
                sqlQuery += " AND  p.term_name" + VMVServices.Web.Utils.LanguageCode + "  is not null ";
            }
            sqlQuery += " ORDER BY p.term_name ";

            return sqlQuery;
        }

        public static string GetSalesQuoteDetails(string QUOTE_HDR_ID)
        {
            sqlQuery = string.Empty;


            sqlQuery = " SELECT SQ.QUOTE_DTL_ID,to_char(SQ.QUOTE_ITEM_UNIT_PRICE) as QUOTE_ITEM_UNIT_PRICE,to_char(SQ.QUOTE_ITEM_QTY) as QUOTE_ITEM_QTY,SQ.QUOTE_LINE_NUM,to_char(SQ.QUOTE_LINE_AMT) as QUOTE_LINE_AMT,IM.ITEM_ID,IM.ITEM_NAME,sq.QUOTE_ITEM_UOM,'0' AS DELETED";
            sqlQuery += " FROM OM_SALE_QUOTE_DTL SQ,INV_ITEM_MASTER IM";
            sqlQuery += " WHERE SQ.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND SQ.QUOTE_ITEM_ID = IM.ITEM_ID";
           // sqlQuery += " AND SQ.QUOTE_ITEM_UOM = U.UOM_ID";
            sqlQuery += " AND SQ.QUOTE_HDR_ID = '" + QUOTE_HDR_ID + "'";
            sqlQuery += " order by QUOTE_DTL_ID ";

            return sqlQuery;
        }
        public static string GetSalesQuoteBasedQN(string QUOTE_HDR_ID)
        {
            sqlQuery = string.Empty;


            sqlQuery += "    SELECT  COD.QUOTE_LINE_NUM as OM_ORDER_LINE_NUM,";
            sqlQuery += "   IM.ITEM_NAME,'' as OM_ORDER_DTL_ID,";
            sqlQuery += "   '' as OM_ORDER_ID,";
            sqlQuery += "   IM.ITEM_NAME as OM_ORDER_DESC,";
            sqlQuery += "   COD.Quote_ITEM_ID as ITEM_ID,";
            sqlQuery += "   COD.QUOTE_ITEM_QTY as OM_ORDER_QTY,";
            sqlQuery += "   COD.QUOTE_ITEM_UOM as UOM_CODE,";
           // sqlQuery += "   (select um.UOM_DESC from INV_UOM_MASTER um where um.uom_id=cod.quote_item_uom) uom_name,";
            sqlQuery += " cod.QUOTE_ITEM_UOM as uom_name,";
            sqlQuery += "  to_char(COD.QUOTE_ITEM_UNIT_PRICE) as OM_ORDER_PRICE,";
            sqlQuery += "  to_char(nvl(COD.Quote_Item_Qty, 0) * nvl(COD.QUOTE_ITEM_UNIT_PRICE, 0)) AS AMOUNT,";
            sqlQuery += "   (case when cod.enabled_flag='1' then 'TRUE' else 'false' end)  AS ENABLED_FLAG,";
            sqlQuery += "   '0' AS DELETED";
            sqlQuery += "   FROM OM_SALE_QUOTE_HDR COH, Om_Sale_Quote_Dtl COD, INV_ITEM_MASTER IM";
            sqlQuery += "   WHERE COD.QUOTE_ITEM_ID = IM.ITEM_ID";
            sqlQuery += "   AND COH.QUOTE_HDR_ID = COD.QUOTE_HDR_ID";
            sqlQuery += "   AND COH.QUOTE_HDR_ID = '" + QUOTE_HDR_ID + "'";
            sqlQuery += "   AND COH.WORKFLOW_COMPLETION_STATUS = '1'";
            sqlQuery += "   AND COH.ENABLED_FLAG = '1'";
            sqlQuery += "   AND COD.WORKFLOW_COMPLETION_STATUS = '1'";
            sqlQuery += "   AND COD.ENABLED_FLAG = '1'";
            sqlQuery += "   AND IM.WORKFLOW_COMPLETION_STATUS = '1'";
            sqlQuery += "  AND IM.ENABLED_FLAG = '1'";
            sqlQuery += "  ORDER BY COD.QUOTE_LINE_NUM ";

            return sqlQuery;
        }

        public static string getReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_SALES_QUOTE V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["QUOTE_HDR_ID"] != null)
                {
                    sqlQuery += " AND V.QUOTE_NUMBER = '" + VMVServices.Web.Utils.ReportViewFilterParameter["QUOTE_HDR_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["VENDOR_ID"] != null)
                {
                    sqlQuery += " AND V.VENDOR_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["VENDOR_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["ITEM_ID"] != null)
                {
                    sqlQuery += " AND V.ITEM_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["ITEM_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["FROM_QUOTE_VALUE"] != null)
                {
                    sqlQuery += " AND V.AMOUNT >= '" + VMVServices.Web.Utils.ReportViewFilterParameter["FROM_QUOTE_VALUE"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["TO_QUOTE_VALUE"] != null)
                {
                    sqlQuery += " AND V.AMOUNT = '" + VMVServices.Web.Utils.ReportViewFilterParameter["TO_QUOTE_VALUE"].ToString() + "'";
                }
               
            }
            return sqlQuery;
        }
    }
}
