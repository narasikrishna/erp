﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.AR
{
    public class DayBookPayment_DAL
    {
        static string sqlQuery = "";
        public static string getReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM AP_DAY_BOOK_PAYMENT V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["SEGMENT_VALUE_ID"] != null)
                {
                    sqlQuery += " AND V.GLOBAL_SEGMENT = '" + VMVServices.Web.Utils.ReportViewFilterParameter["SEGMENT_VALUE_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
                {
                    sqlQuery += " AND V.PAY_DATE >= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy') ";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["FromPaymentID"] != null)
                {
                    sqlQuery += " AND V.PAYMENT_REF >= '" + VMVServices.Web.Utils.ReportViewFilterParameter["FromPaymentID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["ToPaymentID"] != null)
                {
                    sqlQuery += " AND V.PAYMENT_REF <= '" + VMVServices.Web.Utils.ReportViewFilterParameter["ToPaymentID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["CURRENCY_CODE"] != null)
                {
                    sqlQuery += " AND V.CURRENCY_CODE = '" + VMVServices.Web.Utils.ReportViewFilterParameter["CURRENCY_CODE"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["FromSupplierID"] != null)
                {
                    sqlQuery += " AND V.VENDOR_ID >= '" + VMVServices.Web.Utils.ReportViewFilterParameter["FromSupplierID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["ToSupplierID"] != null)
                {
                    sqlQuery += " AND V.VENDOR_ID <= '" + VMVServices.Web.Utils.ReportViewFilterParameter["ToSupplierID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["FromAmount"] != null)
                {
                    sqlQuery += " AND V.AMOUNT_PAID >=  '" + VMVServices.Web.Utils.ReportFilterParameter["FromAmount"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["ToAmount"] != null)
                {
                    sqlQuery += " AND V.AMOUNT_PAID <=  '" + VMVServices.Web.Utils.ReportFilterParameter["ToAmount"].ToString() + "'";
                }


            }
            return sqlQuery;
        }
        public static string getGlobalSegment()
        {
            sqlQuery = string.Empty;
            sqlQuery += " SELECT SV.SEGMENT_ID,SV.SEGMENT_VALUE" + VMVServices.Web.Utils.LanguageCode;
            sqlQuery += " FROM GL_SEGMENT_VALUES SV ";
            sqlQuery += " WHERE SV.WORKFLOW_COMPLETION_STATUS='1' ";
            sqlQuery += " AND SV.ENABLED_FLAG='1'";


            return sqlQuery;
        }
    }
}
