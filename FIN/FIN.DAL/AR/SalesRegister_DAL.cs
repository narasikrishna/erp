﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.AR
{
    public class SalesRegister_DAL
    {
        static string sqlQuery = "";
        public static string getReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM AR_SALES_REGISTER_R V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["SEGMENT_ID"] != null)
                {
                    sqlQuery += " AND V.GLOBAL_SEGMENT = '" + VMVServices.Web.Utils.ReportViewFilterParameter["SEGMENT_ID"].ToString() + "'";
                }

                if (VMVServices.Web.Utils.ReportViewFilterParameter["CUST_FROM"] != null)
                {
                    sqlQuery += " AND V.CUSTOMER_ID >='" + VMVServices.Web.Utils.ReportFilterParameter["CUST_FROM"].ToString() + "'";
                }

                if (VMVServices.Web.Utils.ReportViewFilterParameter["CUST_TO"] != null)
                {
                    sqlQuery += " AND V.CUSTOMER_ID <='" + VMVServices.Web.Utils.ReportFilterParameter["CUST_TO"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
                {
                    sqlQuery += " AND V.INVOICE_DATE >= to_date('" + VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy')";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
                {
                    sqlQuery += " AND V.INVOICE_DATE <= to_date('" + VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"].ToString() + "','dd/MM/yyyy')";
                }


                if (VMVServices.Web.Utils.ReportViewFilterParameter["INV_FROM"] != null)
                {
                    sqlQuery += " AND V.INVOICE_NUMBER >='" + VMVServices.Web.Utils.ReportFilterParameter["INV_FROM"].ToString() + "'";
                }


                if (VMVServices.Web.Utils.ReportViewFilterParameter["INV_TO"] != null)
                {
                    sqlQuery += " AND V.INVOICE_NUMBER <='" + VMVServices.Web.Utils.ReportFilterParameter["INV_TO"].ToString() + "'";
                }



                if (VMVServices.Web.Utils.ReportViewFilterParameter["FROM_TRN_AMOUNT"] != null)
                {
                    sqlQuery += " AND V.Transaction_Amount >=" + VMVServices.Web.Utils.ReportViewFilterParameter["FROM_TRN_AMOUNT"];

                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["TO_TRN_AMOUNT"] != null)
                {
                    sqlQuery += " AND V.Transaction_Amount <=" + VMVServices.Web.Utils.ReportViewFilterParameter["TO_TRN_AMOUNT"];
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["FROM_FUN_AMOUNT"] != null)
                {
                    sqlQuery += " AND V.func_currency_amount >=" + VMVServices.Web.Utils.ReportViewFilterParameter["FROM_FUN_AMOUNT"];

                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["TO_FUN_AMOUNT"] != null)
                {
                    sqlQuery += " AND V.func_currency_amount <=" + VMVServices.Web.Utils.ReportViewFilterParameter["TO_FUN_AMOUNT"];
                }

                if (VMVServices.Web.Utils.ReportViewFilterParameter["TRAN_TYPE"] != null)
                {
                    sqlQuery += " AND V.INVOICE_TYPE = '" + VMVServices.Web.Utils.ReportViewFilterParameter["TRAN_TYPE"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["TRANC_CURRENCY"] != null)
                {
                    sqlQuery += " AND V.INVOICE_CURRENCY = '" + VMVServices.Web.Utils.ReportViewFilterParameter["TRANC_CURRENCY"].ToString() + "'";
                }
                sqlQuery += " ORDER BY V.INVOICE_DATE";

            }
            return sqlQuery;
        }
        public static string getGlobalSegment()
        {
            sqlQuery = string.Empty;
            sqlQuery += " SELECT SV.SEGMENT_ID,SV.SEGMENT_VALUE" + VMVServices.Web.Utils.LanguageCode;
            sqlQuery += " FROM GL_SEGMENT_VALUES SV ";
            sqlQuery += " WHERE SV.WORKFLOW_COMPLETION_STATUS='1' ";
            sqlQuery += " AND SV.ENABLED_FLAG='1'";


            return sqlQuery;
        }

        public static string getAccountReceivableData()
        {
            sqlQuery = string.Empty;

            sqlQuery = " select v.CUSTNUMBER as vendornumber, v.name_en,  sc.description,  t.AccountReceivableId, t.InvoiceDate as invoicedate, t.DueDate as DueDate, to_char(t.amount) as amount";
            sqlQuery += " ,(case t.MIGRATED when '1' then 'TRUE' ELSE 'FALSE' END) AS MIGRATED";
            sqlQuery += " ,t.AccountReceivableId as InvoiceNumber";
            sqlQuery += " ,(select ie.error_message from gl_intf_errors ie where ie.error_in=t.accountreceivableid and rownum=1) as error_message ";
            sqlQuery += " from AccountReceivable t, dm_intf_cust_mapping v, ssm_code_masters sc";
            sqlQuery += " where v.personid = to_char(t.CustomerId)";
            sqlQuery += " and sc.code = to_char(t.type)";
            sqlQuery += " and sc.parent_code='SO_INV_TY' ";
            sqlQuery += " and (t.migrated is null or t.migrated = '0')";
            sqlQuery += " order by t.AccountReceivableId asc";

            return sqlQuery;
        }

        public static string getCollectionHdrDtlData()
        {
            sqlQuery = string.Empty;

            sqlQuery = " select v.CUSTNUMBER as vendornumber, v.name_en,ch.CollectionNumber as ReceiptNumber, ch.CustomerInvoiceId as InvoiceID, to_char(ch.Amount) as amount";
            sqlQuery += " ,cd.CollectionDetailId, to_char(cd.Amount) as dtlAmount, to_char(cd.Received) as receivedAmt, cd.AccountReceivableId as InvoiceNumber";
            //sqlQuery += ", cd.AccountReceivableNumber as receiptNo";
            sqlQuery += " ,ch.CollectionId";
            sqlQuery += " ,(case ch.MIGRATED when '1' then 'TRUE' ELSE 'FALSE' END) AS MIGRATED";
            sqlQuery += " ,(select ie.error_message from gl_intf_errors ie where ie.error_in = ch.collectionid and rownum=1) as error_message";
            sqlQuery += " from CollectionHdr ch, CollectionDetail cd, dm_intf_cust_mapping v";
            sqlQuery += ", AccountReceivable ar ";
            sqlQuery += " where v.personid = to_char(ch.CustomerId)";
            sqlQuery += " and ch.CollectionId = cd.CollectionId";
            sqlQuery += " and cd.AccountReceivableId = ar.AccountReceivableId";
            sqlQuery += " and ch.status=4";
            sqlQuery += " and (ch.migrated is null or ch.migrated = '0')";
            sqlQuery += " order by cd.AccountReceivableId asc";

            return sqlQuery;
        }
    }
}
