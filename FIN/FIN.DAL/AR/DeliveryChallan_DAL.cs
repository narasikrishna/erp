﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.AR
{
    public class DeliveryChallan_DAL
    {
        static string sqlQuery = "";

        public static string GetDeliveryChallanDetails(string OM_DC_ID)
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT DD.OM_DC_DTL_ID,DD.OM_DC_LINE_NUM,CO.OM_ORDER_ID,CO.OM_ORDER_NUM,DD.OM_ORDER_LINE_NUM,IM.ITEM_ID,IM.ITEM_NAME,DD.OM_ITEM_QTY,";
            sqlQuery += " DD.OM_SHIPPED_QTY,DD.OM_RETURN_QTY,DD.OM_REMARKS,'N' AS DELETED";
            sqlQuery += " FROM OM_DC_DTL DD,OM_CUSTOMER_ORDER_HDR CO,INV_ITEM_MASTER IM";
            sqlQuery += " WHERE DD.OM_ORDER_ID = CO.OM_ORDER_ID";
            sqlQuery += " AND DD.OM_ITEM_ID = IM.ITEM_ID";
            sqlQuery += " AND DD.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND DD.OM_DC_ID = '" + OM_DC_ID + "'";
            sqlQuery += " order by dd.OM_DC_LINE_NUM ";
           

            return sqlQuery;
        }



        public static string GetItemQty(string ITEM_ID)
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT IM.ITEM_REORDER_QTY";
            sqlQuery += " FROM INV_ITEM_MASTER IM";
            sqlQuery += " WHERE IM.ITEM_ID = '" + ITEM_ID + "'";
            sqlQuery += " AND IM.WORKFLOW_COMPLETION_STATUS = 1";


            return sqlQuery;
        }


        public static string GetDCLineNo(string OM_DC_ID)
        {
            sqlQuery = string.Empty;


            sqlQuery = " SELECT DD.OM_DC_LINE_NUM,DD.OM_DC_LINE_NUM";
            sqlQuery += " FROM OM_DC_DTL DD,inv_item_master im";
            sqlQuery += " WHERE DD.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " and im.item_id=dd.om_item_id";
            sqlQuery += " AND DD.ENABLED_FLAG = 1";
            sqlQuery += " AND DD.OM_DC_ID = '" + OM_DC_ID + "'";

            return sqlQuery;
        }

        public static string GetDCLineNo_fr_PR(string OM_DC_ID)
        {
            sqlQuery = string.Empty;


            sqlQuery = " SELECT DD.OM_DC_LINE_NUM,DD.OM_DC_LINE_NUM||' - '||im.item_code||' - '||im.item_name as OM_DC_LINE_NUM_DESC";
            sqlQuery += " FROM OM_DC_DTL DD,inv_item_master im";
            sqlQuery += " WHERE DD.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " and im.item_id=dd.om_item_id";
            sqlQuery += " AND DD.ENABLED_FLAG = 1";
            sqlQuery += " AND DD.OM_DC_ID = '" + OM_DC_ID + "'";

            return sqlQuery;
        }


        public static string getReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_AR_DAILY_COLLECTION V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.VENDOR_ID] != null)
                {
                    sqlQuery += " AND V.Vendor_Id = '" + VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.VENDOR_ID].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
                {
                    sqlQuery += " AND V.SALES_ORDER_DATE between to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy') ";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
                {
                    sqlQuery += " AND to_date('" + VMVServices.Web.Utils.ReportFilterParameter["To_Date"].ToString() + "','dd/MM/yyyy') ";
                }
            }
            return sqlQuery;
        }
        public static string getDeliveryChellanReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM vw_delivery_chellan V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["OM_ORDER_ID"] != null)
                {
                    sqlQuery += " AND V.OM_ORDER_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["OM_ORDER_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
                {
                    sqlQuery += " AND V.dc_date between to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy') ";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
                {
                    sqlQuery += " AND to_date('" + VMVServices.Web.Utils.ReportFilterParameter["To_Date"].ToString() + "','dd/MM/yyyy') ";
                }
            }
            return sqlQuery;
        }
        public static string getItemwiseDailyCollectionReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_AR_DAILY_COLLECTION V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["ITEM_ID"] != null)
                {
                    sqlQuery += " AND V.ITEM_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["ITEM_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
                {
                    sqlQuery += " AND V.SALES_ORDER_DATE between to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy') ";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
                {
                    sqlQuery += " AND to_date('" + VMVServices.Web.Utils.ReportFilterParameter["To_Date"].ToString() + "','dd/MM/yyyy') ";
                }
            }
            return sqlQuery;
        }

    }
}
