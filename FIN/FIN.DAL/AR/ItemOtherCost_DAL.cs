﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.AR
{
    public class ItemOtherCost_DAL
    {

        static string sqlQuery = "";

        public static string getItemothercostDetails(string OM_OTHER_COST_ID)
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT OC.OM_OTHER_COST_DTL_ID,to_char(OC.OM_OTHER_COST_AMT) as OM_OTHER_COST_AMT,C.CODE as LOOKUP_ID,C.CODE as LOOKUP_NAME,'0' AS DELETED";
            sqlQuery += " FROM OM_DC_OTHER_COST_DTL OC,SSM_CODE_MASTERS C";
            sqlQuery += " WHERE OC.OM_OTHER_COST_TYPE = C.CODE";
            sqlQuery += " AND OC.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND OC.ENABLED_FLAG = 1";
            sqlQuery += " AND OC.OM_OTHER_COST_ID = '" + OM_OTHER_COST_ID + "'";
            sqlQuery += " order by OM_OTHER_COST_DTL_ID ";

            return sqlQuery;
        }





    }

}
