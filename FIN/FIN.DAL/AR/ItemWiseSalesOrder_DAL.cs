﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.AR
{
  public  class ItemWiseSalesOrder_DAL
    {
         static string sqlQuery = "";
         public static string getReportData()
         {
             sqlQuery = string.Empty;
             sqlQuery = " SELECT * FROM VW_SALES_ORDER V WHERE ROWNUM > 0 ";
             if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
             {
                 if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
                 {
                     if (VMVServices.Web.Utils.ReportViewFilterParameter["item_id"] != null)
                     {
                         sqlQuery += " AND V.OM_ITEM_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["item_id"].ToString() + "'";
                     }
                     

                 }
            }
             return sqlQuery;
         }
         public static string getItemName()
         {
             sqlQuery = string.Empty;
             sqlQuery += " SELECT IV.ITEM_ID AS ITEM_ID,SV.ITEM_NUMBER||SV.ITEM_NAME  AS ITEM_NAME" + VMVServices.Web.Utils.LanguageCode;
             sqlQuery += " FROM INV_ITEM_MASTER IV ";
             sqlQuery += " WHERE SV.WORKFLOW_COMPLETION_STATUS='1' ";
             sqlQuery += " AND SV.ENABLED_FLAG='1'";


             return sqlQuery;
         }
    }
}
