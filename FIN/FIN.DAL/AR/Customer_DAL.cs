﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.AR
{
    public class Customer_DAL
    {
        static string sqlQuery = "";
        public static string GetCustomerName(string cusNo = "")
        {
            sqlQuery = string.Empty;

            sqlQuery = "  select t.vendor_id,T.VENDOR_CODE ||' - '||t.vendor_name as vendor_name,T.VENDOR_CODE from supplier_customers t";
            sqlQuery += " WHERE t.WORKFLOW_COMPLETION_STATUS = 1  and UPPER(t.vendor_type) in('CUSTOMER','BOTH')";
            if (cusNo != string.Empty)
            {
                sqlQuery += " AND t.vendor_id ='" + cusNo + "'";
            }
            sqlQuery += " AND t.ENABLED_FLAG = 1 ";
            sqlQuery += " ORDER BY t.vendor_id asc ";
            return sqlQuery;

        }

        public static string getVendorReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery = "SELECT * FROM vm_ap_supplier V WHERE ROWNUM > 0 ";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Customer_Id"] != null)
                {
                    sqlQuery += " AND V.VENDOR_ID >= '" + VMVServices.Web.Utils.ReportViewFilterParameter["From_Customer_Id"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Customer_Id"] != null)
                {
                    sqlQuery += " AND V.VENDOR_ID <= '" + VMVServices.Web.Utils.ReportViewFilterParameter["To_Customer_Id"].ToString() + "'";
                }
                
                if (VMVServices.Web.Utils.ReportViewFilterParameter["CREATED_BY"] != null)
                {
                    sqlQuery += " AND UPPER(V.CREATED_BY) = UPPER('" + VMVServices.Web.Utils.ReportViewFilterParameter["CREATED_BY"].ToString() + "')";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["MODIFIED_BY"] != null)
                {
                    sqlQuery += " AND UPPER(V.MODIFIED_BY) = UPPER('" + VMVServices.Web.Utils.ReportViewFilterParameter["MODIFIED_BY"].ToString() + "')";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["CREATED_From_Date"] != null)
                {
                    sqlQuery += " AND V.CREATED_DATE >= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["CREATED_From_Date"].ToString() + "','dd/MM/yyyy') ";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["CREATED_To_Date"] != null)
                {
                    sqlQuery += " AND V.CREATED_DATE <= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["CREATED_To_Date"].ToString() + "','dd/MM/yyyy') ";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["MODIFIED_From_Date"] != null)
                {
                    sqlQuery += " AND V.MODIFIED_DATE >= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["MODIFIED_From_Date"].ToString() + "','dd/MM/yyyy') ";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["MODIFIED_To_Date"] != null)
                {
                    sqlQuery += " AND V.MODIFIED_DATE <= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["MODIFIED_To_Date"].ToString() + "','dd/MM/yyyy') ";
                }

                if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.COUNTRY_CODE] != null)
                {
                    sqlQuery += " AND V.COUNTRY_CODE = '" + VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.COUNTRY_CODE].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.STATE_CODE] != null)
                {
                    sqlQuery += " AND V.STATE_CODE = '" + VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.STATE_CODE].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.CITY_CODE] != null)
                {
                    sqlQuery += " AND V.CITY_CODE = '" + VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.CITY_CODE].ToString() + "'";
                }
                sqlQuery += " AND UPPER(V.vendor_type) in('CUSTOMER','BOTH')";

            }

            return sqlQuery;
        }

    }
}
