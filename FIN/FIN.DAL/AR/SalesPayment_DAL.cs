﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.AR
{
    public class SalesPayment_DAL
    {
        static string sqlQuery = "";

        public static string getPaymentDetails(string StrID)
        {
            sqlQuery = string.Empty;

            sqlQuery = "     select d.attribute1 as line_no,d.attribute2,d.sales_order_type,SCM.DESCRIPTION AS SOT_DESC,";
            sqlQuery += "    d.CUST_PAY_DTL_ID,";
            sqlQuery += "    d.CUST_PAY_ID,";
            sqlQuery += "    d.CUST_INV_ID,iv.OM_INV_ID,";
            sqlQuery += "    to_char(d.CUST_INV_AMT) as CUST_INV_AMT,";
            sqlQuery += "    to_char(d.CUST_INV_PAID_AMT) as CUST_INV_PAID_AMT,";
            sqlQuery += "    0 as amount_to_pay,";
            sqlQuery += "    iv.OM_INV_NUM,";
            sqlQuery += "    iv.OM_INV_DATE,";
            sqlQuery += "    to_char(iv.OM_INV_RETENTION_AMT) as OM_INV_RETENTION_AMT,";
            sqlQuery += "    to_char(iv.OM_INV_AMT) as OM_INV_AMT,";
            sqlQuery += "   'N' as deleted";
            sqlQuery += "   from om_cust_payment_dtl d, om_cust_invoice_hdr iv,SSM_CODE_MASTERS SCM";
            sqlQuery += "  where d.CUST_INV_ID = iv.om_inv_id AND SCM.CODE=D.SALES_ORDER_TYPE";
            sqlQuery += "  and d.workflow_completion_status = '1'";
            sqlQuery += "  and d.CUST_PAY_ID='" + StrID + "'";
            sqlQuery += " and upper(d.sales_order_type)='INVOICE'";
            sqlQuery += " UNION ";
            sqlQuery += "     select d.attribute1 as line_no,d.attribute2,d.sales_order_type,SCM.DESCRIPTION AS SOT_DESC,";
            sqlQuery += "    d.CUST_PAY_DTL_ID,";
            sqlQuery += "    d.CUST_PAY_ID,";
            sqlQuery += "    '' AS CUST_INV_ID,'' as OM_INV_ID,";
            sqlQuery += "    to_char(d.CUST_INV_AMT) as CUST_INV_AMT,";
            sqlQuery += "    to_char(d.CUST_INV_PAID_AMT) as CUST_INV_PAID_AMT,";
            sqlQuery += "    0 as amount_to_pay,";
            sqlQuery += "   d.attribute2 AS OM_INV_NUM,";
            sqlQuery += "    d.inv_date as OM_INV_DATE,";
            sqlQuery += "    '0' as OM_INV_RETENTION_AMT,";
            sqlQuery += "    to_char(d.CUST_INV_PAID_AMT) as OM_INV_AMT,";
            sqlQuery += "   'N' as deleted";
            sqlQuery += "   from om_cust_payment_dtl d,SSM_CODE_MASTERS SCM";
            sqlQuery += "  where SCM.CODE=D.SALES_ORDER_TYPE AND d.workflow_completion_status = '1'";
            sqlQuery += "  and d.CUST_PAY_ID='" + StrID + "'";
            sqlQuery += " and upper(d.sales_order_type) !='INVOICE'";
            sqlQuery += " order by CUST_PAY_DTL_ID ";

            return sqlQuery;

        }
        public static string getInvoiceDetails(string currId)
        {
            sqlQuery = string.Empty;

            sqlQuery = "    select h.OM_INV_ID,";
            sqlQuery += "    h.OM_INV_NUM,";
            sqlQuery += "    h.OM_VENDOR_ID,";
            sqlQuery += "    h.OM_INV_ACCT_CODE_ID,";
            sqlQuery += "    h.OM_INV_RETENTION_AMT,";
            sqlQuery += "    h.GLOBAL_SEGMENT_ID,";
            sqlQuery += "    h.om_inv_curr_code";
            sqlQuery += "  from om_cust_invoice_hdr h";
            sqlQuery += "   where h.OM_INV_CURR_CODE = '" + currId + "'";
            sqlQuery += "    and h.workflow_completion_status = '1'";
            sqlQuery += "   and h.enabled_flag = '1'";

            return sqlQuery;

        }


        public static string getAmtreceivedfrom_Receipt(string cust_inv_id)
        {
            sqlQuery = string.Empty;


            sqlQuery += " select sum(cp.cust_inv_amt) as cust_inv_amt";
            sqlQuery += " from OM_CUST_PAYMENT_DTL cp";
            sqlQuery += " where cp.workflow_completion_status = 1";
            sqlQuery += " and cp.cust_inv_id = '" + cust_inv_id + "'";


            return sqlQuery;

        }

        public static string getSierraCustomerReceipt()
        {
            sqlQuery = string.Empty;

            sqlQuery = " select distinct v.Customer_Name, v.Customer_ID || ' - ' || v.Customer_Name as Cust_Name, sc.description";
            sqlQuery += " ,t.Receipt_ID, t.Receipt_Number,t.Receipt_Date,t.Customer_ID,to_char(t.Receipt_Amount) as Receipt_Amount, t.Invoice_ID, t.Account_Code ";
            sqlQuery += " ,(case t.POSTED when '1' then 'TRUE' ELSE 'FALSE' END) AS POSTED";
            sqlQuery += "  ,(select ie.error_message from gl_intf_errors ie where ie.error_id = t.RECEIPT_ID and rownum=1) as error_message ";
            sqlQuery += " from DM_SR_CUST_RECEIPT@CRCINTERFACE t, DM_SR_CUSTOMER@CRCINTERFACE v, ssm_code_masters sc";
            sqlQuery += " where v.Customer_ID = t.Customer_ID";
            sqlQuery += " and (t.posted is null or t.posted = '0')";
            sqlQuery += " and sc.code='Miscellaneous' and sc.parent_code='LINETYPE'";
            sqlQuery += " order by t.Receipt_Number asc";

            return sqlQuery;
        }

    }
}
