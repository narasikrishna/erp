﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.AR
{
    public class CustomerBankDetails_DAL
    {
        static string sqlQuery = "";

        public static string GetCustomerbankdtls(long Master_id)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select scb.PK_ID,cb.bank_id,cb.bank_name,bb.bank_branch_id,bb.bank_branch_name,BB.BANK_IFSC_CODE,BB.BANK_SWIFT_CODE,v.code value_key_id,v.code value_name,scb.vendor_bank_account_code,'N' AS DELETED,";
            sqlQuery += " CASE scb.ENABLED_FLAG WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS ENABLED_FLAG";
            sqlQuery += " from supplier_customer_bank scb,CA_BANK cb,CA_BANK_BRANCH bb,ssm_code_masters v";
            sqlQuery += " where scb.bank_id = cb.bank_id";
            sqlQuery += " and scb.bank_branch_id = bb.bank_branch_id";
            sqlQuery += " and scb.vendor_bank_acctount_type = v.code";
            sqlQuery += " AND V.PARENT_CODE = 'AT'";
            sqlQuery += " and scb.PK_ID = " + Master_id;
            //  sqlQuery += " and scb.enabled_flag = 1";
            sqlQuery += " and scb.workflow_completion_status = 1";

            return sqlQuery;

        }

        public static string GetIFSC_SWIFTCODE(string BANK_BRANCH_ID)
        {
            sqlQuery = string.Empty;


            sqlQuery = " SELECT BB.BANK_IFSC_CODE,BB.BANK_SWIFT_CODE";
            sqlQuery += " FROM CA_BANK_BRANCH BB";
            sqlQuery += " WHERE BB.BANK_BRANCH_ID = '" + BANK_BRANCH_ID + "'";

            return sqlQuery;

        }

        public static string GetBranchName(string VENDOR_LOC_ID)
        {
            sqlQuery = string.Empty;
            
            sqlQuery = " SELECT  CB.VENDOR_BRANCH_CODE";
            sqlQuery += " FROM SUPPLIER_CUSTOMER_BRANCH CB";
            sqlQuery += " WHERE CB.VENDOR_LOC_ID = '" + VENDOR_LOC_ID + "'";

            return sqlQuery;

        }


    }
}
