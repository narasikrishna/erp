﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.AR
{
    public class PickRelease_DAL
    {
        static string sqlQuery = "";

        public static string getLotNo(string item_id)
        {
            sqlQuery = string.Empty;


            sqlQuery = " SELECT RL.LOT_ID,RL.LOT_NUMBER";
            sqlQuery += " from INV_RECEIPT_LOTS_HDR rl";
            sqlQuery += " WHERE RL.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " and rl.item_id = '" + item_id + "'";
            sqlQuery += " AND RL.ENABLED_FLAG = 1";
            return sqlQuery;
        }

        public static string getShippedQty(string item_id, string om_dc_id)
        {
            sqlQuery = string.Empty;
            
            sqlQuery += " select DD.OM_SHIPPED_QTY";
            sqlQuery += " from OM_DC_HDR DH,OM_DC_DTL DD";
            sqlQuery += " WHERE DH.OM_DC_ID =DD.OM_DC_ID";
            sqlQuery += "  AND DD.OM_ITEM_ID ='" + item_id + "'";
            sqlQuery += "  and dh.om_dc_id ='" + om_dc_id + "'";
            return sqlQuery;
        }


        public static string getPickReleaseDetails(string OM_PR_ID)
        {
            sqlQuery = string.Empty;



            sqlQuery = " SELECT  pr.om_pr_dtl_id,rl.lot_number,rl.lot_id,w.INV_WH_ID,w.INV_WH_NAME,pr.om_lot_qty as LOT_QTY,'N' AS DELETED";
            sqlQuery += " FROM OM_PICK_RELEASE_DTL pr,INV_RECEIPT_LOTS_HDR rl,INV_WAREHOUSES w";
            sqlQuery += " where pr.om_lot_id = rl.lot_id";
            sqlQuery += " and w.INV_WH_ID = pr.OM_WH_ID";
            sqlQuery += " and pr.workflow_completion_status = 1";
            sqlQuery += " and pr.enabled_flag = 1";
            sqlQuery += " AND PR.OM_PR_ID = '" + OM_PR_ID + "'";
            sqlQuery += " order by OM_PR_DTL_ID ";
            return sqlQuery;
        }


        public static string getLotqty(string LOT_ID)
        {
            sqlQuery = string.Empty;

            sqlQuery = "  SELECT RL.LOT_QTY,RL.RECEIPT_ID,RL.ITEM_ID";
            sqlQuery += " from INV_RECEIPT_LOTS_HDR rl";
            sqlQuery += " WHERE RL.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND RL.ENABLED_FLAG = 1";
            sqlQuery += " AND RL.LOT_ID = '" + LOT_ID + "'";
            return sqlQuery;
        }

        public static string getLotqty_fromPR(string LOT_ID)
        {
            sqlQuery = string.Empty;

            sqlQuery += " select prd.om_lot_qty";
            sqlQuery += " from OM_PICK_RELEASE_DTL prd";
            sqlQuery += "  where prd.om_lot_id ='" + LOT_ID + "'";
            return sqlQuery;
        }

        public static string getBalqty(int rec_lot_qty, int tot_qty)
        {
            sqlQuery = string.Empty;

            sqlQuery += " select (" + rec_lot_qty + "-" + tot_qty + ") from dual";

            return sqlQuery;
        }


        public static string getshippedqty(string om_dc_id)
        {
            sqlQuery = string.Empty;

            sqlQuery += " select dd.om_shipped_qty";
            sqlQuery += " from OM_DC_DTL dd";
            sqlQuery += " where dd.workflow_completion_status = 1";
            sqlQuery += " and dd.om_dc_id = '" + om_dc_id + "'";


            return sqlQuery;
        }




        public static string getDcdate(string OM_DC_ID)
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT Dc.Om_Dc_Date";
            sqlQuery += " FROM OM_DC_HDR Dc";
            sqlQuery += " WHERE Dc.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND Dc.ENABLED_FLAG = 1";
            sqlQuery += " AND Dc.OM_DC_ID = '" + OM_DC_ID + "'";
            return sqlQuery;
        }

        public static string getDcdtl(int om_dc_line_num, string OM_DC_ID)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select dd.om_order_line_num,co.om_order_id,co.om_order_num,im.item_id,im.item_name";
            sqlQuery += " from Om_Dc_Dtl dd,OM_CUSTOMER_ORDER_HDR co,inv_item_master im";
            sqlQuery += " where dd.om_order_id = co.om_order_id";
            sqlQuery += " and dd.om_item_id = im.item_id";
            sqlQuery += " and dd.om_dc_line_num = " + om_dc_line_num;
            sqlQuery += " and dd.om_dc_id = '" + OM_DC_ID + "'";

            return sqlQuery;
        }



        public static void UPDATE_Receiptlot_dtl(string P_LOT_ID, decimal P_QTY)
        {
            try
            {

                string strQuery = string.Empty;


                strQuery = "  update INV_RECEIPT_LOTS_HDR ff set ff.MODIFIED_BY='" + VMVServices.Web.Utils.UserName + "',ff.MODIFIED_DATE='" + DateTime.Today.ToString("dd-MMM-yyyy") + "', ff.lot_qty='" + P_QTY + "'";
                strQuery += "  where ff.lot_id='" + P_LOT_ID + "'";

                DBMethod.ExecuteNonQuery(strQuery);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }





    }
}
