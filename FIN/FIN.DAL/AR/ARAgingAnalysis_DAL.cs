﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.AR
{
    public class ARAgingAnalysis_DAL
    {
        static string sqlQuery = "";
        public static string getReportData()
        {
            sqlQuery = string.Empty;
            //sqlQuery = " SELECT * FROM AP_AGING_ANALYSIS V WHERE ROWNUM > 0 ";
            //if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            //{
            //    if (VMVServices.Web.Utils.ReportViewFilterParameter["SEGMENT_ID"] != null)
            //    {
            //        sqlQuery += " AND V.GLOBAL_SEGMENT_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["SEGMENT_ID"].ToString() + "'";
            //    }
            //    if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
            //    {
            //        sqlQuery += " AND V.INV_DATE >= to_date('" + VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy') ";
            //    }
            //}



            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {

                sqlQuery = "    (select SBT_T.VENDOR_ID,SBT_T.VENDOR_NAME,SBT_T.OUTSTANDING_AMOUNT,SBT_T.INV_DATE,SBT_T.INV_NUM,";
                if (VMVServices.Web.Utils.ReportViewFilterParameter["firstCase"] != null)
                {
                    sqlQuery += "   (case";
                    sqlQuery += "   WHEN SBT_T.No_of_days BETWEEN 0 and '" + VMVServices.Web.Utils.ReportViewFilterParameter["firstCase"] + "' THEN SBT_T.OUTSTANDING_AMOUNT END) as COL1_0_30,";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["firstCase"] != null && VMVServices.Web.Utils.ReportViewFilterParameter["secondCase"] != null)
                {
                    sqlQuery += "   (case";
                    sqlQuery += "   WHEN SBT_T.No_of_days BETWEEN '" + (int.Parse(VMVServices.Web.Utils.ReportViewFilterParameter["firstCase"].ToString()) + 1) + "' and  '" + VMVServices.Web.Utils.ReportViewFilterParameter["secondCase"] + "' THEN SBT_T.OUTSTANDING_AMOUNT END) as COL2_30_45,";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["secondCase"] != null && VMVServices.Web.Utils.ReportViewFilterParameter["thirdCase"] != null)
                {
                    sqlQuery += "   (case";
                    sqlQuery += "   WHEN SBT_T.No_of_days BETWEEN '" + (int.Parse(VMVServices.Web.Utils.ReportViewFilterParameter["secondCase"].ToString()) + 1) + "' and  '" + VMVServices.Web.Utils.ReportViewFilterParameter["thirdCase"] + "' THEN SBT_T.OUTSTANDING_AMOUNT END) as COL3_45_60,";

                    sqlQuery += "   (case";
                    sqlQuery += "   WHEN SBT_T.No_of_days > '" + VMVServices.Web.Utils.ReportViewFilterParameter["thirdCase"] + "' THEN SBT_T.OUTSTANDING_AMOUNT END) as COL5_90,";
                }

                sqlQuery += "   SBT_T.GLOBAL_SEGMENT_ID,sbt_t.org_id";
                sqlQuery += "   from";
                sqlQuery += "   (select SBT.INV_DATE,sbt.org_id,";
                sqlQuery += "      SBT.INV_NUM,";
                sqlQuery += "     SBT.VENDOR_ID,";
                sqlQuery += "     SBT.VENDOR_NAME,";
                sqlQuery += "      SBT.INVOICE_AMOUNT,";
                sqlQuery += "    SBT.AMOUNT_PAID,";
                sqlQuery += "     (SBT.INVOICE_AMOUNT - SBT.AMOUNT_PAID) as OUTSTANDING_AMOUNT,";
                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
                {
                    sqlQuery += "    ((to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "', 'DD-MM-YYYY')) -";
                    sqlQuery += "   ((SBT.INV_DATE))) as No_of_days,";
                }
                sqlQuery += "   SBT.GLOBAL_SEGMENT_ID";
                sqlQuery += "    FROM  (select IH.OM_INV_DATE AS INV_DATE,";
                sqlQuery += "    ih.org_id as org_id,";
                sqlQuery += "    IH.OM_INV_NUM AS INV_NUM,";
                sqlQuery += "    IH.OM_VENDOR_ID AS VENDOR_ID,";
                sqlQuery += "    SC.VENDOR_NAME,";
                sqlQuery += "   nvl(SUM(ID.OM_ITEM_COST), 0) as INVOICE_AMOUNT,";
                sqlQuery += "   (select nvl(sum(IPH.PAYMENT_AMT), 0)";
                sqlQuery += "   from inv_payments_hdr IPH";
                sqlQuery += "   where IPH.Pay_Vendor_Id = IH.OM_VENDOR_ID) as AMOUNT_PAID,";
                sqlQuery += "    IH.GLOBAL_SEGMENT_ID";
                sqlQuery += "    from om_cust_invoice_hdr   IH,";
                sqlQuery += "     om_cust_invoice_dtl  ID,";
                sqlQuery += "    supplier_customers SC";
                sqlQuery += "    where IH.OM_INV_ID = ID.OM_INV_ID";
                sqlQuery += "     and ih.rev_flag is null";
                sqlQuery += "     and ih.posted_flag = 1";
                sqlQuery += "     and IH.OM_VENDOR_ID = SC.VENDOR_ID";
                sqlQuery += "    and (ID.OM_ITEM_COST is not null)";
                sqlQuery += "    group by IH.OM_INV_DATE,";
                sqlQuery += "   IH.OM_INV_NUM,";
                sqlQuery += "     IH.OM_VENDOR_ID,";
                sqlQuery += "    SC.VENDOR_NAME,";
                sqlQuery += "    ih.org_id,";
                sqlQuery += "    SC.VENDOR_ID,";
                sqlQuery += "                  IH.GLOBAL_SEGMENT_ID) SBT ) SBT_T ";
                sqlQuery += "  )";

            }
            return sqlQuery;
        }

        public static string getChequesReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_AR_PAYABLE_SYSTEM V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {

                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
                {
                    sqlQuery += " AND V.PAY_DATE between to_date('" + VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy')";
                }
                //if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.EFFECTIVE_TO_DT] != null)
                //{
                //    sqlQuery += " AND to_date('" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.EFFECTIVE_TO_DT].ToString() + "','dd/MM/yyyy') )";
                //}
            }
            return sqlQuery;
        }


        public static string getCollectedReceivablesBalanceReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_AR_PAYMENT V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
            {
                sqlQuery += " AND V.CR_DATE >= to_date('" + VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy')";
            }
            if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
            {
                sqlQuery += " AND V.CR_DATE <= to_date('" + VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"].ToString() + "','dd/MM/yyyy')";
            }

            if (VMVServices.Web.Utils.ReportViewFilterParameter["CUST_FROM_NAME"] != null)
            {
                sqlQuery += " AND V.VENDOR_CODE_NAME >='" + VMVServices.Web.Utils.ReportViewFilterParameter["CUST_FROM_NAME"].ToString() + "'";
            }
            if (VMVServices.Web.Utils.ReportViewFilterParameter["CUST_TO_NAME"] != null)
            {
                sqlQuery += " AND V.VENDOR_CODE_NAME <='" + VMVServices.Web.Utils.ReportViewFilterParameter["CUST_TO_NAME"].ToString() + "'";
            }
            if (VMVServices.Web.Utils.ReportViewFilterParameter["CATEGORY_ID"] != null)
            {
                sqlQuery += " AND V.supplier_category ='" + VMVServices.Web.Utils.ReportViewFilterParameter["CATEGORY_ID"].ToString() + "'";
            }

            if (VMVServices.Web.Utils.ReportViewFilterParameter["INV_FROM"] != null)
            {
                sqlQuery += " AND V.CR_NUMBER >='" + VMVServices.Web.Utils.ReportViewFilterParameter["INV_FROM"].ToString() + "'";
            }
            if (VMVServices.Web.Utils.ReportViewFilterParameter["INV_TO"] != null)
            {
                sqlQuery += " AND V.CR_NUMBER <='" + VMVServices.Web.Utils.ReportViewFilterParameter["INV_TO"].ToString() + "'";
            }

            if (VMVServices.Web.Utils.ReportViewFilterParameter["Debit_From_Amt"] != null)
            {
                sqlQuery += " AND V.Tot_Amt >=" + VMVServices.Web.Utils.ReportViewFilterParameter["Debit_From_Amt"].ToString();
            }
            if (VMVServices.Web.Utils.ReportViewFilterParameter["Debit_To_Amt"] != null)
            {
                sqlQuery += " AND V.Tot_Amt <=" + VMVServices.Web.Utils.ReportViewFilterParameter["Debit_To_Amt"].ToString();
            }
            
            return sqlQuery;
        }

        public static string getReceivablesSystemReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_AR_PAYMENT_SUPPLIER V WHERE ROWNUM > 0 ";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                ////Need to add global segment -- confirm with krishna
                //if (VMVServices.Web.Utils.ReportViewFilterParameter["GlobalSegmentID"] != null)
                //{
                //    sqlQuery += " AND V.GLOBAL_SEGMENT_ID >= '" + VMVServices.Web.Utils.ReportViewFilterParameter["GlobalSegmentID"].ToString() + "'";
                //}
                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Customer_Id"] != null)
                {
                    sqlQuery += " AND V.CUSTOMER_ID >= '" + VMVServices.Web.Utils.ReportViewFilterParameter["From_Customer_Id"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Customer_Id"] != null)
                {
                    sqlQuery += " AND V.CUSTOMER_ID <= '" + VMVServices.Web.Utils.ReportViewFilterParameter["To_Customer_Id"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["Debit_From_Amt"] != null)
                {
                    sqlQuery += " AND V.DEBIT >=  '" + VMVServices.Web.Utils.ReportFilterParameter["Debit_From_Amt"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["Debit_To_Amt"] != null)
                {
                    sqlQuery += " AND V.DEBIT <=  '" + VMVServices.Web.Utils.ReportFilterParameter["Debit_To_Amt"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["Credit_From_Amt"] != null)
                {
                    sqlQuery += " AND V.CREDIT >=  '" + VMVServices.Web.Utils.ReportFilterParameter["Credit_From_Amt"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["Credit_To_Amt"] != null)
                {
                    sqlQuery += " AND V.CREDIT <=  '" + VMVServices.Web.Utils.ReportFilterParameter["Credit_To_Amt"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["POSTEDTYPE"] == "P")
                {
                    sqlQuery += " AND V.POSTED_FLAG='1' and v.rev_flag is null";
                }
                else if (VMVServices.Web.Utils.ReportFilterParameter["POSTEDTYPE"] == "U")
                {
                    sqlQuery += " AND (V.POSTED_FLAG IS NULL OR V.POSTED_FLAG='0') and v.rev_flag is null";
                }
                else if (VMVServices.Web.Utils.ReportFilterParameter["POSTEDTYPE"] == "C")
                {
                    sqlQuery += " AND (V.REV_FLAG='1' and v.POSTED_FLAG in ('1','5'))";
                }
            }

            return sqlQuery;
        }

        public static string getInvoiceListReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_AR_INVOICE_LIST V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {

                if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.EFFECTIVE_FROM_DT] != null)
                {
                    sqlQuery += " AND V.INV_DATE between to_date('" + VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.EFFECTIVE_FROM_DT].ToString() + "','dd/MM/yyyy')";
                }
                //if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.EFFECTIVE_TO_DT] != null)
                //{
                //    sqlQuery += " AND to_date('" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.EFFECTIVE_TO_DT].ToString() + "','dd/MM/yyyy') )";
                //}
            }
            return sqlQuery;
        }
        public static string getCheckListReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_CHEQUE V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {

                if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.EFFECTIVE_FROM_DT] != null)
                {
                    sqlQuery += " AND V.LC_DATE between to_date('" + VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.EFFECTIVE_FROM_DT].ToString() + "','dd/MM/yyyy')";
                }
                //if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.EFFECTIVE_TO_DT] != null)
                //{
                //    sqlQuery += " AND to_date('" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.EFFECTIVE_TO_DT].ToString() + "','dd/MM/yyyy') )";
                //}
            }
            return sqlQuery;
        }

        public static string getGlobalSegment()
        {
            sqlQuery = string.Empty;
            sqlQuery += " SELECT SV.SEGMENT_VALUE_ID,SV.SEGMENT_VALUE" + VMVServices.Web.Utils.LanguageCode;
            sqlQuery += " FROM GL_SEGMENT_VALUES SV ";
            sqlQuery += " WHERE SV.WORKFLOW_COMPLETION_STATUS='1' ";
            sqlQuery += " AND SV.ENABLED_FLAG='1'";


            return sqlQuery;
        }

    }
}
