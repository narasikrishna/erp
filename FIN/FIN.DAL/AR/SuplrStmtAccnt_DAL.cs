﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.AR
{
    public class SuplrStmtAccnt_DAL
    {
        static string sqlQuery = "";
        public static string getReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM AP_SUPLR_STMT_ACCOUNT V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["SEGMENT_ID"] != null)
                {
                    sqlQuery += " AND V.GLOBAL_SEGMENT = '" + VMVServices.Web.Utils.ReportViewFilterParameter["SEGMENT_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["VENDOR_ID"] != null)
                {
                    sqlQuery += " AND V.VENDOR_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["VENDOR_ID"].ToString() + "'";
                }
            }
            return sqlQuery;
        }
        public static string getGlobalSegment()
        {
            sqlQuery = string.Empty;
            sqlQuery += " SELECT SV.SEGMENT_ID,SV.SEGMENT_VALUE" + VMVServices.Web.Utils.LanguageCode;
            sqlQuery += " FROM GL_SEGMENT_VALUES SV ";
            sqlQuery += " WHERE SV.WORKFLOW_COMPLETION_STATUS='1' ";
            sqlQuery += " AND SV.ENABLED_FLAG='1'";


            return sqlQuery;
        }

        public static string getVendorDtls()
        {
            sqlQuery = string.Empty;
            sqlQuery += " select SC.VENDOR_ID";
            sqlQuery += " from SUPPLIER_CUSTOMERS SC ";
            sqlQuery += " where SC.VENDOR_TYPE = 'Supplier' ";
            sqlQuery += " and SC.WORKFLOW_COMPLETION_STATUS = '1' ";
            sqlQuery += " AND SC.ENABLED_FLAG='1'";


            return sqlQuery;
        }
        public static string getSupplierName()
        {
            sqlQuery = string.Empty;
            sqlQuery += " SELECT S.SUPPLIER_NUMBER,S.SUPPLIER_NAME";
            sqlQuery += " FROM INV_SUPPLIER_R S ";
            
            sqlQuery += " WHERE S.WORKFLOW_COMPLETION_STATUS = '1' ";
            sqlQuery += " AND S.ENABLED_FLAG='1'";


            return sqlQuery;
        }

        // public static string getVendorDtls()
        //{
        //    sqlQuery = string.Empty;
        //    sqlQuery += " select SC.VENDOR_ID";
        //    sqlQuery += " from SUPPLIER_CUSTOMERS SC ";
        //    sqlQuery += " where SC.VENDOR_TYPE = 'Supplier' ";
        //    sqlQuery += " and SC.WORKFLOW_COMPLETION_STATUS = '1' ";
        //    sqlQuery += " AND SC.ENABLED_FLAG='1'";


        //    return sqlQuery;
        //}

        public static string getVendorName(string Vendor_id)
        {
            sqlQuery = string.Empty;
            sqlQuery += " select SC.VENDOR_NAME";
            sqlQuery += " from SUPPLIER_CUSTOMERS SC ";
            sqlQuery += " where SC.VENDOR_TYPE = 'Supplier' ";
            sqlQuery += " and SC.WORKFLOW_COMPLETION_STATUS = '1' ";
            sqlQuery += " AND SC.ENABLED_FLAG='1'";
            sqlQuery += " AND SC.VENDOR_ID = " + Vendor_id;


            return sqlQuery;
        }
    }
}
