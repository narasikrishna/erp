﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.AR
{
    public class Invoice_DAL
    {
        static string sqlQuery = "";

        public static string getInvoiceDetails(string str_INVID)
        {
            sqlQuery = string.Empty;

            sqlQuery += " SELECT ID.OM_CUST_INV_DTL_ID,ID.OM_INV_LINE_NUM,ID.OM_DC_ID,IRH.GRN_NUM,id.ATTRIBUTE1 as MIS_NO ";
            sqlQuery += " ,ID.OM_ITEM_ID as ITEM_ID";
            sqlQuery += " ,ITM.ITEM_CODE,ITM.ITEM_NAME ";
            sqlQuery += " ,to_char(ID.OM_ITEM_QTY) as OM_ITEM_QTY,to_char(ID.OM_ITEM_PRICE) as OM_ITEM_PRICE,to_char(ID.OM_ITEM_COST) as OM_ITEM_COST,ID.OM_ITEM_ACCT_CODE_ID as CODE_ID";
            sqlQuery += "  ,(SELECT GAC.ACCT_CODE ||' - '|| GAC.ACCT_CODE_DESC" + VMVServices.Web.Utils.LanguageCode + " AS CODE_NAME";
            sqlQuery += "  FROM GL_ACCT_CODES GAC";
            sqlQuery += "   WHERE GAC.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += "    AND GAC.ENABLED_FLAG = 1 and id.Om_Item_Acct_Code_Id=GAC.ACCT_CODE_ID";
            sqlQuery += "  ) as CODE_NAME";
            sqlQuery += " ,OM_INV_SEGMENT_ID1,OM_INV_SEGMENT_ID2,OM_INV_SEGMENT_ID3,OM_INV_SEGMENT_ID4,OM_INV_SEGMENT_ID5,OM_INV_SEGMENT_ID6";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = OM_INV_SEGMENT_ID1 ) AS SEGMENT_1_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = OM_INV_SEGMENT_ID2 ) AS SEGMENT_2_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = OM_INV_SEGMENT_ID3 ) AS SEGMENT_3_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = OM_INV_SEGMENT_ID4 ) AS SEGMENT_4_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = OM_INV_SEGMENT_ID5 ) AS SEGMENT_5_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = OM_INV_SEGMENT_ID6 ) AS SEGMENT_6_TEXT";
            sqlQuery += " ,ID.OM_INV_LINE_TYPE,ID.OM_INV_LINE_TYPE AS OM_INV_LINE_TYPE_DESC ";
            sqlQuery += " , OM_INV_LINE_TAX_AMT ";
            sqlQuery += " ,'N' AS DELETED ";
            sqlQuery += " FROM OM_CUST_INVOICE_DTL  ID ";
            sqlQuery += " INNER JOIN INV_RECEIPTS_HDR IRH ON IRH.RECEIPT_ID= ID.OM_DC_ID ";
            sqlQuery += " INNER JOIN INV_ITEM_MASTER ITM ON ITM.ITEM_ID= ID.OM_ITEM_ID ";
            sqlQuery += " INNER JOIN SSM_CODE_MASTERS SCM_LT ON SCM_LT.CODE=ID.OM_INV_LINE_TYPE AND SCM_LT.PARENT_CODE='LINETYPE' ";
            sqlQuery += " WHERE ID.OM_INV_ID='" + str_INVID + "' AND lower(OM_INV_LINE_TYPE)=lower('Receipt') ";


            sqlQuery += " UNION ";
            sqlQuery += " SELECT ID.OM_CUST_INV_DTL_ID, ";
            sqlQuery += " ID.OM_INV_LINE_NUM, ";
            sqlQuery += " ID.OM_DC_ID,SCM_OT.Description as GRN_NUM,id.ATTRIBUTE1 as MIS_NO, ";
            sqlQuery += " ID.OM_ITEM_ID as ITEM_ID, ";
            sqlQuery += " SCM_OT.CODE as ITEM_CODE, ";
            sqlQuery += " SCM_OT.Description" + VMVServices.Web.Utils.LanguageCode + " as ITEM_NAME, ";
            sqlQuery += " to_char(ID.OM_ITEM_QTY) as OM_ITEM_QTY, ";
            sqlQuery += " to_char(ID.OM_ITEM_PRICE) as OM_ITEM_PRICE, ";
            sqlQuery += " to_char(ID.OM_ITEM_COST) as OM_ITEM_COST, ";
            sqlQuery += " ID.OM_ITEM_ACCT_CODE_ID as code_id, ";
            sqlQuery += " (SELECT GAC.ACCT_CODE || ' - ' || GAC.ACCT_CODE_DESC" + VMVServices.Web.Utils.LanguageCode + "  AS CODE_NAME ";
            sqlQuery += " FROM GL_ACCT_CODES GAC ";
            sqlQuery += " WHERE GAC.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " AND GAC.ENABLED_FLAG = 1 ";
            sqlQuery += " and id.Om_Item_Acct_Code_Id = GAC.ACCT_CODE_ID) as code_name, ";
            sqlQuery += " OM_INV_SEGMENT_ID1, ";
            sqlQuery += " OM_INV_SEGMENT_ID2, ";
            sqlQuery += " OM_INV_SEGMENT_ID3, ";
            sqlQuery += " OM_INV_SEGMENT_ID4, ";
            sqlQuery += " OM_INV_SEGMENT_ID5, ";
            sqlQuery += " OM_INV_SEGMENT_ID6, ";
            sqlQuery += " (SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = OM_INV_SEGMENT_ID1 ) AS SEGMENT_1_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = OM_INV_SEGMENT_ID2 ) AS SEGMENT_2_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = OM_INV_SEGMENT_ID3 ) AS SEGMENT_3_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = OM_INV_SEGMENT_ID4 ) AS SEGMENT_4_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = OM_INV_SEGMENT_ID5 ) AS SEGMENT_5_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = OM_INV_SEGMENT_ID6 ) AS SEGMENT_6_TEXT";
            sqlQuery += " ,ID.OM_INV_LINE_TYPE, ";
            sqlQuery += " ID.OM_INV_LINE_TYPE AS OM_INV_LINE_TYPE_DESC, ";
            sqlQuery += " OM_INV_LINE_TAX_AMT, ";
            sqlQuery += " 'N' AS DELETED ";
            sqlQuery += "  FROM OM_CUST_INVOICE_DTL  ID   ";
            sqlQuery += "  INNER JOIN SSM_CODE_MASTERS SCM_LT ON SCM_LT.CODE = ID.OM_INV_LINE_TYPE AND SCM_LT.PARENT_CODE = 'LINETYPE' ";
            sqlQuery += " INNER JOIN SSM_CODE_MASTERS SCM_OT ON SCM_OT.CODE = ID.OM_DC_ID AND SCM_OT.PARENT_CODE = 'OC' ";
            sqlQuery += " WHERE ID.OM_INV_ID = '" + str_INVID + "' AND lower(OM_INV_LINE_TYPE)=lower('Other_Charges') ";

            sqlQuery += " UNION ";
            sqlQuery += "  SELECT ID.OM_CUST_INV_DTL_ID, ";
            sqlQuery += " ID.OM_INV_LINE_NUM, ";
            sqlQuery += " ID.OM_DC_ID,ITM.ITEM_NAME as GRN_NUM,id.ATTRIBUTE1 as MIS_NO, ";
            sqlQuery += "  ID.OM_ITEM_ID as ITEM_ID, ";
            sqlQuery += "  ITM.ITEM_CODE, ";
            sqlQuery += "  ITM.ITEM_NAME, ";
            sqlQuery += "  to_char(ID.OM_ITEM_QTY) as OM_ITEM_QTY, ";
            sqlQuery += "  to_char(ID.OM_ITEM_PRICE) as OM_ITEM_PRICE, ";
            sqlQuery += "  to_char(ID.OM_ITEM_COST) as OM_ITEM_COST, ";
            sqlQuery += " ID.OM_ITEM_ACCT_CODE_ID as code_id, ";
            sqlQuery += " (SELECT GAC.ACCT_CODE || ' - ' || GAC.ACCT_CODE_DESC" + VMVServices.Web.Utils.LanguageCode + " AS CODE_NAME ";
            sqlQuery += "    FROM GL_ACCT_CODES GAC ";
            sqlQuery += "  WHERE GAC.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += "   AND GAC.ENABLED_FLAG = 1 ";
            sqlQuery += "   and id.Om_Item_Acct_Code_Id = GAC.ACCT_CODE_ID) as code_name, ";
            sqlQuery += "  OM_INV_SEGMENT_ID1, ";
            sqlQuery += "  OM_INV_SEGMENT_ID2, ";
            sqlQuery += "  OM_INV_SEGMENT_ID3, ";
            sqlQuery += "  OM_INV_SEGMENT_ID4, ";
            sqlQuery += "  OM_INV_SEGMENT_ID5, ";
            sqlQuery += "  OM_INV_SEGMENT_ID6, ";
            sqlQuery += " (SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = OM_INV_SEGMENT_ID1 ) AS SEGMENT_1_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = OM_INV_SEGMENT_ID2 ) AS SEGMENT_2_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = OM_INV_SEGMENT_ID3 ) AS SEGMENT_3_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = OM_INV_SEGMENT_ID4 ) AS SEGMENT_4_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = OM_INV_SEGMENT_ID5 ) AS SEGMENT_5_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = OM_INV_SEGMENT_ID6 ) AS SEGMENT_6_TEXT";
            sqlQuery += "  ,ID.OM_INV_LINE_TYPE, ";
            sqlQuery += "   ID.OM_INV_LINE_TYPE AS OM_INV_LINE_TYPE_DESC, ";
            sqlQuery += "   OM_INV_LINE_TAX_AMT, ";
            sqlQuery += "   'N' AS DELETED ";
            sqlQuery += "  FROM OM_CUST_INVOICE_DTL  ID  ";
            sqlQuery += "  INNER JOIN INV_ITEM_MASTER ITM ON ITM.ITEM_ID = ID.OM_DC_ID ";
            sqlQuery += "  INNER JOIN SSM_CODE_MASTERS SCM_LT ON SCM_LT.CODE = ID.OM_INV_LINE_TYPE AND SCM_LT.PARENT_CODE = 'LINETYPE' ";
            sqlQuery += "  WHERE ID.OM_INV_ID = '" + str_INVID + "'";
            sqlQuery += "  and ((lower(OM_INV_LINE_TYPE) = lower('item'))";
            sqlQuery += "  or (lower(OM_INV_LINE_TYPE) = lower('miscellaneous'))";
            sqlQuery += "  or (lower(OM_INV_LINE_TYPE) = lower('service')))";

            sqlQuery += " UNION ";
            sqlQuery += " SELECT ID.OM_CUST_INV_DTL_ID, ";
            sqlQuery += " ID.OM_INV_LINE_NUM, ";
            sqlQuery += " ID.OM_DC_ID,PO.OM_ORDER_NUM as GRN_NUM,id.ATTRIBUTE1 as MIS_NO, ";
            sqlQuery += " ID.OM_ITEM_ID as ITEM_ID, ";
            sqlQuery += " po.OM_ORDER_ID as ITEM_CODE, ";
            // sqlQuery += " PO.Po_Num AS ITEM_NAME, ";
            sqlQuery += " (select iim.item_name from inv_item_master iim where iim.item_id=id.OM_ITEM_ID) AS ITEM_NAME, ";
            sqlQuery += " to_char(ID.OM_ITEM_QTY) as OM_ITEM_QTY, ";
            sqlQuery += " to_char(ID.OM_ITEM_PRICE) as OM_ITEM_PRICE, ";
            sqlQuery += " to_char(ID.OM_ITEM_COST) as OM_ITEM_COST, ";
            sqlQuery += " ID.OM_ITEM_ACCT_CODE_ID as CODE_ID, ";
            sqlQuery += " (SELECT GAC.ACCT_CODE || ' - ' || GAC.ACCT_CODE_DESC" + VMVServices.Web.Utils.LanguageCode + " AS CODE_NAME ";
            sqlQuery += "    FROM GL_ACCT_CODES GAC ";
            sqlQuery += "   WHERE GAC.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += "     AND GAC.ENABLED_FLAG = 1 ";
            sqlQuery += "     and id.Om_Item_Acct_Code_Id = GAC.ACCT_CODE_ID) as code_name, ";
            sqlQuery += "  OM_INV_SEGMENT_ID1, ";
            sqlQuery += "  OM_INV_SEGMENT_ID2, ";
            sqlQuery += " OM_INV_SEGMENT_ID3, ";
            sqlQuery += " OM_INV_SEGMENT_ID4, ";
            sqlQuery += " OM_INV_SEGMENT_ID5, ";
            sqlQuery += " OM_INV_SEGMENT_ID6, ";
            sqlQuery += " (SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = OM_INV_SEGMENT_ID1 ) AS SEGMENT_1_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = OM_INV_SEGMENT_ID2 ) AS SEGMENT_2_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = OM_INV_SEGMENT_ID3 ) AS SEGMENT_3_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = OM_INV_SEGMENT_ID4 ) AS SEGMENT_4_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = OM_INV_SEGMENT_ID5 ) AS SEGMENT_5_TEXT";
            sqlQuery += " ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = OM_INV_SEGMENT_ID6 ) AS SEGMENT_6_TEXT";
            sqlQuery += " ,ID.OM_INV_LINE_TYPE, ";
            sqlQuery += " ID.OM_INV_LINE_TYPE AS OM_INV_LINE_TYPE_DESC, ";
            sqlQuery += " OM_INV_LINE_TAX_AMT, ";
            sqlQuery += " 'N' AS DELETED ";
            sqlQuery += " FROM OM_CUST_INVOICE_DTL  ID  ";
            sqlQuery += " INNER JOIN OM_CUSTOMER_ORDER_HDR PO ON PO.OM_ORDER_ID = ID.OM_DC_ID ";
            // sqlQuery += " INNER JOIN SSM_CODE_MASTERS SCM_LT ON SCM_LT.CODE = ID.OM_INV_LINE_TYPE AND SCM_LT.PARENT_CODE = 'LINETYPE' ";
            sqlQuery += " WHERE ID.OM_INV_ID = '" + str_INVID + "'";


            sqlQuery += "  UNION ";
            sqlQuery += "      SELECT ID.OM_CUST_INV_DTL_ID,";
            sqlQuery += "      ID.OM_INV_LINE_NUM,";
            sqlQuery += "      ID.OM_DC_ID,";
            sqlQuery += "    '' as GRN_NUM,";
            sqlQuery += "     id.ATTRIBUTE1 as MIS_NO,";
            sqlQuery += "     ID.OM_ITEM_ID as ITEM_ID,";
            sqlQuery += "      '' as ITEM_CODE,";
            sqlQuery += "     '' as ITEM_NAME,";
            sqlQuery += "     to_char(ID.OM_ITEM_QTY) as OM_ITEM_QTY,";
            sqlQuery += "     to_char(ID.OM_ITEM_PRICE) as OM_ITEM_PRICE,";
            sqlQuery += "     to_char(ID.OM_ITEM_COST) as OM_ITEM_COST,";
            sqlQuery += "    ID.OM_ITEM_ACCT_CODE_ID as code_id,";
            sqlQuery += "    (SELECT GAC.ACCT_CODE || ' - ' || GAC.ACCT_CODE_DESC AS CODE_NAME";
            sqlQuery += "    FROM GL_ACCT_CODES GAC";
            sqlQuery += "   WHERE GAC.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += "     AND GAC.ENABLED_FLAG = 1";
            sqlQuery += "     and id.Om_Item_Acct_Code_Id = GAC.ACCT_CODE_ID) as code_name,";
            sqlQuery += "    OM_INV_SEGMENT_ID1,";
            sqlQuery += "     OM_INV_SEGMENT_ID2,";
            sqlQuery += "     OM_INV_SEGMENT_ID3,";
            sqlQuery += "     OM_INV_SEGMENT_ID4,";
            sqlQuery += "    OM_INV_SEGMENT_ID5,";
            sqlQuery += "    OM_INV_SEGMENT_ID6,";
            sqlQuery += "  (SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = OM_INV_SEGMENT_ID1 ) AS SEGMENT_1_TEXT";
            sqlQuery += "  ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = OM_INV_SEGMENT_ID2 ) AS SEGMENT_2_TEXT";
            sqlQuery += "  ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = OM_INV_SEGMENT_ID3 ) AS SEGMENT_3_TEXT";
            sqlQuery += "  ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = OM_INV_SEGMENT_ID4 ) AS SEGMENT_4_TEXT";
            sqlQuery += "  ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = OM_INV_SEGMENT_ID5 ) AS SEGMENT_5_TEXT";
            sqlQuery += "  ,(SELECT SEGMENT_VALUE FROM GL_SEGMENT_VALUES WHERE SEGMENT_VALUE_ID = OM_INV_SEGMENT_ID6 ) AS SEGMENT_6_TEXT";
            sqlQuery += "   ,  ID.OM_INV_LINE_TYPE,";
            sqlQuery += "     ID.OM_INV_LINE_TYPE AS OM_INV_LINE_TYPE_DESC,";
            sqlQuery += "    OM_INV_LINE_TAX_AMT,";
            sqlQuery += "   'N' AS DELETED";
            sqlQuery += "   FROM OM_CUST_INVOICE_DTL ID";
            sqlQuery += "   INNER JOIN SSM_CODE_MASTERS SCM_LT ON SCM_LT.CODE = ID.OM_INV_LINE_TYPE";
            sqlQuery += "        AND SCM_LT.PARENT_CODE = 'LINETYPE'";
            sqlQuery += "   WHERE ID.OM_INV_ID = '" + str_INVID + "'";
            sqlQuery += "     and lower(OM_INV_LINE_TYPE) = lower('miscellaneous')";
            sqlQuery += "  order by OM_CUST_INV_DTL_ID ";
            return sqlQuery;
        }
        public static string getInvoiceHeaderDetails(string str_INVID)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select * from OM_CUST_INVOICE_HDR i where i.OM_INV_ID='" + str_INVID + "'";
            return sqlQuery;
        }

        public static string getInvoiceTaxDetails(string str_Inv_Tax_Dtl_Id, string str_EntityNo, string str_ItemNo)
        {
            sqlQuery = string.Empty;


            sqlQuery += "Select TD.INV_TAX_ID, TD.TAX_ID,'" + str_EntityNo + "' as EntityNo,'" + str_ItemNo + "' as ItemNo,TT.TAX_NAME,TD.INV_TAX_AMT,INV_TAX_PER ";
            sqlQuery += " FROM TAX_TERMS TT INNER JOIN INV_TAX_DTLS TD ON TD.TAX_ID= TT.TAX_ID ";
            sqlQuery += " WHERE TT.ENABLED_FLAG=1 AND TT.WORKFLOW_COMPLETION_STATUS =1 ";
            sqlQuery += " AND TD.INV_LINE_ID='" + str_Inv_Tax_Dtl_Id + "'";
            sqlQuery += " UNION ";
            sqlQuery += " SELECT '0',TT.TAX_ID,'" + str_EntityNo + "' as EntityNo,'" + str_ItemNo + "' as ItemNo,TT.TAX_NAME,NULL as  INV_TAX_AMT ,NULL as INV_TAX_PER FROM TAX_TERMS TT ";
            sqlQuery += " WHERE TT.ENABLED_FLAG=1 AND TT.WORKFLOW_COMPLETION_STATUS =1 ";
            sqlQuery += " AND TAX_ID NOT IN (SELECT TAX_ID FROM INV_TAX_DTLS WHERE INV_LINE_ID='" + str_Inv_Tax_Dtl_Id + "')";
            sqlQuery += " ORDER BY INV_TAX_AMT ";

            return sqlQuery;
        }

        public static string getInvoice4Perpayment()
        {
            sqlQuery = string.Empty;
            sqlQuery += " SELECT IH.OM_INV_NUM as INV_NUM,IH.OM_INV_ID as INV_ID FROM OM_CUST_INVOICE_HDR  IH WHERE IH.OM_INV_TYPE in ('Standard','PO Invoice','4','9','1','3','5','16','17','2')";
            sqlQuery += " AND IH.Enabled_Flag=1 and IH.WORKFLOW_COMPLETION_STATUS=1 ";
            return sqlQuery;
        }
        public static bool IsPaymentRecordFound(string invId)
        {
            sqlQuery = string.Empty;
            bool payCount = false;
            int counts = 0;

            sqlQuery += "   select count(1) as counts from om_cust_payment_dtl i,om_cust_payment_hdr p";
            sqlQuery += "   where i.cust_pay_id=p.cust_pay_id and p.rev_flag is null and p.rev_date is null and  i.cust_inv_id ='" + invId + "'";

            counts = DBMethod.GetIntValue(sqlQuery);
            if (counts > 0)
            {
                payCount = true;
            }
            else
            {
                payCount = false;
            }
            return payCount;
        }
        public static string getInvoiceNumber()
        {
            sqlQuery = string.Empty;
            sqlQuery += " SELECT IH.om_inv_id as INV_ID,IH.OM_INV_NUM as INV_NUM FROM OM_CUST_INVOICE_HDR  IH WHERE IH.om_inv_type in ('Standard','PO Invoice','4','9','1','3','5','16','17','2') ";
            sqlQuery += " AND IH.Enabled_Flag=1 and IH.WORKFLOW_COMPLETION_STATUS=1 ";
            sqlQuery += " order by INV_NUM";
            return sqlQuery;
        }


        public static string getInvoiceNumber_forCustomer(string OM_VENDOR_ID)
        {
            sqlQuery = string.Empty;
            sqlQuery += " SELECT IH.OM_INV_ID ,IH.OM_INV_NUM  FROM OM_CUST_INVOICE_HDR  IH ";
            sqlQuery += " WHERE IH.Enabled_Flag=1 and IH.WORKFLOW_COMPLETION_STATUS=1 ";
            if (OM_VENDOR_ID != "")
            {
                sqlQuery += " AND IH.OM_VENDOR_ID = '" + OM_VENDOR_ID + "'";
            }
            sqlQuery += " ORDER BY IH.OM_INV_NUM ASC";
            return sqlQuery;
        }

        public static string getInvoiceNumber4CustomerPayemnt(string OM_VENDOR_ID, string str_Curr)
        {
            sqlQuery = string.Empty;
            sqlQuery += " SELECT IH.OM_INV_ID ,IH.OM_INV_NUM  FROM OM_CUST_INVOICE_HDR  IH ";
            sqlQuery += " WHERE IH.Enabled_Flag=1 and IH.WORKFLOW_COMPLETION_STATUS=1 ";
            sqlQuery += " AND IH.OM_VENDOR_ID = '" + OM_VENDOR_ID + "'";
            sqlQuery += " AND IH.OM_INV_ID NOT IN (SELECT CPD.CUST_INV_ID FROM  OM_CUST_PAYMENT_DTL CPD ";
            sqlQuery += " INNER JOIN OM_CUST_INVOICE_HDR CIH ON CIH.OM_INV_ID=CPD.CUST_INV_ID ";
            sqlQuery += " WHERE (CIH.OM_INV_AMT - CPD.CUST_INV_PAID_AMT ) =0 AND CUST_INV_ID IS NOT NULL )";
            if (str_Curr.Length > 0)
            {
                sqlQuery += " AND IH.OM_INV_CURR_CODE='" + str_Curr + "'";
            }
            return sqlQuery;
        }

        public static string getInvoice4Payemnt(string str_supp_id, string str_mode)
        {
            sqlQuery = string.Empty;

            if (str_mode == FINTableConstant.Add)
            {

                sqlQuery = "  select INV_ID, INV_NUM";
                sqlQuery += "   from (select nvl(iih.om_inv_amt, 0) - sum(nvl(IPD.CUST_INV_AMT, 0)) bal_amt,";
                sqlQuery += "   iih.om_inv_id AS INV_ID,";
                sqlQuery += "   iih.om_inv_num AS INV_NUM";
                sqlQuery += "   from OM_CUST_INVOICE_HDR iih,";
                sqlQuery += "  OM_CUST_PAYMENT_HDR iph,";
                sqlQuery += "  OM_CUST_PAYMENT_DTL ipd";
                sqlQuery += "  where iih.om_vendor_id = iph.cust_vendor_id";
                sqlQuery += "  and iph.cust_pay_id = ipd.cust_pay_id";
                sqlQuery += "  and iih.om_inv_id = ipd.cust_inv_id";
                sqlQuery += "  and iih.om_vendor_id='" + str_supp_id + "'";
                sqlQuery += "   and iph.PAY_STOP_DATE is null";
                sqlQuery += "    group by iih.om_inv_amt, iih.om_inv_id, iih.om_inv_num) inv_bal";
                sqlQuery += "  where inv_bal.bal_amt > 0";
                sqlQuery += "  UNION";
                sqlQuery += "  select OM_INV_ID AS INV_ID, OM_INV_NUM AS INV_NUM";
                sqlQuery += "  FROM OM_CUST_INVOICE_HDR";
                sqlQuery += "  where OM_INV_ID not in (select CUST_INV_ID from OM_CUST_PAYMENT_DTL)";
                sqlQuery += "  AND POSTED_FLAG = '1'";
                sqlQuery += "  AND REV_FLAG IS NULL";
            }
            else
            {
                sqlQuery += "  select iih.om_inv_id,iih.om_inv_num from OM_CUST_INVOICE_HDR iih where iih.om_vendor_id='" + str_supp_id + "'";
            }

            return sqlQuery;
        }

        public static string getInvoiceForPayment(string str_supp_id, string str_mode, string currId = "")
        {
            sqlQuery = string.Empty;
            if (str_mode == FINTableConstant.Add)
            {
                sqlQuery += " select OM_INV_ID, OM_INV_NUM";
                sqlQuery += " from (select nvl(iih.om_inv_amt, 0) -";
                sqlQuery += " (sum(nvl(ipd.cust_inv_paid_amt, 0)) + sum(nvl(ipd.pay_retentiion_amt, 0))) bal_amt,";
                sqlQuery += " iih.om_inv_id, iih.om_inv_num";
                sqlQuery += " from om_cust_invoice_hdr iih, om_cust_payment_hdr iph, om_cust_payment_dtl ipd";
                sqlQuery += " where iih.om_vendor_id = iph.cust_vendor_id";
                sqlQuery += " and iph.cust_pay_id = ipd.cust_pay_id";
                sqlQuery += " and iih.om_inv_id = ipd.cust_inv_id";
                sqlQuery += " and iph.rev_flag is null";
                sqlQuery += "  and iih.om_vendor_id='" + str_supp_id + "'";
                if (currId.Length > 0)
                {
                    sqlQuery += "  and iih.om_inv_receipt_curr_code='" + currId + "'";
                }
                sqlQuery += " group by iih.om_inv_amt,iih.om_inv_id,iih.om_inv_num,iih.om_inv_retention_amt) inv_bal ";

                sqlQuery += " where inv_bal.bal_amt > 0 ";
                sqlQuery += "  UNION ";
                sqlQuery += " select  uih.om_inv_id, uih.om_inv_num ";
                sqlQuery += " FROM om_cust_invoice_hdr uih where  not exists (select pd.cust_inv_id from om_cust_payment_dtl pd,om_cust_payment_hdr PH where PH.CUST_PAY_ID=pd.cust_pay_id and ph.rev_flag is null  and pd.cust_inv_id= uih.om_inv_id ) ";
                sqlQuery += " AND uih.om_vendor_id ='" + str_supp_id + "'";
                sqlQuery += " AND uih.posted_flag='1' AND uih.rev_flag IS NULL ";
                if (currId.Length > 0)
                {
                    sqlQuery += "  and uih.om_inv_receipt_curr_code='" + currId + "'";
                }

            }
            else if (str_mode != FINTableConstant.Add)
            {
                sqlQuery += " select iih.om_inv_id,iih.om_inv_num from om_cust_invoice_hdr iih where iih.om_vendor_id='" + str_supp_id + "'";
            }

            return sqlQuery;
        }


        public static string getInvoiceListReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_AR_INVOICE V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
                {
                    sqlQuery += " AND V.OM_INV_DATE >= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy') ";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
                {
                    sqlQuery += " AND V.OM_INV_DATE <= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["To_Date"].ToString() + "','dd/MM/yyyy') ";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.EFFECTIVE_TO_DT] != null)
                {
                    sqlQuery += " AND to_date('" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.EFFECTIVE_TO_DT].ToString() + "','dd/MM/yyyy') )";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["inv_id"] != null)
                {
                    sqlQuery += " AND V.om_inv_id ='" + VMVServices.Web.Utils.ReportFilterParameter["inv_id"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.VENDOR_ID] != null)
                {
                    sqlQuery += " AND V.VENDOR_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.VENDOR_ID].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["FromAmount"] != null)
                {
                    sqlQuery += " AND V.om_inv_amt >= " + VMVServices.Web.Utils.ReportViewFilterParameter["FromAmount"];
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["ToAmount"] != null)
                {
                    sqlQuery += " AND V.om_inv_amt<= " + VMVServices.Web.Utils.ReportViewFilterParameter["ToAmount"];
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["INVTYPE"] != null)
                {
                    sqlQuery += " AND V.OM_INV_TYPE='" + VMVServices.Web.Utils.ReportViewFilterParameter["INVTYPE"] + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["INV_ID"] != null)
                {
                    sqlQuery += " AND V.OM_INV_ID='" + VMVServices.Web.Utils.ReportViewFilterParameter["INV_ID"] + "'";
                }
            }
            return sqlQuery;
        }

        public static string getReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_AR_INVOICE_REGISTER V WHERE ROWNUM > 0 ";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["FROM_CUST_ID"] != null)
                {
                    sqlQuery += " AND V.VENDOR_ID >='" + VMVServices.Web.Utils.ReportFilterParameter["FROM_CUST_ID"].ToString() + "'";
                }

                if (VMVServices.Web.Utils.ReportViewFilterParameter["TO_CUST_ID"] != null)
                {
                    sqlQuery += " AND V.VENDOR_ID <='" + VMVServices.Web.Utils.ReportFilterParameter["TO_CUST_ID"].ToString() + "'";
                }

                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
                {
                    sqlQuery += " AND V.INVOICE_DATE >= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy') ";

                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
                {
                    sqlQuery += " AND V.INVOICE_DATE <= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["To_Date"].ToString() + "','dd/MM/yyyy') ";
                }

                if (VMVServices.Web.Utils.ReportViewFilterParameter["FromAmount"] != null)
                {
                    sqlQuery += " AND V.AMOUNT >=" + VMVServices.Web.Utils.ReportViewFilterParameter["FromAmount"];

                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["ToAmount"] != null)
                {
                    sqlQuery += " AND V.AMOUNT <=" + VMVServices.Web.Utils.ReportViewFilterParameter["ToAmount"];
                }

                if (VMVServices.Web.Utils.ReportFilterParameter["From_InvNum"] != null)
                {
                    sqlQuery += " AND V.OM_INV_NUM >=  '" + VMVServices.Web.Utils.ReportFilterParameter["From_InvNum"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["To_InvNum"] != null)
                {
                    sqlQuery += " AND V.OM_INV_NUM <=  '" + VMVServices.Web.Utils.ReportFilterParameter["To_InvNum"].ToString() + "'";
                }

                if (VMVServices.Web.Utils.ReportFilterParameter["POSTEDTYPE"] == "P")
                {
                    sqlQuery += " AND V.POSTED_FLAG =  '1' and v.rev_flag is null";
                }
                else if (VMVServices.Web.Utils.ReportFilterParameter["POSTEDTYPE"] == "U")
                {
                    sqlQuery += " AND (V.POSTED_FLAG IS NULL OR V.POSTED_FLAG='0') and v.rev_flag is null";
                }
                else if (VMVServices.Web.Utils.ReportFilterParameter["POSTEDTYPE"] == "C")
                {
                    sqlQuery += " AND (V.REV_FLAG='1' and v.POSTED_FLAG in ('1','5'))";
                }

                sqlQuery += "  ORDER BY  V.INVOICE_DATE";
            }
            return sqlQuery;
        }

        public static string getTop5CustomerAmt()
        {
            sqlQuery = string.Empty;

            sqlQuery += " select sc.vendor_name,round(sum(nvl(cph.PAYMENT_AMT,0))," + VMVServices.Web.Utils.DecimalPrecision + ") as payment_amt,sc.vendor_id";
            sqlQuery += " from supplier_customers sc, OM_CUST_PAYMENT_HDR cph";
            sqlQuery += " where sc.vendor_id=cph.cust_vendor_id";
            sqlQuery += " and rownum<6";
            sqlQuery += " group by sc.vendor_name,sc.vendor_id";
            sqlQuery += " order by payment_amt desc";

            return sqlQuery;
        }

        public static string GetInvoiceReceivedAmt()
        {
            sqlQuery = string.Empty;

            sqlQuery += " select sc.vendor_id,";
            sqlQuery += " sc.vendor_name,";
            sqlQuery += " round(sum(nvl(cpd.cust_inv_amt, 0)),'" + VMVServices.Web.Utils.DecimalPrecision + "') as cust_inv_amt,";
            sqlQuery += " round(sum(nvl(ii.inv_amt, 0)), '" + VMVServices.Web.Utils.DecimalPrecision + "') as invoice_amt";
            sqlQuery += " from supplier_customers sc, OM_CUST_PAYMENT_HDR cph, OM_CUST_PAYMENT_DTL cpd,inv_invoices_hdr ii";
            sqlQuery += " where sc.vendor_id = cph.cust_vendor_id";
            sqlQuery += " and cpd.cust_pay_id = cph.cust_pay_id";
            sqlQuery += " and ii.inv_id=cpd.cust_inv_id";
            sqlQuery += " group by sc.vendor_name, sc.vendor_id";
            sqlQuery += " order by cust_inv_amt desc";

            return sqlQuery;
        }


        public static string GetSaleOrderQty(string om_order_id, string om_item_id)
        {
            sqlQuery = string.Empty;


            sqlQuery += " select cod.om_order_qty,cod.om_order_price";
            sqlQuery += " from Om_Customer_Order_Dtl cod";
            sqlQuery += " where cod.workflow_completion_status =1";
            sqlQuery += " and cod.om_order_id ='" + om_order_id + "'";
            sqlQuery += " and cod.om_item_id = '" + om_item_id + "'";
            return sqlQuery;
        }


        public static string GetitemQty_fromINVOICE(string om_order_id, string om_item_id)
        {
            sqlQuery = string.Empty;

            sqlQuery += " select idt.om_item_qty";
            sqlQuery += " from OM_CUST_INVOICE_DTL idt";
            sqlQuery += " where idt.workflow_completion_status =1";
            sqlQuery += " and idt.om_dc_id ='" + om_order_id + "'";
            sqlQuery += " and idt.om_item_id ='" + om_item_id + "'";
            return sqlQuery;
        }

        public static string getDayBookInvoiceReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM vw_ar_invoice_daybook V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                //if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.VENDOR_ID] != null)
                //{
                //    sqlQuery += " AND V.om_vendor_id = '" + VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.VENDOR_ID].ToString() + "'";
                //}
                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
                {
                    sqlQuery += " AND V.om_inv_date between to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy') ";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
                {
                    sqlQuery += " AND to_date('" + VMVServices.Web.Utils.ReportFilterParameter["To_Date"].ToString() + "','dd/MM/yyyy') ";
                }

                if (VMVServices.Web.Utils.ReportViewFilterParameter["SEGMENT_VALUE_ID"] != null)
                {
                    sqlQuery += " AND V.GLOBAL_SEGMENT_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["SEGMENT_VALUE_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["From_InvNum"] != null)
                {
                    sqlQuery += " AND V.OM_INV_NUM >=  '" + VMVServices.Web.Utils.ReportFilterParameter["From_InvNum"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["To_InvNum"] != null)
                {
                    sqlQuery += " AND V.OM_INV_NUM <=  '" + VMVServices.Web.Utils.ReportFilterParameter["To_InvNum"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["FROM_CUSTOMER_ID"] != null)
                {
                    sqlQuery += " AND V.OM_VENDOR_ID >= '" + VMVServices.Web.Utils.ReportViewFilterParameter["FROM_CUSTOMER_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["TO_CUSTOMER_ID"] != null)
                {
                    sqlQuery += " AND V.OM_VENDOR_ID <= '" + VMVServices.Web.Utils.ReportViewFilterParameter["TO_CUSTOMER_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["FromAmount"] != null)
                {
                    sqlQuery += " AND V.OM_INV_AMT >=  '" + VMVServices.Web.Utils.ReportFilterParameter["FromAmount"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["ToAmount"] != null)
                {
                    sqlQuery += " AND V.OM_INV_AMT <=  '" + VMVServices.Web.Utils.ReportFilterParameter["ToAmount"].ToString() + "'";
                }

            }
            return sqlQuery;
        }

        public static string getDayBookRemittanceReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM vw_ca_bankremittance_daybook V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["BANK_ID"] != null)
                {
                    sqlQuery += " AND V.remit_bank_id = '" + VMVServices.Web.Utils.ReportViewFilterParameter["BANK_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["BRANCH_ID"] != null)
                {
                    sqlQuery += " AND V.remit_bank_branch_id = '" + VMVServices.Web.Utils.ReportViewFilterParameter["BRANCH_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["TRANC_ID"] != null)
                {
                    sqlQuery += " AND V.Transc_id = '" + VMVServices.Web.Utils.ReportViewFilterParameter["TRANC_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["FROM_AMOUNT"] != null)
                {
                    sqlQuery += " AND V.remit_trans_amount >= '" + VMVServices.Web.Utils.ReportViewFilterParameter["FROM_AMOUNT"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["TO_AMOUNT"] != null)
                {
                    sqlQuery += " AND V.remit_trans_amount <= '" + VMVServices.Web.Utils.ReportViewFilterParameter["TO_AMOUNT"].ToString() + "'";
                }

                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
                {
                    sqlQuery += " AND V.remit_date between to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy') ";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
                {
                    sqlQuery += " AND to_date('" + VMVServices.Web.Utils.ReportFilterParameter["To_Date"].ToString() + "','dd/MM/yyyy') ";
                }
            }
            return sqlQuery;
        }
        public static string getGrossProfitMarginReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM vw_ar_gross_profit_margin V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["CUST_FROM_ID"] != null)
                {
                    sqlQuery += " AND V.om_vendor_id >= '" + VMVServices.Web.Utils.ReportViewFilterParameter["CUST_FROM_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["CUST_TO_ID"] != null)
                {
                    sqlQuery += " AND V.om_vendor_id <= '" + VMVServices.Web.Utils.ReportViewFilterParameter["CUST_TO_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["Transaction_Type"] != null)
                {
                    sqlQuery += " AND V.INVOICE_TYPE = '" + VMVServices.Web.Utils.ReportViewFilterParameter["Transaction_Type"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["CURRENCY_NAME"] != null)
                {
                    sqlQuery += " AND V.currency_desc = '" + VMVServices.Web.Utils.ReportViewFilterParameter["CURRENCY_NAME"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["INV_NO_FROM_ID"] != null)
                {
                    sqlQuery += " AND V.om_inv_id >=  '" + VMVServices.Web.Utils.ReportFilterParameter["INV_NO_FROM_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["INV_NO_TO_ID"] != null)
                {
                    sqlQuery += " AND V.om_inv_id <=  '" + VMVServices.Web.Utils.ReportFilterParameter["INV_NO_TO_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["TRANS_FROM_AMT"] != null)
                {
                    sqlQuery += " AND V.om_inv_amt >=  '" + VMVServices.Web.Utils.ReportFilterParameter["TRANS_FROM_AMT"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["TRANS_TO_AMT"] != null)
                {
                    sqlQuery += " AND V.om_inv_amt <=  '" + VMVServices.Web.Utils.ReportFilterParameter["TRANS_TO_AMT"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["FN_CURR_FROM"] != null)
                {
                    sqlQuery += " AND V.funcurr >=  '" + VMVServices.Web.Utils.ReportFilterParameter["FN_CURR_FROM"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["FN_CURR_TO"] != null)
                {
                    sqlQuery += " AND V.funcurr <=  '" + VMVServices.Web.Utils.ReportFilterParameter["FN_CURR_TO"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
                {
                    sqlQuery += " AND V.om_inv_date between to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy') ";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
                {
                    sqlQuery += " AND to_date('" + VMVServices.Web.Utils.ReportFilterParameter["To_Date"].ToString() + "','dd/MM/yyyy') ";
                }
            }
            return sqlQuery;
        }


        public static string getDayBookReceiptReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM vw_ar_receipt_daybook V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["CUST_FROM_ID"] != null)
                {
                    sqlQuery += " AND V.cust_vendor_id >= '" + VMVServices.Web.Utils.ReportViewFilterParameter["CUST_FROM_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["CUST_TO_ID"] != null)
                {
                    sqlQuery += " AND V.cust_vendor_id <= '" + VMVServices.Web.Utils.ReportViewFilterParameter["CUST_TO_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["Receipt_Ref"] != null)
                {
                    sqlQuery += " AND V.cust_pay_id = '" + VMVServices.Web.Utils.ReportViewFilterParameter["Receipt_Ref"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["CURRENCY_NAME"] != null)
                {
                    sqlQuery += " AND V.currency_desc = '" + VMVServices.Web.Utils.ReportViewFilterParameter["CURRENCY_NAME"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["FROM_AMT"] != null)
                {
                    sqlQuery += " AND V.payment_amt >=  '" + VMVServices.Web.Utils.ReportFilterParameter["FROM_AMT"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportFilterParameter["TO_AMT"] != null)
                {
                    sqlQuery += " AND V.payment_amt <=  '" + VMVServices.Web.Utils.ReportFilterParameter["TO_AMT"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
                {
                    sqlQuery += " AND V.pay_date between to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy') ";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
                {
                    sqlQuery += " AND to_date('" + VMVServices.Web.Utils.ReportFilterParameter["To_Date"].ToString() + "','dd/MM/yyyy') ";
                }
            }
            return sqlQuery;
        }

        public static string getInvStoresLedgerReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM vw_stores_ledger V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["ITEM_ID"] != null)
                {
                    sqlQuery += " AND V.INV_ITEM_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["ITEM_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["WH_ID"] != null)
                {
                    sqlQuery += " AND V.INV_WH_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["WH_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
                {
                    sqlQuery += " AND V.INV_TRANS_DATE between to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy') ";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
                {
                    sqlQuery += " AND to_date('" + VMVServices.Web.Utils.ReportFilterParameter["To_Date"].ToString() + "','dd/MM/yyyy') ";
                }
            }
            return sqlQuery;
        }


        public static string getSierraCustomerInvoice()
        {
            sqlQuery = string.Empty;

            sqlQuery = " select distinct v.Customer_Name, v.Customer_ID || ' - ' || v.Customer_Name as Cust_Name, sc.description";
            sqlQuery += " ,t.Invoice_ID, t.Invoice_Number, t.Invoice_Date, t.Customer_ID, to_char(t.Invoice_Amount) as Invoice_Amount";
            sqlQuery += " ,(case t.POSTED when '1' then 'TRUE' ELSE 'FALSE' END) AS POSTED";
            sqlQuery += " ,(select ie.error_message from gl_intf_errors ie where ie.error_id = t.INVOICE_ID and rownum=1) as error_message ";
            sqlQuery += " from DM_SR_CUST_INVOICE@CRCINTERFACE t, DM_SR_CUSTOMER@CRCINTERFACE v, ssm_code_masters sc";
            sqlQuery += " where v.Customer_ID = t.Customer_ID";
            sqlQuery += " and (t.posted is null or t.posted = '0')";
            sqlQuery += " and sc.code='Miscellaneous' and sc.parent_code='LINETYPE'";
            sqlQuery += " order by t.Invoice_Number asc";

            return sqlQuery;
        }
    }
}

