﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.AR
{
    public class AdvancedRefund_DAL
    {
        static string sqlQuery = "";

        public static string getAdvancedRefundOtherCharges(string str_ID)
        {
            sqlQuery = string.Empty;
            sqlQuery += "SELECT AROC.AROC_ID, AROC.REFUND_ID, AROC.Attribute1 as ACCT_CODE_ID, AROC.Attribute2 as AddSub_value,";
            sqlQuery += " to_char(ROUND(AROC.AROC_AMOUNT," + VMVServices.Web.Utils.DecimalPrecision + ")) as AROC_AMOUNT, ";
            sqlQuery += " AROC.REMARKS, GAC.ACCT_CODE ||' - '|| GAC.ACCT_CODE_DESC" + VMVServices.Web.Utils.LanguageCode + " as ACCT_CODE_DESC,'N' as DELETED ";
            sqlQuery += " FROM ADVANCED_REFUND_OTHER_CHARGES AROC";
            sqlQuery += " INNER JOIN GL_ACCT_CODES GAC ON GAC.ACCT_CODE_ID= AROC.ATTRIBUTE1 ";
            sqlQuery += " where AROC.REFUND_ID='" + str_ID + "'";
            return sqlQuery;
        }

        public static string getAdvancedRefundData(string strId)
        {
            sqlQuery = string.Empty;
            sqlQuery += " select ar.refund_id, ar.refund_amt, ar.invoice_number, ar.pay_vendor_id";
            sqlQuery += " ,ih.inv_amt, nvl((ih.inv_amt - ar.refund_amt),0) as balance_amt";
            sqlQuery += " from advanced_refund ar, inv_invoices_hdr ih";
            sqlQuery += " where ar.invoice_number = ih.inv_id";
            sqlQuery += " and ar.pay_vendor_id = ih.vendor_id";
            sqlQuery += " and ar.invoice_number = '" + strId + "'";

            return sqlQuery;
        }
    }
}
