//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.Serialization;

namespace FIN.DAL
{
    [DataContract(IsReference = true)]
    [KnownType(typeof(HR_FACILITY_DTLS))]
    [KnownType(typeof(HR_TRM_SUBJECT))]
    public partial class HR_FACILITY_HDR: IObjectWithChangeTracker, INotifyPropertyChanged
    {
        #region Primitive Properties
    
        [DataMember]
        public Nullable<int> PK_ID
        {
            get { return _pK_ID; }
            set
            {
                if (_pK_ID != value)
                {
                    _pK_ID = value;
                    OnPropertyChanged("PK_ID");
                }
            }
        }
        private Nullable<int> _pK_ID;
    
        [DataMember]
        public Nullable<int> CHILD_ID
        {
            get { return _cHILD_ID; }
            set
            {
                if (_cHILD_ID != value)
                {
                    _cHILD_ID = value;
                    OnPropertyChanged("CHILD_ID");
                }
            }
        }
        private Nullable<int> _cHILD_ID;
    
        [DataMember]
        public string FACILITY_MASTER_ID
        {
            get { return _fACILITY_MASTER_ID; }
            set
            {
                if (_fACILITY_MASTER_ID != value)
                {
                    if (ChangeTracker.ChangeTrackingEnabled && ChangeTracker.State != ObjectState.Added)
                    {
                        throw new InvalidOperationException("The property 'FACILITY_MASTER_ID' is part of the object's key and cannot be changed. Changes to key properties can only be made when the object is not being tracked or is in the Added state.");
                    }
                    _fACILITY_MASTER_ID = value;
                    OnPropertyChanged("FACILITY_MASTER_ID");
                }
            }
        }
        private string _fACILITY_MASTER_ID;
    
        [DataMember]
        public Nullable<int> ROOM_NUMBER
        {
            get { return _rOOM_NUMBER; }
            set
            {
                if (_rOOM_NUMBER != value)
                {
                    _rOOM_NUMBER = value;
                    OnPropertyChanged("ROOM_NUMBER");
                }
            }
        }
        private Nullable<int> _rOOM_NUMBER;
    
        [DataMember]
        public string ROOM_NAME
        {
            get { return _rOOM_NAME; }
            set
            {
                if (_rOOM_NAME != value)
                {
                    _rOOM_NAME = value;
                    OnPropertyChanged("ROOM_NAME");
                }
            }
        }
        private string _rOOM_NAME;
    
        [DataMember]
        public Nullable<int> CAPACITY
        {
            get { return _cAPACITY; }
            set
            {
                if (_cAPACITY != value)
                {
                    _cAPACITY = value;
                    OnPropertyChanged("CAPACITY");
                }
            }
        }
        private Nullable<int> _cAPACITY;
    
        [DataMember]
        public string BUILDING
        {
            get { return _bUILDING; }
            set
            {
                if (_bUILDING != value)
                {
                    _bUILDING = value;
                    OnPropertyChanged("BUILDING");
                }
            }
        }
        private string _bUILDING;
    
        [DataMember]
        public string FLOOR
        {
            get { return _fLOOR; }
            set
            {
                if (_fLOOR != value)
                {
                    _fLOOR = value;
                    OnPropertyChanged("FLOOR");
                }
            }
        }
        private string _fLOOR;
    
        [DataMember]
        public string CAMPUS
        {
            get { return _cAMPUS; }
            set
            {
                if (_cAMPUS != value)
                {
                    _cAMPUS = value;
                    OnPropertyChanged("CAMPUS");
                }
            }
        }
        private string _cAMPUS;
    
        [DataMember]
        public string ENABLED_FLAG
        {
            get { return _eNABLED_FLAG; }
            set
            {
                if (_eNABLED_FLAG != value)
                {
                    _eNABLED_FLAG = value;
                    OnPropertyChanged("ENABLED_FLAG");
                }
            }
        }
        private string _eNABLED_FLAG;
    
        [DataMember]
        public string WORKFLOW_COMPLETION_STATUS
        {
            get { return _wORKFLOW_COMPLETION_STATUS; }
            set
            {
                if (_wORKFLOW_COMPLETION_STATUS != value)
                {
                    _wORKFLOW_COMPLETION_STATUS = value;
                    OnPropertyChanged("WORKFLOW_COMPLETION_STATUS");
                }
            }
        }
        private string _wORKFLOW_COMPLETION_STATUS;
    
        [DataMember]
        public string CREATE_BY
        {
            get { return _cREATE_BY; }
            set
            {
                if (_cREATE_BY != value)
                {
                    _cREATE_BY = value;
                    OnPropertyChanged("CREATE_BY");
                }
            }
        }
        private string _cREATE_BY;
    
        [DataMember]
        public System.DateTime CREATED_DATE
        {
            get { return _cREATED_DATE; }
            set
            {
                if (_cREATED_DATE != value)
                {
                    _cREATED_DATE = value;
                    OnPropertyChanged("CREATED_DATE");
                }
            }
        }
        private System.DateTime _cREATED_DATE;
    
        [DataMember]
        public string MODIFIED_BY
        {
            get { return _mODIFIED_BY; }
            set
            {
                if (_mODIFIED_BY != value)
                {
                    _mODIFIED_BY = value;
                    OnPropertyChanged("MODIFIED_BY");
                }
            }
        }
        private string _mODIFIED_BY;
    
        [DataMember]
        public Nullable<System.DateTime> MODIFIED_DATE
        {
            get { return _mODIFIED_DATE; }
            set
            {
                if (_mODIFIED_DATE != value)
                {
                    _mODIFIED_DATE = value;
                    OnPropertyChanged("MODIFIED_DATE");
                }
            }
        }
        private Nullable<System.DateTime> _mODIFIED_DATE;
    
        [DataMember]
        public string ATTRIBUTE1
        {
            get { return _aTTRIBUTE1; }
            set
            {
                if (_aTTRIBUTE1 != value)
                {
                    _aTTRIBUTE1 = value;
                    OnPropertyChanged("ATTRIBUTE1");
                }
            }
        }
        private string _aTTRIBUTE1;
    
        [DataMember]
        public string ATTRIBUTE2
        {
            get { return _aTTRIBUTE2; }
            set
            {
                if (_aTTRIBUTE2 != value)
                {
                    _aTTRIBUTE2 = value;
                    OnPropertyChanged("ATTRIBUTE2");
                }
            }
        }
        private string _aTTRIBUTE2;
    
        [DataMember]
        public string ATTRIBUTE3
        {
            get { return _aTTRIBUTE3; }
            set
            {
                if (_aTTRIBUTE3 != value)
                {
                    _aTTRIBUTE3 = value;
                    OnPropertyChanged("ATTRIBUTE3");
                }
            }
        }
        private string _aTTRIBUTE3;
    
        [DataMember]
        public string ATTRIBUTE4
        {
            get { return _aTTRIBUTE4; }
            set
            {
                if (_aTTRIBUTE4 != value)
                {
                    _aTTRIBUTE4 = value;
                    OnPropertyChanged("ATTRIBUTE4");
                }
            }
        }
        private string _aTTRIBUTE4;
    
        [DataMember]
        public string ATTRIBUTE5
        {
            get { return _aTTRIBUTE5; }
            set
            {
                if (_aTTRIBUTE5 != value)
                {
                    _aTTRIBUTE5 = value;
                    OnPropertyChanged("ATTRIBUTE5");
                }
            }
        }
        private string _aTTRIBUTE5;
    
        [DataMember]
        public string ATTRIBUTE6
        {
            get { return _aTTRIBUTE6; }
            set
            {
                if (_aTTRIBUTE6 != value)
                {
                    _aTTRIBUTE6 = value;
                    OnPropertyChanged("ATTRIBUTE6");
                }
            }
        }
        private string _aTTRIBUTE6;
    
        [DataMember]
        public string ATTRIBUTE7
        {
            get { return _aTTRIBUTE7; }
            set
            {
                if (_aTTRIBUTE7 != value)
                {
                    _aTTRIBUTE7 = value;
                    OnPropertyChanged("ATTRIBUTE7");
                }
            }
        }
        private string _aTTRIBUTE7;
    
        [DataMember]
        public string ATTRIBUTE8
        {
            get { return _aTTRIBUTE8; }
            set
            {
                if (_aTTRIBUTE8 != value)
                {
                    _aTTRIBUTE8 = value;
                    OnPropertyChanged("ATTRIBUTE8");
                }
            }
        }
        private string _aTTRIBUTE8;
    
        [DataMember]
        public string ATTRIBUTE9
        {
            get { return _aTTRIBUTE9; }
            set
            {
                if (_aTTRIBUTE9 != value)
                {
                    _aTTRIBUTE9 = value;
                    OnPropertyChanged("ATTRIBUTE9");
                }
            }
        }
        private string _aTTRIBUTE9;
    
        [DataMember]
        public string ATTRIBUTE10
        {
            get { return _aTTRIBUTE10; }
            set
            {
                if (_aTTRIBUTE10 != value)
                {
                    _aTTRIBUTE10 = value;
                    OnPropertyChanged("ATTRIBUTE10");
                }
            }
        }
        private string _aTTRIBUTE10;

        #endregion
        #region Navigation Properties
    
        [DataMember]
        public TrackableCollection<HR_FACILITY_DTLS> HR_FACILITY_DTLS
        {
            get
            {
                if (_hR_FACILITY_DTLS == null)
                {
                    _hR_FACILITY_DTLS = new TrackableCollection<HR_FACILITY_DTLS>();
                    _hR_FACILITY_DTLS.CollectionChanged += FixupHR_FACILITY_DTLS;
                }
                return _hR_FACILITY_DTLS;
            }
            set
            {
                if (!ReferenceEquals(_hR_FACILITY_DTLS, value))
                {
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        throw new InvalidOperationException("Cannot set the FixupChangeTrackingCollection when ChangeTracking is enabled");
                    }
                    if (_hR_FACILITY_DTLS != null)
                    {
                        _hR_FACILITY_DTLS.CollectionChanged -= FixupHR_FACILITY_DTLS;
                    }
                    _hR_FACILITY_DTLS = value;
                    if (_hR_FACILITY_DTLS != null)
                    {
                        _hR_FACILITY_DTLS.CollectionChanged += FixupHR_FACILITY_DTLS;
                    }
                    OnNavigationPropertyChanged("HR_FACILITY_DTLS");
                }
            }
        }
        private TrackableCollection<HR_FACILITY_DTLS> _hR_FACILITY_DTLS;
    
        [DataMember]
        public TrackableCollection<HR_TRM_SUBJECT> HR_TRM_SUBJECT
        {
            get
            {
                if (_hR_TRM_SUBJECT == null)
                {
                    _hR_TRM_SUBJECT = new TrackableCollection<HR_TRM_SUBJECT>();
                    _hR_TRM_SUBJECT.CollectionChanged += FixupHR_TRM_SUBJECT;
                }
                return _hR_TRM_SUBJECT;
            }
            set
            {
                if (!ReferenceEquals(_hR_TRM_SUBJECT, value))
                {
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        throw new InvalidOperationException("Cannot set the FixupChangeTrackingCollection when ChangeTracking is enabled");
                    }
                    if (_hR_TRM_SUBJECT != null)
                    {
                        _hR_TRM_SUBJECT.CollectionChanged -= FixupHR_TRM_SUBJECT;
                    }
                    _hR_TRM_SUBJECT = value;
                    if (_hR_TRM_SUBJECT != null)
                    {
                        _hR_TRM_SUBJECT.CollectionChanged += FixupHR_TRM_SUBJECT;
                    }
                    OnNavigationPropertyChanged("HR_TRM_SUBJECT");
                }
            }
        }
        private TrackableCollection<HR_TRM_SUBJECT> _hR_TRM_SUBJECT;

        #endregion
        #region ChangeTracking
    
        protected virtual void OnPropertyChanged(String propertyName)
        {
            if (ChangeTracker.State != ObjectState.Added && ChangeTracker.State != ObjectState.Deleted)
            {
                ChangeTracker.State = ObjectState.Modified;
            }
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        protected virtual void OnNavigationPropertyChanged(String propertyName)
        {
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        event PropertyChangedEventHandler INotifyPropertyChanged.PropertyChanged{ add { _propertyChanged += value; } remove { _propertyChanged -= value; } }
        private event PropertyChangedEventHandler _propertyChanged;
        private ObjectChangeTracker _changeTracker;
    
        [DataMember]
        public ObjectChangeTracker ChangeTracker
        {
            get
            {
                if (_changeTracker == null)
                {
                    _changeTracker = new ObjectChangeTracker();
                    _changeTracker.ObjectStateChanging += HandleObjectStateChanging;
                }
                return _changeTracker;
            }
            set
            {
                if(_changeTracker != null)
                {
                    _changeTracker.ObjectStateChanging -= HandleObjectStateChanging;
                }
                _changeTracker = value;
                if(_changeTracker != null)
                {
                    _changeTracker.ObjectStateChanging += HandleObjectStateChanging;
                }
            }
        }
    
        private void HandleObjectStateChanging(object sender, ObjectStateChangingEventArgs e)
        {
            if (e.NewState == ObjectState.Deleted)
            {
                ClearNavigationProperties();
            }
        }
    
        protected bool IsDeserializing { get; private set; }
    
        [OnDeserializing]
        public void OnDeserializingMethod(StreamingContext context)
        {
            IsDeserializing = true;
        }
    
        [OnDeserialized]
        public void OnDeserializedMethod(StreamingContext context)
        {
            IsDeserializing = false;
            ChangeTracker.ChangeTrackingEnabled = true;
        }
    
        protected virtual void ClearNavigationProperties()
        {
            HR_FACILITY_DTLS.Clear();
            HR_TRM_SUBJECT.Clear();
        }

        #endregion
        #region Association Fixup
    
        private void FixupHR_FACILITY_DTLS(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (e.NewItems != null)
            {
                foreach (HR_FACILITY_DTLS item in e.NewItems)
                {
                    item.HR_FACILITY_HDR = this;
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        if (!item.ChangeTracker.ChangeTrackingEnabled)
                        {
                            item.StartTracking();
                        }
                        ChangeTracker.RecordAdditionToCollectionProperties("HR_FACILITY_DTLS", item);
                    }
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (HR_FACILITY_DTLS item in e.OldItems)
                {
                    if (ReferenceEquals(item.HR_FACILITY_HDR, this))
                    {
                        item.HR_FACILITY_HDR = null;
                    }
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        ChangeTracker.RecordRemovalFromCollectionProperties("HR_FACILITY_DTLS", item);
                    }
                }
            }
        }
    
        private void FixupHR_TRM_SUBJECT(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (e.NewItems != null)
            {
                foreach (HR_TRM_SUBJECT item in e.NewItems)
                {
                    item.HR_FACILITY_HDR = this;
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        if (!item.ChangeTracker.ChangeTrackingEnabled)
                        {
                            item.StartTracking();
                        }
                        ChangeTracker.RecordAdditionToCollectionProperties("HR_TRM_SUBJECT", item);
                    }
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (HR_TRM_SUBJECT item in e.OldItems)
                {
                    if (ReferenceEquals(item.HR_FACILITY_HDR, this))
                    {
                        item.HR_FACILITY_HDR = null;
                    }
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        ChangeTracker.RecordRemovalFromCollectionProperties("HR_TRM_SUBJECT", item);
                    }
                }
            }
        }

        #endregion
    }
}
