﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;

using System.Data.EntityClient;



using System.Data.Entity.Infrastructure;

namespace FIN.DAL
{
    public static class FINSP
    {
        static string spOutPut = string.Empty;
        public static System.Collections.SortedList ErrorCollection = new System.Collections.SortedList();

        public static DataTable get_RecordExportData(int menu_pk_id, string str_pk_value)
        {
            string strValue = string.Empty;
            string strNextCode = string.Empty;
            string strErrMsg = string.Empty;

            OracleCommand oraCmd = new OracleCommand();
            oraCmd.CommandText = "PROC_EXPORT_DATA";
            oraCmd.CommandType = CommandType.StoredProcedure;
            oraCmd.Parameters.Add("@P_MENU_PK_ID", OracleDbType.Int16 ).Value = menu_pk_id;
            oraCmd.Parameters.Add("@P_PK_VALUE", OracleDbType.Varchar2).Value = str_pk_value;
            oraCmd.Parameters.Add("@P_LNG_CODE", OracleDbType.Varchar2).Value = VMVServices.Web.Utils.LanguageCode;  
            
            oraCmd.Parameters.Add("@P_DATA", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            return DBMethod.ExecuteStoredProcedure4CursorOutput(oraCmd);
        }



        public static string[] GetSPFOR_AboutThisRec(string Screen_Code, string PK_Value)
        {
            try
            {

                string[] str_AboutRec = new string[7];


                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "Workflow_Handler.ABOUT_THIS_RECORD";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add("@P_SCREEN_CODE", OracleDbType.Varchar2).Value = Screen_Code;
                oraCmd.Parameters.Add("@P_Transaction_Code", OracleDbType.Varchar2).Value = PK_Value;
                oraCmd.Parameters.Add("@P_ABT_INFO_1", OracleDbType.Varchar2, 150);
                oraCmd.Parameters.Add("@P_ABT_INFO_2", OracleDbType.Varchar2, 150);
                oraCmd.Parameters.Add("@P_ABT_INFO_3", OracleDbType.Varchar2, 150);
                oraCmd.Parameters.Add("@P_ABT_INFO_4", OracleDbType.Varchar2, 150);
                oraCmd.Parameters.Add("@P_ABT_INFO_5", OracleDbType.Varchar2, 150);
                oraCmd.Parameters.Add("@P_ABT_INFO_6", OracleDbType.Varchar2, 150);
                oraCmd.Parameters.Add("@P_ABT_INFO_7", OracleDbType.Varchar2, 150);

                oraCmd.Parameters["@P_ABT_INFO_1"].Direction = ParameterDirection.Output;
                oraCmd.Parameters["@P_ABT_INFO_2"].Direction = ParameterDirection.Output;
                oraCmd.Parameters["@P_ABT_INFO_3"].Direction = ParameterDirection.Output;
                oraCmd.Parameters["@P_ABT_INFO_4"].Direction = ParameterDirection.Output;
                oraCmd.Parameters["@P_ABT_INFO_5"].Direction = ParameterDirection.Output;
                oraCmd.Parameters["@P_ABT_INFO_6"].Direction = ParameterDirection.Output;
                oraCmd.Parameters["@P_ABT_INFO_7"].Direction = ParameterDirection.Output;

                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

                str_AboutRec[0] = oraCmd.Parameters["@P_ABT_INFO_1"].Value.ToString();
                str_AboutRec[1] = oraCmd.Parameters["@P_ABT_INFO_2"].Value.ToString();
                str_AboutRec[2] = oraCmd.Parameters["@P_ABT_INFO_3"].Value.ToString();
                str_AboutRec[3] = oraCmd.Parameters["@P_ABT_INFO_4"].Value.ToString();
                str_AboutRec[4] = oraCmd.Parameters["@P_ABT_INFO_5"].Value.ToString();
                str_AboutRec[5] = oraCmd.Parameters["@P_ABT_INFO_6"].Value.ToString();
                str_AboutRec[6] = oraCmd.Parameters["@P_ABT_INFO_7"].Value.ToString();

                return str_AboutRec;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public static string GetSPFOR_SEQCode(string sequence_code, bool IsValidation = false, bool IsSaved = false)
        {
            try
            {
                string strValue = string.Empty;
                string strNextCode = string.Empty;
                string strErrMsg = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "Ssm.get_next_sequence";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add("@screencode", OracleDbType.Char).Value = sequence_code;
                oraCmd.Parameters.Add("@ERRMESG", OracleDbType.Varchar2, 150);
                oraCmd.Parameters.Add("@TXNCODE", OracleDbType.Varchar2, 150);
                oraCmd.Parameters["@ERRMESG"].Direction = ParameterDirection.Output;
                oraCmd.Parameters["@TXNCODE"].Direction = ParameterDirection.Output;
                //oraCmd.Parameters.Add("@ErrorCode", OracleDbType.Varchar2, 50);
                //oraCmd.Parameters["@ErrorCode"].Direction = ParameterDirection.Output;
                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

                strNextCode = oraCmd.Parameters["@TXNCODE"].Value.ToString();


                return strNextCode;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }




        public static string SP_POSTING_ACCTCODE_VALIDATION(string str_formid, string str_supp_cust_id,string str_PID)
        {
            try
            {
               
                string str_outputmsg = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "GL_POSTING.POSTING_ACCT_VALIDATION";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add("@P_FORM", OracleDbType.Varchar2, 150).Value = str_formid;
                oraCmd.Parameters.Add("@P_SUPP_CUST_ID", OracleDbType.Varchar2, 150).Value = str_supp_cust_id;
                oraCmd.Parameters.Add("@P_ID", OracleDbType.Varchar2, 150).Value = str_PID;
                oraCmd.Parameters.Add("@P_ORG_ID", OracleDbType.Varchar2, 150).Value = VMVServices.Web.Utils.OrganizationID ;         
                oraCmd.Parameters.Add("@P_OUTPUT", OracleDbType.Varchar2, 1000);               
                oraCmd.Parameters["@P_OUTPUT"].Direction = ParameterDirection.Output;         
                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

                str_outputmsg = oraCmd.Parameters["@P_OUTPUT"].Value.ToString();


                return str_outputmsg;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static void GetSPFOR_ItemTransfer(string transDate, string fromWhId, string toWhId, decimal quantity, DataTable dataTable)
        {
            try
            {
                string strValue = string.Empty;
                string strNextCode = string.Empty;
                string strErrMsg = string.Empty;

                OracleCommand oraCmd = new OracleCommand();

                oraCmd.CommandText = "inv_pkg.inv_item_transfer";
                oraCmd.CommandType = CommandType.StoredProcedure;

                oraCmd.Parameters.Add("@p_trans_dt", OracleDbType.Char).Value = transDate;
                oraCmd.Parameters.Add("@p_from_wh_id", OracleDbType.Varchar2, 150).Value = fromWhId;
                oraCmd.Parameters.Add("@p_to_wh_id", OracleDbType.Varchar2, 150).Value = toWhId;
                oraCmd.Parameters.Add("@p_item_id", OracleDbType.Varchar2, 150).Value = DBNull.Value;
                oraCmd.Parameters.Add("@p_item_qty", OracleDbType.Int32, 150).Value = quantity;
                oraCmd.Parameters.Add("@p_from_acc_code", OracleDbType.Varchar2, 250).Value = DBNull.Value;
                oraCmd.Parameters.Add("@p_to_acc_code", OracleDbType.Varchar2, 250).Value = DBNull.Value;
                oraCmd.Parameters.Add("@p_receipt_id", OracleDbType.Varchar2, 250).Value = DBNull.Value;

                OracleParameter i_param_string_type = new OracleParameter("@p_lot_dtls", OracleDbType.Varchar2);
                //i_param_string_type.OracleDbType = OracleDbType.Varchar2;
                // i_param_string_type.Direction = ParameterDirection.Input;
                i_param_string_type.CollectionType = OracleCollectionType.PLSQLAssociativeArray;

                OracleString[] stringType;

                if (dataTable != null && dataTable.Rows.Count > 0)
                {
                    stringType = new OracleString[dataTable.Rows.Count];
                    foreach (DataRow drdataTableRow2 in dataTable.Rows)
                    {
                        stringType[dataTable.Rows.IndexOf(drdataTableRow2)] = (drdataTableRow2["LOT_ID"].ToString());
                        stringType[dataTable.Rows.IndexOf(drdataTableRow2)] = (drdataTableRow2["INV_ITEM_QTY"].ToString());
                        stringType[dataTable.Rows.IndexOf(drdataTableRow2)] = (drdataTableRow2["item_id"].ToString());
                        stringType[dataTable.Rows.IndexOf(drdataTableRow2)] = (drdataTableRow2["code_id"].ToString());
                    }
                }
                else
                {
                    stringType = new OracleString[4] { OracleString.Null, OracleString.Null, OracleString.Null, OracleString.Null };
                }

                i_param_string_type.Value = stringType;
                oraCmd.Parameters.Add(i_param_string_type);
                // oraCmd.Parameters.Add("@p_lot_dtls", OracleDbType.) = i_param_string_type;


                // OracleParameter prm1 = new OracleParameter("form_trn_name", OracleDbType.Varchar2);
                //// prm1.Direction = ParameterDirection.Input;
                //prm1.Value = form_trn_name;
                //oraCmd.Parameters.Add(prm1);



                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GetFN_Sequence_GL(string str_ScrenCode, string str_Period_id, string str_Module_Code)
        {
            try
            {
                //    string strQuery = string.Empty;

                //    DataTable dtData = new DataTable();

                //    strQuery = DBMethod.GetStringValue("select ssm.get_next_sequence_gl('" + str_ScrenCode + "','EN','" + str_Period_id + "','" + str_Module_Code + "','" + VMVServices.Web.Utils.OrganizationID + "') from dual");



                string strValue = string.Empty;
                string strNextCode = string.Empty;
                string strErrMsg = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "Ssm.get_next_sequence_gl";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add("@screencode", OracleDbType.Varchar2, 150).Value = str_ScrenCode;
                oraCmd.Parameters.Add("@lang", OracleDbType.Varchar2, 150).Value = "EN";
                oraCmd.Parameters.Add("@P_PERIOD_ID", OracleDbType.Varchar2, 150).Value = str_Period_id;
                oraCmd.Parameters.Add("@P_MODULE", OracleDbType.Varchar2, 150).Value = str_Module_Code;
                oraCmd.Parameters.Add("@P_ORG_ID", OracleDbType.Varchar2, 150).Value = VMVServices.Web.Utils.OrganizationID;
                oraCmd.Parameters.Add("@P_SEQ_CODE", OracleDbType.Varchar2, 150);

                oraCmd.Parameters["@P_SEQ_CODE"].Direction = ParameterDirection.Output;

                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

                strNextCode = oraCmd.Parameters["@P_SEQ_CODE"].Value.ToString();


                return strNextCode;



            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GetSPFOR_Listscreen(string str_ScrenCode, bool IsValidation, bool IsSaved)
        {
            try
            {
                string strQuery = string.Empty;

                DataTable dtData = new DataTable();

                strQuery = DBMethod.GetStringValue("select ssm.GET_LIST_QUERY('" + str_ScrenCode + "','" + VMVServices.Web.Utils.OrganizationID + "') from dual");

                //  strQuery = DBMethod.GetStringValue("select ssm.GET_REP_LIST_QUERY('" + str_ScrenCode + "','" + VMVServices.Web.Utils.OrganizationID + "') from dual");
                //  dtData = DBMethod.ExecuteQuery(strQuery).Tables[0];

                return strQuery;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GetSPFOR_FiscalYear(string p_year)
        {
            try
            {
                string strQuery = string.Empty;

                DataTable dtData = new DataTable();
                //DateTime dt_tmp = Convert.ToDateTime(p_year);
                //dt_tmp.ToString("dd/MMM/yyyy");


                strQuery = DBMethod.GetStringValue("select PKG_PAYROLL.get_fiscal_year('" + VMVServices.Web.Utils.OrganizationID + "',to_date('" + p_year + "','dd/MM/yyyy')) from dual");

                //  strQuery = DBMethod.GetStringValue("select ssm.GET_REP_LIST_QUERY('" + str_ScrenCode + "','" + VMVServices.Web.Utils.OrganizationID + "') from dual");
                //  dtData = DBMethod.ExecuteQuery(strQuery).Tables[0];

                return strQuery;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GetSPFOR_PAY_ELE_CAL(string str_dept, string desig_id, string formul)
        {
            try
            {
                string strQuery = string.Empty;

                DataTable dtData = new DataTable();

                strQuery = DBMethod.GetStringValue("select PKG_PAYROLL.GET_PAY_ELEMENT_CALC('" + VMVServices.Web.Utils.OrganizationID + "','" + str_dept + "','" + desig_id + "','" + formul + "') from dual");

                //  strQuery = DBMethod.GetStringValue("select ssm.GET_REP_LIST_QUERY('" + str_ScrenCode + "','" + VMVServices.Web.Utils.OrganizationID + "') from dual");
                //  dtData = DBMethod.ExecuteQuery(strQuery).Tables[0];

                return strQuery;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static string GetSPFOR_REPListscreen(string str_ScrenCode, bool IsValidation, bool IsSaved)
        {
            try
            {
                string strQuery = string.Empty;

                DataTable dtData = new DataTable();

                strQuery = DBMethod.GetStringValue("select ssm.GET_REP_LIST_QUERY('" + str_ScrenCode + "','" + VMVServices.Web.Utils.OrganizationID + "') from dual");


                //  dtData = DBMethod.ExecuteQuery(strQuery).Tables[0];

                return strQuery;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GetSPFOR_Filter_Listscreen(string str_ScrenCode, string str_ColName, string str_ColValue)
        {
            try
            {
                string strQuery = string.Empty;

                DataTable dtData = new DataTable();

                strQuery = DBMethod.GetStringValue("select ssm.GET_FILTER_LIST_QUERY('" + str_ScrenCode + "','" + VMVServices.Web.Utils.OrganizationID + "','" + str_ColName + "','" + str_ColValue + "') from dual");

                //  dtData = DBMethod.ExecuteQuery(strQuery).Tables[0];

                return strQuery;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GetSPFOR_Filter_MULTICOL_Listscreen(string str_ScrenCode, string str_KEYVALUE)
        {
            try
            {
                string strQuery = string.Empty;

                DataTable dtData = new DataTable();

                strQuery = DBMethod.GetStringValue("select ssm.GET_FILTER_MULTICOL_LIST_QUERY('" + str_ScrenCode + "','" + VMVServices.Web.Utils.OrganizationID + "','" + str_KEYVALUE + "') from dual");

                //  dtData = DBMethod.ExecuteQuery(strQuery).Tables[0];

                return strQuery;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GetWorkFlowStatusEmp(string form_trn_name, string login_user, string p_key, string empId)
        {
            try
            {
                VMVServices.Web.Utils.SavedRecordId = p_key;
                string strValue = string.Empty;
                string strNextCode = string.Empty;
                string strErrMsg = string.Empty;


                FINEntities context = new FINEntities();
                EntityConnection conn = (EntityConnection)context.Connection;
                OracleConnection oraConn = (OracleConnection)conn.StoreConnection;
                oraConn.Open();


                OracleCommand oraCmd = new OracleCommand("Workflow_Handler.get_workflow_status", oraConn);

                oraCmd.CommandText = "Workflow_Handler.get_workflow_status";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.BindByName = true;

                OracleParameter prm1 = new OracleParameter("form_trn_name", OracleDbType.Varchar2);
                // prm1.Direction = ParameterDirection.Input;
                prm1.Value = form_trn_name;
                oraCmd.Parameters.Add(prm1);

                OracleParameter prm2 = new OracleParameter("login_user", OracleDbType.Varchar2);
                //  prm2.Direction = ParameterDirection.Input;
                prm2.Value = login_user;
                oraCmd.Parameters.Add(prm2);

                OracleParameter prm3 = new OracleParameter("p_key", OracleDbType.Varchar2);
                //  prm3.Direction = ParameterDirection.Input;
                prm3.Value = p_key;
                oraCmd.Parameters.Add(prm3);

                OracleParameter prm4 = new OracleParameter("p_org_id", OracleDbType.Varchar2);
                //  prm4.Direction = ParameterDirection.Input;
                prm4.Value = VMVServices.Web.Utils.OrganizationID;
                oraCmd.Parameters.Add(prm4);


                OracleParameter prm5 = new OracleParameter("P_emp_id", OracleDbType.Varchar2);
                prm4.Value = empId;
                oraCmd.Parameters.Add(prm5);

                oraCmd.Parameters.Add("wf_status", OracleDbType.Varchar2, 2).Direction = ParameterDirection.ReturnValue;

                oraCmd.ExecuteNonQuery();

                strValue = oraCmd.Parameters["wf_status"].Value.ToString();

                conn.Close();

                return strValue;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool Is_workflow_defined(string formId)
        {
            string sqlQuery = string.Empty;
            bool wfCount = false;
            int counts = 0;

            sqlQuery = "  SELECT COUNT(1) AS COUNTS";
            sqlQuery += "  FROM WFM_WORKFLOW_CODE_MASTERS";
            sqlQuery += "  WHERE WORKFLOW_CODE ='" + formId + "'";
            sqlQuery += "  AND ENABLED_FLAG = 1";
            sqlQuery += "  AND WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += "  AND ROWNUM = 1";

            counts = DBMethod.GetIntValue(sqlQuery);
            if (counts > 0)
            {
                wfCount = true;
            }
            else
            {
                wfCount = false;
            }
            return wfCount;
        }
        public static int GetAuditTrailValue(string form_trn_name)
        {
            int auditLogValue = 0;
            string sqlQuery = string.Empty;
            sqlQuery = "select nvl(trim(ss.audit_trail), 0) as audit_trail from ssm_screens ss where ss.screen_code='" + form_trn_name + "'";
            auditLogValue = DBMethod.GetIntValue(sqlQuery);

            return auditLogValue;
        }
        public static void Get_SP_LOG_CHANGES(string form_trn_name, string login_user, string p_key)
        {
            try
            {
                string strValue = string.Empty;
                string strNextCode = string.Empty;
                string strErrMsg = string.Empty;
                int auditLogValue = 0;

                auditLogValue = GetAuditTrailValue(form_trn_name);

                if (auditLogValue == 1)
                {
                    OracleCommand oraCmd = new OracleCommand();
                    oraCmd.CommandText = "LOGS.LOG_CHANGES";
                    oraCmd.CommandType = CommandType.StoredProcedure;
                    oraCmd.Parameters.Add(new OracleParameter("@P_TRANSACTION_CODE", OracleDbType.Varchar2, 250)).Value = p_key;
                    oraCmd.Parameters.Add(new OracleParameter("@P_FORM_CODE", OracleDbType.Varchar2, 250)).Value = form_trn_name;
                    oraCmd.Parameters.Add(new OracleParameter("@P_USER_CODE", OracleDbType.Varchar2, 250)).Value = login_user;

                    oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static void Get_SP_LOG_CHANGES_UPDATE(string form_trn_name, string login_user, string p_key)
        {
            try
            {
                string strValue = string.Empty;
                string strNextCode = string.Empty;
                string strErrMsg = string.Empty;
                int auditLogValue = 0;

                auditLogValue = GetAuditTrailValue(form_trn_name);

                if (auditLogValue == 1)
                {
                    OracleCommand oraCmd = new OracleCommand();
                    oraCmd.CommandText = "LOGS.LOG_CHANGES_UPDATE";
                    oraCmd.CommandType = CommandType.StoredProcedure;
                    oraCmd.Parameters.Add(new OracleParameter("@P_TRANSACTION_CODE", OracleDbType.Varchar2, 250)).Value = p_key;
                    oraCmd.Parameters.Add(new OracleParameter("@P_FORM_CODE", OracleDbType.Varchar2, 250)).Value = form_trn_name;
                    oraCmd.Parameters.Add(new OracleParameter("@P_USER_CODE", OracleDbType.Varchar2, 250)).Value = login_user;

                    oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static string GetWorhflowstatus(string form_trn_name, string login_user, string p_key)
        {
            try
            {
                VMVServices.Web.Utils.SavedRecordId = p_key;
                string strValue = string.Empty;
                string strNextCode = string.Empty;
                string strErrMsg = string.Empty;


                FINEntities context = new FINEntities();
                EntityConnection conn = (EntityConnection)context.Connection;
                OracleConnection oraConn = (OracleConnection)conn.StoreConnection;
                oraConn.Open();


                OracleCommand oraCmd = new OracleCommand("Workflow_Handler.get_workflow_status", oraConn);

                oraCmd.CommandText = "Workflow_Handler.get_workflow_status";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.BindByName = true;

                OracleParameter prm1 = new OracleParameter("form_trn_name", OracleDbType.Varchar2);
                // prm1.Direction = ParameterDirection.Input;
                prm1.Value = form_trn_name;
                oraCmd.Parameters.Add(prm1);

                OracleParameter prm2 = new OracleParameter("login_user", OracleDbType.Varchar2);
                //  prm2.Direction = ParameterDirection.Input;
                prm2.Value = login_user;
                oraCmd.Parameters.Add(prm2);

                OracleParameter prm3 = new OracleParameter("p_key", OracleDbType.Varchar2);
                //  prm3.Direction = ParameterDirection.Input;
                prm3.Value = p_key;
                oraCmd.Parameters.Add(prm3);

                OracleParameter prm4 = new OracleParameter("p_org_id", OracleDbType.Varchar2);
                //  prm4.Direction = ParameterDirection.Input;
                prm4.Value = VMVServices.Web.Utils.OrganizationID;
                oraCmd.Parameters.Add(prm4);

                oraCmd.Parameters.Add("wf_status", OracleDbType.Varchar2, 2).Direction = ParameterDirection.ReturnValue;

                oraCmd.ExecuteNonQuery();

                strValue = oraCmd.Parameters["wf_status"].Value.ToString();

                conn.Close();

                return strValue;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static string UpdateWorhflowstatus(string ScreenCode, string Status, string userId, string PKValue, string Desc, string LevelCode)
        {
            //try
            //{
            //    string strQuery = string.Empty;

            //    DataTable dtData = new DataTable();

            //    strQuery = DBMethod.GetStringValue("select Workflow_Handler.update_wf_status('" + ScreenCode + "','" + Status + "','" + userId + "'" + PKValue + "'" + Desc + "'" + LevelCode + "') from dual");

            //    //  dtData = DBMethod.ExecuteQuery(strQuery).Tables[0];

            //    return strQuery;

            //}
            //catch (Exception ex)
            //{
            //    throw ex;
            //}

            try
            {
                string strValue = string.Empty;
                string strNextCode = string.Empty;
                string strErrMsg = string.Empty;


                FINEntities context = new FINEntities();
                EntityConnection conn = (EntityConnection)context.Connection;
                OracleConnection oraConn = (OracleConnection)conn.StoreConnection;
                oraConn.Open();


                OracleCommand oraCmd = new OracleCommand("Workflow_Handler.update_wf_status", oraConn);

                oraCmd.CommandText = "Workflow_Handler.update_wf_status";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.BindByName = true;

                OracleParameter prm1 = new OracleParameter("P_SCREEN_CODE", OracleDbType.Varchar2);
                // prm1.Direction = ParameterDirection.Input;
                prm1.Value = ScreenCode;
                oraCmd.Parameters.Add(prm1);

                OracleParameter prm2 = new OracleParameter("P_STATUS", OracleDbType.Varchar2);
                //  prm2.Direction = ParameterDirection.Input;
                prm2.Value = Status;
                oraCmd.Parameters.Add(prm2);

                OracleParameter prm3 = new OracleParameter("P_USERID", OracleDbType.Varchar2);
                //  prm3.Direction = ParameterDirection.Input;
                prm3.Value = userId;
                oraCmd.Parameters.Add(prm3);

                OracleParameter prm4 = new OracleParameter("P_TRANSACTION_CODE", OracleDbType.Varchar2);
                //  prm4.Direction = ParameterDirection.Input;
                prm4.Value = PKValue;
                oraCmd.Parameters.Add(prm4);

                OracleParameter prm5 = new OracleParameter("P_DESCRIPTION", OracleDbType.Varchar2);
                //  prm4.Direction = ParameterDirection.Input;
                prm5.Value = Desc;
                oraCmd.Parameters.Add(prm5);


                OracleParameter prm6 = new OracleParameter("P_LEVEL_CODE", OracleDbType.Varchar2);
                //  prm4.Direction = ParameterDirection.Input;
                prm6.Value = LevelCode;
                oraCmd.Parameters.Add(prm6);

                oraCmd.Parameters.Add("P_RETURN_STATUS", OracleDbType.Int32, 2).Direction = ParameterDirection.Output;

                oraCmd.ExecuteNonQuery();

                strValue = oraCmd.Parameters["P_RETURN_STATUS"].Value.ToString();

                conn.Close();

                return strValue;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static string GetOracleFunction(string packageName, string functionName, string parameter1 = "", string parameter2 = "", string parameter3 = "", string parameter4 = "", string parameter5 = "", string parameter6 = "", string parameter7 = "", string parameter8 = "", string parameter9 = "", string parameter10 = "")
        {
            try
            {
                string strQuery = string.Empty;

                DataTable dtData = new DataTable();

                strQuery = DBMethod.GetStringValue("select " + packageName + "." + functionName + "('" + parameter1 + "','" + parameter2 + "','" + parameter3 + "','" + parameter4 + "','" + parameter5 + "','" + parameter6 + "','" + parameter7 + "','" + parameter8 + "','" + parameter9 + "') from dual");



                return strQuery;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static string GetSPItemLedger(string packageName, string functionName, string parameter1 = "", string parameter2 = "")
        {
            try
            {
                string strQuery = string.Empty;

                DataTable dtData = new DataTable();

                strQuery = DBMethod.GetStringValue("select " + packageName + "." + functionName + "('" + parameter1 + "','" + parameter2 + "') from dual");

                return strQuery;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void GetSPFOR_PERIOD_DTL(string cal_hdr_id, string adj_period, string str_CalDtlId, DateTime from_dte, DateTime end_dte)
        {
            try
            {
                string strValue = string.Empty;
                string strNextCode = string.Empty;
                string strErrMsg = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "GL_PKG.generate_accounting_period";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add(new OracleParameter("@p_cal_hdr_id", OracleDbType.Varchar2, 250)).Value = cal_hdr_id;
                oraCmd.Parameters.Add(new OracleParameter("@p_adj_period", OracleDbType.Varchar2, 250)).Value = adj_period;
                oraCmd.Parameters.Add(new OracleParameter("@p_str_CalDtlId", OracleDbType.Varchar2, 250)).Value = str_CalDtlId;
                oraCmd.Parameters.Add(new OracleParameter("@p_from_dte", OracleDbType.Date, 250)).Value = from_dte.ToString("dd/MMM/yyyy");
                oraCmd.Parameters.Add(new OracleParameter("@p_end_dte", OracleDbType.Date, 250)).Value = end_dte.ToString("dd/MMM/yyyy");

                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);



            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void GetSPFOR_EXCHG_STD_RATE(string currency_id, DateTime from_dte, DateTime end_dte, decimal stand_rate, string comp_id)
        {
            try
            {
                string strValue = string.Empty;
                string strNextCode = string.Empty;
                string strErrMsg = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "GL_PKG.gl_exchange_rates";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add(new OracleParameter("@p_currency_id", OracleDbType.Varchar2, 250)).Value = currency_id;
                oraCmd.Parameters.Add(new OracleParameter("@p_from_dt", OracleDbType.Date, 250)).Value = from_dte.ToString("dd/MMM/yyyy");
                oraCmd.Parameters.Add(new OracleParameter("@p_to_dt", OracleDbType.Date, 250)).Value = end_dte.ToString("dd/MMM/yyyy");
                oraCmd.Parameters.Add(new OracleParameter("@p_std_rate", OracleDbType.Decimal, 250)).Value = stand_rate;
                oraCmd.Parameters.Add(new OracleParameter("@p_comp_id", OracleDbType.Varchar2, 250)).Value = comp_id;


                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);



            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //public static bool GetSPItemLedgerEntry(string itemId, string warehouseId)
        //{
        //    try
        //    {
        //        string strValue = string.Empty;
        //        string strNextCode = string.Empty;
        //        string strErrMsg = string.Empty;
        //        bool values;

        //        OracleCommand oraCmd = new OracleCommand();

        //        oraCmd.CommandText = "item_txn";
        //           //oraCmd.CommandText = "inv_pkg.item_txn_ledger_entry";

        //        oraCmd.CommandType = CommandType.StoredProcedure;

        //        OracleParameter retval = new OracleParameter("myretval", OracleDbType.Varchar2, 50);               
        //        retval.Direction = ParameterDirection.ReturnValue;

        //        oraCmd.Parameters.Add(retval);
        //        oraCmd.Parameters.Add(new OracleParameter("@p_item_id", OracleDbType.Varchar2, 250)).Value = itemId;
        //        oraCmd.Parameters.Add(new OracleParameter("@p_wh_id", OracleDbType.Varchar2, 250)).Value = warehouseId;

        //        oraCmd.ExecuteNonQuery();
        //        DBMethod.ExecuteFunction(oraCmd);


        //        //oraCmd.Parameters.Add("@p_item_id", OracleDbType.Varchar2, 250).Value = itemId;
        //        //oraCmd.Parameters.Add("@p_wh_id", OracleDbType.Varchar2, 250).Value = warehouseId;

        //        ////oraCmd.Parameters.Add("@retnMESG", OracleDbType.Byte, 50);
        //        ////oraCmd.Parameters["@retnMESG"].Direction = ParameterDirection.Output;

        //        //oraCmd.Parameters.Add("return_value", OracleDbType.Varchar2,2).Direction = ParameterDirection.ReturnValue;

        //        //oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

        //        //// strValue = oraCmd.Value.ToString();
        //        if (retval.Value != null)
        //        {
        //            return values = true;

        //        }
        //        else
        //        {
        //            return values = false;
        //        }
        //      //  return retval.Value;

        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        public static int GetSPItemLedgerEntry(string itemId, string warehouseId)
        {
            try
            {
                int strValue = 0;
                string strNextCode = string.Empty;
                string strErrMsg = string.Empty;
                bool values;

                OracleCommand oraCmd = new OracleCommand();


                oraCmd.CommandText = "inv_pkg.item_txn_ledger_entry";

                oraCmd.CommandType = CommandType.StoredProcedure;

                OracleParameter retval = new OracleParameter("myretval", OracleDbType.Varchar2, 50);
                retval.Direction = ParameterDirection.ReturnValue;

                oraCmd.Parameters.Add(retval);
                oraCmd.Parameters.Add(new OracleParameter("@p_item_id", OracleDbType.Varchar2, 250)).Value = itemId;
                oraCmd.Parameters.Add(new OracleParameter("@p_wh_id", OracleDbType.Varchar2, 250)).Value = warehouseId;

                DBMethod.ExecuteFunction(oraCmd);

                return strValue;//= (int)((Oracle.DataAccess.Types.OracleString)(retval.Value)).Value;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static void GetSPItemtxnLedger(string parameter1, string parameter2, string parameter3, string parameter4, string parameter5, string parameter6, string parameter7, string parameter8, string parameter9)
        {
            try
            {

                OracleCommand oraCmd = new OracleCommand();
                DateTime dt_tmp = Convert.ToDateTime(parameter5);

                oraCmd.CommandText = "inv_pkg.inv_txn_ledger";

                oraCmd.CommandType = CommandType.StoredProcedure;

                oraCmd.Parameters.Add(new OracleParameter("@p_parameter1", OracleDbType.Varchar2, 250)).Value = parameter1;
                oraCmd.Parameters.Add(new OracleParameter("@p_parameter2", OracleDbType.Varchar2, 250)).Value = parameter2;
                oraCmd.Parameters.Add(new OracleParameter("@p_parameter3", OracleDbType.Varchar2, 250)).Value = parameter3;
                oraCmd.Parameters.Add(new OracleParameter("@p_parameter4", OracleDbType.Varchar2, 250)).Value = parameter4;
                oraCmd.Parameters.Add(new OracleParameter("@p_parameter5", OracleDbType.Date, 250)).Value = dt_tmp.ToString("dd/MMM/yyyy");
                oraCmd.Parameters.Add(new OracleParameter("@p_parameter6", OracleDbType.Varchar2, 250)).Value = parameter6;
                oraCmd.Parameters.Add(new OracleParameter("@p_parameter7", OracleDbType.Varchar2, 250)).Value = parameter7;
                oraCmd.Parameters.Add(new OracleParameter("@p_parameter8", OracleDbType.Varchar2, 250)).Value = parameter8;
                oraCmd.Parameters.Add(new OracleParameter("@p_parameter9", OracleDbType.Varchar2, 250)).Value = parameter9;
                //oraCmd.Parameters.Add(new OracleParameter("@p_parameter10", OracleDbType.Varchar2, 250)).Value = parameter10;


                DBMethod.ExecuteFunction(oraCmd);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static void GetProc_For_Import_EmTrans(DateTime P_DATE)
        {
            try
            {
                string strValue = string.Empty;
                string strNextCode = string.Empty;
                string strErrMsg = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "IMPORT_TM_EMP_TRANS_MNTH_YR";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add("@P_DATE", OracleDbType.Date, 250).Value = P_DATE.ToString("dd/MMM/yyyy");
                //oraCmd.Parameters.Add("@p_year", OracleDbType.Int64).Value = year;
                //  oraCmd.Parameters.Add("@p_out", OracleDbType.Varchar2, 150);
                //  oraCmd.Parameters["@p_out"].Direction = ParameterDirection.Output;
                // oraCmd.Parameters.Add("@p_org_id", OracleDbType.Varchar2).Value = VMVServices.Web.Utils.OrganizationID;
                DBMethod.ExecuteStoredProcedure(oraCmd);

                //    strNextCode =oraCmd.Parameters["@p_out"].Value.ToString();
                // return strNextCode;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static string GetSPChequeCreation(string bank_id, string bank_branch_id, string account_no, int cheque_start, string no_of_cheques, bool IsValidation = false, bool IsSaved = false)
        {
            try
            {
                string strValue = string.Empty;
                string strNextCode = string.Empty;
                string strErrMsg = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "CA_PKG.CA_CHEQUE_CREATION_PRO";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add("@P_BANK_ID", OracleDbType.Char).Value = bank_id;
                oraCmd.Parameters.Add("@P_BANK_BRANCH_ID", OracleDbType.Char).Value = bank_branch_id;
                oraCmd.Parameters.Add("@P_ACCOUNT_NO", OracleDbType.Char).Value = account_no;
                oraCmd.Parameters.Add("@P_CHEQUE_START", OracleDbType.Int64).Value = cheque_start;
                oraCmd.Parameters.Add("@P_NO_OF_CHEQUE", OracleDbType.Char).Value = no_of_cheques;
                oraCmd.Parameters.Add("@ERRMESG", OracleDbType.Varchar2, 150);
                oraCmd.Parameters.Add("@TXNCODE", OracleDbType.Varchar2, 150);
                oraCmd.Parameters["@ERRMESG"].Direction = ParameterDirection.Output;
                oraCmd.Parameters["@TXNCODE"].Direction = ParameterDirection.Output;
                //oraCmd.Parameters.Add("@ErrorCode", OracleDbType.Varchar2, 50);
                //oraCmd.Parameters["@ErrorCode"].Direction = ParameterDirection.Output;
                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

                strNextCode = oraCmd.Parameters["@TXNCODE"].Value.ToString();


                return strNextCode;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //GL

        public static string GetSPFOR_ERR_MGR_CURRENCY_CODE(string P_CURRENCY_CODE, string P_RECORD_ID)
        {
            try
            {
                string retcode = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "PKG_VALIDATIONS.PROC_ERR_MGR_CURRENCY_CODE";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add("@P_MODULE_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_SCREEN_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_CURRENCY_CODE", OracleDbType.Char).Value = P_CURRENCY_CODE.Trim();
                oraCmd.Parameters.Add("@P_RECORD_ID", OracleDbType.Char).Value = P_RECORD_ID;
                oraCmd.Parameters.Add("@P_RET", OracleDbType.Varchar2, 150);
                oraCmd.Parameters["@P_RET"].Direction = ParameterDirection.Output;
                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

                retcode = oraCmd.Parameters["@P_RET"].Value.ToString();


                return retcode;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GetSPFOR_PAY_ELE_CAL(string P_DEPT, string P_DESIG, string ORG_ID, string FORMULA)
        {
            try
            {
                string retcode = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "PKG_PAYROLL.GET_PAY_ELEMENT_CALC";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add("@P_DEPT", OracleDbType.Char).Value = P_DEPT;
                oraCmd.Parameters.Add("@P_DESIG", OracleDbType.Char).Value = P_DESIG;
                oraCmd.Parameters.Add("@ORG_ID", OracleDbType.Char).Value = ORG_ID;
                oraCmd.Parameters.Add("@FORMULA", OracleDbType.Char).Value = FORMULA;
                oraCmd.Parameters.Add("@P_RET", OracleDbType.Varchar2, 150);
                oraCmd.Parameters["@P_RET"].Direction = ParameterDirection.Output;
                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

                retcode = oraCmd.Parameters["@P_RET"].Value.ToString();


                return retcode;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static string GetSPFOR_ERR_MGR_ACC_GROUP(string P_ACC_GROUP_NAME, string P_RECORD_ID, string P_FROM_DATE, string P_TO_DATE)
        {
            try
            {
                string retcode = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "PKG_VALIDATIONS.PROC_ERR_MGR_ACC_GROUP";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add("@P_MODULE_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_SCREEN_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_ACC_GROUP_NAME", OracleDbType.Char).Value = P_ACC_GROUP_NAME.Trim();
                oraCmd.Parameters.Add("@P_RECORD_ID", OracleDbType.Char).Value = P_RECORD_ID;
                oraCmd.Parameters.Add("@P_FROM_DATE", OracleDbType.Char).Value = P_FROM_DATE;
                oraCmd.Parameters.Add("@P_TO_DATE", OracleDbType.Char).Value = P_TO_DATE;
                oraCmd.Parameters.Add("@P_RET", OracleDbType.Varchar2, 150);
                oraCmd.Parameters["@P_RET"].Direction = ParameterDirection.Output;
                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

                retcode = oraCmd.Parameters["@P_RET"].Value.ToString();


                return retcode;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GetSPFOR_ERR_MGR_SEGMENT(string P_SEGMENT_NAME, string P_RECORD_ID, string P_FROM_DATE, string P_TO_DATE)
        {
            try
            {
                string retcode = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "PKG_VALIDATIONS.PROC_ERR_MGR_SEGMENT";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add("@P_MODULE_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_SCREEN_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_SEGMENT_NAME", OracleDbType.Char).Value = P_SEGMENT_NAME.Trim();
                oraCmd.Parameters.Add("@P_RECORD_ID", OracleDbType.Char).Value = P_RECORD_ID;
                oraCmd.Parameters.Add("@P_FROM_DATE", OracleDbType.Char).Value = P_FROM_DATE;
                oraCmd.Parameters.Add("@P_TO_DATE", OracleDbType.Char).Value = P_TO_DATE;
                oraCmd.Parameters.Add("@P_RET", OracleDbType.Varchar2, 150);
                oraCmd.Parameters["@P_RET"].Direction = ParameterDirection.Output;
                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

                retcode = oraCmd.Parameters["@P_RET"].Value.ToString();


                return retcode;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GetSPFOR_ERR_MGR_ACC_STRUCTURE(string P_ACCT_STRUCT_NAME, string P_RECORD_ID, string P_FROM_DATE, string P_TO_DATE)
        {
            try
            {
                string retcode = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "PKG_VALIDATIONS.PROC_ERR_MGR_ACC_STRUCTURE";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add("@P_MODULE_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_SCREEN_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_ACCT_STRUCT_NAME", OracleDbType.Char).Value = P_ACCT_STRUCT_NAME.Trim();
                oraCmd.Parameters.Add("@P_RECORD_ID", OracleDbType.Char).Value = P_RECORD_ID;
                oraCmd.Parameters.Add("@P_FROM_DATE", OracleDbType.Char).Value = P_FROM_DATE;
                oraCmd.Parameters.Add("@P_TO_DATE", OracleDbType.Char).Value = P_TO_DATE;
                oraCmd.Parameters.Add("@P_RET", OracleDbType.Varchar2, 150);
                oraCmd.Parameters["@P_RET"].Direction = ParameterDirection.Output;
                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

                retcode = oraCmd.Parameters["@P_RET"].Value.ToString();


                return retcode;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GetSPFOR_ERR_MGR_ACC_GRP_DEF_SEG(string P_ACCT_GRP_ID, string P_ACCT_SEGMENT_ID, string P_RECORD_ID, string P_FROM_DATE, string P_TO_DATE)
        {
            try
            {
                string retcode = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "PKG_VALIDATIONS.PROC_ERR_MGR_ACC_GRP_DEF_SEG";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add("@P_MODULE_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_SCREEN_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_ACCT_GRP_ID", OracleDbType.Char).Value = P_ACCT_GRP_ID;
                oraCmd.Parameters.Add("@P_ACCT_SEGMENT_ID", OracleDbType.Char).Value = P_ACCT_SEGMENT_ID;
                oraCmd.Parameters.Add("@P_RECORD_ID", OracleDbType.Char).Value = P_RECORD_ID;
                oraCmd.Parameters.Add("@P_FROM_DATE", OracleDbType.Char).Value = P_FROM_DATE;
                oraCmd.Parameters.Add("@P_TO_DATE", OracleDbType.Char).Value = P_TO_DATE;
                oraCmd.Parameters.Add("@P_RET", OracleDbType.Varchar2, 150);
                oraCmd.Parameters["@P_RET"].Direction = ParameterDirection.Output;
                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

                retcode = oraCmd.Parameters["@P_RET"].Value.ToString();


                return retcode;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GetSPFOR_ERR_MGR_ACC_CODE(string P_ACC_STRUCTURE, string P_ACC_GROUP, string P_ACCT_CODE, string P_RECORD_ID, string P_FROM_DATE, string P_TO_DATE)
        {
            try
            {
                string retcode = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "PKG_VALIDATIONS.PROC_ERR_MGR_ACC_CODE";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add("@P_MODULE_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_SCREEN_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_ACC_STRUCTURE", OracleDbType.Char).Value = P_ACC_STRUCTURE;
                oraCmd.Parameters.Add("@P_ACC_GROUP", OracleDbType.Char).Value = P_ACC_GROUP.Trim();
                oraCmd.Parameters.Add("@P_ACCT_CODE", OracleDbType.Char).Value = P_ACCT_CODE.Trim();
                oraCmd.Parameters.Add("@P_RECORD_ID", OracleDbType.Char).Value = P_RECORD_ID;
                oraCmd.Parameters.Add("@P_FROM_DATE", OracleDbType.Char).Value = P_FROM_DATE;
                oraCmd.Parameters.Add("@P_TO_DATE", OracleDbType.Char).Value = P_TO_DATE;
                oraCmd.Parameters.Add("@P_RETa", OracleDbType.Varchar2, 150);
                oraCmd.Parameters["@P_RETa"].Direction = ParameterDirection.Output;
                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

                retcode = oraCmd.Parameters["@P_RETa"].Value.ToString();


                return retcode;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GetSPFOR_ERR_MGR_ACC_CALENDAR(string P_ACCT_CODE, string P_RECORD_ID, string P_FROM_DATE, string P_TO_DATE)
        {
            try
            {
                string retcode = string.Empty;

                OracleCommand oraCmd = new OracleCommand();


                oraCmd.CommandText = "PKG_VALIDATIONS.PROC_ERR_MGR_ACC_CALENDAR";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add("@P_MODULE_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_SCREEN_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_ACCT_CODE", OracleDbType.Char).Value = P_ACCT_CODE;
                oraCmd.Parameters.Add("@P_RECORD_ID", OracleDbType.Char).Value = P_RECORD_ID;
                oraCmd.Parameters.Add("@P_FROM_DATE", OracleDbType.Char).Value = P_FROM_DATE;
                oraCmd.Parameters.Add("@P_TO_DATE", OracleDbType.Char).Value = P_TO_DATE;
                oraCmd.Parameters.Add("@P_RETt", OracleDbType.Varchar2, 150);
                oraCmd.Parameters["@P_RETt"].Direction = ParameterDirection.Output;
                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

                retcode = oraCmd.Parameters["@P_RETt"].Value.ToString();


                return retcode;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static string GetSPFOR_ERR_MGR_ACC_CALENDAR_DTL(string calName, string P_Acc_Yr, string P_RECORD_ID, string P_FROM_DATE, string P_TO_DATE)
        {
            try
            {
                string retcode = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "PKG_VALIDATIONS.PROC_ERR_MGR_ACC_CALENDAR_DTL";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add("@P_MODULE_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_SCREEN_CODE", OracleDbType.Char).Value = calName;
                oraCmd.Parameters.Add("@p_ACC_YR", OracleDbType.Char).Value = P_Acc_Yr;
                if (P_RECORD_ID == null)
                {
                    oraCmd.Parameters.Add("@P_RECORD_ID", OracleDbType.Char).Value = "0";
                }
                else
                {
                    oraCmd.Parameters.Add("@P_RECORD_ID", OracleDbType.Char).Value = P_RECORD_ID;
                }
                oraCmd.Parameters.Add("@P_FROM_DATE", OracleDbType.Char).Value = P_FROM_DATE;
                oraCmd.Parameters.Add("@P_TO_DATE", OracleDbType.Char).Value = P_TO_DATE;
                oraCmd.Parameters.Add("@P_RETc", OracleDbType.Varchar2, 150);
                oraCmd.Parameters["@P_RETc"].Direction = ParameterDirection.Output;
                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

                retcode = oraCmd.Parameters["@P_RETc"].Value.ToString();


                return retcode;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static string GetSPFOR_ERR_ACC_CAL_DTL_EDIT(string P_RECORD_ID)
        {
            try
            {
                string retcode = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "PKG_VALIDATIONS.PROC_ERR_ACC_CAL_DTL_EDIT";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add("@P_MODULE_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_SCREEN_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@p_cal_dtl_id", OracleDbType.Char).Value = P_RECORD_ID;
                if (P_RECORD_ID == null)
                {
                    oraCmd.Parameters.Add("@P_RECORD_ID", OracleDbType.Char).Value = "0";
                }
                else
                {
                    oraCmd.Parameters.Add("@P_RECORD_ID", OracleDbType.Char).Value = P_RECORD_ID;
                }
                oraCmd.Parameters.Add("@P_ORG_ID", OracleDbType.Char).Value = VMVServices.Web.Utils.OrganizationID;

                oraCmd.Parameters.Add("@P_RET", OracleDbType.Varchar2, 150);
                oraCmd.Parameters["@P_RET"].Direction = ParameterDirection.Output;
                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

                retcode = oraCmd.Parameters["@P_RET"].Value.ToString();


                return retcode;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GetSPFOR_ERR_MGR_ACC_GRP_LINKS(string P_FROM_DATE, string P_TO_DATE)
        {
            try
            {
                string retcode = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "PKG_VALIDATIONS.PROC_ERR_MGR_ACC_GRP_LINK";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add("@P_FROM_DATE", OracleDbType.Char).Value = P_FROM_DATE;
                oraCmd.Parameters.Add("@P_TO_DATE", OracleDbType.Char).Value = P_TO_DATE;
                oraCmd.Parameters.Add("@P_RET", OracleDbType.Varchar2, 150);
                oraCmd.Parameters["@P_RET"].Direction = ParameterDirection.Output;
                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

                retcode = oraCmd.Parameters["@P_RET"].Value.ToString();


                return retcode;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GetSPFOR_ERR_MGR_ORGANIZATION(string P_INTERNAL_NAME, string P_SHORT_NAME, string P_RECORD_ID)
        {
            try
            {
                string retcode = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "PKG_VALIDATIONS.PROC_ERR_MGR_ORGANIZATION";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add("@P_MODULE_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_SCREEN_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_INTERNAL_NAME", OracleDbType.Char).Value = P_INTERNAL_NAME.Trim();
                oraCmd.Parameters.Add("@P_SHORT_NAME", OracleDbType.Char).Value = P_SHORT_NAME.Trim();
                oraCmd.Parameters.Add("@P_RECORD_ID", OracleDbType.Char).Value = P_RECORD_ID;
                oraCmd.Parameters.Add("@P_RET", OracleDbType.Varchar2, 150);
                oraCmd.Parameters["@P_RET"].Direction = ParameterDirection.Output;
                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

                retcode = oraCmd.Parameters["@P_RET"].Value.ToString();


                return retcode;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // AP

        public static string GetSPFOR_ERR_MGR_TAX_TERM(string P_TAX_NAME, string P_RECORD_ID, string P_FROM_DATE, string P_TO_DATE)
        {
            try
            {
                string retcode = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "PKG_VALIDATIONS.PROC_ERR_MGR_TAX_TERM";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add("@P_MODULE_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_SCREEN_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_TAX_NAME", OracleDbType.Char).Value = P_TAX_NAME;
                oraCmd.Parameters.Add("@P_RECORD_ID", OracleDbType.Char).Value = P_RECORD_ID;
                oraCmd.Parameters.Add("@P_FROM_DATE", OracleDbType.Char).Value = P_FROM_DATE;
                oraCmd.Parameters.Add("@P_TO_DATE", OracleDbType.Char).Value = P_TO_DATE;
                oraCmd.Parameters.Add("@P_RET", OracleDbType.Varchar2, 150);
                oraCmd.Parameters["@P_RET"].Direction = ParameterDirection.Output;
                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

                retcode = oraCmd.Parameters["@P_RET"].Value.ToString();


                return retcode;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GetSPFOR_ERR_MGR_ITEM_CATOG(string P_ITEM_CAT_NAME, string P_ORG_ID, string P_RECORD_ID, string P_FROM_DATE, string P_TO_DATE)
        {
            try
            {
                string retcode = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "PKG_VALIDATIONS.PROC_ERR_MGR_ITEM_CATOG";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add("@P_MODULE_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_SCREEN_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_ITEM_CAT_NAME", OracleDbType.Char).Value = P_ITEM_CAT_NAME;
                oraCmd.Parameters.Add("@P_ORG_ID", OracleDbType.Char).Value = P_ORG_ID;
                oraCmd.Parameters.Add("@P_RECORD_ID", OracleDbType.Char).Value = P_RECORD_ID;
                oraCmd.Parameters.Add("@P_FROM_DATE", OracleDbType.Char).Value = P_FROM_DATE;
                oraCmd.Parameters.Add("@P_TO_DATE", OracleDbType.Char).Value = P_TO_DATE;
                oraCmd.Parameters.Add("@P_RET", OracleDbType.Varchar2, 150);
                oraCmd.Parameters["@P_RET"].Direction = ParameterDirection.Output;
                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

                retcode = oraCmd.Parameters["@P_RET"].Value.ToString();


                return retcode;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static string GetSPFOR_ERR_MGR_UOM_MST(string P_UOM_CODE, string P_UOM_BASE, string P_ORG_ID, string P_RECORD_ID)
        {
            try
            {
                string retcode = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "PKG_VALIDATIONS.PROC_ERR_MGER_UOM_MST";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add("@P_MODULE_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_SCREEN_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_UOM_CODE", OracleDbType.Char).Value = P_UOM_CODE;
                oraCmd.Parameters.Add("@P_UOM_BASE", OracleDbType.Char).Value = P_UOM_BASE;
                oraCmd.Parameters.Add("@P_ORG_ID", OracleDbType.Char).Value = P_ORG_ID;
                oraCmd.Parameters.Add("@P_RECORD_ID", OracleDbType.Char).Value = P_RECORD_ID;
                oraCmd.Parameters.Add("@P_RET", OracleDbType.Varchar2, 150);
                oraCmd.Parameters["@P_RET"].Direction = ParameterDirection.Output;
                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

                retcode = oraCmd.Parameters["@P_RET"].Value.ToString();


                return retcode;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GetSPFOR_UOM_CONV_VALIDATOR(string P_UOM_FROM, string P_UOM_TO, string P_ORG_ID, string P_RECORD_ID)
        {
            try
            {
                string retcode = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "PKG_VALIDATIONS.PROC_UOM_CONV_VALIDATOR";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add("@P_MODULE_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_SCREEN_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_UOM_FROM", OracleDbType.Char).Value = P_UOM_FROM;
                oraCmd.Parameters.Add("@P_UOM_TO", OracleDbType.Char).Value = P_UOM_TO;
                oraCmd.Parameters.Add("@P_ORG_ID", OracleDbType.Char).Value = P_ORG_ID;
                oraCmd.Parameters.Add("@P_RECORD_ID", OracleDbType.Char).Value = P_RECORD_ID;
                oraCmd.Parameters.Add("@P_RET", OracleDbType.Varchar2, 150);
                oraCmd.Parameters["@P_RET"].Direction = ParameterDirection.Output;
                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

                retcode = oraCmd.Parameters["@P_RET"].Value.ToString();


                return retcode;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GetSPFOR_ERR_MGR_SUPPLIER(string P_VENDOR_CODE, string P_RECORD_ID)
        {
            try
            {
                string retcode = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "PKG_VALIDATIONS.PROC_ERR_MGR_SUPPLIER";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add("@P_MODULE_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_SCREEN_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_VENDOR_CODE", OracleDbType.Char).Value = P_VENDOR_CODE;
                oraCmd.Parameters.Add("@P_RECORD_ID", OracleDbType.Char).Value = P_RECORD_ID;
                oraCmd.Parameters.Add("@P_RET", OracleDbType.Varchar2, 150);
                oraCmd.Parameters["@P_RET"].Direction = ParameterDirection.Output;
                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

                retcode = oraCmd.Parameters["@P_RET"].Value.ToString();


                return retcode;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GetSPFOR_ERR_MGR_SUP_BRANCH(string P_BRANCH_CODE, string P_RECORD_ID)
        {
            try
            {
                string retcode = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "PKG_VALIDATIONS.PROC_ERR_MGR_SUP_BRANCH";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add("@P_MODULE_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_SCREEN_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_BRANCH_CODE", OracleDbType.Char).Value = P_BRANCH_CODE;
                oraCmd.Parameters.Add("@P_RECORD_ID", OracleDbType.Char).Value = P_RECORD_ID;
                oraCmd.Parameters.Add("@P_RET", OracleDbType.Varchar2, 150);
                oraCmd.Parameters["@P_RET"].Direction = ParameterDirection.Output;
                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

                retcode = oraCmd.Parameters["@P_RET"].Value.ToString();


                return retcode;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GetSPFOR_ERR_MGR_PO(string P_PO_NUMBER, string P_RECORD_ID)
        {
            try
            {
                string retcode = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "PKG_VALIDATIONS.PROC_ERR_MGR_PO";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add("@P_MODULE_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_SCREEN_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_PO_NUMBER", OracleDbType.Char).Value = P_PO_NUMBER;
                oraCmd.Parameters.Add("@P_RECORD_ID", OracleDbType.Char).Value = P_RECORD_ID;
                oraCmd.Parameters.Add("@P_RET", OracleDbType.Varchar2, 150);
                oraCmd.Parameters["@P_RET"].Direction = ParameterDirection.Output;
                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

                retcode = oraCmd.Parameters["@P_RET"].Value.ToString();


                return retcode;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static string GetSPFOR_ERR_IS_CAL_PERIOD_AVIAL(string P_RECORD_ID, string str_Date)
        {
            try
            {
                string retcode = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "PKG_VALIDATIONS.PROC_ERR_IS_PERIOD_AVIAL";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add("@P_MODULE_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_SCREEN_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@p_data_id", OracleDbType.Char).Value = P_RECORD_ID;
                oraCmd.Parameters.Add("@P_RECORD_ID", OracleDbType.Char).Value = P_RECORD_ID;
                oraCmd.Parameters.Add("@P_ORG_ID", OracleDbType.Char).Value = VMVServices.Web.Utils.OrganizationID;
                oraCmd.Parameters.Add("@P_TRANS_DATE", OracleDbType.Char).Value = str_Date;
                oraCmd.Parameters.Add("@P_RET", OracleDbType.Varchar2, 500);
                oraCmd.Parameters["@P_RET"].Direction = ParameterDirection.Output;
                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

                retcode = oraCmd.Parameters["@P_RET"].Value.ToString();


                return retcode;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GetSPFOR_ERR_MGR_PO_RECP(string P_GRN_NUMBER, string P_RECORD_ID)
        {
            try
            {
                string retcode = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "PKG_VALIDATIONS.PROC_ERR_MGR_PO_RECP";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add("@P_MODULE_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_SCREEN_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_GRN_NUMBER", OracleDbType.Char).Value = P_GRN_NUMBER;
                oraCmd.Parameters.Add("@P_RECORD_ID", OracleDbType.Char).Value = P_RECORD_ID;
                oraCmd.Parameters.Add("@P_RET", OracleDbType.Varchar2, 150);
                oraCmd.Parameters["@P_RET"].Direction = ParameterDirection.Output;
                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

                retcode = oraCmd.Parameters["@P_RET"].Value.ToString();


                return retcode;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GetSPFOR_ERR_MGR_RECP_LOT(string P_LOT_NUMBER, string P_RECORD_ID)
        {
            try
            {
                string retcode = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "PKG_VALIDATIONS.PROC_ERR_MGR_PO_RECP";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add("@P_MODULE_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_SCREEN_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_LOT_NUMBER", OracleDbType.Char).Value = P_LOT_NUMBER;
                oraCmd.Parameters.Add("@P_RECORD_ID", OracleDbType.Char).Value = P_RECORD_ID;
                oraCmd.Parameters.Add("@P_RET", OracleDbType.Varchar2, 150);
                oraCmd.Parameters["@P_RET"].Direction = ParameterDirection.Output;
                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

                retcode = oraCmd.Parameters["@P_RET"].Value.ToString();

                return retcode;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public static string GetSPFOR_LEAVE_DATE_MGR(string P_LEAVE_REQ_ID, string P_LEAVE_CANCELATION_DATE)
        {
            try
            {
                string retcode = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "PKG_VALIDATIONS.PROC_LEAVE_DATE_MGR";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add("@P_MODULE_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_SCREEN_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_LEAVE_REQ_ID", OracleDbType.Char).Value = P_LEAVE_REQ_ID;
                oraCmd.Parameters.Add("@P_LEAVE_CANCELATION_DATE", OracleDbType.Char).Value = P_LEAVE_CANCELATION_DATE;
                oraCmd.Parameters.Add("@P_RET", OracleDbType.Varchar2, 150);
                oraCmd.Parameters["@P_RET"].Direction = ParameterDirection.Output;
                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

                retcode = oraCmd.Parameters["@P_RET"].Value.ToString();

                return retcode;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static string CALL_DELETE_MANAGER(string P_SCREEN_CODE, string P_RECORD_ID)
        {
            try
            {
                string retcode = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "PKG_VALIDATIONS.PROC_DELETE_MANAGER";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add("@P_MODULE_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_SCREEN_CODE", OracleDbType.Char).Value = P_SCREEN_CODE;
                oraCmd.Parameters.Add("@P_RECORD_ID", OracleDbType.Char).Value = P_RECORD_ID;
                oraCmd.Parameters.Add("@P_RET", OracleDbType.Varchar2, 150);
                oraCmd.Parameters["@P_RET"].Direction = ParameterDirection.Output;
                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

                retcode = oraCmd.Parameters["@P_RET"].Value.ToString();

                return retcode;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GetSPFOR_ERR_MGR_CATEGORY(string P_SCREEN_CODE, string P_RECORD_ID, string P_VALUE1)
        {
            try
            {
                string retcode = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "PKG_DUPLN_VALDN.PROC_DUPLN_VALIDATION_ONE";
                oraCmd.CommandType = CommandType.StoredProcedure;
                //  oraCmd.Parameters.Add("@P_MODULE_CODE", OracleDbType.Char).Value = DBNull.Value;
                //  oraCmd.Parameters.Add("@P_SCREEN_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_SCREEN_CODE", OracleDbType.Char).Value = P_SCREEN_CODE;
                oraCmd.Parameters.Add("@P_PK_ID", OracleDbType.Char).Value = P_RECORD_ID;
                oraCmd.Parameters.Add("@P_VALUE1", OracleDbType.Char).Value = P_VALUE1;
                oraCmd.Parameters.Add("@P_RET", OracleDbType.Varchar2, 150);
                oraCmd.Parameters.Add("@P_ORG_ID", OracleDbType.Char).Value = VMVServices.Web.Utils.OrganizationID;
                oraCmd.Parameters["@P_RET"].Direction = ParameterDirection.Output;
                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

                retcode = oraCmd.Parameters["@P_RET"].Value.ToString();

                return retcode;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GetSPFOR_ERR_MGR_CATEGORY_TWO(string P_SCREEN_CODE, string P_RECORD_ID, string P_VALUE1, string P_VALUE2)
        {
            try
            {
                string retcode = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "PKG_DUPLN_VALDN.PROC_DUPLN_VALIDATION_TWO";
                oraCmd.CommandType = CommandType.StoredProcedure;
                //  oraCmd.Parameters.Add("@P_MODULE_CODE", OracleDbType.Char).Value = DBNull.Value;
                //  oraCmd.Parameters.Add("@P_SCREEN_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_SCREEN_CODE", OracleDbType.Char).Value = P_SCREEN_CODE;
                oraCmd.Parameters.Add("@P_PK_ID", OracleDbType.Char).Value = P_RECORD_ID;
                oraCmd.Parameters.Add("@P_VALUE1", OracleDbType.Char).Value = P_VALUE1;
                oraCmd.Parameters.Add("@P_VALUE2", OracleDbType.Char).Value = P_VALUE2;
                oraCmd.Parameters.Add("@P_RET", OracleDbType.Varchar2, 150);
                oraCmd.Parameters.Add("@P_ORG_ID", OracleDbType.Char).Value = VMVServices.Web.Utils.OrganizationID;
                oraCmd.Parameters["@P_RET"].Direction = ParameterDirection.Output;
                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

                retcode = oraCmd.Parameters["@P_RET"].Value.ToString();

                return retcode;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GetSPFOR_ERR_MGR_CATEGORY_THREE(string P_SCREEN_CODE, string P_RECORD_ID, string P_VALUE1, string P_VALUE2, string P_VALUE3)
        {
            try
            {
                string retcode = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "PKG_DUPLN_VALDN.PROC_DUPLN_VALIDATION_THREE";
                oraCmd.CommandType = CommandType.StoredProcedure;
                //  oraCmd.Parameters.Add("@P_MODULE_CODE", OracleDbType.Char).Value = DBNull.Value;
                //  oraCmd.Parameters.Add("@P_SCREEN_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_SCREEN_CODE", OracleDbType.Char).Value = P_SCREEN_CODE;
                oraCmd.Parameters.Add("@P_PK_ID", OracleDbType.Char).Value = P_RECORD_ID;
                oraCmd.Parameters.Add("@P_VALUE1", OracleDbType.Char).Value = P_VALUE1;
                oraCmd.Parameters.Add("@P_VALUE2", OracleDbType.Char).Value = P_VALUE2;
                oraCmd.Parameters.Add("@P_VALUE3", OracleDbType.Char).Value = P_VALUE3;
                oraCmd.Parameters.Add("@P_RET", OracleDbType.Varchar2, 150);
                oraCmd.Parameters.Add("@P_ORG_ID", OracleDbType.Char).Value = VMVServices.Web.Utils.OrganizationID;
                oraCmd.Parameters["@P_RET"].Direction = ParameterDirection.Output;
                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

                retcode = oraCmd.Parameters["@P_RET"].Value.ToString();

                return retcode;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static string GetSPFOR_ERR_MGR_CATEGORY_FOUR(string P_SCREEN_CODE, string P_RECORD_ID, string P_VALUE1, string P_VALUE2, string P_VALUE3, string P_VALUE4)
        {
            try
            {
                string retcode = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "PKG_DUPLN_VALDN.PROC_DUPLN_VALIDATION_FOUR";
                oraCmd.CommandType = CommandType.StoredProcedure;
                //  oraCmd.Parameters.Add("@P_MODULE_CODE", OracleDbType.Char).Value = DBNull.Value;
                //  oraCmd.Parameters.Add("@P_SCREEN_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_SCREEN_CODE", OracleDbType.Char).Value = P_SCREEN_CODE;
                oraCmd.Parameters.Add("@P_PK_ID", OracleDbType.Char).Value = P_RECORD_ID;
                oraCmd.Parameters.Add("@P_VALUE1", OracleDbType.Char).Value = P_VALUE1;
                oraCmd.Parameters.Add("@P_VALUE2", OracleDbType.Char).Value = P_VALUE2;
                oraCmd.Parameters.Add("@P_VALUE3", OracleDbType.Char).Value = P_VALUE3;
                oraCmd.Parameters.Add("@P_VALUE4", OracleDbType.Char).Value = P_VALUE4;
                oraCmd.Parameters.Add("@P_RET", OracleDbType.Varchar2, 150);
                oraCmd.Parameters.Add("@P_ORG_ID", OracleDbType.Char).Value = VMVServices.Web.Utils.OrganizationID;
                oraCmd.Parameters["@P_RET"].Direction = ParameterDirection.Output;
                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

                retcode = oraCmd.Parameters["@P_RET"].Value.ToString();

                return retcode;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GetSPFOR_SupplierStmtOfAccount(string P_VENDOR_ID, string P_FROM_DATE, string P_END_DATE)
        {
            try
            {
                DateTime EndDate = new DateTime();
                DateTime StartDate = new DateTime();
                string retcode = string.Empty;

                if (P_FROM_DATE != null && P_FROM_DATE.Length > 0)
                    StartDate = Convert.ToDateTime(P_FROM_DATE);
                if (P_END_DATE != null && P_END_DATE.Length > 0)
                    EndDate = Convert.ToDateTime(P_END_DATE);



                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "PKG_DUPLN_VALDN.PROC_DUPLN_VALIDATION_ONE";
                oraCmd.CommandType = CommandType.StoredProcedure;
                //  oraCmd.Parameters.Add("@P_MODULE_CODE", OracleDbType.Char).Value = DBNull.Value;
                //  oraCmd.Parameters.Add("@P_SCREEN_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_VENDOR_ID", OracleDbType.Char).Value = P_VENDOR_ID;
                if (P_FROM_DATE != null && P_FROM_DATE.Length > 0)
                {
                    oraCmd.Parameters.Add("@P_FROM_DATE", OracleDbType.NVarchar2).Value = StartDate.ToString("dd/MMM/yyyy");
                }
                else
                {
                    oraCmd.Parameters.Add("@P_FROM_DATE", OracleDbType.NVarchar2).Value = null;
                }
                if (P_END_DATE != null && P_END_DATE.Length > 0)
                {
                    oraCmd.Parameters.Add("@P_END_DATE", OracleDbType.NVarchar2).Value = EndDate.ToString("dd/MMM/yyyy");
                }
                else
                {
                    oraCmd.Parameters.Add("@P_END_DATE", OracleDbType.NVarchar2).Value = null;
                }
                //  oraCmd.Parameters.Add("@P_FROM_DATE", OracleDbType.Char).Value = P_FROM_DATE;
                //  oraCmd.Parameters.Add("@P_END_DATE", OracleDbType.Char).Value = P_END_DATE;
                oraCmd.Parameters.Add("@P_RET", OracleDbType.Varchar2, 150);
                oraCmd.Parameters["@P_RET"].Direction = ParameterDirection.Output;
                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

                retcode = oraCmd.Parameters["@P_RET"].Value.ToString();

                return retcode;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GetSPFOR_ACTIVE_FROM_END_DATE(string P_SCREEN_CODE, string P_RECORD_ID, string FROM_DATE, string TO_DATE, int ACTIVE)
        {
            try
            {
                DateTime EndDate = new DateTime();
                string retcode = string.Empty;
                if (TO_DATE != null && TO_DATE.Length > 0)
                    EndDate = Convert.ToDateTime(TO_DATE);

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "PKG_FROM_END_ACTIVE_VALDN.PROC_FROM_DATE_VALDN";
                oraCmd.CommandType = CommandType.StoredProcedure;
                //  oraCmd.Parameters.Add("@P_MODULE_CODE", OracleDbType.Char).Value = DBNull.Value;
                //  oraCmd.Parameters.Add("@P_SCREEN_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_SCREEN_CODE", OracleDbType.NVarchar2).Value = P_SCREEN_CODE;
                oraCmd.Parameters.Add("@P_PK_ID", OracleDbType.NVarchar2).Value = P_RECORD_ID;
                oraCmd.Parameters.Add("@P_FROM_DATE", OracleDbType.NVarchar2).Value = FROM_DATE;
                if (TO_DATE != null && TO_DATE.Length > 0)
                {
                    oraCmd.Parameters.Add("@P_TO_DATE", OracleDbType.NVarchar2).Value = EndDate.ToString("dd/MMM/yyyy");
                }
                else
                {
                    oraCmd.Parameters.Add("@P_TO_DATE", OracleDbType.NVarchar2).Value = null;
                }
                oraCmd.Parameters.Add("@P_ACTIVE", OracleDbType.Int16).Value = ACTIVE;
                oraCmd.Parameters.Add("@P_RET", OracleDbType.Varchar2, 150);
                oraCmd.Parameters["@P_RET"].Direction = ParameterDirection.Output;
                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

                retcode = oraCmd.Parameters["@P_RET"].Value.ToString();

                return retcode;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static void GetSP_GL_Posting(string parameter1, string formCode = "", string str_UserName = "")
        {
            try
            {

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandType = CommandType.StoredProcedure;

                if (formCode != string.Empty && formCode == "AP_014")//GRN Posting
                {
                    oraCmd.CommandText = "GL_POSTING.AP_GRN_POSTING";
                    oraCmd.Parameters.Add(new OracleParameter("@P_RECEIPT_ID", OracleDbType.Varchar2, 250)).Value = parameter1;
                }
                else if (formCode != string.Empty && formCode == "AP_017")//Item Other Cost Posting
                {
                    oraCmd.CommandText = "GL_POSTING.AP_OTHER_COST_POSTING";
                    oraCmd.Parameters.Add(new OracleParameter("@P_RECEIPT_ID", OracleDbType.Varchar2, 250)).Value = parameter1;
                }



                else if (formCode != string.Empty && formCode == "AP_021")//Invoice Posting
                {
                    oraCmd.CommandText = "GL_POSTING.AP_INVOICE_POSTING";
                    oraCmd.Parameters.Add(new OracleParameter("@P_INV_ID", OracleDbType.Varchar2, 250)).Value = parameter1;
                }
                else if (formCode != string.Empty && formCode == "AP_021_R")//invoice stop Posting
                {
                    oraCmd.CommandText = "GL_POSTING.AP_STOP_PAYMENT_POSTING";
                    oraCmd.Parameters.Add(new OracleParameter("@P_PAYMENT_ID", OracleDbType.Varchar2, 250)).Value = parameter1;
                    oraCmd.Parameters.Add(new OracleParameter("@P_FORM", OracleDbType.Varchar2, 250)).Value = formCode;
                    oraCmd.Parameters.Add(new OracleParameter("@P_USER_NAME", OracleDbType.Varchar2, 250)).Value = str_UserName;

                }
                else if (formCode != string.Empty && formCode == "AP_022")//Payment Posting
                {
                    oraCmd.CommandText = "GL_POSTING.AP_PAYMENT_POSTING";
                    oraCmd.Parameters.Add(new OracleParameter("@P_PAYMENT_ID", OracleDbType.Varchar2, 250)).Value = parameter1;
                }
                else if (formCode != string.Empty && formCode == "AP_022_R")//Payment stop Posting
                {
                    oraCmd.CommandText = "GL_POSTING.AP_STOP_PAYMENT_POSTING";
                    oraCmd.Parameters.Add(new OracleParameter("@P_PAYMENT_ID", OracleDbType.Varchar2, 250)).Value = parameter1;
                    oraCmd.Parameters.Add(new OracleParameter("@P_FORM", OracleDbType.Varchar2, 250)).Value = formCode;
                    oraCmd.Parameters.Add(new OracleParameter("@P_USER_NAME", OracleDbType.Varchar2, 250)).Value = str_UserName;
                }




                else if (formCode != string.Empty && formCode == "AP_031")//Payment Posting
                {
                    oraCmd.CommandText = "GL_POSTING.GL_PAYMENT_POSTING";
                    oraCmd.Parameters.Add(new OracleParameter("@P_PAYMENT_ID", OracleDbType.Varchar2, 250)).Value = parameter1;
                }
                else if (formCode != string.Empty && formCode == "AP_031_R")//Payment Posting
                {
                    oraCmd.CommandText = "GL_POSTING.AP_STOP_PAYMENT_POSTING";
                    oraCmd.Parameters.Add(new OracleParameter("@P_PAYMENT_ID", OracleDbType.Varchar2, 250)).Value = parameter1;
                    oraCmd.Parameters.Add(new OracleParameter("@P_FORM", OracleDbType.Varchar2, 250)).Value = formCode;
                    oraCmd.Parameters.Add(new OracleParameter("@P_USER_NAME", OracleDbType.Varchar2, 250)).Value = str_UserName;
                }
                else if (formCode != string.Empty && formCode == "AR_008")//AR Item other cost Posting
                {
                    oraCmd.CommandText = "GL_POSTING.AR_OTHER_COST_POSTING";
                    oraCmd.Parameters.Add(new OracleParameter("@P_PAYMENT_ID", OracleDbType.Varchar2, 250)).Value = parameter1;
                }


                else if (formCode != string.Empty && formCode == "AR_010")//AR Receipts Posting
                {
                    oraCmd.CommandText = "GL_POSTING.AR_RECEIPTS_POSTING";
                    oraCmd.Parameters.Add(new OracleParameter("@P_PAYMENT_ID", OracleDbType.Varchar2, 250)).Value = parameter1;
                }
                else if (formCode != string.Empty && formCode == "AR_010_R")//AR Receipts STOP Posting
                {
                    oraCmd.CommandText = "GL_POSTING.AP_STOP_PAYMENT_POSTING";
                    oraCmd.Parameters.Add(new OracleParameter("@P_PAYMENT_ID", OracleDbType.Varchar2, 250)).Value = parameter1;
                    oraCmd.Parameters.Add(new OracleParameter("@P_FORM", OracleDbType.Varchar2, 250)).Value = formCode;
                    oraCmd.Parameters.Add(new OracleParameter("@P_USER_NAME", OracleDbType.Varchar2, 250)).Value = str_UserName;
                }


                else if (formCode != string.Empty && formCode == "AR_009")//AR Invoice Posting
                {
                    oraCmd.CommandText = "GL_POSTING.AR_INVOICE_POSTING";
                    oraCmd.Parameters.Add(new OracleParameter("@P_INV_ID", OracleDbType.Varchar2, 250)).Value = parameter1;
                }
                else if (formCode != string.Empty && formCode == "AR_009_R")//AR Invoice Stop Posting
                {
                    oraCmd.CommandText = "GL_POSTING.AP_STOP_PAYMENT_POSTING";
                    oraCmd.Parameters.Add(new OracleParameter("@P_INV_ID", OracleDbType.Varchar2, 250)).Value = parameter1;
                    oraCmd.Parameters.Add(new OracleParameter("@P_FORM", OracleDbType.Varchar2, 250)).Value = formCode;
                    oraCmd.Parameters.Add(new OracleParameter("@P_USER_NAME", OracleDbType.Varchar2, 250)).Value = str_UserName;
                }




                else if (formCode != string.Empty && formCode == "AP_024")//Micellaneous Posting
                {
                    oraCmd.CommandText = "GL_POSTING.AP_MICELLANEOUS_POSTING";
                    oraCmd.Parameters.Add(new OracleParameter("@P_RECEIPT_ID", OracleDbType.Varchar2, 250)).Value = parameter1;
                }

                else if (formCode != string.Empty && formCode == "CA_008")//PETTY_CASH_EXPEND_POSTING
                {
                    oraCmd.CommandText = "GL_POSTING.PETTY_CASH_EXPEND_POSTING";
                    oraCmd.Parameters.Add(new OracleParameter("@P_GL_PCE_DTL_ID", OracleDbType.Varchar2, 250)).Value = parameter1;
                }
                else if (formCode != string.Empty && formCode == "CA_009")//PETTY_CASH_EXPEND_POSTING
                {
                    oraCmd.CommandText = "GL_POSTING.PETTY_CASH_ALOCATE_POSTING";
                    oraCmd.Parameters.Add(new OracleParameter("@P_GL_PCE_DTL_ID", OracleDbType.Varchar2, 250)).Value = parameter1;
                }

                else if (formCode != string.Empty && formCode == "FA_005")//FA Asset Entry
                {
                    oraCmd.CommandText = "GL_POSTING.FA_ASSET_MST_POSTING";
                    oraCmd.Parameters.Add(new OracleParameter("@P_ASSET_ID", OracleDbType.Varchar2, 250)).Value = parameter1;
                }

                else if (formCode != string.Empty && formCode == "FA_006")//FA Depreciation
                {
                    oraCmd.CommandText = "GL_POSTING.FA_ASSET_DEPRE_POSTING";
                    oraCmd.Parameters.Add(new OracleParameter("@P_ASSET_DEPRE_ID", OracleDbType.Varchar2, 250)).Value = parameter1;
                }


                else if (formCode != string.Empty && formCode == "PR_014")//Paroll Posting
                {
                    oraCmd.CommandText = "GL_POSTING.PAYROLL_POSTING";
                    oraCmd.Parameters.Add(new OracleParameter("@P_PAYROLL_ID", OracleDbType.Varchar2, 250)).Value = parameter1;
                }
                else if (formCode != string.Empty && formCode == "HR_034")//Loan Request
                {
                    oraCmd.CommandText = "GL_POSTING.HR_EMP_LOAN_REQ_POSTING";
                    oraCmd.Parameters.Add(new OracleParameter("@P_REQ_ID", OracleDbType.Varchar2, 250)).Value = parameter1;
                }
                else if (formCode != string.Empty && formCode == "HR_097")//Annual Leave
                {
                    oraCmd.CommandText = "GL_POSTING.HR_LEAVE_ENCASH_POSTING";
                    oraCmd.Parameters.Add(new OracleParameter("@P_LC_ID", OracleDbType.Varchar2, 250)).Value = parameter1;
                }
                else if (formCode != string.Empty && formCode == "HR_098")//lOAN employee Payment
                {
                    oraCmd.CommandText = "GL_POSTING.HR_EMP_LOAN_PAYMENT_POSTING";
                    oraCmd.Parameters.Add(new OracleParameter("@P_REQ_ID", OracleDbType.Varchar2, 250)).Value = parameter1;
                }


                else if (formCode != string.Empty && formCode == "LN_008")//lLoan Provision
                {
                    oraCmd.CommandText = "GL_POSTING.HR_LOAN_PROVISION_POSTING";
                    oraCmd.Parameters.Add(new OracleParameter("@P_LN_PROVISION_ID", OracleDbType.Varchar2, 250)).Value = parameter1;
                }
                else if (formCode != string.Empty && formCode == "LN_011")//lOAN Payment
                {
                    oraCmd.CommandText = "GL_POSTING.HR_LOAN_PAYMENT_POSTING";
                    oraCmd.Parameters.Add(new OracleParameter("@P_ln_con_pay_id", OracleDbType.Varchar2, 250)).Value = parameter1;
                }
                else if (formCode != string.Empty && formCode == "LN_001")//lOAN Contract Details
                {
                    oraCmd.CommandText = "GL_POSTING.HR_LOAN_CONTRACT_POSTING";
                    oraCmd.Parameters.Add(new OracleParameter("@P_LN_CONTRACT_ID", OracleDbType.Varchar2, 250)).Value = parameter1;
                }
                //Maisonette Posting
                else if (formCode != string.Empty && formCode == "SSM_029")//Maisonette posting - AP
                {
                    oraCmd.CommandText = "inv_pkg.inv_interface_msnet";
                    oraCmd.Parameters.Add(new OracleParameter("@p_acct_id", OracleDbType.Varchar2, 250)).Value = parameter1;                 
                }
                else if (formCode != string.Empty && formCode == "SSM_031")//Maisonette posting - AR
                {
                    oraCmd.CommandText = "inv_pkg.inv_interface_msnet_ar";
                    oraCmd.Parameters.Add(new OracleParameter("@p_acct_id", OracleDbType.Varchar2, 250)).Value = parameter1;
                }
                else if (formCode != string.Empty && formCode == "SSM_032")//Maisonette posting - AR Receivable
                {
                    oraCmd.CommandText = "inv_pkg.inv_intf_msnet_ar_receipt";
                    oraCmd.Parameters.Add(new OracleParameter("@p_collection_id", OracleDbType.Varchar2, 250)).Value = parameter1;
                }
                else if (formCode != string.Empty && formCode == "SSM_037")//Maisonette posting - GL
                {
                    oraCmd.CommandText = "gl_pkg.gl_masonet_JE_import";
                    oraCmd.Parameters.Add(new OracleParameter("@p_transaction_id", OracleDbType.Varchar2, 250)).Value = parameter1;
                }
                else if (formCode != string.Empty && formCode == "SSM_029_M")//Maisonette posting - AP Vendor Mapping
                {
                    oraCmd.CommandText = "inv_pkg.populate_mapping_vendor";
                }
                else if (formCode != string.Empty && formCode == "SSM_031_M")//Maisonette posting - AR Customer Mapping
                {
                    oraCmd.CommandText = "inv_pkg.populate_mapping_customer";
                }
                //Sierra Posting
                else if (formCode != string.Empty && formCode == "SSM_033")//Sierra posting - AP Invoice
                {
                    oraCmd.CommandText = "inv_pkg.prc_intf_sierra_sup_inv";
                    oraCmd.Parameters.Add(new OracleParameter("@p_inv_id", OracleDbType.Varchar2, 250)).Value = parameter1;
                }
                else if (formCode != string.Empty && formCode == "SSM_034")//Sierra posting - AP Payment
                {
                    oraCmd.CommandText = "inv_pkg.prc_intf_sierra_sup_payment";
                    oraCmd.Parameters.Add(new OracleParameter("@p_payment_id", OracleDbType.Varchar2, 250)).Value = parameter1;
                }
                else if (formCode != string.Empty && formCode == "SSM_035")//Sierra posting - AR Invoice
                {
                    oraCmd.CommandText = "inv_pkg.prc_intf_sierra_cust_inv";
                    oraCmd.Parameters.Add(new OracleParameter("@p_inv_id", OracleDbType.Varchar2, 250)).Value = parameter1;
                }
                else if (formCode != string.Empty && formCode == "SSM_036")//Sierra posting - AR Payment
                {
                    oraCmd.CommandText = "inv_pkg.prc_intf_sierra_cust_receipt";
                    oraCmd.Parameters.Add(new OracleParameter("@p_receipt_id", OracleDbType.Varchar2, 250)).Value = parameter1;
                }
               
                
                if (formCode != string.Empty)
                {
                    oraCmd.Parameters.Add(new OracleParameter("@P_ORG_ID", OracleDbType.Varchar2, 250)).Value = VMVServices.Web.Utils.OrganizationID;
                }

                DBMethod.ExecuteFunction(oraCmd);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void Call_AP_INVOICE_GL_POSTING(string invId)
        {
            try
            {
                string strValue = string.Empty;
                string strNextCode = string.Empty;
                string strErrMsg = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "GL_POSTING.AP_INVOICE_POSTING";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add("@P_INV_ID", OracleDbType.Varchar2).Value = invId;
                oraCmd.Parameters.Add("@p_org_id", OracleDbType.Varchar2).Value = VMVServices.Web.Utils.OrganizationID;
                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static string GetSPFOR_PAY_EMP_LEAVE_SALARY(string P_PAY_PERIOD, string P_DEPT_ID, string P_EMP_ID, string P_ORG_ID, DateTime P_FROM_DT, DateTime P_TO_DT)
        {
            try
            {
                string retcode = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "PKG_PAYROLL.LEAVE_SALARY";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add("@p_pay_period", OracleDbType.Varchar2).Value = P_PAY_PERIOD;
                oraCmd.Parameters.Add("@p_dept_id", OracleDbType.Varchar2).Value = P_DEPT_ID;
                oraCmd.Parameters.Add("@p_emp_id", OracleDbType.Varchar2).Value = P_EMP_ID;
                oraCmd.Parameters.Add("@p_org_id", OracleDbType.Varchar2).Value = P_ORG_ID;
                oraCmd.Parameters.Add(new OracleParameter("@p_from_dt", OracleDbType.Date, 250)).Value = P_FROM_DT.ToString("dd/MMM/yyyy");
                oraCmd.Parameters.Add(new OracleParameter("@p_to_dt", OracleDbType.Date, 250)).Value = P_TO_DT.ToString("dd/MMM/yyyy");
                oraCmd.Parameters.Add("@P_RET", OracleDbType.Varchar2, 150);
                oraCmd.Parameters["@P_RET"].Direction = ParameterDirection.Output;

                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

                retcode = oraCmd.Parameters["@P_RET"].Value.ToString();

                return retcode;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static string GetSPFOR_PROC_EMP_LOP_FOR_LATE(string P_GRP_CODE, string P_EMP_ID, string P_DATE, string P_DEPT_ID, string P_LATE_TYP)
        {
            try
            {
                string retcode = string.Empty;
                DateTime PDate = new DateTime();

                if (P_DATE != null && P_DATE.Length > 0)
                    PDate = Convert.ToDateTime(P_DATE);

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "HR_PKG.PROC_EMP_LOP_FOR_LATE";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add("@P_GRP_CODE", OracleDbType.Varchar2).Value = P_GRP_CODE;
                oraCmd.Parameters.Add("@P_EMP_ID", OracleDbType.Varchar2).Value = P_EMP_ID;
                oraCmd.Parameters.Add(new OracleParameter("@P_DATE", OracleDbType.Date, 250)).Value = PDate.ToString("dd/MMM/yyyy");
                oraCmd.Parameters.Add("@P_LATE_TYP", OracleDbType.Varchar2).Value = P_LATE_TYP;
  

                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

                return retcode;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        public static decimal GetPAY_EMP_LEAVE_SALARY(string P_PAY_PERIOD, string P_DEPT_ID, string P_EMP_ID, string P_ORG_ID, DateTime P_FROM_DT, DateTime P_TO_DT)
        {
            try
            {
                decimal retVal = 0;

                retVal = DBMethod.GetDecimalValue("SELECT round(PKG_PAYROLL.LEAVE_SALARY('" + P_PAY_PERIOD + "','" + P_DEPT_ID + "','" + P_EMP_ID + "','" + P_ORG_ID + "','" + P_FROM_DT.ToString("dd/MMM/yyyy") + "','" + P_TO_DT.ToString("dd/MMM/yyyy") + "')," + VMVServices.Web.Utils.DecimalPrecision + ") from dual");

                return retVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static void GetPAY_EMP_LEAVE_OLD_SALARY(string P_DEPT_ID, string P_EMP_ID, string P_ORG_ID, string toDate)
        {
            try
            {
                string query = string.Empty;

                string fromDate = string.Empty;

                DateTime P_TO_DT = Convert.ToDateTime(toDate);


                //query = "select trunc((max(ph.payroll_date))+1) as from_date from pay_final_run_hdr ph,pay_final_run_emp_dtl em";
                //query += " where ph.payroll_id=em.payroll_id";
                //query += " and em.emp_id='" + P_EMP_ID + "'";

                query += "   select nvl(ph.payroll_cut_off_dt,ph.pay_to_dt)+1  as from_date from pay_periods ph,pay_final_run_emp_dtl em";
                query += "  where ph.pay_period_id=em.payroll_period";
                query += "  and em.emp_id='" + P_EMP_ID + "'";
                query += "  and rownum=1 order by ph.pay_from_dt desc";

                DataTable dtFromDate = new DataTable();
                dtFromDate = DBMethod.ExecuteQuery(query).Tables[0];

                if (dtFromDate != null)
                {
                    if (dtFromDate.Rows.Count > 0)
                    {
                        if (dtFromDate.Rows[0]["from_date"].ToString() != string.Empty)
                        {
                            fromDate = dtFromDate.Rows[0]["from_date"].ToString();

                            DateTime P_FROM_DT = Convert.ToDateTime(fromDate);

                            //  DBMethod.GetDecimalValue("SELECT round(nvl(PKG_PAYROLL.leave_salary_old('','" + P_DEPT_ID + "','" + P_EMP_ID + "','" + P_ORG_ID + "','" + P_FROM_DT.ToString("dd/MMM/yyyy") + "','" + P_TO_DT.ToString("dd/MMM/yyyy") + "'),0)," + VMVServices.Web.Utils.DecimalPrecision + ") from dual");


                            OracleCommand oraCmd = new OracleCommand();
                            oraCmd.CommandText = "PKG_PAYROLL.final_settlement";
                            oraCmd.CommandType = CommandType.StoredProcedure;
                            oraCmd.Parameters.Add("@p_pay_period", OracleDbType.Varchar2).Value = "";
                            oraCmd.Parameters.Add("@p_dept_id", OracleDbType.Varchar2).Value = P_DEPT_ID;
                            oraCmd.Parameters.Add("@p_emp_id", OracleDbType.Varchar2).Value = P_EMP_ID;
                            oraCmd.Parameters.Add("@p_org_id", OracleDbType.Varchar2).Value = P_ORG_ID;
                            oraCmd.Parameters.Add(new OracleParameter("@p_from_dt", OracleDbType.Date, 250)).Value = P_FROM_DT.ToString("dd/MMM/yyyy");
                            oraCmd.Parameters.Add(new OracleParameter("@p_to_dt", OracleDbType.Date, 250)).Value = P_TO_DT.ToString("dd/MMM/yyyy");


                            oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
