//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.Serialization;

namespace FIN.DAL
{
    [DataContract(IsReference = true)]
    [KnownType(typeof(HR_TRM_ENROLL_DTL))]
    public partial class HR_TRM_TRAINER: IObjectWithChangeTracker, INotifyPropertyChanged
    {
        #region Primitive Properties
    
        [DataMember]
        public Nullable<int> PK_ID
        {
            get { return _pK_ID; }
            set
            {
                if (_pK_ID != value)
                {
                    _pK_ID = value;
                    OnPropertyChanged("PK_ID");
                }
            }
        }
        private Nullable<int> _pK_ID;
    
        [DataMember]
        public Nullable<int> CHILD_ID
        {
            get { return _cHILD_ID; }
            set
            {
                if (_cHILD_ID != value)
                {
                    _cHILD_ID = value;
                    OnPropertyChanged("CHILD_ID");
                }
            }
        }
        private Nullable<int> _cHILD_ID;
    
        [DataMember]
        public string TRNR_ID
        {
            get { return _tRNR_ID; }
            set
            {
                if (_tRNR_ID != value)
                {
                    if (ChangeTracker.ChangeTrackingEnabled && ChangeTracker.State != ObjectState.Added)
                    {
                        throw new InvalidOperationException("The property 'TRNR_ID' is part of the object's key and cannot be changed. Changes to key properties can only be made when the object is not being tracked or is in the Added state.");
                    }
                    _tRNR_ID = value;
                    OnPropertyChanged("TRNR_ID");
                }
            }
        }
        private string _tRNR_ID;
    
        [DataMember]
        public string TRNR_NAME
        {
            get { return _tRNR_NAME; }
            set
            {
                if (_tRNR_NAME != value)
                {
                    _tRNR_NAME = value;
                    OnPropertyChanged("TRNR_NAME");
                }
            }
        }
        private string _tRNR_NAME;
    
        [DataMember]
        public string TRNR_TYPE
        {
            get { return _tRNR_TYPE; }
            set
            {
                if (_tRNR_TYPE != value)
                {
                    _tRNR_TYPE = value;
                    OnPropertyChanged("TRNR_TYPE");
                }
            }
        }
        private string _tRNR_TYPE;
    
        [DataMember]
        public string TRNR_EMP_ID
        {
            get { return _tRNR_EMP_ID; }
            set
            {
                if (_tRNR_EMP_ID != value)
                {
                    _tRNR_EMP_ID = value;
                    OnPropertyChanged("TRNR_EMP_ID");
                }
            }
        }
        private string _tRNR_EMP_ID;
    
        [DataMember]
        public string TRNR_PROFILE
        {
            get { return _tRNR_PROFILE; }
            set
            {
                if (_tRNR_PROFILE != value)
                {
                    _tRNR_PROFILE = value;
                    OnPropertyChanged("TRNR_PROFILE");
                }
            }
        }
        private string _tRNR_PROFILE;
    
        [DataMember]
        public Nullable<decimal> TRNR_EXPERIENCE
        {
            get { return _tRNR_EXPERIENCE; }
            set
            {
                if (_tRNR_EXPERIENCE != value)
                {
                    _tRNR_EXPERIENCE = value;
                    OnPropertyChanged("TRNR_EXPERIENCE");
                }
            }
        }
        private Nullable<decimal> _tRNR_EXPERIENCE;
    
        [DataMember]
        public string TRNR_CONS_NAME
        {
            get { return _tRNR_CONS_NAME; }
            set
            {
                if (_tRNR_CONS_NAME != value)
                {
                    _tRNR_CONS_NAME = value;
                    OnPropertyChanged("TRNR_CONS_NAME");
                }
            }
        }
        private string _tRNR_CONS_NAME;
    
        [DataMember]
        public string TRNR_CONS_PERSON
        {
            get { return _tRNR_CONS_PERSON; }
            set
            {
                if (_tRNR_CONS_PERSON != value)
                {
                    _tRNR_CONS_PERSON = value;
                    OnPropertyChanged("TRNR_CONS_PERSON");
                }
            }
        }
        private string _tRNR_CONS_PERSON;
    
        [DataMember]
        public string TRNR_CONS_CONTACT_NO
        {
            get { return _tRNR_CONS_CONTACT_NO; }
            set
            {
                if (_tRNR_CONS_CONTACT_NO != value)
                {
                    _tRNR_CONS_CONTACT_NO = value;
                    OnPropertyChanged("TRNR_CONS_CONTACT_NO");
                }
            }
        }
        private string _tRNR_CONS_CONTACT_NO;
    
        [DataMember]
        public string ENABLED_FLAG
        {
            get { return _eNABLED_FLAG; }
            set
            {
                if (_eNABLED_FLAG != value)
                {
                    _eNABLED_FLAG = value;
                    OnPropertyChanged("ENABLED_FLAG");
                }
            }
        }
        private string _eNABLED_FLAG;
    
        [DataMember]
        public string WORKFLOW_COMPLETION_STATUS
        {
            get { return _wORKFLOW_COMPLETION_STATUS; }
            set
            {
                if (_wORKFLOW_COMPLETION_STATUS != value)
                {
                    _wORKFLOW_COMPLETION_STATUS = value;
                    OnPropertyChanged("WORKFLOW_COMPLETION_STATUS");
                }
            }
        }
        private string _wORKFLOW_COMPLETION_STATUS;
    
        [DataMember]
        public string CREATED_BY
        {
            get { return _cREATED_BY; }
            set
            {
                if (_cREATED_BY != value)
                {
                    _cREATED_BY = value;
                    OnPropertyChanged("CREATED_BY");
                }
            }
        }
        private string _cREATED_BY;
    
        [DataMember]
        public System.DateTime CREATED_DATE
        {
            get { return _cREATED_DATE; }
            set
            {
                if (_cREATED_DATE != value)
                {
                    _cREATED_DATE = value;
                    OnPropertyChanged("CREATED_DATE");
                }
            }
        }
        private System.DateTime _cREATED_DATE;
    
        [DataMember]
        public string MODIFIED_BY
        {
            get { return _mODIFIED_BY; }
            set
            {
                if (_mODIFIED_BY != value)
                {
                    _mODIFIED_BY = value;
                    OnPropertyChanged("MODIFIED_BY");
                }
            }
        }
        private string _mODIFIED_BY;
    
        [DataMember]
        public Nullable<System.DateTime> MODIFIED_DATE
        {
            get { return _mODIFIED_DATE; }
            set
            {
                if (_mODIFIED_DATE != value)
                {
                    _mODIFIED_DATE = value;
                    OnPropertyChanged("MODIFIED_DATE");
                }
            }
        }
        private Nullable<System.DateTime> _mODIFIED_DATE;
    
        [DataMember]
        public string ATTRIBUTE1
        {
            get { return _aTTRIBUTE1; }
            set
            {
                if (_aTTRIBUTE1 != value)
                {
                    _aTTRIBUTE1 = value;
                    OnPropertyChanged("ATTRIBUTE1");
                }
            }
        }
        private string _aTTRIBUTE1;
    
        [DataMember]
        public string ATTRIBUTE2
        {
            get { return _aTTRIBUTE2; }
            set
            {
                if (_aTTRIBUTE2 != value)
                {
                    _aTTRIBUTE2 = value;
                    OnPropertyChanged("ATTRIBUTE2");
                }
            }
        }
        private string _aTTRIBUTE2;
    
        [DataMember]
        public string ATTRIBUTE3
        {
            get { return _aTTRIBUTE3; }
            set
            {
                if (_aTTRIBUTE3 != value)
                {
                    _aTTRIBUTE3 = value;
                    OnPropertyChanged("ATTRIBUTE3");
                }
            }
        }
        private string _aTTRIBUTE3;
    
        [DataMember]
        public string ATTRIBUTE4
        {
            get { return _aTTRIBUTE4; }
            set
            {
                if (_aTTRIBUTE4 != value)
                {
                    _aTTRIBUTE4 = value;
                    OnPropertyChanged("ATTRIBUTE4");
                }
            }
        }
        private string _aTTRIBUTE4;
    
        [DataMember]
        public string ATTRIBUTE5
        {
            get { return _aTTRIBUTE5; }
            set
            {
                if (_aTTRIBUTE5 != value)
                {
                    _aTTRIBUTE5 = value;
                    OnPropertyChanged("ATTRIBUTE5");
                }
            }
        }
        private string _aTTRIBUTE5;
    
        [DataMember]
        public string ATTRIBUTE6
        {
            get { return _aTTRIBUTE6; }
            set
            {
                if (_aTTRIBUTE6 != value)
                {
                    _aTTRIBUTE6 = value;
                    OnPropertyChanged("ATTRIBUTE6");
                }
            }
        }
        private string _aTTRIBUTE6;
    
        [DataMember]
        public string ATTRIBUTE7
        {
            get { return _aTTRIBUTE7; }
            set
            {
                if (_aTTRIBUTE7 != value)
                {
                    _aTTRIBUTE7 = value;
                    OnPropertyChanged("ATTRIBUTE7");
                }
            }
        }
        private string _aTTRIBUTE7;
    
        [DataMember]
        public string ATTRIBUTE8
        {
            get { return _aTTRIBUTE8; }
            set
            {
                if (_aTTRIBUTE8 != value)
                {
                    _aTTRIBUTE8 = value;
                    OnPropertyChanged("ATTRIBUTE8");
                }
            }
        }
        private string _aTTRIBUTE8;
    
        [DataMember]
        public string ATTRIBUTE9
        {
            get { return _aTTRIBUTE9; }
            set
            {
                if (_aTTRIBUTE9 != value)
                {
                    _aTTRIBUTE9 = value;
                    OnPropertyChanged("ATTRIBUTE9");
                }
            }
        }
        private string _aTTRIBUTE9;
    
        [DataMember]
        public string ATTRIBUTE10
        {
            get { return _aTTRIBUTE10; }
            set
            {
                if (_aTTRIBUTE10 != value)
                {
                    _aTTRIBUTE10 = value;
                    OnPropertyChanged("ATTRIBUTE10");
                }
            }
        }
        private string _aTTRIBUTE10;
    
        [DataMember]
        public string TRNR_ORG_ID
        {
            get { return _tRNR_ORG_ID; }
            set
            {
                if (_tRNR_ORG_ID != value)
                {
                    _tRNR_ORG_ID = value;
                    OnPropertyChanged("TRNR_ORG_ID");
                }
            }
        }
        private string _tRNR_ORG_ID;

        #endregion
        #region Navigation Properties
    
        [DataMember]
        public TrackableCollection<HR_TRM_ENROLL_DTL> HR_TRM_ENROLL_DTL
        {
            get
            {
                if (_hR_TRM_ENROLL_DTL == null)
                {
                    _hR_TRM_ENROLL_DTL = new TrackableCollection<HR_TRM_ENROLL_DTL>();
                    _hR_TRM_ENROLL_DTL.CollectionChanged += FixupHR_TRM_ENROLL_DTL;
                }
                return _hR_TRM_ENROLL_DTL;
            }
            set
            {
                if (!ReferenceEquals(_hR_TRM_ENROLL_DTL, value))
                {
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        throw new InvalidOperationException("Cannot set the FixupChangeTrackingCollection when ChangeTracking is enabled");
                    }
                    if (_hR_TRM_ENROLL_DTL != null)
                    {
                        _hR_TRM_ENROLL_DTL.CollectionChanged -= FixupHR_TRM_ENROLL_DTL;
                    }
                    _hR_TRM_ENROLL_DTL = value;
                    if (_hR_TRM_ENROLL_DTL != null)
                    {
                        _hR_TRM_ENROLL_DTL.CollectionChanged += FixupHR_TRM_ENROLL_DTL;
                    }
                    OnNavigationPropertyChanged("HR_TRM_ENROLL_DTL");
                }
            }
        }
        private TrackableCollection<HR_TRM_ENROLL_DTL> _hR_TRM_ENROLL_DTL;

        #endregion
        #region ChangeTracking
    
        protected virtual void OnPropertyChanged(String propertyName)
        {
            if (ChangeTracker.State != ObjectState.Added && ChangeTracker.State != ObjectState.Deleted)
            {
                ChangeTracker.State = ObjectState.Modified;
            }
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        protected virtual void OnNavigationPropertyChanged(String propertyName)
        {
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        event PropertyChangedEventHandler INotifyPropertyChanged.PropertyChanged{ add { _propertyChanged += value; } remove { _propertyChanged -= value; } }
        private event PropertyChangedEventHandler _propertyChanged;
        private ObjectChangeTracker _changeTracker;
    
        [DataMember]
        public ObjectChangeTracker ChangeTracker
        {
            get
            {
                if (_changeTracker == null)
                {
                    _changeTracker = new ObjectChangeTracker();
                    _changeTracker.ObjectStateChanging += HandleObjectStateChanging;
                }
                return _changeTracker;
            }
            set
            {
                if(_changeTracker != null)
                {
                    _changeTracker.ObjectStateChanging -= HandleObjectStateChanging;
                }
                _changeTracker = value;
                if(_changeTracker != null)
                {
                    _changeTracker.ObjectStateChanging += HandleObjectStateChanging;
                }
            }
        }
    
        private void HandleObjectStateChanging(object sender, ObjectStateChangingEventArgs e)
        {
            if (e.NewState == ObjectState.Deleted)
            {
                ClearNavigationProperties();
            }
        }
    
        protected bool IsDeserializing { get; private set; }
    
        [OnDeserializing]
        public void OnDeserializingMethod(StreamingContext context)
        {
            IsDeserializing = true;
        }
    
        [OnDeserialized]
        public void OnDeserializedMethod(StreamingContext context)
        {
            IsDeserializing = false;
            ChangeTracker.ChangeTrackingEnabled = true;
        }
    
        protected virtual void ClearNavigationProperties()
        {
            HR_TRM_ENROLL_DTL.Clear();
        }

        #endregion
        #region Association Fixup
    
        private void FixupHR_TRM_ENROLL_DTL(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (e.NewItems != null)
            {
                foreach (HR_TRM_ENROLL_DTL item in e.NewItems)
                {
                    item.HR_TRM_TRAINER = this;
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        if (!item.ChangeTracker.ChangeTrackingEnabled)
                        {
                            item.StartTracking();
                        }
                        ChangeTracker.RecordAdditionToCollectionProperties("HR_TRM_ENROLL_DTL", item);
                    }
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (HR_TRM_ENROLL_DTL item in e.OldItems)
                {
                    if (ReferenceEquals(item.HR_TRM_TRAINER, this))
                    {
                        item.HR_TRM_TRAINER = null;
                    }
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        ChangeTracker.RecordRemovalFromCollectionProperties("HR_TRM_ENROLL_DTL", item);
                    }
                }
            }
        }

        #endregion
    }
}
