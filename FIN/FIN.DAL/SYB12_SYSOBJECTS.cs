//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.Serialization;

namespace FIN.DAL
{
    [DataContract(IsReference = true)]
    public partial class SYB12_SYSOBJECTS: IObjectWithChangeTracker, INotifyPropertyChanged
    {
        #region Primitive Properties
    
        [DataMember]
        public Nullable<int> DB_ID
        {
            get { return _dB_ID; }
            set
            {
                if (_dB_ID != value)
                {
                    _dB_ID = value;
                    OnPropertyChanged("DB_ID");
                }
            }
        }
        private Nullable<int> _dB_ID;
    
        [DataMember]
        public string NAME
        {
            get { return _nAME; }
            set
            {
                if (_nAME != value)
                {
                    _nAME = value;
                    OnPropertyChanged("NAME");
                }
            }
        }
        private string _nAME;
    
        [DataMember]
        public int ID
        {
            get { return _iD; }
            set
            {
                if (_iD != value)
                {
                    if (ChangeTracker.ChangeTrackingEnabled && ChangeTracker.State != ObjectState.Added)
                    {
                        throw new InvalidOperationException("The property 'ID' is part of the object's key and cannot be changed. Changes to key properties can only be made when the object is not being tracked or is in the Added state.");
                    }
                    _iD = value;
                    OnPropertyChanged("ID");
                }
            }
        }
        private int _iD;
    
        [DataMember]
        public Nullable<int> DB_UID
        {
            get { return _dB_UID; }
            set
            {
                if (_dB_UID != value)
                {
                    _dB_UID = value;
                    OnPropertyChanged("DB_UID");
                }
            }
        }
        private Nullable<int> _dB_UID;
    
        [DataMember]
        public string DB_TYPE
        {
            get { return _dB_TYPE; }
            set
            {
                if (_dB_TYPE != value)
                {
                    _dB_TYPE = value;
                    OnPropertyChanged("DB_TYPE");
                }
            }
        }
        private string _dB_TYPE;
    
        [DataMember]
        public Nullable<int> USERSTAT
        {
            get { return _uSERSTAT; }
            set
            {
                if (_uSERSTAT != value)
                {
                    _uSERSTAT = value;
                    OnPropertyChanged("USERSTAT");
                }
            }
        }
        private Nullable<int> _uSERSTAT;
    
        [DataMember]
        public Nullable<int> SYSSTAT
        {
            get { return _sYSSTAT; }
            set
            {
                if (_sYSSTAT != value)
                {
                    _sYSSTAT = value;
                    OnPropertyChanged("SYSSTAT");
                }
            }
        }
        private Nullable<int> _sYSSTAT;
    
        [DataMember]
        public Nullable<int> INDEXDEL
        {
            get { return _iNDEXDEL; }
            set
            {
                if (_iNDEXDEL != value)
                {
                    _iNDEXDEL = value;
                    OnPropertyChanged("INDEXDEL");
                }
            }
        }
        private Nullable<int> _iNDEXDEL;
    
        [DataMember]
        public Nullable<int> SCHEMATACNT
        {
            get { return _sCHEMATACNT; }
            set
            {
                if (_sCHEMATACNT != value)
                {
                    _sCHEMATACNT = value;
                    OnPropertyChanged("SCHEMATACNT");
                }
            }
        }
        private Nullable<int> _sCHEMATACNT;
    
        [DataMember]
        public Nullable<int> SYSSTAT2
        {
            get { return _sYSSTAT2; }
            set
            {
                if (_sYSSTAT2 != value)
                {
                    _sYSSTAT2 = value;
                    OnPropertyChanged("SYSSTAT2");
                }
            }
        }
        private Nullable<int> _sYSSTAT2;
    
        [DataMember]
        public string CRDATE
        {
            get { return _cRDATE; }
            set
            {
                if (_cRDATE != value)
                {
                    _cRDATE = value;
                    OnPropertyChanged("CRDATE");
                }
            }
        }
        private string _cRDATE;
    
        [DataMember]
        public string EXPDATE
        {
            get { return _eXPDATE; }
            set
            {
                if (_eXPDATE != value)
                {
                    _eXPDATE = value;
                    OnPropertyChanged("EXPDATE");
                }
            }
        }
        private string _eXPDATE;
    
        [DataMember]
        public Nullable<int> DELTRIG
        {
            get { return _dELTRIG; }
            set
            {
                if (_dELTRIG != value)
                {
                    _dELTRIG = value;
                    OnPropertyChanged("DELTRIG");
                }
            }
        }
        private Nullable<int> _dELTRIG;
    
        [DataMember]
        public Nullable<int> INSTRIG
        {
            get { return _iNSTRIG; }
            set
            {
                if (_iNSTRIG != value)
                {
                    _iNSTRIG = value;
                    OnPropertyChanged("INSTRIG");
                }
            }
        }
        private Nullable<int> _iNSTRIG;
    
        [DataMember]
        public Nullable<int> UPDTRIG
        {
            get { return _uPDTRIG; }
            set
            {
                if (_uPDTRIG != value)
                {
                    _uPDTRIG = value;
                    OnPropertyChanged("UPDTRIG");
                }
            }
        }
        private Nullable<int> _uPDTRIG;
    
        [DataMember]
        public Nullable<int> SELTRIG
        {
            get { return _sELTRIG; }
            set
            {
                if (_sELTRIG != value)
                {
                    _sELTRIG = value;
                    OnPropertyChanged("SELTRIG");
                }
            }
        }
        private Nullable<int> _sELTRIG;
    
        [DataMember]
        public Nullable<int> CKFIRST
        {
            get { return _cKFIRST; }
            set
            {
                if (_cKFIRST != value)
                {
                    _cKFIRST = value;
                    OnPropertyChanged("CKFIRST");
                }
            }
        }
        private Nullable<int> _cKFIRST;
    
        [DataMember]
        public Nullable<int> DB_CACHE
        {
            get { return _dB_CACHE; }
            set
            {
                if (_dB_CACHE != value)
                {
                    _dB_CACHE = value;
                    OnPropertyChanged("DB_CACHE");
                }
            }
        }
        private Nullable<int> _dB_CACHE;
    
        [DataMember]
        public Nullable<int> AUDFLAGS
        {
            get { return _aUDFLAGS; }
            set
            {
                if (_aUDFLAGS != value)
                {
                    _aUDFLAGS = value;
                    OnPropertyChanged("AUDFLAGS");
                }
            }
        }
        private Nullable<int> _aUDFLAGS;
    
        [DataMember]
        public Nullable<int> OBJSPARE
        {
            get { return _oBJSPARE; }
            set
            {
                if (_oBJSPARE != value)
                {
                    _oBJSPARE = value;
                    OnPropertyChanged("OBJSPARE");
                }
            }
        }
        private Nullable<int> _oBJSPARE;
    
        [DataMember]
        public byte[] VERSIONTS
        {
            get { return _vERSIONTS; }
            set
            {
                if (_vERSIONTS != value)
                {
                    _vERSIONTS = value;
                    OnPropertyChanged("VERSIONTS");
                }
            }
        }
        private byte[] _vERSIONTS;
    
        [DataMember]
        public string LOGINNAME
        {
            get { return _lOGINNAME; }
            set
            {
                if (_lOGINNAME != value)
                {
                    _lOGINNAME = value;
                    OnPropertyChanged("LOGINNAME");
                }
            }
        }
        private string _lOGINNAME;

        #endregion
        #region ChangeTracking
    
        protected virtual void OnPropertyChanged(String propertyName)
        {
            if (ChangeTracker.State != ObjectState.Added && ChangeTracker.State != ObjectState.Deleted)
            {
                ChangeTracker.State = ObjectState.Modified;
            }
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        protected virtual void OnNavigationPropertyChanged(String propertyName)
        {
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        event PropertyChangedEventHandler INotifyPropertyChanged.PropertyChanged{ add { _propertyChanged += value; } remove { _propertyChanged -= value; } }
        private event PropertyChangedEventHandler _propertyChanged;
        private ObjectChangeTracker _changeTracker;
    
        [DataMember]
        public ObjectChangeTracker ChangeTracker
        {
            get
            {
                if (_changeTracker == null)
                {
                    _changeTracker = new ObjectChangeTracker();
                    _changeTracker.ObjectStateChanging += HandleObjectStateChanging;
                }
                return _changeTracker;
            }
            set
            {
                if(_changeTracker != null)
                {
                    _changeTracker.ObjectStateChanging -= HandleObjectStateChanging;
                }
                _changeTracker = value;
                if(_changeTracker != null)
                {
                    _changeTracker.ObjectStateChanging += HandleObjectStateChanging;
                }
            }
        }
    
        private void HandleObjectStateChanging(object sender, ObjectStateChangingEventArgs e)
        {
            if (e.NewState == ObjectState.Deleted)
            {
                ClearNavigationProperties();
            }
        }
    
        protected bool IsDeserializing { get; private set; }
    
        [OnDeserializing]
        public void OnDeserializingMethod(StreamingContext context)
        {
            IsDeserializing = true;
        }
    
        [OnDeserialized]
        public void OnDeserializedMethod(StreamingContext context)
        {
            IsDeserializing = false;
            ChangeTracker.ChangeTrackingEnabled = true;
        }
    
        protected virtual void ClearNavigationProperties()
        {
        }

        #endregion
    }
}
