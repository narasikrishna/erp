//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.Serialization;

namespace FIN.DAL
{
    [DataContract(IsReference = true)]
    [KnownType(typeof(INV_INVOICES_DTLS))]
    [KnownType(typeof(INV_ITEM_COSTING))]
    [KnownType(typeof(INV_RECEIPT_DTLS))]
    [KnownType(typeof(INV_RECEIPT_LOTS_HDR))]
    [KnownType(typeof(TMP_INV_INVOICES_DTLS))]
    public partial class INV_RECEIPTS_HDR: IObjectWithChangeTracker, INotifyPropertyChanged
    {
        #region Primitive Properties
    
        [DataMember]
        public string RECEIPT_ID
        {
            get { return _rECEIPT_ID; }
            set
            {
                if (_rECEIPT_ID != value)
                {
                    if (ChangeTracker.ChangeTrackingEnabled && ChangeTracker.State != ObjectState.Added)
                    {
                        throw new InvalidOperationException("The property 'RECEIPT_ID' is part of the object's key and cannot be changed. Changes to key properties can only be made when the object is not being tracked or is in the Added state.");
                    }
                    _rECEIPT_ID = value;
                    OnPropertyChanged("RECEIPT_ID");
                }
            }
        }
        private string _rECEIPT_ID;
    
        [DataMember]
        public string ORG_ID
        {
            get { return _oRG_ID; }
            set
            {
                if (_oRG_ID != value)
                {
                    _oRG_ID = value;
                    OnPropertyChanged("ORG_ID");
                }
            }
        }
        private string _oRG_ID;
    
        [DataMember]
        public string GRN_NUM
        {
            get { return _gRN_NUM; }
            set
            {
                if (_gRN_NUM != value)
                {
                    _gRN_NUM = value;
                    OnPropertyChanged("GRN_NUM");
                }
            }
        }
        private string _gRN_NUM;
    
        [DataMember]
        public Nullable<System.DateTime> RECEIPT_DATE
        {
            get { return _rECEIPT_DATE; }
            set
            {
                if (_rECEIPT_DATE != value)
                {
                    _rECEIPT_DATE = value;
                    OnPropertyChanged("RECEIPT_DATE");
                }
            }
        }
        private Nullable<System.DateTime> _rECEIPT_DATE;
    
        [DataMember]
        public string INSPECTION_REQ_YN
        {
            get { return _iNSPECTION_REQ_YN; }
            set
            {
                if (_iNSPECTION_REQ_YN != value)
                {
                    _iNSPECTION_REQ_YN = value;
                    OnPropertyChanged("INSPECTION_REQ_YN");
                }
            }
        }
        private string _iNSPECTION_REQ_YN;
    
        [DataMember]
        public string EMP_ID
        {
            get { return _eMP_ID; }
            set
            {
                if (_eMP_ID != value)
                {
                    _eMP_ID = value;
                    OnPropertyChanged("EMP_ID");
                }
            }
        }
        private string _eMP_ID;
    
        [DataMember]
        public string SHIPMENT_NUMBER
        {
            get { return _sHIPMENT_NUMBER; }
            set
            {
                if (_sHIPMENT_NUMBER != value)
                {
                    _sHIPMENT_NUMBER = value;
                    OnPropertyChanged("SHIPMENT_NUMBER");
                }
            }
        }
        private string _sHIPMENT_NUMBER;
    
        [DataMember]
        public Nullable<System.DateTime> SHIPMENT_DATE
        {
            get { return _sHIPMENT_DATE; }
            set
            {
                if (_sHIPMENT_DATE != value)
                {
                    _sHIPMENT_DATE = value;
                    OnPropertyChanged("SHIPMENT_DATE");
                }
            }
        }
        private Nullable<System.DateTime> _sHIPMENT_DATE;
    
        [DataMember]
        public string SHIPMENT_TRACKING_NUMBER
        {
            get { return _sHIPMENT_TRACKING_NUMBER; }
            set
            {
                if (_sHIPMENT_TRACKING_NUMBER != value)
                {
                    _sHIPMENT_TRACKING_NUMBER = value;
                    OnPropertyChanged("SHIPMENT_TRACKING_NUMBER");
                }
            }
        }
        private string _sHIPMENT_TRACKING_NUMBER;
    
        [DataMember]
        public string SHIPMENT_RECEIVED_BY
        {
            get { return _sHIPMENT_RECEIVED_BY; }
            set
            {
                if (_sHIPMENT_RECEIVED_BY != value)
                {
                    _sHIPMENT_RECEIVED_BY = value;
                    OnPropertyChanged("SHIPMENT_RECEIVED_BY");
                }
            }
        }
        private string _sHIPMENT_RECEIVED_BY;
    
        [DataMember]
        public string SHIPMENT_COMPANY
        {
            get { return _sHIPMENT_COMPANY; }
            set
            {
                if (_sHIPMENT_COMPANY != value)
                {
                    _sHIPMENT_COMPANY = value;
                    OnPropertyChanged("SHIPMENT_COMPANY");
                }
            }
        }
        private string _sHIPMENT_COMPANY;
    
        [DataMember]
        public string SHIPMENT_MODE_OF_TRANSPORT
        {
            get { return _sHIPMENT_MODE_OF_TRANSPORT; }
            set
            {
                if (_sHIPMENT_MODE_OF_TRANSPORT != value)
                {
                    _sHIPMENT_MODE_OF_TRANSPORT = value;
                    OnPropertyChanged("SHIPMENT_MODE_OF_TRANSPORT");
                }
            }
        }
        private string _sHIPMENT_MODE_OF_TRANSPORT;
    
        [DataMember]
        public string ENABLED_FLAG
        {
            get { return _eNABLED_FLAG; }
            set
            {
                if (_eNABLED_FLAG != value)
                {
                    _eNABLED_FLAG = value;
                    OnPropertyChanged("ENABLED_FLAG");
                }
            }
        }
        private string _eNABLED_FLAG;
    
        [DataMember]
        public string WORKFLOW_COMPLETION_STATUS
        {
            get { return _wORKFLOW_COMPLETION_STATUS; }
            set
            {
                if (_wORKFLOW_COMPLETION_STATUS != value)
                {
                    _wORKFLOW_COMPLETION_STATUS = value;
                    OnPropertyChanged("WORKFLOW_COMPLETION_STATUS");
                }
            }
        }
        private string _wORKFLOW_COMPLETION_STATUS;
    
        [DataMember]
        public string CREATED_BY
        {
            get { return _cREATED_BY; }
            set
            {
                if (_cREATED_BY != value)
                {
                    _cREATED_BY = value;
                    OnPropertyChanged("CREATED_BY");
                }
            }
        }
        private string _cREATED_BY;
    
        [DataMember]
        public System.DateTime CREATED_DATE
        {
            get { return _cREATED_DATE; }
            set
            {
                if (_cREATED_DATE != value)
                {
                    _cREATED_DATE = value;
                    OnPropertyChanged("CREATED_DATE");
                }
            }
        }
        private System.DateTime _cREATED_DATE;
    
        [DataMember]
        public string MODIFIED_BY
        {
            get { return _mODIFIED_BY; }
            set
            {
                if (_mODIFIED_BY != value)
                {
                    _mODIFIED_BY = value;
                    OnPropertyChanged("MODIFIED_BY");
                }
            }
        }
        private string _mODIFIED_BY;
    
        [DataMember]
        public Nullable<System.DateTime> MODIFIED_DATE
        {
            get { return _mODIFIED_DATE; }
            set
            {
                if (_mODIFIED_DATE != value)
                {
                    _mODIFIED_DATE = value;
                    OnPropertyChanged("MODIFIED_DATE");
                }
            }
        }
        private Nullable<System.DateTime> _mODIFIED_DATE;
    
        [DataMember]
        public string ATTRIBUTE1
        {
            get { return _aTTRIBUTE1; }
            set
            {
                if (_aTTRIBUTE1 != value)
                {
                    _aTTRIBUTE1 = value;
                    OnPropertyChanged("ATTRIBUTE1");
                }
            }
        }
        private string _aTTRIBUTE1;
    
        [DataMember]
        public string ATTRIBUTE2
        {
            get { return _aTTRIBUTE2; }
            set
            {
                if (_aTTRIBUTE2 != value)
                {
                    _aTTRIBUTE2 = value;
                    OnPropertyChanged("ATTRIBUTE2");
                }
            }
        }
        private string _aTTRIBUTE2;
    
        [DataMember]
        public string ATTRIBUTE3
        {
            get { return _aTTRIBUTE3; }
            set
            {
                if (_aTTRIBUTE3 != value)
                {
                    _aTTRIBUTE3 = value;
                    OnPropertyChanged("ATTRIBUTE3");
                }
            }
        }
        private string _aTTRIBUTE3;
    
        [DataMember]
        public string ATTRIBUTE4
        {
            get { return _aTTRIBUTE4; }
            set
            {
                if (_aTTRIBUTE4 != value)
                {
                    _aTTRIBUTE4 = value;
                    OnPropertyChanged("ATTRIBUTE4");
                }
            }
        }
        private string _aTTRIBUTE4;
    
        [DataMember]
        public string ATTRIBUTE5
        {
            get { return _aTTRIBUTE5; }
            set
            {
                if (_aTTRIBUTE5 != value)
                {
                    _aTTRIBUTE5 = value;
                    OnPropertyChanged("ATTRIBUTE5");
                }
            }
        }
        private string _aTTRIBUTE5;
    
        [DataMember]
        public string ATTRIBUTE6
        {
            get { return _aTTRIBUTE6; }
            set
            {
                if (_aTTRIBUTE6 != value)
                {
                    _aTTRIBUTE6 = value;
                    OnPropertyChanged("ATTRIBUTE6");
                }
            }
        }
        private string _aTTRIBUTE6;
    
        [DataMember]
        public string ATTRIBUTE7
        {
            get { return _aTTRIBUTE7; }
            set
            {
                if (_aTTRIBUTE7 != value)
                {
                    _aTTRIBUTE7 = value;
                    OnPropertyChanged("ATTRIBUTE7");
                }
            }
        }
        private string _aTTRIBUTE7;
    
        [DataMember]
        public string ATTRIBUTE8
        {
            get { return _aTTRIBUTE8; }
            set
            {
                if (_aTTRIBUTE8 != value)
                {
                    _aTTRIBUTE8 = value;
                    OnPropertyChanged("ATTRIBUTE8");
                }
            }
        }
        private string _aTTRIBUTE8;
    
        [DataMember]
        public string ATTRIBUTE9
        {
            get { return _aTTRIBUTE9; }
            set
            {
                if (_aTTRIBUTE9 != value)
                {
                    _aTTRIBUTE9 = value;
                    OnPropertyChanged("ATTRIBUTE9");
                }
            }
        }
        private string _aTTRIBUTE9;
    
        [DataMember]
        public string ATTRIBUTE10
        {
            get { return _aTTRIBUTE10; }
            set
            {
                if (_aTTRIBUTE10 != value)
                {
                    _aTTRIBUTE10 = value;
                    OnPropertyChanged("ATTRIBUTE10");
                }
            }
        }
        private string _aTTRIBUTE10;
    
        [DataMember]
        public string VENDOR_ID
        {
            get { return _vENDOR_ID; }
            set
            {
                if (_vENDOR_ID != value)
                {
                    _vENDOR_ID = value;
                    OnPropertyChanged("VENDOR_ID");
                }
            }
        }
        private string _vENDOR_ID;
    
        [DataMember]
        public string GRN_TYPE
        {
            get { return _gRN_TYPE; }
            set
            {
                if (_gRN_TYPE != value)
                {
                    _gRN_TYPE = value;
                    OnPropertyChanged("GRN_TYPE");
                }
            }
        }
        private string _gRN_TYPE;
    
        [DataMember]
        public string VENDOR_LOC_ID
        {
            get { return _vENDOR_LOC_ID; }
            set
            {
                if (_vENDOR_LOC_ID != value)
                {
                    _vENDOR_LOC_ID = value;
                    OnPropertyChanged("VENDOR_LOC_ID");
                }
            }
        }
        private string _vENDOR_LOC_ID;
    
        [DataMember]
        public string INTIATED_BY
        {
            get { return _iNTIATED_BY; }
            set
            {
                if (_iNTIATED_BY != value)
                {
                    _iNTIATED_BY = value;
                    OnPropertyChanged("INTIATED_BY");
                }
            }
        }
        private string _iNTIATED_BY;
    
        [DataMember]
        public string APPROVED_BY
        {
            get { return _aPPROVED_BY; }
            set
            {
                if (_aPPROVED_BY != value)
                {
                    _aPPROVED_BY = value;
                    OnPropertyChanged("APPROVED_BY");
                }
            }
        }
        private string _aPPROVED_BY;
    
        [DataMember]
        public string RECEIPT_FLAG
        {
            get { return _rECEIPT_FLAG; }
            set
            {
                if (_rECEIPT_FLAG != value)
                {
                    _rECEIPT_FLAG = value;
                    OnPropertyChanged("RECEIPT_FLAG");
                }
            }
        }
        private string _rECEIPT_FLAG;
    
        [DataMember]
        public string INSPECTED_BY
        {
            get { return _iNSPECTED_BY; }
            set
            {
                if (_iNSPECTED_BY != value)
                {
                    _iNSPECTED_BY = value;
                    OnPropertyChanged("INSPECTED_BY");
                }
            }
        }
        private string _iNSPECTED_BY;
    
        [DataMember]
        public string APPROVER_NAME
        {
            get { return _aPPROVER_NAME; }
            set
            {
                if (_aPPROVER_NAME != value)
                {
                    _aPPROVER_NAME = value;
                    OnPropertyChanged("APPROVER_NAME");
                }
            }
        }
        private string _aPPROVER_NAME;
    
        [DataMember]
        public string REMARKS
        {
            get { return _rEMARKS; }
            set
            {
                if (_rEMARKS != value)
                {
                    _rEMARKS = value;
                    OnPropertyChanged("REMARKS");
                }
            }
        }
        private string _rEMARKS;

        #endregion
        #region Navigation Properties
    
        [DataMember]
        public TrackableCollection<INV_INVOICES_DTLS> INV_INVOICES_DTLS
        {
            get
            {
                if (_iNV_INVOICES_DTLS == null)
                {
                    _iNV_INVOICES_DTLS = new TrackableCollection<INV_INVOICES_DTLS>();
                    _iNV_INVOICES_DTLS.CollectionChanged += FixupINV_INVOICES_DTLS;
                }
                return _iNV_INVOICES_DTLS;
            }
            set
            {
                if (!ReferenceEquals(_iNV_INVOICES_DTLS, value))
                {
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        throw new InvalidOperationException("Cannot set the FixupChangeTrackingCollection when ChangeTracking is enabled");
                    }
                    if (_iNV_INVOICES_DTLS != null)
                    {
                        _iNV_INVOICES_DTLS.CollectionChanged -= FixupINV_INVOICES_DTLS;
                    }
                    _iNV_INVOICES_DTLS = value;
                    if (_iNV_INVOICES_DTLS != null)
                    {
                        _iNV_INVOICES_DTLS.CollectionChanged += FixupINV_INVOICES_DTLS;
                    }
                    OnNavigationPropertyChanged("INV_INVOICES_DTLS");
                }
            }
        }
        private TrackableCollection<INV_INVOICES_DTLS> _iNV_INVOICES_DTLS;
    
        [DataMember]
        public TrackableCollection<INV_ITEM_COSTING> INV_ITEM_COSTING
        {
            get
            {
                if (_iNV_ITEM_COSTING == null)
                {
                    _iNV_ITEM_COSTING = new TrackableCollection<INV_ITEM_COSTING>();
                    _iNV_ITEM_COSTING.CollectionChanged += FixupINV_ITEM_COSTING;
                }
                return _iNV_ITEM_COSTING;
            }
            set
            {
                if (!ReferenceEquals(_iNV_ITEM_COSTING, value))
                {
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        throw new InvalidOperationException("Cannot set the FixupChangeTrackingCollection when ChangeTracking is enabled");
                    }
                    if (_iNV_ITEM_COSTING != null)
                    {
                        _iNV_ITEM_COSTING.CollectionChanged -= FixupINV_ITEM_COSTING;
                    }
                    _iNV_ITEM_COSTING = value;
                    if (_iNV_ITEM_COSTING != null)
                    {
                        _iNV_ITEM_COSTING.CollectionChanged += FixupINV_ITEM_COSTING;
                    }
                    OnNavigationPropertyChanged("INV_ITEM_COSTING");
                }
            }
        }
        private TrackableCollection<INV_ITEM_COSTING> _iNV_ITEM_COSTING;
    
        [DataMember]
        public TrackableCollection<INV_RECEIPT_DTLS> INV_RECEIPT_DTLS
        {
            get
            {
                if (_iNV_RECEIPT_DTLS == null)
                {
                    _iNV_RECEIPT_DTLS = new TrackableCollection<INV_RECEIPT_DTLS>();
                    _iNV_RECEIPT_DTLS.CollectionChanged += FixupINV_RECEIPT_DTLS;
                }
                return _iNV_RECEIPT_DTLS;
            }
            set
            {
                if (!ReferenceEquals(_iNV_RECEIPT_DTLS, value))
                {
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        throw new InvalidOperationException("Cannot set the FixupChangeTrackingCollection when ChangeTracking is enabled");
                    }
                    if (_iNV_RECEIPT_DTLS != null)
                    {
                        _iNV_RECEIPT_DTLS.CollectionChanged -= FixupINV_RECEIPT_DTLS;
                    }
                    _iNV_RECEIPT_DTLS = value;
                    if (_iNV_RECEIPT_DTLS != null)
                    {
                        _iNV_RECEIPT_DTLS.CollectionChanged += FixupINV_RECEIPT_DTLS;
                    }
                    OnNavigationPropertyChanged("INV_RECEIPT_DTLS");
                }
            }
        }
        private TrackableCollection<INV_RECEIPT_DTLS> _iNV_RECEIPT_DTLS;
    
        [DataMember]
        public TrackableCollection<INV_RECEIPT_LOTS_HDR> INV_RECEIPT_LOTS_HDR
        {
            get
            {
                if (_iNV_RECEIPT_LOTS_HDR == null)
                {
                    _iNV_RECEIPT_LOTS_HDR = new TrackableCollection<INV_RECEIPT_LOTS_HDR>();
                    _iNV_RECEIPT_LOTS_HDR.CollectionChanged += FixupINV_RECEIPT_LOTS_HDR;
                }
                return _iNV_RECEIPT_LOTS_HDR;
            }
            set
            {
                if (!ReferenceEquals(_iNV_RECEIPT_LOTS_HDR, value))
                {
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        throw new InvalidOperationException("Cannot set the FixupChangeTrackingCollection when ChangeTracking is enabled");
                    }
                    if (_iNV_RECEIPT_LOTS_HDR != null)
                    {
                        _iNV_RECEIPT_LOTS_HDR.CollectionChanged -= FixupINV_RECEIPT_LOTS_HDR;
                    }
                    _iNV_RECEIPT_LOTS_HDR = value;
                    if (_iNV_RECEIPT_LOTS_HDR != null)
                    {
                        _iNV_RECEIPT_LOTS_HDR.CollectionChanged += FixupINV_RECEIPT_LOTS_HDR;
                    }
                    OnNavigationPropertyChanged("INV_RECEIPT_LOTS_HDR");
                }
            }
        }
        private TrackableCollection<INV_RECEIPT_LOTS_HDR> _iNV_RECEIPT_LOTS_HDR;
    
        [DataMember]
        public TrackableCollection<TMP_INV_INVOICES_DTLS> TMP_INV_INVOICES_DTLS
        {
            get
            {
                if (_tMP_INV_INVOICES_DTLS == null)
                {
                    _tMP_INV_INVOICES_DTLS = new TrackableCollection<TMP_INV_INVOICES_DTLS>();
                    _tMP_INV_INVOICES_DTLS.CollectionChanged += FixupTMP_INV_INVOICES_DTLS;
                }
                return _tMP_INV_INVOICES_DTLS;
            }
            set
            {
                if (!ReferenceEquals(_tMP_INV_INVOICES_DTLS, value))
                {
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        throw new InvalidOperationException("Cannot set the FixupChangeTrackingCollection when ChangeTracking is enabled");
                    }
                    if (_tMP_INV_INVOICES_DTLS != null)
                    {
                        _tMP_INV_INVOICES_DTLS.CollectionChanged -= FixupTMP_INV_INVOICES_DTLS;
                    }
                    _tMP_INV_INVOICES_DTLS = value;
                    if (_tMP_INV_INVOICES_DTLS != null)
                    {
                        _tMP_INV_INVOICES_DTLS.CollectionChanged += FixupTMP_INV_INVOICES_DTLS;
                    }
                    OnNavigationPropertyChanged("TMP_INV_INVOICES_DTLS");
                }
            }
        }
        private TrackableCollection<TMP_INV_INVOICES_DTLS> _tMP_INV_INVOICES_DTLS;

        #endregion
        #region ChangeTracking
    
        protected virtual void OnPropertyChanged(String propertyName)
        {
            if (ChangeTracker.State != ObjectState.Added && ChangeTracker.State != ObjectState.Deleted)
            {
                ChangeTracker.State = ObjectState.Modified;
            }
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        protected virtual void OnNavigationPropertyChanged(String propertyName)
        {
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        event PropertyChangedEventHandler INotifyPropertyChanged.PropertyChanged{ add { _propertyChanged += value; } remove { _propertyChanged -= value; } }
        private event PropertyChangedEventHandler _propertyChanged;
        private ObjectChangeTracker _changeTracker;
    
        [DataMember]
        public ObjectChangeTracker ChangeTracker
        {
            get
            {
                if (_changeTracker == null)
                {
                    _changeTracker = new ObjectChangeTracker();
                    _changeTracker.ObjectStateChanging += HandleObjectStateChanging;
                }
                return _changeTracker;
            }
            set
            {
                if(_changeTracker != null)
                {
                    _changeTracker.ObjectStateChanging -= HandleObjectStateChanging;
                }
                _changeTracker = value;
                if(_changeTracker != null)
                {
                    _changeTracker.ObjectStateChanging += HandleObjectStateChanging;
                }
            }
        }
    
        private void HandleObjectStateChanging(object sender, ObjectStateChangingEventArgs e)
        {
            if (e.NewState == ObjectState.Deleted)
            {
                ClearNavigationProperties();
            }
        }
    
        protected bool IsDeserializing { get; private set; }
    
        [OnDeserializing]
        public void OnDeserializingMethod(StreamingContext context)
        {
            IsDeserializing = true;
        }
    
        [OnDeserialized]
        public void OnDeserializedMethod(StreamingContext context)
        {
            IsDeserializing = false;
            ChangeTracker.ChangeTrackingEnabled = true;
        }
    
        protected virtual void ClearNavigationProperties()
        {
            INV_INVOICES_DTLS.Clear();
            INV_ITEM_COSTING.Clear();
            INV_RECEIPT_DTLS.Clear();
            INV_RECEIPT_LOTS_HDR.Clear();
            TMP_INV_INVOICES_DTLS.Clear();
        }

        #endregion
        #region Association Fixup
    
        private void FixupINV_INVOICES_DTLS(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (e.NewItems != null)
            {
                foreach (INV_INVOICES_DTLS item in e.NewItems)
                {
                    item.INV_RECEIPTS_HDR = this;
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        if (!item.ChangeTracker.ChangeTrackingEnabled)
                        {
                            item.StartTracking();
                        }
                        ChangeTracker.RecordAdditionToCollectionProperties("INV_INVOICES_DTLS", item);
                    }
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (INV_INVOICES_DTLS item in e.OldItems)
                {
                    if (ReferenceEquals(item.INV_RECEIPTS_HDR, this))
                    {
                        item.INV_RECEIPTS_HDR = null;
                    }
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        ChangeTracker.RecordRemovalFromCollectionProperties("INV_INVOICES_DTLS", item);
                    }
                }
            }
        }
    
        private void FixupINV_ITEM_COSTING(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (e.NewItems != null)
            {
                foreach (INV_ITEM_COSTING item in e.NewItems)
                {
                    item.INV_RECEIPTS_HDR = this;
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        if (!item.ChangeTracker.ChangeTrackingEnabled)
                        {
                            item.StartTracking();
                        }
                        ChangeTracker.RecordAdditionToCollectionProperties("INV_ITEM_COSTING", item);
                    }
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (INV_ITEM_COSTING item in e.OldItems)
                {
                    if (ReferenceEquals(item.INV_RECEIPTS_HDR, this))
                    {
                        item.INV_RECEIPTS_HDR = null;
                    }
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        ChangeTracker.RecordRemovalFromCollectionProperties("INV_ITEM_COSTING", item);
                    }
                }
            }
        }
    
        private void FixupINV_RECEIPT_DTLS(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (e.NewItems != null)
            {
                foreach (INV_RECEIPT_DTLS item in e.NewItems)
                {
                    item.INV_RECEIPTS_HDR = this;
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        if (!item.ChangeTracker.ChangeTrackingEnabled)
                        {
                            item.StartTracking();
                        }
                        ChangeTracker.RecordAdditionToCollectionProperties("INV_RECEIPT_DTLS", item);
                    }
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (INV_RECEIPT_DTLS item in e.OldItems)
                {
                    if (ReferenceEquals(item.INV_RECEIPTS_HDR, this))
                    {
                        item.INV_RECEIPTS_HDR = null;
                    }
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        ChangeTracker.RecordRemovalFromCollectionProperties("INV_RECEIPT_DTLS", item);
                    }
                }
            }
        }
    
        private void FixupINV_RECEIPT_LOTS_HDR(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (e.NewItems != null)
            {
                foreach (INV_RECEIPT_LOTS_HDR item in e.NewItems)
                {
                    item.INV_RECEIPTS_HDR = this;
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        if (!item.ChangeTracker.ChangeTrackingEnabled)
                        {
                            item.StartTracking();
                        }
                        ChangeTracker.RecordAdditionToCollectionProperties("INV_RECEIPT_LOTS_HDR", item);
                    }
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (INV_RECEIPT_LOTS_HDR item in e.OldItems)
                {
                    if (ReferenceEquals(item.INV_RECEIPTS_HDR, this))
                    {
                        item.INV_RECEIPTS_HDR = null;
                    }
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        ChangeTracker.RecordRemovalFromCollectionProperties("INV_RECEIPT_LOTS_HDR", item);
                    }
                }
            }
        }
    
        private void FixupTMP_INV_INVOICES_DTLS(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (e.NewItems != null)
            {
                foreach (TMP_INV_INVOICES_DTLS item in e.NewItems)
                {
                    item.INV_RECEIPTS_HDR = this;
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        if (!item.ChangeTracker.ChangeTrackingEnabled)
                        {
                            item.StartTracking();
                        }
                        ChangeTracker.RecordAdditionToCollectionProperties("TMP_INV_INVOICES_DTLS", item);
                    }
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (TMP_INV_INVOICES_DTLS item in e.OldItems)
                {
                    if (ReferenceEquals(item.INV_RECEIPTS_HDR, this))
                    {
                        item.INV_RECEIPTS_HDR = null;
                    }
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        ChangeTracker.RecordRemovalFromCollectionProperties("TMP_INV_INVOICES_DTLS", item);
                    }
                }
            }
        }

        #endregion
    }
}
