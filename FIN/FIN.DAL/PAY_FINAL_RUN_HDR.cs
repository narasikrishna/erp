//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.Serialization;

namespace FIN.DAL
{
    [DataContract(IsReference = true)]
    [KnownType(typeof(PAY_FINAL_RUN_DTL))]
    [KnownType(typeof(PAY_PERIODS))]
    [KnownType(typeof(PAY_GROUP_DTLS))]
    public partial class PAY_FINAL_RUN_HDR: IObjectWithChangeTracker, INotifyPropertyChanged
    {
        #region Primitive Properties
    
        [DataMember]
        public Nullable<int> PK_ID
        {
            get { return _pK_ID; }
            set
            {
                if (_pK_ID != value)
                {
                    _pK_ID = value;
                    OnPropertyChanged("PK_ID");
                }
            }
        }
        private Nullable<int> _pK_ID;
    
        [DataMember]
        public Nullable<int> CHILD_ID
        {
            get { return _cHILD_ID; }
            set
            {
                if (_cHILD_ID != value)
                {
                    _cHILD_ID = value;
                    OnPropertyChanged("CHILD_ID");
                }
            }
        }
        private Nullable<int> _cHILD_ID;
    
        [DataMember]
        public string PAYROLL_ID
        {
            get { return _pAYROLL_ID; }
            set
            {
                if (_pAYROLL_ID != value)
                {
                    if (ChangeTracker.ChangeTrackingEnabled && ChangeTracker.State != ObjectState.Added)
                    {
                        throw new InvalidOperationException("The property 'PAYROLL_ID' is part of the object's key and cannot be changed. Changes to key properties can only be made when the object is not being tracked or is in the Added state.");
                    }
                    _pAYROLL_ID = value;
                    OnPropertyChanged("PAYROLL_ID");
                }
            }
        }
        private string _pAYROLL_ID;
    
        [DataMember]
        public string PAYROLL_PERIOD
        {
            get { return _pAYROLL_PERIOD; }
            set
            {
                if (_pAYROLL_PERIOD != value)
                {
                    ChangeTracker.RecordOriginalValue("PAYROLL_PERIOD", _pAYROLL_PERIOD);
                    if (!IsDeserializing)
                    {
                        if (PAY_PERIODS != null && PAY_PERIODS.PAY_PERIOD_ID != value)
                        {
                            PAY_PERIODS = null;
                        }
                    }
                    _pAYROLL_PERIOD = value;
                    OnPropertyChanged("PAYROLL_PERIOD");
                }
            }
        }
        private string _pAYROLL_PERIOD;
    
        [DataMember]
        public Nullable<decimal> PAYROLL_DAYS_HRS
        {
            get { return _pAYROLL_DAYS_HRS; }
            set
            {
                if (_pAYROLL_DAYS_HRS != value)
                {
                    _pAYROLL_DAYS_HRS = value;
                    OnPropertyChanged("PAYROLL_DAYS_HRS");
                }
            }
        }
        private Nullable<decimal> _pAYROLL_DAYS_HRS;
    
        [DataMember]
        public string PAYROLL_STATUS
        {
            get { return _pAYROLL_STATUS; }
            set
            {
                if (_pAYROLL_STATUS != value)
                {
                    _pAYROLL_STATUS = value;
                    OnPropertyChanged("PAYROLL_STATUS");
                }
            }
        }
        private string _pAYROLL_STATUS;
    
        [DataMember]
        public string PAY_GROUP_ID
        {
            get { return _pAY_GROUP_ID; }
            set
            {
                if (_pAY_GROUP_ID != value)
                {
                    ChangeTracker.RecordOriginalValue("PAY_GROUP_ID", _pAY_GROUP_ID);
                    if (!IsDeserializing)
                    {
                        if (PAY_GROUP_DTLS != null && PAY_GROUP_DTLS.PAY_GROUP_ID != value)
                        {
                            PAY_GROUP_DTLS = null;
                        }
                    }
                    _pAY_GROUP_ID = value;
                    OnPropertyChanged("PAY_GROUP_ID");
                }
            }
        }
        private string _pAY_GROUP_ID;
    
        [DataMember]
        public Nullable<System.DateTime> PAYROLL_DATE
        {
            get { return _pAYROLL_DATE; }
            set
            {
                if (_pAYROLL_DATE != value)
                {
                    _pAYROLL_DATE = value;
                    OnPropertyChanged("PAYROLL_DATE");
                }
            }
        }
        private Nullable<System.DateTime> _pAYROLL_DATE;
    
        [DataMember]
        public Nullable<decimal> PAYROLL_TOTAL_PAYMENT
        {
            get { return _pAYROLL_TOTAL_PAYMENT; }
            set
            {
                if (_pAYROLL_TOTAL_PAYMENT != value)
                {
                    _pAYROLL_TOTAL_PAYMENT = value;
                    OnPropertyChanged("PAYROLL_TOTAL_PAYMENT");
                }
            }
        }
        private Nullable<decimal> _pAYROLL_TOTAL_PAYMENT;
    
        [DataMember]
        public Nullable<decimal> PAYROLL_TOTAL_DEDUCTION
        {
            get { return _pAYROLL_TOTAL_DEDUCTION; }
            set
            {
                if (_pAYROLL_TOTAL_DEDUCTION != value)
                {
                    _pAYROLL_TOTAL_DEDUCTION = value;
                    OnPropertyChanged("PAYROLL_TOTAL_DEDUCTION");
                }
            }
        }
        private Nullable<decimal> _pAYROLL_TOTAL_DEDUCTION;
    
        [DataMember]
        public string ADDITIONAL_IDENTIFIER1
        {
            get { return _aDDITIONAL_IDENTIFIER1; }
            set
            {
                if (_aDDITIONAL_IDENTIFIER1 != value)
                {
                    _aDDITIONAL_IDENTIFIER1 = value;
                    OnPropertyChanged("ADDITIONAL_IDENTIFIER1");
                }
            }
        }
        private string _aDDITIONAL_IDENTIFIER1;
    
        [DataMember]
        public string ADDITIONAL_IDENTIFIER2
        {
            get { return _aDDITIONAL_IDENTIFIER2; }
            set
            {
                if (_aDDITIONAL_IDENTIFIER2 != value)
                {
                    _aDDITIONAL_IDENTIFIER2 = value;
                    OnPropertyChanged("ADDITIONAL_IDENTIFIER2");
                }
            }
        }
        private string _aDDITIONAL_IDENTIFIER2;
    
        [DataMember]
        public string ADDITIONAL_IDENTIFIER3
        {
            get { return _aDDITIONAL_IDENTIFIER3; }
            set
            {
                if (_aDDITIONAL_IDENTIFIER3 != value)
                {
                    _aDDITIONAL_IDENTIFIER3 = value;
                    OnPropertyChanged("ADDITIONAL_IDENTIFIER3");
                }
            }
        }
        private string _aDDITIONAL_IDENTIFIER3;
    
        [DataMember]
        public string ADDITIONAL_IDENTIFIER4
        {
            get { return _aDDITIONAL_IDENTIFIER4; }
            set
            {
                if (_aDDITIONAL_IDENTIFIER4 != value)
                {
                    _aDDITIONAL_IDENTIFIER4 = value;
                    OnPropertyChanged("ADDITIONAL_IDENTIFIER4");
                }
            }
        }
        private string _aDDITIONAL_IDENTIFIER4;
    
        [DataMember]
        public string ADDITIONAL_IDENTIFIER5
        {
            get { return _aDDITIONAL_IDENTIFIER5; }
            set
            {
                if (_aDDITIONAL_IDENTIFIER5 != value)
                {
                    _aDDITIONAL_IDENTIFIER5 = value;
                    OnPropertyChanged("ADDITIONAL_IDENTIFIER5");
                }
            }
        }
        private string _aDDITIONAL_IDENTIFIER5;
    
        [DataMember]
        public string PAYROLL_REMARKS
        {
            get { return _pAYROLL_REMARKS; }
            set
            {
                if (_pAYROLL_REMARKS != value)
                {
                    _pAYROLL_REMARKS = value;
                    OnPropertyChanged("PAYROLL_REMARKS");
                }
            }
        }
        private string _pAYROLL_REMARKS;
    
        [DataMember]
        public string ENABLED_FLAG
        {
            get { return _eNABLED_FLAG; }
            set
            {
                if (_eNABLED_FLAG != value)
                {
                    _eNABLED_FLAG = value;
                    OnPropertyChanged("ENABLED_FLAG");
                }
            }
        }
        private string _eNABLED_FLAG;
    
        [DataMember]
        public string WORKFLOW_COMPLETION_STATUS
        {
            get { return _wORKFLOW_COMPLETION_STATUS; }
            set
            {
                if (_wORKFLOW_COMPLETION_STATUS != value)
                {
                    _wORKFLOW_COMPLETION_STATUS = value;
                    OnPropertyChanged("WORKFLOW_COMPLETION_STATUS");
                }
            }
        }
        private string _wORKFLOW_COMPLETION_STATUS;
    
        [DataMember]
        public string CREATED_BY
        {
            get { return _cREATED_BY; }
            set
            {
                if (_cREATED_BY != value)
                {
                    _cREATED_BY = value;
                    OnPropertyChanged("CREATED_BY");
                }
            }
        }
        private string _cREATED_BY;
    
        [DataMember]
        public System.DateTime CREATED_DATE
        {
            get { return _cREATED_DATE; }
            set
            {
                if (_cREATED_DATE != value)
                {
                    _cREATED_DATE = value;
                    OnPropertyChanged("CREATED_DATE");
                }
            }
        }
        private System.DateTime _cREATED_DATE;
    
        [DataMember]
        public string MODIFIED_BY
        {
            get { return _mODIFIED_BY; }
            set
            {
                if (_mODIFIED_BY != value)
                {
                    _mODIFIED_BY = value;
                    OnPropertyChanged("MODIFIED_BY");
                }
            }
        }
        private string _mODIFIED_BY;
    
        [DataMember]
        public Nullable<System.DateTime> MODIFIED_DATE
        {
            get { return _mODIFIED_DATE; }
            set
            {
                if (_mODIFIED_DATE != value)
                {
                    _mODIFIED_DATE = value;
                    OnPropertyChanged("MODIFIED_DATE");
                }
            }
        }
        private Nullable<System.DateTime> _mODIFIED_DATE;
    
        [DataMember]
        public string ATTRIBUTE1
        {
            get { return _aTTRIBUTE1; }
            set
            {
                if (_aTTRIBUTE1 != value)
                {
                    _aTTRIBUTE1 = value;
                    OnPropertyChanged("ATTRIBUTE1");
                }
            }
        }
        private string _aTTRIBUTE1;
    
        [DataMember]
        public string ATTRIBUTE2
        {
            get { return _aTTRIBUTE2; }
            set
            {
                if (_aTTRIBUTE2 != value)
                {
                    _aTTRIBUTE2 = value;
                    OnPropertyChanged("ATTRIBUTE2");
                }
            }
        }
        private string _aTTRIBUTE2;
    
        [DataMember]
        public string ATTRIBUTE3
        {
            get { return _aTTRIBUTE3; }
            set
            {
                if (_aTTRIBUTE3 != value)
                {
                    _aTTRIBUTE3 = value;
                    OnPropertyChanged("ATTRIBUTE3");
                }
            }
        }
        private string _aTTRIBUTE3;
    
        [DataMember]
        public string ATTRIBUTE4
        {
            get { return _aTTRIBUTE4; }
            set
            {
                if (_aTTRIBUTE4 != value)
                {
                    _aTTRIBUTE4 = value;
                    OnPropertyChanged("ATTRIBUTE4");
                }
            }
        }
        private string _aTTRIBUTE4;
    
        [DataMember]
        public string ATTRIBUTE5
        {
            get { return _aTTRIBUTE5; }
            set
            {
                if (_aTTRIBUTE5 != value)
                {
                    _aTTRIBUTE5 = value;
                    OnPropertyChanged("ATTRIBUTE5");
                }
            }
        }
        private string _aTTRIBUTE5;
    
        [DataMember]
        public string ATTRIBUTE6
        {
            get { return _aTTRIBUTE6; }
            set
            {
                if (_aTTRIBUTE6 != value)
                {
                    _aTTRIBUTE6 = value;
                    OnPropertyChanged("ATTRIBUTE6");
                }
            }
        }
        private string _aTTRIBUTE6;
    
        [DataMember]
        public string ATTRIBUTE7
        {
            get { return _aTTRIBUTE7; }
            set
            {
                if (_aTTRIBUTE7 != value)
                {
                    _aTTRIBUTE7 = value;
                    OnPropertyChanged("ATTRIBUTE7");
                }
            }
        }
        private string _aTTRIBUTE7;
    
        [DataMember]
        public string ATTRIBUTE8
        {
            get { return _aTTRIBUTE8; }
            set
            {
                if (_aTTRIBUTE8 != value)
                {
                    _aTTRIBUTE8 = value;
                    OnPropertyChanged("ATTRIBUTE8");
                }
            }
        }
        private string _aTTRIBUTE8;
    
        [DataMember]
        public string ATTRIBUTE9
        {
            get { return _aTTRIBUTE9; }
            set
            {
                if (_aTTRIBUTE9 != value)
                {
                    _aTTRIBUTE9 = value;
                    OnPropertyChanged("ATTRIBUTE9");
                }
            }
        }
        private string _aTTRIBUTE9;
    
        [DataMember]
        public string ATTRIBUTE10
        {
            get { return _aTTRIBUTE10; }
            set
            {
                if (_aTTRIBUTE10 != value)
                {
                    _aTTRIBUTE10 = value;
                    OnPropertyChanged("ATTRIBUTE10");
                }
            }
        }
        private string _aTTRIBUTE10;

        #endregion
        #region Navigation Properties
    
        [DataMember]
        public TrackableCollection<PAY_FINAL_RUN_DTL> PAY_FINAL_RUN_DTL
        {
            get
            {
                if (_pAY_FINAL_RUN_DTL == null)
                {
                    _pAY_FINAL_RUN_DTL = new TrackableCollection<PAY_FINAL_RUN_DTL>();
                    _pAY_FINAL_RUN_DTL.CollectionChanged += FixupPAY_FINAL_RUN_DTL;
                }
                return _pAY_FINAL_RUN_DTL;
            }
            set
            {
                if (!ReferenceEquals(_pAY_FINAL_RUN_DTL, value))
                {
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        throw new InvalidOperationException("Cannot set the FixupChangeTrackingCollection when ChangeTracking is enabled");
                    }
                    if (_pAY_FINAL_RUN_DTL != null)
                    {
                        _pAY_FINAL_RUN_DTL.CollectionChanged -= FixupPAY_FINAL_RUN_DTL;
                    }
                    _pAY_FINAL_RUN_DTL = value;
                    if (_pAY_FINAL_RUN_DTL != null)
                    {
                        _pAY_FINAL_RUN_DTL.CollectionChanged += FixupPAY_FINAL_RUN_DTL;
                    }
                    OnNavigationPropertyChanged("PAY_FINAL_RUN_DTL");
                }
            }
        }
        private TrackableCollection<PAY_FINAL_RUN_DTL> _pAY_FINAL_RUN_DTL;
    
        [DataMember]
        public PAY_PERIODS PAY_PERIODS
        {
            get { return _pAY_PERIODS; }
            set
            {
                if (!ReferenceEquals(_pAY_PERIODS, value))
                {
                    var previousValue = _pAY_PERIODS;
                    _pAY_PERIODS = value;
                    FixupPAY_PERIODS(previousValue);
                    OnNavigationPropertyChanged("PAY_PERIODS");
                }
            }
        }
        private PAY_PERIODS _pAY_PERIODS;
    
        [DataMember]
        public PAY_GROUP_DTLS PAY_GROUP_DTLS
        {
            get { return _pAY_GROUP_DTLS; }
            set
            {
                if (!ReferenceEquals(_pAY_GROUP_DTLS, value))
                {
                    var previousValue = _pAY_GROUP_DTLS;
                    _pAY_GROUP_DTLS = value;
                    FixupPAY_GROUP_DTLS(previousValue);
                    OnNavigationPropertyChanged("PAY_GROUP_DTLS");
                }
            }
        }
        private PAY_GROUP_DTLS _pAY_GROUP_DTLS;

        #endregion
        #region ChangeTracking
    
        protected virtual void OnPropertyChanged(String propertyName)
        {
            if (ChangeTracker.State != ObjectState.Added && ChangeTracker.State != ObjectState.Deleted)
            {
                ChangeTracker.State = ObjectState.Modified;
            }
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        protected virtual void OnNavigationPropertyChanged(String propertyName)
        {
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        event PropertyChangedEventHandler INotifyPropertyChanged.PropertyChanged{ add { _propertyChanged += value; } remove { _propertyChanged -= value; } }
        private event PropertyChangedEventHandler _propertyChanged;
        private ObjectChangeTracker _changeTracker;
    
        [DataMember]
        public ObjectChangeTracker ChangeTracker
        {
            get
            {
                if (_changeTracker == null)
                {
                    _changeTracker = new ObjectChangeTracker();
                    _changeTracker.ObjectStateChanging += HandleObjectStateChanging;
                }
                return _changeTracker;
            }
            set
            {
                if(_changeTracker != null)
                {
                    _changeTracker.ObjectStateChanging -= HandleObjectStateChanging;
                }
                _changeTracker = value;
                if(_changeTracker != null)
                {
                    _changeTracker.ObjectStateChanging += HandleObjectStateChanging;
                }
            }
        }
    
        private void HandleObjectStateChanging(object sender, ObjectStateChangingEventArgs e)
        {
            if (e.NewState == ObjectState.Deleted)
            {
                ClearNavigationProperties();
            }
        }
    
        protected bool IsDeserializing { get; private set; }
    
        [OnDeserializing]
        public void OnDeserializingMethod(StreamingContext context)
        {
            IsDeserializing = true;
        }
    
        [OnDeserialized]
        public void OnDeserializedMethod(StreamingContext context)
        {
            IsDeserializing = false;
            ChangeTracker.ChangeTrackingEnabled = true;
        }
    
        protected virtual void ClearNavigationProperties()
        {
            PAY_FINAL_RUN_DTL.Clear();
            PAY_PERIODS = null;
            PAY_GROUP_DTLS = null;
        }

        #endregion
        #region Association Fixup
    
        private void FixupPAY_PERIODS(PAY_PERIODS previousValue, bool skipKeys = false)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (previousValue != null && previousValue.PAY_FINAL_RUN_HDR.Contains(this))
            {
                previousValue.PAY_FINAL_RUN_HDR.Remove(this);
            }
    
            if (PAY_PERIODS != null)
            {
                if (!PAY_PERIODS.PAY_FINAL_RUN_HDR.Contains(this))
                {
                    PAY_PERIODS.PAY_FINAL_RUN_HDR.Add(this);
                }
    
                PAYROLL_PERIOD = PAY_PERIODS.PAY_PERIOD_ID;
            }
            else if (!skipKeys)
            {
                PAYROLL_PERIOD = null;
            }
    
            if (ChangeTracker.ChangeTrackingEnabled)
            {
                if (ChangeTracker.OriginalValues.ContainsKey("PAY_PERIODS")
                    && (ChangeTracker.OriginalValues["PAY_PERIODS"] == PAY_PERIODS))
                {
                    ChangeTracker.OriginalValues.Remove("PAY_PERIODS");
                }
                else
                {
                    ChangeTracker.RecordOriginalValue("PAY_PERIODS", previousValue);
                }
                if (PAY_PERIODS != null && !PAY_PERIODS.ChangeTracker.ChangeTrackingEnabled)
                {
                    PAY_PERIODS.StartTracking();
                }
            }
        }
    
        private void FixupPAY_GROUP_DTLS(PAY_GROUP_DTLS previousValue, bool skipKeys = false)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (previousValue != null && previousValue.PAY_FINAL_RUN_HDR.Contains(this))
            {
                previousValue.PAY_FINAL_RUN_HDR.Remove(this);
            }
    
            if (PAY_GROUP_DTLS != null)
            {
                if (!PAY_GROUP_DTLS.PAY_FINAL_RUN_HDR.Contains(this))
                {
                    PAY_GROUP_DTLS.PAY_FINAL_RUN_HDR.Add(this);
                }
    
                PAY_GROUP_ID = PAY_GROUP_DTLS.PAY_GROUP_ID;
            }
            else if (!skipKeys)
            {
                PAY_GROUP_ID = null;
            }
    
            if (ChangeTracker.ChangeTrackingEnabled)
            {
                if (ChangeTracker.OriginalValues.ContainsKey("PAY_GROUP_DTLS")
                    && (ChangeTracker.OriginalValues["PAY_GROUP_DTLS"] == PAY_GROUP_DTLS))
                {
                    ChangeTracker.OriginalValues.Remove("PAY_GROUP_DTLS");
                }
                else
                {
                    ChangeTracker.RecordOriginalValue("PAY_GROUP_DTLS", previousValue);
                }
                if (PAY_GROUP_DTLS != null && !PAY_GROUP_DTLS.ChangeTracker.ChangeTrackingEnabled)
                {
                    PAY_GROUP_DTLS.StartTracking();
                }
            }
        }
    
        private void FixupPAY_FINAL_RUN_DTL(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (e.NewItems != null)
            {
                foreach (PAY_FINAL_RUN_DTL item in e.NewItems)
                {
                    item.PAY_FINAL_RUN_HDR = this;
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        if (!item.ChangeTracker.ChangeTrackingEnabled)
                        {
                            item.StartTracking();
                        }
                        ChangeTracker.RecordAdditionToCollectionProperties("PAY_FINAL_RUN_DTL", item);
                    }
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (PAY_FINAL_RUN_DTL item in e.OldItems)
                {
                    if (ReferenceEquals(item.PAY_FINAL_RUN_HDR, this))
                    {
                        item.PAY_FINAL_RUN_HDR = null;
                    }
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        ChangeTracker.RecordRemovalFromCollectionProperties("PAY_FINAL_RUN_DTL", item);
                    }
                }
            }
        }

        #endregion
    }
}
