﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.LOAN
{
    public class Interest_DAL
    {
        public static string getInterestDetails(string str_Int_id)
        {
            string sqlQuery = string.Empty;
            sqlQuery += "   SELECT LD.LN_INT_DTL_ID,LD.LN_INT_ID,LD.LN_INSTALLMENT_NO,LD.LN_INSTALLMENT_DATE ";
            sqlQuery += "   ,to_char(LD.LN_PROFIT_SHARE_AMT) as LN_PROFIT_SHARE_AMT ,LD.LN_REV_PROFIT_SHARE_AMT,'N' AS DELETED ";
            sqlQuery += "  ,LD.ENABLED_FLAG ";
            sqlQuery += "   FROM LN_INTEREST_DTL LD ";
            sqlQuery += " WHERE LD.LN_INT_ID='" + str_Int_id + "'";
            return sqlQuery;
        }

        public static string getInterestDetails4Contract(string str_Int_id)
        {
            string sqlQuery = string.Empty;
            sqlQuery += "   SELECT LD.LN_INT_DTL_ID,LD.LN_INT_ID,LD.LN_INSTALLMENT_NO,LD.LN_INSTALLMENT_DATE ";
            sqlQuery += "   ,to_char(LD.LN_PROFIT_SHARE_AMT) as LN_PROFIT_SHARE_AMT ,LD.LN_REV_PROFIT_SHARE_AMT,'N' AS DELETED ";
            sqlQuery += "  ,LD.ENABLED_FLAG ";
            sqlQuery += " ,LH.LN_INT_TYPE ";
            sqlQuery += "   FROM LN_INTEREST_DTL LD ";
            sqlQuery += " INNER JOIN LN_INTEREST_HDR LH on LH.LN_INT_ID=LD.LN_INT_ID ";
            sqlQuery += " WHERE LH.LN_CONTRACT_DTL_ID='" + str_Int_id + "'";
            sqlQuery += " ORDER BY LN_INSTALLMENT_DATE ";
            return sqlQuery;
        }
    }
}
