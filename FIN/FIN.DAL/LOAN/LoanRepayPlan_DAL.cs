﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;

namespace FIN.DAL.LOAN
{
    public class LoanRepayPlan_DAL
    {
        static string sqlQuery = "";

        public static string getLoanRepayDet4InstallmentId(string strInstId)
        {
            sqlQuery = string.Empty;
            sqlQuery += " SELECT * FROM lN_REPAY_PLAN_HDR A WHERE A.LN_LOAN_DTL_ID='" + strInstId + "'";
            return sqlQuery;
        }
        public static string getLoanInstallmentHistory(int str_installno)
        {

            sqlQuery = string.Empty;
            sqlQuery += " SELECT * FROM LN_INSTALLMENT_HISTROY_DTL IHD WHERE IHD.LN_INST_HIS_ID IN ( ";
            sqlQuery += " select IHH.LN_INST_HIS_ID FROM LN_INSTALLMENT_HISTROY_HDR IHH WHERE IHH.LN_INSTALLMENT_NO = " + (str_installno - 1).ToString() + ") ";
            sqlQuery += " AND IHD.LN_INSTALLMENT_NO <= " + (str_installno - 1).ToString();
            return sqlQuery;
        }

        public static string GetLoanDetails()
        {
            sqlQuery = string.Empty;
            sqlQuery = " select LR.LN_REQUEST_DESC,LH.LN_LOAN_ID ";
            sqlQuery += " from LN_APPROVED_LOAN_HDR LH,ln_loan_request LR ";
            sqlQuery += " where LH.LN_REQUEST_ID = LR.LN_REQUEST_ID ";
            sqlQuery += " and LH.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " AND LH.ENABLED_FLAG = 1 ";
            sqlQuery += " ORDER BY LN_REQUEST_DESC asc ";

            return sqlQuery;
        }

        public static string GetInstallmentDetails(string LOAN_ID)
        {
            sqlQuery = string.Empty;
            sqlQuery = " select LD.LN_LOAN_DTL_ID,LD.LN_INSTALLMENT_NO ";
            sqlQuery += " from LN_APPROVED_LOAN_DTL LD,LN_APPROVED_LOAN_HDR LH ";
            sqlQuery += " where LH.LN_LOAN_ID = LD.LN_LOAN_ID ";
            sqlQuery += " AND LH.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " AND LH.ENABLED_FLAG = 1 ";
            sqlQuery += " and LH.LN_LOAN_ID = '" + LOAN_ID + "'";
            sqlQuery += " ORDER BY LN_INSTALLMENT_NO asc ";

            return sqlQuery;
        }

        public static string GetLoanRepayDetails(string LOAN_REPAY_ID)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select RD.LN_REPAY_INSTALLMENT_NO,RD.LN_REPAY_DT,RD.LN_REPAY_AMOUNT,";
            sqlQuery += " CASE RD.LN_PAID WHEN '1' THEN 'TRUE' ELSE 'FALSE' END as  LN_PAID,RD.LN_REPAY_DTL_ID";
            sqlQuery += " ,CASE RD.ENABLED_FLAG WHEN '1' THEN 'TRUE' ELSE 'FALSE' END as ENABLED_FLAG,'N' as DELETED, 0 as SNO ";
            sqlQuery += " from Ln_Repay_Plan_Hdr RH,LN_REPAY_PLAN_DTL RD ";
            sqlQuery += " where RH.LN_REPAY_ID = RD.LN_REPAY_ID ";
            sqlQuery += " and RH.ENABLED_FLAG = 1";
            sqlQuery += " and RH.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " and RH.LN_REPAY_ID = '" + LOAN_REPAY_ID + "'";
            return sqlQuery;
        }
        public static string GetLoanNewRepayPlanDetails(string bankId, string installDate)
        {
            sqlQuery = string.Empty;
            /*
                        sqlQuery += "   select LN_INSTALLMENT_NO,LN_INSTALLMENT_DT,to_char(LN_AMOUNT) as LN_AMOUNT,to_char(LN_REVISED_PS_AMT) as LN_REVISED_PS_AMT,to_char(LN_PAID_AMT) as LN_PAID_AMT,to_char(LS_PS_PAID_AMT) as LS_PS_PAID_AMT,LN_PRN_REPAY_DTL_ID,LN_PAID_DT ";
                        sqlQuery += "  from ln_repay_dtl ad, ln_repay_hdr h,LN_LOAN_REQUEST_dtl dd";
                        sqlQuery += "  where ad.ln_prn_repay_id = h.ln_prn_repay_id";
                        sqlQuery += "  and dd.ln_request_id=h.ln_request_id";
                        sqlQuery += "  and dd.ln_party_id='" + bankId + "'";
                        sqlQuery += " and to_char(ad.ln_installment_dt,'MM') =to_char( to_date('" + installDate.ToString() + "', 'dd/MM/yyyy'),'MM') ";
                        sqlQuery += "  and to_char(ad.ln_installment_dt,'YYYY') =to_char( to_date('" + installDate.ToString() + "', 'dd/MM/yyyy'),'YYYY') ";

                        */

            sqlQuery += "   SELECT MAX(P_PAY_TYPE) AS P_PAY_TYPE,Z.LN_CONTRACT_ID,LRD.LN_CONTRACT_NUM,MAX(LN_PRN_REPAY_DTL_ID) AS LN_PRN_REPAY_DTL_ID,MAX(LN_PRN_REPAY_ID) AS LN_PRN_REPAY_ID,MAX(LN_INSTALLMENT_NO) AS LN_INSTALLMENT_NO,MAX(LN_INSTALLMENT_DT) AS LN_INSTALLMENT_DT,to_char(MAX(LN_AMOUNT)) AS LN_AMOUNT,to_char(MAX(LN_PAID_AMT)) AS LN_PAID_AMT,MAX(LN_PAID_DT) AS LN_PAID_DT ";
            sqlQuery += "   ,MAX(PS_PAY_TYPE) AS PS_PAY_TYPE,MAX(PS_DTL_ID) AS PS_DTL_ID,MAX(PS_ID) AS PS_ID,MAX(PS_INSTALLMENT_NO) AS PS_INSTALLMENT_NO,MAX(PS_INSTALLMENT_DT) AS PS_INSTALLMENT_DT,to_char(MAX(LN_REV_PROFIT_SHARE_AMT)) AS LN_REV_PROFIT_SHARE_AMT,to_char(MAX(LN_PS_PAID_AMT)) AS LN_PS_PAID_AMT,MAX(LN_PS_PAID_DATE) AS LN_PS_PAID_DATE ";
            sqlQuery += "   FROM ( ";
            sqlQuery += "   SELECT  'P' AS P_PAY_TYPE,RH.LN_CONTRACT_ID, RD.LN_PRN_REPAY_DTL_ID,RD.LN_PRN_REPAY_ID,RD.LN_INSTALLMENT_NO,RD.LN_INSTALLMENT_DT,RD.LN_AMOUNT,RD.LN_PAID_AMT,RD.LN_PAID_DT  ";
            sqlQuery += "   ,'' AS PS_PAY_TYPE,'' AS PS_DTL_ID,'' AS PS_ID,0 AS PS_INSTALLMENT_NO,NULL AS PS_INSTALLMENT_DT,0 AS LN_REV_PROFIT_SHARE_AMT,NULL AS LN_PS_PAID_AMT,NULL AS LN_PS_PAID_DATE ";
            sqlQuery += "   FROM LN_REPAY_DTL RD ";
            sqlQuery += "   INNER JOIN LN_REPAY_HDR RH ON RH.LN_PRN_REPAY_ID= RD.LN_PRN_REPAY_ID ";
            sqlQuery += "   INNER JOIN LN_LOAN_REQUEST_DTL LRD ON LRD.LN_CONTRACT_ID= RH.LN_CONTRACT_ID ";
            sqlQuery += "   WHERE TO_CHAR(RD.LN_INSTALLMENT_DT,'MM') =to_char( to_date('" + installDate.ToString() + "', 'dd/MM/yyyy'),'MM')  AND TO_CHAR(RD.LN_INSTALLMENT_DT,'YYYY')=to_char( to_date('" + installDate.ToString() + "', 'dd/MM/yyyy'),'YYYY') ";
            sqlQuery += "  AND LRD.LN_PARTY_ID='" + bankId + "'";
            sqlQuery += "   UNION ";

            sqlQuery += "   SELECT '',IH.LN_CONTRACT_DTL_ID,'','',0,null,0,null,null ";
            sqlQuery += "   ,'PS' AS PS_PAY_TYPE, ID.LN_INT_DTL_ID,ID.LN_INT_ID,ID.LN_INSTALLMENT_NO,ID.LN_INSTALLMENT_DATE,ID.LN_REV_PROFIT_SHARE_AMT, ID.LN_PS_PAID_AMT,ID.LN_PS_PAID_DATE ";
            sqlQuery += "   FROM LN_INTEREST_DTL ID ";
            sqlQuery += "   INNER JOIN LN_INTEREST_HDR IH ON IH.LN_INT_ID= ID.LN_INT_ID ";
            sqlQuery += "   INNER JOIN LN_LOAN_REQUEST_DTL LRD ON LRD.LN_CONTRACT_ID = IH.LN_CONTRACT_DTL_ID ";
            sqlQuery += "   WHERE TO_CHAR(ID.LN_INSTALLMENT_DATE,'MM') =to_char( to_date('" + installDate.ToString() + "', 'dd/MM/yyyy'),'MM')  AND TO_CHAR(ID.LN_INSTALLMENT_DATE,'YYYY')=to_char( to_date('" + installDate.ToString() + "', 'dd/MM/yyyy'),'YYYY')";
            sqlQuery += "  AND LRD.LN_PARTY_ID='" + bankId + "'";
            sqlQuery += "   ) Z ";
            sqlQuery += " INNER JOIN LN_LOAN_REQUEST_DTL LRD ON LRD.LN_CONTRACT_ID= Z.LN_CONTRACT_ID ";
            sqlQuery += "   GROUP BY Z.LN_CONTRACT_ID,LRD.LN_CONTRACT_NUM    ";

            return sqlQuery;
        }
    }
}
