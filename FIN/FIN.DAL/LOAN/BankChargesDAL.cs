﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;

namespace FIN.DAL.LOAN
{
    public class BankChargesDAL
    {
        static string sqlQuery = "";
        public static string getProperty()
        {
            sqlQuery = string.Empty;

            sqlQuery += "  select lph.ln_property_id, lph.ln_property_name from ln_loan_property_hdr lph";
            //sqlQuery += " ,ln_loan_request_hdr  lrh  where lph.ln_property_id=lrh.ln_property_id";
            sqlQuery += " where lph.WORKFLOW_COMPLETION_STATUS = '1' ";
            sqlQuery += " AND lph.ENABLED_FLAG = '1' ";
            sqlQuery += " ORDER BY lph.ln_property_name ";

            return sqlQuery;
        }

        public static string getContractNumber()
        {
            sqlQuery = string.Empty;
            sqlQuery += "  select rd.ln_contract_id,rd.ln_contract_num   from ln_loan_request_dtl rd,";
            sqlQuery += " ln_loan_request_hdr rh";
            sqlQuery += " where rh.ln_request_id=rd.ln_request_id";
            sqlQuery += " and rd.WORKFLOW_COMPLETION_STATUS = '1' ";
            sqlQuery += " AND rd.ENABLED_FLAG = '1' ";
            sqlQuery += " ORDER BY rd.ln_contract_num ";

            return sqlQuery;
        }
    }
}
