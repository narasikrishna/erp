﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.LOAN
{
    public class PrincipalRepayPlan_DAL
    {
        static string sqlQuery = "";
        public static string GetLoanRepayDtls(string ln_prn_repay_id, string str_Contract_id = "")
        {
            sqlQuery = string.Empty;
            sqlQuery = " select rd.ln_prn_repay_dtl_id, rd.ln_prn_repay_id, rd.ln_installment_no,rd.ln_installment_dt,to_char(rd.ln_amount) as ln_amount, rd.ln_amount as loan_amount, to_char(rd.LN_PS_AMOUNT) as LN_PS_AMOUNT,LN_REVISED_PS_AMT, ";
            sqlQuery += " case rd.ENABLED_FLAG when '1' then 'TRUE' else 'FALSE' end as ENABLED_FLAG,'N' as DELETED";
            sqlQuery += "  from ln_repay_hdr rh, ln_repay_dtl rd ";
            sqlQuery += " where rh.ln_prn_repay_id = rd.ln_prn_repay_id ";
            if (ln_prn_repay_id.Length > 0)
            {
                sqlQuery += " and rh.ln_prn_repay_id=  '" + ln_prn_repay_id + "'";
            }
            if (str_Contract_id.Length > 0)
            {
                sqlQuery += " and rh.ln_contract_id=  '" + str_Contract_id + "'";
            }
            sqlQuery += " Order by ln_installment_dt ";
            return sqlQuery;

        }
    }
}
