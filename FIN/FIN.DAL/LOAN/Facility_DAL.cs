﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.LOAN
{
    public class Facility_DAL
    {
        
        public static string getFacilityName()
        {
            string sqlQuery = string.Empty;
            sqlQuery += " SELECT * FROM LN_LOAN_FACILITY_HDR ";
            return sqlQuery;
        }
        public static string getPorpertyname4Facility(string str_Facility_id)
        {
            string sqlQuery = string.Empty;
            sqlQuery = "SELECT PH.LN_PROPERTY_ID,PH.LN_PROPERTY_NAME FROM LN_LOAN_FACILITY_HDR FH ";
            sqlQuery += " INNER JOIN LN_LOAN_PROPERTY_HDR PH ON PH.LN_PROPERTY_ID= FH.LN_PROPERTY_ID ";
            sqlQuery += "  WHERE FH.LN_FACILITY_ID='" + str_Facility_id + "'";
            return sqlQuery;
        }

        public static string getLoanPropertyDetails()
        {
            string sqlQuery = string.Empty;

            sqlQuery += " SELECT lph.ln_property_id, lph.ln_property_name";
            sqlQuery += " FROM ln_loan_property_hdr lph";
            sqlQuery += " WHERE lph.enabled_flag=1";
            sqlQuery += " and lph.workflow_completion_status=1";
            sqlQuery += " ORDER BY lph.ln_property_name asc";

            return sqlQuery;
        }
        public static string get_FinancialDetailsReport()
        {
           string sqlQuery = string.Empty;
           sqlQuery = " SELECT * FROM VW_FINANCE_DETAILS V WHERE ROWNUM > 0 ";
           // sqlQuery += " AND V.org_id='" + VMVServices.Web.Utils.OrganizationID + "'";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["FACILTTY_ID"] != null)
                {
                    sqlQuery += " AND V.LN_FACILITY_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["FACILTTY_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["CONTRACT_ID"] != null)
                {
                    sqlQuery += " AND V.LN_CONTRACT_NUM = '" + VMVServices.Web.Utils.ReportViewFilterParameter["CONTRACT_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["PROPERTY_ID"] != null)
                {
                    sqlQuery += " AND V.LN_PROPERTY_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["PROPERTY_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["BANK_ID"] != null)
                {
                    sqlQuery += " AND V.BANK_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["BANK_ID"].ToString() + "'";
                }
            }
            return sqlQuery;
        }
        public static string get_AccruedExpenseReport()
        {
            string sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_LN_ACCRUED_EXPENSES V WHERE ROWNUM > 0 ";
            // sqlQuery += " AND V.org_id='" + VMVServices.Web.Utils.OrganizationID + "'";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
               
                if (VMVServices.Web.Utils.ReportViewFilterParameter["CONTRACT_ID"] != null)
                {
                    sqlQuery += " AND V.LN_CONTRACT_NUM = '" + VMVServices.Web.Utils.ReportViewFilterParameter["CONTRACT_ID"].ToString() + "'";
                }
               
                if (VMVServices.Web.Utils.ReportViewFilterParameter["BANK_ID"] != null)
                {
                    sqlQuery += " AND V.BANK_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["BANK_ID"].ToString() + "'";
                }
            }
            return sqlQuery;
        }

        public static string get_PaidInstallmentReport()
        {
            string sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM vw_loan_paid_installment V WHERE ROWNUM > 0 ";
            // sqlQuery += " AND V.org_id='" + VMVServices.Web.Utils.OrganizationID + "'";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {

                if (VMVServices.Web.Utils.ReportViewFilterParameter["CONTRACT_ID"] != null)
                {
                    sqlQuery += " AND V.LN_CONTRACT_NUM = '" + VMVServices.Web.Utils.ReportViewFilterParameter["CONTRACT_ID"].ToString() + "'";
                }

                if (VMVServices.Web.Utils.ReportViewFilterParameter["BANK_ID"] != null)
                {
                    sqlQuery += " AND V.BANK_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["BANK_ID"].ToString() + "'";
                }
            }
            return sqlQuery;
        }
        public static string getFinancePaymentReport()
        {
            string sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_FINANCE_PAYMENTS V WHERE ROWNUM > 0 ";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["FACILTTY_ID"] != null)
                {
                    sqlQuery += " AND V.LN_FACILITY_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["FACILTTY_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["CONTRACT_ID"] != null)
                {
                    sqlQuery += " AND V.finance_no = '" + VMVServices.Web.Utils.ReportViewFilterParameter["CONTRACT_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["BANK_ID"] != null)
                {
                    sqlQuery += " AND V.BANK_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["BANK_ID"].ToString() + "'";
                }
            }
            return sqlQuery;
        }

        public static string getFinancingAccountStatmentReport()
        {
            string sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM vw_finance_account_stmt V WHERE ROWNUM > 0 ";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["CONTRACT_TYPE"] != null)
                {
                    sqlQuery += " AND V.ln_contract_type = '" + VMVServices.Web.Utils.ReportViewFilterParameter["CONTRACT_TYPE"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["LN_CONTRACT_ID"] != null)
                {
                    sqlQuery += " AND V.ln_contract_id = '" + VMVServices.Web.Utils.ReportViewFilterParameter["LN_CONTRACT_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["BANK_ID"] != null)
                {
                    sqlQuery += " AND V.BANK_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["BANK_ID"].ToString() + "'";
                }
                //if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
                //{
                //    sqlQuery += " AND V.installment_date >= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy') ";
                //}
                //if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
                //{
                //    sqlQuery += " AND V.installment_date <= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["To_Date"].ToString() + "','dd/MM/yyyy') ";
                //}
            }
            return sqlQuery;
        }

        public static string getInstallment()
        {
            string sqlQuery = string.Empty;
            sqlQuery = "";
            sqlQuery += " select * from vw_ln_installment";
            return sqlQuery;
        }
        public static string getCostCentre()
        {
            string sqlQuery = string.Empty;
            sqlQuery = "";
            sqlQuery += " select * from vw_ln_cost_centre";
            return sqlQuery;
        }
        public static string getFacilityDtls(string facilityID)
        {
            string sqlQuery = string.Empty;
            sqlQuery += " SELECT * FROM LN_LOAN_FACILITY_HDR where ln_facility_id='" + facilityID + "'";
            return sqlQuery;
        }
    }
}
