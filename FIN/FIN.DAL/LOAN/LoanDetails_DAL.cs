﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;
namespace FIN.DAL.LOAN
{
    public class LoanDetails_DAL
    {
        static string sqlQuery = "";

        public static string getLoanInstallmentDetails(string inst_id)
        {
            sqlQuery = string.Empty;
            sqlQuery = "SELECT * FROM LN_APPROVED_LOAN_DTL WHERE LN_LOAN_DTL_ID='" + inst_id + "'";
            return sqlQuery;

        }


        public static string GetLoanDetails(string LN_LOAN_ID)
        {
            sqlQuery = string.Empty;

            //sqlQuery = " select LD.LN_LOAN_DTL_ID,LD.LN_INSTALLMENT_NO,LD.LN_INSTALLMENT_DT,LD.LN_LOAN_AMOUNT,LD.LN_PAID_AMOUNT,";
            //sqlQuery += " DS.CODE AS LOOKUP_ID,DS.DESCRIPTION AS LOOKUP_NAME,PS.CODE AS LOOKUP_ID,PS.DESCRIPTION AS LOOKUP_NAME,'N' AS DELETED,";
            //sqlQuery += " CASE LD.ENABLED_FLAG WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS ENABLED_FLAG";
            //sqlQuery += " FROM LN_APPROVED_LOAN_DTL LD,SSM_CODE_MASTERS DS,SSM_CODE_MASTERS PS";
            //sqlQuery += " WHERE LD.LN_DISTRIBUTION_STATUS = DS.CODE";
            //sqlQuery += " AND LD.LN_PS_TYPE = PS.CODE";
            //sqlQuery += " AND LD.LN_LOAN_ID = '" + LN_LOAN_ID + "'";

            sqlQuery += " select LD.LN_LOAN_DTL_ID,LD.LN_INSTALLMENT_NO,LD.LN_INSTALLMENT_DT,LD.LN_LOAN_AMOUNT,LD.LN_PAID_AMOUNT,";
            sqlQuery += " (select DS.CODE AS LOOKUP_ID from SSM_CODE_MASTERS DS where LD.LN_DISTRIBUTION_STATUS = DS.CODE and ds.parent_code='DISTRIBUTION_STATUS')LOOKUP_ID,";
            sqlQuery += " (select DS.DESCRIPTION" + VMVServices.Web.Utils.LanguageCode + " AS LOOKUP_NAME from SSM_CODE_MASTERS DS where LD.LN_DISTRIBUTION_STATUS = DS.CODE and ds.parent_code='DISTRIBUTION_STATUS')LOOKUP_NAME,";
            sqlQuery += " (select pS.CODE  from SSM_CODE_MASTERS pS where LD.LN_PS_TYPE = pS.CODE and ps.parent_code='PROFIT_SHR_TYP')ps_id,";
            sqlQuery += " (select pS.DESCRIPTION" + VMVServices.Web.Utils.LanguageCode + "  from SSM_CODE_MASTERS pS where LD.LN_PS_TYPE = pS.CODE and ps.parent_code='PROFIT_SHR_TYP')ps_name,";
            sqlQuery += " 'N' AS DELETED,CASE LD.ENABLED_FLAG WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS ENABLED_FLAG";
            sqlQuery += " FROM LN_APPROVED_LOAN_DTL LD";
            sqlQuery += " WHERE  LD.LN_LOAN_ID = '" + LN_LOAN_ID + "'";

            return sqlQuery;
        }


        public static string GetDocumentdtls(string entity_id)
        {
            sqlQuery = string.Empty;
            
            sqlQuery += " select d.doc_id,d.doc_type,d.doc_desc,'N'AS DELETED,";
            sqlQuery += "  case d.enabled_flag when '1' then 'TRUE' else 'FALSE' end as enabled_flag";
            sqlQuery += "  from ssm_documents d";
            sqlQuery += " where d.entity_id = '" + entity_id + "'";
            sqlQuery += " and d.workflow_completion_status = 1";
            return sqlQuery;
        }


        public static string GetLoanRequest(string str_mode)
        {
            sqlQuery = string.Empty;

            sqlQuery += " SELECT LR.LN_REQUEST_ID,LR.LN_REQUEST_DESC";
            sqlQuery += " FROM LN_LOAN_REQUEST LR";
            sqlQuery += " WHERE LR.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND LR.ENABLED_FLAG = 1";
            if (str_mode == FINTableConstant.Add)
            {
                sqlQuery += " AND LR.LN_REQUEST_ID NOT IN (select ln_request_id from ln_approved_loan_hdr) ";
            }
            return sqlQuery;
        }
        public static string GetPartyID()
        {
            sqlQuery = string.Empty;

            sqlQuery += " SELECT B.BANK_ID,B.BANK_NAME";
            sqlQuery += " FROM CA_BANK B";
            sqlQuery += " WHERE B.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND B.ENABLED_FLAG = 1";


            return sqlQuery;
        }

    }
}
