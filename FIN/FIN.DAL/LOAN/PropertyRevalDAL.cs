﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;

namespace FIN.DAL.LOAN
{
    public class PropertyRevalDAL
    {
        static string sqlQuery = "";

        public static string getRevalPropDetails(string Master_id)
        {
            sqlQuery = string.Empty;
            sqlQuery += " select LD.REVAL_DTL_ID,LD.REVAL_CHARGE,LD.REVAL_REMARKS,LD.REVAL_DT,";
            sqlQuery += " (select DS.CODE AS REVAL_TYPE from SSM_CODE_MASTERS DS where LD.REVAL_TYPE = DS.CODE and ds.parent_code='REVAL_TYPE')REVAL_TYPE,";
            sqlQuery += " 'N' AS DELETED,CASE LD.ENABLED_FLAG WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS ENABLED_FLAG";
            sqlQuery += " FROM ln_property_revaluation_dtl LD";
            sqlQuery += " WHERE  LD.REVAL_ID = '" + Master_id + "'";
            return sqlQuery;
        }


        public static string getProperty()
        {
            sqlQuery = string.Empty;

            sqlQuery += "  select lph.ln_property_id, lph.ln_property_name,lrh.LN_REQUEST_ID from ln_loan_property_hdr lph,";
            sqlQuery += " ln_loan_request_hdr  lrh  where lph.ln_property_id=lrh.ln_property_id";
            sqlQuery += " and lph.WORKFLOW_COMPLETION_STATUS = '1' ";
            sqlQuery += " AND lph.ENABLED_FLAG = '1' ";
            sqlQuery += " ORDER BY lph.ln_property_name ";

            return sqlQuery;
        }
    }
}
