﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;

namespace FIN.DAL.LOAN
{
    public class PropertyPledgeDAL
    {
        static string sqlQuery = "";
        public static string getProperty(string Bank_id)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select lph.ln_property_id,lph.ln_property_name from  ln_loan_property_hdr  lph, ln_loan_request_hdr rh,";
            sqlQuery += " ln_loan_request_dtl rd";
            sqlQuery += " where rh.ln_request_id=rd.ln_request_id";
            sqlQuery += " and rh.ln_property_id=lph.ln_property_id";
            sqlQuery += " and rd.ln_party_id='" + Bank_id + "'";
            sqlQuery += " and lph.WORKFLOW_COMPLETION_STATUS = '1' ";
            sqlQuery += " AND lph.ENABLED_FLAG = '1' ";
            sqlQuery += " ORDER BY lph.ln_property_name ";

            return sqlQuery;
        }

    }
}
