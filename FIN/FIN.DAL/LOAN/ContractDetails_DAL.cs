﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.LOAN
{
    public class ContractDetails_DAL
    {
        public static string getContractNumber4PropFacility(string str_Property_id, string str_Facility_id)
        {
            string sqlQuery = string.Empty;
            sqlQuery += " SELECT RD.LN_CONTRACT_ID,RD.LN_CONTRACT_NUM FROM LN_LOAN_REQUEST_DTL RD ";
            sqlQuery += " INNER JOIN LN_LOAN_REQUEST_HDR RH ON RD.LN_REQUEST_ID= RH.LN_REQUEST_ID ";
            sqlQuery += " WHERE RH.LN_PROPERTY_ID='" + str_Property_id + "' AND  RH.LN_FACILITY_ID='" + str_Facility_id + "'";

            return sqlQuery;
        }


        public static string getContractNumberByFacilty(string str_Facility_id)
        {
            string sqlQuery = string.Empty;
            sqlQuery += " SELECT RD.LN_CONTRACT_ID,RD.LN_CONTRACT_NUM FROM LN_LOAN_REQUEST_DTL RD,ln_loan_request_hdr rh ";
            sqlQuery += " where rh.ln_request_id = rd.ln_request_id ";
            sqlQuery += " and rh.ln_facility_id = '" + str_Facility_id + "'";
            sqlQuery += " order by rd.ln_contract_num";
            return sqlQuery;
        }
        public static string getContractNumberByProperty(string str_Property_id)
        {
            string sqlQuery = string.Empty;
            sqlQuery += " SELECT RD.LN_CONTRACT_ID,RD.LN_CONTRACT_NUM FROM LN_LOAN_REQUEST_DTL RD,ln_loan_request_hdr rh ";
            sqlQuery += " where rh.ln_request_id = rd.ln_request_id ";
            sqlQuery += " and rh.ln_property_id = '" + str_Property_id + "'";
            sqlQuery += " order by rd.ln_contract_num";
            return sqlQuery;
        }
        public static string getContractNumberByBank(string str_bank_id)
        {
            string sqlQuery = string.Empty;
            sqlQuery += " SELECT RD.LN_CONTRACT_ID,RD.LN_CONTRACT_NUM FROM LN_LOAN_REQUEST_DTL RD,ln_loan_request_hdr rh ";
            sqlQuery += " where rh.ln_request_id = rd.ln_request_id ";
            sqlQuery += " and rd.ln_party_id = '" + str_bank_id + "'";
            sqlQuery += " order by rd.ln_contract_num";
            return sqlQuery;
        }
        public static string getContractNumberByBankFacility(string facilityID, string bankID)
        {
            string sqlQuery = string.Empty;
            sqlQuery += " SELECT RD.LN_CONTRACT_ID,RD.LN_CONTRACT_NUM FROM LN_LOAN_REQUEST_DTL RD,ln_loan_request_hdr rh ";
            sqlQuery += " where rh.ln_request_id = rd.ln_request_id ";

            if (bankID != string.Empty)
            {
                sqlQuery += " and rd.ln_party_id = '" + bankID + "'";
            }
            if (facilityID != string.Empty)
            {
                sqlQuery += " and rh.ln_facility_id = '" + facilityID + "'";
            }
            sqlQuery += " order by rd.ln_contract_num";

            return sqlQuery;
        }
        public static string getContractNumberByAll(string str_Facility_id, string str_Property_id, string str_bank_id)
        {
            string sqlQuery = string.Empty;
            sqlQuery += " SELECT RD.LN_CONTRACT_ID,RD.LN_CONTRACT_NUM FROM LN_LOAN_REQUEST_DTL RD,ln_loan_request_hdr rh ";
            sqlQuery += " where rh.ln_request_id = rd.ln_request_id ";
            if (str_bank_id != string.Empty)
            {
                sqlQuery += " and rd.ln_party_id = '" + str_bank_id + "'";
            }
            if (str_Property_id != string.Empty)
            {
                sqlQuery += " and rh.ln_property_id = '" + str_Property_id + "'";
            }
            if (str_Facility_id != string.Empty)
            {
                sqlQuery += " and rh.ln_facility_id = '" + str_Facility_id + "'";
            }
            sqlQuery += " order by rd.ln_contract_num";
            return sqlQuery;
        }
        public static string getContractNumber()
        {
            string sqlQuery = string.Empty;
            sqlQuery += " SELECT RD.LN_CONTRACT_ID,RD.LN_CONTRACT_NUM FROM LN_LOAN_REQUEST_DTL RD ";
            sqlQuery += " order by RD.LN_CONTRACT_NUM ";
            return sqlQuery;
        }

        public static string getDebitCreditCostCentreDtls()
        {
            string sqlQuery = string.Empty;
            sqlQuery += " select segment_value_id, segment_value from gl_segment_values where segment_id='SEG_ID-0000000101'";
            return sqlQuery;
        }

        public static string getContractType()
        {
            string sqlQuery = string.Empty;
            
            sqlQuery += " SELECT RD.LN_CONTRACT_ID, RD.LN_CONTRACT_NUM, ";
            sqlQuery += " RD.LN_CONTRACT_TYPE, RD.LN_CONTRACT_NUM || ' - ' || RD.LN_CONTRACT_TYPE AS CONTRACT_NUM_TYPE";
            sqlQuery += " FROM LN_LOAN_REQUEST_DTL RD ";
            sqlQuery += " ORDER BY RD.LN_CONTRACT_NUM";

            return sqlQuery;
        }

    }
}
