﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;

using System.Data.EntityClient;
using System.Data.Entity.Infrastructure;

namespace FIN.DAL.LOAN
{
    public class LoanProvisionDAL
    {
        static string sql_Query = string.Empty;
        static string sqlQuery = string.Empty;
        public static string getLoanProvisionDet(string str_date, string str_bank_id)
        {
             sql_Query = string.Empty;

            sql_Query += " SELECT LN_CONTRACT_ID,LN_CONTRACT_NUM,LN_CONTRACT_DESC,PROVISION_DATE,PREVIOUS_PROVISION_DATE,LN_CONTRACT_DT,LN_ROI,LN_PRINCIPAL_AMOUNT,LN_PAID_AMT  ";
            sql_Query += " ,TO_CHAR(CASE WHEN ((PROVISION_DATE-PREVIOUS_PROVISION_DATE) * ((LN_ROI/100)/365) * (LN_PRINCIPAL_AMOUNT-LN_PAID_AMT)) <0 THEN 0 ELSE (PROVISION_DATE-PREVIOUS_PROVISION_DATE) * ((LN_ROI/100)/365) * (LN_PRINCIPAL_AMOUNT-LN_PAID_AMT) END)  as LN_PROFIT_SHARE_AMT ";
            sql_Query += " ,(PROVISION_DATE-PREVIOUS_PROVISION_DATE) as LN_NO_OF_DAYS ";
            sql_Query += ", (LN_PRINCIPAL_AMOUNT-LN_PAID_AMT) as REM_PRINCIPAL_AMT ";
            sql_Query += " FROM ( ";
            sql_Query += " SELECT LRD.LN_CONTRACT_ID,LRD.LN_CONTRACT_NUM,LRD.LN_CONTRACT_DESC ";
            sql_Query += " ,TO_DATE('" + str_date + "','DD/MM/YYYY') AS PROVISION_DATE ";
            sql_Query += " ,NVL((SELECT MAX(LP.LN_PROVISION_DATE) FROM LN_LOAN_PROVISION LP WHERE LP.LN_CONTRACT_ID = LRD.LN_CONTRACT_ID),LRD.LN_CONTRACT_DT -1) AS PREVIOUS_PROVISION_DATE ";
            sql_Query += " ,LRD.LN_CONTRACT_DT ";
            sql_Query += " ,LRD.LN_ROI ";
            sql_Query += " ,LRD.LN_PRINCIPAL_AMOUNT ";
            sql_Query += " ,NVL((SELECT SUM(CPH.LN_PAID_AMT) FROM LN_CONTRACT_PAYMENT_HDR CPH  WHERE CPH.LN_CONTRACT_ID= LRD.LN_CONTRACT_ID),0) AS LN_PAID_AMT ";
            sql_Query += " FROM LN_LOAN_REQUEST_DTL LRD ";
            sql_Query += "  WHERE LRD.LN_CONTRACT_DT <= TO_DATE('" + str_date + "','DD/MM/YYYY') ";
            sql_Query += " AND LRD.LN_PARTY_ID='" + str_bank_id + "'";
            sql_Query += "  ) Z ";
            return sql_Query;
        }

        public static int IsLoanProvisionExists(string provisionDate)
        {
            sqlQuery = string.Empty;
            int count = 0;
            sqlQuery = "   select count(1) as counts from lN_LOAN_PROVISION ll";
            sqlQuery += "   where ll.ln_provision_date>=to_date('" + provisionDate + "','dd/MM/yyyy')";

            DataTable dtData = new DataTable();

            dtData = DBMethod.ExecuteQuery(sqlQuery).Tables[0];
            if (dtData != null)
            {
                if (dtData.Rows.Count > 0)
                {
                    count = int.Parse(dtData.Rows[0]["counts"].ToString());
                }
            }
            return count;
        }
        public static int IsLoanProvisionPosted(string provisionId)
        {
            sqlQuery = string.Empty;
            int count = 0;

            sqlQuery = "      select count(1) as counts";
            sqlQuery += "   from lN_LOAN_PROVISION ll";
            sqlQuery += "  where ll.posted_flag = 1";
            sqlQuery += "  and ll.ln_provision_id = '" + provisionId + "'";

            DataTable dtData = new DataTable();

            dtData = DBMethod.ExecuteQuery(sqlQuery).Tables[0];
            if (dtData != null)
            {
                if (dtData.Rows.Count > 0)
                {
                    count = int.Parse(dtData.Rows[0]["counts"].ToString());
                }
            }
            return count;
        }
        public static int IsLoanPaymentPosted(string provisionId)
        {
            sqlQuery = string.Empty;
            int count = 0;

            sqlQuery = "      select count(1) as counts";
            sqlQuery += "   from lN_CONTRACT_PAYMENT_HDR ll";
            sqlQuery += "  where ll.posted_flag = 1";
            sqlQuery += "  and ll.ln_provision_id = '" + provisionId + "'";

            DataTable dtData = new DataTable();

            dtData = DBMethod.ExecuteQuery(sqlQuery).Tables[0];
            if (dtData != null)
            {
                if (dtData.Rows.Count > 0)
                {
                    count = int.Parse(dtData.Rows[0]["counts"].ToString());
                }
            }
            return count;
        }
        public static int IsLoanPaymentExists(string provisionDate)
        {
            sqlQuery = string.Empty;
            int count = 0;
            sqlQuery = "   select count(1) as counts from lN_CONTRACT_PAYMENT_HDR ll";
            sqlQuery += "   where ll.LN_PAYMENT_DT>=to_date('" + provisionDate + "','dd/MM/yyyy')";

            DataTable dtData = new DataTable();

            dtData = DBMethod.ExecuteQuery(sqlQuery).Tables[0];
            if (dtData != null)
            {
                if (dtData.Rows.Count > 0)
                {
                    count = int.Parse(dtData.Rows[0]["counts"].ToString());
                }
            }
            return count;
        }

        public static int IsLoanContractExists(string provisionDate)
        {
            sqlQuery = string.Empty;
            int count = 0;
            sqlQuery = "   select count(1) as counts from lN_LOAN_REQUEST_DTL ll";
            sqlQuery += "   where ll.LN_CONTRACT_DT>=to_date('" + provisionDate + "','dd/MM/yyyy')";

            DataTable dtData = new DataTable();

            dtData = DBMethod.ExecuteQuery(sqlQuery).Tables[0];
            if (dtData != null)
            {
                if (dtData.Rows.Count > 0)
                {
                    count = int.Parse(dtData.Rows[0]["counts"].ToString());
                }
            }
            return count;
        }
        public static int IsLoanContractPosted(string provisionId)
        {
            sqlQuery = string.Empty;
            int count = 0;

            sqlQuery = "      select count(1) as counts";
            sqlQuery += "   from lN_LOAN_REQUEST_DTL ll";
            sqlQuery += "  where ll.posted_flag = 1";
            sqlQuery += "  and ll.LN_CONTRACT_ID = '" + provisionId + "'";

            DataTable dtData = new DataTable();

            dtData = DBMethod.ExecuteQuery(sqlQuery).Tables[0];
            if (dtData != null)
            {
                if (dtData.Rows.Count > 0)
                {
                    count = int.Parse(dtData.Rows[0]["counts"].ToString());
                }
            }
            return count;
        }
        public static DataTable get_LoanProvisonAmt(string str_date, string str_bank_id)
        {
            string strValue = string.Empty;
            string strNextCode = string.Empty;
            string strErrMsg = string.Empty;

            OracleCommand oraCmd = new OracleCommand();
            oraCmd.CommandText = "LN_PROVISION_AMT";
            oraCmd.CommandType = CommandType.StoredProcedure;
            oraCmd.Parameters.Add("@P_BANK_ID", OracleDbType.Varchar2).Value = str_bank_id;
            oraCmd.Parameters.Add("@P_PROV_DATE", OracleDbType.Varchar2).Value = str_date;            
            oraCmd.Parameters.Add("@P_DATA", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            return DBMethod.ExecuteStoredProcedure4CursorOutput(oraCmd);
        }
    }
}
