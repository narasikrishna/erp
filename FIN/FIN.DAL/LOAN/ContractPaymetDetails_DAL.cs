﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.LOAN
{
    public class ContractPaymetDetails_DAL
    {

        public static string getPaymentDetails(string str_Bank_ID, string str_process_dt)
        {
            string sqlQuery = "";
            sqlQuery += " SELECT CPH.LN_CON_PAY_ID,CPH.LN_CONTRACT_ID,LRD.LN_CONTRACT_NUM ,TO_CHAR( CPH.TOTAL_PRICE_AMT )as LN_PRINCIPAL_AMOUNT,TO_CHAR(CPH.PAID_PRICE_AMT) as PAID_PRICE_AMT,TO_CHAR(CPH.TOTAL_PRICE_AMT-CPH.PAID_PRICE_AMT) AS BALANCE_PRINCIPAL_AMT  ";
            sqlQuery += " ,TO_CHAR(CPH.TOTAL_PS_AMT) as TOTAL_PS_AMT,TO_CHAR(CPH.PAID_PS_AMT) as PAID_PS_AMT,TO_CHAR(CPH.TOTAL_PS_AMT- CPH.PAID_PS_AMT) AS BALANCE_PS_AMT,TO_CHAR(CPH.LN_PAID_AMT) AS LN_PAID_AMT,TO_CHAR(CPH.LS_PS_PAID_AMT) AS LN_PS_PAID_AMT ";
            sqlQuery += "  FROM LN_CONTRACT_PAYMENT_HDR CPH ";
            sqlQuery += " INNER JOIN LN_LOAN_REQUEST_DTL LRD ON LRD.LN_CONTRACT_ID= CPH.LN_CONTRACT_ID ";
            sqlQuery += " WHERE TO_CHAR(CPH.LN_PAYMENT_DT ,'MM')= TO_CHAR(TO_DATE('" + str_process_dt + "','DD/MM/YYYY'),'MM') AND TO_CHAR(CPH.LN_PAYMENT_DT ,'YYYY')= TO_CHAR(TO_DATE('" + str_process_dt + "','DD/MM/YYYY'),'YYYY')  ";
            sqlQuery += " AND TO_CHAR(CPH.LN_PAYMENT_DT ,'DD')= TO_CHAR(TO_DATE('" + str_process_dt + "','DD/MM/YYYY'),'DD')  ";
            sqlQuery += " AND LRD.LN_PARTY_ID='" + str_Bank_ID + "'";
            sqlQuery += " UNION  ";
            sqlQuery += " SELECT '0' as LN_CON_PAY_ID ,LN_CONTRACT_ID,LN_CONTRACT_NUM,TO_CHAR(LN_PRINCIPAL_AMOUNT),TO_CHAR(PAID_PRICE_AMT),TO_CHAR(LN_PRINCIPAL_AMOUNT-PAID_PRICE_AMT) AS BALANCE_PRINCIPAL_AMT,TO_CHAR(TOTAL_PS_AMT),TO_CHAR(PAID_PS_AMT),TO_CHAR(TOTAL_PS_AMT- PAID_PS_AMT) AS BALANCE_PS_AMT ";
            sqlQuery += " ,'0' as LN_PAID_AMT,'0' AS LN_PS_PAID_AMT ";
            sqlQuery += " FROM ( ";
            sqlQuery += " SELECT LRD.LN_CONTRACT_ID,LRD.LN_CONTRACT_NUM ";
            sqlQuery += " ,(SELECT SUM(LN_AMOUNT) FROM LN_REPAY_DTL RD INNER JOIN LN_REPAY_HDR RH ON RH.Ln_Prn_Repay_Id=RD.LN_PRN_REPAY_ID WHERE RD.LN_INSTALLMENT_DT <TO_DATE('" + str_process_dt + "','DD/MM/YYYY') AND RH.LN_CONTRACT_ID=LRD.LN_CONTRACT_ID)  AS LN_PRINCIPAL_AMOUNT  ";
            sqlQuery += " ,(SELECT NVL(SUM(LN_PAID_AMT),0) FROM LN_CONTRACT_PAYMENT_HDR LCPH WHERE LCPH.LN_CONTRACT_ID= LRD.LN_CONTRACT_ID) AS PAID_PRICE_AMT ";
            sqlQuery += " ,(SELECT NVL(SUM(ID.LN_REV_PROFIT_SHARE_AMT),0) FROM LN_INTEREST_DTL ID INNER JOIN LN_INTEREST_HDR IH  ON ID.LN_INT_ID = IH.LN_INT_ID WHERE ID.LN_INSTALLMENT_DATE <TO_DATE('" + str_process_dt + "','DD/MM/YYYY') AND IH.LN_CONTRACT_DTL_ID= LRD.LN_CONTRACT_ID) AS TOTAL_PS_AMT ";
            sqlQuery += " ,(SELECT NVL(SUM(LS_PS_PAID_AMT),0) FROM LN_CONTRACT_PAYMENT_HDR LCPH WHERE LCPH.LN_CONTRACT_ID= LRD.LN_CONTRACT_ID) AS PAID_PS_AMT ";
            sqlQuery += "  FROM LN_LOAN_REQUEST_DTL LRD ";
            sqlQuery += " WHERE LRD.LN_CONTRACT_ID NOT IN  (SELECT CPH.LN_CONTRACT_ID FROM LN_CONTRACT_PAYMENT_HDR CPH WHERE TO_CHAR(CPH.LN_PAYMENT_DT ,'MM')= TO_CHAR(TO_DATE('" + str_process_dt + "','DD/MM/YYYY'),'MM') AND TO_CHAR(CPH.LN_PAYMENT_DT ,'YYYY')= TO_CHAR(TO_DATE('" + str_process_dt + "','DD/MM/YYYY'),'YYYY')  ";
            sqlQuery += " AND TO_CHAR(CPH.LN_PAYMENT_DT ,'DD')= TO_CHAR(TO_DATE('" + str_process_dt + "','DD/MM/YYYY'),'DD')";
            sqlQuery += " ) ";
            sqlQuery += " AND LRD.LN_PARTY_ID='" + str_Bank_ID + "'";
            sqlQuery += "  ) ";
            sqlQuery += "  Z WHERE (LN_PRINCIPAL_AMOUNT-PAID_PRICE_AMT >0 OR TOTAL_PS_AMT- PAID_PS_AMT >0 )";
            return sqlQuery;
        }


        public static string getPaymentDetails4Contrat(string str_contract_id)
        {
            string sqlQuery = "";
            sqlQuery += " SELECT * FROM LN_CONTRACT_PAYMENT_HDR WHERE LN_CONTRACT_ID='" + str_contract_id + "'";
            return sqlQuery;
        }


        public static string getPaymentDetailswithProvision(string str_Bank_id, string str_date)
        {
            string sqlQuery = "";

            sqlQuery += " select CPH.LN_CON_PAY_ID,CPH.LN_CONTRACT_ID,LRD.LN_CONTRACT_NUM,LRD.LN_CONTRACT_DESC,'0' AS LN_PRINCIPAL_AMOUNT ";
            sqlQuery += " ,TO_CHAR(CPH.PAID_PRICE_AMT) AS LN_PAID_AMT,TO_CHAR(CPH.TOTAL_PS_AMT) AS BAL_PRIN_AMT,TO_CHAR(CPH.LN_PAID_AMT) AS LN_CURR_PAID_AMT ";
            sqlQuery += " ,TO_CHAR(CPH.LS_PS_PAID_AMT) AS LN_PS_PAID_AMT,NVL(CPH.JE_HDR_ID,0) as JE_HDR_ID ";
            sqlQuery += " FROM LN_CONTRACT_PAYMENT_HDR CPH ";
            sqlQuery += " INNER JOIN LN_LOAN_REQUEST_DTL LRD ON LRD.LN_CONTRACT_ID= CPH.LN_CONTRACT_ID ";
            sqlQuery += " WHERE LRD.LN_PARTY_ID='" + str_Bank_id + "'";
            sqlQuery += " AND CPH.LN_PAYMENT_DT = TO_DATE('" + str_date + "','DD/MM/YYYY')";

            sqlQuery += " UNION ";

            sqlQuery += " SELECT DISTINCT '0' as LN_CON_PAY_ID, LN_CONTRACT_ID,LN_CONTRACT_NUM,LN_CONTRACT_DESC,TO_CHAR(LN_PRINCIPAL_AMOUNT) AS LN_PRINCIPAL_AMOUNT ,TO_CHAR(LN_PAID_AMT) AS LN_PAID_AMT,TO_CHAR(LN_PRINCIPAL_AMOUNT-LN_PAID_AMT) AS BAL_PRIN_AMT,'0' AS LN_CURR_PAID_AMT,TO_CHAR(LN_PS_AMT-LN_PS_PAID_AMT) AS LN_PS_PAID_AMT,'0' AS JE_HDR_ID";
            sqlQuery += " FROM ( ";
            sqlQuery += " SELECT LRD.LN_CONTRACT_ID,LRD.LN_CONTRACT_NUM,LRD.LN_CONTRACT_DESC,LRD.LN_PRINCIPAL_AMOUNT ";
            sqlQuery += " ,NVL((SELECT SUM(CPH.LN_PAID_AMT) FROM LN_CONTRACT_PAYMENT_HDR CPH  WHERE CPH.LN_CONTRACT_ID= LRD.LN_CONTRACT_ID),0) AS LN_PAID_AMT ";
            sqlQuery += " ,NVL((SELECT SUM(LP.LN_PROFIT_SHARE_AMT) FROM LN_LOAN_PROVISION LP WHERE LP.LN_CONTRACT_ID= LRD.LN_CONTRACT_ID ),0) AS LN_PS_AMT ";
            sqlQuery += " ,NVL((SELECT SUM(CPH.LS_PS_PAID_AMT) FROM LN_CONTRACT_PAYMENT_HDR CPH  WHERE CPH.LN_CONTRACT_ID= LRD.LN_CONTRACT_ID),0) AS LN_PS_PAID_AMT ";
            sqlQuery += " FROM LN_LOAN_REQUEST_DTL LRD ";
            sqlQuery += " INNER JOIN LN_LOAN_PROVISION LP ON LP.LN_CONTRACT_ID= LRD.LN_CONTRACT_ID  ";
            sqlQuery += "  WHERE LRD.LN_PARTY_ID='" + str_Bank_id + "'";
            sqlQuery += " AND LRD.LN_CONTRACT_ID NOT IN (SELECT CPH.LN_CONTRACT_ID FROM LN_CONTRACT_PAYMENT_HDR CPH WHERE CPH.LN_PAYMENT_DT = TO_DATE('" + str_date + "','DD/MM/YYYY'))";
            sqlQuery += "  ) Z ";

            return sqlQuery;
        }
    }
}
