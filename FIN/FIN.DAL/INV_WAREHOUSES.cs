//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.Serialization;

namespace FIN.DAL
{
    [DataContract(IsReference = true)]
    [KnownType(typeof(INV_RECEIPT_ITEM_WH_HDR))]
    [KnownType(typeof(INV_TRANSACTION_LEDGER))]
    [KnownType(typeof(INV_TRANSFER_HDR))]
    [KnownType(typeof(INV_WAREHOUSE_FACILITIES))]
    [KnownType(typeof(INV_WAREHOUSE_LOCATIONS))]
    [KnownType(typeof(PO_HEADERS))]
    [KnownType(typeof(PO_LINES))]
    [KnownType(typeof(MATERIAL_RETURN_DTL))]
    public partial class INV_WAREHOUSES: IObjectWithChangeTracker, INotifyPropertyChanged
    {
        #region Primitive Properties
    
        [DataMember]
        public Nullable<int> PK_ID
        {
            get { return _pK_ID; }
            set
            {
                if (_pK_ID != value)
                {
                    _pK_ID = value;
                    OnPropertyChanged("PK_ID");
                }
            }
        }
        private Nullable<int> _pK_ID;
    
        [DataMember]
        public Nullable<int> CHILD_ID
        {
            get { return _cHILD_ID; }
            set
            {
                if (_cHILD_ID != value)
                {
                    _cHILD_ID = value;
                    OnPropertyChanged("CHILD_ID");
                }
            }
        }
        private Nullable<int> _cHILD_ID;
    
        [DataMember]
        public string INV_WH_ID
        {
            get { return _iNV_WH_ID; }
            set
            {
                if (_iNV_WH_ID != value)
                {
                    if (ChangeTracker.ChangeTrackingEnabled && ChangeTracker.State != ObjectState.Added)
                    {
                        throw new InvalidOperationException("The property 'INV_WH_ID' is part of the object's key and cannot be changed. Changes to key properties can only be made when the object is not being tracked or is in the Added state.");
                    }
                    _iNV_WH_ID = value;
                    OnPropertyChanged("INV_WH_ID");
                }
            }
        }
        private string _iNV_WH_ID;
    
        [DataMember]
        public string INV_WH_NAME
        {
            get { return _iNV_WH_NAME; }
            set
            {
                if (_iNV_WH_NAME != value)
                {
                    _iNV_WH_NAME = value;
                    OnPropertyChanged("INV_WH_NAME");
                }
            }
        }
        private string _iNV_WH_NAME;
    
        [DataMember]
        public string INV_WH_TYPE
        {
            get { return _iNV_WH_TYPE; }
            set
            {
                if (_iNV_WH_TYPE != value)
                {
                    _iNV_WH_TYPE = value;
                    OnPropertyChanged("INV_WH_TYPE");
                }
            }
        }
        private string _iNV_WH_TYPE;
    
        [DataMember]
        public string INV_WH_CONTACT
        {
            get { return _iNV_WH_CONTACT; }
            set
            {
                if (_iNV_WH_CONTACT != value)
                {
                    _iNV_WH_CONTACT = value;
                    OnPropertyChanged("INV_WH_CONTACT");
                }
            }
        }
        private string _iNV_WH_CONTACT;
    
        [DataMember]
        public string INV_WH_DESC
        {
            get { return _iNV_WH_DESC; }
            set
            {
                if (_iNV_WH_DESC != value)
                {
                    _iNV_WH_DESC = value;
                    OnPropertyChanged("INV_WH_DESC");
                }
            }
        }
        private string _iNV_WH_DESC;
    
        [DataMember]
        public string INV_WH_STATUS
        {
            get { return _iNV_WH_STATUS; }
            set
            {
                if (_iNV_WH_STATUS != value)
                {
                    _iNV_WH_STATUS = value;
                    OnPropertyChanged("INV_WH_STATUS");
                }
            }
        }
        private string _iNV_WH_STATUS;
    
        [DataMember]
        public string ENABLED_FLAG
        {
            get { return _eNABLED_FLAG; }
            set
            {
                if (_eNABLED_FLAG != value)
                {
                    _eNABLED_FLAG = value;
                    OnPropertyChanged("ENABLED_FLAG");
                }
            }
        }
        private string _eNABLED_FLAG;
    
        [DataMember]
        public string WORKFLOW_COMPLETION_STATUS
        {
            get { return _wORKFLOW_COMPLETION_STATUS; }
            set
            {
                if (_wORKFLOW_COMPLETION_STATUS != value)
                {
                    _wORKFLOW_COMPLETION_STATUS = value;
                    OnPropertyChanged("WORKFLOW_COMPLETION_STATUS");
                }
            }
        }
        private string _wORKFLOW_COMPLETION_STATUS;
    
        [DataMember]
        public string CREATED_BY
        {
            get { return _cREATED_BY; }
            set
            {
                if (_cREATED_BY != value)
                {
                    _cREATED_BY = value;
                    OnPropertyChanged("CREATED_BY");
                }
            }
        }
        private string _cREATED_BY;
    
        [DataMember]
        public System.DateTime CREATED_DATE
        {
            get { return _cREATED_DATE; }
            set
            {
                if (_cREATED_DATE != value)
                {
                    _cREATED_DATE = value;
                    OnPropertyChanged("CREATED_DATE");
                }
            }
        }
        private System.DateTime _cREATED_DATE;
    
        [DataMember]
        public string MODIFIED_BY
        {
            get { return _mODIFIED_BY; }
            set
            {
                if (_mODIFIED_BY != value)
                {
                    _mODIFIED_BY = value;
                    OnPropertyChanged("MODIFIED_BY");
                }
            }
        }
        private string _mODIFIED_BY;
    
        [DataMember]
        public Nullable<System.DateTime> MODIFIED_DATE
        {
            get { return _mODIFIED_DATE; }
            set
            {
                if (_mODIFIED_DATE != value)
                {
                    _mODIFIED_DATE = value;
                    OnPropertyChanged("MODIFIED_DATE");
                }
            }
        }
        private Nullable<System.DateTime> _mODIFIED_DATE;
    
        [DataMember]
        public string ATTRIBUTE1
        {
            get { return _aTTRIBUTE1; }
            set
            {
                if (_aTTRIBUTE1 != value)
                {
                    _aTTRIBUTE1 = value;
                    OnPropertyChanged("ATTRIBUTE1");
                }
            }
        }
        private string _aTTRIBUTE1;
    
        [DataMember]
        public string ATTRIBUTE2
        {
            get { return _aTTRIBUTE2; }
            set
            {
                if (_aTTRIBUTE2 != value)
                {
                    _aTTRIBUTE2 = value;
                    OnPropertyChanged("ATTRIBUTE2");
                }
            }
        }
        private string _aTTRIBUTE2;
    
        [DataMember]
        public string ATTRIBUTE3
        {
            get { return _aTTRIBUTE3; }
            set
            {
                if (_aTTRIBUTE3 != value)
                {
                    _aTTRIBUTE3 = value;
                    OnPropertyChanged("ATTRIBUTE3");
                }
            }
        }
        private string _aTTRIBUTE3;
    
        [DataMember]
        public string ATTRIBUTE4
        {
            get { return _aTTRIBUTE4; }
            set
            {
                if (_aTTRIBUTE4 != value)
                {
                    _aTTRIBUTE4 = value;
                    OnPropertyChanged("ATTRIBUTE4");
                }
            }
        }
        private string _aTTRIBUTE4;
    
        [DataMember]
        public string ATTRIBUTE5
        {
            get { return _aTTRIBUTE5; }
            set
            {
                if (_aTTRIBUTE5 != value)
                {
                    _aTTRIBUTE5 = value;
                    OnPropertyChanged("ATTRIBUTE5");
                }
            }
        }
        private string _aTTRIBUTE5;
    
        [DataMember]
        public string ATTRIBUTE6
        {
            get { return _aTTRIBUTE6; }
            set
            {
                if (_aTTRIBUTE6 != value)
                {
                    _aTTRIBUTE6 = value;
                    OnPropertyChanged("ATTRIBUTE6");
                }
            }
        }
        private string _aTTRIBUTE6;
    
        [DataMember]
        public string ATTRIBUTE7
        {
            get { return _aTTRIBUTE7; }
            set
            {
                if (_aTTRIBUTE7 != value)
                {
                    _aTTRIBUTE7 = value;
                    OnPropertyChanged("ATTRIBUTE7");
                }
            }
        }
        private string _aTTRIBUTE7;
    
        [DataMember]
        public string ATTRIBUTE8
        {
            get { return _aTTRIBUTE8; }
            set
            {
                if (_aTTRIBUTE8 != value)
                {
                    _aTTRIBUTE8 = value;
                    OnPropertyChanged("ATTRIBUTE8");
                }
            }
        }
        private string _aTTRIBUTE8;
    
        [DataMember]
        public string ATTRIBUTE9
        {
            get { return _aTTRIBUTE9; }
            set
            {
                if (_aTTRIBUTE9 != value)
                {
                    _aTTRIBUTE9 = value;
                    OnPropertyChanged("ATTRIBUTE9");
                }
            }
        }
        private string _aTTRIBUTE9;
    
        [DataMember]
        public string ATTRIBUTE10
        {
            get { return _aTTRIBUTE10; }
            set
            {
                if (_aTTRIBUTE10 != value)
                {
                    _aTTRIBUTE10 = value;
                    OnPropertyChanged("ATTRIBUTE10");
                }
            }
        }
        private string _aTTRIBUTE10;
    
        [DataMember]
        public string ORG_ID
        {
            get { return _oRG_ID; }
            set
            {
                if (_oRG_ID != value)
                {
                    _oRG_ID = value;
                    OnPropertyChanged("ORG_ID");
                }
            }
        }
        private string _oRG_ID;
    
        [DataMember]
        public string INV_WH_NAME_OL
        {
            get { return _iNV_WH_NAME_OL; }
            set
            {
                if (_iNV_WH_NAME_OL != value)
                {
                    _iNV_WH_NAME_OL = value;
                    OnPropertyChanged("INV_WH_NAME_OL");
                }
            }
        }
        private string _iNV_WH_NAME_OL;

        #endregion
        #region Navigation Properties
    
        [DataMember]
        public TrackableCollection<INV_RECEIPT_ITEM_WH_HDR> INV_RECEIPT_ITEM_WH_HDR
        {
            get
            {
                if (_iNV_RECEIPT_ITEM_WH_HDR == null)
                {
                    _iNV_RECEIPT_ITEM_WH_HDR = new TrackableCollection<INV_RECEIPT_ITEM_WH_HDR>();
                    _iNV_RECEIPT_ITEM_WH_HDR.CollectionChanged += FixupINV_RECEIPT_ITEM_WH_HDR;
                }
                return _iNV_RECEIPT_ITEM_WH_HDR;
            }
            set
            {
                if (!ReferenceEquals(_iNV_RECEIPT_ITEM_WH_HDR, value))
                {
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        throw new InvalidOperationException("Cannot set the FixupChangeTrackingCollection when ChangeTracking is enabled");
                    }
                    if (_iNV_RECEIPT_ITEM_WH_HDR != null)
                    {
                        _iNV_RECEIPT_ITEM_WH_HDR.CollectionChanged -= FixupINV_RECEIPT_ITEM_WH_HDR;
                    }
                    _iNV_RECEIPT_ITEM_WH_HDR = value;
                    if (_iNV_RECEIPT_ITEM_WH_HDR != null)
                    {
                        _iNV_RECEIPT_ITEM_WH_HDR.CollectionChanged += FixupINV_RECEIPT_ITEM_WH_HDR;
                    }
                    OnNavigationPropertyChanged("INV_RECEIPT_ITEM_WH_HDR");
                }
            }
        }
        private TrackableCollection<INV_RECEIPT_ITEM_WH_HDR> _iNV_RECEIPT_ITEM_WH_HDR;
    
        [DataMember]
        public TrackableCollection<INV_TRANSACTION_LEDGER> INV_TRANSACTION_LEDGER
        {
            get
            {
                if (_iNV_TRANSACTION_LEDGER == null)
                {
                    _iNV_TRANSACTION_LEDGER = new TrackableCollection<INV_TRANSACTION_LEDGER>();
                    _iNV_TRANSACTION_LEDGER.CollectionChanged += FixupINV_TRANSACTION_LEDGER;
                }
                return _iNV_TRANSACTION_LEDGER;
            }
            set
            {
                if (!ReferenceEquals(_iNV_TRANSACTION_LEDGER, value))
                {
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        throw new InvalidOperationException("Cannot set the FixupChangeTrackingCollection when ChangeTracking is enabled");
                    }
                    if (_iNV_TRANSACTION_LEDGER != null)
                    {
                        _iNV_TRANSACTION_LEDGER.CollectionChanged -= FixupINV_TRANSACTION_LEDGER;
                    }
                    _iNV_TRANSACTION_LEDGER = value;
                    if (_iNV_TRANSACTION_LEDGER != null)
                    {
                        _iNV_TRANSACTION_LEDGER.CollectionChanged += FixupINV_TRANSACTION_LEDGER;
                    }
                    OnNavigationPropertyChanged("INV_TRANSACTION_LEDGER");
                }
            }
        }
        private TrackableCollection<INV_TRANSACTION_LEDGER> _iNV_TRANSACTION_LEDGER;
    
        [DataMember]
        public TrackableCollection<INV_TRANSFER_HDR> INV_TRANSFER_HDR
        {
            get
            {
                if (_iNV_TRANSFER_HDR == null)
                {
                    _iNV_TRANSFER_HDR = new TrackableCollection<INV_TRANSFER_HDR>();
                    _iNV_TRANSFER_HDR.CollectionChanged += FixupINV_TRANSFER_HDR;
                }
                return _iNV_TRANSFER_HDR;
            }
            set
            {
                if (!ReferenceEquals(_iNV_TRANSFER_HDR, value))
                {
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        throw new InvalidOperationException("Cannot set the FixupChangeTrackingCollection when ChangeTracking is enabled");
                    }
                    if (_iNV_TRANSFER_HDR != null)
                    {
                        _iNV_TRANSFER_HDR.CollectionChanged -= FixupINV_TRANSFER_HDR;
                    }
                    _iNV_TRANSFER_HDR = value;
                    if (_iNV_TRANSFER_HDR != null)
                    {
                        _iNV_TRANSFER_HDR.CollectionChanged += FixupINV_TRANSFER_HDR;
                    }
                    OnNavigationPropertyChanged("INV_TRANSFER_HDR");
                }
            }
        }
        private TrackableCollection<INV_TRANSFER_HDR> _iNV_TRANSFER_HDR;
    
        [DataMember]
        public TrackableCollection<INV_TRANSFER_HDR> INV_TRANSFER_HDR1
        {
            get
            {
                if (_iNV_TRANSFER_HDR1 == null)
                {
                    _iNV_TRANSFER_HDR1 = new TrackableCollection<INV_TRANSFER_HDR>();
                    _iNV_TRANSFER_HDR1.CollectionChanged += FixupINV_TRANSFER_HDR1;
                }
                return _iNV_TRANSFER_HDR1;
            }
            set
            {
                if (!ReferenceEquals(_iNV_TRANSFER_HDR1, value))
                {
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        throw new InvalidOperationException("Cannot set the FixupChangeTrackingCollection when ChangeTracking is enabled");
                    }
                    if (_iNV_TRANSFER_HDR1 != null)
                    {
                        _iNV_TRANSFER_HDR1.CollectionChanged -= FixupINV_TRANSFER_HDR1;
                    }
                    _iNV_TRANSFER_HDR1 = value;
                    if (_iNV_TRANSFER_HDR1 != null)
                    {
                        _iNV_TRANSFER_HDR1.CollectionChanged += FixupINV_TRANSFER_HDR1;
                    }
                    OnNavigationPropertyChanged("INV_TRANSFER_HDR1");
                }
            }
        }
        private TrackableCollection<INV_TRANSFER_HDR> _iNV_TRANSFER_HDR1;
    
        [DataMember]
        public TrackableCollection<INV_WAREHOUSE_FACILITIES> INV_WAREHOUSE_FACILITIES
        {
            get
            {
                if (_iNV_WAREHOUSE_FACILITIES == null)
                {
                    _iNV_WAREHOUSE_FACILITIES = new TrackableCollection<INV_WAREHOUSE_FACILITIES>();
                    _iNV_WAREHOUSE_FACILITIES.CollectionChanged += FixupINV_WAREHOUSE_FACILITIES;
                }
                return _iNV_WAREHOUSE_FACILITIES;
            }
            set
            {
                if (!ReferenceEquals(_iNV_WAREHOUSE_FACILITIES, value))
                {
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        throw new InvalidOperationException("Cannot set the FixupChangeTrackingCollection when ChangeTracking is enabled");
                    }
                    if (_iNV_WAREHOUSE_FACILITIES != null)
                    {
                        _iNV_WAREHOUSE_FACILITIES.CollectionChanged -= FixupINV_WAREHOUSE_FACILITIES;
                    }
                    _iNV_WAREHOUSE_FACILITIES = value;
                    if (_iNV_WAREHOUSE_FACILITIES != null)
                    {
                        _iNV_WAREHOUSE_FACILITIES.CollectionChanged += FixupINV_WAREHOUSE_FACILITIES;
                    }
                    OnNavigationPropertyChanged("INV_WAREHOUSE_FACILITIES");
                }
            }
        }
        private TrackableCollection<INV_WAREHOUSE_FACILITIES> _iNV_WAREHOUSE_FACILITIES;
    
        [DataMember]
        public TrackableCollection<INV_WAREHOUSE_LOCATIONS> INV_WAREHOUSE_LOCATIONS
        {
            get
            {
                if (_iNV_WAREHOUSE_LOCATIONS == null)
                {
                    _iNV_WAREHOUSE_LOCATIONS = new TrackableCollection<INV_WAREHOUSE_LOCATIONS>();
                    _iNV_WAREHOUSE_LOCATIONS.CollectionChanged += FixupINV_WAREHOUSE_LOCATIONS;
                }
                return _iNV_WAREHOUSE_LOCATIONS;
            }
            set
            {
                if (!ReferenceEquals(_iNV_WAREHOUSE_LOCATIONS, value))
                {
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        throw new InvalidOperationException("Cannot set the FixupChangeTrackingCollection when ChangeTracking is enabled");
                    }
                    if (_iNV_WAREHOUSE_LOCATIONS != null)
                    {
                        _iNV_WAREHOUSE_LOCATIONS.CollectionChanged -= FixupINV_WAREHOUSE_LOCATIONS;
                    }
                    _iNV_WAREHOUSE_LOCATIONS = value;
                    if (_iNV_WAREHOUSE_LOCATIONS != null)
                    {
                        _iNV_WAREHOUSE_LOCATIONS.CollectionChanged += FixupINV_WAREHOUSE_LOCATIONS;
                    }
                    OnNavigationPropertyChanged("INV_WAREHOUSE_LOCATIONS");
                }
            }
        }
        private TrackableCollection<INV_WAREHOUSE_LOCATIONS> _iNV_WAREHOUSE_LOCATIONS;
    
        [DataMember]
        public TrackableCollection<PO_HEADERS> PO_HEADERS
        {
            get
            {
                if (_pO_HEADERS == null)
                {
                    _pO_HEADERS = new TrackableCollection<PO_HEADERS>();
                    _pO_HEADERS.CollectionChanged += FixupPO_HEADERS;
                }
                return _pO_HEADERS;
            }
            set
            {
                if (!ReferenceEquals(_pO_HEADERS, value))
                {
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        throw new InvalidOperationException("Cannot set the FixupChangeTrackingCollection when ChangeTracking is enabled");
                    }
                    if (_pO_HEADERS != null)
                    {
                        _pO_HEADERS.CollectionChanged -= FixupPO_HEADERS;
                    }
                    _pO_HEADERS = value;
                    if (_pO_HEADERS != null)
                    {
                        _pO_HEADERS.CollectionChanged += FixupPO_HEADERS;
                    }
                    OnNavigationPropertyChanged("PO_HEADERS");
                }
            }
        }
        private TrackableCollection<PO_HEADERS> _pO_HEADERS;
    
        [DataMember]
        public TrackableCollection<PO_LINES> PO_LINES
        {
            get
            {
                if (_pO_LINES == null)
                {
                    _pO_LINES = new TrackableCollection<PO_LINES>();
                    _pO_LINES.CollectionChanged += FixupPO_LINES;
                }
                return _pO_LINES;
            }
            set
            {
                if (!ReferenceEquals(_pO_LINES, value))
                {
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        throw new InvalidOperationException("Cannot set the FixupChangeTrackingCollection when ChangeTracking is enabled");
                    }
                    if (_pO_LINES != null)
                    {
                        _pO_LINES.CollectionChanged -= FixupPO_LINES;
                    }
                    _pO_LINES = value;
                    if (_pO_LINES != null)
                    {
                        _pO_LINES.CollectionChanged += FixupPO_LINES;
                    }
                    OnNavigationPropertyChanged("PO_LINES");
                }
            }
        }
        private TrackableCollection<PO_LINES> _pO_LINES;
    
        [DataMember]
        public TrackableCollection<MATERIAL_RETURN_DTL> MATERIAL_RETURN_DTL
        {
            get
            {
                if (_mATERIAL_RETURN_DTL == null)
                {
                    _mATERIAL_RETURN_DTL = new TrackableCollection<MATERIAL_RETURN_DTL>();
                    _mATERIAL_RETURN_DTL.CollectionChanged += FixupMATERIAL_RETURN_DTL;
                }
                return _mATERIAL_RETURN_DTL;
            }
            set
            {
                if (!ReferenceEquals(_mATERIAL_RETURN_DTL, value))
                {
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        throw new InvalidOperationException("Cannot set the FixupChangeTrackingCollection when ChangeTracking is enabled");
                    }
                    if (_mATERIAL_RETURN_DTL != null)
                    {
                        _mATERIAL_RETURN_DTL.CollectionChanged -= FixupMATERIAL_RETURN_DTL;
                    }
                    _mATERIAL_RETURN_DTL = value;
                    if (_mATERIAL_RETURN_DTL != null)
                    {
                        _mATERIAL_RETURN_DTL.CollectionChanged += FixupMATERIAL_RETURN_DTL;
                    }
                    OnNavigationPropertyChanged("MATERIAL_RETURN_DTL");
                }
            }
        }
        private TrackableCollection<MATERIAL_RETURN_DTL> _mATERIAL_RETURN_DTL;

        #endregion
        #region ChangeTracking
    
        protected virtual void OnPropertyChanged(String propertyName)
        {
            if (ChangeTracker.State != ObjectState.Added && ChangeTracker.State != ObjectState.Deleted)
            {
                ChangeTracker.State = ObjectState.Modified;
            }
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        protected virtual void OnNavigationPropertyChanged(String propertyName)
        {
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        event PropertyChangedEventHandler INotifyPropertyChanged.PropertyChanged{ add { _propertyChanged += value; } remove { _propertyChanged -= value; } }
        private event PropertyChangedEventHandler _propertyChanged;
        private ObjectChangeTracker _changeTracker;
    
        [DataMember]
        public ObjectChangeTracker ChangeTracker
        {
            get
            {
                if (_changeTracker == null)
                {
                    _changeTracker = new ObjectChangeTracker();
                    _changeTracker.ObjectStateChanging += HandleObjectStateChanging;
                }
                return _changeTracker;
            }
            set
            {
                if(_changeTracker != null)
                {
                    _changeTracker.ObjectStateChanging -= HandleObjectStateChanging;
                }
                _changeTracker = value;
                if(_changeTracker != null)
                {
                    _changeTracker.ObjectStateChanging += HandleObjectStateChanging;
                }
            }
        }
    
        private void HandleObjectStateChanging(object sender, ObjectStateChangingEventArgs e)
        {
            if (e.NewState == ObjectState.Deleted)
            {
                ClearNavigationProperties();
            }
        }
    
        protected bool IsDeserializing { get; private set; }
    
        [OnDeserializing]
        public void OnDeserializingMethod(StreamingContext context)
        {
            IsDeserializing = true;
        }
    
        [OnDeserialized]
        public void OnDeserializedMethod(StreamingContext context)
        {
            IsDeserializing = false;
            ChangeTracker.ChangeTrackingEnabled = true;
        }
    
        protected virtual void ClearNavigationProperties()
        {
            INV_RECEIPT_ITEM_WH_HDR.Clear();
            INV_TRANSACTION_LEDGER.Clear();
            INV_TRANSFER_HDR.Clear();
            INV_TRANSFER_HDR1.Clear();
            INV_WAREHOUSE_FACILITIES.Clear();
            INV_WAREHOUSE_LOCATIONS.Clear();
            PO_HEADERS.Clear();
            PO_LINES.Clear();
            MATERIAL_RETURN_DTL.Clear();
        }

        #endregion
        #region Association Fixup
    
        private void FixupINV_RECEIPT_ITEM_WH_HDR(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (e.NewItems != null)
            {
                foreach (INV_RECEIPT_ITEM_WH_HDR item in e.NewItems)
                {
                    item.INV_WAREHOUSES = this;
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        if (!item.ChangeTracker.ChangeTrackingEnabled)
                        {
                            item.StartTracking();
                        }
                        ChangeTracker.RecordAdditionToCollectionProperties("INV_RECEIPT_ITEM_WH_HDR", item);
                    }
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (INV_RECEIPT_ITEM_WH_HDR item in e.OldItems)
                {
                    if (ReferenceEquals(item.INV_WAREHOUSES, this))
                    {
                        item.INV_WAREHOUSES = null;
                    }
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        ChangeTracker.RecordRemovalFromCollectionProperties("INV_RECEIPT_ITEM_WH_HDR", item);
                    }
                }
            }
        }
    
        private void FixupINV_TRANSACTION_LEDGER(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (e.NewItems != null)
            {
                foreach (INV_TRANSACTION_LEDGER item in e.NewItems)
                {
                    item.INV_WAREHOUSES = this;
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        if (!item.ChangeTracker.ChangeTrackingEnabled)
                        {
                            item.StartTracking();
                        }
                        ChangeTracker.RecordAdditionToCollectionProperties("INV_TRANSACTION_LEDGER", item);
                    }
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (INV_TRANSACTION_LEDGER item in e.OldItems)
                {
                    if (ReferenceEquals(item.INV_WAREHOUSES, this))
                    {
                        item.INV_WAREHOUSES = null;
                    }
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        ChangeTracker.RecordRemovalFromCollectionProperties("INV_TRANSACTION_LEDGER", item);
                    }
                }
            }
        }
    
        private void FixupINV_TRANSFER_HDR(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (e.NewItems != null)
            {
                foreach (INV_TRANSFER_HDR item in e.NewItems)
                {
                    item.INV_WAREHOUSES = this;
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        if (!item.ChangeTracker.ChangeTrackingEnabled)
                        {
                            item.StartTracking();
                        }
                        ChangeTracker.RecordAdditionToCollectionProperties("INV_TRANSFER_HDR", item);
                    }
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (INV_TRANSFER_HDR item in e.OldItems)
                {
                    if (ReferenceEquals(item.INV_WAREHOUSES, this))
                    {
                        item.INV_WAREHOUSES = null;
                    }
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        ChangeTracker.RecordRemovalFromCollectionProperties("INV_TRANSFER_HDR", item);
                    }
                }
            }
        }
    
        private void FixupINV_TRANSFER_HDR1(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (e.NewItems != null)
            {
                foreach (INV_TRANSFER_HDR item in e.NewItems)
                {
                    item.INV_WAREHOUSES1 = this;
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        if (!item.ChangeTracker.ChangeTrackingEnabled)
                        {
                            item.StartTracking();
                        }
                        ChangeTracker.RecordAdditionToCollectionProperties("INV_TRANSFER_HDR1", item);
                    }
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (INV_TRANSFER_HDR item in e.OldItems)
                {
                    if (ReferenceEquals(item.INV_WAREHOUSES1, this))
                    {
                        item.INV_WAREHOUSES1 = null;
                    }
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        ChangeTracker.RecordRemovalFromCollectionProperties("INV_TRANSFER_HDR1", item);
                    }
                }
            }
        }
    
        private void FixupINV_WAREHOUSE_FACILITIES(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (e.NewItems != null)
            {
                foreach (INV_WAREHOUSE_FACILITIES item in e.NewItems)
                {
                    item.INV_WAREHOUSES = this;
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        if (!item.ChangeTracker.ChangeTrackingEnabled)
                        {
                            item.StartTracking();
                        }
                        ChangeTracker.RecordAdditionToCollectionProperties("INV_WAREHOUSE_FACILITIES", item);
                    }
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (INV_WAREHOUSE_FACILITIES item in e.OldItems)
                {
                    if (ReferenceEquals(item.INV_WAREHOUSES, this))
                    {
                        item.INV_WAREHOUSES = null;
                    }
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        ChangeTracker.RecordRemovalFromCollectionProperties("INV_WAREHOUSE_FACILITIES", item);
                    }
                }
            }
        }
    
        private void FixupINV_WAREHOUSE_LOCATIONS(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (e.NewItems != null)
            {
                foreach (INV_WAREHOUSE_LOCATIONS item in e.NewItems)
                {
                    item.INV_WAREHOUSES = this;
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        if (!item.ChangeTracker.ChangeTrackingEnabled)
                        {
                            item.StartTracking();
                        }
                        ChangeTracker.RecordAdditionToCollectionProperties("INV_WAREHOUSE_LOCATIONS", item);
                    }
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (INV_WAREHOUSE_LOCATIONS item in e.OldItems)
                {
                    if (ReferenceEquals(item.INV_WAREHOUSES, this))
                    {
                        item.INV_WAREHOUSES = null;
                    }
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        ChangeTracker.RecordRemovalFromCollectionProperties("INV_WAREHOUSE_LOCATIONS", item);
                    }
                }
            }
        }
    
        private void FixupPO_HEADERS(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (e.NewItems != null)
            {
                foreach (PO_HEADERS item in e.NewItems)
                {
                    item.INV_WAREHOUSES = this;
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        if (!item.ChangeTracker.ChangeTrackingEnabled)
                        {
                            item.StartTracking();
                        }
                        ChangeTracker.RecordAdditionToCollectionProperties("PO_HEADERS", item);
                    }
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (PO_HEADERS item in e.OldItems)
                {
                    if (ReferenceEquals(item.INV_WAREHOUSES, this))
                    {
                        item.INV_WAREHOUSES = null;
                    }
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        ChangeTracker.RecordRemovalFromCollectionProperties("PO_HEADERS", item);
                    }
                }
            }
        }
    
        private void FixupPO_LINES(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (e.NewItems != null)
            {
                foreach (PO_LINES item in e.NewItems)
                {
                    item.INV_WAREHOUSES = this;
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        if (!item.ChangeTracker.ChangeTrackingEnabled)
                        {
                            item.StartTracking();
                        }
                        ChangeTracker.RecordAdditionToCollectionProperties("PO_LINES", item);
                    }
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (PO_LINES item in e.OldItems)
                {
                    if (ReferenceEquals(item.INV_WAREHOUSES, this))
                    {
                        item.INV_WAREHOUSES = null;
                    }
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        ChangeTracker.RecordRemovalFromCollectionProperties("PO_LINES", item);
                    }
                }
            }
        }
    
        private void FixupMATERIAL_RETURN_DTL(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (e.NewItems != null)
            {
                foreach (MATERIAL_RETURN_DTL item in e.NewItems)
                {
                    item.INV_WAREHOUSES = this;
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        if (!item.ChangeTracker.ChangeTrackingEnabled)
                        {
                            item.StartTracking();
                        }
                        ChangeTracker.RecordAdditionToCollectionProperties("MATERIAL_RETURN_DTL", item);
                    }
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (MATERIAL_RETURN_DTL item in e.OldItems)
                {
                    if (ReferenceEquals(item.INV_WAREHOUSES, this))
                    {
                        item.INV_WAREHOUSES = null;
                    }
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        ChangeTracker.RecordRemovalFromCollectionProperties("MATERIAL_RETURN_DTL", item);
                    }
                }
            }
        }

        #endregion
    }
}
