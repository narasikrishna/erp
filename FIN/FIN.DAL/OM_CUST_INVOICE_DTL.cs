//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.Serialization;

namespace FIN.DAL
{
    [DataContract(IsReference = true)]
    [KnownType(typeof(INV_ITEM_MASTER))]
    [KnownType(typeof(OM_CUST_INVOICE_HDR))]
    [KnownType(typeof(OM_DC_HDR))]
    public partial class OM_CUST_INVOICE_DTL: IObjectWithChangeTracker, INotifyPropertyChanged
    {
        #region Primitive Properties
    
        [DataMember]
        public Nullable<int> PK_ID
        {
            get { return _pK_ID; }
            set
            {
                if (_pK_ID != value)
                {
                    _pK_ID = value;
                    OnPropertyChanged("PK_ID");
                }
            }
        }
        private Nullable<int> _pK_ID;
    
        [DataMember]
        public Nullable<int> CHILD_ID
        {
            get { return _cHILD_ID; }
            set
            {
                if (_cHILD_ID != value)
                {
                    _cHILD_ID = value;
                    OnPropertyChanged("CHILD_ID");
                }
            }
        }
        private Nullable<int> _cHILD_ID;
    
        [DataMember]
        public string OM_CUST_INV_DTL_ID
        {
            get { return _oM_CUST_INV_DTL_ID; }
            set
            {
                if (_oM_CUST_INV_DTL_ID != value)
                {
                    if (ChangeTracker.ChangeTrackingEnabled && ChangeTracker.State != ObjectState.Added)
                    {
                        throw new InvalidOperationException("The property 'OM_CUST_INV_DTL_ID' is part of the object's key and cannot be changed. Changes to key properties can only be made when the object is not being tracked or is in the Added state.");
                    }
                    _oM_CUST_INV_DTL_ID = value;
                    OnPropertyChanged("OM_CUST_INV_DTL_ID");
                }
            }
        }
        private string _oM_CUST_INV_DTL_ID;
    
        [DataMember]
        public string OM_INV_ID
        {
            get { return _oM_INV_ID; }
            set
            {
                if (_oM_INV_ID != value)
                {
                    ChangeTracker.RecordOriginalValue("OM_INV_ID", _oM_INV_ID);
                    if (!IsDeserializing)
                    {
                        if (OM_CUST_INVOICE_HDR != null && OM_CUST_INVOICE_HDR.OM_INV_ID != value)
                        {
                            OM_CUST_INVOICE_HDR = null;
                        }
                    }
                    _oM_INV_ID = value;
                    OnPropertyChanged("OM_INV_ID");
                }
            }
        }
        private string _oM_INV_ID;
    
        [DataMember]
        public string OM_DC_ID
        {
            get { return _oM_DC_ID; }
            set
            {
                if (_oM_DC_ID != value)
                {
                    ChangeTracker.RecordOriginalValue("OM_DC_ID", _oM_DC_ID);
                    if (!IsDeserializing)
                    {
                        if (OM_DC_HDR != null && OM_DC_HDR.OM_DC_ID != value)
                        {
                            OM_DC_HDR = null;
                        }
                    }
                    _oM_DC_ID = value;
                    OnPropertyChanged("OM_DC_ID");
                }
            }
        }
        private string _oM_DC_ID;
    
        [DataMember]
        public Nullable<decimal> OM_INV_LINE_NUM
        {
            get { return _oM_INV_LINE_NUM; }
            set
            {
                if (_oM_INV_LINE_NUM != value)
                {
                    _oM_INV_LINE_NUM = value;
                    OnPropertyChanged("OM_INV_LINE_NUM");
                }
            }
        }
        private Nullable<decimal> _oM_INV_LINE_NUM;
    
        [DataMember]
        public string OM_ITEM_ID
        {
            get { return _oM_ITEM_ID; }
            set
            {
                if (_oM_ITEM_ID != value)
                {
                    ChangeTracker.RecordOriginalValue("OM_ITEM_ID", _oM_ITEM_ID);
                    if (!IsDeserializing)
                    {
                        if (INV_ITEM_MASTER != null && INV_ITEM_MASTER.ITEM_ID != value)
                        {
                            INV_ITEM_MASTER = null;
                        }
                    }
                    _oM_ITEM_ID = value;
                    OnPropertyChanged("OM_ITEM_ID");
                }
            }
        }
        private string _oM_ITEM_ID;
    
        [DataMember]
        public Nullable<decimal> OM_ITEM_QTY
        {
            get { return _oM_ITEM_QTY; }
            set
            {
                if (_oM_ITEM_QTY != value)
                {
                    _oM_ITEM_QTY = value;
                    OnPropertyChanged("OM_ITEM_QTY");
                }
            }
        }
        private Nullable<decimal> _oM_ITEM_QTY;
    
        [DataMember]
        public Nullable<decimal> OM_ITEM_PRICE
        {
            get { return _oM_ITEM_PRICE; }
            set
            {
                if (_oM_ITEM_PRICE != value)
                {
                    _oM_ITEM_PRICE = value;
                    OnPropertyChanged("OM_ITEM_PRICE");
                }
            }
        }
        private Nullable<decimal> _oM_ITEM_PRICE;
    
        [DataMember]
        public Nullable<decimal> OM_ITEM_COST
        {
            get { return _oM_ITEM_COST; }
            set
            {
                if (_oM_ITEM_COST != value)
                {
                    _oM_ITEM_COST = value;
                    OnPropertyChanged("OM_ITEM_COST");
                }
            }
        }
        private Nullable<decimal> _oM_ITEM_COST;
    
        [DataMember]
        public string OM_ITEM_ACCT_CODE_ID
        {
            get { return _oM_ITEM_ACCT_CODE_ID; }
            set
            {
                if (_oM_ITEM_ACCT_CODE_ID != value)
                {
                    _oM_ITEM_ACCT_CODE_ID = value;
                    OnPropertyChanged("OM_ITEM_ACCT_CODE_ID");
                }
            }
        }
        private string _oM_ITEM_ACCT_CODE_ID;
    
        [DataMember]
        public string ENABLED_FLAG
        {
            get { return _eNABLED_FLAG; }
            set
            {
                if (_eNABLED_FLAG != value)
                {
                    _eNABLED_FLAG = value;
                    OnPropertyChanged("ENABLED_FLAG");
                }
            }
        }
        private string _eNABLED_FLAG;
    
        [DataMember]
        public string WORKFLOW_COMPLETION_STATUS
        {
            get { return _wORKFLOW_COMPLETION_STATUS; }
            set
            {
                if (_wORKFLOW_COMPLETION_STATUS != value)
                {
                    _wORKFLOW_COMPLETION_STATUS = value;
                    OnPropertyChanged("WORKFLOW_COMPLETION_STATUS");
                }
            }
        }
        private string _wORKFLOW_COMPLETION_STATUS;
    
        [DataMember]
        public string CREATED_BY
        {
            get { return _cREATED_BY; }
            set
            {
                if (_cREATED_BY != value)
                {
                    _cREATED_BY = value;
                    OnPropertyChanged("CREATED_BY");
                }
            }
        }
        private string _cREATED_BY;
    
        [DataMember]
        public System.DateTime CREATED_DATE
        {
            get { return _cREATED_DATE; }
            set
            {
                if (_cREATED_DATE != value)
                {
                    _cREATED_DATE = value;
                    OnPropertyChanged("CREATED_DATE");
                }
            }
        }
        private System.DateTime _cREATED_DATE;
    
        [DataMember]
        public string MODIFIED_BY
        {
            get { return _mODIFIED_BY; }
            set
            {
                if (_mODIFIED_BY != value)
                {
                    _mODIFIED_BY = value;
                    OnPropertyChanged("MODIFIED_BY");
                }
            }
        }
        private string _mODIFIED_BY;
    
        [DataMember]
        public Nullable<System.DateTime> MODIFIED_DATE
        {
            get { return _mODIFIED_DATE; }
            set
            {
                if (_mODIFIED_DATE != value)
                {
                    _mODIFIED_DATE = value;
                    OnPropertyChanged("MODIFIED_DATE");
                }
            }
        }
        private Nullable<System.DateTime> _mODIFIED_DATE;
    
        [DataMember]
        public string ATTRIBUTE1
        {
            get { return _aTTRIBUTE1; }
            set
            {
                if (_aTTRIBUTE1 != value)
                {
                    _aTTRIBUTE1 = value;
                    OnPropertyChanged("ATTRIBUTE1");
                }
            }
        }
        private string _aTTRIBUTE1;
    
        [DataMember]
        public string ATTRIBUTE2
        {
            get { return _aTTRIBUTE2; }
            set
            {
                if (_aTTRIBUTE2 != value)
                {
                    _aTTRIBUTE2 = value;
                    OnPropertyChanged("ATTRIBUTE2");
                }
            }
        }
        private string _aTTRIBUTE2;
    
        [DataMember]
        public string ATTRIBUTE3
        {
            get { return _aTTRIBUTE3; }
            set
            {
                if (_aTTRIBUTE3 != value)
                {
                    _aTTRIBUTE3 = value;
                    OnPropertyChanged("ATTRIBUTE3");
                }
            }
        }
        private string _aTTRIBUTE3;
    
        [DataMember]
        public string ATTRIBUTE4
        {
            get { return _aTTRIBUTE4; }
            set
            {
                if (_aTTRIBUTE4 != value)
                {
                    _aTTRIBUTE4 = value;
                    OnPropertyChanged("ATTRIBUTE4");
                }
            }
        }
        private string _aTTRIBUTE4;
    
        [DataMember]
        public string ATTRIBUTE5
        {
            get { return _aTTRIBUTE5; }
            set
            {
                if (_aTTRIBUTE5 != value)
                {
                    _aTTRIBUTE5 = value;
                    OnPropertyChanged("ATTRIBUTE5");
                }
            }
        }
        private string _aTTRIBUTE5;
    
        [DataMember]
        public string ATTRIBUTE6
        {
            get { return _aTTRIBUTE6; }
            set
            {
                if (_aTTRIBUTE6 != value)
                {
                    _aTTRIBUTE6 = value;
                    OnPropertyChanged("ATTRIBUTE6");
                }
            }
        }
        private string _aTTRIBUTE6;
    
        [DataMember]
        public string ATTRIBUTE7
        {
            get { return _aTTRIBUTE7; }
            set
            {
                if (_aTTRIBUTE7 != value)
                {
                    _aTTRIBUTE7 = value;
                    OnPropertyChanged("ATTRIBUTE7");
                }
            }
        }
        private string _aTTRIBUTE7;
    
        [DataMember]
        public string ATTRIBUTE8
        {
            get { return _aTTRIBUTE8; }
            set
            {
                if (_aTTRIBUTE8 != value)
                {
                    _aTTRIBUTE8 = value;
                    OnPropertyChanged("ATTRIBUTE8");
                }
            }
        }
        private string _aTTRIBUTE8;
    
        [DataMember]
        public string ATTRIBUTE9
        {
            get { return _aTTRIBUTE9; }
            set
            {
                if (_aTTRIBUTE9 != value)
                {
                    _aTTRIBUTE9 = value;
                    OnPropertyChanged("ATTRIBUTE9");
                }
            }
        }
        private string _aTTRIBUTE9;
    
        [DataMember]
        public string ATTRIBUTE10
        {
            get { return _aTTRIBUTE10; }
            set
            {
                if (_aTTRIBUTE10 != value)
                {
                    _aTTRIBUTE10 = value;
                    OnPropertyChanged("ATTRIBUTE10");
                }
            }
        }
        private string _aTTRIBUTE10;
    
        [DataMember]
        public string OM_INV_LINE_TYPE
        {
            get { return _oM_INV_LINE_TYPE; }
            set
            {
                if (_oM_INV_LINE_TYPE != value)
                {
                    _oM_INV_LINE_TYPE = value;
                    OnPropertyChanged("OM_INV_LINE_TYPE");
                }
            }
        }
        private string _oM_INV_LINE_TYPE;
    
        [DataMember]
        public Nullable<decimal> OM_INV_LINE_TAX_AMT
        {
            get { return _oM_INV_LINE_TAX_AMT; }
            set
            {
                if (_oM_INV_LINE_TAX_AMT != value)
                {
                    _oM_INV_LINE_TAX_AMT = value;
                    OnPropertyChanged("OM_INV_LINE_TAX_AMT");
                }
            }
        }
        private Nullable<decimal> _oM_INV_LINE_TAX_AMT;
    
        [DataMember]
        public string OM_INV_SEGMENT_ID1
        {
            get { return _oM_INV_SEGMENT_ID1; }
            set
            {
                if (_oM_INV_SEGMENT_ID1 != value)
                {
                    _oM_INV_SEGMENT_ID1 = value;
                    OnPropertyChanged("OM_INV_SEGMENT_ID1");
                }
            }
        }
        private string _oM_INV_SEGMENT_ID1;
    
        [DataMember]
        public string OM_INV_SEGMENT_ID2
        {
            get { return _oM_INV_SEGMENT_ID2; }
            set
            {
                if (_oM_INV_SEGMENT_ID2 != value)
                {
                    _oM_INV_SEGMENT_ID2 = value;
                    OnPropertyChanged("OM_INV_SEGMENT_ID2");
                }
            }
        }
        private string _oM_INV_SEGMENT_ID2;
    
        [DataMember]
        public string OM_INV_SEGMENT_ID3
        {
            get { return _oM_INV_SEGMENT_ID3; }
            set
            {
                if (_oM_INV_SEGMENT_ID3 != value)
                {
                    _oM_INV_SEGMENT_ID3 = value;
                    OnPropertyChanged("OM_INV_SEGMENT_ID3");
                }
            }
        }
        private string _oM_INV_SEGMENT_ID3;
    
        [DataMember]
        public string OM_INV_SEGMENT_ID4
        {
            get { return _oM_INV_SEGMENT_ID4; }
            set
            {
                if (_oM_INV_SEGMENT_ID4 != value)
                {
                    _oM_INV_SEGMENT_ID4 = value;
                    OnPropertyChanged("OM_INV_SEGMENT_ID4");
                }
            }
        }
        private string _oM_INV_SEGMENT_ID4;
    
        [DataMember]
        public string OM_INV_SEGMENT_ID5
        {
            get { return _oM_INV_SEGMENT_ID5; }
            set
            {
                if (_oM_INV_SEGMENT_ID5 != value)
                {
                    _oM_INV_SEGMENT_ID5 = value;
                    OnPropertyChanged("OM_INV_SEGMENT_ID5");
                }
            }
        }
        private string _oM_INV_SEGMENT_ID5;
    
        [DataMember]
        public string OM_INV_SEGMENT_ID6
        {
            get { return _oM_INV_SEGMENT_ID6; }
            set
            {
                if (_oM_INV_SEGMENT_ID6 != value)
                {
                    _oM_INV_SEGMENT_ID6 = value;
                    OnPropertyChanged("OM_INV_SEGMENT_ID6");
                }
            }
        }
        private string _oM_INV_SEGMENT_ID6;
    
        [DataMember]
        public string INV_ENTITY_ID
        {
            get { return _iNV_ENTITY_ID; }
            set
            {
                if (_iNV_ENTITY_ID != value)
                {
                    _iNV_ENTITY_ID = value;
                    OnPropertyChanged("INV_ENTITY_ID");
                }
            }
        }
        private string _iNV_ENTITY_ID;
    
        [DataMember]
        public string RECEIPT_ID
        {
            get { return _rECEIPT_ID; }
            set
            {
                if (_rECEIPT_ID != value)
                {
                    _rECEIPT_ID = value;
                    OnPropertyChanged("RECEIPT_ID");
                }
            }
        }
        private string _rECEIPT_ID;

        #endregion
        #region Navigation Properties
    
        [DataMember]
        public INV_ITEM_MASTER INV_ITEM_MASTER
        {
            get { return _iNV_ITEM_MASTER; }
            set
            {
                if (!ReferenceEquals(_iNV_ITEM_MASTER, value))
                {
                    var previousValue = _iNV_ITEM_MASTER;
                    _iNV_ITEM_MASTER = value;
                    FixupINV_ITEM_MASTER(previousValue);
                    OnNavigationPropertyChanged("INV_ITEM_MASTER");
                }
            }
        }
        private INV_ITEM_MASTER _iNV_ITEM_MASTER;
    
        [DataMember]
        public OM_CUST_INVOICE_HDR OM_CUST_INVOICE_HDR
        {
            get { return _oM_CUST_INVOICE_HDR; }
            set
            {
                if (!ReferenceEquals(_oM_CUST_INVOICE_HDR, value))
                {
                    var previousValue = _oM_CUST_INVOICE_HDR;
                    _oM_CUST_INVOICE_HDR = value;
                    FixupOM_CUST_INVOICE_HDR(previousValue);
                    OnNavigationPropertyChanged("OM_CUST_INVOICE_HDR");
                }
            }
        }
        private OM_CUST_INVOICE_HDR _oM_CUST_INVOICE_HDR;
    
        [DataMember]
        public OM_DC_HDR OM_DC_HDR
        {
            get { return _oM_DC_HDR; }
            set
            {
                if (!ReferenceEquals(_oM_DC_HDR, value))
                {
                    var previousValue = _oM_DC_HDR;
                    _oM_DC_HDR = value;
                    FixupOM_DC_HDR(previousValue);
                    OnNavigationPropertyChanged("OM_DC_HDR");
                }
            }
        }
        private OM_DC_HDR _oM_DC_HDR;

        #endregion
        #region ChangeTracking
    
        protected virtual void OnPropertyChanged(String propertyName)
        {
            if (ChangeTracker.State != ObjectState.Added && ChangeTracker.State != ObjectState.Deleted)
            {
                ChangeTracker.State = ObjectState.Modified;
            }
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        protected virtual void OnNavigationPropertyChanged(String propertyName)
        {
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        event PropertyChangedEventHandler INotifyPropertyChanged.PropertyChanged{ add { _propertyChanged += value; } remove { _propertyChanged -= value; } }
        private event PropertyChangedEventHandler _propertyChanged;
        private ObjectChangeTracker _changeTracker;
    
        [DataMember]
        public ObjectChangeTracker ChangeTracker
        {
            get
            {
                if (_changeTracker == null)
                {
                    _changeTracker = new ObjectChangeTracker();
                    _changeTracker.ObjectStateChanging += HandleObjectStateChanging;
                }
                return _changeTracker;
            }
            set
            {
                if(_changeTracker != null)
                {
                    _changeTracker.ObjectStateChanging -= HandleObjectStateChanging;
                }
                _changeTracker = value;
                if(_changeTracker != null)
                {
                    _changeTracker.ObjectStateChanging += HandleObjectStateChanging;
                }
            }
        }
    
        private void HandleObjectStateChanging(object sender, ObjectStateChangingEventArgs e)
        {
            if (e.NewState == ObjectState.Deleted)
            {
                ClearNavigationProperties();
            }
        }
    
        protected bool IsDeserializing { get; private set; }
    
        [OnDeserializing]
        public void OnDeserializingMethod(StreamingContext context)
        {
            IsDeserializing = true;
        }
    
        [OnDeserialized]
        public void OnDeserializedMethod(StreamingContext context)
        {
            IsDeserializing = false;
            ChangeTracker.ChangeTrackingEnabled = true;
        }
    
        protected virtual void ClearNavigationProperties()
        {
            INV_ITEM_MASTER = null;
            OM_CUST_INVOICE_HDR = null;
            OM_DC_HDR = null;
        }

        #endregion
        #region Association Fixup
    
        private void FixupINV_ITEM_MASTER(INV_ITEM_MASTER previousValue, bool skipKeys = false)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (previousValue != null && previousValue.OM_CUST_INVOICE_DTL.Contains(this))
            {
                previousValue.OM_CUST_INVOICE_DTL.Remove(this);
            }
    
            if (INV_ITEM_MASTER != null)
            {
                if (!INV_ITEM_MASTER.OM_CUST_INVOICE_DTL.Contains(this))
                {
                    INV_ITEM_MASTER.OM_CUST_INVOICE_DTL.Add(this);
                }
    
                OM_ITEM_ID = INV_ITEM_MASTER.ITEM_ID;
            }
            else if (!skipKeys)
            {
                OM_ITEM_ID = null;
            }
    
            if (ChangeTracker.ChangeTrackingEnabled)
            {
                if (ChangeTracker.OriginalValues.ContainsKey("INV_ITEM_MASTER")
                    && (ChangeTracker.OriginalValues["INV_ITEM_MASTER"] == INV_ITEM_MASTER))
                {
                    ChangeTracker.OriginalValues.Remove("INV_ITEM_MASTER");
                }
                else
                {
                    ChangeTracker.RecordOriginalValue("INV_ITEM_MASTER", previousValue);
                }
                if (INV_ITEM_MASTER != null && !INV_ITEM_MASTER.ChangeTracker.ChangeTrackingEnabled)
                {
                    INV_ITEM_MASTER.StartTracking();
                }
            }
        }
    
        private void FixupOM_CUST_INVOICE_HDR(OM_CUST_INVOICE_HDR previousValue, bool skipKeys = false)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (previousValue != null && previousValue.OM_CUST_INVOICE_DTL.Contains(this))
            {
                previousValue.OM_CUST_INVOICE_DTL.Remove(this);
            }
    
            if (OM_CUST_INVOICE_HDR != null)
            {
                if (!OM_CUST_INVOICE_HDR.OM_CUST_INVOICE_DTL.Contains(this))
                {
                    OM_CUST_INVOICE_HDR.OM_CUST_INVOICE_DTL.Add(this);
                }
    
                OM_INV_ID = OM_CUST_INVOICE_HDR.OM_INV_ID;
            }
            else if (!skipKeys)
            {
                OM_INV_ID = null;
            }
    
            if (ChangeTracker.ChangeTrackingEnabled)
            {
                if (ChangeTracker.OriginalValues.ContainsKey("OM_CUST_INVOICE_HDR")
                    && (ChangeTracker.OriginalValues["OM_CUST_INVOICE_HDR"] == OM_CUST_INVOICE_HDR))
                {
                    ChangeTracker.OriginalValues.Remove("OM_CUST_INVOICE_HDR");
                }
                else
                {
                    ChangeTracker.RecordOriginalValue("OM_CUST_INVOICE_HDR", previousValue);
                }
                if (OM_CUST_INVOICE_HDR != null && !OM_CUST_INVOICE_HDR.ChangeTracker.ChangeTrackingEnabled)
                {
                    OM_CUST_INVOICE_HDR.StartTracking();
                }
            }
        }
    
        private void FixupOM_DC_HDR(OM_DC_HDR previousValue, bool skipKeys = false)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (previousValue != null && previousValue.OM_CUST_INVOICE_DTL.Contains(this))
            {
                previousValue.OM_CUST_INVOICE_DTL.Remove(this);
            }
    
            if (OM_DC_HDR != null)
            {
                if (!OM_DC_HDR.OM_CUST_INVOICE_DTL.Contains(this))
                {
                    OM_DC_HDR.OM_CUST_INVOICE_DTL.Add(this);
                }
    
                OM_DC_ID = OM_DC_HDR.OM_DC_ID;
            }
            else if (!skipKeys)
            {
                OM_DC_ID = null;
            }
    
            if (ChangeTracker.ChangeTrackingEnabled)
            {
                if (ChangeTracker.OriginalValues.ContainsKey("OM_DC_HDR")
                    && (ChangeTracker.OriginalValues["OM_DC_HDR"] == OM_DC_HDR))
                {
                    ChangeTracker.OriginalValues.Remove("OM_DC_HDR");
                }
                else
                {
                    ChangeTracker.RecordOriginalValue("OM_DC_HDR", previousValue);
                }
                if (OM_DC_HDR != null && !OM_DC_HDR.ChangeTracker.ChangeTrackingEnabled)
                {
                    OM_DC_HDR.StartTracking();
                }
            }
        }

        #endregion
    }
}
