﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL
{
    public class FINColumnConstants
    {
        public const string VALUE_KEY_ID = "VALUE_KEY_ID";
        public const string VALUE_NAME = "VALUE_NAME";

        public const string LOOKUP_ID = "LOOKUP_ID";
        public const string LOOKUP_NAME = "LOOKUP_NAME";
        public const string DESCRIPTION_OL = "DESCRIPTION_OL";
        public const string SEC_ID = "SEC_ID";

        public const string EMAIL = "EMAIL";
        public const string CATEGORY_CODE = "CATEGORY_CODE";
        public const string CATEGORY_DESC = "CATEGORY_DESC";
        public const string CATEGORY_ID = "CATEGORY_ID";
        public const string REIMB_TYP_ID = "REIMB_TYP_ID";
        public const string REIMB_TYP_DESC = "REIMB_TYP_DESC";
        public const string GRADE_DESC = "GRADE_DESC";
        public const string GRADE_NAME = "GRADE_NAME";
        public const string GRADE_ID = "GRADE_ID";
        public const string USER_CODE = "USER_CODE";
        public const string ACCT_CODE_SEG_ID = "ACCT_CODE_SEG_ID";
        public const string DELETED = "DELETED";
        public const string EFFECTIVE_START_DT = "EFFECTIVE_START_DT";
        public const string EFFECTIVE_END_DT = "EFFECTIVE_END_DT";

        public const string CAL_EFF_START_DT = "CAL_EFF_START_DT";
        public const string CAL_EFF_END_DT = "CALL_EFF_END_DT";
        public const string ITEM_CODE = "ITEM_CODE";

        public const string PK_ID = "PK_ID";
        public const string ACCT_STRUCT_ID = "ACCT_STRUCT_ID";
        public const string ACCT_STRUCT_NAME = "ACCT_STRUCT_NAME";
        public const string CAL_ID = "CAL_ID";
        public const string CAL_DESC = "CAL_DESC";
        public const string COMP_BASE_CURRENCY = "COMP_BASE_CURRENCY";
        public const string ACCT_CODE_SEGMENT_ID = "ACCT_CODE_SEGMENT_ID";
        public const string ENABLED_FLAG = "ENABLED_FLAG";
        public const string AUDIT_TRAIL = "AUDIT_TRAIL";
        
        public const string COMP_ACCT_STRUCT_ID = "COMP_ACCT_STRUCT_ID";
        public const string COMP_CAL_ID = "COMP_CAL_ID";
        public const string SEGMENT_ID = "SEGMENT_ID";
        public const string CURRENCY_ID = "CURRENCY_ID";
        public const string CURRENCY_DESC = "CURRENCY_DESC";
        public const string CURRENCY_NAME = "CURRENCY_NAME";
        public const string CURRENCY_SYMBOL = "CURRENCY_SYMBOL";
        public const string CREDIT = "CREDIT";
        public const string DEBIT = "DEBIT";
        public const string LEAVE_SAL_ELEMENT_ID = "LEAVE_SAL_ELEMENT_ID";
        
        public const string SEGMENT_VALUE = "SEGMENT_VALUE";
        public const string SEGMENT_VALUE_OL = "SEGMENT_VALUE_OL";
        public const string PARENT_SEGMENT_VALUE_ID = "PARENT_SEGMENT_VALUE_ID";
        public const string SEGMENT_NAME = "SEGMENT_NAME";
        public const string INV_WH_ID = "INV_WH_ID";
        public const string INV_WH_NAME = "INV_WH_NAME";

        public const string COUNTRY_NAME = "COUNTRY_NAME";
        public const string COUNTRY_CODE = "COUNTRY_CODE";
        public const string STATE_NAME = "STATE_NAME";
        public const string STATE_CODE = "STATE_CODE";
        public const string CITY_NAME = "CITY_NAME";
        public const string CITY_CODE = "CITY_CODE";
        public const string SHORT_NAME = "SHORT_NAME";
        public const string FULL_NAME = "FULL_NAME";

        public const string ACCT_CODE_ID = "ACCT_CODE_ID";

        public const string PO_REQ_LINE_ID = "PO_REQ_LINE_ID";
        public const string PO_REQ_ID = "PO_REQ_ID";
        public const string PO_REQ_LINE_NUM = "PO_REQ_LINE_NUM";
        public const string PO_LINE_NUM = "PO_LINE_NUM";


        public const string ITEM_ID = "ITEM_ID";
        public const string PO_REQ_UOM = "PO_REQ_UOM";
        public const string PO_DESCRIPTION = "PO_DESCRIPTION";

        public const string PO_QUANTITY = "PO_QUANTITY";
        public const string PO_ITEM_UNIT_PRICE = "PO_ITEM_UNIT_PRICE";
        public const string PO_UNIT_PRICE = "PO_UNIT_PRICE";
        public const string ITEM_NAME = "ITEM_NAME";
        public const string UOM_ID = "UOM_ID";
        public const string UOM_NAME = "UOM_NAME";
        public const string UOM_CODE = "UOM_CODE";
        public const string ITEM_TYPE = "ITEM_TYPE";

        public const string DEPT_ID = "DEPT_ID";
        public const string DEPT_NAME = "DEPT_NAME";
        public const string DEPT_TYPE = "DEPT_TYPE";

        public const string TERM_ID = "TERM_ID";
        public const string TERM_NAME = "TERM_NAME";
        public const string TERM_DTL_ID = "TERM_DTL_ID";
        public const string TERM_TYPE = "TERM_TYPE";
        public const string TERM_CR_DAYS = "TERM_CR_DAYS";
        public const string TERM_AMT = "TERM_AMT";
        public const string TERM_DISCOUNT_TYPE = "TERM_DISCOUNT_TYPE";
        public const string TERM_DISCOUNT_DAYS = "TERM_DISCOUNT_DAYS";
        public const string TERM_DISCOUNT_AMT = "TERM_DISCOUNT_AMT";

        public const string ACCT_GRP_LNK_DTL_ID = "ACCT_GRP_LNK_DTL_ID";
        public const string ACCT_GRP_ID = "ACCT_GRP_ID";
        public const string ACCT_CODE = "ACCT_CODE";


        public const string VENDOR_ID = "VENDOR_ID";
        public const string VENDOR_NAME = "VENDOR_NAME";
        public const string GROUP_ID = "GROUP_ID";
        public const string GROUP_NAME = "GROUP_NAME";
        public const string JOB_DESC = "JOB_DESC";
        public const string JOB_ID = "JOB_ID";
        public const string PO_LINE_ID = "PO_LINE_ID";
        public const string PO_HEADER_ID = "PO_HEADER_ID";
        public const string PO_Amount = "PO_Amount";

        public const string EMP_ID = "EMP_ID";

        public const string EMP_NO = "EMP_NO";
        public const string EMP_FIRST_NAME = "EMP_FIRST_NAME";
        public const string EMP_MIDDLE_NAME = "EMP_MIDDLE_NAME";
        public const string EMP_LAST_NAME = "EMP_LAST_NAME";
        public const string QUALI_TYPE = "QUALI_TYPE";
        public const string EMP_SKILL_TYPE = "EMP_SKILL_TYPE";
        public const string SKILL_CATEGORY = "SKILL_CATEGORY";
        public const string EMP_BANK_CODE = "EMP_BANK_CODE";
        public const string EMP_BANK_BRANCH_CODE = "EMP_BANK_BRANCH_CODE";

        public const string EMP_NAME = "EMP_NAME";
        public const string RECEIPT_DTL_ID = "RECEIPT_DTL_ID";

        public const string RECEIPT_ID = "RECEIPT_ID";

        public const string LINE_NUM = "LINE_NUM";
        public const string PO_NUM = "PO_NUM";
        public const string QTY_RECEIVED = "QTY_RECEIVED";
        public const string inv_description = "inv_description";

        public const string QTY_REJECTED = "QTY_REJECTED";
        public const string REMARKS = "REMARKS";
        public const string INV_DESCRIPTION = "INV_DESCRIPTION";

        public const string CURRENCY_RATE_ID = "CURRENCY_RATE_ID";
        public const string CURRENCY_RATE_DT = "CURRENCY_RATE_DT";

        public const string CURRENCY_STD_RATE = "CURRENCY_STD_RATE";
        public const string CURRENCY_SEL_RATE = "CURRENCY_SEL_RATE";
        public const string CURRENCY_BUY_RATE = "CURRENCY_BUY_RATE";
        public const string LOT_ID = "LOT_ID";

        public const string PO_ITEM_ID = "PO_ITEM_ID";

        public const string GRN_NUM = "GRN_NUM";

        public const string INV_LINE_ID = "INV_LINE_ID";
        public const string INV_LINE_NUM = "INV_LINE_NUM";
        public const string INV_ITEM_ID = "INV_ITEM_ID";
        public const string INV_ITEM_QTY_BILLED = "INV_ITEM_QTY_BILLED";
        public const string INV_ITEM_PRICE = "INV_ITEM_PRICE";
        public const string INV_ITEM_AMT = "INV_ITEM_AMT";
        public const string INV_ITEM_ACCT_CODE_ID = "INV_ITEM_ACCT_CODE_ID";
        public const string BUDGET_CODE = "BUDGET_CODE";

        public const string INV_ITEM_COST_ID = "INV_ITEM_COST_ID";
        public const string INV_ITEM_COST_TYPE = "INV_ITEM_COST_TYPE";

        public const string INV_CHARGE_AMT = "INV_CHARGE_AMT";
        public const string RECEIPT_DATE = "RECEIPT_DATE";

        public const string BUDGET_DTL_ID = "BUDGET_DTL_ID";
        public const string BUDGET_ACCT_GRP_ID = "BUDGET_ACCT_GRP_ID";
        public const string BUDGET_ACCT_CODE_ID = "BUDGET_ACCT_CODE_ID";
        public const string BUDGET_MONTH_AMT_01 = "BUDGET_MONTH_AMT_01";
        public const string BUDGET_MONTH_AMT_02 = "BUDGET_MONTH_AMT_02";
        public const string BUDGET_MONTH_AMT_03 = "BUDGET_MONTH_AMT_03";
        public const string BUDGET_MONTH_AMT_04 = "BUDGET_MONTH_AMT_04";
        public const string BUDGET_MONTH_AMT_05 = "BUDGET_MONTH_AMT_05";
        public const string BUDGET_MONTH_AMT_06 = "BUDGET_MONTH_AMT_06";
        public const string BUDGET_MONTH_AMT_07 = "BUDGET_MONTH_AMT_07";
        public const string BUDGET_MONTH_AMT_08 = "BUDGET_MONTH_AMT_08";
        public const string BUDGET_MONTH_AMT_09 = "BUDGET_MONTH_AMT_09";
        public const string BUDGET_MONTH_AMT_10 = "BUDGET_MONTH_AMT_10";
        public const string BUDGET_MONTH_AMT_11 = "BUDGET_MONTH_AMT_11";
        public const string BUDGET_MONTH_AMT_12 = "BUDGET_MONTH_AMT_12";
        public const string BUDGET_SEGMENT_ID_1 = "BUDGET_SEGMENT_ID_1";
        public const string BUDGET_SEGMENT_ID_2 = "BUDGET_SEGMENT_ID_2";
        public const string BUDGET_SEGMENT_ID_3 = "BUDGET_SEGMENT_ID_3";
        public const string BUDGET_SEGMENT_ID_4 = "BUDGET_SEGMENT_ID_4";
        public const string BUDGET_SEGMENT_ID_5 = "BUDGET_SEGMENT_ID_5";
        public const string BUDGET_SEGMENT_ID_6 = "BUDGET_SEGMENT_ID_6";

        public const string CODE_NAME = "CODE_NAME";
        public const string CODE_ID = "CODE_ID";

        public const string PO_DATA = "PO_DATA";

        public const string item_service = "item_service";

        public const string BATCH_ID = "BATCH_ID";
        public const string BATCH_DESC = "BATCH_DESC";
        public const string REASON_DESC = "REASON_DESC";
        public const string REASON_ID = "REASON_ID";

        public const string HOLIDAY_ID = "HOLIDAY_ID";
        public const string HOLIDAY_DATE = "HOLIDAY_DATE";
        public const string HOLIDAY_DESC = "HOLIDAY_DESC";

        public const string LEAVE_REQ_ID = "LEAVE_REQ_ID";
        public const string LEAVE_NAME = "LEAVE_NAME";



        public const string VACATION_ID = "VACATION_ID";
        public const string VACCATION_TYPE = "VACCATION_TYPE";
        public const string VACATION_DESC = "VACATION_DESC";

        public const string WITH_PAY_YN = "WITH_PAY_YN";
        public const string CARRY_OVER_YN = "CARRY_OVER_YN";

        public const string FROM_DATE = "FROM_DATE";
        public const string TO_DATE = "TO_DATE";

       

        public const string EFFECTIVE_FROM_DT = "EFFECTIVE_FROM_DT";

        public const string EFFECTIVE_TO_DT = "EFFECTIVE_TO_DT";
        public const string VAC_ORG_ID = "VAC_ORG_ID";
        public const string DEPT_DESIG_ID = "DEPT_DESIG_ID";


        public const string LEAVE_DESC = "LEAVE_DESC";
        public const string LEAVE_ID = "LEAVE_ID";
        public const string LEAVE_TYPE = "LEAVE_TYPE";
        public const string NO_OF_DAYS = "NO_OF_DAYS";

        public const string SEGMENT_VALUE_ID = "SEGMENT_VALUE_ID";

        public const string CARRY_OVER_TYPE = "CARRY_OVER_TYPE";
        public const string CAL_DTL_ID = "CAL_DTL_ID";
        public const string COMP_DTL_ID = "COMP_DTL_ID";

        public const string DEPT_LEAVE_ID = "DEPT_LEAVE_ID";
        public const string TM_SCH_CODE = "TM_SCH_CODE";
        public const string TM_SCH_DESC = "TM_SCH_DESC";

        public const string LEAVE_AVAILED = "LEAVE_AVAILED";
        public const string LSD_ID = "LSD_ID";
        public const string LEAVE_BALANCE = "LEAVE_BALANCE";
        public const string ACCURED_LEAVE_BALANCE = "ACCURED_LEAVE_BALANCE";
        
        public const string DESIGN_ID = "DESIGN_ID";
        public const string DESIG_NAME = "DESIG_NAME";
        public const string REQ_ID = "REQ_ID";
        public const string RES_ID = "RES_ID";
        public const string REPAY_ID = "REPAY_ID";
        public const string REPAY_AMT = "REPAY_AMT";
        public const string INSTALLMENT_NO = "INSTALLMENT_NO";
        public const string REPAY_DT = "REPAY_DT";
        public const string REPAY_ACTUAL_DT = "REPAY_ACTUAL_DT";
        public const string REPAY_PAIDYN = "REPAY_PAIDYN";
        public const string REPAY_COMMENTS = "REPAY_COMMENTS";

        public const string ASSIGN_DTL_ID = "ASSIGN_DTL_ID";
        public const string ASSIGN_PER_OBJ_NAME = "ASSIGN_PER_OBJ_NAME";
        public const string PER_DESC = "PER_DESC";
        public const string ASSIGN_HDR_ID = "ASSIGN_HDR_ID";
        public const string ASSIGN_TYPE = "ASSIGN_TYPE";
        public const string ASSIGN_TYPE_NAME = "ASSIGN_TYPE_NAME";

        public const string TYPE_NAME = "TYPE_NAME";
        public const string TYPE_ID = "TYPE_ID";
        public const string PER_OBJ_NAME = "PER_OBJ_NAME";
        public const string PER_OBJ_ID = "PER_OBJ_ID";
        public const string ASSIGN_PER_OBJ_ID = "ASSIGN_PER_OBJ_ID";
        public const string JOB_NAME = "JOB_NAME";

        public const string APP_NAME = "APP_NAME";
       
        public const string STATT_ID = "STATT_ID";
        public const string STAFF_ID = "STAFF_ID";
        public const string ATTENDANCE_TYPE = "ATTENDANCE_TYPE";
        public const string REASON = "REASON";

        public const string RA_EMP_ID = "RA_EMP_ID";
        public const string RA_DTL_ID = "RA_DTL_ID";
        public const string BANK_NAME = "BANK_NAME";
        public const string BANK_ID = "BANK_ID";

        public const string BANK_BRANCH_NAME = "BANK_BRANCH_NAME";
        public const string BANK_BRANCH_ID = "BANK_BRANCH_ID";
        public const string JR_ID = "JR_ID";

        public const string CHECK_DTL_ID = "CHECK_DTL_ID";
        public const string CHECK_LINE_NUM = "CHECK_LINE_NUM";
        public const string CHECK_NUMBER = "CHECK_NUMBER";
        public const string CHECK_DT = "CHECK_DT";
        public const string CHECK_PAY_TO = "CHECK_PAY_TO";
        public const string CHECK_AMT = "CHECK_AMT";
        public const string CHECK_STATUS = "CHECK_STATUS";
        public const string CHECK_CANCEL_REMARKS = "CHECK_CANCEL_REMARKS";
        public const string CHECK_CROSSED_YN = "CHECK_CROSSED_YN";
        public const string ACCOUNT_ID = "ACCOUNT_ID";
        public const string VENDOR_BANK_ACCOUNT_CODE = "VENDOR_BANK_ACCOUNT_CODE";

        public const string PAY_ELEMENT_ID = "PAY_ELEMENT_ID";
        public const string APP_QUALI_SHORT_NAME = "APP_QUALI_SHORT_NAME";
        public const string APP_QUALI_DESCRIPTION = "APP_QUALI_DESCRIPTION";
        public const string APP_ISSUING_AUTHORITY = "APP_ISSUING_AUTHORITY";
        public const string APP_QUALI_DURATION = "APP_QUALI_DURATION";
        public const string PAY_ELEMENT = "PAY_ELEMENT";
        public const string PAY_GROUP_CURR = "PAY_GROUP_CURR";
        public const string CURRENCY_CODE = "CURRENCY_CODE";

        public const string PAY_ID = "PAY_ID";
        public const string PAY_DATE = "PAY_DATE";
        public const string PAY_REMARKS = "PAY_REMARKS";
        public const string PAY_VENDOR_ID = "PAY_VENDOR_ID";
        public const string PAY_BANK_ID = "PAY_BANK_ID";
        public const string PAY_BANK_BRANCH_ID = "PAY_BANK_BRANCH_ID";
        public const string PAY_MODE = "PAY_MODE";
        public const string PAY_CHECK_NUMBER = "PAY_CHECK_NUMBER";
        public const string PAY_ACCOUNT_ID = "PAY_ACCOUNT_ID";
        public const string PAY_DTL_ID = "PAY_DTL_ID";
        public const string PAY_INVOICE_ID = "PAY_INVOICE_ID";
        public const string PAY_INVOICE_AMT = "PAY_INVOICE_AMT";
        public const string PAY_INVOICE_PAID_AMT = "PAY_INVOICE_PAID_AMT";
        public const string inv_id = "inv_id";
        public const string INV_NUM = "INV_NUM";
        public const string INV_DATE = "INV_DATE";
        public const string INV_AMT = "INV_AMT";
        public const string INV_SEGMENT_ID1 = "INV_SEGMENT_ID1";
        public const string INV_SEGMENT_ID2 = "INV_SEGMENT_ID2";
        public const string INV_SEGMENT_ID3 = "INV_SEGMENT_ID3";
        public const string INV_SEGMENT_ID4 = "INV_SEGMENT_ID4";
        public const string INV_SEGMENT_ID5 = "INV_SEGMENT_ID5";
        public const string INV_SEGMENT_ID6 = "INV_SEGMENT_ID6";
        public const string ACCT_TYPE = "ACCT_TYPE";
        public const string acct_code_desc = "acct_code_desc";
        public const string JE_DTL_ID = "JE_DTL_ID";
        public const string DED_PARTY_CODE = "DED_PARTY_CODE";
        public const string BANK_CODE = "BANK_CODE";
        public const string BRANCH_NAME = "BRANCH_NAME";
        public const string BRANCH_CODE = "BRANCH_CODE";
        public const string ACCOUNT_NUMBER = "ACCOUNT_NUMBER";
        public const string VENDOR_CODE = "VENDOR_CODE";

        public const string ORG_NAME = "ORG_NAME";
        public const string ORG_ID = "ORG_ID";

        //Asset Categories
        public const string MINOR_CATEGORY_ID = "MINOR_CATEGORY_ID";
        public const string MINOR_CATEGORY_DESC = "MINOR_CATEGORY_DESC";
        public const string MINOR_CATEGORY_NAME = "MINOR_CATEGORY_NAME";

        public const string CATEGORY_NAME = "CATEGORY_NAME";
        public const string MAJOR_CATEGORY_ID = "MAJOR_CATEGORY_ID";

        public const string MODIFIED_URL = "MODIFIED_URL";
        public const string DELETE_URL = "DELETE_URL";



        public const string INITIAL_AMT_SEP_NAME = "INITIAL_AMT_SEP_NAME";
        public const string INITIAL_AMT_SEP_ID = "INITIAL_AMT_SEP_ID";

        public const string SUBSEQUENT_AMT_SEP_NAME = "SUBSEQUENT_AMT_SEP_NAME";
        public const string SUBSEQUENT_AMT_SEP_ID = "SUBSEQUENT_AMT_SEP_ID";

        //Depreciation Methods
        public const string DPRN_METHOD_MST_ID = "DPRN_METHOD_MST_ID";
        public const string DPRN_NAME = "DPRN_NAME";
        public const string DPRN_METHOD_ID = "DPRN_METHOD_ID";
        public const string DPRN_METHOD_NAME = "DPRN_METHOD_NAME";
        public const string DPRN_DESC = "DPRN_DESC";
        public const string DPRN_TYPE_DURATION_ID = "DPRN_TYPE_DURATION_ID";
        public const string DPRN_RATE = "DPRN_RATE";
        public const string DPRN_TYPE_DURATION_NAME = "DPRN_TYPE_DURATION_NAME";
        public const string DPRN_TYPE_DURATION = "DPRN_TYPE_DURATION";

        //Asset Locations
        public const string ASSET_LOCATION_ID = "ASSET_LOCATION_ID";
        public const string LOCATION_NAME = "LOCATION_NAME";
        public const string LOCATION_DESC = "LOCATION_DESC";

        //Asset
        public const string ASSET_NAME = "ASSET_NAME";
        public const string ASSET_MST_ID = "ASSET_MST_ID";
        public const string ASSET_LOCATION_DTL_ID = "ASSET_LOCATION_DTL_ID";
        public const string ASSET_NUMBER = "ASSET_NUMBER";
        public const string ASSET_MODEL = "ASSET_MODEL";




        //ChangeAssetCategory

        public const string ASSET = "ASSET";
        public const string BASE_CURRENCY_ID = "BASE_CURRENCY_ID";

        //Asset Inwards Service
        public const string OUTWARD_SERVICE_ID = "OUTWARD_SERVICE_ID";
        public const string SERVICE_REFERENCE = "SERVICE_REFERENCE";
        public const string INWARD_DATE = "INWARD_DATE";
        public const string INWARD_SERVICE_ID = "INWARD_SERVICE_ID";


        //Discard
        public const string ASSET_DISCARD_ID = "ASSET_DISCARD_ID";
        public const string DISCARD_TYPE_ID = "DISCARD_TYPE_ID";
        public const string DISCARD_DATE = "DISCARD_DATE";
        public const string SELL_PRICE = "SELL_PRICE";
        public const string THIRD_PARTY_ID = "THIRD_PARTY_ID";
        public const string THIRD_PARTY_NAME = "THIRD_PARTY_NAME";

        public const string DECIMAL_PRECISION = "DECIMAL_PRECISION";

        public const string MENU_CODE = "MENU_CODE";
        public const string FORM_CODE = "FORM_CODE";
        public const string FORM_LABEL = "FORM_LABEL";
        public const string PARENT_MENU_CODE = "PARENT_MENU_CODE";
        public const string LANGUAGE_CODE = "LANGUAGE_CODE";
        public const string MENU_URL = "MENU_URL";
        public const string ORDER_NO = "ORDER_NO";
        public const string REPORT_NAME = "REPORT_NAME";
        public const string ENTRY_PAGE_OPEN = "ENTRY_PAGE_OPEN";
        public const string PK_COLUMN_NAME = "PK_COLUMN_NAME";
        public const string REPORT_CODE = "REPORT_CODE";
        public const string LANGUAGE_DESCRIPTION = "LANGUAGE_DESCRIPTION";
        public const string CURR_ID = "CURR_ID";

        public const string SCREEN_CODE = "SCREEN_CODE";
        public const string SCREEN_NAME = "SCREEN_NAME";
        public const string DESCRIPTION = "DESCRIPTION";
        public const string UNIT_TYPE = "UNIT_TYPE";

        public const string ROLE_CODE = "ROLE_CODE";
        public const string MODULE_CODE = "MODULE_CODE";
        public const string MODULE_ACCESS_FLAG = "MODULE_ACCESS_FLAG";

        public const string FORM = "FORM";
        public const string REPORT = "REPORT";

        public const string ATTRIBUTE1 = "ATTRIBUTE1";
        public const string ATTRIBUTE2 = "ATTRIBUTE2";
        public const string ATTRIBUTE3 = "ATTRIBUTE3";


        public const string State_name = "State_name";
        public const string QUOTE_NAME = "QUOTE_NAME";
        public const string QUOTE_HDR_ID = "QUOTE_HDR_ID";
        public const string OM_ORDER_DTL_ID = "OM_ORDER_DTL_ID";
        public const string OM_ORDER_LINE_NUM = "OM_ORDER_LINE_NUM";
        public const string OM_ORDER_ID = "OM_ORDER_ID";
        public const string OM_ITEM_ID = "OM_ITEM_ID";
        public const string OM_ORDER_DESC = "OM_ORDER_DESC";
        public const string OM_ORDER_QTY = "OM_ORDER_QTY";
        public const string OM_ORDER_UOM = "OM_ORDER_UOM";
        public const string OM_ORDER_PRICE = "OM_ORDER_PRICE";
        public const string OM_ORDER_NUM = "OM_ORDER_NUM";

        public const string ITEM_UNIT_PRICE = "ITEM_UNIT_PRICE";
        public const string QUOTE_DTL_ID = "QUOTE_DTL_ID";
        public const string UOM_DESC = "UOM_DESC";
        public const string PER_CATEGORY = "PER_CATEGORY";
        public const string DATE = "DATE";
        public const string ACCT_GRP_DESC = "GROUP_NAME";
        public const string COM_LEVEL = "COM_LEVEL";
        public const string PER_TYPE = "PER_TYPE";
        public const string VENDOR_TYPE = "VENDOR_TYPE";
        public const string ITEM_CAT_NAME = "ITEM_CATEGORY";
        public const string PERIOD_ID = "PERIOD_ID";
        public const string PERIOD_NAME = "PERIOD_NAME";

        public const string PARAM_CODE = "PARAM_CODE";
        public const string PARAM_NAME = "PARAM_NAME";
        public const string PARAM_DESC = "PARAM_DESC";
        public const string PARAM_VALUE = "PARAM_VALUE";


        public const string JE_SEGMENT_ID_1 = "JE_SEGMENT_ID_1";
        public const string JE_SEGMENT_ID_2 = "JE_SEGMENT_ID_2";

        public const string JE_SEGMENT_ID_3 = "JE_SEGMENT_ID_3";
        public const string JE_SEGMENT_ID_4 = "JE_SEGMENT_ID_4";
        public const string JE_SEGMENT_ID_5 = "JE_SEGMENT_ID_5";
        public const string JE_SEGMENT_ID_6 = "JE_SEGMENT_ID_6";


        public const string INSERT_ALLOWED_FLAG = "INSERT_ALLOWED_FLAG";
        public const string UPDATE_ALLOWED_FLAG = "UPDATE_ALLOWED_FLAG";
        public const string DELETE_ALLOWED_FLAG = "DELETE_ALLOWED_FLAG";
        public const string QUERY_ALLOWED_FLAG = "QUERY_ALLOWED_FLAG";
        public const string FORM_ACCESS_FLAG = "FORM_ACCESS_FLAG";

        public const string effective_date = "effective_date";
        public const string end_date = "end_date";


        public const string FIRST_NAME = "FIRST_NAME";
        public const string MIDDLE_NAME = "MIDDLE_NAME";

        public const string LAST_NAME = "LAST_NAME";
        public const string SURNAME = "SURNAME";
        public const string USER_NAME = "USER_NAME";


        public const string MASTER_CODE = "MASTER_CODE";
        public const string CODE = "CODE";
        public const string PARENT_TYPE = "PARENT_TYPE";
        public const string PARENT_CODE = "PARENT_CODE";
        public const string ROLE_NAME = "ROLE_NAME";
        public const string ROLE_DESC = "ROLE_DESC";
        public const string CURRENCY = "CURRENCY";
        public const string HR_IND_DTL_ID = "HR_IND_DTL_ID";
        public const string HR_APPLICANT_ID = "HR_APPLICANT_ID";

        public const string COURSE_ID = "COURSE_ID";
        public const string TRN_DATE = "TRN_DATE";
        public const string PROG_ID = "PROG_ID";
        public const string SUBJECT_ID = "SUBJECT_ID";
        public const string HR_LTR_DTL_ID = "HR_LTR_DTL_ID";
        public const string APP_QUALI_ID = "APP_QUALI_ID";
        public const string COST_DTL_ID = "COST_DTL_ID";
        public const string APP_ID = "APP_ID";
        public const string HR_IND_EMP_ID = "HR_IND_EMP_ID";
        public const string HR_APPLICANT_NAME = "HR_APPLICANT_ID";

        public const string TRN_BUDGET_SHORT_NAME = "TRN_BUDGET_SHORT_NAME";
        public const string TRN_BUDGET_ID = "TRN_BUDGET_ID";
        public const string ASSET_RETURNABLE = "ASSET_RETURNABLE";



        public const string HR_LTR_ELEMENT_AMOUNT = "HR_LTR_ELEMENT_AMOUNT";
        public const string ENRL_DTL_ID = "ENRL_DTL_ID";
        public const string ENRL_HDR_ID = "ENRL_HDR_ID";
        public const string TRN_SCH_HDR_ID = "TRN_SCH_HDR_ID";
        public const string ASSET_TYPE = "ASSET_TYPE";
        public const string APP_FIRST_NAME = "APP_FIRST_NAME";


        public const string FB_HDR_ID = "FB_HDR_ID";
        public const string FB_DTL_ID = "FB_DTL_ID";
        public const string FB_DESC = "FB_DESC";
        public const string FB_COMMENTS = "FB_COMMENTS";

        public const string VERIFICATION_CATEGORY = "VERIFICATION_CATEGORY";
        public const string VERIFICATION_TYPE = "VERIFICATION_TYPE";
        public const string VERIFICATION_STATUS = "VERIFICATION_STATUS";

        public const string assign_hdr_id = "assign_hdr_id";

        public const string ATT_HDR_ID = "ATT_HDR_ID";
        public const string ATT_DTL_ID = "ATT_DTL_ID";
        public const string ATT_EMP_ID = "ATT_EMP_ID";

        public const string PER_APP_INIT_ID = "PER_APP_INIT_ID";
        public const string RA_HDR_ID = "RA_HDR_ID";

        public const string PROF_ID = "PROF_ID";
        public const string PROF_NAME = "PROF_NAME";
        public const string COM_REMARKS = "COM_REMARKS";
        public const string PROF_DTL_ID = "PROF_DTL_ID";

        public const string POSITION_ID = "POSITION_ID";
        public const string PATH_NAME = "PATH_NAME";
        public const string PATH_DTL_ID = "PATH_DTL_ID";

        public const string PROB_ID = "PROB_ID";
        public const string PROB_STATUS = "PROB_STATUS";
        public const string GL_PCA_DTL_ID = "GL_PCA_DTL_ID";
        public const string GL_PCA_USER_ID = "GL_PCA_USER_ID";
        public const string GL_PCA_DATE = "GL_PCA_DATE";
        public const string GL_PCA_AMOUNT = "GL_PCA_AMOUNT";
        public const string GL_PCA_BALANCE_AMOUNT = "GL_PCA_BALANCE_AMOUNT";
        public const string GL_PCA_REMARKS = "GL_PCA_REMARKS";

        public const string CHECK_HDR_ID = "CHECK_HDR_ID";

        public const string INV_LINE_TYPE = "INV_LINE_TYPE";
        public const string COM_LEVEL_DESC = "COM_LEVEL_DESC";


        public const string OM_INV_LINE_TYPE="OM_INV_LINE_TYPE";
        public const string OM_ITEM_ACCT_CODE_ID="OM_ITEM_ACCT_CODE_ID";
        public const string OM_DC_ID = "OM_DC_ID";
        public const string OM_INV_LINE_NUM="OM_INV_LINE_NUM";
        public const string OM_ITEM_NAME="OM_ITEM_NAME";
        public const string OM_ITEM_QTY="OM_ITEM_QTY";
        public const string OM_ITEM_PRICE="OM_ITEM_PRICE";
        public const string OM_ITEM_AMT="OM_ITEM_AMT";
        public const string OM_INV_SEGMENT_ID1 = "OM_INV_SEGMENT_ID1";
        public const string OM_INV_SEGMENT_ID2 = "OM_INV_SEGMENT_ID2";
        public const string OM_INV_SEGMENT_ID3 = "OM_INV_SEGMENT_ID3";
        public const string OM_INV_SEGMENT_ID4 = "OM_INV_SEGMENT_ID4";
        public const string OM_INV_SEGMENT_ID5 = "OM_INV_SEGMENT_ID5";
        public const string OM_INV_SEGMENT_ID6 = "OM_INV_SEGMENT_ID6";

        public const string OM_INV_NUM = "OM_INV_NUM";
        public const string OM_INV_ID = "OM_INV_ID";

        


        public const string TRNR_NAME = "TRNR_NAME";
        public const string TRNR_ID = "TRNR_ID";

        public const string LOC_DESC = "LOC_DESC";
        public const string LOC_ID = "LOC_ID";

        public const string SHORT_LIST_DTL_ID = "SHORT_LIST_DTL_ID";

        public const string APPLICANT_ID = "APPLICANT_ID";
        public const string APPLICANT_NAME = "APPLICANT_NAME";
        public const string CONTACT_NUMBER = "CONTACT_NUMBER";
        public const string TOT_EXP = "TOT_EXP";
        public const string LN_PAID = "LN_PAID";
        public const string APPLICANT_QUAL_ID = "APPLICANT_QUALI_ID";
        public const string ENTITY_NAME = "ENTITY_NAME";

        public const string COST_TYPE = "COST_TYPE";
        public const string COST_ITEM_DESC = "COST_ITEM_DESC";
        public const string COST_ITEM_REMARK = "COST_ITEM_REMARK";
        public const string COST_QTY = "COST_QTY";
        public const string COST_UNIT_PRICE = "COST_UNIT_PRICE";
        public const string COST_CURRENCY = "COST_CURRENCY";
        public const string COST_AMOUNT = "COST_AMOUNT";
        public const string COM_LINE_ID = "COM_LINE_ID";
        public const string COM_LINE_NUM = "COM_LINE_NUM";
        public const string COM_RATING = "COM_RATING";

        public const string PROB_DTL_ID = "PROB_DTL_ID";
        public const string COM_HDR_ID = "COM_HDR_ID";
        public const string PROB_LINK_DTL_ID = "PROB_LINK_DTL_ID";
        public const string RATING = "RATING";

        public const string PAY_PERIOD_ID = "PAY_PERIOD_ID";

        public const string VAC_EVAL_ID = "VAC_EVAL_ID";
        public const string VAC_CRIT_NAME = "VAC_CRIT_NAME";

         public const string VAC_CRIT_VALUE = "VAC_CRIT_VALUE";

        public const string VAC_CRIT_CONDITION = "VAC_CRIT_CONDITION";
        public const string VAC_EVAL_CONDITION = "VAC_EVAL_CONDITION";

        public const string INDENT_ID = "INDENT_ID";
        public const string INDENT_DTL_ID = "INDENT_DTL_ID";
        public const string ITEM_UOM = "ITEM_UOM";
        public const string ITEM_REQ_QTY = "ITEM_REQ_QTY";
        public const string ITEM_ISSUE_QTY = "ITEM_ISSUE_QTY";

        public const string INDEM_ID = "INDEM_ID";

        public const string OVERTIME_ID = "OVERTIME_ID";

        public const string PAY_AMOUNT = "PAY_AMOUNT";

        public const string APPRAISAL_TRAINING_ID = "APPRAISAL_TRAINING_ID";
        public const string COMMENTS = "COMMENTS";
        public const string COURSE_DESC = "COURSE_DESC";
        public const string REV_TRNING_NEEDED = "REV_TRNING_NEEDED";

        public const string HR_IND_ACT_ID = "HR_IND_ACT_ID";
        public const string CLEARANCE_ID = "CLEARANCE_ID";
        public const string EMP_CLEARANCE_DTL_ID = "EMP_CLEARANCE_DTL_ID";
        public const string PO_SCOPE_ID = "PO_SCOPE_ID";
        public const string PO_SCOPE_DESC = "PO_SCOPE_DESC";

        //Insurance Plan
        public const string ELIGIBLE_PLAN_ID = "ELIGIBLE_PLAN_ID";
        public const string SELECTED_PLAN_ID = "SELECTED_PLAN_ID";
        public const string TOTAL_COST = "TOTAL_COST";
        public const string COMPANY_COST = "COMPANY_COST";
        public const string EMP_COST = "EMP_COST";
        public const string FAMILY_MEMBER = "FAMILY_MEMBER";
        public const string TOTAL_AMOUNT = "TOTAL_AMOUNT";
        public const string PLAN_ENTRY_ID = "PLAN_ENTRY_ID";
        public const string PLAN_ENTRY_DTL_ID = "PLAN_ENTRY_DTL_ID";
        public const string previous_bal_amount = "previous_bal_amount";
        public const string Gen_id = "Gen_id";
        public const string Gen_name = "Gen_name";
        public const string REVAL_TYPE = "REVAL_TYPE";
    }
}

