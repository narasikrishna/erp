﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.HR
{
    public class TimeAttendanceReason_DAL
    {
        static string sqlQuery = "";

        public static string GetTMATNDdtls(String reason_id)
        {
            sqlQuery = string.Empty;



            sqlQuery = " select r.reason_id,r.reason_id as reason_code,r.reason_desc,'N' AS DELETED,";
            sqlQuery += " case r.REASON_MODE when '1' then 'TRUE' else 'FALSE' end as REASON_MODE,";
            sqlQuery += " case r.ATTRIBUTE1 when '1' then 'TRUE' else 'FALSE' end as REASON_MODE_OUT,";
            sqlQuery += " R.ATTRIBUTE2 AS CATEGORY_ID,scm.description AS CATEGORY_DESC";
            sqlQuery += " from tm_reasons r,ssm_code_masters scm";
            sqlQuery += " where r.enabled_flag = 1";
            sqlQuery += " and r.workflow_completion_status = 1";
            sqlQuery += " and scm.code = r.attribute2 ";
            sqlQuery += " and scm.parent_code = 'PER_CAT' ";
            sqlQuery += " and r.reason_id = '" + reason_id + "'";
            return sqlQuery;

        }
        public static string IsValidGraceTime(string startDate, string pKId)
        {
            sqlQuery = string.Empty;


            sqlQuery = "  SELECT count(1) as counts from tM_COMP_GRACE t";
            //sqlQuery += "  where t.tm_start_date=' to_date (" + startDate + "','dd/MM/yyyy') ";
           // sqlQuery += "  where t.pk_id<>'" + pKId + "'";
            sqlQuery += "  where t.tm_end_date is null";

            return sqlQuery;

        }
        public static string GetDiffValidGraceTime(string startDate, string endDate)
        {
            sqlQuery = string.Empty;


            sqlQuery = "  SELECT count(1) as counts from tM_COMP_GRACE t";        
          //  sqlQuery += "  where (t.tm_start_date between  to_date ('" + startDate + "','dd/MM/yyyy')  and  to_date ('" + endDate + "','dd/MM/yyyy')";
            sqlQuery += "  where  ( (to_date ('" + startDate + "','dd/MM/yyyy')  between t.tm_start_date and  t.tm_end_date) ";
            sqlQuery += "  or t.tm_start_date=to_date ('" + startDate + "','dd/MM/yyyy') or t.tm_start_date=to_date ('" + endDate + "','dd/MM/yyyy') ";
            sqlQuery += "   or t.tm_end_date=to_date ('" + startDate + "','dd/MM/yyyy') or t.tm_end_date=to_date ('" + endDate + "','dd/MM/yyyy') )";
            sqlQuery += "  and t.tm_end_date is not null";

            return sqlQuery;

        }

    }
}
