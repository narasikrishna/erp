﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.HR
{
    public class Applicant_DAL
    {
        static string sqlQuery = "";
        public static string GetApplicantNM()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT  A.APP_ID,A.APP_FIRST_NAME";
            sqlQuery += " FROM HR_APPLICANTS A";
            sqlQuery += " WHERE A.ENABLED_FLAG = 1";
            sqlQuery += " AND A.WORKFLOW_COMPLETION_STATUS = 1";

            return sqlQuery;

        }
        public static string GetApplicantNM4EmployeeEntry(string str_status, string mode)
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT  A.APP_ID,A.APP_FIRST_NAME || ' ' || A.APP_MIDDLE_NAME || ' ' || A.APP_LAST_NAME AS APP_FIRST_NAME";
            sqlQuery += " FROM HR_APPLICANTS A";
            sqlQuery += " WHERE A.ENABLED_FLAG = 1";
            sqlQuery += " AND A.WORKFLOW_COMPLETION_STATUS = 1";
            //sqlQuery += " AND APP_ID IN (SELECT hr_ltr_applicant_id FROM HR_OFFER_LETTER_HDR ) ";
            if (mode == FINTableConstant.Add)
            {
                sqlQuery += " AND A.STATUS in (" + str_status + ")";

                if (str_status == "'Offered'")
                {
                    sqlQuery += " AND A.APP_ID IN (select HR_LTR_APPLICANT_ID from HR_OFFER_LETTER_HDR WHERE HR_LTR_ACCEPTED =1) ";
                }
            }

            return sqlQuery;

        }

        public static string GetApplicantQualDtls(string App_id)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select AQ.APP_QUALI_ID,AQ.APP_ID,AQ.APP_QUALI_SHORT_NAME,AQ.APP_QUALI_DESCRIPTION,AQ.APP_ISSUING_AUTHORITY,'N' AS DELETED,";
            sqlQuery += " AQ.APP_QUALI_DURATION,AQ.ENABLED_FLAG";
            sqlQuery += " from hr_applicants_qualification AQ";
            sqlQuery += " where AQ.APP_ID = '" + App_id + "'";

            return sqlQuery;

        }


        public static string GetApplicantPrevCTCDtls(string App_id)
        {
            sqlQuery = string.Empty;

            sqlQuery += " select apc.app_prev_ctc_id,apc.element_name ,to_char(ROUND(apc.element_amount," + VMVServices.Web.Utils.DecimalPrecision + ")) AS element_amount,'N' AS DELETED";
            sqlQuery += " from HR_APPLICANTS_PREV_CTC apc";
            sqlQuery += " where apc.workflow_completion_status = 1";
            sqlQuery += " and apc.app_id = '" + App_id + "'";
            return sqlQuery;

        }

        public static string GetResumeFileName(string resume_id)
        {
            sqlQuery = string.Empty;
            sqlQuery += " select b.FILE_NAME , b.FILE_EXTENSTION from HR_APPLICANTS a , TMP_FILE_DET b ";
            sqlQuery += "  where a.RESUME_ID = b.Tmp_File_Id ";
            sqlQuery += " AND a.RESUME_ID = '" + resume_id + "'";

            return sqlQuery;
        }

        public static string GetPhotoFileName(string photo_id)
        {
            sqlQuery = string.Empty;
            sqlQuery += " select b.FILE_NAME , b.FILE_EXTENSTION from HR_EMPLOYEES a , TMP_FILE_DET b";
            sqlQuery += "  where a.PHOTO_ID = b.Tmp_File_Id";
            sqlQuery += " AND a.PHOTO_ID = '" + photo_id + "'";

            return sqlQuery;
        }
        public static string GetPhotoFileNameBasedEmpId(string emp_id)
        {
            sqlQuery = string.Empty;
            sqlQuery += " select b.FILE_NAME , b.FILE_EXTENSTION from HR_EMPLOYEES a , TMP_FILE_DET b";
            sqlQuery += "  where a.PHOTO_ID = b.Tmp_File_Id";
            sqlQuery += " AND a.EMP_ID = '" + emp_id + "'";

            return sqlQuery;
        }
    }
}
