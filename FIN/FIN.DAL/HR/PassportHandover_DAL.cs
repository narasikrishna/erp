﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.HR
{
   public class PassportHandover_DAL
    {
        static string sqlQuery = "";
        public static string GetHandoverType()
        {
            sqlQuery = string.Empty;

            sqlQuery = "  SELECT HD.HO_TYPE, HD.HO_TYPE_ID";
            sqlQuery += " FROM HR_HANDOVER_DTL HD";
            sqlQuery += " WHERE HD.ENABLED_FLAG = 1";
            sqlQuery += " AND HD.WORKFLOW_COMPLETION_STATUS = 1";

            return sqlQuery;

        }
        public static string GetHandoverTo(string Emp_Id)
        {
            sqlQuery = string.Empty;
            string strLngCode = VMVServices.Web.Utils.LanguageCode;
            sqlQuery = " select t.emp_id,(t.emp_no" + strLngCode + "||' '||t.emp_first_name" + strLngCode + "||' '||t.emp_middle_name" + strLngCode + "||' '||t.emp_last_name" + strLngCode + ") emp_name ";
            sqlQuery += " from hr_employees t";
            sqlQuery += " where t.workflow_completion_status=1";
            sqlQuery += " and t.enabled_flag=1";
            sqlQuery += " and t.emp_id not in '" + Emp_Id + "'";
          //  sqlQuery += " and t.emp_Internal_external = 'I'";
            sqlQuery += " order by t.emp_first_name asc";

            return sqlQuery;
        }
        public static string GetPassportName(string Emp_Id)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select PD.PASS_TXN_ID,PD.PASSPORT_NUMBER || ' - ' || PD.NAME_IN_PASSPORT AS PASSPORT_NAME ";
            sqlQuery += " from HR_EMP_PASSPORT_DETAILS PD";
            sqlQuery += " where PD.workflow_completion_status=1";
            sqlQuery += " and PD.enabled_flag=1";
            sqlQuery += " and ID_TYPE='Passport' ";
            sqlQuery += "and PD.PASSPORT_EMP_ID  = '"+ Emp_Id + "'";
            sqlQuery += " order by PD.NAME_IN_PASSPORT asc";

            return sqlQuery;
        }
        public static string GetReturnBy()
        {
            sqlQuery = string.Empty;
            string strLngCode = VMVServices.Web.Utils.LanguageCode;
            sqlQuery = " SELECT E.EMP_ID, (E.emp_no" + strLngCode + "||' '||E.emp_first_name" + strLngCode + "||' '||E.emp_middle_name" + strLngCode + "||' '||E.emp_last_name" + strLngCode + ") emp_name ";
            sqlQuery += " FROM HR_EMPLOYEES E ";
            sqlQuery += " where E.workflow_completion_status=1";
            sqlQuery += " and E.enabled_flag=1";            
            sqlQuery +="  and e.EMP_ORG_ID='"+VMVServices.Web.Utils.OrganizationID+"'";
          //  sqlQuery += " and e.emp_Internal_external = 'I'";
            sqlQuery += " order by E.EMP_ID asc";

            return sqlQuery;
        }
    }
}
