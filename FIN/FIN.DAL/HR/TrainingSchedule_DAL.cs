﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;


namespace FIN.DAL.HR
{
    public class TrainingSchedule_DAL
    {
        static string sqlQuery = "";

        public static string GetTRSchdtls(String TRN_SCH_HDR_ID)
        {
            sqlQuery = string.Empty;

            //sqlQuery = " SELECT SC.TRN_SCH_DTL_ID,SC.TRN_DATE,P.PROG_ID,P.PROG_DESC,C.COURSE_ID,C.COURSE_DESC,S.SUBJECT_ID,S.SUBJECT_DESC,";
            //sqlQuery += " CM.CODE AS LOOKUP_ID,CM.CODE AS LOOKUP_NAME,'N' AS DELETED";
            //sqlQuery += " FROM HR_TRM_SCHEDULE_DTL SC,HR_TRM_PROG_HDR P,HR_TRM_COURSE C,HR_TRM_SUBJECT S,SSM_CODE_MASTERS CM";
            //sqlQuery += " WHERE SC.PROG_ID = P.PROG_ID";
            //sqlQuery += " AND SC.PROG_COURSE_ID = C.COURSE_ID";
            //sqlQuery += " AND SC.SUBJECT_ID = S.SUBJECT_ID";
            //sqlQuery += " AND SC.TRN_SESSION = CM.CODE";
            //sqlQuery += " AND SC.WORKFLOW_COMPLETION_STATUS = 1";
            //sqlQuery += " AND SC.TRN_SCH_HDR_ID = '" + TRN_SCH_HDR_ID + "'";


            sqlQuery = " SELECT SC.TRN_SCH_DTL_ID,SC.TRN_DATE,P.PROG_ID,P.PROG_DESC,C.COURSE_ID,C.COURSE_DESC,S.SUBJECT_ID,S.SUBJECT_DESC,";
            sqlQuery += " CM.CODE AS LOOKUP_ID,CM.CODE AS LOOKUP_NAME,'N' AS DELETED,e.emp_id,e.emp_first_name as TRAINER,";
            sqlQuery += " TRH.req_date || ', ' || TRH.req_audiance_type || ', ' || TRH.req_comments as REQ_TYPE,";
            sqlQuery += " TRD.req_hdr_id || ', ' || TRD.req_dept_id || ', ' || TRD.req_prog_id as REQ_DTLS,";
            sqlQuery += " TRD.req_hdr_id, TRD.req_dtl_id";
            sqlQuery += " FROM HR_TRM_SCHEDULE_DTL SC,HR_TRM_PROG_HDR P,HR_TRM_COURSE C,HR_TRM_SUBJECT S,SSM_CODE_MASTERS CM,";
            sqlQuery += " hr_trm_request_hdr TRH, hr_trm_request_dtl TRD,hr_employees e,HR_TRM_TRAINER HT";
            sqlQuery += " WHERE SC.PROG_ID = P.PROG_ID";
            sqlQuery += " AND SC.PROG_COURSE_ID = C.COURSE_ID";
            sqlQuery += " AND SC.SUBJECT_ID = S.SUBJECT_ID";
            sqlQuery += " AND SC.TRN_SESSION = CM.CODE";
            sqlQuery += " AND SC.REQ_HDR_ID = TRH.REQ_HDR_ID";
            sqlQuery += " AND SC.REQ_DTL_ID = TRD.REQ_DTL_ID";
            sqlQuery += " and sc.trnr_id = e.emp_id";
            sqlQuery += " and e.emp_id = ht.TRNR_EMP_ID ";
            sqlQuery += " AND SC.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND SC.TRN_SCH_HDR_ID = '" + TRN_SCH_HDR_ID + "'";
            sqlQuery += " order by TRN_SCH_DTL_ID ";

            return sqlQuery;

        }


        public static string GetCourse()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT C.COURSE_ID,C.COURSE_DESC";
            sqlQuery += " FROM HR_TRM_COURSE C";
            sqlQuery += " WHERE C.ENABLED_FLAG = 1";
            sqlQuery += " AND C.WORKFLOW_COMPLETION_STATUS = 1";
            return sqlQuery;

        }
        public static string GetProgram()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT P.PROG_ID,P.PROG_DESC";
            sqlQuery += " FROM HR_TRM_PROG_HDR P";
            sqlQuery += " WHERE P.ENABLED_FLAG = 1";
            sqlQuery += " AND P.WORKFLOW_COMPLETION_STATUS = 1";


            return sqlQuery;

        }
        public static string GetSubject()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT S.SUBJECT_ID,S.SUBJECT_DESC";
            sqlQuery += " FROM HR_TRM_SUBJECT S";
            sqlQuery += " WHERE S.ENABLED_FLAG = 1";
            sqlQuery += " AND S.WORKFLOW_COMPLETION_STATUS = 1";

            return sqlQuery;

        }
        public static string GetScheduleData()
        {
            sqlQuery = string.Empty;

            sqlQuery += " SELECT TSH.TRN_SCH_HDR_ID, TSH.TRN_DESC FROM HR_TRM_SCHEDULE_HDR TSH ";
            sqlQuery += " WHERE TSH.WORKFLOW_COMPLETION_STATUS=1 AND TSH.ENABLED_FLAG=1 ";
            sqlQuery += " AND TSH.TRN_ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";

            return sqlQuery;

        }


        public static string GetProgram4Shedule(string TRN_SCH_HDR_ID)
        {
            sqlQuery = string.Empty;

            sqlQuery += " SELECT DISTINCT TPH.PROG_ID,TPH.PROG_DESC  ";
            sqlQuery += " FROM  HR_TRM_SCHEDULE_HDR TSH ";
            sqlQuery += " INNER JOIN HR_TRM_SCHEDULE_DTL TSD ON TSH.TRN_SCH_HDR_ID = TSD.TRN_SCH_HDR_ID ";
            sqlQuery += " INNER JOIN HR_TRM_PROG_HDR TPH ON TPH.PROG_ID = TSD.PROG_ID ";
            sqlQuery += " WHERE TPH.WORKFLOW_COMPLETION_STATUS =1 AND TPH.ENABLED_FLAG=1 ";
            sqlQuery += " AND TSH.TRN_SCH_HDR_ID ='" + TRN_SCH_HDR_ID + "'";

            return sqlQuery;
        }
        public static string GetCourse4SheduleProg(string TRN_SCH_HDR_ID, string SCH_PROG_ID)
        {
            sqlQuery = string.Empty;

            sqlQuery += " SELECT DISTINCT TC.COURSE_ID,TC.COURSE_DESC  ";
            sqlQuery += " FROM  HR_TRM_SCHEDULE_HDR TSH ";
            sqlQuery += " INNER JOIN HR_TRM_SCHEDULE_DTL TSD ON TSH.TRN_SCH_HDR_ID = TSD.TRN_SCH_HDR_ID ";
            sqlQuery += " INNER JOIN HR_TRM_COURSE TC ON TC.COURSE_ID = TSD.PROG_COURSE_ID ";
            sqlQuery += " WHERE TC.WORKFLOW_COMPLETION_STATUS =1 AND TC.ENABLED_FLAG=1 ";
            sqlQuery += " AND TSH.TRN_SCH_HDR_ID ='" + TRN_SCH_HDR_ID + "' AND TSD.PROG_ID='" + SCH_PROG_ID + "'";

            return sqlQuery;

        }

        public static string GetCourse4Prog(string SCH_PROG_ID)
        {
            sqlQuery = string.Empty;

            sqlQuery += " SELECT DISTINCT TC.COURSE_ID,TC.COURSE_DESC  ";
            sqlQuery += " FROM  HR_TRM_SCHEDULE_HDR TSH ";
            sqlQuery += " INNER JOIN HR_TRM_SCHEDULE_DTL TSD ON TSH.TRN_SCH_HDR_ID = TSD.TRN_SCH_HDR_ID ";
            sqlQuery += " INNER JOIN HR_TRM_COURSE TC ON TC.COURSE_ID = TSD.PROG_COURSE_ID ";
            sqlQuery += " WHERE TC.WORKFLOW_COMPLETION_STATUS =1 AND TC.ENABLED_FLAG=1 ";
            sqlQuery += "  AND TSD.PROG_ID='" + SCH_PROG_ID + "'";

            return sqlQuery;

        }
        public static string GetSubject4ScheduleprogCourse(string TRN_SCH_HDR_ID, string SCH_PROG_ID, string SCH_COURSE_ID)
        {
            sqlQuery = string.Empty;

            sqlQuery += " SELECT DISTINCT TS.SUBJECT_ID,TS.SUBJECT_DESC  ";
            sqlQuery += " FROM  HR_TRM_SCHEDULE_HDR TSH ";
            sqlQuery += " INNER JOIN HR_TRM_SCHEDULE_DTL TSD ON TSH.TRN_SCH_HDR_ID = TSD.TRN_SCH_HDR_ID ";
            sqlQuery += " INNER JOIN HR_TRM_SUBJECT TS ON TS.SUBJECT_ID = TSD.SUBJECT_ID ";
            sqlQuery += " WHERE TS.WORKFLOW_COMPLETION_STATUS =1 AND TS.ENABLED_FLAG=1 ";
            sqlQuery += " AND TSH.TRN_SCH_HDR_ID ='" + TRN_SCH_HDR_ID + "' AND TSD.PROG_ID='" + SCH_PROG_ID + "' AND TSD.PROG_COURSE_ID='" + SCH_COURSE_ID + "'";

            return sqlQuery;
        }

        public static string GetSubject4progCourse(string SCH_PROG_ID, string SCH_COURSE_ID)
        {
            sqlQuery = string.Empty;

            sqlQuery += " SELECT DISTINCT TS.SUBJECT_ID,TS.SUBJECT_DESC  ";
            sqlQuery += " FROM  HR_TRM_SCHEDULE_HDR TSH ";
            sqlQuery += " INNER JOIN HR_TRM_SCHEDULE_DTL TSD ON TSH.TRN_SCH_HDR_ID = TSD.TRN_SCH_HDR_ID ";
            sqlQuery += " INNER JOIN HR_TRM_SUBJECT TS ON TS.SUBJECT_ID = TSD.SUBJECT_ID ";
            sqlQuery += " WHERE TS.WORKFLOW_COMPLETION_STATUS =1 AND TS.ENABLED_FLAG=1 ";
            sqlQuery += " AND TSD.PROG_ID='" + SCH_PROG_ID + "' AND TSD.PROG_COURSE_ID='" + SCH_COURSE_ID + "'";

            return sqlQuery;
        }

        public static string GetTrngSchDtlID4ScheduleprogCourseSubjSession(string TRN_SCH_HDR_ID, string SCH_PROG_ID, string SCH_COURSE_ID, string SUBJECT_ID, string SESSION)
        {
            sqlQuery = string.Empty;

            sqlQuery += " SELECT TSD.TRN_SCH_DTL_ID,TSD.TRNR_ID,TSD.TRN_DATE  ";
            sqlQuery += " FROM  HR_TRM_SCHEDULE_DTL TSD ";
            sqlQuery += " INNER JOIN HR_TRM_SCHEDULE_HDR TSH ON TSH.TRN_SCH_HDR_ID = TSD.TRN_SCH_HDR_ID";
            sqlQuery += " INNER JOIN HR_TRM_SUBJECT TS ON TS.SUBJECT_ID = TSD.SUBJECT_ID ";
            sqlQuery += " INNER JOIN SSM_CODE_MASTERS SCM ON SCM.CODE = TSD.TRN_SESSION ";
            sqlQuery += " WHERE TS.WORKFLOW_COMPLETION_STATUS =1 AND TS.ENABLED_FLAG=1 ";
            sqlQuery += " AND TSD.TRN_SCH_HDR_ID ='" + TRN_SCH_HDR_ID + "' AND TSD.PROG_ID='" + SCH_PROG_ID + "' AND TSD.PROG_COURSE_ID='" + SCH_COURSE_ID + "'";
            sqlQuery += " AND TSD.SUBJECT_ID ='" + SUBJECT_ID + "' AND TSD.TRN_SESSION ='" + SESSION + "'";

            return sqlQuery;
        }


        public static string GetTrngReqHdrDtlID4ProgDeptEmp(string REQ_HDR_ID, string REQ_PROG_ID, string REQ_DEPT_ID, string REQ_EMP_GROUP)
        {
            sqlQuery = string.Empty;

            sqlQuery += " SELECT TRD.REQ_DTL_ID, TRD.REQ_HDR_ID ";
            sqlQuery += " FROM HR_TRM_REQUEST_DTL TRD ";
            sqlQuery += " INNER JOIN HR_TRM_REQUEST_HDR TRH ON TRH.REQ_HDR_ID = TRD.REQ_HDR_ID";
            sqlQuery += " WHERE TRD.WORKFLOW_COMPLETION_STATUS = 1 AND TRD.ENABLED_FLAG = 1 ";
            sqlQuery += " AND TRD.REQ_HDR_ID ='" + REQ_HDR_ID + "' AND TRD.REQ_PROG_ID ='" + REQ_PROG_ID + "' AND TRD.REQ_DEPT_ID ='" + REQ_DEPT_ID + "'";
            sqlQuery += " AND TRD.REQ_EMP_GROUP ='" + REQ_EMP_GROUP + "'";

            return sqlQuery;
        }

        public static string GetRequestType(string req_prog_id)
        {
            sqlQuery = string.Empty;

            sqlQuery += " select req_hdr_id, req_date || ', ' || req_audiance_type || ', ' || req_comments as Req_Header";
            sqlQuery += " from hr_trm_request_hdr rh";
            sqlQuery += " where rh.enabled_flag = 1";
            sqlQuery += " and rh.workflow_completion_status = 1";
            sqlQuery += " and rh.req_prog_id ='" + req_prog_id + "'";

            return sqlQuery;
        }

        public static string GetRequestDtls(string REQ_HDR_ID)
        {
            sqlQuery = string.Empty;

            sqlQuery += " select req_dtl_id, req_hdr_id || ', ' || req_dept_id || ', ' || req_prog_id as Req_Dtls";
            sqlQuery += " from hr_trm_request_dtl";
            sqlQuery += " where req_hdr_id = '" + REQ_HDR_ID + "'";

            return sqlQuery;
        }

        public static string getTrngDtlID(string trnSchHdrID, string progID, string courseID, string subjID)
        {
            sqlQuery = string.Empty;

            sqlQuery += " select trn_sch_dtl_id from hr_trm_schedule_dtl";
            sqlQuery += " where trn_sch_hdr_id = '" + trnSchHdrID + "'";
            sqlQuery += " and prog_id = '" + progID + "'";
            sqlQuery += " and prog_course_id = '" + courseID + "'";
            sqlQuery += " and subject_id = '" + subjID + "'";

            return sqlQuery;
        }

    }
}
