﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.HR
{
    public class Attendance_DAL
    {
        public static string fngetEnrolledEmployee(string str_Enrolled_Id)
        {
            string sqlQuery = "";
            sqlQuery += " SELECT ATT_DTL_ID,EMP.EMP_ID,EMP.EMP_FIRST_NAME AS  EMP_NAME,'TRUE' AS ATT_EMP ";
            sqlQuery += " FROM HR_TRM_ATTEND_DTL TAD ";
            sqlQuery += " INNER JOIN HR_TRM_ATTEND_HDR  TAH ON TAH.ATT_HDR_ID = TAD.ATT_HDR_ID ";
            sqlQuery += " INNER JOIN HR_EMPLOYEES EMP ON EMP.EMP_ID = TAD.ATT_EMP_ID ";
            sqlQuery += " WHERE TAH.ATTRIBUTE1='" + str_Enrolled_Id + "'";
            sqlQuery += " UNION ";
            sqlQuery += " SELECT '0' AS ATT_DTL_ID,EMP.EMP_ID,EMP.EMP_FIRST_NAME AS EMP_NAME,'FALSE' AS ATT_EMP  ";
            sqlQuery += " FROM HR_TRM_ENROLL_HDR TEH ";
            sqlQuery += " INNER JOIN HR_TRM_ENROLL_DTL TED ON TEH.ENRL_HDR_ID = TED.ENRL_HDR_ID ";
            sqlQuery += " INNER JOIN HR_EMPLOYEES EMP ON EMP.EMP_ID = TED.EMP_ID ";
            sqlQuery += " WHERE TEH.ENRL_HDR_ID='" + str_Enrolled_Id + "'";
            sqlQuery += " AND not exists (SELECT ATT_EMP_ID FROM HR_TRM_ATTEND_DTL TAD INNER JOIN HR_TRM_ATTEND_HDR  TAH ON TAH.ATT_HDR_ID = TAD.ATT_HDR_ID ";
            sqlQuery += " WHERE TAH.ATTRIBUTE1='" + str_Enrolled_Id + "' and tad.att_emp_id = ted.emp_id)";
            return sqlQuery;
        }
    }
}
