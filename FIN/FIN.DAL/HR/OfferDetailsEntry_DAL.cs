﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.HR
{
    public class OfferDetailsEntry_DAL
    {
        static string sqlQuery = "";


        public static string GetOfferEntryDetails(string Master_id)
        {
            sqlQuery = string.Empty;

            sqlQuery += "  SELECT OLD.HR_LTR_DTL_ID,OLD.HR_LTR_HDR_ID,PE.PAY_ELEMENT_ID,PE.PAY_ELEMENT_CODE||' - '||PE.PAY_ELEMENT_DESC  as PAY_ELEMENT,to_char(OLD.HR_LTR_ELEMENT_AMOUNT) as HR_LTR_ELEMENT_AMOUNT,OLD.ENABLED_FLAG ";
            sqlQuery += " ,'N' AS DELETED FROM HR_OFFER_LETTER_DTL OLD ";
            sqlQuery += " INNER JOIN PAY_ELEMENTS PE ON OLD.HR_LTR_ELEMENT_ID = PE.PAY_ELEMENT_ID ";
            sqlQuery += " WHERE OLD.HR_LTR_HDR_ID='" + Master_id + "'";
            sqlQuery += " order by HR_LTR_DTL_ID ";

            return sqlQuery;

        }


        public static string GetNoofVacancy(string vac_dept_id, string vac_desig_id)
        {
            sqlQuery = string.Empty;
            sqlQuery += " SELECT sum(v.vac_no_of_peoples) as no_of_vac";
            sqlQuery += " FROM Hr_Vacancies v";
            sqlQuery += " where v.vac_dept_id = '" + vac_dept_id + "'";
            sqlQuery += " and v.vac_desig_id = '" + vac_desig_id + "'";
            sqlQuery += " and v.enabled_flag = 1";
            sqlQuery += " and v.workflow_completion_status = 1";

            return sqlQuery;

        }


        public static string GetNoofApplicant_frVac(string hr_ltr_dept_id, string hr_ltr_desig_id)
        {
            sqlQuery = string.Empty;

            sqlQuery += " select count(*) as no_of_applcnt from HR_OFFER_LETTER_HDR od where od.hr_ltr_accepted = 1";
            sqlQuery += " and od.hr_ltr_dept_id = '" + hr_ltr_dept_id + "'";
            sqlQuery += " and od.hr_ltr_desig_id = '" + hr_ltr_desig_id + "'";
            sqlQuery += " and od.hr_ltr_org_id = '" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " and od.enabled_flag = 1";
            sqlQuery += " and od.workflow_completion_status = 1";
            return sqlQuery;

        }

        public static string GetVac_id(string vac_dept_id, string vac_desig_id)
        {
            sqlQuery = string.Empty;
            sqlQuery += " select distinct ih.vac_id";
            sqlQuery += " from HR_INTERVIEWS_HDR ih";
            sqlQuery += " where ih.app_id in (";
            sqlQuery += " select od.hr_ltr_applicant_id from HR_OFFER_LETTER_HDR od where od.hr_ltr_accepted = 1";
            sqlQuery += " and od.hr_ltr_dept_id = '" + vac_dept_id + "'";
            sqlQuery += " and od.hr_ltr_desig_id = '" + vac_desig_id + "'";
            sqlQuery += " and od.hr_ltr_org_id ='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " and od.enabled_flag = 1";
            sqlQuery += " and od.workflow_completion_status = 1 )";
            sqlQuery += " and ih.int_org_id='" + VMVServices.Web.Utils.OrganizationID + "'";
            return sqlQuery;

        }

        public static void UPDATE_vacancy_Completed(string vac_id)
        {
            try
            {

                string strQuery = string.Empty;

                strQuery += " update Hr_Vacancies ff set ff.MODIFIED_BY='" + VMVServices.Web.Utils.UserName + "',";
                strQuery += " ff.MODIFIED_DATE='" + DateTime.Today.ToString("dd-MMM-yyyy") + "',";
                strQuery += " upper(ff.status) =upper('COMPLETED')";
                strQuery += " where ff.vac_id='" + vac_id + "'";
                strQuery += " and ff.org_id='" + VMVServices.Web.Utils.OrganizationID + "'";

                DBMethod.ExecuteNonQuery(strQuery);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }




    }
}
