﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;


namespace FIN.DAL.HR
{
    public class Program_DAL
    {
        static string sqlQuery = "";

        public static string GetProgramdtls(String PROG_ID)
        {
            sqlQuery = string.Empty;

            sqlQuery = "  SELECT P.PROG_DTL_ID,P.PROG_DURATION,C.COURSE_ID,C.COURSE_DESC,'N' AS DELETED, case when p.enabled_flag =1 then 'true' else 'false' end enabled_flag";
            sqlQuery += "  FROM HR_TRM_PROG_DTL P,HR_TRM_COURSE C";
            sqlQuery += "  WHERE P.PROG_COURSE_ID = C.COURSE_ID";
            sqlQuery += "  AND P.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += "  AND P.PROG_ID = '" + PROG_ID + "'";
            sqlQuery += "  order by PROG_DTL_ID ";

            return sqlQuery;

        }

        public static string GetProgramName()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT PH.PROG_DESC,PH.PROG_ID ";
            sqlQuery += " FROM HR_TRM_PROG_HDR PH ";
            sqlQuery += " WHERE PH.WORKFLOW_COMPLETION_STATUS = 1 ";

            sqlQuery += " AND PH.ENABLED_FLAG = 1 ";


            return sqlQuery;
        }

        public static string GetCourse()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT C.COURSE_ID,C.COURSE_DESC";
            sqlQuery += " FROM HR_TRM_COURSE C";
            sqlQuery += " WHERE C.ENABLED_FLAG = 1";
            sqlQuery += " AND C.COURSE_ORG_ID  = '" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " AND C.WORKFLOW_COMPLETION_STATUS = 1";
            return sqlQuery;

        }

        public static string GetCourseDuration(string COURSE_ID)
        {
            sqlQuery = string.Empty;


            sqlQuery = " SELECT C.COURSE_DURATION";
            sqlQuery += " FROM HR_TRM_COURSE C";
            sqlQuery += " WHERE C.ENABLED_FLAG = 1";
            sqlQuery += " AND C.COURSE_ID = '" + COURSE_ID + "'";
            sqlQuery += " AND C.WORKFLOW_COMPLETION_STATUS = 1";
            return sqlQuery;

        }

        public static string GetCourse(String ProgID)
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT C.COURSE_ID,C.COURSE_DESC";
            sqlQuery += " FROM HR_TRM_COURSE C";
            sqlQuery += " where course_id =(select distinct tsd.prog_course_id from hr_trm_schedule_hdr TSH, hr_trm_schedule_dtl tsd where ";
            sqlQuery += " tsh.trn_sch_hdr_id = tsd.trn_sch_hdr_id";
            sqlQuery += " and tsd.prog_id = '" + ProgID + "')";
            sqlQuery += " and C.COURSE_ORG_ID  = '" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " AND C.ENABLED_FLAG = 1";
            sqlQuery += " AND C.WORKFLOW_COMPLETION_STATUS = 1";

            return sqlQuery;
        }


    }
}
