﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;

namespace FIN.DAL.HR
{
    public class Interview_DAL
    {
        static string sqlQuery = "";

        public static string GetInterviewdtls(String INT_ID)
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT DISTINCT IND.INT_DTL_ID,IND.INT_DATE,IND.INT_LEVEL_SCORED,IND.INT_COMMENTS,D.DEPT_ID,D.DEPT_NAME" + VMVServices.Web.Utils.LanguageCode + " AS DEPT_NAME,DD.DEPT_DESIG_ID,DD.DESIG_NAME" + VMVServices.Web.Utils.LanguageCode + " AS DESIG_NAME,";
            sqlQuery += " E.EMP_ID,E.EMP_FIRST_NAME" + VMVServices.Web.Utils.LanguageCode + " AS EMP_FIRST_NAME ,L.LOC_ID,L.LOC_DESC,C.CODE AS LOOKUP_ID,C.CODE AS LOOKUP_NAME,'N' AS DELETED";
            sqlQuery += " ,IND.ATTRIBUTE1 as time_hr,ind.ATTRIBUTE2 as time_min,ind.ATTRIBUTE3 as time_session";
            sqlQuery += " FROM HR_INTERVIEWS_DTL IND,HR_DEPARTMENTS D,HR_DEPT_DESIGNATIONS DD,HR_EMPLOYEES E,";
            sqlQuery += " HR_LOCATIONS L,SSM_CODE_MASTERS C";
            //sqlQuery += ",HR_VAC_CRITERIA_MASTER VC";
            sqlQuery += " WHERE IND.DEPT_ID = D.DEPT_ID";
            sqlQuery += " AND IND.DEPT_DESIG_ID = DD.DEPT_DESIG_ID";
            sqlQuery += " AND IND.EMP_ID = E.EMP_ID";
            sqlQuery += " AND IND.INT_LOCATION = L.LOC_ID";
            sqlQuery += " AND IND.INT_MODE = C.CODE";
            //sqlQuery += " AND IND.VAC_CRIT_ID = VC.VAC_CRIT_ID";
            sqlQuery += " AND IND.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND IND.INT_ID = '" + INT_ID + "'";
            sqlQuery += " order by INT_DTL_ID ";
            return sqlQuery;

        }

        public static string GetInterviewdtls_basedon_emp(String emp_id)
        {
            sqlQuery = string.Empty;

            sqlQuery += " select ind.int_date,d.dept_name" + VMVServices.Web.Utils.LanguageCode + " AS dept_name,dd.desig_name" + VMVServices.Web.Utils.LanguageCode + " AS desig_name,l.loc_desc,cm.code";
            sqlQuery += " , ind.INT_COMMENTS,ind.INT_STATUS ";
            sqlQuery += " from HR_EMPLOYEES e,HR_INTERVIEWS_DTL ind,hr_departments d,";
            sqlQuery += "  hr_dept_designations dd,hr_locations l,SSM_CODE_MASTERS cm";
            sqlQuery += " where e.emp_id = ind.emp_id";
            sqlQuery += " and ind.dept_id = d.dept_id";
            sqlQuery += " and ind.dept_desig_id = dd.dept_desig_id";
            sqlQuery += " and ind.int_location = l.loc_id";
            sqlQuery += " and ind.int_mode = cm.code";
            sqlQuery += " and ind.INT_DTL_ID = '" + emp_id + "'";
            return sqlQuery;

        }



        public static string getInterview4InterveiwId(string str_ID)
        {
            sqlQuery = string.Empty;
            sqlQuery += " SELECT V.VAC_NAME,V.VAC_ID,A.APP_FIRST_NAME,A.APP_ID,A.APP_CONTACT_NO ";
            sqlQuery += " FROM HR_INTERVIEWS_HDR IH ";
            sqlQuery += " INNER JOIN HR_VACANCIES V ON V.VAC_ID =IH.VAC_ID ";
            sqlQuery += " INNER JOIN HR_APPLICANTS A ON A.APP_ID= IH.APP_ID ";
            sqlQuery += " WHERE IH.INT_ID='" + str_ID + "'";
            return sqlQuery;
        }

        public static string getShortListApp4vacancy(string str_vacid, string Int_ID)
        {
            sqlQuery = string.Empty;
            sqlQuery += " SELECT IH.INT_ID,IH.APP_ID,APP.APP_FIRST_NAME AS APP_NAME,'N' AS DELETED,'TRUE' AS SEL_APP FROM HR_INTERVIEWS_HDR IH ";
            sqlQuery += " INNER JOIN HR_APPLICANTS APP ON APP.APP_ID= IH.APP_ID ";
            sqlQuery += " WHERE IH.INT_ID='" + Int_ID + "'";
            sqlQuery += " UNION ";
            sqlQuery += " SELECT '0' AS INT_ID,APP.APP_ID,APP.APP_FIRST_NAME  AS APP_NAME,'N' AS DELETED,'FALSE' AS SEL_APP FROM HR_SHORT_LIST_HDR SLH ";
            sqlQuery += " INNER JOIN HR_SHORT_LIST_DTL SLD ON SLH.SHORT_LIST_HDR_ID= SLD.SHORT_LIST_HDR_ID ";
            sqlQuery += " INNER JOIN HR_APPLICANTS APP ON APP.APP_ID= SLD.APPLICANT_ID ";
            sqlQuery += " WHERE SLH.VACANCY_ID='" + str_vacid + "'";
           // sqlQuery += "  AND APP.STATUS IN ('New','Re Interview')";
            sqlQuery += "  AND APP.STATUS IN ('Verified')";
            return sqlQuery;
        }

        public static string GetApplicantNM_basedon_vacancy(string vac_id)
        {
            sqlQuery = string.Empty;


            sqlQuery += " select  distinct A.APP_ID, A.APP_FIRST_NAME || ' ' || A.APP_MIDDLE_NAME || ' ' || A.APP_LAST_NAME AS APP_FIRST_NAME";
            sqlQuery += " from HR_APPLICANTS a,HR_INTERVIEWS_HDR i";
            sqlQuery += " where a.app_id = i.app_id";
            sqlQuery += " and i.vac_id = '" + vac_id + "'";


            return sqlQuery;
        }



        public static string GetVacancyNM()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT  V.VAC_ID,V.VAC_NAME";
            sqlQuery += " FROM HR_VACANCIES V";
            sqlQuery += " WHERE V.ENABLED_FLAG = 1";
            sqlQuery += " AND V.ORG_ID = '" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " AND V.WORKFLOW_COMPLETION_STATUS = 1";
            return sqlQuery;

        }

        public static string GetApplicantNM()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT  A.APP_ID,A.APP_FIRST_NAME ||' '|| A.APP_MIDDLE_NAME ||' '|| A.APP_LAST_NAME AS APP_FIRST_NAME ";
            sqlQuery += " FROM HR_APPLICANTS A";
            sqlQuery += " WHERE A.ENABLED_FLAG = 1";
            sqlQuery += " AND A.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND A.STATUS != 'Rejected' ";
            sqlQuery += " AND A.STATUS != 'Schedule' ";

            return sqlQuery;

        }
        public static string GetInterview()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT  A.INT_ID,A.INT_DESC";
            sqlQuery += " FROM HR_INTERVIEWS_HDR A";
            sqlQuery += " WHERE A.ENABLED_FLAG = 1";
            sqlQuery += " AND A.WORKFLOW_COMPLETION_STATUS = 1";

            return sqlQuery;

        }

        public static string GetVacCriteria()
        {
            sqlQuery = string.Empty;


            sqlQuery = " SELECT VC.VAC_CRIT_ID,VC.VAC_CRIT_NAME";
            sqlQuery += " FROM HR_VAC_CRITERIA_MASTER VC";
            sqlQuery += " WHERE VC.ENABLED_FLAG = 1";
            sqlQuery += " AND VC.WORKFLOW_COMPLETION_STATUS = 1";
            return sqlQuery;

        }

        public static string getLocation()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT L.LOC_ID,L.LOC_DESC";
            sqlQuery += " FROM HR_LOCATIONS L";
            sqlQuery += " WHERE L.ENABLED_FLAG = 1";
            sqlQuery += " AND L.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND L.LOC_ORG_ID = '" + VMVServices.Web.Utils.OrganizationID + "'";

            return sqlQuery;
        }

        public static string getIntDtlCRIT4DTLID(string str_dtlid, string str_VacId)
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT IDC.INT_CRIT_ID,IDC.INT_DTL_ID,IDC.VAC_EVAL_ID,IDC.INT_LEVEL_SCORED,VEC.VAC_CRIT_NAME AS VAC_EVAL_NAME ,VEC.VAC_CRIT_VALUE";
            sqlQuery += " ,'N' as DELETED ";
            sqlQuery += " FROM HR_INTERVIEWS_DTL_CRIT IDC ";
            sqlQuery += " INNER JOIN HR_VAC_EVAL_CONDITION VEC ON VEC.VAC_EVAL_ID= IDC.VAC_EVAL_ID ";
            sqlQuery += " WHERE IDC.INT_DTL_ID='" + str_dtlid + "'";
            sqlQuery += " UNION ";
            sqlQuery += " SELECT '0' AS INT_CRIT_ID,'0' AS  INT_DTL_ID,VEC.VAC_EVAL_ID ";
            sqlQuery += " , NULL AS  INT_LEVEL_SCORED ,VEC.VAC_CRIT_NAME AS VAC_EVAL_NAME,VEC.VAC_CRIT_VALUE,'N' AS DELETED ";
            sqlQuery += "  FROM HR_VAC_EVAL_CONDITION VEC ";
            sqlQuery += " WHERE not exists (SELECT cc.VAC_EVAL_ID FROM HR_INTERVIEWS_DTL_CRIT cc WHERE cc.vac_eval_id = vec.vac_eval_id and cc.INT_DTL_ID='" + str_dtlid + "')";
            sqlQuery += " AND VEC.VAC_ID='" + str_VacId + "'";

            return sqlQuery;
        }

        public static string GetEmployeeName(string deptID, string DesigID)
        {
            sqlQuery = string.Empty;
            sqlQuery = " select HE.EMP_ID,HE.EMP_NO || ' - ' ||HE.EMP_FIRST_NAME" + VMVServices.Web.Utils.LanguageCode + " AS EMP_FIRST_NAME  from HR_EMP_WORK_DTLS EWD,HR_EMPLOYEES HE ";
            sqlQuery += " where HE.EMP_ID = EWD.EMP_ID ";
            sqlQuery += " and EWD.EMP_DEPT_ID = '" + deptID + "'";
            sqlQuery += " and EWD.EMP_DESIG_ID = '" + DesigID + "'";
            sqlQuery += " and HE.ENABLED_FLAG = 1";
            sqlQuery += " AND HE.WORKFLOW_COMPLETION_STATUS = 1 ";
            //sqlQuery += " AND EWD.EFFECTIVE_TO_DT IS NULL ";
            sqlQuery += " AND (EWD.EFFECTIVE_TO_DT IS NULL OR";
            sqlQuery += " (SYSDATE BETWEEN EWD.EFFECTIVE_FROM_DT AND EWD.EFFECTIVE_TO_DT))";
            sqlQuery += " AND HE.EMP_ORG_ID = '" + VMVServices.Web.Utils.OrganizationID + "'";
            //sqlQuery += " and HE.EMP_INTERNAL_EXTERNAL = 'I'";
            sqlQuery += " ORDER BY HE.EMP_FIRST_NAME ";

            return sqlQuery;
        }

        public static string getRPTExitInterview()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_EXIT_INTERVIEW V WHERE ROWNUM > 0 ";
            sqlQuery += " AND V.EMP_ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
                {
                    sqlQuery += " AND V.eos_date between to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy')";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
                {
                    sqlQuery += " AND to_date('" + VMVServices.Web.Utils.ReportFilterParameter["To_Date"].ToString() + "','dd/MM/yyyy')";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["Emp_id"] != null)
                {
                    sqlQuery += "  and V.emp_id ='" + VMVServices.Web.Utils.ReportFilterParameter["Emp_id"].ToString() + "'";
                }
            }
            return sqlQuery;
        }

    }
}
