﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;

namespace FIN.DAL.HR
{
    public class LeaveApplication_DAL
    {
        static string sqlQuery = "";

        public static string GetLeaveApplication4YearStaffId(String staff_id, string str_Year, string mode, string lc_hdr_id)
        {
            sqlQuery = string.Empty;

            //sqlQuery = " SELECT HLA.LEAVE_REQ_ID AS LEAVE_REQ_ID,HLA.LEAVE_ID, HLA.LEAVE_ID || ' - ' || to_char(HLA.leave_date_from,'dd/mm/yyyy') || ' - ' ||to_char(HLA.leave_date_to,'dd/mm/yyyy')  AS LEAVE_NAME ";
            //sqlQuery += " FROM HR_LEAVE_APPLICATIONS HLA ";
            //sqlQuery += " WHERE HLA.WORKFLOW_COMPLETION_STATUS = 1 ";
            //sqlQuery += " AND HLA.ENABLED_FLAG = 1 ";
            //sqlQuery += " AND HLA.STAFF_ID =  '" + staff_id + "'";
            //sqlQuery += " AND HLA.FISCAL_YEAR='" + str_Year + "'";

            //sqlQuery += "  and hla.leave_req_id not in";
            //sqlQuery += "    (select ch.leave_req_id";
            //sqlQuery += "    from hr_leave_cancellation_hdr ch";
            //sqlQuery += "   where ch.emp_id ='" + staff_id + "'   and ch.leave_date_from = hla.leave_date_from ";
            //sqlQuery += "     and ch.leave_date_to = hla.leave_date_to)";

            //if (lvReqId != string.Empty)
            //{
            //    sqlQuery += " AND HLA.LEAVE_REQ_ID='" + lvReqId + "'";
            //}
            //sqlQuery += " ORDER BY LEAVE_NAME ";

            sqlQuery += " SELECT HLA.LEAVE_REQ_ID AS LEAVE_REQ_ID, HLA.LEAVE_ID,";

            sqlQuery += " HLA.LEAVE_ID || ' - ' || to_char(HLA.leave_date_from, 'dd/mm/yyyy') ||";
            sqlQuery += " ' - ' || to_char(HLA.leave_date_to, 'dd/mm/yyyy') AS LEAVE_NAME";
            sqlQuery += " FROM HR_LEAVE_APPLICATIONS HLA";
            sqlQuery += " WHERE HLA.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND HLA.ENABLED_FLAG = 1";
            sqlQuery += " AND HLA.STAFF_ID =  '" + staff_id + "'";
            sqlQuery += " AND HLA.FISCAL_YEAR = '" + str_Year + "'";
            sqlQuery += " AND upper(HLA.APP_STATUS) = upper('Approved')";
            sqlQuery += " and hla.no_of_days >";
            sqlQuery += " (select sum(ch.lc_no_of_days)";
            sqlQuery += " from hr_leave_cancellation_hdr ch";
            sqlQuery += " where ch.emp_id =  '" + staff_id + "'";
            sqlQuery += " and ch.emp_id = hla.staff_id";
            sqlQuery += " and hla.leave_req_id = ch.leave_req_id";
            sqlQuery += " and (hla.leave_date_from >= ch.leave_date_from or";
            sqlQuery += " hla.leave_date_from <= ch.leave_date_from)";
            sqlQuery += " and (hla.leave_date_to >= ch.leave_date_to or";
            sqlQuery += " hla.leave_date_to <= ch.leave_date_to))";
            sqlQuery += " union";
            sqlQuery += " SELECT HLA.LEAVE_REQ_ID AS LEAVE_REQ_ID,HLA.LEAVE_ID,";

            sqlQuery += " HLA.LEAVE_ID || ' - ' || to_char(HLA.leave_date_from, 'dd/mm/yyyy') ||";
            sqlQuery += " ' - ' || to_char(HLA.leave_date_to, 'dd/mm/yyyy') AS LEAVE_NAME";
            sqlQuery += " FROM HR_LEAVE_APPLICATIONS HLA";
            sqlQuery += " WHERE HLA.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND HLA.ENABLED_FLAG = 1";
            sqlQuery += " AND HLA.STAFF_ID =  '" + staff_id + "'";
            sqlQuery += " AND HLA.FISCAL_YEAR = '" + str_Year + "'";
            sqlQuery += " AND upper(HLA.APP_STATUS) = upper('Approved')";
            sqlQuery += " and hla.leave_req_id not in";
            sqlQuery += " (select ch.leave_req_id";
            sqlQuery += " from hr_leave_cancellation_hdr ch";
            sqlQuery += " where ch.emp_id =  '" + staff_id + "'";
            if (mode != FINTableConstant.Add)
            {
                sqlQuery += " and ch.lc_hdr_id <>  '" + lc_hdr_id + "'";
            }

            sqlQuery += " )";
            sqlQuery += " ORDER BY LEAVE_NAME";

            return sqlQuery;

        }

        public static string GetLeaveApplication(String staff_id)
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT HLA.LEAVE_REQ_ID AS LEAVE_REQ_ID, HLA.LEAVE_ID AS LEAVE_NAME ";
            sqlQuery += " FROM HR_LEAVE_APPLICATIONS HLA ";
            sqlQuery += " WHERE HLA.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " AND HLA.ENABLED_FLAG = 1 ";
            sqlQuery += " AND HLA.STAFF_ID =  '" + staff_id + "'";
            sqlQuery += " ORDER BY LEAVE_NAME ";

            return sqlQuery;

        }

        public static string GetLeaveID(String staff_id, string fiscal_year, string leave_req_id)
        {
            sqlQuery = string.Empty;

            sqlQuery += " select la.leave_id";
            sqlQuery += " from  HR_LEAVE_APPLICATIONS la";
            sqlQuery += " where la.workflow_completion_status = 1";
            sqlQuery += " and la.staff_id= '" + staff_id + "'";
            sqlQuery += " and la.fiscal_year= '" + fiscal_year + "'";
            sqlQuery += " and la.leave_req_id =  '" + leave_req_id + "'";
            return sqlQuery;

        }

        public static string GetLeaveReqID(String staff_id, string mode)
        {
            sqlQuery = string.Empty;

            //sqlQuery += " select la.leave_req_id,la.leave_req_id||' - '||la.leave_date_from||' - '||la.leave_date_to as Trans_id";
            //sqlQuery += " from  hr_leave_applications la";
            //sqlQuery += " where la.workflow_completion_status = 1";
            //sqlQuery += " and la.enabled_flag = 1";
            //sqlQuery += " and la.staff_id =  '" + staff_id + "'";
            //sqlQuery += " and la.dept_id = '" + dept_id + "'";

            sqlQuery += " select la.leave_req_id,la.leave_req_id||' - '||la.leave_date_from||' - '||la.leave_date_to as Trans_id";
            sqlQuery += " from  hr_leave_applications la";
            sqlQuery += " where la.workflow_completion_status = 1";
            sqlQuery += " and la.enabled_flag = 1";
            sqlQuery += " and la.leave_id = (SELECT SO.HR_EARN_LEAVE FROM SSM_SYSTEM_OPTIONS SO WHERE SO.ORG_ID = '" + VMVServices.Web.Utils.OrganizationID + "' and SO.HR_EARN_LEAVE is not null )";
            if (FINTableConstant.Add == mode)
            {
                sqlQuery += " and la.leave_req_id not in (select le.trans_id from hr_leave_encashment le where le.trans_id is not null)";
            }
            sqlQuery += " and la.staff_id =  '" + staff_id + "'";


            return sqlQuery;

        }

        public static string GetLeaveReqDat(String staff_id, string leave_req_id)
        {
            sqlQuery = string.Empty;

            //sqlQuery += " select la.leave_req_id,la.leave_req_id||' - '||la.leave_date_from||' - '||la.leave_date_to as Trans_id";
            //sqlQuery += " from  hr_leave_applications la";
            //sqlQuery += " where la.workflow_completion_status = 1";
            //sqlQuery += " and la.enabled_flag = 1";
            //sqlQuery += " and la.staff_id =  '" + staff_id + "'";
            //sqlQuery += " and la.dept_id = '" + dept_id + "'";

            sqlQuery += " select la.leave_req_id,to_date(la.leave_date_from,'dd/mm/yy') as leave_date_from,to_date(la.leave_date_to,'dd/mm/yy') as leave_date_to";
            sqlQuery += " from  hr_leave_applications la";
            sqlQuery += " where la.workflow_completion_status = 1";
            sqlQuery += " and la.enabled_flag = 1";
            sqlQuery += " and la.leave_id = (SELECT SO.HR_EARN_LEAVE FROM SSM_SYSTEM_OPTIONS SO WHERE SO.ORG_ID = '" + VMVServices.Web.Utils.OrganizationID + "' and SO.HR_EARN_LEAVE is not null)";
            sqlQuery += " and la.staff_id =  '" + staff_id + "'";
            sqlQuery += " and la.leave_req_id = '" + leave_req_id + "'";


            return sqlQuery;

        }

        public static string GetLeaveApplDat(String leave_req_id)
        {
            sqlQuery = string.Empty;

            sqlQuery += " select l.leave_date_from,l.leave_date_to";
            sqlQuery += " from HR_LEAVE_APPLICATIONS l";
            sqlQuery += " where l.leave_req_id = '" + leave_req_id + "'";
            return sqlQuery;

        }

        public static string GetLeavID_AL(String leave_req_id)
        {
            sqlQuery = string.Empty;

            sqlQuery += " select la.leave_id from ";
            sqlQuery += "  HR_LEAVE_APPLICATIONS la,ssm_system_options so";
            sqlQuery += "  where la.leave_id = so.hr_earn_leave";
            sqlQuery += "  and la.leave_req_id = '" + leave_req_id + "'";

            return sqlQuery;

        }
        public static string GetLeaveID_for_Provision()
        {
            sqlQuery = string.Empty;

            sqlQuery += "  SELECT X.HR_EARN_LEAVE";
            sqlQuery += "  FROM SSM_SYSTEM_OPTIONS X";
            sqlQuery += "   WHERE X.HR_EFFECTIVE_TO_DATE IS NULL";


            return sqlQuery;

        }
        public static string GetNoofDys(String leave_req_id)
        {
            sqlQuery = string.Empty;

            sqlQuery += " select l.no_of_days  from  hr_leave_applications l";
            sqlQuery += " where l.workflow_completion_status = 1";
            sqlQuery += " and l.leave_req_id = '" + leave_req_id + "'";

            return sqlQuery;

        }


        static string spOutPut = string.Empty;
        public static System.Collections.SortedList ErrorCollection = new System.Collections.SortedList();


        public static string GetSPFOR_DUPLICATE_CHECK(string P_STAFF_ID, string P_LEAVE_FROM, string P_LEAVE_TO, string P_RECORD_ID)
        {
            try
            {
                string retcode = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "PKG_VALIDATIONS.PROC_ERR_MGR_LEAVE_APPL";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add("@P_MODULE_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_SCREEN_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_STAFF_ID", OracleDbType.Char).Value = P_STAFF_ID;
                oraCmd.Parameters.Add("@P_LEAVE_FROM", OracleDbType.Char).Value = P_LEAVE_FROM;
                oraCmd.Parameters.Add("@P_LEAVE_TO", OracleDbType.Char).Value = P_LEAVE_TO;
                oraCmd.Parameters.Add("@P_RECORD_ID", OracleDbType.Char).Value = P_RECORD_ID;
                oraCmd.Parameters.Add("@P_RET", OracleDbType.Varchar2, 150);
                oraCmd.Parameters["@P_RET"].Direction = ParameterDirection.Output;
                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

                retcode = oraCmd.Parameters["@P_RET"].Value.ToString();

                return retcode;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GetSPFOR_LEAVE_CANCEL_OR_NOT(string P_STAFF_ID, string P_FISCAL_YR, string P_LEAVE_FROM, string P_LEAVE_TO, string P_RECORD_ID)
        {
            try
            {
                string retcode = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "PKG_VALIDATIONS.PROC_ERR_LEAVE_CANCEL_OR_NOT";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add("@P_MODULE_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_SCREEN_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_STAFF_ID", OracleDbType.Char).Value = P_STAFF_ID;
                oraCmd.Parameters.Add("@P_FISCAL_YR", OracleDbType.Char).Value = P_FISCAL_YR;
                oraCmd.Parameters.Add("@P_LEAVE_FROM", OracleDbType.Char).Value = P_LEAVE_FROM;
                oraCmd.Parameters.Add("@P_LEAVE_TO", OracleDbType.Char).Value = P_LEAVE_TO;
                oraCmd.Parameters.Add("@P_RECORD_ID", OracleDbType.Char).Value = P_RECORD_ID;
                oraCmd.Parameters.Add("@P_RET", OracleDbType.Varchar2, 150);
                oraCmd.Parameters["@P_RET"].Direction = ParameterDirection.Output;
                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

                retcode = oraCmd.Parameters["@P_RET"].Value.ToString();

                return retcode;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static string GetSPFOR_EMP_LEAVE_BAL(string Fiscal_yr, string str_dept, string emp_id, string leav_id)
        {
            try
            {
                string strQuery = string.Empty;

                DataTable dtData = new DataTable();

                strQuery = DBMethod.GetStringValue("select PKG_PAYROLL.get_emp_leave_bal('" + Fiscal_yr + "','" + VMVServices.Web.Utils.OrganizationID + "','" + str_dept + "','" + emp_id + "','" + leav_id + "') from dual");


                return strQuery;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static string GetSPFOR_Leave_Sal(string PAY_PRD, string str_dept, string emp_id, string TO_DT)
        {
            try
            {
                string strQuery = string.Empty;

                DataTable dtData = new DataTable();

                strQuery = DBMethod.GetStringValue("select round(PKG_PAYROLL.leave_salary('" + PAY_PRD + "','" + str_dept + "','" + emp_id + "','" + VMVServices.Web.Utils.OrganizationID + "',to_date(sysdate,'dd/mm/yy'),to_date(sysdate +'" + TO_DT + "','dd/mm/yy'))," + VMVServices.Web.Utils.DecimalPrecision + ") from dual");


                return strQuery;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string getBasicAmount(string str_empid)
        {
            try
            {
                string strQuery = string.Empty;

                DataTable dtData = new DataTable();

                strQuery = DBMethod.GetStringValue("select round(PKG_PAYROLL.get_basic_amt('" + VMVServices.Web.Utils.OrganizationID + "','" + str_empid + "')," + VMVServices.Web.Utils.DecimalPrecision + ") from dual");


                return strQuery;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static string getNoOf_days(string frm_dt, string to_dt)
        {
            try
            {
                string strQuery = string.Empty;

                DataTable dtData = new DataTable();


                strQuery = DBMethod.GetStringValue("select PKG_PAYROLL.get_leave_days('" + VMVServices.Web.Utils.OrganizationID + "',to_date('" + frm_dt + "','dd/MM/yyyy'),to_date('" + to_dt + "','dd/MM/yyyy')) from dual");


                return strQuery;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static string getAsonDateLeaveBalance(string deptId, string empId, string LeaveId, string frm_dt)
        {
            try
            {
                string strQuery = string.Empty;

                DataTable dtData = new DataTable();


                strQuery = DBMethod.GetStringValue("select round(hr_pkg.get_leave_provision('" + VMVServices.Web.Utils.OrganizationID + "','" + deptId + "','" + empId + "','" + LeaveId + "',to_date('" + frm_dt + "','dd/MM/yyyy'),''),1) from dual");


                return strQuery;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static string GetSPFOR_Leave_Sal_app(string PAY_PRD, string str_dept, string emp_id, DateTime frm_dt, DateTime to_dt)
        {
            try
            {
                string strQuery = string.Empty;

                DataTable dtData = new DataTable();

                strQuery = DBMethod.GetStringValue("select round(PKG_PAYROLL.leave_salary('" + PAY_PRD + "','" + str_dept + "','" + emp_id + "','" + VMVServices.Web.Utils.OrganizationID + "', '" + frm_dt.ToString("dd/MMM/yyyy") + "','" + to_dt.ToString("dd/MMM/yyyy") + "')," + VMVServices.Web.Utils.DecimalPrecision + ") from dual");


                return strQuery;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static string get_Leave_Emp_Assign_Substitue_chk(string P_STAFF_ID, string P_LEAVE_FROM, string P_LEAVE_TO)
        {
            sqlQuery = "";
            sqlQuery += "  SELECT * FROM HR_LEAVE_APPLICATIONS LA WHERE LA.SUBSTITUTE_EMP_ID ='" + P_STAFF_ID + "'";
            sqlQuery += " AND ((to_date('" + P_LEAVE_FROM + "','dd/mm/yyyy') BETWEEN LA.LEAVE_DATE_FROM AND LA.LEAVE_DATE_TO) ";
            sqlQuery += " OR (to_date('" + P_LEAVE_TO + "','dd/mm/yyyy') BETWEEN LA.LEAVE_DATE_FROM AND LA.LEAVE_DATE_TO))  ";
            sqlQuery += " AND LA.ENABLED_FLAG=1";
            return sqlQuery;
        }
        public static string get_Substitue_emp_Leave_chk(string P_STAFF_ID, string P_LEAVE_FROM, string P_LEAVE_TO)
        {
            sqlQuery = "";
            sqlQuery += "  SELECT * FROM HR_LEAVE_APPLICATIONS LA WHERE LA.STAFF_ID ='" + P_STAFF_ID + "'";
            sqlQuery += " AND ((to_date('" + P_LEAVE_FROM + "','dd/mm/yyyy') BETWEEN LA.LEAVE_DATE_FROM AND LA.LEAVE_DATE_TO) ";
            sqlQuery += " OR (to_date('" + P_LEAVE_TO + "','dd/mm/yyyy') BETWEEN LA.LEAVE_DATE_FROM AND LA.LEAVE_DATE_TO))  ";
            sqlQuery += " AND LA.ENABLED_FLAG=1";
            return sqlQuery;
        }


        public static string GetLeaveAvail(string fiscal_year, String staff_id, string leave_id)
        {
            sqlQuery = string.Empty;


            sqlQuery += " select (nvl(ls.no_of_days,0)-nvl(ls.leave_availed,0)) as leave_avail";
            sqlQuery += " from hr_leave_staff_defintions ls";
            sqlQuery += " where ls.fiscal_year = '" + fiscal_year + "'";
            sqlQuery += " and ls.staff_id = '" + staff_id + "'";
            sqlQuery += " and ls.leave_id = '" + leave_id + "'";

            return sqlQuery;

        }

        public static string GetLeaveAvailabilit(String staff_id, string leave_id)
        {
            sqlQuery = string.Empty;


            sqlQuery += " select (nvl(ls.no_of_days,0)-nvl(ls.leave_availed,0)) as leave_avail";
            sqlQuery += " from hr_leave_staff_defintions ls";
            //sqlQuery += " where ls.fiscal_year = '" + fiscal_year + "'";
            sqlQuery += " where ls.staff_id = '" + staff_id + "'";
            sqlQuery += " and ls.leave_id = '" + leave_id + "'";

            return sqlQuery;

        }

        public static string GetLeaveAvailability(String staff_id, string leave_id)
        {
            sqlQuery = string.Empty;


            sqlQuery += " select (nvl(ls.no_of_days,0)-nvl(ls.leave_availed,0)) as leave_avail";
            sqlQuery += " from hr_leave_staff_defintions ls";
            //sqlQuery += " where ls.fiscal_year = '" + fiscal_year + "'";
            sqlQuery += " where ls.staff_id = '" + staff_id + "'";
            sqlQuery += " and ls.leave_id = '" + leave_id + "'";

            return sqlQuery;

        }
        public static bool IsPlannedLeave(string fromDate, string toDate, string empId)
        {
            sqlQuery = string.Empty;
            int month_count = 0;
            int reccount = 0;
            bool PlannedFlag = false;

            sqlQuery = " SELECT (SS.HR_LEAVE_ADJUSTMENT_MONTH)";
            sqlQuery += "  FROM SSM_SYSTEM_OPTIONS SS";
            //sqlQuery += "  WHERE SS.HR_EARN_LEAVE = 'AL'";  // KRISHNA COMMENT OUT THIS... 
            //sqlQuery += "  AND SS.MODULE_CODE = 'HR'";
            // sqlQuery += "  WHERE SS.HR_LEAVE_ADJUSTMENT_MONTH IS NOT NULL";

            month_count = DBMethod.GetIntValue(sqlQuery);

            sqlQuery = string.Empty;

            if (month_count > 0)
            {
                sqlQuery = " SELECT count(1) as counts";
                sqlQuery += "  FROM hr_leave_plan_for_al LL";
                sqlQuery += "  WHERE ";
                sqlQuery += "  ll.emp_id = '" + empId + "'";
                sqlQuery += "  and (to_date('" + fromDate + "','dd/MM/yyyy')  between ll.FROM_DATE and ll.to_date";
                sqlQuery += "  or to_date('" + fromDate + "','dd/MM/yyyy')  between add_months(ll.FROM_DATE,-'" + month_count + "') and add_months(ll.to_date,'" + month_count + "')";
                sqlQuery += "  or to_date('" + fromDate + "','dd/MM/yyyy')  between add_months(ll.FROM_DATE,-'" + month_count + "') and add_months(ll.to_date,'" + month_count + "'))";

                sqlQuery += "  and (to_date('" + toDate + "','dd/MM/yyyy')  between ll.FROM_DATE and ll.to_date";
                sqlQuery += "  or to_date('" + toDate + "','dd/MM/yyyy')  between add_months(ll.FROM_DATE,-'" + month_count + "') and add_months(ll.to_date,'" + month_count + "')";
                sqlQuery += "  or to_date('" + toDate + "','dd/MM/yyyy')  between add_months(ll.FROM_DATE,-'" + month_count + "') and add_months(ll.to_date,'" + month_count + "'))";
            }

            reccount = DBMethod.GetIntValue(sqlQuery);

            if (reccount > 0)
            {
                PlannedFlag = true;
            }
            else
            {
                PlannedFlag = false;
            }
            return PlannedFlag;

        }
        public static string getEmpStaffAttandance()
        {
            sqlQuery = string.Empty;

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null && VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
                {
                    sqlQuery = "  select a.EMP_NO ,  ( a.emp_first_name || ' ' ||";
                    sqlQuery += " a.emp_middle_name || ' ' || a.emp_last_name) emp_name,( a.emp_first_name_OL || ' ' ||";
                    sqlQuery += " a.emp_middle_name_ol || ' ' || a.emp_last_name_ol) emp_name_OL ,";
                    sqlQuery += " (select Count(*) from Hr_Staff_Attendance where a.EMP_ID = Staff_Id and Attendance_date >=to_date('" + VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] + "','dd/MM/yyyy')";
                    sqlQuery += " and Attendance_date <=to_date('" + VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] + "','dd/MM/yyyy')) as Workeddays,";
                    sqlQuery += " (select sum(OVERTIME_HOURS) from TM_EMP_OVERTIME where OVERTIME_EMP_ID = a.EMP_ID ";
                    sqlQuery += " and OVERTIME_DATE >=  to_date('" + VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] + "','dd/MM/yyyy') and Overtime_DATE <=  to_date('" + VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] + "','dd/MM/yyyy')) as Overtime_Hours,";
                    sqlQuery += " (select Count(*) from Hr_Staff_Attendance where a.EMP_ID = Staff_Id and Attendance_date>=  to_date('" + VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] + "','dd/MM/yyyy') and Attendance_date <=  to_date('" + VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] + "','dd/MM/yyyy')";
                    sqlQuery += " and Attendance_Type = 'Absent') as Absent_Days,";
                    sqlQuery += " (select Count(*) from Hr_Staff_Attendance where a.EMP_ID = Staff_Id and Attendance_date>=  to_date('" + VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] + "','dd/MM/yyyy') and Attendance_date <=  to_date('" + VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] + "','dd/MM/yyyy')";
                    sqlQuery += " and Attendance_Type in (select leave_id from hr_leave_applications)) as Leave_Days,a.EMp_id";
                    sqlQuery += " ,ewd.emp_dept_id,hd.dept_name, hd.dept_name_ol";
                    sqlQuery += " from Hr_Employees a, hr_emp_work_dtls ewd, hr_departments hd";
                    sqlQuery += " WHERE A.Emp_Org_Id='" + VMVServices.Web.Utils.OrganizationID + "'";
                    sqlQuery += " and hd.dept_id = ewd.emp_dept_id";
                    sqlQuery += " and ewd.effective_to_dt is null";
                    sqlQuery += " and a.emp_id = ewd.emp_id ";
                    if (VMVServices.Web.Utils.ReportViewFilterParameter["Dept_id"] != null)
                    {
                        sqlQuery += " and ewd.emp_dept_id ='" + VMVServices.Web.Utils.ReportFilterParameter["Dept_id"].ToString() + "'";
                    }
                    if (VMVServices.Web.Utils.ReportViewFilterParameter["Emp_id"] != null)
                    {
                        sqlQuery += "  and a.emp_id ='" + VMVServices.Web.Utils.ReportFilterParameter["Emp_id"].ToString() + "'";
                    }
                    sqlQuery += " ORDER BY TO_NUMBER(a.emp_no) asc ";
                }
            }

            return sqlQuery;
        }

        public static void GetSPFOR_LEAVE_ENCASH_POSTING(string P_RECORD_ID, string paymentMode, string period_id = "")
        {
            try
            {
                string retcode = string.Empty;

                OracleCommand oraCmd = new OracleCommand();

                if (paymentMode.ToUpper() == "PAY_NOW")
                {
                    oraCmd.CommandText = "GL_POSTING.HR_LEAVE_ENCASH_POSTING";
                }
                else
                {
                    oraCmd.CommandText = "GL_POSTING.ANNUAL_LEAVE_NON_RECUR_POSTING";
                }

                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add("@P_LC_ID", OracleDbType.Char).Value = P_RECORD_ID;
                oraCmd.Parameters.Add("@P_ORG_ID", OracleDbType.Char).Value = VMVServices.Web.Utils.OrganizationID;

                if (paymentMode.ToUpper() == "IN_PAYROLL")
                {
                    oraCmd.Parameters.Add("@p_period_id", OracleDbType.Char).Value = period_id;
                }

                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string getRPTLeaveHandover()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_LEAVE_HANDOVER V WHERE ROWNUM > 0 ";
            // sqlQuery += " AND V.org_id='" + VMVServices.Web.Utils.OrganizationID + "'";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
                {
                    sqlQuery += " AND V.HO_DATE between to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy')";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
                {
                    sqlQuery += " AND to_date('" + VMVServices.Web.Utils.ReportFilterParameter["To_Date"].ToString() + "','dd/MM/yyyy')";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["Emp_id"] != null)
                {
                    sqlQuery += "  and v.Emp_id ='" + VMVServices.Web.Utils.ReportFilterParameter["Emp_id"].ToString() + "'";
                }
            }
            return sqlQuery;
        }

        public static string getEmpLeaveApplicationSummary()
        {

            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_LEAVE_APP_SUMMARY V WHERE ROWNUM > 0 ";
            sqlQuery += " AND V.org_id='" + VMVServices.Web.Utils.OrganizationID + "'";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
                {
                    sqlQuery += " AND V.LEAVE_DATE_FROM >= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy')";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
                {
                    sqlQuery += " AND V.LEAVE_DATE_FROM <= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["To_Date"].ToString() + "','dd/MM/yyyy')";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["DEPT_ID"] != null)
                {
                    sqlQuery += "  and V.DEPT_ID ='" + VMVServices.Web.Utils.ReportFilterParameter["DEPT_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["Emp_id"] != null)
                {
                    sqlQuery += "  and v.Emp_id ='" + VMVServices.Web.Utils.ReportFilterParameter["Emp_id"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["LEAVE_TYPE"] != null)
                {
                    sqlQuery += "  and V.LEAVE_ID ='" + VMVServices.Web.Utils.ReportFilterParameter["LEAVE_TYPE"].ToString() + "'";
                }

                if (VMVServices.Web.Utils.ReportViewFilterParameter["LEAVE_STATUS"] != null)
                {
                    sqlQuery += "  and V.APP_STATUS ='" + VMVServices.Web.Utils.ReportFilterParameter["LEAVE_STATUS"].ToString() + "'";
                }
            }
            return sqlQuery;
        }

    }
}
