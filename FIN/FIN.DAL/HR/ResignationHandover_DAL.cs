﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;

namespace FIN.DAL.HR
{
    public class ResignationHandover_DAL
    {

        static string sqlQuery = "";



        public static string getResigIntervDtls(string HO_HDR_ID)
        {
            sqlQuery = string.Empty;
            //sqlQuery += " SELECT HD.HO_DTL_ID,HD.HO_LINE_NUM,HD.HO_TYPE,HD.HO_REMARKS,'N' as DELETED,hd.HO_TO";
            //sqlQuery += " FROM HR_HANDOVER_DTL HD";
            //sqlQuery += " WHERE HD.WORKFLOW_COMPLETION_STATUS = 1";
            //sqlQuery += " AND HD.ENABLED_FLAG = 1";
            //sqlQuery += " AND HD.HO_HDR_ID = '" + HO_HDR_ID + "'";
            //sqlQuery += " order by HO_DTL_ID ";

            sqlQuery += " SELECT HD.HO_DTL_ID,AID.ASSET_DTL_ID,AID.ASSET_ID||AID.ASET_NUM AS ASSET,HD.HO_TYPE,HD.HO_REMARKS,'N' as DELETED,hd.HO_TO,E.EMP_ID,E.EMP_FIRST_NAME,";
            sqlQuery += " case hd.enabled_flag when '1' then 'TRUE' ELSE 'FALSE' END AS enabled_flag";
            sqlQuery += " FROM HR_HANDOVER_DTL HD,HR_ASSET_ISSUE_DTL AID,HR_EMPLOYEES e";
            sqlQuery += " WHERE HD.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND E.EMP_ID(+)=HD.HO_TO";
            sqlQuery += " AND HD.ASSET_DTL_ID = AID.ASSET_DTL_ID(+)";
            sqlQuery += " AND HD.ENABLED_FLAG = 1";
            sqlQuery += " AND HD.HO_HDR_ID = '" + HO_HDR_ID + "'";
            sqlQuery += " order by HO_DTL_ID ";


            return sqlQuery;
        }


        public static string getResigReqDtl(string RESIG_HDR_ID)
        {
            sqlQuery = string.Empty;
            sqlQuery += " select RR.RESIG_REASON,RR.RESIG_REMARKS,RR.RESIG_REQ_DT,E.EMP_ID,E.EMP_FIRST_NAME" + VMVServices.Web.Utils.LanguageCode + " AS EMP_FIRST_NAME,D.DEPT_NAME";
            sqlQuery += " from HR_RESIG_REQUEST_HDR RR,HR_EMPLOYEES E,HR_DEPARTMENTS D";
            sqlQuery += " WHERE RR.RESIG_EMP_ID = E.EMP_ID";
            sqlQuery += " AND RR.RESIG_DEPT_ID = D.DEPT_ID";
            sqlQuery += " AND RR.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND RR.ENABLED_FLAG = 1";
            sqlQuery += " AND RR.RESIG_ORG_ID = '" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " AND RR.RESIG_HDR_ID = '" + RESIG_HDR_ID + "'";
            sqlQuery += " ORDER BY RR.RESIG_HDR_ID";

            return sqlQuery;
        }

        public static string getAssetDtl(string emp_id)
        {
            sqlQuery = string.Empty;

            sqlQuery += " SELECT AID.ASSET_DTL_ID,0 as HO_DTL_ID,aid.asset_type as HO_TYPE,AID.ASSET_ID||AID.ASET_NUM AS ASSET,'' as HO_REMARKS,'' as HO_TO,0 as EMP_ID,";
            sqlQuery += " 'N' as DELETED,'TRUE' AS enabled_flag";
            sqlQuery += " FROM HR_ASSET_ISSUE_HDR ai,HR_ASSET_ISSUE_DTL aid";
            sqlQuery += " where ai.asset_hdr_id = aid.asset_hdr_id";
            sqlQuery += " and ai.emp_id = '" + emp_id + "'  and aid.ASSET_RETURNABLE = 1";
            return sqlQuery;
        }


        static string spOutPut = string.Empty;
        public static System.Collections.SortedList ErrorCollection = new System.Collections.SortedList();


        public static string GetSPFOR_DUPLICATE_CHECK(string P_RESIG_HDR_ID, string P_RECORD_ID)
        {
            try
            {
                string retcode = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "PKG_VALIDATIONS.PROC_ERR_MGR_RESIGN_HAND";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add("@P_MODULE_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_SCREEN_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_RESIG_HDR_ID", OracleDbType.Char).Value = P_RESIG_HDR_ID;
                oraCmd.Parameters.Add("@P_RECORD_ID", OracleDbType.Char).Value = P_RECORD_ID;
                oraCmd.Parameters.Add("@P_RET", OracleDbType.Varchar2, 150);
                oraCmd.Parameters["@P_RET"].Direction = ParameterDirection.Output;
                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

                retcode = oraCmd.Parameters["@P_RET"].Value.ToString();

                return retcode;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
