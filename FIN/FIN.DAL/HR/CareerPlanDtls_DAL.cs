﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;

namespace FIN.DAL.HR
{
    public class CareerPlanDtls_DAL
    {
        static string sqlQuery = "";

        public static string GetDepartmentDetails()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT d.DEPT_NAME" + VMVServices.Web.Utils.LanguageCode + " AS DEPT_NAME,d.DEPT_ID ";
            sqlQuery += " FROM HR_DEPARTMENTS d ";
            sqlQuery += " WHERE d.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " and d.org_id = '" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " AND d.ENABLED_FLAG = 1 ";
            sqlQuery += " ORDER BY DEPT_NAME asc ";

            return sqlQuery;
        }

        public static string GetEmployeeProfiles()
        {
            sqlQuery = string.Empty;
            sqlQuery = " select EPH.PROF_ID,EPH.PROF_NAME from HR_EMP_PROFILE_HDR EPH ";
            sqlQuery += " where EPH.WORKFLOW_COMPLETION_STATUS = '1'";
            sqlQuery += " and EPH.ENABLED_FLAG = '1' ";
            sqlQuery += " and EPH.PROF_ORG_ID = '" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " ORDER BY PROF_ID asc ";

            return sqlQuery;
        }

        public static string GetEmployeeProfiles_Dtls(string Emp_prof_id)
        {
            sqlQuery = string.Empty;
            sqlQuery = "  select  EPD.PROF_DTL_ID,HCD.COM_LEVEL_DESC ";
            sqlQuery += " from  HR_EMP_PROFILE_HDR EPH,HR_EMP_PROFILE_DTLS EPD,HR_COMPETANCY_LINKS_DTL CLD,HR_COMPETENCY_DTL HCD ";
            sqlQuery += " where EPH.PROF_ID = EPD.PROF_ID ";
            sqlQuery += " and EPD.COM_LINK_DTL_ID = CLD.COM_LINK_DTL_ID ";
            sqlQuery += " and CLD.COM_LINE_ID = HCD.COM_LINE_ID ";
            sqlQuery += " and EPD.ENABLED_FLAG = '1' ";
            sqlQuery += " and EPD.WORKFLOW_COMPLETION_STATUS = '1' ";
            sqlQuery += " and EPH.PROF_ORG_ID = '" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " and EPH.PROF_ID = '" + Emp_prof_id + "'";
            sqlQuery += " ORDER BY PROF_DTL_ID asc ";

            return sqlQuery;
        }
        public static string GetCareerPlanDtls(string Career_Path_id)
        {
            sqlQuery = string.Empty;
            sqlQuery = " select CPH.PLAN_HDR_ID,CPD.PLAN_DTL_ID,EPH.PROF_ID,EPH.PROF_NAME,CPD.PLAN_PROFILE_HDR_ID,CPD.PLAN_PROFILE_DTL_ID,CPD.PLAN_PROFILE_MIN_VALUE,CPD.PLAN_PROFILE_MAX_VALUE,CPD.PLAN_PROFILE_REQUIRED_VALUE,EPD.PROF_DTL_ID,HCD.COM_LEVEL_DESC,CLD.COM_REMARKS,";
            sqlQuery += " case CPD.ENABLED_FLAG when '1' then 'TRUE' else 'FALSE' end as ENABLED_FLAG,'N' as DELETED";
            sqlQuery += " from Hr_Career_Plan_Hdr CPH,HR_CAREER_PLAN_DTL CPD,HR_EMP_PROFILE_HDR EPH,HR_EMP_PROFILE_DTLS EPD,HR_COMPETANCY_LINKS_DTL CLD,HR_COMPETENCY_DTL HCD";
            sqlQuery += " where CPH.PLAN_HDR_ID = CPD.PLAN_HDR_ID ";
            sqlQuery += " and CPD.PLAN_PROFILE_HDR_ID = EPH.PROF_ID ";
            sqlQuery += " and cpd.plan_profile_dtl_id = epd.prof_dtl_id";
            sqlQuery += " and EPH.PROF_ID = EPD.PROF_ID ";
            sqlQuery += " and EPD.Com_Link_Dtl_Id = CLD.COM_LINK_DTL_ID ";
            sqlQuery += " and CLD.COM_LINE_ID = HCD.COM_LINE_ID ";
            sqlQuery += " and CPH.PLAN_HDR_ID = '" + Career_Path_id + "'";
            sqlQuery += " order by PLAN_DTL_ID ";

            return sqlQuery;
        }

        public static string GetDesignationName(string Desig_id)
        {
            sqlQuery = string.Empty;
            sqlQuery = " select CLD.COM_REMARKS from HR_EMP_PROFILE_DTLS EPD,HR_COMPETANCY_LINKS_DTL CLD";
            sqlQuery += " where EPD.COM_LINK_DTL_ID = CLD.COM_LINK_DTL_ID ";
            sqlQuery += " and EPD.Enabled_Flag = '1'";
            // sqlQuery += " and EPH.WORKFLOW_COMPLETION_STATUS = '1'";
            sqlQuery += " and EPD.PROF_DTL_ID =  '" + Desig_id + "'";

            return sqlQuery;
        }

        public static string GetCareerPathHdr(string Career_path_dtl)
        {
            sqlQuery = string.Empty;
            sqlQuery = " select CPD.PATH_HDR_ID from HR_CAREER_PATH_DTL CPD where CPD.PATH_DTL_ID ='" + Career_path_dtl + "'";

            return sqlQuery;
        }
        public static string GetCareerPlan(string Career_path_dtl)
        {
            sqlQuery = string.Empty;
            sqlQuery = " select CPD.PLAN_HDR_ID,CPD.PLAN_DESC from hr_career_plan_hdr CPD where CPD.PATH_DTL_ID ='" + Career_path_dtl + "'";

            return sqlQuery;
        }
        public static string GetCareerPath(string Dept_id, string Desig_id,string pathdtlId = "")
        {
            sqlQuery = string.Empty;
            sqlQuery = " select CPH.PATH_HDR_ID, CPH.PATH_NAME||' - ' || CPH.PATH_TYPE||' - '||CPH.PATH_DESC as PATH_NAME,CPD.PATH_DTL_ID,cph.PATH_DESC";
            sqlQuery += " from HR_CAREER_PATH_HDR CPH,HR_CAREER_PATH_DTL CPD,HR_DEPT_DESIGNATIONS HDD ";
            sqlQuery += " where CPH.PATH_HDR_ID = CPD.PATH_HDR_ID ";
            sqlQuery += " and CPD.PATH_DESIGNATION = HDD.DEPT_DESIG_ID ";
            sqlQuery += " and CPH.ENABLED_FLAG = '1' ";
            sqlQuery += " and CPH.WORKFLOW_COMPLETION_STATUS = '1' ";
            sqlQuery += " and CPH.PATH_DEPT_ID = '" + Dept_id +"'";
            sqlQuery += " and CPD.PATH_DESIGNATION = '" + Desig_id + "'";
            if (pathdtlId != string.Empty)
            {
                sqlQuery += " and CPD.PATH_DTL_ID='" + pathdtlId + "'";
            }
            sqlQuery += " order by CPH.PATH_DESC";

            return sqlQuery;
        }

        public static string GetCareerPathType(string Path_dtl_id)
        {
            sqlQuery = string.Empty;
            sqlQuery = " select CPH.PATH_TYPE from HR_CAREER_PATH_HDR CPH, HR_CAREER_PATH_DTL CPD  ";
            sqlQuery += " where CPH.PATH_HDR_ID = CPD.PATH_HDR_ID   ";
            sqlQuery += " and CPD.PATH_DTL_ID = '" + Path_dtl_id + "'";

            return sqlQuery;
        }
    }
}
