﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;

namespace FIN.DAL.HR
{
    public class EmployeeContractDetails_DAL
    {
        static string sqlQuery = "";
        public static string GetContractName()
        {
            sqlQuery = string.Empty;

            sqlQuery = " select ch.contract_hdr_id, ch.contract_desc";
            sqlQuery += " from hr_emp_contract_hdr ch";
            sqlQuery += " WHERE ch.ENABLED_FLAG = 1";
            sqlQuery += " AND ch.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND CH.CONTRACT_ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";

            return sqlQuery;

        }

        public static string getContractName4EmpId(string strEmpid)
        {
            sqlQuery = string.Empty;
            sqlQuery += "  SELECT ECH.CONTRACT_HDR_ID,ECH.CONTRACT_DESC FROM HR_EMP_CONTRACT_HDR ECH ";
            sqlQuery += " INNER JOIN HR_EMP_WORK_DTLS EWD ON EWD.EMP_DEPT_ID = ECH.CONTRACT_EMP_DEPT_ID AND EWD.EMP_DESIG_ID = ECH.CONTRACT_EMP_DESIG_ID ";
            sqlQuery += " AND EWD.EMP_CATEGORY =ECH.CONTRACT_EMP_CATEGORY AND EWD.EMP_GRADE_ID = ECH.CONTRACT_EMP_GRADE ";
            sqlQuery += " AND EWD.EMP_JOB_ID = ECH.CONTRACT_EMP_JOB AND EWD.EMP_POSITION_ID =  ECH.POSITION_ID  ";
            sqlQuery += " WHERE EWD.EFFECTIVE_TO_DT IS NULL AND EWD.EMP_ID='" + strEmpid + "'";
            return sqlQuery;
        }

        public static string GetEmployeeContractDtls(string emp_contract_hdr)
        {
            sqlQuery = string.Empty;
            sqlQuery = " select ECD.CONTRACT_LINE_NUM,ECD.CONTRACT_TERM_CONDTIONS,ECD.CONTRACT_REMARKS,";
            sqlQuery += " case  ECD.CONTRACT_TC_APPLICABLE when '1' then 'TRUE' ELSE 'FALSE' END as CONTRACT_TC_APPLICABLE,";
            sqlQuery += " case ECD.ENABLED_FLAG when '1' then 'TRUE' else 'FALSE' END as ENABLED_FLAG,ECD.CONTRACT_DTL_ID,'N' as DELETED";
            sqlQuery += " from HR_EMP_CONTRACT_DTL ECD,HR_EMP_CONTRACT_HDR ECH ";
            sqlQuery += " where ECH.CONTRACT_HDR_ID = ECD.CONTRACT_HDR_ID ";
            sqlQuery += " and ECH.CONTRACT_HDR_ID = '" + emp_contract_hdr + "'";
            sqlQuery += " order by ECD.CONTRACT_DTL_ID asc";

            return sqlQuery;
        }

        public static string GetSPFOR_ERR_MGR_CATEGORY(string P_SCREEN_CODE, string P_RECORD_ID, string P_VALUE1)
        {
            try
            {
                string retcode = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "PKG_DUPLN_VALDN.PROC_DUPLN_VALIDATION_ONE";
                oraCmd.CommandType = CommandType.StoredProcedure;
                //  oraCmd.Parameters.Add("@P_MODULE_CODE", OracleDbType.Char).Value = DBNull.Value;
                //  oraCmd.Parameters.Add("@P_SCREEN_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_SCREEN_CODE", OracleDbType.Char).Value = P_SCREEN_CODE;
                oraCmd.Parameters.Add("@P_PK_ID", OracleDbType.Char).Value = P_RECORD_ID;
                oraCmd.Parameters.Add("@P_VALUE1", OracleDbType.Char).Value = P_VALUE1;
                oraCmd.Parameters.Add("@P_RET", OracleDbType.Varchar2, 150);
                oraCmd.Parameters["@P_RET"].Direction = ParameterDirection.Output;
                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

                retcode = oraCmd.Parameters["@P_RET"].Value.ToString();

                return retcode;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
