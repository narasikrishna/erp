﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.HR
{
   public  class InitiateApprisal_DAL
    {
        static string sqlQuery = "";


        public static string GetInitiateAppraisal(string Master_id)
        {
            sqlQuery = string.Empty;
            string strLngCode = VMVServices.Web.Utils.LanguageCode;
        sqlQuery = "select HE.EMP_NO, ";
        sqlQuery += " HE.emp_first_name" + strLngCode + " || '' || HE.emp_middle_name" + strLngCode + " || '' || ";
        sqlQuery += " HE.emp_last_name" + strLngCode + " as Employee_Name, ";
        sqlQuery += " pai.PER_APP_INIT_ID, ";
        sqlQuery += " HE.EMP_ID, ";
        sqlQuery += " ARD.RA_HDR_ID, ";
        sqlQuery += " case HE.ENABLED_FLAG when '1' then 'TRUE' else 'FALSE' end as ENABLED_FLAG,'N' as DELETED";
       sqlQuery += " from hr_per_app_review_asgnmnt_hdr ARH, ";
       sqlQuery += " hr_per_app_review_asgnmnt_dtl ARD, ";
       sqlQuery += " HR_EMPLOYEES                  HE, ";
       sqlQuery += " hr_per_app_init pai ";
       sqlQuery += " where ARH.RA_HDR_ID = ARD.RA_HDR_ID ";
       sqlQuery += " and ARD.RA_EMP_ID = HE.EMP_ID ";
       sqlQuery += "  and pai.per_app_emp_id = he.emp_id ";
       sqlQuery += " and pai.ra_hdr_id = arh.ra_hdr_id ";
       sqlQuery += " and pai.per_app_init_id = '" + Master_id + "'";
            
            return sqlQuery;

        }

    }
}
