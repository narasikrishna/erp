﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;


namespace FIN.DAL.HR
{
    public class TrainingAttendance_DAL
    {
        static string sqlQuery = "";

        public static string fn_GetTrnAttendanceDtls(String ATT_HDR_ID)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select tad.att_hdr_id, tad.att_dtl_id, tad.att_emp_id, HE.EMP_NO ||' - '|| HE.EMP_FIRST_NAME" + VMVServices.Web.Utils.LanguageCode +" AS EMP_NAME ";
            sqlQuery += " from hr_trm_attend_dtl tad, hr_trm_attend_hdr tah, HR_EMPLOYEES he";
            sqlQuery += " where tad.att_hdr_id = tah.att_hdr_id";
            sqlQuery += " and tad.att_hdr_id = '" + ATT_HDR_ID + "'";
            sqlQuery += " and he.emp_id = tad.att_emp_id";
            sqlQuery += " AND tad.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND tad.ENABLED_FLAG = 1";
            sqlQuery += " ORDER BY EMP_FIRST_NAME asc ";

            return sqlQuery;

        }

        public static string GetSchedule()
        {
            sqlQuery = string.Empty;

            //sqlQuery = " select tsh.trn_sch_hdr_id, tsh.trn_desc  ";
            //sqlQuery += " from hr_trm_schedule_hdr tsh, hr_trm_schedule_dtl tsd ";
            //sqlQuery += " where tsh.trn_sch_hdr_id = tsd.trn_sch_hdr_id";
            //sqlQuery += " and TSH.WORKFLOW_COMPLETION_STATUS = 1";
            //sqlQuery += " and TSH.ENABLED_FLAG = 1";

            sqlQuery = " select tsh.trn_sch_hdr_id, tsh.trn_desc  ";
            sqlQuery += " from hr_trm_schedule_hdr tsh";
            sqlQuery += " where TSH.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " and TSH.ENABLED_FLAG = 1";
            sqlQuery += " and TRN_ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";


            return sqlQuery;
        }

        public static string GetProgram(String TRN_SCH_HDR_ID)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select prog_id, prog_desc from hr_trm_prog_hdr ";
            sqlQuery += " where prog_id =(select distinct tsd.prog_id from hr_trm_schedule_hdr TSH, hr_trm_schedule_dtl tsd where ";
            sqlQuery += " tsh.trn_sch_hdr_id = tsd.trn_sch_hdr_id ";
            sqlQuery += " and tsh.trn_sch_hdr_id = '" + TRN_SCH_HDR_ID + "')";
            sqlQuery += " and WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " and ENABLED_FLAG = 1";
            
            return sqlQuery;
        }

        public static string GetEmployeeDetails()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT HE.EMP_ID AS EMP_ID, HE.EMP_NO ||' - '|| HE.EMP_FIRST_NAME" + VMVServices.Web.Utils.LanguageCode +" AS EMP_NAME  ";
            sqlQuery += " FROM HR_EMPLOYEES HE ";
            sqlQuery += " WHERE HE.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " AND HE.EMP_ORG_ID = '" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " AND HE.ENABLED_FLAG = 1 ";
            sqlQuery += " ORDER BY EMP_FIRST_NAME asc ";

            return sqlQuery;
        }

        public static string GetSPFOR_ERR_MGR_CATEGORY(string P_SCREEN_CODE, string P_RECORD_ID, string P_VALUE1, string P_VALUE2)
        {
            try
            {
                string retcode = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "PKG_DUPLN_VALDN.PROC_DUPLN_VALIDATION_TWO";
                oraCmd.CommandType = CommandType.StoredProcedure;
                //  oraCmd.Parameters.Add("@P_MODULE_CODE", OracleDbType.Char).Value = DBNull.Value;
                //  oraCmd.Parameters.Add("@P_SCREEN_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_SCREEN_CODE", OracleDbType.Char).Value = P_SCREEN_CODE;
                oraCmd.Parameters.Add("@P_PK_ID", OracleDbType.Char).Value = P_RECORD_ID;
                oraCmd.Parameters.Add("@P_VALUE1", OracleDbType.Char).Value = P_VALUE1;
                oraCmd.Parameters.Add("@P_VALUE2", OracleDbType.Char).Value = P_VALUE2;
                oraCmd.Parameters.Add("@P_RET", OracleDbType.Varchar2, 150);
                oraCmd.Parameters["@P_RET"].Direction = ParameterDirection.Output;
                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

                retcode = oraCmd.Parameters["@P_RET"].Value.ToString();

                return retcode;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
