﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.HR
{
   public  class EmployeeContractAssignment_DAL
    {
        static string sqlQuery = "";
        
        public static string GetEmployeeName(string Emp_Id)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select E.EMP_FIRST_NAME" + VMVServices.Web.Utils.LanguageCode + " AS EMP_FIRST_NAME";
            sqlQuery += " from HR_EMPLOYEES E";
            sqlQuery += " where E.workflow_completion_status = 1";
            sqlQuery += " and E.enabled_flag = 1";
            sqlQuery += " and E.EMP_ID = '" + Emp_Id+ "'";
            return sqlQuery;
        }

    }
}
