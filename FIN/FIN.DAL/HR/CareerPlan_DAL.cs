﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;


namespace FIN.DAL.HR
{
    public class CareerPlan_DAL
    {
        static string sqlQuery = "";

        public static string GetDepartmentDetails()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT d.DEPT_NAME" + VMVServices.Web.Utils.LanguageCode + " AS DEPT_NAME,d.DEPT_ID ";
            sqlQuery += " FROM HR_DEPARTMENTS d ";
            sqlQuery += " WHERE d.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " and d.org_id = '" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " AND d.ENABLED_FLAG = 1 ";
            sqlQuery += " ORDER BY DEPT_NAME asc ";

            return sqlQuery;
        }

        public static string GetDesignationName(string Dept_id)
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT HDD.DEPT_DESIG_ID AS DESIGN_ID,HDD.DESIG_NAME" + VMVServices.Web.Utils.LanguageCode + " AS DESIG_NAME ";
            sqlQuery += " FROM HR_DEPT_DESIGNATIONS HDD ";
            sqlQuery += " WHERE HDD.ENABLED_FLAG = 1 ";
            sqlQuery += " AND HDD.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " AND HDD.ORG_ID = '" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " AND HDD.DEPT_ID = '" + Dept_id + "'";
            sqlQuery += " ORDER BY HDD.DESIG_NAME ";

            return sqlQuery;
        }

        public static string GetCareerPlanDtls(string Verify_id)
        {
            sqlQuery = string.Empty;
            sqlQuery = " select CPD.PATH_DESIGNATION,CPD.PATH_LEVEL,HDD.DESIG_NAME" + VMVServices.Web.Utils.LanguageCode + " AS DESIG_NAME,HDD.DEPT_DESIG_ID AS DESIGN_ID,CPD.PATH_DTL_ID,CPD.PATH_HDR_ID, ";
            //  (select SM.DESCRIPTION from SSM_CODE_MASTERS SM where SM.CODE = ASID.ASSET_RETURNABLE and SM.PARENT_CODE = 'AST_TYP') as ASSET_TYPE, ";
            sqlQuery += "  case CPD.ENABLED_FLAG when '1' then 'TRUE' else 'FALSE' end as ENABLED_FLAG,'Y' as DELETED";
            sqlQuery += " from HR_CAREER_PATH_DTL CPD,HR_CAREER_PATH_HDR CPH,HR_DEPT_DESIGNATIONS HDD";
            sqlQuery += " where CPD.PATH_HDR_ID = CPH.PATH_HDR_ID ";
            sqlQuery += " and HDD.DEPT_DESIG_ID = CPD.PATH_DESIGNATION ";
            sqlQuery += " and CPH.PATH_HDR_ID = '" + Verify_id + "'";
            //sqlQuery += " order by CPD.PATH_LEVEL ";
            sqlQuery += " order by CPD.PATH_DTL_ID asc ";

            return sqlQuery;
        }
    }
}
