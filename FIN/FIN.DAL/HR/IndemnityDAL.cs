﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;
namespace FIN.DAL.HR
{
    public class IndemnityDAL
    {
        static string sqlQuery = "";

        public static void GetSP_Indemnity_Ledger(string deptId, string empId, DateTime transDate)
        {
            try
            {

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandType = CommandType.StoredProcedure;

                oraCmd.CommandText = "hr_pkg.hr_indemnity_ledger";

                oraCmd.Parameters.Add(new OracleParameter("@P_ORG_ID", OracleDbType.Varchar2, 250)).Value = VMVServices.Web.Utils.OrganizationID;
                oraCmd.Parameters.Add(new OracleParameter("@p_dept_id", OracleDbType.Varchar2, 250)).Value = deptId;
                oraCmd.Parameters.Add(new OracleParameter("@p_emp_id", OracleDbType.Varchar2, 250)).Value = empId;
                oraCmd.Parameters.Add(new OracleParameter("@p_sep_type", OracleDbType.Varchar2, 250)).Value = "R";
                oraCmd.Parameters.Add(new OracleParameter("@p_from_dte", OracleDbType.Date, 250)).Value = transDate.ToString("dd/MMM/yyyy");

                DBMethod.ExecuteFunction(oraCmd);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static decimal GetSP_Indemnity_Amount(string empId, DateTime transDate)
        {
            try
            {

                decimal strAmt = 0; 
              //  OracleCommand oraCmd = new OracleCommand();
              //  oraCmd.CommandType = CommandType.StoredProcedure;
              //  oraCmd.CommandText = "hr_pkg.get_indemnity_amount";
              //  oraCmd.Parameters.Add(new OracleParameter("@P_ORG_ID", OracleDbType.Varchar2, 250)).Value = VMVServices.Web.Utils.OrganizationID;
              ////  oraCmd.Parameters.Add(new OracleParameter("@p_dept_id", OracleDbType.Varchar2, 250)).Value = deptId;
              //  oraCmd.Parameters.Add(new OracleParameter("@p_emp_id", OracleDbType.Varchar2, 250)).Value = empId;
              //  oraCmd.Parameters.Add(new OracleParameter("@p_sep_type", OracleDbType.Varchar2, 250)).Value = "R";
              //  oraCmd.Parameters.Add(new OracleParameter("@p_from_dte", OracleDbType.Date, 250)).Value = transDate.ToString("dd/MMM/yyyy");
              //  oraCmd.Parameters.Add("@TXNCODE", OracleDbType.Varchar2, 150);
              //  oraCmd.Parameters["@ERRMESG"].Direction = ParameterDirection.Output;             
              //  oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);
             //   DBMethod.ExecuteFunction(oraCmd);
               //strAmt  = oraCmd.Parameters["@TXNCODE"].Value.ToString();

               strAmt =DBMethod.GetDecimalValue("select round(NVL(hr_pkg.get_indemnity_amount('" + VMVServices.Web.Utils.OrganizationID + "','" + empId + "','R','" + transDate.ToString("dd/MMM/yyyy") + "'),0),3) from dual");
               return strAmt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        
        public static string ValidateExistingEmp(string deptId, string empId, DateTime transDate)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select count(1) as counts from hr_indem_txn_ledger tl";
            sqlQuery += " where tl.indem_org_id='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " and tl.indem_dept_id='" + deptId + "'";
            sqlQuery += " and tl.indem_emp_id='" + empId + "'";
            sqlQuery += " and tl.indem_txn_date=to_date('" + transDate + "','dd/MM/yyyy')";

            return sqlQuery;

        }
        public static string getIndemnityProvisionReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_INDEMNITY_LEDGER V WHERE ROWNUM > 0 ";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["IndemDate"] != null)
                {
                    sqlQuery += " AND V.indem_txn_date = to_date('" + VMVServices.Web.Utils.ReportFilterParameter["IndemDate"].ToString() + "','dd/MM/yyyy') ";
                }

                sqlQuery += " AND V.INDEM_ORG_ID ='" + VMVServices.Web.Utils.OrganizationID+ "'";
                
            }
            return sqlQuery;
        }


    }
}
