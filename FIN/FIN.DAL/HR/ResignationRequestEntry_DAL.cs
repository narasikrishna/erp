﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;

namespace FIN.DAL.HR
{
    public class ResignationRequestEntry_DAL
    {

        static string sqlQuery = "";
        static string spOutPut = string.Empty;
        public static System.Collections.SortedList ErrorCollection = new System.Collections.SortedList();


        public static string GetSPFOR_DUPLICATE_CHECK(string P_RESIG_EMP_ID, string P_RECORD_ID)
        {
            try
            {
                string retcode = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "PKG_VALIDATIONS.PROC_ERR_MGR_RESIGN_REQ";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add("@P_MODULE_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_SCREEN_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_RESIG_EMP_ID", OracleDbType.Char).Value = P_RESIG_EMP_ID;
                oraCmd.Parameters.Add("@P_RECORD_ID", OracleDbType.Char).Value = P_RECORD_ID;
                oraCmd.Parameters.Add("@P_RET", OracleDbType.Varchar2, 150);
                oraCmd.Parameters["@P_RET"].Direction = ParameterDirection.Output;
                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

                retcode = oraCmd.Parameters["@P_RET"].Value.ToString();

                return retcode;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string getEmployee_dept(string Dept_id)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select ewd.emp_id,he.emp_first_name" + VMVServices.Web.Utils.LanguageCode + "AS emp_first_name from hr_emp_work_dtls ewd,hr_employees he ";
            sqlQuery += " where  he.emp_id = ewd.emp_id";
            sqlQuery += " and ewd.enabled_flag = '1' ";
            sqlQuery += " and ewd.workflow_completion_status = '1' ";
            sqlQuery += " and ewd.emp_dept_id = '" + Dept_id + "'";

            return sqlQuery;
        }
    }
}
