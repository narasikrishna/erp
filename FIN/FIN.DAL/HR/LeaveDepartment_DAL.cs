﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;

namespace FIN.DAL.HR
{
    public class LeaveDepartment_DAL
    {
        static string sqlQuery = "";

        public static string GetLeaveDepartmentDtls(string deptLeaveId)
        {
            sqlQuery = string.Empty;

            sqlQuery = "    select f.DEPT_LEAVE_ID,";
            sqlQuery += "   f.LEAVE_ID,LL.LEAVE_DESC,";
            // sqlQuery += "    (SELECT LL.LEAVE_DESC FROM HR_LEAVE_DEFINITIONS LL WHERE LL.LEAVE_ID=F.LEAVE_ID) AS LEAVE_DESC,";
            sqlQuery += "    f.DEPT_ID,";
            sqlQuery += "    f.FISCAL_YEAR,";
            sqlQuery += "    f.EFFECTIVE_FROM_DT,";
            sqlQuery += "    f.EFFECTIVE_TO_DT,";
            sqlQuery += "  CASE f.ENABLED_FLAG WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS ENABLED_FLAG,";
            sqlQuery += "    f.WORKFLOW_COMPLETION_STATUS,";
            sqlQuery += "     'N' AS DELETED";
            sqlQuery += "   from hr_leave_dept F,HR_LEAVE_DEFINITIONS ll";
            sqlQuery += "  where f.workflow_completion_status = 1 and LL.LEAVE_ID = F.LEAVE_ID AND F.FISCAL_YEAR = LL.FISCAL_YEAR";
            sqlQuery += "  AND f.DEPT_LEAVE_ID = '" + deptLeaveId + "'";


            return sqlQuery;

        }


        public static string GetLeaveDepartmentDtls_BasedDept_fsyr(string dept_id, string FISCAL_YEAR)
        {
            sqlQuery = string.Empty;


            sqlQuery = " select F.DEPT_LEAVE_ID,F.LEAVE_ID,LL.LEAVE_DESC,F.EFFECTIVE_FROM_DT,F.EFFECTIVE_TO_DT,CASE F.ENABLED_FLAG WHEN '1' THEN 'TRUE' ELSE 'FALSE' END as ENABLED_FLAG,";
            sqlQuery += " 'N' AS DELETED  from hr_leave_dept F, HR_LEAVE_DEFINITIONS ll";
            sqlQuery += " where F.workflow_completion_status = 1";
            sqlQuery += " and LL.LEAVE_ID = F.LEAVE_ID";
            sqlQuery += " AND F.dept_id = '" + dept_id + "'";
            sqlQuery += " AND F.FISCAL_YEAR = '" + FISCAL_YEAR + "'";
            sqlQuery += " AND LL.ORG_ID=F.Org_Id";
            sqlQuery += " AND LL.ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";

            return sqlQuery;

        }



        public static string GetLeaveDepartmentBasedFisYear(string FISCAL_YEAR)
        {
            sqlQuery = string.Empty;

            sqlQuery = "    select distinct f.DEPT_LEAVE_ID,";
            sqlQuery += "   f.LEAVE_ID,LL.LEAVE_DESC,";
            sqlQuery += "    f.DEPT_ID,hd.DEPT_NAME,";
            sqlQuery += "    f.FISCAL_YEAR";
            sqlQuery += "   from hr_leave_dept F,HR_LEAVE_DEFINITIONS ll,HR_DEPARTMENTS hd";
            sqlQuery += "   where f.workflow_completion_status = 1 and f.ENABLED_FLAG='1' and LL.PK_ID = F.LEAVE_ID and f.DEPT_ID=hd.DEPT_ID";
            sqlQuery += "   AND f.FISCAL_YEAR = '" + FISCAL_YEAR + "'";


            return sqlQuery;

        }

        public static string GetLeaveDepartment()
        {
            sqlQuery = string.Empty;

            sqlQuery = "    select distinct f.DEPT_LEAVE_ID,";
            sqlQuery += "   f.LEAVE_ID,LL.LEAVE_DESC,";
            sqlQuery += "    f.DEPT_ID,hd.DEPT_NAME,";
            sqlQuery += "    f.FISCAL_YEAR";
            sqlQuery += "   from hr_leave_dept F,HR_LEAVE_DEFINITIONS ll,HR_DEPARTMENTS hd";
            sqlQuery += "   where f.workflow_completion_status = 1 and f.ENABLED_FLAG='1' and LL.PK_ID = F.LEAVE_ID and f.DEPT_ID=hd.DEPT_ID";
            //sqlQuery += "   AND f.FISCAL_YEAR = '" + FISCAL_YEAR + "'";


            return sqlQuery;

        }

        public static string GetDeptBasedFisYear(string FISCAL_YEAR)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select distinct hd.DEPT_ID,hd.DEPT_NAME" + VMVServices.Web.Utils.LanguageCode + " AS DEPT_NAME";
            sqlQuery += " from hr_leave_dept F,HR_DEPARTMENTS hd";
            sqlQuery += " where f.workflow_completion_status = 1";
            sqlQuery += " and f.ENABLED_FLAG = '1'";
            sqlQuery += " and f.DEPT_ID = hd.DEPT_ID";
            sqlQuery += " AND f.FISCAL_YEAR = '" + FISCAL_YEAR + "'";


            return sqlQuery;

        }




        static string spOutPut = string.Empty;
        public static System.Collections.SortedList ErrorCollection = new System.Collections.SortedList();


        public static string GetSPFOR_DUPLICATE_CHECK(string P_FISCAL_YEAR, string P_DEPT_ID, string P_LEAVE_ID, string P_RECORD_ID)
        {
            try
            {
                string retcode = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "PKG_VALIDATIONS.PROC_ERR_MGR_LEAVE_DEPT";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add("@P_MODULE_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_SCREEN_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_FISCAL_YEAR", OracleDbType.Char).Value = P_FISCAL_YEAR;
                oraCmd.Parameters.Add("@P_DEPT_ID", OracleDbType.Char).Value = P_DEPT_ID;
                oraCmd.Parameters.Add("@P_LEAVE_ID", OracleDbType.Char).Value = P_LEAVE_ID;
                oraCmd.Parameters.Add("@P_RECORD_ID", OracleDbType.Char).Value = P_RECORD_ID;
                oraCmd.Parameters.Add("@P_RET", OracleDbType.Varchar2, 150);
                oraCmd.Parameters["@P_RET"].Direction = ParameterDirection.Output;
                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

                retcode = oraCmd.Parameters["@P_RET"].Value.ToString();

                return retcode;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static string GetLeaveBasedFiscalYearDept(string fisYear, string deptId,string emp_id)
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT distinct LD.LEAVE_ID AS LEAVE_ID,LD.LEAVE_DESC";
            sqlQuery += " FROM  HR_LEAVE_DEFINITIONS LD,hr_leave_staff_defintions ls";
            sqlQuery += " WHERE LD.ENABLED_FLAG = 1";
            sqlQuery += " AND LD.WORKFLOW_COMPLETION_STATUS = 1";          
            sqlQuery += " and ld.leave_id = ls.leave_id";            
            sqlQuery += " and ls.FISCAL_YEAR='" + fisYear + "'";
            sqlQuery += " and ls.DEPT_ID='" + deptId + "'";
            sqlQuery += " and ls.staff_id = '" + emp_id + "'";
            sqlQuery += " ORDER BY LD.LEAVE_DESC";

            return sqlQuery;

        }

        public static string GetLeaveBasedDept(string deptId)
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT distinct LD.LEAVE_ID AS LEAVE_ID,LD.LEAVE_DESC";
            sqlQuery += " FROM  HR_LEAVE_DEFINITIONS LD,hr_leave_dept F";
            sqlQuery += " WHERE LD.ENABLED_FLAG = 1";
            sqlQuery += " AND LD.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += "  and f.LEAVE_ID=LD.LEAVE_ID";
           // sqlQuery += " and f.FISCAL_YEAR='" + fisYear + "'";
            sqlQuery += " and f.DEPT_ID='" + deptId + "'";
            sqlQuery += " ORDER BY LD.LEAVE_DESC";

            return sqlQuery;

        }


        public static string GetLeaveBasedFiscalYearDept_frSLD(string fisYear, string deptId)
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT distinct LD.LEAVE_ID,LD.LEAVE_DESC";
            sqlQuery += " FROM  HR_LEAVE_DEFINITIONS LD,hr_leave_dept F";
            sqlQuery += " WHERE LD.ENABLED_FLAG = 1";
            sqlQuery += " AND LD.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += "  and F.LEAVE_ID=LD.LEAVE_ID";
            sqlQuery += " and ld.FISCAL_YEAR='" + fisYear + "'";
            sqlQuery += " and f.DEPT_ID='" + deptId + "'";
            sqlQuery += " ORDER BY LD.LEAVE_DESC";

            return sqlQuery;

        }

        public static string GetLeaveBasedDept_frSLD( string deptId)
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT distinct LD.LEAVE_ID,LD.LEAVE_DESC";
            sqlQuery += " FROM  HR_LEAVE_DEFINITIONS LD,hr_leave_dept F";
            sqlQuery += " WHERE LD.ENABLED_FLAG = 1";
            sqlQuery += " AND LD.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += "  and F.LEAVE_ID=LD.LEAVE_ID";
            //sqlQuery += " and ld.FISCAL_YEAR='" + fisYear + "'";
            sqlQuery += " and f.DEPT_ID='" + deptId + "'";
            sqlQuery += " ORDER BY LD.LEAVE_DESC";

            return sqlQuery;

        }

        public static string GetLeaveBasedFiscalYrOrgID(string fisYear)
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT LD.DEPT_LEAVE_ID, LD.LEAVE_ID, HLD.LEAVE_DESC";
            sqlQuery += " FROM  HR_LEAVE_DEPT LD, HR_LEAVE_DEFINITIONS HLD";
            sqlQuery += " WHERE LD.ORG_ID = HLD.ORG_ID";
            sqlQuery += " AND LD.LEAVE_ID = HLD.LEAVE_ID";
            sqlQuery += " AND LD.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND LD.ORG_ID = '" + VMVServices.Web.Utils.OrganizationID + "'";
            if (fisYear != "")
            {
                sqlQuery += " and LD.FISCAL_YEAR='" + fisYear + "'";
            }
            sqlQuery += " ORDER BY HLD.LEAVE_DESC";

            return sqlQuery;

        }
       

    }
}
