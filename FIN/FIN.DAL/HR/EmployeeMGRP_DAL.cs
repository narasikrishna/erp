﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;
namespace FIN.DAL.HR
{
    public class EmployeeMGRP_DAL
    {
        static string sqlQuery = "";
        public static string GetEmpMGRPDtl(string emp_mgrp_id)
        {
            sqlQuery = string.Empty;

            sqlQuery += " select emd.emp_mgrp_id,e.emp_id,e.emp_no,e.emp_first_name,d.dept_name,dd.desig_name,EMD.EMP_MGRP_AMT,'N' AS DELETED";
            sqlQuery += " from hr_emp_mgrp_dtls emd,hr_employees e,hr_departments d,hr_dept_designations dd";
            sqlQuery += " where emd.workflow_completion_status = 1";
            sqlQuery += " and emd.emp_id = e.emp_id";
            sqlQuery += " and emd.attribute1 = d.dept_id";
            sqlQuery += " and emd.attribute2 = dd.dept_desig_id";
            sqlQuery += " and emd.emp_mgrp_id = '" + emp_mgrp_id + "'";
            sqlQuery += " order by emp_mgrp_id ";

            return sqlQuery;
        }


        public static string GetEmpMGRPDtl_asperDept_Desig(string dept_id, string dept_desig_id)
        {
            sqlQuery = string.Empty;

            sqlQuery += " select '0' as emp_mgrp_id,e.emp_id,e.emp_no,e.emp_first_name,d.dept_name,dd.desig_name,'' as EMP_MGRP_AMT,'N' AS DELETED";
            sqlQuery += " from hr_employees e,hr_departments d,hr_dept_designations dd,hR_EMP_WORK_DTLS ewd";
            sqlQuery += " where e.workflow_completion_status = 1";
            sqlQuery += " and ewd.emp_id = e.emp_id";
            sqlQuery += " and ewd.emp_dept_id = d.dept_id";
            sqlQuery += " and ewd.emp_desig_id = dd.dept_desig_id";
            if (dept_id != "")
            {
                sqlQuery += " and d.dept_id = '" + dept_id + "'";
            }
            if (dept_desig_id != "")
            {
                sqlQuery += " and dd.dept_desig_id  ='" + dept_desig_id + "'";
            }
            return sqlQuery;
        }

        public static string GetEmpMGRPDtl_asper_FSYR_Dept_Desig(string emp_mgrp_year, string dept_id, string dept_desig_id)
        {
            sqlQuery = string.Empty;

            sqlQuery += " select emd.emp_mgrp_id,e.emp_id,e.emp_no,e.emp_first_name,d.dept_name,dd.desig_name,EMD.EMP_MGRP_AMT,'N' AS DELETED";
            sqlQuery += " from hr_emp_mgrp_dtls emd,hr_employees e,hr_departments d,hr_dept_designations dd";
            sqlQuery += " where emd.workflow_completion_status = 1";
            sqlQuery += " and emd.emp_id = e.emp_id";
            sqlQuery += " and emd.attribute1 = d.dept_id";
            sqlQuery += " and emd.attribute2 = dd.dept_desig_id";
            sqlQuery += " and emd.emp_mgrp_year = '" + emp_mgrp_year + "'";
            if (dept_id != "")
            {
                sqlQuery += " and d.dept_id = '" + dept_id + "'";
            }
            if (dept_desig_id != "")
            {
                sqlQuery += " and dd.dept_desig_id = '" + dept_desig_id + "'";
            }

            return sqlQuery;
        }



    }
}
