﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.HR
{
    public class TrainingBudgetPlan_DAL
    {
        static string sqlQuery = "";

        public static string GetTrainingBudgetPlan(string Master_id)
        {
            sqlQuery = string.Empty;

            sqlQuery = "  select tb.TRN_BUDGET_PLAN_ID,";
            sqlQuery += " tb.TRN_COURSE_ID as COURSE_ID,";
            sqlQuery += "  tb.TRN_ORGANISER,";
            sqlQuery += "  tb.TRN_QUARTER as quarter_id,";
            sqlQuery += "  tb.TRN_ESTIMATION_CST,";
            sqlQuery += "  tb.TRN_PERIODS,";
            sqlQuery += "  tb.TRN_NO_OF_PEOPLES,";
            sqlQuery += "  tb.TRN_EMPLOYEE_ID as EMP_ID,";
            sqlQuery += "  tb.TRN_COVERED_BY_KFAS,";
            sqlQuery += "  tb.TRN_TRAINING_TYPE as rating_id,";
            sqlQuery += "  (select hc.course_desc";
            sqlQuery += "   from hr_trm_course HC";
            sqlQuery += "  where hc.course_id = tb.trn_course_id) as course_desc,";
            sqlQuery += "  (select ee.emp_no || ' - ' || ee.emp_first_name || ' ' ||";
            sqlQuery += "    ee.emp_middle_name || ' ' || ee.emp_last_name";
            sqlQuery += "  from hr_employees ee";
            sqlQuery += "   where ee.emp_id = tb.trn_employee_id) as emp_name,";
            sqlQuery += "   (select sc.description";
            sqlQuery += "   from ssm_code_masters sc";
            sqlQuery += "   where sc.parent_code = 'TRAIN_TYPE'";
            sqlQuery += "   and sc.code = tb.TRN_TRAINING_TYPE) as TRAIN_TYPE,";
            sqlQuery += "  (select sc.description";
            sqlQuery += "   from ssm_code_masters sc";
            sqlQuery += "   where sc.parent_code = 'IAS'";
            sqlQuery += "  and sc.code = tb.TRN_QUARTER) as Quarter_desc";
            sqlQuery += " from hr_training_budget_plan tb, hr_training_budget_plan_hdr ph";
            sqlQuery += " where tb.TRN_BUDGET_PLAN_HDR_ID = ph.trn_budget_plan_hdr_id";
            sqlQuery += " and tb.trn_budget_plan_hdr_id ='" + Master_id + "'";

            return sqlQuery;


        }
        public static string GetTrainingEmployee(string appId)
        {
            sqlQuery = string.Empty;

            string strLngCode = VMVServices.Web.Utils.LanguageCode;

            sqlQuery += "     select '' as TRN_BUDGET_PLAN_ID,";
            sqlQuery += "    '' as COURSE_ID,";
            sqlQuery += "     '' as TRN_ORGANISER,";
            sqlQuery += "     '' as quarter_id,";
            sqlQuery += "     '' as TRN_ESTIMATION_CST,";
            sqlQuery += "     '' as TRN_PERIODS,";
            sqlQuery += "    '' as TRN_NO_OF_PEOPLES,";
            sqlQuery += "    tb.REV_APPRAISEE_ID as EMP_ID,";
            sqlQuery += "    '' as TRN_COVERED_BY_KFAS,";
            sqlQuery += "    '' as rating_id,";
            sqlQuery += "    '' as course_desc,";
            sqlQuery += "    (select ee.emp_no || ' - ' || ee.emp_first_name" + strLngCode + " || ' ' ||";
            sqlQuery += "      ee.emp_middle_name" + strLngCode + " || ' ' || ee.emp_last_name" + strLngCode + "";
            sqlQuery += "  from hr_employees ee ";
            sqlQuery += "   where ee.emp_id = tb.REV_APPRAISEE_ID) as emp_name,";
            sqlQuery += "    '' as TRAIN_TYPE,";
            sqlQuery += "   '' as Quarter_desc";
            sqlQuery += "  from HR_PER_APP_REVIEW_HDR tb, HR_PER_APPRAISAL_DEF dd,HR_PER_APP_ASSIGNMENT_HDR aa";
            sqlQuery += "   where UPPER(tb.rev_recommendation) = 'TRAINING'     ";
            sqlQuery += "   and tb.workflow_completion_status = 1";
            sqlQuery += "   and tb.enabled_flag = 1";
            sqlQuery += "   and tb.REV_ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += "   and aa.assign_app_id='" + appId + "'";
             sqlQuery += "   and aa.assign_app_id=dd.app_id";
             sqlQuery += "   and aa.assign_hdr_id=tb.assign_hdr_id";
            sqlQuery += "   order by emp_name asc";


            return sqlQuery;


        }

    }
}
