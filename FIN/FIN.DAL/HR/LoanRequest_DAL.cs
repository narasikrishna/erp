﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.HR
{
    public class LoanRequest_DAL
    {

        static string sqlQuery = "";

        public static string getRequestNumber(string emp_ID)
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT HAR.REQ_ID AS REQ_ID ";
            sqlQuery += " FROM HR_ADVANCE_REQ HAR";
            sqlQuery += " WHERE HAR.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND HAR.ENABLED_FLAG = 1";
            sqlQuery += " AND HAR.REQ_EMP_ID = '" + emp_ID + "'";
            sqlQuery += " ORDER BY REQ_ID ";

            return sqlQuery;
        }

        public static string GetEmployeeDetails()
        {
            sqlQuery = string.Empty;
            string strLngCode = VMVServices.Web.Utils.LanguageCode;
            sqlQuery = " select t.emp_id,(t.emp_no||' - '||t.emp_first_name" + strLngCode + "||' '||t.emp_middle_name" + strLngCode + "||' '||t.emp_last_name" + strLngCode + ") emp_name ";
            sqlQuery += " from hr_employees t";
            sqlQuery += " where t.workflow_completion_status=1";
            sqlQuery += " and t.enabled_flag=1";
            sqlQuery += " order by t.emp_first_name asc";

            return sqlQuery;
        }

        public static string GetEmployeeDept_design(string emp_id)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select hd.dept_name" + VMVServices.Web.Utils.LanguageCode + " AS dept_name,hdd.desig_name" + VMVServices.Web.Utils.LanguageCode + " AS desig_name,ewd.emp_dept_id,ewd.emp_desig_id ";
            sqlQuery += " from hr_emp_work_dtls ewd,hr_departments hd, hr_dept_designations hdd";
            sqlQuery += " where ewd.emp_dept_id = hd.dept_id";
            sqlQuery += " and ewd.emp_desig_id = hdd.dept_desig_id ";
            sqlQuery += " and ewd.enabled_flag=1";
            sqlQuery += " and ewd.emp_id = '" + emp_id + "'";

            return sqlQuery;
        }



        public static string GetEmpdays_fromjoining(string emp_id)
        {
            sqlQuery = string.Empty;

            sqlQuery += " select (TRUNC(sysdate)-E.EMP_DOJ) as DAYS_EMP";
            sqlQuery += " from hr_employees e";
            sqlQuery += " where e.emp_id = '" + emp_id + "'";
            return sqlQuery;
        }

        public static string Getlastloanreqdt(string emp_id)
        {
            sqlQuery = string.Empty;

            sqlQuery += " select (trunc(sysdate)- max(ar.req_dt)) as last_loan_req_dt";
            sqlQuery += " from HR_ADVANCE_REQ ar";
            sqlQuery += " where ar.req_emp_id = '" + emp_id + "'";
            sqlQuery += " order by ar.req_dt desc ";
            return sqlQuery;
        }


        public static string Getloanreeligibledt()
        {
            sqlQuery = string.Empty;
            sqlQuery += " SELECT (SO.Loan_Reeligible_Year*365) reeligible_dt";
            sqlQuery += " FROM SSM_SYSTEM_OPTIONS SO";
            sqlQuery += " WHERE ";
            sqlQuery += "  SO.HR_EFFECTIVE_TO_DATE IS  NULL";
            sqlQuery += " AND SO.ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";

            return sqlQuery;
        }

        public static string GetLookUpValues_Loan(string typeKeyId, string loan = null)
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT SCM.CODE AS LOOKUP_ID, SCM.DESCRIPTION" + VMVServices.Web.Utils.LanguageCode + " AS LOOKUP_NAME ";
            sqlQuery += " FROM SSM_CODE_MASTERS SCM ";
            sqlQuery += " WHERE SCM.PARENT_CODE = '" + typeKeyId + "'";
            if (loan == "LOAN")
            {
                sqlQuery += " AND SCM.CODE = 'LOAN'";
            }
            else
            {
                sqlQuery += " AND SCM.CODE NOT IN 'LOAN'";
            }
            sqlQuery += " ORDER BY SCM.CODE  ";

            return sqlQuery;
        }




        public static string GetLoaneligibleindays()
        {
            sqlQuery = string.Empty;

            sqlQuery += " SELECT (SO.LOAN_ELIGIBLE_YEAR*365) loan_eligible";
            sqlQuery += " FROM SSM_SYSTEM_OPTIONS SO";
            sqlQuery += " WHERE ";
            sqlQuery += "  SO.HR_EFFECTIVE_TO_DATE IS  NULL";
            sqlQuery += " AND SO.ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";


            return sqlQuery;
        }
        public static string GetLoanDetails(string loanType, string empid, string loanReqId = "")
        {
            sqlQuery = string.Empty;

            sqlQuery = "   select rr.req_id || ' - ' || rr.req_dt as req_name, rr.req_id, rr.req_amount";
            sqlQuery += "  from HR_ADVANCE_REQ rr";
            sqlQuery += "  where rr.req_org_id ='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " and rr.enabled_flag = '1'";
            sqlQuery += " and rr.workflow_completion_status = '1' and rr.req_emp_id = '" + empid + "' ";
            sqlQuery += " and upper(rr.req_advance_type) = '" + loanType + "'";
            if (loanReqId != string.Empty)
            {
                sqlQuery += " and rr.REQ_ID = '" + loanReqId + "'";
            }

            return sqlQuery;
        }
        public static string GetLoanDetail(string loanType, string empid, string loanReqId = "")
        {
            sqlQuery = string.Empty;

            sqlQuery = "   select rr.req_id || ' - ' || rr.req_dt as req_name, rr.req_id, rr.req_amount";
            sqlQuery += "  from HR_ADVANCE_REQ rr";
            sqlQuery += "  where rr.req_org_id ='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " and rr.enabled_flag = '1'";
            sqlQuery += " and rr.workflow_completion_status = '1' and rr.req_id = '" + empid + "' ";
            sqlQuery += " and upper(rr.req_advance_type) = '" + loanType + "'";
            if (loanReqId != string.Empty)
            {
                sqlQuery += " and rr.REQ_ID = '" + loanReqId + "'";
            }

            return sqlQuery;
        }
        public static string GetBalanceAmt(string loanReqId = "")
        {
            sqlQuery = string.Empty;

            sqlQuery = "   select nvl(sum(pp.repay_amt),0) as repay_amt  from Hr_Advance_Repay_Plan pp where (pp.repay_paidyn='0' or pp.repay_paidyn is null)";

            sqlQuery += "  and pp.RES_ID ='" + loanReqId + "'";
            sqlQuery += " and pp.enabled_flag = '1'";
            sqlQuery += " and pp.workflow_completion_status = '1'";


            return sqlQuery;
        }
        public static string UpdateLoanRepay(string repayDate = "", string reqId = "", string type = "", decimal repayAmt = 0, string repayId = "")
        {
            sqlQuery = string.Empty;
            if (type == "F")
            {
                sqlQuery = "  update HR_ADVANCE_REPAY_PLAN rp set rp.repay_dt=to_date('" + repayDate + "','dd/MM/yyyy'),rp.repay_paidyn='1'";
                sqlQuery += " where rp.RES_ID='" + reqId + "'";
            }
            else
            {
                sqlQuery = "  update HR_ADVANCE_REPAY_PLAN rp set rp.REPAY_AMT='" + repayAmt + "' , rp.repay_dt=to_date('" + repayDate + "','dd/MM/yyyy'),rp.repay_paidyn='1'";
                sqlQuery += " where rp.RES_ID='" + reqId + "'";
                sqlQuery += " and rp.REPAY_ID='" + repayId + "'";
            }

            return sqlQuery;
        }
        public static string GetRepayPlan(string reqId = "")
        {
            sqlQuery = string.Empty;
            sqlQuery = "  select * from HR_ADVANCE_REPAY_PLAN kk where kk.res_id='" + reqId + "'";
            return sqlQuery;
        }

        public static string getEmpLoanRepayDtls()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM vw_hr_emp_loan_repay_plan V WHERE ROWNUM > 0 ";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.EFFECTIVE_FROM_DT] != null)
                {
                    sqlQuery += " AND V.Loan_date between to_date('" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.EFFECTIVE_FROM_DT].ToString() + "','dd/MM/yyyy')";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.EFFECTIVE_TO_DT] != null)
                {
                    sqlQuery += " AND to_date('" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.EFFECTIVE_TO_DT].ToString() + "','dd/MM/yyyy')";
                }
            }
            return sqlQuery;
        }

        public static string getEmpLoanDtls()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM vw_emp_loan_dtls V WHERE ROWNUM > 0 ";
            sqlQuery += " AND V.org_id='" + VMVServices.Web.Utils.OrganizationID + "'";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["EMP_ID"] != null)
                {
                    sqlQuery += " AND V.EMP_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["EMP_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["Dept_id"] != null)
                {
                    sqlQuery += " AND V.emp_dept_id = '" + VMVServices.Web.Utils.ReportViewFilterParameter["Dept_id"].ToString() + "'";
                }
            }
            return sqlQuery;
        }

        public static string GetLoanBalAmount(string empId)
        {
            sqlQuery = string.Empty;
            sqlQuery += "   select nvl(sum(rp.repay_amt), 0) as balance_amount,ar.req_id";
            sqlQuery += "  from HR_ADVANCE_REQ ar, HR_ADVANCE_REPAY_PLAN rp";
            sqlQuery += "  where ar.req_emp_id = '" + empId + "'";
            sqlQuery += "  and rp.res_id = ar.req_id";
            sqlQuery += "  and rp.repay_paidyn='0'";
            sqlQuery += "  group by ar.req_id";
            return sqlQuery;
        }

        public static string GetPropertyName(string propertyTypeID)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select llph.ln_property_id, llph.ln_property_name ";
            sqlQuery += " from ln_loan_property_hdr llph ";
            sqlQuery += " where llph.ln_property_type = '" + propertyTypeID + "'";
            sqlQuery += " and llph.enabled_flag=1";
            sqlQuery += " and llph.workflow_completion_status=1";
            sqlQuery += " order by llph.ln_property_name asc";

            return sqlQuery;
        }

        public static string GetLoanRequestDetails(string lnReqID, string str_Ln_Req_Dtl_id = "")
        {
            sqlQuery = string.Empty;


            sqlQuery = " select llrh.ln_request_id, llrh.ln_property_type, llrh.ln_property_id, llrh.ln_purpose,";
            sqlQuery += " llrd.ln_contract_id, llrd.ln_party_type, llrd.ln_party_id, llrd.ln_contract_desc, llrd.ln_contract_num, llrd.ln_contract_dt,";
            sqlQuery += " llrd.ln_contract_status, to_char(llrd.ln_principal_amount) as ln_principal_amount,llrd.ln_principal_amount as dbl_pr_amt, llrd.ln_currency_id, llrd.ln_roi, llrd.ln_start_date, llrd.ln_end_date,";
            sqlQuery += " scm.description as ln_contract_status_desc,";
            sqlQuery += " case llrh.enabled_flag when '1' then 'TRUE' else 'FALSE' END AS enabled_flag, cb.bank_name, sc.currency_desc, '0' as DELETED ";
            sqlQuery += " ,llrd.ln_contract_type, cm.description as ln_contract_type_desc ";
            sqlQuery += " , llrd.ln_contract_desc_ol"; 
            //, llrd.ln_debit_acct_code, llrd.ln_credit_acct_code, llrd.ln_debit_cost_centre, llrd.ln_credit_cost_centre";
            sqlQuery += " , GAC.ACCT_CODE_ID AS LN_DEBIT_ACTID, AC.ACCT_CODE_ID AS LN_CREDIT_ACTID, GAC.ACCT_CODE as DebitACC, AC.ACCT_CODE as CreditACC";
            sqlQuery += " , GAC.ACCT_CODE ||' - '|| GAC.ACCT_CODE_DESC AS ln_debit_acct_code, AC.ACCT_CODE ||' - '|| AC.ACCT_CODE_DESC AS ln_credit_acct_code ";
            sqlQuery += " ,gsv.segment_value_id as LN_DEBIT_CCID, gsv.segment_value as ln_debit_cost_centre ";
            sqlQuery += " ,sv.segment_value_id as LN_CREDIT_CCID, sv.segment_value as ln_credit_cost_centre ";

            sqlQuery += " , glac.ACCT_CODE_ID AS LN_PRV_DEBIT_ACTID, glc.ACCT_CODE_ID AS LN_PRV_CREDIT_ACTID, glac.ACCT_CODE as PrvDebitACC, glc.ACCT_CODE as PrvCreditACC";
            sqlQuery += " , glac.ACCT_CODE ||' - '|| glac.ACCT_CODE_DESC AS ln_prv_debit_acct_code, glc.ACCT_CODE ||' - '|| glc.ACCT_CODE_DESC AS ln_prv_credit_acct_code ";
            sqlQuery += " ,glsv.segment_value_id as LN_PRV_DEBIT_CCID, glsv.segment_value as ln_prv_debit_cost_centre ";
            sqlQuery += " ,glv.segment_value_id as LN_PRV_CREDIT_CCID, glv.segment_value as ln_prv_credit_cost_centre ";

            sqlQuery += " from LN_LOAN_REQUEST_HDR llrh, LN_LOAN_REQUEST_DTL llrd, ca_bank cb, ssm_currencies sc, SSM_CODE_MASTERS scm, SSM_CODE_MASTERS cm";
            sqlQuery += " ,GL_ACCT_CODES GAC, gl_companies_dtl g, gl_segment_values gsv ,  GL_ACCT_CODES ac, gl_segment_values sv";
            sqlQuery += " ,GL_ACCT_CODES glac,GL_ACCT_CODES glc, gl_segment_values glsv, gl_segment_values glv ";
            sqlQuery += " where llrh.ln_request_id = llrd.ln_request_id";
            sqlQuery += " and llrd.ln_party_id = cb.bank_id";
            sqlQuery += " and llrd.ln_currency_id = sc.currency_id";
            sqlQuery += " and scm.code = llrd.ln_contract_status and scm.parent_code='LRequest'";
            sqlQuery += " and cm.code = llrd.ln_contract_type and cm.parent_code='Contract_Type'";
            sqlQuery += " and gac.acct_struct_id = g.comp_acct_struct_id";
            sqlQuery += " and ac.acct_struct_id = g.comp_acct_struct_id";
            sqlQuery += " and llrd.ln_debit_acct_code = gac.acct_code_id";
            sqlQuery += " and llrd.ln_credit_acct_code = ac.acct_code_id";
            sqlQuery += " and llrd.ln_debit_cost_centre = gsv.segment_value_id";
            sqlQuery += " and llrd.ln_credit_cost_centre = sv.segment_value_id";

            sqlQuery += " and llrd.ln_prv_debit_acct_code = glac.acct_code_id";
            sqlQuery += " and llrd.ln_prv_credit_acct_code = glc.acct_code_id";
            sqlQuery += " and llrd.ln_prv_debit_cost_centre = glsv.segment_value_id";
            sqlQuery += " and llrd.ln_prv_credit_cost_centre = glv.segment_value_id";

            sqlQuery += " and llrh.enabled_flag = 1";
            sqlQuery += " and llrd.workflow_completion_status = 1";
            if (lnReqID.Length > 0)
            {
                sqlQuery += " and llrh.ln_request_id='" + lnReqID + "'";
            }
            if (str_Ln_Req_Dtl_id.Length > 0)
            {
                sqlQuery += " and llrd.ln_contract_id='" + str_Ln_Req_Dtl_id + "'";
            }
            sqlQuery += " order by llrd.ln_contract_num asc";


            return sqlQuery;
        }


        public static string GetLoanDetails4PropertyId(string str_prop_id)
        {
            sqlQuery = string.Empty;


            sqlQuery = " select llrh.ln_request_id, llrh.ln_property_type, llrh.ln_property_id, llrh.ln_purpose,";
            sqlQuery += " llrd.ln_contract_id, llrd.ln_party_type, llrd.ln_party_id, llrd.ln_contract_desc, llrd.ln_contract_num, llrd.ln_contract_dt,";
            sqlQuery += " llrd.ln_contract_status, llrd.ln_principal_amount, llrd.ln_currency_id, llrd.ln_roi, llrd.ln_start_date, llrd.ln_end_date,";
            sqlQuery += " scm.description as ln_contract_status_desc,";
            sqlQuery += " case llrh.enabled_flag when '1' then 'TRUE' else 'FALSE' END AS enabled_flag, cb.bank_name, sc.currency_desc, '0' as DELETED ";
            sqlQuery += " from LN_LOAN_REQUEST_HDR llrh, LN_LOAN_REQUEST_DTL llrd, ca_bank cb, ssm_currencies sc, SSM_CODE_MASTERS scm";
            sqlQuery += " where llrh.ln_request_id = llrd.ln_request_id";
            sqlQuery += " and llrd.ln_party_id = cb.bank_id";
            sqlQuery += " and llrd.ln_currency_id = sc.currency_id";
            sqlQuery += " and scm.code = llrd.ln_contract_status and scm.parent_code='LRequest'";
            sqlQuery += " and llrh.enabled_flag = 1";
            sqlQuery += " and llrd.workflow_completion_status = 1";
            sqlQuery += " and llrh.ln_property_id='" + str_prop_id + "'";

            sqlQuery += " order by llrd.ln_contract_num asc";
            return sqlQuery;
        }

        public static string GetLoanReqDtlsForFacility(string facilityID, string propertyID)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select llrh.ln_request_id, llrh.ln_property_type, llrh.ln_property_id, llrh.ln_purpose,";
            sqlQuery += " llrd.ln_contract_id, llrd.ln_party_type, llrd.ln_party_id, llrd.ln_contract_desc, llrd.ln_contract_num, llrd.ln_contract_dt,";
            sqlQuery += " llrd.ln_contract_status, to_char(llrd.ln_principal_amount) as ln_principal_amount , llrd.ln_principal_amount as dbl_pr_amt, llrd.ln_currency_id, llrd.ln_roi, llrd.ln_start_date, llrd.ln_end_date,";
            sqlQuery += " scm.description as ln_contract_status_desc,";
            sqlQuery += " case llrh.enabled_flag when '1' then 'TRUE' else 'FALSE' END AS enabled_flag, cb.bank_name, sc.currency_desc, '0' as DELETED ";
            sqlQuery += " ,llrd.ln_contract_type, cm.description as ln_contract_type_desc ";
            sqlQuery += " , llrd.ln_contract_desc_ol";
            //, llrd.ln_debit_acct_code, llrd.ln_credit_acct_code, llrd.ln_debit_cost_centre, llrd.ln_credit_cost_centre";
            //sqlQuery += " , GAC.ACCT_CODE_ID AS CODE_ID,GAC.ACCT_CODE ,GAC.ACCT_CODE ||' - '|| GAC.ACCT_CODE_DESC AS CODE_NAME";
            //sqlQuery += " ,gsv.segment_value_id, gsv.segment_value ";
            sqlQuery += " , GAC.ACCT_CODE_ID AS LN_DEBIT_ACTID, AC.ACCT_CODE_ID AS LN_CREDIT_ACTID, GAC.ACCT_CODE as DebitACC, AC.ACCT_CODE as CreditACC";
            sqlQuery += " , GAC.ACCT_CODE ||' - '|| GAC.ACCT_CODE_DESC AS ln_debit_acct_code, AC.ACCT_CODE ||' - '|| AC.ACCT_CODE_DESC AS ln_credit_acct_code ";
            sqlQuery += " ,gsv.segment_value_id as LN_DEBIT_CCID, gsv.segment_value as ln_debit_cost_centre ";
            sqlQuery += " ,sv.segment_value_id as LN_CREDIT_CCID, sv.segment_value as ln_credit_cost_centre ";

            sqlQuery += " , glac.ACCT_CODE_ID AS LN_PRV_DEBIT_ACTID, glc.ACCT_CODE_ID AS LN_PRV_CREDIT_ACTID, glac.ACCT_CODE as PrvDebitACC, glc.ACCT_CODE as PrvCreditACC";
            sqlQuery += " , glac.ACCT_CODE ||' - '|| glac.ACCT_CODE_DESC AS ln_prv_debit_acct_code, glc.ACCT_CODE ||' - '|| glc.ACCT_CODE_DESC AS ln_prv_credit_acct_code ";
            sqlQuery += " ,glsv.segment_value_id as LN_PRV_DEBIT_CCID, glsv.segment_value as ln_prv_debit_cost_centre ";
            sqlQuery += " ,glv.segment_value_id as LN_PRV_CREDIT_CCID, glv.segment_value as ln_prv_credit_cost_centre ";

            sqlQuery += " from LN_LOAN_REQUEST_HDR llrh, LN_LOAN_REQUEST_DTL llrd, ca_bank cb, ssm_currencies sc, SSM_CODE_MASTERS scm, SSM_CODE_MASTERS cm";
            sqlQuery += " ,GL_ACCT_CODES GAC, gl_companies_dtl g, gl_segment_values gsv ,  GL_ACCT_CODES ac, gl_segment_values sv ";
            sqlQuery += " ,GL_ACCT_CODES glac,GL_ACCT_CODES glc, gl_segment_values glsv, gl_segment_values glv ";

            sqlQuery += " where llrh.ln_request_id = llrd.ln_request_id";
            sqlQuery += " and llrd.ln_party_id = cb.bank_id";
            sqlQuery += " and llrd.ln_currency_id = sc.currency_id";
            sqlQuery += " and scm.code = llrd.ln_contract_status and scm.parent_code='LRequest'";
            sqlQuery += " and cm.code = llrd.ln_contract_type and cm.parent_code='Contract_Type'";
            sqlQuery += " and gac.acct_struct_id = g.comp_acct_struct_id";
            sqlQuery += " and ac.acct_struct_id = g.comp_acct_struct_id";
            sqlQuery += " and llrd.ln_debit_acct_code = gac.acct_code_id";
            sqlQuery += " and llrd.ln_credit_acct_code = ac.acct_code_id";
            sqlQuery += " and llrd.ln_debit_cost_centre = gsv.segment_value_id";
            sqlQuery += " and llrd.ln_credit_cost_centre = sv.segment_value_id";

            sqlQuery += " and llrd.ln_prv_debit_acct_code = glac.acct_code_id";
            sqlQuery += " and llrd.ln_prv_credit_acct_code = glc.acct_code_id";
            sqlQuery += " and llrd.ln_prv_debit_cost_centre = glsv.segment_value_id";
            sqlQuery += " and llrd.ln_prv_credit_cost_centre = glv.segment_value_id";

            sqlQuery += " and llrh.enabled_flag = 1";
            sqlQuery += " and llrd.workflow_completion_status = 1";
            sqlQuery += " and llrd.ln_request_id in (select llrh.ln_request_id from LN_LOAN_REQUEST_HDR llrh where llrh.ln_facility_id='" + facilityID + "'";
            sqlQuery += " and llrh.ln_property_id='" + propertyID + "')";
            sqlQuery += " order by llrd.ln_contract_num asc";


            return sqlQuery;
        }


        public static string getLoanContractDetails(string str_Contract_id)
        {
            sqlQuery = string.Empty;
            sqlQuery += " SELECT * FROM LN_LOAN_REQUEST_DTL LRD WHERE LRD.LN_CONTRACT_ID='" + str_Contract_id + "'";
            return sqlQuery;
        }
    }
}
