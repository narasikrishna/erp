﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;

namespace FIN.DAL.HR
{
    public class ResignationCancellation_DAL
    {
        static string sqlQuery = "";

        public static string GetResigNo()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT RR.RESIG_HDR_ID";
            sqlQuery += " FROM HR_RESIG_REQUEST_HDR RR";
            sqlQuery += " WHERE RR.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND RR.ENABLED_FLAG  = 1";
            sqlQuery += " AND not exists (select RC.RESIG_HDR_ID from HR_RESIG_CANCELLATION RC where rc.resig_hdr_id=rr.resig_hdr_id) ";
            sqlQuery += " ORDER BY RR.RESIG_HDR_ID";
            return sqlQuery;

        }

        //Manoj
        public static string GetResigNofromCancel()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT RR.RESIG_HDR_ID";
            sqlQuery += " FROM HR_RESIG_REQUEST_HDR RR";
            sqlQuery += " WHERE RR.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND RR.ENABLED_FLAG  = 1";
            sqlQuery += " AND RR.RESIG_HDR_ID  in (select RC.RESIG_HDR_ID from HR_RESIG_CANCELLATION RC) ";
            sqlQuery += " ORDER BY RR.RESIG_HDR_ID";
            return sqlQuery;

        }


        public static string GetReqDate(string RESIG_HDR_ID)
        {
            sqlQuery = string.Empty;

            sqlQuery = "  SELECT RR.RESIG_REQ_DT";
            sqlQuery += " FROM HR_RESIG_REQUEST_HDR RR";
            sqlQuery += " WHERE RR.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND RR.ENABLED_FLAG  = 1";
            sqlQuery += " AND RR.RESIG_HDR_ID = '" + RESIG_HDR_ID + "'";

            return sqlQuery;
        }

        public static string GetEmpName(string RESIG_HDR_ID)
        {
            sqlQuery = string.Empty;

            sqlQuery = "  select HE.EMP_FIRST_NAME" + VMVServices.Web.Utils.LanguageCode + " AS EMP_FIRST_NAME from HR_RESIG_REQUEST_HDR RRH,HR_EMPLOYEES HE ";
            sqlQuery += " where RRH.RESIG_EMP_ID = HE.EMP_ID ";
            sqlQuery += " and RRH.ENABLED_FLAG = '1' ";
            sqlQuery += " and RRH.WORKFLOW_COMPLETION_STATUS = '1' ";
            sqlQuery += " AND RRH.RESIG_HDR_ID = '" + RESIG_HDR_ID + "'";

            return sqlQuery;
        }

        static string spOutPut = string.Empty;
        public static System.Collections.SortedList ErrorCollection = new System.Collections.SortedList();


        public static string GetSPFOR_DUPLICATE_CHECK(string P_RESIG_CANC_EMP_ID, string P_RECORD_ID)
        {
            try
            {
                string retcode = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "PKG_VALIDATIONS.PROC_ERR_MGR_RESIGN_CANC";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add("@P_MODULE_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_SCREEN_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_RESIG_CANC_EMP_ID", OracleDbType.Char).Value = P_RESIG_CANC_EMP_ID;
                oraCmd.Parameters.Add("@P_RECORD_ID", OracleDbType.Char).Value = P_RECORD_ID;
                oraCmd.Parameters.Add("@P_RET", OracleDbType.Varchar2, 150);
                oraCmd.Parameters["@P_RET"].Direction = ParameterDirection.Output;
                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

                retcode = oraCmd.Parameters["@P_RET"].Value.ToString();

                return retcode;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
