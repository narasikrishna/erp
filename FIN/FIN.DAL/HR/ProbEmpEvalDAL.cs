﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;

namespace FIN.DAL.HR
{
    public class ProbEmpEvalDAL
    {
        static string sqlQuery = "";

        //public static string GetProbationEmpdtls(String EMP_ID)
        //{
        //    sqlQuery = string.Empty;


        //    sqlQuery = " select PED.PROB_DTL_ID,PED.PROB_ID,PED.COM_HDR_ID,PC.COM_DESC,PED.COM_LINE_ID,PD.COM_LEVEL_DESC,";
        //    sqlQuery += " 'N' AS DELETED, 0 as COM_LINE_NUM, CASE PED.ENABLED_FLAG WHEN '1' THEN 'TRUE' ELSE 'FALSE'END as ENABLED_FLAG,0 as RATING";
        //    sqlQuery += " from HR_PROBATION_EMP_HDR  PEH,HR_PROBATION_EMP_DTL  PED,HR_PROBATION_COMP_HDR PC,HR_PROBATION_COMP_DTL PD";
        //    sqlQuery += " where PEH.PROB_ID = PED.PROB_ID";
        //    sqlQuery += " and PED.COM_HDR_ID = PC.COM_HDR_ID ";
        //    sqlQuery += " and PED.COM_LINE_ID = PD.COM_LINE_ID ";
        //    sqlQuery += " AND PEH.ENABLED_FLAG = '1'  ";
        //    sqlQuery += " AND PEH.WORKFLOW_COMPLETION_STATUS = '1' ";
        //    sqlQuery += " and PEH.EMP_ID = '" + EMP_ID + "'";

        //    return sqlQuery;

        //}


        public static string GetProbationEmpDeptdtls(String PROB_LINK_ID)
        {
            sqlQuery = string.Empty;
            sqlQuery += " SELECT PEDD.PROB_LINK_DTL_ID,PEDD.ATTRIBUTE1 AS LINE_NUM_1,PED.PROB_DTL_ID,PH.COM_DESC||' - '||COM_LEVEL_DESC AS COM_DESC,PD.COM_LEVEL_DESC,PEDD.PROB_EVAL_RATING,'N' AS DELETED,";
            sqlQuery += " CASE PEDD.ENABLED_FLAG WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS ENABLED_FLAG,C.CODE AS LOOKUP_ID,C.DESCRIPTION AS LOOKUP_NAME";
            sqlQuery += " FROM HR_PROBATION_EMP_DEPT_DTL PEDD,HR_PROBATION_EMP_DTL ped,HR_PROBATION_COMP_HDR PH,HR_PROBATION_COMP_DTL PD,SSM_CODE_MASTERS C ";
            sqlQuery += " WHERE PEDD.PROB_DTL_ID = PED.PROB_DTL_ID";
            sqlQuery += " AND PH.COM_HDR_ID = PD.COM_HDR_ID ";
            sqlQuery += " AND PEDD.PROB_EVAL_RATING = C.CODE";
            sqlQuery += " AND C.PARENT_CODE = 'RATING'";
            sqlQuery += " and ped.com_line_id = pd.com_line_id";
            sqlQuery += " and ped.com_hdr_id = ph.com_hdr_id";
            sqlQuery += " AND PEDD.PROB_LINK_ID = '" + PROB_LINK_ID + "'";
            sqlQuery += " order by PEDD.PROB_LINK_DTL_ID asc";

            return sqlQuery;

        }


        public static string GetProbationEmpDeptCommentsdtls(String PROB_LINK_ID)
        {
            sqlQuery = string.Empty;
            sqlQuery += " SELECT PEDC.PROB_COMMENT_ID,PEDC.ATTRIBUTE1 AS LINE_NUM_2,PEDC.PROB_COMMENT,'N' AS DELETED";
            sqlQuery += " FROM HR_PROBATION_EMP_DEPT_COMMENTS PEDC";
            sqlQuery += " WHERE PEDC.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND PEDC.PROB_LINK_ID = '" + PROB_LINK_ID + "'";
            return sqlQuery;

        }

        public static string GetProbationEmpImpdtls(String PROB_LINK_ID)
        {
            sqlQuery = string.Empty;
            sqlQuery += " SELECT PEI.PROB_IMP_ID,PEI.PROB_IMP_COMMENTS,'N' AS DELETED";
            sqlQuery += " FROM HR_PROBATION_EMP_IMP PEI";
            sqlQuery += " WHERE PEI.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND PEI.ATTRIBUTE1 = '" + PROB_LINK_ID + "'";
            return sqlQuery;

        }


        public static string GetCompetencyDtl(String emp_id)
        {
            sqlQuery = string.Empty;

            sqlQuery += " SELECT  '0' as PROB_LINK_DTL_ID,ROWNUM as LINE_NUM_1,PED.PROB_DTL_ID,PH.COM_DESC||' - '||COM_LEVEL_DESC AS COM_DESC,PD.COM_LEVEL_DESC,'N' AS DELETED,";
            sqlQuery += " 'TRUE' as ENABLED_FLAG,C.CODE AS LOOKUP_ID,C.DESCRIPTION AS LOOKUP_NAME";
            sqlQuery += " FROM HR_PROBATION_EMP_DTL ped,HR_PROBATION_EMP_HDR peh,HR_PROBATION_COMP_HDR PH,HR_PROBATION_COMP_DTL PD,SSM_CODE_MASTERS C ";
            sqlQuery += " WHERE PH.COM_HDR_ID = PD.COM_HDR_ID ";
            sqlQuery += " and peh.prob_id = ped.prob_id";
            sqlQuery += " AND PED.COMP_RATING = C.CODE";
            sqlQuery += " AND C.PARENT_CODE = 'RATING'";
            sqlQuery += " and ped.com_line_id = pd.com_line_id";
            sqlQuery += " and ped.com_hdr_id = ph.com_hdr_id";
            sqlQuery += " and peh.emp_id = '" + emp_id + "'";
            return sqlQuery;

        }


        public static string GetLeveldesc(String Prob_Dtl_Id)
        {
            sqlQuery = string.Empty;

            sqlQuery += " select pd.COM_LEVEL_DESC ";
            sqlQuery += " from HR_PROBATION_EMP_DTL ped,HR_PROBATION_EMP_HDR peh,HR_PROBATION_COMP_HDR PH,HR_PROBATION_COMP_DTL PD ";
            sqlQuery += " where PH.COM_HDR_ID = PD.COM_HDR_ID ";
            sqlQuery += " and ped.com_line_id = pd.com_line_id";
            sqlQuery += " and peh.prob_id = ped.prob_id";
            sqlQuery += " and ped.com_hdr_id = ph.com_hdr_id  ";
            sqlQuery += " AND PED.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND PED.ENABLED_FLAG = 1";
            sqlQuery += " and Ped.Prob_Dtl_Id = '" + Prob_Dtl_Id + "'";

            return sqlQuery;

        }

        public static string getProbAppraiser4Emp(string str_empid)
        {
            sqlQuery = string.Empty;
            string strLngCode = VMVServices.Web.Utils.LanguageCode;
            sqlQuery += " SELECT EPA.PROB_APPR_ID, (EMP.emp_no||' '||EMP.emp_first_name" + strLngCode+"||' '||EMP.EMP_MIDDLE_NAME"+ strLngCode + "||' '||EMP.emp_last_name" + strLngCode + ") AS EMP_NAME FROM HR_EMP_PROB_APPRAISER EPA ";
            sqlQuery += " INNER JOIN HR_EMP_PROBATION EP ON EP.PROB_ID = EPA.PROB_ID ";
            sqlQuery += " INNER JOIN HR_EMPLOYEES EMP ON EMP.EMP_ID= EPA.EMP_ID ";
            sqlQuery += " WHERE EP.PROB_EMP_ID='" + str_empid + "'";
            sqlQuery += " ORDER BY EPA.LINE_NO ";
            return sqlQuery;
        }


    }
}
