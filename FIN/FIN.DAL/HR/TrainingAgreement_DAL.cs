﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;


namespace FIN.DAL.HR
{
    public class TrainingAgreement_DAL
    {
        static string sqlQuery = "";

        public static string GetTrainingAgreementHdrDtl(string AGR_HDR_ID)
        {
            sqlQuery = string.Empty;

            sqlQuery += "  SELECT AH.AGR_HDR_ID,AH.AGR_LOW_LIMIT,AH.AGR_HIGH_LIMIT,AH.AGR_EFFECTIVE_FROM_DT,AH.AGR_EFFECTIVE_TO_DT,'N' AS DELETED,";
            sqlQuery += "  CASE AH.ENABLED_FLAG WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS ENABLED_FLAG";
            sqlQuery += "  FROM HR_TRM_AGR_HDR AH";
            sqlQuery += "  WHERE AH.AGR_HDR_ID = '" + AGR_HDR_ID + "'";
            sqlQuery += "  AND AH.WORKFLOW_COMPLETION_STATUS =1";
            return sqlQuery;

        }

        public static string GetTrainingAgreementDtlDtl(string AGR_HDR_ID)
        {
            sqlQuery = string.Empty;

            sqlQuery += " select ad.agr_dtl_id,ad.agr_clause,case ad.enabled_flag when '1' then 'TRUE' else 'FALSE' end as enabled_flag,'N' AS DELETED";
            sqlQuery += " from HR_TRM_AGR_DTL ad";
            sqlQuery += " where ad.workflow_completion_status =1 ";
            sqlQuery += " and ad.agr_hdr_id = '" + AGR_HDR_ID + "'";
            return sqlQuery;

        }

    }
}
