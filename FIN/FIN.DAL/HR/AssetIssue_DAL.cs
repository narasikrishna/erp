﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;

namespace FIN.DAL.HR
{
    public class AssetIssue_DAL
    {
        static string sqlQuery = "";

        public static string GetDepartmentDetails()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT d.DEPT_NAME" + VMVServices.Web.Utils.LanguageCode + " AS DEPT_NAME,d.DEPT_ID ";
            sqlQuery += " FROM HR_DEPARTMENTS d ";
            sqlQuery += " WHERE d.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " and d.org_id = '" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " AND d.ENABLED_FLAG = 1 ";
            sqlQuery += " ORDER BY DEPT_NAME asc ";

            return sqlQuery;
        }

        public static string GetDesignationName(string deptID)
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT HDD.DEPT_DESIG_ID AS DESIGN_ID,HDD.DESIG_NAME" + VMVServices.Web.Utils.LanguageCode + " AS DESIG_NAME ";
            sqlQuery += " FROM HR_DEPT_DESIGNATIONS HDD ";
            sqlQuery += " WHERE HDD.ENABLED_FLAG = 1 ";
            sqlQuery += " AND HDD.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " AND HDD.DEPT_ID = '" + deptID + "'";
            sqlQuery += " AND HDD.ORG_ID = '" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " ORDER BY HDD.DESIG_NAME ";

            return sqlQuery;
        }

        public static string GetEmployeeName(string deptID, string DesigID)
        {
            sqlQuery = string.Empty;
            sqlQuery = " select HE.EMP_ID,HE.EMP_NO || ' - ' ||HE.EMP_FIRST_NAME||' '||HE.EMP_MIDDLE_NAME||' ' ||HE.EMP_LAST_NAME" + VMVServices.Web.Utils.LanguageCode + " AS EMP_FIRST_NAME  from HR_EMP_WORK_DTLS EWD,HR_EMPLOYEES HE ";
            sqlQuery += " where HE.EMP_ID = EWD.EMP_ID ";
            sqlQuery += " and EWD.EMP_DEPT_ID = '" + deptID + "'";
            sqlQuery += " and EWD.EMP_DESIG_ID = '" + DesigID + "'";
            sqlQuery += " and HE.ENABLED_FLAG = 1";
            sqlQuery += " AND HE.WORKFLOW_COMPLETION_STATUS = 1 ";
            //sqlQuery += " AND EWD.EFFECTIVE_TO_DT IS NULL ";
            sqlQuery += " AND (EWD.EFFECTIVE_TO_DT IS NULL OR";
            sqlQuery += " (SYSDATE BETWEEN EWD.EFFECTIVE_FROM_DT AND EWD.EFFECTIVE_TO_DT))";
            sqlQuery += " AND HE.EMP_ORG_ID = '" + VMVServices.Web.Utils.OrganizationID + "'";
          //  sqlQuery += " and HE.EMP_INTERNAL_EXTERNAL = 'I'";
            sqlQuery += " ORDER BY HE.EMP_FIRST_NAME ";

            return sqlQuery;
        }

        public static string GetEmployeeName_asper_Applicant(string app_id)
        {
            sqlQuery = string.Empty;

            sqlQuery += " select e.emp_id,e.emp_first_name" + VMVServices.Web.Utils.LanguageCode + " || ' ' || e.emp_middle_name" + VMVServices.Web.Utils.LanguageCode + "  || ' ' || e.emp_last_name" + VMVServices.Web.Utils.LanguageCode + " AS emp_first_name";
            //e.emp_first_name" + VMVServices.Web.Utils.LanguageCode +" AS emp_first_name,IND.INT_DTL_ID";
            sqlQuery += " ,IND.INT_DTL_ID";
            sqlQuery += " from HR_EMPLOYEES e,HR_INTERVIEWS_DTL ind,HR_INTERVIEWS_HDR i";
            sqlQuery += " where e.emp_id = ind.emp_id";
            sqlQuery += " and ind.int_id = i.int_id";
            sqlQuery += " and i.app_id = '" + app_id + "'";
            return sqlQuery;
        }


        public static string GetAssetName()
        {
            sqlQuery = string.Empty;
            sqlQuery = " select AM.ASSET_MST_ID,AM.ASSET_NUMBER||' - '||AM.ASSET_NAME as ASSET_NAME from AST_ASSET_MST AM ";
            sqlQuery += " where AM.ORGANIZATION_MASTER_ID =  '" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " and AM.ENABLED_FLAG = 1";
            sqlQuery += " ORDER BY AM.ASSET_NAME ";

            return sqlQuery;
        }

        public static string GetAssetIssueDtls(string Asset_Mst_id)
        {
            sqlQuery = string.Empty;
            //sqlQuery = " select ASID.ASSET_ID,AM.ASSET_NAME,ASID.ASSET_TYPE as ASSET_TYPE_ID,ASID.ASSET_TYPE,ASID.ASET_NUM,ASID.ASET_LINE_NO,";
            ////  (select SM.DESCRIPTION from SSM_CODE_MASTERS SM where SM.CODE = ASID.ASSET_RETURNABLE and SM.PARENT_CODE = 'AST_TYP') as ASSET_TYPE, ";
            //sqlQuery += " CASE ASID.ASSET_RETURNABLE WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS ASSET_RETURNABLE,ASID.ASSET_ISSUE_DATE,ASID.ASSET_RETURN_DATE,CASE ASID.ENABLED_FLAG WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS ENABLED_FLAG,ASI.ASSET_HDR_ID,ASID.ASSET_DTL_ID,AM.ASSET_MST_ID,'N' as DELETED";
            //sqlQuery += " from HR_ASSET_ISSUE_HDR ASI,HR_ASSET_ISSUE_DTL ASID,AST_ASSET_MST AM";
            //sqlQuery += " where ASI.ASSET_HDR_ID = ASID.ASSET_HDR_ID ";
            //sqlQuery += " and ASID.ASSET_ID = AM.ASSET_MST_ID ";
            //sqlQuery += " and ASI.ASSET_HDR_ID ='" + Asset_Mst_id + "'";

            sqlQuery = " select ASID.ASSET_ID,ASID.ASSET_ID as ASSET_NAME,ASID.ASSET_TYPE as ASSET_TYPE_ID,ASID.ASSET_TYPE,ASID.ASET_NUM,ASID.ASET_LINE_NO,";         
            sqlQuery += " CASE ASID.ASSET_RETURNABLE WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS ASSET_RETURNABLE,ASID.ASSET_ISSUE_DATE,ASID.ASSET_RETURN_DATE,CASE ASID.ENABLED_FLAG WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS ENABLED_FLAG,ASI.ASSET_HDR_ID,ASID.ASSET_DTL_ID,ASID.ASSET_ID as ASSET_MST_ID,'N' as DELETED";
            sqlQuery += " from HR_ASSET_ISSUE_HDR ASI,HR_ASSET_ISSUE_DTL ASID";
            sqlQuery += " where ASI.ASSET_HDR_ID = ASID.ASSET_HDR_ID ";          
            sqlQuery += " and ASI.ASSET_HDR_ID ='" + Asset_Mst_id + "'";
            sqlQuery += " order by ASID.ASSET_DTL_ID asc";


            return sqlQuery;
        }

        public static string CheckDuplicateData(string Dept_id, string Desig_id, string Emp_id)
        {
            sqlQuery = string.Empty;
            sqlQuery = " select count(*) as counts from HR_ASSET_ISSUE_HDR AIH ";
            sqlQuery += " where AIH.EMP_DEPT_ID =   '" + Dept_id + "'";
            sqlQuery += " and AIH.EMP_DESIG_ID = '" + Desig_id + "'";
            sqlQuery += " and AIH.EMP_ID = '" + Emp_id + "'";
            sqlQuery += " and AIH.ENABLED_FLAG = 1";

            return sqlQuery;
        }

        public static string getRPTAssetIssue()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM vw_hr_emp_asset_details V WHERE ROWNUM > 0 ";
            sqlQuery += " AND V.org_id='" + VMVServices.Web.Utils.OrganizationID + "'";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
                {
                    sqlQuery += " AND V.emp_doj >= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy')";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
                {
                    sqlQuery += " AND V.emp_doj <= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["To_Date"].ToString() + "','dd/MM/yyyy')";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["DEPT_ID"] != null)
                {
                    sqlQuery += " AND V.dept_id='" + VMVServices.Web.Utils.ReportViewFilterParameter["DEPT_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["DESIGN_ID"] != null)
                {
                    sqlQuery += " AND V.DEPT_DESIG_ID='" + VMVServices.Web.Utils.ReportViewFilterParameter["DESIGN_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["Emp_id"] != null)
                {
                    sqlQuery += "  and v.Emp_id ='" + VMVServices.Web.Utils.ReportFilterParameter["Emp_id"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["ASSET_ID"] != null)
                {
                    sqlQuery += "  and v.asset_type ='" + VMVServices.Web.Utils.ReportFilterParameter["ASSET_ID"].ToString() + "'";
                }
            }
            return sqlQuery;
        }

        
    }
}
