﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;
using VMVServices.Web;

namespace FIN.DAL.HR
{
    public class LeaveReturn_DAL
    {
        static string sqlQuery = "";
        public static string GetLeaveEncash()
        {
            sqlQuery = string.Empty;

            sqlQuery += " select LE.LC_ID";
            sqlQuery += " from hr_leave_encashment LE";
            sqlQuery += " WHERE LE.LC_PAY_PERIOD_ID IS NOT NULL";
            sqlQuery += " AND LE.WORKFLOW_COMPLETION_STATUS =1";
            sqlQuery += " AND LE.ENABLED_FLAG =1";
            return sqlQuery;

        }

        public static string GetLeaveEncashDtls(String LC_ID)
        {
            sqlQuery = string.Empty;
            sqlQuery += " SELECT LE.LEAVE_DATE_FROM,LE.LEAVE_DATE_TO,LE.NO_OF_DAYS,LE.FISCAL_YEAR,LEAVE_ID";
            sqlQuery += " FROM HR_LEAVE_APPLICATIONS LE";
            sqlQuery += " WHERE LE.LEAVE_REQ_ID ='" + LC_ID + "'";
            sqlQuery += " AND LE.WORKFLOW_COMPLETION_STATUS =1";
            sqlQuery += " AND LE.ENABLED_FLAG =1";


            return sqlQuery;

        }

        public static string GetLeaveReqdtls(string str_emp_id, string mode)
        {
            sqlQuery = string.Empty;
            sqlQuery += "  select LA.LEAVE_REQ_ID,TO_CHAR(LA.LEAVE_DATE_FROM,'dd/MM/yyyy')  || ' - ' || TO_CHAR(LA.LEAVE_DATE_TO,'dd/MM/yyyy')   AS LEAVE_REQ";
            sqlQuery += "  from Hr_Leave_Applications LA,HR_EMPLOYEES E";
            sqlQuery += "  WHERE LA.STAFF_ID = E.EMP_ID";
            sqlQuery += "  AND LA.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += "  AND LA.ENABLED_FLAG = 1";
            sqlQuery += " AND E.EMP_ID='" + str_emp_id + "'";
            if (FINTableConstant.Add == mode)
            {
                sqlQuery += "  AND LA.LEAVE_REQ_ID NOT IN(SELECT LR.LC_ID FROM HR_LEAVE_RETURN LR)";
            }

            sqlQuery += "  AND LA.ORG_ID = '" + VMVServices.Web.Utils.OrganizationID + "'";

            return sqlQuery;
        }


        public static string GetLeaveRegDtls(String lcrRegID)
        {
            sqlQuery = string.Empty;

            sqlQuery = "   select hl.lr_type as LEAVE_ID,hl.attribute1 as fiscal_year,ee.emp_dept_id as dept_id";
            sqlQuery += "   from hR_LEAVE_REGULARIZATION hl,hr_emp_work_dtls ee";
            sqlQuery += "  where hl.lr_emp_id=ee.emp_id";
            sqlQuery += "  and hl.lr_id = '" + lcrRegID + "'";

            return sqlQuery;
        }
    }
}