﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.HR
{
    public class TrainingCost_DAL
    {
        static string sqlQuery = "";
        public static string GetTrainingCostDtls(string Trn_id)
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT TCD.COST_HDR_ID,TCD.COST_DTL_ID,TCD.COVERED_KFAS,TCD.COST_TYPE, SCM.DESCRIPTION" + VMVServices.Web.Utils.LanguageCode + " AS CT_DESC, TCD.COST_ITEM_DESC,TCD.COST_ITEM_REMARK,TCD.COST_QTY,to_char(TCD.COST_UNIT_PRICE) as COST_UNIT_PRICE,";
            sqlQuery += " TCD.COST_CURRENCY,SC.CURRENCY_DESC" + VMVServices.Web.Utils.LanguageCode + " AS CURRENCY_DESC,to_char(TCD.COST_AMOUNT) as COST_AMOUNT,'N' AS DELETED,TCD.ENABLED_FLAG";
            sqlQuery += " FROM HR_TRAINING_COST_DTL TCD";
            sqlQuery += " INNER JOIN SSM_CODE_MASTERS SCM ON SCM.CODE= TCD.COST_TYPE ";
            sqlQuery += " INNER JOIN SSM_CURRENCIES SC ON SC.CURRENCY_ID = TCD.COST_CURRENCY ";
            sqlQuery += " where TCD.COST_HDR_ID = '" + Trn_id + "'";
            sqlQuery += " order by COST_DTL_ID ";

            return sqlQuery;

        }
        public static string GetBudgetName(string TRN_BUDGET_PROG_ID)
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT TB.TRN_BUDGET_SHORT_NAME,TB.TRN_BUDGET_ID ";
            sqlQuery += " FROM HR_TRAINING_BUDGET TB ";
            sqlQuery += " WHERE TB.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " and TB.TRN_BUDGET_PROG_ID ='" + TRN_BUDGET_PROG_ID + "'";
            sqlQuery += " AND TB.ENABLED_FLAG = 1 ";


            return sqlQuery;
        }

        public static string getBudgetDtls(string TRN_BUDGET_ID)
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT TB.TRN_BUDGET_AMOUNT,TB.TRN_EFFECTIVE_FROM_DT,TB.TRN_EFFECTIVE_TO_DT  ";
            sqlQuery += " FROM HR_TRAINING_BUDGET TB";
            sqlQuery += " WHERE TB.ENABLED_FLAG = 1";
            sqlQuery += " AND TB.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND TB.TRN_BUDGET_ID =  '" + TRN_BUDGET_ID + "'";

            return sqlQuery;
        }
    }
}
