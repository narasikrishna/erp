﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.HR
{
    public class AppraisalAssesment_DAL
    {
        static string sqlQuery = "";

        public static string GetAppraisalAssessmentDetails(string Master_id)
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT HPARD.REV_DTL_ID, HPARD.REV_SELF_RATING, HPARD.REV_SELF_COMMENTS, HPARD.REV_REVIEWER_RATING, HPARD.REV_REVIEWER_COMMENTS, 'N' AS DELETED ";
            sqlQuery += " FROM HR_PER_APP_REVIEW_DTL HPARD ";
            sqlQuery += " WHERE HPARD.WORKFLOW_COMPLETION_STATUS = 1 ";
            //sqlQuery += " AND HPARD.ENABLED_FLAG = 1 ";
            sqlQuery += " AND HPARD.REV_ID = '" + Master_id + "'";
            sqlQuery += " ORDER BY HPARD.REV_DTL_ID ";

            return sqlQuery;

        }

        public static string GetAppraisalAssmntDtl2(string Appraisal_id,string apraiser_id,string appraisee_id)
        {
            sqlQuery = string.Empty;


            sqlQuery += " SELECT PARD.REV_DTL_ID as REV_DTL_ID,PARH.REV_ID,PAAD.ASSIGN_DTL_ID,POS.PER_DESC,POS.PER_CATEGORY,POS.PER_TYPE,POS.PER_MIN_RATING,POS.PER_MAX_RATING ";
            sqlQuery += " ,PARD.REV_SELF_RATING AS REV_SELF_RATING,PARD.REV_SELF_COMMENTS AS REV_SELF_COMMENTS,PARD.REV_SELF_APP_DATE AS REV_SELF_APP_DATE ";
            sqlQuery += " ,PARD.REV_REVIEWER_RATING AS REV_REVIEWER_RATING,PARD.REV_REVIEWER_COMMENTS AS REV_REVIEWER_COMMENTS,PARD.REV_DATE AS REV_DATE,'N' AS DELETED";
            sqlQuery += " FROM HR_PER_APP_ASSIGNMENT_DTL  PAAD,HR_PER_OBJECTIVE_SET POS,HR_PER_APP_REVIEW_DTL PARD,HR_PER_APP_REVIEW_HDR PARH";
            sqlQuery += " WHERE POS.PER_OBJ_ID  = PAAD.ASSIGN_PER_OBJ_ID ";
            sqlQuery += " AND PARH.REV_ID = PARD.REV_ID";
            sqlQuery += " AND PARD.ASSIGN_DTL_ID = PAAD.ASSIGN_DTL_ID ";
            sqlQuery += " AND PARH.ASSIGN_HDR_ID = '" + Appraisal_id + "'";
            sqlQuery += " AND PARH.REV_APPRAISER_ID = '" + apraiser_id + "'";
            sqlQuery += " AND PARH.REV_APPRAISEE_ID = '" + appraisee_id + "'";
            return sqlQuery;

        }



        public static string GetKPI4Apprsial(string Master_id, string Appraisal_id)
        {
            sqlQuery = string.Empty;

            sqlQuery += " SELECT PARD.REV_DTL_ID as REV_DTL_ID,PAAD.ASSIGN_DTL_ID,POS.PER_DESC,POS.PER_CATEGORY,POS.PER_TYPE,POS.PER_MIN_RATING,POS.PER_MAX_RATING ";
            sqlQuery += " ,PARD.REV_SELF_RATING AS REV_SELF_RATING,PARD.REV_SELF_COMMENTS AS REV_SELF_COMMENTS,PARD.REV_SELF_APP_DATE AS REV_SELF_APP_DATE ";
            sqlQuery += " ,PARD.REV_REVIEWER_RATING AS REV_REVIEWER_RATING,PARD.REV_REVIEWER_COMMENTS AS REV_REVIEWER_COMMENTS,PARD.REV_DATE AS REV_DATE ";
            sqlQuery += " FROM HR_PER_APP_ASSIGNMENT_DTL  PAAD  ";
            sqlQuery += " INNER JOIN HR_PER_OBJECTIVE_SET POS ON POS.PER_OBJ_ID  = PAAD.ASSIGN_PER_OBJ_ID ";
            sqlQuery += " INNER JOIN HR_PER_APP_REVIEW_DTL PARD ON PARD.ASSIGN_DTL_ID = PAAD.ASSIGN_DTL_ID ";
            sqlQuery += " WHERE PARD.REV_ID='" + Master_id + "'";
            sqlQuery += "  UNION ";
            sqlQuery += " SELECT '0' as REV_DTL_ID,PAAD.ASSIGN_DTL_ID,POS.PER_DESC" + VMVServices.Web.Utils.LanguageCode + " AS PER_DESC,POS.PER_CATEGORY,POS.PER_TYPE,POS.PER_MIN_RATING,POS.PER_MAX_RATING ";
            sqlQuery += " ,NULL AS REV_SELF_RATING,'' AS REV_SELF_COMMENTS,NULL AS REV_SELF_APP_DATE,NULL AS REV_REVIEWER_RATING,'' AS REV_REVIEWER_COMMENTS,NULL AS REV_DATE ";
            sqlQuery += " FROM HR_PER_APP_ASSIGNMENT_DTL  PAAD ";
            sqlQuery += " INNER JOIN HR_PER_OBJECTIVE_SET POS ON POS.PER_OBJ_ID  = PAAD.ASSIGN_PER_OBJ_ID ";
            sqlQuery += " WHERE ASSIGN_HDR_ID='" + Appraisal_id + "'";
            sqlQuery += " AND PAAD.ASSIGN_DTL_ID NOT IN(SELECT ASSIGN_DTL_ID FROM HR_PER_APP_REVIEW_DTL WHERE REV_ID='" + Master_id + "')";
            sqlQuery += " order by REV_DTL_ID ";

            return sqlQuery;
        }


        public static string GetReviewerCmts(string appraiser_id, string Appraisee_id, string rev_appraisal_id, string ra_dept_id)
        {
            sqlQuery = string.Empty;


            sqlQuery += " select distinct j.ra_reviewer_id, ah.rev_appraisee_id,POS.PER_DESC,POS.PER_CATEGORY,POS.PER_TYPE,POS.PER_MIN_RATING,POS.PER_MAX_RATING,";
            sqlQuery += " ad.REV_SELF_RATING AS REV_SELF_RATING,ad.REV_SELF_COMMENTS AS REV_SELF_COMMENTS,ad.REV_SELF_APP_DATE AS REV_SELF_APP_DATE,";
            sqlQuery += " ad.REV_REVIEWER_RATING AS REV_REVIEWER_RATING,ad.REV_REVIEWER_COMMENTS AS REV_REVIEWER_COMMENTS,ad.REV_DATE AS REV_DATE,";
            sqlQuery += " (select e.emp_first_name from hr_employees e where e.emp_id = ah.rev_appraiser_id) as APPR_NAME";
            sqlQuery += " from HR_PER_APP_REVIEW_ASGNMNT_HDR j,HR_PER_APP_REVIEW_HDR ah,HR_PER_APP_REVIEW_DTL ad,";
            sqlQuery += " HR_PER_APP_ASSIGNMENT_DTL  PAAD,HR_PER_OBJECTIVE_SET POS";
            sqlQuery += " where j.rev_levels < (select h.rev_levels from HR_PER_APP_REVIEW_ASGNMNT_HDR h";
            sqlQuery += " where h.ra_reviewer_id = '" + appraiser_id + "'";
            sqlQuery += " and h.rev_levels is not null";
            sqlQuery += " and rownum = 1)";
            sqlQuery += " and j.ra_reviewer_id = ah.rev_appraiser_id";
            sqlQuery += " and ad.rev_id = ah.rev_id";
            sqlQuery += " AND POS.PER_OBJ_ID  = PAAD.ASSIGN_PER_OBJ_ID";
            sqlQuery += " AND   AD.ASSIGN_DTL_ID = PAAD.ASSIGN_DTL_ID";
            sqlQuery += " and paad.assign_hdr_id=ah.assign_hdr_id";
            sqlQuery += " and ah.rev_appraisee_id = '" + Appraisee_id + "'";
            sqlQuery += " and ah.assign_hdr_id = '" + rev_appraisal_id + "'";
            sqlQuery += " and j.ra_dept_id = '" + ra_dept_id + "'";

            return sqlQuery;
        }


        public static string GetRPTAppraisalAssessment()
        {
            sqlQuery = string.Empty;

            sqlQuery = " select * from VW_APPRAISAL_ASSESSMENT aa  where rownum>0 ";
            sqlQuery += " AND aa.org_id='" + VMVServices.Web.Utils.OrganizationID + "'";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["DEPT_ID"] != null)
                {
                    sqlQuery += " and aa.DEPT_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["DEPT_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["RA_HDR_ID"] != null)
                {
                    sqlQuery += " and aa.RA_HDR_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["RA_HDR_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
                {
                    sqlQuery += " AND aa.REV_DATE >= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy')";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
                {
                    sqlQuery += " AND aa.REV_DATE <= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["To_Date"].ToString() + "','dd/MM/yyyy')";
                }
            }
            return sqlQuery;
        }

        public static string GetAppraisalTrainingDetails(string MasterID)
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT '' AS EMP_NAME, HAT.APPRAISAL_TRAINING_ID, HAT.EMP_ID, HAT.COURSE_ID, HAT.COMMENTS,HC.COURSE_DESC, 'N' AS DELETED, HAT.ASSIGN_HDR_ID as ASSIGN_HDR_ID, HAT.REV_TRNING_NEEDED as REV_TRNING_NEEDED ";
            sqlQuery += " FROM hr_appraisal_training HAT,hr_trm_course HC ";
            sqlQuery += " WHERE HAT.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " AND HAT.COURSE_ID = HC.COURSE_ID ";
            sqlQuery += " AND HAT.ENABLED_FLAG = 1 ";
            sqlQuery += " AND HAT.APPRAISAL_TRAINING_ID = '" + MasterID + "'";
            sqlQuery += " ORDER BY HAT.APPRAISAL_TRAINING_ID ";

            return sqlQuery;
        }

        public static string GetAppraisalTrainingDtls(string AssignID)
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT '' APPRAISAL_TRAINING_ID, '' as COURSE_ID, '' COMMENTS, '' AS EMP_NAME, '' as COURSE_DESC, 'N' AS DELETED, '' as EMP_ID";
            sqlQuery += " ,rh.assign_hdr_id, rh.rev_recommendation, rh.rev_trning_needed";
            sqlQuery += " FROM HR_PER_APP_ASSIGNMENT_HDR paad, HR_PER_APP_REVIEW_HDR rh";
            sqlQuery += " where paad.assign_hdr_id=rh.assign_hdr_id";
            sqlQuery += " and rh.rev_recommendation='Training'";
            sqlQuery += " and rh.rev_trning_needed is not null";
            sqlQuery += " and rh.assign_hdr_id='" + AssignID + "'";

            return sqlQuery;
        }

        public static string GetAppAssessmentEmpDetails()
        {
            sqlQuery = string.Empty;
            string strLngCode = VMVServices.Web.Utils.LanguageCode;

            sqlQuery = " select aa.rev_appraisee_id, aa.rev_recommendation, he.emp_id, he.(t.emp_no|| ' - ' ||t.emp_first_name" + strLngCode + "||' '||t.emp_middle_name" + strLngCode + "||' '||t.emp_last_name" + strLngCode + ") emp_name ";
            sqlQuery += " from HR_PER_APP_REVIEW_HDR aa, hr_employees he ";
            sqlQuery += " where rev_recommendation like '%Training%' ";
            sqlQuery += " and aa.rev_appraisee_id=he.emp_id";
            sqlQuery += " and aa.enabled_flag=1";
            sqlQuery += " and aa.workflow_completion_status=1";
            sqlQuery += " and aa.rev_org_id='" + VMVServices.Web.Utils.OrganizationID + "'";


            return sqlQuery;
        }

        public static string GetAppAssessmentCourseDetails()
        {
            sqlQuery = string.Empty;
            
            sqlQuery = " select htc.course_id, htc.course_desc, htc.course_group";
            sqlQuery += " from hr_trm_course htc";
            sqlQuery += " where htc.enabled_flag=1";
            sqlQuery += " and htc.workflow_completion_status=1";
            sqlQuery += " and htc.course_org_id='" + VMVServices.Web.Utils.OrganizationID + "'";

            return sqlQuery;
        }
        
    }
}
