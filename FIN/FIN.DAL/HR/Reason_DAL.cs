﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.HR
{
   public class Reason_DAL
   {
       static string sqlQuery = "";

       



       public static string getReason()
       {
           sqlQuery = string.Empty;

           sqlQuery = " select  r.reason_id,r.reason_desc";
           sqlQuery += " from TM_REASONS r";
           sqlQuery += " where r.workflow_completion_status = 1";
           sqlQuery += " and r.enabled_flag = 1";
           sqlQuery += " order by reason_desc asc";


           return sqlQuery;
       }



   }
}
