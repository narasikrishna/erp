﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;


namespace FIN.DAL.HR
{
    public class Vacation_DAL
    {
        static string sqlQuery = "";

        public static string GetVacationdtls(string vacationId)
        {
            sqlQuery = string.Empty;

            sqlQuery = "select VACATION_ID,";
            sqlQuery += " VACCATION_TYPE,";
            sqlQuery += " VACATION_DESC,VACCATION_TYPE_OL,";
            sqlQuery += "  CASE WITH_PAY_YN WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS WITH_PAY_YN,";
            sqlQuery += "  CASE CARRY_OVER_YN WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS CARRY_OVER_YN,";
            sqlQuery += "  CASE ENABLED_FLAG WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS ENABLED_FLAG,";          
            sqlQuery += " EFFECTIVE_FROM_DT,";
            sqlQuery += "  EFFECTIVE_TO_DT,";
            sqlQuery += "  VAC_ORG_ID, '' as DELETED";
            sqlQuery += " from hr_vacations ";
            sqlQuery += " where workflow_completion_status =1";
            sqlQuery += " AND VACATION_ID = '" + vacationId + "'";

            return sqlQuery;

        }



        static string spOutPut = string.Empty;
        public static System.Collections.SortedList ErrorCollection = new System.Collections.SortedList();


        public static string GetSPFOR_DUPLICATE_CHECK(string P_VACATION_DESC, string P_RECORD_ID)
        {
            try
            {
                string retcode = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "PKG_VALIDATIONS.PROC_ERR_MGR_VACATIONS";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add("@P_MODULE_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_SCREEN_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_VACATION_DESC", OracleDbType.Char).Value = P_VACATION_DESC;
                oraCmd.Parameters.Add("@P_RECORD_ID", OracleDbType.Char).Value = P_RECORD_ID;
                oraCmd.Parameters.Add("@P_RET", OracleDbType.Varchar2, 150);
                oraCmd.Parameters["@P_RET"].Direction = ParameterDirection.Output;
                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

                retcode = oraCmd.Parameters["@P_RET"].Value.ToString();

                return retcode;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    
    }
}
