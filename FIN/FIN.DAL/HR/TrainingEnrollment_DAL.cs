﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;


namespace FIN.DAL.HR
{
    public class TrainingEnrollment_DAL
    {
        static string sqlQuery = "";

        public static string GetTREnrollmentdtls(String enrlDtlId)
        {
            sqlQuery = string.Empty;

            //sqlQuery = "   select e.enrl_dtl_id,e.enrl_hdr_id,e.trn_sch_hdr_id,e.prog_id,e.prog_course_id as course_id  ,e.subject_id,e.trnr_id as emp_id,";
            //sqlQuery += "  sh.trn_desc,p.prog_desc,c.course_desc,s.subject_desc,(he.trnr_name) as emp_name,'N' as deleted";
            //sqlQuery += "  from hr_trm_enroll_dtl e,hr_trm_schedule_hdr sh ,hr_trm_prog_hdr p,hr_trm_course c,hr_trm_subject s,hr_trm_trainer he";
            //sqlQuery += "   where e.trn_sch_hdr_id=sh.trn_sch_hdr_id";
            //sqlQuery += "   and e.prog_id=p.prog_id";
            //sqlQuery += "  and e.prog_course_id=c.course_id";
            //sqlQuery += "  and e.subject_id=s.subject_id";
            //sqlQuery += "  and e.trnr_id=he.trnr_id";
            //sqlQuery += "  and e.workflow_completion_status='1'";            
            //sqlQuery += "  and e.enrl_hdr_id= '" + enrlDtlId + "'";

            sqlQuery += "  SELECT TED.ENRL_DTL_ID,TED.ENRL_HDR_ID,TED.TRN_SCH_HDR_ID,TED.PROG_ID,TED.PROG_COURSE_ID as course_id  ,TED.SUBJECT_ID,TED.TRNR_ID  as EMP_ID ";
            sqlQuery += "  ,TSH.TRN_DESC,TPH.PROG_DESC,TC.COURSE_DESC,TS.SUBJECT_DESC,TT.TRNR_NAME AS EMP_NAME,'N' AS DELETED, '' as trn_sch_dtl_id  ";
            sqlQuery += "  FROM HR_TRM_ENROLL_HDR TEH  ";
            sqlQuery += "  INNER JOIN HR_TRM_ENROLL_DTL TED ON TEH.ENRL_HDR_ID = TED.ENRL_HDR_ID  ";
            sqlQuery += "  INNER JOIN HR_TRM_SCHEDULE_HDR TSH ON TSH.TRN_SCH_HDR_ID = TED.TRN_SCH_HDR_ID  ";
            sqlQuery += "  INNER JOIN HR_TRM_PROG_HDR TPH ON TPH.PROG_ID = TED.PROG_ID  ";
            sqlQuery += "  INNER JOIN HR_TRM_COURSE TC ON TC.COURSE_ID = TED.PROG_COURSE_ID  ";
            sqlQuery += "  INNER JOIN HR_TRM_SUBJECT TS ON TS.SUBJECT_ID = TED.SUBJECT_ID  ";
            sqlQuery += "  INNER JOIN HR_TRM_TRAINER TT ON TT.TRNR_ID = TED.TRNR_ID  ";
            sqlQuery += "  WHERE TEH.ENRL_HDR_ID='" + enrlDtlId + "'";

            return sqlQuery;

        }
        public static string getTrngReqDeptEmpDet(string str_trngreqid)
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT TED.ENRL_DTL_ID,EMP.EMP_ID,EMP.EMP_FIRST_NAME AS EMP_NAME,'TRUE' AS SEL_EMP ";
            sqlQuery += " FROM HR_TRM_ENROLL_DTL TED ";
            sqlQuery += " INNER JOIN HR_EMPLOYEES EMP ON EMP.EMP_ID = TED.EMP_ID ";
            sqlQuery += " WHERE TED.TRN_SCH_DTL_ID='" + str_trngreqid + "'";
            sqlQuery += " UNION ";
            sqlQuery += " SELECT '0' as ENRL_DTL_ID ,EMP.EMP_ID, EMP.EMP_FIRST_NAME AS EMP_NAME,'FALSE' AS SEL_EMP ";
            sqlQuery += " FROM HR_TRM_SCHEDULE_DTL TSD ";
            sqlQuery += " INNER JOIN HR_TRM_REQUEST_DTL TRD ON TSD.REQ_DTL_ID = TRD.REQ_DTL_ID ";
            sqlQuery += " INNER JOIN HR_EMP_WORK_DTLS EWD ON EWD.EMP_DEPT_ID = TRD.REQ_DEPT_ID AND EWD.EFFECTIVE_TO_DT IS NULL ";
            sqlQuery += " INNER JOIN HR_EMPLOYEES EMP ON EMP.EMP_ID= EWD.EMP_ID ";
            sqlQuery += " WHERE TSD.TRN_SCH_DTL_ID='" + str_trngreqid + "'";
            sqlQuery += " AND not exists ( SELECT EMP_ID FROM HR_TRM_ENROLL_DTL TED WHERE ted.emp_id=emp.emp_id and TED.TRN_SCH_DTL_ID='" + str_trngreqid + "')";
            sqlQuery += " order by ENRL_DTL_ID ";
            return sqlQuery;
        }


    }
}
