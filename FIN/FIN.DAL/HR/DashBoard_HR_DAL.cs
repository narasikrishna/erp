﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.HR
{
    public class DashBoard_HR_DAL
    {
        public static string getEmployeeVacancyDet(string str_dept_id)
        {
            string sqlQuery = "";
            string strLngCode = VMVServices.Web.Utils.LanguageCode;


            sqlQuery += " SELECT EMP.EMP_ID, EMP.EMP_NO, ";
            sqlQuery += " EMP.EMP_FIRST_NAME" + strLngCode + " || ' ' || EMP.EMP_MIDDLE_NAME" + strLngCode + "   || ' '  || EMP.EMP_LAST_NAME" + strLngCode + " AS EMP_NAME , ";
            sqlQuery += " DESIG.DEPT_DESIG_ID AS DESIG_ID,DESIG.DESIG_NAME" + strLngCode + " as DESIG_NAME, NVL(DESIG.PARENT_DEPT_DESIG_ID,'HEAD') as PARENT_DESIG,DESIG.NO_OF_EMP FROM HR_EMPLOYEES EMP ";
            sqlQuery += " INNER JOIN HR_EMP_WORK_DTLS EWD ON EMP.EMP_ID= EWD.EMP_ID AND EWD.EFFECTIVE_TO_DT IS NULL ";
            sqlQuery += " INNER JOIN HR_DEPT_DESIGNATIONS DESIG ON DESIG.DEPT_DESIG_ID= EWD.EMP_DESIG_ID ";
            sqlQuery += " WHERE EWD.EMP_DEPT_ID='" + str_dept_id + "'";
            sqlQuery += " AND  EMP.ENABLED_FLAG       =1   AND  EMP.TERMINATION_DATE is NULL ";
            return sqlQuery;
        }

        public static string getOrganizationStruDet()
        {
            string sqlQuery = "";
            sqlQuery += "   SELECT CAT.CATEGORY_ID,CAT.CATEGORY_CODE,CAT.CATEGORY_DESC ";
            sqlQuery += " ,JOBS.JOB_ID,JOBS.JOB_CODE,JOBS.JOB_DESC ";
            sqlQuery += " ,POS.POSITION_ID,POS.POSITION_CODE,POS.POSITION_DESC ";
            sqlQuery += " ,GRAD.GRADE_ID,GRAD.GRADE_CODE,GRAD.GRADE_DESC ";
            sqlQuery += " FROM HR_CATEGORIES CAT ";
            sqlQuery += " INNER JOIN HR_JOBS JOBS ON JOBS.CATEGORY_ID= CAT.CATEGORY_ID ";
            sqlQuery += " INNER JOIN HR_POSITIONS POS ON POS.JOB_ID = JOBS.JOB_ID ";
            sqlQuery += " INNER JOIN HR_GRADES GRAD ON GRAD.POSITION_ID= POS.POSITION_ID ";
            sqlQuery += " WHERE CAT.CATEGORY_ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " AND GRAD.GRADE_ID in (select distinct emp_grade_id from hr_emp_work_dtls) ";
            return sqlQuery;
        }
        public static string getEmployOrgStructure(string str_DeptGroup="", string str_Dept_id="")
        {
            string sqlQuery = "";
            string strLngCode = VMVServices.Web.Utils.LanguageCode;

            if (str_DeptGroup.Length ==0 && str_Dept_id.Length == 0)
            {
                sqlQuery += " SELECT EMP.EMP_ID,   EMP.EMP_NO,  EWD.EMP_DEPT_ID, ";
                sqlQuery += " NVL(EWD.REPORTING_TO,'HEAD') AS REPORTING_TO , ";
                sqlQuery += " EMP.EMP_FIRST_NAME" + strLngCode + "   || ' '  || EMP.EMP_LAST_NAME" + strLngCode + " AS EMP_NAME , ";
                sqlQuery += " DEPT.DEPT_NAME" + strLngCode + "       AS DEPT_NAME, ";
                sqlQuery += " DESG.DESIG_NAME" + strLngCode + "      AS DESIG_NAME  ";
                sqlQuery += " ,(SELECT TO_CHAR(ROUND(EEV.PAY_AMOUNT," + VMVServices.Web.Utils.DecimalPrecision + "))   FROM PAY_EMP_ELEMENT_VALUE EEV  WHERE EEV.PAY_EMP_ID     = EMP.EMP_ID  AND EEV.EFFECTIVE_TO_DT IS NULL  AND EEV.PAY_ELEMENT_ID   =    (SELECT HR_BASIC_ELEMENT_CODE FROM SSM_SYSTEM_OPTIONS    )  ) AS BASIC  ";
                sqlQuery += " ,(SELECT TO_CHAR(ROUND(SUM(NVL(EEV_E.PAY_AMOUNT,0))," + VMVServices.Web.Utils.DecimalPrecision + ")) FROM PAY_EMP_ELEMENT_VALUE EEV_E   WHERE EEV_E.PAY_EMP_ID     = EMP.EMP_ID   AND EEV_E.EFFECTIVE_TO_DT IS NULL  AND EEV_E.PAY_ELEMENT_ID  IN    (SELECT PAY_ELEMENT_ID FROM PAY_ELEMENTS WHERE PAY_ELE_CLASS='Earning'     )  AND EEV_E.PAY_ELEMENT_ID NOT IN    (SELECT HR_BASIC_ELEMENT_CODE FROM SSM_SYSTEM_OPTIONS    )  ) AS ALLOWANCE ";
                sqlQuery += " FROM HR_EMPLOYEES EMP ";
                sqlQuery += " INNER JOIN HR_EMP_WORK_DTLS EWD ON EMP.EMP_ID=EWD.EMP_ID AND EWD.EFFECTIVE_TO_DT IS NULL ";
                sqlQuery += " INNER JOIN HR_DEPARTMENTS DEPT ON DEPT.DEPT_ID=EWD.EMP_DEPT_ID ";
                sqlQuery += " INNER JOIN HR_DEPT_DESIGNATIONS DESG ON DESG.DEPT_DESIG_ID      =EWD.EMP_DESIG_ID ";
                sqlQuery += " WHERE  EMP.ENABLED_FLAG       =1   AND EMP.TERMINATION_DATE IS NULL  ";
            }           
            else if(str_Dept_id.Length > 0)
            {
                sqlQuery += " SELECT EMP_ID,EMP_NO,EMP_DEPT_ID,CASE WHEN EMP_DEPT_ID=REP_TO_DEPT_ID THEN REPORTING_TO ELSE 'HEAD' END AS REPORTING_TO,EMP_NAME,DEPT_NAME,DESIG_NAME,BASIC,ALLOWANCE FROM  ( ";
                sqlQuery += " SELECT EMP.EMP_ID,   EMP.EMP_NO,  EWD.EMP_DEPT_ID, ";
                sqlQuery += "  (SELECT EMP_DEPT_ID FROM HR_EMP_WORK_DTLS S_EWD WHERE S_EWD.EMP_ID= EWD.REPORTING_TO) AS REP_TO_DEPT_ID, ";
                sqlQuery += " NVL(EWD.REPORTING_TO,'HEAD') AS REPORTING_TO , ";
                sqlQuery += " EMP.EMP_FIRST_NAME" + strLngCode + "   || ' '  || EMP.EMP_LAST_NAME" + strLngCode + " AS EMP_NAME , ";
                sqlQuery += " DEPT.DEPT_NAME" + strLngCode + "       AS DEPT_NAME, ";
                sqlQuery += " DESG.DESIG_NAME" + strLngCode + "      AS DESIG_NAME  ";
                sqlQuery += " ,(SELECT TO_CHAR(ROUND(EEV.PAY_AMOUNT,3))   FROM PAY_EMP_ELEMENT_VALUE EEV  WHERE EEV.PAY_EMP_ID     = EMP.EMP_ID  AND EEV.EFFECTIVE_TO_DT IS NULL  AND EEV.PAY_ELEMENT_ID   =    (SELECT HR_BASIC_ELEMENT_CODE FROM SSM_SYSTEM_OPTIONS    )  ) AS BASIC  ";
                sqlQuery += " ,(SELECT TO_CHAR(ROUND(SUM(NVL(EEV_E.PAY_AMOUNT,0)),3)) FROM PAY_EMP_ELEMENT_VALUE EEV_E   WHERE EEV_E.PAY_EMP_ID     = EMP.EMP_ID   AND EEV_E.EFFECTIVE_TO_DT IS NULL  AND EEV_E.PAY_ELEMENT_ID  IN    (SELECT PAY_ELEMENT_ID FROM PAY_ELEMENTS WHERE PAY_ELE_CLASS='Earning'     )  AND EEV_E.PAY_ELEMENT_ID NOT IN    (SELECT HR_BASIC_ELEMENT_CODE FROM SSM_SYSTEM_OPTIONS    )  ) AS ALLOWANCE ";
                sqlQuery += " FROM HR_EMPLOYEES EMP ";
                sqlQuery += " INNER JOIN HR_EMP_WORK_DTLS EWD ON EMP.EMP_ID=EWD.EMP_ID AND EWD.EFFECTIVE_TO_DT IS NULL ";
                sqlQuery += " INNER JOIN HR_DEPARTMENTS DEPT ON DEPT.DEPT_ID=EWD.EMP_DEPT_ID ";
                sqlQuery += " INNER JOIN HR_DEPT_DESIGNATIONS DESG ON DESG.DEPT_DESIG_ID      =EWD.EMP_DESIG_ID ";
                sqlQuery += " WHERE  EMP.ENABLED_FLAG       =1   AND EMP.TERMINATION_DATE IS NULL ";
                sqlQuery += " AND DEPT.DEPT_ID='" + str_Dept_id + "'";
                sqlQuery += " ) Z ";
            }
             else if (str_DeptGroup.Length > 0)
            {

                sqlQuery += " SELECT EMP_ID,EMP_NO,EMP_DEPT_ID,CASE WHEN DEPT_TYPE=REP_TO_DEPT_TYPE THEN REPORTING_TO ELSE 'HEAD' END AS REPORTING_TO,EMP_NAME,DEPT_NAME,DESIG_NAME,BASIC,ALLOWANCE FROM  ( ";
                sqlQuery += " SELECT EMP.EMP_ID,   EMP.EMP_NO,  EWD.EMP_DEPT_ID,DEPT.DEPT_TYPE, ";
                sqlQuery += "  (SELECT EMP_DEPT_ID FROM HR_EMP_WORK_DTLS S_EWD WHERE S_EWD.EMP_ID= EWD.REPORTING_TO) AS REP_TO_DEPT_ID, ";
                sqlQuery += "  (SELECT DEPT.DEPT_TYPE FROM HR_EMP_WORK_DTLS S_EWD,HR_DEPARTMENTS DEPT_S WHERE DEPT_S.DEPT_ID= S_EWD.EMP_DEPT_ID  AND S_EWD.EMP_ID= EWD.REPORTING_TO) AS REP_TO_DEPT_TYPE, ";
                sqlQuery += " NVL(EWD.REPORTING_TO,'HEAD') AS REPORTING_TO , ";
                sqlQuery += " EMP.EMP_FIRST_NAME" + strLngCode + "   || ' '  || EMP.EMP_LAST_NAME" + strLngCode + " AS EMP_NAME , ";
                sqlQuery += " DEPT.DEPT_NAME" + strLngCode + "       AS DEPT_NAME, ";
                sqlQuery += " DESG.DESIG_NAME" + strLngCode + "      AS DESIG_NAME  ";
                sqlQuery += " ,(SELECT TO_CHAR(ROUND(EEV.PAY_AMOUNT,3))   FROM PAY_EMP_ELEMENT_VALUE EEV  WHERE EEV.PAY_EMP_ID     = EMP.EMP_ID  AND EEV.EFFECTIVE_TO_DT IS NULL  AND EEV.PAY_ELEMENT_ID   =    (SELECT HR_BASIC_ELEMENT_CODE FROM SSM_SYSTEM_OPTIONS    )  ) AS BASIC  ";
                sqlQuery += " ,(SELECT TO_CHAR(ROUND(SUM(NVL(EEV_E.PAY_AMOUNT,0)),3)) FROM PAY_EMP_ELEMENT_VALUE EEV_E   WHERE EEV_E.PAY_EMP_ID     = EMP.EMP_ID   AND EEV_E.EFFECTIVE_TO_DT IS NULL  AND EEV_E.PAY_ELEMENT_ID  IN    (SELECT PAY_ELEMENT_ID FROM PAY_ELEMENTS WHERE PAY_ELE_CLASS='Earning'     )  AND EEV_E.PAY_ELEMENT_ID NOT IN    (SELECT HR_BASIC_ELEMENT_CODE FROM SSM_SYSTEM_OPTIONS    )  ) AS ALLOWANCE ";
                sqlQuery += " FROM HR_EMPLOYEES EMP ";
                sqlQuery += " INNER JOIN HR_EMP_WORK_DTLS EWD ON EMP.EMP_ID=EWD.EMP_ID AND EWD.EFFECTIVE_TO_DT IS NULL ";
                sqlQuery += " INNER JOIN HR_DEPARTMENTS DEPT ON DEPT.DEPT_ID=EWD.EMP_DEPT_ID ";
                sqlQuery += " INNER JOIN HR_DEPT_DESIGNATIONS DESG ON DESG.DEPT_DESIG_ID      =EWD.EMP_DESIG_ID ";
                sqlQuery += " WHERE  EMP.ENABLED_FLAG       =1   AND EMP.TERMINATION_DATE IS NULL ";
                sqlQuery += " AND DEPT.DEPT_TYPE='" + str_DeptGroup + "'";
                sqlQuery += " ) Z ";
            }

            return sqlQuery;
        }
    }
}
