﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.HR
{
    public class AttendanceImport_DAL
    {
        static string sqlQuery = "";

        public static string GetTransserialNo(string attribute1)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select et.attribute1";
            sqlQuery += "  from tm_emp_trans et";
            sqlQuery += " where et.attribute1 = '" + attribute1 + "'";

            return sqlQuery;

        }

        public static string GetEmpID(string EMP_NO)
        {
            sqlQuery = string.Empty;

            sqlQuery += " select e.emp_id  from hr_employees e ";
            sqlQuery += " where e.emp_no='" + EMP_NO + "'";
            sqlQuery += " and e.enabled_flag = 1";
            sqlQuery += " and e.workflow_completion_status=1";

            return sqlQuery;

        }


        public static string GetEmpTransDtl(int Month, int Year)
        {
            sqlQuery = string.Empty;

            sqlQuery += " select E.EMP_FIRST_NAME||' ' || E.EMP_MIDDLE_NAME ||' '|| E.EMP_LAST_NAME AS EMP_NAME,ET.TM_DATE,ET.TM_TIME,ET.TM_MODE";
            sqlQuery += " from Tm_Emp_Trans et,HR_EMPLOYEES E";
            sqlQuery += " WHERE ET.TM_EMP_ID=E.EMP_ID";
            sqlQuery += " AND ET.TM_MODE='I'";
            sqlQuery += " AND to_char(et.tm_date,'MM')= " + Month;
            sqlQuery += " AND to_char(et.tm_date,'YYYY') = " + Year;

            return sqlQuery;

        }

        public static string GetDummyEmpTransDtl(DateTime p_Date)
        {
            sqlQuery = string.Empty;


            //sqlQuery += " select distinct E.EMP_FIRST_NAME||' ' || E.EMP_MIDDLE_NAME ||' '|| E.EMP_LAST_NAME AS EMP_NAME,DT.TM_DATE,DT.LATE_IN,";
            //sqlQuery += " CASE SUBSTR(DT.LATE_IN,1,1) WHEN ' - ' THEN 'NO' ELSE 'YES' END AS FLG_LATE,dt.early_out,dt.emp_in_time,dt.emp_out_time";
            //sqlQuery += " from DUMMY_EMP_TRANS dt,HR_EMPLOYEES E";
            //sqlQuery += " where dt.tm_mode='I'";
            //sqlQuery += " and dt.emp_id=e.emp_id";
            //sqlQuery += " and dt.tm_date='" + p_Date.ToString("dd/MMM/yyyy") + "'";
            //sqlQuery += " AND to_char(Dt.Tm_Date,'MM') =" + Month;
            //sqlQuery += " AND to_char(Dt.tm_date,'YYYY')=" + Year;


            //sqlQuery += "  select distinct e.emp_no,E.EMP_FIRST_NAME || ' ' || E.EMP_MIDDLE_NAME || ' ' ||";
            //sqlQuery += "      E.EMP_LAST_NAME AS EMP_NAME,";
            //sqlQuery += "      DT.TM_DATE,";
            //sqlQuery += "   case";
            //sqlQuery += "   when DT.LATE_IN < 0 then";
            //sqlQuery += "   null";
            //sqlQuery += "   else";
            //sqlQuery += "   DT.LATE_IN";
            //sqlQuery += "    end as LATE_IN,";
            ////sqlQuery += "   CASE SUBSTR(DT.LATE_IN, 1, 1)";
            ////sqlQuery += "   WHEN '-' THEN";

            //sqlQuery += "   CASE ";
            //sqlQuery += "   WHEN DT.LATE_IN < 0 THEN";
            //sqlQuery += "   'NO'";
            //sqlQuery += "    ELSE";
            //sqlQuery += "       'YES'";
            //sqlQuery += "    END AS FLG_LATE,";
            //sqlQuery += "   case";
            //sqlQuery += "   when dt.early_out < 0 then";
            //sqlQuery += "  Round(abs(dt.early_out),3)";
            //sqlQuery += "   else";
            //sqlQuery += "   null";
            //sqlQuery += "   end as early_out,";
            ////sqlQuery += "    case";
            ////sqlQuery += "    when dt.early_out < 0 then";
            ////sqlQuery += "   null";
            ////sqlQuery += "   else";
            ////sqlQuery += "    round(dt.early_out,3)";
            ////sqlQuery += "    end as early_out,";
            //sqlQuery += "   to_char(dt.emp_in_time, 'HH24:MI') as emp_in_time,";
            //sqlQuery += "   to_char(dt.emp_out_time, 'HH24:MI') as emp_out_time,";
            //sqlQuery += "   (select substr(p.from_time, 12)";
            //sqlQuery += "    from hr_permissions p";
            //sqlQuery += "   where p.per_emp_id = tt.tm_emp_id";
            //sqlQuery += "     and p.permission_date = tt.tm_date) as per_from_time,";
            //sqlQuery += "   (select substr(p.to_time, 12)";
            //sqlQuery += "    from hr_permissions p";
            //sqlQuery += "   where p.per_emp_id = tt.tm_emp_id";
            //sqlQuery += "    and p.permission_date = tt.tm_date) as per_to_time";
            //sqlQuery += "  from DUMMY_EMP_TRANS dt, HR_EMPLOYEES E, tm_emp_trans tt";
            //sqlQuery += "  where dt.tm_mode = 'I'";
            //sqlQuery += "   and dt.emp_id = e.emp_id";
            //sqlQuery += "  and tt.tm_date = dt.tm_date";
            //sqlQuery += "  and tt.tm_emp_id = dt.emp_id";
            //sqlQuery += "  and dt.tm_date='" + p_Date.ToString("dd/MMM/yyyy") + "'";
            //sqlQuery += " ORDER BY to_number(e.emp_no) ";




            sqlQuery += "    select distinct e.emp_no,";

            sqlQuery += " e.emp_id,";

            sqlQuery += " E.EMP_FIRST_NAME || ' ' || E.EMP_MIDDLE_NAME || ' ' ||";

            sqlQuery += " E.EMP_LAST_NAME AS EMP_NAME,";

            sqlQuery += " DT.TM_DATE,";

            sqlQuery += " decode(nvl(DT.LATE_IN,0),null,0,dt.late_in) LATE_IN,";

            sqlQuery += " decode(nvl(DT.LATE_IN,0),0,'NO','YES') FLG_LATE,";
            sqlQuery += "  DECODE(NVL(dt.emp_in_time, NULL), NULL, 'NO_IN_TIME') AS NO_IN_TIME,";
            sqlQuery += "     DECODE(NVL(dt.emp_OUT_time, NULL), NULL, 'NO_OUT_TIME') AS NO_OUT_TIME,";

            sqlQuery += " decode(nvl(dt.early_out,0),0,null,round(dt.early_out,3)) early_out,";

            sqlQuery += " to_char(dt.emp_in_time, 'HH24:MI AM') as emp_in_time,";

            sqlQuery += " to_char(dt.emp_out_time, 'HH24:MI AM') as emp_out_time,";

            sqlQuery += " (select substr(p.from_time, 12)";

            sqlQuery += " from hr_permissions p";

            sqlQuery += " where p.per_emp_id = tt.tm_emp_id";

            sqlQuery += " and p.permission_date = tt.tm_date) as per_from_time,";

            sqlQuery += " (select substr(p.to_time, 12)";

            sqlQuery += " from hr_permissions p";

            sqlQuery += "   where p.per_emp_id = tt.tm_emp_id";

            sqlQuery += " and p.permission_date = tt.tm_date) as per_to_time";

            sqlQuery += " from DUMMY_EMP_TRANS dt, HR_EMPLOYEES E, tm_emp_trans tt";

            sqlQuery += " where dt.tm_mode = 'I'";

            sqlQuery += " and dt.emp_id = e.emp_id";

            sqlQuery += " and tt.tm_date = dt.tm_date";

            sqlQuery += " and tt.tm_emp_id = dt.emp_id";

            sqlQuery += " and dt.tm_date='" + p_Date.ToString("dd/MMM/yyyy") + "'";

            sqlQuery += " ORDER BY to_number(e.emp_no)";
            return sqlQuery;

        }

        public static string GetEmpLateDeduction(int Month, int Year, string empId = "")
        {
            sqlQuery = string.Empty;

            sqlQuery += "  select e.emp_id,ld.LATE_TM_ID,e.emp_first_name||' ' || E.EMP_MIDDLE_NAME ||' '|| E.EMP_LAST_NAME as emp_first_name,ld.tm_date,ld.tm_emp_group,ld.tm_emp_id,ld.tm_sch_code,ld.tm_late_min, round(ld.tm_late_amt,3) as tm_late_amt,";
            sqlQuery += "     case  ";
            sqlQuery += "      when ld.attribute1 = '0' OR ld.attribute1 is null then";
            sqlQuery += "     'FALSE' ";
            sqlQuery += "      else";
            sqlQuery += "         'TRUE'  end AS attribute1";
            sqlQuery += "  from TM_EMP_LATE_DEDUCTION ld,hr_employees e";
            sqlQuery += "  where ld.tm_emp_id=e.emp_id";
            sqlQuery += "  and to_char(lD.Tm_Date,'MM') =" + Month;
            sqlQuery += "  AND to_char(lD.tm_date,'YYYY')=" + Year;
            if (empId != string.Empty)
            {
                sqlQuery += "  and e.emp_id='" + empId + "'";
            }

            sqlQuery += "    order by ld.tm_date, ld.tm_late_min";
            return sqlQuery;

        }
        public static string GetEmpLateTotalDeduction()
        {
            sqlQuery = string.Empty;

            sqlQuery += "   select e.emp_id,e.emp_no,";
            sqlQuery += "   e.emp_first_name || ' ' || E.EMP_MIDDLE_NAME || ' ' ||";
            sqlQuery += "   E.EMP_LAST_NAME as emp_first_name,       ";
            sqlQuery += "   ld.tm_emp_group,               ";
            sqlQuery += "    ld.tm_sch_code,";
            sqlQuery += "    sum(ld.tm_late_min) as tm_late_min     ";
            sqlQuery += "    from TM_EMP_LATE_DEDUCTION ld, hr_employees e";
            sqlQuery += "   where ld.tm_emp_id = e.emp_id";
            sqlQuery += "   group by  e.emp_no,e.emp_first_name , E.EMP_MIDDLE_NAME , ld.tm_emp_group,e.emp_id,";
            sqlQuery += "     ld.tm_sch_code,";
            sqlQuery += "    E.EMP_LAST_NAME";
            sqlQuery += "   order by to_number(e.emp_no),e.emp_first_name";
            return sqlQuery;

        }

    }
}
