﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;

namespace FIN.DAL.HR
{
    public class Grades_DAL
    {

        static string sqlQuery = "";

        public static string GetGradedtls(String grade_id)
        {
            sqlQuery = string.Empty;


            sqlQuery = " select  g.grade_id,g.grade_code,g.grade_desc,G.GRADE_CODE_OL,G.GRADE_DESC_OL,g.effective_from_date,g.effective_to_date,'N' AS DELETED,";
            sqlQuery += " case g.enabled_flag when '1' THEN 'TRUE' ELSE 'FALSE' END AS enabled_flag";
            sqlQuery += " from hr_grades g";
            sqlQuery += " where g.workflow_completion_status = 1";
            sqlQuery += " and g.grade_id = '" + grade_id + "'";

            return sqlQuery;

        }

        public static string GetGradedtls_asperposition(String position_id)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select  g.grade_id,g.grade_code,g.grade_desc,G.GRADE_CODE_OL,G.GRADE_DESC_OL,g.effective_from_date,g.effective_to_date,'N' AS DELETED,";
            sqlQuery += "  case g.enabled_flag when '1' THEN 'TRUE' ELSE 'FALSE' END AS enabled_flag";
            sqlQuery += " from hr_grades g";
            sqlQuery += " where g.workflow_completion_status = 1";
            sqlQuery += " and g.position_id = '" + position_id + "'";
            return sqlQuery;

        }


        public static string GetCategoryname(String CATEGORY_ID)
        {
            sqlQuery = string.Empty;


            sqlQuery = " select C.CATEGORY_DESC" + VMVServices.Web.Utils.LanguageCode + " as CATEGORY_DESC";
            sqlQuery += " from HR_CATEGORIES C";
            sqlQuery += " WHERE C.ENABLED_FLAG = 1";
            sqlQuery += " AND C.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND C.CATEGORY_ID =  '" + CATEGORY_ID + "'";

            return sqlQuery;

        }

        public static string GetGradeNM()
        {
            sqlQuery = string.Empty;

            sqlQuery = " select G.GRADE_ID,G.GRADE_DESC" + VMVServices.Web.Utils.LanguageCode + " as GRADE_DESC";
            sqlQuery += " from HR_GRADES G";
            sqlQuery += " WHERE G.ENABLED_FLAG = 1";
            sqlQuery += " AND G.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND G.ORG_ID = '" + VMVServices.Web.Utils.OrganizationID + "'";
            return sqlQuery;

        }




        public static string GetChildGrade(String GRADE_ID)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select J.GRADE_ID";
            sqlQuery += " from HR_JOBS J";
            sqlQuery += " WHERE J.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND J.ENABLED_FLAG = 1";
            sqlQuery += " AND J.GRADE_ID = '" + GRADE_ID + "'";
            return sqlQuery;

        }
        //public static string GetChildGrade(String GRADE_ID)
        //{
        //    sqlQuery = string.Empty;



        //    sqlQuery += " select G.GRADE_CODE";
        //    sqlQuery += " from HR_JOBS J,HR_GRADES G";
        //    sqlQuery += " WHERE J.WORKFLOW_COMPLETION_STATUS = 1";
        //    sqlQuery += " AND J.GRADE_ID = G.GRADE_ID";
        //    sqlQuery += " AND J.ENABLED_FLAG = 1";
        //    sqlQuery += " AND J.GRADE_ID = '" + GRADE_ID + "'";

        //    return sqlQuery;

        //}

        public static string GetGradeName()
        {
            sqlQuery = string.Empty;


            sqlQuery = " select DISTINCT G.GRADE_ID, G.GRADE_CODE" + VMVServices.Web.Utils.LanguageCode + "||' - '||C.CATEGORY_CODE" + VMVServices.Web.Utils.LanguageCode + " as GRADE_CODE, G.GRADE_DESC" + VMVServices.Web.Utils.LanguageCode + " as GRADE_DESC ";
            sqlQuery += " from HR_GRADES G,HR_CATEGORIES C";
            sqlQuery += " WHERE G.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND C.CATEGORY_ID = G.CATEGORY_ID ";
            sqlQuery += " AND G.ENABLED_FLAG = 1";
            sqlQuery += " AND G.ORG_ID = '" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " AND (G.EFFECTIVE_TO_DATE IS NULL OR (SYSDATE BETWEEN G.EFFECTIVE_FROM_DATE AND G.EFFECTIVE_TO_DATE)) ";
            sqlQuery += " ORDER BY G.GRADE_DESC";

            return sqlQuery;

        }
        public static string GetGrade(string CATEGORY_ID, string mode)
        {
            sqlQuery = string.Empty;


            sqlQuery = " select  G.GRADE_ID, G.GRADE_CODE" + VMVServices.Web.Utils.LanguageCode + "||' - '||C.CATEGORY_CODE" + VMVServices.Web.Utils.LanguageCode + " as GRADE_CODE, G.GRADE_DESC" + VMVServices.Web.Utils.LanguageCode + " as GRADE_DESC ";
            sqlQuery += " from HR_GRADES G,HR_CATEGORIES C";
            sqlQuery += " WHERE G.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND C.CATEGORY_ID = G.CATEGORY_ID ";
            sqlQuery += " AND C.CATEGORY_ID =  '" + CATEGORY_ID + "'";
            if (mode == FINTableConstant.Add)
            {
                sqlQuery += " AND G.ENABLED_FLAG = 1";
                sqlQuery += " AND G.ORG_ID = '" + VMVServices.Web.Utils.OrganizationID + "'";
                sqlQuery += " AND (G.EFFECTIVE_TO_DATE IS NULL OR (SYSDATE BETWEEN G.EFFECTIVE_FROM_DATE AND G.EFFECTIVE_TO_DATE)) ";
            }
            sqlQuery += " ORDER BY G.GRADE_DESC";

            return sqlQuery;

        }





        static string spOutPut = string.Empty;
        public static System.Collections.SortedList ErrorCollection = new System.Collections.SortedList();


        public static string GetGrade4Position(String Position)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select DISTINCT GRADE_ID,G.GRADE_CODE" + VMVServices.Web.Utils.LanguageCode + " as GRADE_CODE,G.GRADE_DESC" + VMVServices.Web.Utils.LanguageCode + " as GRADE_DESC, G.GRADE_CODE" + VMVServices.Web.Utils.LanguageCode + " ||' - '|| G.GRADE_DESC" + VMVServices.Web.Utils.LanguageCode + "  as GRADE_NAME,G.effective_from_date,G.effective_to_date,";
            sqlQuery += " 'TRUE' AS ENABLED_FLAG";
            sqlQuery += " from HR_GRADES G";
            sqlQuery += " WHERE G.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND G.POSITION_ID = '" + Position + "'";
            return sqlQuery;

        }

        public static string GetSPFOR_ERR_MGR_GRADE(string P_CATEGORY_ID, string P_GRADE_CODE, string P_RECORD_ID)
        {
            try
            {
                string retcode = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "PKG_VALIDATIONS.PROC_ERR_MGR_GRADE";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add("@P_MODULE_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_SCREEN_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_CATEGORY_ID", OracleDbType.Char).Value = P_CATEGORY_ID;
                oraCmd.Parameters.Add("@P_GRADE_CODE", OracleDbType.Char).Value = P_GRADE_CODE;
                oraCmd.Parameters.Add("@P_RECORD_ID", OracleDbType.Char).Value = P_RECORD_ID;
                oraCmd.Parameters.Add("@P_RET", OracleDbType.Varchar2, 150);
                oraCmd.Parameters["@P_RET"].Direction = ParameterDirection.Output;
                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

                retcode = oraCmd.Parameters["@P_RET"].Value.ToString();

                return retcode;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }


}


