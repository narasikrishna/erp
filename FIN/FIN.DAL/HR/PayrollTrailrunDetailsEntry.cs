﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;

using System.Data.EntityClient;



using System.Data.Entity.Infrastructure;
namespace FIN.DAL.HR
{
    public class PayrollTrailrunDetailsEntry
    {
        static string sqlQuery = "";
        public static void RunSPFOR_Trailpayroll(string PayGroupId, string Periodid, string str_deptids,string str_emp_id="")
        {
            try
            {
                string strValue = string.Empty;
                string strNextCode = string.Empty;
                string strErrMsg = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "PKG_PAYROLL.payroll_process";
                oraCmd.CommandType = CommandType.StoredProcedure;
                if (PayGroupId.ToString().Trim().Length == 0)
                {
                    oraCmd.Parameters.Add("@p_pay_group", OracleDbType.Varchar2).Value = DBNull.Value;
                }
                else
                {
                    oraCmd.Parameters.Add("@p_pay_group", OracleDbType.Varchar2).Value = PayGroupId;
                }
                oraCmd.Parameters.Add("@p_pay_period", OracleDbType.Varchar2, 150).Value = Periodid;
                oraCmd.Parameters.Add("@p_dept_list", OracleDbType.Varchar2).Value = str_deptids;
                if (str_emp_id.Trim().Length == 0)
                {
                    oraCmd.Parameters.Add("@p_emp_id", OracleDbType.Varchar2).Value = DBNull.Value;
                }
                else
                {
                    oraCmd.Parameters.Add("@p_emp_id", OracleDbType.Varchar2).Value = str_emp_id;
                }
                oraCmd.Parameters.Add("@p_org_id", OracleDbType.Varchar2).Value = VMVServices.Web.Utils.OrganizationID;
                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public static string GetPayrollID(String payroll_period, string pay_group_id)
        {
            sqlQuery = string.Empty;

            sqlQuery += " select pt.payroll_id";
            sqlQuery += " from PAY_TRIAL_RUN_HDR pt";
            sqlQuery += " where pt.payroll_period = '" + payroll_period + "'";
            sqlQuery += " and pt.pay_group_id = '" + pay_group_id + "'";
            //  sqlQuery += " and pt.workflow_completion_status = 1";

            return sqlQuery;

        }


        public static void RunSPFOR_GL_POSTING_PR(string PayrollID)
        {
            try
            {
                string strValue = string.Empty;
                string strNextCode = string.Empty;
                string strErrMsg = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "GL_POSTING.PAYROLL_POSTING";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add("@P_PAYROLL_ID", OracleDbType.Varchar2).Value = PayrollID;

                // oraCmd.Parameters.Add("@p_org_id", OracleDbType.Varchar2).Value = VMVServices.Web.Utils.OrganizationID;
                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



    }
}
