﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;
namespace FIN.DAL.HR
{
    public class InterviewResult_DAL
    {
        static string sqlQuery = "";

        public static string GetSPFOR_DUPLICATE_CHECK(string P_Result_id, string P_InterviewId)
        {
            try
            {
                string retcode = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "PKG_VALIDATIONS.PROC_ERR_MGR_LEAVE_APPL";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add("@P_Result_id", OracleDbType.NVarchar2).Value = P_Result_id;
                oraCmd.Parameters.Add("@P_InterviewId", OracleDbType.NVarchar2).Value = P_InterviewId;
                oraCmd.Parameters["@P_RET"].Direction = ParameterDirection.Output;
                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

                retcode = oraCmd.Parameters["@P_RET"].Value.ToString();

                return retcode;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string getInterviewAttended(string fromDate, string toDate)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select count(*) as numberOfAttended from hr_interviews_dtl hid ";
            sqlQuery += " where hid.int_date between to_date('" + fromDate + "','dd/MM/yyyy') and to_date('" + toDate + "','dd/MM/yyyy')";
            sqlQuery += " and ENABLED_FLAG = 1";
            sqlQuery += " AND WORKFLOW_COMPLETION_STATUS = 1";

            return sqlQuery;
        }

        public static string getInterviewSelected(string fromDate, string toDate)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select count(*) as numberOfSelect from hr_interviews_results hir ";
            sqlQuery += " right outer join hr_interviews_dtl hid ";
            sqlQuery += " on hid.int_date between to_date('" + fromDate + "','dd/MM/yyyy') and to_date('" + toDate + "','dd/MM/yyyy')";
            sqlQuery += " and hir.int_result='Result1' ";
            sqlQuery += " and hid.int_id  = hir.int_id ";
            sqlQuery += " and hir.ENABLED_FLAG = 1";
            sqlQuery += " AND hir.WORKFLOW_COMPLETION_STATUS = 1";
            return sqlQuery;
        }

        public static string getInterviewRejected(string fromDate, string toDate)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select count(*) as numberOfReject from hr_interviews_results hir ";
            sqlQuery += " right outer join hr_interviews_dtl hid ";
            sqlQuery += " on hid.int_date between to_date('" + fromDate + "','dd/MM/yyyy') and to_date('" + toDate + "','dd/MM/yyyy')";
            sqlQuery += " and hir.int_result='Result2' ";
            sqlQuery += " and hid.int_id  = hir.int_id ";
            sqlQuery += " and hir.ENABLED_FLAG = 1";
            sqlQuery += " AND hir.WORKFLOW_COMPLETION_STATUS = 1";
            return sqlQuery;
        }

        public static string getCriteraEvalDet(string str_appid)
        {
            sqlQuery = string.Empty;
            sqlQuery += " SELECT IH.INT_ID, EMP.EMP_FIRST_NAME,VEC.VAC_CRIT_NAME,VEC.VAC_CRIT_CONDITION,VEC.VAC_CRIT_VALUE,VEC.VAC_EVAL_CONDITION  ";
            sqlQuery += " ,IDC.INT_LEVEL_SCORED ";
            sqlQuery += " ,IDC.INT_LEVEL_SCORED || VEC.VAC_EVAL_CONDITION AS CONDITIONS ";
            sqlQuery += " FROM HR_INTERVIEWS_DTL_CRIT IDC  ";
            sqlQuery += " INNER JOIN HR_VAC_EVAL_CONDITION VEC ON IDC.VAC_EVAL_ID = VEC.VAC_EVAL_ID ";
            sqlQuery += " INNER JOIN HR_INTERVIEWS_DTL HID ON IDC.INT_DTL_ID = HID.INT_DTL_ID ";
            sqlQuery += " INNER JOIN HR_EMPLOYEES EMP ON EMP.EMP_ID= HID.EMP_ID ";
            sqlQuery += " INNER JOIN HR_INTERVIEWS_HDR IH ON IH.INT_ID= HID.INT_ID ";
            sqlQuery += " WHERE IH.APP_ID='" + str_appid +"'";
            return sqlQuery;
        }

        public static string GetInterviewStatus(string str_appid)
        {
            sqlQuery = string.Empty;
            sqlQuery += " select c.EMP_FIRST_NAME as EMP_NAME ,b.Int_Status";
            sqlQuery += " from HR_INTERVIEWS_HDR a, HR_INTERVIEWS_DTL b, HR_EMPLOYEES c";
            sqlQuery += " where a.INT_ID  = b.INT_ID and b.Emp_Id = c.Emp_ID";
            sqlQuery += " and a.APP_ID='" + str_appid + "'";
            return sqlQuery;
        }
    }
}
