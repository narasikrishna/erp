﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;

using System.Data.EntityClient;
using System.Data.Entity.Infrastructure;

namespace FIN.DAL.HR
{
    public class OverTimeSelection_DAL
    {
        static string sqlQuery = "";
        public static void GetSP_OverTime(DateTime tb_Fromdate, DateTime tb_Todate, string Emp_id)
        {
            try
            {
                string strValue = string.Empty;
                string strNextCode = string.Empty;
                string strErrMsg = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "HR_TA.Generate_OverTime";
                oraCmd.CommandType = CommandType.StoredProcedure;
                //oraCmd.Parameters.Add(new OracleParameter("@p_org_id", OracleDbType.Varchar2, 250)).Value = VMVServices.Web.Utils.OrganizationID;
                //oraCmd.Parameters.Add(new OracleParameter("@p_global_segment_id", OracleDbType.Varchar2, 250)).Value = global_segment_id;
                oraCmd.Parameters.Add(new OracleParameter("@p_from_dt", OracleDbType.Date, 250)).Value = tb_Fromdate.ToString("dd/MMM/yyyy");
                oraCmd.Parameters.Add(new OracleParameter("@p_to_dt", OracleDbType.Date, 250)).Value = tb_Todate.ToString("dd/MMM/yyyy");
                oraCmd.Parameters.Add(new OracleParameter("@p_comp_id", OracleDbType.Varchar2, 250)).Value = VMVServices.Web.Utils.OrganizationID;

                oraCmd.Parameters.Add(new OracleParameter("@p_EMP_ID", OracleDbType.Varchar2, 250)).Value = Emp_id;


                DBMethod.ExecuteStoredProcedure(oraCmd);



            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GettmpOverTimedtls(string str, string fromDate, string toDate)
        {
            sqlQuery = string.Empty;
            //sqlQuery += " SELECT D.DEPT_NAME,D.DEPT_ID,DD.DESIG_NAME,DD.DEPT_DESIG_ID,E.EMP_FIRST_NAME,E.EMP_ID, ";
            //sqlQuery += " TT.OVERTIME_DATE,TT.OVERTIME_HOURS,TT.LOGIN_EMP_ID,'N' as DELETED ";

            //sqlQuery += " FROM HR_DEPARTMENTS D,HR_DEPT_DESIGNATIONS DD,HR_EMPLOYEES E,HR_EMP_WORK_DTLS ED,TMP_TM_EMP_OVERTIME TT ";
            //sqlQuery += " WHERE D.DEPT_ID = DD.DEPT_ID ";
            //sqlQuery += " AND ED.EMP_DEPT_ID = D.DEPT_ID ";
            //sqlQuery += " AND E.EMP_ID = ED.EMP_ID ";
            //sqlQuery += "  AND ED.EMP_ID = TT.OVERTIME_EMP_ID ";
            //sqlQuery += " AND TT.LOGIN_EMP_ID = '" + str + "' ";
            //sqlQuery += " and tt.OVERTIME_DATE between to_date('" + fromDate + "','dd/MM/yyyy')  and to_date('" + toDate + "','dd/MM/yyyy') ";



           sqlQuery += "  SELECT tt.OVERTIME_EMP_GROUP,to_char(nvl(tt.OVERTIME_AMOUNT,0)) as OVERTIME_AMOUNT,to_char(nvl(tt.OVERTIME_PAID_AMOUNT,0)) as OVERTIME_PAID_AMOUNT,tt.overtime_id,D.DEPT_NAME,D.DEPT_ID,DD.DESIG_NAME, DD.DEPT_DESIG_ID,  E.EMP_ID,TT.OVERTIME_DATE,  TT.OVERTIME_HOURS, TT.OVERTIME_EMP_ID,        ";
       
            sqlQuery += "    e.emp_no,       E.EMP_FIRST_NAME||' '||e.emp_middle_name||' '||e.emp_last_name as EMP_FIRST_NAME,";
            sqlQuery += "  case when tt.overtime_paid=1 then";
            sqlQuery += "   'TRUE' ELSE 'FALSE' END AS OVERTIME_PAID";
            sqlQuery += "   FROM HR_DEPARTMENTS       D, ";
            sqlQuery += "     HR_DEPT_DESIGNATIONS DD, ";
            sqlQuery += "    HR_EMPLOYEES         E, ";
            sqlQuery += "    HR_EMP_WORK_DTLS     ED, ";
            sqlQuery += "    TM_EMP_OVERTIME  TT ";
            sqlQuery += "   WHERE D.DEPT_ID = DD.DEPT_ID ";
            sqlQuery += "   AND ED.EMP_DEPT_ID = D.DEPT_ID ";
            sqlQuery += "   AND E.EMP_ID = ED.EMP_ID ";
            sqlQuery += "   AND ED.EMP_DESIG_ID=DD.DEPT_DESIG_ID ";
            sqlQuery += "   AND E.EMP_ID = TT.OVERTIME_EMP_ID ";
            sqlQuery += "   AND TT.OVERTIME_DEPT_ID=D.DEPT_ID and TT.OVERTIME_HOURS<>0 ";
            sqlQuery += "   AND TT.OVERTIME_EMP_ID = '" + str + "' ";
            sqlQuery += "  and tt.OVERTIME_DATE =to_date('" + fromDate + "','dd/MM/yyyy') order by TT.OVERTIME_DATE,       TT.OVERTIME_HOURS";
            // sqlQuery += "  and tt.OVERTIME_DATE between to_date('" + fromDate + "','dd/MM/yyyy')  and to_date('" + toDate + "','dd/MM/yyyy') ";



            return sqlQuery;

        }
        public static string GettmpOverTimeParentdtls(string str, string fromDate, string toDate)
        {
            sqlQuery = string.Empty;
            sqlQuery += "     SELECT tt.OVERTIME_EMP_GROUP,    ";
            sqlQuery += "    D.DEPT_NAME,";
            sqlQuery += "    D.DEPT_ID,";
            sqlQuery += "    DD.DESIG_NAME,";
            sqlQuery += "    DD.DEPT_DESIG_ID,";
            sqlQuery += "     e.emp_no,       E.EMP_FIRST_NAME||' '||e.emp_middle_name||' '||e.emp_last_name as EMP_FIRST_NAME,";
            sqlQuery += "    E.EMP_ID,     ";
            sqlQuery += "     round(nvl(sum(TT.OVERTIME_HOURS),0)/60,3) as OVERTIME_HOURS,";
            sqlQuery += "    TT.OVERTIME_EMP_ID";     
            sqlQuery += "    FROM HR_DEPARTMENTS       D,";
            sqlQuery += "    HR_DEPT_DESIGNATIONS DD,";
            sqlQuery += "    HR_EMPLOYEES         E,";
            sqlQuery += "    HR_EMP_WORK_DTLS     ED,";
            sqlQuery += "     TM_EMP_OVERTIME      TT";
            sqlQuery += "   WHERE D.DEPT_ID = DD.DEPT_ID";
            sqlQuery += "    AND ED.EMP_DEPT_ID = D.DEPT_ID";
            sqlQuery += "    AND E.EMP_ID = ED.EMP_ID";
            sqlQuery += "    AND ED.EMP_DESIG_ID = DD.DEPT_DESIG_ID";
            sqlQuery += "   AND E.EMP_ID = TT.OVERTIME_EMP_ID";
            sqlQuery += "     AND TT.OVERTIME_DEPT_ID = D.DEPT_ID";
            //sqlQuery += "   AND TT.OVERTIME_EMP_ID = '" + str + "' ";
            sqlQuery += "  and tt.OVERTIME_DATE >=to_date('" + fromDate + "','dd/MM/yyyy') ";
            sqlQuery += "  and tt.OVERTIME_DATE <=to_date('" + toDate + "','dd/MM/yyyy') ";
             
            sqlQuery += "    group by tt.OVERTIME_EMP_GROUP,e.emp_middle_name,e.emp_last_name,e.emp_no,  ";
            sqlQuery += "    D.DEPT_NAME,";
            sqlQuery += "    D.DEPT_ID,";
            sqlQuery += "    DD.DESIG_NAME,";
            sqlQuery += "    DD.DEPT_DESIG_ID,";
            sqlQuery += "    E.EMP_FIRST_NAME,";
            sqlQuery += "    E.EMP_ID,";
            sqlQuery += "    TT.OVERTIME_EMP_ID order by D.DEPT_NAME, DD.DESIG_NAME,to_number(e.emp_no)";

            return sqlQuery;

        }
        public static string GetEmpDeptData(string str, string fromDate, string toDate)
        {
            sqlQuery = string.Empty;
            sqlQuery += "     SELECT D.DEPT_NAME,";
            sqlQuery += "    D.DEPT_ID,";
            sqlQuery += "    DD.DESIG_NAME,";
            sqlQuery += "      DD.DEPT_DESIG_ID,";
            sqlQuery += "      E.EMP_FIRST_NAME,";
            sqlQuery += "      E.EMP_ID      ";
            sqlQuery += "   FROM HR_DEPARTMENTS         D,";
            sqlQuery += "       HR_DEPT_DESIGNATIONS   DD,";
            sqlQuery += "       HR_EMPLOYEES           E,  ";
            sqlQuery += "       HR_EMP_WORK_DTLS       ED ";
            sqlQuery += "   WHERE D.DEPT_ID = DD.DEPT_ID";
            sqlQuery += "   AND ED.EMP_DEPT_ID = D.DEPT_ID";
            sqlQuery += "   AND E.EMP_ID = ED.EMP_ID";
            sqlQuery += "   AND ED.EMP_DESIG_ID = DD.DEPT_DESIG_ID";
            return sqlQuery;

        }
        public static string GetOverTimeDtls(string fromDate, string toDate)
        {
            sqlQuery = string.Empty;

            sqlQuery += " SELECT DISTINCT E.EMP_ID,E.EMP_FIRST_NAME,TEO.OT_TYPE,TEO.OVERTIME_HOURS,TEO.OVERTIME_DATE,TEO.Overtime_Paid, 'N' as DELETED";
            sqlQuery += " ,(select pev.pay_amount from ssm_system_options sso, pay_emp_element_value pev";
            sqlQuery += " where sso.hr_basic_element_code=pev.pay_element_id and sso.hr_effective_from_date between to_date('" + fromDate + "','dd/MM/yyyy') and to_date('" + fromDate + "','dd/MM/yyyy')";
            sqlQuery += " and sso.hr_effective_to_date between to_date('" + fromDate + "','dd/MM/yyyy') and to_date('" + fromDate + "','dd/MM/yyyy')";
            sqlQuery += " ) as basic_salary";
            sqlQuery += " FROM HR_DEPARTMENTS D,HR_DEPT_DESIGNATIONS DD,HR_EMPLOYEES E,HR_EMP_WORK_DTLS ED,TM_EMP_OVERTIME TEO";
            sqlQuery += " WHERE D.DEPT_ID = DD.DEPT_ID ";
            sqlQuery += " AND E.EMP_ID = ED.EMP_ID ";
            sqlQuery += " AND ED.EMP_DEPT_ID = D.DEPT_ID ";
            sqlQuery += " AND ED.EMP_ID = TEO.OVERTIME_EMP_ID";

            //sqlQuery += " and t.emp_doj between to_date('" + fromDate + "','dd/MM/yyyy')  and to_date('" + toDate + "','dd/MM/yyyy') ";

            return sqlQuery;

        }
        public static string GetEMPGroupCode()
        {
            sqlQuery = string.Empty;

            sqlQuery += "   SELECT D.DEPT_NAME,";
            sqlQuery += "    D.DEPT_ID,";
            sqlQuery += "    DD.DESIG_NAME,";
            sqlQuery += "    DD.DEPT_DESIG_ID,";
            sqlQuery += "    E.EMP_FIRST_NAME,";
            sqlQuery += "    E.EMP_ID,tt.tm_grp_code";
            sqlQuery += "    FROM HR_DEPARTMENTS       D,";
            sqlQuery += "    HR_DEPT_DESIGNATIONS DD,";
            sqlQuery += "    HR_EMPLOYEES         E,";
            sqlQuery += "   HR_EMP_WORK_DTLS     ED,";
            sqlQuery += "   TM_DEPT_SCH  TT";
            sqlQuery += "   WHERE D.DEPT_ID = DD.DEPT_ID";
            sqlQuery += "     AND ED.EMP_DEPT_ID = D.DEPT_ID";
            sqlQuery += "   AND E.EMP_ID = ED.EMP_ID";
            sqlQuery += "   AND ED.EMP_DESIG_ID=DD.DEPT_DESIG_ID";
            sqlQuery += "    AND d.dept_id =tt.tm_dept_id";

            return sqlQuery;

        }
        public static string GetEMPGroupDatefromDummy()
        {
            sqlQuery = string.Empty;

            sqlQuery += "     SELECT D.DEPT_NAME,de.tm_date,";
            sqlQuery += "     D.DEPT_ID,";
            sqlQuery += "     DD.DESIG_NAME,";
            sqlQuery += "     DD.DEPT_DESIG_ID,";
            sqlQuery += "      E.EMP_FIRST_NAME,";
            sqlQuery += "       E.EMP_ID,";
            sqlQuery += "    tt.tm_grp_code";
            sqlQuery += "   FROM HR_DEPARTMENTS       D,";
            sqlQuery += "     HR_DEPT_DESIGNATIONS DD,";
            sqlQuery += "     HR_EMPLOYEES         E,";
            sqlQuery += "    HR_EMP_WORK_DTLS     ED,";
            sqlQuery += "    TM_DEPT_SCH          TT,";
            sqlQuery += "    dummy_emp_trans      de";
            sqlQuery += "   WHERE D.DEPT_ID = DD.DEPT_ID";
            sqlQuery += "    AND ED.EMP_DEPT_ID = D.DEPT_ID";
            sqlQuery += "   AND E.EMP_ID = ED.EMP_ID";
            sqlQuery += "   AND ED.EMP_DESIG_ID = DD.DEPT_DESIG_ID";
            sqlQuery += "   AND d.dept_id = tt.tm_dept_id";
            sqlQuery += "   and de.emp_id = ed.emp_id";
            return sqlQuery;

        }
        public static string getEmpOvertime()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_EMP_OVERTIME V WHERE ROWNUM > 0 ";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
                {
                    sqlQuery += " AND V.OVERTIME_DATE between to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy')";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
                {
                    sqlQuery += " AND to_date('" + VMVServices.Web.Utils.ReportFilterParameter["To_Date"].ToString() + "','dd/MM/yyyy')";
                }
            }

            return sqlQuery;
        }

        public static DataSet GetSP_OverTime(string group_code, string emp_id, string From_Date, string str_Query)
        {
            try
            {
                string strValue = string.Empty;
                string strNextCode = string.Empty;
                string strErrMsg = string.Empty;

                DateTime dt_tmp = Convert.ToDateTime(From_Date);

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "HR_PKG.PROC_EMP_OVERTIME_PAYMENT_CALC";
                oraCmd.CommandType = CommandType.StoredProcedure;
                //oraCmd.Parameters.Add(new OracleParameter("@P_GRP_CODE", OracleDbType.Varchar2, 250)).Value = VMVServices.Web.Utils.OrganizationID;
                oraCmd.Parameters.Add(new OracleParameter("@P_GRP_CODE", OracleDbType.Varchar2, 250)).Value = group_code;
                oraCmd.Parameters.Add(new OracleParameter("@P_EMP", OracleDbType.Varchar2, 250)).Value = emp_id;
                oraCmd.Parameters.Add(new OracleParameter("@P_DATE", OracleDbType.Date, 250)).Value = dt_tmp.ToString("dd/MMM/yyyy");
                // oraCmd.Parameters.Add("@V_EMP_OVRTM_HR_MNS", OracleDbType.Decimal).Direction = ParameterDirection.Output;


                return DBMethod.ExecuteStoredProcedure(oraCmd, str_Query);



            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string getReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM vw_hr_emp_overtime V WHERE ROWNUM > 0 ";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["Group_code"] != null)
                {
                    sqlQuery += " AND V.overtime_emp_group = '" + VMVServices.Web.Utils.ReportViewFilterParameter["Group_code"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["Dept_Id"] != null)
                {
                    sqlQuery += " AND V.overtime_dept_id = '" + VMVServices.Web.Utils.ReportViewFilterParameter["Dept_Id"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["Emp_id"] != null)
                {
                    sqlQuery += " AND V.emp_id = '" + VMVServices.Web.Utils.ReportViewFilterParameter["Emp_id"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
                {
                    sqlQuery += " AND V.overtime_date >= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy')";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
                {
                    sqlQuery += " AND V.overtime_date <= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["To_Date"].ToString() + "','dd/MM/yyyy')";
                }

            }


            return sqlQuery;
        }
        public static string getRPTDailyInOutReport()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM vw_hr_daily_in_out V WHERE ROWNUM > 0 ";


            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
                {
                    sqlQuery += " AND V.TM_DATE >= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy')";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
                {
                    sqlQuery += " AND V.TM_DATE <= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["To_Date"].ToString() + "','dd/MM/yyyy')";
                }

                if (VMVServices.Web.Utils.ReportViewFilterParameter["Emp_id"] != null)
                {
                    sqlQuery += "  and V.EMP_ID ='" + VMVServices.Web.Utils.ReportFilterParameter["Emp_id"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["Dept_id"] != null)
                {
                    sqlQuery += "  and V.emp_dept_id ='" + VMVServices.Web.Utils.ReportFilterParameter["Dept_id"].ToString() + "'";
                }

            }
            return sqlQuery;
        }
    }
}
