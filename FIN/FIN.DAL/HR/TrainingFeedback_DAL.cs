﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;


namespace FIN.DAL.HR
{
    public class TrainingFeedback_DAL
    {
        static string sqlQuery = "";

        public static string get_PositionGradeEmpReport()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_POSITIONWISE_EMP V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["POSITION_ID"] != null)
                {
                    sqlQuery += " AND V.POSITION_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["POSITION_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["GRADE_ID"] != null)
                {
                    sqlQuery += " AND V.GRADE_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["GRADE_ID"].ToString() + "'";
                }

            }
            return sqlQuery;
        }
        public static string get_NationalityCountReport()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_NATIONALITY_COUNT V WHERE ROWNUM > 0 ";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["NATIONALITY"] != null)
                {
                    sqlQuery += " AND V.NATIONALITY = '" + VMVServices.Web.Utils.ReportViewFilterParameter["NATIONALITY"].ToString() + "'";
                }
            }

            //if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            //{
            //    if (VMVServices.Web.Utils.ReportViewFilterParameter["EMP_ID"] != null)
            //    {
            //        sqlQuery += " AND V.EMP_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["EMP_ID"].ToString() + "'";
            //    }

            //}
            return sqlQuery;
        }

        public static string fn_GetTrnFeedbackDtls(String FB_HDR_ID)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select fd.fb_dtl_id, fd.fb_hdr_id, fd.fb_desc, fd.fb_comments,c.code as lookup_id,c.description as lookup_name ";
            sqlQuery += " from hr_trm_feedback_dtl fd,ssm_code_masters c";
            sqlQuery += " where fb_hdr_id = '" + FB_HDR_ID + "'";
            sqlQuery += " and fd.training_type = c.code";
            sqlQuery += " and c.parent_code='TRAINING_TYPE'";
            sqlQuery += " AND fd.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND fd.ENABLED_FLAG = 1";

            return sqlQuery;

        }

        public static string GetSchedule()
        {
            sqlQuery = string.Empty;

            sqlQuery = " select tsh.trn_sch_hdr_id, tsh.trn_desc  ";
            sqlQuery += " from hr_trm_schedule_hdr tsh ";

            sqlQuery += " where TSH.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " and TSH.ENABLED_FLAG = 1";

            return sqlQuery;
        }

        public static string GetProgram(String TRN_SCH_HDR_ID)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select prog_id, prog_desc from hr_trm_prog_hdr ";
            sqlQuery += " where prog_id =(select distinct tsd.prog_id from hr_trm_schedule_hdr TSH, hr_trm_schedule_dtl tsd where ";
            sqlQuery += " tsh.trn_sch_hdr_id = tsd.trn_sch_hdr_id ";
            sqlQuery += " and tsh.trn_sch_hdr_id = '" + TRN_SCH_HDR_ID + "')";
            sqlQuery += " and WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " and ENABLED_FLAG = 1";

            return sqlQuery;
        }

        public static string GetEmployeeDetails()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT HE.EMP_ID AS EMP_ID, HE.EMP_NO ||' - '|| HE.EMP_FIRST_NAME" + VMVServices.Web.Utils.LanguageCode + " AS EMP_NAME  ";
            sqlQuery += " FROM HR_EMPLOYEES HE ";
            sqlQuery += " WHERE HE.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " AND HE.EMP_ORG_ID = '" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " AND HE.ENABLED_FLAG = 1 ";
            sqlQuery += " ORDER BY EMP_FIRST_NAME asc ";

            return sqlQuery;
        }

        public static string GetProgramLearnAnalysis()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_HR_TRAINING_FEEDBACK V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["PROG_ID"] != null)
                {
                    sqlQuery += " AND V.PROG_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["PROG_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["EMP_ID"] != null)
                {
                    sqlQuery += " AND V.EMP_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["EMP_ID"].ToString() + "'";
                }
                //sqlQuery += " FROM HR_EMPLOYEES HE ";
                //sqlQuery += " WHERE HE.WORKFLOW_COMPLETION_STATUS = 1 ";
                //sqlQuery += " AND HE.EMP_ORG_ID = '" + VMVServices.Web.Utils.OrganizationID + "'";
                //sqlQuery += " AND HE.ENABLED_FLAG = 1 ";
                //sqlQuery += " ORDER BY EMP_FIRST_NAME asc ";
            }
            return sqlQuery;
        }


        public static string get_TrainingFeedbackReport()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_HR_TRAINING_FEEDBACK V WHERE ROWNUM > 0 ";
            sqlQuery += " AND V.fb_org_id='" + VMVServices.Web.Utils.OrganizationID + "'";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["PROG_ID"] != null)
                {
                    sqlQuery += " AND V.PROG_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["PROG_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["COURSE_ID"] != null)
                {
                    sqlQuery += " AND V.PROG_COURSE_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["COURSE_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["EMP_ID"] != null)
                {
                    sqlQuery += " AND V.EMP_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["EMP_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["Dept_id"] != null)
                {
                    sqlQuery += " AND V.dept_id = '" + VMVServices.Web.Utils.ReportViewFilterParameter["Dept_id"].ToString() + "'";
                }

            }
            return sqlQuery;
        }


        public static string get_ClearanceFormReport()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_HR_EMP_CLEARENCE_FORM V WHERE ROWNUM > 0 ";
            sqlQuery += " AND V.org_id='" + VMVServices.Web.Utils.OrganizationID + "'";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["EMP_ID"] != null)
                {
                    sqlQuery += " AND V.EMP_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["EMP_ID"].ToString() + "'";
                }

            }
            return sqlQuery;
        }

        public static string get_MinistryofSocialAffairsReport()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_HR_EMP_SOCIAL_FORM V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["EMP_ID"] != null)
                {
                    sqlQuery += " AND V.EMP_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["EMP_ID"].ToString() + "'";
                }

            }
            return sqlQuery;
        }

        public static string get_MonthlysalariesRevealedReport()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_MONTHLY_SALARIES_REVEALED V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["EMP_ID"] != null)
                {
                    sqlQuery += " AND V.EMP_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["EMP_ID"].ToString() + "'";
                }

                if (VMVServices.Web.Utils.ReportViewFilterParameter["PERIOD_ID"] != null)
                {
                    sqlQuery += " AND v.pay_period_id = '" + VMVServices.Web.Utils.ReportViewFilterParameter["PERIOD_ID"].ToString() + "'";
                }

            }
            return sqlQuery;
        }
        public static string get_MobileBillingReport()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM vw_mobile_billing V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["dept_id"] != null)
                {
                    sqlQuery += " AND V.dept_id = '" + VMVServices.Web.Utils.ReportViewFilterParameter["dept_id"].ToString() + "'";
                }
                //if (VMVServices.Web.Utils.ReportViewFilterParameter["DESIGN_ID"] != null)
                //{
                //    sqlQuery += " AND V.DESIGN_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["DESIGN_ID"].ToString() + "'";
                //}
                if (VMVServices.Web.Utils.ReportViewFilterParameter["PERIOD_ID"] != null)
                {
                    sqlQuery += " AND v.pay_period_id = '" + VMVServices.Web.Utils.ReportViewFilterParameter["PERIOD_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["EMP_ID"] != null)
                {
                    sqlQuery += " AND V.EMP_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["EMP_ID"].ToString() + "'";
                }
            }
            return sqlQuery;
        }

        public static string get_MonthlysalariesRevealedTrailReport()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_MONTHLY_SALARY_REVEAL_TRIAL V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["dept_id"] != null)
                {
                    sqlQuery += " AND V.dept_id = '" + VMVServices.Web.Utils.ReportViewFilterParameter["dept_id"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["DEPT_DESIG_ID"] != null)
                {
                    sqlQuery += " AND V.DEPT_DESIG_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["DEPT_DESIG_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["EMP_ID"] != null)
                {
                    sqlQuery += " AND V.EMP_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["EMP_ID"].ToString() + "'";
                }

                if (VMVServices.Web.Utils.ReportViewFilterParameter["PERIOD_ID"] != null)
                {
                    sqlQuery += " AND v.pay_period_id = '" + VMVServices.Web.Utils.ReportViewFilterParameter["PERIOD_ID"].ToString() + "'";
                }

            }
            return sqlQuery;
        }


        public static string get_SocialInsuranceInstallmentReport()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_SOCIAL_INSURANCE_INSTALL V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["EMP_ID"] != null)
                {
                    sqlQuery += " AND V.EMP_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["EMP_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["PERIOD_ID"] != null)
                {
                    sqlQuery += " AND v.PAY_PERIOD = '" + VMVServices.Web.Utils.ReportViewFilterParameter["PERIOD_ID"].ToString() + "'";
                }

            }
            return sqlQuery;
        }

        public static string get_SocialInsuranceInstallmentTrailReport()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_SOCIAL_INSUR_INSTALL_TRIAL V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["dept_id"] != null)
                {
                    sqlQuery += " AND V.dept_id = '" + VMVServices.Web.Utils.ReportViewFilterParameter["dept_id"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["DEPT_DESIG_ID"] != null)
                {
                    sqlQuery += " AND V.DEPT_DESIG_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["DEPT_DESIG_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["EMP_ID"] != null)
                {
                    sqlQuery += " AND V.EMP_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["EMP_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["PERIOD_ID"] != null)
                {
                    sqlQuery += " AND v.PAY_PERIOD = '" + VMVServices.Web.Utils.ReportViewFilterParameter["PERIOD_ID"].ToString() + "'";
                }
            }
            return sqlQuery;
        }

        public static string get_FinalClearanceFormReport()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_FINAL_CLEARANCE V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["EMP_ID"] != null)
                {
                    sqlQuery += " AND V.EMP_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["EMP_ID"].ToString() + "'";
                }

            }
            return sqlQuery;
        }
        public static string get_PermissionduringwrkReport()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_HR_PERMISSION_DUR_WORK_HRS V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["EMP_ID"] != null)
                {
                    sqlQuery += " AND V.EMP_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["EMP_ID"].ToString() + "'";
                }

            }
            return sqlQuery;
        }

        public static string get_SocialAffairContractReport()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_HR_SOCIAL_AFFAIRS_CONTRACT V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["EMP_ID"] != null)
                {
                    sqlQuery += " AND V.EMP_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["EMP_ID"].ToString() + "'";
                }

            }
            return sqlQuery;
        }

    }
}
