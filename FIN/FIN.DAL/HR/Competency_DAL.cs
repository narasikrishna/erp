﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;


namespace FIN.DAL.HR
{
    public class Competency_DAL
    {
        static string sqlQuery = "";

        public static string GetCompetencydtls(String com_hdr_id)
        {
            sqlQuery = string.Empty;


            sqlQuery = " select cd.com_line_id,cd.com_line_num,cd.com_level, scm.CODE VALUE_KEY_ID, SCM.DESCRIPTION" + VMVServices.Web.Utils.LanguageCode + " AS LOOKUP_NAME ,cd.com_level_desc,'N' AS DELETED,";
            sqlQuery += " CASE CD.ENABLED_FLAG WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS ENABLED_FLAG";
            sqlQuery += " from HR_COMPETENCY_DTL cd, SSM_CODE_MASTERS SCM";
            sqlQuery += " where cd.workflow_completion_status = 1";
            sqlQuery += " and cd.com_hdr_id = '" + com_hdr_id + "'";
            sqlQuery += " AND SCM.CODE = CD.COM_LEVEL ";
            sqlQuery += " order by cd.com_line_id asc";

            return sqlQuery;

        }

        public static string GetPayrollDistributiondtls(String dis_id)
        {
            sqlQuery = string.Empty;


            sqlQuery = " SELECT DD.DIS_DTL_ID,DD.SEG_ID,DD.PERCENTAGE,SV.SEGMENT_VALUE,sv.segment_value_id,'N' AS DELETED,";
            sqlQuery += " CASE DD.ENABLED_FLAG WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS ENABLED_FLAG";
            sqlQuery += " FROM HR_EMP_PAY_DISTRIBUTION_HDR DH, GL_SEGMENT_VALUES SV, HR_EMP_PAY_DISTRIBUTION_DTL DD ";
            sqlQuery += " where DD.workflow_completion_status = 1";
            sqlQuery += " and dh.dis_id = '" + dis_id + "'";
            sqlQuery += "  AND DH.DIS_ID = DD.DIS_ID ";
            sqlQuery += "  AND DD.SEG_ID = SV.Segment_Value_Id ";
            sqlQuery += " order by dd.DIS_DTL_ID asc";

            return sqlQuery;

        }

        public static string GetCompetencyName()
        {
            sqlQuery = string.Empty;


            sqlQuery = " select c.com_hdr_id,c.com_desc";
            sqlQuery += " from Hr_Competency_Hdr c";
            sqlQuery += " where c.workflow_completion_status = 1";
            sqlQuery += " and c.enabled_flag = 1";
            sqlQuery += " AND (c.COM_EFFECTIVE_TO_DT IS NULL OR (SYSDATE BETWEEN c.COM_EFFECTIVE_FROM_DT AND c.COM_EFFECTIVE_TO_DT)) ";
            sqlQuery += " order by c.com_desc";
            return sqlQuery;

        }



        static string spOutPut = string.Empty;
        public static System.Collections.SortedList ErrorCollection = new System.Collections.SortedList();


        public static string GetSPFOR_DUPLICATE_CHECK(string P_COM_DESC, string P_RECORD_ID)
        {
            try
            {
                string retcode = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "PKG_VALIDATIONS.PROC_ERR_MGR_COMPETENCY";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add("@P_MODULE_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_SCREEN_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_COM_DESC", OracleDbType.Char).Value = P_COM_DESC;
                oraCmd.Parameters.Add("@P_RECORD_ID", OracleDbType.Char).Value = P_RECORD_ID;
                oraCmd.Parameters.Add("@P_RET", OracleDbType.Varchar2, 150);
                oraCmd.Parameters["@P_RET"].Direction = ParameterDirection.Output;
                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

                retcode = oraCmd.Parameters["@P_RET"].Value.ToString();

                return retcode;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



    }
}
