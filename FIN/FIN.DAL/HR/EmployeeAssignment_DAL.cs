﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;

namespace FIN.DAL.HR
{
    public class EmployeeAssignment_DAL
    {
        static string sqlQuery = "";

        public static string GetEmpAssignmentdtls(string assignmentId)
        {
            sqlQuery = string.Empty;
            string strLngCode = VMVServices.Web.Utils.LanguageCode;
            sqlQuery = "      select ea.assignment_id,ea.location_id,ea.category_id,";
            sqlQuery += "      (select l.loc_desc from hr_locations l where l.loc_id=ea.location_id and rownum=1) as LOCATION_NAME,";
            sqlQuery += "      (select cc.category_code||' - '||cc.category_desc from hr_categories cc where cc.category_id=ea.category_id and rownum=1) as CATEGORY_NAME ,";
            sqlQuery += "     ea.assignment_emp_id,";
            sqlQuery += "     ea.assignment_from_dt,";
            sqlQuery += "     ea.assignment_to_dt,";
            sqlQuery += "     ea.assignment_grade,ea.assignment_job,ea.assignment_position,ea.assignment_reporting_to,";
            sqlQuery += "    (select e.grade_code" + VMVServices.Web.Utils.LanguageCode + " || ' ' || e.grade_desc" + VMVServices.Web.Utils.LanguageCode + "";
            sqlQuery += "       from hr_grades e";
            sqlQuery += "      where e.grade_id = ea.assignment_grade) as grade_name,";
            sqlQuery += "    (select j.job_code" + VMVServices.Web.Utils.LanguageCode + " || '  ' || j.job_desc" + VMVServices.Web.Utils.LanguageCode + "";
            sqlQuery += "       from hr_jobs j";
            sqlQuery += "      where j.job_id = ea.assignment_job) as job_name,";
            sqlQuery += "    (select p.position_code" + VMVServices.Web.Utils.LanguageCode + " || '  ' || p.position_desc" + VMVServices.Web.Utils.LanguageCode + "";
            sqlQuery += "       from hr_positions p";
            sqlQuery += "      where p.position_id = ea.assignment_position) as position_name,";
            sqlQuery += "    (select ee.emp_first_name" + strLngCode + " || ' - ' || ee.emp_middle_name" + strLngCode + " || ' - ' ||";
            sqlQuery += "            ee.emp_last_name" + strLngCode + "";
            sqlQuery += "       from hr_employees ee";
            sqlQuery += "      where ee.emp_id = ea.assignment_reporting_to) as reporting_name,";
            sqlQuery += "      'N' as deleted,";
            sqlQuery += "      (case when ea.enabled_flag = '1' then 'TRUE' else 'FALSE' end) as enabled_flag";
            sqlQuery += "   from HR_EMP_ASSIGNMENT_DTLS ea";
            sqlQuery += "   where ea.workflow_completion_status = '1'";
            sqlQuery += "  AND ea.assignment_id = '" + assignmentId + "'";

            return sqlQuery;

        }
    }
}
