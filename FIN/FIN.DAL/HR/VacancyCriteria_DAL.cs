﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.HR
{
    public class VacancyCriteria_DAL
    {
        static string sqlQuery = "";

        public static string GetVacationCriteriadtls(string vacationEvalId)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select vc.vac_crit_id,'N' as DELETED,";
            sqlQuery += " vc.vac_id,";
            sqlQuery += " va.vac_crit_name,";
            sqlQuery += " va.vac_crit_condition,";
            sqlQuery += " va.vac_crit_value,";
            sqlQuery += " va.vac_eval_id,";
            sqlQuery += " va.vac_eval_condition,";
            sqlQuery += " va.vac_eval_result, vc.enabled_flag,";
            sqlQuery += " v.vac_name";
            sqlQuery += " from hr_vac_criteria_master vc, hr_vacancies v, hr_vac_eval_condition va";
            sqlQuery += " where vc.enabled_flag = '1'";
            sqlQuery += " and v.enabled_flag = '1'";
            sqlQuery += " and v.vac_id = vc.vac_id";
            sqlQuery += "   and vc.vac_crit_id=va.vac_crit_id";
            sqlQuery += "   and  v.org_id='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " and va.vac_crit_id='" + vacationEvalId + "'";
            sqlQuery += " order by VAC_EVAL_ID ";

            return sqlQuery;

        }

        public static string getEvalCondition(string str_vacid)
        {
            sqlQuery = string.Empty;

            sqlQuery += "  SELECT VEC.VAC_CRIT_NAME,VEC.VAC_CRIT_CONDITION,VEC.VAC_CRIT_VALUE,VEC.VAC_EVAL_ID ";
            sqlQuery += "  FROM HR_VAC_EVAL_CONDITION VEC ";
            sqlQuery += "  WHERE VEC.ENABLED_FLAG=1 AND VEC.WORKFLOW_COMPLETION_STATUS=1 ";
            sqlQuery += " AND VEC.VAC_ID='" + str_vacid + "'";
            return sqlQuery;
        }

    }
}
