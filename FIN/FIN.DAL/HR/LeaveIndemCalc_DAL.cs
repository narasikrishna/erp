﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;

namespace FIN.DAL.HR
{
    public class LeaveIndemCalc_DAL
    {
        static string sqlQuery = "";

        public static string GetIndemDetails(string Master_id)
        {
            sqlQuery = string.Empty;

            sqlQuery += " SELECT ics.indem_id,ics.INDEM_LOW_VALUE,ics.INDEM_HIGH_VALUE,ics.INDEM_VALUE,ics.INDEM_REMARKS,";
            sqlQuery += " C.CODE AS LOOKUP_ID,C.DESCRIPTION AS LOOKUP_NAME,CASE ICS.ENABLED_FLAG WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS ENABLED_FLAG,'N' AS DELETED,";
            sqlQuery += " (select con.code from SSM_CODE_MASTERS con where ics.indem_type = con.code) as indem_type";
            sqlQuery += " FROM hr_indem_calc_slab ics, SSM_CODE_MASTERS C";
            sqlQuery += " WHERE ICS.INDEM_TYPE = C.CODE";
            sqlQuery += " AND ICS.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND ICS.INDEM_NAME in (select INDEM_NAME from hr_indem_calc_slab where INDEM_ID = '" + Master_id + "')";
            sqlQuery += " order by   indem_id ";

            return sqlQuery;

        }

        public static string GetIndemDetailsBasedOnType(string indemType)
        {
            sqlQuery = string.Empty;

            sqlQuery += " SELECT ics.indem_id,ics.INDEM_LOW_VALUE,ics.INDEM_HIGH_VALUE,ics.INDEM_VALUE,ics.INDEM_REMARKS,";
            sqlQuery += " C.CODE AS LOOKUP_ID,C.DESCRIPTION AS LOOKUP_NAME,CASE ICS.ENABLED_FLAG WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS ENABLED_FLAG,'N' AS DELETED,";
            sqlQuery += " (select con.code from SSM_CODE_MASTERS con where ics.indem_type = con.code) as indem_type";
            sqlQuery += " FROM hr_indem_calc_slab ics, SSM_CODE_MASTERS C";
            sqlQuery += " WHERE ICS.INDEM_TYPE = C.CODE";
            sqlQuery += " AND ICS.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND ICS.INDEM_TYPE = '" + indemType + "'";
            return sqlQuery;

        }

        public static string GetIndemName()
        {
            sqlQuery = string.Empty;
            sqlQuery += " select distinct indem_name ";
            sqlQuery += " from hr_indem_calc_slab ";
            sqlQuery += " where WORKFLOW_COMPLETION_STATUS = 1 and enabled_flag=1";
            return sqlQuery;
        }

    }
}
