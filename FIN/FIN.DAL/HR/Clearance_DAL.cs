﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;

namespace FIN.DAL.HR
{
    public class Clearance_DAL
    {
        static string sqlQuery = "";
        public static string GetClearancedtls(String CLEARANCE_ID)
        {
            sqlQuery = string.Empty;


            sqlQuery = " SELECT CH.CLEARANCE_ID,CH.DEPT_ID,CH.PARTICULAR,CH.PARTICULAR_OL,'N' AS DELETED,";
            sqlQuery += "  CASE CH.ENABLED_FLAG WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS ENABLED_FLAG";
            sqlQuery += " FROM HR_CLEARANCE_HDR CH";
            sqlQuery += " WHERE CH.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND DEPT_ID  in (select dept_id from HR_CLEARANCE_HDR where CLEARANCE_ID = '" + CLEARANCE_ID + "')";

            return sqlQuery;

        }

        public static string GetClearancedtls4Dept(String str_dept_id)
        {
            sqlQuery = string.Empty;


            sqlQuery = " SELECT CH.CLEARANCE_ID,CH.DEPT_ID,CH.PARTICULAR,CH.PARTICULAR_OL,'N' AS DELETED,";
            sqlQuery += "  CASE CH.ENABLED_FLAG WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS ENABLED_FLAG";
            sqlQuery += " FROM HR_CLEARANCE_HDR CH";
            sqlQuery += " WHERE CH.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND CH.DEPT_ID = '" + str_dept_id + "'";

            return sqlQuery;

        }
        public static string getParticulars()
        {
            sqlQuery = string.Empty;

            sqlQuery = "  SELECT CH.PARTICULAR,CH.PARTICULAR_OL,CH.CLEARANCE_ID FROM HR_CLEARANCE_HDR CH ";
            sqlQuery += " WHERE CH.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " AND CH.ENABLED_FLAG = 1 ";

            return sqlQuery;
        }

        public static string GetChkClearencedtls(string emp_id)
        {
            sqlQuery = string.Empty;
            sqlQuery = "  Select dept_id from hR_EMP_CLEARANCE_HDR where EMP_ID = '" + emp_id + "'";

            return sqlQuery;
        }

        public static string GetEmpClearanceDetails(string Master_id)
        {
            sqlQuery = string.Empty;
            sqlQuery += " SELECT CH.PARTICULAR,CH.PARTICULAR_OL,CH.CLEARANCE_ID,ECD.EMP_CLEARANCE_DTL_ID,'N' as DELETED, ";
            sqlQuery += "  CASE ECD.ENABLED_FLAG WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS ENABLED_FLAG";
            sqlQuery += " FROM HR_EMP_CLEARANCE_DTL ECD,HR_CLEARANCE_HDR CH,HR_EMP_CLEARANCE_HDR ECH ";
            sqlQuery += "  WHERE ECD.CLEARANCE_ID = CH.CLEARANCE_ID ";
            sqlQuery += " AND ECH.EMP_CLEARANCE_ID = ECD.EMP_CLEARANCE_ID";

            sqlQuery += " AND ECH.EMP_CLEARANCE_ID = '" + Master_id + "' ";
            sqlQuery += " order by ECH.EMP_CLEARANCE_ID asc ";
            return sqlQuery;

        }

        public static string GetClearanceDetails(string Master_id)
        {
            sqlQuery = string.Empty;
            
            sqlQuery += " SELECT CID.CLEARANCE_INIT_ID, CID.CLEARANCE_INIT_DTL_ID ,'N' as DELETED, ";
            sqlQuery += "  CASE CID.ENABLED_FLAG WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS ENABLED_FLAG";
            sqlQuery += " ,d.dept_id,d.dept_name,e.emp_id,(e.emp_no|| ' - ' ||e.emp_first_name ||' '||e.emp_middle_name ||' '||e.emp_last_name) emp_name";
            sqlQuery += " FROM HR_CLEARANCE_INITIATION_DTL CID, hr_employees e, hr_departments d";
            sqlQuery += " WHERE e.emp_id = cid.emp_id";
            sqlQuery += " and d.dept_id = cid.dept_id";
            sqlQuery += " AND CID.CLEARANCE_INIT_ID = '" + Master_id + "' ";
            sqlQuery += " order by CID.CLEARANCE_INIT_ID asc ";

            return sqlQuery;

        }

        public static string GetOvertimeCaldtls(String tm_ot_id)
        {
            sqlQuery = string.Empty;


            sqlQuery = " select pc.tm_ot_id,pc.tm_ot_type, pc.tm_ot_rate_per_hour,pc.tm_effective_from_dt,pc.tm_effective_to_dt,pc.tm_ot_remarks,SSM.DESCRIPTION,SSM.CODE,'N' AS DELETED,";
            sqlQuery += "  CASE pc.ENABLED_FLAG WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS ENABLED_FLAG";
            sqlQuery += " from tm_overtime_payment_calc pc,ssm_code_masters ssm";
            sqlQuery += " WHERE ssm.code=pc.tm_ot_type ";
            sqlQuery += " and  pc.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += "  and ssm.parent_code='ENTRY_TYPE'";
            sqlQuery += " AND  pc.tm_ot_id= '" + tm_ot_id + "'";


            return sqlQuery;

        }

        public static string GetLabEntrySlabdtls(String TM_LATE_ID)
        {
            sqlQuery = string.Empty;


            sqlQuery = " SELECT ES.TM_LATE_ID,ES.TM_LATE_LOW_VALUE,ES.TM_LATE_HIGH_VALUE,ES.TM_LATE_VALUE,ES.TM_LATE_REMARKS,ES.TM_EFFECTIVE_FROM_DT,ES.TM_EFFECTIVE_TO_DT,'N' AS DELETED,";
            sqlQuery += "  CASE ES.ENABLED_FLAG WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS ENABLED_FLAG";
            sqlQuery += " FROM TM_EMP_LATE_ENTRY_SLAB ES";
            sqlQuery += " WHERE ES.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND  ES.TM_LATE_ID= '" + TM_LATE_ID + "'";

            return sqlQuery;

        }


    }
}
