﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;


namespace FIN.DAL.HR
{
    public class Position_DAL
    {

        static string sqlQuery = "";

        public static string GetPositiondtls(String position_id)
        {
            sqlQuery = string.Empty;


            sqlQuery = " select p.position_id,p.position_code,p.position_desc,p.POSITION_DESC_OL,p.POSITION_CODE_OL,p.effective_from_date,p.effective_to_date,'N' AS DELETED,";
            sqlQuery += " CASE p.ENABLED_FLAG WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS ENABLED_FLAG";
            sqlQuery += " from hr_positions p";
            sqlQuery += " where p.workflow_completion_status = 1";
            sqlQuery += " and p.position_id =  '" + position_id + "'";

            return sqlQuery;

        }
        public static string GetPositionName(string mode)
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT P.POSITION_ID,P.POSITION_CODE" + VMVServices.Web.Utils.LanguageCode + "||P.POSITION_DESC" + VMVServices.Web.Utils.LanguageCode + " AS POSITION_DESC";
            sqlQuery += " FROM HR_POSITIONS P";
            sqlQuery += " WHERE P.WORKFLOW_COMPLETION_STATUS =1 ";
            if (mode == FINTableConstant.Add)
            {
                sqlQuery += " AND (p.EFFECTIVE_TO_DATE IS NULL OR (SYSDATE BETWEEN p.EFFECTIVE_FROM_DATE AND p.EFFECTIVE_TO_DATE)) ";
                sqlQuery += " AND P.ENABLED_FLAG = 1";
            }
            sqlQuery += " order by p.POSITION_DESC";
            return sqlQuery;

        }

        public static string GetPositionNameBasedonJob(string JOB_ID)
        {
            sqlQuery = string.Empty;


            sqlQuery = " SELECT P.POSITION_ID,P.POSITION_CODE" + VMVServices.Web.Utils.LanguageCode + "||' - '||P.POSITION_DESC" + VMVServices.Web.Utils.LanguageCode + " AS POSITION_DESC";
            sqlQuery += " FROM HR_POSITIONS P";
            sqlQuery += " WHERE P.WORKFLOW_COMPLETION_STATUS =1 ";
            sqlQuery += " AND P.ENABLED_FLAG = 1";
            sqlQuery += " AND P.JOB_ID = '" + JOB_ID + "'";

            return sqlQuery;

        }

        public static string GetPositionNam()
        {
            sqlQuery = string.Empty;


            sqlQuery = " SELECT P.POSITION_ID,P.POSITION_CODE" + VMVServices.Web.Utils.LanguageCode + "||' - '||P.POSITION_DESC" + VMVServices.Web.Utils.LanguageCode + " AS POSITION_DESC";
            sqlQuery += " FROM HR_POSITIONS P";
            sqlQuery += " WHERE P.WORKFLOW_COMPLETION_STATUS =1 ";
            sqlQuery += " AND P.ENABLED_FLAG = 1";
           

            return sqlQuery;

        }

        public static string GetPossdtls(String job_id)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select p.position_id,p.position_code,p.position_desc,p.POSITION_CODE_OL,p.POSITION_DESC_OL,p.effective_from_date,p.effective_to_date,";
            sqlQuery += " CASE p.ENABLED_FLAG WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS ENABLED_FLAG";
            sqlQuery += " from hr_positions p";
            sqlQuery += " where p.workflow_completion_status = 1";
            sqlQuery += " and p.job_id =  '" + job_id + "'";

            //sqlQuery = " select p.position_id,p.position_code,p.position_desc,p.effective_from_date,p.effective_to_date,";
            //sqlQuery += " 'TRUE' AS ENABLED_FLAG";
            //sqlQuery += " from hr_positions p";
            //sqlQuery += " where p.workflow_completion_status = 1";
            //sqlQuery += " and p.job_id =  '" + job_id + "'";

            return sqlQuery;

        }

        public static string CheckChildRecord(String positionId)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select p.position_id ";
            sqlQuery += " from hr_positions p, hr_emp_work_dtls wd";
            sqlQuery += " where p.position_id = wd.emp_position_id";
            sqlQuery += " and wd.enabled_flag = 1";
            sqlQuery += " and wd.emp_position_id =  '" + positionId + "'";


            return sqlQuery;

        }


        public static string getPositionName(String positionId)
        {
            sqlQuery = string.Empty;


            sqlQuery = " select p.position_desc" + VMVServices.Web.Utils.LanguageCode + " as position_desc";
            sqlQuery += " from hr_positions p";
            sqlQuery += " where p.enabled_flag = 1";
            sqlQuery += " and p.position_id = '" + positionId + "'";
            return sqlQuery;

        }


        static string spOutPut = string.Empty;
        public static System.Collections.SortedList ErrorCollection = new System.Collections.SortedList();


        public static string GetSPFOR_DUPLICATE_CHECK(string P_JOB_ID, string P_POSITION_CODE, string P_RECORD_ID)
        {
            try
            {
                string retcode = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "PKG_VALIDATIONS.PROC_ERR_MGR_POSITIONS";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add("@P_MODULE_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_SCREEN_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_JOB_ID", OracleDbType.Char).Value = P_JOB_ID;
                oraCmd.Parameters.Add("@P_POSITION_CODE", OracleDbType.Char).Value = P_POSITION_CODE;
                oraCmd.Parameters.Add("@P_RECORD_ID", OracleDbType.Char).Value = P_RECORD_ID;
                oraCmd.Parameters.Add("@P_RET", OracleDbType.Varchar2, 150);
                oraCmd.Parameters["@P_RET"].Direction = ParameterDirection.Output;
                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

                retcode = oraCmd.Parameters["@P_RET"].Value.ToString();

                return retcode;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

}
