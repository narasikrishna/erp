﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;


namespace FIN.DAL.HR
{
    public class Location_DAL
    {
        static string sqlQuery = "";

        public static string GetLocationDetails(string countryId)
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM hr_locations d";       
            sqlQuery += " WHERE d.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " and d.LOC_COUNTRY = '" + countryId + "'";
            sqlQuery += " and d.LOC_ORG_ID =  '" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " AND d.ENABLED_FLAG = 1 ";
            sqlQuery += " ORDER BY LOC_DESC asc ";

            return sqlQuery;
        }
        public static string fn_GetLocationGridDtls(String LOC_id)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select ls.loc_day_id,SSM.CODE AS LOOKUP_ID,ssm.description as LOOKUP_NAME,ls.loc_id,ls.loc_day_name,ls.loc_sch_code,";
            sqlQuery += " ls.from_date,ls.to_date,TO_CHAR(TO_DATE(s.tm_from1, 'dd/MM/yyyy hh:mi:ss AM'), 'HH12:MI AM') || ";
            sqlQuery += "  ' - ' ||TO_CHAR(TO_DATE(s.tm_to1, 'dd/MM/yyyy hh:mi:ss AM'), 'HH12:MI AM') AS SCHEDULE_TIME, ";
            sqlQuery += " CASE LS.ENABLED_FLAG WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS ENABLED_FLAG,'N' as DELETED ";
            sqlQuery += " from HR_LOCATION_DAY_SCH LS, SSM_CODE_MASTERS SSM, TM_SCHEDULES S ";
            sqlQuery += " where ls.loc_day_name=ssm.code ";
            sqlQuery += " and LS.loc_id = '" + LOC_id + "'";
            sqlQuery += " and ls.loc_sch_code=s.tm_sch_code ";
            sqlQuery += " and ssm.parent_code = 'DAY_NAME'";

            return sqlQuery;

        }

        public static string fn_GetWorkingSaturdayDtl(String LOC_id)
        {
            sqlQuery = string.Empty;

            sqlQuery += " SELECT WS.loc_sat_id,WS.LOC_SAT_DT,TO_CHAR(TO_DATE(s.tm_from1, 'dd/MM/yyyy hh:mi:ss AM'), 'HH12:MI AM') ||";
            sqlQuery += "  ' - ' ||TO_CHAR(TO_DATE(s.tm_to1, 'dd/MM/yyyy hh:mi:ss AM'), 'HH12:MI AM') AS SCHEDULE_TIME,WS.LOC_SCH_CODE ";
            sqlQuery += " ,CASE WS.ENABLED_FLAG WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS ENABLED_FLAG ";
            sqlQuery += " FROM HR_LOC_WORKING_SATURDAYS WS,TM_SCHEDULES S";
            sqlQuery += "  WHERE WS.LOC_SCH_CODE = S.TM_SCH_CODE";
            sqlQuery += " AND WS.LOC_ID = '" + LOC_id + "'";

            return sqlQuery;

        }

        public static string GetLocations()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT d.loc_id,d.loc_desc FROM hr_locations d";
            sqlQuery += " WHERE d.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " and d.LOC_ORG_ID =  '" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " AND d.ENABLED_FLAG = 1 ";
            sqlQuery += " ORDER BY LOC_DESC asc ";

            return sqlQuery;
        }
        public static string GetSchedule()
        {
            sqlQuery = string.Empty;
            sqlQuery = " select TO_CHAR(TO_DATE(s.tm_from1, 'dd/MM/yyyy hh:mi:ss AM'), 'HH12:MI AM') || ' - ' || ";
            sqlQuery += " TO_CHAR(TO_DATE(s.tm_to1, 'dd/MM/yyyy hh:mi:ss AM'), 'HH12:MI AM') AS SCHEDULE_TIME,S.TM_SCH_CODE ";
            sqlQuery += " from tm_schedules s ";
            sqlQuery += " WHERE S.ENABLED_FLAG = 1 ";
            sqlQuery += "AND S.workflow_completion_status = 1 ";

            return sqlQuery;
        }
    }
}
