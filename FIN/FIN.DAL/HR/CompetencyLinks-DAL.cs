﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace FIN.DAL.HR
{
    public class CompetencyLinks_DAL
    {
        static string sqlQuery = "";

        public static string GetCompetencyLinksdtls(String com_link_id)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select cld.com_link_dtl_id,cd.com_line_id,cd.com_line_num,cd.com_level,cld.com_remarks,cld.com_rate,'N' AS DELETED,SCM.DESCRIPTION" + VMVServices.Web.Utils.LanguageCode + "||' - '||cd.com_level_desc AS LOOKUP_NAME,";
            sqlQuery += " case cld.enabled_flag when '1' then 'TRUE' ELSE 'FALSE' END AS enabled_flag";
            sqlQuery += " from HR_COMPETANCY_LINKS_DTL cld,hr_competency_dtl cd, SSM_CODE_MASTERS SCM";
            sqlQuery += " where cld.com_line_id = cd.com_line_id";
            sqlQuery += " and cld.com_link_id = '" + com_link_id + "'";
            sqlQuery += " and cld.workflow_completion_status = 1";
            sqlQuery += " AND SCM.CODE = cd.COM_LEVEL ";
            sqlQuery += " order by cld.com_link_dtl_id asc";

            return sqlQuery;

        }

        public static string GetCompetency(String com_hdr_id)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select cd.com_line_id,cd.com_line_num,cd.com_level,'' as COM_REMARKS,0 as COM_RATE,0 as com_link_dtl_id,'TRUE' as enabled_flag,SCM.DESCRIPTION" + VMVServices.Web.Utils.LanguageCode + "||' - '||cd.com_level_desc AS LOOKUP_NAME";
            sqlQuery += " from Hr_Competency_Dtl cd,Hr_Competency_Hdr ch, SSM_CODE_MASTERS SCM";
            sqlQuery += " where cd.com_hdr_id = ch.com_hdr_id";
            sqlQuery += " and ch.com_hdr_id = '" + com_hdr_id + "'";
            sqlQuery += " AND SCM.CODE = cd.COM_LEVEL "; 
            sqlQuery += " order by cd.com_line_num ";
            
            return sqlQuery;

        }

        public static string GetCompetencytyp(String com_hdr_id)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select c.com_type";
            sqlQuery += " from Hr_Competency_Hdr c";
            sqlQuery += " where c.com_hdr_id = '" + com_hdr_id + "'";
            sqlQuery += " and c.workflow_completion_status =1";

            return sqlQuery;

        }

    }
}
