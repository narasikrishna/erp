﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;

namespace FIN.DAL.HR
{
    public class AppraisalObjectives_DAL
    {
        static string sqlQuery = "";

        public static string GetAppraisalObjectivedtls(String PER_OBJ_ID)
        {
            sqlQuery = string.Empty;


            sqlQuery = " SELECT AO.PER_OBJ_ID,AO.PER_CODE,AO.PER_DESC,AO.PER_CODE_OL,AO.PER_DESC_OL,AO.PER_CATEGORY, SCM.CODE AS LOOKUP_ID, SCM.DESCRIPTION" + VMVServices.Web.Utils.LanguageCode + " AS LOOKUP_NAME, AO.PER_TYPE,AO.PER_MIN_RATING,AO.PER_MAX_RATING,'N' AS DELETED";
            sqlQuery += " FROM HR_PER_OBJECTIVE_SET AO, SSM_CODE_MASTERS SCM";
            sqlQuery += " WHERE AO.PER_OBJ_ID = '" + PER_OBJ_ID + "'";
            sqlQuery += " AND SCM.CODE = AO.Per_Category";
            sqlQuery += " and scm.parent_code = 'CAT'";          
            sqlQuery += " AND AO.WORKFLOW_COMPLETION_STATUS = 1";

            return sqlQuery;

        }


        static string spOutPut = string.Empty;
        public static System.Collections.SortedList ErrorCollection = new System.Collections.SortedList();


        public static string GetSPFOR_DUPLICATE_CHECK(string P_PER_CODE, string P_PER_CATEGORY, string P_PER_TYPE, string P_RECORD_ID)
        {
            try
            {
                string retcode = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "PKG_VALIDATIONS.PROC_ERR_MGR_APP_OBJ";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add("@P_MODULE_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_SCREEN_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_PER_CODE", OracleDbType.Char).Value = P_PER_CODE;
                oraCmd.Parameters.Add("@P_PER_CATEGORY", OracleDbType.Char).Value = P_PER_CATEGORY;
                oraCmd.Parameters.Add("@P_PER_TYPE", OracleDbType.Char).Value = P_PER_TYPE;
                oraCmd.Parameters.Add("@P_RECORD_ID", OracleDbType.Char).Value = P_RECORD_ID;
                oraCmd.Parameters.Add("@P_RET", OracleDbType.Varchar2, 150);
                oraCmd.Parameters["@P_RET"].Direction = ParameterDirection.Output;
                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

                retcode = oraCmd.Parameters["@P_RET"].Value.ToString();

                return retcode;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
