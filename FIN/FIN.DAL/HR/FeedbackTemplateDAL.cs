﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;

namespace FIN.DAL.HR
{
    public class FeedbackTemplateDAL
    {
        static string sqlQuery = "";

        //public static string GetFeedbackDtls(String feedbackId)
        //{
        //    sqlQuery = string.Empty;

        //    sqlQuery = " select fh.feedback_id, fh.feedback_temp_name, fh.effective_from_dt, fh.effective_to_dt";
        //    sqlQuery += " ,fd.feedback_dtl_id, fd.feedback_type, fd.feedback_desc, fd.feedback_rating_type, fd.effective_from_dt as dtlFromDate";
        //    sqlQuery += " ,fd.effective_to_dt as dtlToDate,fd.enabled_flag as dtlActive,fd.workflow_completion_status as dtlWFStatus";
        //    sqlQuery += " ,scm.description " + VMVServices.Web.Utils.LanguageCode + " as feed_type, cm.description " + VMVServices.Web.Utils.LanguageCode + " as feed_rating_type";
        //    sqlQuery += " ,scm.code as code_id, cm.code as dtl_code_id, scm.code as code_name, cm.code as dtl_code_name";
        //    sqlQuery += " ,(case fd.enabled_flag when '1' then 'TRUE' ELSE 'FALSE' END) AS enabled_flag, 'N' as deleted";
        //    sqlQuery += " from hr_feedback_hdr fh, hr_feedback_dtl fd, ssm_code_masters scm, ssm_code_masters cm";
        //    sqlQuery += " where fd.feedback_id = fh.feedback_id";
        //    sqlQuery += " and fd.feedback_type=scm.code";
        //    sqlQuery += " and fd.feedback_rating_type=cm.code";
        //    sqlQuery += " and scm.parent_code='FEEDBACK_TYP'";
        //    sqlQuery += " and cm.parent_code='FEEDBACK_RATING_TYP'";
        //    sqlQuery += " and fh.feedback_id='" + feedbackId + "'";
        //    sqlQuery += " and fh.enabled_flag=1";
        //    sqlQuery += " and fh.workflow_completion_status=1";
        //    sqlQuery += " and fh.effective_from_dt is not null";
        //    sqlQuery += " order by fh.feedback_temp_name asc";

        //    return sqlQuery;

        //}

        public static string GetFeedbackDtls(String feedbackId)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select fh.feedback_id, fh.feedback_temp_name, fh.effective_from_dt, fh.effective_to_dt, fh.feedback_template_comments";
            sqlQuery += " ,fd.feedback_dtl_id, fd.feedback_type, fd.feedback_desc,fd.feedback_desc_ol, fd.feedback_rating_type, fd.effective_from_dt as dtlFromDate";
            sqlQuery += " ,fd.effective_to_dt as dtlToDate";
            sqlQuery += " ,(case fd.enabled_flag when '1' then 'TRUE' ELSE 'FALSE' END) AS enabled_flag, 'N' as deleted";
            sqlQuery += " ,ftt.feedback_type_id as code_id, ftt.feedback_type_name as code_name, ftt.feedback_rating_type as rating_type";
            sqlQuery += " ,scm.code as dtl_code_id, scm.code as dtl_code_name";
            sqlQuery += " from hr_feedback_hdr fh, hr_feedback_dtl fd, hr_feedback_template_type ftt, ssm_code_masters scm";
            sqlQuery += " where fd.feedback_id = fh.feedback_id";
            sqlQuery += " and ftt.feedback_type_id (+)= fd.feedback_type_id";
            sqlQuery += " and fd.feedback_rating_type=scm.code";
            sqlQuery += " and scm.parent_code='FEEDBACK_RATING_TYP'";
            sqlQuery += " and fh.feedback_id='" + feedbackId + "'";
            sqlQuery += " and fh.enabled_flag=1";
            sqlQuery += " and fh.workflow_completion_status=1";
            sqlQuery += " and fh.effective_from_dt is not null";
            sqlQuery += " order by fd.feedback_dtl_id asc";

            return sqlQuery;

        }


        public static string GetFeedbackDtls4EMP(String feedbackId,string str_emp_id)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select efd.EMP_FEEDBACK_DTL_ID as EMP_FEEDBACK_DTL_ID, fh.feedback_id, fh.feedback_temp_name, fh.effective_from_dt, fh.effective_to_dt, fh.feedback_template_comments";
            sqlQuery += " ,fd.feedback_dtl_id, fd.feedback_type, fd.feedback_desc, fd.feedback_rating_type, fd.effective_from_dt as dtlFromDate";
            sqlQuery += " ,fd.effective_to_dt as dtlToDate";
            sqlQuery += " ,(case fd.enabled_flag when '1' then 'TRUE' ELSE 'FALSE' END) AS enabled_flag, 'N' as deleted";
            sqlQuery += " ,ftt.feedback_type_id as code_id, ftt.feedback_type_name as code_name, ftt.feedback_rating_type as rating_type";
            sqlQuery += " ,scm.code as dtl_code_id, scm.code as dtl_code_name";
            sqlQuery += " ,efd.EMP_FEEDBACK_RATE as SEL_RATE,efd.EMP_COMMENTS AS TYPED_COMMENTS ";
            sqlQuery += " from hr_feedback_hdr fh, hr_feedback_dtl fd, hr_feedback_template_type ftt, ssm_code_masters scm,hR_EMP_FEEDBACK_DTL efd,hR_EMP_FEEDBACK_HDR efh";
            sqlQuery += " where fd.feedback_id = fh.feedback_id";
            sqlQuery += " and ftt.feedback_type_id (+)= fd.feedback_type_id";
            sqlQuery += " and fd.feedback_rating_type=scm.code";
            sqlQuery += " and scm.parent_code='FEEDBACK_RATING_TYP'";
            sqlQuery += " and fh.feedback_id='" + feedbackId + "'";
            sqlQuery += " and fh.enabled_flag=1";
            sqlQuery += " and fh.workflow_completion_status=1";           
            sqlQuery += " and fh.effective_from_dt is not null";
            sqlQuery += " and efh.emp_feedback_id=efd.emp_feedback_id and efd.emp_feedback_desc=fd.feedback_dtl_id ";
            sqlQuery += " and efh.emp_id='" + str_emp_id + "' and efh.emp_feedback_temp_name='" + feedbackId + "'";
            sqlQuery += " UNION ";            

            sqlQuery += " select '0' as EMP_FEEDBACK_DTL_ID, fh.feedback_id, fh.feedback_temp_name, fh.effective_from_dt, fh.effective_to_dt, fh.feedback_template_comments";
            sqlQuery += " ,fd.feedback_dtl_id, fd.feedback_type, fd.feedback_desc, fd.feedback_rating_type, fd.effective_from_dt as dtlFromDate";
            sqlQuery += " ,fd.effective_to_dt as dtlToDate";
            sqlQuery += " ,(case fd.enabled_flag when '1' then 'TRUE' ELSE 'FALSE' END) AS enabled_flag, 'N' as deleted";
            sqlQuery += " ,ftt.feedback_type_id as code_id, ftt.feedback_type_name as code_name, ftt.feedback_rating_type as rating_type";
            sqlQuery += " ,scm.code as dtl_code_id, scm.code as dtl_code_name";
            sqlQuery += " ,'' as SEL_RATE,'' AS TYPED_COMMENTS ";
            sqlQuery += " from hr_feedback_hdr fh, hr_feedback_dtl fd, hr_feedback_template_type ftt, ssm_code_masters scm";
            sqlQuery += " where fd.feedback_id = fh.feedback_id";
            sqlQuery += " and ftt.feedback_type_id (+)= fd.feedback_type_id";
            sqlQuery += " and fd.feedback_rating_type=scm.code";
            sqlQuery += " and scm.parent_code='FEEDBACK_RATING_TYP'";
            sqlQuery += " and fh.feedback_id='" + feedbackId + "'";
            sqlQuery += " and fh.enabled_flag=1";
            sqlQuery += " and fh.workflow_completion_status=1";
            sqlQuery += " and fh.effective_from_dt is not null";
            sqlQuery += " and fd.feedback_dtl_id not in (select emp_feedback_desc from hR_EMP_FEEDBACK_DTL efd,hR_EMP_FEEDBACK_HDR efh where efh.emp_feedback_id=efd.emp_feedback_id and efh.emp_id='" + str_emp_id + "' and efh.emp_feedback_temp_name='" + feedbackId + "')";
            sqlQuery += " order by feedback_dtl_id asc";

            return sqlQuery;

        }
        public static string getFeedbackTemplateName()
        {
            sqlQuery = string.Empty;

            sqlQuery = " select fh.feedback_id, fh.feedback_temp_name, fh.effective_from_dt";
            sqlQuery += " from hr_feedback_hdr fh";
            sqlQuery += " where fh.enabled_flag=1";
            sqlQuery += " and fh.workflow_completion_status=1";

            return sqlQuery;
        }

        public static string getFeedbackTypes(string feedbackID)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select distinct fh.feedback_id as feedback_id_type, fd.feedback_type";
            sqlQuery += " from hr_feedback_hdr fh ,hr_feedback_dtl fd";
            sqlQuery += " where fh.feedback_id=fd.feedback_id";
            sqlQuery += " and fh.enabled_flag=1";
            sqlQuery += " and fh.workflow_completion_status=1";
            sqlQuery += " and fh.feedback_id='" + feedbackID + "'";

            return sqlQuery;
        }

        public static string getFeedbackRating(string feedbackID)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select distinct fh.feedback_id as feedback_id_rating, fd.feedback_rating_type";
            sqlQuery += " from hr_feedback_hdr fh , hr_feedback_dtl fd";
            sqlQuery += " where fh.feedback_id=fd.feedback_id";
            sqlQuery += " and fh.enabled_flag=1";
            sqlQuery += " and fh.workflow_completion_status=1";
            sqlQuery += " and fh.feedback_id='" + feedbackID + "'";

            return sqlQuery;
        }
        public static string getFeedbackDesc(string feedbackID)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select distinct fd.feedback_dtl_id as feedback_id_desc, fd.feedback_desc, fh.feedback_id";
            sqlQuery += " from hr_feedback_hdr fh, hr_feedback_dtl fd";
            sqlQuery += " where fh.feedback_id=fd.feedback_id";
            sqlQuery += " and fh.enabled_flag=1";
            sqlQuery += " and fh.workflow_completion_status=1";
            sqlQuery += " and fh.feedback_id='" + feedbackID + "'";

            return sqlQuery;
        }

        public static string GetEmpFeedbackDtls(String empFeedbackId)
        {
            sqlQuery = string.Empty;
            string strLngCode = VMVServices.Web.Utils.LanguageCode;

            sqlQuery = " select fh.emp_feedback_id, fh.emp_feedback_temp_name, f.feedback_temp_name,";
            sqlQuery += " t.emp_id,(t.emp_no|| ' - ' ||t.emp_first_name" + strLngCode + "||' '||t.emp_middle_name" + strLngCode + "||' '||t.emp_last_name" + strLngCode + ") emp_name,t.emp_no";
            sqlQuery += " ,d.feedback_dtl_id, d.feedback_type, d.feedback_rating_type, d.feedback_desc,";
            sqlQuery += " f.feedback_id,f.feedback_id as feedback_id_desc, f.feedback_id as feedback_id_rating, f.feedback_id as feedback_id_type";
            sqlQuery += " ,fd.emp_feedback_dtl_id, fd.emp_feedback_type, fd.emp_feedback_desc, fd.emp_feedback_rating";
            sqlQuery += " ,(case fd.enabled_flag when '1' then 'TRUE' ELSE 'FALSE' END) AS enabled_flag, 'N' as deleted, fd.emp_comments,fd.emp_feedback_rate";
            sqlQuery += " ,scm.code as rating_code_id, scm.code as rating_code_name";
            sqlQuery += " from hr_emp_feedback_hdr fh, hr_emp_feedback_dtl fd, hr_feedback_hdr f, hr_feedback_dtl d, ssm_code_masters scm, hr_employees t";
            sqlQuery += " where fd.emp_feedback_id = fh.emp_feedback_id";
            sqlQuery += " and fh.emp_feedback_temp_name = f.feedback_id";
            sqlQuery += " and t.emp_id=fh.emp_id";
            sqlQuery += " and d.feedback_dtl_id = fd.emp_feedback_desc";
            sqlQuery += " and fd.emp_feedback_rate = scm.code";
            sqlQuery += " and (scm.parent_code = 'FEEDBACK_RATING_N' or scm.parent_code = 'FEEDBACK_RATING')";
            sqlQuery += " and fh.emp_feedback_id='" + empFeedbackId + "'";
            sqlQuery += " and fh.enabled_flag=1";
            sqlQuery += " and fh.workflow_completion_status=1";
            sqlQuery += " order by fh.emp_feedback_id asc";

            return sqlQuery;

        }



        public static string getEmployeeFeedbackPercentage()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT * FROM VW_FEEDBACK_RATING_PER V WHERE ROWNUM > 0 ";
          

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["TMPL_ID"] != null)
                {
                    sqlQuery += " AND v.FEEDBACK_ID='" + VMVServices.Web.Utils.ReportViewFilterParameter["TMPL_ID"] + "'";
                }
            }
            return sqlQuery;
        }

        public static string getEmployeeFeedbackPercentage4Option()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT * FROM vw_feedback_opt_rating V WHERE ROWNUM > 0 ";


            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["TMPL_ID"] != null)
                {
                    sqlQuery += " AND v.FEEDBACK_ID='" + VMVServices.Web.Utils.ReportViewFilterParameter["TMPL_ID"] + "'";
                }
            }
            return sqlQuery;
        }

        public static string getEmployeeFeedbackPercentage4Paragraph()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT * FROM VW_FEEDBACK_PARAGRAPH V WHERE ROWNUM > 0 ";


            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["TMPL_ID"] != null)
                {
                    sqlQuery += " AND v.FEEDBACK_ID='" + VMVServices.Web.Utils.ReportViewFilterParameter["TMPL_ID"] + "'";
                }
            }
            return sqlQuery;
        }
        public static string getFeedbackTemplateTypes(string str_typeName="",string str_masterId="")
        {
            sqlQuery = string.Empty;

            sqlQuery = " select ftt.feedback_type_id, ftt.feedback_type_name,ftt.feedback_rating_type";
            sqlQuery += " from hr_feedback_template_type ftt";
            sqlQuery += " where ftt.enabled_flag=1";
            sqlQuery += " and ftt.workflow_completion_status=1";
            if (str_typeName.Length > 0)
            {
                sqlQuery += " and UPPER(ftt.feedback_type_name)='" + str_typeName.ToUpper() + "'";
            }
            if (str_masterId.Length > 0)
            {
                sqlQuery += " and UPPER(ftt.feedback_type_id) !='" + str_masterId.ToUpper() + "'";
            }
            sqlQuery += " order by ftt.feedback_type_name asc";

            return sqlQuery;
        }

        public static string getFeedbackTemplateRatingTypeName(string fdTypeId)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select ftt.feedback_type_id, ftt.feedback_rating_type";
            sqlQuery += " from hr_feedback_template_type ftt";
            sqlQuery += " where ftt.feedback_type_id = '" + fdTypeId + "'";
            sqlQuery += " and ftt.enabled_flag=1";
            sqlQuery += " and ftt.workflow_completion_status=1";
            //sqlQuery += " order by ftt.feedback_type_name asc";

            return sqlQuery;
        }
    }
}