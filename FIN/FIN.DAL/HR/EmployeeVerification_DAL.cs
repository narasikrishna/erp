﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;

namespace FIN.DAL.HR
{
    public class EmployeeVerification_DAL
    {
        static string sqlQuery = "";

        public static string GetApplicantDetails()
        {
            sqlQuery = string.Empty;
            sqlQuery = " select HA.APP_ID,HA.APP_ID||' - '||HA.APP_FIRST_NAME||' '||HA.APP_MIDDLE_NAME||' ' ||HA.APP_LAST_NAME AS APP_NAME from HR_APPLICANTS HA ";
            sqlQuery += " where HA.ENABLED_FLAG = 1 ";
            sqlQuery += " and HA.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " and not exists (select EVH.APP_ID from HR_EMP_VERIFICATION_HDR EVH where evh.app_id = HA.APP_ID) ";
            sqlQuery += " ORDER BY HA.APP_ID asc ";

            return sqlQuery;
        }

        public static string GetApplicantDetails_Edit()
        {
            sqlQuery = string.Empty;
            sqlQuery = " select HA.APP_ID,HA.APP_ID||' - '||HA.APP_FIRST_NAME||' '||HA.APP_MIDDLE_NAME||' ' ||HA.APP_LAST_NAME AS APP_NAME from HR_APPLICANTS HA ";
            sqlQuery += " where HA.ENABLED_FLAG = 1 ";
            sqlQuery += " and HA.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " ORDER BY HA.APP_ID asc ";

            return sqlQuery;
        }
        public static string GetApplicantName(string App_id)
        {
            sqlQuery = string.Empty;
            sqlQuery = " select HA.APP_FIRST_NAME||' '||HA.APP_MIDDLE_NAME||' ' ||HA.APP_LAST_NAME AS APP_NAME from HR_APPLICANTS HA ";
            sqlQuery += " where HA.ENABLED_FLAG = 1 ";
            sqlQuery += " and HA.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " and HA.APP_ID = '" + App_id + "'";

            return sqlQuery;
        }

        public static string GetEmployeeVerfnDtls(string Verify_id)
        {
            sqlQuery = string.Empty;
            sqlQuery = " select  VERIFICATION_CATEGORY,VERIFICATION_CATEGORY as VERIFICATION_CATEGORY_ID, ";
            //  (select SM.DESCRIPTION from SSM_CODE_MASTERS SM where SM.CODE = ASID.ASSET_RETURNABLE and SM.PARENT_CODE = 'AST_TYP') as ASSET_TYPE, ";
            sqlQuery += "  VERIFICATION_TYPE,VERIFICATION_TYPE as VERIFICATION_TYPE_ID,";
            sqlQuery += " EVD.VERIFICATION_AGENCY_EMP,VERIFICATION_STATUS,VERIFICATION_STATUS as VERIFICATION_STATUS_ID, 'N' as DELETED,EVD.VERIFY_ID,EVD.VERIFY_DTL_ID";
            sqlQuery += " from HR_EMP_VERIFICATION_DTL EVD,HR_EMP_VERIFICATION_HDR EVH ";
            sqlQuery += " where EVH.VERIFY_ID = EVD.VERIFY_ID ";
            sqlQuery += " and EVD.VERIFY_ID ='" + Verify_id + "'";
            sqlQuery += " order by VERIFY_DTL_ID ";

            return sqlQuery;
        }

    }
}
