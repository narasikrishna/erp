﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;

namespace FIN.DAL.HR
{
    public class LeaveCancellation_DAL
    {
        static string sqlQuery = "";

        public static string GetLeaveCancelDtls(String lcHdrID)
        {
            sqlQuery = string.Empty;

            sqlQuery = "  select hlcr.leave_req_id, hla.fiscal_year, hla.dept_id from hr_leave_cancellation_hdr hlcr ";
            sqlQuery += "  inner join hr_leave_applications hla ";
            sqlQuery += "  on hla.leave_req_id = hlcr.leave_req_id ";
            sqlQuery += "  where hlcr.lc_hdr_id = '" + lcHdrID + "'";

            return sqlQuery;

        }

        static string spOutPut = string.Empty;
        public static System.Collections.SortedList ErrorCollection = new System.Collections.SortedList();


        public static string GetSPFOR_DUPLICATE_CHECK(string P_STAFF_ID, string P_LEAVE_FROM, string P_LEAVE_TO, string P_RECORD_ID, string P_LEAVE_REQ_ID)
        {
            try
            {
                string retcode = string.Empty;

                OracleCommand oraCmd = new OracleCommand();


                //oraCmd.Parameters.Clear();

                oraCmd.CommandText = "PKG_VALIDATIONS.PROC_ERR_MGR_LEAVE_CANC";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add("@P_MODULE_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_SCREEN_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_STAFF_ID", OracleDbType.Char).Value = P_STAFF_ID;
                oraCmd.Parameters.Add("@P_LEAVE_FROM", OracleDbType.Char).Value = P_LEAVE_FROM;
                oraCmd.Parameters.Add("@P_LEAVE_TO", OracleDbType.Char).Value = P_LEAVE_TO;
                oraCmd.Parameters.Add("@P_RECORD_ID", OracleDbType.Char).Value = P_RECORD_ID;
                oraCmd.Parameters.Add("@P_LEAVE_REQ_ID", OracleDbType.Char).Value = P_LEAVE_REQ_ID;
                oraCmd.Parameters.Add("@P_RET", OracleDbType.Varchar2, 150);
                oraCmd.Parameters["@P_RET"].Direction = ParameterDirection.Output;
                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

                retcode = oraCmd.Parameters["@P_RET"].Value.ToString();

                return retcode;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
