﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;

namespace FIN.DAL.HR
{
    public class ProbEmpCompetencyDAL
    {
        static string sqlQuery = "";
        public static string GetProbationEmpdtls(String PROB_EMP_ID)
        {
            sqlQuery = string.Empty;


            sqlQuery = " select PED.PROB_DTL_ID,PED.PROB_ID,PED.COM_HDR_ID,PC.COM_DESC,PED.COM_LINE_ID,PD.COM_LEVEL_DESC,PED.ATTRIBUTE1,pd.COM_LINE_NUM,'N' AS DELETED, ";
            sqlQuery += " CASE PED.ENABLED_FLAG WHEN '1' THEN 'TRUE' ELSE 'FALSE' END  as ENABLED_FLAG";
            sqlQuery += " ,PED.COMP_RATING,CM.DESCRIPTION ";
            sqlQuery += " from HR_PROBATION_EMP_HDR PEH,HR_PROBATION_EMP_DTL PED,HR_PROBATION_COMP_HDR PC,HR_PROBATION_COMP_DTL PD,SSM_CODE_MASTERS CM";
            sqlQuery += " where PEH.PROB_ID = PED.PROB_ID";
            sqlQuery += " AND CM.CODE = PED.COMP_RATING ";
            sqlQuery += " and PED.COM_HDR_ID = PC.COM_HDR_ID ";
            sqlQuery += " and PED.COM_LINE_ID= PD.COM_LINE_ID ";
            sqlQuery += " AND PEH.ENABLED_FLAG = '1'  ";
            sqlQuery += " AND PEH.WORKFLOW_COMPLETION_STATUS = '1' ";
            sqlQuery += " AND    CM.parent_code='RATING' ";
            sqlQuery += " and PEH.PROB_ID =   '" + PROB_EMP_ID + "'";
            sqlQuery += " ORDER BY PROB_DTL_ID asc";


            return sqlQuery;

        }

        public static string GetCompetencyName()
        {
            sqlQuery = string.Empty;


            sqlQuery = " select PH.COM_HDR_ID,PH.COM_DESC from HR_PROBATION_COMP_HDR PH";
            sqlQuery += " where PH.ENABLED_FLAG = '1'";
            sqlQuery += " and PH.COM_ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " and PH.WORKFLOW_COMPLETION_STATUS = '1'";

            return sqlQuery;

        }

        public static string GetCompetency_DescName(string COM_HDR_ID)
        {
            sqlQuery = string.Empty;


            sqlQuery = " select PD.COM_LINE_ID,PD.COM_LEVEL_DESC";
            sqlQuery += " from HR_PROBATION_COMP_HDR PH,HR_PROBATION_COMP_DTL PD ";
            sqlQuery += "where PH.COM_HDR_ID = PD.COM_HDR_ID ";
            sqlQuery += " and PH.ENABLED_FLAG = '1'";
            sqlQuery += " and PH.WORKFLOW_COMPLETION_STATUS = '1'";
            sqlQuery += " and PH.COM_HDR_ID = '" + COM_HDR_ID + "'";

            return sqlQuery;

        }
        public static string GetProbation_Competency_Name(string emp_id)
        {
            sqlQuery = string.Empty;
            sqlQuery += " select Ped.Prob_Dtl_Id,PH.COM_DESC||' - '||COM_LEVEL_DESC AS COM_DESC";
            sqlQuery += " from HR_PROBATION_EMP_DTL ped,HR_PROBATION_EMP_HDR peh,HR_PROBATION_COMP_HDR PH,HR_PROBATION_COMP_DTL PD ";
            sqlQuery += " where PH.COM_HDR_ID = PD.COM_HDR_ID ";
            sqlQuery += " and ped.com_line_id = pd.com_line_id";
            sqlQuery += " and peh.prob_id = ped.prob_id";
            sqlQuery += " and ped.com_hdr_id = ph.com_hdr_id  ";
            sqlQuery += " AND PED.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND PED.ENABLED_FLAG = 1";
            sqlQuery += " and peh.emp_id='" + emp_id + "'";
            sqlQuery += " and ph.com_org_id ='" + VMVServices.Web.Utils.OrganizationID + "'";
            return sqlQuery;

        }


        public static string GetProbation_Emp_Name(string emp_id)
        {
            sqlQuery = string.Empty;

            sqlQuery += " select peh.prob_id,peh.prob_id||' - '||e.emp_first_name as PROB_COMPETENCY";
            sqlQuery += " from HR_PROBATION_EMP_HDR peh,hr_employees e";
            sqlQuery += " where peh.workflow_completion_status = 1";
            sqlQuery += " and peh.enabled_flag = 1";
            sqlQuery += " and peh.emp_id = e.emp_id";
            sqlQuery += " and peh.emp_id = '" + emp_id + "'";
            sqlQuery += " and peh.prob_org_id = '" + VMVServices.Web.Utils.OrganizationID + "'";

            return sqlQuery;

        }



    }
}