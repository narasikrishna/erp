﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;

namespace FIN.DAL.HR
{
    public class ProbationCompetencyDAL
    {
        static string sqlQuery = "";

        public static string GetProbationdtls(String PROB_ID)
        {
            sqlQuery = string.Empty;


            sqlQuery = "  SELECT PD.COM_LINE_NUM,PD.COM_LEVEL_DESC,PD.COM_RATING,'' as DESCRIPTION,'N' AS DELETED,";
            sqlQuery += "  CASE PD.ENABLED_FLAG WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS ENABLED_FLAG,PH.COM_HDR_ID,PD.COM_LINE_ID";
            sqlQuery += "  FROM HR_PROBATION_COMP_HDR PH,HR_PROBATION_COMP_DTL PD";
            sqlQuery += "   WHERE PH.COM_HDR_ID = PD.COM_HDR_ID   ";
            sqlQuery += "   AND PH.ENABLED_FLAG = '1'  ";
            sqlQuery += "   AND PH.WORKFLOW_COMPLETION_STATUS = '1' ";
            sqlQuery += "  AND PH.COM_HDR_ID =  '" + PROB_ID + "'";
            sqlQuery += " order by PD.COM_LINE_ID asc";

            //sqlQuery = " SELECT PD.COM_LINE_NUM,PD.COM_LEVEL_DESC,PD.COM_RATING,CM.DESCRIPTION,'N' AS DELETED,";
            //sqlQuery += " CASE PD.ENABLED_FLAG WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS ENABLED_FLAG,PH.COM_HDR_ID,PD.COM_LINE_ID";
            //sqlQuery += " FROM HR_PROBATION_COMP_HDR PH,HR_PROBATION_COMP_DTL PD,SSM_CODE_MASTERS CM";
            //sqlQuery += " WHERE PH.COM_HDR_ID = PD.COM_HDR_ID";
            //sqlQuery += " AND CM.CODE = PD.COM_RATING ";
            //sqlQuery += " and CM.PARENT_CODE = 'RATING' ";
            //sqlQuery += " AND PH.ENABLED_FLAG = '1'  ";
            //sqlQuery += " AND PH.WORKFLOW_COMPLETION_STATUS = '1' ";


            return sqlQuery;

        }
    }
}
