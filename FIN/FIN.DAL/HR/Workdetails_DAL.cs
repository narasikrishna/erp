﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.HR
{
    public class Workdetails_DAL
    {
        static string sqlQuery = "";

        public static string getDesignation(String deptID)
        {
            sqlQuery = string.Empty;


            sqlQuery = " select c.category_id,c.category_code" + VMVServices.Web.Utils.LanguageCode + " as category_code,c.category_desc" + VMVServices.Web.Utils.LanguageCode + " as category_desc,c.effective_from_date,c.effective_to_date,'N' AS DELETED,";
            sqlQuery += " case c.enabled_flag when '1' then 'TRUE' else 'FALSE' END AS enabled_flag";
            sqlQuery += " from hr_categories c";
            sqlQuery += " where c.workflow_completion_status =1";
            sqlQuery += " AND C.CATEGORY_ID = '" + deptID + "'";

            return sqlQuery;

        }

    }
}
