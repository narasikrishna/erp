﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;

namespace FIN.DAL.HR
{
    public class AppraisalDefinition_DAL
    {
        static string sqlQuery = "";

        public static string fn_GetAppraisalPeriodDetails()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT HPAD.APP_ID AS APP_ID, HPAD.APP_CODE ||' - '||HPAD.APP_DESC" + VMVServices.Web.Utils.LanguageCode +" AS APP_NAME ";
            sqlQuery += " FROM HR_PER_APPRAISAL_DEF HPAD ";
            sqlQuery += " WHERE HPAD.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " AND HPAD.ENABLED_FLAG = 1 ";
            sqlQuery += " ORDER BY APP_DESC ";

            return sqlQuery;

        }

        public static string getEmpYrlyNonMgrlDtls()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM vw_hr_app_managerical_report V WHERE ROWNUM > 0 ";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["DEPT_ID"] != null)
                {
                    sqlQuery += " AND V.EMP_DEPT_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["DEPT_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["EMP_ID"] != null)
                {
                    sqlQuery += " AND V.EMP_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["EMP_ID"].ToString() + "'";
                }
            }
            return sqlQuery;
        }


        static string spOutPut = string.Empty;
        public static System.Collections.SortedList ErrorCollection = new System.Collections.SortedList();


        public static string GetSPFOR_DUPLICATE_CHECK(string P_APP_CODE, string P_RECORD_ID)
        {
            try
            {
                string retcode = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "PKG_VALIDATIONS.PROC_ERR_MGR_APP_DEF";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add("@P_MODULE_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_SCREEN_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_APP_CODE", OracleDbType.Char).Value = P_APP_CODE;
                oraCmd.Parameters.Add("@P_RECORD_ID", OracleDbType.Char).Value = P_RECORD_ID;
                oraCmd.Parameters.Add("@P_RET", OracleDbType.Varchar2, 150);
                oraCmd.Parameters["@P_RET"].Direction = ParameterDirection.Output;
                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

                retcode = oraCmd.Parameters["@P_RET"].Value.ToString();

                return retcode;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
