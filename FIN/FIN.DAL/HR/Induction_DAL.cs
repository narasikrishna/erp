﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.HR
{
    public class Induction_DAL
    {

        static string sqlQuery = "";


        public static string GetInductionDetails(string Master_id)
        {
            sqlQuery = string.Empty;

            //sqlQuery = " select hid.hr_ind_dtl_id,HID.HR_APPLICANT_ID,a.app_first_name||' '||a.app_middle_name||' '||a.app_last_name as applicant_name,'N' AS DELETED "; 
            //sqlQuery += " from hr_applicants a,hr_induction_dtl hid ";
            //sqlQuery += " where a.app_id = hid.hr_applicant_id ";
            //sqlQuery += " and hid.enabled_flag = 1 ";
            //sqlQuery += " and a.enabled_flag = 1 ";
            //sqlQuery += " and a.workflow_completion_status = 1 ";
            //sqlQuery += " AND hid.hr_ind_hdr_id =   '" + Master_id + "'";
            //sqlQuery += " ORDER BY app_id ";

            sqlQuery += " SELECT HR_IND_DTL_ID,ACTIVITY_CARRIED,REMARKS,STATUS,SCM.DESCRIPTION,IA.HR_ACTIVITY_DESC,IA.HR_IND_ACT_ID,'N' as DELETED ";
            sqlQuery += " FROM HR_INDUCTION_DTL HID,SSM_CODE_MASTERS SCM,HR_INDUCTION_ACTIVITIES IA ";
            sqlQuery += "  WHERE HID.ACTIVITY_CARRIED = IA.HR_IND_ACT_ID  AND SCM.CODE = HID.STATUS ";
            sqlQuery += " AND SCM.PARENT_CODE = 'STS' ";
            sqlQuery += " AND HID.HR_IND_HDR_ID = '" + Master_id + "' ";
            sqlQuery += " order by HID.HR_IND_DTL_ID asc ";





            //sqlQuery += " SELECT HR_IND_DTL_ID,ACTIVITY_CARRIED,REMARKS,STATUS,SCM.DESCRIPTION,'N' as DELETED FROM HR_INDUCTION_DTL HID ";
            //sqlQuery += " INNER JOIN SSM_CODE_MASTERS SCM ON SCM.CODE= HID.STATUS AND SCM.PARENT_CODE='STS' ";
            //sqlQuery += " WHERE HID.HR_IND_HDR_ID='" + Master_id + "'";
            //sqlQuery += " order by HID.HR_IND_DTL_ID asc";


            return sqlQuery;

        }
        public static string GetApplicantDetails()
        {
            sqlQuery = string.Empty;
            sqlQuery = "select a.app_id as HR_APPLICANT_ID,a.app_first_name||''||a.app_middle_name||''||a.app_last_name as HR_APPLICANT_NAME ";
            sqlQuery += "from hr_applicants a,HR_OFFER_LETTER_HDR LH where a.enabled_flag = 1 ";
           sqlQuery += "and a.workflow_completion_status = 1 ";
           sqlQuery += " and a.app_id = lh.hr_ltr_applicant_id ";

            return sqlQuery;
        }

        public static string getApplicantName(string APP_ID)
        {
            sqlQuery = string.Empty;

            sqlQuery = string.Empty;
            sqlQuery = "select a.app_first_name||''||a.app_middle_name||''||a.app_last_name as HR_APPLICANT_NAME ";
            sqlQuery += "from hr_applicants a where a.enabled_flag = 1 ";
            sqlQuery += "and a.app_id = '" + APP_ID + "'";
            sqlQuery += "and a.workflow_completion_status = 1 ";
            return sqlQuery;
        }

        public static string getActivityName()
        {
            sqlQuery = string.Empty;
            
           sqlQuery = "  select ia.hr_activity_desc,ia.hr_ind_act_id from hr_induction_activities ia ";
            sqlQuery += " where ia.enabled_flag = 1 ";
            sqlQuery += " and ia.workflow_completion_status =1 ";
            
            return sqlQuery;
        }

        public static string GetInductionActivitydtls(String HR_IND_ACT_ID)
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT IA.HR_IND_ACT_ID,IA.HR_ACTIVITY_DESC,IA.HR_EFF_FROM_DT,IA.HR_EFF_TO_DT,'N' AS DELETED,CASE IA.ENABLED_FLAG WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS ENABLED_FLAG FROM HR_INDUCTION_ACTIVITIES IA";
            
            sqlQuery += " WHERE IA.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND IA.HR_IND_ACT_ID = '" + HR_IND_ACT_ID + "'";
            return sqlQuery;

        }
    }
}
