﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;

namespace FIN.DAL.HR
{
    public class Department_DAL
    {
        static string sqlQuery = "";


        public static string GetDeptdtls(String DEPT_ID)
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT D.DEPT_ID,D.DEPT_NAME,D.DEPT_NAME_OL,V.CODE AS VALUE_KEY_ID,V.CODE AS VALUE_NAME,'N' AS DELETED,s.SEGMENT_ID,s.SEGMENT_NAME" + VMVServices.Web.Utils.LanguageCode + " ,sv.SEGMENT_VALUE_ID,sv.SEGMENT_VALUE" + VMVServices.Web.Utils.LanguageCode + ",";
            sqlQuery += "  CASE D.ENABLED_FLAG WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS ENABLED_FLAG";
            sqlQuery += " FROM HR_DEPARTMENTS D,ssm_code_masters v,GL_SEGMENTS s,GL_SEGMENT_VALUES sv";
            sqlQuery += " WHERE D.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " and d.SEGMENT_ID = s.SEGMENT_ID";
            sqlQuery += " and d.SEGMENT_VALUE_ID = sv.SEGMENT_VALUE_ID";
            sqlQuery += " and d.org_id = '" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += "  AND D.DEPT_TYPE = V.CODE  and v.parent_code='DEPT_TYP'";
            sqlQuery += " AND D.DEPT_ID = '" + DEPT_ID + "'";
            return sqlQuery;

        }

        public static string GetSecdtls(String SEC_ID)
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT HS.SEC_ID,HS.SEC_NAME,HS.SEC_NAME_OL,D.DEPT_ID,D.DEPT_NAME" + VMVServices.Web.Utils.LanguageCode + " AS DEPT_NAME,'N' AS DELETED,s.SEGMENT_ID,s.SEGMENT_NAME" + VMVServices.Web.Utils.LanguageCode + " AS SEGMENT_NAME,sv.SEGMENT_VALUE_ID,sv.SEGMENT_VALUE" + VMVServices.Web.Utils.LanguageCode + " as SEGMENT_VALUE ,";
            sqlQuery += "    CASE HS.ENABLED_FLAG WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS ENABLED_FLAG";
            sqlQuery += " FROM HR_SECTION     HS,HR_DEPARTMENTS D,GL_SEGMENTS       s,GL_SEGMENT_VALUES sv";
            sqlQuery += "  WHERE HS.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND HS.DEPT_ID = D.DEPT_ID";
            sqlQuery += " and d.SEGMENT_ID = s.SEGMENT_ID";
            sqlQuery += " and d.SEGMENT_VALUE_ID = sv.SEGMENT_VALUE_ID ";
            sqlQuery += " and HS.org_id = '" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " AND HS.SEC_ID = '" + SEC_ID + "'";
            return sqlQuery;

        }

        public static string GetDepartmentDetails()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT d.DEPT_NAME" + VMVServices.Web.Utils.LanguageCode + " AS DEPT_NAME,d.DEPT_ID ";
            sqlQuery += " FROM HR_DEPARTMENTS d ";
            sqlQuery += " WHERE d.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " and d.org_id = '" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " AND d.ENABLED_FLAG = 1 ";
            sqlQuery += " ORDER BY DEPT_NAME asc ";

            return sqlQuery;
        }

        public static string GetDepartmentDetails4Group(string  str_group)
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT d.DEPT_NAME" + VMVServices.Web.Utils.LanguageCode + " AS DEPT_NAME,d.DEPT_ID ";
            sqlQuery += " FROM HR_DEPARTMENTS d ";
            sqlQuery += " WHERE d.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " and d.org_id = '" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " and d.DEPT_TYPE='" + str_group + "'";                
            sqlQuery += " AND d.ENABLED_FLAG = 1 ";
            sqlQuery += " ORDER BY DEPT_NAME asc ";

            return sqlQuery;
        }


        public static string GetLetterDetails()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT LT.LETTER_DESC,LT.LETTER_ID  ";
            sqlQuery += " FROM SL_MST_LETTER_TEMPLATES LT ";
            //sqlQuery += " WHERE LT.ENABLED_FLAG=1 ";
            ////sqlQuery += " and d.org_id = '" + VMVServices.Web.Utils.OrganizationID + "'";
            //sqlQuery += " AND LT.WORKFLOW_COMPLETION_STATUS=1 ";
            sqlQuery += " ORDER BY LETTER_DESC asc ";

            return sqlQuery;
        }

        public static string GetDesignationName(string deptID)
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT HDD.DEPT_DESIG_ID AS DESIGN_ID,HDD.DESIG_NAME" + VMVServices.Web.Utils.LanguageCode + " AS DESIG_NAME ";
            sqlQuery += " FROM HR_DEPT_DESIGNATIONS HDD ";
            sqlQuery += " WHERE HDD.ENABLED_FLAG = 1 ";
            sqlQuery += " AND HDD.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " AND HDD.DEPT_ID = '" + deptID + "'";
            sqlQuery += " AND HDD.ORG_ID = '" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " ORDER BY HDD.DESIG_NAME ";

            return sqlQuery;
        }

        public static string GetDesignation()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT HDD.DEPT_DESIG_ID AS DESIGN_ID,HDD.DESIG_NAME" + VMVServices.Web.Utils.LanguageCode + " AS DESIG_NAME ";
            sqlQuery += " FROM HR_DEPT_DESIGNATIONS HDD ";
            sqlQuery += " WHERE HDD.ENABLED_FLAG = 1 ";
            sqlQuery += " AND HDD.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " AND HDD.ORG_ID = '" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " ORDER BY HDD.DESIG_NAME ";

            return sqlQuery;
        }
        public static string GetDepartmentType(string deptID)
        {
            sqlQuery = string.Empty;
            sqlQuery = " select hd.dept_id as dept_id,hd.dept_type as dept_type ";
            sqlQuery += " from hr_departments hd ";
            sqlQuery += " WHERE HD.ENABLED_FLAG = 1 ";
            sqlQuery += " AND HD.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " AND HD.DEPT_ID = '" + deptID + "'";
            sqlQuery += " AND HD.ORG_ID = '" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " ORDER BY HD.dept_type ";

            return sqlQuery;
        }

        public static string GetEmployeeDetails()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT HE.EMP_ID AS EMP_ID, HE.EMP_NO ||' - '|| HE.EMP_FIRST_NAME ||' '||HE.EMP_MIDDLE_NAME||' ' ||HE.EMP_LAST_NAME" + VMVServices.Web.Utils.LanguageCode + " AS EMP_NAME,'N' AS DELETED,";
            sqlQuery += "  CASE HE.ENABLED_FLAG WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS ENABLED_FLAG";
            sqlQuery += " FROM HR_EMPLOYEES HE ";
            sqlQuery += " WHERE HE.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " AND HE.EMP_ORG_ID = '" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " AND HE.ENABLED_FLAG = 1 ";
           // sqlQuery += " And HE.Emp_Internal_External = 'I'";
            sqlQuery += " ORDER BY EMP_FIRST_NAME asc ";

            return sqlQuery;
        }



        static string spOutPut = string.Empty;
        public static System.Collections.SortedList ErrorCollection = new System.Collections.SortedList();


        public static string GetSPFOR_DUPLICATE_CHECK(string P_DEPT_ID, string P_DEPT_TYPE, string P_RECORD_ID)
        {
            try
            {
                string retcode = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "PKG_VALIDATIONS.PROC_ERR_MGR_DEPARTMENT";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add("@P_MODULE_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_SCREEN_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_DEPT_ID", OracleDbType.Char).Value = P_DEPT_ID;
                oraCmd.Parameters.Add("@P_DEPT_TYPE", OracleDbType.Char).Value = P_DEPT_TYPE;
                oraCmd.Parameters.Add("@P_RECORD_ID", OracleDbType.Char).Value = P_RECORD_ID;
                oraCmd.Parameters.Add("@P_RET", OracleDbType.Varchar2, 150);
                oraCmd.Parameters["@P_RET"].Direction = ParameterDirection.Output;
                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

                retcode = oraCmd.Parameters["@P_RET"].Value.ToString();

                return retcode;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static string Get_Childdataof_Department(String DEPT_ID)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select dd.DEPT_ID ";
            sqlQuery += " from hr_dept_designations dd";
            sqlQuery += " where dd.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += "  and dd.ENABLED_FLAG = 1 ";
            sqlQuery += " and dd.dept_id = '" + DEPT_ID + "'";
            return sqlQuery;

        }

        public static string getDeptList()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT * FROM vw_dept_list V WHERE ROWNUM > 0 ";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.EFFECTIVE_FROM_DT] != null)
                {
                    sqlQuery += " AND V.CREATED_DATE between to_date('" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.EFFECTIVE_FROM_DT].ToString() + "','dd/MM/yyyy')";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.EFFECTIVE_TO_DT] != null)
                {
                    sqlQuery += " AND to_date('" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.EFFECTIVE_TO_DT].ToString() + "','dd/MM/yyyy')";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["LOOKUP_NAME"] != null)
                {
                    sqlQuery += " AND V.dept_type = '" + VMVServices.Web.Utils.ReportViewFilterParameter["LOOKUP_NAME"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["CREATED_BY"] != null)
                {
                    sqlQuery += " AND UPPER(V.CREATED_BY) = UPPER('" + VMVServices.Web.Utils.ReportViewFilterParameter["CREATED_BY"].ToString() + "')";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["MODIFIED_BY"] != null)
                {
                    sqlQuery += " AND UPPER(V.MODIFIED_BY) = UPPER('" + VMVServices.Web.Utils.ReportViewFilterParameter["MODIFIED_BY"].ToString() + "')";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["CREATED_From_Date"] != null)
                {
                    sqlQuery += " AND V.CREATED_DATE >= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["CREATED_From_Date"].ToString() + "','dd/MM/yyyy') ";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["CREATED_To_Date"] != null)
                {
                    sqlQuery += " AND V.CREATED_DATE <= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["CREATED_To_Date"].ToString() + "','dd/MM/yyyy') ";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["MODIFIED_From_Date"] != null)
                {
                    sqlQuery += " AND V.MODIFIED_DATE >= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["MODIFIED_From_Date"].ToString() + "','dd/MM/yyyy') ";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["MODIFIED_To_Date"] != null)
                {
                    sqlQuery += " AND V.MODIFIED_DATE <= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["MODIFIED_To_Date"].ToString() + "','dd/MM/yyyy') ";
                }
            }

            return sqlQuery;
        }

        public static string get_ManpowerPlanReport()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_MANPOWER_PLAN V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["DEPT_ID"] != null)
                {
                    sqlQuery += " AND V.DEPT_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["DEPT_ID"].ToString() + "'";
                }
                //if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.EFFECTIVE_FROM_DT] != null)
                //{
                //    sqlQuery += " AND V.EMP_DOJ between to_date('" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.EFFECTIVE_FROM_DT].ToString() + "','dd/MM/yyyy') )";
                //}
                //if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.EFFECTIVE_TO_DT] != null)
                //{
                //    sqlQuery += " AND to_date('" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.EFFECTIVE_TO_DT].ToString() + "','dd/MM/yyyy') )";
                //}
            }
            return sqlQuery;
        }

        public static string get_LetterTemplateReport()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_LETTER_TEMPLATE V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["LETTER_ID"] != null)
                {
                    sqlQuery += " AND V.LETTER_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["LETTER_ID"].ToString() + "'";
                }
                //if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.EFFECTIVE_FROM_DT] != null)
                //{
                //    sqlQuery += " AND V.EMP_DOJ between to_date('" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.EFFECTIVE_FROM_DT].ToString() + "','dd/MM/yyyy') )";
                //}
                //if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.EFFECTIVE_TO_DT] != null)
                //{
                //    sqlQuery += " AND to_date('" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.EFFECTIVE_TO_DT].ToString() + "','dd/MM/yyyy') )";
                //}
            }
            return sqlQuery;
        }


        public static string get_EmpDtlsByender()
        {
            sqlQuery = string.Empty;
            sqlQuery = " select * from HR_VW_EMP_GENDER  V where ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter["dept_id"] != null)
            {
                sqlQuery += " AND V.dept_id = '" + VMVServices.Web.Utils.ReportViewFilterParameter["dept_id"].ToString() + "'";
            }
            if (VMVServices.Web.Utils.ReportViewFilterParameter["emp_gender"] != null)
            {
                sqlQuery += " AND V.emp_gender = '" + VMVServices.Web.Utils.ReportViewFilterParameter["emp_gender"].ToString() + "'";
            }
            sqlQuery += "order by v.emp_no asc";
             return sqlQuery;
        }
       


    }
}