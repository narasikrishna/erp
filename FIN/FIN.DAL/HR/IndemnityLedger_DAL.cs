﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;

namespace FIN.DAL.HR
{
    public class IndemnityLedger_DAL
    {
        static string sqlQuery = "";


        public static string IndemnityLedgerDtls(string deptId, string empId, string transDate)
        {
            sqlQuery = string.Empty;

            sqlQuery += " SELECT round(IL.INDEM_AMT_BAL,3) as INDEM_AMT_BAL,trunc(IL.INDEM_TXN_DATE) as INDEM_TXN_DATE,round(IL.INDEM_SERVICE_YEARS,3) as INDEM_SERVICE_YEARS,round(IL.INDEM_ELIGIBLE_DAYS,3) as INDEM_ELIGIBLE_DAYS";
            sqlQuery += " FROM HR_INDEM_TXN_LEDGER IL";
            sqlQuery += " WHERE IL.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND IL.INDEM_DEPT_ID = '" + deptId + "'";
            sqlQuery += " AND IL.INDEM_EMP_ID = '" + empId + "'";
            sqlQuery += " AND IL.INDEM_TXN_DATE <= to_date('" + transDate + "','dd/MM/yyyy')";
            return sqlQuery;

        }

        public static string IndemnityProvisionToDate(string period_id)
        {
            sqlQuery = string.Empty;

            sqlQuery += " SELECT pp.pay_to_dt FROM pay_periods pp";

            sqlQuery += " WHERE pp.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " and pp.pay_period_id='" + period_id + "'";

            return sqlQuery;

        }

        public static string IndemnityProvisionDepartment()
        {
            sqlQuery = string.Empty;

            sqlQuery += " SELECT D.DEPT_ID FROM HR_DEPARTMENTS D";

            sqlQuery += " WHERE D.WORKFLOW_COMPLETION_STATUS=1 AND D.ENABLED_FLAG=1";
            sqlQuery += " and d.org_id = '" + VMVServices.Web.Utils.OrganizationID + "'";

            return sqlQuery;

        }

        public static string IndemnityProvisionEmployee(string dept_id)
        {
            sqlQuery = string.Empty;

            sqlQuery += " SELECT H.EMP_ID,H.EMP_FIRST_NAME || '' || H.EMP_MIDDLE_NAME || '' || H.EMP_LAST_NAME AS EMP_NAME ";
            sqlQuery += " FROM HR_EMPLOYEES H, HR_EMP_WORK_DTLS ED, HR_DEPARTMENTS D ";
            sqlQuery += " WHERE D.DEPT_ID = ED.EMP_DEPT_ID ";
            sqlQuery += " AND ED.EMP_ID = H.EMP_ID ";
            sqlQuery += " AND H.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " AND H.ENABLED_FLAG = 1 ";
            sqlQuery += " AND D.DEPT_ID = '" + dept_id + "'";
            sqlQuery += " and H.EMP_ORG_ID = '" + VMVServices.Web.Utils.OrganizationID + "'";

            return sqlQuery;

        }

        public static string IndemnityProvision(string To_Date)
        {
            sqlQuery = string.Empty;

            sqlQuery += " SELECT HL.INDEM_EMP_ID,E.EMP_NO, E.EMP_FIRST_NAME||' '||E.EMP_MIDDLE_NAME||' '||E.EMP_LAST_NAME AS EMPLOYEE_NAME, TO_CHAR(ROUND(HL.INDEM_AMT_BAL," + VMVServices.Web.Utils.DecimalPrecision + ")) AS CURRENT_BALANCE, ";
            sqlQuery += " TO_CHAR(ROUND(HL.INDEM_OPEN_BAL," + VMVServices.Web.Utils.DecimalPrecision + ")) AS PERVIOUS_BALANCE ";
            sqlQuery += " ,TO_CHAR(ROUND(HL.indem_amt_avail," + VMVServices.Web.Utils.DecimalPrecision + ")) AS CURRENT_ACCUMULATION  ";
            //,TO_CHAR(ROUND((NVL(INDEM_AMT_BAL, 0) - NVL(INDEM_OPEN_BAL, 0))," + VMVServices.Web.Utils.DecimalPrecision + ")) AS CURRENT_ACCUMULATION ";
            sqlQuery += " FROM HR_INDEM_TXN_LEDGER HL,HR_EMPLOYEES E  ";
            sqlQuery += " where HL.indem_txn_date = to_date('" + To_Date + "','dd/MM/yyyy')";
            sqlQuery += " and  HL.INDEM_EMP_ID = E.EMP_ID ";
            sqlQuery += " AND HL.ENABLED_FLAG = 1 ";
            // sqlQuery += " AND HL.DEPT_ID = '" + dept_id + "'";
            sqlQuery += " and HL.INDEM_ORG_ID = '" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " ORDER BY to_number(EMP_NO)";
            // AND IL.INDEM_TXN_DATE <= to_date('" + transDate + "','dd/MM/yyyy')
            return sqlQuery;

        }

        public static void GetSP_IndemnityProvision(string to_date)
        {
            try
            {
                string strValue = string.Empty;
                string strNextCode = string.Empty;
                string strErrMsg = string.Empty;

                DateTime dt_tmp = Convert.ToDateTime(to_date);

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "HR_PKG.HR_INDEMNITY_PROVOSION";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add(new OracleParameter("@p_org_id", OracleDbType.Varchar2, 250)).Value = VMVServices.Web.Utils.OrganizationID;
                oraCmd.Parameters.Add(new OracleParameter("@p_date", OracleDbType.Date, 250)).Value = dt_tmp.ToString("dd/MMM/yyyy");
                //oraCmd.Parameters.Add(new OracleParameter("@p_unposted", OracleDbType.Varchar2, 250)).Value = str_unposted;



                DBMethod.ExecuteStoredProcedure(oraCmd);



            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static void GetSP_IndemnityPaymentPosting(string hdDate)
        {
            try
            {
                string strValue = string.Empty;
                string strNextCode = string.Empty;
                string strErrMsg = string.Empty;

                DateTime dt_tmp = Convert.ToDateTime(hdDate);

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "GL_POSTING.INDEM_PAYMENT_POSTING";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add(new OracleParameter("@p_org_id", OracleDbType.Varchar2, 250)).Value = VMVServices.Web.Utils.OrganizationID;
                oraCmd.Parameters.Add(new OracleParameter("@p_date", OracleDbType.Date, 250)).Value = dt_tmp.ToString("dd/MMM/yyyy");
                //oraCmd.Parameters.Add(new OracleParameter("@p_unposted", OracleDbType.Varchar2, 250)).Value = str_unposted;

                DBMethod.ExecuteStoredProcedure(oraCmd);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static void GetSP_IndemnityProvisionPosting(string  hdDate)
        {
            try
            {
                string strValue = string.Empty;
                string strNextCode = string.Empty;
                string strErrMsg = string.Empty;

                DateTime dt_tmp = Convert.ToDateTime(hdDate);

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "GL_POSTING.INDEM_PROVISION_POSTING";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add(new OracleParameter("@p_org_id", OracleDbType.Varchar2, 250)).Value = VMVServices.Web.Utils.OrganizationID;
                oraCmd.Parameters.Add(new OracleParameter("@p_date", OracleDbType.Date, 250)).Value = dt_tmp.ToString("dd/MMM/yyyy");
                //oraCmd.Parameters.Add(new OracleParameter("@p_unposted", OracleDbType.Varchar2, 250)).Value = str_unposted;
                
                DBMethod.ExecuteStoredProcedure(oraCmd);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void GetSP_LeaveProvisionPosting(string hdDate)
        {
            try
            {
                string strValue = string.Empty;
                string strNextCode = string.Empty;
                string strErrMsg = string.Empty;
                DateTime dt_tmp = Convert.ToDateTime(hdDate);
                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "GL_POSTING.LEAVE_PROVISION_POSTING";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add(new OracleParameter("@p_org_id", OracleDbType.Varchar2, 250)).Value = VMVServices.Web.Utils.OrganizationID;
                oraCmd.Parameters.Add(new OracleParameter("@p_date", OracleDbType.Date, 250)).Value = dt_tmp.ToString("dd/MMM/yyyy");
                //oraCmd.Parameters.Add(new OracleParameter("@p_unposted", OracleDbType.Varchar2, 250)).Value = str_unposted;

                DBMethod.ExecuteStoredProcedure(oraCmd);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
