﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;

namespace FIN.DAL.HR
{
    public class LeaveCondition_DAL
    {
        static string sqlQuery = "";

        public static string GetLeaveconditiondtls(string Master_id)
        {
            sqlQuery = string.Empty;

            sqlQuery += " SELECT LC.LC_DTL_ID,LC.LC_HIGH_VALUE,LC.LC_LOW_VALUE,LC.LC_VALUE,LC.LC_FORMULA,LC.EFFECTIVE_FROM_DT,LC.EFFECTIVE_TO_DT,";
            sqlQuery += " C.CODE AS LOOKUP_ID,C.CODE AS LOOKUP_NAME,CASE LC.ENABLED_FLAG WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS ENABLED_FLAG,'N' AS DELETED,";
            sqlQuery += " (select con.code from SSM_CODE_MASTERS con where lc.lc_condition = con.code) as condition_id,";
            sqlQuery += " (select con.code from SSM_CODE_MASTERS con where lc.lc_condition = con.code) as condition_name";
            sqlQuery += " FROM HR_LEAVE_CONDITIONS_DTL LC,SSM_CODE_MASTERS C";
            sqlQuery += " WHERE LC.LC_TYPE = C.CODE";
            sqlQuery += " AND LC.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND LC.LC_HDR_ID = '" + Master_id + "'";
            sqlQuery += " order by LC_DTL_ID ";
            return sqlQuery;

        }

        public static string GetLeaveconditiondtls_basedon_leave(string lc_leave_id)
        {
            sqlQuery = string.Empty;

            sqlQuery += " SELECT LC.LC_DTL_ID,LC.LC_HIGH_VALUE,LC.LC_LOW_VALUE,LC.LC_VALUE,LC.LC_FORMULA,LC.EFFECTIVE_FROM_DT,LC.EFFECTIVE_TO_DT,";
            sqlQuery += " C.CODE AS LOOKUP_ID,C.CODE AS LOOKUP_NAME,CASE LC.ENABLED_FLAG WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS ENABLED_FLAG,'N' AS DELETED,";
            sqlQuery += " (select con.code from SSM_CODE_MASTERS con where lc.lc_condition = con.code) as condition_id,";
            sqlQuery += " (select con.code from SSM_CODE_MASTERS con where lc.lc_condition = con.code) as condition_name";
            sqlQuery += " FROM HR_LEAVE_CONDITIONS_DTL LC,SSM_CODE_MASTERS C,HR_LEAVE_CONDITIONS_HDR lh";
            sqlQuery += " WHERE LC.LC_TYPE = C.CODE";
            sqlQuery += " AND LC.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND Lh.Lc_Hdr_Id = lc.lc_hdr_id";
            sqlQuery += " and lh.lc_leave_id =  '" + lc_leave_id + "'";
            return sqlQuery;

        }



    }
}
