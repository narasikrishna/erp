﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;

namespace FIN.DAL.HR
{
    public class GradeLevel_DAL
    {
        static string sqlQuery = "";

        public static string GetGradeLeveldtls(String GRADE_LEVEL_ID)
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT GL.GRADE_LEVEL_ID,G.GRADE_ID,G.GRADE_DESC" + VMVServices.Web.Utils.LanguageCode + " as GRADE_DESC,P.PAY_ELEMENT_ID,P.PAY_ELEMENT_DESC as PAY_ELEMENT,to_char(GL.LOW_LIMIT) as LOW_LIMIT,to_char(GL.HIGH_LIMIT) as HIGH_LIMIT,to_char(GL.INCREMENT_AMT) as INCREMENT_AMT,to_char(GL.AVERAGE_AMT) as AVERAGE_AMT,GL.EFFECTIVE_FROM_DT,GL.EFFECTIVE_TO_DT,";
            sqlQuery += " C.CATEGORY_ID,c.CATEGORY_DESC" + VMVServices.Web.Utils.LanguageCode + " as CATEGORY_DESC,J.JOB_ID,J.JOB_DESC" + VMVServices.Web.Utils.LanguageCode + " as JOB_DESC,PS.POSITION_ID,PS.POSITION_DESC"+VMVServices.Web.Utils.LanguageCode+" as POSITION_DESC,";
            sqlQuery += " CASE GL.ENABLED_FLAG WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS ENABLED_FLAG,'N' AS DELETED";
            sqlQuery += " FROM HR_GRADES_LEVEL_DTLS GL,HR_GRADES G,PAY_ELEMENTS P,HR_POSITIONS PS,HR_JOBS J,HR_CATEGORIES C";
            sqlQuery += " WHERE GL.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND G.POSITION_ID = PS.POSITION_ID";
            sqlQuery += " AND PS.JOB_ID = J.JOB_ID";
            sqlQuery += " AND J.CATEGORY_ID = C.CATEGORY_ID";
            sqlQuery += " AND GL.GRADE_ID = G.GRADE_ID";
            sqlQuery += " AND GL.PAY_ELEMENT_ID = P.PAY_ELEMENT_ID";
            sqlQuery += " AND GL.GRADE_LEVEL_ID = '" + GRADE_LEVEL_ID + "'";

            return sqlQuery;

        }

        public static string GetGradeLeveldtls4Validation(String GRADE_ID,String POSITION_ID,String ELEMENT_ID,String JOB_ID,String CATEGORY_ID,string GRADE_LEVEL_ID)
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT Count(*) as Counts FROM HR_GRADES_LEVEL_DTLS GL,HR_GRADES G,PAY_ELEMENTS P,HR_POSITIONS PS,HR_JOBS J,HR_CATEGORIES C";
            sqlQuery += " WHERE GL.WORKFLOW_COMPLETION_STATUS = 1 AND G.POSITION_ID = PS.POSITION_ID AND PS.JOB_ID = J.JOB_ID";
            sqlQuery += " AND J.CATEGORY_ID = C.CATEGORY_ID AND GL.GRADE_ID = G.GRADE_ID AND GL.PAY_ELEMENT_ID = P.PAY_ELEMENT_ID";
            sqlQuery += " and GL.Grade_Id = '" + GRADE_ID +"' and P.Pay_Element_Id = '" + ELEMENT_ID +"'";
            sqlQuery += " and C.Category_Id = '" + CATEGORY_ID +"' and J.Job_Id = '" + JOB_ID + "' and PS.Position_Id = '" + POSITION_ID + "'";
            sqlQuery += " and GL.GRADE_LEVEL_ID !='" + GRADE_LEVEL_ID + "'";
            return sqlQuery;

        }


    }
}
