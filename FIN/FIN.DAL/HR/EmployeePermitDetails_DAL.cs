﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;


namespace FIN.DAL.HR
{
    public class EmployeePermitDetails_DAL
    {
        static string sqlQuery = "";

        public static string getEmployeePermitExpiry4Period(string str_Period_id)
        {
            sqlQuery = string.Empty;
            sqlQuery += " SELECT DEPT.DEPT_ID,DEPT.DEPT_NAME,COUNT(*) as No_Of_Emp FROM GL_ACCT_PERIOD_DTL APD,HR_EMPLOYEES EMP ";
            sqlQuery += " INNER JOIN HR_EMP_WORK_DTLS EWD ON EWD.EMP_ID=EMP.EMP_ID AND EWD.EFFECTIVE_TO_DT IS NULL ";
            sqlQuery += " INNER JOIN HR_DEPARTMENTS DEPT ON DEPT.DEPT_ID= EWD. EMP_DEPT_ID ";
            sqlQuery += " INNER JOIN HR_EMP_PASSPORT_DETAILS EPD ON EPD.PASSPORT_EMP_ID = EMP.EMP_ID AND EPD.ID_TYPE ='Civil ID' ";
            sqlQuery += " WHERE EMP.ENABLED_FLAG=1 AND EMP.TERMINATION_DATE IS NULL ";
            sqlQuery += " AND EPD.PASSPORT_EXPIRY_DATE >= APD.PERIOD_FROM_DT AND EPD.PASSPORT_EXPIRY_DATE <= APD.PERIOD_TO_DT ";
            sqlQuery += " AND APD.PERIOD_ID='" + str_Period_id + "' ";
            sqlQuery += " GROUP BY DEPT.DEPT_ID,DEPT.DEPT_NAME,DEPT.ATTRIBUTE1 ";
            sqlQuery += " ORDER BY TO_NUMBER(DEPT.ATTRIBUTE1) ";
            return sqlQuery;

        }

        public static string getEmployeePermitExpiry4PeriodDept(string str_Period_id,string str_Dept_Id)
        {
            sqlQuery = string.Empty;
            string strLngCode = VMVServices.Web.Utils.LanguageCode;
            sqlQuery += " SELECT DEPT.DEPT_ID,DEPT.DEPT_NAME,EMP.EMP_NO,EPD.PASSPORT_EXPIRY_DATE AS EXPIRY_DATE ";
            sqlQuery += " ,(EMP.emp_first_name" + strLngCode + "||' '||EMP.emp_middle_name" + strLngCode + "||' '||EMP.emp_last_name" + strLngCode + ") EMP_NAME ";
            sqlQuery += " FROM GL_ACCT_PERIOD_DTL APD,HR_EMPLOYEES EMP ";
            sqlQuery += " INNER JOIN HR_EMP_WORK_DTLS EWD ON EWD.EMP_ID=EMP.EMP_ID AND EWD.EFFECTIVE_TO_DT IS NULL ";
            sqlQuery += " INNER JOIN HR_DEPARTMENTS DEPT ON DEPT.DEPT_ID= EWD. EMP_DEPT_ID ";
            sqlQuery += " INNER JOIN HR_EMP_PASSPORT_DETAILS EPD ON EPD.PASSPORT_EMP_ID = EMP.EMP_ID AND EPD.ID_TYPE ='Civil ID' ";
            sqlQuery += " WHERE EMP.ENABLED_FLAG=1 AND EMP.TERMINATION_DATE IS NULL ";
            sqlQuery += " AND EPD.PASSPORT_EXPIRY_DATE >= APD.PERIOD_FROM_DT AND EPD.PASSPORT_EXPIRY_DATE <= APD.PERIOD_TO_DT ";
            sqlQuery += " AND APD.PERIOD_ID='" + str_Period_id + "' ";
            sqlQuery += " AND DEPT.DEPT_ID='" + str_Dept_Id + "'";
           // sqlQuery += " GROUP BY DEPT.DEPT_ID,DEPT.DEPT_NAME,DEPT.ATTRIBUTE1 ";
            sqlQuery += " ORDER BY EPD.PASSPORT_EXPIRY_DATE ";
            return sqlQuery;

        }

        public static string GetEmployeeDetails()
        {
            sqlQuery = string.Empty;
            sqlQuery = " select HE.EMP_ID,HE.EMP_NO||' - '||HE.EMP_FIRST_NAME|| ' ' || HE.EMP_MIDDLE_NAME || ' ' ||HE.EMP_LAST_NAME AS EMP_NO from HR_EMPLOYEES HE  ";
            sqlQuery += " where HE.ENABLED_FLAG = '1' ";
            sqlQuery += " AND HE.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " AND HE.EMP_ORG_ID = '" + VMVServices.Web.Utils.OrganizationID + "'";
            //  sqlQuery += " And HE.EMP_INTERNAL_EXTERNAL = 'I'";
            sqlQuery += " ORDER BY HE.EMP_NO asc ";

            return sqlQuery;
        }

        public static string GetEmployeeName(string Emp_id)
        {
            sqlQuery = string.Empty;
            sqlQuery = " select HE.EMP_FIRST_NAME" + VMVServices.Web.Utils.LanguageCode + " AS EMP_FIRST_NAME from HR_EMPLOYEES HE  ";
            sqlQuery += " where HE.ENABLED_FLAG = '1' ";
            sqlQuery += " AND HE.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " AND HE.EMP_ORG_ID = '" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " and HE.EMP_ID = '" + Emp_id + "'";
            sqlQuery += " ORDER BY EMP_FIRST_NAME asc ";

            return sqlQuery;
        }
    }
}
