﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;


namespace FIN.DAL.HR
{
    public class Employee_DAL
    {

        static string sqlQuery = "";

        public static string get_EmpDetails_With_ReportingTo()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_EMP_REPORTING_TO_DET V WHERE ROWNUM > 0 ";
            sqlQuery += " AND V.org_id='" + VMVServices.Web.Utils.OrganizationID + "'";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null && VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
                {
                    sqlQuery += " AND V.EMP_DOJ between to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy') ";

                    sqlQuery += " AND to_date('" + VMVServices.Web.Utils.ReportFilterParameter["To_Date"].ToString() + "','dd/MM/yyyy') ";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["dept_id"] != null)
                {
                    sqlQuery += " AND v.dept_id='" + VMVServices.Web.Utils.ReportViewFilterParameter["dept_id"] + "'";
                }
            }
            return sqlQuery;
        }


        public static string GetEmployeeDetails_all()
        {
            sqlQuery = string.Empty;

            string strLngCode = VMVServices.Web.Utils.LanguageCode;
            sqlQuery = " select t.emp_id,(t.emp_no|| ' - ' ||t.emp_first_name" + strLngCode + "||' '||t.emp_middle_name" + strLngCode + "||' '||t.emp_last_name" + strLngCode + ") emp_name,t.emp_no ";
            sqlQuery += " from hr_employees t";
            sqlQuery += " where t.workflow_completion_status=1";
            sqlQuery += " and t.enabled_flag=1";
            sqlQuery += " and t.emp_org_id='" + VMVServices.Web.Utils.OrganizationID + "'";
            //  sqlQuery += " and t.termination_date is null ";
            //sqlQuery += " and t.emp_internal_external  = 'I'";
            sqlQuery += " order by t.emp_no asc";

            return sqlQuery;
        }


        public static string get_EmpDetails_4SelectedFilter()
        {
            sqlQuery = string.Empty;
            string str_sel_filter = VMVServices.Web.Utils.ReportViewFilterParameter["RECORDTYPE"].ToString();

            string strLngCode = VMVServices.Web.Utils.LanguageCode;

            sqlQuery += " CREATE OR REPLACE VIEW VW_EMP_DET_WITH_FILTER AS ";
            sqlQuery += " SELECT EMP.EMP_NO,EMP.EMP_FIRST_NAME" + strLngCode + " ||' '||EMP.emp_middle_name" + strLngCode + "||' '||EMP.emp_last_name AS EMP_NAME ";
            if (str_sel_filter.ToUpper() == "DEPARTMENT")
            {
                sqlQuery += " ,DEPT.DEPT_NAME" + strLngCode + " AS SEL_COL ";
            }
            else if (str_sel_filter.ToUpper() == "DESIGNATION")
            {
                sqlQuery += " ,DESIG.DESIG_NAME" + strLngCode + "  AS SEL_COL ";
            }
            else if (str_sel_filter.ToUpper() == "CATEGORY")
            {
                sqlQuery += " ,CAT.CATEGORY_DESC" + strLngCode + "  AS SEL_COL ";
            }
            else if (str_sel_filter.ToUpper() == "GRADE")
            {
                sqlQuery += " ,G.GRADE_DESC" + strLngCode + "  AS SEL_COL ";
            }
            else if (str_sel_filter.ToUpper() == "JOB")
            {
                sqlQuery += " ,J.JOB_DESC" + strLngCode + "  AS SEL_COL ";
            }
            else if (str_sel_filter.ToUpper() == "POSITION")
            {
                sqlQuery += " ,P.POSITION_DESC" + strLngCode + "  AS SEL_COL ";
            }

            sqlQuery += " FROM HR_EMPLOYEES EMP ";
            sqlQuery += " INNER JOIN HR_EMP_WORK_DTLS EWD ON EWD.EMP_ID= EMP.EMP_ID AND EWD.EFFECTIVE_TO_DT IS NULL ";

            if (str_sel_filter.ToUpper() == "DEPARTMENT")
            {
                sqlQuery += " INNER JOIN HR_DEPARTMENTS DEPT ON DEPT.DEPT_ID= EWD.EMP_DEPT_ID ";
            }
            else if (str_sel_filter.ToUpper() == "DESIGNATION")
            {
                sqlQuery += " INNER JOIN HR_DEPT_DESIGNATIONS DESIG ON DESIG.DEPT_DESIG_ID = EWD.EMP_DESIG_ID ";
            }
            else if (str_sel_filter.ToUpper() == "CATEGORY")
            {
                sqlQuery += " INNER JOIN HR_CATEGORIES CAT ON CAT.CATEGORY_ID= EWD.EMP_CATEGORY ";
            }
            else if (str_sel_filter.ToUpper() == "GRADE")
            {
                sqlQuery += " INNER JOIN HR_GRADES G ON G.GRADE_ID= EWD.EMP_GRADE_ID ";
            }
            else if (str_sel_filter.ToUpper() == "JOB")
            {
                sqlQuery += " INNER JOIN HR_JOBS J ON J.JOB_ID = EWD.EMP_JOB_ID ";
            }
            else if (str_sel_filter.ToUpper() == "POSITION")
            {
                sqlQuery += " INNER JOIN HR_POSITIONS P ON P.POSITION_ID = EWD.EMP_POSITION_ID ";
            }
            sqlQuery += " WHERE EMP.ENABLED_FLAG=1 ";
            sqlQuery += " AND EMP.EMP_ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " ORDER BY SEL_COL ";


            return sqlQuery;
        }

        public static string get_Emp_Salary_Certificate(string str_emp_id)
        {

            sqlQuery = string.Empty;


            sqlQuery = " SELECT * FROM VW_EMP_SAL_CER V WHERE ROWNUM > 0 ";
            sqlQuery += " AND V.org_id='" + VMVServices.Web.Utils.OrganizationID + "'";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["EMP_ID"] != null)
                {
                    sqlQuery += " AND V.EMP_ID ='" + VMVServices.Web.Utils.ReportFilterParameter["EMP_ID"].ToString() + "' ";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["Dept_Id"] != null)
                {
                    sqlQuery += " AND V.EMP_DEPT_ID ='" + VMVServices.Web.Utils.ReportFilterParameter["Dept_Id"].ToString() + "' ";
                }
            }

            return sqlQuery;
        }


        public static string get_EmpDetails_4SelectedFilter_data()
        {
            sqlQuery = "select * from VW_EMP_DET_WITH_FILTER ";
            return sqlQuery;
        }

        public static string getEmpAddress()
        {
            sqlQuery = "";
            sqlQuery += " select * from VW_HR_EMPLOYEE_ADDRESS";
            return sqlQuery;
        }
        public static string getEmpSAL()
        {
            sqlQuery = "";
            sqlQuery += " select * from VW_HR_EMPLOYEE_PAY";
            return sqlQuery;
        }

        public static string getEmpEDU()
        {
            sqlQuery = "";
            sqlQuery += " select * from VW_HR_EMPLOYEE_EDU";
            return sqlQuery;
        }

        public static string getEmpQUA()
        {
            sqlQuery = "";
            sqlQuery += " select * from vw_hr_emp_qualification";
            return sqlQuery;
        }


        public static string getEmpStopPaymentReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_EMP_STOP_PAYMENT V WHERE ROWNUM > 0 ";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null && VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
                {
                    sqlQuery += " AND V.EMP_STOP_FROM_DT between to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy') ";

                    sqlQuery += " AND to_date('" + VMVServices.Web.Utils.ReportFilterParameter["To_Date"].ToString() + "','dd/MM/yyyy') ";
                }
            }

            return sqlQuery;
        }


        public static string get_EmpDetailsWithBasic()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_EMP_DETAILS_WITH_BASIC V WHERE ROWNUM > 0 ";
            sqlQuery += " AND V.org_id='" + VMVServices.Web.Utils.OrganizationID + "'";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["DEPT_ID"] != null)
                {
                    sqlQuery += " AND V.DEPT_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["DEPT_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["EMP_ID"] != null)
                {
                    sqlQuery += " AND V.EMP_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["EMP_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
                {
                    sqlQuery += " AND V.EMP_DOJ between to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy') ";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
                {
                    sqlQuery += " AND to_date('" + VMVServices.Web.Utils.ReportFilterParameter["To_Date"].ToString() + "','dd/MM/yyyy') ";
                }
            }
            return sqlQuery;
        }

        public static string getEmployeeLeaveDetails(string str_Period_id)
        {
            sqlQuery = "";
            sqlQuery += " SELECT DEPT.DEPT_ID,DEPT.DEPT_NAME,COUNT(*) AS No_Of_Emp FROM  GL_ACCT_PERIOD_DTL APD,HR_LEAVE_APPLICATIONS LA ";
            sqlQuery += " INNER JOIN HR_EMPLOYEES EMP ON EMP.EMP_ID= LA.STAFF_ID ";
            sqlQuery += " INNER JOIN HR_DEPARTMENTS DEPT ON DEPT.DEPT_ID= LA.DEPT_ID ";
            sqlQuery += " WHERE LA.LEAVE_DATE_FROM >= APD.PERIOD_FROM_DT AND LA.LEAVE_DATE_FROM <= APD.PERIOD_TO_DT ";
            sqlQuery += " AND APD.PERIOD_ID='" + str_Period_id + "'";
            sqlQuery += " AND EMP.EMP_ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " GROUP BY DEPT.DEPT_ID,DEPT.DEPT_NAME ";
            return sqlQuery;
        }

        public static string getEmployDistributionList()
        {
            sqlQuery = "";
            sqlQuery += " SELECT EMP_EXPI,COUNT(EMP_EXPI) No_Of_Emp FROM ( ";
            sqlQuery += " SELECT floor( MONTHS_BETWEEN(sysdate,emp_doj) /12) AS EMP_EXPI ";
            sqlQuery += " FROM hr_employees ";
            sqlQuery += " WHERE enabled_flag    =1 ";
            sqlQuery += " AND termination_date IS NULL  ";
            sqlQuery += " AND EMP_ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " )z GROUP BY EMP_EXPI ORDER BY EMP_EXPI ";
            return sqlQuery;
        }

        public static string getEmployAgeDetails()
        {
            sqlQuery = "";
            sqlQuery += " select '21 - 30' as AgeDiff, count(*) as NoOfEmp from ( ";
            sqlQuery += " SELECT floor( MONTHS_BETWEEN(sysdate,emp_doB) /12) as emp_age ";
            sqlQuery += " FROM hr_employees where enabled_flag=1 and termination_date is null ";
            sqlQuery += " AND EMP_ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += "  ) Z where emp_age >= 21 and emp_age<= 30 ";
            sqlQuery += " UNION ";
            sqlQuery += " select '31 - 40' as AgeDiff, count(*) as NoOfEmp from ( ";
            sqlQuery += " SELECT floor( MONTHS_BETWEEN(sysdate,emp_doB) /12) as emp_age ";
            sqlQuery += "  FROM hr_employees where enabled_flag=1 and termination_date is null ";
            sqlQuery += " AND EMP_ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " ) Z where emp_age >= 31 and emp_age<= 40 ";
            sqlQuery += "  UNION ";
            sqlQuery += " select '41 - 50' as AgeDiff, count(*) as NoOfEmp from ( ";
            sqlQuery += " SELECT floor( MONTHS_BETWEEN(sysdate,emp_doB) /12) as emp_age ";
            sqlQuery += " FROM hr_employees  where enabled_flag=1 and termination_date is null ";
            sqlQuery += " AND EMP_ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " ) Z where emp_age >= 41 and emp_age<= 50 ";
            sqlQuery += " UNION ";
            sqlQuery += " select '51 - 60' as AgeDiff, count(*) as NoOfEmp from ( ";
            sqlQuery += " SELECT floor( MONTHS_BETWEEN(sysdate,emp_doB) /12) as emp_age ";
            sqlQuery += "  FROM hr_employees where enabled_flag=1 and termination_date is null ";
            sqlQuery += " AND EMP_ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " ) Z where emp_age >= 51 and emp_age<= 60 ";
            sqlQuery += " UNION ";
            sqlQuery += " select 'Above 61' as AgeDiff, count(*) as NoOfEmp from ( ";
            sqlQuery += " SELECT floor( MONTHS_BETWEEN(sysdate,emp_doB) /12) as emp_age ";
            sqlQuery += "  FROM hr_employees where enabled_flag=1 and termination_date is null ";
            sqlQuery += " AND EMP_ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += "  ) Z where emp_age >= 61 ";
            return sqlQuery;
        }

        public static string getEmpCount4FinYear(string str_finyear)
        {
            sqlQuery = "";
            sqlQuery += "  SELECT 1 as row_number ";
            sqlQuery += " ,(SELECT COUNT(*) FROM HR_EMPLOYEES EMP WHERE  EMP.EMP_DOJ <= ACD.CALL_EFF_END_DT AND  EMP.TERMINATION_DATE IS NULL ) AS TOTAL_EMP ";
            sqlQuery += " ,(SELECT COUNT(*) FROM HR_EMPLOYEES EMP WHERE EMP.EMP_DOJ >= ACD.CAL_EFF_START_DT AND EMP.EMP_DOJ <= ACD.CALL_EFF_END_DT ) AS NEW_JOIN_EMP ";
            sqlQuery += " ,(SELECT COUNT(*) FROM HR_EMPLOYEES EMP WHERE EMP.CONFIRMATION_DATE >= ACD.CAL_EFF_START_DT AND EMP.CONFIRMATION_DATE <= ACD.CALL_EFF_END_DT ) AS CONFIRMED_EMP ";
            sqlQuery += " ,(SELECT COUNT(*) FROM HR_EMPLOYEES EMP WHERE EMP.TERMINATION_DATE >= ACD.CAL_EFF_START_DT AND EMP.TERMINATION_DATE <= ACD.CALL_EFF_END_DT ) AS TERMINATION_EMP ";
            sqlQuery += " FROM GL_ACCT_CALENDAR_DTL ACD ";
            sqlQuery += " WHERE ACD.CAL_DTL_ID='" + str_finyear + "'";
            return sqlQuery;

        }

        public static string GetEmployeeDetails4Dashbord()
        {
            string strLngCode = VMVServices.Web.Utils.LanguageCode;
            sqlQuery = "";
            sqlQuery += " SELECT DEPT.DEPT_ID, DEPT.DEPT_NAME" + strLngCode + " as DEPT_NAME ,DD.DEPT_DESIG_ID,DD.DESIG_NAME" + strLngCode + " as DESIG_NAME, EMP.EMP_ID,EMP.EMP_NO ";
            sqlQuery += " ,(EMP.emp_first_name" + strLngCode + "||' '||EMP.emp_middle_name" + strLngCode + "||' '||EMP.emp_last_name" + strLngCode + ") EMP_NAME ";
            sqlQuery += " FROM HR_EMP_WORK_DTLS EWD ";
            sqlQuery += " INNER JOIN HR_EMPLOYEES EMP ON EMP.EMP_ID= EWD.EMP_ID ";
            sqlQuery += " INNER JOIN HR_DEPARTMENTS DEPT ON EWD.EMP_DEPT_ID = DEPT.DEPT_ID ";
            sqlQuery += " INNER JOIN HR_DEPT_DESIGNATIONS DD ON DD.DEPT_DESIG_ID = EWD.EMP_DESIG_ID ";
            sqlQuery += " WHERE EWD.EFFECTIVE_TO_DT IS NULL AND EMP.TERMINATION_DATE IS NULL AND EMP.ENABLED_FLAG= 1 ";
            sqlQuery += " AND EMP.EMP_ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += "  ORDER BY TO_NUMBER(DEPT.ATTRIBUTE1),DD.DESIG_NAME,TO_NUMBER(EMP.EMP_NO) ";
            return sqlQuery;
        }

        public static string GetEmpMaxNumber()
        {
            sqlQuery = string.Empty;
            sqlQuery += "  select nvl(max(to_number(emp_no)),0) +1 as MaxEmpNo ";
            sqlQuery += "  from hr_employees  where translate(emp_no, '.1234567890', '.') is null ";
            sqlQuery += " and emp_org_id='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += "  order by to_number(emp_no) ";
            return sqlQuery;
        }

        public static string GetEmployeeDetails()
        {
            sqlQuery = string.Empty;

            string strLngCode = VMVServices.Web.Utils.LanguageCode;
            sqlQuery = " select t.emp_id,(t.emp_no|| ' - ' ||t.emp_first_name" + strLngCode + "||' '||t.emp_middle_name" + strLngCode + "||' '||t.emp_last_name" + strLngCode + ") emp_name,t.emp_no ";
            sqlQuery += " from hr_employees t";
            sqlQuery += " where t.workflow_completion_status=1";
            sqlQuery += " and t.enabled_flag=1";
            sqlQuery += " and t.emp_org_id='" + VMVServices.Web.Utils.OrganizationID + "'";
            //sqlQuery += " and t.emp_internal_external  = 'I'";
            sqlQuery += " order by t.emp_no asc";

            return sqlQuery;
        }
        public static string GetEmployeeDetails_t()
        {
            sqlQuery = string.Empty;

            string strLngCode = VMVServices.Web.Utils.LanguageCode;
            sqlQuery = " select t.emp_id,(t.emp_no|| ' - ' ||t.emp_first_name" + strLngCode + "||' '||t.emp_middle_name" + strLngCode + "||' '||t.emp_last_name" + strLngCode + ") emp_name,t.emp_no ";
            sqlQuery += " from hr_employees t";
            sqlQuery += " where t.workflow_completion_status=1";
            sqlQuery += " and t.enabled_flag=1";
            sqlQuery += "  and t.termination_date is not null ";
            sqlQuery += " and t.emp_org_id='" + VMVServices.Web.Utils.OrganizationID + "'";
            //sqlQuery += " and t.emp_internal_external  = 'I'";
            sqlQuery += " order by t.emp_no asc";

            return sqlQuery;
        }

        public static string GetEmployeeDetails_D()
        {
            sqlQuery = string.Empty;

            string strLngCode = VMVServices.Web.Utils.LanguageCode;
            sqlQuery = " select t.emp_id,(t.emp_no|| ' - ' ||t.emp_first_name" + strLngCode + "||' '||t.emp_middle_name" + strLngCode + "||' '||t.emp_last_name" + strLngCode + ") emp_name,t.emp_no ";
            sqlQuery += " from hr_resig_request_hdr E, HR_EMPLOYEES T";
            sqlQuery += " WHERE E.RESIG_EMP_ID = T.EMP_ID ";
           
            sqlQuery += " and t.emp_org_id='" + VMVServices.Web.Utils.OrganizationID + "'";
            //sqlQuery += " and t.emp_internal_external  = 'I'";
            sqlQuery += " order by T.emp_no asc";

            return sqlQuery;
        }
        public static string GetTrainingEmployeeDetails()
        {
            sqlQuery = string.Empty;

            string strLngCode = VMVServices.Web.Utils.LanguageCode;

            sqlQuery = "    select ee.emp_id,";
            sqlQuery += "   (ee.emp_no || ' - ' || ee.emp_first_name" + strLngCode + " || ' ' || ee.emp_middle_name" + strLngCode + " || ' ' ||";
            sqlQuery += "   ee.emp_last_name" + strLngCode + ") emp_name,";
            sqlQuery += "   ee.emp_no";
            sqlQuery += "  from HR_PER_APP_REVIEW_HDR h, hr_employees ee";
            sqlQuery += "  where UPPER(h.rev_recommendation) = 'TRAINING'";
            sqlQuery += "   and ee.emp_id = h.rev_appraisee_id";
            sqlQuery += "  and ee.workflow_completion_status=1";
            sqlQuery += "  and ee.enabled_flag=1";
            sqlQuery += "  and ee.emp_org_id='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += "  order by ee.emp_first_name asc";

            return sqlQuery;
        }
        public static string GetEmployeeTrainerDetails()
        {
            sqlQuery = string.Empty;

            string strLngCode = VMVServices.Web.Utils.LanguageCode;
            sqlQuery = " select t.emp_id,(t.emp_first_name" + strLngCode + "||' '||t.emp_last_name" + strLngCode + ") emp_name,t.emp_no ";
            sqlQuery += " from hr_employees t,HR_TRM_TRAINER HT";
            sqlQuery += " where t.workflow_completion_status=1";
            sqlQuery += " and t.emp_id = ht.TRNR_EMP_ID ";
            sqlQuery += " and t.enabled_flag=1";
            sqlQuery += " and t.emp_org_id='" + VMVServices.Web.Utils.OrganizationID + "'";
            // sqlQuery += " and t.emp_internal_external  = 'I'";
            sqlQuery += " order by t.emp_first_name asc";

            return sqlQuery;
        }


        public static string GetEmployeeDetailsBothIntExt()
        {
            sqlQuery = string.Empty;

            string strLngCode = VMVServices.Web.Utils.LanguageCode;
            sqlQuery = " select t.emp_id,(t.emp_no|| ' - ' ||t.emp_first_name" + strLngCode + "||' '||t.emp_middle_name" + strLngCode + "||' '||t.emp_last_name" + strLngCode + ") emp_name,t.emp_no ";
            sqlQuery += " from hr_employees t";
            sqlQuery += " where t.workflow_completion_status=1";
            sqlQuery += " and t.enabled_flag=1";
            sqlQuery += " and t.emp_org_id='" + VMVServices.Web.Utils.OrganizationID + "'";
            //sqlQuery += " and t.emp_internal_external  = 'I'";
            sqlQuery += " order by t.emp_first_name asc";

            return sqlQuery;
        }

        public static string GetEmployeeDetailswithcode()
        {
            sqlQuery = string.Empty;

            string strLngCode = VMVServices.Web.Utils.LanguageCode;
            sqlQuery = " select t.emp_id,(t.emp_no || ' - ' || t.emp_first_name" + strLngCode + "||' '||t.emp_middle_name" + strLngCode + "||' '||t.emp_last_name" + strLngCode + " ) emp_name ";
            sqlQuery += " from hr_employees t";
            sqlQuery += " where t.workflow_completion_status=1";
            sqlQuery += " and t.enabled_flag=1";
            sqlQuery += " and t.emp_org_id='" + VMVServices.Web.Utils.OrganizationID + "'";
            // sqlQuery += " and t.emp_internal_external = 'I'";
            sqlQuery += " order by t.emp_first_name asc";

            return sqlQuery;
        }

        public static string GetEmpDetails()
        {
            sqlQuery = string.Empty;

            string strLngCode = VMVServices.Web.Utils.LanguageCode;
            sqlQuery = " select t.emp_id,(t.emp_no|| ' - ' ||t.emp_first_name" + strLngCode + "||' '||t.emp_middle_name" + strLngCode + "||' '||t.emp_last_name" + strLngCode + ") emp_name,t.emp_no ";
            sqlQuery += " from hr_employees t";
            sqlQuery += " where t.workflow_completion_status=1";
            sqlQuery += " and t.enabled_flag=1";
            sqlQuery += " and t.emp_org_id='" + VMVServices.Web.Utils.OrganizationID + "'";
            //  sqlQuery += " and t.emp_internal_external= 'I'";
            sqlQuery += " order by t.emp_first_name asc";

            return sqlQuery;
        }
        public static string GetResignedEmpDetails(string empId="")
        {
            sqlQuery = string.Empty;

            string strLngCode = VMVServices.Web.Utils.LanguageCode;
            sqlQuery = " select t.emp_id,(t.emp_no|| ' - ' ||t.emp_first_name" + strLngCode + "||' '||t.emp_middle_name" + strLngCode + "||' '||t.emp_last_name" + strLngCode + ") emp_name,t.emp_no ";
            sqlQuery += "   ,t.notice_period_date,t.termination_date";
            sqlQuery += " from hr_employees t";
            sqlQuery += " where t.workflow_completion_status=1";
            sqlQuery += " and t.enabled_flag=1";
            sqlQuery += " and t.emp_org_id='" + VMVServices.Web.Utils.OrganizationID + "'";
            //sqlQuery += " and t.notice_period_date is not null";
            sqlQuery += "  and t.termination_date is not null";
           // sqlQuery += "   and t.termination_date > sysdate";
            if (empId.Length > 0)
            {
                sqlQuery += " and t.emp_id='" + empId + "'";
            }
            sqlQuery += " order by t.emp_first_name asc";

            return sqlQuery;
        }
        public static string GetEmp_whohaveinLevapp(string emp_dept_id)
        {
            sqlQuery = string.Empty;

            string strLngCode = VMVServices.Web.Utils.LanguageCode;
            sqlQuery = " select distinct t.emp_id,(t.emp_no|| ' - ' ||t.emp_first_name" + strLngCode + "||' '||t.emp_middle_name" + strLngCode + "||' '||t.emp_last_name" + strLngCode + ") emp_name,t.emp_no ";
            sqlQuery += " from hr_employees t,hr_emp_work_dtls ew,hr_leave_applications la";
            sqlQuery += "  where t.workflow_completion_status=1";
            sqlQuery += "  and t.enabled_flag=1";
            sqlQuery += " and t.emp_org_id='" + VMVServices.Web.Utils.OrganizationID + "'";
            // sqlQuery += " and t.emp_internal_external= 'I'";
            sqlQuery += " and t.emp_id = ew.emp_id";
            sqlQuery += " and t.emp_id = la.staff_id and ew.EFFECTIVE_TO_DT is null";
            sqlQuery += " and ew.emp_dept_id = '" + emp_dept_id + "'";

            return sqlQuery;
        }

        public static string GetEmployeeName(string EmpID)
        {
            sqlQuery = string.Empty;
            string strLngCode = VMVServices.Web.Utils.LanguageCode;
            sqlQuery = " select (t.emp_first_name" + strLngCode + "||' '||t.emp_middle_name" + strLngCode + "||' '||t.emp_last_name" + strLngCode + ") emp_name ";
            sqlQuery += " ,t.Emp_Org_Id ";
            sqlQuery += " from hr_employees t";
            sqlQuery += " where t.emp_id ='" + EmpID + "'";
            sqlQuery += " and t.workflow_completion_status=1";
            sqlQuery += " and t.enabled_flag=1";
            sqlQuery += " order by t.emp_first_name asc";

            return sqlQuery;
        }

        public static string GetApplicntdtl(string app_id)
        {
            sqlQuery = string.Empty;

            sqlQuery += " select a.app_first_name,a.app_middle_name,a.app_last_name,trunc(a.app_dob) as app_dob,a.app_gender,nationality";
            sqlQuery += " from HR_APPLICANTS a";
            sqlQuery += " where a.app_id = '" + app_id + "'";
            return sqlQuery;
        }


        public static string GetEmployeeNMasperdept(string emp_dept_id)
        {
            sqlQuery = string.Empty;
            string strLngCode = VMVServices.Web.Utils.LanguageCode;
            sqlQuery = " select distinct t.emp_id,(t.emp_no|| ' - ' ||t.emp_first_name" + strLngCode + "||' '||t.emp_middle_name" + strLngCode + "||' '||t.emp_last_name" + strLngCode + ") emp_name ";
            sqlQuery += " from hr_employees t,hr_emp_work_dtls ew";
            sqlQuery += " where t.workflow_completion_status=1";
            sqlQuery += " and t.enabled_flag=1";
            sqlQuery += " and t.emp_id = ew.emp_id";
            sqlQuery += " and ew.emp_dept_id ='" + emp_dept_id + "'";
            sqlQuery += " and ew.effective_to_dt is null";
            //  sqlQuery += " and t.emp_internal_external = 'I'";
            sqlQuery += " order by emp_name asc";

            return sqlQuery;
        }


        public static string GetEmployeeNMasperdept_whohavALonly(string emp_dept_id)
        {
            sqlQuery = string.Empty;
            string strLngCode = VMVServices.Web.Utils.LanguageCode;
            sqlQuery = " select distinct t.emp_id,(t.emp_no||' - '||t.emp_first_name" + strLngCode + "||' '||t.emp_middle_name" + strLngCode + "||' '||t.emp_last_name" + strLngCode + ") emp_name ";
            sqlQuery += " from hr_employees t,hr_emp_work_dtls ew,HR_LEAVE_STAFF_DEFINTIONS ls";
            sqlQuery += " where t.workflow_completion_status=1";
            sqlQuery += " and t.enabled_flag=1";
            sqlQuery += " and t.emp_id = ew.emp_id";
            sqlQuery += " and t.emp_id = ls.staff_id";
            sqlQuery += " and ls.leave_id =(SELECT SO.HR_EARN_LEAVE FROM SSM_SYSTEM_OPTIONS SO WHERE SO.ORG_ID = '" + VMVServices.Web.Utils.OrganizationID + "' and SO.HR_EARN_LEAVE is not null)";
            sqlQuery += " and ew.emp_dept_id ='" + emp_dept_id + "'";
            sqlQuery += " and ew.effective_to_dt is null";
            // sqlQuery += " and t.emp_internal_external = 'I'";
            sqlQuery += " order by emp_name asc";

            return sqlQuery;
        }

        public static string GetEmployeeNMasperdeptBothIntExt(string emp_dept_id)
        {
            sqlQuery = string.Empty;
            string strLngCode = VMVServices.Web.Utils.LanguageCode;
            sqlQuery = " select distinct t.emp_id,(t.emp_no||' - '||t.emp_first_name" + strLngCode + "||' '||t.emp_middle_name" + strLngCode + "||' '||t.emp_last_name" + strLngCode + ") emp_name ";
            sqlQuery += " from hr_employees t,hr_emp_work_dtls ew";
            sqlQuery += " where t.workflow_completion_status=1";
            sqlQuery += " and t.enabled_flag=1";
            sqlQuery += " and t.emp_id = ew.emp_id";
            sqlQuery += " and ew.emp_dept_id ='" + emp_dept_id + "'";
            sqlQuery += " and ew.effective_to_dt is null";
            //sqlQuery += " and t.emp_internal_external = 'I'";
            sqlQuery += " order by emp_name asc";

            return sqlQuery;
        }

        public static string GetEmployee_NotinAppliedemp(string EMP_ID)
        {
            sqlQuery = string.Empty;
            string strLngCode = VMVServices.Web.Utils.LanguageCode;
            sqlQuery = " select distinct t.emp_id,(t.emp_no||' '||t.emp_first_name" + strLngCode + "||' '||t.emp_middle_name" + strLngCode + "||' '||t.emp_last_name" + strLngCode + ") emp_name ";
            sqlQuery += " from hr_employees t,hr_emp_work_dtls ew";
            sqlQuery += " where t.workflow_completion_status=1";
            sqlQuery += " and t.enabled_flag=1";
            sqlQuery += " and t.emp_id = ew.emp_id";
            sqlQuery += " and t.EMP_ID ='" + EMP_ID + "'";
            sqlQuery += " and ew.effective_to_dt is null";
            //  sqlQuery += " and t.emp_internal_external = 'I'";
            sqlQuery += " order by emp_name asc";

            return sqlQuery;
        }

        public static string GetEmployeeNoticePeriod(string EMP_ID)
        {
            sqlQuery = string.Empty;
            sqlQuery = " Select NOTICE_PERIOD FROM HR_EMPLOYEES WHERE EMP_ID ='" + EMP_ID + "' and ENABLED_FLAG = 1 ";
            return sqlQuery;
        }


        public static string GetEmployeeNdeptNdes(string emp_dept_id, string emp_desi_id)
        {
            sqlQuery = string.Empty;
            string strLngCode = VMVServices.Web.Utils.LanguageCode;
            sqlQuery = " select t.emp_id,(t.emp_no ||' - '||t.emp_first_name" + strLngCode + " || ' ' || t.emp_middle_name" + strLngCode + " || ' ' ||t.emp_last_name" + strLngCode + ") emp_name ";
            sqlQuery += " from hr_employees t,hr_emp_work_dtls ew";
            sqlQuery += " where t.workflow_completion_status=1";
            sqlQuery += " and t.enabled_flag=1";
            sqlQuery += " and t.emp_id = ew.emp_id";
            sqlQuery += " and ew.emp_dept_id ='" + emp_dept_id + "'";
            sqlQuery += " and ew.emp_desig_id = '" + emp_desi_id + "' ";
            sqlQuery += " AND (EW.EFFECTIVE_TO_DT IS NULL OR  (SYSDATE BETWEEN EW.EFFECTIVE_FROM_DT AND EW.EFFECTIVE_TO_DT)) ";
            // sqlQuery += " and t.emp_Internal_External = 'I'";
            sqlQuery += " order by t.emp_first_name asc";

            return sqlQuery;
        }
        public static string getWorkDetails(string str_Emp_Id)
        {
            sqlQuery = string.Empty;
            sqlQuery += "  SELECT EWD.EMP_WH_ID,EWD.POS_IN_WORK_PERMIT,EWD.EMP_ID,EWD.EMP_DEPT_ID,EWD.EMP_DESIG_ID,EWD.EMP_CATEGORY,EWD.EMP_GRADE_ID,EWD.EMP_JOB_ID,EWD.EMP_POSITION_ID,EWD.EFFECTIVE_FROM_DT,EWD.EFFECTIVE_TO_DT,SS.SEC_NAME" + VMVServices.Web.Utils.LanguageCode + " as SEC_NAME,SS.SEC_ID,LC.LOC_ID,LC.LOC_DESC ";
            sqlQuery += " ,HD.DEPT_NAME" + VMVServices.Web.Utils.LanguageCode + " AS DEPT_NAME,HDD.DESIG_NAME" + VMVServices.Web.Utils.LanguageCode + " AS DESIG_NAME,HC.CATEGORY_DESC" + VMVServices.Web.Utils.LanguageCode + " as CATEGORY_DESC,HG.GRADE_DESC" + VMVServices.Web.Utils.LanguageCode + " as GRADE_DESC,HJ.JOB_DESC" + VMVServices.Web.Utils.LanguageCode + " as JOB_DESC,HP.POSITION_DESC" + VMVServices.Web.Utils.LanguageCode + " as POSITION_DESC ";
            sqlQuery += " ,REP_EMP.EMP_ID AS  REPORTINGTO_ID,REP_EMP.EMP_FIRST_NAME||' ' ||REP_EMP.EMP_MIDDLE_NAME||' ' ||REP_EMP.EMP_LAST_NAME AS REPORTINGTO_DESC ";
            sqlQuery += " ,CP_EMP.EMP_ID AS  CONTINGENCY_PLAN,CP_EMP.EMP_FIRST_NAME||' ' ||CP_EMP.EMP_MIDDLE_NAME||' ' ||CP_EMP.EMP_LAST_NAME AS CONTINGENCY_PLAN_DESC ";

            sqlQuery += " ,'N' as DELETED ";
            sqlQuery += "  FROM HR_EMP_WORK_DTLS EWD  ";
            sqlQuery += " INNER JOIN HR_DEPARTMENTS HD ON EWD.EMP_DEPT_ID = HD.DEPT_ID ";
            sqlQuery += " INNER JOIN HR_DEPT_DESIGNATIONS HDD ON EWD.EMP_DESIG_ID =  HDD.DEPT_DESIG_ID ";
            sqlQuery += " INNER JOIN HR_CATEGORIES HC ON EWD.EMP_CATEGORY = HC.CATEGORY_ID ";
            sqlQuery += " INNER JOIN HR_GRADES HG ON EWD.EMP_GRADE_ID = HG.GRADE_ID ";
            sqlQuery += " INNER JOIN HR_JOBS HJ ON EWD.EMP_JOB_ID = HJ.JOB_ID ";
            sqlQuery += " INNER JOIN HR_POSITIONS HP ON EWD.EMP_POSITION_ID=  HP.POSITION_ID ";
            sqlQuery += " LEFT JOIN HR_EMPLOYEES REP_EMP ON REP_EMP.EMP_ID= EWD.Reporting_To ";
            sqlQuery += " LEFT JOIN HR_EMPLOYEES CP_EMP ON CP_EMP.EMP_ID= EWD.CONTINGENCY_PLAN ";
            sqlQuery += " INNER JOIN HR_SECTION SS ON EWD.EMP_SEC_ID = SS.SEC_ID ";
            sqlQuery += " INNER JOIN HR_LOCATIONS LC ON EWD.LOC_ID = LC.LOC_ID ";
            sqlQuery += " WHERE EWD.EMP_ID='" + str_Emp_Id + "'";
            return sqlQuery;
        }
        public static string getBankDetails(string str_Emp_Id)
        {
            sqlQuery = string.Empty;
            sqlQuery += "SELECT EBD.PK_ID,EBD.EMP_IBAN_NUM, EMP_ID,EBD.emp_bank_acct_code,EBD.EMP_BANK_CODE,B.BANK_NAME,EBD.EMP_BANK_BRANCH_CODE,BB.BANK_BRANCH_NAME,EBD.EFFECTIVE_FROM_DT,EBD.EFFECTIVE_TO_DT ";
            sqlQuery += " ,'N' as DELETED,EBD.ENABLED_FLAG ";
            sqlQuery += " FROM HR_EMP_BANK_DTLS EBD ";
            sqlQuery += " INNER JOIN CA_BANK B ON EBD.EMP_BANK_CODE = B.BANK_ID ";
            sqlQuery += " INNER JOIN CA_BANK_BRANCH BB ON EBD.EMP_BANK_BRANCH_CODE = BB.BANK_BRANCH_ID ";
            sqlQuery += " where EBD.EMP_ID='" + str_Emp_Id + "'";
            return sqlQuery;
        }
        public static string getQualificationDetails(string str_Emp_Id)
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT HQ.QUALI_ID,HQ.EMP_ID,HQ.QUALI_TYPE,HQ.QUALI_NAME,HQ.DURATION,HQ.PRIMARY_FLAG,HQ.EFFECTIVE_FROM_DT,HQ.EFFECTIVE_TO_DT,'N' AS DELETED ";
            sqlQuery += " ,SCM.DESCRIPTION AS QUALI_TYPE_NAME ";
            sqlQuery += " FROM HR_QUALIFICATIONS HQ ";
            sqlQuery += " INNER JOIN SSM_CODE_MASTERS SCM ON SCM.CODE=HQ.QUALI_TYPE";
            sqlQuery += " WHERE HQ.EMP_ID='" + str_Emp_Id + "'";
            return sqlQuery;
        }

        public static string getPasswordDetails(string str_Emp_Id)
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT EPD.EPD_ID,EPD.USER_ID,EPD.USER_PASSWORD ";
            sqlQuery += " ,CASE  EPD.ENABLED_FLAG WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS ENABLED_FLAG,EPD.EFFECTIVE_FROM,EPD.EFFECTIVE_TO  ";
            sqlQuery += " ,'N' as DELETED ";
            sqlQuery += " FROM HR_EMP_PASSWORD_DTLS EPD ";
            sqlQuery += " WHERE EPD.EMP_ID='" + str_Emp_Id + "'";
            return sqlQuery;
        }
        public static string getPassPortDetails(string str_Emp_Id)
        {
            sqlQuery = string.Empty;
            sqlQuery += " SELECT PD.PASS_TXN_ID,PD.NAME_IN_PASSPORT,PD.PASSPORT_RELATIONSHIP, SCM_R.DESCRIPTION" + VMVServices.Web.Utils.LanguageCode + " AS PASSPORT_RELATIONSHIP_NAME ";
            sqlQuery += " ,PD.PASSPORT_NUMBER,PD.PASSPORT_NATIONALITY,SCM_N.DESCRIPTION" + VMVServices.Web.Utils.LanguageCode + " AS PASSPORT_NATIONALITY_NAME ";
            sqlQuery += " ,PD.PASSPORT_ISSUE_DATE,PD.PASSPORT_EXPIRY_DATE,PD.PASSPORT_ISSUE_PLACE,PD.PASSPORT_ISSUED_BY,SCM_ID.CODE AS LOOKUP_ID,SCM_ID.CODE AS LOOKUP_NAME ";
            sqlQuery += " ,PD.ENABLED_FLAG,'N' AS DELETED ";
            sqlQuery += "  FROM HR_EMP_PASSPORT_DETAILS PD ";
            sqlQuery += " INNER JOIN SSM_CODE_MASTERS SCM_N ON SCM_N.CODE= PD.PASSPORT_NATIONALITY  AND SCM_N.PARENT_CODE='NATIONALITY'";
            sqlQuery += " INNER JOIN SSM_CODE_MASTERS SCM_R ON SCM_R.CODE= PD.PASSPORT_RELATIONSHIP  AND SCM_R.PARENT_CODE='RELT'";
            sqlQuery += " INNER JOIN SSM_CODE_MASTERS SCM_ID ON SCM_ID.CODE= PD.ID_TYPE  AND SCM_ID.PARENT_CODE='ID TYPE'";
            sqlQuery += " WHERE PD.PASSPORT_EMP_ID='" + str_Emp_Id + "'";
            return sqlQuery;
        }
        public static string getEmployeeAddress(string str_Emp_Id)
        {
            sqlQuery = string.Empty;
            sqlQuery += " select * from HR_ADDRESS where emp_id ='" + str_Emp_Id + "' order by enabled_flag desc ";
            return sqlQuery;
        }

        public static string getEmployeeContact(string str_Emp_Id)
        {
            sqlQuery = string.Empty;
            sqlQuery += " select * from HR_EMP_CONTACT where emp_id ='" + str_Emp_Id + "' order by enabled_flag desc ";
            return sqlQuery;
        }

        public static string getEmployeeDependants(string str_Emp_Id)
        {
            sqlQuery = string.Empty;
            sqlQuery += " select * from hr_dependants where DEPNDANT_EMP_ID ='" + str_Emp_Id + "' order by enabled_flag desc ";
            return sqlQuery;
        }
        public static string getEmployeeEmail(string empId)
        {
            sqlQuery = string.Empty;
            sqlQuery = " select ee.emp_email from hr_employees ee where ee.EMP_NO='" + empId + "'";
            return sqlQuery;
        }
        public static string getSkillDetails(string str_Emp_Id)
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT EMP_SKILL_ID,EMP_ID,SKILL_CATEGORY,SCM.DESCRIPTION" + VMVServices.Web.Utils.LanguageCode + " AS CAT_DESC,EMP_SKILL_TYPE,SCM_T.DESCRIPTION" + VMVServices.Web.Utils.LanguageCode + " AS TYPE_DESC ";
            sqlQuery += " ,EMP_SKILL_NAME,EMP_VALIDITY_DTLS,EMP_REMARKS,EFFECTIVE_FROM ";
            sqlQuery += " ,'N' as DELETED ";
            sqlQuery += "  FROM HR_EMP_SKILL_DTLS ESD ";
            sqlQuery += " INNER JOIN SSM_CODE_MASTERS SCM ON SCM.CODE= ESD.SKILL_CATEGORY AND SCM.PARENT_CODE='SC' ";
            sqlQuery += " INNER JOIN SSM_CODE_MASTERS SCM_T ON SCM_T.CODE = ESD.EMP_SKILL_TYPE AND SCM_T.PARENT_CODE='SKT' ";
            sqlQuery += " WHERE ESD.EMP_ID='" + str_Emp_Id + "'";

            return sqlQuery;
        }

        public static string GetEmployeeDeptDetails(string deptID, string desigID, string bgName)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select hd.dept_name" + VMVServices.Web.Utils.LanguageCode + " as dept_name,hdd.desig_name" + VMVServices.Web.Utils.LanguageCode + " as desig_name,ewd.emp_dept_id,ewd.emp_desig_id, ";
            sqlQuery += " hre.emp_id, hre.emp_no, (hre.emp_first_name || ' ' || hre.emp_middle_name || ' ' ||hre.emp_last_name) emp_name, hre.emp_blood_group ";
            sqlQuery += " from hr_emp_work_dtls ewd,hr_departments hd, hr_dept_designations hdd, HR_EMPLOYEES hre";
            sqlQuery += " where ewd.emp_dept_id = hd.dept_id";
            sqlQuery += " and ewd.emp_desig_id = hdd.dept_desig_id ";
            if (deptID != string.Empty)
            {
                sqlQuery += " and ewd.emp_dept_id = '" + deptID + "'";
            }
            if (desigID != string.Empty)
            {
                sqlQuery += " and ewd.emp_desig_id = '" + desigID + "'";
            }
            sqlQuery += " and ewd.enabled_flag=1";
            sqlQuery += " and ewd.emp_id = hre.emp_id";
            sqlQuery += " and hre.emp_blood_group = '" + bgName + "'";

            return sqlQuery;
        }

        static string spOutPut = string.Empty;
        public static System.Collections.SortedList ErrorCollection = new System.Collections.SortedList();

        public static string GetSPFOR_ERR_MGR_CATEGORY(string P_SCREEN_CODE, string P_RECORD_ID, string P_VALUE1)
        {
            try
            {
                string retcode = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "PKG_DUPLN_VALDN.PROC_DUPLN_VALIDATION_ONE";
                oraCmd.CommandType = CommandType.StoredProcedure;
                //  oraCmd.Parameters.Add("@P_MODULE_CODE", OracleDbType.Char).Value = DBNull.Value;
                //  oraCmd.Parameters.Add("@P_SCREEN_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_SCREEN_CODE", OracleDbType.Char).Value = P_SCREEN_CODE;
                oraCmd.Parameters.Add("@P_PK_ID", OracleDbType.Char).Value = P_RECORD_ID;
                oraCmd.Parameters.Add("@P_VALUE1", OracleDbType.Char).Value = P_VALUE1;
                oraCmd.Parameters.Add("@P_RET", OracleDbType.Varchar2, 150);
                oraCmd.Parameters["@P_RET"].Direction = ParameterDirection.Output;
                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

                retcode = oraCmd.Parameters["@P_RET"].Value.ToString();

                return retcode;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static string GetSPFOR_ERR_MGR_EMPNO(string P_EMP_NO, string P_EMP_ID)
        {
            try
            {
                string retcode = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "PKG_VALIDATIONS.PROC_ERR_MGR_CATEGORY";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add("@P_MODULE_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_SCREEN_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_EMP_NO", OracleDbType.Char).Value = P_EMP_NO;
                oraCmd.Parameters.Add("@P_EMP_ID", OracleDbType.Char).Value = P_EMP_ID;
                oraCmd.Parameters.Add("@P_RET", OracleDbType.Varchar2, 150);
                oraCmd.Parameters["@P_RET"].Direction = ParameterDirection.Output;
                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

                retcode = oraCmd.Parameters["@P_RET"].Value.ToString();

                return retcode;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Code by Manoj

        public static string GetEmployeeNMasperdeptnDesig(string emp_dept_id, string dept_desig_id)
        {
            sqlQuery = string.Empty;
            string strLngCode = VMVServices.Web.Utils.LanguageCode;
            sqlQuery = " select distinct t.emp_id,(t.emp_no|| ' - ' ||t.emp_first_name" + strLngCode + "||' '||t.emp_middle_name" + strLngCode + "||' '||t.emp_last_name" + strLngCode + ") emp_name ";
            sqlQuery += " from hr_employees t,hr_emp_work_dtls ew";
            sqlQuery += " where t.workflow_completion_status=1";
            sqlQuery += " and t.enabled_flag=1";
            sqlQuery += " and t.emp_id = ew.emp_id";
            sqlQuery += " and ew.emp_dept_id ='" + emp_dept_id + "'";
            sqlQuery += " and ew.emp_desig_id='" + dept_desig_id + "'";
            sqlQuery += " and ew.effective_to_dt is null";
            sqlQuery += " and t.TERMINATION_DATE is null ";
                 
            //   sqlQuery += " and t.emp_internal_external = 'I'";
            sqlQuery += " order by emp_name asc";

            return sqlQuery;
        }


        public static string getEmployeeCurrentdepartment(string str_empid)
        {
            sqlQuery = string.Empty;
            sqlQuery += " SELECT EMP_DEPT_ID FROM HR_EMP_WORK_DTLS EWD ";
            sqlQuery += " WHERE EWD.EFFECTIVE_TO_DT IS NULL ";
            sqlQuery += " AND EWD.EMP_ID='" + str_empid + "'";
            return sqlQuery;
        }

        public static string getLeaveDetails(string str_finyear, string str_deptid)
        {
            sqlQuery = string.Empty;
            sqlQuery += "SELECT LDEF.LEAVE_ID,LDEF.NO_OF_DAYS ";
            sqlQuery += " FROM HR_LEAVE_DEPT LD ";
            sqlQuery += " INNER JOIN HR_LEAVE_DEFINITIONS LDEF  ON LDEF.LEAVE_ID = LD.LEAVE_ID AND LD.ORG_ID= LDEF.ORG_ID";
            sqlQuery += " WHERE LD.DEPT_ID='" + str_deptid + "' AND LD.FISCAL_YEAR='" + str_finyear + "'";


            return sqlQuery;
        }

        public static string GetEmpDetailsDOJ(string fromDate, string toDate)
        {
            sqlQuery = string.Empty;

            //DateTime StartDate = new DateTime();
            //if (fromDate != null && fromDate.Length > 0)
            //    StartDate = Convert.ToDateTime(fromDate);
            //DateTime EndDate = new DateTime();
            //if (toDate != null && toDate.Length > 0)
            //    EndDate = Convert.ToDateTime(toDate);


            string strLngCode = VMVServices.Web.Utils.LanguageCode;

            sqlQuery = " select t.nationality,(t.emp_first_name" + strLngCode + "||' '||t.emp_middle_name" + strLngCode + "||' '||t.emp_last_name" + strLngCode + ") emp_name,t.emp_no, TO_CHAR(t.emp_doj,'DD/MON/YYYY') as emp_doj ";


            sqlQuery += " from hr_employees t";
            sqlQuery += " where t.workflow_completion_status=1";
            sqlQuery += " and t.emp_doj between to_date('" + fromDate + "','dd/MM/yyyy')  and to_date('" + toDate + "','dd/MM/yyyy') ";
            sqlQuery += " and t.enabled_flag=1";
            sqlQuery += " and t.emp_org_id='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " order by t.emp_doj asc";

            return sqlQuery;
        }
        public static string GetAREmpDetailsDOJ(string fromDate, string toDate)
        {
            sqlQuery = string.Empty;

            //DateTime StartDate = new DateTime();
            //if (fromDate != null && fromDate.Length > 0)
            //    StartDate = Convert.ToDateTime(fromDate);
            //DateTime EndDate = new DateTime();
            //if (toDate != null && toDate.Length > 0)
            //    EndDate = Convert.ToDateTime(toDate);


            string strLngCode = VMVServices.Web.Utils.LanguageCode;
            sqlQuery = " select t.nationality,(t.emp_first_name" + strLngCode + "||' '||t.emp_middle_name" + strLngCode + "||' '||t.emp_last_name" + strLngCode + ") emp_name,t.emp_no,  t.emp_doj ";
            sqlQuery += " from hr_employees t";
            sqlQuery += " where t.workflow_completion_status=1";
            sqlQuery += " and t.emp_doj between to_date('" + fromDate + "','dd/MM/yyyy')  and to_date('" + toDate + "','dd/MM/yyyy') ";
            sqlQuery += " and t.enabled_flag=1";
            sqlQuery += " and t.emp_org_id='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " order by t.emp_first_name asc";

            return sqlQuery;
        }

        public static string GetEmpDetailsEOS(string fromDate, string toDate)
        {
            sqlQuery = string.Empty;

            string strLngCode = VMVServices.Web.Utils.LanguageCode;
            sqlQuery = " select (t.emp_first_name" + strLngCode + "||' '||t.emp_middle_name" + strLngCode + "||' '||t.emp_last_name" + strLngCode + ") emp_name,t.emp_no, t.nationality, t.emp_doj, ";
            sqlQuery += " t.confirmation_date, t.termination_date, dd.desig_name, d.dept_name, ptr.pay_amount ";
            sqlQuery += " from hr_employees t, hr_emp_work_dtls ewd, hr_dept_designations dd, hr_departments d,  pay_elements pe, pay_trial_run_dtl_dtl ptr ";
            sqlQuery += " where t.emp_id=ewd.emp_id";
            sqlQuery += " and ewd.emp_desig_id = dd.dept_desig_id";
            sqlQuery += " and ewd.emp_dept_id=d.dept_id";
            sqlQuery += " and pe.pay_element_id=ptr.pay_emp_element_id";
            sqlQuery += " and pe.pay_ele_alias = 'Basic'";
            sqlQuery += " and ptr.pay_emp_id=t.emp_id";
            sqlQuery += " and t.termination_date is not null ";
            sqlQuery += " and t.termination_date between to_date('" + fromDate + "','dd/MM/yyyy')  and to_date('" + toDate + "','dd/MM/yyyy') ";
            //sqlQuery += " and t.enabled_flag=1";
            sqlQuery += " and t.workflow_completion_status=1";
            sqlQuery += " and t.emp_org_id='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " order by t.termination_date asc";

            return sqlQuery;
        }

        public static string GetEmpSalList(string periodID)
        {
            sqlQuery = string.Empty;
            string strLngCode = VMVServices.Web.Utils.LanguageCode;

            sqlQuery = " select e.emp_first_name,b.bank_name,ebd.emp_iban_num,ptr.pay_amount,";
            sqlQuery += " ((select sum(ptr.pay_amount) from pay_elements pe,pay_trial_run_dtl_dtl ptr where pe.pay_element_id = ptr.pay_emp_element_id and pe.pay_ele_class='Earning') - ";
            sqlQuery += " (select sum(ptr.pay_amount) from pay_elements pe,pay_trial_run_dtl_dtl ptr where pe.pay_element_id = ptr.pay_emp_element_id and pe.pay_ele_class='Deduction'))as tot_amt ";
            sqlQuery += " from pay_elements pe,hr_emp_bank_dtls ebd,pay_trial_run_dtl_dtl ptr,ca_bank b,hr_employees e, pay_periods pp, pay_trial_run_hdr ptrh";
            sqlQuery += " where pe.pay_element_id = ptr.pay_emp_element_id";
            sqlQuery += " and pp.pay_period_id=ptrh.payroll_period";
            sqlQuery += " and ebd.emp_bank_code = b.bank_id";
            sqlQuery += " and ebd.emp_id = e.emp_id";
            sqlQuery += " and e.emp_id = ptr.pay_emp_id";
            sqlQuery += " and pp.pay_period_id ='" + periodID + "'";

            return sqlQuery;
        }

        public static string GetEmpNameFromPayroll(string periodID)
        {
            sqlQuery = string.Empty;
            string strLngCode = VMVServices.Web.Utils.LanguageCode;

            sqlQuery = " select emp.emp_id, (emp.EMP_NO||' '||EMP.emp_first_name" + strLngCode + "||' '||EMP.emp_middle_name" + strLngCode + "||' '||EMP.emp_last_name" + strLngCode + ") emp_name, emp.emp_no";
            sqlQuery += " from Pay_Nr_Elements_hdr peh, Pay_Nr_Elements_Dtl ped, pay_elements pe, hr_employees emp, hr_departments dept, pay_periods pp";
            sqlQuery += " where pe.pay_ele_class='Earning'";
            sqlQuery += " and peh.pay_nr_id=ped.pay_nr_id";
            sqlQuery += " and peh.pay_element_id=pe.pay_element_id";
            sqlQuery += " and emp.emp_id=ped.pay_emp_id";
            sqlQuery += " and emp.emp_id=ped.pay_emp_id";
            sqlQuery += " and dept.dept_id=ped.pay_emp_dept_id";
            sqlQuery += " and pp.pay_period_id=peh.pay_period_id";
            sqlQuery += " and pp.pay_period_id ='" + periodID + "'";

            return sqlQuery;
        }

        public static string GetEmpNameFromPayrollPeriod(string periodID)
        {
            sqlQuery = string.Empty;
            string strLngCode = VMVServices.Web.Utils.LanguageCode;

            sqlQuery = " select e.emp_id,e.emp_no||'-'||e.emp_first_name||' '||e.emp_middle_name||' '||e.emp_last_name as emp_name";
            sqlQuery += "  from hr_employees e,pay_periods pp,pay_final_run_emp_dtl fn";
            sqlQuery += " where fn.emp_id = e.emp_id ";
            sqlQuery += " and fn.payroll_period = pp.pay_period_id ";
           
            sqlQuery += " and pp.pay_period_id ='" + periodID + "'";
            sqlQuery += "  order by e.emp_no asc ";

            return sqlQuery;
        }

        public static string getReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT EMP_NAME,EMP_NAME_OL,BANK_NAME,BANK_NAME_OL,EMP_IBAN_NUM,PAY_PERIOD_ID,sum(tot_amt) as PAY_AMOUNT , TOT_AMT FROM VW_SALARY_LIST V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["PAY_PERIOD_ID"] != null)
                {
                    sqlQuery += " AND V.PAY_PERIOD_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["PAY_PERIOD_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["EMP_ID"] != null)
                {
                    sqlQuery += " AND V.EMP_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["EMP_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["BANK_ID"] != null)
                {
                    sqlQuery += " AND V.BANK_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["BANK_ID"].ToString() + "'";
                }
            }
            sqlQuery += " group by EMP_NAME,EMP_NAME_OL,BANK_NAME,BANK_NAME_OL,EMP_IBAN_NUM,PAY_PERIOD_ID,TOT_AMT";
            return sqlQuery;
        }
        public static string getDetailedMonthlyScaleReport()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_DETAILED_MONTH_SCALE V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["PAY_PERIOD_ID"] != null)
                {
                    sqlQuery += " AND V.PAY_PERIOD_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["PAY_PERIOD_ID"].ToString() + "'";
                }
            }
            return sqlQuery;
        }

        public static string getEmpIncrement()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_EMPLOYEES_INCREAMENT V WHERE ROWNUM > 0 ";


            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {

                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
                {
                    sqlQuery += " AND V.EMP_DOJ >= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy')";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
                {
                    sqlQuery += " AND V.EMP_DOJ <= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["To_Date"].ToString() + "','dd/MM/yyyy')";
                }


                if (VMVServices.Web.Utils.ReportViewFilterParameter["EMP_ID"] != null)
                {
                    sqlQuery += " AND V.EMP_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["EMP_ID"].ToString() + "'";
                }

                if (VMVServices.Web.Utils.ReportViewFilterParameter["DEPT_ID"] != null)
                {
                    sqlQuery += " AND V.DEPT_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["DEPT_ID"].ToString() + "'";
                }

                if (VMVServices.Web.Utils.ReportViewFilterParameter["DESIGN_ID"] != null)
                {
                    sqlQuery += " AND V.DEPT_DESIG_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["DESIGN_ID"].ToString() + "'";
                }

                //if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.EFFECTIVE_FROM_DT] != null)
                //{
                //    sqlQuery += " AND V.EMP_DOJ between to_date('" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.EFFECTIVE_FROM_DT].ToString() + "','dd/MM/yyyy')";
                //}
                //if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.EFFECTIVE_TO_DT] != null)
                //{
                //    sqlQuery += " AND to_date('" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.EFFECTIVE_TO_DT].ToString() + "','dd/MM/yyyy')";
                //}
            }
            return sqlQuery;
        }




        public static void InsertPassPortDtl(string pass_txn_id, string passport_emp_id, string ID_Typ, string str_nationality)
        {
            try
            {

                string strQuery = string.Empty;

                strQuery += " insert into HR_EMP_PASSPORT_DETAILS p";
                strQuery += " (p.pass_txn_id,p.passport_number,p.passport_emp_id,passport_nationality,p.pass_org_id,p.passport_relationship,p.id_type,";
                strQuery += " p.enabled_flag,p.workflow_completion_status,p.created_by,p.created_date)";
                strQuery += " values('" + pass_txn_id + "',' ','" + passport_emp_id + "','" + str_nationality + "','" + VMVServices.Web.Utils.OrganizationID + "','Self','" + ID_Typ + "',1,'0','" + VMVServices.Web.Utils.UserName + "','" + DateTime.Today.ToString("dd-MMM-yyyy") + "')";


                DBMethod.ExecuteNonQuery(strQuery);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GetTerminatedEmpDetails()
        {
            sqlQuery = string.Empty;

            string strLngCode = VMVServices.Web.Utils.LanguageCode;
            sqlQuery = " select t.emp_id,(t.emp_no|| ' - ' ||t.emp_first_name" + strLngCode + "||' '||t.emp_middle_name" + strLngCode + "||' '||t.emp_last_name" + strLngCode + ") emp_name,t.emp_no ";
            sqlQuery += " from hr_employees t";
            sqlQuery += " where t.workflow_completion_status=1";
            sqlQuery += " and t.enabled_flag=1";
            sqlQuery += " and t.emp_org_id='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " and t.termination_date is not null";
            sqlQuery += " order by t.emp_first_name asc";

            return sqlQuery;
        }

        public static string GetActiveEmpDetails()
        {
            sqlQuery = string.Empty;

            string strLngCode = VMVServices.Web.Utils.LanguageCode;
            sqlQuery = " select t.emp_id,(t.emp_no|| ' - ' ||t.emp_first_name" + strLngCode + "||' '||t.emp_middle_name" + strLngCode + "||' '||t.emp_last_name" + strLngCode + ") emp_name,t.emp_no ";
            sqlQuery += " from hr_employees t";
            sqlQuery += " where t.workflow_completion_status=1";
            sqlQuery += " and t.enabled_flag=1";
            sqlQuery += " and t.emp_org_id='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " and t.termination_date is null";
            sqlQuery += " order by t.emp_first_name asc";

            return sqlQuery;
        }

        public static string GetEmployeeNameBasedOnDept(string emp_dept_id)
        {
            sqlQuery = string.Empty;

            string strLngCode = VMVServices.Web.Utils.LanguageCode;
            sqlQuery = " select distinct t.emp_id,(t.emp_no|| ' - ' ||t.emp_first_name" + strLngCode + "||' '||t.emp_middle_name" + strLngCode + "||' '||t.emp_last_name" + strLngCode + ") emp_name ";
            sqlQuery += " from hr_employees t,hr_emp_work_dtls ew";
            sqlQuery += " where t.workflow_completion_status=1";
            sqlQuery += " and t.enabled_flag=1";
            sqlQuery += " and t.emp_id = ew.emp_id";
            sqlQuery += " and ew.emp_dept_id ='" + emp_dept_id + "'";
            sqlQuery += " and ew.effective_to_dt is null";
            sqlQuery += " and t.termination_date is null";
            sqlQuery += " order by emp_name asc";

            return sqlQuery;
        }
        public static string GetEmpBasedDept(string deptId)
        {
            try
            {
                string strQuery = string.Empty;

                strQuery = " SELECT '0' as LEAVE_PLAN_ID, E.EMP_ID,'N' AS DELETED,";
                strQuery += "  e.emp_no||' - '||E.EMP_FIRST_NAME||' '||e.emp_middle_name||' '||e.emp_last_name as EMP_NAME,";
                strQuery += "  D.DEPT_ID,";
                strQuery += "   D.DEPT_NAME,";
                strQuery += "  DD.DESIG_NAME,null as FROM_DATE,null as TO_DATE";
                strQuery += "  FROM HR_EMPLOYEES         E,";
                strQuery += "  HR_EMP_WORK_DTLS     WD,";
                strQuery += "  HR_DEPARTMENTS       D,";
                strQuery += "   HR_DEPT_DESIGNATIONS DD";
                strQuery += "  WHERE E.EMP_ID = WD.EMP_ID";
                strQuery += "  AND WD.EMP_DEPT_ID = D.DEPT_ID";
                strQuery += "  AND WD.EMP_DESIG_ID = dd.dept_desig_id";
                strQuery += "  and wd.emp_dept_id = '" + deptId + "'";
                strQuery += " and Wd.effective_to_dt is null ";
                strQuery += "   order by E.EMP_NO,e.emp_first_name";
                return strQuery;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GetFacilityData(string LimitId)
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT LD.VALUE_KEY_ID FROM  HR_EMP_MOB_LIMIT_HDR LH,HR_EMP_MOB_LIMIT_DTL LD ";
            sqlQuery += " WHERE LH.MOB_LIMIT_ID = LD.MOB_LIMIT_ID ";
            sqlQuery += " AND LH.ENABLED_FLAG = 1 ";
            sqlQuery += " AND LH.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " AND LD.MOB_LIMIT_ID = '" + LimitId + "'";
            sqlQuery += "   and LH.ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";


            return sqlQuery;
        }
        public static string GetDepartmentName(string EmpId)
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT D.DEPT_NAME,D.DEPT_ID,md.mob_limit,md.mob_number,md.MOB_LIMIT_ID FROM HR_EMP_WORK_DTLS ED,HR_DEPARTMENTS D,hr_emp_mob_limit_hdr md WHERE ED.EMP_DEPT_ID=D.DEPT_ID ";
            sqlQuery += " and md.emp_id = ed.emp_id ";
            sqlQuery += " AND ED.ENABLED_FLAG = 1 ";
            sqlQuery += " AND ED.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " AND ED.EMP_ID = '" + EmpId + "'";
            sqlQuery += " and ED.Emp_Org_Id='" + VMVServices.Web.Utils.OrganizationID + "'";


            return sqlQuery;
        }

        public static string GetDeptDesigName(string EmpId)
        {
            sqlQuery = string.Empty;

            sqlQuery += " SELECT D.DEPT_NAME,D.DEPT_ID,dd.DESIG_NAME";
            sqlQuery += " FROM HR_EMP_WORK_DTLS ED,HR_DEPARTMENTS D,hr_dept_designations dd";
            sqlQuery += " WHERE ED.EMP_DEPT_ID=D.DEPT_ID ";
            sqlQuery += " and ed.emp_desig_id = dd.dept_desig_id";
            sqlQuery += " and d.dept_id=dd.dept_id";
            sqlQuery += " AND ED.ENABLED_FLAG = 1 ";
            sqlQuery += " AND ED.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " AND ED.EMP_ID ='" + EmpId + "'";
            sqlQuery += " and ED.Emp_Org_Id='" + VMVServices.Web.Utils.OrganizationID + "'";
            return sqlQuery;
        }


        public static string GetFacilityDtls(string EmpId)
        {
            sqlQuery = string.Empty;
            sqlQuery += " SELECT DISTINCT 0 AS MOB_LIMIT_ENTRY_DTL_ID ,SSM.DESCRIPTION,SSM.CODE,'FALSE'  as ELIGIBILITY, 'FALSE'  as ENABLED_FLAG, ";
            sqlQuery += " 0  AS ELIGIBILITY_AMT,0  AS REMAINING_AMT, ";
            sqlQuery += " 0  AS VALUE_AMOUNT,'N' AS DELETED ";
            sqlQuery += " FROM HR_EMP_MOB_LIMIT_DTL MD,SSM_CODE_MASTERS SSM,HR_EMP_MOB_LIMIT_HDR MH ";
            sqlQuery += " WHERE MD.VALUE_KEY_ID = SSM.CODE";
            sqlQuery += " AND MH.MOB_LIMIT_ID = MD.MOB_LIMIT_ID";
            sqlQuery += " AND MH.EMP_ID = '" + EmpId + "'";

            return sqlQuery;
        }
        public static string GetMobileBillingDtl(string mob_limit_id)
        {
            sqlQuery = string.Empty;

            sqlQuery += " SELECT DISTINCT MED.MOB_LIMIT_ENTRY_DTL_ID, SSM.DESCRIPTION, SSM.CODE,MED.ELIGIBILITY_AMT,MED.REMAINING_AMT,MED.VALUE_AMOUNT,case MED.ELIGIBILITY when '1' then 'TRUE' else 'FALSE' end as ELIGIBILITY,case MED.ENABLED_FLAG when '1' then 'TRUE' else 'FALSE' end as ENABLED_FLAG,'N' AS DELETED";
            sqlQuery += " FROM HR_EMP_MOB_LIMIT_DTL MD, SSM_CODE_MASTERS SSM,HR_EMP_MOB_LIMIT_HDR MH,HR_EMP_MOB_LIMIT_ENTRY_DTL MED,HR_EMP_MOB_LIMIT_ENTRY_HDR MEH";

            sqlQuery += " WHERE MD.VALUE_KEY_ID = SSM.CODE";
            sqlQuery += " AND MH.MOB_LIMIT_ID = MD.MOB_LIMIT_ID";
            sqlQuery += " AND MED.MOB_LIMIT_ENTRY_ID = MEH.MOB_LIMIT_ENTRY_ID";
            sqlQuery += " AND MED.VALUE_KEY_ID = MD.VALUE_KEY_ID";
            sqlQuery += " and MEH.MOB_LIMIT_ENTRY_ID ='" + mob_limit_id + "'";
            return sqlQuery;

        }

        public static string GetEmployeeNationality()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT DISTINCT E.NATIONALITY ";
            sqlQuery += " FROM HR_EMPLOYEES E";
            sqlQuery += " WHERE E.ENABLED_FLAG = 1";
            sqlQuery += " AND E.TERMINATION_DATE IS NULL";
            sqlQuery += " ORDER BY E.NATIONALITY ASC";

            return sqlQuery;
        }

    }
}
