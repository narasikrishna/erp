﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.HR
{
    public class AppraisalFinalAssessment_DAL
    {
        static string sqlQuery = "";


        public static string GetDeptDesig(string emp_id)
        {
            sqlQuery = string.Empty;

            sqlQuery += " select wd.emp_id,d.dept_name||' - '||d.dept_name_ol as dept_name,dd.desig_name||' - '||dd.desig_name_ol as desig_name";
            sqlQuery += " from hr_departments d,hr_dept_designations dd,hr_emp_work_dtls wd";
            sqlQuery += " where d.dept_id = wd.emp_dept_id";
            sqlQuery += " and dd.dept_desig_id= wd.emp_desig_id";
            sqlQuery += " and wd.enabled_flag = 1";
            sqlQuery += " and wd.workflow_completion_status = 1";
            sqlQuery += " and wd.emp_id = '" + emp_id + "'";
            return sqlQuery;

        }


        public static string Getcountofappraiser(string emp_id)
        {
            sqlQuery = string.Empty;

            sqlQuery += " select count(*) as count";
            sqlQuery += " from HR_PER_APP_REVIEW_HDR rh";
            sqlQuery += " where rh.rev_appraisee_id = '" + emp_id + "'";

            return sqlQuery;

        }

        public static string GetRating(string emp_id, int count)
        {
            sqlQuery = string.Empty;

            sqlQuery += " select round((sum(rd.rev_reviewer_rating)/" + count + "),2) as rating";
            sqlQuery += " from HR_PER_APP_REVIEW_HDR rh,HR_PER_APP_REVIEW_DTL rd";
            sqlQuery += " where rh.rev_id = rd.rev_id";
            sqlQuery += " and rh.rev_appraisee_id = '" + emp_id + "'";


            return sqlQuery;

        }

        public static string GetTotalRating(decimal rat, decimal HR_RAT, decimal CHR_RAT)
        {
            sqlQuery = string.Empty;

            sqlQuery += " select round((" + rat + "*65/100)+(" + HR_RAT + "*25/100)+(" + CHR_RAT + "*10/100),2) as TOT_RAT from dual";


            return sqlQuery;

        }


        public static string GetAraisalFinalAssessmentDtl(string apfa_hdr_id)
        {
            sqlQuery = string.Empty;

            sqlQuery += " select fad.apfa_dtl_id,fad.final_rating,fad.final_hr_ceo_rating,fad.final_chairman_rating,fad.final_total_rating,fad.emp_id,";
            sqlQuery += " e.emp_first_name||''||e.emp_last_name as emp_name,d.dept_name,'N' AS DELETED";
            sqlQuery += " from HR_APPR_FNL_ASESMNT_DTL fad,HR_PER_APP_ASSIGNMENT_HDR ah,HR_APPR_FNL_ASESMNT_HDR fah,hr_employees e,hr_departments d";
            sqlQuery += " where fad.apfa_hdr_id = fah.apfa_hdr_id";
            sqlQuery += " and fah.assign_hdr_id = ah.assign_hdr_id";
            sqlQuery += " and fad.emp_id = e.emp_id";
            sqlQuery += " and d.dept_id = ah.assign_dept_id";
            sqlQuery += " and fad.apfa_hdr_id ='" + apfa_hdr_id + "'";
            return sqlQuery;

        }

        public static string GetAraisalFinalAssessmentDtlforAraisal(string assign_hdr_id)
        {
            sqlQuery = string.Empty;

            sqlQuery += " select distinct rh.rev_appraisee_id,e.emp_id,e.emp_first_name||''||e.emp_last_name as EMP_NAME,";
            sqlQuery += " d.DEPT_NAME,0 as APFA_DTL_ID,0 as FINAL_HR_CEO_RATING,0 as FINAL_CHAIRMAN_RATING,0 as FINAL_TOTAL_RATING,";
            sqlQuery += " (select round(sum(dc.rev_reviewer_rating), 2) / count(1)";
            sqlQuery += " from HR_PER_APP_REVIEW_HDR dd, HR_PER_APP_REVIEW_DTL dc";
            sqlQuery += " where dd.rev_appraiser_id = rh.rev_appraiser_id";
            sqlQuery += " and dd.rev_id = dc.rev_id)as FINAL_RATING,'N' AS DELETED";
            sqlQuery += " from HR_PER_APP_REVIEW_HDR rh, HR_PER_APP_REVIEW_DTL rd,HR_PER_APP_ASSIGNMENT_HDR ah,";
            sqlQuery += " hr_employees e,hr_departments d";
            sqlQuery += " where rh.rev_id = rd.rev_id";
            sqlQuery += " and ah.assign_hdr_id = rh.assign_hdr_id ";
            sqlQuery += " and rh.rev_appraisee_id = e.emp_id";
            sqlQuery += " and ah.assign_dept_id = d.dept_id";
            sqlQuery += " and rh.assign_hdr_id = '" + assign_hdr_id + "'";
            sqlQuery += " and rh.rev_appraisee_id is not null";
            sqlQuery += " group by rh.rev_appraisee_id, rh.rev_appraiser_id,e.emp_id,e.emp_first_name,";
            sqlQuery += " e.emp_last_name,d.dept_name";


            return sqlQuery;

        }

    }
}
