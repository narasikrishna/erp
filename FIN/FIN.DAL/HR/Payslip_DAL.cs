﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.HR
{
   public class Payslip_DAL
    {
        static string sqlQuery = "";
        public static string get_EarningsDeductReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_PAYSLIP_EARN_DEDUC V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.EMP_ID] != null)
                {
                    sqlQuery += " AND V.PAY_EMP_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.EMP_ID].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.PAY_PERIOD_ID] != null)
                {
                    sqlQuery += " AND V.PAY_PERIOD_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.PAY_PERIOD_ID].ToString() + "'";
                }
            }

            return sqlQuery;
        }

        public static string get_EmpDtlsReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_PAY_SLIP_EMP_DTLS V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.EMP_ID] != null)
                {
                    sqlQuery += " AND V.EMP_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.EMP_ID].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.PAY_PERIOD_ID] != null)
                {
                    sqlQuery += " AND V.PAY_PERIOD_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.PAY_PERIOD_ID].ToString() + "'";
                }
            }
            return sqlQuery;
        }

        public static string get_DeductionReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_DEDUCTION V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["PAY_PERIOD_ID"] != null)
                {
                    sqlQuery += " AND V.PAY_PERIOD_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["PAY_PERIOD_ID"].ToString() + "'";
                }
            }
            return sqlQuery;
        }

        public static string get_EarningsReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_EARNING V WHERE ROWNUM > 0 ";
           
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.PAY_PERIOD_ID] != null)
                {
                    sqlQuery += " AND V.PAY_PERIOD_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.PAY_PERIOD_ID].ToString() + "'";
                }
            }
            return sqlQuery;
        }

        public static string getPayrollComparsionDet()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_EMP_SALARY_DET V WHERE ROWNUM > 0 ";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Period"] != null)
                {
                    sqlQuery += " AND V.PAY_FROM_DT >= TO_DATE('" + VMVServices.Web.Utils.ReportViewFilterParameter["From_Period"].ToString() + "','DD/MM/YYYY')";
                }

                if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Period"] != null)
                {
                    sqlQuery += " AND V.PAY_FROM_DT <=TO_DATE('" + VMVServices.Web.Utils.ReportViewFilterParameter["To_Period"].ToString() + "','DD/MM/YYYY')";
                }

                if (VMVServices.Web.Utils.ReportViewFilterParameter["EMP_ID"] != null)
                {
                    sqlQuery += " AND V.EMP_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["EMP_ID"].ToString() + "'";
                }
            }
            return sqlQuery;
        }
    }
}
