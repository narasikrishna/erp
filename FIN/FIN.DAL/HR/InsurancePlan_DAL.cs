﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.HR
{
    public class InsurancePlan_DAL
    {
        static string sqlQuery = "";

        public static string GetStudentfmDept(string dept)
        {
            sqlQuery = string.Empty;
            sqlQuery = " select c.Emp_No ,(c.emp_first_name || ' ' || c.emp_middle_name || ' ' || c.emp_last_name) as EMP_NAME,";
            sqlQuery += "  a.ELIGIBLE_PLAN_ID  as ELIGIBLE_PLAN_ID , a.SELECTED_PLAN_ID as SELECTED_PLAN_ID,a.PLAN_ENTRY_DTL_ID,";
            sqlQuery += "  a.TOTAL_COST ,a.COMPANY_COST as COMPANY_COST,a.EMP_COST , a.FAMILY_MEMBER as FAMILY_MEMBER, a.TOTAL_AMOUNT AS TOTAL_AMOUNT , 0 as DELETED,c.EMP_ID,e.PLAN_LEVAL as ELIGIBLE_LEVEL_PLAN,f.PLAN_LEVAL as SELECTED_PLAN_LEVEL ";
            sqlQuery += "  from INSURANCE_PLAN_ENTRY_DTL a, INSURANCE_PLAN_ENTRY_HDR b , HR_EMPLOYEES c,HR_EMP_WORK_DTLS d , INSURANCE_PLAN e, INSURANCE_PLAN f";
            sqlQuery += "  where b.Plan_Entry_Id = a.plan_entry_id and a.emp_id = c.emp_id and c.EMP_ID = d.emp_id and a.ELIGIBLE_PLAN_ID = e.PLAN_ID ";
            sqlQuery += "   and a.SELECTED_PLAN_ID = f.PLAN_ID and d.emp_dept_id = '" + dept + "'";
            sqlQuery += "  union";
            sqlQuery += "  select a.EMP_NO,(a.emp_first_name || ' ' || a.emp_middle_name || ' ' || a.emp_last_name) as EMP_NAME,";
            sqlQuery += "  '0' as ELIGIBLE_PLAN_ID , '0' as SELECTED_PLAN_ID,'0' as PLAN_ENTRY_DTL_ID , 0  as TOTAL_COST,0 as COMPANY_COST,";
            sqlQuery += "  0 as EMP_COST , 0 as FAMILY_MEMBER , 0 as TOTAL_AMOUNT, 0 as DELETED , a.EMP_ID ,'0' as ELIGIBLE_LEVEL_PLAN,'0' as SELECTED_PLAN_LEVEL from HR_EMPLOYEES a , HR_EMP_WORK_DTLS b, HR_DEPARTMENTS c";
            sqlQuery += "  where a.Emp_Id= b.Emp_Id and b.Emp_Dept_Id = c.dept_id and a.emp_org_id = b.emp_org_id";
            sqlQuery += "  and b.emp_org_id = c.org_id and c.dept_id = '" + dept + "' and a.EMP_ID not in (select EMP_ID from INSURANCE_PLAN_ENTRY_DTL where DEPT_ID = '" + dept + "')";

            return sqlQuery;
        }

        public static string GetPlanDetails(string planid, string finyear)
        {
            sqlQuery = string.Empty;
            sqlQuery = " select c.Emp_No ,(c.emp_first_name || ' ' || c.emp_middle_name || ' ' || c.emp_last_name) as EMP_NAME,";
            sqlQuery += "  a.ELIGIBLE_PLAN_ID  as ELIGIBLE_PLAN_ID , a.SELECTED_PLAN_ID as SELECTED_PLAN_ID,a.PLAN_ENTRY_DTL_ID,";
            sqlQuery += "  a.TOTAL_COST ,a.COMPANY_COST as COMPANY_COST,a.EMP_COST , a.FAMILY_MEMBER , a.TOTAL_AMOUNT , 0 as DELETED , c.EMP_ID,e.PLAN_LEVAL as ELIGIBLE_LEVEL_PLAN,f.PLAN_LEVAL as SELECTED_PLAN_LEVEL ";
            sqlQuery += "  from INSURANCE_PLAN_ENTRY_DTL a, INSURANCE_PLAN_ENTRY_HDR b , HR_EMPLOYEES c  , INSURANCE_PLAN e, INSURANCE_PLAN f";
            sqlQuery += "  where b.Plan_Entry_Id = a.plan_entry_id and a.emp_id = c.emp_id and a.ELIGIBLE_PLAN_ID = e.PLAN_ID  ";
            sqlQuery += "  and a.SELECTED_PLAN_ID = f.PLAN_ID and b.PLAN_ENTRY_ID = '" + planid +"' and b.FISCAL_YEAR = '" + finyear + "'";
            //sqlQuery += "  union";
            //sqlQuery += "  select a.EMP_NO,(a.emp_first_name || ' ' || a.emp_middle_name || ' ' || a.emp_last_name) as EMP_NAME,";
            //sqlQuery += "  '0' as ELIGIBLE_PLAN_ID , '0' as SELECTED_PLAN_ID , 0  as TOTAL_COST,0 as COMPANY_COST,";
            //sqlQuery += "  0 as EMP_COST , 0 as FAMILY_MEMBER , 0 as TOTAL_AMOUNT, 0 as DELETED, a.EMP_ID,'0' as ELIGIBLE_LEVEL_PLAN,'0' as SELECTED_PLAN_LEVEL from HR_EMPLOYEES a , HR_EMP_WORK_DTLS b, HR_DEPARTMENTS c";
            //sqlQuery += "  where a.Emp_Id= b.Emp_Id and b.Emp_Dept_Id = c.dept_id and a.emp_org_id = b.emp_org_id";
            //sqlQuery += "  and b.emp_org_id = c.org_id and a.emp_id not in (select EMP_ID from INSURANCE_PLAN_ENTRY_DTL where PLAN_ENTRY_ID = " + planid + ")";

            return sqlQuery;
        }

        public static string GetPlanAmountDtls(string planid)
        {
            sqlQuery = string.Empty;
            sqlQuery = " select TOTAL_COST,COMPANY_COST,EMP_COST from INSURANCE_PLAN where PLAN_ID = '" +  planid + "'";
            

            //sqlQuery += "  union";
            //sqlQuery += "  select a.EMP_NO,(a.emp_first_name || ' ' || a.emp_middle_name || ' ' || a.emp_last_name) as EMP_NAME,";
            //sqlQuery += "  '0' as ELIGIBLE_PLAN_ID , '0' as SELECTED_PLAN_ID , 0  as TOTAL_COST,0 as COMPANY_COST,";
            //sqlQuery += "  0 as EMP_COST , 0 as FAMILY_MEMBER , 0 as TOTAL_AMOUNT, 0 as DELETED, a.EMP_ID,'0' as ELIGIBLE_LEVEL_PLAN,'0' as SELECTED_PLAN_LEVEL from HR_EMPLOYEES a , HR_EMP_WORK_DTLS b, HR_DEPARTMENTS c";
            //sqlQuery += "  where a.Emp_Id= b.Emp_Id and b.Emp_Dept_Id = c.dept_id and a.emp_org_id = b.emp_org_id";
            //sqlQuery += "  and b.emp_org_id = c.org_id and a.emp_id not in (select EMP_ID from INSURANCE_PLAN_ENTRY_DTL where PLAN_ENTRY_ID = " + planid + ")";

            return sqlQuery;
        }


    }
}
