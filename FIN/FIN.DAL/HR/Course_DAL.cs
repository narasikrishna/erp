﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;


namespace FIN.DAL.HR
{
    public class Course_DAL
    {
        static string sqlQuery = "";

        public static string GetSubjectdtls(String COURSE_ID)
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT S.SUBJECT_ID,S.SUB_CODE,";
            sqlQuery += " S.SUBJECT_DESC,CASE S.INSTRUCTOR_FLAG WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS INSTRUCTOR_FLAG,";
            sqlQuery += " CASE S.CBT_FLAG WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS CBT_FLAG,'N' AS DELETED";
            sqlQuery += " FROM HR_TRM_SUBJECT S";
            sqlQuery += " WHERE ";
            sqlQuery += " S.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND S.COURSE_ID = '" + COURSE_ID + "'";

            return sqlQuery;

        }


        public static string GetFacilitydtls()
        {
            sqlQuery = string.Empty;

            sqlQuery = " select F.FACILITY_MASTER_ID AS FACILITY_ID,F.ROOM_NUMBER||' '||F.ROOM_NAME AS FACILITY_DTL";
            sqlQuery += " from HR_FACILITY_HDR F";
            sqlQuery += " WHERE F.ENABLED_FLAG = 1";
            sqlQuery += " AND F.WORKFLOW_COMPLETION_STATUS = 1";
            return sqlQuery;

        }

        public static string GetCourseBasedPgm(string pgmId)
        {
            sqlQuery = string.Empty;


            sqlQuery += "  select C.COURSE_ID,C.COURSE_DESC";
            sqlQuery += "  from HR_TRM_COURSE c, hr_trm_prog_dtl d";
            sqlQuery += "  where c.course_id = d.prog_course_id";
            sqlQuery += " AND C.ENABLED_FLAG = 1";
            sqlQuery += " AND C.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += "  and d.prog_id='" + pgmId + "'";
            sqlQuery += " AND C.COURSE_ORG_ID = '" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " order by C.COURSE_DESC asc";


            return sqlQuery;

        }

        public static string GetSubject()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT S.SUBJECT_ID,S.SUBJECT_DESC";
            sqlQuery += " FROM HR_TRM_SUBJECT S";
            sqlQuery += " WHERE S.ENABLED_FLAG = 1";
            sqlQuery += " AND S.WORKFLOW_COMPLETION_STATUS = 1";

            return sqlQuery;

        }
        public static string GetSubjectBasedCourse(string courseId)
        {
            sqlQuery = string.Empty;

            sqlQuery = "  SELECT S.SUBJECT_ID,S.SUBJECT_DESC";
            sqlQuery += "  FROM HR_TRM_SUBJECT S";
            sqlQuery += "  WHERE S.ENABLED_FLAG = 1";
            sqlQuery += "  AND S.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += "  and s.COURSE_ID='" + courseId + "'";
            sqlQuery += "  order by s.SUBJECT_DESC asc";
            return sqlQuery;

        }


        public static string GetCourse()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT HC.COURSE_ID, HC.COURSE_DESC ";
            sqlQuery += " from HR_TRM_COURSE HC";
            sqlQuery += " WHERE HC.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND HC.ENABLED_FLAG = 1";

            return sqlQuery;

        }
        public static string GetTriningCourse(string assignHdrId)
        {
            sqlQuery = string.Empty;

            sqlQuery = "  SELECT distinct HC.COURSE_ID, HC.COURSE_DESC";
            sqlQuery += "  from HR_TRM_COURSE HC, hr_appraisal_training att,HR_PER_APP_ASSIGNMENT_HDR aa";
            sqlQuery += "  WHERE HC.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += "    AND HC.ENABLED_FLAG = 1";
            sqlQuery += "    and att.course_id = hc.course_id";         
            sqlQuery += "  and aa.assign_app_id='" + assignHdrId + "'";
            sqlQuery += "   and aa.assign_hdr_id = att.assign_hdr_id";
            sqlQuery += " order by HC.COURSE_DESC asc";

            return sqlQuery;

        }

    }
}
