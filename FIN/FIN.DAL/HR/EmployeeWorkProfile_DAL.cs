﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;

namespace FIN.DAL.HR
{
    public class EmployeeWorkProfile_DAL
    {
        static string sqlQuery = "";


        public static string GetEmployeeName(string Dept_ID, string Desg_ID)
        {
            sqlQuery = string.Empty;

            string strLngCode = VMVServices.Web.Utils.LanguageCode;
            sqlQuery = " SELECT E.EMP_ID,E.EMP_NO || ' - ' || E.EMP_FIRST_NAME" + strLngCode + "||' '||E.EMP_MIDDLE_NAME" + strLngCode + "||' '||E.EMP_LAST_NAME" + strLngCode + " AS EMPLOYEE_NAME";
            sqlQuery += " FROM HR_EMPLOYEES E,HR_EMP_WORK_DTLS WD ";
            sqlQuery += " WHERE WD.EMP_ID = E.EMP_ID  ";
            sqlQuery += " AND WD.WORKFLOW_COMPLETION_STATUS =1 ";
            sqlQuery += " AND WD.ENABLED_FLAG = 1";
            sqlQuery += " AND WD.EMP_DEPT_ID = '" + Dept_ID + "'";
            sqlQuery += " AND WD.EMP_DESIG_ID = '" + Desg_ID + "' ";
            sqlQuery += " and wd.effective_to_dt is null";
         //   sqlQuery += " AND E.EMP_INTERNAL_EXTERNAL = 'I'";

            return sqlQuery;

        }
        public static string GetEmployeeprofdtl(string profileId="")
        {
            sqlQuery = string.Empty;
            sqlQuery = "  SELECT PD.PROF_DTL_ID,CD.COM_LEVEL || ' - ' || CD.COM_LEVEL_DESC as COM_LEVEL_DESC";
            sqlQuery += " FROM HR_EMP_PROFILE_DTLS PD,HR_COMPETENCY_DTL cd, hr_emp_profile_hdr ph ";
            sqlQuery += " WHERE PD.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND PD.COM_LINE_ID = CD.COM_LINE_ID";
            sqlQuery += " and ph.prof_id = pd.prof_id";
            sqlQuery += " and ph.prof_id='" + profileId + "'";
            return sqlQuery;
        }

        //public static string GetEmployeeWorkProfileDtlid(string COM_HDR_ID, string COM_LINE_ID)
        //{
        //    sqlQuery = string.Empty;

        //    sqlQuery = " SELECT LD.COM_LINK_DTL_ID,CD.COM_LEVEL_DESC";
        //    sqlQuery += " FROM HR_COMPETENCY_HDR C,hr_competency_dtl CD,Hr_Competancy_Links_Dtl LD";
        //    sqlQuery += "  WHERE C.COM_HDR_ID = CD.COM_HDR_ID";
        //    sqlQuery += " AND CD.COM_LINE_ID = LD.COM_LINE_ID";
        //    sqlQuery += " AND C.COM_HDR_ID = '" + COM_HDR_ID + "'";
        //    sqlQuery += " AND CD.COM_LINE_ID = '" + COM_LINE_ID + "'";
        //    sqlQuery += " AND C.WORKFLOW_COMPLETION_STATUS = 1";
        //    sqlQuery += "  AND C.ENABLED_FLAG = 1";
        //    return sqlQuery;

        //}


        public static string GetEmpWorkProfdtls(String PROF_ID)
        {
            sqlQuery = string.Empty;
            string strLngCode = VMVServices.Web.Utils.LanguageCode;
            sqlQuery = " SELECT WPD.EMP_WORK_PROF_DTL_ID,WPD.EMP_ID,WPD.EMP_PROF_DTL_ID,WPD.EMPLOYEE_RATING,E.EMP_ID,CD.COM_LEVEL_DESC,";
            sqlQuery += "  E.EMP_FIRST_NAME" + strLngCode + " || '' || E.EMP_MIDDLE_NAME" + strLngCode + " || '' || E.EMP_LAST_NAME" + strLngCode + " AS EMPLOYEE_NAME,EPD.PROF_DTL_ID ,'N' AS DELETED";
            sqlQuery += " FROM HR_EMP_WORK_PROFILE_DTL WPD, HR_EMPLOYEES E,HR_EMP_PROFILE_DTLS EPD,HR_COMPETENCY_DTL CD ";
            sqlQuery += "  WHERE WPD.EMP_ID = E.EMP_ID ";
            sqlQuery += " AND WPD.EMP_PROF_DTL_ID=EPD.PROF_DTL_ID ";
            sqlQuery += " AND EPD.COM_LINE_ID = CD.COM_LINE_ID";
            sqlQuery += " AND WPD.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND WPD.EMP_WORK_PROF_ID = '" + PROF_ID + "'";
            sqlQuery += " order by WPD.EMP_WORK_PROF_DTL_ID asc";

            return sqlQuery;

        }
        public static string GetTerminationDetails(string fromDate,string toDate)
        {
            sqlQuery = string.Empty;
            sqlQuery = "  select ee.emp_no,";
            sqlQuery += "   ee.emp_first_name" + VMVServices.Web.Utils.LanguageCode + " || ' ' || ee.emp_middle_name" + VMVServices.Web.Utils.LanguageCode + " || ' ' ||";
            sqlQuery += "   ee.emp_last_name" + VMVServices.Web.Utils.LanguageCode + "  as emp_name,";
            sqlQuery += "   to_char(to_date(ee.termination_date,'dd/MM/yyyy')) as termination_date";
            sqlQuery += "   from hr_employees ee";
            sqlQuery += "   where ee.termination_date is not null";
            sqlQuery += "    and ee.workflow_completion_status='1'";
            sqlQuery += "    and ee.enabled_flag='1'";
            sqlQuery += "  and ee.termination_date between to_date('" + fromDate + "','dd/MM/yyyy') and to_date('" + toDate + "','dd/MM/yyyy') ";

            sqlQuery += "    order by ee.emp_first_name asc";
            return sqlQuery;

        }

        public static string GetDeptidfromEMpid(string empid)
        {
            sqlQuery = string.Empty;
            sqlQuery += " Select b.EMP_DEPT_ID from HR_EMPLOYEES a, HR_EMP_WORK_DTLS b";
            sqlQuery += "   where a.Emp_Id = b.Emp_Id and a.EMP_ID = '"+ empid + "'";
            return sqlQuery;
        }
    }
}
