﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;

namespace FIN.DAL.HR
{
    public class FinalSettlement_DAL
    {
        static string sqlQuery = "";

        public static string GetFinalSettlementdtls(String str_emp_id)
        {
            sqlQuery = string.Empty;

            //sqlQuery += " select distinct ptdd.FNL_STL_DTL_ID,'' as PAYROLL_DTL_ID,";
            //sqlQuery += " '' as PAYROLL_DTL_DTL_ID,pe.PAY_ELE_CLASS as pay_element_Type,  ptdd.amt_value pay_amount,'' pay_deduction_amount,";
            //sqlQuery += " '' as pay_net_amount,'' PAY_EMP_ID,pe.pay_element_id PAY_EMP_ELEMENT_ID,(pe.pay_element_code || ' - ' || pe.pay_element_desc) as pay_element_desc,";
            //sqlQuery += " '' as emp_name  from HR_FINAL_SETTLEMENT_DTL ptdd, PAY_ELEMENTS pe";
            //sqlQuery += " where ptdd.element_id = pe.pay_element_id";
            //sqlQuery += " and ptdd.fnl_stl_hdr_id= '" + str_emp_id + "'";

            sqlQuery += "  select distinct ptdd.FNL_STL_DTL_ID,'' as PAYROLL_DTL_ID,'N' AS DELETED,";
            sqlQuery += "      '' as PAYROLL_DTL_DTL_ID,";
            sqlQuery += "      pe.PAY_ELE_CLASS as pay_element_Type,     ";
            sqlQuery += "     to_char(round(ptdd.AMT_VALUE, " + VMVServices.Web.Utils.DecimalPrecision  +")) pay_amount,";
            sqlQuery += "  fsh.emp_id as PAY_EMP_ID,";
            sqlQuery += " ptdd.ELEMENT_ID as PAY_EMP_ELEMENT_ID,";
            sqlQuery += "  (pe.pay_element_code || ' - ' || pe.pay_element_desc) as pay_element_desc,";
            sqlQuery += "  (select (e.emp_first_name || ' - ' || e.emp_middle_name ||";
            sqlQuery += "       ' - ' || e.emp_last_name)";
            sqlQuery += "   from hr_employees e";
            sqlQuery += "   where e.emp_id = fsh.emp_id) as emp_name";
            sqlQuery += "   from HR_FINAL_SETTLEMENT_DTL ptdd, PAY_ELEMENTS pe,hr_final_settlement_hdr fsh";
            sqlQuery += "   where ptdd.element_id = pe.pay_element_id";
            sqlQuery += "   AND fsh.fnl_stl_hdr_id= ptdd.fnl_stl_hdr_id and  fsh.emp_id= '" + str_emp_id + "'";
            sqlQuery += "  order by pe.PAY_ELE_CLASS";


            return sqlQuery;

        }


        public static string getPayDtlFromTmp(string str_emp_id)
        {
            string sqlQuery = "";
            sqlQuery += "  select distinct '0' as FNL_STL_DTL_ID,'' as PAYROLL_DTL_ID,'N' AS DELETED,";
            sqlQuery += "      '' as PAYROLL_DTL_DTL_ID,";
            sqlQuery += "      pe.PAY_ELE_CLASS as pay_element_Type,     ";
            sqlQuery += "     to_char(round(ptdd.pay_amount, 3)) pay_amount,";
            //sqlQuery += "   to_char(round(ptdd.pay_dept_ded_amt, 3)) pay_deduction_amount,";
            //sqlQuery += "  to_char(round(ptdd.pay_amount - ptdd.pay_dept_ded_amt, 3)) as pay_net_amount,";
            sqlQuery += "   ptdd.PAY_EMP_ID,";
            sqlQuery += "  ptdd.PAY_EMP_ELEMENT_ID,";
            sqlQuery += "  (pe.pay_element_code || ' - ' || pe.pay_element_desc) as pay_element_desc,";
            sqlQuery += "  (select (e.emp_first_name || ' - ' || e.emp_middle_name ||";
            sqlQuery += "       ' - ' || e.emp_last_name)";
            sqlQuery += "   from hr_employees e";
            sqlQuery += "   where e.emp_id = ptdd.PAY_EMP_ID) as emp_name";
            sqlQuery += "   from TMP_PAYROLL ptdd, PAY_ELEMENTS pe";
            sqlQuery += "   where ptdd.pay_emp_element_id = pe.pay_element_id";
            sqlQuery += "  and pe.pay_element_id = ptdd.PAY_EMP_ELEMENT_ID";
            sqlQuery += "  and ptdd.PAY_EMP_ID = '" + str_emp_id + "'";
            sqlQuery += "  order by pe.PAY_ELE_CLASS";
            return sqlQuery;
        }



        public static void UpdateLeaveLedger_FinalSettlment(string str_Emp_Id)
        {
           // DBMethod.ExecuteNonQuery("");
           
        }
        public static void UpdateIndemnityAmount_FinalSettlment(string str_emp_id)
        {
           // DBMethod.ExecuteNonQuery("");
        }


    }
}
