﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.HR
{
    public class RepayPlan_DAL
    {
        static string sqlQuery = "";

        //public static string getRepayPlanDetailsDetails(string StrID)
        //{
        //    sqlQuery = string.Empty;

        //    sqlQuery = " SELECT HARP.REPAY_ID, HARP.RES_ID, HARP.REPAY_AMT, HARP.INSTALLMENT_NO, HARP.REPAY_DT, HARP.REPAY_ACTUAL_DT, HARP.REPAY_COMMENTS, HARP.REPAY_PAIDYN, 'N' AS DELETED ";
        //    sqlQuery += " FROM HR_ADVANCE_REPAY_PLAN HARP";
        //    sqlQuery += " WHERE HARP.WORKFLOW_COMPLETION_STATUS = 1";
        //    sqlQuery += " AND HARP.ENABLED_FLAG = 1";
        //    sqlQuery += " AND HARP.RES_ID = '" + StrID + "'";
        //    sqlQuery += " ORDER BY HARP.REPAY_ID ";

        //    return sqlQuery;
        //}

        public static string getRepayPlanDetailsDetails(string StrID)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select 0 as REPAY_ID,REQ_ID,(1 + LEVEL - 1) as INSTALLMENT_NO, REPAY_AMT,'' AS REPAY_DT,'' as REPAY_ACTUAL_DT,'FALSE' as REPAY_PAIDYN,'' as REPAY_COMMENTS ";
            sqlQuery += " from (select to_char((AR.REQ_AMOUNT / AR.NO_OF_INSTALLMENTS)) as REPAY_AMT,0 as REPAY_ID,AR.REQ_ID,AR.NO_OF_INSTALLMENTS,'' as REPAY_DT,'' as REPAY_ACTUAL_DT,";
            sqlQuery += " '' as REPAY_PAIDYN,'' as REPAY_COMMENTS from HR_ADVANCE_REQ AR where AR.REQ_ID = '" + StrID + "'";
            sqlQuery += " AND AR.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND AR.ENABLED_FLAG = 1)";
            sqlQuery += " CONNECT BY LEVEL <= NO_OF_INSTALLMENTS ";

            return sqlQuery;
        }

        public static string getRepayPlanDetailsDetails_Edit(string StrID)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select ARP.INSTALLMENT_NO,to_char(ARP.REPAY_AMT) as REPAY_AMT,ARP.REPAY_DT,ARP.REPAY_ACTUAL_DT,ARP.REPAY_COMMENTS, ";
            sqlQuery += " CASE ARP.REPAY_PAIDYN WHEN '1' THEN 'TRUE' ELSE 'FALSE' END as REPAY_PAIDYN,ARP.REPAY_ID";
            sqlQuery += " from HR_ADVANCE_REPAY_PLAN ARP ";
            sqlQuery += " where ARP.ENABLED_FLAG = '1' ";
            sqlQuery += " and ARP.WORKFLOW_COMPLETION_STATUS = '1'";
            sqlQuery += " and ARP.RES_ID = '" + StrID + "'";
            sqlQuery += " Order by ARP.INSTALLMENT_NO ";

            return sqlQuery;
        }
        public static string GetRepayDetailsBasedReq(string reqID)
        {
            sqlQuery = string.Empty;
            sqlQuery = "  SELECT HAR.REQ_DEPT_ID, HAR.REQ_DESIG_ID, HAR.REQ_DT, HAR.REQ_ADVANCE_TYPE, HAR.REQ_AMOUNT ";
            sqlQuery += " FROM HR_ADVANCE_REQ HAR";
            sqlQuery += " WHERE HAR.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " AND HAR.ENABLED_FLAG = 1 ";
            sqlQuery += " AND HAR.REQ_ID = '" + reqID + "'";

            return sqlQuery;
        }

        public static string GetLoanReqId(string Emp_id)
        {
            sqlQuery = string.Empty;
            sqlQuery = "  select HAR.REQ_ID from HR_ADVANCE_REQ HAR ";
            sqlQuery += " WHERE HAR.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " AND HAR.ENABLED_FLAG = 1 ";
            sqlQuery += " AND HAR.REQ_EMP_ID =  '" + Emp_id + "'";

            return sqlQuery;
        }

        public static string GetDept_design_amt(string Reg_id)
        {
            sqlQuery = string.Empty;
            sqlQuery = "  select HD.DEPT_NAME" + VMVServices.Web.Utils.LanguageCode + " AS DEPT_NAME,HDD.DESIG_NAME" + VMVServices.Web.Utils.LanguageCode + " AS DESIG_NAME,to_char(HAR.REQ_AMOUNT) as REQ_AMOUNT,HAR.REQ_ADVANCE_TYPE ";
            sqlQuery += " from HR_ADVANCE_REQ HAR,HR_DEPARTMENTS HD,HR_DEPT_DESIGNATIONS HDD ";
            sqlQuery += " where HAR.REQ_DEPT_ID = HD.DEPT_ID ";
            sqlQuery += " and HAR.REQ_DESIG_ID = HDD.DEPT_DESIG_ID ";
            sqlQuery += " and HAR.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " AND HAR.ENABLED_FLAG = 1 ";
            sqlQuery += " AND HAR.REQ_ID =  '" + Reg_id + "'";

            return sqlQuery;
        }

        public static string GetEmployee_nameEdit(string Req_id)
        {
            sqlQuery = string.Empty;
            sqlQuery = "  select distinct  HE.EMP_ID,HE.EMP_FIRST_NAME" + VMVServices.Web.Utils.LanguageCode + " AS EMP_FIRST_NAME ";
            sqlQuery += " from HR_ADVANCE_REPAY_PLAN ARP,HR_ADVANCE_REQ AR,HR_EMPLOYEES HE ";
            sqlQuery += " where AR.REQ_ID = ARP.RES_ID ";
            sqlQuery += " and HE.EMP_ID = AR.REQ_EMP_ID ";
            sqlQuery += " and AR.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " AND AR.ENABLED_FLAG = 1 ";
            sqlQuery += " and ARP.RES_ID = '" + Req_id + "'";

            return sqlQuery;
        }
    }
}
