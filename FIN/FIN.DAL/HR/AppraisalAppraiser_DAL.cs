﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.HR
{
    public class AppraisalAppraiser_DAL
    {

        static string sqlQuery = "";


        public static string GetAppraisalDetails(string Master_id)
        {
            sqlQuery = string.Empty;
            string strLngCode = VMVServices.Web.Utils.LanguageCode;
            sqlQuery = " SELECT ARAD.RA_DTL_ID, ARAD.RA_EMP_ID AS RA_EMP_ID, HE.EMP_NO ||' - '|| HE.EMP_FIRST_NAME"+ strLngCode+"|| ' - ' ||  HE.EMP_MIDDLE_NAME" + strLngCode + " ||' - '|| HE.EMP_LAST_NAME" + strLngCode + " AS EMP_NAME, 'N' AS DELETED ";
            sqlQuery += " FROM HR_PER_APP_REVIEW_ASGNMNT_DTL ARAD, HR_EMPLOYEES HE ";
            sqlQuery += " WHERE ARAD.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " AND ARAD.ENABLED_FLAG = 1 ";
            sqlQuery += " AND ARAD.RA_EMP_ID = HE.EMP_ID ";
            sqlQuery += " AND HE.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " AND HE.ENABLED_FLAG = 1 ";
            sqlQuery += " AND ARAD.RA_HDR_ID =   '" + Master_id + "'";
            sqlQuery += " ORDER BY RA_EMP_ID ";


            return sqlQuery;

        }

        public static string GetElementSalaryDetails(string Master_id)
        {
            sqlQuery = string.Empty;
            sqlQuery = "select a.LEAVE_SAL_ELEMENT_ID , a.PAY_EMP_ID,a.PAY_ELEMENT_ID,to_char(a.PAY_AMOUNT) as PAY_AMOUNT,0 as DELETED,a.LC_ID,b.PAY_ELEMENT_ID as LOOKUP_ID , b.PAY_ELEMENT_CODE || ' - ' || b.PAY_ELEMENT_DESC as LOOKUP_NAME";
            sqlQuery += " from HR_EMP_LEAVE_SAL_ELEMENT a, PAY_ELEMENTS b";
            sqlQuery += " where a.Pay_Element_Id = b.PAY_ELEMENT_ID and A.LC_ID = '" + Master_id + "'";

            return sqlQuery;
        }

        public static string GetAppraiser4Appraisal(string str_Appsal_id)
        {
            sqlQuery = string.Empty;
            sqlQuery += " SELECT RAH.RA_HDR_ID,(E.emp_no|| ' - ' ||E.emp_first_name" + VMVServices.Web.Utils.LanguageCode + "||' '||E.emp_middle_name" + VMVServices.Web.Utils.LanguageCode + "||' '||E.emp_last_name" + VMVServices.Web.Utils.LanguageCode + ") AS EMP_FIRST_NAME,E.EMP_ID FROM HR_PER_APP_REVIEW_ASGNMNT_HDR RAH ";
            sqlQuery += " INNER JOIN HR_EMPLOYEES E ON E.EMP_ID = RAH.RA_REVIEWER_ID ";
            sqlQuery += " where RAH.ASSIGN_HDR_ID='" + str_Appsal_id + "'";
           // sqlQuery += " AND E.EMP_INTERNAL_EXTERNAL = 'I'";
            return sqlQuery;
        }
        public static string GetAppraiser()
        {
            sqlQuery = string.Empty;
            sqlQuery += " SELECT RAH.RA_HDR_ID,(E.emp_no|| ' - ' ||E.emp_first_name" + VMVServices.Web.Utils.LanguageCode + "||' '||E.emp_middle_name" + VMVServices.Web.Utils.LanguageCode + "||' '||E.emp_last_name" + VMVServices.Web.Utils.LanguageCode + ") AS EMP_FIRST_NAME,E.EMP_ID FROM HR_PER_APP_REVIEW_ASGNMNT_HDR RAH ";
            sqlQuery += " INNER JOIN HR_EMPLOYEES E ON E.EMP_ID = RAH.RA_REVIEWER_ID ";
            
           // sqlQuery += " WHERE E.EMP_INTERNAL_EXTERNAL = 'I'";
            return sqlQuery;
        }

        public static string GetAppraisee4Appraiser(string str_Appraiser_id)
        {
            sqlQuery = string.Empty;

            sqlQuery += " SELECT RAD.RA_DTL_ID,(E.emp_no|| ' - ' ||E.emp_first_name" + VMVServices.Web.Utils.LanguageCode + "||' '||E.emp_middle_name" + VMVServices.Web.Utils.LanguageCode + "||' '||E.emp_last_name" + VMVServices.Web.Utils.LanguageCode + ") AS EMP_FIRST_NAME,E.EMP_ID FROM HR_PER_APP_REVIEW_ASGNMNT_DTL RAD  ";
            sqlQuery += " INNER JOIN HR_EMPLOYEES E ON E.EMP_ID= RAD.RA_EMP_ID ";
            sqlQuery += " WHERE RAD.RA_HDR_ID='" + str_Appraiser_id + "'";
           // sqlQuery += " AND E.EMP_INTERNAL_EXTERNAL = 'I'";

            return sqlQuery;
        }

    }
}
