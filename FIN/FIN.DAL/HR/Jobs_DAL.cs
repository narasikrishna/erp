﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;

namespace FIN.DAL.HR
{
    public class Jobs_DAL
    {

        static string sqlQuery = "";

        public static string GetJobCategory(string str_Category)
        {
            sqlQuery = string.Empty;


            sqlQuery = " select G.JOB_ID,G.JOB_CODE,G.JOB_DESC,G.JOB_CODE" + VMVServices.Web.Utils.LanguageCode + " || ' - ' || + G.JOB_DESC" + VMVServices.Web.Utils.LanguageCode + " AS JOB_NAME";
            sqlQuery += ",G.EFFECTIVE_FROM_DATE,G.EFFECTIVE_TO_DATE,G.ENABLED_FLAG ";
            sqlQuery += " from HR_JOBS G";
            sqlQuery += " WHERE G.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND G.ENABLED_FLAG = 1";
            sqlQuery += " AND G.job_org_id = '" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " AND (G.EFFECTIVE_TO_DATE IS NULL OR (SYSDATE BETWEEN G.EFFECTIVE_FROM_DATE AND G.EFFECTIVE_TO_DATE)) ";
            sqlQuery += " AND G.CATEGORY_ID='" + str_Category + "'";
            sqlQuery += " ORDER BY G.JOB_DESC";

            return sqlQuery;

        }

        public static string Getjobdtls(String JOB_ID)
        {
            sqlQuery = string.Empty;


            sqlQuery = " select j.job_id,j.job_code,j.job_desc,j.JOB_CODE_OL,j.JOB_DESC_OL,j.effective_from_date,j.effective_to_date,'N' AS DELETED,";
            sqlQuery += " CASE J.ENABLED_FLAG WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS ENABLED_FLAG";
            sqlQuery += " from Hr_Jobs j";
            sqlQuery += " WHERE J.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND J.JOB_ID = '" + JOB_ID + "'";

            return sqlQuery;

        }






        public static string Getjobdtls4Category(String Cat_ID)
        {
            sqlQuery = string.Empty;


            sqlQuery = " select j.job_id,j.job_code,j.job_desc,j.JOB_CODE_OL,j.JOB_DESC_OL,j.effective_from_date,j.effective_to_date,'N' AS DELETED,";
            sqlQuery += " CASE J.ENABLED_FLAG WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS ENABLED_FLAG";
            sqlQuery += " from Hr_Jobs j";
            sqlQuery += " WHERE J.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND J.CATEGORY_ID = '" + Cat_ID + "'";
            sqlQuery += " order by j.job_id asc";

            return sqlQuery;

        }


        public static string GetSectiondtls4Dept(String DEPT_ID)
        {
            sqlQuery = string.Empty;


            sqlQuery = " select S.SEC_ID,S.SEC_NAME,S.DEPT_ID,S.SEC_NAME_OL,S.SEGMENT_ID,S.SEGMENT_VALUE_ID,'N' AS DELETED,";
            sqlQuery += " CASE S.ENABLED_FLAG WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS ENABLED_FLAG";
            sqlQuery += " from HR_SECTION S";
            sqlQuery += "  WHERE S.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND S.DEPT_ID = '" + DEPT_ID + "'";
            sqlQuery += " order by S.SEC_ID asc";

            return sqlQuery;

        }


        public static string GetGradename(String GRADE_ID)
        {
            sqlQuery = string.Empty;



            sqlQuery = " select C.CATEGORY_CODE" + VMVServices.Web.Utils.LanguageCode + " || ' - ' || G.GRADE_DESC" + VMVServices.Web.Utils.LanguageCode + " AS GRADE_DESCRIPTION";
            sqlQuery += " from HR_GRADES G, HR_CATEGORIES C";
            sqlQuery += " WHERE C.CATEGORY_ID = G.CATEGORY_ID";
            sqlQuery += " AND  G.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND G.ENABLED_FLAG = 1";
            sqlQuery += " AND G.GRADE_ID = '" + GRADE_ID + "'";
            sqlQuery += " ORDER BY G.GRADE_ID";

            return sqlQuery;

        }

        public static string GetJobname()
        {
            sqlQuery = string.Empty;


            sqlQuery = " SELECT J.JOB_ID,J.JOB_CODE, J.JOB_CODE" + VMVServices.Web.Utils.LanguageCode + " ||' - '|| J.JOB_DESC" + VMVServices.Web.Utils.LanguageCode + " as JOB_NAME";
            sqlQuery += " FROM HR_JOBS J";
            sqlQuery += " WHERE J.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND J.ENABLED_FLAG = 1";
            sqlQuery += " AND J.JOB_ORG_ID = '" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " AND (J.EFFECTIVE_TO_DATE IS NULL OR (SYSDATE BETWEEN J.EFFECTIVE_FROM_DATE AND J.EFFECTIVE_TO_DATE)) ";
            sqlQuery += " ORDER BY J.JOB_DESC";

            return sqlQuery;

        }

        public static string GetJobname_basedon_category(string CATEGORY_ID)
        {
            sqlQuery = string.Empty;


            sqlQuery = " SELECT J.JOB_ID,J.JOB_CODE, J.JOB_CODE" + VMVServices.Web.Utils.LanguageCode + " ||' - '|| J.JOB_DESC" + VMVServices.Web.Utils.LanguageCode + " as JOB_NAME";
            sqlQuery += " FROM HR_JOBS J";
            sqlQuery += " WHERE J.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND J.ENABLED_FLAG = 1";
            sqlQuery += " AND J.CATEGORY_ID = '" + CATEGORY_ID + "'";
            sqlQuery += " AND J.JOB_ORG_ID = '" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " AND (J.EFFECTIVE_TO_DATE IS NULL OR (SYSDATE BETWEEN J.EFFECTIVE_FROM_DATE AND J.EFFECTIVE_TO_DATE)) ";
            sqlQuery += " ORDER BY J.JOB_DESC";

            return sqlQuery;

        }


       


                static string spOutPut = string.Empty;
        public static System.Collections.SortedList ErrorCollection = new System.Collections.SortedList();


        public static string GetSPFOR_ERR_MGR_JOBS(string P_JOB_CODE, string P_RECORD_ID)
        {
            try
            {
                string retcode = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "PKG_VALIDATIONS.PROC_ERR_MGR_JOBS";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add("@P_MODULE_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_SCREEN_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_JOB_CODE", OracleDbType.Char).Value = P_JOB_CODE;                
                oraCmd.Parameters.Add("@P_RECORD_ID", OracleDbType.Char).Value = P_RECORD_ID;
                oraCmd.Parameters.Add("@P_RET", OracleDbType.Varchar2, 150);
                oraCmd.Parameters["@P_RET"].Direction = ParameterDirection.Output;
                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

                retcode = oraCmd.Parameters["@P_RET"].Value.ToString();

                return retcode;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
            public static string Get_Childdataof_Job(String JOB_ID)
            {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT JR.JOB_ID  ";
            sqlQuery += " FROM HR_JOB_RESPONSIBILITY JR";
            sqlQuery += " WHERE JR.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += "  AND JR.ENABLED_FLAG = 1";
            sqlQuery += " AND JR.JOB_ID = '" + JOB_ID  + "'";
            return sqlQuery;

            }
            public static string get_JobOfferedReport()
            {
                sqlQuery = string.Empty;
                sqlQuery = " SELECT * FROM VW_JOB_OFFERED V WHERE ROWNUM > 0 ";
                if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
                {
                    if (VMVServices.Web.Utils.ReportViewFilterParameter["HR_LTR_APPLICANT_ID"] != null)
                    {
                        sqlQuery += " AND V.HR_LTR_APPLICANT_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["HR_LTR_APPLICANT_ID"].ToString() + "'";
                    }
                    if (VMVServices.Web.Utils.ReportViewFilterParameter["FROM_DATE"] != null)
                    {
                        sqlQuery += " AND V.HR_LTR_DOJ >= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["FROM_DATE"].ToString() + "','dd/MM/yyyy')";
                    }
                    if (VMVServices.Web.Utils.ReportViewFilterParameter["TO_DATE"] != null)
                    {
                        sqlQuery += " AND V.HR_LTR_DOJ <= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["TO_DATE"].ToString() + "','dd/MM/yyyy')";
                    }
                }
                return sqlQuery;
            }
      
    }
    


    }

