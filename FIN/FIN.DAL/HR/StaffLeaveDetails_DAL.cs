﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data.EntityClient;
using System.Data;
namespace FIN.DAL.HR
{
    public class StaffLeaveDetails_DAL
    {

        static string sqlQuery = "";

        public static string GetStaffLeaveDtls(string staffLeaveId)
        {
            sqlQuery = string.Empty;

            sqlQuery = "    select s.LSD_ID,";
            sqlQuery += "    s.ORG_ID,";
            sqlQuery += "     s.FISCAL_YEAR,";
            sqlQuery += "     s.STAFF_ID,";
            sqlQuery += "      s.LEAVE_ID,s.no_of_days,";
            sqlQuery += "    LL.LEAVE_DESC  AS LEAVE_NAME,LL.NO_OF_DAYS,";
            sqlQuery += "      s.DEPT_ID,";
            sqlQuery += "      s.LEAVE_AVAILED,";
            //            sqlQuery += "      s.LEAVE_BALANCE,";
            sqlQuery += " to_char(ROUND(s.LEAVE_BALANCE," + VMVServices.Web.Utils.DecimalPrecision + ")) as LEAVE_BALANCE, ";
            sqlQuery += " to_char(ROUND(s.ACCURED_LEAVE_BALANCE," + VMVServices.Web.Utils.DecimalPrecision + ")) as ACCURED_LEAVE_BALANCE, ";

            sqlQuery += "  CASE s.ENABLED_FLAG WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS ENABLED_FLAG,";
            sqlQuery += "       s.WORKFLOW_COMPLETION_STATUS,";
            sqlQuery += "      'N' AS deleted";
            sqlQuery += "    from HR_LEAVE_STAFF_DEFINTIONS s,HR_LEAVE_DEFINITIONS LL";
            sqlQuery += "  where s.workflow_completion_status = 1";
            sqlQuery += "  and LL.LEAVE_ID = s.LEAVE_ID and LL.ORG_ID= s.ORG_ID";
            sqlQuery += "  AND s.LSD_ID = '" + staffLeaveId + "'";
            sqlQuery += " and s.org_id='" + VMVServices.Web.Utils.OrganizationID + "'";

            return sqlQuery;

        }

        public static string GetStaffLeaveDts(string staffLeaveId)
        {
            sqlQuery = string.Empty;

            sqlQuery += " select lsd.lsd_id,lsd.FISCAL_YEAR,lsd.STAFF_ID,lsd.DEPT_ID";
            sqlQuery += " from HR_LEAVE_STAFF_DEFINTIONS lsd";
            sqlQuery += " where lsd.workflow_completion_status = 1";
            sqlQuery += " and lsd.enabled_flag = 1";
            sqlQuery += " and lsd.lsd_id =  '" + staffLeaveId + "'";
            return sqlQuery;

        }



        public static string GetStaffLeaveDtls_asperemp(string fiscal_year, string dept_id, string staff_id)
        {
            sqlQuery = string.Empty;

            sqlQuery += " select s.LSD_ID,s.ORG_ID,s.FISCAL_YEAR,s.STAFF_ID,s.LEAVE_ID,s.no_of_days,LL.LEAVE_DESC  AS LEAVE_NAME,LL.NO_OF_DAYS,";
            sqlQuery += " s.DEPT_ID,s.LEAVE_AVAILED,";
            sqlQuery += " to_char(ROUND(s.LEAVE_BALANCE," + VMVServices.Web.Utils.DecimalPrecision + ")) as LEAVE_BALANCE, ";
            sqlQuery += "   to_char(ROUND(s.Accured_leave_Balance," + VMVServices.Web.Utils.DecimalPrecision + ")) as Accured_leave_Balance";
            //sqlQuery += "  case ll.leave_id when 'AL' then";
            //sqlQuery += "  to_char(ROUND(s.Accured_leave_Balance," + VMVServices.Web.Utils.DecimalPrecision + "))";
            //sqlQuery += "  else ";
            //sqlQuery += "   to_char(ROUND(s.LEAVE_BALANCE," + VMVServices.Web.Utils.DecimalPrecision + ")) end as Accured_leave_Balance";


            sqlQuery += "  ,CASE s.ENABLED_FLAG WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS ENABLED_FLAG,";
            sqlQuery += "  s.WORKFLOW_COMPLETION_STATUS,'N' AS deleted  from HR_LEAVE_STAFF_DEFINTIONS s,HR_LEAVE_DEFINITIONS LL";
            sqlQuery += "  where s.workflow_completion_status = 1";
            sqlQuery += "  and LL.LEAVE_ID = s.LEAVE_ID";
            sqlQuery += "  and s.fiscal_year = '" + fiscal_year + "'";
            sqlQuery += "  and s.dept_id = '" + dept_id + "'";
            sqlQuery += "  and s.staff_id = '" + staff_id + "'";
            sqlQuery += "  and LL.org_id=s.org_id";
            sqlQuery += "  and LL.org_id='" + VMVServices.Web.Utils.OrganizationID + "'";
            return sqlQuery;

        }



        public static string GetLeaveBal(string leave_id, string fiscal_yr)
        {
            sqlQuery = string.Empty;


            sqlQuery = " select l.no_of_days";
            sqlQuery += " from HR_LEAVE_DEFINITIONS l";
            sqlQuery += " where l.workflow_completion_status = 1";
            sqlQuery += " and l.leave_id = '" + leave_id + "'";
            sqlQuery += " and l.fiscal_year = '" + fiscal_yr + "'";

            return sqlQuery;

        }
        public static string GetNoOfDays(string leave_id)
        {
            sqlQuery = string.Empty;


            sqlQuery = " select l.no_of_days";
            sqlQuery += " from HR_LEAVE_DEFINITIONS l";
            sqlQuery += " where l.workflow_completion_status = 1";
            sqlQuery += " and l.leave_id = '" + leave_id + "'";
            //sqlQuery += " and l.fiscal_year = '" + fiscal_yr + "'";

            return sqlQuery;

        }
        public static string GetMonth(String From_Date)
        {
            sqlQuery = string.Empty;


            sqlQuery = " select to_char(to_date('" + From_Date + "','dd/MM/yyyy'),'MM') as Month from dual;";


            return sqlQuery;

        }

        public static string getLeaveDetails4Empid(string str_empid)
        {
            sqlQuery = string.Empty;
            sqlQuery += " SELECT LSD.LSD_ID, ACD.CAL_ACCT_YEAR,LD.LEAVE_ID,LSD.NO_OF_DAYS, LSD.LEAVE_AVAILED,ROUND(LSD.LEAVE_BALANCE,3) as LEAVE_BALANCE  ";
            sqlQuery += " FROM HR_LEAVE_STAFF_DEFINTIONS LSD ";
            sqlQuery += " INNER JOIN GL_ACCT_CALENDAR_DTL ACD ON LSD.FISCAL_YEAR= ACD.CAL_DTL_ID ";
            sqlQuery += " INNER JOIN HR_LEAVE_DEFINITIONS LD ON LD.LEAVE_ID= LSD.LEAVE_ID AND LSD.ORG_ID = LD.ORG_ID ";
            sqlQuery += " WHERE LSD.STAFF_ID='" + str_empid + "'";
            return sqlQuery;
        }
        public static string getAccuredleavebalnce(string str_deptid, string str_empid, string str_leaveid, string fiscaldate, string str_FisYear_id = "")
        {
            sqlQuery = string.Empty;

            sqlQuery = " select nvl(sum(round(ss.accured_leave_balance, 3)),0) as accured_leave_balance from HR_LEAVE_STAFF_DEFINTIONS ss";
            sqlQuery += "  where ss.dept_id='" + str_deptid + "'";
            sqlQuery += "  and ss.staff_id='" + str_empid + "'";
            sqlQuery += "  and ss.leave_id='" + str_leaveid + "'";
            if (str_FisYear_id.Length > 0)
            {
                sqlQuery += "  and ss.FISCAL_YEAR ='" + str_FisYear_id + "'";
            }
            else
            {
                sqlQuery += "  and ss.FISCAL_YEAR =(select gg.cal_dtl_id";
                sqlQuery += "  from gl_acct_calendar_dtl gg";
                sqlQuery += "  where gg.enabled_flag = '1'";
                sqlQuery += "  and gg.workflow_completion_status = '1'";
                sqlQuery += "  and (to_date('" + fiscaldate + "', 'dd/mm/yyyy')) between gg.CAL_EFF_START_DT and gg.call_eff_end_dt and rownum=1)";
            }
            return sqlQuery;
        }

        public static string getAppEmpLeaveDtls()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_HR_App_EMP_Leave_details V WHERE ROWNUM > 0 ";
            sqlQuery += " AND V.org_id='" + VMVServices.Web.Utils.OrganizationID + "'";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
                {
                    sqlQuery += " AND V.Leave_date_from  >= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy')";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
                {
                    sqlQuery += " AND V.Leave_date_from  <= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["To_Date"].ToString() + "','dd/MM/yyyy')";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["LeaveType"] != null)
                {
                    sqlQuery += " AND V.leave_id='" + VMVServices.Web.Utils.ReportViewFilterParameter["LeaveType"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["EMP_ID"] != null)
                {
                    sqlQuery += " AND V.EMp_id='" + VMVServices.Web.Utils.ReportViewFilterParameter["EMP_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["DEPT_ID"] != null)
                {
                    sqlQuery += " AND V.dept_id='" + VMVServices.Web.Utils.ReportViewFilterParameter["DEPT_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["DESIG_ID"] != null)
                {
                    sqlQuery += " AND V.dept_desig_id='" + VMVServices.Web.Utils.ReportViewFilterParameter["DESIG_ID"].ToString() + "'";
                }
            }
            return sqlQuery;
        }
        public static void GetSP_UPDATE_STAFF_ATTENDANCE(string P_USER_ID, string P_TRAN_ID, string P_REASON, string P_TRN_DATE, decimal P_TRN_MONTH, string P_FISCAL_YEAR, string P_EMP_ID, string P_ORG_ID, string P_LEAVE_ID, string P_DEPT_ID, decimal P_DAYS)
        {
            try
            {
                string strValue = string.Empty;
                string strNextCode = string.Empty;
                string strErrMsg = string.Empty;
                DateTime dt_tmp = DBMethod.ConvertStringToDate(P_TRN_DATE);


                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "HR_PKG.hr_update_leave_attendance";
                oraCmd.CommandType = CommandType.StoredProcedure;

                oraCmd.Parameters.Add(new OracleParameter("@p_emp_id", OracleDbType.Varchar2, 250)).Value = P_EMP_ID;
                oraCmd.Parameters.Add(new OracleParameter("@p_leave_type", OracleDbType.Varchar2, 250)).Value = P_LEAVE_ID;
                if (P_TRN_DATE != null)
                {
                    oraCmd.Parameters.Add(new OracleParameter("@p_leave_from_date", OracleDbType.Date, 250)).Value = dt_tmp.ToString("dd/MMM/yyyy");
                }
                else
                {
                    oraCmd.Parameters.Add(new OracleParameter("@p_leave_from_date", OracleDbType.Date, 250)).Value = null;
                }
                oraCmd.Parameters.Add(new OracleParameter("@p_no_days", OracleDbType.Int32, 250)).Value = P_DAYS;
                oraCmd.Parameters.Add(new OracleParameter("@p_org_id", OracleDbType.Varchar2, 250)).Value = P_ORG_ID;
                oraCmd.Parameters.Add(new OracleParameter("@p_dept_id", OracleDbType.Varchar2, 250)).Value = P_DEPT_ID;
                oraCmd.Parameters.Add(new OracleParameter("@p_fin_year", OracleDbType.Varchar2, 250)).Value = P_FISCAL_YEAR;
                oraCmd.Parameters.Add(new OracleParameter("@p_leave_reason", OracleDbType.Varchar2, 250)).Value = P_REASON;
                oraCmd.Parameters.Add(new OracleParameter("@p_leave_req_id", OracleDbType.Varchar2, 250)).Value = P_TRAN_ID;
                oraCmd.Parameters.Add(new OracleParameter("@p_user", OracleDbType.Varchar2, 250)).Value = P_USER_ID;

                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void GetSP_EMP_LEAVE_TRN_LEDGER(string P_TRAN_TYPE, string P_TRAN_ID, string P_TXN_TYPE, string P_TRN_DATE, decimal P_TRN_MONTH, string P_FISCAL_YEAR, string P_EMP_ID, string P_ORG_ID, string P_LEAVE_ID, string P_DEPT_ID, decimal P_DAYS)
        {
            try
            {
                string strValue = string.Empty;
                string strNextCode = string.Empty;
                string strErrMsg = string.Empty;
                DateTime dt_tmp = DBMethod.ConvertStringToDate(P_TRN_DATE);


                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "PKG_PAYROLL.EMP_LEAVE_TXN_LEDGER";
                oraCmd.CommandType = CommandType.StoredProcedure;

                oraCmd.Parameters.Add(new OracleParameter("@p_tran_type", OracleDbType.Varchar2, 250)).Value = P_TRAN_TYPE;
                oraCmd.Parameters.Add(new OracleParameter("@p_tran_id", OracleDbType.Varchar2, 250)).Value = P_TRAN_ID;
                oraCmd.Parameters.Add(new OracleParameter("@p_txn_type", OracleDbType.Varchar2, 250)).Value = P_TXN_TYPE;
                if (P_TRN_DATE != null)
                {
                    oraCmd.Parameters.Add(new OracleParameter("@p_trn_date", OracleDbType.Date, 250)).Value = dt_tmp.ToString("dd/MMM/yyyy");
                }
                else
                {
                    oraCmd.Parameters.Add(new OracleParameter("@p_trn_date", OracleDbType.Date, 250)).Value = null;
                }
                oraCmd.Parameters.Add(new OracleParameter("@p_trn_month", OracleDbType.Int32, 250)).Value = P_TRN_MONTH;
                oraCmd.Parameters.Add(new OracleParameter("@p_fiscal_year", OracleDbType.Varchar2, 250)).Value = P_FISCAL_YEAR;
                oraCmd.Parameters.Add(new OracleParameter("@p_emp_id", OracleDbType.Varchar2, 250)).Value = P_EMP_ID;
                oraCmd.Parameters.Add(new OracleParameter("@p_org_id", OracleDbType.Varchar2, 250)).Value = P_ORG_ID;
                oraCmd.Parameters.Add(new OracleParameter("@p_leave_id", OracleDbType.Varchar2, 250)).Value = P_LEAVE_ID;
                oraCmd.Parameters.Add(new OracleParameter("@p_dept_id", OracleDbType.Varchar2, 250)).Value = P_DEPT_ID;
                oraCmd.Parameters.Add(new OracleParameter("@p_days", OracleDbType.Int32, 250)).Value = P_DAYS;

                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static string GetSP_EMP_LEAVE_PROVISION(string P_dept_id, string p_Emp_id, string p_leave_id, DateTime p_Date, string p_Trans_typ)
        {
            try
            {
                try
                {
                    string strQuery = string.Empty;

                    DataTable dtData = new DataTable();

                    //cmtd on 21/01/2015 as per ilam req
                    //  decimal dec = DBMethod.GetDecimalValue("select nvl(round(HR_PKG.get_leave_provision('" + VMVServices.Web.Utils.OrganizationID + "','" + P_dept_id + "','" + p_Emp_id + "','" + p_leave_id + "','" + p_Date.ToString("dd/MMM/yyyy") + "'),2),0) from dual");
                    decimal dec = DBMethod.GetDecimalValue("select nvl(round(HR_PKG.get_leave_provision('" + VMVServices.Web.Utils.OrganizationID + "','" + P_dept_id + "','" + p_Emp_id + "','" + p_leave_id + "','" + p_Date.ToString("dd/MMM/yyyy") + "','" + p_Trans_typ + "'),2),0) from dual");

                    //   decimal dec = DBMethod.GetDecimalValue("select round(HR_PKG.get_leave_provision('" + VMVServices.Web.Utils.OrganizationID + "','" + P_dept_id + "','" + p_Emp_id + "','" + p_leave_id + "','" + p_Date+ "'),2) from dual");


                    strQuery = dec.ToString();
                    //  strQuery = DBMethod.GetStringValue("select ssm.GET_REP_LIST_QUERY('" + str_ScrenCode + "','" + VMVServices.Web.Utils.OrganizationID + "') from dual");
                    //  dtData = DBMethod.ExecuteQuery(strQuery).Tables[0];

                    return strQuery;

                }
                catch (Exception ex)
                {
                    throw ex;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static void UPDATE_EMP_LEAVE_BALANCE(string P_FISCAL_YEAR, string P_EMP_ID, string P_ORG_ID, string P_LEAVE_ID, string P_DEPT_ID, decimal P_DAYS, string leaveMode = "")
        {
            try
            {

                string strQuery = string.Empty;
                decimal balvalue = 0;
                decimal availLeave = 0;

                decimal balanceLeave = 0;
                decimal availableLeave = 0;

                DataTable dtData = new DataTable();

                //balvalue = DBMethod.GetDecimalValue("select PKG_PAYROLL.get_emp_leave_bal('" + P_FISCAL_YEAR + "','" + VMVServices.Web.Utils.OrganizationID + "','" + P_DEPT_ID + "','" + P_EMP_ID + "','" + P_LEAVE_ID + "') from dual");
                //availLeave = DBMethod.GetDecimalValue("select HR_TA.get_emp_leave_Avail('" + P_FISCAL_YEAR + "','" + VMVServices.Web.Utils.OrganizationID + "','" + P_DEPT_ID + "','" + P_EMP_ID + "','" + P_LEAVE_ID + "') from dual");


                //if (leaveMode.ToUpper() == "APPLICATION")
                //{
                //    balanceLeave = balvalue;//// -P_DAYS;
                //    availableLeave = availLeave; //+P_DAYS;
                //}
                //else if (leaveMode.ToUpper() == "CANCELLATION")
                //{
                //    balanceLeave = balvalue;// +P_DAYS;
                //    availableLeave = availLeave;// -P_DAYS;
                //}


                //strQuery = "  update HR_LEAVE_STAFF_DEFINTIONS ff set ff.MODIFIED_BY='" + VMVServices.Web.Utils.UserName + "',ff.MODIFIED_DATE='" + DateTime.Today.ToString("dd-MMM-yyyy") + "', ff.leave_availed='" + availableLeave + "',ff.leave_balance='" + balanceLeave + "'";
                //strQuery += "  ,ff.ACCURED_LEAVE_BALANCE='" + balanceLeave + "'";
                //strQuery += "  where ff.staff_id='" + P_EMP_ID + "' and ff.fiscal_year='" + P_FISCAL_YEAR + "' and ff.org_id='" + VMVServices.Web.Utils.OrganizationID + "' and ff.dept_id='" + P_DEPT_ID + "'";
                //strQuery += "  AND FF.LEAVE_ID='" + P_LEAVE_ID + "'";

                // DBMethod.ExecuteNonQuery(strQuery);

                if (leaveMode.ToUpper() == "APPLICATION")
                {
                    strQuery = "  update HR_LEAVE_STAFF_DEFINTIONS ff set ff.MODIFIED_BY='" + VMVServices.Web.Utils.UserName + "',ff.MODIFIED_DATE='" + DateTime.Today.ToString("dd-MMM-yyyy") + "', ff.leave_availed=leave_availed +" + P_DAYS + ",ff.leave_balance=leave_balance-" + P_DAYS + "";
                    strQuery += "  ,ff.ACCURED_LEAVE_BALANCE=ACCURED_LEAVE_BALANCE-" + P_DAYS;
                    strQuery += "  where ff.staff_id='" + P_EMP_ID + "' and ff.fiscal_year='" + P_FISCAL_YEAR + "' and ff.org_id='" + VMVServices.Web.Utils.OrganizationID + "' and ff.dept_id='" + P_DEPT_ID + "'";
                    strQuery += "  AND FF.LEAVE_ID='" + P_LEAVE_ID + "'";
                }
                else if (leaveMode.ToUpper() == "CANCELLATION")
                {

                    strQuery = "  update HR_LEAVE_STAFF_DEFINTIONS ff set ff.MODIFIED_BY='" + VMVServices.Web.Utils.UserName + "',ff.MODIFIED_DATE='" + DateTime.Today.ToString("dd-MMM-yyyy") + "', ff.leave_availed=leave_availed -" + P_DAYS + ",ff.leave_balance=leave_balance+" + P_DAYS + "";
                    strQuery += "  ,ff.ACCURED_LEAVE_BALANCE=ACCURED_LEAVE_BALANCE+" + P_DAYS;
                    strQuery += "  where ff.staff_id='" + P_EMP_ID + "' and ff.fiscal_year='" + P_FISCAL_YEAR + "' and ff.org_id='" + VMVServices.Web.Utils.OrganizationID + "' and ff.dept_id='" + P_DEPT_ID + "'";
                    strQuery += "  AND FF.LEAVE_ID='" + P_LEAVE_ID + "'";
                }

                DBMethod.ExecuteNonQuery(strQuery);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static void UPDATE_Staff_Attendance(string P_FISCAL_YEAR, string P_EMP_ID, string P_ORG_ID, string LEAVE_REQ_ID, string P_DEPT_ID, DateTime frm_dt, DateTime to_dt, string LEAVE_REQ_CNCL)
        {
            try
            {

                string strQuery = string.Empty;



                strQuery = " update HR_STAFF_ATTENDANCE ff set ff.MODIFIED_BY='" + VMVServices.Web.Utils.UserName + "',ff.MODIFIED_DATE='" + DateTime.Today.ToString("dd-MMM-yyyy") + "', ff.LEAVE_REQ_ID='" + LEAVE_REQ_CNCL + "'";
                strQuery += "  where ff.staff_id='" + P_EMP_ID + "' and ff.fiscal_year='" + P_FISCAL_YEAR + "' and ff.org_id='" + VMVServices.Web.Utils.OrganizationID + "' and ff.dept_id='" + P_DEPT_ID + "'";
                strQuery += "  AND FF.leave_req_id='" + LEAVE_REQ_ID + "'";
                strQuery += "  AND FF.attendance_date between '" + frm_dt.ToString("dd/MMM/yyyy") + "' AND '" + to_dt.ToString("dd/MMM/yyyy") + "'";

                DBMethod.ExecuteNonQuery(strQuery);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //public static void GetSP_EMP_LEAVE_TRN_LEDGER_Old(string P_TRAN_TYPE, string P_TRAN_ID, string P_TXN_TYPE, string P_TRN_DATE, int P_TRN_MONTH, string P_FISCAL_YEAR, string P_EMP_ID, string P_ORG_ID, string P_LEAVE_ID, string P_DEPT_ID, int P_DAYS)
        //{
        //    try
        //    {
        //        //string retcode = string.Empty;
        //        DateTime dt_tmpDate = DateTime.ParseExact(P_TRN_DATE, "dd/MM/yyyy", System.Globalization.CultureInfo.GetCultureInfo("en-US"));

        //        FINEntities context = new FINEntities();
        //        EntityConnection conn = (EntityConnection)context.Connection;
        //        OracleConnection oraConn = (OracleConnection)conn.StoreConnection;
        //        oraConn.Open();

        //        //  OracleCommand oraCmd = new OracleCommand();
        //        OracleCommand oraCmd = new OracleCommand("PKG_PAYROLL.EMP_LEAVE_TXN_LEDGER", oraConn);
        //        oraCmd.CommandText = "PKG_PAYROLL.EMP_LEAVE_TXN_LEDGER";
        //        oraCmd.CommandType = CommandType.StoredProcedure;
        //        oraCmd.BindByName = true;

        //        oraCmd.Parameters.Add(new OracleParameter("@p_tran_type", OracleDbType.Varchar2, 250)).Value = P_TRAN_TYPE;
        //        oraCmd.Parameters.Add(new OracleParameter("@p_tran_id", OracleDbType.Varchar2, 250)).Value = P_TRAN_ID;
        //        oraCmd.Parameters.Add(new OracleParameter("@p_txn_type", OracleDbType.Varchar2, 250)).Value = P_TXN_TYPE;
        //        if (P_TRN_DATE != null)
        //        {
        //            oraCmd.Parameters.Add(new OracleParameter("@p_trn_date", OracleDbType.Date, 250)).Value = "18-JUN-2014"; // dt_tmpDate.ToString("dd/MMM/yyyy");
        //        }
        //        else
        //        {
        //            oraCmd.Parameters.Add(new OracleParameter("@p_trn_date", OracleDbType.Date, 250)).Value = null;
        //        }
        //        oraCmd.Parameters.Add(new OracleParameter("@p_trn_month", OracleDbType.Int32, 250)).Value = P_TRN_MONTH;
        //        oraCmd.Parameters.Add(new OracleParameter("@p_fiscal_year", OracleDbType.Varchar2, 250)).Value = P_FISCAL_YEAR;
        //        oraCmd.Parameters.Add(new OracleParameter("@p_emp_id", OracleDbType.Varchar2, 250)).Value = P_EMP_ID;
        //        oraCmd.Parameters.Add(new OracleParameter("@p_org_id", OracleDbType.Varchar2, 250)).Value = P_ORG_ID;
        //        oraCmd.Parameters.Add(new OracleParameter("@p_leave_id", OracleDbType.Varchar2, 250)).Value = P_LEAVE_ID;
        //        oraCmd.Parameters.Add(new OracleParameter("@p_dept_id", OracleDbType.Varchar2, 250)).Value = P_DEPT_ID;
        //        oraCmd.Parameters.Add(new OracleParameter("@p_days", OracleDbType.Int32, 250)).Value = P_DAYS;

        //        oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

        //        conn.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}



    }
}
