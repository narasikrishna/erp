﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;

namespace FIN.DAL.HR
{
    public class Reimbursementtype_DAL
    {
        static string sqlQuery = "";

        public static string GetReimbursementtype(String REIMB_TYP_ID)
        {
            sqlQuery = string.Empty;


            sqlQuery = " select c.REIMB_TYP_id,c.REIMB_TYP_code,c.REIMB_TYP_desc,C.REIMB_TYP_CODE_OL,C.REIMB_TYP_DESC_OL,c.effective_from_date,c.effective_to_date,'N' AS DELETED,";
            sqlQuery += " case c.enabled_flag when '1' then 'TRUE' else 'FALSE' END AS enabled_flag";
            sqlQuery += " from HR_REIMB_TYP c";
            sqlQuery += " where c.workflow_completion_status =1";
            sqlQuery += " AND C.REIMB_TYP_ID = '" + REIMB_TYP_ID + "'";
            

            return sqlQuery;

        }

        public static string Get_Childdataof_Category(String CATEGORY_ID)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select G.CATEGORY_ID ";
            sqlQuery += " from HR_JOBS G";
            sqlQuery += " WHERE G.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND G.ENABLED_FLAG = 1";
            sqlQuery += " AND G.CATEGORY_ID = '" + CATEGORY_ID + "'";
            return sqlQuery;

        }


        public static string GetCategoryName(String CATEGORY_ID)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select G.CATEGORY_DESC" + VMVServices.Web.Utils.LanguageCode + " as CATEGORY_DESC ";
            sqlQuery += " from HR_CATEGORIES G";
            sqlQuery += " WHERE G.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND G.ENABLED_FLAG = 1";
            sqlQuery += " AND G.CATEGORY_ID = '" + CATEGORY_ID + "'";
            return sqlQuery;

        }

        public static string getCategory(string mode)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select C.CATEGORY_ID,C.CATEGORY_CODE,C.CATEGORY_CODE" + VMVServices.Web.Utils.LanguageCode + "||' - '||C.CATEGORY_DESC" + VMVServices.Web.Utils.LanguageCode + " as CATEGORY_DESC";
            sqlQuery += " from HR_CATEGORIES C";
            sqlQuery += " WHERE C.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND C.CATEGORY_ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            if (mode == FINTableConstant.Add)
            {
                sqlQuery += " AND C.ENABLED_FLAG = 1";
                sqlQuery += " AND (c.EFFECTIVE_TO_DATE IS NULL OR (SYSDATE BETWEEN c.EFFECTIVE_FROM_DATE AND c.EFFECTIVE_TO_DATE)) ";
            }
            sqlQuery += " ORDER BY CATEGORY_DESC";


            return sqlQuery;
        }

        public static string getReimbtype(string mode)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select C.REIMB_TYP_ID,C.REIMB_TYP_CODE,C.REIMB_TYP_CODE" + VMVServices.Web.Utils.LanguageCode + "||' - '||C.REIMB_TYP_DESC" + VMVServices.Web.Utils.LanguageCode + " as REIMB_TYP_DESC";
            sqlQuery += " from HR_REIMB_TYP C";
            sqlQuery += " WHERE C.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND C.REIMB_TYP_ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            if (mode == FINTableConstant.Add)
            {
                sqlQuery += " AND C.ENABLED_FLAG = 1";
                sqlQuery += " AND (c.EFFECTIVE_TO_DATE IS NULL OR (SYSDATE BETWEEN c.EFFECTIVE_FROM_DATE AND c.EFFECTIVE_TO_DATE)) ";
            }
            sqlQuery += " ORDER BY REIMB_TYP_DESC";


            return sqlQuery;
        }


        static string spOutPut = string.Empty;
        public static System.Collections.SortedList ErrorCollection = new System.Collections.SortedList();


        public static string GetSPFOR_ERR_MGR_CATEGORY(string P_CATEGORY_CODE, string P_RECORD_ID)
        {
            try
            {
                string retcode = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "PKG_VALIDATIONS.PROC_ERR_MGR_CATEGORY";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add("@P_MODULE_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_SCREEN_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_CATEGORY_CODE", OracleDbType.Char).Value = P_CATEGORY_CODE;
                oraCmd.Parameters.Add("@P_RECORD_ID", OracleDbType.Char).Value = P_RECORD_ID;
                oraCmd.Parameters.Add("@P_RET", OracleDbType.Varchar2, 150);
                oraCmd.Parameters["@P_RET"].Direction = ParameterDirection.Output;
                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

                retcode = oraCmd.Parameters["@P_RET"].Value.ToString();

                return retcode;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
