﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.HR
{
    public class ShortList_DAL
    {

        static string sqlQuery = "";

        public static string GetShortListData(string shortListId)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select d.short_list_dtl_id,d.short_list_hdr_id,d.applicant_id,(aa.app_first_name || ' ' || aa.app_middle_name || ' ' || aa.app_last_name) as applicant_name,";
            sqlQuery += "  aa.app_mobile as contact_number,aa.app_email email,aa.app_years_experience as tot_exp,";
            sqlQuery += "  (case d.IS_SELECT when '1' then 'TRUE' ELSE 'FALSE' END) AS IS_SELECT,";
            sqlQuery += "  'N' as DELETED,d.workflow_completion_status";
            sqlQuery += " ,(select aa.app_id || '_RESUME'||'.'|| REPLACE(TFD.FILE_EXTENSTION,'.','') from TMP_FILE_DET TFD where tmp_file_id=aa.resume_id) as file_path ";
            sqlQuery += "  from hr_short_list_dtl d,hr_applicants aa";
            sqlQuery += "  where d.enabled_flag='1'";
            sqlQuery += "  and d.applicant_id=aa.app_id";
            sqlQuery += "  and d.workflow_completion_status='1'";
            sqlQuery += "  and d.short_list_hdr_id='" + shortListId + "'";
            sqlQuery += " order by short_list_dtl_id ";

            return sqlQuery;
        }
        public static string GetApplicantsData(string str_vacancy_id, string reqTech, string minExp, string maxExp, string minAge, string MaxAge, string quali = "", string nation = "")
        {
            //    public static string GetApplicantsData(string reqTech, decimal minExp, decimal maxExp, decimal minAge, decimal MaxAge, string quali = "", string nation = "")
            //{
            sqlQuery = string.Empty;

            sqlQuery = "  select a.app_id as applicant_id,";
            sqlQuery += "   (a.app_first_name || ' ' || a.app_middle_name || ' ' ||";
            sqlQuery += "   a.app_last_name) as applicant_name,";
            sqlQuery += "   a.APP_CONTACT_NO as contact_number,";
            sqlQuery += "   a.app_email as email,";
            sqlQuery += "   a.app_prime_skills,";
            sqlQuery += "   a.app_sec_skills,";
            sqlQuery += "   trunc(MONTHS_BETWEEN(SYSDATE, a.app_dob) / 12) as age,";
            sqlQuery += "   a.app_years_experience as tot_exp,";
            sqlQuery += "   0 as SHORT_LIST_DTL_ID,'FALSE' AS IS_SELECT,'N' as DELETED";
            sqlQuery += " ,(select a.app_id || '_RESUME'||'.'|| REPLACE(TFD.FILE_EXTENSTION,'.','') from TMP_FILE_DET TFD where tmp_file_id=a.resume_id) as file_path ";
            sqlQuery += "   from hr_applicants a";
            sqlQuery += "   where a.enabled_flag=1";
            sqlQuery += "   and a.workflow_completion_status=1";
            sqlQuery += "    and a.app_id not in";
            sqlQuery += "    (select ee.app_id";
            sqlQuery += "     from hr_employees ee";
            sqlQuery += "     where ee.enabled_flag = 1";
            sqlQuery += "     and ee.workflow_completion_status = 1 and (ee.app_id is not null))";
            sqlQuery += " and a.app_id not in (select APPLICANT_ID from HR_SHORT_LIST_DTL R,HR_SHORT_LIST_HDR S where R.SHORT_LIST_HDR_ID= S.SHORT_LIST_HDR_ID AND S.VACANCY_ID='" + str_vacancy_id + "' ) ";
            sqlQuery += "  and ( a.attribute10 is not null   ";

            if (reqTech != string.Empty)
            {
                sqlQuery += " or (upper(a.app_prime_skills) like '%" + reqTech.ToUpper() + "%' or  upper(a.app_sec_skills) like '%" + reqTech.ToUpper() + "%')";
            }

            if (minExp != string.Empty && maxExp != string.Empty)
            {
                if (minExp != null && decimal.Parse(minExp) >= 0 && maxExp != null && decimal.Parse(maxExp) > 0)
                {
                    sqlQuery += "    or a.app_years_experience between '" + minExp + "' and '" + maxExp + "' ";
                }
            }
            else if (minExp != string.Empty)
            {
                if (minExp != null && decimal.Parse(minExp) >= 0)
                {
                    sqlQuery += "    or (a.app_years_experience <='" + minExp + "' and a.app_years_experience is not null)";
                }
            }
            else if (maxExp != string.Empty)
            {
                if (maxExp != null && decimal.Parse(maxExp) > 0)
                {
                    sqlQuery += "    or (a.app_years_experience <='" + maxExp + "' and a.app_years_experience is not null)";
                }
            }

            if (minAge != string.Empty && MaxAge != string.Empty)
            {
                if (minAge != null && decimal.Parse(minAge) > 0 && MaxAge != null && decimal.Parse(MaxAge) > 0)
                {
                    sqlQuery += "    or trunc(MONTHS_BETWEEN(SYSDATE, a.app_dob) / 12) between '" + minAge + "' and '" + MaxAge + "'";
                }
            }
            else if (minAge != string.Empty)
            {
                if (minAge != null && decimal.Parse(minAge) > 0)
                {
                    sqlQuery += "    or (trunc(MONTHS_BETWEEN(SYSDATE, a.app_dob) / 12) <='" + minAge + "' and trunc(MONTHS_BETWEEN(SYSDATE, a.app_dob) / 12) is not null)";
                }
            }
            else if (MaxAge != string.Empty)
            {
                if (MaxAge != null && decimal.Parse(MaxAge) > 0)
                {
                    sqlQuery += "    or (trunc(MONTHS_BETWEEN(SYSDATE, a.app_dob) / 12) <='" + MaxAge + "' and trunc(MONTHS_BETWEEN(SYSDATE, a.app_dob) / 12) is not null)";
                }

            }

            if (nation != string.Empty)
            {
                sqlQuery += "    or a.NATIONALITY in (" + nation + ") ";
            }

            if (quali != string.Empty)
            {
                sqlQuery += "    OR  ( a.app_id in (select aq.app_id from hr_applicants_qualification aq where UPPER(aq.app_quali_short_name)  like '%" + quali + "%')) ";
            }

            sqlQuery += "   ) order by a.app_id asc";

            return sqlQuery;
        }
    }
}
