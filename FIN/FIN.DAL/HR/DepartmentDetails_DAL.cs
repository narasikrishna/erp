﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;

namespace FIN.DAL.HR
{
    public class DepartmentDetails_DAL
    {
        static string sqlQuery = "";


        public static string GetDeptDtls(string DEPT_ID)
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT DD.DEPT_DESIG_ID,DD.DESIG_NAME,DD.DESIG_SHORT_NAME,dd.DESIG_SHORT_NAME_OL,dd.DESIG_NAME_OL,DD.PARENT_DEPT_DESIG_ID,";
            sqlQuery += " case DD.ENABLED_FLAG WHEN '1' THEN 'TRUE' else 'FALSE' END AS ENABLED_FLAG";
            sqlQuery += " ,(SELECT DD.DESIG_NAME FROM HR_DEPT_DESIGNATIONS WHERE DEPT_DESIG_ID = DD.PARENT_DEPT_DESIG_ID) AS PARENT_DESIG_DESC ";
            sqlQuery += " , DD.NO_OF_EMP ";
            sqlQuery += " FROM HR_DEPT_DESIGNATIONS DD";
            //sqlQuery += " WHERE DD.ENABLED_FLAG = 1";
            sqlQuery += " WHERE DD.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND DD.DEPT_ID = '" + DEPT_ID + "'";
            return sqlQuery;
        }


        public static string GetDepartmentDetails(string DEPT_DESIG_ID)
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT DD.DEPT_DESIG_ID,DD.DESIG_NAME,DD.DESIG_SHORT_NAME,DD.DESIG_NAME_OL,DD.DESIG_SHORT_NAME_OL,DD.PARENT_DEPT_DESIG_ID,'N' AS DELETED,";
            sqlQuery += " case DD.ENABLED_FLAG WHEN '1' THEN 'TRUE' else 'FALSE' END AS ENABLED_FLAG";
            sqlQuery += " ,(SELECT DD.DESIG_NAME FROM HR_DEPT_DESIGNATIONS WHERE DEPT_DESIG_ID = DD.PARENT_DEPT_DESIG_ID) AS PARENT_DESIG_DESC ";
            sqlQuery += " , DD.NO_OF_EMP ";
            sqlQuery += " FROM HR_DEPT_DESIGNATIONS DD";
            //sqlQuery += " WHERE DD.ENABLED_FLAG = 1";
            sqlQuery += " WHERE DD.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND DD.DEPT_DESIG_ID = '" + DEPT_DESIG_ID + "'";
            return sqlQuery;
        }

       


        public static string GetDesigName()
        {
            sqlQuery = string.Empty;

            sqlQuery = " select dd.dept_desig_id as PARENT_DEPT_DESIG_ID,dd.desig_name"+VMVServices.Web.Utils.LanguageCode+" AS desig_name";
            sqlQuery += " from HR_DEPT_DESIGNATIONS dd";
            sqlQuery += " where dd.enabled_flag = 1";
            sqlQuery += " and dd.workflow_completion_status = 1";
            return sqlQuery;
        }
        public static string GetDesigName4DeptId(string str_DeptID)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select dd.dept_desig_id as PARENT_DEPT_DESIG_ID,dd.desig_name" + VMVServices.Web.Utils.LanguageCode + " AS desig_name";
            sqlQuery += " from HR_DEPT_DESIGNATIONS dd";
            sqlQuery += " where dd.enabled_flag = 1";
            sqlQuery += " and dd.workflow_completion_status = 1";
            sqlQuery += " and dd.DEPT_ID='" + str_DeptID + "'";
            return sqlQuery;
        }

        public static string GetSectionName4DeptId(string str_DeptID)
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT S.SEC_ID,S.SEC_NAME" + VMVServices.Web.Utils.LanguageCode + " AS SEC_NAME ";
            sqlQuery += " FROM HR_SECTION S";
            sqlQuery += " where S.enabled_flag = 1";
            sqlQuery += " and S.workflow_completion_status = 1";
            sqlQuery += " and S.DEPT_ID='" + str_DeptID + "'";
            return sqlQuery;
        }
        public static string GetDepttype(string dept_id)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select d.dept_type";
            sqlQuery += " from hr_departments d";
            sqlQuery += " where d.workflow_completion_status = 1";
            sqlQuery += " and d.enabled_flag = 1";
            sqlQuery += " and d.dept_id = '" + dept_id + "'";
            return sqlQuery;
        }


        public static string GetDeptdtls(String DEPT_ID)
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT D.DEPT_ID,D.DEPT_NAME,D.DEPT_NAME_OL,V.CODE AS VALUE_KEY_ID,V.CODE AS VALUE_NAME,'N' AS DELETED,s.SEGMENT_ID,s.SEGMENT_NAME" + VMVServices.Web.Utils.LanguageCode + " ,sv.SEGMENT_VALUE_ID,sv.SEGMENT_VALUE" + VMVServices.Web.Utils.LanguageCode + ",";
            sqlQuery += "  CASE D.ENABLED_FLAG WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS ENABLED_FLAG";
            sqlQuery += " FROM HR_DEPARTMENTS D,ssm_code_masters v,GL_SEGMENTS s,GL_SEGMENT_VALUES sv";
            sqlQuery += " WHERE D.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " and d.SEGMENT_ID = s.SEGMENT_ID";
            sqlQuery += " and d.SEGMENT_VALUE_ID = sv.SEGMENT_VALUE_ID";
            sqlQuery += " and d.org_id = '" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += "  AND D.DEPT_TYPE = V.CODE  and v.parent_code='DEPT_TYP'";
            sqlQuery += " AND D.DEPT_ID = '" + DEPT_ID + "'";
            return sqlQuery;

        }


        static string spOutPut = string.Empty;
        public static System.Collections.SortedList ErrorCollection = new System.Collections.SortedList();


        public static string GetSPFOR_DUPLICATE_CHECK(string P_DEPT_ID, string P_DESIG_SHORT_NAME, string P_RECORD_ID)
        {
            try
            {
                string retcode = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "PKG_VALIDATIONS.PROC_ERR_MGR_DESIGNATION";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add("@P_MODULE_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_SCREEN_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_DEPT_ID", OracleDbType.Char).Value = P_DEPT_ID;
                oraCmd.Parameters.Add("@P_DESIG_SHORT_NAME", OracleDbType.Char).Value = P_DESIG_SHORT_NAME;
                oraCmd.Parameters.Add("@P_RECORD_ID", OracleDbType.Char).Value = P_RECORD_ID;
                oraCmd.Parameters.Add("@P_RET", OracleDbType.Varchar2, 150);
                oraCmd.Parameters["@P_RET"].Direction = ParameterDirection.Output;
                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

                retcode = oraCmd.Parameters["@P_RET"].Value.ToString();

                return retcode;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }




    }
}
