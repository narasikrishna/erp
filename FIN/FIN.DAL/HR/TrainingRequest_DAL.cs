﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;

namespace FIN.DAL.HR
{
    public class TrainingRequest_DAL
    {
        static string sqlQuery = "";

        public static string GetTrainingReqdtls(String REQ_HDR_ID)
        {
            sqlQuery = string.Empty;


            //sqlQuery = " select r.NO_OF_MEMBERS,R.REQ_DTL_ID,dp.dept_id,Dp.dept_name" + VMVServices.Web.Utils.LanguageCode + " AS dept_name,C.CODE AS LOOKUP_ID,C.CODE AS LOOKUP_NAME,'N' AS DELETED";
            //sqlQuery += " from HR_TRM_REQUEST_DTL R,SSM_CODE_MASTERS C,hr_departments dp";
            //sqlQuery += " WHERE r.REQ_DEPT_ID = dp.dept_id";   
            //sqlQuery += " AND R.REQ_EMP_GROUP = C.CODE and c.parent_code='EMP_GROUP' ";
            //sqlQuery += " AND R.WORKFLOW_COMPLETION_STATUS = 1";
            //sqlQuery += " AND R.REQ_HDR_ID = '" + REQ_HDR_ID + "'";
            //sqlQuery += " order by REQ_DTL_ID ";

            sqlQuery += " select r.NO_OF_MEMBERS,R.REQ_DTL_ID,dp.dept_id,Dp.dept_name AS dept_name,R.REQ_EMP_GROUP AS LOOKUP_ID,R.REQ_EMP_GROUP AS LOOKUP_NAME,'N' AS DELETED";
            sqlQuery += " from HR_TRM_REQUEST_DTL R,hr_departments dp";
            sqlQuery += " WHERE r.REQ_DEPT_ID = dp.dept_id";
            sqlQuery += " AND R.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND R.REQ_HDR_ID = '" + REQ_HDR_ID + "'";
            sqlQuery += " order by REQ_DTL_ID ";

            return sqlQuery;

        }


        public static string GetDeptNM()
        {
            sqlQuery = string.Empty;
            sqlQuery = " select distinct dd.dept_id,d.dept_name" + VMVServices.Web.Utils.LanguageCode + " AS dept_name";
            sqlQuery += " from HR_DEPT_DESIGNATIONS dd,hr_departments d";
            sqlQuery += " where dd.enabled_flag = 1";
            sqlQuery += " and dd.dept_id = d.dept_id";
            sqlQuery += " and dd.ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " and dd.workflow_completion_status = 1";
            return sqlQuery;

        }
        public static string GetRequestType()
        {
            sqlQuery = string.Empty;
            sqlQuery = " select ER.REQ_ID || ' - ' || ER.REQ_TYPE AS REQUEST,ER.REQ_ID ";
            sqlQuery += " from HR_EMP_REQUESTS ER";
            //, SSM_CODE_MASTERS CM ";
            //  sqlQuery += " WHERE CM.CODE = ER.REQ_TYPE ";
            sqlQuery += " WHERE ER.REQ_ORG_ID = '" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " AND ER.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND ER.ENABLED_FLAG = 1";
            return sqlQuery;

        }
        public static string GetRequestDescription(string req_id)
        {
            sqlQuery = string.Empty;
            string strLngCode = VMVServices.Web.Utils.LanguageCode;
            sqlQuery = " SELECT ER.REQ_DESC,E.EMP_FIRST_NAME" + strLngCode + "|| '' || E.EMP_MIDDLE_NAME" + strLngCode + " || '' || E.EMP_LAST_NAME" + strLngCode + " AS EMPLOYEE_NAME,E.EMP_ID";
            sqlQuery += "  FROM HR_EMP_REQUESTS ER, HR_EMPLOYEES E ";
            sqlQuery += " WHERE ER.REQ_EMP_ID = E.EMP_ID ";
            sqlQuery += " AND ER.REQ_ID = '" + req_id + "'";

            return sqlQuery;

        }

        public static string GetRequestPage(string reqid)
        {
            sqlQuery = string.Empty;
            sqlQuery = " select a.REQ_DEPT , a.REQ_DESIG , a.REQ_EMP_ID, a.REQ_DESC,a.REQ_TYPE,a.REQ_DATE,a.REQ_STATUS,a.REQ_HR_COMMENTS,a.ENABLED_FLAG";
            sqlQuery += "    from hR_EMP_REQUESTS a, hR_EMP_REQUESTS_ASSIGNMENT b where b.REQ_ID = a.req_id and b.REQ_ID = '" + reqid + "'";
            return sqlQuery;
        }

        public static string GetProgram()
        {
            sqlQuery = string.Empty;


            sqlQuery = " select p.prog_id,p.prog_desc";
            sqlQuery += " from  HR_TRM_PROG_HDR p";
            sqlQuery += " where p.enabled_flag = 1";
            sqlQuery += " and p.workflow_completion_status = 1";
            sqlQuery += " and PROG_ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";

            return sqlQuery;

        }

    }
}
