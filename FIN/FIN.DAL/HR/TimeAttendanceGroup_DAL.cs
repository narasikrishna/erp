﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.HR
{
    public class TimeAttendanceGroup_DAL
    {
        static string sqlQuery = "";

        public static string GetTmAttndGrpdtls(int PK_ID)
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT GL.PK_ID,E.EMP_ID,E.EMP_NO||''||E.EMP_FIRST_NAME" + VMVServices.Web.Utils.LanguageCode +" AS EMP_NAME,'N' AS DELETED,";
            sqlQuery += " CASE GL.ENABLED_FLAG WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS ENABLED_FLAG";
            sqlQuery += " FROM TM_EMP_GROUP_LINKS GL,HR_EMPLOYEES E";
            sqlQuery += " WHERE GL.TM_EMP_ID = E.EMP_ID";
            sqlQuery += " AND GL.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND GL.TM_GRP_CODE in (SELECT TM_GRP_CODE FROM TM_EMP_GROUP_LINKS WHERE PK_ID=" + PK_ID + ")";
            //sqlQuery += " AND GL.PK_ID = " + PK_ID;

            return sqlQuery;

        }
        public static string GetTmAttndGrpdtls()
        {
            sqlQuery = string.Empty;


            sqlQuery = "   select distinct t.tm_grp_code";
            sqlQuery += "   from tm_emp_group_links t";
            sqlQuery += "  where t.enabled_flag = '1'";
            sqlQuery += "  and t.workflow_completion_status = '1'";

            return sqlQuery;

        }
        public static string GetEmployees(string emp_dept_id)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select 0 as pk_id,e.emp_id,(e.emp_no|| ' - ' ||e.emp_first_name" + VMVServices.Web.Utils.LanguageCode + "||' '||e.emp_middle_name" + VMVServices.Web.Utils.LanguageCode + "||' '||e.emp_last_name" + VMVServices.Web.Utils.LanguageCode + ") as emp_name,'FALSE' AS enabled_flag";
            sqlQuery += " from hr_emp_work_dtls wd,hr_employees e";
            sqlQuery += " where wd.emp_id = e.emp_id";
            sqlQuery += " and wd.workflow_completion_status = 1";
            sqlQuery += " and wd.enabled_flag = 1";
            sqlQuery += " and wd.effective_to_dt is  null";
            sqlQuery += " and wd.emp_org_id = '" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " and wd.emp_dept_id = '" + emp_dept_id + "'";
            sqlQuery += " and not exists (select vv.tm_emp_id from TM_EMP_GROUP_LINKS vv where vv.tm_emp_id = e.emp_id) ";


            return sqlQuery;

        }




    }
}
