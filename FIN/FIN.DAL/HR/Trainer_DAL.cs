﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;


namespace FIN.DAL.HR
{
    public class Trainer_DAL
    {
        static string sqlQuery = "";

        public static string GetTrainer()
        {
            sqlQuery = string.Empty;


            sqlQuery = "    SELECT t.trnr_id as emp_id, t.trnr_name as emp_name";
            sqlQuery += "   FROM hr_trm_trainer t";
            sqlQuery += "   WHERE t.ENABLED_FLAG = 1";
            sqlQuery += "    and t.workflow_completion_status = 1";
            sqlQuery += " and t.TRNR_ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += "   order by t.trnr_name";

            return sqlQuery;

        }

        public static string GetTrainer_frTrainingFeedback(string emp_id)
        {
            sqlQuery = string.Empty;


            sqlQuery = "    SELECT t.trnr_id as emp_id, t.trnr_name as emp_name";
            sqlQuery += "   FROM hr_trm_trainer t";
            sqlQuery += "   WHERE t.ENABLED_FLAG = 1";
            sqlQuery += "   AND T.TRNR_EMP_ID NOT IN ('" + emp_id + "')";
            sqlQuery += "    and t.workflow_completion_status = 1";
            sqlQuery += " and t.TRNR_ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += "   order by t.trnr_name";

            return sqlQuery;

        }


        public static string GetTrainer_ForTrainingEnrolment(string subject_id, string trn_session)
        {
            sqlQuery = string.Empty;

            sqlQuery += " select distinct tt.trnr_id as emp_id,e.emp_first_name as emp_name,tt.trnr_name from HR_TRM_SCHEDULE_DTL tr,hr_trm_trainer tt,hr_employees e";
            sqlQuery += " where tr.trnr_id=e.emp_id";
            sqlQuery += " and e.emp_id=tt.trnr_emp_id";
            sqlQuery += " and tr.subject_id ='" + subject_id + "'";
            sqlQuery += " and tr.trn_session ='" + trn_session + "'";
            sqlQuery += "  order by tt.trnr_name";
            return sqlQuery;

        }

        public static string getRPTTrainingPlan()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_TRAINING_PLAN V WHERE ROWNUM > 0 ";
            // sqlQuery += " AND V.org_id='" + VMVServices.Web.Utils.OrganizationID + "'";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["TRNR_ID"] != null)
                {
                    sqlQuery += "  and v.TRNR_ID ='" + VMVServices.Web.Utils.ReportFilterParameter["TRNR_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
                {
                    sqlQuery += " AND V.TRN_FROM_DT between to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy')";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
                {
                    sqlQuery += " AND to_date('" + VMVServices.Web.Utils.ReportFilterParameter["To_Date"].ToString() + "','dd/MM/yyyy')";
                }
            }
            return sqlQuery;
        }

    }
}
