﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;


namespace FIN.DAL.HR
{
    public class SuccessionPlanning_DAL
    {
        static string sqlQuery = "";

        public static string GetEmpProfile(string careerPlanId)
        {
            sqlQuery = string.Empty;

            sqlQuery += "   select '0' as PROC_DTL_ID,eh.prof_id,CP.PLAN_DTL_ID,eh.prof_name,(case when eh.enabled_flag='1' then 'TRUE' else 'FALSE' end) as enabled_flag,cp.PLAN_PROFILE_HDR_ID,'N' as deleted";
            sqlQuery += " , cd.com_level_desc ";
            sqlQuery += "   from hr_emp_profile_hdr eh, HR_CAREER_PLAN_DTL cp";
            sqlQuery += " , HR_EMP_PROFILE_DTLS EPD, HR_COMPETENCY_DTL CD ";
            sqlQuery += "   where eh.enabled_flag = '1'";
            sqlQuery += "   and cp.plan_profile_hdr_id = eh.prof_id";
            sqlQuery += "   and eh.workflow_completion_status = '1'";
            sqlQuery += " AND CP.PLAN_PROFILE_DTL_ID =EPD.PROF_DTL_ID ";
            sqlQuery += " AND EPD.COM_LINE_ID = CD.COM_LINE_ID ";
            sqlQuery += "   and cp.plan_hdr_id= '" + careerPlanId + "'";
            

            return sqlQuery;

        }
        public static string GetEmpProfileName(string prof_id = "")
        {
            sqlQuery = string.Empty;

            sqlQuery += "   select eh.prof_id, eh.prof_name,'N' as deleted";
            sqlQuery += "   from hr_emp_profile_hdr eh";
            sqlQuery += "   where eh.enabled_flag = '1'";
            sqlQuery += "   and eh.workflow_completion_status = '1'";
            if (prof_id != string.Empty)
            {
                sqlQuery += "   and eh.prof_id '" + prof_id + "'";
            }

            return sqlQuery;

        }

        public static string GetSuccessPlanDtl(string sucesPlanId)
        {
            sqlQuery = string.Empty;

            //sqlQuery = "     select sp.PROC_DTL_ID,eh.prof_id,CP.PLAN_DTL_ID,eh.prof_name,(case when eh.enabled_flag='1' then 'TRUE' else 'FALSE' end) as enabled_flag,cp.PLAN_PROFILE_HDR_ID,'N' as deleted";
            //sqlQuery += "   from hr_emp_profile_hdr eh, HR_CAREER_PLAN_DTL cp,hr_succession_plan_dtl sp";
            //sqlQuery += "   where ";
            //sqlQuery += "     cp.plan_profile_hdr_id = eh.prof_id";
            //sqlQuery += "     and sp.plan_profile_hdr_id=eh.prof_id";
            //sqlQuery += "    and eh.workflow_completion_status = '1'";
            //sqlQuery += "   and sp.proc_hdr_id= '" + sucesPlanId + "'";

            sqlQuery += " select  sp.PROC_DTL_ID,eh.prof_id,eh.prof_name,";
            sqlQuery += " (case  when eh.enabled_flag = '1' then 'TRUE' else 'FALSE' end) as enabled_flag,";
            sqlQuery += " 0 as PLAN_DTL_ID,0 as PLAN_PROFILE_HDR_ID, 'N' as deleted";
            sqlQuery += " , cd.com_level_desc ";
            sqlQuery += " from hr_emp_profile_hdr eh,hr_succession_plan_dtl sp";
            sqlQuery += ", HR_CAREER_PLAN_DTL cp, HR_EMP_PROFILE_DTLS EPD, HR_COMPETENCY_DTL CD ";
            sqlQuery += " where sp.plan_profile_hdr_id = eh.prof_id";
            sqlQuery += " and eh.workflow_completion_status = '1'";
            sqlQuery += "   and cp.plan_profile_hdr_id = eh.prof_id";           
            sqlQuery += " AND CP.PLAN_PROFILE_DTL_ID =EPD.PROF_DTL_ID ";
            sqlQuery += " AND EPD.COM_LINE_ID = CD.COM_LINE_ID ";
            sqlQuery += " and sp.proc_hdr_id = '" + sucesPlanId + "'";
            sqlQuery += " order by sp.PROC_DTL_ID asc";

            return sqlQuery;

        }
        public static string GetProfileData(string ProfileId)
        {
            sqlQuery = string.Empty;
            string strLngCode = VMVServices.Web.Utils.LanguageCode;
            sqlQuery += "  select '' as PROC_DTL_DTL_ID,cp.plan_dtl_id,";
            sqlQuery += "    cp.plan_hdr_id,cp.plan_profile_dtl_id,";
            sqlQuery += "    eh.prof_id,wp.emp_id,";
            sqlQuery += "     (select ee.emp_first_name" + strLngCode + " || ' ' || ee.emp_middle_name" + strLngCode + " || ' ' ||";
            sqlQuery += "           ee.emp_last_name" + strLngCode + "";
            sqlQuery += "      from hr_employees ee";
            sqlQuery += "     where ee.emp_id = wp.emp_id) as emp_name,";
            sqlQuery += "     cp.plan_profile_min_value min_value,cp.plan_profile_max_value max_value,cp.PLAN_PROFILE_REQUIRED_VALUE,";
            sqlQuery += "     wp.employee_rating,";
            sqlQuery += "     '' as recommendations,'' as remarks,";
            sqlQuery += "     (case";
            sqlQuery += "     when eh.enabled_flag = '1' then";
            sqlQuery += "      'TRUE'";
            sqlQuery += "     else";
            sqlQuery += "     'FALSE'";
            sqlQuery += "   end) as enabled_flag,'N' as deleted";
            sqlQuery += "    from hr_emp_profile_hdr      eh,";
            sqlQuery += "      HR_CAREER_PLAN_DTL      cp,";
            sqlQuery += "      HR_EMP_WORK_PROFILE_DTL wp,";
            sqlQuery += "     hr_emp_profile_dtls     pd";
            sqlQuery += "   where eh.enabled_flag = '1'";
            sqlQuery += "    and cp.plan_profile_hdr_id = eh.prof_id";
            sqlQuery += "    and eh.prof_id = pd.prof_id";
            sqlQuery += "     and wp.emp_prof_dtl_id = pd.prof_dtl_id";
            sqlQuery += "   and eh.workflow_completion_status = '1'";
            //  sqlQuery += "   and eh.prof_id in (" + ProfileId + ")";
            sqlQuery += " AND CP.PLAN_DTL_ID  in (" + ProfileId + ")";
            return sqlQuery;

        }


        public static string GetSucPlanDtldtlData(string proc_hdr_id)
        {
            sqlQuery = string.Empty;
            string strLngCode = VMVServices.Web.Utils.LanguageCode;

            sqlQuery += " select dd.PROC_DTL_DTL_ID,";
            sqlQuery += " 0 as plan_dtl_id,0 as plan_hdr_id,dd.plan_profile_dtl_id,sp.plan_profile_hdr_id as prof_id,";
            sqlQuery += " dd.proc_emp_id as emp_id,";
            sqlQuery += " (select ee.emp_first_name || ' ' || ee.emp_middle_name || ' ' ||ee.emp_last_name from hr_employees ee";
            sqlQuery += " where ee.emp_id = dd.proc_emp_id) as emp_name,";
            sqlQuery += " dd.plan_profile_min_value as min_value,";
            sqlQuery += " dd.plan_profile_max_value as max_value,";
            sqlQuery += " dd.PLAN_PROFILE_REQUIRED_VALUE,";
            sqlQuery += " dd.proc_emp_rating as employee_rating,";
            sqlQuery += " dd.proc_recommend as recommendations,";
            sqlQuery += " dd.proc_remarks as remarks,";
            sqlQuery += " (case when dd.enabled_flag = '1' then 'TRUE' else 'FALSE' end) as enabled_flag,";
            sqlQuery += " 'N' as deleted";
            sqlQuery += " from hr_emp_profile_hdr eh,hr_succession_plan_dtl_dtl dd,hr_succession_plan_dtl sp";
            sqlQuery += " where sp.plan_profile_hdr_id = eh.prof_id";
            sqlQuery += " and dd.proc_dtl_id = sp.proc_dtl_id";
            sqlQuery += " and sp.proc_hdr_id = '" + proc_hdr_id + "'";
            sqlQuery += " order by dd.PROC_DTL_DTL_ID asc";


            return sqlQuery;

        }

        public static string GetSuccessPlanDtlDtl(string ProfileId)
        {
            sqlQuery = string.Empty;
            string strLngCode = VMVServices.Web.Utils.LanguageCode;
            sqlQuery += "  select dd.PROC_DTL_DTL_ID,cp.plan_dtl_id,";
            sqlQuery += "    cp.plan_hdr_id,cp.plan_profile_dtl_id,";
            sqlQuery += "    eh.prof_id,wp.emp_id,";
            sqlQuery += "     (select ee.emp_first_name" + strLngCode + " || ' ' || ee.emp_middle_name" + strLngCode + " || ' ' ||";
            sqlQuery += "           ee.emp_last_name" + strLngCode + "";
            sqlQuery += "      from hr_employees ee";
            sqlQuery += "     where ee.emp_id = wp.emp_id) as emp_name,";
            sqlQuery += "     cp.plan_profile_min_value as min_value,cp.plan_profile_max_value as max_value,cp.PLAN_PROFILE_REQUIRED_VALUE,";
            sqlQuery += "     wp.employee_rating,";
            sqlQuery += "     '' as recommendations,'' as remarks,";
            sqlQuery += "     (case";
            sqlQuery += "     when eh.enabled_flag = '1' then";
            sqlQuery += "      'TRUE'";
            sqlQuery += "     else";
            sqlQuery += "     'FALSE'";
            sqlQuery += "   end) as enabled_flag,'N' as deleted";
            sqlQuery += "    from hr_emp_profile_hdr      eh,";
            sqlQuery += "      HR_CAREER_PLAN_DTL      cp,";
            sqlQuery += "      HR_EMP_WORK_PROFILE_DTL wp,";
            sqlQuery += "     hr_emp_profile_dtls     pd,hr_succession_plan_dtl_dtl dd";
            sqlQuery += "   where eh.enabled_flag = '1'";
            sqlQuery += "    and cp.plan_profile_hdr_id = eh.prof_id";
            sqlQuery += "    and eh.prof_id = pd.prof_id";
            sqlQuery += "    and wp.emp_prof_dtl_id = pd.prof_dtl_id";
            sqlQuery += "   and eh.workflow_completion_status = '1'";
            // sqlQuery += "   and eh.prof_id in ('" + ProfileId + "')";
            sqlQuery += "   and CP.PLAN_DTL_ID in (" + ProfileId + ")";
            sqlQuery += "   and dd.proc_emp_id=wp.emp_id";
            sqlQuery += "   and dd.plan_profile_dtl_id=cp.plan_dtl_id";

            return sqlQuery;

        }
    }
}
