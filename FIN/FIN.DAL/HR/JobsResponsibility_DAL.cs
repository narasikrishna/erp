﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;			

namespace FIN.DAL.HR
{
    public class JobsResponsibility_DAL
    {

        static string sqlQuery = "";

        public static string GetJRdtls(String jr_id)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select  v.code as value_key_id,v.code as value_name,jr.jr_id,jr.jr_name,jr.JR_NAME_OL,jr.effective_from_date,jr.effective_to_date,'N' AS DELETED,";
            sqlQuery += " case jr.enabled_flag when '1' then 'TRUE' ELSE 'FALSE' END AS enabled_flag";
            sqlQuery += " from hr_job_responsibility jr,ssm_code_masters v";
            sqlQuery += " where jr.jr_type = v.code";
            sqlQuery += " and jr.workflow_completion_status = 1";
            sqlQuery += " and jr.jr_id = '" + jr_id + "'";

            return sqlQuery;

        }


        public static string GetJobname(String JOB_ID)
        {
            sqlQuery = string.Empty;


            sqlQuery = " SELECT J.JOB_DESC" + VMVServices.Web.Utils.LanguageCode + " as JOB_DESC";
            sqlQuery += " FROM HR_JOBS J";
            sqlQuery += " WHERE J.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND J.ENABLED_FLAG = 1";
            sqlQuery += " AND J.JOB_ID =   '" + JOB_ID + "'";


            return sqlQuery;

        }


        public static string GetJobResdtl(String job_id)
        {
            sqlQuery = string.Empty;


            sqlQuery = " select  v.code as value_key_id,v.code as value_name,jr.jr_id,jr.jr_name,jr.JR_NAME_OL,jr.effective_from_date,jr.effective_to_date,'N' AS DELETED,";
            sqlQuery += " 'TRUE' AS enabled_flag";
            sqlQuery += " from hr_job_responsibility jr,ssm_code_masters v";
            sqlQuery += " where jr.jr_type = v.code";
            sqlQuery += " and jr.workflow_completion_status = 1";
            sqlQuery += " and jr.job_id ='" + job_id + "'";

            return sqlQuery;

        }


        
        static string spOutPut = string.Empty;
        public static System.Collections.SortedList ErrorCollection = new System.Collections.SortedList();


        public static string GetSPFOR_DUPLICATE_CHECK(string P_JOB_ID, string P_JR_TYPE, string P_RECORD_ID)
        {
            try
            {
                string retcode = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "PKG_VALIDATIONS.PROC_ERR_MGR_JOB_RESP";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add("@P_MODULE_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_SCREEN_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_JOB_ID", OracleDbType.Char).Value = P_JOB_ID;
                oraCmd.Parameters.Add("@P_JR_TYPE", OracleDbType.Char).Value = P_JR_TYPE;
                oraCmd.Parameters.Add("@P_RECORD_ID", OracleDbType.Char).Value = P_RECORD_ID;
                oraCmd.Parameters.Add("@P_RET", OracleDbType.Varchar2, 150);
                oraCmd.Parameters["@P_RET"].Direction = ParameterDirection.Output;
                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

                retcode = oraCmd.Parameters["@P_RET"].Value.ToString();

                return retcode;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }


    }
