﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;


namespace FIN.DAL.HR
{
    public class EmployeeProbation_DAL
    {
        static string sqlQuery = "";

        public static string GetEmployeeNumber()
        {
            sqlQuery = string.Empty;

            sqlQuery = " select t.emp_id,t.emp_no ";
            sqlQuery += " from hr_employees t";
            sqlQuery += " where t.workflow_completion_status=1";
            sqlQuery += " and t.enabled_flag=1";
            sqlQuery += " order by t.emp_id asc";

            return sqlQuery;
        }
        public static string GetEmployeeWithoutProbation()
        {
            sqlQuery = string.Empty;

            sqlQuery = "  select t.emp_id,t.emp_no || ' - ' || t.emp_First_Name ||' ' || t.emp_Middle_Name || ' ' || t.emp_last_name as emp_no";
            sqlQuery += "  from hr_employees t";
            sqlQuery += "  where t.workflow_completion_status=1";
            sqlQuery += "  and t.enabled_flag=1";
            sqlQuery += "   and t.emp_org_id='" + VMVServices.Web.Utils.OrganizationID + "'";
            //  sqlQuery += "   and t.emp_id not in (select hh.emp_id from hr_probation_emp_hdr hh)";
            sqlQuery += " and t.prob_flag = 1";
            sqlQuery += " and t.confirmation_date is null";
           // sqlQuery += " and t.emp_internal_external = 'I'";
            sqlQuery += "  order by t.emp_id asc";

            return sqlQuery;
        }

        public static string fn_GetEmpProbationDtls(String prob_id)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select ep.prob_id, ep.prob_from_dt, ep.prob_to_dt, ep.prob_status, SCM.DESCRIPTION" + VMVServices.Web.Utils.LanguageCode + " AS LOOKUP_NAME,ep.PROBATION_PERIOD, ";
            sqlQuery += " CASE ep.ENABLED_FLAG WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS ENABLED_FLAG";
            sqlQuery += " from hr_emp_probation ep, SSM_CODE_MASTERS SCM";
            sqlQuery += " where ep.workflow_completion_status = 1";
            sqlQuery += " and ep.prob_id = '" + prob_id + "'";
            sqlQuery += " AND SCM.CODE = ep.prob_status ";

            return sqlQuery;

        }

        public static string fn_GetEmpProbationAppraiserDtl(String prob_id)
        {
            sqlQuery = string.Empty;

            sqlQuery += " SELECT EPA.PROB_APPR_ID,E.EMP_ID,E.EMP_NO||''||E.EMP_FIRST_NAME AS EMP_NAME";
            sqlQuery += " FROM HR_EMP_PROB_APPRAISER EPA,HR_EMPLOYEES E";
            sqlQuery += " WHERE EPA.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND EPA.EMP_ID  = E.EMP_ID";
            sqlQuery += " AND EPA.PROB_ID = '" + prob_id + "'";

            return sqlQuery;

        }


    }
}
