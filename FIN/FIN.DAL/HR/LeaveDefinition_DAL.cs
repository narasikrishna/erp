﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;

namespace FIN.DAL.HR
{
    public class LeaveDefinition_DAL
    {
        static string sqlQuery = "";

        public static string get_yearEndLeaveCarryOver(string dt_date)
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM vw_yr_end_lve_carry_over V WHERE ROWNUM > 0 ";
            //sqlQuery += " and v.org_id = '" + VMVServices.Web.Utils.OrganizationID + "'";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                //if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
                //{
                sqlQuery += " AND V.leave_txn_date =  to_date('" + dt_date + "','dd/mm/yyyy')";

                //sqlQuery += " AND V.emp_doj <= to_date('" + VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"].ToString() + "','dd/mm/yyyy')";
                //}
            }
            return sqlQuery;
        }


        public static string get_SalaryReviewReport()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_SALARY_REVIEW V WHERE ROWNUM > 0 ";
            //sqlQuery += " AND V.org_id='" + VMVServices.Web.Utils.OrganizationID + "'";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["EMP_ID"] != null)
                {
                    sqlQuery += " AND V.EMP_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["EMP_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
                {
                    sqlQuery += " AND V.effective_to_dt >= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy')";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
                {
                    sqlQuery += " AND V.effective_to_dt <= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["To_Date"].ToString() + "','dd/MM/yyyy')";
                }
            }
            return sqlQuery;
        }

        public static string get_PositionHierarchyReport()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM vw_position_hierarchy V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["category_id"] != null)
                {
                    sqlQuery += " AND V.category_id = '" + VMVServices.Web.Utils.ReportViewFilterParameter["category_id"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["job_id"] != null)
                {
                    sqlQuery += " AND V.job_id = '" + VMVServices.Web.Utils.ReportViewFilterParameter["job_id"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["position_code"] != null)
                {
                    sqlQuery += " AND V.position_id = '" + VMVServices.Web.Utils.ReportViewFilterParameter["position_id"].ToString() + "'";
                }
            }

            return sqlQuery;
        }


        public static string get_EmigrationReport()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM vw_hr_emgration_form V WHERE ROWNUM > 0 ";
            sqlQuery += " AND V.org_id='" + VMVServices.Web.Utils.OrganizationID + "'";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["EMP_ID"] != null)
                {
                    sqlQuery += " AND V.EMP_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["EMP_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["EMP_ID_S"] != null)
                {
                    sqlQuery += " AND V.EMP_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["EMP_ID_S"].ToString() + "'";
                }
            }
            return sqlQuery;
        }


        public static string getcaldate(string start_date)
        {
            sqlQuery = string.Empty;
            sqlQuery = " select distinct cd.call_eff_end_dt ";
            sqlQuery += " from gl_acct_calendar_hdr ch, gl_acct_calendar_dtl cd ";
            sqlQuery += " where ch.cal_id = cd.cal_id ";
            sqlQuery += " and  cd.cal_eff_start_dt < to_date('" + start_date + "','dd/MM/yyyy') ";
            //if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            //{
            //    //if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.EFFECTIVE_FROM_DT] != null)
            //    //{
            //    //    sqlQuery += " AND V.ATTENDANCE_DATE = '" + VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.EFFECTIVE_FROM_DT].ToString() + "'";
            //    //}
            //    //if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.EFFECTIVE_TO_DT] != null)
            //    //{
            //    //    sqlQuery += " AND V.ATTENDANCE_DATE = '" + VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.EFFECTIVE_TO_DT].ToString() + "'";
            //    //}
            //}
            return sqlQuery;
        }

        public static string GetLeaveDefnDtls(int pkId)
        {
            sqlQuery = string.Empty;

            sqlQuery = "   select PK_ID,LEAVE_ID,";
            sqlQuery += "  LEAVE_DESC,";
            sqlQuery += "  ORG_ID,";
            sqlQuery += "  FISCAL_YEAR,LEAVE_ID_OL,";
            sqlQuery += "   LEAVE_TYPE,";
            sqlQuery += "  CASE WITH_PAY_YN WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS WITH_PAY_YN,";
            sqlQuery += "  CASE CARRY_OVER_YN WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS CARRY_OVER_YN,";
            sqlQuery += "  CASE ENABLED_FLAG WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS ENABLED_FLAG,";
            sqlQuery += "    NO_OF_DAYS,";
            sqlQuery += "    EFFECTIVE_FROM_DT,";
            sqlQuery += "    EFFECTIVE_TO_DT,";
            sqlQuery += "     WORKFLOW_COMPLETION_STATUS,";
            sqlQuery += "     'N' AS DELETED";
            sqlQuery += "  from hr_leave_definitions";
            sqlQuery += "  where workflow_completion_status = 1";
            sqlQuery += "  AND PK_ID = " + pkId;
            return sqlQuery;

        }

        public static string getFindate(string cal_dtl_id)
        {
            sqlQuery = string.Empty;
            sqlQuery = " select cd.cal_eff_start_dt ";
            sqlQuery += " from gl_acct_calendar_hdr ch, gl_acct_calendar_dtl cd ";
            sqlQuery += " where ch.cal_id = cd.cal_id ";
            sqlQuery += " and  cd.CAL_DTL_ID = '" + cal_dtl_id + "'";
            //if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            //{
            //    //if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.EFFECTIVE_FROM_DT] != null)
            //    //{
            //    //    sqlQuery += " AND V.ATTENDANCE_DATE = '" + VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.EFFECTIVE_FROM_DT].ToString() + "'";
            //    //}
            //    //if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.EFFECTIVE_TO_DT] != null)
            //    //{
            //    //    sqlQuery += " AND V.ATTENDANCE_DATE = '" + VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.EFFECTIVE_TO_DT].ToString() + "'";
            //    //}
            //}
            return sqlQuery;
        }
        public static string GetLeaveLedgerData(string fromDate, string toDate, string leaveID = "")
        {
            sqlQuery = string.Empty;

            sqlQuery = "   WITH pivot_data AS (select  distinct ee.emp_first_name" + VMVServices.Web.Utils.LanguageCode + " || '' || ee.emp_middle_name" + VMVServices.Web.Utils.LanguageCode + " || '' ||";
            sqlQuery += "   ee.emp_last_name" + VMVServices.Web.Utils.LanguageCode + " as emp_name,hd.LEAVE_DESC,ll.LDR_DATE,";
            sqlQuery += "   ee.emp_no,ll.ldr_leave_id,ll.ldr_open_balance,ll.ldr_leave_id as leave_Id, ll.ldr_leave_bal, ll.ldr_leave_avail";
            sqlQuery += "  from hr_leave_ledger ll,hr_employees ee,hr_leave_definitions hd where ee.emp_id=ll.LDR_EMP_ID and hd.LEAVE_ID=ll.LDR_LEAVE_ID";
            sqlQuery += "   and ll.LDR_ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";

            if (leaveID != string.Empty)
            {
                sqlQuery += "   and ll.LDR_LEAVE_ID='" + leaveID + "'";
            }
            sqlQuery += "   )";
            sqlQuery += "  select * from pivot_data ";
            sqlQuery += "  PIVOT (sum(ldr_leave_bal)";
            sqlQuery += "  for ldr_leave_id in  ";
            sqlQuery += "  ('AL',";
            sqlQuery += " 'EL',";
            sqlQuery += " 'CLD',";
            sqlQuery += " 'CLINMW',";
            sqlQuery += " 'CLIMW',";
            sqlQuery += " 'SL',";
            sqlQuery += " 'ML'))";

            return sqlQuery;

        }
        public static string GetAbsentLeaveDetails(string fromDate, string toDate)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select to_char(to_date(s.attendance_date,'dd/MM/yyyy')) as attendance_date,";
            sqlQuery += "  ee.emp_first_name" + VMVServices.Web.Utils.LanguageCode + " || '' || ee.emp_middle_name" + VMVServices.Web.Utils.LanguageCode + " || '' ||";
            sqlQuery += "   ee.emp_last_name" + VMVServices.Web.Utils.LanguageCode + " as emp_name,";
            sqlQuery += "   ee.emp_no";
            sqlQuery += "  from HR_STAFF_ATTENDANCE s, hr_employees ee";
            sqlQuery += "  where s.enabled_flag = '1'";
            sqlQuery += "  and ee.emp_id = s.staff_id";
            sqlQuery += "  and s.workflow_completion_status = '1'";
            sqlQuery += "  and lower(s.attendance_type)='absent'";
            sqlQuery += "  and s.attendance_date between to_date('" + fromDate + "','dd/MM/yyyy') and to_date('" + toDate + "','dd/MM/yyyy') ";

            return sqlQuery;

        }

        public static string getEmpSalDeduction()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_DEDUCTION_LIST V WHERE ROWNUM > 0 ";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
                {
                    sqlQuery += " AND V.PAY_FROM_DT >= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy')";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
                {
                    sqlQuery += " AND V.PAY_TO_DT <= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["To_Date"].ToString() + "','dd/MM/yyyy')";
                }
            }
            return sqlQuery;
        }


        public static string get_AbsentReport()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_ABSENT_REPORT V WHERE ROWNUM > 0 ";
            sqlQuery += " AND V.org_id='" + VMVServices.Web.Utils.OrganizationID + "'";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
                {
                    sqlQuery += " AND V.ATTENDANCE_DATE >= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy') ";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
                {
                    sqlQuery += " AND V.ATTENDANCE_DATE <= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["To_Date"].ToString() + "','dd/MM/yyyy') ";
                }

                if (VMVServices.Web.Utils.ReportViewFilterParameter["Emp_id"] != null)
                {
                    sqlQuery += "  and V.EMP_ID ='" + VMVServices.Web.Utils.ReportFilterParameter["Emp_id"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["Dept_id"] != null)
                {
                    sqlQuery += "  and V.emp_dept_id ='" + VMVServices.Web.Utils.ReportFilterParameter["Dept_id"].ToString() + "'";
                }
            }
            return sqlQuery;
        }
        public static string get_EmpDetailsReport()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_HR_EMPLOYEE_DETAILS V WHERE ROWNUM > 0 ";
            sqlQuery += " AND V.org_id='" + VMVServices.Web.Utils.OrganizationID + "'";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["DEPT_ID"] != null)
                {
                    sqlQuery += " AND V.dept_id='" + VMVServices.Web.Utils.ReportViewFilterParameter["DEPT_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["DESIGN_ID"] != null)
                {
                    sqlQuery += " AND V.DEPT_DESIG_ID='" + VMVServices.Web.Utils.ReportViewFilterParameter["DESIGN_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["EMP_ID"] != null)
                {
                    sqlQuery += " AND V.EMP_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["EMP_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
                {
                    sqlQuery += " AND V.EMP_DOJ between to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy') ";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
                {
                    sqlQuery += " AND to_date('" + VMVServices.Web.Utils.ReportFilterParameter["To_Date"].ToString() + "','dd/MM/yyyy') ";
                }
            }
            return sqlQuery;
        }


        public static string get_MedicalInsureRequestReport()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM vw_medical_insurance_request V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["EMP_ID"] != null)
                {
                    sqlQuery += " AND V.EMP_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["EMP_ID"].ToString() + "'";
                }
                //if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.EFFECTIVE_FROM_DT] != null)
                //{
                //    sqlQuery += " AND V.EMP_DOJ between to_date('" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.EFFECTIVE_FROM_DT].ToString() + "','dd/MM/yyyy') )";
                //}
                //if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.EFFECTIVE_TO_DT] != null)
                //{
                //    sqlQuery += " AND to_date('" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.EFFECTIVE_TO_DT].ToString() + "','dd/MM/yyyy') )";
                //}
            }
            return sqlQuery;
        }

        public static string get_EmegrationWPhotoReport()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM vw_hr_emgration_form V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["EMP_ID"] != null)
                {
                    sqlQuery += " AND V.EMP_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["EMP_ID"].ToString() + "'";
                }
                //if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.EFFECTIVE_FROM_DT] != null)
                //{
                //    sqlQuery += " AND V.EMP_DOJ between to_date('" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.EFFECTIVE_FROM_DT].ToString() + "','dd/MM/yyyy') )";
                //}
                //if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.EFFECTIVE_TO_DT] != null)
                //{
                //    sqlQuery += " AND to_date('" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.EFFECTIVE_TO_DT].ToString() + "','dd/MM/yyyy') )";
                //}
            }
            return sqlQuery;
        }

        public static string get_TrainingRequestReport()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM vw_hr_training_enrollment V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["EMP_ID"] != null)
                {
                    sqlQuery += " AND V.EMP_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["EMP_ID"].ToString() + "'";
                }
                //if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.EFFECTIVE_FROM_DT] != null)
                //{
                //    sqlQuery += " AND V.EMP_DOJ between to_date('" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.EFFECTIVE_FROM_DT].ToString() + "','dd/MM/yyyy') )";
                //}
                //if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.EFFECTIVE_TO_DT] != null)
                //{
                //    sqlQuery += " AND to_date('" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.EFFECTIVE_TO_DT].ToString() + "','dd/MM/yyyy') )";
                //}
            }
            return sqlQuery;
        }

        public static string get_InductionCheckilistReport()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_Induction_List V WHERE ROWNUM > 0 ";
            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["DEPT_ID"] != null)
                {
                    sqlQuery += " AND V.dept_id='" + VMVServices.Web.Utils.ReportViewFilterParameter["DEPT_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["DESIGN_ID"] != null)
                {
                    sqlQuery += " AND V.DEPT_DESIG_ID='" + VMVServices.Web.Utils.ReportViewFilterParameter["DESIGN_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["EMP_ID"] != null)
                {
                    sqlQuery += " AND V.EMP_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["EMP_ID"].ToString() + "'";
                }
                //if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.EFFECTIVE_FROM_DT] != null)
                //{
                //    sqlQuery += " AND V.EMP_DOJ between to_date('" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.EFFECTIVE_FROM_DT].ToString() + "','dd/MM/yyyy') )";
                //}
                //if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.EFFECTIVE_TO_DT] != null)
                //{
                //    sqlQuery += " AND to_date('" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.EFFECTIVE_TO_DT].ToString() + "','dd/MM/yyyy') )";
                //}
            }
            return sqlQuery;
        }

        public static string get_EmpPermitDetailsReport()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_HR_EMP_PERMIT_DETAILS V WHERE ROWNUM > 0 ";
            sqlQuery += " AND V.org_id='" + VMVServices.Web.Utils.OrganizationID + "'";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["DEPT_ID"] != null)
                {
                    sqlQuery += " AND V.dept_id='" + VMVServices.Web.Utils.ReportViewFilterParameter["DEPT_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["DESIGN_ID"] != null)
                {
                    sqlQuery += " AND V.DEPT_DESIG_ID='" + VMVServices.Web.Utils.ReportViewFilterParameter["DESIGN_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["EMP_ID"] != null)
                {
                    sqlQuery += " AND V.EMP_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["EMP_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
                {
                    sqlQuery += " AND V.PERMIT_ISSUE_DATE >= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy') ";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
                {
                    sqlQuery += " AND V.PERMIT_ISSUE_DATE <= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["To_Date"].ToString() + "','dd/MM/yyyy') ";
                }
            }
            return sqlQuery;
        }

        public static string get_EmpProbationReport()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_HR_EMPLOYEE_PROBATION V WHERE ROWNUM > 0 ";
            sqlQuery += " AND V.org_id='" + VMVServices.Web.Utils.OrganizationID + "'";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {

                if (VMVServices.Web.Utils.ReportViewFilterParameter["DEPT_ID"] != null)
                {
                    sqlQuery += " AND V.dept_id='" + VMVServices.Web.Utils.ReportViewFilterParameter["DEPT_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["DESIGN_ID"] != null)
                {
                    sqlQuery += " AND V.DEPT_DESIG_ID='" + VMVServices.Web.Utils.ReportViewFilterParameter["DESIGN_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["EMP_ID"] != null)
                {
                    sqlQuery += " AND V.EMP_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["EMP_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
                {
                    sqlQuery += " AND V.PROB_FROM_DT between to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy') ";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
                {
                    sqlQuery += " AND to_date('" + VMVServices.Web.Utils.ReportFilterParameter["To_Date"].ToString() + "','dd/MM/yyyy') ";
                }
            }
            return sqlQuery;
        }

        public static string get_EmpBankDetailsReport()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_HR_EMPLOYEE_BANK_DETAILS V WHERE ROWNUM > 0 ";
            sqlQuery += " AND V.org_id='" + VMVServices.Web.Utils.OrganizationID + "'";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["DEPT_ID"] != null)
                {
                    sqlQuery += " AND V.dept_id='" + VMVServices.Web.Utils.ReportViewFilterParameter["DEPT_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["DESIGN_ID"] != null)
                {
                    sqlQuery += " AND V.DEPT_DESIG_ID='" + VMVServices.Web.Utils.ReportViewFilterParameter["DESIGN_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["EMP_ID"] != null)
                {
                    sqlQuery += " AND V.EMP_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["EMP_ID"].ToString() + "'";
                }
                //if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.EFFECTIVE_FROM_DT] != null)
                //{
                //    sqlQuery += " AND V.PERMIT_ISSUE_DATE between to_date('" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.EFFECTIVE_FROM_DT].ToString() + "','dd/MM/yyyy') )";
                //}
                //if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.EFFECTIVE_TO_DT] != null)
                //{
                //    sqlQuery += " AND to_date('" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.EFFECTIVE_TO_DT].ToString() + "','dd/MM/yyyy') )";
                //}
                if (VMVServices.Web.Utils.ReportViewFilterParameter["Active"].ToString() == "1")
                {
                    sqlQuery += " and v.enabled_flag='1'";
                }
                else if (VMVServices.Web.Utils.ReportViewFilterParameter["Active"].ToString() == "0")
                {
                    sqlQuery += " and v.enabled_flag='0'";
                }
            }
            return sqlQuery;
        }

        public static string get_EmpProbationEvalutionReport()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_HR_EMP_PROBATION_EVALUATION V WHERE ROWNUM > 0 ";
            sqlQuery += " AND V.org_id='" + VMVServices.Web.Utils.OrganizationID + "'";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["Dept_id"] != null)
                {
                    sqlQuery += " AND V.DEPT_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["Dept_id"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["EMP_ID"] != null)
                {
                    sqlQuery += " AND V.EMP_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["EMP_ID"].ToString() + "'";
                }
                //if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.EFFECTIVE_FROM_DT] != null)
                //{
                //    sqlQuery += " AND V.PERMIT_ISSUE_DATE between to_date('" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.EFFECTIVE_FROM_DT].ToString() + "','dd/MM/yyyy') )";
                //}
                //if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.EFFECTIVE_TO_DT] != null)
                //{
                //    sqlQuery += " AND to_date('" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.EFFECTIVE_TO_DT].ToString() + "','dd/MM/yyyy') )";
                //}
            }
            return sqlQuery;
        }
        public static string get_ApplicantDetailsReport()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_HR_APPLICANT_DETAILS V WHERE ROWNUM > 0 ";
            sqlQuery += " AND V.org_id='" + VMVServices.Web.Utils.OrganizationID + "'";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["APP_ID"] != null)
                {
                    sqlQuery += " AND V.APP_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["APP_ID"].ToString() + "'";
                }
                //if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.EFFECTIVE_FROM_DT] != null)
                //{
                //    sqlQuery += " AND V.PERMIT_ISSUE_DATE between to_date('" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.EFFECTIVE_FROM_DT].ToString() + "','dd/MM/yyyy') )";
                //}
                //if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.EFFECTIVE_TO_DT] != null)
                //{
                //    sqlQuery += " AND to_date('" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.EFFECTIVE_TO_DT].ToString() + "','dd/MM/yyyy') )";
                //}
            }
            return sqlQuery;
        }

        public static string get_InterviewAssesmentReport()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_INTERVIEW_ASSESSMENT V WHERE ROWNUM > 0 ";
            //sqlQuery += " AND V.org_id='" + VMVServices.Web.Utils.OrganizationID + "'";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["APP_ID"] != null)
                {
                    sqlQuery += " AND V.APP_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["APP_ID"].ToString() + "'";
                }
                //if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.EFFECTIVE_FROM_DT] != null)
                //{
                //    sqlQuery += " AND V.PERMIT_ISSUE_DATE between to_date('" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.EFFECTIVE_FROM_DT].ToString() + "','dd/MM/yyyy') )";
                //}
                //if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.EFFECTIVE_TO_DT] != null)
                //{
                //    sqlQuery += " AND to_date('" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.EFFECTIVE_TO_DT].ToString() + "','dd/MM/yyyy') )";
                //}
            }
            return sqlQuery;
        }

        public static string get_TerminationReport()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_EMP_TERMINATION V WHERE ROWNUM > 0 ";
            //if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            //{
            //    //if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.EFFECTIVE_FROM_DT] != null)
            //    //{
            //    //    sqlQuery += " AND V.ATTENDANCE_DATE = '" + VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.EFFECTIVE_FROM_DT].ToString() + "'";
            //    //}
            //    //if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.EFFECTIVE_TO_DT] != null)
            //    //{
            //    //    sqlQuery += " AND V.ATTENDANCE_DATE = '" + VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.EFFECTIVE_TO_DT].ToString() + "'";
            //    //}
            //}
            return sqlQuery;
        }

        public static string get_LeaveBalanceReport()
        {
            sqlQuery = string.Empty;

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["Fis_year"] != null && VMVServices.Web.Utils.ReportViewFilterParameter["Fis_year"] != string.Empty)
                {

                    sqlQuery = "  SELECT * FROM vw_leave_balance V WHERE ROWNUM > 0 ";
                    sqlQuery += "  and v.org_id = '" + VMVServices.Web.Utils.OrganizationID + "'";
                    sqlQuery += "  and v.fiscal_year = '" + VMVServices.Web.Utils.ReportViewFilterParameter["Fis_year"].ToString() + "'";
                    if (VMVServices.Web.Utils.ReportViewFilterParameter["EMP_ID"] != null)
                    {
                        sqlQuery += " AND V.EMP_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["EMP_ID"].ToString() + "'";
                    }
                    if (VMVServices.Web.Utils.ReportViewFilterParameter["Dept_Id"] != null)
                    {
                        sqlQuery += " AND V.EMP_DEPT_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["Dept_Id"].ToString() + "'";
                    }

                    //sqlQuery += "   and v.leave_txn_date >=  to_date('" + VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"].ToString() + "','dd/mm/yyyy')";

                    //sqlQuery = "    (select ee.emp_first_name || '' || ee.emp_middle_name || '' ||";
                    //sqlQuery += "   ee.emp_last_name as emp_name,";
                    //sqlQuery += "   ee.emp_first_name_OL || '' || ee.emp_middle_name_OL || '' ||";
                    //sqlQuery += "   ee.emp_last_name_OL as emp_name_OL,";
                    //sqlQuery += "   hd.LEAVE_DESC,";
                    //sqlQuery += "   ee.emp_no,      ";
                    //sqlQuery += "   round(SUM(sd.no_of_days), 3) as ldr_open_balance,";
                    //sqlQuery += "   ll.LEAVE_ID as leave_Id,";
                    //sqlQuery += "   round(SUM(sd.leave_balance), 3) as ldr_leave_bal,";
                    //sqlQuery += "   round(SUM(sd.leave_availed), 3) as ldr_leave_avail";
                    //sqlQuery += "   from hr_leave_applications ll, hr_employees ee, hr_leave_definitions hd,hr_leave_staff_defintions sd";
                    //sqlQuery += "   where ee.emp_id = ll.staff_id";
                    //sqlQuery += "   and hd.LEAVE_ID = ll.LEAVE_ID and sd.LEAVE_ID= ll.LEAVE_ID  and sd.staff_id=ee.emp_id";
                    //sqlQuery += "   and ll.leave_date_from >=  to_date('" + VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"].ToString() + "','dd/mm/yyyy')";
                    ////sqlQuery += "   between ll.leave_date_from and  ll.leave_date_to)";
                    //sqlQuery += "   AND LL.org_id='" + VMVServices.Web.Utils.OrganizationID + "'";
                    //sqlQuery += "   group by ll.leave_id,";
                    //sqlQuery += "   ee.emp_id,";
                    //sqlQuery += "   ee.emp_first_name,";
                    //sqlQuery += "   ee.emp_middle_name,";
                    //sqlQuery += "   ee.emp_last_name,";
                    //sqlQuery += "   ee.emp_first_name_OL,";
                    //sqlQuery += "   ee.emp_middle_name_OL,";
                    //sqlQuery += "   ee.emp_last_name_OL,";
                    //sqlQuery += "   hd.LEAVE_DESC,";
                    //sqlQuery += "   ee.emp_no";
                    //sqlQuery += "        )";


                    //                    select ee.emp_id,
                    //       ee.emp_first_name || '' || ee.emp_middle_name || '' ||
                    //       ee.emp_last_name as emp_name,
                    //       ee.emp_first_name_OL || '' || ee.emp_middle_name_OL || '' ||
                    //       ee.emp_last_name_OL as emp_name_OL,
                    //       hd.LEAVE_DESC,
                    //       ee.emp_no,
                    //       round(SUM(sd.no_of_days), 3) as ldr_open_balance,
                    //       sd.leave_id as leave_Id,
                    //       round(SUM(sd.leave_balance), 3) as ldr_leave_bal,
                    //       round(SUM(sd.leave_availed), 3) as ldr_leave_avail
                    //  from hr_employees              ee,
                    //       hr_leave_definitions      hd,
                    //       hr_leave_staff_defintions sd
                    // where sd.leave_id = hd.leave_id
                    //   and sd.staff_id = ee.emp_id
                    //   and sd.staff_id in
                    //       (select ll.staff_id
                    //          from hr_leave_applications ll
                    //         where upper(ll.app_status) = upper('approved'))
                    //--  and ( to_date('" + VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"].ToString() + "','dd/mm/yyyy')
                    //--between ll.leave_date_from and ll.leave_date_to)
                    //--  AND LL.org_id='" + VMVServices.Web.Utils.OrganizationID + "'
                    // group by sd.leave_id,
                    //          ee.emp_id,
                    //          ee.emp_first_name,
                    //          ee.emp_middle_name,
                    //          ee.emp_last_name,
                    //          ee.emp_first_name_OL,
                    //          ee.emp_middle_name_OL,
                    //          ee.emp_last_name_OL,
                    //          hd.LEAVE_DESC,
                    //          ee.emp_no
                }
            }

            return sqlQuery;
        }

        public static string get_EmpListDOA()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_EMP_LIST_DOA V WHERE ROWNUM > 0 ";
            sqlQuery += " and v.org_id = '" + VMVServices.Web.Utils.OrganizationID + "'";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
                {
                    sqlQuery += " AND V.emp_doj >=  to_date('" + VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"].ToString() + "','dd/mm/yyyy')";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
                {
                    sqlQuery += " AND V.emp_doj <=  to_date('" + VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"].ToString() + "','dd/mm/yyyy')";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["EMP_ID"] != null)
                {
                    sqlQuery += " AND V.EMp_id='" + VMVServices.Web.Utils.ReportViewFilterParameter["EMP_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["DEPT_ID"] != null)
                {
                    sqlQuery += " AND V.dept_id='" + VMVServices.Web.Utils.ReportViewFilterParameter["DEPT_ID"].ToString() + "'";
                }
            }
            return sqlQuery;
        }
        public static string get_EmployeeVacation()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_EMPLOYEE_VACATIONS V WHERE ROWNUM > 0 ";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {

                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
                {
                    sqlQuery += " AND V.LEAVE_DATE_FROM >= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy')";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
                {
                    sqlQuery += " AND V.LEAVE_DATE_FROM <= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["To_Date"].ToString() + "','dd/MM/yyyy')";
                }

                //if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.EFFECTIVE_FROM_DT] != null)
                //{
                //    sqlQuery += " AND V.EMP_DOJ between to_date('" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.EFFECTIVE_FROM_DT].ToString() + "','dd/MM/yyyy')";
                //}
                //if (VMVServices.Web.Utils.ReportViewFilterParameter[FINColumnConstants.EFFECTIVE_TO_DT] != null)
                //{
                //    sqlQuery += " AND to_date('" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.EFFECTIVE_TO_DT].ToString() + "','dd/MM/yyyy')";
                //}
            }

            //if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            //{

            //    //if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
            //    //{
            //    //    sqlQuery += " AND V.LEAVE_DATE_FROM >= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy')";
            //    //}
            //    //if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
            //    //{
            //    //    sqlQuery += " AND V.LEAVE_DATE_FROM <= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["To_Date"].ToString() + "','dd/MM/yyyy')";
            //    //}
            //    if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
            //    {
            //        sqlQuery += " AND to_date(V.LEAVE_DATE_FROM,'dd/MM/yyyy') between  to_date('" + VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"].ToString() + "','dd/mm/yyyy')";

            //        sqlQuery += " AND  to_date('" + VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"].ToString() + "','dd/mm/yyyy')";
            //    }
            //}
            return sqlQuery;
        }

        //public static string get_VacationSpecified()
        //{
        //    sqlQuery = string.Empty;
        //    sqlQuery = " SELECT * FROM VW_VACATIONS_SPECIFIED V WHERE ROWNUM > 0 ";
        //    if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
        //    {
        //        if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
        //        {
        //            if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
        //            {
        //                sqlQuery += " AND to_date(V.ATTENDANCE_DATE,'dd/MM/yyyy') between  to_date('" + VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"].ToString() + "','dd/mm/yyyy')";

        //                sqlQuery += " AND  to_date('" + VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"].ToString() + "','dd/mm/yyyy')";
        //            }
        //        }
        //    }
        //    return sqlQuery;
        //}

        public static string get_VacationSpecified()
        {
            sqlQuery = string.Empty;

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["EFFECTIVE_FROM_DT"] != null)
                {
                    sqlQuery += " select DISTINCT D.DEPT_ID,D.DEPT_NAME,to_number(e.emp_no) as emp_no,e.emp_first_name" + VMVServices.Web.Utils.LanguageCode + " || '' || e.emp_middle_name" + VMVServices.Web.Utils.LanguageCode + " || '' ||e.emp_last_name" + VMVServices.Web.Utils.LanguageCode + " as emp_first_name,e.emp_first_name" + VMVServices.Web.Utils.LanguageCode + " || '' || e.emp_middle_name" + VMVServices.Web.Utils.LanguageCode + " || '' ||e.emp_last_name" + VMVServices.Web.Utils.LanguageCode + " as emp_first_name_OL,eev.pay_amount AS SALARY,e.emp_doj AS START_WORKING_DT,";
                    sqlQuery += " round((SELECT HR_PKG.get_curr_year_lp(E.EMP_ORG_ID,D.DEPT_ID,E.EMP_ID,LL.LDR_LEAVE_ID,to_date('" + VMVServices.Web.Utils.ReportViewFilterParameter["EFFECTIVE_FROM_DT"].ToString() + "','dd/mm/yyyy')) FROM DUAL),3) as relay,";
                    sqlQuery += " round((SELECT HR_PKG.get_prev_year_lp(E.EMP_ORG_ID,D.DEPT_ID,E.EMP_ID,LL.LDR_LEAVE_ID,to_date('" + VMVServices.Web.Utils.ReportViewFilterParameter["EFFECTIVE_FROM_DT"].ToString() + "','dd/mm/yyyy')) FROM DUAL),3) as balance,";
                    sqlQuery += "  round((SELECT HR_PKG.get_leave_consumed_lp(E.EMP_ORG_ID,D.DEPT_ID,E.EMP_ID,LL.LDR_LEAVE_ID,to_date('" + VMVServices.Web.Utils.ReportViewFilterParameter["EFFECTIVE_FROM_DT"].ToString() + "','dd/mm/yyyy')) FROM DUAL),3) as consumed,";
                    sqlQuery += " round((((SELECT HR_PKG.get_curr_year_lp(E.EMP_ORG_ID,D.DEPT_ID,E.EMP_ID,LL.LDR_LEAVE_ID,to_date('" + VMVServices.Web.Utils.ReportViewFilterParameter["EFFECTIVE_FROM_DT"].ToString() + "','dd/mm/yyyy'))";
                    sqlQuery += " FROM DUAL) + (SELECT HR_PKG.get_prev_year_lp(E.EMP_ORG_ID,D.DEPT_ID,E.EMP_ID,LL.LDR_LEAVE_ID,to_date('" + VMVServices.Web.Utils.ReportViewFilterParameter["EFFECTIVE_FROM_DT"].ToString() + "','dd/mm/yyyy')) FROM DUAL)) -";
                    sqlQuery += " NVL((SELECT HR_PKG.get_leave_consumed_lp(E.EMP_ORG_ID,D.DEPT_ID,E.EMP_ID,LL.LDR_LEAVE_ID,to_date('" + VMVServices.Web.Utils.ReportViewFilterParameter["EFFECTIVE_FROM_DT"].ToString() + "','dd/mm/yyyy')) FROM DUAL), 0)),3) as bal_end_of_year,";
                    sqlQuery += " round((SELECT HR_PKG.get_leave_provision(E.EMP_ORG_ID,D.DEPT_ID, E.EMP_ID, LL.LDR_LEAVE_ID, to_date('" + VMVServices.Web.Utils.ReportViewFilterParameter["EFFECTIVE_FROM_DT"].ToString() + "','dd/mm/yyyy')) FROM DUAL),3) as bal_to_dat,";
                    sqlQuery += " round(((eev.pay_amount / 26) * NVL((SELECT HR_PKG.get_leave_provision(E.EMP_ORG_ID, D.DEPT_ID, E.EMP_ID, LL.LDR_LEAVE_ID, to_date('" + VMVServices.Web.Utils.ReportViewFilterParameter["EFFECTIVE_FROM_DT"].ToString() + "','dd/mm/yyyy')) FROM DUAL),  0)),3) as EARNING_AMT";
                    sqlQuery += " from pay_emp_element_value eev, ssm_system_options so,hr_leave_ledger ll,hr_employees e,PAY_ELEMENTS pe,HR_DEPARTMENTS D";
                    sqlQuery += " where eev.pay_element_id = so.hr_basic_element_code";
                    sqlQuery += " AND D.DEPT_ID = EEV.PAY_EMP_DEPT_ID";
                    sqlQuery += " and pe.pay_element_id = eev.pay_element_id";
                    sqlQuery += " and e.emp_id = ll.ldr_emp_id";
                    sqlQuery += " and e.emp_id = eev.pay_emp_id";


                    sqlQuery += " AND to_date(ll.ldr_date,'dd/MM/yyyy') <=  to_date('" + VMVServices.Web.Utils.ReportViewFilterParameter["EFFECTIVE_FROM_DT"].ToString() + "','dd/mm/yyyy')";
                    sqlQuery += " order by to_number(e.emp_no) asc ";
                }
            }


            return sqlQuery;
        }



        public static string get_Custom_Vac_Des_Bal_Demo()
        {
            sqlQuery = string.Empty;

            sqlQuery += " SELECT DEPT_NAME,DEPT_NAME_OL,SUM(NO_OF_DAYS* BASIC_AMT) AS ABCM, SUM(NO_OF_DAYS_NEXTMONTH* BASIC_AMT) AS ABNM,";
            sqlQuery += " (SUM(NO_OF_DAYS* BASIC_AMT) -SUM(NO_OF_DAYS_NEXTMONTH* BASIC_AMT)) AS ARD";
            sqlQuery += " FROM (SELECT DEP.DEPT_ID,DEP.DEPT_NAME,DEP.DEPT_NAME_OL, LSD.STAFF_ID,LSD.DEPT_ID,LSD.ORG_ID ,LSD.LEAVE_ID";

            sqlQuery += " ,(select hr_pkg.get_leave_provision(LSD.ORG_ID,LSD.DEPT_ID,LSD.STAFF_ID,LSD.LEAVE_ID, TO_DATE('" + VMVServices.Web.Utils.ReportViewFilterParameter["EFFECTIVE_FROM_DT"].ToString() + "','dd/mm/yyyy')) from dual) AS NO_OF_DAYS";
            sqlQuery += " ,(select hr_pkg.get_emp_basic(LSD.STAFF_ID) from dual) AS BASIC_AMT";
            sqlQuery += " ,(select hr_pkg.get_leave_provision(LSD.ORG_ID,LSD.DEPT_ID,LSD.STAFF_ID,LSD.LEAVE_ID,ADD_MONTHS( TO_DATE('" + VMVServices.Web.Utils.ReportViewFilterParameter["EFFECTIVE_FROM_DT"].ToString() + "','dd/mm/yyyy'),1)  ) from dual) AS NO_OF_DAYS_NEXTMONTH";
            sqlQuery += " FROM HR_LEAVE_STAFF_DEFINTIONS LSD";
            sqlQuery += " INNER JOIN SSM_SYSTEM_OPTIONS SSO ON SSO.HR_EARN_LEAVE = LSD.LEAVE_ID AND SSO.HR_EFFECTIVE_TO_DATE IS NULL ";
            sqlQuery += " INNER JOIN HR_DEPARTMENTS DEP ON DEP.DEPT_ID = LSD.DEPT_ID";
            sqlQuery += " WHERE LSD.FISCAL_YEAR=  PKG_PAYROLL.get_fiscal_year('" + VMVServices.Web.Utils.OrganizationID + "',TO_DATE('" + VMVServices.Web.Utils.ReportViewFilterParameter["EFFECTIVE_FROM_DT"].ToString() + "','dd/mm/yyyy'))";
            sqlQuery += " )Z GROUP BY DEPT_NAME,DEPT_NAME_OL";

            return sqlQuery;
        }



        public static string get_EmpListEOS()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_EOS_SEPCIFIED V WHERE ROWNUM > 0 ";
            sqlQuery += " AND V.org_id='" + VMVServices.Web.Utils.OrganizationID + "'";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {

                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
                {
                    sqlQuery += " AND V.EMP_EOS >= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy')";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
                {
                    sqlQuery += " AND V.EMP_EOS <= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["To_Date"].ToString() + "','dd/MM/yyyy')";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["EMP_ID"] != null)
                {
                    sqlQuery += " AND V.EMp_id='" + VMVServices.Web.Utils.ReportViewFilterParameter["EMP_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["DEPT_ID"] != null)
                {
                    sqlQuery += " AND V.dept_id='" + VMVServices.Web.Utils.ReportViewFilterParameter["DEPT_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["DESIGN_ID"] != null)
                {
                    sqlQuery += " AND V.DEPT_DESIG_ID='" + VMVServices.Web.Utils.ReportViewFilterParameter["DESIGN_ID"].ToString() + "'";
                }

                //if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
                //{
                //    sqlQuery += " AND to_date(V.EMP_EOS,'dd/MM/yyyy') between  to_date('" + VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"].ToString() + "','dd/mm/yyyy')";

                //    sqlQuery += " AND  to_date('" + VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"].ToString() + "','dd/mm/yyyy')";
                //}
            }
            return sqlQuery;
        }

        public static string GetLeave()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT LD.LEAVE_ID,LD.LEAVE_DESC";
            sqlQuery += " FROM  HR_LEAVE_DEFINITIONS LD";
            sqlQuery += " WHERE LD.ENABLED_FLAG = 1";
            sqlQuery += " AND LD.ORG_ID = '" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " AND LD.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " ORDER BY LD.LEAVE_DESC";

            return sqlQuery;

        }
        public static string GetLeaveBasedFiscalYear(string fisYear)
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT LD.LEAVE_ID,LD.LEAVE_DESC";
            sqlQuery += " FROM  HR_LEAVE_DEFINITIONS LD";
            sqlQuery += " WHERE LD.ENABLED_FLAG = 1";
            sqlQuery += " AND LD.WORKFLOW_COMPLETION_STATUS = 1";
            //sqlQuery += " and ld.FISCAL_YEAR='" + fisYear + "'";
            sqlQuery += " and ld.org_id='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " ORDER BY LD.LEAVE_DESC";

            return sqlQuery;

        }

        public static string GetLeaveBasedFiscalYear_frLD(string fisYear)
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT LD.PK_ID AS LEAVE_ID,LD.LEAVE_DESC";
            sqlQuery += " FROM  HR_LEAVE_DEFINITIONS LD";
            sqlQuery += " WHERE LD.ENABLED_FLAG = 1";
            sqlQuery += " AND LD.WORKFLOW_COMPLETION_STATUS = 1";
            //sqlQuery += " and ld.FISCAL_YEAR='" + fisYear + "'";
            sqlQuery += " ORDER BY LD.LEAVE_DESC";

            return sqlQuery;

        }
        public static string GetLeaveFrom_To()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT LD.EFFECTIVE_FROM_DT, LD.EFFECTIVE_TO_DT";
            sqlQuery += " FROM HR_LEAVE_DEFINITIONS LD";
            sqlQuery += " WHERE LD.ENABLED_FLAG = 1";
            sqlQuery += "  AND LD.WORKFLOW_COMPLETION_STATUS = 1";
            //sqlQuery += " and ld.FISCAL_YEAR='" + fisYear + "'";


            return sqlQuery;

        }


        public static string GetLeaveType(string dept_id, string staff_id)
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT LD.LEAVE_TYPE, LA.DEPT_ID, LA.STAFF_ID";
            sqlQuery += " FROM HR_LEAVE_DEFINITIONS LD, HR_LEAVE_APPLICATIONS LA";
            sqlQuery += " WHERE LD.LEAVE_ID = LA.LEAVE_ID";
            sqlQuery += " AND LD.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND LD.ENABLED_FLAG = 1";
            sqlQuery += " AND LA.DEPT_ID = '" + dept_id + "'";
            sqlQuery += " AND LA.STAFF_ID = '" + staff_id + "'";
            //sqlQuery += " and ld.FISCAL_YEAR='" + fisYear + "'";
            sqlQuery += " ORDER BY LD.LEAVE_DESC";

            return sqlQuery;

        }



        static string spOutPut = string.Empty;
        public static System.Collections.SortedList ErrorCollection = new System.Collections.SortedList();


        public static string GetSPFOR_DUPLICATE_CHECK(string P_LEAVE_DESC, string P_RECORD_ID)
        {
            try
            {
                string retcode = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "PKG_VALIDATIONS.PROC_ERR_MGR_LEAVE_DEF";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add("@P_MODULE_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_SCREEN_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_LEAVE_DESC", OracleDbType.Char).Value = P_LEAVE_DESC;
                oraCmd.Parameters.Add("@P_RECORD_ID", OracleDbType.Char).Value = P_RECORD_ID;
                oraCmd.Parameters.Add("@P_RET", OracleDbType.Varchar2, 150);
                oraCmd.Parameters["@P_RET"].Direction = ParameterDirection.Output;
                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

                retcode = oraCmd.Parameters["@P_RET"].Value.ToString();

                return retcode;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static void InserSP_EMP_LEAVE_PRORATE(string P_FISCAL_YEAR, string P_DEPT_ID, string P_EMP_ID, DateTime p_DOJ)
        {
            try
            {
                string strValue = string.Empty;
                string strNextCode = string.Empty;
                string strErrMsg = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "HR_PKG.hr_prorate_leave";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add(new OracleParameter("@p_org_id", OracleDbType.Varchar2, 250)).Value = VMVServices.Web.Utils.OrganizationID;
                oraCmd.Parameters.Add(new OracleParameter("@p_fiscal_year", OracleDbType.Varchar2, 250)).Value = P_FISCAL_YEAR;
                oraCmd.Parameters.Add(new OracleParameter("@p_dept_id", OracleDbType.Varchar2, 250)).Value = P_DEPT_ID;
                oraCmd.Parameters.Add(new OracleParameter("@p_emp_id", OracleDbType.Varchar2, 250)).Value = P_EMP_ID;
                if (p_DOJ != null)
                {
                    oraCmd.Parameters.Add(new OracleParameter("@p_doj", OracleDbType.Date, 250)).Value = p_DOJ.ToString("dd/MMM/yyyy");
                }
                else
                {
                    oraCmd.Parameters.Add(new OracleParameter("@p_doj", OracleDbType.Date, 250)).Value = null;
                }

                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GetRPTCashVacations()
        {
            sqlQuery = string.Empty;

            sqlQuery = "   SELECT EMP.EMP_ID,EMP.EMP_NO as EMP_NO ";
            sqlQuery += "   ,(EMP.EMP_FIRST_NAME || ' ' || EMP.EMP_MIDDLE_NAME || ' ' ||EMP.EMP_LAST_NAME) EMP_NAME";
            sqlQuery += "   ,(EMP.EMP_FIRST_NAME_OL || ' ' || EMP.EMP_MIDDLE_NAME_OL || ' ' ||EMP.EMP_LAST_NAME_OL) EMP_NAME_OL";
            sqlQuery += "   ,TO_CHAR(LE.LC_DATE, 'DD/MM/YYYY') AS LC_DATE,LE.LC_NO_OF_DAYS,LE.LC_LEAVE_SALARY AS AMOUNT";
            sqlQuery += "    FROM HR_EMPLOYEES EMP";
            sqlQuery += "    INNER JOIN HR_LEAVE_ENCASHMENT LE ON LE.LC_EMP_ID = EMP.EMP_ID";
            sqlQuery += " where EMP.emp_org_id ='" + VMVServices.Web.Utils.OrganizationID + "'";
            if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
            {
                sqlQuery += " and LE.LC_DATE >= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy')";
            }
            if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
            {
                sqlQuery += " AND LE.LC_DATE <= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["To_Date"].ToString() + "','dd/MM/yyyy')";
            }
            sqlQuery += " order by to_number(EMP.EMP_NO) asc ";
            return sqlQuery;

        }
        public static string GetLeaveProvision(string To_Date)
        {
            sqlQuery = string.Empty;

            //sqlQuery += "  SELECT HL.LEAVE_EMP_ID as EMP_ID,";
            //sqlQuery += " E.EMP_NO ";
            //sqlQuery += "   E.EMP_FIRST_NAME" + VMVServices.Web.Utils.LanguageCode + " ||' '||e.emp_middle_name" + VMVServices.Web.Utils.LanguageCode + " ||' '||e.emp_last_name" + VMVServices.Web.Utils.LanguageCode + " as EMPLOYEE_NAME,";
            //sqlQuery += "   ROUND(HL.Leave_Total_Provision_Amt," + VMVServices.Web.Utils.DecimalPrecision + ") AS CURRENT_BALANCE,       ";
            //sqlQuery += "   ROUND(HL.LEAVE_PROVISION_AMT," + VMVServices.Web.Utils.DecimalPrecision + ") AS PERVIOUS_BALANCE,";
            //sqlQuery += "  ROUND(NVL(Leave_Total_Provision_Amt, 0) - NVL(LEAVE_OPEN_BAL, 0), " + VMVServices.Web.Utils.DecimalPrecision + ") AS CURRENT_ACCUMULATION";
            //sqlQuery += "  FROM HR_LEAVE_TXN_LEDGER HL, HR_EMPLOYEES E";
            //sqlQuery += "  where HL.LEAVE_TXN_DATE =to_date('" + To_Date + "','dd/MM/yyyy')";
            //sqlQuery += "  and HL.LEAVE_EMP_ID = E.EMP_ID";
            //sqlQuery += "  and HL.LEAVE_ORG_ID = '" + VMVServices.Web.Utils.OrganizationID + "'";
            //sqlQuery += "   AND HL.ENABLED_FLAG = 1";


            sqlQuery += "  SELECT HL.LEAVE_EMP_ID as EMP_ID,";
            sqlQuery += " E.EMP_NO, ";
            sqlQuery += "  E.EMP_FIRST_NAME" + VMVServices.Web.Utils.LanguageCode + " ||' '||e.emp_middle_name" + VMVServices.Web.Utils.LanguageCode + " ||' '||e.emp_last_name" + VMVServices.Web.Utils.LanguageCode + " as EMPLOYEE_NAME,";
            sqlQuery += " to_char(ROUND(nvl(HL.leave_eligible_days,0)," + VMVServices.Web.Utils.DecimalPrecision + ")) AS leave_eligible_days,       ";
            sqlQuery += " to_char(ROUND(nvl(HL.leave_provision_amt,0)," + VMVServices.Web.Utils.DecimalPrecision + ")) AS leave_provision_amt,       ";
            sqlQuery += " to_char(ROUND(nvl(HL.leave_bal,0)," + VMVServices.Web.Utils.DecimalPrecision + ")) AS leave_bal       ";
            sqlQuery += "  FROM HR_LEAVE_TXN_LEDGER HL, HR_EMPLOYEES E";
            sqlQuery += "  where HL.LEAVE_TXN_DATE =to_date('" + To_Date + "','dd/MM/yyyy')";
            sqlQuery += "  and HL.LEAVE_EMP_ID = E.EMP_ID";
            sqlQuery += "  and HL.LEAVE_ORG_ID = '" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += "   AND HL.ENABLED_FLAG = 1";
            sqlQuery += " ORDER BY to_number(E.EMP_NO) ";
            return sqlQuery;

        }
        public static void UpdateAccuredLeave(string empId, string leaveId, string leaveBal)
        {
            sqlQuery = string.Empty;

            sqlQuery = "  update hr_leave_staff_defintions set Accured_leave_Balance='" + leaveBal + "' where STAFF_ID='" + empId + "',LEAVE_ID='" + leaveBal + "'";

            DBMethod.ExecuteNonQuery(sqlQuery);

        }
        public static string GetLeaveEligibleDays(string empId, string leaveId)
        {
            sqlQuery = string.Empty;

            sqlQuery += " select  ll.leave_eligible_days ";
            sqlQuery += " from hr_leave_txn_ledger ll ";
            sqlQuery += "  where  ll.leave_org_id='" + VMVServices.Web.Utils.OrganizationID + "' and ll.leave_emp_id='" + empId + "'";
            //sqlQuery += "    and ll.leave_id='" + leaveId + "'";
            sqlQuery += " having ll.leave_txn_date=";
            sqlQuery += " (select max(leave_txn_date) from hr_leave_txn_ledger )";
            sqlQuery += " group by ll.leave_txn_date,ll.leave_eligible_days ";


            return sqlQuery;

        }


        public static void GetSP_LeaveProvision(string to_date)
        {
            try
            {
                string strValue = string.Empty;
                string strNextCode = string.Empty;
                string strErrMsg = string.Empty;

                DateTime dt_tmp = Convert.ToDateTime(to_date);

                OracleCommand oraCmd = new OracleCommand();

                oraCmd.CommandText = "HR_PKG.proc_leave_provision";
                oraCmd.CommandType = CommandType.StoredProcedure;

                oraCmd.Parameters.Add(new OracleParameter("@p_org_id", OracleDbType.Varchar2, 250)).Value = VMVServices.Web.Utils.OrganizationID;
                oraCmd.Parameters.Add(new OracleParameter("@p_date", OracleDbType.Date, 250)).Value = dt_tmp.ToString("dd/MMM/yyyy");
                oraCmd.Parameters.Add(new OracleParameter("@p_type", OracleDbType.Varchar2, 250)).Value = "R";



                DBMethod.ExecuteStoredProcedure(oraCmd);



            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static string getLeaveProvisionReportData()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_LEAVE_PROVISION V WHERE ROWNUM > 0 ";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["LeaveDate"] != null)
                {
                    sqlQuery += " AND V.LEAVE_TXN_DATE = to_date('" + VMVServices.Web.Utils.ReportFilterParameter["LeaveDate"].ToString() + "','dd/MM/yyyy') ";
                }

                sqlQuery += " AND V.LEAVE_ORG_ID ='" + VMVServices.Web.Utils.OrganizationID + "'";

            }
            return sqlQuery;
        }

        public static string getAnnualLeaveLedger()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_ANNUALLEAVE_LEDGER V WHERE ROWNUM > 0 ";
            sqlQuery += " AND V.org_id='" + VMVServices.Web.Utils.OrganizationID + "'";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {

                if (VMVServices.Web.Utils.ReportViewFilterParameter["CALDTLID"] != null)
                {
                    sqlQuery += " AND V.CAL_DTL_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["CALDTLID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["EMP_ID"] != null)
                {
                    sqlQuery += " AND V.EMP_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["EMP_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
                {
                    sqlQuery += " AND V.LDR_DATE >= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy') ";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
                {
                    sqlQuery += " AND LDR_DATE<= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["To_Date"].ToString() + "','dd/MM/yyyy') ";
                }
            }
            return sqlQuery;
        }
    }
}
