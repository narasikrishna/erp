﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;

namespace FIN.DAL.HR
{
    public static class Permission_DAL
    {

        static string sqlQuery = "";
        static string spOutPut = string.Empty;
        public static System.Collections.SortedList ErrorCollection = new System.Collections.SortedList();


        public static string GetSPFOR_DUPLICATE_CHECK(string P_EMP_ID, string P_FROM_DATE, string P_TO_DATE, string P_RECORD_ID)
        {
            try
            {
                string retcode = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "PKG_VALIDATIONS.PROC_ERR_MGR_PERMISSION";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add("@P_MODULE_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_SCREEN_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_EMP_ID", OracleDbType.Char).Value = P_EMP_ID;
                oraCmd.Parameters.Add("@P_FROM_DATE", OracleDbType.Char).Value = P_FROM_DATE;
                oraCmd.Parameters.Add("@P_TO_DATE", OracleDbType.Char).Value = P_TO_DATE;
                oraCmd.Parameters.Add("@P_RECORD_ID", OracleDbType.Char).Value = P_RECORD_ID;
                oraCmd.Parameters.Add("@P_RET", OracleDbType.Varchar2, 150);
                oraCmd.Parameters["@P_RET"].Direction = ParameterDirection.Output;
                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

                retcode = oraCmd.Parameters["@P_RET"].Value.ToString();

                return retcode;

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public static string GetEmployeeDetails(string dept_id)
        {
            sqlQuery = string.Empty;
            string strLngCode = VMVServices.Web.Utils.LanguageCode;
            sqlQuery = " select wd.emp_id,(e.emp_no|| ' - ' ||e.emp_first_name" + strLngCode + "||' '||e.emp_middle_name" + strLngCode + "||' '||e.emp_last_name" + strLngCode + ") as Employee_Name ";
            sqlQuery += " from hr_emp_work_dtls wd, hr_employees e";
            sqlQuery += "  where wd.emp_id = e.emp_id";
            sqlQuery += "  and wd.enabled_flag = 1";
            sqlQuery += " and wd.workflow_completion_status = 1";
            sqlQuery += " and wd.effective_to_dt is null";
            sqlQuery += " and e.emp_org_id='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += "and wd.emp_dept_id = '" + dept_id + "' ";
            //  sqlQuery += " and e.emp_internal_external = 'I'";

            return sqlQuery;
        }

        public static string GetReasonBasedonCategory(string category_id)
        {
            sqlQuery = string.Empty;
            string strLngCode = VMVServices.Web.Utils.LanguageCode;
            //sqlQuery = " select a.Reason_ID ,a.Reason_desc as Reason_Name";
            //sqlQuery += " from Tm_reasons a, HR_Categories b" ;
            //sqlQuery += " where a.Attribute2 = b.Category_id";
            //sqlQuery += " and a.Org_id ='" + VMVServices.Web.Utils.OrganizationID + "'";
            //sqlQuery += " and b.Category_id ='" + category_id + "'";

            sqlQuery += " select a.Reason_ID, a.Reason_desc as Reason_Name";
            sqlQuery += " from Tm_reasons a, ssm_code_masters b";
            sqlQuery += " where a.Attribute2 = b.code";
            sqlQuery += " and b.parent_code = 'PER_CAT'";
            sqlQuery += " and a.Org_id ='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += "and b.code ='" + category_id + "'";
            return sqlQuery;
        }

        public static string GetTimeScheduleofOrg()
        {
            sqlQuery = string.Empty;
            sqlQuery += " Select TM_IN_TIME,TM_OUT_TIME From TM_COMP_SCH";
            sqlQuery += " where TM_ORG_ID ='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " and Enabled_Flag = 1 and TM_END_DATE is NULL";
            return sqlQuery;
        }

        public static string getEmpPermissionDtls()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM vw_employee_permission V WHERE ROWNUM > 0 ";
            sqlQuery += " AND V.org_id='" + VMVServices.Web.Utils.OrganizationID + "'";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["EMP_ID"] != null)
                {
                    sqlQuery += "  and V.emp_id ='" + VMVServices.Web.Utils.ReportFilterParameter["EMP_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["STATUS_VALUE"] != null)
                {
                    sqlQuery += "  and V.STATUS_CODE ='" + VMVServices.Web.Utils.ReportFilterParameter["STATUS_VALUE"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
                {
                    sqlQuery += " AND V.permission_date >= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy')";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
                {
                    sqlQuery += " AND V.permission_date <= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["To_Date"].ToString() + "','dd/MM/yyyy')";
                }
            }
            return sqlQuery;
        }

        public static string getRPTPermissionReport(string str_type)
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_HR_PERMISSIONS V WHERE ROWNUM > 0 ";
           

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
                {
                    sqlQuery += " AND V.TM_DATE >= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy')";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
                {
                    sqlQuery += " AND V.TM_DATE <= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["To_Date"].ToString() + "','dd/MM/yyyy')";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Late"] != null)
                {
                    sqlQuery += "  and V.LATE_IN >='" + VMVServices.Web.Utils.ReportFilterParameter["From_Late"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Late"] != null)
                {
                    sqlQuery += "  and V.LATE_IN <='" + VMVServices.Web.Utils.ReportFilterParameter["To_Late"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Early"] != null)
                {
                    sqlQuery += "  and V.EARLY_OUT >='" + VMVServices.Web.Utils.ReportFilterParameter["From_Early"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Early"] != null)
                {
                    sqlQuery += "  and V.EARLY_OUT <='" + VMVServices.Web.Utils.ReportFilterParameter["To_Early"].ToString() + "'";
                }

                if (VMVServices.Web.Utils.ReportViewFilterParameter["Emp_id"] != null)
                {
                    sqlQuery += "  and V.EMP_ID ='" + VMVServices.Web.Utils.ReportFilterParameter["Emp_id"].ToString() + "'";
                }

                if (str_type == "EARLY")
                {
                    sqlQuery += "  and V.EARLY_OUT > 0";
                }
                else if (str_type == "LATE")
                {
                    sqlQuery += "  and V.LATE_IN > 0";
                }
               
            }
            return sqlQuery;
        }

    }
}
