﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;

namespace FIN.DAL.HR
{
    public class ResignationInterview_DAL
    {

        static string sqlQuery = "";

        public static string GetResigRequestdtls()
        {
            sqlQuery = string.Empty;

            sqlQuery += " select RR.RESIG_HDR_ID,RR.RESIG_HDR_ID||' - '||RR.RESIG_TYPE as RESIG_TYPE";
            sqlQuery += " from HR_RESIG_REQUEST_HDR RR";
            sqlQuery += " WHERE RR.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND RR.ENABLED_FLAG = 1";
            sqlQuery += " AND RR.RESIG_ORG_ID = '" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " AND RR.RESIG_HDR_ID NOT IN (select RESIG_HDR_ID from HR_RESIG_CANCELLATION where workflow_completion_status=1) ";
            sqlQuery += " ORDER BY RR.RESIG_HDR_ID DESC";

            return sqlQuery;
        }

        public static string getResigIntervDtls(string ex_int_id)
        {
            sqlQuery = string.Empty;
            sqlQuery += " SELECT id.ex_int_dtl_id,id.ex_int_line_num,id.ex_int_question,ID.ATTRIBUTE1 AS LOOKUP_ID,ID.ATTRIBUTE2 AS QUESTION_GROUP,";
            sqlQuery += " id.ex_int_response,'N' as DELETED";
            sqlQuery += " FROM HR_EXIT_INTERVIEW_DTL id";
            sqlQuery += " where id.workflow_completion_status = 1";
            sqlQuery += " and id.enabled_flag = 1";
            sqlQuery += " and id.ex_int_id = '" + ex_int_id + "'";
            sqlQuery += " order by ex_int_dtl_id ";

            return sqlQuery;
        }

        public static string getIntervQusdtls()
        {
            sqlQuery = string.Empty;

            sqlQuery += " SELECT 0 AS ex_int_dtl_id,ROWNUM AS ex_int_line_num,IQ.QUESTION_GROUP,IQ.QUESTION_NAME AS ex_int_question,'' AS LOOKUP_ID,";
            sqlQuery += " '' AS ex_int_response,'N' as DELETED";
            sqlQuery += " FROM HR_EXIT_INTERVIEW_QUESTIONS IQ";
            sqlQuery += " WHERE IQ.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND IQ.ORG_ID = '" + VMVServices.Web.Utils.OrganizationID + "'";
            return sqlQuery;
        }

        public static string updateLastwrkingDate(string req_id, string last_date)
        {
            sqlQuery = string.Empty;
            sqlQuery += " update HR_RESIG_REQUEST_HDR RH set LAST_WORKING_DATE   = to_date('" + last_date.ToString() + "','dd/MM/yyyy')";
            sqlQuery += " WHERE RH.RESIG_HDR_ID = '" + req_id + "'";

            return sqlQuery;
        }


        public static string getResigReasonsDtl(string RESIG_HDR_ID)
        {
            sqlQuery = string.Empty;

            sqlQuery += " select rr.reasons";
            sqlQuery += " from HR_REASONS_FOR_RESIGNATION rr";
            sqlQuery += " where rr.workflow_completion_status = 1";
            sqlQuery += " and rr.ex_int_id ='" + RESIG_HDR_ID + "'";
            return sqlQuery;
        }

        public static string getResigReasons2(string EX_INT_ID, string REASONS)
        {
            sqlQuery = string.Empty;


            sqlQuery += " SELECT RR.EX_INT_ID,RR.REASONS,RR.ENABLED_FLAG,RR.RFR_ID";
            sqlQuery += " FROM HR_REASONS_FOR_RESIGNATION RR";
            sqlQuery += " WHERE RR.EX_INT_ID = '" + EX_INT_ID + "'";
            sqlQuery += " AND RR.REASONS = '" + REASONS + "'";
            return sqlQuery;
        }

        public static void deleteResigres(string ex_int_id)
        {
            try
            {

                string strQuery = string.Empty;

                strQuery += " delete from HR_REASONS_FOR_RESIGNATION rr";
                strQuery += " where rr.ex_int_id = '" + ex_int_id + "'";
                DBMethod.ExecuteNonQuery(strQuery);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static string getResigReqDtl(string RESIG_HDR_ID)
        {
            sqlQuery = string.Empty;
            sqlQuery += " select RR.RESIG_REASON,RR.RESIG_REMARKS,RR.RESIG_REQ_DT";
            sqlQuery += " from HR_RESIG_REQUEST_HDR RR";
            sqlQuery += " WHERE RR.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND RR.ENABLED_FLAG = 1";
            sqlQuery += " AND RR.RESIG_ORG_ID = '" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " AND RR.RESIG_HDR_ID = '" + RESIG_HDR_ID + "'";
            sqlQuery += " ORDER BY RR.RESIG_HDR_ID";

            return sqlQuery;
        }

        static string spOutPut = string.Empty;
        public static System.Collections.SortedList ErrorCollection = new System.Collections.SortedList();


        public static string GetSPFOR_DUPLICATE_CHECK(string P_RESIG_HDR_ID, string P_RECORD_ID)
        {
            try
            {
                string retcode = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "PKG_VALIDATIONS.PROC_ERR_MGR_RESIGN_INTR";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add("@P_MODULE_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_SCREEN_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_RESIG_HDR_ID", OracleDbType.Char).Value = P_RESIG_HDR_ID;
                oraCmd.Parameters.Add("@P_RECORD_ID", OracleDbType.Char).Value = P_RECORD_ID;
                oraCmd.Parameters.Add("@P_RET", OracleDbType.Varchar2, 150);
                oraCmd.Parameters["@P_RET"].Direction = ParameterDirection.Output;
                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

                retcode = oraCmd.Parameters["@P_RET"].Value.ToString();

                return retcode;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
