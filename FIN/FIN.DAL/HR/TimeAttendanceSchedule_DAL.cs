﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;

namespace FIN.DAL.HR
{
    public class TimeAttendanceSchedule_DAL
    {
        static string sqlQuery = "";

        public static string GetSchedulecode()
        {
            sqlQuery = string.Empty;

            sqlQuery = " select s.tm_sch_code,s.tm_sch_desc";
            sqlQuery += " from TM_SCHEDULES s";
            sqlQuery += " where s.enabled_flag =1";
            sqlQuery += " and s.workflow_completion_status = 1";
            sqlQuery += " and s.tm_org_id = '" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " order by tm_sch_code";


            return sqlQuery;

        }

        static string spOutPut = string.Empty;
        public static System.Collections.SortedList ErrorCollection = new System.Collections.SortedList();


        public static string GetSPFOR_DUPLICATE_CHECK(string P_TIME_SCHDULE_ID, string P_RECORD_ID)
        {
            try
            {
                string retcode = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "PKG_VALIDATIONS.PROC_ERR_MGR_TM_ATTN_SCH";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add("@P_MODULE_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_SCREEN_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_TIME_SCHDULE_ID", OracleDbType.Char).Value = P_TIME_SCHDULE_ID;
                oraCmd.Parameters.Add("@P_RECORD_ID", OracleDbType.Char).Value = P_RECORD_ID;
                oraCmd.Parameters.Add("@P_RET", OracleDbType.Varchar2, 150);
                oraCmd.Parameters["@P_RET"].Direction = ParameterDirection.Output;
                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

                retcode = oraCmd.Parameters["@P_RET"].Value.ToString();

                return retcode;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
