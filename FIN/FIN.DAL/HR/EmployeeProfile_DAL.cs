﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;

namespace FIN.DAL.HR
{
    public class EmployeeProfile_DAL
    {
        static string sqlQuery = "";

        public static string GetEmpProfdtls(String PROF_ID)
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT PD.PROF_DTL_ID,C.COM_HDR_ID,C.COM_DESC,PD.EMP_PROF_RATING,CD.COM_LINE_ID,CD.COM_LEVEL_DESC,LD.COM_LINK_DTL_ID,'N' AS DELETED";
            sqlQuery += " FROM HR_EMP_PROFILE_DTLS PD,HR_COMPETENCY_HDR C,HR_COMPETENCY_DTL CD,HR_COMPETANCY_LINKS_DTL LD";
            sqlQuery += " WHERE PD.COM_LINK_ID = C.COM_HDR_ID";
            sqlQuery += " AND PD.COM_LINE_ID = CD.COM_LINE_ID";
            sqlQuery += " AND PD.COM_LINK_DTL_ID = LD.COM_LINK_DTL_ID";
            sqlQuery += " AND CD.COM_LINE_ID = LD.COM_LINE_ID";
            sqlQuery += " AND C.COM_HDR_ID = CD.COM_HDR_ID";
            sqlQuery += " AND PD.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND PD.PROF_ID = '" + PROF_ID + "'";
            sqlQuery += " order by PD.PROF_DTL_ID asc";
            return sqlQuery;

        }


        public static string GetCompetency()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT C.COM_HDR_ID,C.COM_DESC";
            sqlQuery += " FROM HR_COMPETENCY_HDR C";
            // sqlQuery += " WHERE C.COM_HDR_ID = CLH.COM_HDR_ID ";
            sqlQuery += " where C.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND C.ENABLED_FLAG = 1";
            return sqlQuery;

        }


        public static string GetCompetencyLevedesc(string COM_HDR_ID)
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT distinct CD.COM_LINE_ID,CD.COM_LEVEL_DESC";
            sqlQuery += " FROM HR_COMPETENCY_HDR C,hr_competency_dtl CD,Hr_Competancy_Links_Dtl LD";
            sqlQuery += " WHERE C.COM_HDR_ID = CD.COM_HDR_ID";
            sqlQuery += " AND CD.COM_LINE_ID = LD.COM_LINE_ID";
            sqlQuery += " AND C.COM_HDR_ID = '" + COM_HDR_ID + "'";
            sqlQuery += " AND C.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND C.ENABLED_FLAG = 1";

            return sqlQuery;

        }
        public static string GetEmployeeProfName(string pos_id)
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT PH.PROF_NAME,PH.PROF_ID ";
            sqlQuery += " FROM HR_EMP_PROFILE_HDR PH ";
            sqlQuery += " WHERE PH.PROF_POSITION_ID = '" + pos_id + "'";
            sqlQuery += " AND PH.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND PH.ENABLED_FLAG = 1";

            return sqlQuery;

        }

        public static string GetCompetencyLinkDtlid(string COM_HDR_ID, string COM_LINE_ID)
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT LD.COM_LINK_DTL_ID,CD.COM_LEVEL_DESC";
            sqlQuery += " FROM HR_COMPETENCY_HDR C,hr_competency_dtl CD,Hr_Competancy_Links_Dtl LD";
            sqlQuery += "  WHERE C.COM_HDR_ID = CD.COM_HDR_ID";
            sqlQuery += " AND CD.COM_LINE_ID = LD.COM_LINE_ID";
            sqlQuery += " AND C.COM_HDR_ID = '" + COM_HDR_ID + "'";
            sqlQuery += " AND CD.COM_LINE_ID = '" + COM_LINE_ID + "'";
            sqlQuery += " AND C.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += "  AND C.ENABLED_FLAG = 1";
            return sqlQuery;

        }
        public static string GetRPTCashSalaries()
        {
            sqlQuery = string.Empty;

            sqlQuery = " select * from vw_cash_salaries ee ";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["PAY_PERIOD_ID"] != null)
                {
                    sqlQuery += " WHERE ee.PAY_PERIOD_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["PAY_PERIOD_ID"].ToString() + "'";
                }
                //if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
                //{
                //    sqlQuery += " WHERE ee.EMP_DOJ >= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy')";
                //}
                //if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
                //{
                //    sqlQuery += " AND ee.EMP_DOJ <= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["To_Date"].ToString() + "','dd/MM/yyyy')";
                //}

            }

            //if (VMVServices.Web.Utils.ReportFilterParameter != null || VMVServices.Web.Utils.ReportFilterParameter.Count != 0)
            //{
            //    if (VMVServices.Web.Utils.ReportFilterParameter["From_Date"] != null && VMVServices.Web.Utils.ReportFilterParameter["To_Date"] != null)
            //    {
            //        sqlQuery += " where ( to_date(ee.EMP_DOJ,'dd/MM/yyyy')  between to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy')  and to_date('" + VMVServices.Web.Utils.ReportFilterParameter["To_Date"].ToString() + "','dd/MM/yyyy') )";
            //    }
            //}

            //sqlQuery = " select distinct ((select sum(ptr.pay_amount)";
            //sqlQuery += "    from pay_elements          pe,";
            //sqlQuery += "    pay_final_run_dtl_dtl ptr,";
            //sqlQuery += "    pay_final_run_hdr     frh,";
            //sqlQuery += "   pay_final_run_dtl     frd";
            //sqlQuery += "    where pe.pay_element_id = ptr.pay_emp_element_id";
            //sqlQuery += "     and frh.payroll_id = frd.payroll_id";
            //sqlQuery += "     and frd.payroll_dtl_id = ptr.payroll_dtl_id";
            //sqlQuery += "     and frh.payroll_period = pp.pay_period_id";
            //sqlQuery += "    and pe.pay_ele_class = 'Earning') -";
            //sqlQuery += "    (select sum(ptr.pay_amount)";
            //sqlQuery += "     from pay_elements          pe,";
            //sqlQuery += "    pay_final_run_dtl_dtl ptr,";
            //sqlQuery += "    pay_final_run_hdr     frh,";
            //sqlQuery += "    pay_final_run_dtl     frd";
            //sqlQuery += "     where pe.pay_element_id = ptr.pay_emp_element_id";
            //sqlQuery += "     and frh.payroll_id = frd.payroll_id";
            //sqlQuery += "     and frd.payroll_dtl_id = ptr.payroll_dtl_id";
            //sqlQuery += "     and frh.payroll_period = pp.pay_period_id";
            //sqlQuery += "      and pe.pay_ele_class = 'Deduction')) as earnings,";
            //sqlQuery += "      ee.emp_first_name || ' ' || ee.emp_middle_name || ' ' ||";
            //sqlQuery += "      ee.emp_last_name as emp_name,";
            //sqlQuery += "     ee.emp_first_name_ol || ' ' || ee.emp_middle_name_ol || ' ' ||";
            //sqlQuery += "     ee.emp_last_name_ol as emp_name_ol,";
            //sqlQuery += "     ee.emp_no";
            //sqlQuery += "    from pay_elements          pe,";
            //sqlQuery += "     pay_final_run_dtl_dtl ptr,";
            //sqlQuery += "     hr_employees          ee,";
            //sqlQuery += "     pay_periods           pp,";
            //sqlQuery += "     pay_final_run_hdr     ptrh,";
            //sqlQuery += "     pay_final_run_dtl     pfrd";
            //sqlQuery += "    where pe.pay_element_id = ptr.pay_emp_element_id";
            //sqlQuery += "     and pfrd.payroll_id = ptrh.payroll_id";
            //sqlQuery += "     and pfrd.payroll_dtl_id = ptr.payroll_dtl_id";
            //sqlQuery += "     and pp.pay_period_id = ptrh.payroll_period";
            //sqlQuery += "    and ee.emp_id = ptr.pay_emp_id";

            //if (VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.EFFECTIVE_FROM_DT] != null)
            //{
            //    sqlQuery += " AND (ee.EMP_DOJ between to_date('" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.EFFECTIVE_FROM_DT].ToString() + "','dd/MM/yyyy')  and to_date('" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.EFFECTIVE_TO_DT].ToString() + "','dd/MM/yyyy') )";
            //}
            //sqlQuery += "    group by ee.emp_first_name,";
            //sqlQuery += "      ee.emp_middle_name,";
            //sqlQuery += "      ee.emp_last_name,";
            //sqlQuery += "      ee.emp_first_name_ol,";
            //sqlQuery += "       ee.emp_middle_name_ol,";
            //sqlQuery += "      ee.emp_last_name_ol,";
            //sqlQuery += "      pp.pay_period_id,";
            //sqlQuery += "    ee.emp_no";



            return sqlQuery;

        }
        public static string GetRPTBankTransformation()
        {
            sqlQuery = string.Empty;

            sqlQuery = " select * from vw_bank_transformation ebd ";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["PAY_PERIOD_ID"] != null)
                {
                    sqlQuery += " WHERE ebd.PAY_PERIOD_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["PAY_PERIOD_ID"].ToString() + "'";
                }
            }


            //if (VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.EFFECTIVE_TO_DT] != null)
            //{
            //    sqlQuery += " AND (ebd.effective_to_dt <= to_date('" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.EFFECTIVE_TO_DT].ToString() + "','dd/MM/yyyy') or ebd.effective_to_dt is null)";
            //}
            //sqlQuery = "  select tbl.earnings as earnings,";
            //sqlQuery += "   tbl.bank_name,";
            //sqlQuery += "    tbl.bank_name_ol,";
            //sqlQuery += "    count(ebd.emp_id) as no_of_emp";
            //sqlQuery += "   from hr_emp_bank_dtls ebd,";
            //sqlQuery += "      (select distinct ((select sum(ptr.pay_amount)";
            //sqlQuery += "       from pay_elements          pe,";
            //sqlQuery += "     pay_final_run_dtl_dtl ptr,";
            //sqlQuery += "   pay_final_run_hdr     frh,";
            //sqlQuery += "   pay_final_run_dtl     frd";
            //sqlQuery += "    where pe.pay_element_id = ptr.pay_emp_element_id";
            //sqlQuery += "    and frh.payroll_id = frd.payroll_id";
            //sqlQuery += "    and frd.payroll_dtl_id = ptr.payroll_dtl_id";
            //sqlQuery += "    and frh.payroll_period = pp.pay_period_id";
            //sqlQuery += "   and pe.pay_ele_class = 'Earning') -";
            //sqlQuery += "    (select sum(ptr.pay_amount)";
            //sqlQuery += "    from pay_elements          pe,";
            //sqlQuery += "    pay_final_run_dtl_dtl ptr,";
            //sqlQuery += "    pay_final_run_hdr     frh,";
            //sqlQuery += "   pay_final_run_dtl     frd";
            //sqlQuery += "     where pe.pay_element_id = ptr.pay_emp_element_id";
            //sqlQuery += "   and frh.payroll_id = frd.payroll_id";
            //sqlQuery += "    and frd.payroll_dtl_id = ptr.payroll_dtl_id";
            //sqlQuery += "   and frh.payroll_period = pp.pay_period_id";
            //sqlQuery += "   and pe.pay_ele_class = 'Deduction')) as earnings,";
            //sqlQuery += "   b.bank_name,";
            //sqlQuery += "    b.bank_id,";
            //sqlQuery += "    b.bank_name_ol";
            //sqlQuery += "   from pay_elements          pe,";
            //sqlQuery += "     pay_final_run_dtl_dtl ptr,";
            //sqlQuery += "    ca_bank               b,";
            //sqlQuery += "    pay_periods           pp,";
            //sqlQuery += "   hr_emp_bank_dtls      ebd,";
            //sqlQuery += "   pay_final_run_hdr     ptrh,";
            //sqlQuery += "   pay_final_run_dtl     pfrd";
            //sqlQuery += "    where pe.pay_element_id = ptr.pay_emp_element_id";
            //sqlQuery += "    and pfrd.payroll_id = ptrh.payroll_id";
            //sqlQuery += "    and pfrd.payroll_dtl_id = ptr.payroll_dtl_id";
            //sqlQuery += "    and pp.pay_period_id = ptrh.payroll_period";
            //sqlQuery += "    and ebd.emp_id = ptr.pay_emp_id";
            //sqlQuery += "   and ebd.emp_bank_code = b.bank_id";
            //sqlQuery += "   group by b.bank_name_ol, b.bank_name, b.bank_id, pp.pay_period_id) tbl";
            //sqlQuery += "   where ebd.emp_bank_code = tbl.bank_id";

            //if (VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.EFFECTIVE_FROM_DT] != null)
            //{
            //    sqlQuery += " AND (ebd.effective_from_dt>= to_date('" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.EFFECTIVE_FROM_DT].ToString() + "','dd/MM/yyyy') )";
            //}
            //if (VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.EFFECTIVE_TO_DT] != null)
            //{
            //    sqlQuery += " AND (ebd.effective_to_dt <= to_date('" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.EFFECTIVE_TO_DT].ToString() + "','dd/MM/yyyy') or ebd.effective_to_dt is null)";
            //}
            //sqlQuery += "   group by tbl.earnings, tbl.bank_name_ol,tbl.bank_name";

            return sqlQuery;

        }

        public static string getEmpDocumentExpiry()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT * FROM vw_hr_emp_document_expiry V WHERE ROWNUM > 0 ";

            sqlQuery += " AND V.org_id='" + VMVServices.Web.Utils.OrganizationID + "'";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["DEPT_ID"] != null)
                {
                    sqlQuery += " AND V.dept_id='" + VMVServices.Web.Utils.ReportViewFilterParameter["DEPT_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["DESIGN_ID"] != null)
                {
                    sqlQuery += " AND V.DEPT_DESIG_ID='" + VMVServices.Web.Utils.ReportViewFilterParameter["DESIGN_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["EMP_ID"] != null)
                {
                    sqlQuery += " AND V.EMP_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["EMP_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["TYPE"] != null)
                {
                    sqlQuery += " AND V.DOCUMENT_TYPE = '" + VMVServices.Web.Utils.ReportViewFilterParameter["TYPE"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
                {
                    sqlQuery += " AND V.EXPIRY_DATE >= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["From_Date"].ToString() + "','dd/MM/yyyy')";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"] != null)
                {
                    sqlQuery += " AND V.EXPIRY_DATE <= to_date('" + VMVServices.Web.Utils.ReportFilterParameter["To_Date"].ToString() + "','dd/MM/yyyy')";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["exp_days"] != null)
                {
                    sqlQuery += "  and remaining_days <= '" + VMVServices.Web.Utils.ReportFilterParameter["exp_days"].ToString() + "'";
                }

            }
            return sqlQuery;
        }
    }
}
