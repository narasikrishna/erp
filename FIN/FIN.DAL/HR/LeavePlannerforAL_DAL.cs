﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.HR
{
    public class LeavePlannerforAL_DAL
    {
        static string sqlQuery = "";

        public static string GetLeaveplannerfrAl(String LEAVE_PLAN_ID)
        {
            sqlQuery = string.Empty;

            //sqlQuery += " SELECT LP.LEAVE_PLAN_ID,E.EMP_ID,E.EMP_FIRST_NAME as EMP_NAME,D.DEPT_ID,D.DEPT_NAME,DD.DESIG_NAME,LP.FROM_DATE,LP.TO_DATE,'N' AS DELETED";
            //sqlQuery += " FROM HR_LEAVE_PLAN_FOR_AL LP,HR_EMPLOYEES E,HR_EMP_WORK_DTLS WD,";
            //sqlQuery += " HR_DEPARTMENTS D,HR_DEPT_DESIGNATIONS DD";
            //sqlQuery += " WHERE LP.EMP_ID = E.EMP_ID";
            //sqlQuery += " AND E.EMP_ID = WD.EMP_ID";
            //sqlQuery += " AND WD.EMP_DEPT_ID = D.DEPT_ID";
            //sqlQuery += " AND WD.EMP_DESIG_ID =dd.dept_desig_id";
            //sqlQuery += " AND LP.LEAVE_PLAN_ID = '" + LEAVE_PLAN_ID + "'";

            sqlQuery += " SELECT  LP.LEAVE_PLAN_ID,E.EMP_ID,E.EMP_FIRST_NAME as EMP_NAME,D.DEPT_ID,";
            sqlQuery += " D.DEPT_NAME,DD.DESIG_NAME,LP.FROM_DATE,LP.TO_DATE,'N' AS DELETED";
            sqlQuery += " FROM HR_LEAVE_PLAN_FOR_AL LP,HR_EMPLOYEES E,HR_EMP_WORK_DTLS EWD ,";
            sqlQuery += " HR_DEPARTMENTS D,HR_DEPT_DESIGNATIONS DD";
            sqlQuery += " WHERE LP.EMP_ID = E.EMP_ID";
            sqlQuery += " AND lp.attribute1 = D.DEPT_ID";
            sqlQuery += " AND d.dept_id = dd.dept_id";
            sqlQuery += " AND EWD.EMP_ID= E.EMP_ID AND EWD.EMP_DEPT_ID= D.DEPT_ID AND EWD.EMP_DESIG_ID= DD.DEPT_DESIG_ID ";
            sqlQuery += " AND EWD.EFFECTIVE_TO_DT is null ";
            sqlQuery += " AND LP.LEAVE_PLAN_ID = '" + LEAVE_PLAN_ID + "'";

            return sqlQuery;

        }

        public static string GetLeaveplannerDtl(String dept_id,string Cal_dtl_id)
        {
            sqlQuery = string.Empty;

           
            sqlQuery += " SELECT  LP.LEAVE_PLAN_ID,E.EMP_ID,E.EMP_FIRST_NAME as EMP_NAME,D.DEPT_ID,";
            sqlQuery += " D.DEPT_NAME,DD.DESIG_NAME,LP.FROM_DATE,LP.TO_DATE,'N' AS DELETED";
            sqlQuery += " FROM HR_LEAVE_PLAN_FOR_AL LP,HR_EMPLOYEES E, ";
            sqlQuery += " HR_DEPARTMENTS D,HR_DEPT_DESIGNATIONS DD,HR_EMP_WORK_DTLS EWD";
            sqlQuery += " WHERE LP.EMP_ID = E.EMP_ID   ";
            sqlQuery += " AND lp.attribute1 = D.DEPT_ID";
            sqlQuery += " AND d.dept_id = dd.dept_id";
            sqlQuery += " AND EWD.EMP_ID= E.EMP_ID AND EWD.EMP_DEPT_ID= D.DEPT_ID AND EWD.EMP_DESIG_ID= DD.DEPT_DESIG_ID ";
            sqlQuery += " AND EWD.EFFECTIVE_TO_DT is null ";
            sqlQuery += " and d.dept_id = '" + dept_id + "'";
            sqlQuery += " AND LP.CAL_DTL_ID='" + Cal_dtl_id + "'";
            return sqlQuery;

        }

    }
}


