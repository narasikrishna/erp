﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.HR
{
    public class EmpTrainingReq_DAL
    {
        static string sqlQuery = "";

        public static string GetTrainingSchDtl(string emp_id)
        {
            sqlQuery = string.Empty;



            sqlQuery += " select ed.emp_id,ts.trn_from_dt,ts.trn_to_dt,ts.training_type";
            sqlQuery += " from HR_TRM_ENROLL_DTL ed,HR_TRM_SCHEDULE_HDR ts";
            sqlQuery += " where ed.trn_sch_hdr_id = ts.trn_sch_hdr_id";
            sqlQuery += " and ts.workflow_completion_status = 1";
            sqlQuery += " and ts.enabled_flag = 1";
            sqlQuery += " and ts.trn_org_id= '" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " and ed.emp_id = '" + emp_id + "'";

            return sqlQuery;

        }

    }
}
