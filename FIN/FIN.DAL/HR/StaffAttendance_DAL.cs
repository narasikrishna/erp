﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.HR
{
    public class StaffAttendance_DAL
    {
        static string sqlQuery = "";

        public static string GetStaffAttedDtls(string staffAttendId)
        {
            sqlQuery = string.Empty;
            string strLngCode = VMVServices.Web.Utils.LanguageCode;
            sqlQuery = "    select distinct sa.PK_ID,";
            sqlQuery += "     sa.CHILD_ID,";
            sqlQuery += "       sa.STATT_ID,";
            sqlQuery += "       sa.ORG_ID,";
            sqlQuery += "       sa.FISCAL_YEAR,";
            //LL.LEAVE_DESC AS LEAVE_NAME,";
            sqlQuery += "       sa.STAFF_ID as EMP_ID,";
            sqlQuery += "           (select (he.emp_no || ' - ' ||  he.emp_first_name" + strLngCode + " || ' - ' || he.emp_middle_name" + strLngCode + " || ' - ' ||";
            sqlQuery += "              he.emp_last_name" + strLngCode + ") as emp_name";
            sqlQuery += "        from hr_employees he";
            sqlQuery += "       where he.emp_id = sa.staff_id) as emp_name,";
            sqlQuery += " LL.LEAVE_DESC as LEAVE_NAME,";           
            //sqlQuery += "  (SELECT LL.LEAVE_DESC FROM HR_LEAVE_DEFINITIONS LL,HR_LEAVE_APPLICATIONS la";
            //sqlQuery += " WHERE LL.LEAVE_ID=la.LEAVE_ID and ll.fiscal_year = la.fiscal_year and sa.leave_req_id=la.leave_req_id) AS LEAVE_NAME,";
            sqlQuery += "        sa.ATTENDANCE_DATE,";
            sqlQuery += "        sa.ATTENDANCE_TYPE,";
            sqlQuery += "   (select lv.code from ssm_code_masters lv where lv.code=sa.attendance_type and lv.parent_code='ATY') as ATTENDANCE_TYPE_name,";
            sqlQuery += "        sa.LEAVE_REQ_ID,";
            sqlQuery += "        sa.REASON,";
            sqlQuery += "        sa.ENABLED_FLAG,'N' as DELETED,";
            sqlQuery += "       sa.WORKFLOW_COMPLETION_STATUS";
            sqlQuery += "     from hr_staff_attendance sa,HR_LEAVE_DEFINITIONS LL, HR_LEAVE_APPLICATIONS la";           
            sqlQuery += "   where sa.workflow_completion_status = 1";
            sqlQuery += "   and LL.LEAVE_ID(+) = la.LEAVE_ID";
            sqlQuery += "    and sa.leave_req_id = la.leave_req_id(+)";
            //sqlQuery += "    and sa.staff_id=la.staff_id";
            sqlQuery += "   AND sa.STATT_ID = '" + staffAttendId + "'";



            return sqlQuery;

        }

        public static string GetStaffAbsencelList(string attendanceDate)
        {
            sqlQuery = string.Empty;

            sqlQuery = "   select s.attendance_date,";
            sqlQuery += "  (select ee.emp_first_name || '' || ee.emp_middle_name || '' ||";
            sqlQuery += "       ee.emp_last_name";
            sqlQuery += "   from hr_employees ee";
            sqlQuery += "   where ee.emp_id = s.staff_id)";
            sqlQuery += "   from HR_STAFF_ATTENDANCE s";
            sqlQuery += "   where s.enabled_flag = '1'";
            sqlQuery += "   and s.workflow_completion_status = '1'";
            sqlQuery += "   and s.attendance_date = '" + attendanceDate + "'";

            return sqlQuery;
        }
        public static string getAttendanceRep4Month()
        {
            sqlQuery = string.Empty;
            string strLngCode = VMVServices.Web.Utils.LanguageCode;
            //sqlQuery = "SELECT * FROM VW_STAFF_ATTENDANCE_4_MONTH V WHERE ROWNUM > 0 ";

            //if (VMVServices.Web.Utils.ReportFilterParameter != null || VMVServices.Web.Utils.ReportFilterParameter.Count != 0)
            //{
            //    if (VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.ITEM_TYPE] != null)
            //    {
            //        sqlQuery += " AND V.ITEM_TYPE = '" + VMVServices.Web.Utils.ReportFilterParameter[FINColumnConstants.ITEM_TYPE].ToString() + "'";
            //    }
            //    if (VMVServices.Web.Utils.ReportFilterParameter["ITEM_CAT_NAME"] != null)
            //    {
            //        sqlQuery += " AND V.ITEM_CATEGORY = '" + VMVServices.Web.Utils.ReportFilterParameter["ITEM_CAT_NAME"].ToString() + "'";
            //    }
            //    if (VMVServices.Web.Utils.ReportFilterParameter["ITEM_UOM"] != null)
            //    {
            //        sqlQuery += " AND V.ITEM_UOM = '" + VMVServices.Web.Utils.ReportFilterParameter["ITEM_UOM"].ToString() + "'";
            //    }
            //    if (VMVServices.Web.Utils.ReportFilterParameter["ITEM_GROUP_NAME"] != null)
            //    {
            //        sqlQuery += " AND V.ITEM_GROUP = '" + VMVServices.Web.Utils.ReportFilterParameter["ITEM_GROUP_NAME"].ToString() + "'";
            //    }
            //}

            sqlQuery += " SELECT 0 AS ROW_NUM,'' AS EMPTY_COL,'' AS EMP_NAME,'' AS D1,'' AS D2,'' AS D3,'' AS D4,'' AS D5,'' AS D6,'' AS D7,'' AS D8,'' AS D9,'' AS D10,'' AS D11,'' AS D12,'' AS D13 ";
            sqlQuery += "  ,'' AS D14,'' AS D15,'' AS D16,'' AS D17,'' AS D18,'' AS D19,'' AS D20,'' AS D21,'' AS D22,'' AS D23,'' AS D24,'' AS D25,'' AS D26,'' AS D27,'' AS D28,'' AS D29,'' AS D30,'' AS D31  FROM DUAL ";
            sqlQuery += " UNION ";
            sqlQuery += " SELECT rownum,'' AS Empty_Col, EMP_NAME,D1,D2,D3,D4,D5,D6,D7,D8,D9,D10,D11,D12,D13,D14,D15,D16,D17,D18,D19,D20,D21,D22,D23,D24,D25,D26,D27,D28,D29,D30,D31 ";
            sqlQuery += " FROM ( ";
            sqlQuery += " SELECT EMP.EMP_FIRST_NAME" + strLngCode + " ||  EMP.EMP_MIDDLE_NAME" + strLngCode + " || EMP.EMP_LAST_NAME" + strLngCode + " AS EMP_NAME ";
            sqlQuery += " , CASE ATTENDANCE_TYPE  WHEN 'Present'  THEN 'P' ELSE ";
            sqlQuery += " ATTENDANCE_TYPE END AS ATTENDANCE_TYPE ";
            //sqlQuery += " (SELECT LD.LEAVE_TYPE FROM HR_LEAVE_DEFINITIONS LD WHERE LD.LEAVE_ID = SA.LEAVE_REQ_ID) END AS ATTENDANCE_TYPE ";
            sqlQuery += " ,TO_CHAR(ATTENDANCE_DATE,'DD') AS ATTDAY ";
            sqlQuery += " FROM HR_EMPLOYEES EMP";
            sqlQuery += " LEFT JOIN HR_STAFF_ATTENDANCE SA  ON EMP.EMP_ID = SA.STAFF_ID ";
            if (VMVServices.Web.Utils.ReportFilterParameter["MONTH"] != null)
            {
                sqlQuery += " AND TO_CHAR(ATTENDANCE_DATE,'MM') = '" + VMVServices.Web.Utils.ReportFilterParameter["MONTH"].ToString() + "'";
            }
            if (VMVServices.Web.Utils.ReportFilterParameter["YEAR"] != null)
            {
                sqlQuery += " AND TO_CHAR(ATTENDANCE_DATE,'YYYY') = '" + VMVServices.Web.Utils.ReportFilterParameter["YEAR"].ToString() + "'";
            }
            sqlQuery += "  INNER JOIN HR_EMP_WORK_DTLS EWD ON EWD.EMP_ID= EMP.EMP_ID AND ((NVL(SA.Attendance_Date,SYSDATE) BETWEEN EWD.EFFECTIVE_FROM_DT AND  EWD.EFFECTIVE_TO_DT) ";
            sqlQuery += " OR (EWD.EFFECTIVE_FROM_DT <= NVL(SA.Attendance_Date,SYSDATE) AND EWD.EFFECTIVE_TO_DT IS NULL)) ";
            sqlQuery += " WHERE EMP.EMP_ID !='0' ";

            if (VMVServices.Web.Utils.ReportFilterParameter["DEPT_NAME"] != null)
            {
                sqlQuery += " AND ewd.emp_dept_id='" + VMVServices.Web.Utils.ReportFilterParameter["DEPT_NAME"].ToString() + "'";
            }

            if (VMVServices.Web.Utils.ReportFilterParameter["EMPID"] != null)
            {
                sqlQuery += " AND EMP.EMP_ID='" + VMVServices.Web.Utils.ReportFilterParameter["EMPID"].ToString() + "'";
            }
            sqlQuery += " ORDER BY EMP_NAME ) Z ";
            sqlQuery += " PIVOT( ";
            sqlQuery += " MAX( ATTENDANCE_TYPE) ";
            sqlQuery += " FOR ATTDAY IN ( ";
            sqlQuery += " '01' AS D1 ,'02' AS D2  ,'03' AS D3,'04' AS D4,'05' AS D5,'06' AS D6,'07' AS D7,'08' AS D8,'09' AS D9,'10' AS D10 ";
            sqlQuery += " ,'11' AS D11,'12' AS D12,'13' AS D13,'14' AS D14,'15' AS D15,'16' AS D16,'17' AS D17,'18' AS D18,'19' AS D19,'20' AS D20 ";
            sqlQuery += " ,'21' AS D21,'22' AS D22,'23' AS D23,'24' AS D24,'25' AS D25,'26' AS D26,'27' AS D27,'28' AS D28,'29' AS D29,'30' AS D30,'31' AS D31) )  PVT ";
            sqlQuery += " ORDER BY ROW_NUM ";


            return sqlQuery;
        }
    }
}
