﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;

namespace FIN.DAL.HR
{
    public class AppraisalDefineKRA_DAL
    {
        static string sqlQuery = "";


        public static string GetAppraisalDefineKRADetails(string Master_id)
        {
            sqlQuery = string.Empty;

            //sqlQuery = " SELECT HPOS.PER_TYPE AS ASSIGN_TYPE_NAME,  HPOSET.PER_CODE" + VMVServices.Web.Utils.LanguageCode + " ||' - '|| HPOSET.PER_DESC" + VMVServices.Web.Utils.LanguageCode + " AS ASSIGN_PER_OBJ_NAME , HAAD.ASSIGN_TYPE, HAAD.ASSIGN_PER_OBJ_ID, HAAD.ASSIGN_DTL_ID, HAAD.ASSIGN_HDR_ID,'N' AS DELETED ";
            //sqlQuery += " FROM HR_PER_APP_ASSIGNMENT_DTL HAAD, HR_PER_OBJECTIVE_SET HPOS, HR_PER_OBJECTIVE_SET HPOSET ";
            //sqlQuery += " WHERE HAAD.WORKFLOW_COMPLETION_STATUS = 1";
            //sqlQuery += " AND HAAD.ENABLED_FLAG = 1";
            //sqlQuery += " AND HPOS.WORKFLOW_COMPLETION_STATUS = 1";
            //sqlQuery += " AND HPOS.ENABLED_FLAG = 1";
            //sqlQuery += " AND HPOSET.WORKFLOW_COMPLETION_STATUS = 1";
            //sqlQuery += " AND HPOSET.ENABLED_FLAG = 1";
            //sqlQuery += " AND HAAD.ASSIGN_TYPE = HPOS.PER_OBJ_ID";
            //sqlQuery += " AND HAAD.ASSIGN_PER_OBJ_ID = HPOSET.PER_OBJ_ID";
            //sqlQuery += " AND HAAD.ASSIGN_HDR_ID = '" + Master_id + "'";
            //sqlQuery += " ORDER BY HAAD.ASSIGN_DTL_ID";

            sqlQuery += " SELECT HPOS.CODE AS LOOKUP_ID,HPOS.DESCRIPTION AS LOOKUP_NAME,HPOSET.PER_CODE" + VMVServices.Web.Utils.LanguageCode + " ||' - '|| HPOSET.PER_DESC" + VMVServices.Web.Utils.LanguageCode + " AS PER_OBJ_NAME , HPOSET.PER_OBJ_ID, HAAD.ASSIGN_DTL_ID, HAAD.ASSIGN_HDR_ID,'N' AS DELETED ";
            sqlQuery += " FROM HR_PER_APP_ASSIGNMENT_DTL HAAD, SSM_CODE_MASTERS HPOS, HR_PER_OBJECTIVE_SET HPOSET ";
            sqlQuery += " WHERE HAAD.WORKFLOW_COMPLETION_STATUS = 1";
            sqlQuery += " AND HAAD.ENABLED_FLAG = 1";
            sqlQuery += " AND HAAD.ASSIGN_TYPE = HPOS.CODE";
            sqlQuery += " and hpos.parent_code = 'AAT'";
            sqlQuery += " AND HAAD.ASSIGN_PER_OBJ_ID = HPOSET.PER_OBJ_ID";
            sqlQuery += " AND HAAD.ASSIGN_HDR_ID = '" + Master_id + "'";
            sqlQuery += " ORDER BY HAAD.ASSIGN_DTL_ID";
            return sqlQuery;

        }



        public static string GetType()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT HPOS.PER_OBJ_ID AS TYPE_ID, (HPOS.PER_CODE||' '||HPOS.PER_TYPE) AS TYPE_NAME ";
            sqlQuery += " FROM HR_PER_OBJECTIVE_SET HPOS ";
            sqlQuery += " WHERE HPOS.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " AND HPOS.ENABLED_FLAG = 1 ";
            sqlQuery += " ORDER BY PER_TYPE ";

            return sqlQuery;

        }
        public static string GetKPINumber(string str_DeptId, string JOB_ID)
        {
            sqlQuery = string.Empty;
            sqlQuery = " select aa.assign_hdr_id,aa.assign_version from HR_PER_APP_ASSIGNMENT_HDR aa";
            sqlQuery += " where aa.enabled_flag='1'";
            sqlQuery += " and aa.workflow_completion_status='1'";
            sqlQuery += " AND aa.ASSIGN_DEPT_ID='" + str_DeptId + "'";
            sqlQuery += " AND aa.ASSIGN_JOB_ID ='" + JOB_ID + "'";
            sqlQuery += " order by aa.assign_hdr_id asc";
            return sqlQuery;

        }
        public static string GetObjectiveDescription()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT HPOS.PER_OBJ_ID AS PER_OBJ_ID, HPOS.PER_CODE" + VMVServices.Web.Utils.LanguageCode + " ||' - '|| HPOS.PER_DESC" + VMVServices.Web.Utils.LanguageCode + " AS PER_OBJ_NAME ";
            sqlQuery += " FROM HR_PER_OBJECTIVE_SET HPOS ";
            sqlQuery += " WHERE HPOS.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " AND HPOS.ENABLED_FLAG = 1 ";
            sqlQuery += " ORDER BY PER_OBJ_NAME ";

            return sqlQuery;

        }

        public static string GetObjectiveDescriptionasperTyp(string PER_TYPE)
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT HPOS.PER_OBJ_ID AS PER_OBJ_ID, HPOS.PER_CODE" + VMVServices.Web.Utils.LanguageCode + " ||' - '|| HPOS.PER_DESC" + VMVServices.Web.Utils.LanguageCode + " AS PER_OBJ_NAME ";
            sqlQuery += " FROM HR_PER_OBJECTIVE_SET HPOS ";
            sqlQuery += " WHERE HPOS.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " AND HPOS.ENABLED_FLAG = 1 ";
            sqlQuery += " AND HPOS.PER_TYPE = '" + PER_TYPE + "'";
            sqlQuery += " AND HPOS.PER_ORG_ID = '" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " ORDER BY PER_OBJ_NAME ";

            return sqlQuery;

        }


        public static string GetJobDetails()
        {
            sqlQuery = string.Empty;

            sqlQuery = " SELECT HJ.JOB_ID AS JOB_ID, HJ.JOB_CODE" + VMVServices.Web.Utils.LanguageCode + " ||' - '|| HJ.JOB_DESC" + VMVServices.Web.Utils.LanguageCode + " AS JOB_NAME ";
            sqlQuery += " FROM HR_JOBS HJ ";
            sqlQuery += " WHERE HJ.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " AND HJ.ENABLED_FLAG = 1 ";
            sqlQuery += " ORDER BY JOB_DESC ";

            return sqlQuery;

        }

        public static string GetAraisal()
        {
            sqlQuery = string.Empty;

            sqlQuery += " select distinct ah.assign_hdr_id,ad.app_desc from HR_PER_APP_ASSIGNMENT_HDR ah, ";
            sqlQuery += " HR_PER_APPRAISAL_DEF ad ";
            sqlQuery += " where AH.ASSIGN_APP_ID = AD.APP_ID ";
            sqlQuery += " and ah.workflow_completion_status = 1 ";
            sqlQuery += " and ah.enabled_flag = 1 ";
            sqlQuery += " and ah.assign_org_id ='" + VMVServices.Web.Utils.OrganizationID + "'";
            return sqlQuery;

        }


        public static string GetAppraisalDefKPI(string str_DeptId, string JOB_ID)
        {
            sqlQuery = string.Empty;


            sqlQuery += " select AH.ASSIGN_HDR_ID,AD.APP_DESC" + VMVServices.Web.Utils.LanguageCode + " AS APP_DESC from HR_PER_APP_ASSIGNMENT_HDR AH ";
            sqlQuery += " INNER JOIN HR_PER_APPRAISAL_DEF AD ON AH.ASSIGN_APP_ID = AD.APP_ID ";
            sqlQuery += " where AH.workflow_completion_status=1 ";
            sqlQuery += " AND ASSIGN_DEPT_ID='" + str_DeptId + "'";
            sqlQuery += " AND ASSIGN_JOB_ID ='" + JOB_ID + "'";

            return sqlQuery;

        }
        public static string GetAppraisal_Employee(string str_App_REvw_Id)
        {
            sqlQuery = string.Empty;
            string strLngCode = VMVServices.Web.Utils.LanguageCode;
            sqlQuery += " select HE.EMP_NO, HE.emp_first_name" + strLngCode + " || '' || HE.emp_middle_name" + strLngCode + " || '' ||HE.emp_last_name" + strLngCode + " as Employee_Name, 0 as PER_APP_INIT_ID,HE.EMP_ID,ARD.RA_HDR_ID,'N' AS DELETED,";
            sqlQuery += " CASE HE.ENABLED_FLAG WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS ENABLED_FLAG ";
            sqlQuery += " from hr_per_app_review_asgnmnt_hdr ARH,hr_per_app_review_asgnmnt_dtl ARD,HR_EMPLOYEES HE ";
            sqlQuery += " where ARH.RA_HDR_ID = '" + str_App_REvw_Id + "'";
            sqlQuery += " and ARH.RA_HDR_ID = ARD.RA_HDR_ID";
            sqlQuery += "  and ARD.RA_EMP_ID = HE.EMP_ID";
          //  sqlQuery += " and HE.EMP_INTERNAL_EXTERNAL = 'I'";
            sqlQuery += " and arh.enabled_flag = '1'";


            return sqlQuery;

        }

        public static string GetAppraisalDescription(string str_DeptId, string JOB_ID)
        {
            sqlQuery = string.Empty;


            sqlQuery += " select ARH.Description, arh.ra_hdr_id ";
            sqlQuery += " from hr_per_app_review_asgnmnt_hdr ARH ";
            sqlQuery += " where arh.ra_dept_id = '" + str_DeptId + "' ";
            sqlQuery += " and arh.ra_job_id ='" + JOB_ID + "'";
            sqlQuery += " and arh.enabled_flag = '1' ";


            return sqlQuery;

        }

        public static string GetAppraisal()
        {
            sqlQuery = string.Empty;


            sqlQuery += " select ARH.Description, arh.ra_hdr_id ";
            sqlQuery += " from hr_per_app_review_asgnmnt_hdr ARH ";
            sqlQuery += " where arh.enabled_flag = '1' ";


            return sqlQuery;

        }

        public static string GetAppraisalDtlsBasedInitiateAppr(string str_DeptId, string JOB_ID)
        {
            sqlQuery = string.Empty;

            sqlQuery += " select DISTINCT AH.ASSIGN_HDR_ID, AD.APP_DESC" + VMVServices.Web.Utils.LanguageCode + " AS APP_DESC, AD.APP_ID, AD.APP_CODE, AD.APP_PERIOD";
            sqlQuery += " from HR_PER_APP_INIT IA, hr_per_app_review_asgnmnt_hdr ARH, HR_PER_APP_ASSIGNMENT_HDR AH";
            sqlQuery += " INNER JOIN HR_PER_APPRAISAL_DEF AD ON AH.ASSIGN_APP_ID = AD.APP_ID ";
            sqlQuery += " where IA.RA_HDR_ID = ARH.RA_HDR_ID";
            sqlQuery += " AND AH.ASSIGN_HDR_ID = ARH.ASSIGN_HDR_ID";
            sqlQuery += " and IA.PER_ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " and IA.ENABLED_FLAG='1' ";
            sqlQuery += " AND arh.ra_dept_id='" + str_DeptId + "'";
            sqlQuery += " AND arh.ra_job_id ='" + JOB_ID + "'";
            sqlQuery += " and ah.assign_status='Approved'";

            return sqlQuery;

        }
        public static string GetAppraisalDtlsBasedInitiateAppr()
        {
            sqlQuery = string.Empty;

            sqlQuery += " select distinct  AH.ASSIGN_HDR_ID, AD.APP_DESC" + VMVServices.Web.Utils.LanguageCode + " AS APP_DESC, AD.APP_ID, AD.APP_CODE, AD.APP_PERIOD";
            sqlQuery += " from HR_PER_APP_INIT IA, hr_per_app_review_asgnmnt_hdr ARH, HR_PER_APP_ASSIGNMENT_HDR AH";
            sqlQuery += " INNER JOIN HR_PER_APPRAISAL_DEF AD ON AH.ASSIGN_APP_ID = AD.APP_ID ";
            sqlQuery += " where IA.RA_HDR_ID = ARH.RA_HDR_ID";
            sqlQuery += " AND AH.ASSIGN_HDR_ID = ARH.ASSIGN_HDR_ID";
            sqlQuery += " and IA.PER_ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " and IA.ENABLED_FLAG='1' ";
            sqlQuery += " and ah.assign_status='Approved'";

            return sqlQuery;

        }
        public static string GetAppraisalDetails()
        {
            sqlQuery = string.Empty;

            sqlQuery += "  select ad.app_id,ad.app_desc from HR_PER_APPRAISAL_DEF AD";
            sqlQuery += "  where ad.enabled_flag=1";
            sqlQuery += "  and ad.workflow_completion_status=1";
            sqlQuery += "  and ad.APP_ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";

            return sqlQuery;

        }

        static string spOutPut = string.Empty;
        public static System.Collections.SortedList ErrorCollection = new System.Collections.SortedList();


        public static string GetSPFOR_DUPLICATE_CHECK(string P_ASSIGN_APP_ID, string P_ASSIGN_DEPT_ID, string P_ASSIGN_JOB_ID, string P_RECORD_ID)
        {
            try
            {
                string retcode = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "PKG_VALIDATIONS.PROC_ERR_MGR_APP_DEF_KRA";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add("@P_MODULE_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_SCREEN_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_ASSIGN_APP_ID", OracleDbType.Char).Value = P_ASSIGN_APP_ID;
                oraCmd.Parameters.Add("@P_ASSIGN_DEPT_ID", OracleDbType.Char).Value = P_ASSIGN_DEPT_ID;
                oraCmd.Parameters.Add("@P_ASSIGN_JOB_ID", OracleDbType.Char).Value = P_ASSIGN_JOB_ID;
                oraCmd.Parameters.Add("@P_RECORD_ID", OracleDbType.Char).Value = P_RECORD_ID;
                oraCmd.Parameters.Add("@P_RET", OracleDbType.Varchar2, 150);
                oraCmd.Parameters["@P_RET"].Direction = ParameterDirection.Output;
                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

                retcode = oraCmd.Parameters["@P_RET"].Value.ToString();

                return retcode;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
