﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;

namespace FIN.DAL.HR
{
    public class HolidayMaster_DAL
    {
        static string sqlQuery = "";

        public static string GetHolodayMasterDetails(String holidayId)
        {
            sqlQuery = string.Empty;


            sqlQuery = " select h.ORG_ID,h.HOLIDAY_LOC_ID as LOC_ID,h.COUNTRY_ID as COUNTRY_CODE,(select ss.full_name||ss.short_name as country_name from ssm_countries ss where ss.country_code=h.COUNTRY_ID) as country_name,";
            sqlQuery += " h.FISCAL_YEAR,(select l.loc_desc from hr_locations l where l.loc_id=h.HOLIDAY_LOC_ID) as loc_desc,";
            sqlQuery += "  h.HOLIDAY_ID,";
            sqlQuery += "  h.HOLIDAY_DATE,";
            sqlQuery += "  CASE h.ENABLED_FLAG WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS ENABLED_FLAG,";
            sqlQuery += "   h.HOLIDAY_DESC,'N' AS DELETED";
            sqlQuery += "  from hr_holidays_master h";

            sqlQuery += " where h.WORKFLOW_COMPLETION_STATUS='1'";
            sqlQuery += " and h.org_id='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += " and h.HOLIDAY_ID='" + holidayId + "'";

            return sqlQuery;

        }
        static string spOutPut = string.Empty;
        public static System.Collections.SortedList ErrorCollection = new System.Collections.SortedList();


        public static string GetSPFOR_DUPLICATE_CHECK(string P_HOLIDAY_DATE, string P_RECORD_ID)
        {
            try
            {
                string retcode = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "PKG_VALIDATIONS.PROC_ERR_MGR_HOLIDAY_MST";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add("@P_MODULE_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_SCREEN_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_HOLIDAY_DATE", OracleDbType.Char).Value = P_HOLIDAY_DATE;
                oraCmd.Parameters.Add("@P_RECORD_ID", OracleDbType.Char).Value = P_RECORD_ID;
                oraCmd.Parameters.Add("@P_RET", OracleDbType.Varchar2, 150);
                oraCmd.Parameters["@P_RET"].Direction = ParameterDirection.Output;
                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

                retcode = oraCmd.Parameters["@P_RET"].Value.ToString();

                return retcode;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static string GetHolodayDetails_finyr(String fiscal_year)
        {
            sqlQuery = string.Empty;



            sqlQuery += " select h.ORG_ID,h.HOLIDAY_LOC_ID as LOC_ID,h.COUNTRY_ID as COUNTRY_CODE,(select ss.full_name|| ' - ' ||ss.short_name as country_name from ssm_countries ss where ss.country_code=h.COUNTRY_ID) as country_name,";
            sqlQuery += " h.FISCAL_YEAR,(select l.loc_desc from hr_locations l where l.loc_id=h.HOLIDAY_LOC_ID) as loc_desc,";
            sqlQuery += " h.HOLIDAY_ID,h.HOLIDAY_DATE,CASE h.ENABLED_FLAG WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS ENABLED_FLAG, h.HOLIDAY_DESC,'N' AS DELETED";
            sqlQuery += " from hr_holidays_master h";
            sqlQuery += " where h.WORKFLOW_COMPLETION_STATUS='1'";
            sqlQuery += " and h.fiscal_year = '" + fiscal_year + "'";

            return sqlQuery;

        }
        public static string GetNoOfHolidaysBWDays(string str_fromdate, string str_todate)
        {
            sqlQuery = string.Empty;
            sqlQuery += " SELECT COUNT(*) AS NO_OF_HOLIDAY ";
            sqlQuery += " FROM HR_HOLIDAYS_MASTER WHERE HOLIDAY_DATE >= TO_DATE('" + str_fromdate + "','DD/MM/YYYY') ";
            sqlQuery += " AND HOLIDAY_DATE<=TO_DATE('" + str_todate + "','DD/MM/YYYY') ";
            return sqlQuery;
        }
    }
}
