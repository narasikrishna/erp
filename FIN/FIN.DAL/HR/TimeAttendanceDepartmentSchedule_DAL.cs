﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;

namespace FIN.DAL.HR
{
  public  class TimeAttendanceDepartmentSchedule_DAL
    {
        static string sqlQuery = "";

        public static string GetTMATNDdeptschdtls(String tm_sch_id)
        {
            sqlQuery = string.Empty;



            sqlQuery = " SELECT DS.TM_SCH_ID,ds.TM_GRP_CODE,d.dept_id,d.dept_name,s.tm_sch_code,s.tm_sch_desc,DS.TM_START_DATE,DS.TM_END_DATE,'N' AS DELETED,";
            sqlQuery += " case DS.ENABLED_FLAG when '1' then 'TRUE' else 'FALSE' end as ENABLED_FLAG";
            sqlQuery += " FROM tm_dept_sch DS,hr_departments d,TM_SCHEDULES s";
            sqlQuery += " where ";
            sqlQuery += "  DS.TM_DEPT_ID = D.DEPT_ID";
            sqlQuery += " AND DS.TM_SCH_CODE = S.TM_SCH_CODE";
            sqlQuery += " and ds.workflow_completion_status = 1";
            sqlQuery += " and ds.tm_sch_id = '" + tm_sch_id + "'";
            sqlQuery += " and ds.TM_ORG_ID='" + VMVServices.Web.Utils.OrganizationID + "'";
            return sqlQuery;

        }


        static string spOutPut = string.Empty;
        public static System.Collections.SortedList ErrorCollection = new System.Collections.SortedList();


        public static string GetSPFOR_DUPLICATE_CHECK(string P_DEPT_ID, string P_GROUP_ID,string P_SCHEDULE_CODE, string P_RECORD_ID)
        {
            try
            {
                string retcode = string.Empty;

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandText = "PKG_VALIDATIONS.PROC_ERR_MGR_TM_DEPT_SCH";
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.Parameters.Add("@P_MODULE_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_SCREEN_CODE", OracleDbType.Char).Value = DBNull.Value;
                oraCmd.Parameters.Add("@P_DEPT_ID", OracleDbType.Char).Value = P_DEPT_ID;
                oraCmd.Parameters.Add("@P_GROUP_ID", OracleDbType.Char).Value = P_GROUP_ID;
                oraCmd.Parameters.Add("@P_SCHEDULE_CODE", OracleDbType.Char).Value = P_SCHEDULE_CODE;
                oraCmd.Parameters.Add("@P_RECORD_ID", OracleDbType.Char).Value = P_RECORD_ID;
                oraCmd.Parameters.Add("@P_RET", OracleDbType.Varchar2, 150);
                oraCmd.Parameters["@P_RET"].Direction = ParameterDirection.Output;
                oraCmd = DBMethod.ExecuteStoredProcedure(oraCmd);

                retcode = oraCmd.Parameters["@P_RET"].Value.ToString();

                return retcode;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}