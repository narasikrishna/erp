﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.HR
{
    public class Vacancies_DAL
    {
        static string sqlQuery = "";
        public static string getVacancies()
        {
            sqlQuery = string.Empty;

            sqlQuery = " select v.vac_id,v.vac_name";
            sqlQuery += "  from hr_vacancies v where v.enabled_flag='1'";
            sqlQuery += "  and v.workflow_completion_status='1'";
            sqlQuery += "   and  v.org_id='" + VMVServices.Web.Utils.OrganizationID + "'";
            sqlQuery += "  order by v.vac_name asc";

            return sqlQuery;
        }

        public static string GetVacancyReport()
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM VW_VACANCIES V WHERE ROWNUM > 0 ";
          ///  sqlQuery += " AND V.org_id='" + VMVServices.Web.Utils.OrganizationID + "'";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["DEPT_ID"] != null)
                {
                    sqlQuery += " AND V.DEPT_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["DEPT_ID"].ToString() + "'";
                }
                if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
                {
                    sqlQuery += " AND (V.CREATED_DATE) between  to_date('" + VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"].ToString() + "','dd/mm/yyyy')";

                    sqlQuery += " AND  to_date('" + VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"].ToString() + "','dd/mm/yyyy')";
                }
            }
            return sqlQuery;
        }
    }
}
