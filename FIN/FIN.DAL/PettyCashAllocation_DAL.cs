﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.DAL.CA
{
    public class PettyCashAllocation_DAL
    {
        static string sqlQuery = "";
        public static string GetChequeDetails(string check_bank_id, string check_branch_id, string account_id)
        {
            sqlQuery = string.Empty;

            sqlQuery += " select CCH.CHECK_HDR_ID,CCH.CHECK_HDR_ID||' - '||a.vendor_bank_account_code as CHECK_HDR_ID";
            sqlQuery += " from CA_CHECK_HDR CCH,CA_BANK_ACCOUNTS a";
            sqlQuery += " where CCH.ENABLED_FLAG = 1 ";
            sqlQuery += " and cch.account_id = a.ACCOUNT_ID";
            sqlQuery += " and CCH.WORKFLOW_COMPLETION_STATUS = 1 ";
            sqlQuery += " and cch.check_bank_id = '" + check_bank_id + "'";
            sqlQuery += " and cch.check_branch_id = '" + check_branch_id + "'";
            sqlQuery += " and cch.account_id = '" + account_id + "'";
            sqlQuery += " ORDER BY CCH.CHECK_HDR_ID ";


            return sqlQuery;
        }

        public static string GetChequeDetails_dtls(string Chk_hdr_id)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select CCD.CHECK_DTL_ID,CCD.CHECK_NUMBER,CCD.CHECK_AMT from CA_CHECK_DTL CCD ";
            sqlQuery += " where CCD.Enabled_Flag = '1' and CCD.WORKFLOW_COMPLETION_STATUS = '1'";
            sqlQuery += " and CCD.CHECK_HDR_ID = '" + Chk_hdr_id + "'";
            sqlQuery += " ORDER BY CCD.CHECK_NUMBER ";
            return sqlQuery;
        }

        public static string GetChequeNo()
        {
            sqlQuery = string.Empty;

            sqlQuery = " select CCD.CHECK_DTL_ID,CCD.CHECK_NUMBER,CCD.CHECK_AMT from CA_CHECK_DTL CCD ";
            sqlQuery += " where CCD.Enabled_Flag = '1' and CCD.WORKFLOW_COMPLETION_STATUS = '1'";
           // sqlQuery += " and CCD.CHECK_HDR_ID = '" + Chk_hdr_id + "'";
            sqlQuery += " ORDER BY CCD.CHECK_NUMBER ";
            return sqlQuery;
        }

        public static string GetChequeAmount(string CHECK_DTL_ID)
        {
            sqlQuery = string.Empty;

            sqlQuery = " select CCD.CHECK_DTL_ID,CCD.CHECK_NUMBER,CCD.CHECK_AMT from CA_CHECK_DTL CCD ";
            sqlQuery += " where CCD.Enabled_Flag = '1' and CCD.WORKFLOW_COMPLETION_STATUS = '1'";
            sqlQuery += " and CCD.CHECK_DTL_ID = '" + CHECK_DTL_ID + "'";
            sqlQuery += " ORDER BY CCD.CHECK_NUMBER ";
            return sqlQuery;
        }

        public static string GetUsers()
        {
            sqlQuery = string.Empty;

            sqlQuery = " select su.user_code from ssm_users su ";
            sqlQuery += " where su.enabled_flag = '1'";
            sqlQuery += " and su.workflow_completion_status = '1' ";
            sqlQuery += " ORDER BY su.user_code ";
            return sqlQuery;
        }

        public static string GetPettyCashAllocationRepData()
        {
            sqlQuery = string.Empty;
            sqlQuery += " SELECT * FROM VW_PETTY_CASH_ALLOCATE V WHERE ROWNUM>0 ";


            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                if (VMVServices.Web.Utils.ReportViewFilterParameter["ALLOCATE_ID"] != null)
                {
                    sqlQuery += " AND V.GL_PCA_HDR_ID = '" + VMVServices.Web.Utils.ReportViewFilterParameter["ALLOCATE_ID"].ToString() + "'";
                }
            }


            return sqlQuery;

        }
    }
}
