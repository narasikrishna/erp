﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="RPTAssetwiseEmpParam.aspx.cs" Inherits="FIN.Client.HR_Reports.RPTAssetwiseEmpParam" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 700px">
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 130px" id="Div1">
                Asset
            </div>
            <div class="divtxtBox  LNOrient" style="width: 300px">
                <asp:DropDownList ID="ddlAsset" runat="server" TabIndex="1" CssClass=" ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 130px" id="Div3">
                Department
            </div>
            <div class="divtxtBox  LNOrient" style="width: 300px">
                <asp:DropDownList ID="ddlDepartment" runat="server" AutoPostBack="true" TabIndex="1"
                    CssClass=" ddlStype" OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 130px" id="Div3">
                Designation
            </div>
            <div class="divtxtBox  LNOrient" style="width: 300px">
                <asp:DropDownList ID="ddlDesig" runat="server" TabIndex="2" AutoPostBack="true" CssClass=" ddlStype"
                    OnSelectedIndexChanged="ddlDesig_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 130px" id="lblPeriodID">
                Employee Name
            </div>
            <div class="divtxtBox  LNOrient" style="width: 300px">
                <asp:DropDownList ID="ddlEmployeeName" runat="server" TabIndex="3" CssClass="ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 130px" id="lblDate">
                From Date
            </div>
            <div class="divtxtBox  LNOrient" style="width: 100px">
                <asp:TextBox ID="txtFromDate" runat="server" TabIndex="4" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]]  RequiredField txtBox"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calExtender1" TargetControlID="txtFromDate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
            <div class="lblBox LNOrient" style="width: 80px" id="Div2">
                To Date
            </div>
            <div class="divtxtBox  LNOrient" style="width: 100px">
                <asp:TextBox ID="txtToDate" runat="server" TabIndex="5" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]]  RequiredField txtBox"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtToDate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divFormcontainer" style="width: 430px" id="divMainContainer">
            <div class="divRowContainer divReportAction">
                <table class="ReportTable">
                    <tr>
                        <td>
                            <asp:ImageButton ID="btnSave" runat="server" ImageUrl="../Images/show-report-icon.png"
                                OnClick="btnSave_Click" Width="35px" Height="25px" Style="border: 0px" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
 <script src="../LanguageScript/HR/HRChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
