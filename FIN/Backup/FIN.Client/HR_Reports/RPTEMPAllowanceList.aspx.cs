﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.BLL;
using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;
using Novacode;
using System.Diagnostics;
using System.Net;
using System.IO;

namespace FIN.Client.HR_Reports
{
    public partial class RPTEMPAllowanceList : PageBase
    {
        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();
        public int doc_tablecount;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EmpAllowance", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EmpAllowanceATC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void Startup()
        {
            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));
        }
        
        private void FillComboBox()
        {
            FIN.BLL.PER.PayrollPeriods_BLL.fn_GetPayrollPeriods(ref ddlPayrollPeriod,true);
         
        }
        
        protected void ddlPayrollPeriod_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlPayrollPeriod.SelectedValue.ToString().Length > 0)
            {
                FIN.BLL.HR.Employee_BLL.GetEmpNameFromPayroll_R(ref ddlEmpName, ddlPayrollPeriod.SelectedValue.ToString());
              
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;

                if (ddlPayrollPeriod.SelectedValue.ToString().Length > 0)
                {
                    htFilterParameter.Add("PAY_PERIOD_ID", ddlPayrollPeriod.SelectedValue.ToString());
                }
                if (ddlEmpName.SelectedValue.ToString().Length > 0)
                {
                    htFilterParameter.Add("EMP_ID", ddlEmpName.SelectedValue.ToString());
                }

                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;
                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;

                ReportData = DBMethod.ExecuteQuery(FIN.DAL.PER.NONRecurringElement_DAL.getEmpAllowanceList());

                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerPath + "?Id=" + Master.ProgramID + "','','" + DbConsts.ReportProperties + "');", true);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EmpAllowanceReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
      
    }
}