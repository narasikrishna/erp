﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.GL;
using FIN.DAL.AP;
using FIN.DAL.AR;
using FIN.BLL;
using FIN.BLL.AP;
using FIN.BLL.AR;
using FIN.BLL.HR;
using FIN.BLL.GL;
using FIN.BLL.SSM;
using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;

namespace FIN.Client.HR_Reports
{
    public partial class RPTYearEndLeaveCarryOverParam : PageBase
    {
        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("YearEndLeaveCarryOverReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                FillComboBox();

                Startup();
                //txtFromDate.Text = DBMethod.ConvertDateToString(DateTime.Now.ToShortDateString());
                //txtFromDate.Enabled = false;

                FIN.BLL.GL.AccountingCalendar_BLL.GetCalAcctYear(ref ddlFinancialYear);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("YearEndLeaveCarryOverReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void Startup()
        {

            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));

            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();

        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                ReportFile = Master.ReportName;

                ErrorCollection.Remove("datdemsg");

                DataTable dtFindate = new DataTable();
                dtFindate = DBMethod.ExecuteQuery(FIN.DAL.HR.LeaveDefinition_DAL.getFindate(ddlFinancialYear.SelectedValue.ToString())).Tables[0];
                string findt = DBMethod.ConvertDateToString(dtFindate.Rows[0]["cal_eff_start_dt"].ToString());
                DataTable dtcaldate;
                if (dtFindate.Rows.Count > 0)
                {
                    //ReportData = FIN.BLL.HR.Payslip_BLL.GetLeaveBalanceReport();
                    dtcaldate = DBMethod.ExecuteQuery(FIN.DAL.HR.LeaveDefinition_DAL.getcaldate(findt)).Tables[0];
                    if (dtcaldate.Rows.Count > 0)
                    {
                        VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;
                        VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
                        string dttxmdate = DBMethod.ConvertDateToString(dtcaldate.Rows[0]["call_eff_end_dt"].ToString());
                        ReportData = FIN.BLL.HR.Payslip_BLL.GetYearEndLeaveCarryOver(dttxmdate.ToString());

                        htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());

                        ReportFormulaParameter = htHeadingParameters;

                        ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerReportPath + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);
                    }
                    else
                    {
                        ErrorCollection.Add("datdemsg", "Please run year end process");
                    }
                }

                    //htFilterParameter.Add("From_Date", txtFromDate.Text);
                //htFilterParameter.Add("Fis_year", ddlFinancialYear.SelectedValue);
                //htFilterParameter.Add("Fin_Year_Text", ddlFinancialYear.SelectedItem.Text);




                else
                {
                    ErrorCollection.Add("datdemsg", "Please run year end process");
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("YearEndLeaveCarryOverReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }
        private void FillComboBox()
        {

            //FIN.BLL.AR.DayBookPayment_BLL.GetGroupName(ref ddlGlobalSegment);

        }
    }
}