﻿using System;
using System.Linq;
using System.Web;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.BLL;
using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;
using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace FIN.Client.HR_Reports
{
    public partial class RPTEmpDetwithFilterParam : PageBase
    {
        public int doc_tablecount;

        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    ChangeLanguage();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("HREmpListEOS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                Startup();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATC_HREmpListEOS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        
        private void Startup()
        {
            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));
        }

        private void ChangeLanguage()
        {
            try
            {
                ErrorCollection.Clear();
                if (VMVServices.Web.Utils.LanguageCode.ToString().Length > 0)
                {
                    Dictionary<string, string> Prop_File_Data;
                    Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));
                    rbl_FilterCont.Items[0].Text = (Prop_File_Data["Department_P"]);
                    rbl_FilterCont.Items[0].Selected = true;
                    rbl_FilterCont.Items[1].Text = (Prop_File_Data["Designation_P"]);
                    rbl_FilterCont.Items[2].Text = (Prop_File_Data["Category_P"]);
                    rbl_FilterCont.Items[3].Text = (Prop_File_Data["Job_P"]);
                    rbl_FilterCont.Items[4].Text = (Prop_File_Data["Position_P"]);
                    rbl_FilterCont.Items[5].Text = (Prop_File_Data["Grade_P"]);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("HREmpListChangeLang", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
           
        }

        protected void btnView_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;

                htFilterParameter.Add("RECORDTYPE", rbl_FilterCont.SelectedValue);
                htFilterParameter.Add("RECORDTYPETEXT", rbl_FilterCont.SelectedItem.Text.ToString());

                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;
                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;

              DBMethod.ExecuteNonQuery(Employee_DAL.get_EmpDetails_4SelectedFilter());

              ReportData = DBMethod.ExecuteQuery(Employee_DAL.get_EmpDetails_4SelectedFilter_data());
                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerReportPath + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("LeaveBalanceReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }

    }
}