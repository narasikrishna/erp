﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;

using FIN.BLL;

using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;
namespace FIN.Client.HR_Reports
{
    public partial class FeedBackRatingPercentageParam : PageBase
    {
        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ExitInterviewReports", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                FillComboBox();
             
                Startup();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ExitInterviewReports", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
       

        private void Startup()
        {

            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));

            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();

        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                ReportFile = Master.ReportName;
                
                if (ddlTemplateName.SelectedValue != string.Empty)
                {

                    htFilterParameter.Add("TMPL_NAME", ddlTemplateName.SelectedItem.Text);
                    htFilterParameter.Add("TMPL_ID", ddlTemplateName.SelectedItem.Value);
                }
                
                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;

                ReportData = DBMethod.ExecuteQuery(FIN.DAL.HR.FeedbackTemplateDAL.getEmployeeFeedbackPercentage());


                SubReportData = new DataSet("subrep");

                DataTable dt_2 = new DataTable("Salary");
                dt_2 = DBMethod.ExecuteQuery(FIN.DAL.HR.FeedbackTemplateDAL.getEmployeeFeedbackPercentage4Paragraph()).Tables[0];
                DataTable dt_3 = dt_2.Copy();
                dt_3.TableName = "SS";
                SubReportData.Tables.Add(dt_3);



                DataTable dt_1 = new DataTable("OptionParam");
                dt_1 = DBMethod.ExecuteQuery(FIN.DAL.HR.FeedbackTemplateDAL.getEmployeeFeedbackPercentage4Option()).Tables[0];
                SubReportData.Tables.Add(dt_1.Copy());




              
                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());

                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerReportPath + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ExitInterviewReports", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }
        private void FillComboBox()
        {
            FIN.BLL.HR.FeedbackTemplateBLL.fn_GetFeedbackTemplateName(ref ddlTemplateName);          
            
        }

      
    }
}