﻿using System;
using System.Linq;
using System.Web;
using FIN.DAL;
using FIN.BLL;
using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;
using System.IO;
using System.Web.UI;
using System.Collections;
using FIN.DAL.HR;


namespace FIN.Client.HR_Reports
{
    public partial class RPTEmpLeaveDtlsParam : PageBase
    {
        public int doc_tablecount;
        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EmpIncrement", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void FillComboBox()
        {
            FIN.BLL.HR.Department_BLL.GetDepartmentNam(ref ddlDepartmentName);
        }
        protected void ddlDepartmentName_SelectedIndexChanged(object sender, EventArgs e)
        {
            FIN.BLL.HR.Employee_BLL.GetEmployeeNameBasedOnDept(ref ddlEmployeeName, ddlDepartmentName.SelectedValue);
        }
        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                Startup();
                FillStartDate();
                FillComboBox();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATC_EmpIncrement", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void FillStartDate()
        {

            string str_finyear = FINSP.GetSPFOR_FiscalYear(DateTime.Now.Date.ToString("dd/MM/yyyy"));
            DataTable dtDate = new DataTable();
            if (str_finyear != string.Empty)
            {
                dtDate = DBMethod.ExecuteQuery(FIN.DAL.GL.InventoryReports_DAL.getCalDate(str_finyear)).Tables[0];
                txtFromDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["cal_eff_start_dt"].ToString());
                txtToDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["call_eff_end_dt"].ToString());
            }


        }
        private void Startup()
        {
            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;

                if (txtFromDate.Text != string.Empty && txtToDate.Text != string.Empty)
                {
                    htFilterParameter.Add("From_Date", txtFromDate.Text);

                    if (txtToDate.Text != string.Empty)
                    {
                        htFilterParameter.Add("To_Date", txtToDate.Text);
                    }

                    if (ddlDepartmentName.SelectedValue != string.Empty)
                    {

                        htFilterParameter.Add("Dept_Name", ddlDepartmentName.SelectedItem.Text);
                        htFilterParameter.Add("Dept_Id", ddlDepartmentName.SelectedItem.Value);
                    }
                    if (ddlEmployeeName.SelectedValue != string.Empty)
                    {
                        htFilterParameter.Add("Emp_id", ddlEmployeeName.SelectedItem.Value);
                        htFilterParameter.Add("Emp_Name", ddlEmployeeName.SelectedItem.Text);
                    }

                    VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;
                    VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;

                    ReportData = DBMethod.ExecuteQuery(FIN.DAL.HR.LeaveApplication_DAL.getEmpStaffAttandance());

                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerReportPath + "?Id=" + Master.ProgramID + "','','" + DbConsts.ReportProperties + "');", true);
                }
                else
                {
                    ErrorCollection.Add("Datevalidation","From date and To Date cannot be empty");
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EmpIncrementReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
    }
}