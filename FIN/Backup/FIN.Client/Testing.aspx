﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Testing.aspx.cs" Inherits="FIN.Client.Testing" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="Scripts/JQuery/jquery-1.11.0.js" type="text/javascript"></script>
    <script src="Scripts/JQuery/jquery-1.11.0.min.js" type="text/javascript"></script>
    <script src="Scripts/JQueryValidatioin/jquery.validationEngine_EN.js" type="text/javascript"></script>
    <script src="Scripts/JQueryValidatioin/jquery.validationEngine.js" type="text/javascript"></script>
    <link href="Scripts/JQueryValidatioin/validationEngine.jquery.css" rel="stylesheet"
        type="text/css" />
    <link href="Styles/main.css" rel="stylesheet" type="text/css" id="MainCss" />
    <link href="Styles/GridStyle.css" rel="stylesheet" type="text/css" />
    <link href="Scripts/DDLChoosen/chosen.css" rel="stylesheet" type="text/css" />
    <script src="Scripts/DDLChoosen/chosen.jquery.js" type="text/javascript"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            $(".ddlStype").chosen();
        });

        //         $(document).ready(function () {
        //             Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(PageLoaded)
        //         });
        //         function PageLoaded(sender, args) {
        //             $(".ddlStype").chosen();
        //         }
    </script>
    <script type="text/javascript">

        $(document).ready(function () {

            $("#form1").validationEngine('validate');
            
        });

        //        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //        prm.add_endRequest(function () {
        //            return fn_SaveValidation();
        //        });

        $("#btnSave").click(function (e) {
           
            return $("#form1").validationEngine('validate')
        })
        function fn_SaveValidation() {

            alert('value' + $("#form1").validationEngine('validate'));

        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:DropDownList ID="ddlData" runat="server" CssClass="validate[required] ">
            <asp:ListItem Value="">---Select--</asp:ListItem>
            <asp:ListItem Value="I">India</asp:ListItem>
            <asp:ListItem Value="S">SouthAfrica</asp:ListItem>
        </asp:DropDownList>
        <asp:Button ID="btnSave" runat="server" Text="Button" 
            OnClientClick="fn_SaveValidation()" onclick="btnSave_Click" />
    </div>
    </form>
</body>
</html>
