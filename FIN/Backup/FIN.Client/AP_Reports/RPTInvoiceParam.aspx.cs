﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.GL;
using FIN.DAL.AP;
using FIN.DAL.AR;
using FIN.BLL;
using FIN.BLL.AP;
using FIN.BLL.AR;
using FIN.BLL.HR;
using FIN.BLL.GL;
using FIN.BLL.SSM;
using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;

namespace FIN.Client.Reports.AP
{
    public partial class RPTInvoiceParam : PageBase
    {
        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("InvoiceReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                Startup();
                FillStartDate();
                FillComboBox();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("InvoiceReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void Startup()
        {

            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));

            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();

        }
        private void FillStartDate()
        {

            string str_finyear = FINSP.GetSPFOR_FiscalYear(DateTime.Now.Date.ToString("dd/MM/yyyy"));
            DataTable dtDate = new DataTable();
            if (str_finyear != string.Empty)
            {
                dtDate = DBMethod.ExecuteQuery(FIN.DAL.GL.InventoryReports_DAL.getCalDate(str_finyear)).Tables[0];
                txtFromDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["cal_eff_start_dt"].ToString());
                txtToDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["call_eff_end_dt"].ToString());
            }


        }
        private void AmountParam()
        {
            try
            {
                ErrorCollection.Clear();
                if (CommonUtils.ConvertStringToDecimal(txtFromAmount.Text) > 0)
                {
                    htFilterParameter.Add("FromAmount", txtFromAmount.Text);
                    htFilterParameter.Add("From_Amt", DBMethod.GetAmtDecimalCommaSeparationValue(txtFromAmount.Text));

                    if (CommonUtils.ConvertStringToDecimal(txtToAmount.Text) > 0)
                    {
                        htFilterParameter.Add("ToAmount", txtToAmount.Text);
                        htFilterParameter.Add("To_Amt", DBMethod.GetAmtDecimalCommaSeparationValue(txtToAmount.Text));
                    }
                }

                //ErrorCollection = CommonUtils.Validate2Amounts(CommonUtils.ConvertStringToDecimal(txtFromAmount.Text), CommonUtils.ConvertStringToDecimal(txtToAmount.Text));

            }
            catch (Exception ex)
            {
               // ErrorCollection.Add("amtErr", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void ParamValidation()
        {

            ErrorCollection.Clear();
            ErrorCollection = CommonUtils.ValidateDateRange(txtFromDate.Text, txtToDate.Text);
            if (ErrorCollection.Count > 0)
            {
                return;
            }

            ErrorCollection = CommonUtils.Validate2AmountsExp(txtFromAmount.Text, txtToAmount.Text);
            if (ErrorCollection.Count > 0)
            {
                return;
            }


        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;
                ParamValidation();
                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                if (ddlInvoiceNumber.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("inv_id", ddlInvoiceNumber.SelectedValue);
                }

                if (ddlSupplierName.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add(FINColumnConstants.VENDOR_ID, ddlSupplierName.SelectedItem.Value);
                    htFilterParameter.Add(FINColumnConstants.VENDOR_NAME, ddlSupplierName.SelectedItem.Text);
                }

                ErrorCollection.Clear();
                if (CommonUtils.ConvertStringToDecimal(txtFromAmount.Text) > 0)
                {
                    htFilterParameter.Add("FromAmount", txtFromAmount.Text);
                    htFilterParameter.Add("From_Amt", txtFromAmount.Text);

                    if (CommonUtils.ConvertStringToDecimal(txtToAmount.Text) > 0)
                    {
                        htFilterParameter.Add("ToAmount", txtToAmount.Text);
                        htFilterParameter.Add("To_Amt", txtToAmount.Text);
                    }
                }
                //if (txtFromAmount.Text.Length > 0 && txtToAmount.Text.Length > 0)
                //{

                //    ErrorCollection = CommonUtils.Validate2Amounts(CommonUtils.ConvertStringToDecimal(txtFromAmount.Text), CommonUtils.ConvertStringToDecimal(txtToAmount.Text));
                //}
                //if (ErrorCollection.Count > 0)
                //{
                //    return;
                //}

                if (txtFromDate.Text != string.Empty)
                {
                    htFilterParameter.Add("From_Date", txtFromDate.Text);

                    if (txtToDate.Text != string.Empty)
                    {
                        htFilterParameter.Add("To_Date", txtToDate.Text);
                    }
                }

                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;


                ReportData = FIN.BLL.AP.APAginAnalysis_BLL.GetInvoiceListReportData();

                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());

                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerPath + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);
            }

            catch (Exception ex)
            {
                ErrorCollection.Add("InvoiceReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }
        private void FillComboBox()
        {
            //FIN.BLL.AP.Invoice_BLL.GetCustomerName(ref ddlCustomerName);
            //FIN.BLL.AP.Invoice_BLL.fn_getInvoiceNumber(ref ddlInvoiceNumber, true);
            Supplier_BLL.GetSupplierNames(ref ddlSupplierName, true);
            FIN.BLL.AP.Invoice_BLL.fn_getInvoiceNumber(ref ddlInvoiceNumber, true);
            Lookup_BLL.GetLookUpValues(ref ddlInvoiceType, "INV_TY");
        }

        protected void ddlSupplierName_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSupplierName.SelectedValue.ToString().Length > 0)
            {
                FIN.BLL.AP.Invoice_BLL.fn_getInvoiceNumberForSupplier(ref ddlInvoiceNumber, ddlSupplierName.SelectedValue.ToString());
            }
            else
            {
                FIN.BLL.AP.Invoice_BLL.fn_getInvoiceNumber(ref ddlInvoiceNumber, true);
            }
        }

        

    }
}