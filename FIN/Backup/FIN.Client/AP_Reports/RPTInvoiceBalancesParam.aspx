﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="RPTInvoiceBalancesParam.aspx.cs" Inherits="FIN.Client.AP_Reports.RPTInvoiceBalancesParam" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 700px" id="div1">
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="lblRequisitionType">
                Supplier Name
            </div>
            <div class="divtxtBox LNOrient" style="width: 470px">
                <asp:DropDownList ID="ddlSupplierName" runat="server" TabIndex="1"  AutoPostBack="true"
                    CssClass="ddlStype" 
                    onselectedindexchanged="ddlSupplierName_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="Div2">
                Invoice Type
            </div>
            <div class="divtxtBox LNOrient" style="width: 470px">
                <asp:DropDownList ID="ddlInvoiceType" runat="server" TabIndex="2" CssClass="ddlStype"
                    Width="150px">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="Div3">
                From Invoice Number
            </div>
            <div class="divtxtBox LNOrient" style="width: 155px">
                <asp:DropDownList ID="ddlFromInvoiceNum" runat="server" AutoPostBack="true" TabIndex="3" CssClass="ddlStype">
                </asp:DropDownList>
            </div>
            <div class="colspace LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 145px" id="Div8">
               To Invoice Number
            </div>
            <div class="divtxtBox LNOrient" style=" width: 155px">
                <asp:DropDownList ID="ddlToInvoiceNum" runat="server" AutoPostBack="true" TabIndex="4" CssClass="ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px;" id="Div4">
                From Balance Amount
            </div>
            <div class="divtxtBox LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtFromBalAmt" runat="server" TabIndex="5" MaxLength="15" CssClass="txtBox_N"
                    Width="150px" ></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers,Custom"
                    ValidChars=".," TargetControlID="txtFromBalAmt" />
            </div>
            <div class="colspace LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style=" width: 150px;" id="Div7">
                To Balance Amount
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:TextBox ID="txtToBalAmt" runat="server" TabIndex="6" MaxLength="15" CssClass="txtBox_N"
                    Width="150px" ></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Numbers,Custom"
                    ValidChars=".," TargetControlID="txtToBalAmt" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style=" width: 150px;" id="Div5">
                From Paid Amount
            </div>
            <div class="divtxtBox LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtFromPayAmt" runat="server" TabIndex="7" MaxLength="15"
                    CssClass="txtBox_N" Width="150px" ></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Numbers,Custom"
                    ValidChars=".," TargetControlID="txtFromPayAmt" />
            </div>
            <div class="colspace LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style=" width: 150px;" id="Div6">
                To Paid Amount
            </div>
            <div class="divtxtBox LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtToPayAmt" runat="server" TabIndex="8" MaxLength="15"
                    CssClass="txtBox_N" Width="150px" ></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers,Custom"
                    ValidChars=".," TargetControlID="txtToPayAmt" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style=" width: 150px" id="lblFromDate">
                From Date
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:TextBox ID="txtFromDate" runat="server" TabIndex="9" CssClass="txtBox"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calExtender1" TargetControlID="txtFromDate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
            <div class="colspace  LNOrient">
                &nbsp</div>
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblToDate">
                To Date
            </div>
            <div class="divtxtBox LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtToDate" runat="server" TabIndex="10" CssClass="txtBox"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtToDate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divFormcontainer" style="width: 370px" id="divMainContainer">
            <div class="divRowContainer divReportAction">
                <table class="ReportTable">
                    <tr>
                        <td>
                            <asp:ImageButton ID="btnSave" runat="server" ImageUrl="../Images/show-report-icon.png"
                                Width="35px" Height="25px" OnClick="btnSave_Click" />
                            <%--<asp:Button ID="btnSave" runat="server" Text="Show Report" CssClass="btn" OnClick="btnSave_Click" />--%>
                        </td>
                        <td>
                            <%-- <asp:Button ID="btnPrint" runat="server" Text="Print" CssClass="btn" />--%>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
  <script src="../LanguageScript/AP/APChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
