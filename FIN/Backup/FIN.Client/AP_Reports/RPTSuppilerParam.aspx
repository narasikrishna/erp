﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="RPTSuppilerParam.aspx.cs" Inherits="FIN.Client.Reports.AP.RPTSuppilerParam" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 900px" id="divMainContainer">
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="Div1">
                From Supplier
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 250px">
                <asp:DropDownList ID="ddlFromSupplier" runat="server" TabIndex="1" CssClass="ddlStype"
                    Width="250px">
                </asp:DropDownList>
            </div>
            <div class="lblBox  LNOrient" style=" width: 150px" id="Div2">
                To Supplier
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 250px">
                <asp:DropDownList ID="ddlToSupplier" runat="server" TabIndex="2" CssClass="ddlStype"
                    Width="250px">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblSupplierCountry">
                Supplier Country
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:DropDownList ID="ddlSupplierCountry" runat="server" TabIndex="3" CssClass="ddlStype"
                    AutoPostBack="true" Width="150px" OnSelectedIndexChanged="ddlSupplierCountry_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div class="lblBox  LNOrient" style=" width: 100px" id="Div4">
                State
            </div>
            <div class="divtxtBox  LNOrient" >
                <asp:DropDownList ID="ddlState" runat="server" TabIndex="4" CssClass="ddlStype" AutoPostBack="true"
                    Width="150px" OnSelectedIndexChanged="ddlState_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div class="lblBox  LNOrient" style=" width: 85px" id="Div3">
                City
            </div>
            <div class="divtxtBox  LNOrient" >
                <asp:DropDownList ID="ddlCity" runat="server" TabIndex="5" CssClass="ddlStype" AutoPostBack="true"
                    Width="150px">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="Div5">
                Created By
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 200px">
                <asp:DropDownList ID="ddlCreatedBy" runat="server" TabIndex="6" CssClass="ddlStype"
                    Width="200px">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox  LNOrient" style=" width: 150px" id="Div6">
                Modified By
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 200px">
                <asp:DropDownList ID="ddlModifiedBy" runat="server" TabIndex="7" CssClass="ddlStype"
                    Width="200px">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblDate">
                From Created Date
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 200px">
                <asp:TextBox ID="txtFromCreatedDate" runat="server" TabIndex="8" CssClass="  txtBox"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calExtender1" TargetControlID="txtFromCreatedDate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox  LNOrient" style=" width: 150px" id="Div7">
                To Created Date
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 200px">
                <asp:TextBox ID="txtToCreatedDate" runat="server" TabIndex="9" CssClass="txtBox"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtToCreatedDate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="Div8">
                From Modified Date
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 200px">
                <asp:TextBox ID="txtFromModifiedDate" runat="server" TabIndex="10" CssClass=" txtBox"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender2" TargetControlID="txtFromModifiedDate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox  LNOrient" style=" width: 150px" id="Div9">
                To Modified Date
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 200px">
                <asp:TextBox ID="txtToModifiedDate" runat="server" TabIndex="11" CssClass=" txtBox"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender3" TargetControlID="txtToModifiedDate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer divReportAction">
            <table class="ReportTable">
                <tr>
                    <td style="width: 150px">
                        <asp:ImageButton ID="btnSave" runat="server" ImageUrl="../Images/show-report-icon.png"
                            Width="35px" Height="25px" OnClick="btnSave_Click" />
                        <%--<asp:Button ID="btnSave" runat="server" Text="Show Report" CssClass="btn" OnClick="btnSave_Click" />--%>
                    </td>
                    <td>
                        <%-- <asp:Button ID="btnPrint" runat="server" Text="Print" CssClass="btn" />--%>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/AP/APChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
