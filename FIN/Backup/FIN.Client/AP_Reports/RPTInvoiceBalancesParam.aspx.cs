﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.GL;
using FIN.DAL.AP;
using FIN.DAL.AR;
using FIN.BLL;
using FIN.BLL.AP;
using FIN.BLL.AR;
using FIN.BLL.HR;
using FIN.BLL.GL;
using FIN.BLL.SSM;
using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;

namespace FIN.Client.AP_Reports
{
    public partial class RPTInvoiceBalancesParam : PageBase
    {
        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("InvoiceBalancesReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                FillComboBox();
                FillStartDate();
                Startup();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("InvoiceBalancesReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void Startup()
        {

            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));

            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();

        }

        private void FillStartDate()
        {

            string str_finyear = FINSP.GetSPFOR_FiscalYear(DateTime.Now.Date.ToString("dd/MM/yyyy"));
            DataTable dtDate = new DataTable();
            if (str_finyear != string.Empty)
            {
                dtDate = DBMethod.ExecuteQuery(FIN.DAL.GL.InventoryReports_DAL.getCalDate(str_finyear)).Tables[0];
                txtFromDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["cal_eff_start_dt"].ToString());
                txtToDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["call_eff_end_dt"].ToString());
            }


        }
        private void ParamValidation()
        {

            ErrorCollection.Clear();
            ErrorCollection = CommonUtils.ValidateDateRange(txtFromDate.Text, txtToDate.Text);
            if (ErrorCollection.Count > 0)
            {
                return;
            }

            ErrorCollection = CommonUtils.Validate2AmountsExp(txtFromBalAmt.Text, txtToBalAmt.Text);
            if (ErrorCollection.Count > 0)
            {
                return;
            }

            ErrorCollection = CommonUtils.Validate2AmountsExp(txtFromPayAmt.Text, txtToPayAmt.Text);
            if (ErrorCollection.Count > 0)
            {
                return;
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;
                ParamValidation();
                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                if (ddlSupplierName.SelectedValue != string.Empty)
                {

                    htFilterParameter.Add(FINColumnConstants.VENDOR_ID, ddlSupplierName.SelectedItem.Value);
                    htFilterParameter.Add("VENDOR_NAME", ddlSupplierName.SelectedItem.Text);
                }
                if (txtFromDate.Text != string.Empty)
                {
                    htFilterParameter.Add("From_Date", txtFromDate.Text);
                }
                if (txtToDate.Text != string.Empty)
                {
                    htFilterParameter.Add("To_Date", txtToDate.Text);
                }
                if (ddlInvoiceType.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("INV_TYPE", ddlInvoiceType.SelectedItem.Text);
                }
                if ( ddlFromInvoiceNum.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("FROM_INV_NUM", ddlFromInvoiceNum.SelectedItem.Text);
                }
                if (ddlToInvoiceNum.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("TO_INV_NUM", ddlToInvoiceNum.SelectedItem.Text);
                }
                if (txtFromPayAmt.Text != string.Empty)
                {
                    htFilterParameter.Add("FROM_PAY_AMT", txtFromPayAmt.Text);
                }
                if (txtToPayAmt.Text != string.Empty)
                {
                    htFilterParameter.Add("TO_PAY_AMT", txtToPayAmt.Text);
                }
                if (txtFromBalAmt.Text != string.Empty)
                {
                    htFilterParameter.Add("FROM_BAL_AMT", txtFromBalAmt.Text);
                }
                if (txtToBalAmt.Text != string.Empty)
                {
                    htFilterParameter.Add("TO_BAL_AMT", txtToBalAmt.Text);
                }

                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;

                ReportData = FIN.BLL.AP.InvoiceRegister_BLL.GetInvoiceBalancesReport();

                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());

                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerPath + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("InvoiceBalancesReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }
        private void FillComboBox()
        {
            Supplier_BLL.GetSupplierNames(ref ddlSupplierName);
            Lookup_BLL.GetLookUpValue(ref ddlInvoiceType, "INV_TY");
            FIN.BLL.CA.Bank_BLL.fn_getInvoiceNumbers(ref ddlFromInvoiceNum);
            FIN.BLL.CA.Bank_BLL.fn_getInvoiceNumbers(ref ddlToInvoiceNum);
            //FIN.BLL.AR.DayBookPayment_BLL.GetGroupName(ref ddlGlobalSegment);

        }

        protected void ddlSupplierName_SelectedIndexChanged(object sender, EventArgs e)
        {
            FIN.BLL.CA.Bank_BLL.fn_getInvoiceNumbersWithVendor(ref ddlFromInvoiceNum,ddlSupplierName.SelectedValue);
            FIN.BLL.CA.Bank_BLL.fn_getInvoiceNumbersWithVendor(ref ddlToInvoiceNum, ddlSupplierName.SelectedValue);
        }
    }
}