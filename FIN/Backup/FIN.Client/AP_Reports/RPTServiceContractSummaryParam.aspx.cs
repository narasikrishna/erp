﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.GL;
using FIN.DAL.AP;
using FIN.DAL.AR;
using FIN.BLL;
using FIN.BLL.AP;
using FIN.BLL.AR;
using FIN.BLL.HR;
using FIN.BLL.GL;
using FIN.BLL.PER;
using FIN.BLL.SSM;
using FIN.BLL.LOAN;
using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;

namespace FIN.Client.AP_Reports
{
    public partial class RPTServiceContractSummaryParam : PageBase
    {
        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();
        DataTable dtEmployeeDtls = new DataTable();
        DataTable dtEarningDeduc = new DataTable();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EmpDetailsReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                FillComboBox();
                FillStartDate();
                Startup();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AbsentReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void Startup()
        {

            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));

            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();

        }
        private void FillStartDate()
        {

            //string str_finyear = FINSP.GetSPFOR_FiscalYear(DateTime.Now.Date.ToString("dd/MM/yyyy"));
            //DataTable dtDate = new DataTable();
            //if (str_finyear != string.Empty)
            //{
            //    dtDate = DBMethod.ExecuteQuery(FIN.DAL.GL.InventoryReports_DAL.getCalDate(str_finyear)).Tables[0];
            //    txtFromDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["cal_eff_start_dt"].ToString());
            //    txtToDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["call_eff_end_dt"].ToString());
            //}


        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dtEmployeeDtls = new DataTable();
                DataTable dtEarningDed = new DataTable();

                ErrorCollection.Clear();
                ReportFile = Master.ReportName;


                if (ddlContractNum.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("serv_contr_id", ddlContractNum.SelectedItem.Value);
                    htFilterParameter.Add("serv_contr", ddlContractNum.SelectedItem.Text);
                }

                if (ddlSupplierName.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("VENDOR_ID", ddlSupplierName.SelectedValue);
                    htFilterParameter.Add("VENDOR_NAME", ddlSupplierName.SelectedItem.Text);
                }
                if (ddlSupplierSite.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("SITE_ID", ddlSupplierSite.SelectedItem.Value);
                    htFilterParameter.Add("SITE_NAME", ddlSupplierSite.SelectedItem.Text);
                }
                if (ddlContractType.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("CONTRACT_TYPE", ddlContractType.SelectedItem.Value);
                    htFilterParameter.Add("CONTRACT_TYPE_N", ddlContractType.SelectedItem.Text);
                }
                if (ddlPaymentType.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("PAYMENT_TYPE", ddlPaymentType.SelectedItem.Value);
                    htFilterParameter.Add("PAYMENT_TYPE_N", ddlPaymentType.SelectedItem.Text);
                }


                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;

                //ReportData = FIN.BLL.HR.Payslip_BLL.GetReportData();
                //dsData = DBMethod.ExecuteQuery(FIN.DAL.HR.LeaveDefinition_DAL.GetAbsentLeaveDetails(txtFromDate.Text.ToString(), txtToDate.Text.ToString()));
                ReportData = FIN.BLL.AP.ServiceContract_BLL.GetServiceContractSummaryReportData();




               




                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());

                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerReportPath + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AbsentReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }
        private void FillComboBox()
        {

            FIN.BLL.AP.Item_BLL.GetItemNames(ref ddlContractType, "Service");
            Lookup_BLL.GetLookUpValue(ref ddlPaymentType, "PAY_TYPE");
            Supplier_BLL.GetSupplierNamesR(ref ddlSupplierName);
            ServiceContract_BLL.GetContractNumber(ref ddlContractNum);

        }

        protected void ddlSupplierName_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindSupplierSite();

        }


        private void BindSupplierSite()
        {
            try
            {
                ErrorCollection.Clear();

                FIN.BLL.AP.SupplierBranch_BLL.fn_getSupplierSite4SupplierName(ref  ddlSupplierSite, ddlSupplierName.SelectedValue);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }



    }
}