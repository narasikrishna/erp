﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="RPTInvoiceParam.aspx.cs" Inherits="FIN.Client.Reports.AP.RPTInvoiceParam" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 700px" id="divMainContainer">
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="lblRequisitionType">
                Supplier Name
            </div>
            <div class="divtxtBox LNOrient" style="width: 470px">
                <asp:DropDownList ID="ddlSupplierName" runat="server" TabIndex="1" CssClass="ddlStype"
                    AutoPostBack="true" OnSelectedIndexChanged="ddlSupplierName_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style=" width: 150px" id="lblInvoiceNumber">
                Invoice Number
            </div>
            <div class="divtxtBox LNOrient" style=" width: 470px">
                <asp:DropDownList ID="ddlInvoiceNumber" runat="server" TabIndex="2" CssClass="ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" style="display:none">
            <div class="lblBox LNOrient" style=" width: 150px" id="lblInvoiceType">
                Invoice Type
            </div>
            <div class="divtxtBox LNOrient" style=" width: 470px">
                <asp:DropDownList ID="ddlInvoiceType" runat="server" TabIndex="3" 
                    CssClass=" ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style=" width: 150px" id="Div2">
                From Amount
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:TextBox ID="txtFromAmount" runat="server" TabIndex="4" CssClass="txtBox_N" 
                    MaxLength="13"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers,Custom"
                    ValidChars=".,-" TargetControlID="txtFromAmount" />
            </div>
            <div class="lblBox LNOrient" style="width: 150px" id="Div3">
                To Amount
            </div>
            <div class="divtxtBox LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtToAmount" runat="server" TabIndex="5" CssClass=" txtBox_N" 
                    MaxLength="13"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers,Custom"
                    ValidChars=".,-" TargetControlID="txtToAmount" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style=" width: 150px" id="lblFromDate">
                From Date
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:TextBox ID="txtFromDate" runat="server" TabIndex="6" 
                    CssClass="txtBox"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calExtender1" TargetControlID="txtFromDate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
            <div class="lblBox LNOrient" style="width: 150px" id="lblToDate">
                To Date
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:TextBox ID="txtToDate" runat="server" TabIndex="7" 
                    CssClass="txtBox"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtToDate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer divReportAction">
            <table class="ReportTable">
                <tr>
                    <td>
                        <asp:ImageButton ID="btnSave" runat="server" ImageUrl="../Images/show-report-icon.png"
                            Width="35px" Height="25px" OnClick="btnSave_Click" TabIndex="8" />
                        <%--<asp:Button ID="btnSave" runat="server" Text="Show Report" CssClass="btn" OnClick="btnSave_Click" />--%>
                    </td>
                    <td>
                        <%--<asp:Button ID="btnPrint" runat="server" Text="Print" CssClass="btn" />--%>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
     <script src="../LanguageScript/AP/APChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

    </script>
</asp:Content>
