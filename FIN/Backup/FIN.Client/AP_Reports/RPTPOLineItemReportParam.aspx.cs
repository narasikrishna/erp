﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.GL;
using FIN.DAL.AP;
using FIN.DAL.AR;
using FIN.BLL;
using FIN.BLL.AP;
using FIN.BLL.AR;
using FIN.BLL.HR;
using FIN.BLL.GL;
using FIN.BLL.SSM;
using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;


namespace FIN.Client.Reports.AP
{
    public partial class RPTPOLineItemReportParam : PageBase
    {
        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POLineItemReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                FillComboBox();
                FillStartDate();
                Startup();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POLineItemReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void Startup()
        {

            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));

            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();

        }
        private void FillStartDate()
        {
            string str_finyear = FINSP.GetSPFOR_FiscalYear(DateTime.Now.Date.ToString("dd/MM/yyyy"));
            DataTable dtDate = new DataTable();
            if (str_finyear != string.Empty)
            {
                dtDate = DBMethod.ExecuteQuery(FIN.DAL.GL.InventoryReports_DAL.getCalDate(str_finyear)).Tables[0];
                txtFromDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["cal_eff_start_dt"].ToString());
                txtToDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["call_eff_end_dt"].ToString());
            }
        }
        private void ParamValidation()
        {
            ErrorCollection.Clear();

            ErrorCollection = CommonUtils.ValidateDateRange(txtFromDate.Text, txtToDate.Text);
            if (ErrorCollection.Count > 0)
            {
                return;
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;

                ParamValidation();
                if (ErrorCollection.Count > 0)
                {
                    return;
                }


                if (ddlSupplierName.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add(FINColumnConstants.VENDOR_ID, ddlSupplierName.SelectedItem.Value);
                }
                if (ddlItemName.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add(FINColumnConstants.ITEM_ID, ddlItemName.SelectedItem.Value);
                }
                if (ddlSupplierName.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add(FINColumnConstants.VENDOR_NAME, ddlSupplierName.SelectedItem.Text);
                }
                if (ddlPOLineNumber.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add(FINColumnConstants.PO_LINE_NUM, ddlPOLineNumber.SelectedItem.Text);
                }
                if (ddlPONumber.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add(FINColumnConstants.PO_HEADER_ID, ddlPONumber.SelectedValue);
                }
                if (ddlPONumber.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add(FINColumnConstants.PO_NUM, ddlPONumber.SelectedItem.Text);
                }
                //if (CommonUtils.ConvertStringToDecimal(txtFromCost.Text) > 0)
                //{
                //    htFilterParameter.Add("FromAmount", txtFromCost.Text);
                //    htFilterParameter.Add("From_Amt", DBMethod.GetAmtDecimalCommaSeparationValue(txtFromCost.Text));
                //}
                //if (CommonUtils.ConvertStringToDecimal(txtToCost.Text) > 0)
                //{
                //    htFilterParameter.Add("ToAmount", txtToCost.Text);
                //    htFilterParameter.Add("To_Amt", DBMethod.GetAmtDecimalCommaSeparationValue(txtToCost.Text));
                //}
                if (txtFromDate.Text != string.Empty)
                {
                    htFilterParameter.Add("From_Date", txtFromDate.Text);


                    if (txtToDate.Text != string.Empty)
                    {
                        htFilterParameter.Add("To_Date", txtToDate.Text);
                    }
                    if (DBMethod.ConvertStringToDate(txtFromDate.Text) > DBMethod.ConvertStringToDate(txtToDate.Text))
                    {
                        ErrorCollection.Add("fromdatetodate", "From date should be less than the To date");
                        return;
                    }
                }

                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;


                ReportData = FIN.BLL.AP.PurchaseOrder_BLL.GetPOLineNumberReportData();

                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());

                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerPath + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POLineItemReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }
        private void FillComboBox()
        {

            DataTable dtDropDownData = new DataTable();
            dtDropDownData = DBMethod.ExecuteQuery(PurchaseOrder_DAL.getPONumber()).Tables[0];
            CommonUtils.LoadDropDownList(ddlPONumber, "PO_NUMBER", "PO_HEADER_ID", dtDropDownData, false, true);

            Supplier_BLL.GetSupplier(ref ddlSupplierName);
            PurchaseOrder_BLL.GetPOLineNumber(ref ddlPOLineNumber);
            Item_BLL.GetItemNam(ref ddlItemName);
        }

        protected void ddlSupplierName_SelectedIndexChanged(object sender, EventArgs e)
        {

        }


    }
}