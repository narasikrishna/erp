﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.GL;
using FIN.DAL.AP;
using FIN.DAL.AR;
using FIN.BLL;
using FIN.BLL.AP;
using FIN.BLL.AR;
using FIN.BLL.HR;
using FIN.BLL.GL;
using FIN.BLL.SSM;
using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;

namespace FIN.Client.AP_Reports
{
    public partial class RPTChequesParam : PageBase
    {
        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("APAgingAnalysisReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                Startup();
                FillComboBox();
                FillStartDate();


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("APAgingAnalysisReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void Startup()
        {

            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));

            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();

        }
        private void FillStartDate()
        {

            string str_finyear = FINSP.GetSPFOR_FiscalYear(DateTime.Now.Date.ToString("dd/MM/yyyy"));
            DataTable dtDate = new DataTable();
            if (str_finyear != string.Empty)
            {
                dtDate = DBMethod.ExecuteQuery(FIN.DAL.GL.InventoryReports_DAL.getCalDate(str_finyear)).Tables[0];
                txtFromDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["cal_eff_start_dt"].ToString());
                txtToDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["call_eff_end_dt"].ToString());
            }


        }
        private void ParamValidation()
        {

            ErrorCollection.Clear();
            ErrorCollection = CommonUtils.ValidateDateRange(txtFromDate.Text, txtToDate.Text);
            if (ErrorCollection.Count > 0)
            {
                return;
            }

            ErrorCollection = CommonUtils.Validate2AmountsExp(txtFromAmount.Text, txtToAmount.Text);
            if (ErrorCollection.Count > 0)
            {
                return;
            }
          

        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;
                ParamValidation();
                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                // AmountParam();
                ErrorCollection.Clear();
                if (txtFromAmount.Text != string.Empty)
                {
                    htFilterParameter.Add("From_Amt", txtFromAmount.Text);
                }
                if (txtToAmount.Text != string.Empty)
                {
                    htFilterParameter.Add("To_Amt", txtToAmount.Text);
                }


                //if (txtFromAmount.Text.Length > 0 && txtToAmount.Text.Length > 0)
                //{
                //    ErrorCollection = CommonUtils.Validate2Amounts(CommonUtils.ConvertStringToDecimal(txtFromAmount.Text), CommonUtils.ConvertStringToDecimal(txtToAmount.Text));
                //}
                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                // chequeNumber();

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                //if (ddlSupplierName.SelectedValue != string.Empty)
                //{
                //    htFilterParameter.Add("SUPPLIER_NAME", ddlSupplierName.SelectedValue);
                //}
                if (txtFromDate.Text != string.Empty)
                {
                    htFilterParameter.Add("From_Date", txtFromDate.Text);
                }

                if (txtToDate.Text != string.Empty)
                {
                    htFilterParameter.Add("To_Date", txtToDate.Text);
                }



                if (txtFromCheqNum.Text != string.Empty)
                {
                    htFilterParameter.Add("FROM_CHEQ", txtFromCheqNum.Text);
                }
                if (txtToCheqNum.Text != string.Empty)
                {
                    htFilterParameter.Add("TO_CHEQ", txtToCheqNum.Text);
                }
                if (ddlSupplierName.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("SUPPLIER_NAME", ddlSupplierName.SelectedItem.Text);
                    htFilterParameter.Add("SUPPLIER_ID", ddlSupplierName.SelectedValue.ToString());
                }
                if (ddlBankName.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("BANK_NAME", ddlBankName.SelectedItem.Text);
                }
                if (ddlBankAccountNum.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("BANK_ACCOUNT_NUMBER", ddlBankAccountNum.SelectedItem.Text);
                    htFilterParameter.Add("ACCOUNT_ID", ddlBankAccountNum.SelectedValue.ToString());
                }

                if (ddlCurrency.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("CURRENCY", ddlCurrency.SelectedItem.Text);
                }
                if (ddlPaymentType.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("PAYMENT_TYPE", ddlPaymentType.SelectedItem.Text);
                }

                if (ddlPaymentNumber.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("PAYMENT_NUMBER", ddlPaymentNumber.SelectedItem.Text);
                }
                //if (ddlCurrency.SelectedValue != string.Empty)
                //{
                //    htFilterParameter.Add("CURRENCY_ID", ddlCurrency.SelectedItem.Text);
                //}

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    htFilterParameter.Add("PAY_ID", Master.StrRecordId.ToString());
                }

                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;

                ReportData = FIN.BLL.AP.APAginAnalysis_BLL.GetChequesReportData();

                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());

                ReportFormulaParameter = htHeadingParameters;
                if (ErrorCollection.Count == 0)
                {

                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerPath + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("APAgingAnalysisReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }
        private void FillComboBox()
        {

            //FIN.BLL.AR.SuplrStmtAccnt_BLL.GetSupplierName(ref ddlSupplierName);
            Supplier_BLL.GetSupplierNams(ref ddlSupplierName);
            Lookup_BLL.GetLookUpValues(ref ddlPaymentType, "PAYMENT_MODE");
            FIN.BLL.CA.Bank_BLL.fn_getBankName(ref ddlBankName);

            FIN.BLL.CA.Bank_BLL.fn_getCurrency(ref ddlCurrency);



        }
        protected void ddlBankName_SelectedIndexChanged(object sender, EventArgs e)
        {
            FIN.BLL.CA.BankBranch_BLL.fn_getBranchName(ref ddlBankBranch, ddlBankName.SelectedValue.ToString());
        }
        protected void ddlBankBranch_SelectedIndexChanged(object sender, EventArgs e)
        {
            FIN.BLL.CA.BankAccounts_BLL.fn_getBankAccount(ref ddlBankAccountNum, ddlBankName.SelectedValue, ddlBankBranch.SelectedValue);
        }

        private void AmountParam()
        {
            try
            {
                ErrorCollection.Clear();
                if (CommonUtils.ConvertStringToDecimal(txtFromAmount.Text) > 0)
                {
                    htFilterParameter.Add("From_Amt", txtFromAmount.Text);

                    if (CommonUtils.ConvertStringToDecimal(txtToAmount.Text) > 0)
                    {
                        htFilterParameter.Add("To_Amt", txtToAmount.Text);
                    }
                }

                ErrorCollection = CommonUtils.Validate2Amounts(CommonUtils.ConvertStringToDecimal(txtFromAmount.Text), CommonUtils.ConvertStringToDecimal(txtToAmount.Text));
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("amtErr", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void chequeNumber()
        {
            try
            {
                ErrorCollection.Clear();
                if (CommonUtils.ConvertStringToDecimal(txtFromCheqNum.Text) > 0)
                {
                    htFilterParameter.Add("FROM_CHEQ_NUM", txtFromCheqNum.Text);

                    if (CommonUtils.ConvertStringToDecimal(txtToCheqNum.Text) > 0)
                    {
                        htFilterParameter.Add("TO_CHEQ_NUM", txtToCheqNum.Text);
                    }
                }
                if (txtFromAmount.Text != string.Empty && txtFromCheqNum.Text != string.Empty)
                {
                    ErrorCollection = CommonUtils.Validate2ChequeNumber(CommonUtils.ConvertStringToDecimal(txtFromCheqNum.Text), CommonUtils.ConvertStringToDecimal(txtToCheqNum.Text));
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("amtErr", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void ddlSupplierName_SelectedIndexChanged(object sender, EventArgs e)
        {
            FIN.BLL.CA.Bank_BLL.fn_getPaymentNumber(ref ddlPaymentNumber, ddlSupplierName.SelectedValue.ToString());
        }


    }
}