﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="RPTPOLineItemReportParam.aspx.cs" Inherits="FIN.Client.Reports.AP.RPTPOLineItemReportParam" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 750px" id="divMainContainer">
        <div class="divRowContainer" style="display: none;">
            <div class="lblBox LNOrient" style=" width: 200px" id="lblRequisitionType">
                Supplier Name
            </div>
            <div class="divtxtBox LNOrient" style=" width: 150px">
                <asp:DropDownList ID="ddlSupplierName" runat="server" TabIndex="1" CssClass="ddlStype"
                    Width="150px" OnSelectedIndexChanged="ddlSupplierName_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 100px" id="lblPONumber">
                Purchase Order Number
            </div>
            <div class="divtxtBox LNOrient" style="width: 250px">
                <asp:DropDownList ID="ddlPONumber" runat="server"  TabIndex="2" CssClass="ddlStype">
                </asp:DropDownList>
            </div>
            <div class="colspace LNOrient" style="width: 20px; ">
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 100px" id="Div1">
                Item Name
            </div>
            <div class="divtxtBox LNOrient" style=" width: 250px">
                <asp:DropDownList ID="ddlItemName" runat="server" TabIndex="3" CssClass="ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <%-- <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 150px;" id="Div5">
                From Cost
            </div>
            <div class="divtxtBox" style="float: left; width: 200px">
                <asp:TextBox ID="txtFromCost" runat="server" TabIndex="5" MaxLength="13" CssClass="EntryFont txtBox_N"
                    FilterType="Numbers,Custom" ValidChars=".-"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Numbers,Custom"
                    ValidChars=".-" TargetControlID="txtFromCost" />
            </div>
            <div class="colspace" style="width: 20px; float: left">
            </div>
            <div class="lblBox" style="float: left; width: 150px;" id="Div6">
                To Cost
            </div>
            <div class="divtxtBox" style="float: left; width: 200px">
                <asp:TextBox ID="txtToCost" runat="server" TabIndex="6" MaxLength="13" CssClass="EntryFont txtBox_N"
                    FilterType="Numbers,Custom" ValidChars=".-"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers,Custom"
                    ValidChars=".-" TargetControlID="txtToCost" />
            </div>
        </div>--%>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 100px" id="lblDate">
                From Date
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtFromDate" runat="server" TabIndex="4" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]]  RequiredField txtBox"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calExtender1" TargetControlID="txtFromDate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style=" width: 100px" id="Div2">
                To Date
            </div>
            <div class="divtxtBox LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtToDate" runat="server" TabIndex="5" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]]  RequiredField txtBox"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtToDate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
        </div>
        <div class="divRowContainer" style="display: none;">
            <div class="lblBox LNOrient" style=" width: 200px" id="lblPOLineNumber">
                Purchase Order Line Number
            </div>
            <div class="divtxtBox LNOrient" style=" width: 150px">
                <asp:DropDownList ID="ddlPOLineNumber" runat="server" TabIndex="6" CssClass="validate[required]  RequiredField ddlStype"
                    Width="150px">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer LNOrient divReportAction">
            <table class="ReportTable">
                <tr>
                    <td style="float: left; width: 120px">
                        <asp:ImageButton ID="btnSave" runat="server" ImageUrl="../Images/show-report-icon.png"
                            Width="35px" Height="25px" OnClick="btnSave_Click" />
                        <%--<asp:Button ID="btnSave" runat="server" Text="Show Report" CssClass="btn" OnClick="btnSave_Click" />--%>
                    </td>
                    <td>
                        <%--<asp:Button ID="btnPrint" runat="server" Text="Print" CssClass="btn" />--%>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
 <script src="../LanguageScript/AP/APChangeLang.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        fn_changeLng('<%= Session["Sel_Lng"] %>');
    });

    $(document).ready(function () {
        $("#form1").validationEngine();
        return fn_SaveValidation();
    });

    var prm = Sys.WebForms.PageRequestManager.getInstance();
    prm.add_endRequest(function () {
        return fn_SaveValidation();
    });

    function fn_SaveValidation() {
        $("#FINContent_btnSave").click(function (e) {
            //e.preventDefault();
            return $("#form1").validationEngine('validate')
        })
    }

    </script>
</asp:Content>