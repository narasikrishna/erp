﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.GL;
using FIN.DAL.AP;
using FIN.DAL.AR;
using FIN.BLL;
using FIN.BLL.AP;
using FIN.BLL.AR;
using FIN.BLL.HR;
using FIN.BLL.GL;
using FIN.BLL.SSM;
using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;

namespace FIN.Client.Reports.AP
{
    public partial class RPTSupplierStatementOfAccountParam : PageBase
    {
        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("APSupplierStatementOfAccount", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                FillComboBox();
                FillStartDate();
                Startup();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("APSupplierStatementOfAccount", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void Startup()
        {

            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));

            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();

        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;

                if (txtFromDate.Text != string.Empty && txtToDate.Text != string.Empty)
                {
                    if (ddlGlobalSegment.SelectedValue != string.Empty)
                    {
                        htFilterParameter.Add("SEGMENT_ID", ddlGlobalSegment.SelectedValue);
                    }
                    if (ddlGlobalSegment.SelectedValue != string.Empty)
                    {
                        htFilterParameter.Add("SEGMENT_name", ddlGlobalSegment.SelectedItem.Text);
                    }
                    if (txtFromDate.Text != string.Empty)
                    {
                        htFilterParameter.Add("From_Date", txtFromDate.Text);
                    }
                    if (txtToDate.Text != string.Empty)
                    {
                        htFilterParameter.Add("To_Date", txtToDate.Text);
                    }
                    if (ddlSupplierNumber.SelectedValue != string.Empty)
                    {
                        htFilterParameter.Add("VENDOR_ID", ddlSupplierNumber.SelectedValue);
                    }
                    if (ddlSupplierNumber.SelectedValue != string.Empty)
                    {
                        htFilterParameter.Add("VENDOR_Name", ddlSupplierNumber.SelectedItem.Text);
                    }

                    FIN.DAL.AP.SupplierStatementOfAccount_DAL.GetSP_SupplierStatementOfAccount(ddlSupplierNumber.SelectedValue.ToString(), txtFromDate.Text.ToString(), txtToDate.Text.ToString());

                    VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
                    ReportData = FIN.BLL.AP.SupplierStatementOfAccount_BLL.GetReportData();




                    //ReportData = FIN.BLL.AP.APAginAnalysis_BLL.GetReportData();

                    htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());
                    htHeadingParameters.Add("OrgName", VMVServices.Web.Utils.OrganizationName);
                    ReportFormulaParameter = htHeadingParameters;

                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerPath + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);
                }
                else
                {
                    ErrorCollection.Add("frwomtdoedate", "From Date and To Date cannot be empty");
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("APSupplierStatementOfAccount", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }
        private void FillStartDate()
        {

            string str_finyear = FINSP.GetSPFOR_FiscalYear(DateTime.Now.Date.ToString("dd/MM/yyyy"));
            DataTable dtDate = new DataTable();
            if (str_finyear != string.Empty)
            {
                dtDate = DBMethod.ExecuteQuery(FIN.DAL.GL.InventoryReports_DAL.getCalDate(str_finyear)).Tables[0];
                txtFromDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["cal_eff_start_dt"].ToString());
                txtToDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["call_eff_end_dt"].ToString());
            }


        }
        private void FillComboBox()
        {
            //FIN.BLL.AP.APAginAnalysis_BLL.GetGroupName(ref ddlGlobalSegment);
            FIN.BLL.GL.Segments_BLL.GetGlobalSegmentvaluesByOrgName(ref ddlGlobalSegment);
            FIN.BLL.AP.Supplier_BLL.GetSupplierName(ref ddlSupplierNumber);

        }
    }
}