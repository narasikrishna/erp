﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.DAL.GL;
using FIN.DAL.LOAN;
using FIN.BLL;
using FIN.BLL.GL;
using FIN.BLL.HR;
using VMVServices.Web;


namespace FIN.Client.LOAN
{
    public partial class LoanPaymentEntry : PageBase
    {

        LN_REPAY_HDR LN_REPAY_HDR = new LN_REPAY_HDR();
        LN_REPAY_DTL LN_REPAY_DTL = new LN_REPAY_DTL();
        System.Collections.SortedList slControls = new System.Collections.SortedList();

        DataTable dtGridData = new DataTable();
        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                //pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                //pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";

            }

            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
            }

            if (VMVServices.Web.Utils.AddAllowed != FINAppConstants.Y && Master.Mode == FINAppConstants.Add)
            {
                btnSave.Visible = false;
            }

        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                Startup();
                FillComboBox();


                EntityData = null;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        private void FillComboBox()
        {
            FIN.BLL.CA.Bank_BLL.fn_getBankName(ref ddlBank);

        }



        # region Save,Update and Delete

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();

                if (dtData.Rows.Count > 0)
                {
                    dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("LN_AMOUNT", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("LN_AMOUNT"))));
                    dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("LN_REVISED_PS_AMT", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("LN_REVISED_PS_AMT"))));
                    dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("LN_PAID_AMT", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("LN_PAID_AMT"))));
                    dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("LS_PS_PAID_AMT", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("LS_PS_PAID_AMT"))));

                    dtData.AcceptChanges();
                }
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dt_tmp.Rows.Add(dr);
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WRI_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        protected void btnProcess_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                dtGridData = DBMethod.ExecuteQuery(LoanRepayPlan_DAL.GetLoanNewRepayPlanDetails(ddlBank.SelectedValue, txtDate.Text)).Tables[0];
                BindGrid(dtGridData);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                var tmpChildEntity = new List<Tuple<object, string>>();
                if (gvData.Rows.Count > 0)
                {
                    for (int i = 0; i < gvData.Rows.Count; i++)
                    {
                        LN_REPAY_DTL = new LN_REPAY_DTL();

                        if (gvData.DataKeys[i].Values["LN_PRN_REPAY_DTL_ID"].ToString() != "0" && gvData.DataKeys[i].Values["LN_PRN_REPAY_DTL_ID"].ToString() != string.Empty)
                        {
                            using (IRepository<LN_REPAY_DTL> userCtx = new DataRepository<LN_REPAY_DTL>())
                            {
                                LN_REPAY_DTL = userCtx.Find(r =>
                                    (r.LN_PRN_REPAY_DTL_ID == gvData.DataKeys[i].Values["LN_PRN_REPAY_DTL_ID"].ToString())
                                    ).SingleOrDefault();
                            }
                        }


                        TextBox txtPayAmt = gvData.Rows[i].FindControl("txtPayAmt") as TextBox;
                        TextBox txtPayProfitShareAmt = gvData.Rows[i].FindControl("txtPayProfitShareAmt") as TextBox;
                        TextBox txtPayDate = gvData.Rows[i].FindControl("txtPayDate") as TextBox;
                        if (txtPayDate.Text.Length > 0)
                        {
                            LN_REPAY_DTL.LN_PAID_DT = DBMethod.ConvertStringToDate(txtPayDate.Text);
                            LN_REPAY_DTL.LS_PS_PAID_DT = DBMethod.ConvertStringToDate(txtPayDate.Text);
                        }

                        LN_REPAY_DTL.LN_PAID_AMT = CommonUtils.ConvertStringToDecimal(txtPayAmt.Text);
                        LN_REPAY_DTL.LS_PS_PAID_AMT = CommonUtils.ConvertStringToDecimal(txtPayAmt.Text);

                        LN_REPAY_DTL.MODIFIED_BY = this.LoggedUserName;
                        LN_REPAY_DTL.MODIFIED_DATE = DateTime.Today;

                        tmpChildEntity.Add(new Tuple<object, string>(LN_REPAY_DTL, FINAppConstants.Update));
                    }
                }
                CommonUtils.SaveSingleEntity<LN_REPAY_DTL>(tmpChildEntity, true);

                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.SAVED);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion




        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


    }
}
