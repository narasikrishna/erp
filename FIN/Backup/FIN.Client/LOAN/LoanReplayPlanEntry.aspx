﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="LoanReplayPlanEntry.aspx.cs" Inherits="FIN.Client.HR.LoanReplayPlanEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1000px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblDept">
                Loan Id
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:DropDownList ID="ddlLoanId" runat="server" TabIndex="3" CssClass="validate[required] RequiredField ddlStype"
                    AutoPostBack="True" OnSelectedIndexChanged="ddlLoanId_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp; </div>
            <div class="lblBox  LNOrient" style=" width: 150px" id="Div1">
                Loan Installment
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:DropDownList ID="ddlInstallment" runat="server" TabIndex="3" CssClass="validate[required] RequiredField ddlStype"
                    AutoPostBack="True" OnSelectedIndexChanged="ddlInstallment_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" style="display:none">
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblDepartmentName">
                Paid Loan Amount
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtPaidLoanAmt" CssClass=" txtBox" runat="server" TabIndex="2" 
                    Enabled="false"></asp:TextBox>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp;</div>
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblDepartmentType">
                Balance
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtBalance" CssClass=" txtBox" runat="server" Enabled="false" TabIndex="2"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="Div2">
                Installemnt
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtInstallment" CssClass=" txtBox_N" runat="server" Enabled="false"
                    TabIndex="2" ></asp:TextBox>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp;</div>
            <div class="lblBox  LNOrient" style=" width: 150px" id="Div3">
                Monthly Interest
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtMonthlYIntest" CssClass="txtBox_N" runat="server" Enabled="false"
                    TabIndex="2"></asp:TextBox>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp;</div>
            <div class="lblBox  LNOrient" style=" width: 150px" id="Div4">
                Principal Repayment
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtprinRepay" CssClass="txtBox_N" runat="server" Enabled="false" TabIndex="2"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="Div5">
                Paid Monthly Interest
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtPaidMonInt" CssClass="validate[required] RequiredField txtBox_N" runat="server"
                    TabIndex="2" AutoPostBack="True" ontextchanged="txtPaidMonInt_TextChanged"></asp:TextBox>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp;</div>
            <div class="lblBox  LNOrient" style=" width: 150px" id="Div6">
                Paid Principal Repayment
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtPaidPrinRepay" CssClass="validate[required] RequiredField txtBox_N" runat="server" AutoPostBack="true"
                    TabIndex="2" ontextchanged="txtPaidMonInt_TextChanged"></asp:TextBox>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp;</div>
            <div class="lblBox  LNOrient" style=" width: 150px" id="Div7">
                Total Installment
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txttotalInst" CssClass="validate[required] RequiredField  txtBox_N" runat="server"
                    TabIndex="2"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div align="left" style="display: none">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                DataKeyNames="LN_REPAY_DTL_ID,DELETED" OnRowCancelingEdit="gvData_RowCancelingEdit"
                OnRowCommand="gvData_RowCommand" OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound"
                OnRowDeleting="gvData_RowDeleting" OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating"
                ShowFooter="True">
                <Columns>
                    <asp:TemplateField HeaderText="SNo">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtSno" TabIndex="3" Width="160px" MaxLength="10" runat="server"
                                CssClass=" RequiredField  txtBox" Text='<%# Eval("SNO") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtSno" TabIndex="3" Width="160px" MaxLength="10" runat="server"
                                CssClass="RequiredField   txtBox"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblSno" Width="160px" runat="server" Text='<%# Eval("SNO") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Installment Number">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtInstallmntNo" TabIndex="3" Width="160px" MaxLength="10" runat="server"
                                CssClass=" RequiredField  txtBox" Text='<%# Eval("LN_REPAY_INSTALLMENT_NO") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtInstallmntNo" TabIndex="3" Width="160px" MaxLength="10" runat="server"
                                CssClass="RequiredField   txtBox"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblInstallmntNo" Width="160px" runat="server" Text='<%# Eval("LN_REPAY_INSTALLMENT_NO") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Repay Date">
                        <EditItemTemplate>
                            <asp:TextBox ID="dtpRepayDate" TabIndex="3" MaxLength="10" runat="server" CssClass="RequiredField EntryFont  txtBox"
                                Width="100px" Text='<%#  Eval("LN_REPAY_DT","{0:dd/MM/yyyy}") %>' Onkeypress="return DateKeyCheck(event,this);"></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender9" runat="server" Format="dd/MM/yyyy" TargetControlID="dtpReturnDate">
                            </cc2:CalendarExtender>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars="/"
                                FilterType="Numbers,Custom" TargetControlID="dtpIssueDate" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="dtpRepayDate" TabIndex="3" MaxLength="10" runat="server" CssClass="RequiredField EntryFont  txtBox"
                                Width="100px" Text='<%#  Eval("LN_REPAY_DT","{0:dd/MM/yyyy}") %>' Onkeypress="return DateKeyCheck(event,this);"></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender9" runat="server" Format="dd/MM/yyyy" TargetControlID="dtpRepayDate">
                            </cc2:CalendarExtender>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars="/"
                                FilterType="Numbers,Custom" TargetControlID="dtpRepayDate" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblRepayDate" runat="server" Text='<%# Eval("LN_REPAY_DT","{0:dd/MM/yyyy}") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Amount">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtAmount" TabIndex="4" Width="160px" MaxLength="100" runat="server"
                                CssClass=" RequiredField  txtBox" Text='<%# Eval("LN_REPAY_AMOUNT") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtAmount" TabIndex="4" Width="160px" MaxLength="100" runat="server"
                                CssClass="RequiredField   txtBox"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblAmount" Width="160px" runat="server" Text='<%# Eval("LN_REPAY_AMOUNT") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Paid">
                        <EditItemTemplate>
                            <asp:CheckBox ID="chkPaid" TabIndex="8" runat="server" Checked='<%# Convert.ToBoolean(Eval("LN_PAID")) %>' />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:CheckBox ID="chkPaid" TabIndex="8" runat="server" Checked="true" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkPaid" runat="server" Checked='<%# Convert.ToBoolean(Eval("LN_PAID")) %>' />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Active">
                        <EditItemTemplate>
                            <asp:CheckBox ID="chkact" runat="server" Checked='<%# Convert.ToBoolean(Eval("ENABLED_FLAG")) %>' />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:CheckBox ID="chkact" TabIndex="6" runat="server" Checked="true" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkact" runat="server" Checked='<%# Convert.ToBoolean(Eval("ENABLED_FLAG")) %>' />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" TabIndex="8" runat="server" AlternateText="Edit" CausesValidation="false"
                                ToolTip="Edit" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                            <asp:ImageButton ID="ibtnDelete" TabIndex="9" runat="server" AlternateText="Delete"
                                CausesValidation="false" ToolTip="Delete" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibtnUpdate" TabIndex="8" runat="server" AlternateText="Update"
                                CommandName="Update" ToolTip="Update" Height="20px" ImageUrl="~/Images/Update.png" />
                            <asp:ImageButton ID="ibtnCancel" TabIndex="9" runat="server" AlternateText="Cancel"
                                CausesValidation="false" ToolTip="Cancel" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="ibtnInsert" TabIndex="7" runat="server" AlternateText="Add"
                                CommandName="FooterInsert" ToolTip="Add" ImageUrl="~/Images/Add.jpg" />
                        </FooterTemplate>
                        <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                            TabIndex="2" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" OnClick="btnDelete_Click"
                            TabIndex="3" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="4" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="5" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table>
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/LN/LNChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
