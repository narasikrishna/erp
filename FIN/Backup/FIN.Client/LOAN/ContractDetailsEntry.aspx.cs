﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.AP;
using FIN.BLL;
using FIN.BLL.GL;
using FIN.BLL.AP;
using FIN.BLL.HR;
using VMVServices.Web;
using System.Data;
using System.Collections;
using VMVServices.Services.Data;
using FIN.DAL.LOAN;

namespace FIN.Client.LOAN
{
    public partial class ContractDetailsEntry : PageBase
    {
        LN_LOAN_REQUEST_HDR lN_LOAN_REQUEST_HDR = new LN_LOAN_REQUEST_HDR();
        LN_LOAN_REQUEST_DTL lN_LOAN_REQUEST_DTL = new LN_LOAN_REQUEST_DTL();
        LN_LOAN_REQUEST_TERMS lN_LOAN_REQUEST_TERMS = new LN_LOAN_REQUEST_TERMS();


        DataTable dtGridData = new DataTable();
        DataTable dtLotGridData = new DataTable();
        Boolean bol_rowVisiable;
        Boolean savedBool;
        string ProReturn = null;
        int rowIndexVal = 1;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    Session[FINSessionConstants.LotGridData] = null;
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("MI", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }
            UserRightsChecking();

        }
        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        #endregion Pageload
        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>
        /// 
        private void FillComboBox()
        {
            Lookup_BLL.GetLookUpValues(ref ddlPropertyType, "Request_Type");
        }
        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>
        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();

                EntityData = null;
                Session[FINSessionConstants.GridData] = null;
                Session[FINSessionConstants.LotGridData] = null;

                Session[FINSessionConstants.LotGridData + hdItemIndex.Value] = null;

                Session["GridData"] = null;
                Session["rowindex"] = null;


                dtGridData = DBMethod.ExecuteQuery(FIN.DAL.HR.LoanRequest_DAL.GetLoanRequestDetails(Master.StrRecordId)).Tables[0];
                BindGrid(dtGridData);

                //dtLotGridData = DBMethod.ExecuteQuery(MaterialIssue_DAL.GetItemLotDtls(Master.StrRecordId)).Tables[0];
                //BindLotGrid(dtLotGridData);

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    lN_LOAN_REQUEST_HDR = LoanRequest_BLL.getClassEntity(Master.StrRecordId);

                    EntityData = lN_LOAN_REQUEST_HDR;

                    if (lN_LOAN_REQUEST_HDR.LN_PROPERTY_TYPE != null)
                    {
                        ddlPropertyType.SelectedValue = lN_LOAN_REQUEST_HDR.LN_PROPERTY_TYPE.ToString();
                        LoanRequest_BLL.GetLoanPropertyDtls(ref ddlPropertyID, ddlPropertyType.SelectedValue.ToString());
                    }
                    if (lN_LOAN_REQUEST_HDR.LN_PROPERTY_ID != null)
                    {
                        ddlPropertyID.SelectedValue = lN_LOAN_REQUEST_HDR.LN_PROPERTY_ID.ToString();
                    }
                    if (lN_LOAN_REQUEST_HDR.LN_PURPOSE != null)
                    {
                        txtLoanPurpose.Text = lN_LOAN_REQUEST_HDR.LN_PURPOSE;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("MIATC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session["GridData"] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr["ENABLED_FLAG"] = "FALSE";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("MIBindGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }
        }

        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddlBankName = tmpgvr.FindControl("ddlBankName") as DropDownList;
                DropDownList ddlCurrency = tmpgvr.FindControl("ddlCurrency") as DropDownList;
                DropDownList ddlContractStatus = tmpgvr.FindControl("ddlContractStatus") as DropDownList;


                FIN.BLL.CA.Bank_BLL.fn_getBankName(ref ddlBankName);
                Currency_BLL.getCurrencyDesc(ref ddlCurrency);
                Lookup_BLL.GetLookUpValues(ref ddlContractStatus, "LRequest");

                if (gvData.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                {
                    ddlBankName.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["LN_PARTY_ID"].ToString();
                    ddlCurrency.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["LN_CURRENCY_ID"].ToString();
                    ddlContractStatus.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["LN_CONTRACT_STATUS"].ToString();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("MIFillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }
        }

        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            if (Session["GridData"] != null)
            {
                dtGridData = (DataTable)Session["GridData"];
            }
            gvData.EditIndex = -1;

            BindGrid(dtGridData);

        }
        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session["GridData"] != null)
                {
                    dtGridData = (DataTable)Session["GridData"];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                        //Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// .RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {

            System.Collections.SortedList slControls = new System.Collections.SortedList();
            System.Collections.SortedList slControls_n = new System.Collections.SortedList();

            TextBox txtCM = gvr.FindControl("txtCN") as TextBox;
            TextBox txtContractDesc = gvr.FindControl("txtContractDesc") as TextBox;
            DropDownList ddlBankName = gvr.FindControl("ddlBankName") as DropDownList;
            TextBox txtContractDate = gvr.FindControl("dtpContractDate") as TextBox;
            DropDownList ddlContractStatus = gvr.FindControl("ddlContractStatus") as DropDownList;
            DropDownList ddlCurrency = gvr.FindControl("ddlCurrency") as DropDownList;
            TextBox txtPrincipalAmt = gvr.FindControl("txtPrincipalAmt") as TextBox;
            TextBox txtProfit = gvr.FindControl("txtProfit") as TextBox;
            TextBox txtStartDate = gvr.FindControl("dtpStartDate") as TextBox;
            TextBox txtEndDate = gvr.FindControl("dtpEndDate") as TextBox;
            CheckBox chkact = gvr.FindControl("chkact") as CheckBox;

            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList["LN_REQUEST_ID"] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);
            }

            if (ddlBankName.SelectedValue.ToString().Length > 0)
            {
                slControls[0] = txtCM;
                slControls[1] = txtContractDesc;
                slControls[2] = ddlBankName;
                slControls[3] = txtContractDate;
                slControls[4] = ddlContractStatus;
                slControls[5] = ddlCurrency;
                slControls[6] = txtPrincipalAmt;
                slControls[7] = txtProfit;
                slControls[8] = txtStartDate;
                slControls[9] = txtEndDate;

                ErrorCollection.Clear();

                string strCtrlTypes = FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX;

                string strMessage = "Contract Number ~ Contract Description ~ Bank Name ~ Contract Date ~ Contract Status ~ Currency ~ Principal Amount ~ Profit ~ Start Date ~ End Date";
                EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

                if (EmptyErrorCollection.Count > 0)
                {
                    ErrorCollection = EmptyErrorCollection;
                    return drList;
                }

                string strCondition = "LN_PARTY_ID='" + ddlBankName.SelectedValue.Trim() + "'";
                strMessage = FINMessageConstatns.RecordAlreadyExists;
                ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);

                if (ErrorCollection.Count > 0)
                {
                    return drList;
                }
                if (Session[FINSessionConstants.LotGridData] != null)
                {
                    dtLotGridData = (DataTable)Session[FINSessionConstants.LotGridData];
                    if (dtLotGridData.Select("LN_PARTY_ID='" + ddlBankName.SelectedValue.ToString() + "'").Length == 0)
                    {
                        ErrorCollection.Add("BankDetails", "Bank Details Not Found");
                        return drList;
                    }
                }
                if (ErrorCollection.Count > 0)
                {
                    return drList;
                }
            }
            else
            {
                slControls_n[0] = txtPrincipalAmt;
                string strCtrlTypes1 = FINAppConstants.TEXT_BOX;
                string strMessage1 = "Principal Amount";
                EmptyErrorCollection = CommonUtils.IsValid(slControls_n, strCtrlTypes1, strMessage1);

                if (EmptyErrorCollection.Count > 0)
                {
                    ErrorCollection = EmptyErrorCollection;
                    return drList;
                }
            }

            ErrorCollection.Clear();

            if (hdBankId.Value != string.Empty)
            {
                drList["LN_PARTY_ID"] = (hdBankId.Value);
            }
            drList["BANK_NAME"] = ddlBankName.SelectedItem.Text;
            drList["LN_PARTY_ID"] = ddlBankName.SelectedValue;
            if (txtCM.Text != String.Empty)
            {
                drList["LN_CONTRACT_NUM"] = txtCM.Text;
            }
            else
            {
                drList["LN_CONTRACT_NUM"] = "0";
            }

            if (txtContractDesc.Text != String.Empty)
            {
                drList["LN_CONTRACT_DESC"] = txtContractDesc.Text;
            }
            else
            {
                drList["LN_CONTRACT_DESC"] = "0";
            }
            if (txtContractDate.Text != String.Empty)
            {
                drList["LN_CONTRACT_DT"] = txtContractDate.Text;
            }
            else
            {
                drList["LN_CONTRACT_DT"] = "";
            }

            if (ddlCurrency.SelectedValue.ToString().Length > 0)
            {
                drList["CURRENCY_DESC"] = ddlCurrency.SelectedItem.Text;
                drList["LN_CURRENCY_ID"] = ddlCurrency.SelectedValue;
            }
            else
            {
                drList["CURRENCY_DESC"] = "";
                drList["LN_CURRENCY_ID"] = "";
            }

            if (ddlContractStatus.SelectedValue.ToString().Length > 0)
            {
                drList["LN_CONTRACT_STATUS_DESC"] = ddlContractStatus.SelectedItem.Text;
                drList["LN_CONTRACT_STATUS"] = ddlContractStatus.SelectedValue;
            }
            else
            {
                drList["LN_CONTRACT_STATUS_DESC"] = "";
                drList["LN_CONTRACT_STATUS"] = "";
            }

            if (txtPrincipalAmt.Text != string.Empty)
            {
                drList["LN_PRINCIPAL_AMOUNT"] = txtPrincipalAmt.Text;
            }
            else
            {
                drList["LN_PRINCIPAL_AMOUNT"] = "0";
            }
            if (txtProfit.Text != string.Empty)
            {
                drList["LN_ROI"] = txtProfit.Text;
            }
            else
            {
                drList["LN_ROI"] = "0";
            }
            if (txtStartDate.Text != string.Empty)
            {
                drList["LN_START_DATE"] = DBMethod.ConvertStringToDate(txtStartDate.Text);
            }
            else
            {
                drList["LN_START_DATE"] = "";
            }
            if (txtEndDate.Text != string.Empty)
            {
                drList["LN_END_DATE"] = DBMethod.ConvertStringToDate(txtEndDate.Text);
            }
            else
            {
                drList["LN_END_DATE"] = "";
            }

            if (chkact.Checked)
            {
                drList[FINColumnConstants.ENABLED_FLAG] = "TRUE";
            }
            else
            {
                drList[FINColumnConstants.ENABLED_FLAG] = "FALSE";
            }



            drList["DELETED"] = FINAppConstants.N;

            return drList;
        }



        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session["GridData"] != null)
                {
                    dtGridData = (DataTable)Session["GridData"];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }
        }

        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session["GridData"] != null)
                {
                    dtGridData = (DataTable)Session["GridData"];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList["DELETED"] = FINAppConstants.Y;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }
        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session["GridData"] != null)
                {
                    dtGridData = (DataTable)Session["GridData"];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }

        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row["DELETED"].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }

                    DropDownList ddlContractStatus = e.Row.FindControl("ddlContractStatus") as DropDownList;
                    TextBox dtpContractDate = e.Row.FindControl("dtpContractDate") as TextBox;
                    ImageButton imgBtnPost = e.Row.FindControl("imgBtnPost") as ImageButton;

                    if (LoanProvisionDAL.IsLoanPaymentExists(dtpContractDate.Text) > 0)
                    {
                        gvData.Columns[14].Visible = false;
                        gvData.Columns[15].Visible = false;
                        imgBtnPost.Visible = false;
                    }
                    else if (LoanProvisionDAL.IsLoanPaymentPosted(gvData.DataKeys[e.Row.RowIndex].Values["LN_CONTRACT_ID"].ToString()) > 0)
                    {
                        if (ddlContractStatus.SelectedValue.Trim().ToUpper() == "APPROVED")
                        {
                            gvData.Columns[14].Visible = true;
                            gvData.Columns[15].Visible = true;
                            imgBtnPost.Visible = false;
                        }
                    }
                    else if (LoanProvisionDAL.IsLoanPaymentPosted(gvData.DataKeys[e.Row.RowIndex].Values["LN_CONTRACT_ID"].ToString()) == 0)
                    {
                        if (ddlContractStatus.SelectedValue.Trim().ToUpper() == "APPROVED")
                        {
                            gvData.Columns[14].Visible = true;
                            gvData.Columns[15].Visible = false;
                            imgBtnPost.Visible = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }

        }

        #endregion



        protected void ddlPropertyType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (ddlPropertyType.SelectedValue.ToString().Length > 0)
                {
                    LoanRequest_BLL.GetLoanPropertyDtls(ref ddlPropertyID, ddlPropertyType.SelectedValue.ToString());
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PropertyType", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                var tmpSecondChildEntity = new List<Tuple<object, string>>();
                if (EntityData != null)
                {
                    lN_LOAN_REQUEST_HDR = (LN_LOAN_REQUEST_HDR)EntityData;
                }

                lN_LOAN_REQUEST_HDR.LN_PROPERTY_TYPE = ddlPropertyType.SelectedValue.ToString();
                lN_LOAN_REQUEST_HDR.LN_PROPERTY_ID = ddlPropertyID.SelectedValue.ToString();
                lN_LOAN_REQUEST_HDR.LN_PURPOSE = txtLoanPurpose.Text;
                lN_LOAN_REQUEST_HDR.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                lN_LOAN_REQUEST_HDR.LN_ORG_ID = VMVServices.Web.Utils.OrganizationID;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    lN_LOAN_REQUEST_HDR.MODIFIED_BY = this.LoggedUserName;
                    lN_LOAN_REQUEST_HDR.MODIFIED_DATE = DateTime.Today;
                }
                else
                {
                    lN_LOAN_REQUEST_HDR.LN_REQUEST_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.LN_001_M.ToString(), false, true);
                    lN_LOAN_REQUEST_HDR.CREATED_BY = this.LoggedUserName;
                    lN_LOAN_REQUEST_HDR.CREATED_DATE = DateTime.Today;
                }

                lN_LOAN_REQUEST_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, lN_LOAN_REQUEST_HDR.LN_REQUEST_ID);

                //Save Detail Table

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                ErrorCollection = CommonUtils.IsEmptyGrid(dtGridData, "Loan Request");
                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                var tmpChildEntity = new List<Tuple<object, string>>();
                LN_LOAN_REQUEST_DTL lN_LOAN_REQUEST_DTL = new LN_LOAN_REQUEST_DTL();

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    lN_LOAN_REQUEST_DTL = new LN_LOAN_REQUEST_DTL();

                    if (dtGridData.Rows[iLoop]["LN_CONTRACT_ID"].ToString() != "0" && dtGridData.Rows[iLoop]["LN_CONTRACT_ID"].ToString() != string.Empty)
                    {
                        lN_LOAN_REQUEST_DTL = LoanRequest_BLL.getDetailClassEntity(dtGridData.Rows[iLoop]["LN_CONTRACT_ID"].ToString());
                    }

                    lN_LOAN_REQUEST_DTL.LN_PARTY_ID = (dtGridData.Rows[iLoop]["LN_PARTY_ID"].ToString());
                    lN_LOAN_REQUEST_DTL.LN_PARTY_TYPE = (dtGridData.Rows[iLoop]["BANK_NAME"].ToString());
                    lN_LOAN_REQUEST_DTL.LN_CURRENCY_ID = (dtGridData.Rows[iLoop]["LN_CURRENCY_ID"].ToString());
                    lN_LOAN_REQUEST_DTL.LN_CONTRACT_NUM = (dtGridData.Rows[iLoop]["LN_CONTRACT_NUM"].ToString());
                    lN_LOAN_REQUEST_DTL.LN_CONTRACT_DESC = (dtGridData.Rows[iLoop]["LN_CONTRACT_DESC"].ToString());
                    lN_LOAN_REQUEST_DTL.LN_CONTRACT_DT = Convert.ToDateTime(dtGridData.Rows[iLoop]["LN_CONTRACT_DT"].ToString());
                    lN_LOAN_REQUEST_DTL.LN_CONTRACT_STATUS = (dtGridData.Rows[iLoop]["LN_CONTRACT_STATUS"].ToString());

                    lN_LOAN_REQUEST_DTL.LN_PRINCIPAL_AMOUNT = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop]["LN_PRINCIPAL_AMOUNT"].ToString());
                    lN_LOAN_REQUEST_DTL.LN_ROI = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop]["LN_ROI"].ToString());

                    lN_LOAN_REQUEST_DTL.LN_START_DATE = Convert.ToDateTime(dtGridData.Rows[iLoop]["LN_START_DATE"].ToString());
                    lN_LOAN_REQUEST_DTL.LN_END_DATE = Convert.ToDateTime(dtGridData.Rows[iLoop]["LN_END_DATE"].ToString());


                    lN_LOAN_REQUEST_DTL.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                    lN_LOAN_REQUEST_DTL.LN_REQUEST_ID = lN_LOAN_REQUEST_HDR.LN_REQUEST_ID;

                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        tmpChildEntity.Add(new Tuple<object, string>(lN_LOAN_REQUEST_DTL, FINAppConstants.Delete));
                    }
                    else
                    {
                        if (dtGridData.Rows[iLoop]["LN_CONTRACT_ID"].ToString() != "0" && dtGridData.Rows[iLoop]["LN_CONTRACT_ID"].ToString() != string.Empty)
                        {
                            lN_LOAN_REQUEST_DTL.LN_CONTRACT_ID = dtGridData.Rows[iLoop]["LN_CONTRACT_ID"].ToString();
                            lN_LOAN_REQUEST_DTL.MODIFIED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(lN_LOAN_REQUEST_DTL, FINAppConstants.Update));
                        }
                        else
                        {
                            lN_LOAN_REQUEST_DTL.LN_CONTRACT_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.LN_001_D.ToString(), false, true);
                            lN_LOAN_REQUEST_DTL.CREATED_BY = this.LoggedUserName;
                            lN_LOAN_REQUEST_DTL.CREATED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(lN_LOAN_REQUEST_DTL, FINAppConstants.Add));
                        }
                    }

                    lN_LOAN_REQUEST_DTL.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;

                    ////Terms Entry
                    //if (Session[FINSessionConstants.LotGridData] != null)
                    //{
                    //    dtLotGridData = (DataTable)Session[FINSessionConstants.LotGridData];

                    //    if (dtLotGridData.Rows.Count > 0)
                    //    {
                    //        for (int jLoop = 0; jLoop < dtLotGridData.Rows.Count; jLoop++)
                    //        {
                    //            if (dtGridData.Rows[iLoop]["ITEM_ID"].ToString() == dtLotGridData.Rows[jLoop]["ITEM_ID"].ToString())
                    //            {
                    //                lN_LOAN_REQUEST_TERMS = new LN_LOAN_REQUEST_TERMS();

                    //                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                    //                {
                    //                    if (dtLotGridData.Rows[jLoop]["ISSUE_MAT_WH_ID"].ToString() != "0" && dtLotGridData.Rows[jLoop]["ISSUE_MAT_WH_ID"].ToString() != string.Empty)
                    //                    {
                    //                        lN_LOAN_REQUEST_TERMS = MaterialIssue_BLL.getWHClassEntity(dtLotGridData.Rows[jLoop]["ISSUE_MAT_WH_ID"].ToString());
                    //                    }
                    //                }

                    //                lN_LOAN_REQUEST_TERMS.INDENT_DTL_ID = iNV_MATERIAL_ISSUE_DTL.INDENT_DTL_ID;
                    //                lN_LOAN_REQUEST_TERMS.ISSUE_WH_ID = dtLotGridData.Rows[jLoop]["INV_WH_ID"].ToString();
                    //                lN_LOAN_REQUEST_TERMS.ISSUE_LOT_ID = dtLotGridData.Rows[jLoop]["lot_id"].ToString();
                    //                lN_LOAN_REQUEST_TERMS.ISSUE_LOT_QTY = CommonUtils.ConvertStringToDecimal(dtLotGridData.Rows[jLoop]["LOT_QTY"].ToString());
                    //                lN_LOAN_REQUEST_TERMS.ENABLED_FLAG = FINAppConstants.EnabledFlag;


                    //                if (dtLotGridData.Rows[jLoop]["DELETED"].ToString() == FINAppConstants.Y)
                    //                {
                    //                    tmpSecondChildEntity.Add(new Tuple<object, string>(lN_LOAN_REQUEST_TERMS, FINAppConstants.Delete));
                    //                }
                    //                else
                    //                {
                    //                    if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                    //                    {
                    //                        if (dtLotGridData.Rows[jLoop]["ISSUE_MAT_WH_ID"].ToString() != "0" && dtLotGridData.Rows[jLoop]["ISSUE_MAT_WH_ID"].ToString() != string.Empty)
                    //                        {
                    //                            lN_LOAN_REQUEST_TERMS.MODIFIED_DATE = DateTime.Today;
                    //                            tmpSecondChildEntity.Add(new Tuple<object, string>(lN_LOAN_REQUEST_TERMS, FINAppConstants.Update));
                    //                        }
                    //                    }
                    //                    else
                    //                    {
                    //                        lN_LOAN_REQUEST_TERMS.ISSUE_MAT_WH_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.AP_028_T.ToString(), false, true);
                    //                        lN_LOAN_REQUEST_TERMS.CREATED_BY = Session["UserId"].ToString();
                    //                        lN_LOAN_REQUEST_TERMS.CREATED_DATE = DateTime.Today;
                    //                        tmpSecondChildEntity.Add(new Tuple<object, string>(lN_LOAN_REQUEST_TERMS, FINAppConstants.Add));
                    //                    }
                    //                }

                    //                lN_LOAN_REQUEST_TERMS.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;
                    //            }
                    //        }
                    //    }
                    //}

                }

                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            //CommonUtils.SaveMultipleEntity<LN_LOAN_REQUEST_HDR, LN_LOAN_REQUEST_DTL, LN_LOAN_REQUEST_TERMS>(lN_LOAN_REQUEST_HDR, tmpChildEntity, lN_LOAN_REQUEST_DTL, tmpSecondChildEntity, lN_LOAN_REQUEST_TERMS);
                            CommonUtils.SavePCEntity<LN_LOAN_REQUEST_HDR, LN_LOAN_REQUEST_DTL>(lN_LOAN_REQUEST_HDR, tmpChildEntity, lN_LOAN_REQUEST_DTL);
                            savedBool = true;


                            //for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                            //{
                            //    for (int jLoop = 0; jLoop < dtLotGridData.Rows.Count; jLoop++)
                            //    {
                            //        DataTable dtQTY = new DataTable();
                            //        decimal dtrecqty = 0;
                            //        decimal balqty = 0;


                            //        dtQTY = DBMethod.ExecuteQuery(MaterialIssue_DAL.getLotqtyissed_fromRecLotHdr(dtLotGridData.Rows[jLoop][FINColumnConstants.LOT_ID].ToString())).Tables[0];
                            //        dtrecqty = CommonUtils.ConvertStringToDecimal(dtQTY.Rows[0]["LOT_QTY_ISSUED"].ToString());
                            //        balqty = CommonUtils.ConvertStringToDecimal(dtLotGridData.Rows[jLoop]["LOT_QTY"].ToString());

                            //        HFlotqty.Value = (dtrecqty + balqty).ToString();

                            //        MaterialIssue_DAL.UPDATE_Receiptlot_dtl(dtLotGridData.Rows[jLoop][FINColumnConstants.LOT_ID].ToString(), decimal.Parse(HFlotqty.Value));

                            //        DataTable dtrecptid = new DataTable();
                            //        dtrecptid = DBMethod.ExecuteQuery(MaterialIssue_DAL.GetLotQty(dtLotGridData.Rows[jLoop]["lot_id"].ToString())).Tables[0];

                            //        FINSP.GetSPItemtxnLedger("ISSUE", dtGridData.Rows[iLoop][FINColumnConstants.ITEM_ID].ToString(), dtLotGridData.Rows[jLoop]["LOT_QTY"].ToString(), dtLotGridData.Rows[jLoop]["INV_WH_ID"].ToString(), DateTime.Now.ToString(), dtrecptid.Rows[0]["RECEIPT_ID"].ToString(), null, null, "ISSUES of " + dtGridData.Rows[iLoop][FINColumnConstants.ITEM_ID].ToString());
                            //    }
                            //}


                            break;
                        }
                    case FINAppConstants.Update:
                        {

                            //for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                            //{
                            //    for (int jLoop = 0; jLoop < dtLotGridData.Rows.Count; jLoop++)
                            //    {
                            //        DataTable dtQTY = new DataTable();
                            //        DataTable QtyfromMI = new DataTable();
                            //        decimal dtrecqty = 0;
                            //        decimal balqty = 0;
                            //        decimal qtyMI = 0;
                            //        decimal Finlqty = 0;

                            //        dtQTY = DBMethod.ExecuteQuery(MaterialIssue_DAL.getLotqtyissed_fromRecLotHdr(dtLotGridData.Rows[jLoop][FINColumnConstants.LOT_ID].ToString())).Tables[0];
                            //        QtyfromMI = DBMethod.ExecuteQuery(MaterialIssue_DAL.getLotqty_fromMatIssu(dtLotGridData.Rows[jLoop][FINColumnConstants.LOT_ID].ToString())).Tables[0];
                            //        dtrecqty = CommonUtils.ConvertStringToDecimal(dtQTY.Rows[0]["LOT_QTY_ISSUED"].ToString());
                            //        qtyMI = CommonUtils.ConvertStringToDecimal(QtyfromMI.Rows[0]["ISSUE_LOT_QTY"].ToString());
                            //        balqty = CommonUtils.ConvertStringToDecimal(dtLotGridData.Rows[jLoop]["LOT_QTY"].ToString());
                            //        Finlqty = (qtyMI - balqty);

                            //        HFlotqty.Value = (dtrecqty - Finlqty).ToString();
                            //        MaterialIssue_DAL.UPDATE_Receiptlot_dtl(dtLotGridData.Rows[jLoop][FINColumnConstants.LOT_ID].ToString(), decimal.Parse(HFlotqty.Value));

                            //    }
                            //}

                            CommonUtils.SaveMultipleEntity<LN_LOAN_REQUEST_HDR, LN_LOAN_REQUEST_DTL, LN_LOAN_REQUEST_TERMS>(lN_LOAN_REQUEST_HDR, tmpChildEntity, lN_LOAN_REQUEST_DTL, tmpSecondChildEntity, lN_LOAN_REQUEST_TERMS, true);
                            savedBool = true;
                            //for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                            //{
                            //    for (int jLoop = 0; jLoop < dtLotGridData.Rows.Count; jLoop++)
                            //    {
                            //        DataTable dtrecptid = new DataTable();
                            //        dtrecptid = DBMethod.ExecuteQuery(MaterialIssue_DAL.GetLotQty(dtLotGridData.Rows[jLoop]["lot_id"].ToString())).Tables[0];
                            //        FINSP.GetSPItemtxnLedger("ISSUE", dtGridData.Rows[iLoop][FINColumnConstants.ITEM_ID].ToString(), dtLotGridData.Rows[jLoop]["LOT_QTY"].ToString(), dtLotGridData.Rows[jLoop]["INV_WH_ID"].ToString(), DateTime.Now.ToString(), dtrecptid.Rows[0]["RECEIPT_ID"].ToString(), null, null, "ISSUES of " + dtGridData.Rows[iLoop][FINColumnConstants.ITEM_ID].ToString());
                            //    }
                            //}
                            break;
                        }
                }


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("MI", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        protected void imgBtnJVPrint_Click(object sender, ImageClickEventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            LN_LOAN_REQUEST_DTL lN_LOAN_REQUEST_DTL = new LN_LOAN_REQUEST_DTL();
            using (IRepository<LN_LOAN_REQUEST_DTL> userCtx = new DataRepository<LN_LOAN_REQUEST_DTL>())
            {
                lN_LOAN_REQUEST_DTL = userCtx.Find(r =>
                    (r.LN_CONTRACT_ID == gvData.DataKeys[gvr.RowIndex].Values["LN_CONTRACT_ID"].ToString())
                    ).SingleOrDefault();
            }

            if (lN_LOAN_REQUEST_DTL != null)
            {
                Hashtable htParameters = new Hashtable();
                Hashtable htHeadingParameters = new Hashtable();
                Hashtable htFilterParameter = new Hashtable();

                ReportFile = "GL_REPORTS//RPTJournalVoucher.rpt";


                ReportData = DBMethod.ExecuteQuery(FIN.DAL.GL.JournalEntry_DAL.GetJournalVoucherDetailsRPT("", lN_LOAN_REQUEST_DTL.JE_HDR_ID));


                Session["ProgramName"] = "Journal Voucher";
                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());



                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;


                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('../RPTCrystalReportViewer.aspx?Id?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);

            }
        }

        protected void imgBtnPost_Click(object sender, ImageClickEventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            Label lblPosted = gvr.FindControl("lblPosted") as Label;
            ImageButton imgBtnJVPrint = gvr.FindControl("imgBtnJVPrint") as ImageButton;

            ImageButton img_tmp = (ImageButton)sender;
            img_tmp.Enabled = false;
            try
            {
                if (EntityData != null)
                {
                    lN_LOAN_REQUEST_HDR = (LN_LOAN_REQUEST_HDR)EntityData;
                }


                LN_LOAN_REQUEST_DTL lN_LOAN_REQUEST_DTL = new LN_LOAN_REQUEST_DTL();
                using (IRepository<LN_LOAN_REQUEST_DTL> userCtx = new DataRepository<LN_LOAN_REQUEST_DTL>())
                {
                    lN_LOAN_REQUEST_DTL = userCtx.Find(r =>
                        (r.LN_CONTRACT_ID == gvData.DataKeys[gvr.RowIndex].Values["LN_CONTRACT_ID"].ToString())
                        ).SingleOrDefault();
                }

                if (lN_LOAN_REQUEST_HDR.WORKFLOW_COMPLETION_STATUS == "1")
                {
                    FINSP.GetSP_GL_Posting(gvData.DataKeys[gvr.RowIndex].Values["LN_CONTRACT_ID"].ToString(), "LN_001");


                    if (ErrorCollection.Count > 0)
                    {
                        return;
                    }
                    img_tmp.Visible = false;
                    lblPosted.Visible = true;
                    imgBtnJVPrint.Visible = true;
                    hdPayId.Value = lN_LOAN_REQUEST_DTL.LN_CONTRACT_ID;

                    lN_LOAN_REQUEST_DTL.POSTED_FLAG = FINAppConstants.Y;
                    lN_LOAN_REQUEST_DTL.POSTED_DATE = DateTime.Now;
                    DBMethod.SaveEntity<LN_LOAN_REQUEST_DTL>(lN_LOAN_REQUEST_DTL, true);

                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.POSTED);
                }

            }
            catch (Exception ex)
            {
                img_tmp.Visible = true;
                lblPosted.Visible = false;
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {

                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                AssignToBE();

                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("MISAVE", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// .RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                // DBMethod.DeleteEntity<INV_RECEIPTS_HDR>(INV_RECEIPTS_HDR);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void btnTerm_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                //GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
                //hdRowIndex.Value = gvr.RowIndex.ToString();



                //DropDownList ddlItemName = gvr.FindControl("ddlItemName") as DropDownList;
                //TextBox txtRequestedQty = gvr.FindControl("txtRequestedQuantity") as TextBox;
                //TextBox txtIssuedQty = gvr.FindControl("txtIssuedQuantity") as TextBox;

                //if (ddlItemName.SelectedValue.ToString().Trim().Length > 0 && txtIssuedQty.Text.Length > 0)
                //{
                //    hdItemId.Value = ddlItemName.SelectedValue.ToString();



                //    string itemId = string.Empty;

                //    if (gvData.EditIndex >= 0)
                //    {
                //        if (gvData.DataKeys[gvr.RowIndex].Values["ITEM_ID"].ToString() != string.Empty)
                //        {
                //            itemId = gvData.DataKeys[gvr.RowIndex].Values["ITEM_ID"].ToString();
                //            hdItemIndex.Value = itemId;
                //        }
                //    }

                //    if (Session[FINSessionConstants.LotGridData] == null)
                //    {
                //        dtLotGridData = DBMethod.ExecuteQuery(MaterialIssue_DAL.GetItemLotDtls(Master.StrRecordId)).Tables[0];
                //    }
                //    else
                //    {
                //        dtLotGridData = (DataTable)Session[FINSessionConstants.LotGridData];
                //    }

                //    lblLotError.Visible = false;
                //    Session[FINSessionConstants.LotGridData] = dtLotGridData;
                //    BindLotGrid(dtLotGridData);
                //    hf_qty.Value = txtIssuedQty.Text.ToString();
                //    mpeReceipt.Show();
                //}
                //else
                //{
                //    ErrorCollection.Add("PleaseSelectItem", "Please Select the Item / Please Entry The Quanty");
                //}
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("MILotButtonClick", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }
        }
        protected void btnTermNo_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                //if (Session[FINSessionConstants.LotGridData] != null)
                //{
                //    dtLotGridData = (DataTable)Session[FINSessionConstants.LotGridData];
                //    double dbl_Qty = 0;
                //    if (dtLotGridData.Rows.Count > 0)
                //    {
                //        for (int jLoop = 0; jLoop < dtLotGridData.Rows.Count; jLoop++)
                //        {
                //            if (hdItemId.Value == dtLotGridData.Rows[jLoop]["ITEM_ID"].ToString() && dtLotGridData.Rows[jLoop]["DELETED"].ToString() != "1")
                //            {
                //                dbl_Qty += Convert.ToDouble(dtLotGridData.Rows[jLoop]["LOT_QTY"].ToString());
                //            }
                //        }
                //    }
                //    if (dbl_Qty > 0)
                //    {
                //        if (dbl_Qty <= Convert.ToDouble(hf_qty.Value))
                //        {
                //            mpeReceipt.Hide();
                //        }
                //        else
                //        {
                //            mpeReceipt.Show();
                //            lblLotError.Text = "Issued Quantity and Lot Quantity must be Same";
                //            lblLotError.Visible = true;
                //        }
                //    }
                //    else
                //    {
                //        mpeReceipt.Hide();
                //    }
                //}


                // mpeReceipt.Hide();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("MILotClose", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }
        }

        # region Grid Events

        private void BindLotGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();


                bol_rowVisiable = false;
                Session[FINSessionConstants.LotGridData + hdItemIndex.Value] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvLot.DataSource = dt_tmp;
                gvLot.DataBind();
                GridViewRow gvr = gvLot.FooterRow;
                FillFooterLotGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RLD_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }
        }

        private void FillFooterLotGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                //DropDownList ddlWH = tmpgvr.FindControl("ddlWH") as DropDownList;
                //DropDownList ddlLot = tmpgvr.FindControl("ddlLot") as DropDownList;

                ////Warehouse_BLL.GetWareHouseName(ref ddlWH);
                ////MaterialIssue_BLL.GetLotNumber(ref ddlLot);
                //MaterialIssue_BLL.getWHDetails_fritem(ref ddlWH, hdItemId.Value.ToString());


                //if (gvLot.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                //{
                //    // ddlWH.SelectedItem.Text = gvData.DataKeys[gvData.EditIndex].Values["INV_WH_NAME"].ToString();
                //    ddlWH.SelectedValue = gvLot.DataKeys[gvLot.EditIndex].Values["INV_WH_ID"].ToString();
                //    //ddlLot.SelectedItem.Text = gvData.DataKeys[gvData.EditIndex].Values["LOT_NUMBER"].ToString();
                //    fillLot();
                //    ddlLot.SelectedValue = gvLot.DataKeys[gvLot.EditIndex].Values["LOT_ID"].ToString();
                //}
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("MIFillFootLOTGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }
        }
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvLot_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            //if (Session[FINSessionConstants.LotGridData + hdItemIndex.Value] != null)
            //{
            //    dtLotGridData = (DataTable)Session[FINSessionConstants.LotGridData + hdItemIndex.Value];
            //}
            if (Session[FINSessionConstants.LotGridData] != null)
            {
                dtLotGridData = (DataTable)Session[FINSessionConstants.LotGridData];
            }
            gvLot.EditIndex = -1;

            BindLotGrid(dtLotGridData);

        }

        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvLot_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                //if (Session[FINSessionConstants.LotGridData + hdItemIndex.Value] != null)
                //{
                //    dtLotGridData = (DataTable)Session[FINSessionConstants.LotGridData + hdItemIndex.Value];
                //}
                if (Session[FINSessionConstants.LotGridData] != null)
                {
                    dtLotGridData = (DataTable)Session[FINSessionConstants.LotGridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvLot.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }

                }

                //if (e.CommandName.Equals("btnPop"))
                //{
                //    int rowindex = Convert.ToInt32(e.CommandArgument);
                //    hdRowIndex.Value = rowindex.ToString();
                //}

                if (Session["rowindex"] == null)
                {
                    Session["rowindex"] = 0;
                    hdRowIndex.Value = Session["rowindex"].ToString();
                }
                else
                {
                    hdRowIndex.Value = (int.Parse(Session["rowindex"].ToString()) + 1).ToString();
                }

                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    if (dtLotGridData != null)
                    {
                        int rowLotIndex = 0;
                        //if (gvr.RowIndex == -1)
                        //{
                        //    rowLotIndex = gvr.RowIndex + rowIndexVal;
                        //}
                        //else if (gvr.RowIndex >= 0)
                        //{
                        //    rowLotIndex = gvr.RowIndex;
                        //}

                        drList = AssignToLotGridControl(gvr, dtLotGridData, "A", 0);
                        if (ErrorCollection.Count > 0)
                        {
                            Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                            //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                            return;
                        }
                        dtLotGridData.Rows.Add(drList);
                        // Session[FINSessionConstants.LotGridData + hdItemIndex.Value] = dtLotGridData;
                        Session[FINSessionConstants.LotGridData] = dtLotGridData;
                        BindLotGrid(dtLotGridData);
                    }

                }
                mpeReceipt.Show();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RLD_ROW_CMD", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToLotGridControl(GridViewRow gvr, DataTable tmpdtLotGridData, string GMode, int rowindex)
        {

            //System.Collections.SortedList slControls = new System.Collections.SortedList();

            //DropDownList ddlWH = gvr.FindControl("ddlWH") as DropDownList;
            //DropDownList ddlLot = gvr.FindControl("ddlLot") as DropDownList;
            //TextBox txtQty = gvr.FindControl("txtQty") as TextBox;

            DataRow drList;
            DataTable dt_tmp = tmpdtLotGridData.Copy();
            if (GMode == "A")
            {
                drList = dtLotGridData.NewRow();
                drList["INV_WH_ID"] = "0";
            }
            else
            {
                //if (dtLotGridData.Rows.Count > 0)
                //{
                drList = dtLotGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);
                //}

            }

            //slControls[0] = ddlWH;
            //slControls[1] = ddlLot;
            //slControls[2] = txtQty;

            //ErrorCollection.Clear();
            //string strCtrlTypes = "DropDownList ~ DropDownList ~ TextBox";

            //string strMessage = "Warehouse ~ Lot ~ Quantity";

            //EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

            //if (EmptyErrorCollection.Count > 0)
            //{
            //    ErrorCollection = EmptyErrorCollection;
            //    return drList;
            //}

            //string strCondition = string.Empty;

            ////strCondition = "INV_WH_ID='" + ddlWH.SelectedValue + "'";
            ////strMessage = "Record Already Exists";

            ////ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            ////if (ErrorCollection.Count > 0)
            ////{
            ////    return drList;
            ////}


            //strCondition = " INV_WH_ID='" + ddlWH.SelectedValue + "' AND LOT_ID= '" + ddlLot.SelectedValue + "'";

            //strMessage = "Record Already Exists";
            //ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            //if (ErrorCollection.Count > 0)
            //{

            //    return drList;
            //}

            ////DataTable dtqty = new DataTable();
            ////dtqty = DBMethod.ExecuteQuery(ReceiptLotDetails_DAL.getLotqty(ddlLineNumber.SelectedValue)).Tables[0];
            //////hf_qty.Value = dtqty.Rows[0]["tot_qty"].ToString();
            ////hf_qty.Value = "0";

            ////if (decimal.Parse(txtQuantityforthislot.Text) > decimal.Parse(hf_qty.Value))
            ////{
            ////    ErrorCollection.Add("lotqty", "Quantity Cannot be greater than purchase item received quantity");
            ////    return drList;
            ////}
            ////if (ErrorCollection.Count > 0)
            ////{
            ////    return drList;
            ////}

            ////DataTable dtlotqty = new DataTable();

            //// dtlotqty = DBMethod.ExecuteQuery(ReceiptLotDetails_DAL.GetTotalReceivedLotqty(txtGRNNumber.Text, ddlLineNumber.SelectedValue, hid_Item_Id.Value)).Tables[0];

            ////if (decimal.Parse(dtlotqty.Rows[0]["lot_qty"].ToString()) > 0)
            ////{
            ////    decimal diffLot = decimal.Parse(hf_qty.Value) - decimal.Parse(dtlotqty.Rows[0]["lot_qty"].ToString());

            ////    if (decimal.Parse(txtQuantityforthislot.Text) > Math.Abs(diffLot))
            ////    {
            ////        ErrorCollection.Add("totlotqty", "Quantity for this lot exceeding the GRN quantity.Received quantity is " + dtlotqty.Rows[0]["lot_qty"].ToString() + ".Pending Quantity is " + diffLot);
            ////        return drList;
            ////    }
            ////}
            ////if (ErrorCollection.Count > 0)
            ////{
            ////    return drList;
            ////}

            //drList["INV_WH_NAME"] = ddlWH.SelectedItem.Text.ToString();
            //drList["INV_WH_ID"] = ddlWH.SelectedValue.ToString();
            //drList["LOT_NUMBER"] = ddlLot.SelectedItem.Text.ToString();
            //drList["LOT_ID"] = ddlLot.SelectedValue.ToString();
            //drList["LOT_QTY"] = txtQty.Text.ToString();
            //drList["ITEM_ID"] = hdItemId.Value.ToString();

            return drList;

        }

        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvLot_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvLot.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                //if (Session[FINSessionConstants.LotGridData + hdItemIndex.Value] != null)
                //{
                //    dtLotGridData = (DataTable)Session[FINSessionConstants.LotGridData + hdItemIndex.Value];
                //}
                if (Session[FINSessionConstants.LotGridData] != null)
                {
                    dtLotGridData = (DataTable)Session[FINSessionConstants.LotGridData];
                }
                if (gvr == null)
                {
                    return;
                }
                //int rowindex = Convert.ToInt32(e.CommandArgument);

                //hdRowIndex.Value = rowindex.ToString();
                if (dtLotGridData != null)
                {
                    drList = AssignToLotGridControl(gvr, dtLotGridData, "U", e.RowIndex);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    gvLot.EditIndex = -1;
                    //Session[FINSessionConstants.LotGridData + hdItemIndex.Value] = dtLotGridData;
                    Session[FINSessionConstants.LotGridData] = dtLotGridData;
                    BindLotGrid(dtLotGridData);
                }
                //if (Session[FINSessionConstants.LotGridData + hdRowIndex.Value] == null)
                //{
                //    Session[FINSessionConstants.LotGridData + hdRowIndex.Value] = Session[FINSessionConstants.LotGridData];
                //}

                mpeReceipt.Show();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RLD_ROW_UP", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }
        }

        protected void gvLot_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                //if (Session[FINSessionConstants.LotGridData + hdItemIndex.Value] != null)
                //{
                //    dtLotGridData = (DataTable)Session[FINSessionConstants.LotGridData + hdItemIndex.Value];
                //}
                if (Session[FINSessionConstants.LotGridData] != null)
                {
                    dtLotGridData = (DataTable)Session[FINSessionConstants.LotGridData];
                }
                DataRow drList = null;
                drList = dtLotGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;
                BindLotGrid(dtLotGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RLD_ROW_DEL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }
        }

        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvLot_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                //GridViewRow gvr = gvLot.Rows[e.RowIndex] as GridViewRow;
                //if (Session[FINSessionConstants.LotGridData + ] != null)
                //{
                //    dtLotGridData = (DataTable)Session[FINSessionConstants.LotGridData + hdRowIndex.Value];
                //}
                gvLot.EditIndex = e.NewEditIndex;
                BindLotGrid(dtLotGridData);
                GridViewRow gvr = gvLot.Rows[e.NewEditIndex];
                FillFooterLotGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RLD_ROW_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvLot_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvLot.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RLD_RowCreated", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }

        }

        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvLot_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Cells[2].HorizontalAlign = HorizontalAlign.Right;
                    if (bol_rowVisiable)
                        e.Row.Visible = false;
                    else
                    {
                        //if (((DataRowView)e.Row.DataItem).Row["ITEM_ID"].ToString() != hdItemId.Value)
                        //{
                        //    e.Row.Visible = false;
                        //}
                    }
                    if (((DataRowView)e.Row.DataItem).Row["DELETED"].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RLD_RDB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }

        }
        #endregion


        protected void getLotData()
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddlLot = new DropDownList();
                TextBox txtQuantity = new TextBox();

                ddlLot.ID = "ddlLot";
                txtQuantity.ID = "txtQty";


                //if (gvLot.FooterRow != null)
                //{
                //    if (gvLot.EditIndex < 0)
                //    {
                //        //ddlItemName = (DropDownList)gvData.FooterRow.FindControl("ddlItemName");
                //        ddlLot = (DropDownList)gvLot.FooterRow.FindControl("ddlLot");
                //        txtQuantity = (TextBox)gvLot.FooterRow.FindControl("txtQty");
                //    }
                //    else
                //    {
                //        ddlLot = (DropDownList)gvLot.Rows[gvLot.EditIndex].FindControl("ddlLot");
                //        txtQuantity = (TextBox)gvLot.Rows[gvLot.EditIndex].FindControl("txtQty");
                //    }
                //}
                //else
                //{
                //    ddlLot = (DropDownList)gvLot.Controls[0].Controls[1].FindControl("ddlLot");
                //    txtQuantity = (TextBox)gvLot.Controls[0].Controls[1].FindControl("txtQty");
                //}
                //DataTable dtData = new DataTable();
                //if (ddlLot.SelectedValue != string.Empty && ddlLot.SelectedValue != "0")
                //{
                //    dtData = DBMethod.ExecuteQuery(MaterialIssue_DAL.GetLotQty(ddlLot.SelectedValue.ToString())).Tables[0];
                //    if (dtData != null)
                //    {
                //        if (dtData.Rows.Count > 0)
                //        {
                //            txtQuantity.Text = dtData.Rows[0]["LOT_QTY"].ToString();
                //        }
                //    }
                //}
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("MIGetLotData", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }
        }

    }
}