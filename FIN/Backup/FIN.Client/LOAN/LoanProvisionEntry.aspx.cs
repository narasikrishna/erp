﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.DAL.GL;
using FIN.DAL.LOAN;
using FIN.BLL;
using FIN.BLL.GL;
using FIN.BLL.HR;
using VMVServices.Web;
using System.Collections;
using VMVServices.Services.Data;

namespace FIN.Client.LOAN
{
    public partial class LoanProvisionEntry : PageBase
    {
        LN_LOAN_PROVISION lN_LOAN_PROVISION = new LN_LOAN_PROVISION();
        LN_REPAY_HDR LN_REPAY_HDR = new LN_REPAY_HDR();
        LN_REPAY_DTL LN_REPAY_DTL = new LN_REPAY_DTL();
        System.Collections.SortedList slControls = new System.Collections.SortedList();

        DataTable dtGridData = new DataTable();
        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                // btnSave.Visible = false;
                //btnDelete.Visible = true;

            }
            else
            {
                // btnSave.Visible = true;
                // btnDelete.Visible = false;

            }
            if (Master.Mode == FINAppConstants.Update)
            {
                //btnSave.Text = "Update";

            }

            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
            }

            if (VMVServices.Web.Utils.AddAllowed != FINAppConstants.Y && Master.Mode == FINAppConstants.Add)
            {
                // btnSave.Visible = false;
            }

        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                Startup();
                FillComboBox();


                EntityData = null;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    LN_LOAN_PROVISION lN_LOAN_PROVISION = new LN_LOAN_PROVISION();
                    using (IRepository<LN_LOAN_PROVISION> userCtx = new DataRepository<LN_LOAN_PROVISION>())
                    {
                        lN_LOAN_PROVISION = userCtx.Find(r =>
                            (r.LN_PROVISION_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    if (lN_LOAN_PROVISION != null)
                    {
                        txtDate.Text = DBMethod.ConvertDateToString(lN_LOAN_PROVISION.LN_PROVISION_DATE.ToString());
                        DataTable dt_cont_det = DBMethod.ExecuteQuery(FIN.DAL.HR.LoanRequest_DAL.getLoanContractDetails(lN_LOAN_PROVISION.LN_CONTRACT_ID.ToString())).Tables[0];
                        if (dt_cont_det.Rows.Count > 0)
                        {
                            ddlBank.SelectedValue = dt_cont_det.Rows[0]["LN_PARTY_ID"].ToString();
                            btnProcess_Click(btnProcess, new EventArgs());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        private void FillComboBox()
        {
            FIN.BLL.CA.Bank_BLL.fn_getBankName(ref ddlBank);

        }



        # region Save,Update and Delete

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();

                if (dtData.Rows.Count > 0)
                {
                    dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("LN_PROFIT_SHARE_AMT", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("LN_PROFIT_SHARE_AMT"))));
                    dtData.AcceptChanges();
                }

                gvData.DataSource = dtData;
                gvData.DataBind();


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WRI_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        protected void btnProcess_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                //dtGridData = DBMethod.ExecuteQuery(LoanProvisionDAL.getLoanProvisionDet(txtDate.Text, ddlBank.SelectedValue)).Tables[0];
                dtGridData = FIN.DAL.LOAN.LoanProvisionDAL.get_LoanProvisonAmt(txtDate.Text, ddlBank.SelectedValue);
                BindGrid(dtGridData);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>


        #endregion



        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    ImageButton imgBtnPost = e.Row.FindControl("imgBtnPost") as ImageButton;
                    ImageButton imgBtnPrint = e.Row.FindControl("imgBtnJVPrint") as ImageButton;
                    Label lblPosted = e.Row.FindControl("lblPosted") as Label;
                    //if (LoanProvisionDAL.IsLoanProvisionExists(txtDate.Text) > 0)
                    //{
                    //    gvData.Columns[3].Visible = false;
                    //    gvData.Columns[4].Visible = false;
                    //    imgBtnPost.Visible = false;
                    //}
                    //else 

                    using (IRepository<LN_LOAN_PROVISION> userCtx = new DataRepository<LN_LOAN_PROVISION>())
                    {
                        lN_LOAN_PROVISION = userCtx.Find(r =>
                            (r.LN_PROVISION_ID == gvData.DataKeys[e.Row.RowIndex].Values["LN_PROVISION_ID"].ToString())
                            ).SingleOrDefault();
                    }

                    if (lN_LOAN_PROVISION != null)
                    {
                        if (lN_LOAN_PROVISION.JE_HDR_ID != null)
                        {
                            imgBtnPrint.Visible = true;
                            lblPosted.Visible = true;
                            imgBtnPost.Visible = false;
                        }
                        else
                        {
                            imgBtnPost.Visible = true;
                            lblPosted.Visible = false;
                            imgBtnPrint.Visible = false;
                        }
                    }
                    
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }

        }
        protected void imgBtnJVPrint_Click(object sender, ImageClickEventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            LN_LOAN_PROVISION lN_LOAN_PROVISION = new LN_LOAN_PROVISION();
            using (IRepository<LN_LOAN_PROVISION> userCtx = new DataRepository<LN_LOAN_PROVISION>())
            {
                lN_LOAN_PROVISION = userCtx.Find(r =>
                    (r.LN_PROVISION_ID == gvData.DataKeys[gvr.RowIndex].Values["LN_PROVISION_ID"].ToString())
                    ).SingleOrDefault();
            }

            if (lN_LOAN_PROVISION != null)
            {
                Hashtable htParameters = new Hashtable();
                Hashtable htHeadingParameters = new Hashtable();
                Hashtable htFilterParameter = new Hashtable();

                ReportFile = "GL_REPORTS//RPTJournalVoucher.rpt";


                ReportData = DBMethod.ExecuteQuery(FIN.DAL.GL.JournalEntry_DAL.GetJournalVoucherDetailsRPT("", lN_LOAN_PROVISION.JE_HDR_ID));


                Session["ProgramName"] = "Journal Voucher";
                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());



                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;


                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('../RPTCrystalReportViewer.aspx?Id?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);

            }
        }

        protected void imgBtnPost_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton img_tmp = (ImageButton)sender;
            img_tmp.Enabled = false;
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            Label lblPosted = gvr.FindControl("lblPosted") as Label;
            ImageButton imgBtnJVPrint = gvr.FindControl("imgBtnJVPrint") as ImageButton;

            try
            {


                LN_LOAN_PROVISION lN_LOAN_PROVISION = new LN_LOAN_PROVISION();
                lN_LOAN_PROVISION.LN_CONTRACT_ID = gvData.DataKeys[gvr.RowIndex]["LN_CONTRACT_ID"].ToString();
                lN_LOAN_PROVISION.LN_PROVISION_DATE = DBMethod.ConvertStringToDate(txtDate.Text);
                lN_LOAN_PROVISION.LN_PRINCIPLE_AMT = decimal.Parse(gvData.DataKeys[gvr.RowIndex]["REM_PRINCIPAL_AMT"].ToString());
                lN_LOAN_PROVISION.LN_NO_OF_DAYS = decimal.Parse(gvData.DataKeys[gvr.RowIndex]["LN_NO_OF_DAYS"].ToString());
                lN_LOAN_PROVISION.LN_PROFIT_SHARE_AMT = decimal.Parse(gvData.DataKeys[gvr.RowIndex]["LN_PROFIT_SHARE_AMT"].ToString());
                lN_LOAN_PROVISION.LN_ORG_ID = VMVServices.Web.Utils.OrganizationID;
                lN_LOAN_PROVISION.ENABLED_FLAG = FINAppConstants.Y;
                lN_LOAN_PROVISION.CREATED_BY = this.LoggedUserName;
                lN_LOAN_PROVISION.CREATED_DATE = DateTime.Today;
                lN_LOAN_PROVISION.LN_PROVISION_ID = FINSP.GetSPFOR_SEQCode("LN_008", false, true);
                lN_LOAN_PROVISION.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, lN_LOAN_PROVISION.LN_PROVISION_ID);
                DBMethod.SaveEntity<LN_LOAN_PROVISION>(lN_LOAN_PROVISION);

                if (lN_LOAN_PROVISION.WORKFLOW_COMPLETION_STATUS == "1")
                {
                    FINSP.GetSP_GL_Posting(lN_LOAN_PROVISION.LN_PROVISION_ID, "LN_008");
                }

                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                img_tmp.Visible = false;
                lblPosted.Visible = true;
                imgBtnJVPrint.Visible = true;
                hdProvisionId.Value = lN_LOAN_PROVISION.LN_PROVISION_ID;

                gvData.DataKeys[gvr.RowIndex].Values["LN_PROVISION_ID"] = lN_LOAN_PROVISION.LN_PROVISION_ID;
                using (IRepository<LN_LOAN_PROVISION> userCtx = new DataRepository<LN_LOAN_PROVISION>())
                {
                    lN_LOAN_PROVISION = userCtx.Find(r =>
                        (r.LN_PROVISION_ID == hdProvisionId.Value)
                        ).SingleOrDefault();
                }


                lN_LOAN_PROVISION.POSTED_FLAG = FINAppConstants.Y;
                lN_LOAN_PROVISION.POSTED_DATE = DateTime.Now;
                DBMethod.SaveEntity<LN_LOAN_PROVISION>(lN_LOAN_PROVISION, true);

                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.POSTED);


            }
            catch (Exception ex)
            {
                img_tmp.Visible = true;
                lblPosted.Visible = false;
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {

                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


    }
}
