﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="InterestDetails.aspx.cs" Inherits="FIN.Client.LOAN.LoanDetailsParam" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 800px" id="divMainContainer">
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style="width: 200px" id="lblName">
                Contract Number
            </div>
            <div class="divtxtBox  LNOrient" style="width: 500px">
                <asp:DropDownList ID="ddlContractNumber" CssClass="validate[required] RequiredField ddlStype"
                    TabIndex="1" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlloanrequest_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" style="display: none">
            <div class="lblBox  LNOrient" style="width: 200px" id="Div4">
                Facility Name
            </div>
            <div class="divtxtBox  LNOrient" style="width: 500px">
                <asp:DropDownList ID="ddlFacilityName" CssClass="validate[required] RequiredField ddlStype"
                    Enabled="false" TabIndex="1" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlFacilityName_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style="width: 200px" id="Div3">
                Property Name
            </div>
            <div class="divtxtBox  LNOrient" style="width: 500px">
                <asp:DropDownList ID="ddlPropertyName" CssClass="validate[required] RequiredField ddlStype"
                    Enabled="false" TabIndex="1" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlPropertyName_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style="width: 200px" id="lblCompetencyType">
                Contract Description
            </div>
            <div class="divtxtBox  LNOrient" style="width: 500px">
                <asp:TextBox ID="txtCD" CssClass="validate[required]  RequiredField  txtBox" runat="server"
                    Enabled="false" TabIndex="2"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style="width: 200px" id="Div1">
                Contract Date
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:TextBox ID="txtContractDate" CssClass="validate[required]  RequiredField  txtBox"
                    runat="server" TabIndex="3"></asp:TextBox>
                <cc2:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MM/yyyy" TargetControlID="txtContractDate"
                    OnClientDateSelectionChanged="checkDate">
                </cc2:CalendarExtender>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtContractDate" />
            </div>
            <div class="colspace  LNOrient">
                &nbsp</div>
            <div class="lblBox  LNOrient" style="width: 170px" id="Div2">
                Bank Name
            </div>
            <div class="divtxtBox  LNOrient" style="width: 155px">
                <asp:TextBox ID="txtBankName" CssClass="validate[required]  RequiredField  txtBox"
                    runat="server" TabIndex="3"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style="width: 200px" id="lblEffectiveDate">
                Principal Amount
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:TextBox ID="txtPrincipalAmt" CssClass="validate[required]  RequiredField  txtBox_N"
                    runat="server" TabIndex="3"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars=".,"
                    FilterType="Numbers,Custom" TargetControlID="txtPrincipalAmt" />
            </div>
            <div class="colspace  LNOrient">
                &nbsp</div>
            <div class="lblBox  LNOrient" style="width: 170px" id="lblEndDate">
                Profit Share
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:TextBox ID="txtProfitShare" CssClass="validate[]    txtBox_N" runat="server"
                    TabIndex="4"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars=".,"
                    FilterType="Numbers,Custom" TargetControlID="txtProfitShare" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style="width: 200px" id="Div9">
                Repay Period Type
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:DropDownList ID="ddlRepayPeriodType" runat="server" TabIndex="6" CssClass="validate[required] RequiredField ddlStype">
                </asp:DropDownList>
            </div>
            <div class="lblBox  LNOrient" style="width: 200px" id="Div8">
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:Button ID="btnGenerateInstall" runat="server" Text="Generate Installment" OnClick="btnGenerateInstall_Click" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="LNOrient ">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                DataKeyNames="LN_INT_DTL_ID,DELETED" OnRowCancelingEdit="gvData_RowCancelingEdit"
                OnRowCommand="gvData_RowCommand" OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound"
                OnRowDeleting="gvData_RowDeleting" OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating">
                <Columns>
                    <asp:TemplateField HeaderText="Installment No">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtinstallmentno" TabIndex="5" MaxLength="10" runat="server" CssClass=" txtBox_N"
                                Text='<%# Eval("LN_INSTALLMENT_NO") %>' Width="95%"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" ValidChars="0"
                                FilterType="Numbers,Custom" TargetControlID="txtinstallmentno" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtinstallmentno" MaxLength="10" TabIndex="5" runat="server" CssClass=" txtBox_N"
                                Width="95%"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" ValidChars="0"
                                FilterType="Numbers,Custom" TargetControlID="txtinstallmentno" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblinstallmentno" runat="server" Text='<%# Eval("LN_INSTALLMENT_NO") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Installment Date">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtinstallmentdate" MaxLength="10" runat="server" CssClass="RequiredField EntryFont  txtBox"
                                Text='<%#  Eval("LN_INSTALLMENT_DATE","{0:dd/MM/yyyy}") %>' Width="95%" Onkeypress="return DateKeyCheck(event,this);"></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender8" runat="server" Format="dd/MM/yyyy" TargetControlID="txtinstallmentdate">
                            </cc2:CalendarExtender>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" ValidChars="/"
                                FilterType="Numbers,Custom" TargetControlID="txtinstallmentdate" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtinstallmentdate" TabIndex="5" MaxLength="10" runat="server" CssClass="RequiredField EntryFont  txtBox"
                                Onkeypress="return DateKeyCheck(event,this);" Width="95%"></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender9" runat="server" Format="dd/MM/yyyy" TargetControlID="txtinstallmentdate">
                            </cc2:CalendarExtender>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" ValidChars="/"
                                FilterType="Numbers,Custom" TargetControlID="txtinstallmentdate" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblinstallmentdate" runat="server" Text='<%# Eval("LN_INSTALLMENT_DATE","{0:dd/MM/yyyy}") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Profit Share Amount">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtLN_MONTHLY_INT" TabIndex="5" MaxLength="10" runat="server" CssClass=" txtBox_N"
                                Text='<%# Eval("LN_PROFIT_SHARE_AMT") %>' Width="95%"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender120" runat="server" ValidChars=".,"
                                FilterType="Numbers,Custom" TargetControlID="txtLN_MONTHLY_INT" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtLN_MONTHLY_INT" MaxLength="10" TabIndex="5" runat="server" CssClass=" txtBox_N"
                                Width="95%"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender121" runat="server" ValidChars=".,"
                                FilterType="Numbers,Custom" TargetControlID="txtLN_MONTHLY_INT" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblLN_MONTHLY_INT" runat="server" Text='<%# Eval("LN_PROFIT_SHARE_AMT") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="" Visible="false">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" TabIndex="10" runat="server" AlternateText="Edit"
                                CausesValidation="false" ToolTip="Edit" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                            <asp:ImageButton ID="ibtnDelete" TabIndex="11" runat="server" AlternateText="Delete"
                                CausesValidation="false" ToolTip="Delete" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibtnUpdate" TabIndex="10" runat="server" AlternateText="Update"
                                CommandName="Update" ToolTip="Update" Height="20px" ImageUrl="~/Images/Update.png" />
                            <asp:ImageButton ID="ibtnCancel" TabIndex="11" runat="server" AlternateText="Cancel"
                                CausesValidation="false" ToolTip="Cancel" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="ibtnInsert" TabIndex="9" runat="server" AlternateText="Add"
                                CommandName="FooterInsert" ToolTip="Add" ImageUrl="~/Images/Add.jpg" />
                        </FooterTemplate>
                        <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divClear_10">
        </div>
        <div class="LNOrient ">
            <asp:GridView ID="gv_docsub" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                DataKeyNames="DOC_ID,DELETED" OnRowCancelingEdit="gv_docsub_RowCancelingEdit"
                OnRowCommand="gv_docsub_RowCommand" OnRowCreated="gv_docsub_RowCreated" OnRowDataBound="gv_docsub_RowDataBound"
                OnRowDeleting="gv_docsub_RowDeleting" OnRowEditing="gv_docsub_RowEditing" OnRowUpdating="gv_docsub_RowUpdating"
                ShowFooter="True">
                <Columns>
                    <asp:TemplateField HeaderText="Document Type">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtdoctyp" TabIndex="5" MaxLength="10" runat="server" CssClass=" txtBox"
                                Text='<%# Eval("DOC_TYPE") %>' Width="95%"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtdoctyp" MaxLength="10" TabIndex="5" runat="server" CssClass=" txtBox"
                                Width="95%"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lbldoctyp" runat="server" Text='<%# Eval("DOC_TYPE") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Document Description">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtDocdescription" TabIndex="5" MaxLength="10" runat="server" CssClass=" txtBox"
                                Text='<%# Eval("DOC_DESC") %>' Width="95%"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtDocdescription" MaxLength="10" TabIndex="5" runat="server" CssClass=" txtBox"
                                Width="95%"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblDocdescription" runat="server" Text='<%# Eval("DOC_DESC") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Active">
                        <EditItemTemplate>
                            <asp:CheckBox ID="chkDSACT" TabIndex="8" runat="server" Checked='<%# Convert.ToBoolean(Eval("ENABLED_FLAG")) %>' />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:CheckBox ID="chkDSACT" TabIndex="8" runat="server" Checked="true" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkDSACT" runat="server" Checked='<%# Convert.ToBoolean(Eval("ENABLED_FLAG")) %>' />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" TabIndex="10" runat="server" AlternateText="Edit"
                                CausesValidation="false" ToolTip="Edit" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                            <asp:ImageButton ID="ibtnDelete" TabIndex="11" runat="server" AlternateText="Delete"
                                CausesValidation="false" ToolTip="Delete" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibtnUpdate" TabIndex="10" runat="server" AlternateText="Update"
                                CommandName="Update" ToolTip="Update" Height="20px" ImageUrl="~/Images/Update.png" />
                            <asp:ImageButton ID="ibtnCancel" TabIndex="11" runat="server" AlternateText="Cancel"
                                CausesValidation="false" ToolTip="Cancel" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="ibtnInsert" TabIndex="9" runat="server" AlternateText="Add"
                                CommandName="FooterInsert" ToolTip="Add" ImageUrl="~/Images/Add.jpg" />
                        </FooterTemplate>
                        <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                            TabIndex="4" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" OnClick="btnDelete_Click"
                            TabIndex="5" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="6" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="7" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table>
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/LN/LNChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
