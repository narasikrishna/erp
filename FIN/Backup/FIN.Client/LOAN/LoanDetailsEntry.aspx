﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="LoanDetailsEntry.aspx.cs" Inherits="FIN.Client.LOAN.LoanDetailsEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 800px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblName">
                Loan Request
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:DropDownList ID="ddlloanrequest" CssClass="validate[required] RequiredField ddlStype"
                    TabIndex="1" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlloanrequest_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblCompetencyType">
                Party ID
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 155px">
                <asp:DropDownList ID="ddlPartyID" CssClass="validate[required] RequiredField ddlStype"
                    TabIndex="2" runat="server">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 200px" id="Div1">
                Approval Date
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtApprovaldate" CssClass="validate[required]  RequiredField  txtBox"
                    runat="server" TabIndex="3"></asp:TextBox>
                <cc2:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MM/yyyy" TargetControlID="txtApprovaldate"
                    OnClientDateSelectionChanged="checkDate">
                </cc2:CalendarExtender>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtApprovaldate" />
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox  LNOrient" style=" width: 200px" id="Div2">
                Currency
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 155px">
                <asp:DropDownList ID="ddlcurrency" CssClass="validate[required] RequiredField ddlStype"
                    TabIndex="4" runat="server">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblEffectiveDate">
                Approval Loan Amount
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtApprloanamt" CssClass="validate[required]  RequiredField  txtBox_N"
                    runat="server" TabIndex="3"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars=".,"
                    FilterType="Numbers,Custom" TargetControlID="txtApprloanamt" />
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblEndDate">
                Service Charge Amount
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtservicechargeamt" CssClass="validate[]    txtBox_N" runat="server"
                    TabIndex="4"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars=".,"
                    FilterType="Numbers,Custom" TargetControlID="txtservicechargeamt" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 200px" id="Div3">
                Service Charge Pay Date
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtservicechargepaydate" CssClass="validate[required]  RequiredField  txtBox"
                    runat="server" TabIndex="3"></asp:TextBox>
                <cc2:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MM/yyyy" TargetControlID="txtservicechargepaydate"
                    OnClientDateSelectionChanged="checkDate">
                </cc2:CalendarExtender>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtservicechargepaydate" />
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox  LNOrient" style=" width: 200px; display: none" id="Div4">
                Paid Loan Amount
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px; display: none">
                <asp:TextBox ID="txtpaidamt" CssClass="validate[]    txtBox_N" runat="server"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" ValidChars=".,"
                    FilterType="Numbers,Custom" TargetControlID="txtpaidamt" />
            </div>
            <div class="lblBox  LNOrient" style=" width: 200px" id="Div5">
                Pay Period Type
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:DropDownList ID="ddlPayperiodtyp" CssClass="validate[required] RequiredField ddlStype"
                    runat="server">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 200px" id="Div6">
                Profit Share Percentage
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtintrustRate" CssClass="validate[requried] RequiredField   txtBox_N"
                    runat="server"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender14" runat="server" ValidChars=".,"
                    FilterType="Numbers,Custom" TargetControlID="txtintrustRate" />
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox  LNOrient" style=" width: 200px" id="Div7">
                Repayment Duration
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtRepayDur" CssClass="validate[requried] RequiredField   txtBox_N"
                    runat="server"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender15" runat="server" ValidChars=".,"
                    FilterType="Numbers,Custom" TargetControlID="txtRepayDur" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 200px" id="Div9">
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
            </div>
            <div class="lblBox  LNOrient" style=" width: 200px" id="Div8">
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:ImageButton ID="btnGenerateInstall" runat="server" ImageUrl="~/Images/btnGenerateInstallment.png"
                    OnClick="btnGenerateInstall_Click" Style="border: 0px;" />
                <%--   <asp:Button ID="btnGenerateInstall" runat="server" Text="Generate Installment" OnClick="btnGenerateInstall_Click" />--%>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div align="left">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                DataKeyNames="LN_LOAN_DTL_ID,LN_DISTRIBUTION_STATUS,LN_PS_TYPE,DELETED" OnRowCancelingEdit="gvData_RowCancelingEdit"
                OnRowCommand="gvData_RowCommand" OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound"
                OnRowDeleting="gvData_RowDeleting" OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating"
                ShowFooter="True">
                <Columns>
                    <asp:TemplateField HeaderText="Installment No">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtinstallmentno" TabIndex="5" MaxLength="10" runat="server" CssClass=" txtBox_N"
                                Text='<%# Eval("LN_INSTALLMENT_NO") %>' Width="95%"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" ValidChars="0"
                                FilterType="Numbers,Custom" TargetControlID="txtinstallmentno" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtinstallmentno" MaxLength="10" TabIndex="5" runat="server" CssClass=" txtBox_N"
                                Width="95%"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" ValidChars="0"
                                FilterType="Numbers,Custom" TargetControlID="txtinstallmentno" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblinstallmentno" runat="server" Text='<%# Eval("LN_INSTALLMENT_NO") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Installment Date">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtinstallmentdate" MaxLength="10" runat="server" CssClass="RequiredField EntryFont  txtBox"
                                Text='<%#  Eval("LN_INSTALLMENT_DT","{0:dd/MM/yyyy}") %>' Width="95%" Onkeypress="return DateKeyCheck(event,this);"></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender8" runat="server" Format="dd/MM/yyyy" TargetControlID="txtinstallmentdate">
                            </cc2:CalendarExtender>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" ValidChars="/"
                                FilterType="Numbers,Custom" TargetControlID="txtinstallmentdate" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtinstallmentdate" TabIndex="5" MaxLength="10" runat="server" CssClass="RequiredField EntryFont  txtBox"
                                Onkeypress="return DateKeyCheck(event,this);" Width="95%"></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender9" runat="server" Format="dd/MM/yyyy" TargetControlID="txtinstallmentdate">
                            </cc2:CalendarExtender>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" ValidChars="/"
                                FilterType="Numbers,Custom" TargetControlID="txtinstallmentdate" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblinstallmentdate" runat="server" Text='<%# Eval("LN_INSTALLMENT_DT","{0:dd/MM/yyyy}") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Prin Balance">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtLN_PRIN_BALANCE" TabIndex="5" MaxLength="10" runat="server" CssClass=" txtBox_N"
                                Text='<%# Eval("LN_PRIN_BALANCE") %>' Width="95%"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender110" runat="server" ValidChars=".,"
                                FilterType="Numbers,Custom" TargetControlID="txtLN_PRIN_BALANCE" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtLN_PRIN_BALANCE" MaxLength="10" TabIndex="5" runat="server" CssClass=" txtBox_N"
                                Width="95%"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender111" runat="server" ValidChars=".,"
                                FilterType="Numbers,Custom" TargetControlID="txtLN_PRIN_BALANCE" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblLN_PRIN_BALANCE" runat="server" Text='<%# Eval("LN_PRIN_BALANCE") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Monthly Interest">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtLN_MONTHLY_INT" TabIndex="5" MaxLength="10" runat="server" CssClass=" txtBox_N"
                                Text='<%# Eval("LN_MONTHLY_INT") %>' Width="95%"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender120" runat="server" ValidChars=".,"
                                FilterType="Numbers,Custom" TargetControlID="txtLN_MONTHLY_INT" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtLN_MONTHLY_INT" MaxLength="10" TabIndex="5" runat="server" CssClass=" txtBox_N"
                                Width="95%"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender121" runat="server" ValidChars=".,"
                                FilterType="Numbers,Custom" TargetControlID="txtLN_MONTHLY_INT" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblLN_MONTHLY_INT" runat="server" Text='<%# Eval("LN_MONTHLY_INT") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Loan Balance">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtLN_BALANCE" TabIndex="5" MaxLength="10" runat="server" CssClass=" txtBox_N"
                                Text='<%# Eval("LN_BALANCE") %>' Width="95%"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender130" runat="server" ValidChars=".,"
                                FilterType="Numbers,Custom" TargetControlID="txtLN_BALANCE" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtLN_BALANCE" MaxLength="10" TabIndex="5" runat="server" CssClass=" txtBox_N"
                                Width="95%"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender131" runat="server" ValidChars=".,"
                                FilterType="Numbers,Custom" TargetControlID="txtLN_BALANCE" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblLN_BALANCE" runat="server" Text='<%# Eval("LN_BALANCE") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Loan Installment">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtLN_INSTALMENT" TabIndex="5" MaxLength="10" runat="server" CssClass="RequiredField txtBox_N"
                                Text='<%# Eval("LN_INSTALMENT") %>' Width="95%"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender140" runat="server" ValidChars=".,"
                                FilterType="Numbers,Custom" TargetControlID="txtLN_INSTALMENT" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtLN_INSTALMENT" MaxLength="10" TabIndex="5" runat="server" CssClass="RequiredField txtBox_N"
                                Width="95%"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender141" runat="server" ValidChars=".,"
                                FilterType="Numbers,Custom" TargetControlID="txtLN_INSTALMENT" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblLN_INSTALMENT" runat="server" Text='<%# Eval("LN_INSTALMENT") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Principal Repayment">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtLN_PRINCIPAL_REPAYMENT" TabIndex="5" MaxLength="10" runat="server"
                                CssClass=" txtBox_N" Text='<%# Eval("LN_PRINCIPAL_REPAYMENT") %>' Width="95%"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender150" runat="server" ValidChars=".,"
                                FilterType="Numbers,Custom" TargetControlID="txtLN_PRINCIPAL_REPAYMENT" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtLN_PRINCIPAL_REPAYMENT" MaxLength="10" TabIndex="5" runat="server"
                                CssClass=" txtBox_N" Width="95%"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender151" runat="server" ValidChars=".,"
                                FilterType="Numbers,Custom" TargetControlID="txtLN_PRINCIPAL_REPAYMENT" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblLN_PRINCIPAL_REPAYMENT" runat="server" Text='<%# Eval("LN_PRINCIPAL_REPAYMENT") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Paid Amount">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtpaidamt" TabIndex="5" MaxLength="10" runat="server" CssClass=" txtBox_N"
                                Text='<%# Eval("LN_PAID_AMOUNT") %>' Width="95%"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender162" runat="server" ValidChars=".,"
                                FilterType="Numbers,Custom" TargetControlID="txtpaidamt" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtpaidamt" MaxLength="10" TabIndex="5" runat="server" CssClass=" txtBox_N"
                                Width="95%"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender163" runat="server" ValidChars="0"
                                FilterType="Numbers,Custom" TargetControlID="txtpaidamt" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblpaidamt" runat="server" Text='<%# Eval("LN_PAID_AMOUNT") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Distribution Status">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddldistributionstatus" runat="server" CssClass="ddlStype" Width="150px">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddldistributionstatus" runat="server" CssClass="ddlStype" Width="150px"
                                TabIndex="6">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lbldistributionstatus" runat="server" Text='<%# Eval("LN_DISTRIBUTION_STATUS_DESC") %>'
                                Width="150px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Profit Share Type">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlpstype" runat="server" CssClass="ddlStype" Width="150px">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlpstype" runat="server" CssClass="ddlStype" Width="150px"
                                TabIndex="6">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblpstype" runat="server" Text='<%# Eval("LN_PS_TYPE_DESC") %>' Width="150px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Active">
                        <EditItemTemplate>
                            <asp:CheckBox ID="chkact" TabIndex="8" runat="server" Checked='<%# Convert.ToBoolean(Eval("ENABLED_FLAG")) %>' />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:CheckBox ID="chkact" TabIndex="8" runat="server" Checked="true" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkact" runat="server" Checked='<%# Convert.ToBoolean(Eval("ENABLED_FLAG")) %>' />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" TabIndex="10" runat="server" AlternateText="Edit"
                                CausesValidation="false" ToolTip="Edit" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                            <asp:ImageButton ID="ibtnDelete" TabIndex="11" runat="server" AlternateText="Delete"
                                CausesValidation="false" ToolTip="Delete" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibtnUpdate" TabIndex="10" runat="server" AlternateText="Update"
                                CommandName="Update" ToolTip="Update" Height="20px" ImageUrl="~/Images/Update.png" />
                            <asp:ImageButton ID="ibtnCancel" TabIndex="11" runat="server" AlternateText="Cancel"
                                CausesValidation="false" ToolTip="Cancel" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="ibtnInsert" TabIndex="9" runat="server" AlternateText="Add"
                                CommandName="FooterInsert" ToolTip="Add" ImageUrl="~/Images/Add.jpg" />
                        </FooterTemplate>
                        <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divClear_10">
        </div>
        </br>
        <div align="left">
            <asp:GridView ID="gv_docsub" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                DataKeyNames="DOC_ID,DELETED" OnRowCancelingEdit="gv_docsub_RowCancelingEdit"
                OnRowCommand="gv_docsub_RowCommand" OnRowCreated="gv_docsub_RowCreated" OnRowDataBound="gv_docsub_RowDataBound"
                OnRowDeleting="gv_docsub_RowDeleting" OnRowEditing="gv_docsub_RowEditing" OnRowUpdating="gv_docsub_RowUpdating"
                ShowFooter="True">
                <Columns>
                    <asp:TemplateField HeaderText="Document Type">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtdoctyp" TabIndex="5" MaxLength="10" runat="server" CssClass=" txtBox"
                                Text='<%# Eval("DOC_TYPE") %>' Width="95%"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtdoctyp" MaxLength="10" TabIndex="5" runat="server" CssClass=" txtBox"
                                Width="95%"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lbldoctyp" runat="server" Text='<%# Eval("DOC_TYPE") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Document Description">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtDocdescription" TabIndex="5" MaxLength="10" runat="server" CssClass=" txtBox"
                                Text='<%# Eval("DOC_DESC") %>' Width="95%"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtDocdescription" MaxLength="10" TabIndex="5" runat="server" CssClass=" txtBox"
                                Width="95%"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblDocdescription" runat="server" Text='<%# Eval("DOC_DESC") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Active">
                        <EditItemTemplate>
                            <asp:CheckBox ID="chkDSACT" TabIndex="8" runat="server" Checked='<%# Convert.ToBoolean(Eval("ENABLED_FLAG")) %>' />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:CheckBox ID="chkDSACT" TabIndex="8" runat="server" Checked="true" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkDSACT" runat="server" Checked='<%# Convert.ToBoolean(Eval("ENABLED_FLAG")) %>' />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" TabIndex="10" runat="server" AlternateText="Edit"
                                CausesValidation="false" ToolTip="Edit" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                            <asp:ImageButton ID="ibtnDelete" TabIndex="11" runat="server" AlternateText="Delete"
                                CausesValidation="false" ToolTip="Delete" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibtnUpdate" TabIndex="10" runat="server" AlternateText="Update"
                                CommandName="Update" ToolTip="Update" Height="20px" ImageUrl="~/Images/Update.png" />
                            <asp:ImageButton ID="ibtnCancel" TabIndex="11" runat="server" AlternateText="Cancel"
                                CausesValidation="false" ToolTip="Cancel" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="ibtnInsert" TabIndex="9" runat="server" AlternateText="Add"
                                CommandName="FooterInsert" ToolTip="Add" ImageUrl="~/Images/Add.jpg" />
                        </FooterTemplate>
                        <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                            TabIndex="4" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" OnClick="btnDelete_Click"
                            TabIndex="5" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="6" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="7" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table>
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/LN/LNChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
