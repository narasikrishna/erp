﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.AP;
using FIN.BLL;
using FIN.BLL.GL;
using FIN.BLL.AP;
using FIN.BLL.HR;
using FIN.BLL.CA;
using FIN.BLL.LOAN;
using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;

namespace FIN.Client.LOAN
{
    public partial class PropertyRevaluationEntry : PageBase
    {
        LN_PROPERTY_REVALUATION_HDR LN_PROP_REVAL_HDR = new LN_PROPERTY_REVALUATION_HDR();
        LN_PROPERTY_REVALUATION_DTL LN_PROP_REVAL_DTL = new LN_PROPERTY_REVALUATION_DTL();
        string ProReturn = null;
        string ProReturn_Valdn = null;
        DataTable dtGridData = new DataTable();
        Boolean bol_rowVisiable;

        Boolean savedBool = false;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();

        }
        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();

                EntityData = null;

                dtGridData = FIN.BLL.LOAN.PropertyRevaluationBLL.getRevalPropDetails(Master.StrRecordId);
                BindGrid(dtGridData);
                imgBtnPost.Visible = false;
                imgBtnJVPrint.Visible = false;
                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    using (IRepository<LN_PROPERTY_REVALUATION_HDR> userCtx = new DataRepository<LN_PROPERTY_REVALUATION_HDR>())
                    {
                        LN_PROP_REVAL_HDR = userCtx.Find(r =>
                            (r.REVAL_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = LN_PROP_REVAL_HDR;
                    txtvaluation.Text = LN_PROP_REVAL_HDR.REVAL_ID;
                    ddlPropertyID.SelectedValue = LN_PROP_REVAL_HDR.REVAL_PROPERTY_ID;
                    ddlRevalOrgType.SelectedValue = LN_PROP_REVAL_HDR.REVAL_ORG_TYPE;
                    txtPerOrg.Text = LN_PROP_REVAL_HDR.REV_PERSON;

                    imgBtnJVPrint.Visible = false;
                    if (LN_PROP_REVAL_HDR.WORKFLOW_COMPLETION_STATUS == "1")
                    {
                        imgBtnPost.Visible = true;
                        //lblPosted.Visible = false;
                        //lblCancelled.Visible = false;
                    }
                    if (LN_PROP_REVAL_HDR.POSTED_FLAG == FINAppConstants.Y)
                    {
                        imgBtnPost.Visible = false;
                        imgBtnJVPrint.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    LN_PROP_REVAL_HDR = (LN_PROPERTY_REVALUATION_HDR)EntityData;
                }


               
                LN_PROP_REVAL_HDR.REVAL_PROPERTY_ID = ddlPropertyID.SelectedValue.ToString();
                LN_PROP_REVAL_HDR.REVAL_ORG_TYPE = ddlRevalOrgType.SelectedValue.ToString();
                if (txtPerOrg.Text != string.Empty)
                {
                    LN_PROP_REVAL_HDR.REV_PERSON = txtPerOrg.Text.ToString();
                }

                LN_PROP_REVAL_HDR.ENABLED_FLAG = chkActive.Checked == true ? FINAppConstants.EnabledFlag : FINAppConstants.DisabledFlag;

             
                LN_PROP_REVAL_HDR.REVAL_ORG_ID = VMVServices.Web.Utils.OrganizationID;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    LN_PROP_REVAL_HDR.MODIFIED_BY = this.LoggedUserName;
                    LN_PROP_REVAL_HDR.MODIFIED_DATE = DateTime.Today;

                }
                else
                {
                    LN_PROP_REVAL_HDR.REVAL_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.LN_004_M.ToString(), false, true);

                    LN_PROP_REVAL_HDR.CREATED_BY = this.LoggedUserName;
                    LN_PROP_REVAL_HDR.CREATED_DATE = DateTime.Today;

                }
                LN_PROP_REVAL_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, LN_PROP_REVAL_HDR.REVAL_ID);

           


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                ErrorCollection = CommonUtils.IsEmptyGrid(dtGridData, "Account Structure ");
                if (ErrorCollection.Count > 0)
                {
                    return;
                }


                var tmpChildEntity = new List<Tuple<object, string>>();

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    LN_PROP_REVAL_DTL = new LN_PROPERTY_REVALUATION_DTL();
                    if (dtGridData.Rows[iLoop]["REVAL_DTL_ID"].ToString() != "0")
                    {
                        using (IRepository<LN_PROPERTY_REVALUATION_DTL> userCtx = new DataRepository<LN_PROPERTY_REVALUATION_DTL>())
                        {
                            LN_PROP_REVAL_DTL = userCtx.Find(r =>
                                (r.REVAL_DTL_ID == dtGridData.Rows[iLoop]["REVAL_DTL_ID"].ToString())
                                ).SingleOrDefault();
                        }
                    }

                    LN_PROP_REVAL_DTL.REVAL_ID = LN_PROP_REVAL_HDR.REVAL_ID.ToString();

                    if (dtGridData.Rows[iLoop]["REVAL_DT"] != DBNull.Value)
                    {
                        LN_PROP_REVAL_DTL.REVAL_DT = DateTime.Parse(dtGridData.Rows[iLoop]["REVAL_DT"].ToString());
                    }
                    LN_PROP_REVAL_DTL.REVAL_TYPE = dtGridData.Rows[iLoop]["REVAL_TYPE"].ToString();
                    LN_PROP_REVAL_DTL.REVAL_CHARGE = decimal.Parse(dtGridData.Rows[iLoop]["REVAL_CHARGE"].ToString());
                    LN_PROP_REVAL_DTL.REVAL_REMARKS = dtGridData.Rows[iLoop]["REVAL_REMARKS"].ToString();



                    LN_PROP_REVAL_DTL.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;




                    if (dtGridData.Rows[iLoop][FINColumnConstants.ENABLED_FLAG].ToString().ToUpper() == "TRUE")
                    {
                        LN_PROP_REVAL_DTL.ENABLED_FLAG = FINAppConstants.Y;
                    }
                    else
                    {
                        LN_PROP_REVAL_DTL.ENABLED_FLAG = FINAppConstants.N;
                    }

                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {

                        tmpChildEntity.Add(new Tuple<object, string>(LN_PROP_REVAL_DTL, "D"));
                    }
                    else
                    {
                        if (dtGridData.Rows[iLoop]["REVAL_DTL_ID"].ToString() != "0")
                        {
                            LN_PROP_REVAL_DTL.REVAL_DTL_ID = dtGridData.Rows[iLoop]["REVAL_DTL_ID"].ToString();
                            LN_PROP_REVAL_DTL.MODIFIED_BY = this.LoggedUserName;
                            LN_PROP_REVAL_DTL.MODIFIED_DATE = DateTime.Today;

                            tmpChildEntity.Add(new Tuple<object, string>(LN_PROP_REVAL_DTL, "U"));


                        }
                        else
                        {

                            LN_PROP_REVAL_DTL.REVAL_DTL_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.LN_004_D.ToString(), false, true);
                            LN_PROP_REVAL_DTL.CREATED_BY = this.LoggedUserName;
                            LN_PROP_REVAL_DTL.CREATED_DATE = DateTime.Today;

                            tmpChildEntity.Add(new Tuple<object, string>(LN_PROP_REVAL_DTL, "A"));
                        }
                    }

                }


                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            CommonUtils.SavePCEntity<LN_PROPERTY_REVALUATION_HDR, LN_PROPERTY_REVALUATION_DTL>(LN_PROP_REVAL_HDR, tmpChildEntity, LN_PROP_REVAL_DTL);
                            savedBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {
                            CommonUtils.SavePCEntity<LN_PROPERTY_REVALUATION_HDR, LN_PROPERTY_REVALUATION_DTL>(LN_PROP_REVAL_HDR, tmpChildEntity, LN_PROP_REVAL_DTL, true);
                            savedBool = true;
                            break;
                        }
                }




            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void FillComboBox()
        {
            Lookup_BLL.GetLookUpValues(ref ddlRevalOrgType, "REVAL_ORG_TYPE");
            PropertyRevaluationBLL.GetPropertyID(ref ddlPropertyID);

        }


        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddlRevalType = tmpgvr.FindControl("ddlRevalType") as DropDownList;
                Lookup_BLL.GetLookUpValues(ref ddlRevalType, "REVAL_TYPE");


                if (gvData.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                {
                    ddlRevalType.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.REVAL_TYPE].ToString();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                AssignToBE();

                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion
        protected void imgBtnPost_Click(object sender, ImageClickEventArgs e)
        {
            try
            {

                if (EntityData != null)
                {
                    LN_PROP_REVAL_HDR = (LN_PROPERTY_REVALUATION_HDR)EntityData;
                }
                if (LN_PROP_REVAL_HDR.WORKFLOW_COMPLETION_STATUS == "1")
                {
                    FINSP.GetSP_GL_Posting(LN_PROP_REVAL_HDR.REVAL_ID, "AP_021");

                }

                LN_PROP_REVAL_HDR = PropertyRevaluationBLL.getClassEntity(Master.StrRecordId);
                LN_PROP_REVAL_HDR.POSTED_FLAG = FINAppConstants.Y;
                LN_PROP_REVAL_HDR.POSTED_DATE = DateTime.Now;
                DBMethod.SaveEntity<LN_PROPERTY_REVALUATION_HDR>(LN_PROP_REVAL_HDR, true);

                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.POSTED);


                imgBtnJVPrint.Visible = true;
                btnSave.Visible = false;

                //pnlgridview.Enabled = false;
                //pnltdHeader.Enabled = false;
                //pnlReason.Enabled = false;
                //pnlRejReason.Enabled = false;

                ErrorCollection.Clear();

                Hashtable htParameters = new Hashtable();
                Hashtable htHeadingParameters = new Hashtable();
                Hashtable htFilterParameter = new Hashtable();


                ReportFile = "GL_REPORTS//RPTJournalVoucher.rpt";// "../Reports/GL/RPTJournalVoucher.rpt";


                ReportData = DBMethod.ExecuteQuery(FIN.DAL.GL.JournalEntry_DAL.GetJournalVoucherDetailsRPT("", LN_PROP_REVAL_HDR.JE_HDR_ID));


                Session["ProgramName"] = "Journal Voucher";
                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());



                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;


                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('../RPTCrystalReportViewer.aspx?Id?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("posting", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    imgBtnPost.Visible = true;
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        protected void imgBtnJVPrint_Click(object sender, ImageClickEventArgs e)
        {
            LN_PROP_REVAL_HDR = PropertyRevaluationBLL.getClassEntity(Master.StrRecordId);

            if (LN_PROP_REVAL_HDR != null)
            {
                Hashtable htParameters = new Hashtable();
                Hashtable htHeadingParameters = new Hashtable();
                Hashtable htFilterParameter = new Hashtable();

                ReportFile = "GL_REPORTS//RPTJournalVoucher.rpt";// "../Reports/GL/RPTJournalVoucher.rpt";


                ReportData = DBMethod.ExecuteQuery(FIN.DAL.GL.JournalEntry_DAL.GetJournalVoucherDetailsRPT("", LN_PROP_REVAL_HDR.JE_HDR_ID));


                Session["ProgramName"] = "Journal Voucher";
                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());



                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;


                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('../RPTCrystalReportViewer.aspx?Id?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);

            }
        }

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr["ENABLED_FLAG"] = "FALSE";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountsTRUCTEntry_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            if (Session[FINSessionConstants.GridData] != null)
            {
                dtGridData = (DataTable)Session[FINSessionConstants.GridData];
            }
            gvData.EditIndex = -1;

            BindGrid(dtGridData);

        }
        #endregion

        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AS_RC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AS_RCR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }


        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (bol_rowVisiable)
                    e.Row.Visible = false;

                if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                {
                    e.Row.Visible = false;
                }

            }
        }

        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                dtGridData.Rows[e.RowIndex][FINColumnConstants.DELETED] = FINAppConstants.Y;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AS_Row_del", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Row_Edt", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>



        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                //else if (FacilityMasterBLL.ErrorCollection.Count > 0)
                //{
                //    ErrorCollection = FacilityMasterBLL.ErrorCollection;
                //    return;
                //}
                else
                {
                    gvData.EditIndex = -1;
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ACS_Row_upd", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {


            System.Collections.SortedList slControls = new System.Collections.SortedList();
            DropDownList ddlRevalType = gvr.FindControl("ddlRevalType") as DropDownList;
            CheckBox chkact = gvr.FindControl("chkact") as CheckBox;
            TextBox txtRevDate = gvr.FindControl("txtRevalDate") as TextBox;
            TextBox txtRevalCharge = gvr.FindControl("txtRevalCharge") as TextBox;
            TextBox txtRemarks = gvr.FindControl("txtRemarks") as TextBox;


            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList["REVAL_DTL_ID"] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }


            slControls[0] = txtRevDate;
            slControls[1] = ddlRevalType;
            slControls[2] = txtRevalCharge;



            ErrorCollection.Clear();
            string strCtrlTypes = "DateTime~DropDownList~TextBox";
            string strMessage = "Revaluation Date ~ Revaluation Type ~ Revaluation Charge";

            EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

            if (EmptyErrorCollection.Count > 0)
            {
                ErrorCollection = EmptyErrorCollection;
                return drList;
            }



            string strCondition = "REVAL_TYPE='" + ddlRevalType.SelectedValue.ToString() + "'";
            strMessage = FINMessageConstatns.RevalType;
            ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            if (ErrorCollection.Count > 0)
            {

                return drList;
            }


            if (txtRevDate.Text.ToString().Length > 0)
            {
                drList["REVAL_DT"] = DBMethod.ConvertStringToDate(txtRevDate.Text.ToString());
            }


            drList[FINColumnConstants.REVAL_TYPE] = ddlRevalType.SelectedValue.ToString();
            drList[FINColumnConstants.REVAL_TYPE] = ddlRevalType.SelectedItem.Text;

            drList["REVAL_CHARGE"] = txtRevalCharge.Text;
            drList["REVAL_REMARKS"] = txtRemarks.Text;


            if (chkact.Checked)
            {
                drList[FINColumnConstants.ENABLED_FLAG] = "TRUE";
            }
            else
            {
                drList[FINColumnConstants.ENABLED_FLAG] = "FALSE";
            }
            drList[FINColumnConstants.DELETED] = FINAppConstants.N;


            return drList;

        }



        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                DBMethod.DeleteEntity<LN_PROPERTY_REVALUATION_HDR>(LN_PROP_REVAL_HDR);
                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATADELETED);
                //DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {

        }




    }
}