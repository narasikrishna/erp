﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="PrincipalRepayPlan.aspx.cs" Inherits="FIN.Client.LOAN.LoanRepayPlanParam" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 800px" id="divMainContainer">
        <div class="divRowContainer" style="display: none">
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblRequestId">
                Loan Repay Id
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 450px">
                <asp:TextBox ID="txtLoanRepayId" CssClass="txtBox" runat="server" Enabled="false"
                    TabIndex="1"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
           
            <div class="lblBox  LNOrient" style=" width: 150px" id="Div3">
                Contract Number
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 455px">
                <asp:DropDownList ID="ddlContractNumber" runat="server" TabIndex="2" CssClass="validate[required] RequiredField ddlStype"
                    AutoPostBack="True" OnSelectedIndexChanged="ddlContractNumber_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <asp:HiddenField ID="hdnFldFromDate" runat="server" />
        <asp:HiddenField ID="hdnFldToDate" runat="server" />
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="Div2">
                Loan Property
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 455px">
                <asp:DropDownList ID="ddlLoanProperty" runat="server" TabIndex="1" CssClass="validate[required] RequiredField ddlStype"
                    Enabled="false">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="Div16">
                Loan Amount
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtLoanAmt" runat="server" TabIndex="3" CssClass="EntryFont txtBox_N"
                    Width="150px" FilterType="Numbers,Custom" ValidChars=".-" Enabled="False"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Numbers,Custom"
                    ValidChars=",.-" TargetControlID="txtLoanAmt" />
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox  LNOrient" style=" width: 140px" id="Div5">
                Loan Paid Amount
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtLoanPaidAmt" runat="server" TabIndex="4" CssClass="EntryFont txtBox_N"
                    Width="150px" FilterType="Numbers,Custom" ValidChars=".-"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Numbers,Custom"
                    ValidChars=",.-" TargetControlID="txtLoanPaidAmt" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="Div6">
                Repay Type
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:DropDownList ID="ddlRepayType" runat="server" TabIndex="5" CssClass="validate[required] RequiredField ddlStype"
                    AutoPostBack="True" OnSelectedIndexChanged="ddlRepayType_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox  LNOrient" style=" width: 140px" id="Div7">
                Repay Period Type
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 155px">
                <asp:DropDownList ID="ddlRepayPeriodType" runat="server" TabIndex="6" CssClass="validate[required] RequiredField ddlStype"
                    AutoPostBack="True">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 500px">
                <div align="right">
                    <asp:Button ID="btnProcess" runat="server" Text="Process" Visible="false" OnClick="btnProcess_Click"
                        TabIndex="7" />
                </div>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" align="left" style="width: 750px;">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                DataKeyNames="ln_prn_repay_id,ln_prn_repay_dtl_id,DELETED" OnRowCancelingEdit="gvData_RowCancelingEdit"
                OnRowCommand="gvData_RowCommand" OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound"
                OnRowDeleting="gvData_RowDeleting" OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating"
                ShowFooter="True">
                <Columns>
                    <asp:TemplateField HeaderText="Installment Number">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtInstallmentNo" Width="180px" MaxLength="50" runat="server" TabIndex="16"
                                CssClass="RequiredField   txtBox_N" Text='<%# Eval("ln_installment_no") %>'></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="Filteretener50" runat="server" FilterType="Numbers,Custom"
                                ValidChars=".," TargetControlID="txtInstallmentNo" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtInstallmentNo" Width="180px" MaxLength="50" runat="server" CssClass="RequiredField   txtBox_N"
                                TabIndex="8"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="Filteretener51" runat="server" FilterType="Numbers,Custom"
                                ValidChars=".," TargetControlID="txtInstallmentNo" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblInstallmentNo" CssClass="adminFormFieldHeading" runat="server"
                                Text='<%# Eval("ln_installment_no") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Installment Date">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtInstallmentDate" MaxLength="10" runat="server" CssClass="RequiredField EntryFont  txtBox"
                                TabIndex="17" Text='<%#  Eval("ln_installment_dt","{0:dd/MM/yyyy}") %>' Width="130px"
                                Onkeypress="return DateKeyCheck(event,this);"></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender8" runat="server" Format="dd/MM/yyyy" TargetControlID="txtInstallmentDate">
                            </cc2:CalendarExtender>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" ValidChars="/"
                                FilterType="Numbers,Custom" TargetControlID="txtInstallmentDate" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtInstallmentDate" TabIndex="9" MaxLength="10" runat="server" CssClass="RequiredField EntryFont  txtBox"
                                Onkeypress="return DateKeyCheck(event,this);" Width="130px"></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender9" runat="server" Format="dd/MM/yyyy" TargetControlID="txtInstallmentDate">
                            </cc2:CalendarExtender>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" ValidChars="/"
                                FilterType="Numbers,Custom" TargetControlID="txtInstallmentDate" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblStartDate" Width="100px" runat="server" Text='<%# Eval("ln_installment_dt","{0:dd/MM/yyyy}") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Installment Amount">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtInstAmt" Width="180px" MaxLength="50" runat="server" CssClass="RequiredField   txtBox_N"
                                Text='<%# Eval("ln_amount") %>' TabIndex="18"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="Filteretener28" runat="server" FilterType="Numbers,Custom"
                                ValidChars=".," TargetControlID="txtInstAmt" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtInstAmt" Width="180px" MaxLength="50" runat="server" CssClass="RequiredField   txtBox_N"
                                TabIndex="10"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="Filtdderetener29" runat="server" FilterType="Numbers,Custom"
                                ValidChars=".," TargetControlID="txtInstAmt" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblinstallAmt" CssClass="adminFormFieldHeading" runat="server" Text='<%# Eval("ln_amount") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Profit Show Amount" Visible="false">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtProfitShowAmt" Width="180px" MaxLength="50" runat="server" CssClass="RequiredField   txtBox_N"
                                AutoPostBack="true" Text='<%# Eval("LN_PS_AMOUNT") %>' TabIndex="19"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="Filteretener30" runat="server" FilterType="Numbers,Custom"
                                ValidChars=".," TargetControlID="txtProfitShowAmt" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtProfitShowAmt" Width="180px" MaxLength="50" runat="server" CssClass="RequiredField   txtBox_N"
                                AutoPostBack="true" TabIndex="11"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="Filtdderetener31" runat="server" FilterType="Numbers,Custom"
                                ValidChars=".," TargetControlID="txtProfitShowAmt" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblProfitShowAmt" CssClass="adminFormFieldHeading" runat="server"
                                Text='<%# Eval("LN_PS_AMOUNT") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Active">
                        <EditItemTemplate>
                            <asp:CheckBox ID="chkGridActive" runat="server" Checked='<%# Convert.ToBoolean(Eval("ENABLED_FLAG")) %>'
                                TabIndex="20" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:CheckBox ID="chkGridActive" runat="server" Checked="True" TabIndex="12" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkGridActive" runat="server" Checked='<%# Convert.ToBoolean(Eval("ENABLED_FLAG")) %>'
                                TabIndex="13" />
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" runat="server" AlternateText="Edit" CausesValidation="false"
                                TabIndex="14" ToolTip="Edit" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                            <asp:ImageButton ID="ibtnDelete" runat="server" AlternateText="Delete" CausesValidation="false"
                                TabIndex="15" ToolTip="Delete" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibtnUpdate" runat="server" AlternateText="Update" CommandName="Update"
                                TabIndex="21" ToolTip="Update" Height="20px" ImageUrl="~/Images/Update.png" />
                            <asp:ImageButton ID="ibtnCancel" runat="server" AlternateText="Cancel" CausesValidation="false"
                                TabIndex="22" ToolTip="Cancel" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="ibtnInsert" runat="server" AlternateText="Add" CommandName="FooterInsert"
                                TabIndex="13" ToolTip="Add" ImageUrl="~/Images/Add.jpg" />
                        </FooterTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table>
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" CssClass="button" ID="btnYes" Text="Yes" Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/LN/LNChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
