﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.BLL;
using FIN.BLL.HR;
using VMVServices.Web;
using Microsoft.VisualBasic;

namespace FIN.Client.LOAN
{
    public partial class LoanDetailsParam : PageBase
    {

        LN_INTEREST_HDR lN_INTEREST_HDR = new LN_INTEREST_HDR();
        LN_INTEREST_DTL lN_INTEREST_DTL = new LN_INTEREST_DTL();
        SSM_DOCUMENTS sSM_DOCUMENTS = new SSM_DOCUMENTS();
        string ProReturn = null;

        Competency_BLL Competency_BLL = new Competency_BLL();

        DataTable dtGridData = new DataTable();
        DataTable dtGrid2Data = new DataTable();
        Boolean bol_rowVisiable;
        Boolean saveBool;
        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("LD_PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));


            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
        }

        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>
        /// 

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void FillComboBox()
        {
            FIN.BLL.LOAN.Facility_BLL.fn_GetFacilityName(ref ddlFacilityName);
            FIN.BLL.LOAN.PropertyRevaluationBLL.GetRequestID(ref ddlPropertyName);
            Lookup_BLL.GetLookUpValues(ref ddlRepayPeriodType, "PY_PRD_TYPE");
            FIN.BLL.LOAN.Contract_BLL.fn_getContractNumber(ref ddlContractNumber);

        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();
                EntityData = null;

                //  txtEndDate.Text = DBMethod.ConvertDateToString(DateTime.Now.ToShortDateString());

                dtGridData = FIN.BLL.LOAN.Interest_BLL.getInterestDetails(Master.StrRecordId);
                BindGrid(dtGridData);
                dtGrid2Data = DBMethod.ExecuteQuery(FIN.DAL.LOAN.LoanDetails_DAL.GetDocumentdtls(Master.StrRecordId)).Tables[0];
                BindGrid2(dtGrid2Data);


                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    using (IRepository<LN_INTEREST_HDR> userCtx = new DataRepository<LN_INTEREST_HDR>())
                    {
                        lN_INTEREST_HDR = userCtx.Find(r =>
                            (r.LN_INT_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = lN_INTEREST_HDR;

                    ddlContractNumber.SelectedValue = lN_INTEREST_HDR.LN_CONTRACT_DTL_ID;
                    ddlPropertyName.SelectedValue = lN_INTEREST_HDR.LN_PROPERTY_ID;

                    ddlRepayPeriodType.SelectedValue = lN_INTEREST_HDR.LN_INT_TYPE;
                    GetLoanAmount();

                    ddlFacilityName.Enabled = false;
                    ddlPropertyName.Enabled = false;
                    ddlContractNumber.Enabled = false;
                    btnGenerateInstall.Enabled = false;
                    ddlRepayPeriodType.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("LD_ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    lN_INTEREST_HDR = (LN_INTEREST_HDR)EntityData;
                }

                if (ddlFacilityName.SelectedValue.ToString().Length > 0)
                {
                    lN_INTEREST_HDR.LN_FACILITY_ID = ddlFacilityName.SelectedValue;
                }
                lN_INTEREST_HDR.LN_PROPERTY_ID = ddlPropertyName.SelectedValue;
                lN_INTEREST_HDR.LN_CONTRACT_DTL_ID = ddlContractNumber.SelectedValue;
                lN_INTEREST_HDR.LN_INT_TYPE = ddlRepayPeriodType.SelectedValue;
                lN_INTEREST_HDR.LN_ORG_ID = VMVServices.Web.Utils.OrganizationID;
                lN_INTEREST_HDR.ENABLED_FLAG = FINAppConstants.Y;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    lN_INTEREST_HDR.MODIFIED_BY = this.LoggedUserName;
                    lN_INTEREST_HDR.MODIFIED_DATE = DateTime.Today;

                }
                else
                {
                    lN_INTEREST_HDR.LN_INT_ID = FINSP.GetSPFOR_SEQCode("LN_002_M".ToString(), false, true);

                    lN_INTEREST_HDR.CREATED_BY = this.LoggedUserName;
                    lN_INTEREST_HDR.CREATED_DATE = DateTime.Today;

                }
                lN_INTEREST_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, lN_INTEREST_HDR.LN_INT_ID);

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }


                var tmpChildEntity = new List<Tuple<object, string>>();

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    lN_INTEREST_DTL = new LN_INTEREST_DTL();
                    if (dtGridData.Rows[iLoop]["LN_INT_DTL_ID"].ToString() != "0" && dtGridData.Rows[iLoop]["LN_INT_DTL_ID"].ToString() != string.Empty)
                    {
                        using (IRepository<LN_INTEREST_DTL> userCtx = new DataRepository<LN_INTEREST_DTL>())
                        {
                            lN_INTEREST_DTL = userCtx.Find(r =>
                                (r.LN_INT_DTL_ID == dtGridData.Rows[iLoop]["LN_INT_DTL_ID"].ToString())
                                ).SingleOrDefault();
                        }
                    }
                    // lN_INTEREST_DTL.COM_LINE_NUM = int.Parse(dtGridData.Rows[iLoop]["COM_LINE_NUM"].ToString());

                    lN_INTEREST_DTL.LN_INSTALLMENT_NO = int.Parse(dtGridData.Rows[iLoop]["LN_INSTALLMENT_NO"].ToString());
                    if (dtGridData.Rows[iLoop]["LN_INSTALLMENT_DATE"] != DBNull.Value)
                    {
                        lN_INTEREST_DTL.LN_INSTALLMENT_DATE = DateTime.Parse(dtGridData.Rows[iLoop]["LN_INSTALLMENT_DATE"].ToString());
                    }
                    lN_INTEREST_DTL.LN_PROFIT_SHARE_AMT = decimal.Parse(dtGridData.Rows[iLoop]["LN_PROFIT_SHARE_AMT"].ToString());
                    lN_INTEREST_DTL.LN_REV_PROFIT_SHARE_AMT = decimal.Parse(dtGridData.Rows[iLoop]["LN_PROFIT_SHARE_AMT"].ToString());

                    lN_INTEREST_DTL.LN_INT_ID = lN_INTEREST_HDR.LN_INT_ID;
                    lN_INTEREST_DTL.WORKFLOW_COMPLETION_STATUS = "1";

                    if (dtGridData.Rows[iLoop][FINColumnConstants.ENABLED_FLAG].ToString().ToUpper() == "TRUE")
                    {
                        lN_INTEREST_DTL.ENABLED_FLAG = FINAppConstants.Y;
                    }
                    else
                    {
                        lN_INTEREST_DTL.ENABLED_FLAG = FINAppConstants.N;
                    }

                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        if (dtGridData.Rows[iLoop]["LN_INT_DTL_ID"].ToString() != "0" && dtGridData.Rows[iLoop]["LN_INT_DTL_ID"].ToString() != string.Empty)
                        {
                            tmpChildEntity.Add(new Tuple<object, string>(lN_INTEREST_DTL, "D"));
                        }
                    }
                    else
                    {


                        if (dtGridData.Rows[iLoop]["LN_INT_DTL_ID"].ToString() != "0" && dtGridData.Rows[iLoop]["LN_INT_DTL_ID"].ToString() != string.Empty)
                        {                           
                            lN_INTEREST_DTL.MODIFIED_BY = this.LoggedUserName;
                            lN_INTEREST_DTL.MODIFIED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(lN_INTEREST_DTL, "U"));

                        }
                        else
                        {

                            lN_INTEREST_DTL.LN_INT_DTL_ID = FINSP.GetSPFOR_SEQCode("LN_002_D".ToString(), false, true);
                            lN_INTEREST_DTL.CREATED_BY = this.LoggedUserName;
                            lN_INTEREST_DTL.CREATED_DATE = DateTime.Today;                            
                            tmpChildEntity.Add(new Tuple<object, string>(lN_INTEREST_DTL, "A"));
                        }
                    }

                }

                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            FIN.BLL.HR.Competency_BLL.SavePCEntity<LN_INTEREST_HDR, LN_INTEREST_DTL>(lN_INTEREST_HDR, tmpChildEntity, lN_INTEREST_DTL);
                            saveBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {

                            FIN.BLL.HR.Competency_BLL.SavePCEntity<LN_INTEREST_HDR, LN_INTEREST_DTL>(lN_INTEREST_HDR, tmpChildEntity, lN_INTEREST_DTL, true);
                            saveBool = true;
                            break;

                        }
                }


                if (Session["GridData2"] != null)
                {
                    dtGrid2Data = (DataTable)Session["GridData2"];
                }

                for (int jLoop = 0; jLoop < dtGrid2Data.Rows.Count; jLoop++)
                {
                    sSM_DOCUMENTS = new SSM_DOCUMENTS();
                    if (dtGrid2Data.Rows[jLoop]["DOC_ID"].ToString() != "0" && dtGrid2Data.Rows[jLoop]["DOC_ID"].ToString() != string.Empty)
                    {
                        using (IRepository<SSM_DOCUMENTS> userCtx = new DataRepository<SSM_DOCUMENTS>())
                        {
                            sSM_DOCUMENTS = userCtx.Find(r =>
                                (r.DOC_ID == int.Parse(dtGrid2Data.Rows[jLoop]["DOC_ID"].ToString()))
                                ).SingleOrDefault();
                        }
                    }
                    // lN_INTEREST_DTL.COM_LINE_NUM = int.Parse(dtGridData.Rows[iLoop]["COM_LINE_NUM"].ToString());

                    sSM_DOCUMENTS.DOC_TYPE = dtGrid2Data.Rows[jLoop]["DOC_TYPE"].ToString();
                    sSM_DOCUMENTS.ENTITY_TYPE = "LOAN";
                    sSM_DOCUMENTS.DOC_DESC = dtGrid2Data.Rows[jLoop]["DOC_DESC"].ToString();


                    sSM_DOCUMENTS.ENTITY_ID = lN_INTEREST_HDR.LN_INT_ID;
                    //  lN_INTEREST_DTL.CHILD_ID = lN_INTEREST_HDR.PK_ID;
                    sSM_DOCUMENTS.WORKFLOW_COMPLETION_STATUS = "1";

                    if (dtGrid2Data.Rows[jLoop][FINColumnConstants.ENABLED_FLAG].ToString().ToUpper() == "TRUE")
                    {
                        sSM_DOCUMENTS.ENABLED_FLAG = FINAppConstants.Y;
                    }
                    else
                    {
                        sSM_DOCUMENTS.ENABLED_FLAG = FINAppConstants.N;
                    }

                    if (dtGrid2Data.Rows[jLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {

                        tmpChildEntity.Add(new Tuple<object, string>(sSM_DOCUMENTS, "D"));
                    }
                    else
                    {

                        if (dtGrid2Data.Rows[jLoop]["DOC_ID"].ToString() != "0" && dtGrid2Data.Rows[jLoop]["DOC_ID"].ToString() != string.Empty)
                        {
                            // gL_ACCT_CODE_SEGMENTS.PK_ID = Convert.ToInt32(dtGridData.Rows[iLoop][FINColumnConstants.PK_ID].ToString());
                            sSM_DOCUMENTS.MODIFIED_BY = this.LoggedUserName;
                            sSM_DOCUMENTS.MODIFIED_DATE = DateTime.Today;
                            DBMethod.SaveEntity<SSM_DOCUMENTS>(sSM_DOCUMENTS, true);


                        }
                        else
                        {

                            sSM_DOCUMENTS.DOC_ID = DBMethod.GetPrimaryKeyValue(FINSequenceConstant.SSM_DOCUMENTS_SEQ);
                            sSM_DOCUMENTS.CREATED_BY = this.LoggedUserName;
                            sSM_DOCUMENTS.CREATED_DATE = DateTime.Today;
                            DBMethod.SaveEntity<SSM_DOCUMENTS>(sSM_DOCUMENTS);

                        }
                    }

                }

                if (Master.Mode == FINAppConstants.Add)
                {
                    LN_INSTALLMENT_HISTROY_DTL LN_INSTALLMENT_HISTROY_DTL = new LN_INSTALLMENT_HISTROY_DTL();
                    LN_INSTALLMENT_HISTROY_HDR lN_INSTALLMENT_HISTROY_HDR = new LN_INSTALLMENT_HISTROY_HDR();
                    lN_INSTALLMENT_HISTROY_HDR.LN_INSTALLMENT_NO = 1;
                    lN_INSTALLMENT_HISTROY_HDR.ENABLED_FLAG = FINAppConstants.Y;
                    lN_INSTALLMENT_HISTROY_HDR.CREATED_BY = this.LoggedUserName;
                    lN_INSTALLMENT_HISTROY_HDR.CREATED_DATE = DateTime.Today;
                    lN_INSTALLMENT_HISTROY_HDR.WORKFLOW_COMPLETION_STATUS = FINAppConstants.Y;
                    lN_INSTALLMENT_HISTROY_HDR.LN_INST_HIS_ID = FINSP.GetSPFOR_SEQCode("LN_IHH".ToString(), false, true);
                    tmpChildEntity = new List<Tuple<object, string>>();
                    for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                    {
                        LN_INSTALLMENT_HISTROY_DTL = new LN_INSTALLMENT_HISTROY_DTL();
                        LN_INSTALLMENT_HISTROY_DTL.LN_INST_HIS_ID = lN_INSTALLMENT_HISTROY_HDR.LN_INST_HIS_ID;
                        LN_INSTALLMENT_HISTROY_DTL.LN_INSTALLMENT_NO = int.Parse(dtGridData.Rows[iLoop]["LN_INSTALLMENT_NO"].ToString());
                        if (dtGridData.Rows[iLoop]["LN_INSTALLMENT_DT"] != DBNull.Value)
                        {
                            LN_INSTALLMENT_HISTROY_DTL.LN_INSTALLMENT_DT = DateTime.Parse(dtGridData.Rows[iLoop]["LN_INSTALLMENT_DT"].ToString());
                        }
                        // LN_INSTALLMENT_HISTROY_DTL.LN_PRIN_BALANCE = decimal.Parse(dtGridData.Rows[iLoop]["LN_PRIN_BALANCE"].ToString());
                        LN_INSTALLMENT_HISTROY_DTL.LN_MONTHLY_INT = decimal.Parse(dtGridData.Rows[iLoop]["LN_MONTHLY_INT"].ToString());

                        LN_INSTALLMENT_HISTROY_DTL.LN_BALANCE = decimal.Parse(dtGridData.Rows[iLoop]["LN_BALANCE"].ToString());
                        LN_INSTALLMENT_HISTROY_DTL.LN_INSTALMENT = decimal.Parse(dtGridData.Rows[iLoop]["LN_INSTALMENT"].ToString());
                        LN_INSTALLMENT_HISTROY_DTL.LN_PRINCIPAL_REPAYMENT = decimal.Parse(dtGridData.Rows[iLoop]["LN_PRINCIPAL_REPAYMENT"].ToString());
                        //LN_INSTALLMENT_HISTROY_DTL.LN_PAID_AMOUNT = decimal.Parse(dtGridData.Rows[iLoop]["LN_PAID_AMOUNT"].ToString());



                        LN_INSTALLMENT_HISTROY_DTL.WORKFLOW_COMPLETION_STATUS = "1";

                        if (dtGridData.Rows[iLoop][FINColumnConstants.ENABLED_FLAG].ToString().ToUpper() == "TRUE")
                        {
                            LN_INSTALLMENT_HISTROY_DTL.ENABLED_FLAG = FINAppConstants.Y;
                        }
                        else
                        {
                            LN_INSTALLMENT_HISTROY_DTL.ENABLED_FLAG = FINAppConstants.N;
                        }

                        if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                        {

                            tmpChildEntity.Add(new Tuple<object, string>(LN_INSTALLMENT_HISTROY_DTL, "D"));
                        }
                        else
                        {

                            if (dtGridData.Rows[iLoop]["LN_LOAN_DTL_ID"].ToString() != "0" && dtGridData.Rows[iLoop]["LN_LOAN_DTL_ID"].ToString() != string.Empty)
                            {

                                LN_INSTALLMENT_HISTROY_DTL.MODIFIED_BY = this.LoggedUserName;
                                LN_INSTALLMENT_HISTROY_DTL.MODIFIED_DATE = DateTime.Today;
                                tmpChildEntity.Add(new Tuple<object, string>(LN_INSTALLMENT_HISTROY_DTL, "U"));

                            }
                            else
                            {

                                LN_INSTALLMENT_HISTROY_DTL.LN_INST_HIS_DTL_ID = FINSP.GetSPFOR_SEQCode("LN_IHD".ToString(), false, true);
                                LN_INSTALLMENT_HISTROY_DTL.CREATED_BY = this.LoggedUserName;
                                LN_INSTALLMENT_HISTROY_DTL.CREATED_DATE = DateTime.Today;
                                tmpChildEntity.Add(new Tuple<object, string>(LN_INSTALLMENT_HISTROY_DTL, "A"));
                            }
                        }
                    }

                    FIN.BLL.HR.Competency_BLL.SavePCEntity<LN_INSTALLMENT_HISTROY_HDR, LN_INSTALLMENT_HISTROY_DTL>(lN_INSTALLMENT_HISTROY_HDR, tmpChildEntity, LN_INSTALLMENT_HISTROY_DTL);
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("LN_ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                /*DropDownList ddldistributionstatus = tmpgvr.FindControl("ddldistributionstatus") as DropDownList;
                DropDownList ddlpstype = tmpgvr.FindControl("ddlpstype") as DropDownList;

                Lookup_BLL.GetLookUpValues(ref ddldistributionstatus, "DISTRIBUTION_STATUS");
                Lookup_BLL.GetLookUpValues(ref ddlpstype, "PROFIT_SHR_TYP");


                if (gvData.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                {
                    ddldistributionstatus.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["LN_DISTRIBUTION_STATUS"].ToString();
                    ddlpstype.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["LN_PS_TYPE"].ToString();


                }*/
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("LD_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session[FINSessionConstants.GridData], "Loan ");
                AssignToBE();

                //ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session["GridData2"], "Document ");

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                if (saveBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        #endregion

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;

                if (dtData.Rows.Count > 0)
                {
                    dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("LN_PROFIT_SHARE_AMT", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("LN_PROFIT_SHARE_AMT"))));
                    dtData.AcceptChanges();
                }
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr["ENABLED_FLAG"] = "FALSE";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("LD_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void BindGrid2(DataTable dtData2)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session["GridData2"] = dtData2;
                DataTable dt_tmp = dtData2.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr["ENABLED_FLAG"] = "FALSE";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gv_docsub.DataSource = dt_tmp;
                gv_docsub.DataBind();
                GridViewRow gvr_DS = gv_docsub.FooterRow;
                // FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("LD_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {

            if (Session[FINSessionConstants.GridData] != null)
            {
                dtGridData = (DataTable)Session[FINSessionConstants.GridData];
            }
            gvData.EditIndex = -1;

            BindGrid(dtGridData);
        }

        protected void gv_docsub_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {

            if (Session["GridData2"] != null)
            {
                dtGrid2Data = (DataTable)Session["GridData2"];
            }
            gv_docsub.EditIndex = -1;

            BindGrid2(dtGrid2Data);

        }

        #endregion

        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("LN_ROW_CMD", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void gv_docsub_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr_DS = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session["GridData2"] != null)
                {
                    dtGrid2Data = (DataTable)Session["GridData2"];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr_DS = gv_docsub.FooterRow;
                    if (gvr_DS == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl2(gvr_DS, dtGrid2Data, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGrid2Data.Rows.Add(drList);
                    BindGrid2(dtGrid2Data);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("LN_ROW_CMD", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {


            System.Collections.SortedList slControls = new System.Collections.SortedList();


            TextBox txtinstallmentno = gvr.FindControl("txtinstallmentno") as TextBox;
            TextBox txtinstallmentdate = gvr.FindControl("txtinstallmentdate") as TextBox;
            TextBox txt_LN_MONTHLY_INT = gvr.FindControl("txtLN_MONTHLY_INT") as TextBox;

            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList["LN_LOAN_DTL_ID"] = "0";

            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }

            // txtlineno.Text = (txtlineno.Text + rowindex + 1).ToString();

            slControls[0] = txtinstallmentno;
            slControls[1] = txtinstallmentdate;
            slControls[2] = txtinstallmentdate;
            slControls[3] = txt_LN_MONTHLY_INT;

            ErrorCollection.Clear();
            string strCtrlTypes = "TextBox~TextBox~DateTime~TextBox";

            string strMessage = " Installment No ~ Installment Date ~ Installment Date ~ Installment Amount ";

            EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

            if (EmptyErrorCollection.Count > 0)
            {
                ErrorCollection = EmptyErrorCollection;
                return drList;
            }


            //string strCondition = "LOT_ID='" + ddlLotNo.SelectedValue + "'";
            //strMessage = FINMessageConstatns.RecordAlreadyExists;
            //ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            //if (ErrorCollection.Count > 0)
            //{

            //    return drList;
            //}



            drList["LN_INSTALLMENT_NO"] = txtinstallmentno.Text;
            if (txtinstallmentdate.Text.ToString().Length > 0)
            {
                drList["LN_INSTALLMENT_DATE"] = DBMethod.ConvertStringToDate(txtinstallmentdate.Text.ToString());
            }

            drList["LN_PROFIT_SHARE_AMT"] = txt_LN_MONTHLY_INT.Text;
            drList[FINColumnConstants.ENABLED_FLAG] = "TRUE";
            drList[FINColumnConstants.DELETED] = FINAppConstants.N;


            return drList;

        }

        private DataRow AssignToGridControl2(GridViewRow gvr_DS, DataTable tmpdtGridData, string GMode, int rowindex)
        {


            System.Collections.SortedList slControls = new System.Collections.SortedList();


            TextBox txtdoctyp = gvr_DS.FindControl("txtdoctyp") as TextBox;
            TextBox txtDocdescription = gvr_DS.FindControl("txtDocdescription") as TextBox;


            CheckBox chkDSACT = gvr_DS.FindControl("chkDSACT") as CheckBox;

            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGrid2Data.NewRow();
                drList["DOC_ID"] = "0";

            }
            else
            {
                drList = dtGrid2Data.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }



            //slControls[0] = txtinstallmentno;
            //slControls[1] = txtinstallmentdate;
            //slControls[2] = txtinstallmentdate;
            //slControls[3] = txtLoanamt;
            //slControls[4] = txtpaidamt;
            //slControls[5] = ddldistributionstatus;
            //slControls[6] = ddlpstype;

            //ErrorCollection.Clear();
            //string strCtrlTypes = "TextBox~TextBox~DateTime~TextBox~TextBox~Dropdownlist~Dropdownlist";

            //string strMessage = " Installment No ~ Installment Date ~ Installment Date ~ Loan Amount ~ Paid Amount ~ Distribution Status ~ Profit Share Type";

            //EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

            //if (EmptyErrorCollection.Count > 0)
            //{
            //    ErrorCollection = EmptyErrorCollection;
            //    return drList;
            //}




            drList["DOC_TYPE"] = txtdoctyp.Text;

            drList["DOC_DESC"] = txtDocdescription.Text;


            if (chkDSACT.Checked)
            {
                drList[FINColumnConstants.ENABLED_FLAG] = "TRUE";
            }
            else
            {
                drList[FINColumnConstants.ENABLED_FLAG] = "FALSE";
            }



            drList[FINColumnConstants.DELETED] = FINAppConstants.N;


            return drList;

        }

        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("LD_ROW_UP", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void gv_docsub_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr_DS = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session["GridData2"] != null)
                {
                    dtGrid2Data = (DataTable)Session["GridData2"];
                }
                if (gvr_DS == null)
                {
                    return;
                }

                drList = AssignToGridControl2(gvr_DS, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gv_docsub.EditIndex = -1;
                BindGrid2(dtGrid2Data);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("LD_ROW_UP", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("LD_ROW_DEL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void gv_docsub_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();


                if (Session["GridData2"] != null)
                {
                    dtGrid2Data = (DataTable)Session["GridData2"];
                }
                DataRow drList = null;
                drList = dtGrid2Data.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;

                BindGrid2(dtGrid2Data);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("LD_ROW_DEL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("LD_ROW_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void gv_docsub_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session["GridData2"] != null)
                {
                    dtGrid2Data = (DataTable)Session["GridData2"];
                }
                gv_docsub.EditIndex = e.NewEditIndex;
                BindGrid2(dtGrid2Data);
                GridViewRow gvr_DS = gv_docsub.Rows[e.NewEditIndex];
                // FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("LD_ROW_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("LD_RowCreated", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void gv_docsub_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr_DS = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gv_docsub.Controls[0].Controls.AddAt(0, gvr_DS);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("LD_RowCreated", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("LD_RDB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        protected void gv_docsub_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("LD_RDB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                // COURSE_GROUP.COURSE_GROUP_ID = short.Parse(Master.RecordID.ToString());
                DBMethod.DeleteEntity<LN_INTEREST_HDR>(lN_INTEREST_HDR);
                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATADELETED);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("LD_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {

        }

        private void GenerateProfitShare()
        {
            DataTable dt_LoanReqDtl = DBMethod.ExecuteQuery(FIN.DAL.HR.LoanRequest_DAL.GetLoanRequestDetails("", ddlContractNumber.SelectedValue)).Tables[0];
            DataTable dt_Contract_Det = FIN.BLL.LOAN.LoanRepayPlan_BLL.fn_GetLoanRepayDtls4Contract(ddlContractNumber.SelectedValue);
            if (dt_LoanReqDtl.Rows.Count > 0)
            {
                DateTime compareTo = DateTime.Parse(dt_LoanReqDtl.Rows[0]["ln_end_date"].ToString());
                DateTime now = DateTime.Parse(dt_LoanReqDtl.Rows[0]["ln_start_date"].ToString());

                double dbl_LoanAmount = double.Parse(dt_LoanReqDtl.Rows[0]["ln_principal_amount"].ToString());
                double dbl_Prin_amt = dbl_LoanAmount;
                double dbl_PSPercentage = double.Parse(dt_LoanReqDtl.Rows[0]["ln_roi"].ToString());
                var dateSpan = DateTimeSpan.CompareDates(compareTo, now);
                int int_Tot_Months = dateSpan.Months + 1;
              
                int int_calmonth = 0;
                int int_CalMonth = 0;
                if (ddlRepayPeriodType.SelectedValue.ToString().ToUpper() == "MONTHLY")
                {
                    int_CalMonth = 1;
                }
                else if (ddlRepayPeriodType.SelectedValue.ToString().ToUpper() == "QUARTERLY")
                {
                    int_CalMonth = 3;
                }
                else if (ddlRepayPeriodType.SelectedValue.ToString().ToUpper() == "HALFYEARLY")
                {
                    int_CalMonth = 6;
                }
                else if (ddlRepayPeriodType.SelectedValue.ToString().ToUpper() == "ANNUAL")
                {
                    int_CalMonth = 12;
                }

                int_calmonth = int_Tot_Months / int_CalMonth;

                double dbl_totLoanAmt = double.Parse(txtPrincipalAmt.Text);
                double dbl_MonthAmt = dbl_totLoanAmt / int_calmonth;
                double dbl_PreviousPaidAmt = 0;
                double dbl_PaidLoanAmt = 0;

                DateTime PreviousDt = now;
                double No_ofDays = 0;
                dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                dtGridData.Rows.Clear();
                for (int iLoop = 0; iLoop < int_calmonth; iLoop++)
                {

                    now = now.AddMonths(int_CalMonth);                    
                    DataRow drList;
                    drList = dtGridData.NewRow();
                    drList["LN_INT_DTL_ID"] = "0";

                    drList["LN_INSTALLMENT_NO"] = iLoop + 1;
                    drList["LN_INSTALLMENT_DATE"] = now;


                    dateSpan = DateTimeSpan.CompareDates(now, PreviousDt);
                    No_ofDays = dateSpan.Days;

                    if (iLoop == 0)
                        No_ofDays += 1;

                    drList["LN_PROFIT_SHARE_AMT"] = ((No_ofDays / 365) * ((dbl_LoanAmount * dbl_PSPercentage) / 100)).ToString();

                    string str_Amt = dt_Contract_Det.Compute("sum(LN_AMOUNT)", "Paid_Date<'" + now + "'").ToString();
                    if (str_Amt.Length > 0)
                    {
                        dbl_PaidLoanAmt = double.Parse(str_Amt);
                    }

                    if (dbl_PreviousPaidAmt != dbl_PaidLoanAmt)
                    {
                        dbl_LoanAmount = dbl_Prin_amt - dbl_PaidLoanAmt;
                        dbl_PreviousPaidAmt = dbl_PaidLoanAmt;
                    }
                    if (dbl_LoanAmount <= 0)
                    {
                        break;
                    }


                    drList[FINColumnConstants.ENABLED_FLAG] = "TRUE";

                    drList[FINColumnConstants.DELETED] = FINAppConstants.N;
                    dtGridData.Rows.Add(drList);

                    PreviousDt = now;

                }

                BindGrid(dtGridData);


            }
        }

        protected void btnGenerateInstall_Click(object sender, EventArgs e)
        {

            if (ddlContractNumber.SelectedValue.ToString().Length == 0)
            {
                ErrorCollection.Add("Please Select Contract Number", "Please Select Contract Number");
                return;
            }

            if (ddlRepayPeriodType.SelectedValue.ToString().Length == 0)
            {
                ErrorCollection.Add("Please Select Period Type", "Please Select Period Type");
                return;
            }
            GenerateProfitShare();
            //double installmentAmt = Math.Round(Math.Abs(Financial.Pmt(double.Parse(txtintrustRate.Text) / 100 / 12, double.Parse(txtRepayDur.Text), double.Parse(txtApprloanamt.Text))), int.Parse(VMVServices.Web.Utils.DecimalPrecision));

            //DataTable dt_Installment = new DataTable();

            //dt_Installment.Columns.Add("LN_LOAN_DTL_ID", typeof(string));
            //dt_Installment.Columns.Add("LN_INSTALLMENT_NO", typeof(int));
            //dt_Installment.Columns.Add("LN_INSTALLMENT_DT", typeof(DateTime));
            //dt_Installment.Columns.Add("LN_PRIN_BALANCE", typeof(double));
            //dt_Installment.Columns.Add("LN_MONTHLY_INT", typeof(double));
            //dt_Installment.Columns.Add("LN_BALANCE", typeof(double));
            //dt_Installment.Columns.Add("LN_INSTALMENT", typeof(double));
            //dt_Installment.Columns.Add("LN_PRINCIPAL_REPAYMENT", typeof(double));
            //dt_Installment.Columns.Add("LN_PAID_AMOUNT", typeof(double));
            //dt_Installment.Columns.Add("LN_DISTRIBUTION_STATUS", typeof(string));
            //dt_Installment.Columns.Add("LN_DISTRIBUTION_STATUS_DESC", typeof(string));
            //dt_Installment.Columns.Add("LN_PS_TYPE", typeof(string));
            //dt_Installment.Columns.Add("LN_PS_TYPE_DESC", typeof(string));
            //dt_Installment.Columns.Add("ENABLED_FLAG", typeof(string));
            //dt_Installment.Columns.Add("DELETED", typeof(string));
            //DateTime dt_intDate = DateTime.Now;
            //double dbl_Prin_bal = double.Parse(txtApprloanamt.Text);
            //double dbl_previous_pr = 0;
            //for (int rLoop = 0; rLoop < int.Parse(txtRepayDur.Text); rLoop++)
            //{
            //    DataRow dr = dt_Installment.NewRow();
            //    dr["LN_LOAN_DTL_ID"] = "0";
            //    dr["LN_INSTALLMENT_NO"] = rLoop + 1;
            //    dr["LN_INSTALLMENT_DT"] = dt_intDate.AddMonths(rLoop);
            //    dr["LN_PRIN_BALANCE"] = dbl_Prin_bal;
            //    dr["LN_MONTHLY_INT"] = Math.Round(dbl_Prin_bal * (double.Parse(txtintrustRate.Text) / 100 / 12), int.Parse(VMVServices.Web.Utils.DecimalPrecision));
            //    dr["LN_BALANCE"] = Math.Round(double.Parse(dr["LN_PRIN_BALANCE"].ToString()) + double.Parse(dr["LN_MONTHLY_INT"].ToString()), int.Parse(VMVServices.Web.Utils.DecimalPrecision));
            //    dr["LN_INSTALMENT"] = installmentAmt;
            //    if (rLoop == 0)
            //    {
            //        dr["LN_PRINCIPAL_REPAYMENT"] = Math.Round(double.Parse(dr["LN_PRIN_BALANCE"].ToString()) - (double.Parse(dr["LN_BALANCE"].ToString()) - double.Parse(dr["LN_INSTALMENT"].ToString())), int.Parse(VMVServices.Web.Utils.DecimalPrecision));
            //    }
            //    else
            //    {
            //        dr["LN_PRINCIPAL_REPAYMENT"] = Math.Round(dbl_previous_pr + double.Parse(dr["LN_PRIN_BALANCE"].ToString()) - (double.Parse(dr["LN_BALANCE"].ToString()) - double.Parse(dr["LN_INSTALMENT"].ToString())), int.Parse(VMVServices.Web.Utils.DecimalPrecision));
            //    }
            //    dbl_previous_pr = double.Parse(dr["LN_PRINCIPAL_REPAYMENT"].ToString());
            //    dr["LN_PAID_AMOUNT"] = 0;
            //    dr["LN_DISTRIBUTION_STATUS"] = "";
            //    dr["LN_DISTRIBUTION_STATUS_DESC"] = "";
            //    dr["LN_PS_TYPE"] = "";
            //    dr["LN_PS_TYPE_DESC"] = "";
            //    dr["ENABLED_FLAG"] = "TRUE";
            //    dr["DELETED"] = "N";
            //    dt_Installment.Rows.Add(dr);
            //    dbl_Prin_bal = Math.Round(dbl_Prin_bal + double.Parse(dr["LN_MONTHLY_INT"].ToString()) - double.Parse(dr["LN_INSTALMENT"].ToString()), int.Parse(VMVServices.Web.Utils.DecimalPrecision));
            //}

            //BindGrid(dt_Installment);
        }

        protected void ddlloanrequest_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetLoanAmount();
        }
        private void GetLoanAmount()
        {
            DataTable dt_LoanReqDtl = DBMethod.ExecuteQuery(FIN.DAL.HR.LoanRequest_DAL.GetLoanRequestDetails("", ddlContractNumber.SelectedValue)).Tables[0];
            if (dt_LoanReqDtl.Rows.Count > 0)
            {
                ddlPropertyName.SelectedValue = dt_LoanReqDtl.Rows[0]["ln_request_id"].ToString();
                txtCD.Text = dt_LoanReqDtl.Rows[0]["ln_contract_desc"].ToString();
                txtPrincipalAmt.Text = DBMethod.GetAmtDecimalCommaSeparationValue(dt_LoanReqDtl.Rows[0]["ln_principal_amount"].ToString());
                txtProfitShare.Text = dt_LoanReqDtl.Rows[0]["ln_roi"].ToString();
                txtBankName.Text = dt_LoanReqDtl.Rows[0]["bank_name"].ToString();
                txtContractDate.Text = DBMethod.ConvertDateToString(dt_LoanReqDtl.Rows[0]["ln_contract_dt"].ToString());
            }

            DataTable dt_InterstDtl = DBMethod.ExecuteQuery(FIN.DAL.LOAN.Interest_DAL.getInterestDetails4Contract(ddlContractNumber.SelectedValue)).Tables[0];
            if (dt_InterstDtl.Rows.Count > 0)
            {
                BindGrid(dt_InterstDtl);
                using (IRepository<LN_INTEREST_HDR> userCtx = new DataRepository<LN_INTEREST_HDR>())
                {
                    lN_INTEREST_HDR = userCtx.Find(r =>
                        (r.LN_INT_ID == dt_InterstDtl.Rows[0]["LN_INT_ID"].ToString())
                        ).SingleOrDefault();
                }
                Master.StrRecordId = dt_InterstDtl.Rows[0]["LN_INT_ID"].ToString();
                Master.Mode = FINAppConstants.Update;
                EntityData = lN_INTEREST_HDR;

                ddlContractNumber.SelectedValue = lN_INTEREST_HDR.LN_CONTRACT_DTL_ID;
                ddlPropertyName.SelectedValue = lN_INTEREST_HDR.LN_PROPERTY_ID;
                ddlRepayPeriodType.SelectedValue = lN_INTEREST_HDR.LN_INT_TYPE;

                ddlFacilityName.Enabled = false;
                ddlPropertyName.Enabled = false;
                ddlContractNumber.Enabled = false;
                btnGenerateInstall.Enabled = false;
            }
            else
            {
                BindGrid(dt_InterstDtl);
                EntityData = null;
                Master.StrRecordId = "0";
                Master.Mode = FINAppConstants.Add;
            }
        }

        protected void ddlFacilityName_SelectedIndexChanged(object sender, EventArgs e)
        {
            getPropertyName();
        }
        private void getPropertyName()
        {
            FIN.BLL.LOAN.Facility_BLL.fn_getPorpertyname4Facility(ref ddlPropertyName, ddlFacilityName.SelectedValue.ToString());
        }

        protected void ddlPropertyName_SelectedIndexChanged(object sender, EventArgs e)
        {
            getContractNumber();
        }

        private void getContractNumber()
        {
            FIN.BLL.LOAN.Contract_BLL.fn_GetLoanRequest(ref ddlContractNumber, ddlPropertyName.SelectedValue.ToString(), ddlFacilityName.SelectedValue.ToString());
        }

    }
}