﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.BLL.AP;
using FIN.BLL.HR;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.BLL;
using FIN.BLL.SSM;
using VMVServices.Web;

namespace FIN.Client.LOAN
{
    public partial class FacilityEntry : PageBase
    {

        LN_LOAN_FACILITY_HDR lN_LOAN_FACILITY_HDR = new LN_LOAN_FACILITY_HDR();
        LN_FACILITY_PLEDGE lN_FACILITY_PLEDGE = new LN_FACILITY_PLEDGE();
        System.Collections.SortedList slControls = new System.Collections.SortedList();
        //string ProReturn = null;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }
            UserRightsChecking();

        }

        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }
        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();

                EntityData = null;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    using (IRepository<LN_LOAN_FACILITY_HDR> userCtx = new DataRepository<LN_LOAN_FACILITY_HDR>())
                    {
                        lN_LOAN_FACILITY_HDR = userCtx.Find(r =>
                            (r.LN_FACILITY_ID == Master.StrRecordId)
                            ).SingleOrDefault();

                    }

                    EntityData = lN_LOAN_FACILITY_HDR;
                    
                    txtFacility.Text = lN_LOAN_FACILITY_HDR.LN_FACILITY_ID;
                    txtFacilityName.Text = lN_LOAN_FACILITY_HDR.LN_FACILITY_NAME;
                    txtFacilityNameOL.Text = lN_LOAN_FACILITY_HDR.LN_FACILITY_NAME_OL;
                    ddlFaciltiyType.SelectedValue = lN_LOAN_FACILITY_HDR.LN_FACILITY_TYPE;
                    ddlPropertyType.SelectedValue = lN_LOAN_FACILITY_HDR.LN_PROPERTY_ID;
                    txtFacilityAmount.Text = DBMethod.GetAmtDecimalCommaSeparationValue(lN_LOAN_FACILITY_HDR.LN_FACILITY_AMOUNT.ToString());
                    txtBalanceFacilityAmount.Text = DBMethod.GetAmtDecimalCommaSeparationValue(lN_LOAN_FACILITY_HDR.LN_BALANCE_FACILITY_AMOUNT.ToString());

                    if (lN_LOAN_FACILITY_HDR.START_DT != null)
                    {
                        txtStartDate.Text = DBMethod.ConvertDateToString(lN_LOAN_FACILITY_HDR.START_DT.ToString());
                    }
                    if (lN_LOAN_FACILITY_HDR.EXPIRY_DT != null)
                    {
                        txtExpiryDate.Text = DBMethod.ConvertDateToString(lN_LOAN_FACILITY_HDR.EXPIRY_DT.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>

        private void FillComboBox()
        {
            FIN.BLL.LOAN.Facility_BLL.fn_GetLoanProperty(ref ddlPropertyType);
            Lookup_BLL.GetLookUpValues(ref ddlFaciltiyType, "FACILITY_TYPE");
        }

        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();

                //using (IRepository<LN_LOAN_FACILITY_HDR> userCtx = new DataRepository<LN_LOAN_FACILITY_HDR>())
                //{
                //    lN_LOAN_FACILITY_HDR = userCtx.Find(r =>
                //        (r.LN_FACILITY_ID == Master.StrRecordId)
                //        ).SingleOrDefault();

                //}
                //if (lN_LOAN_FACILITY_HDR != null)
                //{
                //    using (IRepository<LN_FACILITY_PLEDGE> userCtx = new DataRepository<LN_FACILITY_PLEDGE>())
                //    {
                //        lN_FACILITY_PLEDGE = userCtx.Find(r =>
                //            (r.PL_PROPERTY_ID == lN_LOAN_FACILITY_HDR.LN_PROPERTY_ID)
                //            ).SingleOrDefault();
                //    }
                //    if (lN_FACILITY_PLEDGE == null)
                //    {
                //        string errMsg = "Property ID not found in Pledge. Do you want to save the record anyway?";
                //        ClientScript.RegisterStartupScript(this.GetType(), "alert", "window.alert('" + errMsg + "');", true);
                //    }
                //}

                if (EntityData != null)
                {
                    lN_LOAN_FACILITY_HDR = (LN_LOAN_FACILITY_HDR)EntityData;
                }

                lN_LOAN_FACILITY_HDR.LN_FACILITY_NAME = txtFacilityName.Text.ToString();
                lN_LOAN_FACILITY_HDR.LN_FACILITY_NAME_OL = txtFacilityNameOL.Text.ToString();
                lN_LOAN_FACILITY_HDR.LN_FACILITY_TYPE = ddlFaciltiyType.SelectedValue;
                lN_LOAN_FACILITY_HDR.LN_PROPERTY_ID = ddlPropertyType.SelectedValue;
                lN_LOAN_FACILITY_HDR.LN_PROPERTY_TYPE = ddlPropertyType.SelectedItem.Text;
                lN_LOAN_FACILITY_HDR.LN_FACILITY_AMOUNT = CommonUtils.ConvertStringToDecimal(txtFacilityAmount.Text.ToString());
                lN_LOAN_FACILITY_HDR.LN_BALANCE_FACILITY_AMOUNT = CommonUtils.ConvertStringToDecimal(txtBalanceFacilityAmount.Text.ToString());

                if (txtStartDate.Text != string.Empty)
                {
                    lN_LOAN_FACILITY_HDR.START_DT = DBMethod.ConvertStringToDate(txtStartDate.Text.ToString());
                }

                if (txtExpiryDate.Text != string.Empty)
                {
                    lN_LOAN_FACILITY_HDR.EXPIRY_DT = DBMethod.ConvertStringToDate(txtExpiryDate.Text.ToString());
                }

                lN_LOAN_FACILITY_HDR.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                lN_LOAN_FACILITY_HDR.LN_FAC_ORG_ID = VMVServices.Web.Utils.OrganizationID;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    lN_LOAN_FACILITY_HDR.MODIFIED_BY = this.LoggedUserName;
                    lN_LOAN_FACILITY_HDR.MODIFIED_DATE = DateTime.Today;
                }
                else
                {
                    lN_LOAN_FACILITY_HDR.LN_FACILITY_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.LN_010.ToString(), false, true);
                    lN_LOAN_FACILITY_HDR.CREATED_BY = this.LoggedUserName;
                    lN_LOAN_FACILITY_HDR.CREATED_DATE = DateTime.Today;
                }

                lN_LOAN_FACILITY_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, lN_LOAN_FACILITY_HDR.LN_FACILITY_ID);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                AssignToBE();

                //emptyvalid();
                if (ErrorCollection.Count > 0)
                {
                    return;
                }


                // Duplicate Validation Through Backend Package PKG_VALIDATIONS

                //ProReturn = FIN.DAL.AP.Item_DAL.GetSPFOR_DUPLICATE_CHECK(iNV_ITEM_MASTER.ITEM_CODE,txtItemName.Text, iNV_ITEM_MASTER.ORG_ID, iNV_ITEM_MASTER.ITEM_ID);

                //if (ProReturn != string.Empty)
                //{
                //    if (ProReturn != "0")
                //    {
                //        ErrorCollection.Add("ITEMNAME", ProReturn);
                //        if (ErrorCollection.Count > 0)
                //        {
                //            return;
                //        }
                //    }
                //}


                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            DBMethod.SaveEntity<LN_LOAN_FACILITY_HDR>(lN_LOAN_FACILITY_HDR);
                            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                            break;
                        }
                    case FINAppConstants.Update:
                        {

                            DBMethod.SaveEntity<LN_LOAN_FACILITY_HDR>(lN_LOAN_FACILITY_HDR, true);
                            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                            break;

                        }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion




        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {

            try
            {
                ErrorCollection.Clear();
                DBMethod.DeleteEntity<LN_LOAN_FACILITY_HDR>(lN_LOAN_FACILITY_HDR);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        protected void btnCancel_Click(object sender, EventArgs e)
        {

        }

        protected void txtFacilityAmount_TextChanged(object sender, EventArgs e)
        {
            txtBalanceFacilityAmount.Text = txtFacilityAmount.Text;

        }


    }

}
