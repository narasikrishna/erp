﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="LoanProvisionEntry.aspx.cs" Inherits="FIN.Client.LOAN.LoanProvisionEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1100px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 70px" id="Div1">
                Bank
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 250px">
                <asp:DropDownList ID="ddlBank" runat="server" TabIndex="1" CssClass="validate[required] RequiredField ddlStype">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" style="float: left">
                &nbsp;</div>
            <div class="lblBox  LNOrient" style=" width: 80px;" id="lblRequestDate">
                Date
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 100px">
                <asp:TextBox runat="server" ID="txtDate" CssClass="validate[required,custom[ReqDateDDMMYYY]]  txtBox"
                    TabIndex="2"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtDate" />
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtDate" />
            </div>
            <div class="lblBox  LNOrient" style=" width: 75px" id="Div2">
                <asp:Button ID="btnProcess" runat="server" Text="Process" CssClass="btn" TabIndex="3"
                    OnClick="btnProcess_Click" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer LNOrient">
            <asp:HiddenField runat="server" ID="hdProvisionId" />
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
              DataKeyNames="LN_PROVISION_ID,LN_CONTRACT_ID,REM_PRINCIPAL_AMT,LN_NO_OF_DAYS,LN_PROFIT_SHARE_AMT"    OnRowDataBound="gvData_RowDataBound"
                Width="700px">
                <Columns>
                    <asp:BoundField DataField="LN_CONTRACT_NUM" HeaderText="Contract Number" ItemStyle-Width="300px"
                        ItemStyle-Wrap="True">
                        <ItemStyle Wrap="True" />
                    </asp:BoundField>
                    <asp:BoundField DataField="LN_CONTRACT_DESC" HeaderText="Contract Name" ItemStyle-Width="300px"
                        ItemStyle-Wrap="True">
                        <ItemStyle Wrap="True" />
                    </asp:BoundField>
                    <asp:BoundField DataField="LN_PROFIT_SHARE_AMT" HeaderText="Profit Share Amount"
                        ItemStyle-Width="200px" ItemStyle-Wrap="True">
                        <ItemStyle Wrap="True" HorizontalAlign="Right" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="Post">
                        <ItemTemplate>
                            <asp:ImageButton ID="imgBtnPost" runat="server" ImageUrl="~/Images/Post.png" Style="border: 0px"
                                OnClick="imgBtnPost_Click" />
                            <asp:Label ID="lblPosted" runat="server" Text="POSTED" Visible="false" CssClass="lblBox  LNOrient"
                                Font-Size="18px" Font-Bold="true" Style="color: Green"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Print">
                        <ItemTemplate>
                            <asp:ImageButton ID="imgBtnJVPrint" runat="server" ImageUrl="~/Images/jvPrint.png"
                                Style="border: 0px" OnClick="imgBtnJVPrint_Click" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divClear_10">
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/LN/LNChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
