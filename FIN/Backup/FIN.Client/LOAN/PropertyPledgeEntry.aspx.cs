﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.BLL.AP;
using FIN.BLL.HR;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.BLL;

using FIN.BLL.CA;
using FIN.BLL.LOAN;
using FIN.BLL.SSM;
using VMVServices.Web;

namespace FIN.Client.LOAN
{
    public partial class PropertyPledgeEntry : PageBase
    {

        LN_FACILITY_PLEDGE lN_FACILITY_PLEDGE = new LN_FACILITY_PLEDGE();
        System.Collections.SortedList slControls = new System.Collections.SortedList();
        //string ProReturn = null;
        Boolean savedBool = false;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                //pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                //pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";

            }

            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
            }

            if (VMVServices.Web.Utils.AddAllowed != FINAppConstants.Y && Master.Mode == FINAppConstants.Add)
            {
                btnSave.Visible = false;
            }

        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                Startup();
                FillComboBox();


                EntityData = null;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    using (IRepository<LN_FACILITY_PLEDGE> userCtx = new DataRepository<LN_FACILITY_PLEDGE>())
                    {
                        lN_FACILITY_PLEDGE = userCtx.Find(r =>
                            (r.PL_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = lN_FACILITY_PLEDGE;
                    txtPledgeID.Text = lN_FACILITY_PLEDGE.PL_ID;
                    if (lN_FACILITY_PLEDGE.PL_PLEDGE_DT != null)
                    {
                        txtPledgeDate.Text = DBMethod.ConvertDateToString(lN_FACILITY_PLEDGE.PL_PLEDGE_DT.ToString());
                    }

                    if (lN_FACILITY_PLEDGE.PL_RELEASE_DT != null)
                    {
                        txtReleaseDate.Text = DBMethod.ConvertDateToString(lN_FACILITY_PLEDGE.PL_RELEASE_DT.ToString());
                    }


                    ddlBank.SelectedValue = lN_FACILITY_PLEDGE.PL_PARTY_ID;
                    ddlProperty.SelectedValue = lN_FACILITY_PLEDGE.PL_PROPERTY_ID;
                    txtRemrks.Text = lN_FACILITY_PLEDGE.PL_REMARKS;

                    chkActive.Checked = lN_FACILITY_PLEDGE.ENABLED_FLAG == FINAppConstants.EnabledFlag ? true : false;
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>

        private void FillComboBox()
        {
            Bank_BLL.fn_getBankName(ref ddlBank);



        }

        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    lN_FACILITY_PLEDGE = (LN_FACILITY_PLEDGE)EntityData;
                }

                if (txtPledgeDate.Text != string.Empty)
                {
                    lN_FACILITY_PLEDGE.PL_PLEDGE_DT = DBMethod.ConvertStringToDate(txtPledgeDate.Text.ToString());
                }
                if (txtReleaseDate.Text != string.Empty)
                {
                    lN_FACILITY_PLEDGE.PL_RELEASE_DT = DBMethod.ConvertStringToDate(txtReleaseDate.Text.ToString());
                }
                lN_FACILITY_PLEDGE.PL_PARTY_ID = ddlBank.SelectedValue.ToString();
                lN_FACILITY_PLEDGE.PL_PROPERTY_ID = ddlProperty.SelectedValue.ToString();


                lN_FACILITY_PLEDGE.PL_ORG_ID = VMVServices.Web.Utils.OrganizationID;
                if (txtRemrks.Text != string.Empty)
                {
                    lN_FACILITY_PLEDGE.PL_REMARKS = txtRemrks.Text.ToString();
                }

                lN_FACILITY_PLEDGE.ENABLED_FLAG = chkActive.Checked == true ? FINAppConstants.EnabledFlag : FINAppConstants.DisabledFlag;


                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    lN_FACILITY_PLEDGE.MODIFIED_BY = this.LoggedUserName;
                    lN_FACILITY_PLEDGE.MODIFIED_DATE = DateTime.Today;
                }
                else
                {
                    lN_FACILITY_PLEDGE.PL_ID = FINSP.GetSPFOR_SEQCode("LN_006".ToString(), false, true);
                    lN_FACILITY_PLEDGE.CREATED_BY = this.LoggedUserName;
                    lN_FACILITY_PLEDGE.CREATED_DATE = DateTime.Today;
                }

                lN_FACILITY_PLEDGE.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, lN_FACILITY_PLEDGE.PL_ID);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                AssignToBE();


                //emptyvalid();
                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            DBMethod.SaveEntity<LN_FACILITY_PLEDGE>(lN_FACILITY_PLEDGE);
                            savedBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {

                            DBMethod.SaveEntity<LN_FACILITY_PLEDGE>(lN_FACILITY_PLEDGE, true);
                            savedBool = true;
                            break;

                        }
                }
                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion




        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                DBMethod.DeleteEntity<LN_FACILITY_PLEDGE>(lN_FACILITY_PLEDGE);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {

        }

        protected void ddlBank_SelectedIndexChanged(object sender, EventArgs e)
        {
            PropertyPledgeBLL.GetPropertyPledge(ref ddlProperty, ddlBank.SelectedValue.ToString());
        }


    }
}
