﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.BLL.AP;
using FIN.BLL.LOAN;
using FIN.DAL;
using FIN.DAL.LOAN;
using FIN.BLL;
using FIN.BLL.CA;
using FIN.BLL.SSM;
using VMVServices.Web;
using System.Collections;
using VMVServices.Services.Data;

namespace FIN.Client.LOAN
{
    public partial class ContractPaymentEntry : PageBase
    {
        //LN_REPAY_HDR LN_REPAY_HDR = new LN_REPAY_HDR();
        // LN_REPAY_DTL LN_REPAY_DTL = new LN_REPAY_DTL();
        LN_CONTRACT_PAYMENT_HDR LN_CONTRACT_PAYMENT_HDR = new LN_CONTRACT_PAYMENT_HDR();
        System.Collections.SortedList slControls = new System.Collections.SortedList();

        DataTable dtGridData = new DataTable();
        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                //pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                //pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";

            }

            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
            }

            if (VMVServices.Web.Utils.AddAllowed != FINAppConstants.Y && Master.Mode == FINAppConstants.Add)
            {
                btnSave.Visible = false;
            }

        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                Startup();
                FillComboBox();
                btnPost.Visible = false;
                Session[FINSessionConstants.GridData] = null;
                EntityData = null;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    LN_CONTRACT_PAYMENT_HDR lN_CONTRACT_PAYMENT_HDR = new LN_CONTRACT_PAYMENT_HDR();
                    using (IRepository<LN_CONTRACT_PAYMENT_HDR> userCtx = new DataRepository<LN_CONTRACT_PAYMENT_HDR>())
                    {
                        lN_CONTRACT_PAYMENT_HDR = userCtx.Find(r =>
                            (r.LN_CON_PAY_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    if (lN_CONTRACT_PAYMENT_HDR != null)
                    {
                        txtDate.Text = DBMethod.ConvertDateToString(lN_CONTRACT_PAYMENT_HDR.LN_PAYMENT_DT.ToString());
                        DataTable dt_cont_det = DBMethod.ExecuteQuery(FIN.DAL.HR.LoanRequest_DAL.getLoanContractDetails(lN_CONTRACT_PAYMENT_HDR.LN_CONTRACT_ID.ToString())).Tables[0];
                        if (dt_cont_det.Rows.Count > 0)
                        {
                            ddlBank.SelectedValue = dt_cont_det.Rows[0]["LN_PARTY_ID"].ToString();
                            btnProcess_Click(btnProcess, new EventArgs());
                        }
                    }
                    
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        private void FillComboBox()
        {
            FIN.BLL.CA.Bank_BLL.fn_getBankName(ref ddlBank);

        }



        # region Save,Update and Delete

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();

                if (dtData.Rows.Count > 0)
                {
                    dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("LN_PRINCIPAL_AMOUNT", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("LN_PRINCIPAL_AMOUNT"))));
                    dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("LN_PAID_AMT", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("LN_PAID_AMT"))));
                    dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("BAL_PRIN_AMT", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("BAL_PRIN_AMT"))));
                    dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("LN_PS_PAID_AMT", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("LN_PS_PAID_AMT"))));
                    dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("LN_CURR_PAID_AMT", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("LN_CURR_PAID_AMT"))));
                    dtData.AcceptChanges();
                }
                gvData.DataSource = dtData;
                gvData.DataBind();
              

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WRI_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        protected void btnProcess_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                dtGridData = DBMethod.ExecuteQuery(ContractPaymetDetails_DAL.getPaymentDetailswithProvision(ddlBank.SelectedValue, txtDate.Text)).Tables[0];
                Session[FINSessionConstants.GridData] = dtGridData;
                BindGrid(dtGridData);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                var tmpChildEntity = new List<Tuple<object, string>>();
                if (gvData.Rows.Count > 0)
                {
                    for (int i = 0; i < gvData.Rows.Count; i++)
                    {
                        LN_CONTRACT_PAYMENT_HDR lN_CONTRACT_PAYMENT_HDR = new LN_CONTRACT_PAYMENT_HDR();

                        if (gvData.DataKeys[i].Values["LN_CON_PAY_ID"].ToString() != "0" && gvData.DataKeys[i].Values["LN_CON_PAY_ID"].ToString() != string.Empty)
                        {
                            using (IRepository<LN_CONTRACT_PAYMENT_HDR> userCtx = new DataRepository<LN_CONTRACT_PAYMENT_HDR>())
                            {
                                lN_CONTRACT_PAYMENT_HDR = userCtx.Find(r =>
                                    (r.LN_CON_PAY_ID == gvData.DataKeys[i].Values["LN_CON_PAY_ID"].ToString())
                                    ).SingleOrDefault();
                            }
                        }


                        TextBox txtPayAmt = gvData.Rows[i].FindControl("txtPayAmt") as TextBox;
                        TextBox txtPayProfitShareAmt = gvData.Rows[i].FindControl("txtPayProfitShareAmt") as TextBox;

                        lN_CONTRACT_PAYMENT_HDR.LN_PAYMENT_DT = DBMethod.ConvertStringToDate(txtDate.Text);
                        lN_CONTRACT_PAYMENT_HDR.ENABLED_FLAG = FINAppConstants.Y;
                        

                        if (txtPayAmt.Text.ToString().Length == 0)
                        {
                            txtPayAmt.Text = "0";
                        }




                        lN_CONTRACT_PAYMENT_HDR.PAID_PRICE_AMT = decimal.Parse(gvData.DataKeys[i].Values["LN_PAID_AMT"].ToString());
                        lN_CONTRACT_PAYMENT_HDR.TOTAL_PS_AMT = decimal.Parse(gvData.DataKeys[i].Values["BAL_PRIN_AMT"].ToString());
                        lN_CONTRACT_PAYMENT_HDR.PAID_PS_AMT = decimal.Parse(gvData.DataKeys[i].Values["LN_PS_PAID_AMT"].ToString());

                        lN_CONTRACT_PAYMENT_HDR.LN_CONTRACT_ID = gvData.DataKeys[i].Values["LN_CONTRACT_ID"].ToString();
                        if (txtPayAmt.Text.ToString().Length > 0)
                        {   
                            lN_CONTRACT_PAYMENT_HDR.LN_PAID_AMT = CommonUtils.ConvertStringToDecimal(txtPayAmt.Text);
                        }
                        if (txtPayProfitShareAmt.Text.ToString().Length > 0)
                        {

                            lN_CONTRACT_PAYMENT_HDR.LS_PS_PAID_AMT = CommonUtils.ConvertStringToDecimal(txtPayProfitShareAmt.Text);
                        }

                        if (gvData.DataKeys[i].Values["LN_CON_PAY_ID"].ToString() != "0" && gvData.DataKeys[i].Values["LN_CON_PAY_ID"].ToString() != string.Empty)
                        {
                            lN_CONTRACT_PAYMENT_HDR.MODIFIED_BY = this.LoggedUserName;
                            lN_CONTRACT_PAYMENT_HDR.MODIFIED_DATE = DateTime.Today;
                            lN_CONTRACT_PAYMENT_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, lN_CONTRACT_PAYMENT_HDR.LN_CON_PAY_ID);
                            DBMethod.SaveEntity<LN_CONTRACT_PAYMENT_HDR>(lN_CONTRACT_PAYMENT_HDR, true);
                        }
                        else
                        {
                            lN_CONTRACT_PAYMENT_HDR.LN_CON_PAY_ID = FINSP.GetSPFOR_SEQCode("LN_011".ToString(), false, true);
                            lN_CONTRACT_PAYMENT_HDR.CREATED_BY = this.LoggedUserName;
                            lN_CONTRACT_PAYMENT_HDR.CREATED_DATE = DateTime.Today;
                            lN_CONTRACT_PAYMENT_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, lN_CONTRACT_PAYMENT_HDR.LN_CON_PAY_ID);
                            DBMethod.SaveEntity<LN_CONTRACT_PAYMENT_HDR>(lN_CONTRACT_PAYMENT_HDR);
                        }

                    }

                }
                
                if (ErrorCollection.Count == 0)
                {
                    if (Session[FINSessionConstants.GridData] != null)
                    {
                        BindGrid((DataTable)Session[FINSessionConstants.GridData]);
                    }
                    
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.SAVED);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    ImageButton imgBtnPost = e.Row.FindControl("imgBtnPost") as ImageButton;
                    ImageButton imgBtnJV = e.Row.FindControl("imgBtnJVPrint") as ImageButton;
                    TextBox txt_PayAmt = (TextBox)e.Row.FindControl("txtPayAmt");
                    TextBox txt_PayProfitShareAmt = (TextBox)e.Row.FindControl("txtPayProfitShareAmt");
                    Label lblPosted = e.Row.FindControl("lblPosted") as Label;
                    if (gvData.DataKeys[e.Row.RowIndex].Values["LN_CON_PAY_ID"].ToString() == "0")
                    {                        
                        imgBtnPost.Visible = false;
                        imgBtnJV.Visible = false;
                    }
                    else 
                    {
                        txt_PayAmt.Enabled = false;
                        txt_PayProfitShareAmt.Enabled = false;

                        if (gvData.DataKeys[e.Row.RowIndex].Values["JE_HDR_ID"].ToString() == "0")
                        {
                            imgBtnPost.Visible = true;
                            imgBtnJV.Visible = false;
                        }
                        else
                        {
                            imgBtnPost.Visible = false;
                            lblPosted.Visible = true;
                            imgBtnJV.Visible = true;
                        }
                      
                       
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }

        }
        protected void imgBtnJVPrint_Click(object sender, ImageClickEventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            LN_CONTRACT_PAYMENT_HDR LN_CONTRACT_PAYMENT_HDR = new LN_CONTRACT_PAYMENT_HDR();

            if (hdPayId.Value.Trim() == string.Empty)
            {
                hdPayId.Value = gvData.DataKeys[gvr.RowIndex].Values["LN_CON_PAY_ID"].ToString();
            }
            using (IRepository<LN_CONTRACT_PAYMENT_HDR> userCtx = new DataRepository<LN_CONTRACT_PAYMENT_HDR>())
            {
                LN_CONTRACT_PAYMENT_HDR = userCtx.Find(r =>
                    (r.LN_CON_PAY_ID == hdPayId.Value)
                    ).SingleOrDefault();
            }

            if (LN_CONTRACT_PAYMENT_HDR != null)
            {
                Hashtable htParameters = new Hashtable();
                Hashtable htHeadingParameters = new Hashtable();
                Hashtable htFilterParameter = new Hashtable();

                ReportFile = "GL_REPORTS//RPTJournalVoucher.rpt";


                ReportData = DBMethod.ExecuteQuery(FIN.DAL.GL.JournalEntry_DAL.GetJournalVoucherDetailsRPT("", LN_CONTRACT_PAYMENT_HDR.JE_HDR_ID));


                Session["ProgramName"] = "Journal Voucher";
                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());



                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;


                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('../RPTCrystalReportViewer.aspx?Id?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);

            }
        }

        protected void imgBtnPost_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton img_tmp = (ImageButton)sender;
            img_tmp.Enabled = false;
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            Label lblPosted = gvr.FindControl("lblPosted") as Label;
            ImageButton imgBtnJVPrint = gvr.FindControl("imgBtnJVPrint") as ImageButton;

            hdPayId.Value = gvData.DataKeys[gvr.RowIndex].Values["LN_CON_PAY_ID"].ToString();
            using (IRepository<LN_CONTRACT_PAYMENT_HDR> userCtx = new DataRepository<LN_CONTRACT_PAYMENT_HDR>())
            {
                LN_CONTRACT_PAYMENT_HDR = userCtx.Find(r =>
                    (r.LN_CON_PAY_ID == hdPayId.Value)
                    ).SingleOrDefault();
            }



            try
            {


                if (LN_CONTRACT_PAYMENT_HDR.WORKFLOW_COMPLETION_STATUS == "1")
                {
                    FINSP.GetSP_GL_Posting(LN_CONTRACT_PAYMENT_HDR.LN_CON_PAY_ID, "LN_011");
                }

                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                img_tmp.Visible = false;
                lblPosted.Visible = true;
                imgBtnJVPrint.Visible = true;
                hdPayId.Value = LN_CONTRACT_PAYMENT_HDR.LN_CON_PAY_ID;

                using (IRepository<LN_CONTRACT_PAYMENT_HDR> userCtx = new DataRepository<LN_CONTRACT_PAYMENT_HDR>())
                {
                    LN_CONTRACT_PAYMENT_HDR = userCtx.Find(r =>
                        (r.LN_CON_PAY_ID == hdPayId.Value)
                        ).SingleOrDefault();
                }


                LN_CONTRACT_PAYMENT_HDR.POSTED_FLAG = FINAppConstants.Y;
                LN_CONTRACT_PAYMENT_HDR.POSTED_DATE = DateTime.Now;
                DBMethod.SaveEntity<LN_CONTRACT_PAYMENT_HDR>(LN_CONTRACT_PAYMENT_HDR, true);

                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.POSTED);


            }
            catch (Exception ex)
            {
                img_tmp.Visible = true;
                lblPosted.Visible = false;
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {

                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void btnPost_Click(object sender, EventArgs e)
        {
            if (gvData.Rows.Count > 0)
            {
                for (int i = 0; i < gvData.Rows.Count; i++)
                {
                    if (gvData.DataKeys[i].Values["LN_CON_PAY_ID"].ToString() != "0" && gvData.DataKeys[i].Values["LN_CON_PAY_ID"].ToString() != string.Empty)
                    {
                        TextBox txtPayAmt = gvData.Rows[i].FindControl("txtPayAmt") as TextBox;
                        if (txtPayAmt.Text.ToString().Length > 0)
                        {
                            if (CommonUtils.ConvertStringToDecimal(txtPayAmt.Text) > 0)
                            {
                                RegenerateProfitShare(i);
                            }
                        }

                    }
                }
            }
        }


        private void RegenerateProfitShare(int rowLoop)
        {
            DataTable dt_LoanReqDtl = DBMethod.ExecuteQuery(FIN.DAL.HR.LoanRequest_DAL.GetLoanRequestDetails("", gvData.DataKeys[rowLoop]["LN_CONTRACT_ID"].ToString())).Tables[0];
            DataTable dt_Contract_Det = FIN.BLL.LOAN.LoanRepayPlan_BLL.fn_GetLoanRepayDtls4Contract(gvData.DataKeys[rowLoop]["LN_CONTRACT_ID"].ToString());

            DataTable dt_paymentDet = DBMethod.ExecuteQuery(FIN.DAL.LOAN.ContractPaymetDetails_DAL.getPaymentDetails4Contrat(gvData.DataKeys[rowLoop]["LN_CONTRACT_ID"].ToString())).Tables[0];
            if (dt_LoanReqDtl.Rows.Count > 0)
            {
                DateTime compareTo = DateTime.Parse(dt_LoanReqDtl.Rows[0]["ln_end_date"].ToString());
                DateTime now = DateTime.Parse(dt_LoanReqDtl.Rows[0]["ln_start_date"].ToString());

                double dbl_LoanAmount = double.Parse(dt_LoanReqDtl.Rows[0]["ln_principal_amount"].ToString());
                double dbl_PSPercentage = double.Parse(dt_LoanReqDtl.Rows[0]["ln_roi"].ToString());
                var dateSpan = FIN.Client.LOAN.DateTimeSpan.CompareDates(compareTo, now);
                int int_Tot_Months = dateSpan.Months + 1;

                int int_calmonth = 0;
                int int_CalMonth = 0;

                DataTable dt_InterstDtl = DBMethod.ExecuteQuery(FIN.DAL.LOAN.Interest_DAL.getInterestDetails4Contract(gvData.DataKeys[rowLoop]["LN_CONTRACT_ID"].ToString())).Tables[0];
                if (dt_InterstDtl.Rows.Count > 0)
                {
                    if (dt_InterstDtl.Rows[0]["LN_INT_TYPE"].ToString().ToUpper() == "MONTHLY")
                    {
                        int_CalMonth = 1;
                    }
                    else if (dt_InterstDtl.Rows[0]["LN_INT_TYPE"].ToString().ToUpper() == "QUARTERLY")
                    {
                        int_CalMonth = 3;
                    }
                    else if (dt_InterstDtl.Rows[0]["LN_INT_TYPE"].ToString().ToUpper() == "HALFYEARLY")
                    {
                        int_CalMonth = 6;
                    }
                    else if (dt_InterstDtl.Rows[0]["LN_INT_TYPE"].ToString().ToUpper() == "ANNUAL")
                    {
                        int_CalMonth = 12;
                    }
                }

                int_calmonth = int_Tot_Months / int_CalMonth;

                double dbl_totLoanAmt = double.Parse(dt_LoanReqDtl.Rows[0]["dbl_pr_amt"].ToString());
                double dbl_MonthAmt = dbl_totLoanAmt / int_calmonth;

                double dbl_PaidLoanAmt = 0;

                DateTime PreviousDt = now;
                double No_ofDays = 0;

                dtGridData = dt_InterstDtl.Copy();
                dtGridData.Rows.Clear();
                double dbl_PreviousPaidAmt = 0;
                for (int iLoop = 0; iLoop < int_calmonth; iLoop++)
                {

                    now = now.AddMonths(int_CalMonth);

                    DataRow drList;
                    drList = dtGridData.NewRow();
                    drList["LN_INT_DTL_ID"] = "0";

                    drList["LN_INSTALLMENT_NO"] = iLoop + 1;
                    drList["LN_INSTALLMENT_DATE"] = now;


                    dateSpan = FIN.Client.LOAN.DateTimeSpan.CompareDates(now, PreviousDt);
                    No_ofDays = dateSpan.Days;

                    if (iLoop == 0)
                        No_ofDays += 1;

                    drList["LN_PROFIT_SHARE_AMT"] = ((No_ofDays / 365) * ((dbl_LoanAmount * dbl_PSPercentage) / 100)).ToString();

                    string str_Amt = dt_paymentDet.Compute("sum(LN_PAID_AMT)", "LN_PAYMENT_DT<'" + now + "'").ToString();
                    if (str_Amt.Length > 0)
                    {
                        dbl_PaidLoanAmt = double.Parse(str_Amt);
                    }

                    if (dbl_PreviousPaidAmt != dbl_PaidLoanAmt)
                    {
                        dbl_LoanAmount = double.Parse(dt_LoanReqDtl.Rows[0]["ln_principal_amount"].ToString()) - dbl_PaidLoanAmt;
                        dbl_PreviousPaidAmt = dbl_PaidLoanAmt;
                    }

                    if (dbl_LoanAmount <= 0)
                    {
                        dbl_LoanAmount = 0;
                    }

                    drList[FINColumnConstants.ENABLED_FLAG] = "TRUE";

                    drList[FINColumnConstants.DELETED] = FINAppConstants.N;
                    dtGridData.Rows.Add(drList);

                    PreviousDt = now;


                }


                for (int kLoop = 0; kLoop < dtGridData.Rows.Count; kLoop++)
                {
                    dt_InterstDtl.Rows[kLoop]["LN_REV_PROFIT_SHARE_AMT"] = dtGridData.Rows[kLoop]["LN_PROFIT_SHARE_AMT"];
                }
                dt_InterstDtl.AcceptChanges();

                for (int kLoop = 0; kLoop < dt_InterstDtl.Rows.Count; kLoop++)
                {
                    LN_INTEREST_DTL lN_INTEREST_DTL = new LN_INTEREST_DTL();
                    using (IRepository<LN_INTEREST_DTL> userCtx = new DataRepository<LN_INTEREST_DTL>())
                    {
                        lN_INTEREST_DTL = userCtx.Find(r =>
                            (r.LN_INT_DTL_ID == dt_InterstDtl.Rows[0]["LN_INT_DTL_ID"].ToString())
                            ).SingleOrDefault();
                    }

                    lN_INTEREST_DTL.LN_REV_PROFIT_SHARE_AMT = decimal.Parse(dt_InterstDtl.Rows[kLoop]["LN_REV_PROFIT_SHARE_AMT"].ToString());
                    DBMethod.SaveEntity<LN_INTEREST_DTL>(lN_INTEREST_DTL, true);
                }

            }
        }


    }
}
