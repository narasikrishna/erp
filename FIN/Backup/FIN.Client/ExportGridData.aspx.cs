using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.VisualBasic;
using System.Collections;
using System.Data;
using System.Diagnostics;
using System.Web.SessionState;


using System.IO;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using System.Configuration;
using System.Drawing;
using System.Text;

using FIN.BLL;

namespace Aon.Client
{
    public partial class ExportGridData : Page
    {
        DataTable dtData = new DataTable();
        DataTable dtData1 = new DataTable();
        GridView grid = new GridView();
        /// <summary>
        /// Page Load Events
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    if (Session["QueryFormData"] != null)
                    {
                        dtData1 = (DataTable)Session["QueryFormData"];
                    }
                    else if (Session[FINSessionConstants.GridData] != null)
                    {
                        dtData1 = (DataTable)Session[FINSessionConstants.GridData];
                    }
                    if (dtData1 != null)
                    {
                        dtData = dtData1.Copy();

                        if (dtData1.Rows.Count > 0)
                        {
                            foreach (DataColumn column in dtData1.Columns)
                            {
                                if (column.ColumnName.ToUpper() == "MODIFYURL")
                                {
                                    dtData.Columns.Remove(column.ColumnName);
                                }
                                if (column.ColumnName.ToUpper() == "DELETEURL")
                                {
                                    dtData.Columns.Remove(column.ColumnName);
                                }
                                // dtData.AcceptChanges();
                            }

                            grid.DataSource = dtData;
                            grid.DataBind();
                        }
                    }

                    Response.Clear();
                    Response.Buffer = true;

                    if (Request.QueryString["ExportId"] == "PDF")
                    {
                        ExportToPDF();
                    }
                    else if (Request.QueryString["ExportId"] == "EXCEL")
                    {
                        ExportToExcel();
                    }
                    else if (Request.QueryString["ExportId"] == "WORD")
                    {
                        ExportToWord();
                    }
                    else if (Request.QueryString["ExportId"] == "CSV")
                    {
                        ExportToCSV();
                    }
                    else if (Request.QueryString["ExportId"] == "TXT")
                    {
                        //  ExportToExcel();
                        ExportToText();
                        //  ExportToTextFile(dtData);

                    }
                    Session["REPORTHEADING"] = null;
                    Session["SHOWFOOTER"] = null;


                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void ExportToPDF()
        {
            using (StringWriter sw = new StringWriter())
            {
                using (HtmlTextWriter hw = new HtmlTextWriter(sw))
                {
                    grid.AllowPaging = false;

                    string imagepath = Server.MapPath("UploadFile");
                   
                    Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
                    HTMLWorker htmlparser = new HTMLWorker(pdfDoc);

                    //giff.ScaleToFit(250f, 250f);

                    //giff.IndentationLeft = 9f;
                    //giff.SpacingAfter = 9f;
                    //giff.BorderWidthTop = 36f;



                    PdfWriter.GetInstance(pdfDoc, (HttpContext.Current.Response.OutputStream));
                    pdfDoc.Open();


                    if (Session["REPORTHEADING"] != null)
                    {
                        iTextSharp.text.Image giff = iTextSharp.text.Image.GetInstance(imagepath + "\\ORG_LOGO\\ORG-0000000066.jpg");
                        giff.Alignment = iTextSharp.text.Image.TEXTWRAP | iTextSharp.text.Image.ALIGN_LEFT;
                        giff.ScaleToFit(100f, 100f);
                        giff.IndentationLeft = 9f;
                        giff.SpacingAfter = 9f;
                        pdfDoc.Add(giff);


                        iTextSharp.text.Table tables = new iTextSharp.text.Table(1, 1);
                        tables.Cellpadding = 5;
                        tables.Cellspacing = 5;
                        //Set the column widths 
                        int[] widths = new int[1];
                        widths[0] = 500;
                        string cellText = Session["REPORTHEADING"].ToString();
                        iTextSharp.text.Cell cells = new iTextSharp.text.Cell(cellText);
                        cells.HorizontalAlignment = Element.ALIGN_CENTER; //0=Left, 1=Centre, 2=Right
                        cells.VerticalAlignment = Element.ALIGN_TOP;
                        cells.Border = 0;
                        cells.BorderWidth = 0;
                        tables.AddCell(cells);
                        tables.SetWidths(widths);
                        tables.Border = 0;
                        pdfDoc.Add(tables);

                        //empty space
                        //iTextSharp.text.Table table1 = new iTextSharp.text.Table(1, 1);
                        //table1.Cellpadding = 5;
                        //table1.Cellspacing = 40;
                        //string cellText1 = string.Empty;
                        //cellText1 = cellText1.Replace(Environment.NewLine, String.Empty).Replace("  ", String.Empty);
                        //iTextSharp.text.Cell cell1 = new iTextSharp.text.Cell(cellText1);
                        //cell1.Border = 0;
                        //cell1.BorderWidth = 0;
                        //table1.AddCell(cell1);
                        //table1.Border = 0;
                        //pdfDoc.Add(table1);


                        Phrase phrase1 = new Phrase(Environment.NewLine);
                        pdfDoc.Add(phrase1);
                        Phrase phrase5 = new Phrase(Environment.NewLine);
                        pdfDoc.Add(phrase5);
                        Phrase phrase2 = new Phrase(Environment.NewLine);
                        pdfDoc.Add(phrase2);
                        Phrase phrase3 = new Phrase(Environment.NewLine);
                        pdfDoc.Add(phrase3);
                        Phrase phrase4 = new Phrase(Environment.NewLine);
                        pdfDoc.Add(phrase4);


                       
                    }
                 

                    grid.RenderControl(hw);

                    StringReader sr = new StringReader(sw.ToString());
                    htmlparser.Parse(sr);
                   
                    
                    //empty space
                    //iTextSharp.text.Table table2 = new iTextSharp.text.Table(1, 1);
                    //table2.Cellpadding = 5;
                    //table2.Cellspacing = 40;
                    //string cellText2 = string.Empty;
                    //cellText2 = cellText2.Replace(Environment.NewLine, String.Empty).Replace("  ", String.Empty);
                    //iTextSharp.text.Cell cell2 = new iTextSharp.text.Cell(cellText2);
                    //cell2.Border = 0;
                    //cell2.BorderWidth = 0;
                    //table2.AddCell(cell2);
                    //pdfDoc.Add(table2);


                    Phrase phrase11 = new Phrase(Environment.NewLine);
                    pdfDoc.Add(phrase11);
                    Phrase phrase51 = new Phrase(Environment.NewLine);
                    pdfDoc.Add(phrase51);
                    Phrase phrase21 = new Phrase(Environment.NewLine);
                    pdfDoc.Add(phrase21);
                    Phrase phrase31 = new Phrase(Environment.NewLine);
                    pdfDoc.Add(phrase31);
                    Phrase phrase41 = new Phrase(Environment.NewLine);
                    pdfDoc.Add(phrase41);
                    Phrase phrase42 = new Phrase(Environment.NewLine);
                    pdfDoc.Add(phrase42);
                    Phrase phrase43 = new Phrase(Environment.NewLine);
                    pdfDoc.Add(phrase43);
                    Phrase phrase44 = new Phrase(Environment.NewLine);
                    pdfDoc.Add(phrase44);
                    Phrase phrase45 = new Phrase(Environment.NewLine);
                    pdfDoc.Add(phrase45);
                    Phrase phrase46 = new Phrase(Environment.NewLine);
                    pdfDoc.Add(phrase46);
                    Phrase phrase47 = new Phrase(Environment.NewLine);
                    pdfDoc.Add(phrase47);
                    Phrase phrase48 = new Phrase(Environment.NewLine);
                    pdfDoc.Add(phrase48);
                    Phrase phrase49 = new Phrase(Environment.NewLine);
                    pdfDoc.Add(phrase49);
                    Phrase phrase50 = new Phrase(Environment.NewLine);
                    pdfDoc.Add(phrase50);
                    Phrase phrase52 = new Phrase(Environment.NewLine);
                    pdfDoc.Add(phrase52);
                    Phrase phrase61 = new Phrase(Environment.NewLine);
                    pdfDoc.Add(phrase61);
                    Phrase phrase53 = new Phrase(Environment.NewLine);
                    pdfDoc.Add(phrase53);
                    Phrase phrase54 = new Phrase(Environment.NewLine);
                    pdfDoc.Add(phrase54);
                    Phrase phrase55 = new Phrase(Environment.NewLine);
                    pdfDoc.Add(phrase55);
                    Phrase phrase56 = new Phrase(Environment.NewLine);
                    pdfDoc.Add(phrase56);
                    Phrase phrase57 = new Phrase(Environment.NewLine);
                    pdfDoc.Add(phrase57);
                    Phrase phrase58 = new Phrase(Environment.NewLine);
                    pdfDoc.Add(phrase58);
                    Phrase phrase59 = new Phrase(Environment.NewLine);
                    pdfDoc.Add(phrase59);
                    Phrase phrase60 = new Phrase(Environment.NewLine);
                    pdfDoc.Add(phrase60);
                    Phrase phrase65 = new Phrase(Environment.NewLine);
                    pdfDoc.Add(phrase65);
                    Phrase phrase62 = new Phrase(Environment.NewLine);
                    pdfDoc.Add(phrase62);
                    //Phrase phrase63 = new Phrase(Environment.NewLine);
                    //pdfDoc.Add(phrase63);
                   
                    if (Session["SHOWFOOTER"] != null)
                    {
                        pdfDoc.Add(new Paragraph("  DATE : " + DateTime.Now.ToShortDateString() + "                                                                                                 Prepared By : " + VMVServices.Web.Utils.UserName.ToString()));
                    }
                
                    pdfDoc.Close();

                    HttpContext.Current.Response.ContentType = "application/pdf";
                    HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=GridViewExport.pdf");
                    HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    HttpContext.Current.Response.Write(pdfDoc);
                    HttpContext.Current.Response.Flush();
                    HttpContext.Current.Response.End();
                }
            }
        }
        private void ExportToWord()
        {
            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition",
            "attachment;filename=GridViewExport.doc");
            Response.Charset = "";
            Response.ContentType = "application/ms-word ";
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            grid.AllowPaging = false;
            grid.DataBind();
            grid.RenderControl(hw);
            grid.GridLines = GridLines.Both;
            grid.HeaderStyle.Font.Bold = true;
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();
        }
        private void ExportToExcel()
        {
            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition",
            "attachment;filename=GridViewExport.xls");
            Response.Charset = "";
            Response.ContentType = "application/vnd.ms-excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            grid.AllowPaging = false;
            grid.DataBind();
            grid.RenderControl(hw);
            grid.GridLines = GridLines.Both;
            grid.HeaderStyle.Font.Bold = true;
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();
        }
        private void ExportToTextFile(DataTable dtExportDatas)
        {
            Response.Clear();
            Response.Buffer = true; ////string filePath = Server.MapPath("../TmpWordDocs/" + filename);
            string filename = "AccountDetails.txt";// +"_" + DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss");


            // Exporting Data to text file
            ExportDataTabletoFile(dtExportDatas, "	", false, Server.MapPath("ExportTxt/" + filename));

            Response.ContentType = "application/octet-stream";
            Response.AppendHeader("Content-Disposition", "attachment;filename=" + filename);
            string filePaths = Server.MapPath("ExportTxt/" + filename);
            Response.TransmitFile(Server.MapPath("ExportTxt/" + filename));
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            ScriptManager.RegisterStartupScript(Page, this.GetType(), "DownloadAsPdf", "window.open('" + filePaths + "','','" + VMVServices.Services.Data.DbConsts.ReportProperties + "');", true);

        }
        public void ExportDataTabletoFile(DataTable datatable, string delimited, bool exportcolumnsheader, string file)
        {
            try
            {

                StreamWriter str = new StreamWriter(file, false, System.Text.Encoding.Default);
                if (exportcolumnsheader)
                {
                    string Columns = string.Empty;
                    foreach (DataColumn column in datatable.Columns)
                    {
                        Columns += column.ColumnName + delimited;
                    }
                    str.WriteLine(Columns.Remove(Columns.Length - 1, 1));
                }
                foreach (DataRow datarow in datatable.Rows)
                {
                    string row = string.Empty;

                    foreach (object items in datarow.ItemArray)
                    {
                        row += items.ToString() + delimited;
                    }
                    str.WriteLine(row.Remove(row.Length - 1, 1));
                }
                str.Flush();
                str.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private void ExportToText()
        {
            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition",
             "attachment;filename=GridViewExport.txt");
            Response.Charset = "";
            Response.ContentType = "application/text";

            grid.AllowPaging = false;
            grid.DataBind();


            StringBuilder sb = new StringBuilder();
            //if (grid.Rows.Count > 0)
            //{

            //    for (int k = 0; k < grid.Rows[0].Cells.Count; k++)
            //    {
            //        //add separator
            //        sb.Append(grid.Columns[k].HeaderText + ',');
            //    }
            //}

            //append new line
            sb.Append("\r\n");
            for (int i = 0; i < grid.Rows.Count; i++)
            {
                for (int k = 0; k < grid.Rows[i].Cells.Count; k++)
                {
                    //add separator
                    sb.Append(grid.Rows[i].Cells[k].Text + ',');
                }
                //append new line
                sb.Append("\r\n");
            }
            Response.Output.Write(sb.ToString());
            Response.Flush();
            Response.End();
        }
        private void ExportToCSV()
        {
            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition",
             "attachment;filename=GridViewExport.csv");
            Response.Charset = "";
            Response.ContentType = "application/text";

            grid.AllowPaging = false;
            grid.DataBind();

            StringBuilder sb = new StringBuilder();
            for (int k = 0; k < grid.Columns.Count; k++)
            {
                //add separator
                sb.Append(grid.Columns[k].HeaderText + ',');
            }
            //append new line
            sb.Append("\r\n");
            for (int i = 0; i < grid.Rows.Count; i++)
            {
                for (int k = 0; k < grid.Columns.Count; k++)
                {
                    //add separator
                    sb.Append(grid.Rows[i].Cells[k].Text + ',');
                }
                //append new line
                sb.Append("\r\n");
            }
            Response.Output.Write(sb.ToString());
            Response.Flush();
            Response.End();
        }
    }
}