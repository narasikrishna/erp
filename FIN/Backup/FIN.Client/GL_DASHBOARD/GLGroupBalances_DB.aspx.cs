﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using VMVServices.Web;
using System.Collections;
namespace FIN.Client.GL_DASHBOARD
{
    public partial class GLGroupBalances_DB : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session["GRPBAL"] = null;
                AssignToControl();
                GenerateGBDetails();
                LoadGraph();
            }
            //else
            //{
            //    LoadGraph();
            //}
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                FillComboBox();
                divGraphData.Visible = false;
                divPrintIcon.Visible = false;
                if (Request.QueryString.ToString().ToUpper().Contains("TVYN"))
                {
                    Startup();
                    divGraphData.Visible = true;
                    divPrintIcon.Visible = true;
                    divHeader.Style.Add(" background-color", "white");
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DetailedRevenueATC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }


        private void Startup()
        {
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[FIN.BLL.QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[FIN.BLL.QueryStringTags.ReportName.ToString()]));
        }

        private void LoadGraph()
        {
            if (Session["GRPBAL"] != null)
            {
                ChartGroupGL.DataSource = (DataTable)Session["GRPBAL"];
                ChartGroupGL.DataBind();

                gvGraphdata.DataSource = (DataTable)Session["GRPBAL"];
                gvGraphdata.DataBind();
            }
        }

        private void FillComboBox()
        {

            FIN.BLL.GL.AccountingCalendar_BLL.GetCalAcctYear(ref ddl_GB_FINYear);
            string str_finyear = FINSP.GetSPFOR_FiscalYear(DateTime.Now.Date.ToString("dd/MM/yyyy"));
            if (ddl_GB_FINYear.Items.Count > 0)
            {
                ddl_GB_FINYear.SelectedValue = str_finyear;
            }
            FIN.BLL.GL.AccountingCalendar_BLL.GetFinancialPeriod4YearAndActual(ref ddl_GB_FINPeriod, ddl_GB_FINYear.SelectedValue);
            DataTable dt_cur_period = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountingCalendar_DAL.GetPeriodId4CurrentDate()).Tables[0];
            if (dt_cur_period.Rows.Count > 0)
            {
                ddl_GB_FINPeriod.SelectedValue = dt_cur_period.Rows[0]["PERIOD_ID"].ToString();
            }
        }

        private void GenerateGBDetails()
        {
            DataTable dt_GB_Details = DBMethod.ExecuteQuery(FIN.DAL.GL.Dashboar_GL_DAL.getLeave1GrpBalance4Period(ddl_GB_FINPeriod.SelectedValue)).Tables[0];
            Session["GRPBAL"] = dt_GB_Details;
            GV_L1.DataSource = dt_GB_Details;
            GV_L1.DataBind();
            div_Level1.Visible = true;
            div_Level2.Visible = false;
            div_Level3.Visible = false;
            div_Level4.Visible = false;
            div_Level5.Visible = false;
            LoadGraph();
        }

        protected void ddl_GB_FINYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            FIN.BLL.GL.AccountingCalendar_BLL.GetFinancialPeriod4Year(ref ddl_GB_FINPeriod, ddl_GB_FINYear.SelectedValue);
            LoadGraph();
        }

        protected void ddl_GB_FINPeriod_SelectedIndexChanged(object sender, EventArgs e)
        {
            GenerateGBDetails();
            LoadGraph();
        }

        protected void lnk_GB1_Name_Click(object sender, EventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            getGBL2Value(gvr);
        }

        private void getGBL2Value(GridViewRow gvr)
        {
            DataTable dt_GB_Details = DBMethod.ExecuteQuery(FIN.DAL.GL.Dashboar_GL_DAL.getLeave2GrpBalance4Period(ddl_GB_FINPeriod.SelectedValue, GV_L1.DataKeys[gvr.RowIndex].Values["GB_ID1"].ToString())).Tables[0];
            Session["GRPBAL"] = dt_GB_Details;
            GV_L2.DataSource = dt_GB_Details;
            GV_L2.DataBind();
            div_Level1.Visible = false;
            div_Level2.Visible = true;
            div_Level3.Visible = false;
            div_Level4.Visible = false;
            div_Level5.Visible = false;
            LoadGraph();

        }

        protected void lnk_GB2_Name_Click(object sender, EventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            getGBL3Value(gvr);
        }

        private void getGBL3Value(GridViewRow gvr)
        {
            DataTable dt_GB_Details = DBMethod.ExecuteQuery(FIN.DAL.GL.Dashboar_GL_DAL.getLeave3GrpBalance4Period(ddl_GB_FINPeriod.SelectedValue, GV_L2.DataKeys[gvr.RowIndex].Values["GB_ID2"].ToString())).Tables[0];
            Session["GRPBAL"] = dt_GB_Details;
            GV_L3.DataSource = dt_GB_Details;
            GV_L3.DataBind();
            div_Level1.Visible = false;
            div_Level2.Visible = false;
            div_Level3.Visible = true;
            div_Level4.Visible = false;
            div_Level5.Visible = false;
            LoadGraph();

        }
        protected void lnk_Group_Click(object sender, EventArgs e)
        {
          
        }

        protected void lnk_GB3_Name_Click(object sender, EventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            getGBL4Value(gvr);
        }

        private void getGBL4Value(GridViewRow gvr)
        {
            DataTable dt_GB_Details = DBMethod.ExecuteQuery(FIN.DAL.GL.Dashboar_GL_DAL.getLeave4GrpBalance4Period(ddl_GB_FINPeriod.SelectedValue, GV_L3.DataKeys[gvr.RowIndex].Values["GB_ID3"].ToString())).Tables[0];
            Session["GRPBAL"] = dt_GB_Details;
            GV_L4.DataSource = dt_GB_Details;
            GV_L4.DataBind();
            div_Level1.Visible = false;
            div_Level2.Visible = false;
            div_Level3.Visible = false;
            div_Level4.Visible = true;
            div_Level5.Visible = false;

            LoadGraph();

        }

        protected void lnk_GB4_Name_Click(object sender, EventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            getGBL5Value(gvr);
        }

        private void getGBL5Value(GridViewRow gvr)
        {
            DataTable dt_GB_Details = DBMethod.ExecuteQuery(FIN.DAL.GL.Dashboar_GL_DAL.getLeave5GrpBalance4Period(ddl_GB_FINPeriod.SelectedValue, GV_L4.DataKeys[gvr.RowIndex].Values["GB_ID4"].ToString())).Tables[0];
            Session["GRPBAL"] = dt_GB_Details;
            GV_L5.DataSource = dt_GB_Details;
            GV_L5.DataBind();
            div_Level1.Visible = false;
            div_Level2.Visible = false;
            div_Level3.Visible = false;
            div_Level4.Visible = false;
            div_Level5.Visible = true;

            LoadGraph();

        }

        protected void imgL2_Click(object sender, ImageClickEventArgs e)
        {
            div_Level1.Visible = true;
            div_Level2.Visible = false;
            div_Level3.Visible = false;
            div_Level4.Visible = false;
            div_Level5.Visible = false;
            LoadGraph();
        }

        protected void imgL3_Click(object sender, ImageClickEventArgs e)
        {
            div_Level1.Visible = false;
            div_Level2.Visible = true;
            div_Level3.Visible = false;
            div_Level4.Visible = false;
            div_Level5.Visible = false;
            LoadGraph();
        }

        protected void imgL4_Click(object sender, ImageClickEventArgs e)
        {
            div_Level1.Visible = false;
            div_Level2.Visible = false;
            div_Level3.Visible = true;
            div_Level4.Visible = false;
            div_Level5.Visible = false;
            LoadGraph();
        }

        protected void imgL5_Click(object sender, ImageClickEventArgs e)
        {
            div_Level1.Visible = false;
            div_Level2.Visible = false;
            div_Level3.Visible = false;
            div_Level4.Visible = true;
            div_Level5.Visible = false;
            LoadGraph();
        }

        protected void imgBtnGenChart_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        protected void btnGraphRep_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;

                Hashtable htParameters = new Hashtable();
                Hashtable htHeadingParameters = new Hashtable();
                Hashtable htFilterParameter = new Hashtable();


                htFilterParameter.Add("FF_YEAR", ddl_GB_FINYear.SelectedItem.Text);
                htFilterParameter.Add("FF_MONTH", ddl_GB_FINPeriod.SelectedItem.Text);

                htFilterParameter.Add("PERIOD", ddl_GB_FINPeriod.SelectedValue.ToString());

                ChartGroupGL.SaveImage(System.Web.Configuration.WebConfigurationManager.AppSettings["ChartFileFolder"].ToString() + Master.ProgramID + ".png");
                htFilterParameter.Add("ChartPath", System.Web.Configuration.WebConfigurationManager.AppSettings["ChartFileFolder"].ToString() + Master.ProgramID + ".png");


                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;

                DataTable dt_data = (DataTable)Session["GRPBAL"];
                ReportData = new DataSet();
                DataTable dt = dt_data.Copy();
                dt.TableName = "VW_GL_GROUPBALANCES";
                dt.Columns[0].ColumnName = "GB_ID";
                ReportData.Tables.Add(dt.Copy());


                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FIN.BLL.FINMessageConstatns.CrystalReportViewerReportPath + "?Id=" + Master.ProgramID + "','','" + VMVServices.Services.Data.DbConsts.ReportProperties + "');", true);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ddd", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }


    }
}