﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="GLCashBalance_BI.aspx.cs" Inherits="FIN.Client.GL_DASHBOARD.GLCashBalance_BI" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 900px" id="div1">
        <div class="divFormcontainer" style="width: 290px" id="diDv1">  <div class="divRowContainer divReportAction">
            <div>
                <asp:ImageButton ID="btnSave" runat="server" ImageUrl="../Images/show-report-icon.png"
                    Width="35px" Height="25px" OnClick="btnSave_Click" />
            </div>
        </div>  </div>
    </div>
    <div id="dividcbGrid" style="width: 100%; height: 310px; overflow: auto; display: none;">
        <asp:GridView ID="gvCashBal" runat="server" CssClass="Grid" Width="98%" DataKeyNames="acct_code_id"
            AutoGenerateColumns="False">
            <Columns>
                <asp:BoundField DataField="ACCOUNT_NAME" HeaderText="Account Name" SortExpression="ACCOUNT_NAME">
                    <ItemStyle HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="BALANCE_AMT" HeaderText="Balance Amount" SortExpression="BALANCE_AMT">
                    <ItemStyle HorizontalAlign="Right" />
                </asp:BoundField>
            </Columns>
            <EmptyDataRowStyle CssClass="EmptyRowStyle" />
            <HeaderStyle CssClass="GridHeader" />
            <AlternatingRowStyle CssClass="GrdAltRow" />
        </asp:GridView>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/GL/GLChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();


                return $("#form1").validationEngine('validate')
            })


        }

    </script>
</asp:Content>
