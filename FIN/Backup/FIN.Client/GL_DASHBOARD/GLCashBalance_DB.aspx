﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="GLCashBalance_DB.aspx.cs" Inherits="FIN.Client.GL_DASHBOARD.GLCashBalance_DB" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div id="dividCB" style="width: 100%">
        <div class="DBGridHeader" style="float: left; width: 100%; height: 25px;"  id="divHeader" runat="server">
            <div style="float: left; padding-left: 10px; padding-top: 2px">
                Cash Balance
            </div>
            <div id="divPrintIcon" runat="server" style="float: right; padding-right: 10px; padding-top: 2px">
                <asp:ImageButton ID="btnGraphRep" runat="server" ImageUrl="../Images/show-report-icon.png"
                    Width="35px" Height="25px" OnClick="btnGraphRep_Click" />
            </div>
        </div>
        <div class="divClear_10" style="width: 58%">
        </div>
        <div id="dividcbGrid" style="width: 100%; height: 310px; overflow: auto">
            <asp:GridView ID="gvCashBal" runat="server" CssClass="Grid" Width="98%" DataKeyNames="acct_code_id"
                AutoGenerateColumns="False">
                <Columns>
                    <asp:BoundField DataField="ACCOUNT_NAME" HeaderText="Account Name" SortExpression="ACCOUNT_NAME">
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:BoundField DataField="BALANCE_AMT" HeaderText="Balance Amount" SortExpression="BALANCE_AMT">
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:BoundField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divClear_10" style="width: 70%">
        </div>
        <div id="divGraphData" runat="server">
            <asp:GridView ID="gvGraphdata" runat="server" CssClass="Grid" Width="100%" AutoGenerateColumns="False"
                ShowHeaderWhenEmpty="true">
                <Columns>
                    <asp:BoundField DataField="ACCOUNT_NAME" HeaderText="Account Name"></asp:BoundField>
                    <asp:BoundField DataField="BALANCE_AMT" HeaderText="Balance Amount">
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:BoundField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
</asp:Content>
