﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="GLGroupBalances_DB.aspx.cs" Inherits="FIN.Client.GL_DASHBOARD.GLGroupBalances_DB" %>

    <%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function fn_GBLevel() {
            $("#div_GBLevel").fadeToggle(1000);
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div id="div_IDGB" style="width: 100%">
        <div class="DBGridHeader" style="float: left; width: 100%; height: 25px" id="divHeader" runat="server">
            <div style="float: left; padding-left: 10px; padding-top: 2px" >
                Group Balances
            </div>
            <div id="divPrintIcon" runat="server" style="float: right; padding-right: 10px; padding-top: 2px">
                <asp:ImageButton ID="btnGraphRep" runat="server" ImageUrl="../Images/show-report-icon.png"
                    Width="35px" Height="25px" OnClick="btnGraphRep_Click" />
            </div>
            <div style="float: right; padding-right: 10px; padding-top: 2px">
                <img src="../Images/DashBoardImage/ViewList.png" width="20px" height="20px" onclick="fn_GBLevel()" />
            </div>
            <div style="width: 120px; float: right; padding-right: 10px; padding-top: 2px">
                <asp:DropDownList ID="ddl_GB_FINPeriod" runat="server" CssClass="ddlStype" AutoPostBack="True"
                    OnSelectedIndexChanged="ddl_GB_FINPeriod_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div style="width: 120px; float: right; padding-right: 10px; padding-top: 2px">
                <asp:DropDownList ID="ddl_GB_FINYear" runat="server" CssClass="ddlStype" AutoPostBack="True"
                    OnSelectedIndexChanged="ddl_GB_FINYear_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div id="div_GBLevel" style="width: 98%; background-color: ThreeDFace; position: absolute;
            top: 30px; left: 5px; display: none">
            <div id="div_Level1" runat="server" visible="true">
                <asp:GridView ID="GV_L1" runat="server" CssClass="Grid" Width="100%" DataKeyNames="GB_ID1"
                    AutoGenerateColumns="False">
                    <Columns>
                        <asp:TemplateField HeaderText="Department">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnk_GB1_Name" runat="server" Text='<%# Eval("GBName") %>' OnClick="lnk_GB1_Name_Click"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="GB" HeaderText="Balance">
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:BoundField>
                    </Columns>
                    <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                    <HeaderStyle CssClass="GridHeader" />
                    <AlternatingRowStyle CssClass="GrdAltRow" />
                </asp:GridView>
            </div>
            <div id="div_Level2" runat="server" visible="true">
                <table width="100%">
                    <tr>
                        <td align="right">
                            <asp:ImageButton ID="imgL2" runat="server" ImageUrl="~/Images/DashBoardImage/back.png"
                                Width="20px" Height="20px" OnClick="imgL2_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:GridView ID="GV_L2" runat="server" CssClass="Grid" Width="100%" DataKeyNames="GB_ID2"
                                AutoGenerateColumns="False">
                                <Columns>
                                    <asp:TemplateField HeaderText="Department">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnk_GB2_Name" runat="server" Text='<%# Eval("GBName") %>' OnClick="lnk_GB2_Name_Click"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="GB" HeaderText="Balance">
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                </Columns>
                                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                <HeaderStyle CssClass="GridHeader" />
                                <AlternatingRowStyle CssClass="GrdAltRow" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </div>
            <div id="div_Level3" runat="server" visible="true">
                <table width="100%">
                    <tr>
                        <td align="right">
                            <asp:ImageButton ID="imgL3" runat="server" ImageUrl="~/Images/DashBoardImage/back.png"
                                Width="20px" Height="20px" OnClick="imgL3_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:GridView ID="GV_L3" runat="server" CssClass="Grid" Width="100%" DataKeyNames="GB_ID3"
                                AutoGenerateColumns="False">
                                <Columns>
                                    <asp:TemplateField HeaderText="Department">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnk_GB3_Name" runat="server" Text='<%# Eval("GBName") %>' OnClick="lnk_GB3_Name_Click"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="GB" HeaderText="Balance">
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                </Columns>
                                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                <HeaderStyle CssClass="GridHeader" />
                                <AlternatingRowStyle CssClass="GrdAltRow" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </div>
            <div id="div_Level4" runat="server" visible="true">
                <table width="100%">
                    <tr>
                        <td align="right">
                            <asp:ImageButton ID="imgL4" runat="server" ImageUrl="~/Images/DashBoardImage/back.png"
                                Width="20px" Height="20px" OnClick="imgL4_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:GridView ID="GV_L4" runat="server" CssClass="Grid" Width="100%" DataKeyNames="GB_ID4"
                                AutoGenerateColumns="False">
                                <Columns>
                                    <asp:TemplateField HeaderText="Department">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnk_GB4_Name" runat="server" Text='<%# Eval("GBName") %>' OnClick="lnk_GB4_Name_Click"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="GB" HeaderText="Balance">
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                </Columns>
                                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                <HeaderStyle CssClass="GridHeader" />
                                <AlternatingRowStyle CssClass="GrdAltRow" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </div>
            <div id="div_Level5" runat="server" visible="true">
                <table width="100%">
                    <tr>
                        <td align="right">
                            <asp:ImageButton ID="imgL5" runat="server" ImageUrl="~/Images/DashBoardImage/back.png"
                                Width="20px" Height="20px" OnClick="imgL5_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:GridView ID="GV_L5" runat="server" CssClass="Grid" Width="100%" DataKeyNames="GB_ID4"
                                AutoGenerateColumns="False">
                                <Columns>
                                    <asp:BoundField DataField="GBName" HeaderText="Account Code">
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="GB" HeaderText="Balance">
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                </Columns>
                                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                <HeaderStyle CssClass="GridHeader" />
                                <AlternatingRowStyle CssClass="GrdAltRow" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div id="divGBFilter" style="display: none; position: fixed; background-color: ThreeDFace;
            left: 10px; top: 315px; width: 450px;">
            <div class="divRowContainer">
                <div class="lblBox" style="float: left; width: 100px;" id="lblFirstName">
                    From Date
                </div>
                <div class="divtxtBox" style="float: left; width: 100px">
                    <asp:TextBox ID="txtFromDate" runat="server" ToolTip="From Date" TabIndex="1" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]]  RequiredField txtBox"></asp:TextBox>
                    <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calExtender1" TargetControlID="txtFromDate"
                        OnClientDateSelectionChanged="checkDate" />
                </div>
                <div class="lblBox" style="float: left; width: 100px;" id="Div13">
                    To Date
                </div>
                <div class="divtxtBox" style="float: left; width: 100px">
                    <asp:TextBox ID="txtToDate" runat="server" ToolTip="To Date" TabIndex="4" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]]  RequiredField txtBox"></asp:TextBox>
                    <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtToDate"
                        OnClientDateSelectionChanged="checkDate" />
                </div>
            </div>
            <div class="divClear_10" style="width: 50%">
            </div>
            <div class="divRowContainer">
                <div class="lblBox" style="float: left; width: 100px;" id="Div14">
                    Global Segment
                </div>
                <div class="divtxtBox" style="float: left; width: 300px">
                    <asp:DropDownList ID="ddlGlobalSegment" runat="server" TabIndex="1" CssClass=" ddlStype">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="divClear_10" style="width: 50%">
            </div>
            <div class="divRowContainer">
                <div class="lblBox" style="float: left; width: 100px;" id="Div15">
                    GroupName
                </div>
                <div class="divtxtBox" style="float: left; width: 300px">
                    <asp:DropDownList ID="ddlGroupName" runat="server" TabIndex="2" CssClass=" ddlStype">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="divClear_10" style="width: 50%">
            </div>
            <div style="width: 80px; float: right; display: none; padding-right: 10px">
                <asp:DropDownList ID="ddlAccountCode" runat="server" TabIndex="3" CssClass=" ddlStype">
                </asp:DropDownList>
            </div>
            <div class="divClear_10" style="width: 50%">
            </div>
            <div class="divRowContainer">
                <div class="divtxtBox" style="float: right; width: 400px">
                    <asp:ImageButton ID="imgbtnGBFilter" runat="server" ImageUrl="~/Images/DashBoardImage/agt_back.png"
                        OnClick="imgBtnGenChart_Click" Width="20px" Height="20px" Style="border: 0px" />
                </div>
            </div>
            <div class="divClear_10" style="width: 50%">
            </div>
            <div class="divRowContainer">
            </div>
        </div>
        <div class="divClear_10" style="width: 70%">
        </div>
        <div style="float: left; width: 100%">
            <asp:Chart ID="ChartGroupGL" runat="server" Width="500px" Height="310px" EnableViewState="true">
                <Series>
                    <asp:Series ChartArea="ChartArea1" Name="s_GB" IsValueShownAsLabel="True" XValueMember="GBName"
                        YValueMembers="GB">
                    </asp:Series>
                </Series>
                <ChartAreas>
                    <asp:ChartArea Name="ChartArea1">
                        <Area3DStyle Enable3D="false" Inclination="25" Rotation="40"></Area3DStyle>
                        <AxisX>
                            <MajorGrid Enabled="false" />
                        </AxisX>
                        <AxisY>
                            <MajorGrid Enabled="false" />
                        </AxisY>
                    </asp:ChartArea>
                </ChartAreas>
            </asp:Chart>
        </div>
        <div class="divClear_10" style="width: 50%">
        </div>
        <div id="divempGrid" style="width: 98%; display: none; height: 100px; overflow: auto">
            <div id="divEmpDept" runat="server" visible="true">
                <asp:GridView ID="gvDeptDet" runat="server" CssClass="Grid" Width="100%" DataKeyNames="acct_group_ID"
                    AutoGenerateColumns="False">
                    <Columns>
                        <asp:TemplateField HeaderText="Department">
                            <ItemTemplate>
                                <asp:LinkButton ID="account_group_description" runat="server" Text='<%# Eval("account_group_description") %>'
                                    OnClick="lnk_Group_Click"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="opening_balance" HeaderText="Opening Balance" SortExpression="opening_balance">
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:BoundField>
                        <asp:BoundField DataField="transaction_debit" HeaderText="Debit" SortExpression="transaction_debit">
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:BoundField>
                        <asp:BoundField DataField="transaction_credit" HeaderText="Credit" SortExpression="transaction_credit">
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:BoundField>
                        <asp:BoundField DataField="closing_balance" HeaderText="Closing Balance" SortExpression="closing_balance">
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:BoundField>
                    </Columns>
                    <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                    <HeaderStyle CssClass="GridHeader" />
                    <AlternatingRowStyle CssClass="GrdAltRow" />
                </asp:GridView>
            </div>
        </div>

         <div class="divClear_10" style="width: 70%">
    </div>
    <div id="divGraphData" runat="server">
        <asp:GridView ID="gvGraphdata" runat="server" CssClass="Grid" Width="100%" AutoGenerateColumns="False"  ShowHeaderWhenEmpty="true">
            <Columns>
                <asp:BoundField DataField="GBName" HeaderText="Group Name"></asp:BoundField>
                <asp:BoundField DataField="GB" HeaderText="Amount">
                    <ItemStyle HorizontalAlign="Right" />
                </asp:BoundField>
            </Columns>
            <EmptyDataRowStyle CssClass="EmptyRowStyle" />
            <HeaderStyle CssClass="GridHeader" />
            <AlternatingRowStyle CssClass="GrdAltRow" />
        </asp:GridView>
    </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
</asp:Content>
