﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DashBoard_GL.aspx.cs" Inherits="FIN.Client.GL_DASHBOARD.DashBoard_GL" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .frmStyle
        {
            width: 100%;
            height: 100%;
            background-color: transparent;
            border: 0px solid red;
            top: 0;
            left: 0;
        }
        .frmdiv
        {
            border: 1px solid aqua;
            float:left;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div id="divDashboard">
        <div id="divGLTransaction" class="frmdiv" style="width: 500px; height: 350px;">
            <iframe id="frm_GLTransaction" class="frmStyle" src="GLTransaction_DB.aspx" scrolling="no">
            </iframe>
        </div>
        <div id="divGrpBal" class="frmdiv" style="width: 500px; height: 350px;">
            <iframe id="frm_GLGroupBal" class="frmStyle" src="GLGroupBalances_DB.aspx" scrolling="no">
            </iframe>
        </div>
        <div id="divGLTrail" class="frmdiv" style="width: 500px; height: 350px;">
            <iframe id="frm_GLTrail" class="frmStyle" src="GLTrialBalance_DB.aspx" scrolling="no">
            </iframe>
        </div>
        <div id="divGLTopExp" class="frmdiv" style="width: 500px; height: 350px;">
            <iframe id="frmTopExp" class="frmStyle" src="GLTopExpenses_DB.aspx" scrolling="no">
            </iframe>
        </div>
         <div id="divGLBankBal" class="frmdiv" style="width: 500px; height: 350px;">
            <iframe id="frmBankBal" class="frmStyle" src="GLBankBalance_DB.aspx" scrolling="no">
            </iframe>
        </div>
        <div id="divGLCashBal" class="frmdiv" style="width: 500px; height: 350px;">
            <iframe id="frmCashBal" class="frmStyle" src="GLCashBalance_DB.aspx" scrolling="no">
            </iframe>
        </div>
        <div id="divGLRevenue" class="frmdiv" style="width: 500px; height: 350px;">
            <iframe id="frmGLRevenue" class="frmStyle" src="GLTopRevenue_DB.aspx" scrolling="no">
            </iframe>
        </div>
    </div>
    </form>
</body>
</html>
