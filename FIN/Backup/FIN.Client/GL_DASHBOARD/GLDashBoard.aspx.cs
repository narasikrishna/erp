﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.DAL.GL;
using FIN.BLL;
using VMVServices.Web;
using System.Web.UI.DataVisualization.Charting;
using System.Drawing;

namespace FIN.Client
{
    public partial class GLDashBoard : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                System.Data.DataTable dt_menulist = (System.Data.DataTable)Session["TVMenu"];

                if (Request.QueryString["ProgramID"] != null)
                {
                    Session["ProgramID"] = Request.QueryString["ProgramID"];
                    if (CommonUtils.ConvertStringToInt(Session["ProgramID"].ToString()) > 0)
                    {
                        if (dt_menulist != null)
                        {
                            System.Data.DataRow[] dr_list = dt_menulist.Select("MENU_KEY_ID=" + Session["ProgramID"]);

                            string str_LinkQuery = dr_list[0]["ENTRY_PAGE_OPEN"].ToString() + "?";
                            str_LinkQuery += "ProgramID=" + dr_list[0]["MENU_KEY_ID"].ToString() + "&";
                            str_LinkQuery += "TVYN=Y&";
                            str_LinkQuery += QueryStringTags.Mode.ToString() + "=" + ProgramMode.Add.ToString().Substring(0, 1) + "&";
                            str_LinkQuery += QueryStringTags.ID.ToString() + "=0" + "&";
                            str_LinkQuery += QueryStringTags.AddFlag.ToString() + "=" + dr_list[0]["INSERT_ALLOWED_FLAG"].ToString() + "&";
                            str_LinkQuery += QueryStringTags.UpdateFlag.ToString() + "=" + dr_list[0]["UPDATE_ALLOWED_FLAG"].ToString() + "&";
                            str_LinkQuery += QueryStringTags.DeleteFlag.ToString() + "=" + dr_list[0]["DELETE_ALLOWED_FLAG"].ToString() + "&";
                            str_LinkQuery += QueryStringTags.QueryFlag.ToString() + "=" + dr_list[0]["QUERY_ALLOWED_FLAG"].ToString() + "&";
                            str_LinkQuery += QueryStringTags.ReportName.ToString() + "=" + dr_list[0]["REPORT_NAME"].ToString() + "&";
                            str_LinkQuery += QueryStringTags.ReportName_OL.ToString() + "=" + dr_list[0]["ATTRIBUTE3"].ToString() + "'";

                            Response.Redirect(str_LinkQuery, false);
                        }                        
                    }
                }
            }
        }
    }
}