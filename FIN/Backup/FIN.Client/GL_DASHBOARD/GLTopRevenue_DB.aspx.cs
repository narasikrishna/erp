﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using VMVServices.Web;
using System.Collections;
namespace FIN.Client.GL_DASHBOARD
{
    public partial class GLTopRevenue_DB : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                AssignToControl();
                GenerateRevenueChart();
            }
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                FillComboBox();
                divGraphData.Visible = false;
                divPrintIcon.Visible = false;
                if (Request.QueryString.ToString().ToUpper().Contains("TVYN"))
                {
                    Startup();
                    divGraphData.Visible = true;
                    divPrintIcon.Visible = true;
                    divHeader.Style.Add(" background-color", "white");
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DetailedRevenueATC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }


        private void Startup()
        {
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[FIN.BLL.QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[FIN.BLL.QueryStringTags.ReportName.ToString()]));
        }


        private void FillComboBox()
        {
            string str_finyear = FINSP.GetSPFOR_FiscalYear(DateTime.Now.Date.ToString("dd/MM/yyyy"));
            DataTable dt_cur_period = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountingCalendar_DAL.GetPeriodId4CurrentDate()).Tables[0];

            FIN.BLL.GL.AccountingCalendar_BLL.GetCalAcctYear(ref ddl_R_Year);
            if (ddl_R_Year.Items.Count > 0)
            {
                ddl_R_Year.SelectedValue = str_finyear;
            }
            FIN.BLL.GL.AccountingCalendar_BLL.GetFinancialPeriod4YearAndActual(ref ddl_R_Period, ddl_R_Year.SelectedValue);
            if (dt_cur_period.Rows.Count > 0)
            {
                ddl_R_Period.SelectedValue = dt_cur_period.Rows[0]["PERIOD_ID"].ToString();
            }
        }

        private void GenerateRevenueChart()
        {
            try
            {
                ErrorCollection.Clear();
                DataTable dtData = new DataTable();

                dtData = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountingGroups_DAL.GetIncomeBalance(ddl_R_Period.SelectedValue)).Tables[0];

                Session["RevenueBal"] = dtData;
                chartRevenue.DataSource = dtData;
                chartRevenue.DataBind();

                gvGraphdata.DataSource = dtData;
                gvGraphdata.DataBind();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TopRevenueDetailReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void ddl_R_Year_SelectedIndexChanged(object sender, EventArgs e)
        {
            FIN.BLL.GL.AccountingCalendar_BLL.GetFinancialPeriod4Year(ref ddl_R_Period, ddl_R_Year.SelectedValue);
        }

        protected void ddl_R_Period_SelectedIndexChanged(object sender, EventArgs e)
        {
            GenerateRevenueChart();
        }

        protected void btnGraphRep_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;

                Hashtable htParameters = new Hashtable();
                Hashtable htHeadingParameters = new Hashtable();
                Hashtable htFilterParameter = new Hashtable();

                htFilterParameter.Add("FF_YEAR", ddl_R_Year.SelectedItem.Text);
                htFilterParameter.Add("FF_MONTH", ddl_R_Period.SelectedItem.Text);

                htFilterParameter.Add("PERIOD", ddl_R_Period.SelectedValue.ToString());

                chartRevenue.SaveImage(System.Web.Configuration.WebConfigurationManager.AppSettings["ChartFileFolder"].ToString() + Master.ProgramID + ".png");
                htFilterParameter.Add("ChartPath", System.Web.Configuration.WebConfigurationManager.AppSettings["ChartFileFolder"].ToString() + Master.ProgramID + ".png");


                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;


                ReportData = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountingGroups_DAL.GetIncomeBalance(ddl_R_Period.SelectedValue));


                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FIN.BLL.FINMessageConstatns.CrystalReportViewerReportPath + "?Id=" + Master.ProgramID + "','','" + VMVServices.Services.Data.DbConsts.ReportProperties + "');", true);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ddd", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
     
       
    }
}