﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true" CodeBehind="RPTBudgetVSActualReportParam.aspx.cs" Inherits="FIN.Client.Reports.GL.RPTBudgetVSActualReportParam" %>
<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 370px" id="divMainContainer">
      <div class="divRowContainer divReportAction">
            <table class="ReportTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Show Report" CssClass="btn"  />
                    </td>
                    <td>
                        <asp:Button ID="btnPrint" runat="server" Text="Print" CssClass="btn" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>