﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="True"
    EnableEventValidation="false" CodeBehind="GenericReport.aspx.cs" Inherits="FIN.Client.Reports.GenericReport" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="ListGrid" align="left" style="width: 100%;">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" id="div_Filtser" runat="server">
            <div id="div_Filter" runat="server" style="width: 40%">
                <div id="div_txtFilter_1" style="float: left;" runat="server" visible="false">
                    <div class="lblBox" style="float: left; width: 100px" id="div_Filter_txtlbl_1" runat="server">
                        TXT Filter 1
                    </div>
                    <div style="float: left; width: 100px" id="div_Filter_txt_1" runat="server" visible="false">
                        <asp:TextBox ID="txtFilter1" runat="server" CssClass="txtBox" Width="99%"></asp:TextBox>
                    </div>
                </div>
                <div id="div_txtFilter_2" style="float: left;" runat="server">
                    <div class="lblBox" style="float: left; width: 100px" id="div_Filter_txtlbl_2" runat="server"
                        visible="false">
                        TXT Filter 2
                    </div>
                    <div style="float: right; width: 100px" id="div_Filter_txt_2" runat="server" visible="false">
                        <asp:TextBox ID="txtFilter2" runat="server" CssClass="txtBox" Width="99%"></asp:TextBox>
                    </div>
                </div>
                <div id="div_ddlFilter_1" style="float: left;" runat="server">
                    <div class="lblBox" style="float: left; width: 100px" id="div_Filter_ddllbl_1" runat="server"
                        visible="false">
                        DDL Filter 1
                    </div>
                    <div style="float: left; width: 310px" id="div_Filter_ddl_1" runat="server" visible="false">
                        <asp:DropDownList ID="ddlFilter1" runat="server" CssClass="ddlStype" Width="99%">
                        </asp:DropDownList>
                    </div>
                </div>
                <div id="div_ddlFilter_2" style="float: left;" runat="server">
                    <div class="lblBox" style="float: left; width: 100px" id="div_Filter_ddllbl_2" runat="server"
                        visible="false">
                        DDL Filter 2
                    </div>
                    <div style="float: left; width: 310px" id="div_Filter_ddl_2" runat="server" visible="false">
                        <asp:DropDownList ID="ddlFilter2" runat="server" Width="99%">
                        </asp:DropDownList>
                    </div>
                </div>
                <div id="div_dtFilter_1" style="float: left;" runat="server">
                    <div class="lblBox" style="float: left; width: 100px" id="div_Filter_dtlbl_1" runat="server">
                        Date Filter 1
                    </div>
                    <div style="float: left; width: 100px" id="div_Filter_dt_1" runat="server">
                        <asp:TextBox ID="dtFilter1" runat="server" CssClass="txtBox" Width="99%"></asp:TextBox>
                        <cc2:calendarextender runat="server" format="dd/MM/yyyy" id="calExtender1" targetcontrolid="dtFilter1"
                            onclientdateselectionchanged="checkDate" />
                    </div>
                </div>
                <div id="div_dtFilter_2" style="float: left;" runat="server">
                    <div class="lblBox" style="float: left; width: 100px" id="div_Filter_dtlbl_2" runat="server">
                        Date Filter 2
                    </div>
                    <div style="float: right; width: 100px" id="div_Filter_dt_2" runat="server">
                        <asp:TextBox ID="dtFilter2" runat="server" CssClass="txtBox" Width="99%"></asp:TextBox>
                        <cc2:calendarextender runat="server" format="dd/MM/yyyy" id="CalendarExtender1" targetcontrolid="dtFilter2"
                            onclientdateselectionchanged="checkDate" />
                    </div>
                </div>
            </div>
            <div class="lblBox" style="float: left; width: 50%" id="Div2">
                <asp:Button ID="btnShow" runat="server" Text="Show" CssClass="btn" OnClick="btnGridContSearch_Click" />
                &nbsp;
                <asp:ImageButton ID="imgbtnPDF" runat="server" ImageUrl="~/Images/File_Pdf.png" CommandName="PDF"
                    Height="30px" OnClick="imgbtnPDF_Click" Width="30px" Style="cursor: pointer;
                    cursor: hand;" />
                &nbsp;
                <asp:ImageButton ID="imgbtnEXCEL" runat="server" ImageUrl="~/Images/File_Excel.png"
                    CommandName="EXCEL" Height="30px" OnClick="imgbtnPDF_Click" Width="30px" Style="cursor: pointer;
                    cursor: hand;" />
            </div>
        </div>
        <div style="clear: both">
        </div>
        <div class="divClear_10">
        </div>
        <div class="divClear_10">
        </div>
        <div class="divClear_10">
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" style="border: 4; border-color: rgb(216,216,216); background-color: rgb(216,216,216);
            height: 28px;">
            <div align="center" class="lblBox" style="float: left; width: 780px; text-align: center;
                background-color: rgb(216,216,216);" id="Div3">
                Employee List By Date of Appoinment
            </div>
            <div class="lblBox" style="float: left;" id="lblDate">
                <asp:Label runat="server" ID="lblDOA" CssClass=" txtBox"></asp:Label>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div>
            <asp:GridView ID="gridData" runat="server" AutoGenerateColumns="true" GridLines="None"
                Style="width: 100%;" AllowPaging="true" CssClass="mGrid" PagerStyle-CssClass="pgr"
                AlternatingRowStyle-CssClass="alt" SelectedRowStyle-CssClass="selrow" OnPageIndexChanging="gridData_PageIndexChanging"
                EmptyDataText=" No Record Found" ShowHeaderWhenEmpty="True">
                <Columns>
                </Columns>
                <AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
                <PagerStyle CssClass="pgr"></PagerStyle>
                <SelectedRowStyle CssClass="selrow"></SelectedRowStyle>
            </asp:GridView>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
</asp:Content>
