﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;

using System.Net;

namespace FIN.Client
{
    public class PropertiesFileHeader
    {
        public PropertiesFileHeader()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public static Dictionary<string, string> GetProperties(string path)
        {
            string fileData = "";
            using (StreamReader sr = new StreamReader(path))
            {
                fileData = sr.ReadToEnd().Replace("\r", "").Replace("\"", "").Replace(",", "");

            }
            Dictionary<string, string> Properties = new Dictionary<string, string>();
            string[] kvp;
            string[] records1 = fileData.Split("\n".ToCharArray());
            string[] records = records1.Distinct().ToArray();
            foreach (string record in records)
            {
                if (record.Length > 0)
                {
                    if (record.Contains(':'))
                    {
                        kvp = record.Split(":".ToCharArray());
                        if (!Properties.ContainsKey(kvp[0].Trim()))
                            Properties.Add(kvp[0].Trim(), kvp[1].Trim());
                    }
                }
            }
            return Properties;
        }
    }
}