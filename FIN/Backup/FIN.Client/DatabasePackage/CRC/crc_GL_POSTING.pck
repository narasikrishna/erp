CREATE OR REPLACE PACKAGE GL_POSTING IS
  PROCEDURE INSERT_PROCESS_LOG(P_PROCESS_ID     IN VARCHAR2,
                               P_PROCESS_CODE   IN VARCHAR2,
                               P_PROCESS_ERROR  IN VARCHAR2,
                               P_PROCESS_DATE   DATE,
                               P_PROCESS_ORG_ID IN VARCHAR2,
                               P_PROCESS_STATUS IN VARCHAR2);

  PROCEDURE GL_INSERT(P_JE_HDR_ID              IN VARCHAR2,
                      p_JE_COMP_ID             IN VARCHAR2,
                      P_JE_DATE                IN DATE,
                      P_JE_GLOBAL_SEGMENT_ID   IN VARCHAR2,
                      P_JE_NUMBER              IN VARCHAR2,
                      P_JE_TYPE                IN VARCHAR2,
                      P_JE_CURRENCY_ID         IN VARCHAR2,
                      P_JE_DESC                IN VARCHAR2,
                      P_JE_CURRENCY_RATE_ID    IN VARCHAR2,
                      P_JE_STATUS              IN VARCHAR2,
                      P_JE_CURR_VALUE          IN NUMBER,
                      P_JE_PERIOD_ID           IN VARCHAR2,
                      P_JE_REFERENCE           IN VARCHAR2,
                      P_JE_EXCHANGE_RATE_VALUE IN VARCHAR2,
                      P_JE_TOT_DR_AMOUNT       IN VARCHAR2,
                      P_JE_TOT_CR_AMOUNT       IN VARCHAR2,
                      P_JE_RECURRING_END_DATE  IN DATE,
                      P_JE_RECURRING_FREQUENCY IN VARCHAR2,
                      P_JE_SOURCE              IN VARCHAR2,
                      P_JOURNAL_REVERSED       IN VARCHAR2,
                      P_ACTUAL_JOURNAL_ID      IN VARCHAR2,
                      P_MODULE                 VARCHAR2);

  PROCEDURE GL_INSERT_JR_DTL(P_JE_DTL_ID           IN VARCHAR2,
                             P_JE_HDR_ID           IN VARCHAR2,
                             P_JE_ACCT_CODE_ID     IN VARCHAR2,
                             P_JE_PARTICULARS      VARCHAR2,
                             P_JE_CREDIT_DEBIT     IN CHAR,
                             P_JE_AMOUNT_CR        IN NUMBER,
                             P_JE_ACCOUNTED_AMT_CR IN NUMBER,
                             P_JE_SEGMENT_ID_1     IN VARCHAR2,
                             P_JE_SEGMENT_ID_2     IN VARCHAR2,
                             P_JE_SEGMENT_ID_3     IN VARCHAR2,
                             P_JE_SEGMENT_ID_4     IN VARCHAR2,
                             P_JE_SEGMENT_ID_5     IN VARCHAR2,
                             P_JE_SEGMENT_ID_6     IN VARCHAR2,
                             P_JE_AMOUNT_DR        IN NUMBER,
                             P_JE_ACCOUNTED_AMT_DR IN NUMBER);

  PROCEDURE PAYROLL_POSTING(P_PAYROLL_ID IN VARCHAR2);
  PROCEDURE HR_LEAVE_ENCASH_POSTING(P_LC_ID IN VARCHAR2, P_ORG_ID VARCHAR2);
  PROCEDURE PAYROLL_PAY_ADVICE_POSTING(P_BANK_ID       IN VARCHAR2,
                                       P_PAY_PERIOD_ID IN VARCHAR2);
  ---AP POSTINGS
  procedure AP_GRN_POSTING(P_RECEIPT_ID VARCHAR2, P_ORG_ID VARCHAR2);
  procedure AP_OTHER_COST_POSTING(P_RECEIPT_ID VARCHAR2, P_ORG_ID VARCHAR2);
  procedure AP_MICELLANEOUS_POSTING(P_RECEIPT_ID VARCHAR2,
                                    P_ORG_ID     VARCHAR2);
  procedure AP_INVOICE_POSTING(P_INV_ID VARCHAR2, P_ORG_ID VARCHAR2);
  procedure AP_PAYMENT_POSTING(P_PAYMENT_ID VARCHAR2, P_ORG_ID VARCHAR2);

  ---AR POSTINGS
  procedure AR_INVOICE_POSTING(P_INV_ID VARCHAR2, P_ORG_ID VARCHAR2);
  procedure AR_RECEIPTS_POSTING(P_PAYMENT_ID VARCHAR2, P_ORG_ID VARCHAR2);
  procedure AR_OTHER_COST_POSTING(P_OM_DC_ID VARCHAR2, P_ORG_ID VARCHAR2);
  --FA POSTING
  procedure FA_ASSET_DEPRE_POSTING(P_ASSET_DEPRE_ID VARCHAR2,
                                   P_ORG_ID         VARCHAR2);

  procedure ANNUAL_LEAVE_NON_RECUR_POSTING(P_LC_ID     VARCHAR2,
                                           P_ORG_ID    VARCHAR2,
                                           p_period_id VARCHAR2);
  PROCEDURE INDEM_PROVISION_POSTING(P_ORG_ID VARCHAR2, p_date date);
  PROCEDURE INDEM_PAYMENT_POSTING(P_ORG_ID VARCHAR2, p_date date);
  PROCEDURE LEAVE_PROVISION_POSTING(P_ORG_ID VARCHAR2, p_date date);
  PROCEDURE PETTY_CASH_EXPEND_POSTING(P_GL_PCE_DTL_ID VARCHAR2,
                                      P_ORG_ID        VARCHAR2);
  PROCEDURE PETTY_CASH_ALOCATE_POSTING(P_GL_PCE_DTL_ID VARCHAR2,
                                       P_ORG_ID        VARCHAR2);
END GL_POSTING;
/
CREATE OR REPLACE PACKAGE BODY GL_POSTING AS

  PROCEDURE INSERT_PROCESS_LOG(P_PROCESS_ID     IN VARCHAR2,
                               P_PROCESS_CODE   IN VARCHAR2,
                               P_PROCESS_ERROR  IN VARCHAR2,
                               P_PROCESS_DATE   DATE,
                               P_PROCESS_ORG_ID IN VARCHAR2,
                               P_PROCESS_STATUS IN VARCHAR2) IS
  BEGIN

    insert into SSM_PROCESS_LOG
      (PROCESS_ID,
       PROCESS_CODE,
       PROCESS_ERROR,
       PROCESS_DATE,
       PROCESS_ORG_ID,
       PROCESS_STATUS,
       CREATED_BY,
       CREATED_DATE)
    values
      (P_PROCESS_ID || systimestamp,
       P_PROCESS_CODE,
       P_PROCESS_ERROR,
       P_PROCESS_DATE,
       P_PROCESS_ORG_ID,
       P_PROCESS_STATUS,
       'ADMIN',
       SYSDATE);

  END INSERT_PROCESS_LOG;
  ------------------------------------------------

  PROCEDURE GL_INSERT(P_JE_HDR_ID              IN VARCHAR2,
                      p_JE_COMP_ID             IN VARCHAR2,
                      P_JE_DATE                IN DATE,
                      P_JE_GLOBAL_SEGMENT_ID   IN VARCHAR2,
                      P_JE_NUMBER              IN VARCHAR2,
                      P_JE_TYPE                IN VARCHAR2,
                      P_JE_CURRENCY_ID         IN VARCHAR2,
                      P_JE_DESC                IN VARCHAR2,
                      P_JE_CURRENCY_RATE_ID    IN VARCHAR2,
                      P_JE_STATUS              IN VARCHAR2,
                      P_JE_CURR_VALUE          IN NUMBER,
                      P_JE_PERIOD_ID           IN VARCHAR2,
                      P_JE_REFERENCE           IN VARCHAR2,
                      P_JE_EXCHANGE_RATE_VALUE IN VARCHAR2,
                      P_JE_TOT_DR_AMOUNT       IN VARCHAR2,
                      P_JE_TOT_CR_AMOUNT       IN VARCHAR2,
                      P_JE_RECURRING_END_DATE  IN DATE,
                      P_JE_RECURRING_FREQUENCY IN VARCHAR2,
                      P_JE_SOURCE              IN VARCHAR2,
                      P_JOURNAL_REVERSED       IN VARCHAR2,
                      P_ACTUAL_JOURNAL_ID      IN VARCHAR2,
                      P_MODULE                 VARCHAR2) IS

    V_OUT_NUM VARCHAR2(150);
  BEGIN
    ssm.get_next_sequence_gl(screencode  => P_JE_NUMBER,
                             lang        => 'EN',
                             P_PERIOD_ID => P_JE_PERIOD_ID,
                             P_MODULE    => P_MODULE,
                             P_ORG_ID    => p_JE_COMP_ID,
                             P_SEQ_CODE  => V_OUT_NUM);

    INSERT INTO GL_JOURNAL_HDR
      (PK_ID,
       CHILD_ID,
       JE_HDR_ID,
       JE_COMP_ID,
       JE_DATE,
       JE_GLOBAL_SEGMENT_ID,
       JE_NUMBER,
       JE_TYPE,
       JE_CURRENCY_ID,
       JE_DESC,
       JE_CURRENCY_RATE_ID,
       JE_STATUS,
       ENABLED_FLAG,
       WORKFLOW_COMPLETION_STATUS,
       CREATED_BY,
       CREATED_DATE,
       MODIFIED_BY,
       MODIFIED_DATE,
       ATTRIBUTE1,
       ATTRIBUTE2,
       ATTRIBUTE3,
       ATTRIBUTE4,
       ATTRIBUTE5,
       ATTRIBUTE6,
       ATTRIBUTE7,
       ATTRIBUTE8,
       ATTRIBUTE9,
       ATTRIBUTE10,
       JE_CURR_VALUE,
       JE_PERIOD_ID,
       JE_REFERENCE,
       JE_EXCHANGE_RATE_VALUE,
       JE_TOT_DR_AMOUNT,
       JE_TOT_CR_AMOUNT,
       JE_RECURRING_END_DATE,
       JE_RECURRING_FREQUENCY,
       JE_SOURCE,
       JOURNAL_REVERSED,
       ACTUAL_JOURNAL_ID)
    VALUES
      (NULL,
       NULL,
       P_JE_HDR_ID,
       p_JE_COMP_ID,
       P_JE_DATE,
       P_JE_GLOBAL_SEGMENT_ID,
       V_OUT_NUM, --journal number
       P_JE_TYPE,
       P_JE_CURRENCY_ID,
       P_JE_DESC,
       P_JE_CURRENCY_RATE_ID,
       P_JE_STATUS,
       '1',
       '0',
       'ADMIN',
       trunc(SYSDATE),
       NULL,
       NULL,
       NULL,
       NULL,
       NULL,
       NULL,
       NULL,
       NULL,
       NULL,
       NULL,
       NULL,
       NULL,
       P_JE_CURR_VALUE,
       P_JE_PERIOD_ID,
       P_JE_REFERENCE,
       P_JE_EXCHANGE_RATE_VALUE,
       P_JE_TOT_DR_AMOUNT,
       P_JE_TOT_CR_AMOUNT,
       P_JE_RECURRING_END_DATE,
       P_JE_RECURRING_FREQUENCY,
       P_JE_SOURCE,
       P_JOURNAL_REVERSED,
       P_ACTUAL_JOURNAL_ID);

  END GL_INSERT;

  PROCEDURE GL_INSERT_JR_DTL(P_JE_DTL_ID           IN VARCHAR2,
                             P_JE_HDR_ID           IN VARCHAR2,
                             P_JE_ACCT_CODE_ID     IN VARCHAR2,
                             P_JE_PARTICULARS      VARCHAR2,
                             P_JE_CREDIT_DEBIT     IN CHAR,
                             P_JE_AMOUNT_CR        IN NUMBER,
                             P_JE_ACCOUNTED_AMT_CR IN NUMBER,
                             P_JE_SEGMENT_ID_1     IN VARCHAR2,
                             P_JE_SEGMENT_ID_2     IN VARCHAR2,
                             P_JE_SEGMENT_ID_3     IN VARCHAR2,
                             P_JE_SEGMENT_ID_4     IN VARCHAR2,
                             P_JE_SEGMENT_ID_5     IN VARCHAR2,
                             P_JE_SEGMENT_ID_6     IN VARCHAR2,
                             P_JE_AMOUNT_DR        IN NUMBER,
                             P_JE_ACCOUNTED_AMT_DR IN NUMBER) IS
  BEGIN
    debug_proc(P_JE_DTL_ID || sqlerrm || 'P_JE_DTL_ID');
    INSERT INTO GL_JOURNAL_DTL
      (PK_ID,
       CHILD_ID,
       JE_DTL_ID,
       JE_HDR_ID,
       JE_ACCT_CODE_ID,
       JE_PARTICULARS,
       JE_CREDIT_DEBIT,
       JE_AMOUNT_CR,
       JE_ACCOUNTED_AMT_CR,
       JE_SEGMENT_ID_1,
       JE_SEGMENT_ID_2,
       JE_SEGMENT_ID_3,
       JE_SEGMENT_ID_4,
       JE_SEGMENT_ID_5,
       JE_SEGMENT_ID_6,
       ENABLED_FLAG,
       WORKFLOW_COMPLETION_STATUS,
       CREATED_BY,
       CREATED_DATE,
       MODIFIED_BY,
       MODIFIED_DATE,
       ATTRIBUTE1,
       ATTRIBUTE2,
       ATTRIBUTE3,
       ATTRIBUTE4,
       ATTRIBUTE5,
       ATTRIBUTE6,
       ATTRIBUTE7,
       ATTRIBUTE8,
       ATTRIBUTE9,
       ATTRIBUTE10,
       JE_AMOUNT_DR,
       JE_ACCOUNTED_AMT_DR)
    VALUES
      (NULL,
       NULL,
       ssm.get_next_sequence(P_JE_DTL_ID, 'EN'),
       P_JE_HDR_ID,
       P_JE_ACCT_CODE_ID,
       P_JE_PARTICULARS,
       P_JE_CREDIT_DEBIT,
       P_JE_AMOUNT_CR,
       P_JE_ACCOUNTED_AMT_CR,
       P_JE_SEGMENT_ID_1,
       P_JE_SEGMENT_ID_2,
       P_JE_SEGMENT_ID_3,
       P_JE_SEGMENT_ID_4,
       P_JE_SEGMENT_ID_5,
       P_JE_SEGMENT_ID_6,
       1,
       1,
       'ADMIN',
       TRUNC(SYSDATE),
       NULL,
       NULL,
       NULL,
       NULL,
       NULL,
       NULL,
       NULL,
       NULL,
       NULL,
       NULL,
       NULL,
       NULL,
       P_JE_AMOUNT_DR,
       P_JE_ACCOUNTED_AMT_DR);

  END GL_INSERT_JR_DTL;

  PROCEDURE PAYROLL_POSTING(P_PAYROLL_ID IN VARCHAR2) IS
    v_JE_HDR_ID VARCHAR2(50);

    /*PR_AMT   SYS_REFCURSOR;
    C_PR_DTL PAY_FINAL_RUN_DTL%ROWTYPE;*/

  BEGIN

    FOR C_PR IN (SELECT 'GL_007_M' AS JE_HDR_ID,
                        C.COMP_ID,
                        trunc(SYSDATE) AS JE_DATE,
                        d.segment_value_id as COMP_GLOBAL_SEGMENT_ID,
                        'GL_007_JN' AS JE_NUM,
                        'STANDARD' AS JE_TYP,
                        cd.comp_base_currency AS JE_CURRENCY_ID,
                        (select gg.period_name
                           from gl_comp_acct_period_dtl gg,
                                GL_ACCT_CALENDAR_DTL    CDI
                          where gg.enabled_flag = '1'
                            and gg.workflow_completion_status = '1'
                            AND GG.CAL_DTL_ID = CDI.CAL_DTL_ID
                            and gg.PERIOD_STATUS = 'OPEN'
                            and gg.COMP_ID = C.COMP_ID
                            and trunc(sysdate) between gg.period_from_dt and
                                gg.period_to_dt) AS JE_DESC,

                        NULL AS JE_CURRENCY_RATE_ID,
                        'OPEN' AS JE_STATUS,
                        NULL AS JE_CURR_VALUE,

                        (select gg.period_id
                           from gl_comp_acct_period_dtl gg,
                                GL_ACCT_CALENDAR_DTL    CDI
                          where gg.enabled_flag = '1'
                            and gg.workflow_completion_status = '1'
                            AND GG.CAL_DTL_ID = CDI.CAL_DTL_ID
                            and gg.PERIOD_STATUS = 'OPEN'
                            and gg.COMP_ID = C.COMP_ID
                            and trunc(sysdate) between gg.period_from_dt and
                                gg.period_to_dt) AS JE_PERIOD_ID,

                        PD.PAYROLL_DTL_ID AS JE_REFERENCE,
                        NULL AS JE_EXCHANGE_RATE_VALUE,
                        (SELECT SUM(ROUND(PTRDD.PAY_AMOUNT, 2))
                           FROM PAY_ELEMENTS          PE,
                                PAY_FINAL_RUN_DTL_DTL PTRDD,
                                PAY_FINAL_RUN_DTL     PD,
                                GL_ACCT_CODES         AC
                          WHERE PE.PAY_ELEMENT_ID = PTRDD.PAY_EMP_ELEMENT_ID
                            AND PD.PAYROLL_DTL_ID = PTRDD.PAYROLL_DTL_ID
                            AND PD.PAYROLL_ID = P_PAYROLL_ID
                            AND PE.RECEIVABLE_ID = AC.ACCT_CODE_ID) AS JE_TOT_DR_AMOUNT,
                        SUM(round(PTRDD.PAY_AMOUNT, 2)) AS JE_TOT_CR_AMOUNT,
                        trunc(SYSDATE) ASJE_RECURRING_END_DATE,
                        NULL AS JE_RECURRING_FREQUENCY,
                        'PER' AS JE_SOURCE,
                        NULL AS JOURNAL_REVERSED,
                        NULL AS ACTUAL_JOURNAL_ID
                   FROM PAY_ELEMENTS          PE,
                        PAY_FINAL_RUN_HDR     PTR,
                        PAY_FINAL_RUN_DTL_DTL PTRDD,
                        GL_ACCT_CODES         AC,
                        PAY_FINAL_RUN_DTL     PD,
                        HR_DEPARTMENTS        D,
                        HR_EMPLOYEES          E,
                        GL_COMPANIES_HDR      C,
                        GL_COMPANIES_DTL      cd
                  WHERE PE.GL_ID = AC.ACCT_CODE_ID
                    AND PTRDD.PAY_EMP_ID = E.EMP_ID
                    AND E.EMP_ORG_ID = C.COMP_ID
                    AND PTRDD.PAYROLL_DTL_ID = PD.PAYROLL_DTL_ID
                    AND PD.PAYROLL_ID = PTR.PAYROLL_ID
                    AND D.DEPT_ID = PD.PAY_DEPT_ID
                    and c.comp_id = cd.comp_id
                    AND PE.PAY_ELEMENT_ID = PTRDD.PAY_EMP_ELEMENT_ID
                    AND PTR.PAYROLL_ID = P_PAYROLL_ID
                  GROUP BY D.DEPT_ID,
                           D.SEGMENT_VALUE_ID,
                           PD.PAYROLL_DTL_ID,
                           C.COMP_ID,
                           CD.COMP_BASE_CURRENCY)

     LOOP
      v_JE_HDR_ID := ssm.get_next_sequence(C_PR.JE_HDR_ID, 'EN');

      GL_INSERT(P_JE_HDR_ID              => v_JE_HDR_ID,
                p_JE_COMP_ID             => C_PR.COMP_ID,
                P_JE_DATE                => C_PR.JE_DATE,
                P_JE_GLOBAL_SEGMENT_ID   => C_PR.COMP_GLOBAL_SEGMENT_ID,
                P_JE_NUMBER              => C_PR.JE_NUM,
                P_JE_TYPE                => C_PR.JE_TYP,
                P_JE_CURRENCY_ID         => C_PR.JE_CURRENCY_ID,
                P_JE_DESC                => C_PR.JE_DESC,
                P_JE_CURRENCY_RATE_ID    => C_PR.JE_CURRENCY_RATE_ID,
                P_JE_STATUS              => C_PR.JE_STATUS,
                P_JE_CURR_VALUE          => C_PR.JE_CURR_VALUE,
                P_JE_PERIOD_ID           => C_PR.JE_PERIOD_ID,
                P_JE_REFERENCE           => C_PR.JE_REFERENCE,
                P_JE_EXCHANGE_RATE_VALUE => C_PR.JE_EXCHANGE_RATE_VALUE,
                P_JE_TOT_DR_AMOUNT       => C_PR.JE_TOT_DR_AMOUNT,
                P_JE_TOT_CR_AMOUNT       => C_PR.JE_TOT_CR_AMOUNT,
                P_JE_RECURRING_END_DATE  => C_PR.ASJE_RECURRING_END_DATE,
                P_JE_RECURRING_FREQUENCY => C_PR.JE_RECURRING_FREQUENCY,
                P_JE_SOURCE              => C_PR.JE_SOURCE,
                P_JOURNAL_REVERSED       => C_PR.JOURNAL_REVERSED,
                P_ACTUAL_JOURNAL_ID      => C_PR.ACTUAL_JOURNAL_ID,
                P_MODULE                 => 'PER');

      /* FETCH PR_AMT
        IN C_PR;
      EXIT WHEN PR_AMT%NOTFOUND;*/

      -- FOR C_PR IN PR_AMT LOOP

      FOR C_PR_JRD IN (select sum(round(ptrdd.pay_amount, 2)) as JE_AMOUNT_CR,
                              sum(round(ptrdd.pay_amount, 2)) as JE_ACCOUNTED_AMT_CR,
                              'GL_007_D' as JE_DTL_ID,
                              ac.acct_code,
                              ac.acct_code_id,
                              null as JE_PARTICULARS,
                              'Dr' as JE_CREDIT_DEBIT,
                              null as JE_AMOUNT_DR,
                              null as JE_ACCOUNTED_AMT_DR,
                              null as JE_SEGMENT_ID_1,
                              null as JE_SEGMENT_ID_2,
                              null as JE_SEGMENT_ID_3,
                              null as JE_SEGMENT_ID_4,
                              null as JE_SEGMENT_ID_5,
                              null as JE_SEGMENT_ID_6
                         from PAY_ELEMENTS          pe,
                              PAY_FINAL_RUN_HDR     ptr,
                              Pay_FINAL_Run_Dtl_Dtl ptrdd,
                              gl_acct_codes         ac,
                              Pay_FINAL_Run_Dtl     pd
                        where pe.gl_id = ac.acct_code_id
                          and ptrdd.payroll_dtl_id = pd.payroll_dtl_id
                          and pd.payroll_id = ptr.payroll_id
                          and pe.pay_element_id = ptrdd.pay_emp_element_id
                          and pD.Payroll_Dtl_Id = C_PR.JE_REFERENCE
                        group by ac.acct_code_id, ac.acct_code

                       union

                       select null as JE_AMOUNT_CR,
                              null as JE_ACCOUNTED_AMT_CR,
                              'GL_007_D' as JE_DTL_ID,
                              ac.acct_code,
                              ac.acct_code_id,
                              null as JE_PARTICULARS,
                              'Cr' as JE_CREDIT_DEBIT,
                              sum(round(ptrdd.pay_amount, 2)) as JE_AMOUNT_DR,
                              sum(round(ptrdd.pay_amount, 2)) as JE_ACCOUNTED_AMT_DR,
                              null as JE_SEGMENT_ID_1,
                              null as JE_SEGMENT_ID_2,
                              null as JE_SEGMENT_ID_3,
                              null as JE_SEGMENT_ID_4,
                              null as JE_SEGMENT_ID_5,
                              null as JE_SEGMENT_ID_6
                         from PAY_ELEMENTS          pe,
                              PAY_FINAL_RUN_HDR     ptr,
                              Pay_FINAL_Run_Dtl_Dtl ptrdd,
                              gl_acct_codes         ac,
                              Pay_FINAL_Run_Dtl     pd
                        where pe.receivable_id = ac.acct_code_id
                          and ptrdd.payroll_dtl_id = pd.payroll_dtl_id
                          and pd.payroll_id = ptr.payroll_id
                          and pe.pay_element_id = ptrdd.pay_emp_element_id
                          and pD.Payroll_Dtl_Id = C_PR.JE_REFERENCE
                        group by ac.acct_code_id, ac.acct_code) LOOP

        -- FOR C_PR_JRD IN PR_JRD LOOP
        GL_INSERT_JR_DTL(P_JE_DTL_ID           => C_PR_JRD.JE_DTL_ID,
                         P_JE_HDR_ID           => v_JE_HDR_ID,
                         P_JE_ACCT_CODE_ID     => C_PR_JRD.ACCT_CODE_ID,
                         P_JE_PARTICULARS      => C_PR_JRD.JE_PARTICULARS,
                         P_JE_CREDIT_DEBIT     => C_PR_JRD.JE_CREDIT_DEBIT,
                         P_JE_AMOUNT_CR        => C_PR_JRD.JE_AMOUNT_CR,
                         P_JE_ACCOUNTED_AMT_CR => C_PR_JRD.JE_ACCOUNTED_AMT_CR,
                         P_JE_SEGMENT_ID_1     => C_PR_JRD.JE_SEGMENT_ID_1,
                         P_JE_SEGMENT_ID_2     => C_PR_JRD.JE_SEGMENT_ID_2,
                         P_JE_SEGMENT_ID_3     => C_PR_JRD.JE_SEGMENT_ID_3,
                         P_JE_SEGMENT_ID_4     => C_PR_JRD.JE_SEGMENT_ID_4,
                         P_JE_SEGMENT_ID_5     => C_PR_JRD.JE_SEGMENT_ID_5,
                         P_JE_SEGMENT_ID_6     => C_PR_JRD.JE_SEGMENT_ID_6,
                         P_JE_AMOUNT_DR        => C_PR_JRD.JE_AMOUNT_DR,
                         P_JE_ACCOUNTED_AMT_DR => C_PR_JRD.JE_ACCOUNTED_AMT_DR);

      END LOOP;
    END LOOP;
    COMMIT;

  EXCEPTION
    WHEN OTHERS THEN
      NULL;

  END PAYROLL_POSTING;

  PROCEDURE HR_LEAVE_ENCASH_POSTING(P_LC_ID IN VARCHAR2, P_ORG_ID VARCHAR2) IS
    v_JE_HDR_ID VARCHAR2(50);

  BEGIN

    FOR C_JH IN (select 'GL_007_M' AS JE_HDR_ID,
                        le.lc_org_id AS COMP_ID,
                        (SELECT sv.segment_value_id
                           FROM GL_COMPANIES_HDR  CH,
                                GL_SEGMENTS       s,
                                gl_segment_values sv
                          WHERE CH.COMP_ID = le.LC_ORG_Id
                            and s.segment_id = sv.segment_id
                            and ch.comp_global_segment_id = s.segment_id
                            AND CH.COMP_ID = P_ORG_ID
                            AND ROWNUM = 1) AS COMP_GLOBAL_SEGMENT_ID,
                        trunc(SYSDATE) AS JE_DATE,
                        'GL_007_JN' AS JE_NUM,
                        'STANDARD' AS JE_TYP,
                        'HR ANNUAL LEAVE POSTING' AS JE_DESC,
                        (select cd.comp_base_currency
                           from gl_companies_dtl cd
                          where cd.comp_id = ch.comp_id
                            and cd.comp_id = P_ORG_ID
                            AND ROWNUM = 1) as JE_CURRENCY_ID,
                        NULL AS JE_CURRENCY_RATE_ID,
                        'OPEN' AS JE_STATUS,
                        NULL AS JE_CURR_VALUE,
                        (select gg.period_id
                           from gl_comp_acct_period_dtl gg,
                                GL_ACCT_CALENDAR_DTL    CDI
                          where gg.enabled_flag = '1'
                            and gg.workflow_completion_status = '1'
                            AND GG.CAL_DTL_ID = CDI.CAL_DTL_ID
                            and gg.PERIOD_STATUS = 'OPEN'
                            and gg.COMP_ID = Ch.COMP_ID
                            and trunc(le.lc_date) between gg.period_from_dt and
                                gg.period_to_dt) AS JE_PERIOD_ID,
                        LE.LC_ID AS JE_REFERENCE,
                        NULL AS JE_EXCHANGE_RATE_VALUE,
                        trunc(SYSDATE) AS JE_RECURRING_END_DATE,
                        NULL AS JE_RECURRING_FREQUENCY,
                        'HR' AS JE_SOURCE,
                        NULL AS JOURNAL_REVERSED,
                        NULL AS ACTUAL_JOURNAL_ID,
                        SUM(le.lc_leave_salary) as JE_TOT_CR_AMOUNT,
                        SUM(le.lc_leave_salary) as JE_TOT_DR_AMOUNT
                   from hr_leave_encashment le, GL_COMPANIES_HDR ch
                  where le.LC_ORG_Id = ch.comp_id
                    AND CH.ENABLED_FLAG = 1
                    AND CH.WORKFLOW_COMPLETION_STATUS = 1
                    AND le.ENABLED_FLAG = '1'
                    and le.workflow_completion_status = 1
                    AND LE.LC_ID = P_LC_ID
                    AND LE.LC_ORG_ID = P_ORG_ID
                    and UPPER(le.lc_payment_option) = 'PAY_NOW'
                  GROUP BY LE.LC_ID, le.lc_org_id, ch.comp_id

                 )

     LOOP
      v_JE_HDR_ID := ssm.get_next_sequence(C_JH.JE_HDR_ID, 'EN');

      GL_INSERT(P_JE_HDR_ID              => v_JE_HDR_ID,
                p_JE_COMP_ID             => C_JH.COMP_ID,
                P_JE_DATE                => C_JH.JE_DATE,
                P_JE_GLOBAL_SEGMENT_ID   => C_JH.COMP_GLOBAL_SEGMENT_ID,
                P_JE_NUMBER              => C_JH.JE_NUM,
                P_JE_TYPE                => C_JH.JE_TYP,
                P_JE_CURRENCY_ID         => C_JH.JE_CURRENCY_ID,
                P_JE_DESC                => 'PER:Number:' || C_JH.JE_DESC ||
                                            ', Amount:' ||
                                            C_JH.JE_TOT_DR_AMOUNT,
                P_JE_CURRENCY_RATE_ID    => C_JH.JE_CURRENCY_RATE_ID,
                P_JE_STATUS              => C_JH.JE_STATUS,
                P_JE_CURR_VALUE          => C_JH.JE_CURR_VALUE,
                P_JE_PERIOD_ID           => C_JH.JE_PERIOD_ID,
                P_JE_REFERENCE           => 'PER:Number:' ||
                                            C_JH.JE_REFERENCE,
                P_JE_EXCHANGE_RATE_VALUE => C_JH.JE_EXCHANGE_RATE_VALUE,
                P_JE_TOT_DR_AMOUNT       => C_JH.JE_TOT_DR_AMOUNT,
                P_JE_TOT_CR_AMOUNT       => C_JH.JE_TOT_CR_AMOUNT,
                P_JE_RECURRING_END_DATE  => C_JH.JE_RECURRING_END_DATE,
                P_JE_RECURRING_FREQUENCY => C_JH.JE_RECURRING_FREQUENCY,
                P_JE_SOURCE              => C_JH.JE_SOURCE,
                P_JOURNAL_REVERSED       => C_JH.JOURNAL_REVERSED,
                P_ACTUAL_JOURNAL_ID      => C_JH.ACTUAL_JOURNAL_ID,
                P_MODULE                 => 'PER');

      FOR C_HR_JRD IN (select LE.LC_ID,
                              'GL_007_D' as JE_DTL_ID,
                              pe.gl_id AS ACCT_CODE_ID,
                              (nvl(le.lc_leave_salary, 0) +
                              nvl(le.lc_air_ticket_amt, 0)) as JE_AMOUNT_CR,
                              (nvl(le.lc_leave_salary, 0) +
                              nvl(le.lc_air_ticket_amt, 0)) as JE_ACCOUNTED_AMT_CR,
                              'Cr' as JE_CREDIT_DEBIT,
                              0 AS JE_AMOUNT_DR,
                              0 AS JE_ACCOUNTED_AMT_DR
                         from hr_leave_encashment le,
                              pay_elements        pe,
                              ssm_system_options  so,
                              gl_acct_codes       ac
                        where ac.acct_code_id = pe.gl_id
                          and pe.pay_element_id = so.hr_leave_encash
                          AND LE.LC_ID = P_LC_ID
                          AND LE.LC_ORG_ID = P_ORG_ID
                          and UPPER(le.lc_payment_option) = 'PAY_NOW'
                       union
                       select LE.LC_ID,
                              'GL_007_D' as JE_DTL_ID,
                              ba.gl_account AS ACCT_CODE_ID,
                              0 AS JE_AMOUNT_CR,
                              0 AS JE_ACCOUNTED_AMT_CR,
                              'Dr' as JE_CREDIT_DEBIT,
                              (nvl(le.lc_leave_salary, 0) +
                              nvl(le.lc_air_ticket_amt, 0)) -
                              nvl(le.attribute1, 0) as JE_AMOUNT_DR,
                              (nvl(le.lc_leave_salary, 0) +
                              nvl(le.lc_air_ticket_amt, 0)) -
                              nvl(le.attribute1, 0) as JE_ACCOUNTED_AMT_DR

                         from hr_leave_encashment le,
                              ca_bank_accounts    ba,
                              ca_bank             b
                        where le.bank_id = b.bank_id
                          and ba.bank_id = b.bank_id
                          and UPPER(le.lc_payment_option) = 'PAY_NOW'
                          AND LE.LC_ID = P_LC_ID
                          AND LE.LC_ORG_ID = P_ORG_ID
                       union
                       select LE.LC_ID,
                              'GL_007_D' as JE_DTL_ID,
                              ppe.gl_id AS ACCT_CODE_ID,
                              0 AS JE_AMOUNT_CR,
                              0 AS JE_ACCOUNTED_AMT_CR,
                              'Dr' as JE_CREDIT_DEBIT,
                              nvl(SE.PAY_AMOUNT, 0) as JE_AMOUNT_DR,
                              nvl(SE.PAY_AMOUNT, 0) as JE_ACCOUNTED_AMT_DR

                         from hr_leave_encashment      le,
                              pay_elements             ppe,
                              ca_bank                  b,
                              HR_EMP_LEAVE_SAL_ELEMENT SE
                        where le.bank_id = b.bank_id
                          and ppe.pay_element_id = se.pay_element_id
                          AND SE.LC_ID = LE.LC_ID
                          AND SE.PAY_EMP_ID = LE.LC_EMP_ID
                          and UPPER(le.lc_payment_option) = 'PAY_NOW'
                          AND LE.LC_ID = P_LC_ID
                          AND LE.LC_ORG_ID = P_ORG_ID

                       ) LOOP

        -- FOR C_PR_JRD IN PR_JRD LOOP
        GL_INSERT_JR_DTL(P_JE_DTL_ID           => C_HR_JRD.JE_DTL_ID,
                         P_JE_HDR_ID           => v_JE_HDR_ID,
                         P_JE_ACCT_CODE_ID     => C_HR_JRD.ACCT_CODE_ID,
                         P_JE_PARTICULARS      => NULL,
                         P_JE_CREDIT_DEBIT     => C_HR_JRD.JE_CREDIT_DEBIT,
                         P_JE_AMOUNT_CR        => C_HR_JRD.JE_AMOUNT_CR,
                         P_JE_ACCOUNTED_AMT_CR => C_HR_JRD.JE_ACCOUNTED_AMT_CR,
                         P_JE_SEGMENT_ID_1     => NULL,
                         P_JE_SEGMENT_ID_2     => NULL,
                         P_JE_SEGMENT_ID_3     => NULL,
                         P_JE_SEGMENT_ID_4     => NULL,
                         P_JE_SEGMENT_ID_5     => NULL,
                         P_JE_SEGMENT_ID_6     => NULL,
                         P_JE_AMOUNT_DR        => C_HR_JRD.JE_AMOUNT_DR,
                         P_JE_ACCOUNTED_AMT_DR => C_HR_JRD.JE_ACCOUNTED_AMT_DR);

      END LOOP;
    END LOOP;
    COMMIT;

  EXCEPTION
    WHEN OTHERS THEN
      NULL;

  END HR_LEAVE_ENCASH_POSTING;

  PROCEDURE PAYROLL_PAY_ADVICE_POSTING(P_BANK_ID       IN VARCHAR2,
                                       P_PAY_PERIOD_ID IN VARCHAR2) IS
    v_JE_HDR_ID VARCHAR2(50);

  BEGIN

    FOR C_PR IN (select 'GL_007_M' AS JE_HDR_ID,
                        pa.pay_org_id AS COMP_ID,
                        d.segment_value_id as COMP_GLOBAL_SEGMENT_ID,
                        trunc(SYSDATE) AS JE_DATE,
                        'GL_007_JN' AS JE_NUM,
                        'STANDARD' AS JE_TYP,
                        'PER PAY ADVICE POSTING' AS JE_DESC,
                        BA.ACCT_CURRENCY AS JE_CURRENCY_ID,
                        NULL AS JE_CURRENCY_RATE_ID,
                        'OPEN' AS JE_STATUS,
                        NULL AS JE_CURR_VALUE,
                        NULL AS JE_PERIOD_ID,
                        PA.PAY_ADV_ID AS JE_REFERENCE,
                        NULL AS JE_EXCHANGE_RATE_VALUE,
                        trunc(SYSDATE) ASJE_RECURRING_END_DATE,
                        NULL AS JE_RECURRING_FREQUENCY,
                        'PER' AS JE_SOURCE,
                        NULL AS JOURNAL_REVERSED,
                        NULL AS ACTUAL_JOURNAL_ID,

                        SUM((select sum(ROUND(ptr.pay_amount, 3))
                               from pay_elements          pe,
                                    pay_final_run_dtl_dtl ptr,
                                    pay_final_run_dtl     frd
                              where pe.pay_element_id =
                                    ptr.pay_emp_element_id
                                and frd.payroll_dtl_id = ptr.payroll_dtl_id
                                and ptr.payroll_dtl_id = pa.payroll_dtl_id
                                and pe.pay_ele_class = 'Earning') -
                            (select sum(ROUND(ptr.pay_amount, 3))
                               from pay_elements          pe,
                                    pay_final_run_dtl_dtl ptr,
                                    pay_final_run_dtl     frd
                              where pe.pay_element_id =
                                    ptr.pay_emp_element_id
                                and frd.payroll_dtl_id = ptr.payroll_dtl_id
                                and ptr.payroll_dtl_id = pa.payroll_dtl_id
                                and pe.pay_ele_class = 'Deduction')) as JE_TOT_CR_AMOUNT,

                        SUM((select sum(ROUND(ptr.pay_amount, 3))
                               from pay_elements          pe,
                                    pay_final_run_dtl_dtl ptr,
                                    pay_final_run_dtl     frd
                              where pe.pay_element_id =
                                    ptr.pay_emp_element_id
                                and frd.payroll_dtl_id = ptr.payroll_dtl_id
                                and ptr.payroll_dtl_id = pa.payroll_dtl_id
                                and pe.pay_ele_class = 'Earning') -
                            (select sum(ROUND(ptr.pay_amount, 3))
                               from pay_elements          pe,
                                    pay_final_run_dtl_dtl ptr,
                                    pay_final_run_dtl     frd
                              where pe.pay_element_id =
                                    ptr.pay_emp_element_id
                                and frd.payroll_dtl_id = ptr.payroll_dtl_id
                                and ptr.payroll_dtl_id = pa.payroll_dtl_id
                                and pe.pay_ele_class = 'Deduction')) as JE_TOT_DR_AMOUNT,
                        'GL_007_D' as JE_DTL_ID,
                        null as JE_PARTICULARS,
                        'Dr' as JE_CREDIT_DEBIT,
                        null as JE_AMOUNT_CR,
                        null as JE_ACCOUNTED_AMT_CR,
                        null as JE_SEGMENT_ID_1,
                        null as JE_SEGMENT_ID_2,
                        null as JE_SEGMENT_ID_3,
                        null as JE_SEGMENT_ID_4,
                        null as JE_SEGMENT_ID_5,
                        null as JE_SEGMENT_ID_6
                   from pay_advice        pa,
                        ca_bank_accounts  ba,
                        HR_DEPARTMENTS    D,
                        pay_final_run_dtl FRD
                  where pa.bank_id = ba.bank_id
                    AND PA.PAYROLL_DTL_ID = FRD.PAYROLL_DTL_ID
                    AND D.DEPT_ID = FRD.PAY_DEPT_ID
                    AND PA.BANK_ID = P_BANK_ID
                    AND PA.PAY_PERIOD_ID = P_PAY_PERIOD_ID
                  GROUP BY D.DEPT_ID,
                           pa.pay_org_id,
                           d.segment_value_id,
                           BA.ACCT_CURRENCY,
                           PA.PAY_ADV_ID)

     LOOP
      v_JE_HDR_ID := ssm.get_next_sequence(C_PR.JE_HDR_ID, 'EN');

      GL_INSERT(P_JE_HDR_ID              => v_JE_HDR_ID,
                p_JE_COMP_ID             => C_PR.COMP_ID,
                P_JE_DATE                => C_PR.JE_DATE,
                P_JE_GLOBAL_SEGMENT_ID   => C_PR.COMP_GLOBAL_SEGMENT_ID,
                P_JE_NUMBER              => C_PR.JE_NUM,
                P_JE_TYPE                => C_PR.JE_TYP,
                P_JE_CURRENCY_ID         => C_PR.JE_CURRENCY_ID,
                P_JE_DESC                => C_PR.JE_DESC,
                P_JE_CURRENCY_RATE_ID    => C_PR.JE_CURRENCY_RATE_ID,
                P_JE_STATUS              => C_PR.JE_STATUS,
                P_JE_CURR_VALUE          => C_PR.JE_CURR_VALUE,
                P_JE_PERIOD_ID           => C_PR.JE_PERIOD_ID,
                P_JE_REFERENCE           => C_PR.JE_REFERENCE,
                P_JE_EXCHANGE_RATE_VALUE => C_PR.JE_EXCHANGE_RATE_VALUE,
                P_JE_TOT_DR_AMOUNT       => C_PR.JE_TOT_DR_AMOUNT,
                P_JE_TOT_CR_AMOUNT       => C_PR.JE_TOT_CR_AMOUNT,
                P_JE_RECURRING_END_DATE  => C_PR.ASJE_RECURRING_END_DATE,
                P_JE_RECURRING_FREQUENCY => C_PR.JE_RECURRING_FREQUENCY,
                P_JE_SOURCE              => C_PR.JE_SOURCE,
                P_JOURNAL_REVERSED       => C_PR.JOURNAL_REVERSED,
                P_ACTUAL_JOURNAL_ID      => C_PR.ACTUAL_JOURNAL_ID,
                P_MODULE                 => 'PER');

      FOR C_PR_JRD IN (select ba.gl_account as acct_code_id,
                              ((select sum(ROUND(ptr.pay_amount, 3))
                                  from pay_elements          pe,
                                       pay_final_run_dtl_dtl ptr,
                                       pay_final_run_dtl     frd
                                 where pe.pay_element_id =
                                       ptr.pay_emp_element_id
                                   and frd.payroll_dtl_id =
                                       ptr.payroll_dtl_id
                                   and ptr.payroll_dtl_id = pa.payroll_dtl_id
                                   and pe.pay_ele_class = 'Earning') -
                              (select sum(ROUND(ptr.pay_amount, 3))
                                  from pay_elements          pe,
                                       pay_final_run_dtl_dtl ptr,
                                       pay_final_run_dtl     frd
                                 where pe.pay_element_id =
                                       ptr.pay_emp_element_id
                                   and frd.payroll_dtl_id =
                                       ptr.payroll_dtl_id
                                   and ptr.payroll_dtl_id = pa.payroll_dtl_id
                                   and pe.pay_ele_class = 'Deduction')) as JE_AMOUNT_DR,
                              ((select sum(ROUND(ptr.pay_amount, 3))
                                  from pay_elements          pe,
                                       pay_final_run_dtl_dtl ptr,
                                       pay_final_run_dtl     frd
                                 where pe.pay_element_id =
                                       ptr.pay_emp_element_id
                                   and frd.payroll_dtl_id =
                                       ptr.payroll_dtl_id
                                   and ptr.payroll_dtl_id = pa.payroll_dtl_id
                                   and pe.pay_ele_class = 'Earning') -
                              (select sum(ROUND(ptr.pay_amount, 3))
                                  from pay_elements          pe,
                                       pay_final_run_dtl_dtl ptr,
                                       pay_final_run_dtl     frd
                                 where pe.pay_element_id =
                                       ptr.pay_emp_element_id
                                   and frd.payroll_dtl_id =
                                       ptr.payroll_dtl_id
                                   and ptr.payroll_dtl_id = pa.payroll_dtl_id
                                   and pe.pay_ele_class = 'Deduction')) as JE_ACCOUNTED_AMT_DR,
                              'GL_007_D' as JE_DTL_ID,
                              null as JE_PARTICULARS,
                              'Cr' as JE_CREDIT_DEBIT,
                              null as JE_AMOUNT_CR,
                              null as JE_ACCOUNTED_AMT_CR,
                              null as JE_SEGMENT_ID_1,
                              null as JE_SEGMENT_ID_2,
                              null as JE_SEGMENT_ID_3,
                              null as JE_SEGMENT_ID_4,
                              null as JE_SEGMENT_ID_5,
                              null as JE_SEGMENT_ID_6
                         from pay_advice pa, ca_bank_accounts ba
                        where pa.bank_id = ba.bank_id
                          AND PA.BANK_ID = P_BANK_ID
                          AND PA.PAY_PERIOD_ID = P_PAY_PERIOD_ID
                       union
                       SELECT PEL.Receivable_Id as acct_code_id,
                              null              as JE_AMOUNT_DR,
                              null              as JE_ACCOUNTED_AMT_DR,

                              'GL_007_D' as JE_DTL_ID,
                              null as JE_PARTICULARS,
                              'Dr' as JE_CREDIT_DEBIT,
                              ((select sum(ROUND(ptr.pay_amount, 3))
                                  from pay_elements          pe,
                                       pay_final_run_dtl_dtl ptr,
                                       pay_final_run_dtl     frd
                                 where pe.pay_element_id =
                                       ptr.pay_emp_element_id
                                   and frd.payroll_dtl_id =
                                       ptr.payroll_dtl_id
                                   and ptr.pay_emp_element_id =
                                       PE.Pay_Element_Id
                                   and pad.payroll_dtl_id =
                                       frd.payroll_dtl_id
                                   and pe.pay_ele_class = 'Earning') -
                              (select sum(ROUND(ptr.pay_amount, 3))
                                  from pay_elements          pe,
                                       pay_final_run_dtl_dtl ptr,
                                       pay_final_run_dtl     frd
                                 where pe.pay_element_id =
                                       ptr.pay_emp_element_id
                                   and frd.payroll_dtl_id =
                                       ptr.payroll_dtl_id
                                   and pad.payroll_dtl_id =
                                       frd.payroll_dtl_id
                                   and ptr.pay_emp_element_id =
                                       PE.Pay_Element_Id
                                   and pe.pay_ele_class = 'Deduction')) as JE_AMOUNT_CR,
                              ((select sum(ROUND(ptr.pay_amount, 3))
                                  from pay_elements          pe,
                                       pay_final_run_dtl_dtl ptr,
                                       pay_final_run_dtl     frd
                                 where pe.pay_element_id =
                                       ptr.pay_emp_element_id
                                   and frd.payroll_dtl_id =
                                       ptr.payroll_dtl_id
                                   and ptr.pay_emp_element_id =
                                       PE.Pay_Element_Id
                                   and pad.payroll_dtl_id =
                                       frd.payroll_dtl_id
                                   and pe.pay_ele_class = 'Earning') -
                              (select sum(ROUND(ptr.pay_amount, 3))
                                  from pay_elements          pe,
                                       pay_final_run_dtl_dtl ptr,
                                       pay_final_run_dtl     frd
                                 where pe.pay_element_id =
                                       ptr.pay_emp_element_id
                                   and frd.payroll_dtl_id =
                                       ptr.payroll_dtl_id
                                   and pad.payroll_dtl_id =
                                       frd.payroll_dtl_id
                                   and ptr.pay_emp_element_id =
                                       PE.Pay_Element_Id
                                   and pe.pay_ele_class = 'Deduction')) as JE_ACCOUNTED_AMT_CR,
                              null as JE_SEGMENT_ID_1,
                              null as JE_SEGMENT_ID_2,
                              null as JE_SEGMENT_ID_3,
                              null as JE_SEGMENT_ID_4,
                              null as JE_SEGMENT_ID_5,
                              null as JE_SEGMENT_ID_6
                         FROM PAY_ELEMENTS          PEL,
                              GL_ACCT_CODES         AC,
                              PAY_ADVICE            PAD,
                              pay_final_run_dtl     frd,
                              pay_final_run_dtl_dtl ptr
                        WHERE PEL.RECEIVABLE_ID = AC.ACCT_CODE_ID
                          AND PTR.PAYROLL_DTL_ID = FRD.PAYROLL_DTL_ID
                          AND PAD.PAYROLL_DTL_ID = FRD.PAYROLL_DTL_ID
                          AND PEL.PAY_ELEMENT_ID = PTR.PAY_EMP_ELEMENT_ID
                          AND PAD.BANK_ID = P_BANK_ID
                          AND PAD.PAY_PERIOD_ID = P_PAY_PERIOD_ID

                       ) LOOP

        -- FOR C_PR_JRD IN PR_JRD LOOP
        GL_INSERT_JR_DTL(P_JE_DTL_ID           => C_PR_JRD.JE_DTL_ID,
                         P_JE_HDR_ID           => v_JE_HDR_ID,
                         P_JE_ACCT_CODE_ID     => C_PR_JRD.ACCT_CODE_ID,
                         P_JE_PARTICULARS      => C_PR_JRD.JE_PARTICULARS,
                         P_JE_CREDIT_DEBIT     => C_PR_JRD.JE_CREDIT_DEBIT,
                         P_JE_AMOUNT_CR        => C_PR_JRD.JE_AMOUNT_CR,
                         P_JE_ACCOUNTED_AMT_CR => C_PR_JRD.JE_ACCOUNTED_AMT_CR,
                         P_JE_SEGMENT_ID_1     => C_PR_JRD.JE_SEGMENT_ID_1,
                         P_JE_SEGMENT_ID_2     => C_PR_JRD.JE_SEGMENT_ID_2,
                         P_JE_SEGMENT_ID_3     => C_PR_JRD.JE_SEGMENT_ID_3,
                         P_JE_SEGMENT_ID_4     => C_PR_JRD.JE_SEGMENT_ID_4,
                         P_JE_SEGMENT_ID_5     => C_PR_JRD.JE_SEGMENT_ID_5,
                         P_JE_SEGMENT_ID_6     => C_PR_JRD.JE_SEGMENT_ID_6,
                         P_JE_AMOUNT_DR        => C_PR_JRD.JE_AMOUNT_DR,
                         P_JE_ACCOUNTED_AMT_DR => C_PR_JRD.JE_ACCOUNTED_AMT_DR);

      END LOOP;
    END LOOP;
    COMMIT;

  EXCEPTION
    WHEN OTHERS THEN
      NULL;

  END PAYROLL_PAY_ADVICE_POSTING;

  procedure AP_GRN_POSTING(P_RECEIPT_ID VARCHAR2, P_ORG_ID VARCHAR2) is

    --GET THE ITEM,GRN DATA
    cursor C_GET_AP_DATA is
      SELECT 'GL_007_D' as JE_DTL_ID,

             R.RECEIPT_ID,
             R.ORG_ID,
             R.GRN_NUM,
             R.VENDOR_ID,
             D.INV_DESCRIPTION,
             D.ITEM_ID,
             D.QTY_APPROVED,
             (SELECT II.INV_MAT_ACCT_CODE
                FROM INV_ITEM_MASTER II
               WHERE II.ITEM_ID = D.ITEM_ID) AS MATRIAL_ACCOUNT,

             (SELECT NVL(I.ITEM_UNIT_PRICE, 0) * NVL(D.QTY_APPROVED, 0)
                FROM INV_ITEM_MASTER I
               WHERE I.ITEM_ID = D.ITEM_ID) AS MATRIAL_AMOUNT --DR
        FROM INV_RECEIPTS_HDR R, INV_RECEIPT_DTLS D
       WHERE R.RECEIPT_ID = D.RECEIPT_ID
         AND R.RECEIPT_ID = P_RECEIPT_ID
         AND R.ENABLED_FLAG = '1'
         AND R.WORKFLOW_COMPLETION_STATUS = '1';

    V_LIABILITY_AMOUNT      NUMBER;
    V_LIABILITY_ACCOUNT     VARCHAR(500);
    v_JE_HDR_ID             VARCHAR(500);
    v_JE_HDR_ID_VAL         VARCHAR(500);
    v_JN_NUMBER_ID          VARCHAR(500);
    v_JE_DTL_ID             VARCHAR(500);
    V_JOURNAL_TYPE          VARCHAR(500);
    V_JOURNAL_SOURCE        VARCHAR(500);
    V_JOURNAL_DATE          DATE;
    V_JOURNAL_DR_AMOUNT     NUMBER;
    V_JOURNAL_ORG_ID        VARCHAR(500);
    V_JOURNAL_GRN_NUM       VARCHAR(500);
    V_JOURNAL_GL_SEGMENT_ID VARCHAR(500);
    V_DESC                  VARCHAR(1500);
    V_COMP_CURRENCY         VARCHAR(500);
    V_PERIOD_ID             VARCHAR(500);
    V_EXCHANGE_RATE_VALUE   NUMBER;

    V_SUPPLIER_NAME VARCHAR(500);
    V_RECORD_DATE   DATE;
  BEGIN

    V_JOURNAL_TYPE   := 'STANDARD';
    V_JOURNAL_SOURCE := 'AP';
    V_JOURNAL_DATE   := TRUNC(SYSDATE);

    --GET THE GRN DATA JL HEADER
    BEGIN
      SELECT 'GL_007_M' as JE_HDR_ID,
             'GL_007_JN' as JN_NUMBER_ID,
             (SELECT sv.segment_value_id
                FROM GL_COMPANIES_HDR  CH,
                     GL_SEGMENTS       s,
                     gl_segment_values sv
               WHERE CH.COMP_ID = R.ORG_ID
                 and s.segment_id = sv.segment_id
                 and ch.comp_global_segment_id = s.segment_id
                 AND CH.COMP_ID = P_ORG_ID
                 AND ROWNUM = 1) AS COMP_GLOBAL_SEGMENT_ID,
             R.ORG_ID,
             R.GRN_NUM,
             (SELECT NVL(I.ITEM_UNIT_PRICE, 0) * NVL(D.QTY_APPROVED, 0)
                FROM INV_ITEM_MASTER I
               WHERE I.ITEM_ID = D.ITEM_ID
                 AND ROWNUM = 1) AS MATRIAL_AMOUNT, --DR
             (select cd.comp_base_currency
                from gl_companies_dtl cd
               where cd.comp_id = ch.comp_id
                 and cd.comp_id = P_ORG_ID
                 AND ROWNUM = 1) as org_currency,
             r.receipt_date
        into v_JE_HDR_ID_VAL,
             v_JN_NUMBER_ID,
             V_JOURNAL_GL_SEGMENT_ID,
             V_JOURNAL_ORG_ID,
             V_JOURNAL_GRN_NUM,
             V_JOURNAL_DR_AMOUNT,
             V_COMP_CURRENCY,
             V_RECORD_DATE
        FROM INV_RECEIPTS_HDR R, INV_RECEIPT_DTLS D, GL_COMPANIES_HDR ch
       WHERE R.RECEIPT_ID = D.RECEIPT_ID
         and ch.comp_id = r.org_id
         AND CH.ENABLED_FLAG = 1
         AND CH.WORKFLOW_COMPLETION_STATUS = 1
         AND R.ENABLED_FLAG = '1'
         AND R.WORKFLOW_COMPLETION_STATUS = '1'
         and R.ORG_ID = P_ORG_ID
         AND R.RECEIPT_ID = P_RECEIPT_ID
         AND R.ENABLED_FLAG = 1;
    Exception
      When others then
        dbms_output.put_line('GRN DATA JL HEADER ' || sqlerrm);
    End;

    --GET LIABILITY ACC CODE AND AMOUNT
    BEGIN
      SELECT 'GL_007_D' as JE_DTL_ID,
             SUM(NVL(I.ITEM_UNIT_PRICE, 0) * NVL(D.QTY_APPROVED, 0)) AS LIABILITY_AMOUNT,
             (select (case
                       when SC.AP_LIABILITY_ACCOUNT is null then
                        ss.ap_liability_acct
                       else
                        SC.AP_LIABILITY_ACCOUNT
                     end)
                from ssm_system_options ss
               where rownum = 1) AS LIABILITY_ACCOUNT,
             D.INV_DESCRIPTION,
             sc.vendor_name
        INTO v_JE_DTL_ID,
             V_LIABILITY_AMOUNT,
             V_LIABILITY_ACCOUNT,
             V_DESC,
             V_SUPPLIER_NAME
        FROM INV_RECEIPTS_HDR   R,
             SUPPLIER_CUSTOMERS SC,
             INV_RECEIPT_DTLS   D,
             INV_ITEM_MASTER    I
       WHERE SC.VENDOR_ID = R.VENDOR_ID
         AND I.ITEM_ID = D.ITEM_ID
         AND R.RECEIPT_ID = D.RECEIPT_ID
         AND R.ENABLED_FLAG = 1
         AND I.ENABLED_FLAG = 1
         AND R.WORKFLOW_COMPLETION_STATUS = '1'
         AND R.ORG_ID = P_ORG_ID
         AND R.RECEIPT_ID = P_RECEIPT_ID
       GROUP BY SC.AP_LIABILITY_ACCOUNT, D.INV_DESCRIPTION, sc.vendor_name;
    Exception
      When others then
        dbms_output.put_line('LIABILITY ACC CODE ' || sqlerrm);
    End;

    --GET CAL PERIOD
    BEGIN
      if V_RECORD_DATE is not null then
        select gg.period_id
          INTO V_PERIOD_ID
          from gl_comp_acct_period_dtl gg
         where gg.enabled_flag = '1'
           and gg.workflow_completion_status = '1'
           and gg.PERIOD_STATUS = 'OPEN'
           and gg.COMP_ID = V_JOURNAL_ORG_ID
           and (trunc(V_RECORD_DATE)) between gg.period_from_dt and
               gg.period_to_dt
         order by gg.PERIOD_NAME asc;
      end if;
    Exception
      When others then
        dbms_output.put_line('LIABILITY ACC CODE ' || sqlerrm);
    End;

    --GET  EXCHANGE_RATE_VALUE
    BEGIN
      SELECT SCER.CURRENCY_STD_RATE
        INTO V_EXCHANGE_RATE_VALUE
        FROM SSM_CURRENCY_EXCHANGE_RATES SCER
       WHERE SCER.WORKFLOW_COMPLETION_STATUS = 1
         AND SCER.ENABLED_FLAG = 1
         AND SCER.CURRENCY_ID = V_COMP_CURRENCY
         AND SCER.CURRENCY_RATE_DT = trunc(sysdate)
         AND SCER.CURRENCY_RATE_DT =
             (nvl((select sc.currency_rate_dt
                    FROM SSM_CURRENCY_EXCHANGE_RATES SC
                   where SC.CURRENCY_RATE_DT = trunc(sysdate)
                     and rownum = 1),
                  (select max(scr.CURRENCY_RATE_DT)
                     FROM SSM_CURRENCY_EXCHANGE_RATES SCr)));
    Exception
      When others then
        dbms_output.put_line('LIABILITY ACC CODE ' || sqlerrm);
    End;

    /*v_JE_HDR_ID    := 'GL_007_M';
      v_JN_NUMBER_ID := 'GL_007_JN';
    */
    v_JE_HDR_ID := ssm.get_next_sequence(v_JE_HDR_ID_VAL, 'EN');
    debug_proc(v_JE_HDR_ID);
    --INSERT THE JOURNAL HEADER RECORD FOR GRN
    BEGIN
      GL_INSERT(P_JE_HDR_ID              => v_JE_HDR_ID,
                p_JE_COMP_ID             => V_JOURNAL_ORG_ID,
                P_JE_DATE                => V_JOURNAL_DATE,
                P_JE_GLOBAL_SEGMENT_ID   => V_JOURNAL_GL_SEGMENT_ID,
                P_JE_NUMBER              => v_JN_NUMBER_ID,
                P_JE_TYPE                => V_JOURNAL_TYPE,
                P_JE_CURRENCY_ID         => V_COMP_CURRENCY,
                P_JE_DESC                => 'AP GRN for ' || V_SUPPLIER_NAME ||
                                            ', GRN Number:' ||
                                            V_JOURNAL_GRN_NUM ||
                                            ', GRN Amount:' ||
                                            V_JOURNAL_DR_AMOUNT,
                P_JE_CURRENCY_RATE_ID    => NULL,
                P_JE_STATUS              => 'OPEN',
                P_JE_CURR_VALUE          => NULL,
                P_JE_PERIOD_ID           => V_PERIOD_ID,
                P_JE_REFERENCE           => 'AP:GRN Number:' ||
                                            V_JOURNAL_GRN_NUM,
                P_JE_EXCHANGE_RATE_VALUE => V_EXCHANGE_RATE_VALUE,
                P_JE_TOT_DR_AMOUNT       => V_JOURNAL_DR_AMOUNT,
                P_JE_TOT_CR_AMOUNT       => V_JOURNAL_DR_AMOUNT,
                P_JE_RECURRING_END_DATE  => NULL,
                P_JE_RECURRING_FREQUENCY => NULL,
                P_JE_SOURCE              => V_JOURNAL_SOURCE,
                P_JOURNAL_REVERSED       => NULL,
                P_ACTUAL_JOURNAL_ID      => NULL,
                P_MODULE                 => 'AP');
    Exception
      When others then
        dbms_output.put_line('INSERT JOURNAL HEADER  ' || sqlerrm);
    End;
    debug_proc(v_JE_DTL_ID);
    --INSERT THE LIABILITY RECORD
    BEGIN
      /* v_JE_DTL_ID := 'GL_007_D';*/

      GL_INSERT_JR_DTL(P_JE_DTL_ID           => v_JE_DTL_ID,
                       P_JE_HDR_ID           => v_JE_HDR_ID,
                       P_JE_ACCT_CODE_ID     => V_LIABILITY_ACCOUNT,
                       P_JE_PARTICULARS      => V_DESC,
                       P_JE_CREDIT_DEBIT     => 'Cr',
                       P_JE_AMOUNT_CR        => V_LIABILITY_AMOUNT,
                       P_JE_ACCOUNTED_AMT_CR => V_LIABILITY_AMOUNT,
                       P_JE_SEGMENT_ID_1     => NULL,
                       P_JE_SEGMENT_ID_2     => NULL,
                       P_JE_SEGMENT_ID_3     => NULL,
                       P_JE_SEGMENT_ID_4     => NULL,
                       P_JE_SEGMENT_ID_5     => NULL,
                       P_JE_SEGMENT_ID_6     => NULL,
                       P_JE_AMOUNT_DR        => NULL,
                       P_JE_ACCOUNTED_AMT_DR => NULL);
    Exception
      When others then
        dbms_output.put_line('INSERT LIABILITY RECORD  ' || sqlerrm);
    End;
    debug_proc('1');
    ---INSERT THE MATERIAL ACCOUNT RECORDS
    BEGIN
      FOR F_GET_AP_DATA IN C_GET_AP_DATA LOOP

        /* v_JE_DTL_ID := 'GL_007_D';*/
        debug_proc('2');
        GL_INSERT_JR_DTL(P_JE_DTL_ID           => F_GET_AP_DATA.JE_DTL_ID,
                         P_JE_HDR_ID           => v_JE_HDR_ID,
                         P_JE_ACCT_CODE_ID     => F_GET_AP_DATA.MATRIAL_ACCOUNT,
                         P_JE_PARTICULARS      => F_GET_AP_DATA.INV_DESCRIPTION,
                         P_JE_CREDIT_DEBIT     => 'Dr',
                         P_JE_AMOUNT_CR        => NULL,
                         P_JE_ACCOUNTED_AMT_CR => NULL,
                         P_JE_SEGMENT_ID_1     => NULL,
                         P_JE_SEGMENT_ID_2     => NULL,
                         P_JE_SEGMENT_ID_3     => NULL,
                         P_JE_SEGMENT_ID_4     => NULL,
                         P_JE_SEGMENT_ID_5     => NULL,
                         P_JE_SEGMENT_ID_6     => NULL,
                         P_JE_AMOUNT_DR        => F_GET_AP_DATA.MATRIAL_AMOUNT,
                         P_JE_ACCOUNTED_AMT_DR => F_GET_AP_DATA.MATRIAL_AMOUNT);

        debug_proc('3');
      END LOOP;
    Exception
      When others then
        dbms_output.put_line('INSERT MATERIAL ACCOUNT  RECORD  ' ||
                             sqlerrm);
        debug_proc('4' || substr(sqlerrm, 1, 200));
    End;
    --  END LOOP;
    --  COMMIT;
    debug_proc('5');
  END AP_GRN_POSTING;

  ----AP Other Cost POSTING

  procedure AP_OTHER_COST_POSTING(P_RECEIPT_ID VARCHAR2, P_ORG_ID VARCHAR2) is

    --GET THE ITEM,GRN DATA
    cursor C_GET_AP_DATA is
      SELECT 'GL_007_D' as JE_DTL_ID,
             R.RECEIPT_ID,
             R.ORG_ID,
             R.GRN_NUM,
             R.VENDOR_ID,
             D.INV_DESCRIPTION,
             D.ITEM_ID,
             D.QTY_APPROVED,
             (SELECT II.INV_MAT_ACCT_CODE
                FROM INV_ITEM_MASTER II
               WHERE II.ITEM_ID = D.ITEM_ID) AS MATRIAL_ACCOUNT,

             (SELECT NVL(I.ITEM_UNIT_PRICE, 0) * NVL(D.QTY_APPROVED, 0)
                FROM INV_ITEM_MASTER I
               WHERE I.ITEM_ID = D.ITEM_ID) AS MATRIAL_AMOUNT --DR
        FROM INV_RECEIPTS_HDR R, INV_RECEIPT_DTLS D, inv_item_costing ic
       WHERE R.RECEIPT_ID = D.RECEIPT_ID
         and ic.inv_receipt_hdr_id = r.receipt_id
         and ic.inv_receipt_line_id = d.receipt_dtl_id
         AND ic.INV_ITEM_COST_ID = P_RECEIPT_ID
            --         AND R.RECEIPT_ID = P_RECEIPT_ID
         AND R.ENABLED_FLAG = '1'
         AND R.WORKFLOW_COMPLETION_STATUS = '1';

    V_LIABILITY_AMOUNT      NUMBER;
    V_LIABILITY_ACCOUNT     VARCHAR(500);
    v_JE_HDR_ID             VARCHAR(500);
    v_JE_HDR_ID_VAL         VARCHAR(500);
    v_JN_NUMBER_ID          VARCHAR(500);
    v_JE_DTL_ID             VARCHAR(500);
    V_JOURNAL_TYPE          VARCHAR(500);
    V_JOURNAL_SOURCE        VARCHAR(500);
    V_JOURNAL_DATE          DATE;
    V_JOURNAL_DR_AMOUNT     NUMBER;
    V_JOURNAL_ORG_ID        VARCHAR(500);
    V_JOURNAL_GRN_NUM       VARCHAR(500);
    V_JOURNAL_GL_SEGMENT_ID VARCHAR(500);
    V_DESC                  VARCHAR(1500);
    V_COMP_CURRENCY         VARCHAR(500);
    V_PERIOD_ID             VARCHAR(500);
    V_EXCHANGE_RATE_VALUE   NUMBER;

    V_SUPPLIER_NAME VARCHAR(500);
    V_RECORD_DATE   DATE;

  BEGIN

    V_JOURNAL_TYPE   := 'STANDARD';
    V_JOURNAL_SOURCE := 'AP';
    V_JOURNAL_DATE   := TRUNC(SYSDATE);

    --GET THE GRN DATA JL HEADER
    BEGIN
      SELECT 'GL_007_M' as JE_HDR_ID,
             'GL_007_JN' as JN_NUMBER_ID,
             (SELECT sv.segment_value_id
                FROM GL_COMPANIES_HDR  CH,
                     GL_SEGMENTS       s,
                     gl_segment_values sv
               WHERE CH.COMP_ID = R.ORG_ID
                 and s.segment_id = sv.segment_id
                 and ch.comp_global_segment_id = s.segment_id
                 AND CH.COMP_ID = P_ORG_ID
                 AND ROWNUM = 1) AS COMP_GLOBAL_SEGMENT_ID,
             R.ORG_ID,
             R.GRN_NUM,
             (SELECT NVL(I.ITEM_UNIT_PRICE, 0) * NVL(D.QTY_APPROVED, 0)
                FROM INV_ITEM_MASTER I
               WHERE I.ITEM_ID = D.ITEM_ID
                 AND ROWNUM = 1) AS MATRIAL_AMOUNT, --DR
             (select cd.comp_base_currency
                from gl_companies_dtl cd
               where cd.comp_id = ch.comp_id
                 and cd.comp_id = P_ORG_ID
                 AND ROWNUM = 1) as org_currency,
             r.receipt_date
        into v_JE_HDR_ID_VAL,
             v_JN_NUMBER_ID,
             V_JOURNAL_GL_SEGMENT_ID,
             V_JOURNAL_ORG_ID,
             V_JOURNAL_GRN_NUM,
             V_JOURNAL_DR_AMOUNT,
             V_COMP_CURRENCY,
             V_RECORD_DATE
        FROM INV_RECEIPTS_HDR R,
             INV_RECEIPT_DTLS D,
             GL_COMPANIES_HDR ch,
             INV_ITEM_COSTING IC
       WHERE R.RECEIPT_ID = D.RECEIPT_ID
         AND IC.INV_RECEIPT_HDR_ID = R.RECEIPT_ID
         AND IC.INV_RECEIPT_LINE_ID = D.RECEIPT_DTL_ID
         and ch.comp_id = r.org_id
         AND CH.ENABLED_FLAG = 1
         AND CH.WORKFLOW_COMPLETION_STATUS = 1
         AND R.ENABLED_FLAG = '1'
         AND R.WORKFLOW_COMPLETION_STATUS = '1'
         and R.ORG_ID = P_ORG_ID

         AND ic.INV_ITEM_COST_ID = P_RECEIPT_ID
            --         AND R.RECEIPT_ID = P_RECEIPT_ID
         AND R.ENABLED_FLAG = 1;
    Exception
      When others then
        dbms_output.put_line('GRN DATA JL HEADER ' || sqlerrm);
    End;

    --GET MATERIAL OVERHEAD ACC CODE AND AMOUNT
    BEGIN
      SELECT 'GL_007_D' as JE_DTL_ID,
             SUM(NVL(I.ITEM_UNIT_PRICE, 0) * NVL(D.QTY_APPROVED, 0)) AS LIABILITY_AMOUNT,
             I.INV_MAT_OVERHEAD_ACCT_CODE AS MATIRIAL_OVERHEAD_ACCT_CODE,
             D.INV_DESCRIPTION
        INTO v_JE_DTL_ID, V_LIABILITY_AMOUNT, V_LIABILITY_ACCOUNT, V_DESC
        FROM INV_RECEIPTS_HDR R,
             INV_ITEM_COSTING IC,
             INV_RECEIPT_DTLS D,
             INV_ITEM_MASTER  I
       WHERE I.ITEM_ID = D.ITEM_ID
         AND IC.INV_RECEIPT_HDR_ID = R.RECEIPT_ID
         AND IC.INV_RECEIPT_LINE_ID = D.RECEIPT_DTL_ID
         AND R.RECEIPT_ID = D.RECEIPT_ID
         AND R.ENABLED_FLAG = 1
         AND I.ENABLED_FLAG = 1
         AND R.WORKFLOW_COMPLETION_STATUS = '1'
         AND R.ORG_ID = P_ORG_ID
            --         AND R.RECEIPT_ID = P_RECEIPT_ID
         AND ic.INV_ITEM_COST_ID = P_RECEIPT_ID
       GROUP BY I.INV_MAT_OVERHEAD_ACCT_CODE, D.INV_DESCRIPTION;
    Exception
      When others then
        dbms_output.put_line('LIABILITY ACC CODE ' || sqlerrm);
    End;

    --GET CAL PERIOD
    BEGIN
      select gg.period_id
        INTO V_PERIOD_ID
        from gl_comp_acct_period_dtl gg
       where gg.enabled_flag = '1'
         and gg.workflow_completion_status = '1'
         and gg.PERIOD_STATUS = 'OPEN'
         and gg.COMP_ID = V_JOURNAL_ORG_ID
         and (trunc(sysdate)) between gg.period_from_dt and gg.period_to_dt
       order by gg.PERIOD_NAME asc;
    Exception
      When others then
        dbms_output.put_line('LIABILITY ACC CODE ' || sqlerrm);
    End;

    --GET  EXCHANGE_RATE_VALUE
    BEGIN
      SELECT SCER.CURRENCY_STD_RATE
        INTO V_EXCHANGE_RATE_VALUE
        FROM SSM_CURRENCY_EXCHANGE_RATES SCER
       WHERE SCER.WORKFLOW_COMPLETION_STATUS = 1
         AND SCER.ENABLED_FLAG = 1
         AND SCER.CURRENCY_ID = V_COMP_CURRENCY
         AND SCER.CURRENCY_RATE_DT = trunc(sysdate)
         AND SCER.CURRENCY_RATE_DT =
             (nvl((select sc.currency_rate_dt
                    FROM SSM_CURRENCY_EXCHANGE_RATES SC
                   where SC.CURRENCY_RATE_DT = trunc(sysdate)
                     and rownum = 1),
                  (select max(scr.CURRENCY_RATE_DT)
                     FROM SSM_CURRENCY_EXCHANGE_RATES SCr)));
    Exception
      When others then
        dbms_output.put_line('LIABILITY ACC CODE ' || sqlerrm);
    End;

    /*v_JE_HDR_ID    := 'GL_007_M';
      v_JN_NUMBER_ID := 'GL_007_JN';
    */
    v_JE_HDR_ID := ssm.get_next_sequence(v_JE_HDR_ID_VAL, 'EN');
    debug_proc(v_JE_HDR_ID);
    --INSERT THE JOURNAL HEADER RECORD FOR GRN
    BEGIN
      GL_INSERT(P_JE_HDR_ID              => v_JE_HDR_ID,
                p_JE_COMP_ID             => V_JOURNAL_ORG_ID,
                P_JE_DATE                => V_JOURNAL_DATE,
                P_JE_GLOBAL_SEGMENT_ID   => V_JOURNAL_GL_SEGMENT_ID,
                P_JE_NUMBER              => v_JN_NUMBER_ID,
                P_JE_TYPE                => V_JOURNAL_TYPE,
                P_JE_CURRENCY_ID         => V_COMP_CURRENCY,
                P_JE_DESC                => 'AP ITEM OTHER COST',
                P_JE_CURRENCY_RATE_ID    => NULL,
                P_JE_STATUS              => 'OPEN',
                P_JE_CURR_VALUE          => NULL,
                P_JE_PERIOD_ID           => V_PERIOD_ID,
                P_JE_REFERENCE           => 'AP:GRN Number:' ||
                                            V_JOURNAL_GRN_NUM,
                P_JE_EXCHANGE_RATE_VALUE => V_EXCHANGE_RATE_VALUE,
                P_JE_TOT_DR_AMOUNT       => V_JOURNAL_DR_AMOUNT,
                P_JE_TOT_CR_AMOUNT       => V_JOURNAL_DR_AMOUNT,
                P_JE_RECURRING_END_DATE  => NULL,
                P_JE_RECURRING_FREQUENCY => NULL,
                P_JE_SOURCE              => V_JOURNAL_SOURCE,
                P_JOURNAL_REVERSED       => NULL,
                P_ACTUAL_JOURNAL_ID      => NULL,
                P_MODULE                 => 'AP');
    Exception
      When others then
        dbms_output.put_line('INSERT JOURNAL HEADER  ' || sqlerrm);
        debug_proc('11' || substr(sqlerrm, 1, 200));
    End;
    debug_proc('13');
    --INSERT THE MATERIAL OVERHEAD RECORD
    BEGIN
      /* v_JE_DTL_ID := 'GL_007_D';*/
      debug_proc(v_JE_DTL_ID);
      GL_INSERT_JR_DTL(P_JE_DTL_ID           => v_JE_DTL_ID,
                       P_JE_HDR_ID           => v_JE_HDR_ID,
                       P_JE_ACCT_CODE_ID     => V_LIABILITY_ACCOUNT,
                       P_JE_PARTICULARS      => V_DESC,
                       P_JE_CREDIT_DEBIT     => 'Cr',
                       P_JE_AMOUNT_CR        => V_LIABILITY_AMOUNT,
                       P_JE_ACCOUNTED_AMT_CR => V_LIABILITY_AMOUNT,
                       P_JE_SEGMENT_ID_1     => NULL,
                       P_JE_SEGMENT_ID_2     => NULL,
                       P_JE_SEGMENT_ID_3     => NULL,
                       P_JE_SEGMENT_ID_4     => NULL,
                       P_JE_SEGMENT_ID_5     => NULL,
                       P_JE_SEGMENT_ID_6     => NULL,
                       P_JE_AMOUNT_DR        => NULL,
                       P_JE_ACCOUNTED_AMT_DR => NULL);
    Exception
      When others then
        dbms_output.put_line('INSERT LIABILITY RECORD  ' || sqlerrm);
        debug_proc('13' || substr(sqlerrm, 1, 200));
    End;

    ---INSERT THE MATERIAL ACCOUNT RECORDS
    BEGIN
      FOR F_GET_AP_DATA IN C_GET_AP_DATA LOOP

        /* v_JE_DTL_ID := 'GL_007_D';*/

        GL_INSERT_JR_DTL(P_JE_DTL_ID           => F_GET_AP_DATA.JE_DTL_ID,
                         P_JE_HDR_ID           => v_JE_HDR_ID,
                         P_JE_ACCT_CODE_ID     => F_GET_AP_DATA.MATRIAL_ACCOUNT,
                         P_JE_PARTICULARS      => F_GET_AP_DATA.INV_DESCRIPTION,
                         P_JE_CREDIT_DEBIT     => 'Dr',
                         P_JE_AMOUNT_CR        => NULL,
                         P_JE_ACCOUNTED_AMT_CR => NULL,
                         P_JE_SEGMENT_ID_1     => NULL,
                         P_JE_SEGMENT_ID_2     => NULL,
                         P_JE_SEGMENT_ID_3     => NULL,
                         P_JE_SEGMENT_ID_4     => NULL,
                         P_JE_SEGMENT_ID_5     => NULL,
                         P_JE_SEGMENT_ID_6     => NULL,
                         P_JE_AMOUNT_DR        => F_GET_AP_DATA.MATRIAL_AMOUNT,
                         P_JE_ACCOUNTED_AMT_DR => F_GET_AP_DATA.MATRIAL_AMOUNT);
      END LOOP;
    Exception
      When others then
        dbms_output.put_line('INSERT MATERIAL ACCOUNT  RECORD  ' ||
                             sqlerrm);
        debug_proc('14' || substr(sqlerrm, 1, 200));
    End;
    debug_proc('15');
    --  END LOOP;
    --- COMMIT;

  END AP_OTHER_COST_POSTING;

  ----AP MICELLANEOUS POSTING
  procedure AP_MICELLANEOUS_POSTING(P_RECEIPT_ID VARCHAR2,
                                    P_ORG_ID     VARCHAR2) is

    --GET THE ITEM,GRN DATA
    cursor C_GET_AP_DATA is
      SELECT 'GL_007_D' as JE_DTL_ID,
             (SELECT sv.segment_value_id
                FROM GL_COMPANIES_HDR  CH,
                     GL_SEGMENTS       s,
                     gl_segment_values sv
               WHERE CH.COMP_ID = R.ORG_ID
                 and s.segment_id = sv.segment_id
                 and ch.comp_global_segment_id = s.segment_id
                 AND CH.COMP_ID = P_ORG_ID
                 AND ROWNUM = 1) AS COMP_GLOBAL_SEGMENT_ID,
             R.RECEIPT_ID,
             R.ORG_ID,
             R.GRN_NUM,
             R.VENDOR_ID,
             D.INV_DESCRIPTION,
             D.ITEM_ID,
             D.QTY_APPROVED,
             (SELECT II.INV_MAT_ACCT_CODE
                FROM INV_ITEM_MASTER II
               WHERE II.ITEM_ID = D.ITEM_ID) AS MATRIAL_ACCOUNT,

             (SELECT NVL(I.ITEM_UNIT_PRICE, 0) * NVL(D.QTY_APPROVED, 0)
                FROM INV_ITEM_MASTER I
               WHERE I.ITEM_ID = D.ITEM_ID) AS MATRIAL_AMOUNT --DR
        FROM INV_RECEIPTS_HDR R, INV_RECEIPT_DTLS D
       WHERE R.RECEIPT_ID = D.RECEIPT_ID
         AND R.RECEIPT_ID = P_RECEIPT_ID
         AND R.ENABLED_FLAG = '1'
         AND R.WORKFLOW_COMPLETION_STATUS = '1';

    V_LIABILITY_AMOUNT      NUMBER;
    V_LIABILITY_ACCOUNT     VARCHAR(500);
    v_JE_HDR_ID             VARCHAR(500);
    v_JE_HDR_ID_VAL         VARCHAR(500);
    v_JN_NUMBER_ID          VARCHAR(500);
    v_JE_DTL_ID             VARCHAR(500);
    V_JOURNAL_TYPE          VARCHAR(500);
    V_JOURNAL_SOURCE        VARCHAR(500);
    V_JOURNAL_DATE          DATE;
    V_JOURNAL_DR_AMOUNT     NUMBER;
    V_JOURNAL_ORG_ID        VARCHAR(500);
    V_JOURNAL_GRN_NUM       VARCHAR(500);
    V_JOURNAL_GL_SEGMENT_ID VARCHAR(500);
    V_DESC                  VARCHAR(1500);
    V_COMP_CURRENCY         VARCHAR(500);
    V_PERIOD_ID             VARCHAR(500);
    V_EXCHANGE_RATE_VALUE   NUMBER;
    V_SUPPLIER_NAME         VARCHAR(500);
    V_RECORD_DATE           DATE;
  BEGIN

    V_JOURNAL_TYPE   := 'STANDARD';
    V_JOURNAL_SOURCE := 'AP';
    V_JOURNAL_DATE   := TRUNC(SYSDATE);

    --GET THE GRN DATA JL HEADER
    BEGIN
      SELECT 'GL_007_M' as JE_HDR_ID,
             'GL_007_JN' as JN_NUMBER_ID,
             (SELECT sv.segment_value_id
                FROM GL_COMPANIES_HDR  CH,
                     GL_SEGMENTS       s,
                     gl_segment_values sv
               WHERE CH.COMP_ID = R.ORG_ID
                 and s.segment_id = sv.segment_id
                 and ch.comp_global_segment_id = s.segment_id
                 AND CH.COMP_ID = P_ORG_ID
                 AND ROWNUM = 1) AS COMP_GLOBAL_SEGMENT_ID,
             R.ORG_ID,
             R.GRN_NUM,
             (SELECT NVL(I.ITEM_UNIT_PRICE, 0) * NVL(D.QTY_APPROVED, 0)
                FROM INV_ITEM_MASTER I
               WHERE I.ITEM_ID = D.ITEM_ID
                 AND ROWNUM = 1) AS MATRIAL_AMOUNT, --DR
             (select cd.comp_base_currency
                from gl_companies_dtl cd
               where cd.comp_id = ch.comp_id
                 and cd.comp_id = P_ORG_ID
                 AND ROWNUM = 1) as org_currency,
             r.receipt_date
        into v_JE_HDR_ID_VAL,
             v_JN_NUMBER_ID,
             V_JOURNAL_GL_SEGMENT_ID,
             V_JOURNAL_ORG_ID,
             V_JOURNAL_GRN_NUM,
             V_JOURNAL_DR_AMOUNT,
             V_COMP_CURRENCY,
             V_RECORD_DATE
        FROM INV_RECEIPTS_HDR R, INV_RECEIPT_DTLS D, GL_COMPANIES_HDR ch
       WHERE R.RECEIPT_ID = D.RECEIPT_ID
         and ch.comp_id = r.org_id
         AND CH.ENABLED_FLAG = 1
         AND CH.WORKFLOW_COMPLETION_STATUS = 1
         AND R.ENABLED_FLAG = '1'
         AND R.WORKFLOW_COMPLETION_STATUS = '1'
         and R.ORG_ID = P_ORG_ID
         AND R.RECEIPT_ID = P_RECEIPT_ID
         AND R.ENABLED_FLAG = 1;
    Exception
      When others then
        dbms_output.put_line('GRN DATA JL HEADER ' || sqlerrm);
    End;

    --GET LIABILITY ACC CODE AND AMOUNT
    BEGIN
      SELECT 'GL_007_D' as JE_DTL_ID,
             SUM(NVL(I.ITEM_UNIT_PRICE, 0) * NVL(D.QTY_APPROVED, 0)) AS LIABILITY_AMOUNT,
             (select (case
                       when SC.AP_LIABILITY_ACCOUNT is null then
                        ss.ap_liability_acct
                       else
                        SC.AP_LIABILITY_ACCOUNT
                     end)
                from ssm_system_options ss
               where rownum = 1) AS LIABILITY_ACCOUNT,
             D.INV_DESCRIPTION,
             sc.vendor_name
        INTO v_JE_DTL_ID,
             V_LIABILITY_AMOUNT,
             V_LIABILITY_ACCOUNT,
             V_DESC,
             V_SUPPLIER_NAME
        FROM INV_RECEIPTS_HDR   R,
             SUPPLIER_CUSTOMERS SC,
             INV_RECEIPT_DTLS   D,
             INV_ITEM_MASTER    I
       WHERE SC.VENDOR_ID = R.VENDOR_ID
         AND I.ITEM_ID = D.ITEM_ID
         AND R.RECEIPT_ID = D.RECEIPT_ID
         AND R.ENABLED_FLAG = 1
         AND I.ENABLED_FLAG = 1
         AND R.WORKFLOW_COMPLETION_STATUS = '1'
         AND R.ORG_ID = P_ORG_ID
         AND R.RECEIPT_ID = P_RECEIPT_ID
       GROUP BY SC.AP_LIABILITY_ACCOUNT, D.INV_DESCRIPTION, sc.vendor_name;
    Exception
      When others then
        dbms_output.put_line('LIABILITY ACC CODE ' || sqlerrm);
    End;

    --GET CAL PERIOD
    BEGIN
      if V_RECORD_DATE is not null then
        select gg.period_id
          INTO V_PERIOD_ID
          from gl_comp_acct_period_dtl gg
         where gg.enabled_flag = '1'
           and gg.workflow_completion_status = '1'
           and gg.PERIOD_STATUS = 'OPEN'
           and gg.COMP_ID = V_JOURNAL_ORG_ID
           and (trunc(V_RECORD_DATE)) between gg.period_from_dt and
               gg.period_to_dt
         order by gg.PERIOD_NAME asc;
      end if;
    Exception
      When others then
        dbms_output.put_line('LIABILITY ACC CODE ' || sqlerrm);
    End;

    --GET  EXCHANGE_RATE_VALUE
    BEGIN
      SELECT SCER.CURRENCY_STD_RATE
        INTO V_EXCHANGE_RATE_VALUE
        FROM SSM_CURRENCY_EXCHANGE_RATES SCER
       WHERE SCER.WORKFLOW_COMPLETION_STATUS = 1
         AND SCER.ENABLED_FLAG = 1
         AND SCER.CURRENCY_ID = V_COMP_CURRENCY
         AND SCER.CURRENCY_RATE_DT = trunc(sysdate)
         AND SCER.CURRENCY_RATE_DT =
             (nvl((select sc.currency_rate_dt
                    FROM SSM_CURRENCY_EXCHANGE_RATES SC
                   where SC.CURRENCY_RATE_DT = trunc(sysdate)
                     and rownum = 1),
                  (select max(scr.CURRENCY_RATE_DT)
                     FROM SSM_CURRENCY_EXCHANGE_RATES SCr)));
    Exception
      When others then
        dbms_output.put_line('LIABILITY ACC CODE ' || sqlerrm);
    End;

    /*v_JE_HDR_ID    := 'GL_007_M';
      v_JN_NUMBER_ID := 'GL_007_JN';
    */
    v_JE_HDR_ID := ssm.get_next_sequence(v_JE_HDR_ID_VAL, 'EN');

    --INSERT THE JOURNAL HEADER RECORD FOR GRN
    BEGIN
      GL_INSERT(P_JE_HDR_ID              => v_JE_HDR_ID,
                p_JE_COMP_ID             => V_JOURNAL_ORG_ID,
                P_JE_DATE                => V_JOURNAL_DATE,
                P_JE_GLOBAL_SEGMENT_ID   => V_JOURNAL_GL_SEGMENT_ID,
                P_JE_NUMBER              => v_JN_NUMBER_ID,
                P_JE_TYPE                => V_JOURNAL_TYPE,
                P_JE_CURRENCY_ID         => V_COMP_CURRENCY,
                P_JE_DESC                => 'AP MICELLANEOUS for ' ||
                                            V_SUPPLIER_NAME ||
                                            ', GRN Number:' ||
                                            V_JOURNAL_GRN_NUM ||
                                            ', GRN Amount:' ||
                                            V_JOURNAL_DR_AMOUNT,
                P_JE_CURRENCY_RATE_ID    => NULL,
                P_JE_STATUS              => 'OPEN',
                P_JE_CURR_VALUE          => NULL,
                P_JE_PERIOD_ID           => V_PERIOD_ID,
                P_JE_REFERENCE           => 'AP:GRN Number:' ||
                                            V_JOURNAL_GRN_NUM,
                P_JE_EXCHANGE_RATE_VALUE => V_EXCHANGE_RATE_VALUE,
                P_JE_TOT_DR_AMOUNT       => V_JOURNAL_DR_AMOUNT,
                P_JE_TOT_CR_AMOUNT       => V_JOURNAL_DR_AMOUNT,
                P_JE_RECURRING_END_DATE  => NULL,
                P_JE_RECURRING_FREQUENCY => NULL,
                P_JE_SOURCE              => V_JOURNAL_SOURCE,
                P_JOURNAL_REVERSED       => NULL,
                P_ACTUAL_JOURNAL_ID      => NULL,
                P_MODULE                 => 'AP');
    Exception
      When others then
        dbms_output.put_line('INSERT JOURNAL HEADER  ' || sqlerrm);
    End;

    --INSERT THE LIABILITY RECORD
    BEGIN
      /* v_JE_DTL_ID := 'GL_007_D';*/

      GL_INSERT_JR_DTL(P_JE_DTL_ID           => v_JE_DTL_ID,
                       P_JE_HDR_ID           => v_JE_HDR_ID,
                       P_JE_ACCT_CODE_ID     => V_LIABILITY_ACCOUNT,
                       P_JE_PARTICULARS      => V_DESC,
                       P_JE_CREDIT_DEBIT     => 'Cr',
                       P_JE_AMOUNT_CR        => V_LIABILITY_AMOUNT,
                       P_JE_ACCOUNTED_AMT_CR => V_LIABILITY_AMOUNT,
                       P_JE_SEGMENT_ID_1     => NULL,
                       P_JE_SEGMENT_ID_2     => NULL,
                       P_JE_SEGMENT_ID_3     => NULL,
                       P_JE_SEGMENT_ID_4     => NULL,
                       P_JE_SEGMENT_ID_5     => NULL,
                       P_JE_SEGMENT_ID_6     => NULL,
                       P_JE_AMOUNT_DR        => NULL,
                       P_JE_ACCOUNTED_AMT_DR => NULL);
    Exception
      When others then
        dbms_output.put_line('INSERT LIABILITY RECORD  ' || sqlerrm);
    End;

    ---INSERT THE MATERIAL ACCOUNT RECORDS
    BEGIN
      FOR F_GET_AP_DATA IN C_GET_AP_DATA LOOP

        /* v_JE_DTL_ID := 'GL_007_D';*/

        GL_INSERT_JR_DTL(P_JE_DTL_ID           => F_GET_AP_DATA.JE_DTL_ID,
                         P_JE_HDR_ID           => v_JE_HDR_ID,
                         P_JE_ACCT_CODE_ID     => F_GET_AP_DATA.MATRIAL_ACCOUNT,
                         P_JE_PARTICULARS      => F_GET_AP_DATA.INV_DESCRIPTION,
                         P_JE_CREDIT_DEBIT     => 'Dr',
                         P_JE_AMOUNT_CR        => NULL,
                         P_JE_ACCOUNTED_AMT_CR => NULL,
                         P_JE_SEGMENT_ID_1     => NULL,
                         P_JE_SEGMENT_ID_2     => NULL,
                         P_JE_SEGMENT_ID_3     => NULL,
                         P_JE_SEGMENT_ID_4     => NULL,
                         P_JE_SEGMENT_ID_5     => NULL,
                         P_JE_SEGMENT_ID_6     => NULL,
                         P_JE_AMOUNT_DR        => F_GET_AP_DATA.MATRIAL_AMOUNT,
                         P_JE_ACCOUNTED_AMT_DR => F_GET_AP_DATA.MATRIAL_AMOUNT);
      END LOOP;
    Exception
      When others then
        dbms_output.put_line('INSERT MATERIAL ACCOUNT  RECORD  ' ||
                             sqlerrm);
    End;
    --  END LOOP;
    --  COMMIT;

  END AP_MICELLANEOUS_POSTING;

  ----AP INVOICE POSTING
  procedure AP_INVOICE_POSTING(P_INV_ID VARCHAR2, P_ORG_ID VARCHAR2) is

    --GET THE ITEM,INVOICE DATA
    cursor C_GET_AP_DATA is
      SELECT 'GL_007_D' as JE_DTL_ID,
             d.attribute1 AS INV_DESCRIPTION,
             d.inv_item_acct_code_id AS MATRIAL_ACCOUNT,
             d.inv_item_amt AS MATRIAL_AMOUNT,
             nvl(r.inv_retention_amt, 0) as retention_amt,
             (select sc.vendor_retention_acct_id
                from supplier_customers sc
               where sc.vendor_id = r.vendor_id
                 and sc.workflow_completion_status = 1
                 and rownum = 1) as retention_acct_id
        FROM inv_invoices_hdr R, INV_INVOICES_DTLS D
       WHERE R.INV_ID = D.INV_ID
         AND R.INV_ID = P_INV_ID
         and r.inv_org_id = P_ORG_ID
         AND R.ENABLED_FLAG = '1'
         AND R.WORKFLOW_COMPLETION_STATUS = '1';

    -------------------------------------------------------------------------------------------------------------------
    --VARIABLE DECLARATION
    V_LIABILITY_AMOUNT        NUMBER;
    V_LIABILITY_ACCOUNT       VARCHAR(500);
    V_RENTENTION_ACCOUNT      VARCHAR(500);
    v_JE_HDR_ID               VARCHAR(500);
    v_JE_HDR_ID_VAL           VARCHAR(500);
    v_JN_NUMBER_ID            VARCHAR(500);
    v_JE_DTL_ID               VARCHAR(500);
    V_JOURNAL_TYPE            VARCHAR(500);
    V_JOURNAL_SOURCE          VARCHAR(500);
    V_JOURNAL_DATE            DATE;
    V_JOURNAL_DR_AMOUNT       NUMBER;
    V_JOURNAL_ORG_ID          VARCHAR(500);
    V_JOURNAL_GRN_NUM         VARCHAR(500);
    V_JOURNAL_GL_SEGMENT_ID   VARCHAR(500);
    V_DESC                    VARCHAR(1500);
    V_COMP_CURRENCY           VARCHAR(500);
    V_PERIOD_ID               VARCHAR(500);
    V_EXCHANGE_RATE_VALUE     NUMBER;
    V_CREDIT_FLAG             VARCHAR(500);
    V_DEBIT_FLAG              VARCHAR(500);
    V_JOURNAL_RENTENON_AMOUNT NUMBER;
    V_SUPPLIER_NUMBER         VARCHAR(500);
    V_RECORD_DATE             DATE;
  BEGIN

    V_JOURNAL_SOURCE := 'AP';
    V_JOURNAL_DATE   := TRUNC(SYSDATE);

    --GET THE INV DATA JL HEADER
    BEGIN

      SELECT distinct 'GL_007_M' as JE_HDR_ID,
                      'GL_007_JN' as JN_NUMBER_ID,
                      r.GLOBAL_SEGMENT_ID AS COMP_GLOBAL_SEGMENT_ID,
                      R.inv_org_id AS ORG_ID,
                      r.inv_num,
                      r.inv_amt,
                      r.inv_curr_code as org_currency,
                      r.INV_EXCHANGE_RATE,
                      upper(r.inv_type) as inv_type,
                      nvl(r.inv_retention_amt, 0) as inv_retention_amt,
                      R.INV_DATE,
                      (select sc.vendor_retention_acct_id
                         from supplier_customers sc
                        where sc.vendor_id = r.vendor_id
                          and sc.workflow_completion_status = 1
                          and rownum = 1) as retention_acct_id
        into v_JE_HDR_ID_VAL,
             v_JN_NUMBER_ID,
             V_JOURNAL_GL_SEGMENT_ID,
             V_JOURNAL_ORG_ID,
             V_JOURNAL_GRN_NUM,
             V_JOURNAL_DR_AMOUNT,
             V_COMP_CURRENCY,
             V_EXCHANGE_RATE_VALUE,
             V_JOURNAL_TYPE,
             V_JOURNAL_RENTENON_AMOUNT,
             V_RECORD_DATE,
             V_RENTENTION_ACCOUNT
        FROM INV_INVOICES_HDR R
       WHERE R.ENABLED_FLAG = '1'
         AND R.WORKFLOW_COMPLETION_STATUS = '1'
         and R.Inv_Org_Id = P_ORG_ID
         AND R.Inv_Id = P_INV_ID
         AND R.ENABLED_FLAG = 1;
    Exception
      When others then
        dbms_output.put_line('INV DATA JL HEADER ' || sqlerrm);
    End;

    --GET LIABILITY ACC CODE AND AMOUNT
    BEGIN
      SELECT 'GL_007_D' as JE_DTL_ID,
             r.inv_amt AS LIABILITY_AMOUNT,
             (select (case
                       when SC.AP_LIABILITY_ACCOUNT is null then
                        ss.ap_liability_acct
                       else
                        SC.AP_LIABILITY_ACCOUNT
                     end)
                from ssm_system_options ss
               where rownum = 1) AS LIABILITY_ACCOUNT,
             r.inv_remarks,
             SC.VENDOR_NAME
        INTO v_JE_DTL_ID,
             V_LIABILITY_AMOUNT,
             V_LIABILITY_ACCOUNT,
             V_DESC,
             V_SUPPLIER_NUMBER
        FROM INV_INVOICES_HDR R, SUPPLIER_CUSTOMERS SC
       WHERE SC.VENDOR_ID = R.VENDOR_ID
         AND R.ENABLED_FLAG = 1
         and sc.enabled_flag = '1'
         and sc.workflow_completion_status = '1'
         AND R.WORKFLOW_COMPLETION_STATUS = '1'
         AND R.Inv_Org_Id = P_ORG_ID
         AND R.inv_id = P_INV_ID;
    Exception
      When others then
        dbms_output.put_line('LIABILITY ACC CODE ' || sqlerrm);
    End;

    --GET CAL PERIOD
    BEGIN
      IF V_RECORD_DATE IS NOT NULL THEN
        select gg.period_id
          INTO V_PERIOD_ID
          from gl_comp_acct_period_dtl gg
         where gg.enabled_flag = '1'
           and gg.workflow_completion_status = '1'
           and gg.PERIOD_STATUS = 'OPEN'
           and gg.COMP_ID = V_JOURNAL_ORG_ID
           and (trunc(V_RECORD_DATE)) between gg.period_from_dt and
               gg.period_to_dt
           and rownum = 1
         order by gg.PERIOD_NAME asc;
      END IF;
    Exception
      When others then
        dbms_output.put_line('Cal period ' || sqlerrm);
    End;

    v_JE_HDR_ID := ssm.get_next_sequence('GL_007_M', 'EN');

    IF (V_JOURNAL_TYPE <> 'PREPAYMENT' or V_JOURNAL_TYPE <> 'PRE PAYMENT') and
       (sqlerrm = 'ORA-0000: normal, successful completion') THEN

      --INSERT THE JOURNAL HEADER RECORD FOR INV

      BEGIN
        GL_INSERT(P_JE_HDR_ID              => v_JE_HDR_ID,
                  p_JE_COMP_ID             => V_JOURNAL_ORG_ID,
                  P_JE_DATE                => V_RECORD_DATE,
                  P_JE_GLOBAL_SEGMENT_ID   => V_JOURNAL_GL_SEGMENT_ID,
                  P_JE_NUMBER              => v_JN_NUMBER_ID, --'GL_007_JN',
                  P_JE_TYPE                => V_JOURNAL_TYPE,
                  P_JE_CURRENCY_ID         => V_COMP_CURRENCY,
                  P_JE_DESC                => 'Invoice for ' ||
                                              V_SUPPLIER_NUMBER ||
                                              ', Invoice Number:' ||
                                              V_JOURNAL_GRN_NUM ||
                                              ', Invoice Amount:' ||
                                              V_JOURNAL_DR_AMOUNT,
                  P_JE_CURRENCY_RATE_ID    => NULL,
                  P_JE_STATUS              => 'OPEN',
                  P_JE_CURR_VALUE          => NULL,
                  P_JE_PERIOD_ID           => V_PERIOD_ID,
                  P_JE_REFERENCE           => 'AP:Invoice Number:' ||
                                              V_JOURNAL_GRN_NUM,
                  P_JE_EXCHANGE_RATE_VALUE => V_EXCHANGE_RATE_VALUE,
                  P_JE_TOT_DR_AMOUNT       => V_JOURNAL_DR_AMOUNT,
                  P_JE_TOT_CR_AMOUNT       => V_JOURNAL_DR_AMOUNT,
                  P_JE_RECURRING_END_DATE  => NULL,
                  P_JE_RECURRING_FREQUENCY => NULL,
                  P_JE_SOURCE              => V_JOURNAL_SOURCE,
                  P_JOURNAL_REVERSED       => NULL,
                  P_ACTUAL_JOURNAL_ID      => NULL,
                  P_MODULE                 => 'AP');

        INSERT_PROCESS_LOG(P_PROCESS_ID => v_JE_HDR_ID,

                           P_PROCESS_CODE   => 'AP_Invoice_HDR_' ||
                                               V_JOURNAL_GRN_NUM,
                           P_PROCESS_ERROR  => sqlerrm,
                           P_PROCESS_DATE   => SYSDATE,
                           P_PROCESS_ORG_ID => V_JOURNAL_ORG_ID,
                           P_PROCESS_STATUS => 'POSTED');
      Exception
        When others then
          dbms_output.put_line('INSERT JOURNAL HEADER  ' || sqlerrm);

          INSERT_PROCESS_LOG(P_PROCESS_ID     => 'ERROR',
                             P_PROCESS_CODE   => 'AP_Invoice_HDR_' ||
                                                 V_JOURNAL_GRN_NUM,
                             P_PROCESS_ERROR  => sqlerrm,
                             P_PROCESS_DATE   => SYSDATE,
                             P_PROCESS_ORG_ID => V_JOURNAL_ORG_ID,
                             P_PROCESS_STATUS => 'ERROR');
      End;

      IF V_JOURNAL_TYPE = 'CREDIT MEMO' OR V_JOURNAL_TYPE = 'STANDARD' OR
         V_JOURNAL_TYPE = 'PO_INVOICE' THEN
        V_CREDIT_FLAG := 'Cr';
        V_DEBIT_FLAG  := 'Dr';
      END IF;

      IF V_JOURNAL_TYPE = 'DEBIT MEMO' THEN
        V_CREDIT_FLAG := 'Dr';
        V_DEBIT_FLAG  := 'Cr';
      END IF;
      --INSERT THE LIABILITY RECORD

      if V_JOURNAL_RENTENON_AMOUNT > 0 then
        BEGIN
          --INSERT THE dtl record- LIABILITY-rentention
          GL_INSERT_JR_DTL(P_JE_DTL_ID           => v_JE_DTL_ID,
                           P_JE_HDR_ID           => v_JE_HDR_ID,
                           P_JE_ACCT_CODE_ID     => V_LIABILITY_ACCOUNT,
                           P_JE_PARTICULARS      => V_DESC,
                           P_JE_CREDIT_DEBIT     => V_CREDIT_FLAG,
                           P_JE_AMOUNT_CR        => (V_LIABILITY_AMOUNT -
                                                    V_JOURNAL_RENTENON_AMOUNT),
                           P_JE_ACCOUNTED_AMT_CR => (V_LIABILITY_AMOUNT -
                                                    V_JOURNAL_RENTENON_AMOUNT) *
                                                    V_EXCHANGE_RATE_VALUE,
                           P_JE_SEGMENT_ID_1     => NULL,
                           P_JE_SEGMENT_ID_2     => NULL,
                           P_JE_SEGMENT_ID_3     => NULL,
                           P_JE_SEGMENT_ID_4     => NULL,
                           P_JE_SEGMENT_ID_5     => NULL,
                           P_JE_SEGMENT_ID_6     => NULL,
                           P_JE_AMOUNT_DR        => NULL,
                           P_JE_ACCOUNTED_AMT_DR => NULL);

          INSERT_PROCESS_LOG(P_PROCESS_ID     => v_JE_DTL_ID,
                             P_PROCESS_CODE   => 'AP_Invoice_LIABILITY_RENTENTION_DTL' ||
                                                 V_JOURNAL_GRN_NUM,
                             P_PROCESS_ERROR  => sqlerrm,
                             P_PROCESS_DATE   => SYSDATE,
                             P_PROCESS_ORG_ID => V_JOURNAL_ORG_ID,
                             P_PROCESS_STATUS => 'POSTED');
        Exception
          When others then
            dbms_output.put_line('INSERT LIABILITY RECORD retention supplier ' ||
                                 sqlerrm);

            INSERT_PROCESS_LOG(P_PROCESS_ID     => v_JE_DTL_ID,
                               P_PROCESS_CODE   => 'AP_Invoice_LIABILITY_RENTENTION_DTL' ||
                                                   V_JOURNAL_GRN_NUM,
                               P_PROCESS_ERROR  => sqlerrm,
                               P_PROCESS_DATE   => SYSDATE,
                               P_PROCESS_ORG_ID => V_JOURNAL_ORG_ID,
                               P_PROCESS_STATUS => 'ERROR');
        End;

        BEGIN

          GL_INSERT_JR_DTL(P_JE_DTL_ID           => v_JE_DTL_ID,
                           P_JE_HDR_ID           => v_JE_HDR_ID,
                           P_JE_ACCT_CODE_ID     => V_RENTENTION_ACCOUNT,
                           P_JE_PARTICULARS      => V_DESC ||
                                                    ' FOR RETENTION',
                           P_JE_CREDIT_DEBIT     => V_CREDIT_FLAG,
                           P_JE_AMOUNT_CR        => V_JOURNAL_RENTENON_AMOUNT,
                           P_JE_ACCOUNTED_AMT_CR => V_JOURNAL_RENTENON_AMOUNT *
                                                    V_EXCHANGE_RATE_VALUE,
                           P_JE_SEGMENT_ID_1     => NULL,
                           P_JE_SEGMENT_ID_2     => NULL,
                           P_JE_SEGMENT_ID_3     => NULL,
                           P_JE_SEGMENT_ID_4     => NULL,
                           P_JE_SEGMENT_ID_5     => NULL,
                           P_JE_SEGMENT_ID_6     => NULL,
                           P_JE_AMOUNT_DR        => NULL,
                           P_JE_ACCOUNTED_AMT_DR => NULL);

          INSERT_PROCESS_LOG(P_PROCESS_ID     => v_JE_DTL_ID,
                             P_PROCESS_CODE   => 'AP_Invoice_RENTENTION_DTL' ||
                                                 V_JOURNAL_GRN_NUM,
                             P_PROCESS_ERROR  => sqlerrm,
                             P_PROCESS_DATE   => SYSDATE,
                             P_PROCESS_ORG_ID => V_JOURNAL_ORG_ID,
                             P_PROCESS_STATUS => 'POSTED');
        Exception
          When others then
            dbms_output.put_line('INSERT RETENTION ACCOUNT  RECORD  ' ||
                                 sqlerrm);

            INSERT_PROCESS_LOG(P_PROCESS_ID     => v_JE_DTL_ID,
                               P_PROCESS_CODE   => 'AP_Invoice_RENTENTION_DTL' ||
                                                   V_JOURNAL_GRN_NUM,
                               P_PROCESS_ERROR  => sqlerrm,
                               P_PROCESS_DATE   => SYSDATE,
                               P_PROCESS_ORG_ID => V_JOURNAL_ORG_ID,
                               P_PROCESS_STATUS => 'ERROR');
        End;
      else
        BEGIN
          GL_INSERT_JR_DTL(P_JE_DTL_ID           => v_JE_DTL_ID,
                           P_JE_HDR_ID           => v_JE_HDR_ID,
                           P_JE_ACCT_CODE_ID     => V_LIABILITY_ACCOUNT,
                           P_JE_PARTICULARS      => V_DESC,
                           P_JE_CREDIT_DEBIT     => V_CREDIT_FLAG,
                           P_JE_AMOUNT_CR        => V_LIABILITY_AMOUNT,
                           P_JE_ACCOUNTED_AMT_CR => V_LIABILITY_AMOUNT *
                                                    V_EXCHANGE_RATE_VALUE,
                           P_JE_SEGMENT_ID_1     => NULL,
                           P_JE_SEGMENT_ID_2     => NULL,
                           P_JE_SEGMENT_ID_3     => NULL,
                           P_JE_SEGMENT_ID_4     => NULL,
                           P_JE_SEGMENT_ID_5     => NULL,
                           P_JE_SEGMENT_ID_6     => NULL,
                           P_JE_AMOUNT_DR        => NULL,
                           P_JE_ACCOUNTED_AMT_DR => NULL);

          INSERT_PROCESS_LOG(P_PROCESS_ID     => v_JE_DTL_ID,
                             P_PROCESS_CODE   => 'AP_Invoice_LIABILITY_DTL' ||
                                                 V_JOURNAL_GRN_NUM,
                             P_PROCESS_ERROR  => sqlerrm,
                             P_PROCESS_DATE   => SYSDATE,
                             P_PROCESS_ORG_ID => V_JOURNAL_ORG_ID,
                             P_PROCESS_STATUS => 'POSTED');
        Exception
          When others then
            dbms_output.put_line('INSERT LIABILITY RECORD  ' || sqlerrm);

            INSERT_PROCESS_LOG(P_PROCESS_ID     => v_JE_DTL_ID,
                               P_PROCESS_CODE   => 'AP_Invoice_LIABILITY_DTL' ||
                                                   V_JOURNAL_GRN_NUM,
                               P_PROCESS_ERROR  => sqlerrm,
                               P_PROCESS_DATE   => SYSDATE,
                               P_PROCESS_ORG_ID => V_JOURNAL_ORG_ID,
                               P_PROCESS_STATUS => 'ERROR');
        End;
      end if;

      ---INSERT THE MATERIAL ACCOUNT RECORDS
      BEGIN
        FOR F_GET_AP_DATA IN C_GET_AP_DATA LOOP
          GL_INSERT_JR_DTL(P_JE_DTL_ID           => v_JE_DTL_ID,
                           P_JE_HDR_ID           => v_JE_HDR_ID,
                           P_JE_ACCT_CODE_ID     => F_GET_AP_DATA.MATRIAL_ACCOUNT,
                           P_JE_PARTICULARS      => F_GET_AP_DATA.INV_DESCRIPTION,
                           P_JE_CREDIT_DEBIT     => V_DEBIT_FLAG,
                           P_JE_AMOUNT_CR        => NULL,
                           P_JE_ACCOUNTED_AMT_CR => NULL,
                           P_JE_SEGMENT_ID_1     => NULL,
                           P_JE_SEGMENT_ID_2     => NULL,
                           P_JE_SEGMENT_ID_3     => NULL,
                           P_JE_SEGMENT_ID_4     => NULL,
                           P_JE_SEGMENT_ID_5     => NULL,
                           P_JE_SEGMENT_ID_6     => NULL,
                           P_JE_AMOUNT_DR        => F_GET_AP_DATA.MATRIAL_AMOUNT,
                           P_JE_ACCOUNTED_AMT_DR => F_GET_AP_DATA.MATRIAL_AMOUNT *
                                                    V_EXCHANGE_RATE_VALUE);
        END LOOP;
        INSERT_PROCESS_LOG(P_PROCESS_ID     => v_JE_DTL_ID,
                           P_PROCESS_CODE   => 'AP_Invoice_MAIN_DTL' ||
                                               V_JOURNAL_GRN_NUM,
                           P_PROCESS_ERROR  => sqlerrm,
                           P_PROCESS_DATE   => SYSDATE,
                           P_PROCESS_ORG_ID => V_JOURNAL_ORG_ID,
                           P_PROCESS_STATUS => 'POSTED');
      Exception
        When others then
          dbms_output.put_line('INSERT MATERIAL ACCOUNT  RECORD  ' ||
                               sqlerrm);

          INSERT_PROCESS_LOG(P_PROCESS_ID     => v_JE_DTL_ID,
                             P_PROCESS_CODE   => 'AP_Invoice_MAIN_DTL' ||
                                                 V_JOURNAL_GRN_NUM,
                             P_PROCESS_ERROR  => sqlerrm,
                             P_PROCESS_DATE   => SYSDATE,
                             P_PROCESS_ORG_ID => V_JOURNAL_ORG_ID,
                             P_PROCESS_STATUS => 'ERROR');
      End;
      commit;
    else
      INSERT_PROCESS_LOG(P_PROCESS_ID     => 'ERROR',
                         P_PROCESS_CODE   => 'AP_Invoice_HDR_' ||
                                             V_JOURNAL_GRN_NUM,
                         P_PROCESS_ERROR  => sqlerrm,
                         P_PROCESS_DATE   => SYSDATE,
                         P_PROCESS_ORG_ID => V_JOURNAL_ORG_ID,
                         P_PROCESS_STATUS => 'ERROR');
      dbms_output.put_line('Cal period ' || sqlerrm);
      commit;
    end if;

  END AP_INVOICE_POSTING;

  ---AP PAYMENT POSTING
  procedure AP_PAYMENT_POSTING(P_PAYMENT_ID VARCHAR2, P_ORG_ID VARCHAR2) is

    --GET THE ITEM,payment DATA
    cursor C_GET_AP_CASH_AC is
      SELECT 'GL_007_D' as JE_DTL_ID,
             'AP PAYMENT-CASH AC' AS INV_DESCRIPTION,
             (select so.ap_default_cash_acct_code
                from ssm_system_options so
               where so.org_id = r.org_id
                 AND ROWNUM = 1) AS MATRIAL_ACCOUNT,
             R.PAYMENT_AMT AS MATRIAL_AMOUNT,
             (select sc.vendor_retention_acct_id
                from supplier_customers sc
               where sc.vendor_id = r.pay_vendor_id
                 and sc.workflow_completion_status = 1
                 and rownum = 1) as retention_acct_id
        FROM inv_payments_hdr R
       WHERE R.Pay_Id = P_PAYMENT_ID
         and r.org_id = P_ORG_ID
         AND R.ENABLED_FLAG = '1'
         AND R.WORKFLOW_COMPLETION_STATUS = '1';

    cursor C_GET_AP_BANK_AC is
      SELECT 'GL_007_D' as JE_DTL_ID,
             'AP PAYMENT-BANK AC' AS INV_DESCRIPTION,
             (select CA.GL_ACCOUNT
                from CA_BANK_ACCOUNTS CA
               where CA.BANK_ID = R.PAY_BANK_ID
                 AND CA.ORG_ID = R.ORG_ID
                 AND ROWNUM = 1) AS MATRIAL_ACCOUNT,
             R.PAYMENT_AMT AS MATRIAL_AMOUNT
        FROM inv_payments_hdr R
       WHERE R.Pay_Id = P_PAYMENT_ID
         and r.org_id = P_ORG_ID
         AND R.ENABLED_FLAG = '1'
         AND R.WORKFLOW_COMPLETION_STATUS = '1';

    cursor C_GET_AP_BANK_AC_OC is
      SELECT 'GL_007_D' as JE_DTL_ID,
             r.remarks AS INV_DESCRIPTION,
             (select CA.GL_ACCOUNT
                from CA_BANK_ACCOUNTS CA, INV_PAYMENTS_HDR IPR
               where CA.BANK_ID = IPR.PAY_BANK_ID
                 AND CA.ORG_ID = IPR.ORG_ID
                 AND CA.ACCOUNT_ID = ipR.Pay_Account_Id
                 AND IPR.PAY_ID = r.pay_id
                 AND ROWNUM = 1) AS MATRIAL_ACCOUNT,
             R.Poc_Amount AS MATRIAL_AMOUNT,
             r.acct_code_id
        FROM INV_PAYMENTS_OTHER_CHARGES R
       WHERE R.Pay_Id = P_PAYMENT_ID
         and r.org_id = P_ORG_ID
         AND R.ENABLED_FLAG = '1'
         AND R.WORKFLOW_COMPLETION_STATUS = '1';

    --- GET RENTION AMOUNT
    cursor C_GET_REN_AMT is
      SELECT NVL(DD.PAY_RETENTIION_AMT, 0) AS PAY_RETENTION_AMT,
             (select sc.vendor_retention_acct_id
                from supplier_customers sc
               where sc.vendor_id = r.pay_vendor_id
                 and sc.workflow_completion_status = 1
                 and rownum = 1) as retention_acct_id
        FROM inv_payments_hdr R, INV_PAYMENTS_DTL DD
       WHERE r.org_id = P_ORG_ID
         and R.Pay_Id = P_PAYMENT_ID
         AND R.PAY_ID = DD.PAY_ID
         AND R.ENABLED_FLAG = '1'
         AND R.WORKFLOW_COMPLETION_STATUS = '1';

    -------------------------------------------------------------------------------------------------------------------
    --VARIABLE DECLARATION
    V_LIABILITY_AMOUNT      NUMBER;
    V_RENTENTION_ACCOUNT    VARCHAR(500);
    V_LIABILITY_REN_AMOUNT  NUMBER;
    V_REN_AMOUNT            NUMBER;
    V_LIABILITY_ACCOUNT     VARCHAR(500);
    v_JE_HDR_ID             VARCHAR(500);
    v_JE_HDR_ID_VAL         VARCHAR(500);
    v_JN_NUMBER_ID          VARCHAR(500);
    v_JE_DTL_ID             VARCHAR(500);
    V_JOURNAL_TYPE          VARCHAR(500);
    V_JOURNAL_SOURCE        VARCHAR(500);
    V_JOURNAL_DATE          DATE;
    V_JOURNAL_DR_AMOUNT     NUMBER;
    V_JOURNAL_RENTN_AMOUNT  NUMBER;
    V_JOURNAL_ORG_ID        VARCHAR(500);
    V_JOURNAL_GRN_NUM       VARCHAR(500);
    V_JOURNAL_GL_SEGMENT_ID VARCHAR(500);
    V_DESC                  VARCHAR(1500);
    V_COMP_CURRENCY         VARCHAR(500);
    V_PERIOD_ID             VARCHAR(500);
    V_EXCHANGE_RATE_VALUE   NUMBER;
    V_CREDIT_FLAG           VARCHAR(500);
    V_DEBIT_FLAG            VARCHAR(500);
    V_SUPPLIER_NUMBER       VARCHAR(500);
    V_RECORD_DATE           DATE;
    v_inv_num               varchar2(1500);
  BEGIN

    V_JOURNAL_SOURCE := 'AP';
    V_JOURNAL_DATE   := TRUNC(SYSDATE);

    for invd in (select iid.attribute1, iih.inv_num
                   from inv_payments_dtl  ipd,
                        inv_invoices_dtls iid,
                        inv_invoices_hdr  iih
                  where ipd.pay_id = P_PAYMENT_ID
                    and ipd.pay_invoice_id = iid.inv_id
                    and ipd.pay_invoice_id = iih.inv_id) loop

      V_DESC    := V_DESC || ' ' || invd.attribute1;
      v_inv_num := v_inv_num || ',' || invd.inv_num;

    end loop;

    --GET THE INV DATA JL HEADER
    BEGIN
      SELECT distinct 'GL_007_M' as JE_HDR_ID,
                      'GL_007_JN' as JN_NUMBER_ID,
                      r.GLOBAL_SEGMENT_ID AS COMP_GLOBAL_SEGMENT_ID,
                      R.ORG_ID AS ORG_ID,
                      R.PAY_ID,
                      R.PAYMENT_AMT,
                      r.Payment_Currency as org_currency,
                      R.EXC_RATE_VALUE,
                      upper(r.Pay_Mode) as inv_type,
                      R.PAY_RETENTION_AMT,
                      r.pay_date

        into v_JE_HDR_ID_VAL,
             v_JN_NUMBER_ID,
             V_JOURNAL_GL_SEGMENT_ID,
             V_JOURNAL_ORG_ID,
             V_JOURNAL_GRN_NUM,
             V_JOURNAL_DR_AMOUNT,
             V_COMP_CURRENCY,
             V_EXCHANGE_RATE_VALUE,
             V_JOURNAL_TYPE,
             V_JOURNAL_RENTN_AMOUNT,
             V_RECORD_DATE
        FROM INV_PAYMENTS_HDR R
       WHERE R.ENABLED_FLAG = '1'
         AND R.WORKFLOW_COMPLETION_STATUS = '1'
         and R.Org_Id = P_ORG_ID
         AND R.PAY_ID = P_PAYMENT_ID
         AND R.ENABLED_FLAG = 1;
    Exception
      When others then
        dbms_output.put_line('INV DATA JL HEADER ' || sqlerrm);
    End;

    --GET LIABILITY ACC CODE AND AMOUNT
    BEGIN
      SELECT 'GL_007_D' as JE_DTL_ID,
             r.PAYMENT_AMT AS LIABILITY_AMOUNT,
             R.PAY_RETENTION_AMT,
             (select (case
                       when SC.AP_LIABILITY_ACCOUNT is null then
                        ss.ap_liability_acct
                       else
                        SC.AP_LIABILITY_ACCOUNT
                     end)
                from ssm_system_options ss
               where rownum = 1) AS LIABILITY_ACCOUNT,
             -- 'AP PAYMENT-SUPPLIER AC' AS REMARKS,
             sc.vendor_code || ' ' || sc.vendor_name
        INTO v_JE_DTL_ID,
             V_LIABILITY_AMOUNT,
             V_LIABILITY_REN_AMOUNT,
             V_LIABILITY_ACCOUNT,
             -- V_DESC,
             V_SUPPLIER_NUMBER
        FROM INV_PAYMENTS_HDR R, SUPPLIER_CUSTOMERS SC
       WHERE SC.VENDOR_ID = R.PAY_VENDOR_ID
         AND R.ENABLED_FLAG = 1
         and sc.enabled_flag = '1'
         and sc.workflow_completion_status = '1'
         AND R.WORKFLOW_COMPLETION_STATUS = '1'
         AND R.Org_Id = P_ORG_ID
         AND R.PAY_ID = P_PAYMENT_ID;
    Exception
      When others then
        dbms_output.put_line('LIABILITY ACC CODE ' || sqlerrm);
    End;

    --GET CAL PERIOD
    BEGIN
      if V_RECORD_DATE is not null then
        select gg.period_id
          INTO V_PERIOD_ID
          from gl_comp_acct_period_dtl gg
         where gg.enabled_flag = '1'
           and gg.workflow_completion_status = '1'
           and gg.PERIOD_STATUS = 'OPEN'
           and gg.COMP_ID = V_JOURNAL_ORG_ID
           and (trunc(V_RECORD_DATE)) between gg.period_from_dt and
               gg.period_to_dt
           and rownum = 1
         order by gg.PERIOD_NAME asc;
      end if;
    Exception
      When others then
        dbms_output.put_line('Cal period ' || sqlerrm);
    End;

    v_JE_HDR_ID := ssm.get_next_sequence(v_JE_HDR_ID_VAL, 'EN');
    if (sqlerrm = 'ORA-0000: normal, successful completion') then
      --INSERT THE JOURNAL HEADER RECORD FOR INV
      BEGIN
        GL_INSERT(P_JE_HDR_ID              => v_JE_HDR_ID,
                  p_JE_COMP_ID             => V_JOURNAL_ORG_ID,
                  P_JE_DATE                => V_RECORD_DATE,
                  P_JE_GLOBAL_SEGMENT_ID   => V_JOURNAL_GL_SEGMENT_ID,
                  P_JE_NUMBER              => v_JN_NUMBER_ID,
                  P_JE_TYPE                => V_JOURNAL_TYPE,
                  P_JE_CURRENCY_ID         => V_COMP_CURRENCY,
                  P_JE_DESC                => 'Payment for ' ||
                                              V_SUPPLIER_NUMBER ||
                                              ', Payment Number:' ||
                                              V_JOURNAL_GRN_NUM ||
                                              ', Payment Amount:' ||
                                              V_JOURNAL_DR_AMOUNT ||
                                              ' Invoices: ' || v_inv_num,
                  P_JE_CURRENCY_RATE_ID    => NULL,
                  P_JE_STATUS              => 'OPEN',
                  P_JE_CURR_VALUE          => NULL,
                  P_JE_PERIOD_ID           => V_PERIOD_ID,
                  P_JE_REFERENCE           => 'AP:Payment Number:' ||
                                              V_JOURNAL_GRN_NUM,
                  P_JE_EXCHANGE_RATE_VALUE => V_EXCHANGE_RATE_VALUE,
                  P_JE_TOT_DR_AMOUNT       => V_JOURNAL_DR_AMOUNT,
                  P_JE_TOT_CR_AMOUNT       => V_JOURNAL_DR_AMOUNT,
                  P_JE_RECURRING_END_DATE  => NULL,
                  P_JE_RECURRING_FREQUENCY => NULL,
                  P_JE_SOURCE              => V_JOURNAL_SOURCE,
                  P_JOURNAL_REVERSED       => NULL,
                  P_ACTUAL_JOURNAL_ID      => NULL,
                  P_MODULE                 => 'AP');

        INSERT_PROCESS_LOG(P_PROCESS_ID => v_JE_HDR_ID,

                           P_PROCESS_CODE   => 'AP_Payment_HDR_' ||
                                               V_JOURNAL_GRN_NUM,
                           P_PROCESS_ERROR  => sqlerrm,
                           P_PROCESS_DATE   => SYSDATE,
                           P_PROCESS_ORG_ID => V_JOURNAL_ORG_ID,
                           P_PROCESS_STATUS => 'POSTED');
      Exception
        When others then
          dbms_output.put_line('INSERT JOURNAL HEADER  ' || sqlerrm);

          INSERT_PROCESS_LOG(P_PROCESS_ID => v_JE_HDR_ID,

                             P_PROCESS_CODE   => 'AP_Payment_HDR_' ||
                                                 V_JOURNAL_GRN_NUM,
                             P_PROCESS_ERROR  => sqlerrm,
                             P_PROCESS_DATE   => SYSDATE,
                             P_PROCESS_ORG_ID => V_JOURNAL_ORG_ID,
                             P_PROCESS_STATUS => 'ERROR');
      End;

      V_CREDIT_FLAG := 'Cr';
      V_DEBIT_FLAG  := 'Dr';

      --INSERT THE LIABILITY RECORD

      FOR F_GET_REN_AMT IN C_GET_REN_AMT LOOP
        V_REN_AMOUNT := F_GET_REN_AMT.PAY_RETENTION_AMT;
        IF V_REN_AMOUNT > 0 THEN
          BEGIN
            GL_INSERT_JR_DTL(P_JE_DTL_ID           => v_JE_DTL_ID,
                             P_JE_HDR_ID           => v_JE_HDR_ID,
                             P_JE_ACCT_CODE_ID     => V_LIABILITY_ACCOUNT,
                             P_JE_PARTICULARS      => V_DESC,
                             P_JE_CREDIT_DEBIT     => V_DEBIT_FLAG,
                             P_JE_AMOUNT_CR        => NULL,
                             P_JE_ACCOUNTED_AMT_CR => NULL,
                             P_JE_SEGMENT_ID_1     => NULL,
                             P_JE_SEGMENT_ID_2     => NULL,
                             P_JE_SEGMENT_ID_3     => NULL,
                             P_JE_SEGMENT_ID_4     => NULL,
                             P_JE_SEGMENT_ID_5     => NULL,
                             P_JE_SEGMENT_ID_6     => NULL,
                             P_JE_AMOUNT_DR        => V_LIABILITY_AMOUNT -
                                                      V_REN_AMOUNT,
                             P_JE_ACCOUNTED_AMT_DR => V_LIABILITY_AMOUNT -
                                                      V_REN_AMOUNT *
                                                      V_EXCHANGE_RATE_VALUE);

            INSERT_PROCESS_LOG(P_PROCESS_ID => v_JE_HDR_ID,

                               P_PROCESS_CODE   => 'AP_Payment_DTL_2367' ||
                                                   V_JOURNAL_GRN_NUM,
                               P_PROCESS_ERROR  => sqlerrm,
                               P_PROCESS_DATE   => SYSDATE,
                               P_PROCESS_ORG_ID => V_JOURNAL_ORG_ID,
                               P_PROCESS_STATUS => 'POSTED');
          Exception
            When others then
              dbms_output.put_line('INSERT LIABILITY RECORD  ' || sqlerrm);
              INSERT_PROCESS_LOG(P_PROCESS_ID => v_JE_HDR_ID,

                                 P_PROCESS_CODE   => 'AP_Payment_DTL_2378' ||
                                                     V_JOURNAL_GRN_NUM,
                                 P_PROCESS_ERROR  => sqlerrm,
                                 P_PROCESS_DATE   => SYSDATE,
                                 P_PROCESS_ORG_ID => V_JOURNAL_ORG_ID,
                                 P_PROCESS_STATUS => 'ERROR');
          End;
          BEGIN
            GL_INSERT_JR_DTL(P_JE_DTL_ID           => v_JE_DTL_ID,
                             P_JE_HDR_ID           => v_JE_HDR_ID,
                             P_JE_ACCT_CODE_ID     => F_GET_REN_AMT.RETENTION_ACCT_ID,
                             P_JE_PARTICULARS      => V_DESC,
                             P_JE_CREDIT_DEBIT     => V_DEBIT_FLAG,
                             P_JE_AMOUNT_CR        => NULL,
                             P_JE_ACCOUNTED_AMT_CR => NULL,
                             P_JE_SEGMENT_ID_1     => NULL,
                             P_JE_SEGMENT_ID_2     => NULL,
                             P_JE_SEGMENT_ID_3     => NULL,
                             P_JE_SEGMENT_ID_4     => NULL,
                             P_JE_SEGMENT_ID_5     => NULL,
                             P_JE_SEGMENT_ID_6     => NULL,
                             P_JE_AMOUNT_DR        => F_GET_REN_AMT.PAY_RETENTION_AMT,
                             P_JE_ACCOUNTED_AMT_DR => F_GET_REN_AMT.PAY_RETENTION_AMT *
                                                      V_EXCHANGE_RATE_VALUE);

            INSERT_PROCESS_LOG(P_PROCESS_ID => v_JE_HDR_ID,

                               P_PROCESS_CODE   => 'AP_Payment_DTL_2404' ||
                                                   V_JOURNAL_GRN_NUM,
                               P_PROCESS_ERROR  => sqlerrm,
                               P_PROCESS_DATE   => SYSDATE,
                               P_PROCESS_ORG_ID => V_JOURNAL_ORG_ID,
                               P_PROCESS_STATUS => 'POSTED');
          Exception
            When others then
              dbms_output.put_line('INSERT LIABILITY RECORD  ' || sqlerrm);

              INSERT_PROCESS_LOG(P_PROCESS_ID => v_JE_HDR_ID,

                                 P_PROCESS_CODE   => 'AP_Payment_DTL_2416' ||
                                                     V_JOURNAL_GRN_NUM,
                                 P_PROCESS_ERROR  => sqlerrm,
                                 P_PROCESS_DATE   => SYSDATE,
                                 P_PROCESS_ORG_ID => V_JOURNAL_ORG_ID,
                                 P_PROCESS_STATUS => 'ERROR');
          End;
        ELSE
          BEGIN
            GL_INSERT_JR_DTL(P_JE_DTL_ID           => v_JE_DTL_ID,
                             P_JE_HDR_ID           => v_JE_HDR_ID,
                             P_JE_ACCT_CODE_ID     => V_LIABILITY_ACCOUNT,
                             P_JE_PARTICULARS      => V_DESC,
                             P_JE_CREDIT_DEBIT     => V_DEBIT_FLAG,
                             P_JE_AMOUNT_DR        => V_LIABILITY_AMOUNT,
                             P_JE_ACCOUNTED_AMT_DR => V_LIABILITY_AMOUNT *
                                                      V_EXCHANGE_RATE_VALUE,
                             P_JE_SEGMENT_ID_1     => NULL,
                             P_JE_SEGMENT_ID_2     => NULL,
                             P_JE_SEGMENT_ID_3     => NULL,
                             P_JE_SEGMENT_ID_4     => NULL,
                             P_JE_SEGMENT_ID_5     => NULL,
                             P_JE_SEGMENT_ID_6     => NULL,
                             P_JE_AMOUNT_CR        => NULL,
                             P_JE_ACCOUNTED_AMT_CR => NULL);

            INSERT_PROCESS_LOG(P_PROCESS_ID => v_JE_HDR_ID,

                               P_PROCESS_CODE   => 'AP_Payment_DTL_2442' ||
                                                   V_JOURNAL_GRN_NUM,
                               P_PROCESS_ERROR  => sqlerrm,
                               P_PROCESS_DATE   => SYSDATE,
                               P_PROCESS_ORG_ID => V_JOURNAL_ORG_ID,
                               P_PROCESS_STATUS => 'POSTED');
          Exception
            When others then
              dbms_output.put_line('INSERT LIABILITY RECORD  ' || sqlerrm);

              INSERT_PROCESS_LOG(P_PROCESS_ID => v_JE_HDR_ID,

                                 P_PROCESS_CODE   => 'AP_Payment_DTL_2453' ||
                                                     V_JOURNAL_GRN_NUM,
                                 P_PROCESS_ERROR  => sqlerrm,
                                 P_PROCESS_DATE   => SYSDATE,
                                 P_PROCESS_ORG_ID => V_JOURNAL_ORG_ID,
                                 P_PROCESS_STATUS => 'ERROR');
          End;
        END IF;
      END LOOP;

      ---INSERT THE MATERIAL ACCOUNT RECORDS

      BEGIN
        select upper(R.pay_mode) as paymode
          into V_JOURNAL_TYPE
          from inv_payments_hdr R
         WHERE R.Org_Id = P_ORG_ID
           AND R.PAY_ID = P_PAYMENT_ID;
      Exception
        When others then
          dbms_output.put_line('PAYMENT TYPE ' || sqlerrm);
      End;

      if V_JOURNAL_TYPE = 'CASH' THEN
        BEGIN
          FOR F_GET_AP_DATA IN C_GET_AP_CASH_AC LOOP
            GL_INSERT_JR_DTL(P_JE_DTL_ID           => F_GET_AP_DATA.JE_DTL_ID,
                             P_JE_HDR_ID           => v_JE_HDR_ID,
                             P_JE_ACCT_CODE_ID     => F_GET_AP_DATA.MATRIAL_ACCOUNT,
                             P_JE_PARTICULARS      => F_GET_AP_DATA.INV_DESCRIPTION,
                             P_JE_CREDIT_DEBIT     => 'Cr',
                             P_JE_AMOUNT_CR        => F_GET_AP_DATA.MATRIAL_AMOUNT,
                             P_JE_ACCOUNTED_AMT_CR => F_GET_AP_DATA.MATRIAL_AMOUNT *
                                                      V_EXCHANGE_RATE_VALUE,
                             P_JE_SEGMENT_ID_1     => NULL,
                             P_JE_SEGMENT_ID_2     => NULL,
                             P_JE_SEGMENT_ID_3     => NULL,
                             P_JE_SEGMENT_ID_4     => NULL,
                             P_JE_SEGMENT_ID_5     => NULL,
                             P_JE_SEGMENT_ID_6     => NULL,
                             P_JE_AMOUNT_DR        => NULL,
                             P_JE_ACCOUNTED_AMT_DR => NULL);
          END LOOP;
        Exception
          When others then
            dbms_output.put_line('INSERT MATERIAL CASH ACCOUNT RECORD  ' ||
                                 sqlerrm);
        End;
      END IF;
      if V_JOURNAL_TYPE <> 'CASH' THEN
        BEGIN

          FOR F_GET_AP_BANK_AC IN C_GET_AP_BANK_AC LOOP
            GL_INSERT_JR_DTL(P_JE_DTL_ID           => F_GET_AP_BANK_AC.JE_DTL_ID,
                             P_JE_HDR_ID           => v_JE_HDR_ID,
                             P_JE_ACCT_CODE_ID     => F_GET_AP_BANK_AC.MATRIAL_ACCOUNT,
                             P_JE_PARTICULARS      => F_GET_AP_BANK_AC.INV_DESCRIPTION,
                             P_JE_CREDIT_DEBIT     => 'Cr',
                             P_JE_AMOUNT_CR        => F_GET_AP_BANK_AC.MATRIAL_AMOUNT,
                             P_JE_ACCOUNTED_AMT_CR => F_GET_AP_BANK_AC.MATRIAL_AMOUNT *
                                                      V_EXCHANGE_RATE_VALUE,
                             P_JE_SEGMENT_ID_1     => NULL,
                             P_JE_SEGMENT_ID_2     => NULL,
                             P_JE_SEGMENT_ID_3     => NULL,
                             P_JE_SEGMENT_ID_4     => NULL,
                             P_JE_SEGMENT_ID_5     => NULL,
                             P_JE_SEGMENT_ID_6     => NULL,
                             P_JE_AMOUNT_DR        => NULL,
                             P_JE_ACCOUNTED_AMT_DR => NULL);
          END LOOP;
        Exception
          When others then
            dbms_output.put_line('INSERT MATERIAL BANK ACCOUNT RECORD  ' ||
                                 sqlerrm);
        End;
      END IF;

      commit;

      BEGIN

        FOR F_GET_AP_BANK_AC_OC IN C_GET_AP_BANK_AC_OC LOOP
          GL_INSERT_JR_DTL(P_JE_DTL_ID           => F_GET_AP_BANK_AC_OC.JE_DTL_ID,
                           P_JE_HDR_ID           => v_JE_HDR_ID,
                           P_JE_ACCT_CODE_ID     => F_GET_AP_BANK_AC_OC.MATRIAL_ACCOUNT,
                           P_JE_PARTICULARS      => F_GET_AP_BANK_AC_OC.INV_DESCRIPTION,
                           P_JE_CREDIT_DEBIT     => 'Cr',
                           P_JE_AMOUNT_CR        => F_GET_AP_BANK_AC_OC.MATRIAL_AMOUNT,
                           P_JE_ACCOUNTED_AMT_CR => F_GET_AP_BANK_AC_OC.MATRIAL_AMOUNT *
                                                    V_EXCHANGE_RATE_VALUE,
                           P_JE_SEGMENT_ID_1     => NULL,
                           P_JE_SEGMENT_ID_2     => NULL,
                           P_JE_SEGMENT_ID_3     => NULL,
                           P_JE_SEGMENT_ID_4     => NULL,
                           P_JE_SEGMENT_ID_5     => NULL,
                           P_JE_SEGMENT_ID_6     => NULL,
                           P_JE_AMOUNT_DR        => NULL,
                           P_JE_ACCOUNTED_AMT_DR => NULL);

          GL_INSERT_JR_DTL(P_JE_DTL_ID           => F_GET_AP_BANK_AC_OC.JE_DTL_ID,
                           P_JE_HDR_ID           => v_JE_HDR_ID,
                           P_JE_ACCT_CODE_ID     => F_GET_AP_BANK_AC_OC.acct_code_id,
                           P_JE_PARTICULARS      => F_GET_AP_BANK_AC_OC.INV_DESCRIPTION,
                           P_JE_CREDIT_DEBIT     => 'Dr',
                           P_JE_AMOUNT_DR        => F_GET_AP_BANK_AC_OC.MATRIAL_AMOUNT,
                           P_JE_ACCOUNTED_AMT_DR => F_GET_AP_BANK_AC_OC.MATRIAL_AMOUNT *
                                                    V_EXCHANGE_RATE_VALUE,
                           P_JE_SEGMENT_ID_1     => NULL,
                           P_JE_SEGMENT_ID_2     => NULL,
                           P_JE_SEGMENT_ID_3     => NULL,
                           P_JE_SEGMENT_ID_4     => NULL,
                           P_JE_SEGMENT_ID_5     => NULL,
                           P_JE_SEGMENT_ID_6     => NULL,
                           P_JE_AMOUNT_CR        => NULL,
                           P_JE_ACCOUNTED_AMT_CR => NULL);
        END LOOP;
      Exception
        When others then
          dbms_output.put_line('INSERT MATERIAL BANK ACCOUNT RECORD  ' ||
                               sqlerrm);
      End;

    else
      INSERT_PROCESS_LOG(P_PROCESS_ID     => 'ERROR',
                         P_PROCESS_CODE   => 'AP_Invoice_HDR_' ||
                                             V_JOURNAL_GRN_NUM,
                         P_PROCESS_ERROR  => sqlerrm,
                         P_PROCESS_DATE   => SYSDATE,
                         P_PROCESS_ORG_ID => V_JOURNAL_ORG_ID,
                         P_PROCESS_STATUS => 'ERROR');
      dbms_output.put_line('Cal period ' || sqlerrm);
      commit;
    end if;

  END AP_PAYMENT_POSTING;

  ----AR INVOICE POSTING
  procedure AR_INVOICE_POSTING(P_INV_ID VARCHAR2, P_ORG_ID VARCHAR2) is

    --GET THE ITEM,INVOICE DATA
    cursor C_GET_AP_DATA is
      SELECT 'GL_007_D' as JE_DTL_ID,
             'INV LINE DESC' AS INV_DESCRIPTION,
             d.om_item_acct_code_id AS MATRIAL_ACCOUNT,
             d.om_item_cost AS MATRIAL_AMOUNT,
             (select sc.vendor_retention_acct_id
                from supplier_customers sc
               where sc.vendor_id = r.om_vendor_id
                 and sc.workflow_completion_status = 1
                 and rownum = 1) as retention_acct_id
        FROM OM_CUST_INVOICE_HDR R, OM_CUST_INVOICE_DTL D
       WHERE R.OM_INV_ID = D.OM_INV_ID
         AND R.OM_INV_ID = P_INV_ID
         and r.ORG_ID = P_ORG_ID
         AND R.ENABLED_FLAG = '1'
         AND R.WORKFLOW_COMPLETION_STATUS = '1';

    cursor C_GET_REN_AMT is
      SELECT nvl(r.om_inv_retention_amt, 0) as retention_amt
        FROM OM_CUST_INVOICE_HDR R
       WHERE R.OM_INV_ID = P_INV_ID
         and r.ORG_ID = P_ORG_ID
         AND R.ENABLED_FLAG = '1'
         AND R.WORKFLOW_COMPLETION_STATUS = '1';

    -------------------------------------------------------------------------------------------------------------------
    --VARIABLE DECLARATION
    V_LIABILITY_AMOUNT        NUMBER;
    V_LIABILITY_ACCOUNT       VARCHAR(500);
    V_LIABILITY_ret_ACCOUNT   VARCHAR(500);
    v_JE_HDR_ID               VARCHAR(500);
    v_JE_HDR_ID_VAL           VARCHAR(500);
    v_JN_NUMBER_ID            VARCHAR(500);
    v_JE_DTL_ID               VARCHAR(500);
    V_JOURNAL_TYPE            VARCHAR(500);
    V_JOURNAL_SOURCE          VARCHAR(500);
    V_JOURNAL_DATE            DATE;
    V_JOURNAL_DR_AMOUNT       NUMBER;
    V_JOURNAL_ORG_ID          VARCHAR(500);
    V_JOURNAL_GRN_NUM         VARCHAR(500);
    V_JOURNAL_GL_SEGMENT_ID   VARCHAR(500);
    V_DESC                    VARCHAR(1500);
    V_COMP_CURRENCY           VARCHAR(500);
    V_PERIOD_ID               VARCHAR(500);
    V_EXCHANGE_RATE_VALUE     NUMBER;
    V_CREDIT_FLAG             VARCHAR(500);
    V_DEBIT_FLAG              VARCHAR(500);
    V_JOURNAL_RENTENON_AMOUNT NUMBER;
    V_SUPPLIER_NUMBER         VARCHAR(500);
    V_RECORD_DATE             DATE;
  BEGIN

    V_JOURNAL_SOURCE := 'AR';
    V_JOURNAL_DATE   := TRUNC(SYSDATE);

    --GET THE INV DATA JL HEADER
    BEGIN
      SELECT distinct 'GL_007_M' as JE_HDR_ID,
                      'GL_007_JN' as JN_NUMBER_ID,
                      r.GLOBAL_SEGMENT_ID AS COMP_GLOBAL_SEGMENT_ID,
                      R.org_id AS ORG_ID,
                      r.om_inv_num,
                      r.om_inv_amt,
                      r.om_inv_curr_code as org_currency,
                      r.OM_INV_EXCHANGE_RATE,
                      upper(r.om_inv_type) as inv_type,
                      r.om_inv_retention_amt,
                      r.om_inv_date,
                      (select sc.vendor_retention_acct_id
                         from supplier_customers sc
                        where sc.vendor_id = r.om_vendor_id
                          and sc.workflow_completion_status = 1
                          and rownum = 1) as retention_acct_id
        into v_JE_HDR_ID_VAL,
             v_JN_NUMBER_ID,
             V_JOURNAL_GL_SEGMENT_ID,
             V_JOURNAL_ORG_ID,
             V_JOURNAL_GRN_NUM,
             V_JOURNAL_DR_AMOUNT,
             V_COMP_CURRENCY,
             V_EXCHANGE_RATE_VALUE,
             V_JOURNAL_TYPE,
             V_JOURNAL_RENTENON_AMOUNT,
             V_RECORD_DATE,
             V_LIABILITY_ret_ACCOUNT
        FROM om_cust_invoice_hdr R
       WHERE R.ENABLED_FLAG = '1'
         AND R.WORKFLOW_COMPLETION_STATUS = '1'
         and R.Org_Id = P_ORG_ID
         AND R.OM_INV_ID = P_INV_ID
         AND R.ENABLED_FLAG = 1;
    Exception
      When others then
        dbms_output.put_line('INV DATA JL HEADER ' || sqlerrm);
    End;

    --GET LIABILITY ACC CODE AND AMOUNT
    BEGIN
      SELECT 'GL_007_D' as JE_DTL_ID,
             r.om_inv_amt AS LIABILITY_AMOUNT,
             (select (case
                       when SC.AP_LIABILITY_ACCOUNT is null then
                        ss.ap_liability_acct
                       else
                        SC.AP_LIABILITY_ACCOUNT
                     end)
                from ssm_system_options ss
               where rownum = 1) AS LIABILITY_ACCOUNT,
             r.om_inv_remarks,
             sc.vendor_code || ' ' || sc.vendor_name
        INTO v_JE_DTL_ID,
             V_LIABILITY_AMOUNT,
             V_LIABILITY_ACCOUNT,
             V_DESC,
             V_SUPPLIER_NUMBER
        FROM om_cust_invoice_hdr R, SUPPLIER_CUSTOMERS SC
       WHERE SC.VENDOR_ID = R.OM_VENDOR_ID
         AND R.ENABLED_FLAG = 1
         and sc.enabled_flag = '1'
         and sc.workflow_completion_status = '1'
         AND R.WORKFLOW_COMPLETION_STATUS = '1'
         AND R.Org_Id = P_ORG_ID
         AND R.OM_INV_ID = P_INV_ID;
    Exception
      When others then
        dbms_output.put_line('LIABILITY ACC CODE ' || sqlerrm);
    End;

    --GET CAL PERIOD
    BEGIN
      if V_RECORD_DATE is not null then
        select gg.period_id
          INTO V_PERIOD_ID
          from gl_comp_acct_period_dtl gg
         where gg.enabled_flag = '1'
           and gg.workflow_completion_status = '1'
           and gg.PERIOD_STATUS = 'OPEN'
           and gg.COMP_ID = V_JOURNAL_ORG_ID
           and (trunc(V_RECORD_DATE)) between gg.period_from_dt and
               gg.period_to_dt
         order by gg.PERIOD_NAME asc;
      end if;
    Exception
      When others then
        dbms_output.put_line('Cal period ' || sqlerrm);
    End;

    v_JE_HDR_ID := ssm.get_next_sequence(v_JE_HDR_ID_VAL, 'EN');
    if (sqlerrm = 'ORA-0000: normal, successful completion') then
      IF V_JOURNAL_TYPE <> 'PREPAYMENT' and V_JOURNAL_TYPE <> 'PRE PAYMENT' THEN

        --INSERT THE JOURNAL HEADER RECORD FOR INV
        BEGIN
          GL_INSERT(P_JE_HDR_ID              => v_JE_HDR_ID,
                    p_JE_COMP_ID             => V_JOURNAL_ORG_ID,
                    P_JE_DATE                => V_JOURNAL_DATE,
                    P_JE_GLOBAL_SEGMENT_ID   => V_JOURNAL_GL_SEGMENT_ID,
                    P_JE_NUMBER              => v_JN_NUMBER_ID,
                    P_JE_TYPE                => V_JOURNAL_TYPE,
                    P_JE_CURRENCY_ID         => V_COMP_CURRENCY,
                    P_JE_DESC                => 'Invoice for ' ||
                                                V_SUPPLIER_NUMBER ||
                                                ', Invoice Number:' ||
                                                V_JOURNAL_GRN_NUM ||
                                                ', Invoice Amount:' ||
                                                V_JOURNAL_DR_AMOUNT,
                    P_JE_CURRENCY_RATE_ID    => NULL,
                    P_JE_STATUS              => 'OPEN',
                    P_JE_CURR_VALUE          => NULL,
                    P_JE_PERIOD_ID           => V_PERIOD_ID,
                    P_JE_REFERENCE           => 'AR:Invoice number:' ||
                                                V_JOURNAL_GRN_NUM,
                    P_JE_EXCHANGE_RATE_VALUE => V_EXCHANGE_RATE_VALUE,
                    P_JE_TOT_DR_AMOUNT       => V_JOURNAL_DR_AMOUNT,
                    P_JE_TOT_CR_AMOUNT       => V_JOURNAL_DR_AMOUNT,
                    P_JE_RECURRING_END_DATE  => NULL,
                    P_JE_RECURRING_FREQUENCY => NULL,
                    P_JE_SOURCE              => V_JOURNAL_SOURCE,
                    P_JOURNAL_REVERSED       => NULL,
                    P_ACTUAL_JOURNAL_ID      => NULL,
                    P_MODULE                 => 'AR');

          INSERT_PROCESS_LOG(P_PROCESS_ID     => v_JE_DTL_ID,
                             P_PROCESS_CODE   => 'AR_Invoice_HDR' ||
                                                 V_JOURNAL_GRN_NUM,
                             P_PROCESS_ERROR  => sqlerrm,
                             P_PROCESS_DATE   => SYSDATE,
                             P_PROCESS_ORG_ID => V_JOURNAL_ORG_ID,
                             P_PROCESS_STATUS => 'POSTED');
        Exception
          When others then
            dbms_output.put_line('INSERT JOURNAL HEADER  ' || sqlerrm);
            INSERT_PROCESS_LOG(P_PROCESS_ID     => v_JE_DTL_ID,
                               P_PROCESS_CODE   => 'AR_Invoice_HDR' ||
                                                   V_JOURNAL_GRN_NUM,
                               P_PROCESS_ERROR  => sqlerrm,
                               P_PROCESS_DATE   => SYSDATE,
                               P_PROCESS_ORG_ID => V_JOURNAL_ORG_ID,
                               P_PROCESS_STATUS => 'ERROR');
        End;

        IF V_JOURNAL_TYPE = 'CREDIT MEMO' OR V_JOURNAL_TYPE = 'STANDARD' OR
           V_JOURNAL_TYPE = 'PREPAYMENT' THEN
          V_CREDIT_FLAG := 'Dr';
          V_DEBIT_FLAG  := 'Cr';
        END IF;

        IF V_JOURNAL_TYPE = 'DEBIT MEMO' THEN

          V_CREDIT_FLAG := 'Cr';
          V_DEBIT_FLAG  := 'Dr';
        END IF;

        if V_JOURNAL_RENTENON_AMOUNT > 0 then

          --INSERT THE LIABILITY RECORD
          BEGIN
            GL_INSERT_JR_DTL(P_JE_DTL_ID           => v_JE_DTL_ID,
                             P_JE_HDR_ID           => v_JE_HDR_ID,
                             P_JE_ACCT_CODE_ID     => V_LIABILITY_ACCOUNT,
                             P_JE_PARTICULARS      => V_DESC,
                             P_JE_CREDIT_DEBIT     => V_CREDIT_FLAG,
                             P_JE_AMOUNT_CR        => (V_LIABILITY_AMOUNT -
                                                      V_JOURNAL_RENTENON_AMOUNT),
                             P_JE_ACCOUNTED_AMT_CR => (V_LIABILITY_AMOUNT -
                                                      V_JOURNAL_RENTENON_AMOUNT) *
                                                      V_EXCHANGE_RATE_VALUE,
                             P_JE_SEGMENT_ID_1     => NULL,
                             P_JE_SEGMENT_ID_2     => NULL,
                             P_JE_SEGMENT_ID_3     => NULL,
                             P_JE_SEGMENT_ID_4     => NULL,
                             P_JE_SEGMENT_ID_5     => NULL,
                             P_JE_SEGMENT_ID_6     => NULL,
                             P_JE_AMOUNT_DR        => NULL,
                             P_JE_ACCOUNTED_AMT_DR => NULL);

            INSERT_PROCESS_LOG(P_PROCESS_ID     => v_JE_DTL_ID,
                               P_PROCESS_CODE   => 'AR_Invoice_LIABILITY_RENTENTION_DTL1' ||
                                                   V_JOURNAL_GRN_NUM,
                               P_PROCESS_ERROR  => sqlerrm,
                               P_PROCESS_DATE   => SYSDATE,
                               P_PROCESS_ORG_ID => V_JOURNAL_ORG_ID,
                               P_PROCESS_STATUS => 'POSTED');
          Exception
            When others then
              dbms_output.put_line('INSERT RETENTION LIABILITY RECORD  ' ||
                                   sqlerrm);

              INSERT_PROCESS_LOG(P_PROCESS_ID     => v_JE_DTL_ID,
                                 P_PROCESS_CODE   => 'AR_Invoice_LIABILITY_RENTENTION_DTL1' ||
                                                     V_JOURNAL_GRN_NUM,
                                 P_PROCESS_ERROR  => sqlerrm,
                                 P_PROCESS_DATE   => SYSDATE,
                                 P_PROCESS_ORG_ID => V_JOURNAL_ORG_ID,
                                 P_PROCESS_STATUS => 'ERROR');
          End;

          BEGIN
            GL_INSERT_JR_DTL(P_JE_DTL_ID           => v_JE_DTL_ID,
                             P_JE_HDR_ID           => v_JE_HDR_ID,
                             P_JE_ACCT_CODE_ID     => V_LIABILITY_ret_ACCOUNT,
                             P_JE_PARTICULARS      => V_DESC,
                             P_JE_CREDIT_DEBIT     => V_CREDIT_FLAG,
                             P_JE_AMOUNT_CR        => (V_JOURNAL_RENTENON_AMOUNT),
                             P_JE_ACCOUNTED_AMT_CR => (V_JOURNAL_RENTENON_AMOUNT) *
                                                      V_EXCHANGE_RATE_VALUE,
                             P_JE_SEGMENT_ID_1     => NULL,
                             P_JE_SEGMENT_ID_2     => NULL,
                             P_JE_SEGMENT_ID_3     => NULL,
                             P_JE_SEGMENT_ID_4     => NULL,
                             P_JE_SEGMENT_ID_5     => NULL,
                             P_JE_SEGMENT_ID_6     => NULL,
                             P_JE_AMOUNT_DR        => NULL,
                             P_JE_ACCOUNTED_AMT_DR => NULL);

            INSERT_PROCESS_LOG(P_PROCESS_ID     => v_JE_DTL_ID,
                               P_PROCESS_CODE   => 'AR_Invoice_LIABILITY_RENTENTION_DTL2' ||
                                                   V_JOURNAL_GRN_NUM,
                               P_PROCESS_ERROR  => sqlerrm,
                               P_PROCESS_DATE   => SYSDATE,
                               P_PROCESS_ORG_ID => V_JOURNAL_ORG_ID,
                               P_PROCESS_STATUS => 'POSTED');
          Exception
            When others then
              dbms_output.put_line('INSERT RETENTION LIABILITY RECORD  ' ||
                                   sqlerrm);

              INSERT_PROCESS_LOG(P_PROCESS_ID     => v_JE_DTL_ID,
                                 P_PROCESS_CODE   => 'AR_Invoice_LIABILITY_RENTENTION_DTL2' ||
                                                     V_JOURNAL_GRN_NUM,
                                 P_PROCESS_ERROR  => sqlerrm,
                                 P_PROCESS_DATE   => SYSDATE,
                                 P_PROCESS_ORG_ID => V_JOURNAL_ORG_ID,
                                 P_PROCESS_STATUS => 'ERROR');
          End;

        ELSE
          --INSERT THE LIABILITY RECORD
          BEGIN
            GL_INSERT_JR_DTL(P_JE_DTL_ID           => v_JE_DTL_ID,
                             P_JE_HDR_ID           => v_JE_HDR_ID,
                             P_JE_ACCT_CODE_ID     => V_LIABILITY_ACCOUNT,
                             P_JE_PARTICULARS      => V_DESC,
                             P_JE_CREDIT_DEBIT     => V_CREDIT_FLAG,
                             P_JE_AMOUNT_CR        => V_LIABILITY_AMOUNT,
                             P_JE_ACCOUNTED_AMT_CR => V_LIABILITY_AMOUNT *
                                                      V_EXCHANGE_RATE_VALUE,
                             P_JE_SEGMENT_ID_1     => NULL,
                             P_JE_SEGMENT_ID_2     => NULL,
                             P_JE_SEGMENT_ID_3     => NULL,
                             P_JE_SEGMENT_ID_4     => NULL,
                             P_JE_SEGMENT_ID_5     => NULL,
                             P_JE_SEGMENT_ID_6     => NULL,
                             P_JE_AMOUNT_DR        => NULL,
                             P_JE_ACCOUNTED_AMT_DR => NULL);

            INSERT_PROCESS_LOG(P_PROCESS_ID     => v_JE_DTL_ID,
                               P_PROCESS_CODE   => 'AR_Invoice_LIABILITY_DTL' ||
                                                   V_JOURNAL_GRN_NUM,
                               P_PROCESS_ERROR  => sqlerrm,
                               P_PROCESS_DATE   => SYSDATE,
                               P_PROCESS_ORG_ID => V_JOURNAL_ORG_ID,
                               P_PROCESS_STATUS => 'POSTED');
          Exception
            When others then
              dbms_output.put_line('INSERT LIABILITY RECORD  ' || sqlerrm);

              INSERT_PROCESS_LOG(P_PROCESS_ID     => v_JE_DTL_ID,
                                 P_PROCESS_CODE   => 'AR_Invoice_LIABILITY_DTL' ||
                                                     V_JOURNAL_GRN_NUM,
                                 P_PROCESS_ERROR  => sqlerrm,
                                 P_PROCESS_DATE   => SYSDATE,
                                 P_PROCESS_ORG_ID => V_JOURNAL_ORG_ID,
                                 P_PROCESS_STATUS => 'ERROR');
          End;

        end if;

        ---INSERT THE MATERIAL ACCOUNT RECORDS
        BEGIN
          FOR F_GET_AP_DATA IN C_GET_AP_DATA LOOP
            GL_INSERT_JR_DTL(P_JE_DTL_ID           => F_GET_AP_DATA.JE_DTL_ID,
                             P_JE_HDR_ID           => v_JE_HDR_ID,
                             P_JE_ACCT_CODE_ID     => F_GET_AP_DATA.MATRIAL_ACCOUNT,
                             P_JE_PARTICULARS      => F_GET_AP_DATA.INV_DESCRIPTION,
                             P_JE_CREDIT_DEBIT     => V_DEBIT_FLAG,
                             P_JE_AMOUNT_CR        => NULL,
                             P_JE_ACCOUNTED_AMT_CR => NULL,
                             P_JE_SEGMENT_ID_1     => NULL,
                             P_JE_SEGMENT_ID_2     => NULL,
                             P_JE_SEGMENT_ID_3     => NULL,
                             P_JE_SEGMENT_ID_4     => NULL,
                             P_JE_SEGMENT_ID_5     => NULL,
                             P_JE_SEGMENT_ID_6     => NULL,
                             P_JE_AMOUNT_DR        => F_GET_AP_DATA.MATRIAL_AMOUNT,
                             P_JE_ACCOUNTED_AMT_DR => F_GET_AP_DATA.MATRIAL_AMOUNT *
                                                      V_EXCHANGE_RATE_VALUE);
          END LOOP;
          INSERT_PROCESS_LOG(P_PROCESS_ID     => v_JE_DTL_ID,
                             P_PROCESS_CODE   => 'AR_Invoice_material_account' ||
                                                 V_JOURNAL_GRN_NUM,
                             P_PROCESS_ERROR  => sqlerrm,
                             P_PROCESS_DATE   => SYSDATE,
                             P_PROCESS_ORG_ID => V_JOURNAL_ORG_ID,
                             P_PROCESS_STATUS => 'POSTED');
        Exception
          When others then
            dbms_output.put_line('INSERT MATERIAL ACCOUNT  RECORD  ' ||
                                 sqlerrm);
            INSERT_PROCESS_LOG(P_PROCESS_ID     => v_JE_DTL_ID,
                               P_PROCESS_CODE   => 'AR_Invoice_material_account' ||
                                                   V_JOURNAL_GRN_NUM,
                               P_PROCESS_ERROR  => sqlerrm,
                               P_PROCESS_DATE   => SYSDATE,
                               P_PROCESS_ORG_ID => V_JOURNAL_ORG_ID,
                               P_PROCESS_STATUS => 'ERROR');
        End;

      end if;
      COMMIT;
    ELSE
      INSERT_PROCESS_LOG(P_PROCESS_ID     => 'ERROR',
                         P_PROCESS_CODE   => 'AR_Invoice_POSTING',
                         P_PROCESS_ERROR  => sqlerrm,
                         P_PROCESS_DATE   => SYSDATE,
                         P_PROCESS_ORG_ID => V_JOURNAL_ORG_ID,
                         P_PROCESS_STATUS => 'ERROR');
    end if;

  END AR_INVOICE_POSTING;

  ---AR RECEIPTS POSTING
  procedure AR_RECEIPTS_POSTING(P_PAYMENT_ID VARCHAR2, P_ORG_ID VARCHAR2) is

    --GET THE ITEM,payment DATA
    cursor C_GET_AP_CASH_AC is
      SELECT 'GL_007_D' as JE_DTL_ID,
             'AR PAYMENT-CASH AC' AS INV_DESCRIPTION,
             (select so.ar_default_cash_acct_code
                from ssm_system_options so
               where so.org_id = r.cust_org_id
                 AND ROWNUM = 1) AS MATRIAL_ACCOUNT,
             R.PAYMENT_AMT AS MATRIAL_AMOUNT,
             (select sc.vendor_retention_acct_id
                from supplier_customers sc
               where sc.vendor_id = r.cust_vendor_id
                 and sc.workflow_completion_status = 1
                 and rownum = 1) as retention_acct_id
        FROM Om_Cust_Payment_hdr R
       WHERE R.Cust_Pay_Id = P_PAYMENT_ID
         and r.cust_org_id = P_ORG_ID
         AND R.ENABLED_FLAG = '1'
         AND R.WORKFLOW_COMPLETION_STATUS = '1';

    cursor C_GET_AP_BANK_AC is
      SELECT 'GL_007_D' as JE_DTL_ID,
             'AR PAYMENT-BANK AC' AS INV_DESCRIPTION,
             (select CA.GL_ACCOUNT
                from CA_BANK_ACCOUNTS CA
               where CA.BANK_ID = R.CUST_PAY_BANK_ID
                 AND CA.ORG_ID = R.cust_org_id
                 AND ROWNUM = 1) AS MATRIAL_ACCOUNT,
             R.PAYMENT_AMT AS MATRIAL_AMOUNT
        FROM Om_Cust_Payment_hdr R
       WHERE R.Cust_Pay_Id = P_PAYMENT_ID
         and r.cust_org_id = P_ORG_ID
         AND R.ENABLED_FLAG = '1'
         AND R.WORKFLOW_COMPLETION_STATUS = '1';

    --- GET RENTION AMOUNT
    cursor C_GET_REN_AMT is
      SELECT NVL(DD.PAY_RETENTIION_AMT, 0) AS PAY_RETENTION_AMT,
             (select sc.vendor_retention_acct_id
                from supplier_customers sc
               where sc.vendor_id = r.cust_vendor_id
                 and sc.workflow_completion_status = 1
                 and rownum = 1) as retention_acct_id
        FROM OM_CUST_PAYMENT_HDR R, OM_CUST_PAYMENT_DTL DD
       WHERE r.cust_org_id = P_ORG_ID
         and R.CUST_PAY_ID = P_PAYMENT_ID
         AND R.CUST_PAY_ID = DD.CUST_PAY_ID
         AND R.ENABLED_FLAG = '1'
         AND R.WORKFLOW_COMPLETION_STATUS = '1';
    -------------------------------------------------------------------------------------------------------------------
    --VARIABLE DECLARATION
    V_LIABILITY_AMOUNT      NUMBER;
    V_LIABILITY_ACCOUNT     VARCHAR(500);
    V_LIABILITY_RET_ACCOUNT VARCHAR(500);
    v_JE_HDR_ID             VARCHAR(500);
    v_JE_HDR_ID_VAL         VARCHAR(500);
    v_JN_NUMBER_ID          VARCHAR(500);
    v_JE_DTL_ID             VARCHAR(500);
    V_JOURNAL_TYPE          VARCHAR(500);
    V_JOURNAL_SOURCE        VARCHAR(500);
    V_JOURNAL_DATE          DATE;
    V_JOURNAL_DR_AMOUNT     NUMBER;
    V_JOURNAL_ORG_ID        VARCHAR(500);
    V_JOURNAL_GRN_NUM       VARCHAR(500);
    V_JOURNAL_GL_SEGMENT_ID VARCHAR(500);
    V_DESC                  VARCHAR(1500);
    V_COMP_CURRENCY         VARCHAR(500);
    V_PERIOD_ID             VARCHAR(500);
    V_EXCHANGE_RATE_VALUE   NUMBER;
    V_CREDIT_FLAG           VARCHAR(500);
    V_DEBIT_FLAG            VARCHAR(500);
    V_JOURNAL_RENTN_AMOUNT  NUMBER;
    V_REN_AMOUNT            NUMBER;
    V_SUPPLIER_NUMBER       VARCHAR(500);
    V_RECORD_DATE           DATE;
    v_inv_num               VARCHAR(500);
  BEGIN

    V_JOURNAL_SOURCE := 'AP';
    V_JOURNAL_DATE   := TRUNC(SYSDATE);
    for invd in (select iid.attribute1, iih.om_inv_num
                   from Om_Cust_Payment_Dtl ipd,
                        OM_CUST_INVOICE_DTL iid,
                        OM_CUST_INVOICE_HDR iih
                  where ipd.CUST_PAY_ID = P_PAYMENT_ID
                    and ipd.CUST_INV_ID = iid.OM_INV_ID
                    and ipd.CUST_INV_ID = iih.OM_INV_ID) loop

      V_DESC    := V_DESC || ' ' || invd.attribute1;
      v_inv_num := v_inv_num || ',' || invd.om_inv_num;

    end loop;
    --GET THE INV DATA JL HEADER
    BEGIN
      SELECT distinct 'GL_007_M' as JE_HDR_ID,
                      'GL_007_JN' as JN_NUMBER_ID,
                      r.GLOBAL_SEGMENT_ID AS COMP_GLOBAL_SEGMENT_ID,
                      R.CUST_ORG_ID AS ORG_ID,
                      R.CUST_PAY_ID,
                      R.PAYMENT_AMT,
                      r.PAYMENT_CURRENCY as org_currency,
                      R.EXC_RATE_VALUE,
                      upper(r.CUST_PAY_MODE) as inv_type,
                      r.pay_retention_amt,
                      r.pay_date,
                      (select sc.vendor_retention_acct_id
                         from supplier_customers sc
                        where sc.vendor_id = r.cust_vendor_id
                          and sc.workflow_completion_status = 1
                          and rownum = 1) as retention_acct_id

        into v_JE_HDR_ID_VAL,
             v_JN_NUMBER_ID,
             V_JOURNAL_GL_SEGMENT_ID,
             V_JOURNAL_ORG_ID,
             V_JOURNAL_GRN_NUM,
             V_JOURNAL_DR_AMOUNT,
             V_COMP_CURRENCY,
             V_EXCHANGE_RATE_VALUE,
             V_JOURNAL_TYPE,
             V_JOURNAL_RENTN_AMOUNT,
             V_RECORD_DATE,
             V_LIABILITY_RET_ACCOUNT
        FROM om_cust_payment_hdr R
       WHERE R.ENABLED_FLAG = '1'
         AND R.WORKFLOW_COMPLETION_STATUS = '1'
         and R.CUST_ORG_ID = P_ORG_ID
         AND R.CUST_PAY_ID = P_PAYMENT_ID
         AND R.ENABLED_FLAG = 1;
    Exception
      When others then
        dbms_output.put_line('INV DATA JL HEADER ' || sqlerrm);
    End;

    --GET LIABILITY ACC CODE AND AMOUNT
    BEGIN
      SELECT 'GL_007_D' as JE_DTL_ID,
             r.PAYMENT_AMT AS LIABILITY_AMOUNT,
             R.PAY_RETENTION_AMT,
             (select (case
                       when SC.AP_LIABILITY_ACCOUNT is null then
                        ss.ap_liability_acct
                       else
                        SC.AP_LIABILITY_ACCOUNT
                     end)
                from ssm_system_options ss
               where rownum = 1) AS LIABILITY_ACCOUNT,
             'AR PAYMENT-SUPPLIER AC' AS REMARKS,
             sc.vendor_name
        INTO v_JE_DTL_ID,
             V_LIABILITY_AMOUNT,
             V_REN_AMOUNT,
             V_LIABILITY_ACCOUNT,
             V_DESC,
             V_SUPPLIER_NUMBER
        FROM om_cust_payment_hdr R, SUPPLIER_CUSTOMERS SC
       WHERE SC.VENDOR_ID = R.CUST_VENDOR_ID
         AND R.ENABLED_FLAG = 1
         and sc.enabled_flag = '1'
         and sc.workflow_completion_status = '1'
         AND R.WORKFLOW_COMPLETION_STATUS = '1'
         and R.CUST_ORG_ID = P_ORG_ID
         AND R.CUST_PAY_ID = P_PAYMENT_ID;
    Exception
      When others then
        dbms_output.put_line('LIABILITY ACC CODE ' || sqlerrm);
    End;

    --GET CAL PERIOD
    BEGIN
      if V_RECORD_DATE is not null then
        select gg.period_id
          INTO V_PERIOD_ID
          from gl_comp_acct_period_dtl gg
         where gg.enabled_flag = '1'
           and gg.workflow_completion_status = '1'
           and gg.PERIOD_STATUS = 'OPEN'
           and gg.COMP_ID = V_JOURNAL_ORG_ID
           and (trunc(sysdate)) between gg.period_from_dt and
               gg.period_to_dt
         order by gg.PERIOD_NAME asc;
      end if;
    Exception
      When others then
        dbms_output.put_line('Cal period ' || sqlerrm);
    End;

    v_JE_HDR_ID := ssm.get_next_sequence(v_JE_HDR_ID_VAL, 'EN');
    if (sqlerrm = 'ORA-0000: normal, successful completion') then
      --INSERT THE JOURNAL HEADER RECORD FOR INV
      BEGIN
        GL_INSERT(P_JE_HDR_ID              => v_JE_HDR_ID,
                  p_JE_COMP_ID             => V_JOURNAL_ORG_ID,
                  P_JE_DATE                => V_JOURNAL_DATE,
                  P_JE_GLOBAL_SEGMENT_ID   => V_JOURNAL_GL_SEGMENT_ID,
                  P_JE_NUMBER              => v_JN_NUMBER_ID,
                  P_JE_TYPE                => V_JOURNAL_TYPE,
                  P_JE_CURRENCY_ID         => V_COMP_CURRENCY,
                  P_JE_DESC                => 'Payment for ' ||
                                              V_SUPPLIER_NUMBER ||
                                              ', Payment Number:' ||
                                              V_JOURNAL_GRN_NUM ||
                                              ', Payment Amount:' ||
                                              V_JOURNAL_DR_AMOUNT,
                  P_JE_CURRENCY_RATE_ID    => NULL,
                  P_JE_STATUS              => 'OPEN',
                  P_JE_CURR_VALUE          => NULL,
                  P_JE_PERIOD_ID           => V_PERIOD_ID,
                  P_JE_REFERENCE           => 'AR:Payment Number:' ||
                                              V_JOURNAL_GRN_NUM,
                  P_JE_EXCHANGE_RATE_VALUE => V_EXCHANGE_RATE_VALUE,
                  P_JE_TOT_DR_AMOUNT       => V_JOURNAL_DR_AMOUNT,
                  P_JE_TOT_CR_AMOUNT       => V_JOURNAL_DR_AMOUNT,
                  P_JE_RECURRING_END_DATE  => NULL,
                  P_JE_RECURRING_FREQUENCY => NULL,
                  P_JE_SOURCE              => V_JOURNAL_SOURCE,
                  P_JOURNAL_REVERSED       => NULL,
                  P_ACTUAL_JOURNAL_ID      => NULL,
                  P_MODULE                 => 'AR');

        INSERT_PROCESS_LOG(P_PROCESS_ID => v_JE_HDR_ID,

                           P_PROCESS_CODE   => 'AR_Payment_HDR' ||
                                               V_JOURNAL_GRN_NUM,
                           P_PROCESS_ERROR  => sqlerrm,
                           P_PROCESS_DATE   => SYSDATE,
                           P_PROCESS_ORG_ID => V_JOURNAL_ORG_ID,
                           P_PROCESS_STATUS => 'POSTED');
      Exception
        When others then
          dbms_output.put_line('INSERT JOURNAL HEADER  ' || sqlerrm);

          INSERT_PROCESS_LOG(P_PROCESS_ID     => v_JE_HDR_ID,
                             P_PROCESS_CODE   => 'AR_Payment_DTL_2367' ||
                                                 V_JOURNAL_GRN_NUM,
                             P_PROCESS_ERROR  => sqlerrm,
                             P_PROCESS_DATE   => SYSDATE,
                             P_PROCESS_ORG_ID => V_JOURNAL_ORG_ID,
                             P_PROCESS_STATUS => 'ERROR');
      End;

      V_CREDIT_FLAG := 'Cr';
      V_DEBIT_FLAG  := 'Dr';

      FOR F_GET_REN_AMT IN C_GET_REN_AMT LOOP
        V_REN_AMOUNT := F_GET_REN_AMT.PAY_RETENTION_AMT;
        IF V_REN_AMOUNT > 0 THEN

          BEGIN
            GL_INSERT_JR_DTL(P_JE_DTL_ID           => v_JE_DTL_ID,
                             P_JE_HDR_ID           => v_JE_HDR_ID,
                             P_JE_ACCT_CODE_ID     => V_LIABILITY_ACCOUNT,
                             P_JE_PARTICULARS      => V_DESC,
                             P_JE_CREDIT_DEBIT     => V_DEBIT_FLAG,
                             P_JE_AMOUNT_CR        => NULL,
                             P_JE_ACCOUNTED_AMT_CR => NULL,
                             P_JE_SEGMENT_ID_1     => NULL,
                             P_JE_SEGMENT_ID_2     => NULL,
                             P_JE_SEGMENT_ID_3     => NULL,
                             P_JE_SEGMENT_ID_4     => NULL,
                             P_JE_SEGMENT_ID_5     => NULL,
                             P_JE_SEGMENT_ID_6     => NULL,
                             P_JE_AMOUNT_DR        => (V_LIABILITY_AMOUNT -
                                                      F_GET_REN_AMT.PAY_RETENTION_AMT),
                             P_JE_ACCOUNTED_AMT_DR => (V_LIABILITY_AMOUNT -
                                                      F_GET_REN_AMT.PAY_RETENTION_AMT) *
                                                      V_EXCHANGE_RATE_VALUE);

            INSERT_PROCESS_LOG(P_PROCESS_ID     => v_JE_HDR_ID,
                               P_PROCESS_CODE   => 'AR_Payment_dtl1' ||
                                                   V_JOURNAL_GRN_NUM,
                               P_PROCESS_ERROR  => sqlerrm,
                               P_PROCESS_DATE   => SYSDATE,
                               P_PROCESS_ORG_ID => V_JOURNAL_ORG_ID,
                               P_PROCESS_STATUS => 'POSTED');
          Exception
            When others then
              dbms_output.put_line('INSERT LIABILITY RECORD  ' || sqlerrm);

              INSERT_PROCESS_LOG(P_PROCESS_ID     => v_JE_HDR_ID,
                                 P_PROCESS_CODE   => 'AR_Payment_dtl1' ||
                                                     V_JOURNAL_GRN_NUM,
                                 P_PROCESS_ERROR  => sqlerrm,
                                 P_PROCESS_DATE   => SYSDATE,
                                 P_PROCESS_ORG_ID => V_JOURNAL_ORG_ID,
                                 P_PROCESS_STATUS => 'ERROR');
          End;
          BEGIN
            GL_INSERT_JR_DTL(P_JE_DTL_ID           => v_JE_DTL_ID,
                             P_JE_HDR_ID           => v_JE_HDR_ID,
                             P_JE_ACCT_CODE_ID     => F_GET_REN_AMT.RETENTION_ACCT_ID,
                             P_JE_PARTICULARS      => V_DESC,
                             P_JE_CREDIT_DEBIT     => V_DEBIT_FLAG,
                             P_JE_AMOUNT_CR        => NULL,
                             P_JE_ACCOUNTED_AMT_CR => NULL,
                             P_JE_SEGMENT_ID_1     => NULL,
                             P_JE_SEGMENT_ID_2     => NULL,
                             P_JE_SEGMENT_ID_3     => NULL,
                             P_JE_SEGMENT_ID_4     => NULL,
                             P_JE_SEGMENT_ID_5     => NULL,
                             P_JE_SEGMENT_ID_6     => NULL,
                             P_JE_AMOUNT_DR        => (F_GET_REN_AMT.PAY_RETENTION_AMT),
                             P_JE_ACCOUNTED_AMT_DR => (F_GET_REN_AMT.PAY_RETENTION_AMT) *
                                                      V_EXCHANGE_RATE_VALUE);

            INSERT_PROCESS_LOG(P_PROCESS_ID => v_JE_HDR_ID,

                               P_PROCESS_CODE   => 'AR_Payment_dtl2' ||
                                                   V_JOURNAL_GRN_NUM,
                               P_PROCESS_ERROR  => sqlerrm,
                               P_PROCESS_DATE   => SYSDATE,
                               P_PROCESS_ORG_ID => V_JOURNAL_ORG_ID,
                               P_PROCESS_STATUS => 'POSTED');
          Exception
            When others then
              dbms_output.put_line('INSERT LIABILITY RECORD  ' || sqlerrm);

              INSERT_PROCESS_LOG(P_PROCESS_ID     => v_JE_HDR_ID,
                                 P_PROCESS_CODE   => 'AR_Payment_dtl2' ||
                                                     V_JOURNAL_GRN_NUM,
                                 P_PROCESS_ERROR  => sqlerrm,
                                 P_PROCESS_DATE   => SYSDATE,
                                 P_PROCESS_ORG_ID => V_JOURNAL_ORG_ID,
                                 P_PROCESS_STATUS => 'ERROR');
          End;
        ELSE
          --INSERT THE LIABILITY RECORD
          BEGIN
            GL_INSERT_JR_DTL(P_JE_DTL_ID           => v_JE_DTL_ID,
                             P_JE_HDR_ID           => v_JE_HDR_ID,
                             P_JE_ACCT_CODE_ID     => V_LIABILITY_ACCOUNT,
                             P_JE_PARTICULARS      => V_DESC,
                             P_JE_CREDIT_DEBIT     => V_DEBIT_FLAG,
                             P_JE_AMOUNT_CR        => NULL,
                             P_JE_ACCOUNTED_AMT_CR => NULL,
                             P_JE_SEGMENT_ID_1     => NULL,
                             P_JE_SEGMENT_ID_2     => NULL,
                             P_JE_SEGMENT_ID_3     => NULL,
                             P_JE_SEGMENT_ID_4     => NULL,
                             P_JE_SEGMENT_ID_5     => NULL,
                             P_JE_SEGMENT_ID_6     => NULL,
                             P_JE_AMOUNT_DR        => V_LIABILITY_AMOUNT,
                             P_JE_ACCOUNTED_AMT_DR => V_LIABILITY_AMOUNT *
                                                      V_EXCHANGE_RATE_VALUE);

            INSERT_PROCESS_LOG(P_PROCESS_ID     => v_JE_HDR_ID,
                               P_PROCESS_CODE   => 'AR_Payment_dtl1' ||
                                                   V_JOURNAL_GRN_NUM,
                               P_PROCESS_ERROR  => sqlerrm,
                               P_PROCESS_DATE   => SYSDATE,
                               P_PROCESS_ORG_ID => V_JOURNAL_ORG_ID,
                               P_PROCESS_STATUS => 'POSTED');
          Exception
            When others then
              dbms_output.put_line('INSERT LIABILITY RECORD  ' || sqlerrm);

              INSERT_PROCESS_LOG(P_PROCESS_ID     => v_JE_HDR_ID,
                                 P_PROCESS_CODE   => 'AR_Payment_dtl1' ||
                                                     V_JOURNAL_GRN_NUM,
                                 P_PROCESS_ERROR  => sqlerrm,
                                 P_PROCESS_DATE   => SYSDATE,
                                 P_PROCESS_ORG_ID => V_JOURNAL_ORG_ID,
                                 P_PROCESS_STATUS => 'ERROR');
          End;
        END IF;
      END LOOP;

      ---INSERT THE MATERIAL ACCOUNT RECORDS

      BEGIN
        select upper(R.pay_mode) as paymode
          into V_JOURNAL_TYPE
          from inv_payments_hdr R
         WHERE R.Org_Id = P_ORG_ID
           AND R.PAY_ID = P_PAYMENT_ID;
      Exception
        When others then
          dbms_output.put_line('PAYMENT TYPE ' || sqlerrm);
      End;

      if V_JOURNAL_TYPE = 'CASH' THEN
        BEGIN
          FOR F_GET_AP_DATA IN C_GET_AP_CASH_AC LOOP
            GL_INSERT_JR_DTL(P_JE_DTL_ID           => F_GET_AP_DATA.JE_DTL_ID,
                             P_JE_HDR_ID           => v_JE_HDR_ID,
                             P_JE_ACCT_CODE_ID     => F_GET_AP_DATA.MATRIAL_ACCOUNT,
                             P_JE_PARTICULARS      => F_GET_AP_DATA.INV_DESCRIPTION,
                             P_JE_CREDIT_DEBIT     => V_CREDIT_FLAG,
                             P_JE_AMOUNT_CR        => F_GET_AP_DATA.MATRIAL_AMOUNT,
                             P_JE_ACCOUNTED_AMT_CR => F_GET_AP_DATA.MATRIAL_AMOUNT *
                                                      V_EXCHANGE_RATE_VALUE,
                             P_JE_SEGMENT_ID_1     => NULL,
                             P_JE_SEGMENT_ID_2     => NULL,
                             P_JE_SEGMENT_ID_3     => NULL,
                             P_JE_SEGMENT_ID_4     => NULL,
                             P_JE_SEGMENT_ID_5     => NULL,
                             P_JE_SEGMENT_ID_6     => NULL,
                             P_JE_AMOUNT_DR        => NULL,
                             P_JE_ACCOUNTED_AMT_DR => NULL);

            INSERT_PROCESS_LOG(P_PROCESS_ID => v_JE_HDR_ID,

                               P_PROCESS_CODE   => 'AR_Payment CASH _MATERIAL dtl1' ||
                                                   V_JOURNAL_GRN_NUM,
                               P_PROCESS_ERROR  => sqlerrm,
                               P_PROCESS_DATE   => SYSDATE,
                               P_PROCESS_ORG_ID => V_JOURNAL_ORG_ID,
                               P_PROCESS_STATUS => 'POSTED');
          END LOOP;
        Exception
          When others then
            dbms_output.put_line('INSERT MATERIAL CASH ACCOUNT RECORD  ' ||
                                 sqlerrm);

            INSERT_PROCESS_LOG(P_PROCESS_ID     => v_JE_HDR_ID,
                               P_PROCESS_CODE   => 'AR_Payment_ CASH MATERIAL dtl1' ||
                                                   V_JOURNAL_GRN_NUM,
                               P_PROCESS_ERROR  => sqlerrm,
                               P_PROCESS_DATE   => SYSDATE,
                               P_PROCESS_ORG_ID => V_JOURNAL_ORG_ID,
                               P_PROCESS_STATUS => 'ERROR');
        End;
      END IF;
      if V_JOURNAL_TYPE <> 'CASH' THEN
        BEGIN

          FOR F_GET_AP_DATA IN C_GET_AP_BANK_AC LOOP
            GL_INSERT_JR_DTL(P_JE_DTL_ID           => F_GET_AP_DATA.JE_DTL_ID,
                             P_JE_HDR_ID           => v_JE_HDR_ID,
                             P_JE_ACCT_CODE_ID     => F_GET_AP_DATA.MATRIAL_ACCOUNT,
                             P_JE_PARTICULARS      => F_GET_AP_DATA.INV_DESCRIPTION,
                             P_JE_CREDIT_DEBIT     => V_CREDIT_FLAG,
                             P_JE_AMOUNT_CR        => F_GET_AP_DATA.MATRIAL_AMOUNT,
                             P_JE_ACCOUNTED_AMT_CR => F_GET_AP_DATA.MATRIAL_AMOUNT *
                                                      V_EXCHANGE_RATE_VALUE,
                             P_JE_SEGMENT_ID_1     => NULL,
                             P_JE_SEGMENT_ID_2     => NULL,
                             P_JE_SEGMENT_ID_3     => NULL,
                             P_JE_SEGMENT_ID_4     => NULL,
                             P_JE_SEGMENT_ID_5     => NULL,
                             P_JE_SEGMENT_ID_6     => NULL,
                             P_JE_AMOUNT_DR        => NULL,
                             P_JE_ACCOUNTED_AMT_DR => NULL);

            INSERT_PROCESS_LOG(P_PROCESS_ID     => v_JE_HDR_ID,
                               P_PROCESS_CODE   => 'AR_Payment_ WITHOUT CASH MATERIAL dtl1' ||
                                                   V_JOURNAL_GRN_NUM,
                               P_PROCESS_ERROR  => sqlerrm,
                               P_PROCESS_DATE   => SYSDATE,
                               P_PROCESS_ORG_ID => V_JOURNAL_ORG_ID,
                               P_PROCESS_STATUS => 'POSTED');
          END LOOP;
        Exception
          When others then
            dbms_output.put_line('INSERT MATERIAL BANK ACCOUNT RECORD  ' ||
                                 sqlerrm);

            INSERT_PROCESS_LOG(P_PROCESS_ID     => v_JE_HDR_ID,
                               P_PROCESS_CODE   => 'AR_Payment_ WITHOUT CASH MATERIAL dtl1' ||
                                                   V_JOURNAL_GRN_NUM,
                               P_PROCESS_ERROR  => sqlerrm,
                               P_PROCESS_DATE   => SYSDATE,
                               P_PROCESS_ORG_ID => V_JOURNAL_ORG_ID,
                               P_PROCESS_STATUS => 'POSTED');
        End;
      END IF;

      COMMIT;
    ELSE
      INSERT_PROCESS_LOG(P_PROCESS_ID => 'ERROR',

                         P_PROCESS_CODE   => 'AR_Payment_HDR_' ||
                                             V_JOURNAL_GRN_NUM,
                         P_PROCESS_ERROR  => sqlerrm,
                         P_PROCESS_DATE   => SYSDATE,
                         P_PROCESS_ORG_ID => V_JOURNAL_ORG_ID,
                         P_PROCESS_STATUS => 'ERROR');
    END IF;
  END AR_RECEIPTS_POSTING;

  procedure AR_OTHER_COST_POSTING(P_OM_DC_ID VARCHAR2, P_ORG_ID VARCHAR2) is

    cursor C_GET_Ar_DATA is
      SELECT 'GL_007_D' as JE_DTL_ID,
             R.OM_DC_ID,
             --R.ORG_ID,
             R.OM_SHIP_NUM,
             --  R.VENDOR_ID,
             D.OM_REMARKS,
             D.OM_ITEM_ID,
             D.OM_SHIPPED_QTY,
             (SELECT II.INV_MAT_ACCT_CODE
                FROM INV_ITEM_MASTER II
               WHERE II.ITEM_ID = D.OM_ITEM_ID) AS MATRIAL_ACCOUNT,

             (SELECT NVL(I.ITEM_UNIT_PRICE, 0) * NVL(D.OM_SHIPPED_QTY, 0)
                FROM INV_ITEM_MASTER I
               WHERE I.ITEM_ID = D.OM_ITEM_ID) AS MATRIAL_AMOUNT --DR
        FROM OM_DC_HDR R, OM_DC_DTL D, OM_DC_OTHER_COST_HDR ic
       WHERE R.OM_DC_ID = D.OM_DC_ID
         and ic.om_dc_id = r.om_dc_id
         AND IC.OM_OTHER_COST_ID = P_OM_DC_ID
         AND R.ENABLED_FLAG = '1'
         AND R.WORKFLOW_COMPLETION_STATUS = '1';

    V_LIABILITY_AMOUNT      NUMBER;
    V_LIABILITY_ACCOUNT     VARCHAR(500);
    v_JE_HDR_ID             VARCHAR(500);
    v_JE_HDR_ID_VAL         VARCHAR(500);
    v_JN_NUMBER_ID          VARCHAR(500);
    v_JE_DTL_ID             VARCHAR(500);
    V_JOURNAL_TYPE          VARCHAR(500);
    V_JOURNAL_SOURCE        VARCHAR(500);
    V_JOURNAL_DATE          DATE;
    V_JOURNAL_DR_AMOUNT     NUMBER;
    V_JOURNAL_ORG_ID        VARCHAR(500);
    V_JOURNAL_GRN_NUM       VARCHAR(500);
    V_JOURNAL_GL_SEGMENT_ID VARCHAR(500);
    V_DESC                  VARCHAR(1500);
    V_COMP_CURRENCY         VARCHAR(500);
    V_PERIOD_ID             VARCHAR(500);
    V_EXCHANGE_RATE_VALUE   NUMBER;
    V_SC                    VARCHAR(500);
    V_RC_DATE               DATE;
  BEGIN

    V_JOURNAL_TYPE   := 'STANDARD';
    V_JOURNAL_SOURCE := 'AR';
    V_JOURNAL_DATE   := TRUNC(SYSDATE);

    BEGIN
      SELECT 'GL_007_M' as JE_HDR_ID,
             'GL_007_JN' as JN_NUMBER_ID,
             (SELECT sv.segment_value_id
                FROM GL_COMPANIES_HDR  CH,
                     GL_SEGMENTS       s,
                     gl_segment_values sv
               WHERE s.segment_id = sv.segment_id
                 and ch.comp_global_segment_id = s.segment_id
                 AND CH.COMP_ID = P_ORG_ID
                 AND ROWNUM = 1) AS COMP_GLOBAL_SEGMENT_ID,
             CO.OM_ORDER_ORG_ID,
             R.OM_SHIP_NUM as GRN_NUM,
             (SELECT NVL(I.ITEM_UNIT_PRICE, 0) * NVL(D.OM_SHIPPED_QTY, 0)
                FROM INV_ITEM_MASTER I
               WHERE I.ITEM_ID = D.OM_ITEM_ID
                 AND ROWNUM = 1) AS MATRIAL_AMOUNT, --DR
             (select cd.comp_base_currency
                from gl_companies_dtl cd
               where cd.comp_id = ch.comp_id
                 and cd.comp_id = P_ORG_ID
                 AND ROWNUM = 1) as org_currency,
             r.om_dc_date
        into v_JE_HDR_ID_VAL,
             v_JN_NUMBER_ID,
             V_JOURNAL_GL_SEGMENT_ID,
             V_JOURNAL_ORG_ID,
             V_JOURNAL_GRN_NUM,
             V_JOURNAL_DR_AMOUNT,
             V_COMP_CURRENCY,
             V_RC_DATE
        FROM OM_DC_HDR             R,
             OM_DC_DTL             D,
             GL_COMPANIES_HDR      ch,
             OM_DC_OTHER_COST_HDR  IC,
             OM_CUSTOMER_ORDER_HDR CO
       WHERE R.OM_DC_ID = D.OM_DC_ID
         AND IC.OM_DC_ID = R.OM_DC_ID
         and ch.comp_id = CO.OM_ORDER_ORG_ID
         AND CO.OM_ORDER_ID = IC.OM_DC_ORDER_ID
         AND R.ENABLED_FLAG = '1'
         AND R.WORKFLOW_COMPLETION_STATUS = '1'
         and CH.COMP_ID = P_ORG_ID
         AND IC.OM_OTHER_COST_ID = P_OM_DC_ID
         AND R.ENABLED_FLAG = 1;
    Exception
      When others then
        dbms_output.put_line('GRN DATA JL HEADER ' || sqlerrm);
    End;

    --GET MATERIAL OVERHEAD ACC CODE AND AMOUNT
    BEGIN
      SELECT 'GL_007_D' as JE_DTL_ID,
             SUM(NVL(I.ITEM_UNIT_PRICE, 0) * NVL(D.OM_SHIPPED_QTY, 0)) AS LIABILITY_AMOUNT,
             I.INV_MAT_OVERHEAD_ACCT_CODE AS MATIRIAL_OVERHEAD_ACCT_CODE,
             D.OM_REMARKS
        INTO v_JE_DTL_ID, V_LIABILITY_AMOUNT, V_LIABILITY_ACCOUNT, V_DESC
        FROM OM_DC_HDR            R,
             OM_DC_OTHER_COST_HDR IC,
             OM_DC_DTL            D,
             INV_ITEM_MASTER      I
       WHERE I.ITEM_ID = D.OM_ITEM_ID
         AND IC.OM_DC_ID = R.OM_DC_ID

         AND R.OM_DC_ID = D.OM_DC_ID
         AND R.ENABLED_FLAG = 1
         AND I.ENABLED_FLAG = 1
         AND R.WORKFLOW_COMPLETION_STATUS = '1'
            --  AND I.ORG_ID = P_ORG_ID
         AND IC.OM_OTHER_COST_ID = P_OM_DC_ID
       GROUP BY I.INV_MAT_OVERHEAD_ACCT_CODE, D.OM_REMARKS;
    Exception
      When others then
        dbms_output.put_line('LIABILITY ACC CODE ' || sqlerrm);
    End;

    --GET CAL PERIOD
    BEGIN
      if V_RC_DATE is not null then
        select gg.period_id
          INTO V_PERIOD_ID
          from gl_comp_acct_period_dtl gg
         where gg.enabled_flag = '1'
           and gg.workflow_completion_status = '1'
           and gg.PERIOD_STATUS = 'OPEN'
           and gg.COMP_ID = V_JOURNAL_ORG_ID
           and (trunc(V_RC_DATE)) between gg.period_from_dt and
               gg.period_to_dt
         order by gg.PERIOD_NAME asc;
      end if;
    Exception
      When others then
        dbms_output.put_line('LIABILITY ACC CODE ' || sqlerrm);
    End;

    --GET  EXCHANGE_RATE_VALUE
    BEGIN
      SELECT SCER.CURRENCY_STD_RATE
        INTO V_EXCHANGE_RATE_VALUE
        FROM SSM_CURRENCY_EXCHANGE_RATES SCER
       WHERE SCER.WORKFLOW_COMPLETION_STATUS = 1
         AND SCER.ENABLED_FLAG = 1
         AND SCER.CURRENCY_ID = V_COMP_CURRENCY
         AND SCER.CURRENCY_RATE_DT = trunc(sysdate)
         AND SCER.CURRENCY_RATE_DT =
             (nvl((select sc.currency_rate_dt
                    FROM SSM_CURRENCY_EXCHANGE_RATES SC
                   where SC.CURRENCY_RATE_DT = trunc(sysdate)
                     and rownum = 1),
                  (select max(scr.CURRENCY_RATE_DT)
                     FROM SSM_CURRENCY_EXCHANGE_RATES SCr)));
    Exception
      When others then
        dbms_output.put_line('LIABILITY ACC CODE ' || sqlerrm);
    End;

    /*v_JE_HDR_ID    := 'GL_007_M';
      v_JN_NUMBER_ID := 'GL_007_JN';
    */
    v_JE_HDR_ID := ssm.get_next_sequence(v_JE_HDR_ID_VAL, 'EN');

    BEGIN
      GL_INSERT(P_JE_HDR_ID => v_JE_HDR_ID,

                p_JE_COMP_ID             => V_JOURNAL_ORG_ID,
                P_JE_DATE                => V_JOURNAL_DATE,
                P_JE_GLOBAL_SEGMENT_ID   => V_JOURNAL_GL_SEGMENT_ID,
                P_JE_NUMBER              => v_JN_NUMBER_ID,
                P_JE_TYPE                => V_JOURNAL_TYPE,
                P_JE_CURRENCY_ID         => V_COMP_CURRENCY,
                P_JE_DESC                => 'ITEM OTHER COST',
                P_JE_CURRENCY_RATE_ID    => NULL,
                P_JE_STATUS              => 'OPEN',
                P_JE_CURR_VALUE          => NULL,
                P_JE_PERIOD_ID           => V_PERIOD_ID,
                P_JE_REFERENCE           => 'AR:GRN Number:' ||
                                            V_JOURNAL_GRN_NUM,
                P_JE_EXCHANGE_RATE_VALUE => V_EXCHANGE_RATE_VALUE,
                P_JE_TOT_DR_AMOUNT       => V_JOURNAL_DR_AMOUNT,
                P_JE_TOT_CR_AMOUNT       => V_JOURNAL_DR_AMOUNT,
                P_JE_RECURRING_END_DATE  => NULL,
                P_JE_RECURRING_FREQUENCY => NULL,
                P_JE_SOURCE              => V_JOURNAL_SOURCE,
                P_JOURNAL_REVERSED       => NULL,
                P_ACTUAL_JOURNAL_ID      => NULL,
                P_MODULE                 => 'AR');
    Exception
      When others then
        dbms_output.put_line('INSERT JOURNAL HEADER  ' || sqlerrm);
    End;

    --INSERT THE MATERIAL OVERHEAD RECORD
    BEGIN
      /* v_JE_DTL_ID := 'GL_007_D';*/

      GL_INSERT_JR_DTL(P_JE_DTL_ID           => v_JE_DTL_ID,
                       P_JE_HDR_ID           => v_JE_HDR_ID,
                       P_JE_ACCT_CODE_ID     => V_LIABILITY_ACCOUNT,
                       P_JE_PARTICULARS      => V_DESC,
                       P_JE_CREDIT_DEBIT     => 'Dr',
                       P_JE_AMOUNT_CR        => V_LIABILITY_AMOUNT,
                       P_JE_ACCOUNTED_AMT_CR => V_LIABILITY_AMOUNT,
                       P_JE_SEGMENT_ID_1     => NULL,
                       P_JE_SEGMENT_ID_2     => NULL,
                       P_JE_SEGMENT_ID_3     => NULL,
                       P_JE_SEGMENT_ID_4     => NULL,
                       P_JE_SEGMENT_ID_5     => NULL,
                       P_JE_SEGMENT_ID_6     => NULL,
                       P_JE_AMOUNT_DR        => NULL,
                       P_JE_ACCOUNTED_AMT_DR => NULL);
    Exception
      When others then
        dbms_output.put_line('INSERT LIABILITY RECORD  ' || sqlerrm);
    End;

    ---INSERT THE MATERIAL ACCOUNT RECORDS
    BEGIN
      FOR F_GET_Ar_DATA IN C_GET_Ar_DATA LOOP

        /* v_JE_DTL_ID := 'GL_007_D';*/

        GL_INSERT_JR_DTL(P_JE_DTL_ID           => F_GET_Ar_DATA.JE_DTL_ID,
                         P_JE_HDR_ID           => v_JE_HDR_ID,
                         P_JE_ACCT_CODE_ID     => F_GET_Ar_DATA.MATRIAL_ACCOUNT,
                         P_JE_PARTICULARS      => F_GET_Ar_DATA.Om_Remarks,
                         P_JE_CREDIT_DEBIT     => 'Cr',
                         P_JE_AMOUNT_CR        => NULL,
                         P_JE_ACCOUNTED_AMT_CR => NULL,
                         P_JE_SEGMENT_ID_1     => NULL,
                         P_JE_SEGMENT_ID_2     => NULL,
                         P_JE_SEGMENT_ID_3     => NULL,
                         P_JE_SEGMENT_ID_4     => NULL,
                         P_JE_SEGMENT_ID_5     => NULL,
                         P_JE_SEGMENT_ID_6     => NULL,
                         P_JE_AMOUNT_DR        => F_GET_Ar_DATA.MATRIAL_AMOUNT,
                         P_JE_ACCOUNTED_AMT_DR => F_GET_Ar_DATA.MATRIAL_AMOUNT);
      END LOOP;
    Exception
      When others then
        dbms_output.put_line('INSERT MATERIAL ACCOUNT  RECORD  ' ||
                             sqlerrm);
    End;
    --  END LOOP;
    -- COMMIT;

  END AR_OTHER_COST_POSTING;

  procedure FA_ASSET_DEPRE_POSTING(P_ASSET_DEPRE_ID VARCHAR2,
                                   P_ORG_ID         VARCHAR2) is

    cursor C_GET_Ar_DATA is
      SELECT 'GL_007_D' as JE_DTL_ID,
             R.ASSET_DEPRECIATION_ID,
             AA.AP_LIABILITY_ACC_NO AS MATRIAL_ACCOUNT,
             AA.ORGINAL_COST AS MATRIAL_AMOUNT, --DR,
             '' AS Remarks
        FROM ast_asset_depreciation_dtl R, AST_ASSET_MST AA
       WHERE R.ASSET_DEPRECIATION_ID = P_ASSET_DEPRE_ID
         AND R.ENABLED_FLAG = '1'
         AND R.ASSET_MST_ID = AA.ASSET_MST_ID
         AND R.WORKFLOW_COMPLETION_STATUS = '1';

    V_LIABILITY_AMOUNT      NUMBER;
    V_LIABILITY_ACCOUNT     VARCHAR(500);
    v_JE_HDR_ID             VARCHAR(500);
    v_JE_HDR_ID_VAL         VARCHAR(500);
    v_JN_NUMBER_ID          VARCHAR(500);
    v_JE_DTL_ID             VARCHAR(500);
    V_JOURNAL_TYPE          VARCHAR(500);
    V_JOURNAL_SOURCE        VARCHAR(500);
    V_JOURNAL_DATE          DATE;
    V_JOURNAL_DR_AMOUNT     NUMBER;
    V_JOURNAL_ORG_ID        VARCHAR(500);
    V_JOURNAL_GRN_NUM       VARCHAR(500);
    V_JOURNAL_GL_SEGMENT_ID VARCHAR(500);
    V_DESC                  VARCHAR(1500);
    V_COMP_CURRENCY         VARCHAR(500);
    V_PERIOD_ID             VARCHAR(500);
    V_EXCHANGE_RATE_VALUE   NUMBER;
    V_RECORD_DATE           DATE;
  BEGIN

    V_JOURNAL_TYPE   := 'STANDARD';
    V_JOURNAL_SOURCE := 'AR';
    V_JOURNAL_DATE   := TRUNC(SYSDATE);

    BEGIN
      SELECT 'GL_007_M' as JE_HDR_ID,
             'GL_007_JN' as JN_NUMBER_ID,
             (SELECT sv.segment_value_id
                FROM GL_COMPANIES_HDR  CH,
                     GL_SEGMENTS       s,
                     gl_segment_values sv
               WHERE s.segment_id = sv.segment_id
                 and ch.comp_global_segment_id = s.segment_id
                 AND CH.COMP_ID = P_ORG_ID
                 AND ROWNUM = 1) AS COMP_GLOBAL_SEGMENT_ID,
             R.ORGANIZATION_MASTER_ID,
             R.ASSET_DEPRECIATION_ID,
             R.GL_AMOUNT AS MATRIAL_AMOUNT, --DR
             (select cd.comp_base_currency
                from gl_companies_dtl cd
               where cd.comp_id = ch.comp_id
                 and cd.comp_id = P_ORG_ID
                 AND ROWNUM = 1) as org_currency,
             R.GL_TRANSFER_DATE
        into v_JE_HDR_ID_VAL,
             v_JN_NUMBER_ID,
             V_JOURNAL_GL_SEGMENT_ID,
             V_JOURNAL_ORG_ID,
             V_JOURNAL_GRN_NUM,
             V_JOURNAL_DR_AMOUNT,
             V_COMP_CURRENCY,
             V_RECORD_DATE
        FROM ast_asset_depreciation_dtl R,
             AST_ASSET_MST              AA,
             GL_COMPANIES_HDR           ch
       WHERE R.ASSET_DEPRECIATION_ID = P_ASSET_DEPRE_ID
         AND R.ENABLED_FLAG = '1'
         AND R.ORGANIZATION_MASTER_ID = P_ORG_ID
         AND R.ASSET_MST_ID = AA.ASSET_MST_ID
         AND CH.COMP_ID = R.ORGANIZATION_MASTER_ID
         AND R.WORKFLOW_COMPLETION_STATUS = '1';
    Exception
      When others then
        dbms_output.put_line('GRN DATA JL HEADER ' || sqlerrm);
    End;

    --GET MATERIAL OVERHEAD ACC CODE AND AMOUNT
    BEGIN
      SELECT 'GL_007_D' as JE_DTL_ID,
             aa.orginal_cost,
             aa.debit_asset_acc_no AS MATIRIAL_OVERHEAD_ACCT_CODE,
             '' as remarks
        INTO v_JE_DTL_ID, V_LIABILITY_AMOUNT, V_LIABILITY_ACCOUNT, V_DESC
        FROM ast_asset_depreciation_dtl R, AST_ASSET_MST AA
       WHERE R.ASSET_DEPRECIATION_ID = P_ASSET_DEPRE_ID
         AND R.ENABLED_FLAG = '1'
         AND R.ORGANIZATION_MASTER_ID = P_ORG_ID
         AND R.ASSET_MST_ID = AA.ASSET_MST_ID
         AND R.WORKFLOW_COMPLETION_STATUS = '1';
    Exception
      When others then
        dbms_output.put_line('LIABILITY ACC CODE ' || sqlerrm);
    End;

    --GET CAL PERIOD
    BEGIN
      IF V_RECORD_DATE IS NOT NULL THEN
        select gg.period_id
          INTO V_PERIOD_ID
          from gl_comp_acct_period_dtl gg
         where gg.enabled_flag = '1'
           and gg.workflow_completion_status = '1'
           and gg.PERIOD_STATUS = 'OPEN'
           and gg.COMP_ID = V_JOURNAL_ORG_ID
           and (trunc(V_RECORD_DATE)) between gg.period_from_dt and
               gg.period_to_dt
         order by gg.PERIOD_NAME asc;
      END IF;
    Exception
      When others then
        dbms_output.put_line('LIABILITY ACC CODE ' || sqlerrm);
    End;

    --GET  EXCHANGE_RATE_VALUE
    BEGIN
      SELECT SCER.CURRENCY_STD_RATE
        INTO V_EXCHANGE_RATE_VALUE
        FROM SSM_CURRENCY_EXCHANGE_RATES SCER
       WHERE SCER.WORKFLOW_COMPLETION_STATUS = 1
         AND SCER.ENABLED_FLAG = 1
         AND SCER.CURRENCY_ID = V_COMP_CURRENCY
         AND SCER.CURRENCY_RATE_DT = trunc(sysdate)
         AND SCER.CURRENCY_RATE_DT =
             (nvl((select sc.currency_rate_dt
                    FROM SSM_CURRENCY_EXCHANGE_RATES SC
                   where SC.CURRENCY_RATE_DT = trunc(sysdate)
                     and rownum = 1),
                  (select max(scr.CURRENCY_RATE_DT)
                     FROM SSM_CURRENCY_EXCHANGE_RATES SCr)));
    Exception
      When others then
        dbms_output.put_line('LIABILITY ACC CODE ' || sqlerrm);
    End;

    v_JE_HDR_ID := ssm.get_next_sequence(v_JE_HDR_ID_VAL, 'EN');

    BEGIN
      GL_INSERT(P_JE_HDR_ID => v_JE_HDR_ID,

                p_JE_COMP_ID             => V_JOURNAL_ORG_ID,
                P_JE_DATE                => V_JOURNAL_DATE,
                P_JE_GLOBAL_SEGMENT_ID   => V_JOURNAL_GL_SEGMENT_ID,
                P_JE_NUMBER              => v_JN_NUMBER_ID,
                P_JE_TYPE                => V_JOURNAL_TYPE,
                P_JE_CURRENCY_ID         => V_COMP_CURRENCY,
                P_JE_DESC                => 'Asset Depreciation Number:' ||
                                            V_JOURNAL_GRN_NUM ||
                                            ', Depreciation Amount:' ||
                                            V_JOURNAL_DR_AMOUNT,
                P_JE_CURRENCY_RATE_ID    => NULL,
                P_JE_STATUS              => 'OPEN',
                P_JE_CURR_VALUE          => NULL,
                P_JE_PERIOD_ID           => V_PERIOD_ID,
                P_JE_REFERENCE           => 'FA:Depreciation Number:' ||
                                            V_JOURNAL_GRN_NUM,
                P_JE_EXCHANGE_RATE_VALUE => V_EXCHANGE_RATE_VALUE,
                P_JE_TOT_DR_AMOUNT       => V_JOURNAL_DR_AMOUNT,
                P_JE_TOT_CR_AMOUNT       => V_JOURNAL_DR_AMOUNT,
                P_JE_RECURRING_END_DATE  => NULL,
                P_JE_RECURRING_FREQUENCY => NULL,
                P_JE_SOURCE              => V_JOURNAL_SOURCE,
                P_JOURNAL_REVERSED       => NULL,
                P_ACTUAL_JOURNAL_ID      => NULL,
                P_MODULE                 => 'FA');
    Exception
      When others then
        dbms_output.put_line('INSERT JOURNAL HEADER  ' || sqlerrm);
    End;

    --INSERT THE MATERIAL OVERHEAD RECORD
    BEGIN
      /* v_JE_DTL_ID := 'GL_007_D';*/

      GL_INSERT_JR_DTL(P_JE_DTL_ID           => v_JE_DTL_ID,
                       P_JE_HDR_ID           => v_JE_HDR_ID,
                       P_JE_ACCT_CODE_ID     => V_LIABILITY_ACCOUNT,
                       P_JE_PARTICULARS      => V_DESC,
                       P_JE_CREDIT_DEBIT     => 'Dr',
                       P_JE_AMOUNT_CR        => V_LIABILITY_AMOUNT,
                       P_JE_ACCOUNTED_AMT_CR => V_LIABILITY_AMOUNT,
                       P_JE_SEGMENT_ID_1     => NULL,
                       P_JE_SEGMENT_ID_2     => NULL,
                       P_JE_SEGMENT_ID_3     => NULL,
                       P_JE_SEGMENT_ID_4     => NULL,
                       P_JE_SEGMENT_ID_5     => NULL,
                       P_JE_SEGMENT_ID_6     => NULL,
                       P_JE_AMOUNT_DR        => NULL,
                       P_JE_ACCOUNTED_AMT_DR => NULL);
    Exception
      When others then
        dbms_output.put_line('INSERT LIABILITY RECORD  ' || sqlerrm);
    End;

    ---INSERT THE MATERIAL ACCOUNT RECORDS
    BEGIN
      FOR F_GET_Ar_DATA IN C_GET_Ar_DATA LOOP

        /* v_JE_DTL_ID := 'GL_007_D';*/

        GL_INSERT_JR_DTL(P_JE_DTL_ID           => F_GET_Ar_DATA.JE_DTL_ID,
                         P_JE_HDR_ID           => v_JE_HDR_ID,
                         P_JE_ACCT_CODE_ID     => F_GET_Ar_DATA.MATRIAL_ACCOUNT,
                         P_JE_PARTICULARS      => F_GET_Ar_DATA.Remarks,
                         P_JE_CREDIT_DEBIT     => 'Cr',
                         P_JE_AMOUNT_CR        => NULL,
                         P_JE_ACCOUNTED_AMT_CR => NULL,
                         P_JE_SEGMENT_ID_1     => NULL,
                         P_JE_SEGMENT_ID_2     => NULL,
                         P_JE_SEGMENT_ID_3     => NULL,
                         P_JE_SEGMENT_ID_4     => NULL,
                         P_JE_SEGMENT_ID_5     => NULL,
                         P_JE_SEGMENT_ID_6     => NULL,
                         P_JE_AMOUNT_DR        => F_GET_Ar_DATA.MATRIAL_AMOUNT,
                         P_JE_ACCOUNTED_AMT_DR => F_GET_Ar_DATA.MATRIAL_AMOUNT);
      END LOOP;
    Exception
      When others then
        dbms_output.put_line('INSERT MATERIAL ACCOUNT  RECORD  ' ||
                             sqlerrm);
    End;
    --  END LOOP;
    -- COMMIT;

  END FA_ASSET_DEPRE_POSTING;

  procedure ANNUAL_LEAVE_NON_RECUR_POSTING(P_LC_ID     VARCHAR2,
                                           P_ORG_ID    VARCHAR2,
                                           p_period_id VARCHAR2) is

    CURSOR C_ELEMENT IS
      SELECT EE.LEAVE_SAL_ELEMENT_ID,
             F.LC_EMP_ID,
             EE.PAY_ELEMENT_ID,
             EE.PAY_AMOUNT,
             EE.LC_ID,
             F.LC_PAY_PERIOD_ID,
             F.LC_DEPT_ID

        FROM HR_EMP_LEAVE_SAL_ELEMENT EE, HR_LEAVE_ENCASHMENT F
       WHERE upper(F.LC_PAYMENT_OPTION) = upper('IN_PAYROLL')
         and f.workflow_completion_status = 1
         AND F.LC_ID = EE.LC_ID
         and f.enabled_flag = 1
         and f.lc_id = P_LC_ID
         and f.lc_org_id = P_ORG_ID;

    V_TOT_AMOUNT         number := 0;
    V_PERIOD_ID          VARCHAR(500);
    v_LEAVE_SAL_ELE_CODE VARCHAR(500);

    v_period_count number;

    V_LS_EMP_ID           VARCHAR(500);
    V_LS_PAY_ELEMENT_CODE VARCHAR(500);
    V_LS_ELEMENT_AMT      NUMBER := 0;
    V_LS_LC_ID            VARCHAR(100);
    V_LS_LC_PAY_PERIOD_ID VARCHAR(500);
    V_LS_DEPT_ID          VARCHAR(500);

    V_EMP_ID           VARCHAR(500);
    V_PAY_ELEMENT_CODE VARCHAR(500);
    V_ELEMENT_AMT      NUMBER := 0;
    V_LC_ID            VARCHAR(100);

    V_LEAVE_SAL_ELEMENT_ID VARCHAR(500);

    V_LC_PAY_PERIOD_ID VARCHAR(500);
    V_PAY_NR_ID        VARCHAR(500);
    V_DEPT_ID          VARCHAR(500);

    V_CURRENT_PAY_NR_ID VARCHAR(500);
    v_tot_days          number;
    v_leave_id          varchar2(50);
    v_payroll_from      date;
    v_payroll_to        date;
    v_leave_from      date;
    v_leave_to        date;
    v_prev_days       number;
    v_curr_days       number;
    v_next_days       number;

        v_weekly_off_1 varchar2(100) := ssm.get_parameter_value('WEEKOFF1');
    v_weekly_off_2 varchar2(100) := ssm.get_parameter_value('WEEKOFF2');
    v_prev_pay_days number;
    v_basic_amt number;
    v_proc_dt   date;
    v_day varchar2(50);
  BEGIN
    BEGIN

      --GET LEAVE SALARY DTLS
      SELECT F.LC_EMP_ID,
             nvl(F.Lc_Leave_Salary, 0) + nvl(f.lc_air_ticket_amt, 0) as tot_amt,
             f.lc_pay_period_id as period_id,
             (select so.hr_leave_sal
                from ssm_system_options so
               where so.org_id = f.lc_org_id
                 and f.leave_id = so.hr_earn_leave
                 and so.module_code = 'HR') as leave_sal_element_code,

             F.LC_DEPT_ID,
             F.Trans_Id
        into V_LS_EMP_ID,
             V_TOT_AMOUNT,
             V_PERIOD_ID,
             v_LEAVE_SAL_ELE_CODE,
             V_LS_DEPT_ID,
             v_leave_id
        FROM HR_LEAVE_ENCASHMENT F
       WHERE upper(F.LC_PAYMENT_OPTION) = upper('IN_PAYROLL')
         and f.workflow_completion_status = 1
         and f.enabled_flag = 1
         and f.lc_id = P_LC_ID
         and f.lc_org_id = P_ORG_ID;

    Exception
      When others then
        dbms_output.put_line('GET LEAVE SALARY DTLS ERROR  ' || sqlerrm);
    End;

    BEGIN
      --GET INDIVUAL ELEMENT DTLS
      SELECT EE.LEAVE_SAL_ELEMENT_ID,
             F.LC_EMP_ID,
             EE.PAY_ELEMENT_ID,
             EE.PAY_AMOUNT,
             EE.LC_ID,
             F.LC_PAY_PERIOD_ID,
             F.LC_DEPT_ID
        into V_LEAVE_SAL_ELEMENT_ID,
             V_EMP_ID,
             V_PAY_ELEMENT_CODE,
             V_ELEMENT_AMT,
             V_LC_ID,
             V_LC_PAY_PERIOD_ID,
             V_DEPT_ID
        FROM HR_EMP_LEAVE_SAL_ELEMENT EE, HR_LEAVE_ENCASHMENT F
       WHERE upper(F.LC_PAYMENT_OPTION) = upper('IN_PAYROLL')
         and f.workflow_completion_status = 1
         AND F.LC_ID = EE.LC_ID
         and f.enabled_flag = 1
         and f.lc_id = P_LC_ID
         and f.lc_org_id = P_ORG_ID;

    Exception
      When others then
        dbms_output.put_line('GET INDIVUAL ELEMENT DTLS ERROR ' || sqlerrm);
    End;

begin
    select pkg_payroll.get_days(p_from_dt => hla.leave_date_from, p_to_dt => hla.leave_date_to),hla.leave_date_from,hla.leave_date_to
     into v_tot_days,v_leave_from,v_leave_to from hr_leave_applications hla where hla.leave_req_id=v_leave_id;
exception when others then
  null;
  end;
  begin
    select pp.pay_from_dt,pp.pay_to_dt
    into v_payroll_from,v_payroll_to
     from pay_periods pp where pp.pay_period_id=p_period_id;
exception when others then null;
end;

 if v_leave_from >= v_payroll_from and v_leave_to <= v_payroll_to then
    v_prev_days:=0;
    v_curr_days:=v_tot_days;
    v_next_days:=0;
 elsif v_leave_from < v_payroll_from and v_leave_to < v_payroll_from then
    v_prev_days:=v_tot_days;
    v_curr_days:=0;
    v_next_days:=0;
 elsif v_leave_from > v_payroll_to and v_leave_to > v_payroll_to then
    v_prev_days:=0;
    v_curr_days:=0;
    v_next_days:=v_tot_days;
else

  v_proc_dt := v_leave_from;
    v_prev_days:=0;
    v_curr_days:=0;
    v_next_days:=0;

        WHILE v_proc_dt <= v_leave_to LOOP

      v_day := (LTRIM(RTRIM(TO_CHAR(v_proc_dt, 'DAY'))));
      IF (v_day = v_weekly_off_1 or v_day = v_weekly_off_2) THEN
        null;
      else
        if v_proc_dt <= v_payroll_from then

        v_prev_days := v_prev_days + 1;

        elsif v_proc_dt >= v_payroll_from and v_proc_dt <= v_payroll_to then
          v_curr_days := v_curr_days+1;
        else
          v_next_days := v_next_days +1;
        end if;

      END IF;
      v_proc_dt := v_proc_dt + 1;
    END LOOP;

 end if;



      if v_curr_days > 0 then

      BEGIN
        V_CURRENT_PAY_NR_ID := ssm.get_next_sequence('PER_015_M', 'EN');
        ----INSERT NON RECURRING LEAVE SAL HDR TABLE
        INSERT INTO PAY_NR_ELEMENTS_HDR
          (PAY_NR_ID,
           PAY_ELEMENT_ID,
           PAY_PERIOD_ID,
           ENABLED_FLAG,
           WORKFLOW_COMPLETION_STATUS,
           CREATED_BY,
           CREATED_DATE,
           PAY_ORG_ID)
        VALUES
          (V_CURRENT_PAY_NR_ID,
           v_LEAVE_SAL_ELE_CODE,
           V_PERIOD_ID,
           1,
           1,
           'ADMIN',
           trunc(SYSDATE),
           P_ORG_ID);
      Exception
        When others then
          dbms_output.put_line('INSERT NON RECURRING LEAVE SAL HDR TABLE  ' ||
                               sqlerrm);

      End;

      BEGIN

        --INSERT LEAVE SALARY DETAILS

        INSERT INTO PAY_NR_ELEMENTS_DTL
          (PAY_NR_DTL_ID,
           PAY_NR_ID,
           PAY_EMP_DEPT_ID,
           PAY_EMP_ID,
           PAY_DED_AMOUNT,
           ENABLED_FLAG,
           WORKFLOW_COMPLETION_STATUS,
           CREATED_BY,
           CREATED_DATE)
        VALUES
          (ssm.get_next_sequence('PER_015_D', 'EN'),
           V_CURRENT_PAY_NR_ID,
           V_LS_DEPT_ID,
           V_LS_EMP_ID,
           round((V_TOT_AMOUNT/v_tot_days)*v_curr_days,3),
           1,
           1,
           'ADMIN',
           trunc(SYSDATE));
      Exception
        When others then
          dbms_output.put_line('INSERT LEAVE SALARY DETAILS 2 ' || sqlerrm);
          debug_proc('111' || V_TOT_AMOUNT || substr(sqlerrm, 1, 200));
      End;
end if;

      if v_prev_days > 0 or v_next_days > 0 then

      BEGIN
        V_CURRENT_PAY_NR_ID := ssm.get_next_sequence('PER_015_M', 'EN');
        ----INSERT NON RECURRING LEAVE SAL HDR TABLE
        INSERT INTO PAY_NR_ELEMENTS_HDR
          (PAY_NR_ID,
           PAY_ELEMENT_ID,
           PAY_PERIOD_ID,
           ENABLED_FLAG,
           WORKFLOW_COMPLETION_STATUS,
           CREATED_BY,
           CREATED_DATE,
           PAY_ORG_ID)
        VALUES
          (V_CURRENT_PAY_NR_ID,
           'PYE_ID-0000001047',
           V_PERIOD_ID,
           1,
           1,
           'ADMIN',
           trunc(SYSDATE),
           P_ORG_ID);
      Exception
        When others then
          dbms_output.put_line('INSERT NON RECURRING LEAVE SAL HDR TABLE  ' ||
                               sqlerrm);

      End;

      BEGIN

        --INSERT LEAVE SALARY DETAILS

        INSERT INTO PAY_NR_ELEMENTS_DTL
          (PAY_NR_DTL_ID,
           PAY_NR_ID,
           PAY_EMP_DEPT_ID,
           PAY_EMP_ID,
           PAY_DED_AMOUNT,
           ENABLED_FLAG,
           WORKFLOW_COMPLETION_STATUS,
           CREATED_BY,
           CREATED_DATE)
        VALUES
          (ssm.get_next_sequence('PER_015_D', 'EN'),
           V_CURRENT_PAY_NR_ID,
           V_LS_DEPT_ID,
           V_LS_EMP_ID,
           round((V_TOT_AMOUNT/v_tot_days)*(v_prev_days+v_next_days),3),
           1,
           1,
           'ADMIN',
           trunc(SYSDATE));
      Exception
        When others then
          dbms_output.put_line('INSERT LEAVE SALARY DETAILS 2 ' || sqlerrm);
          debug_proc('111' || V_TOT_AMOUNT || substr(sqlerrm, 1, 200));
      End;

      if v_prev_days >0 then

            BEGIN
        V_CURRENT_PAY_NR_ID := ssm.get_next_sequence('PER_015_M', 'EN');
        ----INSERT NON RECURRING LEAVE SAL HDR TABLE
        INSERT INTO PAY_NR_ELEMENTS_HDR
          (PAY_NR_ID,
           PAY_ELEMENT_ID,
           PAY_PERIOD_ID,
           ENABLED_FLAG,
           WORKFLOW_COMPLETION_STATUS,
           CREATED_BY,
           CREATED_DATE,
           PAY_ORG_ID)
        VALUES
          (V_CURRENT_PAY_NR_ID,
           'PYE_ID-0000001044',
           V_PERIOD_ID,
           1,
           1,
           'ADMIN',
           trunc(SYSDATE),
           P_ORG_ID);
      Exception
        When others then
          dbms_output.put_line('INSERT NON RECURRING LEAVE SAL HDR TABLE  ' ||
                               sqlerrm);

      End;

      BEGIN

        --INSERT LEAVE SALARY DETAILS
        v_prev_pay_days := pkg_payroll.get_days(p_from_dt => trunc(v_payroll_from-1,'MM'),p_to_dt => LAST_DAY(trunc(v_payroll_from-1,'MM')));
        v_basic_amt := pkg_payroll.get_basic_amt(p_org_id => P_ORG_ID, p_emp_id => V_LS_EMP_ID);

        INSERT INTO PAY_NR_ELEMENTS_DTL
          (PAY_NR_DTL_ID,
           PAY_NR_ID,
           PAY_EMP_DEPT_ID,
           PAY_EMP_ID,
           PAY_DED_AMOUNT,
           ENABLED_FLAG,
           WORKFLOW_COMPLETION_STATUS,
           CREATED_BY,
           CREATED_DATE)
        VALUES
          (ssm.get_next_sequence('PER_015_D', 'EN'),
           V_CURRENT_PAY_NR_ID,
           V_LS_DEPT_ID,
           V_LS_EMP_ID,
           round((v_basic_amt/v_prev_pay_days)*(v_prev_days),3),
           1,
           1,
           'ADMIN',
           trunc(SYSDATE));
      Exception
        When others then
          dbms_output.put_line('INSERT LEAVE SALARY DETAILS 2 ' || sqlerrm);
          debug_proc('111' || V_TOT_AMOUNT || substr(sqlerrm, 1, 200));
      End;

      end if;

end if;

      BEGIN
        ----INSERT NON RECURRING ELEMENT HDR TABLE
        if V_PAY_ELEMENT_CODE is not null then

        V_CURRENT_PAY_NR_ID := ssm.get_next_sequence('PER_015_M', 'EN');

          INSERT INTO PAY_NR_ELEMENTS_HDR
            (PAY_NR_ID,
             PAY_ELEMENT_ID,
             PAY_PERIOD_ID,
             ENABLED_FLAG,
             WORKFLOW_COMPLETION_STATUS,
             CREATED_BY,
             CREATED_DATE,
             PAY_ORG_ID)
          VALUES
            (V_CURRENT_PAY_NR_ID,
             V_PAY_ELEMENT_CODE,
             V_LC_PAY_PERIOD_ID,
             1,
             1,
             'ADMIN',
             trunc(SYSDATE),
             P_ORG_ID);
        end if;
      Exception
        When others then
          dbms_output.put_line('INSERT NON RECURRING ELEMENT HDR TABLE  ' ||
                               sqlerrm);
      End;

      BEGIN
        --INSERT INDIVUAL ELEMENT DTLS
        FOR F_ELEMENT IN C_ELEMENT LOOP

          INSERT INTO PAY_NR_ELEMENTS_DTL
            (PAY_NR_DTL_ID,
             PAY_NR_ID,
             PAY_EMP_DEPT_ID,
             PAY_EMP_ID,
             PAY_DED_AMOUNT,
             ENABLED_FLAG,
             WORKFLOW_COMPLETION_STATUS,
             CREATED_BY,
             CREATED_DATE)
          VALUES
            (ssm.get_next_sequence('PER_015_D', 'EN'),
             V_CURRENT_PAY_NR_ID,
             F_ELEMENT.LC_DEPT_ID,
             F_ELEMENT.LC_EMP_ID,
             F_ELEMENT.PAY_AMOUNT,
             1,
             1,
             'ADMIN',
             trunc(SYSDATE));

        END LOOP;
        COMMIT;
      Exception
        When others then
          dbms_output.put_line('INSERT INDIVUAL ELEMENT DTLS 2  ' ||
                               sqlerrm);

      End;

  END ANNUAL_LEAVE_NON_RECUR_POSTING;

  PROCEDURE INDEM_PROVISION_POSTING(P_ORG_ID VARCHAR2, p_date date) IS
    v_JE_HDR_ID VARCHAR2(50);

  BEGIN

    FOR C_PR IN (SELECT 'GL_007_M' AS JE_HDR_ID,
                        c.comp_id,
                        trunc(SYSDATE) AS JE_DATE,
                        'GL_007_JN' AS JE_NUM,
                        'STANDARD' AS JE_TYP,
                        cd.comp_base_currency AS JE_CURRENCY_ID,
                        (SELECT sv.segment_value_id
                           FROM GL_SEGMENTS s, gl_segment_values sv
                          WHERE s.segment_id = sv.segment_id
                            and hd.segment_value_id = sv.segment_value_id
                            AND ROWNUM = 1) as COMP_GLOBAL_SEGMENT_ID,
                        'INDEM PROVISION' AS JE_DESC,
                        NULL AS JE_CURRENCY_RATE_ID,
                        'OPEN' AS JE_STATUS,
                        NULL AS JE_CURR_VALUE,
                        (select gg.period_id
                           from gl_comp_acct_period_dtl gg,
                                GL_ACCT_CALENDAR_DTL    CDI
                          where gg.enabled_flag = '1'
                            and gg.workflow_completion_status = '1'
                            AND GG.CAL_DTL_ID = CDI.CAL_DTL_ID
                            and gg.PERIOD_STATUS = 'OPEN'
                            and gg.COMP_ID = C.COMP_ID
                            and rownum = 1
                            and trunc(hl.indem_txn_date) between
                                gg.period_from_dt and gg.period_to_dt) AS JE_PERIOD_ID,
                        INDEM_REMARKS AS JE_REFERENCE,
                        NULL AS JE_EXCHANGE_RATE_VALUE,
                        trunc(SYSDATE) AS JE_RECURRING_END_DATE,
                        NULL AS JE_RECURRING_FREQUENCY,
                        'INDEM PROVISION' AS JE_SOURCE,
                        NULL AS JOURNAL_REVERSED,
                        NULL AS ACTUAL_JOURNAL_ID,
                        sum(HL.INDEM_AMT_AVAIL) as JE_TOT_CR_AMOUNT,
                        sum(HL.INDEM_AMT_AVAIL) as JE_TOT_DR_AMOUNT,
                        hd.dept_id
                   FROM HR_INDEM_TXN_LEDGER HL,
                        gl_companies_hdr    c,
                        GL_COMPANIES_DTL    cd,
                        hr_departments      hd
                  where HL.INDEM_ORG_ID = c.comp_id
                    AND HL.ENABLED_FLAG = 1
                    and hl.workflow_completion_status = 1
                    and hd.dept_id = hl.indem_dept_id
                    and hd.org_id = c.comp_id
                    and c.comp_id = cd.comp_id
                    and c.comp_id = P_ORG_ID
                    and hl.indem_txn_date = p_date
                  GROUP BY c.comp_id,
                           hd.dept_id,
                           hd.segment_value_id,
                           cd.comp_base_currency,
                           HL.INDEM_REMARKS,
                           hl.indem_txn_date)

     LOOP
      v_JE_HDR_ID := ssm.get_next_sequence(C_PR.JE_HDR_ID, 'EN');

      GL_INSERT(P_JE_HDR_ID              => v_JE_HDR_ID,
                p_JE_COMP_ID             => C_PR.COMP_ID,
                P_JE_DATE                => C_PR.JE_DATE,
                P_JE_GLOBAL_SEGMENT_ID   => C_PR.COMP_GLOBAL_SEGMENT_ID,
                P_JE_NUMBER              => C_PR.JE_NUM,
                P_JE_TYPE                => C_PR.JE_TYP,
                P_JE_CURRENCY_ID         => C_PR.JE_CURRENCY_ID,
                P_JE_DESC                => 'Indem Provision: ' || 'Amount:' ||
                                            C_PR.JE_TOT_DR_AMOUNT,
                P_JE_CURRENCY_RATE_ID    => C_PR.JE_CURRENCY_RATE_ID,
                P_JE_STATUS              => C_PR.JE_STATUS,
                P_JE_CURR_VALUE          => C_PR.JE_CURR_VALUE,
                P_JE_PERIOD_ID           => C_PR.JE_PERIOD_ID,
                P_JE_REFERENCE           => 'HR:Indem Provision:' ||
                                            C_PR.JE_REFERENCE,
                P_JE_EXCHANGE_RATE_VALUE => C_PR.JE_EXCHANGE_RATE_VALUE,
                P_JE_TOT_DR_AMOUNT       => C_PR.JE_TOT_DR_AMOUNT,
                P_JE_TOT_CR_AMOUNT       => C_PR.JE_TOT_CR_AMOUNT,
                P_JE_RECURRING_END_DATE  => C_PR.JE_RECURRING_END_DATE,
                P_JE_RECURRING_FREQUENCY => C_PR.JE_RECURRING_FREQUENCY,
                P_JE_SOURCE              => C_PR.JE_SOURCE,
                P_JOURNAL_REVERSED       => C_PR.JOURNAL_REVERSED,
                P_ACTUAL_JOURNAL_ID      => C_PR.ACTUAL_JOURNAL_ID,
                P_MODULE                 => 'HR');

      FOR C_PR_JRD IN (select sum(round(hl.INDEM_AMT_AVAIL, 3)) as JE_AMOUNT_CR,
                              sum(round(hl.INDEM_AMT_AVAIL, 3)) as JE_ACCOUNTED_AMT_CR,
                              'GL_007_D' as JE_DTL_ID,

                              sss.HR_LEAVE_INDM_SUSP_ACC as acct_code_id,
                              null as JE_PARTICULARS,
                              'Cr' as JE_CREDIT_DEBIT,
                              null as JE_AMOUNT_DR,
                              null as JE_ACCOUNTED_AMT_DR,
                              null as JE_SEGMENT_ID_1,
                              null as JE_SEGMENT_ID_2,
                              null as JE_SEGMENT_ID_3,
                              null as JE_SEGMENT_ID_4,
                              null as JE_SEGMENT_ID_5,
                              null as JE_SEGMENT_ID_6
                         from hr_indem_txn_ledger hl, ssm_system_options sss
                        where hl.INDEM_ORG_ID = P_ORG_ID
                          and hl.INDEM_DEPT_ID = C_PR.dept_id
                          and hl.INDEM_TXN_DATE = p_date
                          AND HL.ENABLED_FLAG = 1
                          and hl.workflow_completion_status = 1
                          and sss.org_id = hl.indem_org_id
                        group by sss.HR_LEAVE_INDM_SUSP_ACC
                       union
                       select null as JE_AMOUNT_CR,
                              null as JE_ACCOUNTED_AMT_CR,
                              'GL_007_D' as JE_DTL_ID,
                              sss.HR_INDEM_EXPENSE_AC as acct_code_id,
                              null as JE_PARTICULARS,
                              'Dr' as JE_CREDIT_DEBIT,
                              sum(round(hl.INDEM_AMT_AVAIL, 3)) as JE_AMOUNT_DR,
                              sum(round(hl.INDEM_AMT_AVAIL, 3)) as JE_ACCOUNTED_AMT_DR,
                              null as JE_SEGMENT_ID_1,
                              null as JE_SEGMENT_ID_2,
                              null as JE_SEGMENT_ID_3,
                              null as JE_SEGMENT_ID_4,
                              null as JE_SEGMENT_ID_5,
                              null as JE_SEGMENT_ID_6
                         from hr_indem_txn_ledger hl, ssm_system_options sss
                        where hl.INDEM_ORG_ID = P_ORG_ID
                          and hl.INDEM_DEPT_ID = C_PR.dept_id
                          and hl.INDEM_TXN_DATE = p_date
                          AND HL.ENABLED_FLAG = 1
                          and hl.workflow_completion_status = 1
                          and sss.org_id = hl.indem_org_id
                        group by sss.HR_INDEM_EXPENSE_AC) LOOP

        -- FOR C_PR_JRD IN PR_JRD LOOP
        GL_INSERT_JR_DTL(P_JE_DTL_ID           => C_PR_JRD.JE_DTL_ID,
                         P_JE_HDR_ID           => v_JE_HDR_ID,
                         P_JE_ACCT_CODE_ID     => C_PR_JRD.ACCT_CODE_ID,
                         P_JE_PARTICULARS      => C_PR_JRD.JE_PARTICULARS,
                         P_JE_CREDIT_DEBIT     => C_PR_JRD.JE_CREDIT_DEBIT,
                         P_JE_AMOUNT_CR        => C_PR_JRD.JE_AMOUNT_CR,
                         P_JE_ACCOUNTED_AMT_CR => C_PR_JRD.JE_ACCOUNTED_AMT_CR,
                         P_JE_SEGMENT_ID_1     => C_PR_JRD.JE_SEGMENT_ID_1,
                         P_JE_SEGMENT_ID_2     => C_PR_JRD.JE_SEGMENT_ID_2,
                         P_JE_SEGMENT_ID_3     => C_PR_JRD.JE_SEGMENT_ID_3,
                         P_JE_SEGMENT_ID_4     => C_PR_JRD.JE_SEGMENT_ID_4,
                         P_JE_SEGMENT_ID_5     => C_PR_JRD.JE_SEGMENT_ID_5,
                         P_JE_SEGMENT_ID_6     => C_PR_JRD.JE_SEGMENT_ID_6,
                         P_JE_AMOUNT_DR        => C_PR_JRD.JE_AMOUNT_DR,
                         P_JE_ACCOUNTED_AMT_DR => C_PR_JRD.JE_ACCOUNTED_AMT_DR);

      END LOOP;
    END LOOP;
    COMMIT;

  EXCEPTION
    WHEN OTHERS THEN
      NULL;

  END INDEM_PROVISION_POSTING;
  PROCEDURE INDEM_PAYMENT_POSTING(P_ORG_ID VARCHAR2, p_date date) IS
    v_JE_HDR_ID VARCHAR2(50);

  BEGIN

    FOR C_PR IN (SELECT 'GL_007_M' AS JE_HDR_ID,
                        c.comp_id,
                        trunc(SYSDATE) AS JE_DATE,
                        'GL_007_JN' AS JE_NUM,
                        'STANDARD' AS JE_TYP,
                        cd.comp_base_currency AS JE_CURRENCY_ID,
                        (SELECT sv.segment_value_id
                           FROM GL_SEGMENTS s, gl_segment_values sv
                          WHERE s.segment_id = sv.segment_id
                            and hd.segment_value_id = sv.segment_value_id
                            AND ROWNUM = 1) as COMP_GLOBAL_SEGMENT_ID,
                        'INDEM PAYMENT' AS JE_DESC,
                        NULL AS JE_CURRENCY_RATE_ID,
                        'OPEN' AS JE_STATUS,
                        NULL AS JE_CURR_VALUE,
                        (select gg.period_id
                           from gl_comp_acct_period_dtl gg,
                                GL_ACCT_CALENDAR_DTL    CDI
                          where gg.enabled_flag = '1'
                            and gg.workflow_completion_status = '1'
                            AND GG.CAL_DTL_ID = CDI.CAL_DTL_ID
                            and gg.PERIOD_STATUS = 'OPEN'
                            and gg.COMP_ID = C.COMP_ID
                            and rownum = 1
                            and trunc(hl.indem_pay_date) between
                                gg.period_from_dt and gg.period_to_dt) AS JE_PERIOD_ID,
                        INDEM_PAY_REMARKS AS JE_REFERENCE,
                        NULL AS JE_EXCHANGE_RATE_VALUE,
                        trunc(SYSDATE) AS JE_RECURRING_END_DATE,
                        NULL AS JE_RECURRING_FREQUENCY,
                        'INDEM PROVISION' AS JE_SOURCE,
                        NULL AS JOURNAL_REVERSED,
                        NULL AS ACTUAL_JOURNAL_ID,
                        sum(HL.INDEM_CALC_AMT) as JE_TOT_CR_AMOUNT,
                        sum(HL.INDEM_CALC_AMT) as JE_TOT_DR_AMOUNT,
                        hd.dept_id
                   FROM HR_INDEM_PAYMENT HL,
                        gl_companies_hdr c,
                        GL_COMPANIES_DTL cd,
                        hr_departments   hd
                  where HL.ORG_ID = c.comp_id
                    AND HL.ENABLED_FLAG = 1
                    and hl.workflow_completion_status = 1
                    and hd.dept_id = hl.dept_id
                    and hd.org_id = c.comp_id
                    and c.comp_id = cd.comp_id
                    and c.comp_id = P_ORG_ID
                    and hl.indem_pay_date = p_date
                  GROUP BY c.comp_id,
                           hd.dept_id,
                           hd.segment_value_id,
                           cd.comp_base_currency,
                           HL.INDEM_PAY_REMARKS,
                           hl.indem_pay_date)

     LOOP
      v_JE_HDR_ID := ssm.get_next_sequence(C_PR.JE_HDR_ID, 'EN');

      GL_INSERT(P_JE_HDR_ID              => v_JE_HDR_ID,
                p_JE_COMP_ID             => C_PR.COMP_ID,
                P_JE_DATE                => C_PR.JE_DATE,
                P_JE_GLOBAL_SEGMENT_ID   => C_PR.COMP_GLOBAL_SEGMENT_ID,
                P_JE_NUMBER              => C_PR.JE_NUM,
                P_JE_TYPE                => C_PR.JE_TYP,
                P_JE_CURRENCY_ID         => C_PR.JE_CURRENCY_ID,
                P_JE_DESC                => 'Indem Payment: ' || 'Amount:' ||
                                            C_PR.JE_TOT_DR_AMOUNT,
                P_JE_CURRENCY_RATE_ID    => C_PR.JE_CURRENCY_RATE_ID,
                P_JE_STATUS              => C_PR.JE_STATUS,
                P_JE_CURR_VALUE          => C_PR.JE_CURR_VALUE,
                P_JE_PERIOD_ID           => C_PR.JE_PERIOD_ID,
                P_JE_REFERENCE           => 'HR:Indem Payment: ' ||
                                            C_PR.JE_REFERENCE,
                P_JE_EXCHANGE_RATE_VALUE => C_PR.JE_EXCHANGE_RATE_VALUE,
                P_JE_TOT_DR_AMOUNT       => C_PR.JE_TOT_DR_AMOUNT,
                P_JE_TOT_CR_AMOUNT       => C_PR.JE_TOT_CR_AMOUNT,
                P_JE_RECURRING_END_DATE  => C_PR.JE_RECURRING_END_DATE,
                P_JE_RECURRING_FREQUENCY => C_PR.JE_RECURRING_FREQUENCY,
                P_JE_SOURCE              => C_PR.JE_SOURCE,
                P_JOURNAL_REVERSED       => C_PR.JOURNAL_REVERSED,
                P_ACTUAL_JOURNAL_ID      => C_PR.ACTUAL_JOURNAL_ID,
                P_MODULE                 => 'HR');

      FOR C_PR_JRD IN (select null as JE_AMOUNT_CR,
                              null as JE_ACCOUNTED_AMT_CR,
                              'GL_007_D' as JE_DTL_ID,
                              ss.HR_LEAVE_INDM_SUSP_ACC AS acct_code_id,
                              hl.dept_id,
                              null as JE_PARTICULARS,
                              'Dr' as JE_CREDIT_DEBIT,
                              sum(round(hl.indem_calc_amt, 3)) as JE_AMOUNT_DR,
                              sum(round(hl.indem_calc_amt, 3)) as JE_ACCOUNTED_AMT_DR,
                              null as JE_SEGMENT_ID_1,
                              null as JE_SEGMENT_ID_2,
                              null as JE_SEGMENT_ID_3,
                              null as JE_SEGMENT_ID_4,
                              null as JE_SEGMENT_ID_5,
                              null as JE_SEGMENT_ID_6
                         from HR_INDEM_PAYMENT hl, ssm_system_options ss
                        where hl.ORG_ID = P_ORG_ID
                          and hl.dept_id = C_PR.dept_id
                          and hl.indem_pay_date = p_date
                          AND HL.ENABLED_FLAG = 1
                          AND SS.ORG_ID = hl.ORG_ID
                          and hl.workflow_completion_status = 1
                        group by hl.dept_id,
                                 hl.indem_pay_date,
                                 hl.ORG_ID,
                                 ss.HR_LEAVE_INDM_SUSP_ACC
                       union
                       select sum(round(hl.indem_calc_amt, 3)) as JE_AMOUNT_CR,
                              sum(round(hl.indem_calc_amt, 3)) as JE_ACCOUNTED_AMT_CR,
                              'GL_007_D' as JE_DTL_ID,
                              bb.gl_account as acct_code_id,
                              hl.dept_id,
                              null as JE_PARTICULARS,
                              'Cr' as JE_CREDIT_DEBIT,
                              null as JE_AMOUNT_DR,
                              null as JE_ACCOUNTED_AMT_DR,
                              null as JE_SEGMENT_ID_1,
                              null as JE_SEGMENT_ID_2,
                              null as JE_SEGMENT_ID_3,
                              null as JE_SEGMENT_ID_4,
                              null as JE_SEGMENT_ID_5,
                              null as JE_SEGMENT_ID_6
                         from HR_INDEM_PAYMENT hl, ca_bank_accounts bb
                        where hl.ORG_ID = P_ORG_ID
                          and hl.dept_id = C_PR.dept_id
                          and hl.indem_pay_date = p_date
                          and bb.bank_id = hl.bank_id
                          AND HL.ENABLED_FLAG = 1
                          and hl.workflow_completion_status = 1
                        group by hl.dept_id, bb.gl_account) LOOP

        -- FOR C_PR_JRD IN PR_JRD LOOP
        GL_INSERT_JR_DTL(P_JE_DTL_ID           => C_PR_JRD.JE_DTL_ID,
                         P_JE_HDR_ID           => v_JE_HDR_ID,
                         P_JE_ACCT_CODE_ID     => C_PR_JRD.ACCT_CODE_ID,
                         P_JE_PARTICULARS      => C_PR_JRD.JE_PARTICULARS,
                         P_JE_CREDIT_DEBIT     => C_PR_JRD.JE_CREDIT_DEBIT,
                         P_JE_AMOUNT_CR        => C_PR_JRD.JE_AMOUNT_CR,
                         P_JE_ACCOUNTED_AMT_CR => C_PR_JRD.JE_ACCOUNTED_AMT_CR,
                         P_JE_SEGMENT_ID_1     => C_PR_JRD.JE_SEGMENT_ID_1,
                         P_JE_SEGMENT_ID_2     => C_PR_JRD.JE_SEGMENT_ID_2,
                         P_JE_SEGMENT_ID_3     => C_PR_JRD.JE_SEGMENT_ID_3,
                         P_JE_SEGMENT_ID_4     => C_PR_JRD.JE_SEGMENT_ID_4,
                         P_JE_SEGMENT_ID_5     => C_PR_JRD.JE_SEGMENT_ID_5,
                         P_JE_SEGMENT_ID_6     => C_PR_JRD.JE_SEGMENT_ID_6,
                         P_JE_AMOUNT_DR        => C_PR_JRD.JE_AMOUNT_DR,
                         P_JE_ACCOUNTED_AMT_DR => C_PR_JRD.JE_ACCOUNTED_AMT_DR);

      END LOOP;
    END LOOP;
    COMMIT;

  EXCEPTION
    WHEN OTHERS THEN
      NULL;

  END INDEM_PAYMENT_POSTING;

  PROCEDURE LEAVE_PROVISION_POSTING(P_ORG_ID VARCHAR2, p_date date) IS
    v_JE_HDR_ID VARCHAR2(50);

  BEGIN

    FOR C_PR IN (SELECT 'GL_007_M' AS JE_HDR_ID,
                        c.comp_id,
                        hd.dept_id,
                        trunc(SYSDATE) AS JE_DATE,
                        (SELECT sv.segment_value_id
                           FROM GL_SEGMENTS s, gl_segment_values sv
                          WHERE s.segment_id = sv.segment_id
                            and hd.segment_value_id = sv.segment_value_id
                            AND ROWNUM = 1) as COMP_GLOBAL_SEGMENT_ID,
                        'GL_007_JN' AS JE_NUM,
                        'STANDARD' AS JE_TYP,
                        cd.comp_base_currency AS JE_CURRENCY_ID,
                        'LEAVE PROVISION' AS JE_DESC,
                        NULL AS JE_CURRENCY_RATE_ID,
                        'OPEN' AS JE_STATUS,
                        NULL AS JE_CURR_VALUE,
                        (select gg.period_id
                           from gl_comp_acct_period_dtl gg,
                                GL_ACCT_CALENDAR_DTL    CDI
                          where gg.enabled_flag = '1'
                            and gg.workflow_completion_status = '1'
                            AND GG.CAL_DTL_ID = CDI.CAL_DTL_ID
                            and gg.PERIOD_STATUS = 'OPEN'
                            and gg.COMP_ID = C.COMP_ID
                            and rownum = 1
                            and trunc(hl.leave_txn_date) between
                                gg.period_from_dt and gg.period_to_dt) AS JE_PERIOD_ID,
                        HL.LEAVE_REMARKS AS JE_REFERENCE,
                        NULL AS JE_EXCHANGE_RATE_VALUE,
                        trunc(SYSDATE) AS JE_RECURRING_END_DATE,
                        NULL AS JE_RECURRING_FREQUENCY,
                        'PER' AS JE_SOURCE,
                        NULL AS JOURNAL_REVERSED,
                        NULL AS ACTUAL_JOURNAL_ID,
                        sum(HL.leave_provision_amt) as JE_TOT_CR_AMOUNT,
                        sum(HL.leave_provision_amt) as JE_TOT_DR_AMOUNT
                   FROM HR_LEAVE_TXN_LEDGER HL,
                        gl_companies_hdr    c,
                        GL_COMPANIES_DTL    cd,
                        hr_departments      hd
                  where HL.LEAVE_ORG_ID = c.comp_id
                    AND HL.ENABLED_FLAG = 1
                    and c.comp_id = cd.comp_id
                    and hd.dept_id = hl.leave_dept_id
                    and hd.org_id = c.comp_id
                    and c.comp_id = P_ORG_ID
                    and hl.workflow_completion_status = 1
                    and hl.enabled_flag = 1
                    and hl.LEAVE_TXN_DATE = p_date
                  GROUP BY c.comp_id,
                           hd.segment_value_id,
                           cd.comp_base_currency,
                           hd.dept_id,
                           HL.LEAVE_REMARKS,
                           hl.leave_txn_date)

     LOOP
      v_JE_HDR_ID := ssm.get_next_sequence(C_PR.JE_HDR_ID, 'EN');

      GL_INSERT(P_JE_HDR_ID              => v_JE_HDR_ID,
                p_JE_COMP_ID             => C_PR.COMP_ID,
                P_JE_DATE                => C_PR.JE_DATE,
                P_JE_GLOBAL_SEGMENT_ID   => C_PR.COMP_GLOBAL_SEGMENT_ID,
                P_JE_NUMBER              => C_PR.JE_NUM,
                P_JE_TYPE                => C_PR.JE_TYP,
                P_JE_CURRENCY_ID         => C_PR.JE_CURRENCY_ID,
                P_JE_DESC                => 'Leave Provision: ' || 'Amount:' ||
                                            C_PR.JE_TOT_DR_AMOUNT,
                P_JE_CURRENCY_RATE_ID    => C_PR.JE_CURRENCY_RATE_ID,
                P_JE_STATUS              => C_PR.JE_STATUS,
                P_JE_CURR_VALUE          => C_PR.JE_CURR_VALUE,
                P_JE_PERIOD_ID           => C_PR.JE_PERIOD_ID,
                P_JE_REFERENCE           => 'HR:Leave Provision:' ||
                                            C_PR.JE_REFERENCE,
                P_JE_EXCHANGE_RATE_VALUE => C_PR.JE_EXCHANGE_RATE_VALUE,
                P_JE_TOT_DR_AMOUNT       => C_PR.JE_TOT_DR_AMOUNT,
                P_JE_TOT_CR_AMOUNT       => C_PR.JE_TOT_CR_AMOUNT,
                P_JE_RECURRING_END_DATE  => C_PR.JE_RECURRING_END_DATE,
                P_JE_RECURRING_FREQUENCY => C_PR.JE_RECURRING_FREQUENCY,
                P_JE_SOURCE              => C_PR.JE_SOURCE,
                P_JOURNAL_REVERSED       => C_PR.JOURNAL_REVERSED,
                P_ACTUAL_JOURNAL_ID      => C_PR.ACTUAL_JOURNAL_ID,
                P_MODULE                 => 'HR');

      FOR C_PR_JRD IN (select null as JE_AMOUNT_CR,
                              null as JE_ACCOUNTED_AMT_CR,
                              'GL_007_D' as JE_DTL_ID,
                              ac.acct_code_id,
                              null as JE_PARTICULARS,
                              'Dr' as JE_CREDIT_DEBIT,
                              sum(round(hl.leave_provision_amt, 3)) as JE_AMOUNT_DR,
                              sum(round(hl.leave_provision_amt, 3)) as JE_ACCOUNTED_AMT_DR,
                              null as JE_SEGMENT_ID_1,
                              null as JE_SEGMENT_ID_2,
                              null as JE_SEGMENT_ID_3,
                              null as JE_SEGMENT_ID_4,
                              null as JE_SEGMENT_ID_5,
                              null as JE_SEGMENT_ID_6
                         from HR_LEAVE_TXN_LEDGER hl,
                              gl_acct_codes       ac,
                              pay_elements        pe,
                              ssm_system_options  ss
                        where ss.hr_earn_leave = ac.acct_code_id
                          and hl.LEAVE_ORG_ID = ac.ACCT_ORG_ID
                          and hl.LEAVE_ORG_ID = P_ORG_ID
                          and pe.PAY_ORG_ID = ac.ACCT_ORG_ID
                          and pe.GL_ID = ss.hr_earn_leave
                          and hl.LEAVE_DEPT_ID = C_PR.dept_id
                          and hl.LEAVE_TXN_DATE = p_date
                          and hl.workflow_completion_status = 1
                          and hl.enabled_flag = 1

                        group by ac.acct_code_id, ac.acct_code
                       union
                       select sum(round(hl.leave_provision_amt, 3)) as JE_AMOUNT_CR,
                              sum(round(hl.leave_provision_amt, 3)) as JE_ACCOUNTED_AMT_CR,
                              'GL_007_D' as JE_DTL_ID,
                              ss.hr_leave_suspens_ac as acct_code_id,
                              null as JE_PARTICULARS,
                              'Cr' as JE_CREDIT_DEBIT,
                              null as JE_AMOUNT_DR,
                              null as JE_ACCOUNTED_AMT_DR,
                              null as JE_SEGMENT_ID_1,
                              null as JE_SEGMENT_ID_2,
                              null as JE_SEGMENT_ID_3,
                              null as JE_SEGMENT_ID_4,
                              null as JE_SEGMENT_ID_5,
                              null as JE_SEGMENT_ID_6
                         from HR_LEAVE_TXN_LEDGER hl, ssm_system_options ss
                        where hl.LEAVE_ORG_ID = P_ORG_ID
                          and ss.org_id = hl.leave_org_id
                          and hl.workflow_completion_status = 1
                          and hl.LEAVE_DEPT_ID = C_PR.dept_id
                          and hl.enabled_flag = 1
                          and hl.LEAVE_TXN_DATE = p_date
                        group by hl.LEAVE_DEPT_ID, ss.hr_leave_suspens_ac) LOOP

        -- FOR C_PR_JRD IN PR_JRD LOOP
        GL_INSERT_JR_DTL(P_JE_DTL_ID           => C_PR_JRD.JE_DTL_ID,
                         P_JE_HDR_ID           => v_JE_HDR_ID,
                         P_JE_ACCT_CODE_ID     => C_PR_JRD.ACCT_CODE_ID,
                         P_JE_PARTICULARS      => C_PR_JRD.JE_PARTICULARS,
                         P_JE_CREDIT_DEBIT     => C_PR_JRD.JE_CREDIT_DEBIT,
                         P_JE_AMOUNT_CR        => C_PR_JRD.JE_AMOUNT_CR,
                         P_JE_ACCOUNTED_AMT_CR => C_PR_JRD.JE_ACCOUNTED_AMT_CR,
                         P_JE_SEGMENT_ID_1     => C_PR_JRD.JE_SEGMENT_ID_1,
                         P_JE_SEGMENT_ID_2     => C_PR_JRD.JE_SEGMENT_ID_2,
                         P_JE_SEGMENT_ID_3     => C_PR_JRD.JE_SEGMENT_ID_3,
                         P_JE_SEGMENT_ID_4     => C_PR_JRD.JE_SEGMENT_ID_4,
                         P_JE_SEGMENT_ID_5     => C_PR_JRD.JE_SEGMENT_ID_5,
                         P_JE_SEGMENT_ID_6     => C_PR_JRD.JE_SEGMENT_ID_6,
                         P_JE_AMOUNT_DR        => C_PR_JRD.JE_AMOUNT_DR,
                         P_JE_ACCOUNTED_AMT_DR => C_PR_JRD.JE_ACCOUNTED_AMT_DR);
      END LOOP;

    END LOOP;
    COMMIT;

  EXCEPTION
    WHEN OTHERS THEN
      NULL;

  END LEAVE_PROVISION_POSTING;

  PROCEDURE PETTY_CASH_EXPEND_POSTING(P_GL_PCE_DTL_ID VARCHAR2,
                                      P_ORG_ID        VARCHAR2) IS
    v_JE_HDR_ID VARCHAR2(50);

  BEGIN

    FOR C_PR IN (SELECT 'GL_007_M' AS JE_HDR_ID,
                        c.comp_id,
                        trunc(SYSDATE) AS JE_DATE,
                        (SELECT sv.segment_value_id
                           FROM GL_COMPANIES_HDR  CH,
                                GL_SEGMENTS       s,
                                gl_segment_values sv
                          WHERE CH.COMP_ID = C.COMP_ID
                            and s.segment_id = sv.segment_id
                            and ch.comp_global_segment_id = s.segment_id
                            AND ROWNUM = 1) as COMP_GLOBAL_SEGMENT_ID,
                        'GL_007_JN' AS JE_NUM,
                        'STANDARD' AS JE_TYP,
                        (select cd.comp_base_currency
                           from GL_COMPANIES_DTL cd
                          where c.comp_id = cd.comp_id
                            and rownum = 1) AS JE_CURRENCY_ID,
                        'PETTY CASH EXPENDITURE' AS JE_DESC,
                        NULL AS JE_CURRENCY_RATE_ID,
                        'OPEN' AS JE_STATUS,
                        NULL AS JE_CURR_VALUE,
                        (select gg.period_id
                           from gl_comp_acct_period_dtl gg,
                                GL_ACCT_CALENDAR_DTL    CDI
                          where gg.enabled_flag = '1'
                            and gg.workflow_completion_status = '1'
                            AND GG.CAL_DTL_ID = CDI.CAL_DTL_ID
                            and gg.PERIOD_STATUS = 'OPEN'
                            and gg.COMP_ID = C.COMP_ID
                            and trunc(hl.gl_expd_date) between
                                gg.period_from_dt and gg.period_to_dt
                            and rownum = 1) AS JE_PERIOD_ID,
                        'PETTY CASH EXPENDITURE' AS JE_REFERENCE,
                        NULL AS JE_EXCHANGE_RATE_VALUE,
                        trunc(sysdate) AS JE_RECURRING_END_DATE,
                        NULL AS JE_RECURRING_FREQUENCY,
                        'PER' AS JE_SOURCE,
                        NULL AS JOURNAL_REVERSED,
                        NULL AS ACTUAL_JOURNAL_ID,
                        (select sum(dd.gl_exp_amount)
                           from GL_PETTY_CASH_EXPENDITURE_DTL dd
                          where hl.gl_pce_hdr_id = dd.gl_pce_hdr_id) as JE_TOT_CR_AMOUNT,
                        (select sum(dd.gl_exp_amount)
                           from GL_PETTY_CASH_EXPENDITURE_DTL dd
                          where hl.gl_pce_hdr_id = dd.gl_pce_hdr_id) as JE_TOT_DR_AMOUNT
                   FROM GL_PETTY_CASH_EXPENDITURE_HDR HL, gl_companies_hdr c
                  where hl.GL_ORG_ID = c.comp_id
                    AND HL.ENABLED_FLAG = 1
                    AND HL.WORKFLOW_COMPLETION_STATUS = 1
                    and c.comp_id = P_ORG_ID
                    and hl.gl_pce_hdr_id = P_GL_PCE_DTL_ID
                  GROUP BY c.comp_id, hl.gl_expd_date, hl.gl_pce_hdr_id)

     LOOP
      v_JE_HDR_ID := ssm.get_next_sequence(C_PR.JE_HDR_ID, 'EN');

      GL_INSERT(P_JE_HDR_ID              => v_JE_HDR_ID,
                p_JE_COMP_ID             => C_PR.COMP_ID,
                P_JE_DATE                => C_PR.JE_DATE,
                P_JE_GLOBAL_SEGMENT_ID   => C_PR.COMP_GLOBAL_SEGMENT_ID,
                P_JE_NUMBER              => C_PR.JE_NUM,
                P_JE_TYPE                => C_PR.JE_TYP,
                P_JE_CURRENCY_ID         => C_PR.JE_CURRENCY_ID,
                P_JE_DESC                => 'Petty Cash Expenditure: ' ||
                                            'Amount:' ||
                                            C_PR.JE_TOT_DR_AMOUNT,
                P_JE_CURRENCY_RATE_ID    => C_PR.JE_CURRENCY_RATE_ID,
                P_JE_STATUS              => C_PR.JE_STATUS,
                P_JE_CURR_VALUE          => C_PR.JE_CURR_VALUE,
                P_JE_PERIOD_ID           => C_PR.JE_PERIOD_ID,
                P_JE_REFERENCE           => 'CM:Petty Cash Expenditure: ' ||
                                            C_PR.JE_REFERENCE,
                P_JE_EXCHANGE_RATE_VALUE => C_PR.JE_EXCHANGE_RATE_VALUE,
                P_JE_TOT_DR_AMOUNT       => C_PR.JE_TOT_DR_AMOUNT,
                P_JE_TOT_CR_AMOUNT       => C_PR.JE_TOT_CR_AMOUNT,
                P_JE_RECURRING_END_DATE  => C_PR.JE_RECURRING_END_DATE,
                P_JE_RECURRING_FREQUENCY => C_PR.JE_RECURRING_FREQUENCY,
                P_JE_SOURCE              => C_PR.JE_SOURCE,
                P_JOURNAL_REVERSED       => C_PR.JOURNAL_REVERSED,
                P_ACTUAL_JOURNAL_ID      => C_PR.ACTUAL_JOURNAL_ID,
                P_MODULE                 => 'CM');

      FOR C_PR_JRD IN (select null as JE_AMOUNT_CR,
                              null as JE_ACCOUNTED_AMT_CR,
                              'GL_007_D' as JE_DTL_ID,
                              dd.gl_acct_code_id as acct_code_id,
                              null as JE_PARTICULARS,
                              'Dr' as JE_CREDIT_DEBIT,
                              sum(round(dd.gl_exp_amount, 3)) as JE_AMOUNT_DR,
                              sum(round(dd.gl_exp_amount, 3)) as JE_ACCOUNTED_AMT_DR,
                              null as JE_SEGMENT_ID_1,
                              null as JE_SEGMENT_ID_2,
                              null as JE_SEGMENT_ID_3,
                              null as JE_SEGMENT_ID_4,
                              null as JE_SEGMENT_ID_5,
                              null as JE_SEGMENT_ID_6
                         from gL_PETTY_CASH_EXPENDITURE_HDR hl,
                              Gl_Petty_Cash_Expenditure_Dtl dd
                        where dd.gl_pce_hdr_id = hl.gl_pce_hdr_id
                          and hl.gl_org_id = P_ORG_ID
                          and hl.gl_pce_hdr_id = P_GL_PCE_DTL_ID
                        group by dd.gl_acct_code_id
                       union
                       select (select sum(round(nvl(dd.gl_exp_amount, 0), 3))
                                 from Gl_Petty_Cash_Expenditure_Dtl dd
                                where hl.gl_pce_hdr_id = dd.gl_pce_hdr_id) as JE_AMOUNT_CR,
                              (select sum(round(nvl(dd.gl_exp_amount, 0), 3))
                                 from Gl_Petty_Cash_Expenditure_Dtl dd
                                where hl.gl_pce_hdr_id = dd.gl_pce_hdr_id) as JE_ACCOUNTED_AMT_CR,
                              'GL_007_D' as JE_DTL_ID,
                              ee.emp_account_code as acct_code_id,
                              null as JE_PARTICULARS,
                              'Cr' as JE_CREDIT_DEBIT,
                              null as JE_AMOUNT_DR,
                              null as JE_ACCOUNTED_AMT_DR,
                              null as JE_SEGMENT_ID_1,
                              null as JE_SEGMENT_ID_2,
                              null as JE_SEGMENT_ID_3,
                              null as JE_SEGMENT_ID_4,
                              null as JE_SEGMENT_ID_5,
                              null as JE_SEGMENT_ID_6
                         from gL_PETTY_CASH_EXPENDITURE_HDR hl,

                              ssm_users                     ss,
                              hr_employees                  ee
                        where ss.user_code = hl.gl_user_id
                          and ss.passport_number_or_emp_id = ee.emp_id

                          and hl.gl_org_id = P_ORG_ID
                          and hl.gl_pce_hdr_id = P_GL_PCE_DTL_ID
                        group by ee.emp_account_code, hl.gl_pce_hdr_id

                       ) LOOP

        -- FOR C_PR_JRD IN PR_JRD LOOP
        GL_INSERT_JR_DTL(P_JE_DTL_ID           => C_PR_JRD.JE_DTL_ID,
                         P_JE_HDR_ID           => v_JE_HDR_ID,
                         P_JE_ACCT_CODE_ID     => C_PR_JRD.ACCT_CODE_ID,
                         P_JE_PARTICULARS      => C_PR_JRD.JE_PARTICULARS,
                         P_JE_CREDIT_DEBIT     => C_PR_JRD.JE_CREDIT_DEBIT,
                         P_JE_AMOUNT_CR        => C_PR_JRD.JE_AMOUNT_CR,
                         P_JE_ACCOUNTED_AMT_CR => C_PR_JRD.JE_ACCOUNTED_AMT_CR,
                         P_JE_SEGMENT_ID_1     => C_PR_JRD.JE_SEGMENT_ID_1,
                         P_JE_SEGMENT_ID_2     => C_PR_JRD.JE_SEGMENT_ID_2,
                         P_JE_SEGMENT_ID_3     => C_PR_JRD.JE_SEGMENT_ID_3,
                         P_JE_SEGMENT_ID_4     => C_PR_JRD.JE_SEGMENT_ID_4,
                         P_JE_SEGMENT_ID_5     => C_PR_JRD.JE_SEGMENT_ID_5,
                         P_JE_SEGMENT_ID_6     => C_PR_JRD.JE_SEGMENT_ID_6,
                         P_JE_AMOUNT_DR        => C_PR_JRD.JE_AMOUNT_DR,
                         P_JE_ACCOUNTED_AMT_DR => C_PR_JRD.JE_ACCOUNTED_AMT_DR);

      END LOOP;
    END LOOP;
    COMMIT;

  EXCEPTION
    WHEN OTHERS THEN
      NULL;

  END PETTY_CASH_EXPEND_POSTING;

  PROCEDURE PETTY_CASH_ALOCATE_POSTING(P_GL_PCE_DTL_ID VARCHAR2,
                                       P_ORG_ID        VARCHAR2) IS
    v_JE_HDR_ID VARCHAR2(50);

  BEGIN

    FOR C_PR IN (SELECT 'GL_007_M' AS JE_HDR_ID,
                        c.comp_id,
                        trunc(SYSDATE) AS JE_DATE,
                        (SELECT sv.segment_value_id
                           FROM GL_COMPANIES_HDR  CH,
                                GL_SEGMENTS       s,
                                gl_segment_values sv
                          WHERE CH.COMP_ID = C.COMP_ID
                            and s.segment_id = sv.segment_id
                            and ch.comp_global_segment_id = s.segment_id
                            AND ROWNUM = 1) as COMP_GLOBAL_SEGMENT_ID,
                        'GL_007_JN' AS JE_NUM,
                        'STANDARD' AS JE_TYP,
                        (select cd.comp_base_currency
                           from GL_COMPANIES_DTL cd
                          where c.comp_id = cd.comp_id
                            and rownum = 1) AS JE_CURRENCY_ID,
                        'PETTY CASH ALLOCATION' AS JE_DESC,
                        NULL AS JE_CURRENCY_RATE_ID,
                        'OPEN' AS JE_STATUS,
                        NULL AS JE_CURR_VALUE,
                        (select gg.period_id
                           from gl_comp_acct_period_dtl gg,
                                GL_ACCT_CALENDAR_DTL    CDI
                          where gg.enabled_flag = '1'
                            and gg.workflow_completion_status = '1'
                            AND GG.CAL_DTL_ID = CDI.CAL_DTL_ID
                            and gg.PERIOD_STATUS = 'OPEN'
                            and gg.COMP_ID = C.COMP_ID
                            and trunc(hl.gl_withdraw_date) between
                                gg.period_from_dt and gg.period_to_dt
                            and rownum = 1) AS JE_PERIOD_ID,
                        'PETTY CASH ALLOCATION' AS JE_REFERENCE,
                        NULL AS JE_EXCHANGE_RATE_VALUE,
                        trunc(SYSDATE) AS JE_RECURRING_END_DATE,
                        NULL AS JE_RECURRING_FREQUENCY,
                        'PER' AS JE_SOURCE,
                        NULL AS JOURNAL_REVERSED,
                        NULL AS ACTUAL_JOURNAL_ID,
                        (select sum(dd.gl_pca_amount)
                           from GL_PETTY_CASH_ALLOCATE_DTL dd
                          where hl.gl_pca_hdr_id = dd.gl_pca_hdr_id) as JE_TOT_CR_AMOUNT,
                        (select sum(dd.gl_pca_amount)
                           from GL_PETTY_CASH_ALLOCATE_DTL dd
                          where hl.gl_pca_hdr_id = dd.gl_pca_hdr_id) as JE_TOT_DR_AMOUNT
                   FROM gl_petty_cash_allocate_hdr HL, gl_companies_hdr c
                  where hl.GL_ORG_ID = c.comp_id
                    AND HL.ENABLED_FLAG = 1
                    AND HL.WORKFLOW_COMPLETION_STATUS = 1
                    and c.comp_id = P_ORG_ID
                    and hl.gl_pca_hdr_id = P_GL_PCE_DTL_ID
                  GROUP BY c.comp_id, hl.gl_withdraw_date, hl.gl_pca_hdr_id)

     LOOP
      v_JE_HDR_ID := ssm.get_next_sequence(C_PR.JE_HDR_ID, 'EN');

      GL_INSERT(P_JE_HDR_ID              => v_JE_HDR_ID,
                p_JE_COMP_ID             => C_PR.COMP_ID,
                P_JE_DATE                => C_PR.JE_DATE,
                P_JE_GLOBAL_SEGMENT_ID   => C_PR.COMP_GLOBAL_SEGMENT_ID,
                P_JE_NUMBER              => C_PR.JE_NUM,
                P_JE_TYPE                => C_PR.JE_TYP,
                P_JE_CURRENCY_ID         => C_PR.JE_CURRENCY_ID,
                P_JE_DESC                => 'Petty Cash Allocation: ' ||
                                            'Amount:' ||
                                            C_PR.JE_TOT_DR_AMOUNT,
                P_JE_CURRENCY_RATE_ID    => C_PR.JE_CURRENCY_RATE_ID,
                P_JE_STATUS              => C_PR.JE_STATUS,
                P_JE_CURR_VALUE          => C_PR.JE_CURR_VALUE,
                P_JE_PERIOD_ID           => C_PR.JE_PERIOD_ID,
                P_JE_REFERENCE           => 'CM:Petty Cash Allocation:' ||
                                            C_PR.JE_REFERENCE,
                P_JE_EXCHANGE_RATE_VALUE => C_PR.JE_EXCHANGE_RATE_VALUE,
                P_JE_TOT_DR_AMOUNT       => C_PR.JE_TOT_DR_AMOUNT,
                P_JE_TOT_CR_AMOUNT       => C_PR.JE_TOT_CR_AMOUNT,
                P_JE_RECURRING_END_DATE  => C_PR.JE_RECURRING_END_DATE,
                P_JE_RECURRING_FREQUENCY => C_PR.JE_RECURRING_FREQUENCY,
                P_JE_SOURCE              => C_PR.JE_SOURCE,
                P_JOURNAL_REVERSED       => C_PR.JOURNAL_REVERSED,
                P_ACTUAL_JOURNAL_ID      => C_PR.ACTUAL_JOURNAL_ID,
                P_MODULE                 => 'CM');

      FOR C_PR_JRD IN (select sum(round(dd.GL_PCA_AMOUNT, 3)) as JE_AMOUNT_CR,
                              sum(round(dd.GL_PCA_AMOUNT, 3)) as JE_ACCOUNTED_AMT_CR,
                              'GL_007_D' as JE_DTL_ID,
                              ss.AP_DEFAULT_CASH_ACCT_CODE AS acct_code_id,
                              null as JE_PARTICULARS,
                              'Cr' as JE_CREDIT_DEBIT,
                              null as JE_AMOUNT_DR,
                              null as JE_ACCOUNTED_AMT_DR,
                              null as JE_SEGMENT_ID_1,
                              null as JE_SEGMENT_ID_2,
                              null as JE_SEGMENT_ID_3,
                              null as JE_SEGMENT_ID_4,
                              null as JE_SEGMENT_ID_5,
                              null as JE_SEGMENT_ID_6
                         from gl_petty_cash_allocate_hdr hl,
                              gl_petty_cash_allocate_dtl dd,
                              ssm_system_options         ss
                        where dd.gl_pca_hdr_id = hl.gl_pca_hdr_id
                          and ss.org_id = hl.gl_org_id
                          and hl.gl_org_id = P_ORG_ID
                          and hl.gl_pca_hdr_id = P_GL_PCE_DTL_ID
                        group by ss.AP_DEFAULT_CASH_ACCT_CODE
                       union
                       select NULL as JE_AMOUNT_CR,
                              NULL as JE_ACCOUNTED_AMT_CR,
                              'GL_007_D' as JE_DTL_ID,
                              ee.emp_account_code AS acct_code_id,
                              null as JE_PARTICULARS,
                              'Dr' as JE_CREDIT_DEBIT,
                              sum(round(dd.GL_PCA_AMOUNT, 3)) as JE_AMOUNT_CR,
                              sum(round(dd.GL_PCA_AMOUNT, 3)) as JE_ACCOUNTED_AMT_CR,
                              null as JE_SEGMENT_ID_1,
                              null as JE_SEGMENT_ID_2,
                              null as JE_SEGMENT_ID_3,
                              null as JE_SEGMENT_ID_4,
                              null as JE_SEGMENT_ID_5,
                              null as JE_SEGMENT_ID_6
                         from gl_petty_cash_allocate_hdr hl,
                              gl_petty_cash_allocate_dtl dd,
                              ssm_users                  sss,
                              hr_employees               ee
                        where ee.emp_id = sss.passport_number_or_emp_id
                          and sss.user_code = dd.gl_pca_user_id
                          and dd.gl_pca_hdr_id = hl.gl_pca_hdr_id
                          and hl.gl_org_id = P_ORG_ID
                          and hl.gl_pca_hdr_id = P_GL_PCE_DTL_ID
                        group by ee.emp_account_code

                       ) LOOP

        -- FOR C_PR_JRD IN PR_JRD LOOP
        GL_INSERT_JR_DTL(P_JE_DTL_ID           => C_PR_JRD.JE_DTL_ID,
                         P_JE_HDR_ID           => v_JE_HDR_ID,
                         P_JE_ACCT_CODE_ID     => C_PR_JRD.ACCT_CODE_ID,
                         P_JE_PARTICULARS      => C_PR_JRD.JE_PARTICULARS,
                         P_JE_CREDIT_DEBIT     => C_PR_JRD.JE_CREDIT_DEBIT,
                         P_JE_AMOUNT_CR        => C_PR_JRD.JE_AMOUNT_CR,
                         P_JE_ACCOUNTED_AMT_CR => C_PR_JRD.JE_ACCOUNTED_AMT_CR,
                         P_JE_SEGMENT_ID_1     => C_PR_JRD.JE_SEGMENT_ID_1,
                         P_JE_SEGMENT_ID_2     => C_PR_JRD.JE_SEGMENT_ID_2,
                         P_JE_SEGMENT_ID_3     => C_PR_JRD.JE_SEGMENT_ID_3,
                         P_JE_SEGMENT_ID_4     => C_PR_JRD.JE_SEGMENT_ID_4,
                         P_JE_SEGMENT_ID_5     => C_PR_JRD.JE_SEGMENT_ID_5,
                         P_JE_SEGMENT_ID_6     => C_PR_JRD.JE_SEGMENT_ID_6,
                         P_JE_AMOUNT_DR        => C_PR_JRD.JE_AMOUNT_DR,
                         P_JE_ACCOUNTED_AMT_DR => C_PR_JRD.JE_ACCOUNTED_AMT_DR);

      END LOOP;

      FOR C_PR_JRD IN (select null as JE_AMOUNT_CR,
                              null as JE_ACCOUNTED_AMT_CR,
                              'GL_007_D' as JE_DTL_ID,
                              ss.AP_DEFAULT_CASH_ACCT_CODE AS acct_code_id,
                              null as JE_PARTICULARS,
                              'Dr' as JE_CREDIT_DEBIT,
                              sum(round(dd.GL_PCA_AMOUNT, 3)) as JE_AMOUNT_DR,
                              sum(round(dd.GL_PCA_AMOUNT, 3)) as JE_ACCOUNTED_AMT_DR,
                              null as JE_SEGMENT_ID_1,
                              null as JE_SEGMENT_ID_2,
                              null as JE_SEGMENT_ID_3,
                              null as JE_SEGMENT_ID_4,
                              null as JE_SEGMENT_ID_5,
                              null as JE_SEGMENT_ID_6
                         from gl_petty_cash_allocate_hdr hl,
                              gl_petty_cash_allocate_dtl dd,
                              ssm_system_options         ss
                        where dd.gl_pca_hdr_id = hl.gl_pca_hdr_id
                          and ss.org_id = hl.gl_org_id
                          and hl.gl_org_id = P_ORG_ID
                          and hl.gl_pca_hdr_id = P_GL_PCE_DTL_ID
                        group by ss.AP_DEFAULT_CASH_ACCT_CODE
                       union
                       select sum(round(dd.GL_PCA_AMOUNT, 3)) as JE_AMOUNT_CR,
                              sum(round(dd.GL_PCA_AMOUNT, 3)) as JE_ACCOUNTED_AMT_CR,
                              'GL_007_D' as JE_DTL_ID,
                              ab.gl_account AS acct_code_id,
                              null as JE_PARTICULARS,
                              'Cr' as JE_CREDIT_DEBIT,
                              null as JE_AMOUNT_CR,
                              null as JE_ACCOUNTED_AMT_CR,
                              null as JE_SEGMENT_ID_1,
                              null as JE_SEGMENT_ID_2,
                              null as JE_SEGMENT_ID_3,
                              null as JE_SEGMENT_ID_4,
                              null as JE_SEGMENT_ID_5,
                              null as JE_SEGMENT_ID_6
                         from gl_petty_cash_allocate_hdr hl,
                              gl_petty_cash_allocate_dtl dd,
                              ca_bank_accounts           ab
                        where dd.gl_pca_hdr_id = hl.gl_pca_hdr_id
                          and ab.bank_id = hl.gl_pca_bank_id
                          and ab.bank_branch_id = hl.gl_pca_branch_id
                          and ab.account_id = hl.gl_pca_acct_number
                          and hl.gl_org_id = P_ORG_ID
                          and hl.gl_pca_hdr_id = P_GL_PCE_DTL_ID
                        group by ab.gl_account

                       ) LOOP

        -- FOR C_PR_JRD IN PR_JRD LOOP
        GL_INSERT_JR_DTL(P_JE_DTL_ID           => C_PR_JRD.JE_DTL_ID,
                         P_JE_HDR_ID           => v_JE_HDR_ID,
                         P_JE_ACCT_CODE_ID     => C_PR_JRD.ACCT_CODE_ID,
                         P_JE_PARTICULARS      => C_PR_JRD.JE_PARTICULARS,
                         P_JE_CREDIT_DEBIT     => C_PR_JRD.JE_CREDIT_DEBIT,
                         P_JE_AMOUNT_CR        => C_PR_JRD.JE_AMOUNT_CR,
                         P_JE_ACCOUNTED_AMT_CR => C_PR_JRD.JE_ACCOUNTED_AMT_CR,
                         P_JE_SEGMENT_ID_1     => C_PR_JRD.JE_SEGMENT_ID_1,
                         P_JE_SEGMENT_ID_2     => C_PR_JRD.JE_SEGMENT_ID_2,
                         P_JE_SEGMENT_ID_3     => C_PR_JRD.JE_SEGMENT_ID_3,
                         P_JE_SEGMENT_ID_4     => C_PR_JRD.JE_SEGMENT_ID_4,
                         P_JE_SEGMENT_ID_5     => C_PR_JRD.JE_SEGMENT_ID_5,
                         P_JE_SEGMENT_ID_6     => C_PR_JRD.JE_SEGMENT_ID_6,
                         P_JE_AMOUNT_DR        => C_PR_JRD.JE_AMOUNT_DR,
                         P_JE_ACCOUNTED_AMT_DR => C_PR_JRD.JE_ACCOUNTED_AMT_DR);

      END LOOP;

    END LOOP;
    COMMIT;

  EXCEPTION
    WHEN OTHERS THEN
      NULL;

  END PETTY_CASH_ALOCATE_POSTING;

END GL_POSTING;
/
