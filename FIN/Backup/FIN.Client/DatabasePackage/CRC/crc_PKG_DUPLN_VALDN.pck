CREATE OR REPLACE PACKAGE PKG_DUPLN_VALDN IS
  GV_STR NVARCHAR2(4000) := NULL;

  PROCEDURE PROC_DUPLN_VALIDATION_ONE(P_SCREEN_CODE IN VARCHAR2,
                                      P_PK_ID       IN VARCHAR2,
                                      P_VALUE1      IN VARCHAR2,
                                      P_RET         OUT VARCHAR2,
                                      P_ORG_ID      IN VARCHAR2 DEFAULT NULL);

  FUNCTION FN_VALIDATE_DUPLICATE_ONE(P_TABLE_NAME  IN VARCHAR2,
                                     P_PK_COL_NAME IN VARCHAR2,
                                     P_PK_ID       IN VARCHAR2,
                                     P_COL_NAME    IN VARCHAR2,
                                     P_VALUE1      IN VARCHAR2,
                                     V_ORG_ID      IN VARCHAR2,
                                     P_ORG_ID      IN VARCHAR2) RETURN NUMBER;

  PROCEDURE PROC_DUPLN_VALIDATION_TWO(P_SCREEN_CODE IN VARCHAR2,
                                      P_PK_ID       IN VARCHAR2,
                                      P_VALUE1      IN VARCHAR2,
                                      P_VALUE2      IN VARCHAR2,
                                      P_RET         OUT VARCHAR2,
                                      P_ORG_ID      IN VARCHAR2 DEFAULT NULL);

  FUNCTION FN_VALIDATE_DUPLICATE_TWO(P_TABLE_NAME  IN VARCHAR2,
                                     P_PK_COL_NAME IN VARCHAR2,
                                     P_PK_ID       IN VARCHAR2,
                                     P_COL_NAME1   IN VARCHAR2,
                                     P_COL_NAME2   IN VARCHAR2,
                                     P_VALUE1      IN VARCHAR2,
                                     P_VALUE2      IN VARCHAR2,
                                     V_ORG_ID      IN VARCHAR2,
                                     P_ORG_ID      IN VARCHAR2) RETURN NUMBER;

  PROCEDURE PROC_DUPLN_VALIDATION_THREE(P_SCREEN_CODE IN VARCHAR2,
                                        P_PK_ID       IN VARCHAR2,
                                        P_VALUE1      IN VARCHAR2,
                                        P_VALUE2      IN VARCHAR2,
                                        P_VALUE3      IN VARCHAR2,
                                        P_RET         OUT VARCHAR2,
                                        P_ORG_ID      IN VARCHAR2 DEFAULT NULL);

  FUNCTION FN_VALIDATE_DUPLICATE_THREE(P_TABLE_NAME  IN VARCHAR2,
                                       P_PK_COL_NAME IN VARCHAR2,
                                       P_PK_ID       IN VARCHAR2,
                                       P_COL_NAME1   IN VARCHAR2,
                                       P_COL_NAME2   IN VARCHAR2,
                                       P_COL_NAME3   IN VARCHAR2,
                                       P_VALUE1      IN VARCHAR2,
                                       P_VALUE2      IN VARCHAR2,
                                       P_VALUE3      IN VARCHAR2,
                                       V_ORG_ID      IN VARCHAR2,
                                       P_ORG_ID      IN VARCHAR2)
    RETURN NUMBER;

END PKG_DUPLN_VALDN;
 
 
 
 
 
 
 
/
CREATE OR REPLACE PACKAGE BODY PKG_DUPLN_VALDN AS


  PROCEDURE PROC_DUPLN_VALIDATION_ONE(P_SCREEN_CODE IN VARCHAR2,
                                  P_PK_ID       IN VARCHAR2,
                                  P_VALUE1      IN VARCHAR2,
                                  P_RET         OUT VARCHAR2,
                                  P_ORG_ID      IN VARCHAR2 DEFAULT NULL) IS
    v_table_name     VARCHAR2(2000);
    v_column_nm      VARCHAR2(2000);
    pk_column_nm     VARCHAR2(1000);
    V_ORG_ID         VARCHAR2(100);
    v_count          NUMBER;
    err_code         VARCHAR2(1000);
    lv_sql_statement VARCHAR2(4000);

  BEGIN

    BEGIN
      select ss.table_name, SMI.Column1, SMI.Pk_Column_Name,SMI.ORG_ID
        into v_table_name, v_column_nm, pk_column_nm,V_ORG_ID
        from SSM_SCREENS SS, SSM_MENU_ITEMS SMI
       where ss.screen_code = smi.form_code
         and ss.screen_code = P_SCREEN_CODE;
    EXCEPTION
      WHEN OTHERS THEN
        P_RET := 'Error :' || SQLCODE || '-' || SQLERRM;
    END;
    BEGIN
      select SMI.Error_Code
        into err_code
        from SSM_SCREENS SS, SSM_MENU_ITEMS SMI
       where ss.screen_code = smi.form_code
         and ss.screen_code = P_SCREEN_CODE;
    EXCEPTION
      WHEN OTHERS THEN
        P_RET := 'Error :' || SQLCODE || '-' || SQLERRM;
    END;

    IF (FN_VALIDATE_DUPLICATE_ONE(v_table_name,
                              pk_column_nm,
                              P_PK_ID,
                              v_column_nm,
                              P_VALUE1,
                              V_ORG_ID,
                              P_ORG_ID) = 1) THEN

      GV_STR := SSM.Get_Err_Message('EN', err_code);
      ELSE
      GV_STR := NULL;

    END IF;
    P_RET := NVL(GV_STR, 0);
  EXCEPTION
    WHEN OTHERS THEN
      P_RET := 'Error: ' || SQLCODE || ' - ' || SQLERRM;
  END PROC_DUPLN_VALIDATION_ONE;

  FUNCTION FN_VALIDATE_DUPLICATE_ONE(P_TABLE_NAME  IN VARCHAR2,
                                 P_PK_COL_NAME IN VARCHAR2,
                                 P_PK_ID       IN VARCHAR2,
                                 P_COL_NAME    IN VARCHAR2,
                                 P_VALUE1      IN VARCHAR2,
                                /* P_COL_NAME2    IN VARCHAR2,
                                 P_VALUE2      IN VARCHAR2,*/
                                 V_ORG_ID      IN VARCHAR2,
                                 P_ORG_ID      IN VARCHAR2) RETURN NUMBER IS
    lv_sql_statement VARCHAR2(4000);
    v_count          NUMBER;
    v_col_name2 VARCHAR2(4000);
  BEGIN
  /*v_col_name2:=P_COL_NAME2;*/
    lv_sql_statement := 'Select count(*) as counts from' || ' ' ||
                        P_TABLE_NAME || ' where ' || P_PK_COL_NAME || '!= ' || '''' ||
                        P_PK_ID || '''' || ' and ' || P_COL_NAME || '=' || '''' || P_VALUE1 || '''' ||
                  /*   IF (v_col_name2 IS NOT NULL) THEN
                       \* ' and ' || P_COL_NAME2 || '=' || '''' || P_VALUE2 || '''' || *\
                         END IF;*/

                       ' and ENABLED_FLAG = 1';

   IF(LENGTH(V_ORG_ID) >0) THEN

      lv_sql_statement :=  lv_sql_statement   || ' AND ' || V_ORG_ID || ' ='||'''' ||  P_ORG_ID || '''' ;
   END IF;

    EXECUTE IMMEDIATE lv_sql_statement
      into v_count;

    IF (v_count >= 1) then
      RETURN(1);
    ELSE
      RETURN(0);
    END IF;

  END FN_VALIDATE_DUPLICATE_ONE;



 PROCEDURE PROC_DUPLN_VALIDATION_TWO(P_SCREEN_CODE IN VARCHAR2,
                                  P_PK_ID       IN VARCHAR2,
                                  P_VALUE1      IN VARCHAR2,
                                  P_VALUE2      IN VARCHAR2,
                                  P_RET         OUT VARCHAR2,
                                  P_ORG_ID      IN VARCHAR2 DEFAULT NULL) IS
    v_table_name     VARCHAR2(2000);
    v_column_nm1      VARCHAR2(2000);
    v_column_nm2      VARCHAR2(2000);
    pk_column_nm     VARCHAR2(1000);
    V_ORG_ID         VARCHAR2(200);
    v_count          NUMBER;
    err_code         VARCHAR2(1000);
    lv_sql_statement VARCHAR2(4000);
    GV_STR NVARCHAR2(4000) := NULL;

  BEGIN

    BEGIN
      select ss.table_name, SMI.Column1,SMI.Column2, SMI.Pk_Column_Name,SMI.ORG_ID
        into v_table_name, v_column_nm1,v_column_nm2, pk_column_nm,V_ORG_ID
        from SSM_SCREENS SS, SSM_MENU_ITEMS SMI
       where ss.screen_code = smi.form_code
         and ss.screen_code = P_SCREEN_CODE;
    EXCEPTION
      WHEN OTHERS THEN
        P_RET := 'Error :' || SQLCODE || '-' || SQLERRM;
    END;
    BEGIN
      select SMI.Error_Code
        into err_code
        from SSM_SCREENS SS, SSM_MENU_ITEMS SMI
       where ss.screen_code = smi.form_code
         and ss.screen_code = P_SCREEN_CODE;
    EXCEPTION
      WHEN OTHERS THEN
        P_RET := 'Error :' || SQLCODE || '-' || SQLERRM;
    END;

    IF (FN_VALIDATE_DUPLICATE_TWO(v_table_name,
                              pk_column_nm,
                              P_PK_ID,
                              v_column_nm1,
                              v_column_nm2,
                              P_VALUE1,
                              P_VALUE2,
                              V_ORG_ID,
                              P_ORG_ID) = 1) THEN

      GV_STR := SSM.Get_Err_Message('EN', err_code);

      ELSE
      GV_STR  := NULL;
    END IF;
    P_RET := NVL(GV_STR, 0);
  EXCEPTION
    WHEN OTHERS THEN
      P_RET := 'Error: ' || SQLCODE || ' - ' || SQLERRM;
  END PROC_DUPLN_VALIDATION_TWO;


FUNCTION FN_VALIDATE_DUPLICATE_TWO(P_TABLE_NAME     IN VARCHAR2,
                                 P_PK_COL_NAME  IN VARCHAR2,
                                 P_PK_ID        IN VARCHAR2,
                                 P_COL_NAME1    IN VARCHAR2,
                                 P_COL_NAME2    IN VARCHAR2,
                                 P_VALUE1       IN VARCHAR2,
                                 P_VALUE2       IN VARCHAR2,
                                 V_ORG_ID       IN VARCHAR2,
                                 P_ORG_ID       IN VARCHAR2) RETURN NUMBER IS
    lv_sql_statement VARCHAR2(4000);
    v_count          NUMBER;
  BEGIN

    lv_sql_statement := 'Select count(*) as counts from' || ' ' ||
                        P_TABLE_NAME || ' where ' || P_PK_COL_NAME || '!= ' || '''' ||
                        P_PK_ID || '''' || ' and ' || P_COL_NAME1 || '=' || '''' ||
                        P_VALUE1 || '''' || ' and ' || P_COL_NAME2 || '=' || '''' ||
                        P_VALUE2 || ''''  ||' and ENABLED_FLAG = 1';

   IF(LENGTH(V_ORG_ID) >0) THEN
       lv_sql_statement :=  lv_sql_statement   || ' AND ' || V_ORG_ID || ' ='||'''' ||  P_ORG_ID || '''' ;
   END IF;

    EXECUTE IMMEDIATE lv_sql_statement
      into v_count;

    IF (v_count >= 1) then
      RETURN(1);
    ELSE
      RETURN(0);
    END IF;

  END FN_VALIDATE_DUPLICATE_TWO;




PROCEDURE PROC_DUPLN_VALIDATION_THREE(P_SCREEN_CODE IN VARCHAR2,
                                  P_PK_ID       IN VARCHAR2,
                                  P_VALUE1      IN VARCHAR2,
                                  P_VALUE2      IN VARCHAR2,
                                  P_VALUE3      IN VARCHAR2,
                                  P_RET         OUT VARCHAR2,
                                  P_ORG_ID      IN VARCHAR2) IS
    v_table_name      VARCHAR2(2000);
    v_column_nm1      VARCHAR2(2000);
    v_column_nm2      VARCHAR2(2000);
    v_column_nm3      VARCHAR2(2000);
    V_ORG_ID          VARCHAR2(200);
    pk_column_nm      VARCHAR2(1000);

    v_count          NUMBER;
    err_code         VARCHAR2(1000);
    lv_sql_statement VARCHAR2(4000);
    GV_STR NVARCHAR2(4000) := NULL;

  BEGIN

    BEGIN
      select ss.table_name, SMI.Column1,SMI.Column2,SMI.Column3,SMI.Pk_Column_Name,SMI.ORG_ID
        into v_table_name, v_column_nm1,v_column_nm2,v_column_nm3,pk_column_nm,V_ORG_ID
        from SSM_SCREENS SS, SSM_MENU_ITEMS SMI
       where ss.screen_code = smi.form_code
         and ss.screen_code = P_SCREEN_CODE;
    EXCEPTION
      WHEN OTHERS THEN
        P_RET := 'Error :' || SQLCODE || '-' || SQLERRM;
    END;
    BEGIN
      select SMI.Error_Code
        into err_code
        from SSM_SCREENS SS, SSM_MENU_ITEMS SMI
       where ss.screen_code = smi.form_code
         and ss.screen_code = P_SCREEN_CODE;
    EXCEPTION
      WHEN OTHERS THEN
        P_RET := 'Error :' || SQLCODE || '-' || SQLERRM;
    END;

    IF (FN_VALIDATE_DUPLICATE_THREE(v_table_name,
                              pk_column_nm,
                              P_PK_ID,
                              v_column_nm1,
                              v_column_nm2,
                              v_column_nm3,
                              P_VALUE1,
                              P_VALUE2,
                              P_VALUE3,
                              V_ORG_ID,
                              P_ORG_ID) = 1) THEN

      GV_STR := SSM.Get_Err_Message('EN', err_code);

      ELSE
      GV_STR  := NULL;
    END IF;
    P_RET := NVL(GV_STR, 0);
  EXCEPTION
    WHEN OTHERS THEN
      P_RET := 'Error: ' || SQLCODE || ' - ' || SQLERRM;
  END PROC_DUPLN_VALIDATION_THREE;


FUNCTION FN_VALIDATE_DUPLICATE_THREE(P_TABLE_NAME     IN VARCHAR2,
                                 P_PK_COL_NAME  IN VARCHAR2,
                                 P_PK_ID        IN VARCHAR2,
                                 P_COL_NAME1    IN VARCHAR2,
                                 P_COL_NAME2    IN VARCHAR2,
                                 P_COL_NAME3    IN VARCHAR2,
                                 P_VALUE1       IN VARCHAR2,
                                 P_VALUE2       IN VARCHAR2,
                                 P_VALUE3       IN VARCHAR2,
                                 V_ORG_ID       IN VARCHAR2,
                                 P_ORG_ID       IN VARCHAR2) RETURN NUMBER IS
    lv_sql_statement VARCHAR2(4000);
    v_count          NUMBER;
  BEGIN

    lv_sql_statement := 'Select count(*) as counts from' || ' ' ||
                        P_TABLE_NAME || ' where ' || P_PK_COL_NAME || '!= ' || '''' ||
                        P_PK_ID  || ''''  || ' and '  ||  P_COL_NAME1 || '=' || '''' ||
                        P_VALUE1 || ''''  || ' and ' || P_COL_NAME2 || '=' || '''' ||
                        P_VALUE2 || ''''  ||' and ' || P_COL_NAME3 || '=' || '''' ||
                        P_VALUE3 || ''''  ||' and ENABLED_FLAG = 1';

    IF(LENGTH(V_ORG_ID) >0) THEN
       lv_sql_statement :=  lv_sql_statement   || ' AND ' || V_ORG_ID || ' ='||'''' ||  P_ORG_ID || '''' ;
   END IF;
    EXECUTE IMMEDIATE lv_sql_statement
      into v_count;

    IF (v_count >= 1) then
      RETURN(1);
    ELSE
      RETURN(0);
    END IF;

  END FN_VALIDATE_DUPLICATE_THREE;


END PKG_DUPLN_VALDN;
/
