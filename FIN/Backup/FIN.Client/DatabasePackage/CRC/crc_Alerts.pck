CREATE OR REPLACE PACKAGE Alerts IS
  V_EXISTS BOOLEAN;
  PROCEDURE GENERATE_ALL_ALERTS;
  PROCEDURE generate_alert(alertcode VARCHAR2,
                           usercode  VARCHAR2,
                           langcode  VARCHAR2,
                           message   VARCHAR2,
                           txn_code  VARCHAR2,
                           p_ORG_ID  VARCHAR2);
  PROCEDURE Generate_Alert_Emp(alertcode VARCHAR2,
                               usercode  VARCHAR2,
                               langcode  VARCHAR2,
                               message   VARCHAR2,
                               txn_code  VARCHAR2,
                               from_emp  VARCHAR2,
                               to_emp    VARCHAR2,
                               p_ORG_ID  VARCHAR2);
  Procedure Generate_Email_Alert(from_name varchar2,
                                 to_name   varchar2,
                                 subject   varchar2,
                                 message   varchar2);
  PROCEDURE generate_escalation_alert(ALERTCODE   VARCHAR2,
                                      P_USERCODE  VARCHAR2,
                                      LANGCODE    VARCHAR2,
                                      MESSAGE     VARCHAR2,
                                      TXN_CODE    VARCHAR2,
                                      P_ROLE_CODE VARCHAR2);

  PROCEDURE escalation_time(P_USER_CODE VARCHAR2);

  PROCEDURE SCHEDULED_DOC_EXPIRED_ALERT;
  PROCEDURE SCHEDULED_EMP_PROBATION_ALERT;

END Alerts;

 
 
 
 
 
 
 
/
CREATE OR REPLACE PACKAGE BODY Alerts IS
  PROCEDURE GENERATE_ALL_ALERTS IS
  BEGIN
    NULL;
  END GENERATE_ALL_ALERTS;

  PROCEDURE generate_alert(alertcode VARCHAR2,
                           usercode  VARCHAR2,
                           langcode  VARCHAR2,
                           message   VARCHAR2,
                           txn_code  VARCHAR2,
                           p_ORG_ID  VARCHAR2) IS
    v_errmsg  VARCHAR2(100);
    v_code    SSM_ALERT_USER_LEVELS.alert_user_level_code%TYPE;
    TIMES     NUMBER;
    SET_TIMES NUMBER;
    CODE      VARCHAR2(50);
  
    CURSOR USER IS
      SELECT uri.user_code, am.*
        FROM SSM_ALERT_MASTERS am, SSM_USER_ROLE_INTERSECTS uri
       WHERE am.alert_code = alertcode
         AND UPPER(uri.role_code) = UPPER(am.role_code)
         AND am.language_code = UPPER(langcode)
         AND uri.ENABLED_FLAG = 1
         AND uri.WORKFLOW_COMPLETION_STATUS = 1
         AND am.ENABLED_FLAG = 1
         AND am.WORKFLOW_COMPLETION_STATUS = 1;
  
  BEGIN
  
    BEGIN
      SELECT NVL(MAX(NO_OF_TIMES), 0)
        INTO TIMES
        FROM SSM_ALERT_USER_LEVELS T
       WHERE T.ALERT_CODE = alertcode
         AND T.TRANSACTION_CODE = txn_code
         AND T.WORKFLOW_COMPLETION_STATUS = '1'
         AND T.LANGUAGE_CODE = langcode;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        TIMES := 0;
    END;
  
    BEGIN
      SELECT NVL(M.FREQUENCY_OF_DISPLAY, 1)
        INTO SET_TIMES
        FROM SSM_ALERT_MASTERS M
       WHERE M.ALERT_CODE = alertcode
         AND M.LANGUAGE_CODE = langcode;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        SET_TIMES := 1;
    END;
    IF TIMES < SET_TIMES THEN
    
      TIMES := NVL(TIMES, 0) + 1;
      FOR user_rec IN USER LOOP
      
        SELECT SSM_ALT_USRLVL_CODE_SEQ.NEXTVAL INTO CODE FROM DUAL;
      
        INSERT INTO SSM_ALERT_USER_LEVELS
          (alert_user_level_code,
           alert_code,
           language_code,
           user_code,
           alert_message,
           last_displayed_time,
           no_of_alerts_displayed,
           effective_date,
           end_date,
           remarks,
           enabled_flag,
           workflow_completion_status,
           created_by,
           created_date,
           modified_by,
           modified_date,
           TRANSACTION_CODE,
           NO_OF_TIMES,
           ORG_ID)
        VALUES
          (CODE,
           alertcode,
           user_rec.language_code,
           user_rec.user_code,
           message, --user_rec.alert_message,
           SYSDATE,
           1,
           SYSDATE,
           NULL,
           NULL,
           '1',
           '1',
           usercode,
           SYSDATE,
           NULL,
           NULL,
           txn_code,
           TIMES,
           p_ORG_ID);
      
      END LOOP;
    END IF;
    COMMIT;
  END Generate_Alert;

  Procedure Generate_Email_Alert(from_name varchar2,
                                 to_name   varchar2,
                                 subject   varchar2,
                                 message   varchar2) is
    l_mailhost     VARCHAR2(64) := 'vmvsystems.co.in';
    l_from         VARCHAR2(64) := 'vmvsystems@vmvsystems.co.in';
    l_to           VARCHAR2(64) := 'natesh@vmvsystems.co.in';
    l_mail_conn    sys.UTL_SMTP.connection;
    v_username_b64 varchar2(500);
    v_username     varchar2(500) := 'natesh@vmvsystems.co.in';
    v_password_b64 varchar2(500);
    v_password     varchar2(500) := 'natesh';
  BEGIN
    -- l_mail_conn := sys.UTL_SMTP.open_connection(l_mailhost, 25);
    v_username_b64 := sys.UTL_RAW.cast_to_varchar2(sys.UTL_ENCODE.base64_encode(sys.UTL_RAW.cast_to_raw(v_username)));
    v_password_b64 := sys.UTL_RAW.cast_to_varchar2(sys.UTL_ENCODE.base64_encode(sys.UTL_RAW.cast_to_raw(v_password)));
    l_mail_conn    := sys.UTL_SMTP.open_connection(l_mailhost);
    sys.UTL_SMTP.ehlo(l_mail_conn, 'mydomain.com'); -- Must use EHLO  vs HELO
    sys.UTL_SMTP.command(l_mail_conn, 'AUTH', 'LOGIN'); -- should receive a 334 response, ----prompting for username
    sys.UTL_SMTP.command(l_mail_conn, v_username_b64); -- should receive a 334 response, ----prompting for password
    sys.UTL_SMTP.command(l_mail_conn, v_password_b64);
  
    sys.UTL_SMTP.helo(l_mail_conn, l_mailhost);
    sys.UTL_SMTP.mail(l_mail_conn, l_from);
    sys.UTL_SMTP.rcpt(l_mail_conn, l_to);
  
    sys.UTL_SMTP.open_data(l_mail_conn);
  
    sys.UTL_SMTP.write_data(l_mail_conn,
                            'Date: ' ||
                            TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                            Chr(13));
    sys.UTL_SMTP.write_data(l_mail_conn, 'From: ' || l_from || Chr(13));
    sys.UTL_SMTP.write_data(l_mail_conn, 'Subject: ' || subject || Chr(13));
    sys.UTL_SMTP.write_data(l_mail_conn, 'To: ' || l_to || Chr(13));
    sys.UTL_SMTP.write_data(l_mail_conn, '' || Chr(13));
  
    FOR i IN 1 .. 10 LOOP
      sys.UTL_SMTP.write_data(l_mail_conn,
                              'This is a test message. Line ' || To_Char(i) ||
                              Chr(13));
    END LOOP;
  
    sys.UTL_SMTP.close_data(l_mail_conn);
  
    sys.UTL_SMTP.quit(l_mail_conn);
  END Generate_Email_Alert;

  PROCEDURE Generate_Alert_Emp(alertcode VARCHAR2,
                               usercode  VARCHAR2,
                               langcode  VARCHAR2,
                               message   VARCHAR2,
                               txn_code  VARCHAR2,
                               from_emp  VARCHAR2,
                               to_emp    VARCHAR2,
                               p_ORG_ID  VARCHAR2) IS
    v_errmsg  VARCHAR2(100);
    v_code    SSM_ALERT_USER_LEVELS.alert_user_level_code%TYPE;
    TIMES     NUMBER;
    SET_TIMES NUMBER;
    CODE      VARCHAR2(50);
  
  BEGIN
  
    /*    BEGIN
         SELECT NVL(MAX(NO_OF_TIMES), 0)
           INTO TIMES
           FROM SSM_ALERT_USER_LEVELS T
          WHERE T.ALERT_CODE = alertcode
            AND T.TRANSACTION_CODE = txn_code
            AND T.WORKFLOW_COMPLETION_STATUS = '1'
            AND T.LANGUAGE_CODE = langcode;
       EXCEPTION
         WHEN NO_DATA_FOUND THEN
           TIMES := 0;
       END;
    */
  
    TIMES := 0;
  
    BEGIN
      SELECT NVL(M.FREQUENCY_OF_DISPLAY, 1)
        INTO SET_TIMES
        FROM SSM_ALERT_MASTERS M
       WHERE M.ALERT_CODE = alertcode
         AND M.LANGUAGE_CODE = langcode;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        SET_TIMES := 1;
    END;
    IF TIMES < SET_TIMES THEN
    
      TIMES := NVL(TIMES, 0) + 1;
    
      SELECT SSM_ALT_USRLVL_CODE_SEQ.NEXTVAL INTO CODE FROM DUAL;
    
      INSERT INTO SSM_ALERT_USER_LEVELS
        (alert_user_level_code,
         alert_code,
         language_code,
         user_code,
         alert_message,
         last_displayed_time,
         no_of_alerts_displayed,
         effective_date,
         end_date,
         remarks,
         enabled_flag,
         workflow_completion_status,
         created_by,
         created_date,
         modified_by,
         modified_date,
         TRANSACTION_CODE,
         NO_OF_TIMES,
         attribute1,
         ORG_ID)
      VALUES
        (CODE,
         alertcode,
         langcode,
         to_emp,
         message, --user_rec.alert_message,
         SYSDATE,
         1,
         SYSDATE,
         NULL,
         NULL,
         '1',
         '1',
         usercode,
         SYSDATE,
         NULL,
         NULL,
         txn_code,
         TIMES,
         from_emp,
         p_ORG_ID);
    
    END IF;
    COMMIT;
  END Generate_Alert_Emp;

  PROCEDURE generate_escalation_alert(ALERTCODE   VARCHAR2,
                                      P_USERCODE  VARCHAR2,
                                      LANGCODE    VARCHAR2,
                                      MESSAGE     VARCHAR2,
                                      TXN_CODE    VARCHAR2,
                                      P_ROLE_CODE VARCHAR2) IS
    V_ERRMSG  VARCHAR2(100);
    V_CODE    SSM_ALERT_USER_LEVELS.ALERT_USER_LEVEL_CODE%TYPE;
    TIMES     NUMBER;
    SET_TIMES NUMBER;
    CODE      VARCHAR2(50);
  
    CURSOR c_user IS
      SELECT users.user_code
        FROM ssm_users users, ssm_user_role_intersects uroles
       WHERE uroles.user_code = users.user_code
         AND uroles.enabled_flag = 1
         AND uroles.workflow_completion_status = 1
         AND users.enabled_flag = 1
         AND users.workflow_completion_status = 1
         AND uroles.role_code = P_ROLE_CODE
         AND users.user_code = P_USERCODE;
    --AND users.GROUP_ID = p_group_code;
  
  BEGIN
  
    BEGIN
      SELECT NVL(MAX(NO_OF_TIMES), 0)
        INTO TIMES
        FROM SSM_ALERT_USER_LEVELS T
       WHERE T.ALERT_CODE = ALERTCODE
         AND T.TRANSACTION_CODE = TXN_CODE
         AND T.WORKFLOW_COMPLETION_STATUS = '1'
         AND T.LANGUAGE_CODE = LANGCODE;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        TIMES := 0;
    END;
  
    BEGIN
      SELECT NVL(M.FREQUENCY_OF_DISPLAY, 1)
        INTO SET_TIMES
        FROM SSM_ALERT_MASTERS M
       WHERE M.ALERT_CODE = ALERTCODE
         AND M.LANGUAGE_CODE = LANGCODE;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        SET_TIMES := 1;
    END;
    IF TIMES < SET_TIMES THEN
    
      TIMES := NVL(TIMES, 0) + 1;
      FOR USER_REC IN C_USER LOOP
        SELECT SSM_ALT_USRLVL_CODE_SEQ.NEXTVAL INTO CODE FROM DUAL;
      
        INSERT INTO SSM_ALERT_USER_LEVELS
          (ALERT_USER_LEVEL_CODE,
           ALERT_CODE,
           LANGUAGE_CODE,
           USER_CODE,
           ALERT_MESSAGE,
           LAST_DISPLAYED_TIME,
           NO_OF_ALERTS_DISPLAYED,
           EFFECTIVE_DATE,
           END_DATE,
           REMARKS,
           ENABLED_FLAG,
           WORKFLOW_COMPLETION_STATUS,
           CREATED_BY,
           CREATED_DATE,
           MODIFIED_BY,
           MODIFIED_DATE,
           TRANSACTION_CODE,
           NO_OF_TIMES)
        VALUES
          (CODE,
           ALERTCODE,
           LANGCODE,
           USER_REC.USER_CODE,
           MESSAGE, --user_rec.alert_message,
           SYSDATE,
           1,
           SYSDATE,
           NULL,
           NULL,
           '1',
           '1',
           USER,
           SYSDATE,
           NULL,
           NULL,
           TXN_CODE,
           TIMES);
      
      END LOOP;
    END IF;
    COMMIT;
  END generate_escalation_alert;

  --Author: RakeshKumar.Varanasi
  PROCEDURE escalation_time(p_user_code VARCHAR2) IS
    CURSOR c_waiting_for_approval IS
      SELECT wm.*
        FROM wfm_workflow_monitors wm
       WHERE wm.status NOT IN ('WFST01', 'WFST02')
         AND approved_already =
             (SELECT MAX(approved_already)
                FROM wfm_workflow_monitors
               WHERE status NOT IN ('WFST01', 'WFST02')
                 AND workflow_code = wm.workflow_code
                 AND transaction_code = wm.transaction_code)
         AND level_code =
             (SELECT MIN(level_code)
                FROM wfm_workflow_monitors
               WHERE status NOT IN ('WFST01', 'WFST02')
                 AND workflow_code = wm.workflow_code
                 AND transaction_code = wm.transaction_code
                 AND role_code IN
                     (SELECT role_code
                        FROM ssm_user_role_intersects
                       WHERE UPPER(user_code) = UPPER(p_user_code)
                         AND workflow_completion_status = '1'
                         AND enabled_flag = '1'))
      --   AND wm.transaction_code = 'EC_000000000000024'
       ORDER BY created_date;
  
    CURSOR c_escalation(p_workflow_code VARCHAR2, p_role_code VARCHAR2) IS
      SELECT wwcm.workflow_code, wwl.escalation_type, wwl.escalation_value
        FROM wfm_workflow_code_masters wwcm, wfm_workflow_levels wwl
       WHERE wwcm.workflow_code = wwl.workflow_code
         AND wwcm.workflow_code = p_workflow_code
         AND wwl.role_code = p_role_code;
  
    v_escalation_alert_code   VARCHAR2(20);
    v_created_hour_or_day     DATE;
    v_previous_level_role     wfm_workflow_monitors.created_by%TYPE;
    v_previous_wf_executed_by wfm_workflow_monitors.workflow_executed_by%TYPE;
    v_alert_effective_date    DATE;
    v_alert_count             NUMBER;
  BEGIN
    v_escalation_alert_code := ssm.get_parameter_value(p_param_code => 'SP078');
  
    FOR i IN c_waiting_for_approval LOOP
      BEGIN
        SELECT wwm.role_code, wwm.workflow_executed_by
          INTO v_previous_level_role, v_previous_wf_executed_by
          FROM wfm_workflow_monitors wwm
         WHERE wwm.transaction_code = i.transaction_code
           AND wwm.level_code =
               (SELECT MAX(level_code)
                  FROM wfm_workflow_monitors
                 WHERE transaction_code = wwm.transaction_code
                   AND level_code < i.level_code);
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          NULL;
      END;
    
      FOR j IN c_escalation(i.workflow_code, i.role_code) LOOP
        BEGIN
          SELECT COUNT(1)
            INTO v_alert_count
            FROM ssm_alert_user_levels
           WHERE transaction_code = i.transaction_code
             AND alert_code = v_escalation_alert_code;
        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            v_alert_count := 0;
        END;
      
        IF v_alert_count > 0 THEN
          BEGIN
            SELECT effective_date
              INTO v_alert_effective_date
              FROM ssm_alert_user_levels saul
             WHERE saul.transaction_code = i.transaction_code
               AND saul.alert_code = v_escalation_alert_code
               AND effective_date =
                   (SELECT MAX(effective_date)
                      FROM ssm_alert_user_levels
                     WHERE transaction_code = saul.transaction_code
                       AND alert_code = saul.alert_code);
          EXCEPTION
            WHEN NO_DATA_FOUND THEN
              NULL;
          END;
        
          IF j.escalation_type = 'H' THEN
            v_alert_effective_date := v_alert_effective_date +
                                      (trunc(j.escalation_value) +
                                      MOD(j.escalation_value, 1) / .6) / 24;
          ELSIF j.escalation_type = 'D' THEN
            v_alert_effective_date := v_alert_effective_date +
                                      j.escalation_value;
          END IF;
        
          IF v_alert_effective_date <= SYSDATE THEN
            alerts.generate_escalation_alert(alertcode   => v_escalation_alert_code,
                                             p_usercode  => v_previous_wf_executed_by,
                                             langcode    => 'EN',
                                             MESSAGE     => 'Escalation - ' ||
                                                            i.transaction_code,
                                             txn_code    => i.transaction_code,
                                             p_role_code => v_previous_level_role);
          END IF;
        ELSE
          IF j.escalation_type = 'H' THEN
            v_created_hour_or_day := nvl(i.modified_date, i.created_date) +
                                     (trunc(j.escalation_value) +
                                      MOD(j.escalation_value, 1) / .6) / 24;
          ELSIF j.escalation_type = 'D' THEN
            v_created_hour_or_day := nvl(i.modified_date, i.created_date) +
                                     j.escalation_value;
          END IF;
        
          IF v_created_hour_or_day <= SYSDATE THEN
            alerts.generate_escalation_alert(alertcode   => v_escalation_alert_code,
                                             p_usercode  => v_previous_wf_executed_by,
                                             langcode    => 'EN',
                                             MESSAGE     => 'Escalation - ' ||
                                                            i.transaction_code,
                                             txn_code    => i.transaction_code,
                                             p_role_code => v_previous_level_role);
          END IF;
        END IF;
      END LOOP;
    END LOOP;
  EXCEPTION
    WHEN OTHERS THEN
      raise_application_error(-20201,
                              'Error In Generating Escalation Alerts: ' ||
                              SQLERRM);
  END escalation_time;

  procedure SCHEDULED_DOC_EXPIRED_ALERT is
  
    cursor c_doc_details is
      SELECT DD.PASS_TXN_ID,
             DD.PASSPORT_EMP_ID,
             DD.CREATED_BY,
             DD.PASS_ORG_ID
        FROM HR_EMP_PASSPORT_DETAILS DD
       WHERE
      /*DD.PASS_ORG_ID = p_org_id
                                             and */
       dd.workflow_completion_status = 1
       and dd.passport_expiry_date is not null;
  
    v_msg             varchar2(500);
    v_LN_Code         varchar2(500);
    v_emp_app_No      varchar(50);
    v_expire_Days     NUMBER;
    v_param_value     NUMBER;
    V_PASSPORT_EMP_ID varchar2(50);
    V_PASS_TXN_ID     varchar2(50);
    V_CREATED_BY      varchar2(50);
    V_PASS_ORG_ID     varchar2(50);
    V_temp_msg        varchar2(50);
    PRAGMA AUTONOMOUS_TRANSACTION;
  
  begin
  
    begin
      select am.alert_message, am.language_code
        into v_msg, v_LN_Code
        from ssm_alert_masters am
       where am.alert_code = 'ALRT_CDE-0000000006';
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        v_msg := 0;
        dbms_output.put_line('alert msg error ' || sqlerrm);
    END;
  
    begin
      select ssp.param_value
        into v_param_value
        from ssm_system_parameters ssp
       where ssp.param_code = 'EXPIRED_DAYS';
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        v_param_value := 0;
        dbms_output.put_line('sys param error ' || sqlerrm);
    END;
  
    -- v_expire_Days := 90;
  
    for f_doc_details in c_doc_details loop
    
      v_expire_Days := 0;
    
      begin
        select nvl(to_number((to_date(aa.passport_expiry_date,
                                      'dd-mon-yyyy') -
                             (to_date(sysdate, 'dd-mon-yyyy')))),
                   0) as expired_days
          into v_expire_Days
          from HR_EMP_PASSPORT_DETAILS aa
         where aa.pass_txn_id = f_doc_details.pass_txn_id;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          -- v_expire_Days := 0;
          dbms_output.put_line('document msg error ' || sqlerrm);
      END;
    
      begin
        select ee.emp_no
          into v_emp_app_No
          from hr_employees ee
         where ee.emp_id = f_doc_details.passport_emp_id;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          v_emp_app_No := 0;
          dbms_output.put_line('emp no msg error ' || sqlerrm);
      END;
    
      if v_param_value > 0 and v_expire_Days > 0 then
        if v_param_value >= v_expire_Days then
          V_temp_msg := '';
          V_temp_msg := v_msg || ' with in ' || v_expire_Days || ' days';
        
          ALERTS.generate_alert_emp(alertcode => 'ALRT_CDE-0000000006',
                                    usercode  => v_emp_app_No,
                                    langcode  => v_LN_Code,
                                    message   => V_temp_msg,
                                    txn_code  => f_doc_details.PASS_TXN_ID,
                                    from_emp  => f_doc_details.CREATED_BY,
                                    to_emp    => v_emp_app_No,
                                    p_ORG_ID  => f_doc_details.PASS_ORG_ID);
        end if;
      end if;
    
    end loop;
    commit;
  end SCHEDULED_DOC_EXPIRED_ALERT;

  procedure SCHEDULED_EMP_PROBATION_ALERT is
  
    cursor c_doc_details is
      SELECT DD.PROB_ID,
             DD.PROB_EMP_ID,
             DD.CREATED_BY,
             dd.prob_status,
             dd.probation_period,
             dd.PROB_ORG_ID
        FROM hr_emp_probation dd
       WHERE
      /*DD.PROB_ORG_ID = p_org_id
                                             and*/
       dd.workflow_completion_status = 1
       and dd.PROB_TO_DT is not null;
  
    cursor c_appraiser_details(v_prob_id varchar) is
      select ee.emp_no
        from hr_employees ee, hr_emp_prob_appraiser pa
       where ee.emp_id = pa.emp_id
         and pa.prob_id = v_prob_id;
    v_msg             varchar2(2000);
    v_LN_Code         varchar2(500);
    v_emp_app_No      varchar(50);
    v_expire_Days     NUMBER;
    v_param_value     NUMBER;
    V_PASSPORT_EMP_ID varchar2(50);
    V_PASS_TXN_ID     varchar2(50);
    V_CREATED_BY      varchar2(50);
    V_PASS_ORG_ID     varchar2(50);
    V_temp_msg        varchar2(2000);
    v_from_emp_No     varchar(50);
    PRAGMA AUTONOMOUS_TRANSACTION;
  
  begin
  
    begin
      select am.alert_message, am.language_code
        into v_msg, v_LN_Code
        from ssm_alert_masters am
       where am.alert_code = 'ALRT_CDE-0000000040';
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        v_msg := 0;
        dbms_output.put_line('alert msg error ' || sqlerrm);
    END;
  
    begin
      select ssp.param_value
        into v_param_value
        from ssm_system_parameters ssp
       where ssp.param_code = 'EXPIRED_DAYS';
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        v_param_value := 0;
        dbms_output.put_line('sys param error ' || sqlerrm);
    END;
  
    for f_doc_details in c_doc_details loop
    
      v_expire_Days := 0;
    
      begin
        select nvl(to_number((to_date(aa.prob_to_dt, 'dd-mon-yyyy') -
                             (to_date(sysdate, 'dd-mon-yyyy')))),
                   0) as expired_days
          into v_expire_Days
          from hr_emp_probation aa
         where aa.prob_id = f_doc_details.prob_id;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          -- v_expire_Days := 0;
          dbms_output.put_line('document msg error ' || sqlerrm);
      END;
    
      begin
        select ee.emp_no
          into v_from_emp_No
          from hr_employees ee
         where ee.emp_id = f_doc_details.prob_emp_id;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          v_from_emp_No := 0;
          dbms_output.put_line('emp no1 msg error ' || sqlerrm);
      END;
      begin
        for f_appraiser_details in c_appraiser_details(f_doc_details.prob_id) loop
        
          if v_param_value > 0 and v_expire_Days > 0 then
            if v_param_value >= v_expire_Days then
              V_temp_msg := '';
              V_temp_msg := 'The probation period has been ' ||
                            f_doc_details.prob_status || ' for ' ||
                            v_from_emp_No || ' with in ' || v_expire_Days ||
                            ' days';
            
              ALERTS.generate_alert_emp(alertcode => 'ALRT_CDE-0000000040',
                                        usercode  => f_appraiser_details.emp_no,
                                        langcode  => v_LN_Code,
                                        message   => V_temp_msg,
                                        txn_code  => f_doc_details.prob_id,
                                        from_emp  => f_doc_details.prob_emp_id,
                                        to_emp    => f_appraiser_details.emp_no,
                                        p_ORG_ID  => f_doc_details.PROB_ORG_ID);
            
            end if;
          end if;
        end loop;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          v_from_emp_No := 0;
          dbms_output.put_line('insert error ' || sqlerrm);
      END;
    end loop;
  
    commit;
  end SCHEDULED_EMP_PROBATION_ALERT;

end ALERTS;
/
