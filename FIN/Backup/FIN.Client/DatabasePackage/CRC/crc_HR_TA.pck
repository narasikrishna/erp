create or replace package HR_TA is

  FUNCTION FN_EMP_TOTAL_SERVICE_DAYS(str_EMP_ID IN VARCHAR2) RETURN NUMBER;
  FUNCTION datediff(p_what in varchar2, p_d1 in date, p_d2 in date)
    RETURN NUMBER;
  PROCEDURE Generate_OverTime(p_from_dt DATE,
                              p_to_dt   DATE,
                              p_comp_id VARCHAR2,
                              p_EMP_ID  VARCHAR2);

  FUNCTION get_emp_leave_Avail(p_fiscal_year IN VARCHAR2,
                               p_org_id      IN VARCHAR2,
                               p_dept_id     IN VARCHAR2,
                               p_emp_id      IN VARCHAR2,
                               p_leave_id    IN VARCHAR2) RETURN NUMBER;
end HR_TA;
 
 
 
 
 
 
 
/
create or replace package body HR_TA is

  FUNCTION FN_EMP_TOTAL_SERVICE_DAYS(str_EMP_ID IN VARCHAR2) RETURN NUMBER IS
    str_EOS_DATE       DATE;
    int_CURR_MON_DAY   INT;
    dt_LAST_MON_DATE   DATE;
    dt_DOJ             DATE;
    int_DOJ_MON_DAY    INT;
    int_TOTAL_WRK_DAYS INT;
    int_TOAL_MONTHS    INT;
  BEGIN
    --str_EOS_DATE     := '23/12/2014';
    --str_EMP_ID       := 'EMP_ID-0000000037';

    SELECT to_char(EMP.EMP_DOJ, 'DD'), EMP.EMP_DOJ
      into int_DOJ_MON_DAY, dt_DOJ
      FROM HR_EMPLOYEES EMP
     WHERE EMP.EMP_ID = str_EMP_ID;
    dbms_output.put_line(int_DOJ_MON_DAY);

    SELECT to_char(EMP.TERMINATION_DATE, 'DD'), EMP.TERMINATION_DATE
      into int_CURR_MON_DAY, str_EOS_DATE
      FROM HR_EMPLOYEES EMP
     WHERE EMP.EMP_ID = str_EMP_ID;
    dbms_output.put_line(int_CURR_MON_DAY);
    dt_LAST_MON_DATE := str_EOS_DATE - int_CURR_MON_DAY;
    dbms_output.put_line(dt_LAST_MON_DATE);

    IF (int_DOJ_MON_DAY > 1) THEN
      int_TOTAL_WRK_DAYS := int_TOTAL_WRK_DAYS + int_DOJ_MON_DAY;
      dt_DOJ             := ADD_MONTHS(dt_DOJ - int_DOJ_MON_DAY, 1);
      dbms_output.put_line(dt_DOJ);
    END IF;

    IF (int_CURR_MON_DAY > 26) THEN
      int_TOTAL_WRK_DAYS := int_TOTAL_WRK_DAYS + 26;
    ELSE
      int_TOTAL_WRK_DAYS := int_TOTAL_WRK_DAYS + int_CURR_MON_DAY;
    END IF;
    int_TOAL_MONTHS := MONTHS_BETWEEN(dt_LAST_MON_DATE, dt_DOJ);

    int_TOTAL_WRK_DAYS := int_TOAL_MONTHS * 26;

    dbms_output.put_line('TOAL WORKING DAYS :  ' || int_TOTAL_WRK_DAYS);

    RETURN int_TOTAL_WRK_DAYS;

  END FN_EMP_TOTAL_SERVICE_DAYS;

  FUNCTION datediff(p_what in varchar2, p_d1 in date, p_d2 in date)
    return number as
    l_result number;
  begin
    select (p_d2 - p_d1) * decode(upper(p_what),
                                  'SS',
                                  24 * 60 * 60,
                                  'MI',
                                  24 * 60,
                                  'HH',
                                  24,
                                  NULL)
      into l_result
      from dual;

    return l_result;
  end datediff;
  PROCEDURE Generate_OverTime(p_from_dt DATE,
                              p_to_dt   DATE,
                              p_comp_id VARCHAR2,
                              p_EMP_ID  VARCHAR2) IS

    TYPE TMP_SCHEDULE_DET IS RECORD(
      TM_SCH_CODE   VARCHAR2(50),
      FROM_TIME     VARCHAR2(50),
      FROM_SESSION  VARCHAR2(50),
      TO_TIME       VARCHAR2(50),
      TO_SESSION    VARCHAR2(50),
      FROM_TIME_24  VARCHAR2(50),
      TO_TIME_24    VARCHAR2(50),
      TM_GRACE_TIME number(10),
      TM_FLEX_TIME  NUMBER(10)

      );

    c_TM_GROUP_SCH   SYS_REFCURSOR;
    rec_TM_GROUP_SCH TM_DEPT_SCH%ROWTYPE;

    c_TM_SCHED  SYS_REFCURSOR;
    rec_TM_SECH TMP_SCHEDULE_DET;

    c_EMP_DET   SYS_REFCURSOR;
    rec_EMP_DET HR_EMPLOYEES%ROWTYPE;

    c_EMP_TRANS   SYS_REFCURSOR;
    rec_EMP_TRANS TM_EMP_TRANS%ROWTYPE;

    int_NO_OF_DAYS int;
    dt_START_DATE  date;

    str_FROM_TIME    VARCHAR2(50);
    str_FROM_SESSION VARCHAR2(50);
    str_TO_TIME      VARCHAR2(50);
    str_TO_SESSION   VARCHAR2(50);
    int_GRACE_TIME   INT;
    str_FROM_TIME_24 VARCHAR2(50);
    str_TO_TIME_24   VARCHAR2(50);

    str_FDT VARCHAR2(50);
    str_TDT VARCHAR2(50);

    int_FTIME int;
    int_TTime int;

    int_SFTIME int;
    int_STTIME int;

    int_GraceLate int;
    int_OT        int;

    bol_FIN boolean;

    START_PUNCH   DATE;
    END_PUNCH     DATE;
    int_TOTALHOUR int;

  BEGIN
    dt_START_DATE  := TO_DATE(TO_CHAR(p_from_dt, 'DD/MM/YYYY'),
                              'DD/MM/YYYY');
    int_NO_OF_DAYS := (dt_START_DATE - p_to_dt) + 1;

    DELETE TMP_TM_EMP_OVERTIME WHERE LOGIN_EMP_ID = p_EMP_ID;
    COMMIT;

    -- STARTING LOOP FOR FROM DATE TO DATE IMPORT
    FOR iLOOP IN 1 .. int_NO_OF_DAYS LOOP
      DBMS_OUTPUT.put_line('ATTENDANCE DATE : ' || dt_START_DATE);

      -- SELECT GROUP SCHEDULE FOR EACH DATE
      OPEN c_TM_GROUP_SCH FOR
        SELECT *
          FROM TM_DEPT_SCH DS
         WHERE (dt_START_DATE BETWEEN DS.TM_START_DATE AND DS.TM_END_DATE)
            OR (DS.TM_START_DATE <= dt_START_DATE AND
               DS.TM_END_DATE IS NULL);
      LOOP
        FETCH c_TM_GROUP_SCH
          INTO rec_TM_GROUP_SCH;
        EXIT WHEN c_TM_GROUP_SCH%NOTFOUND;
        --DBMS_OUTPUT.put_line('DEPARTMENT SCHEDULE DETAILS ID : ' || rec_TM_GROUP_SCH.TM_SCH_ID);

        OPEN c_TM_SCHED FOR
          SELECT TM_SCH_CODE,
                 REGEXP_SUBSTR(TM_FROM1, '[^( ,]+', 1, 2) as FROM_TIME,
                 REGEXP_SUBSTR(TM_FROM1, '[^( ,]+', 1, 3) FROM_SESSION,
                 REGEXP_SUBSTR(TM_TO1, '[^( ,]+', 1, 2) as TO_TIME,
                 REGEXP_SUBSTR(TM_TO1, '[^( ,]+', 1, 3) AS TO_SESSION,
                 REGEXP_SUBSTR(TO_CHAR(TO_DATE(TM_FROM1,
                                               'dd/mm/yyyy HH:MI:SS AM'),
                                       'dd-MM-yy hh24:mi:ss'),
                               '[^( ,]+',
                               1,
                               2) as FROM_TIME_24,
                 REGEXP_SUBSTR(TO_CHAR(TO_DATE(TM_TO1,
                                               'dd/mm/yyyy HH:MI:SS AM'),
                                       'dd-MM-yy hh24:mi:ss'),
                               '[^( ,]+',
                               1,
                               2) as TO_TIME_24,
                 TM_GRACE_TIME,
                 TM_FLEX_TIME
            FROM TM_SCHEDULES S
           WHERE S.TM_SCH_CODE = rec_TM_GROUP_SCH.TM_SCH_CODE;
        LOOP
          FETCH c_TM_SCHED
            INTO rec_TM_SECH;
          str_FROM_TIME    := rec_TM_SECH.FROM_TIME;
          str_FROM_SESSION := rec_TM_SECH.FROM_SESSION;
          str_TO_TIME      := rec_TM_SECH.TO_TIME;
          str_TO_SESSION   := rec_TM_SECH.TO_SESSION;
          str_FROM_TIME_24 := rec_TM_SECH.FROM_TIME_24;
          str_TO_TIME_24   := rec_TM_SECH.TO_TIME_24;
          int_GRACE_TIME   := rec_TM_SECH.TM_GRACE_TIME;
          EXIT WHEN c_TM_SCHED%NOTFOUND;
          DBMS_OUTPUT.put_line('SCHEDULE CODE : ' ||
                               rec_TM_SECH.TM_SCH_CODE || ' FROM TIME : ' ||
                               str_FROM_TIME || ' FROM SESSION : ' ||
                               str_FROM_SESSION || ' TO TIME : ' ||
                               str_TO_TIME || ' FROM TIME_24 : ' ||
                               str_FROM_TIME_24 || ' TO TIME_24 : ' ||
                               str_TO_TIME_24 || ' TO SESSION : ' ||
                               str_TO_SESSION || ' GRACE TIME : ' ||
                               int_GRACE_TIME);
          int_FTIME := REGEXP_SUBSTR(str_FROM_TIME_24, '[^(:,]+', 1, 1);
          int_TTIME := REGEXP_SUBSTR(str_TO_TIME_24, '[^(:,]+', 1, 1);

          int_SFTIME := int_FTIME;
          int_STTIME := int_TTIME;

          -- DBMS_OUTPUT.put_line(int_FTIME || ' ' || int_TTIME);
        END LOOP;
        DBMS_OUTPUT.put_line('GROUP_CODE :' ||
                             rec_TM_GROUP_SCH.TM_GRP_CODE);
        -- DBMS_OUTPUT.PUT_LINE('START DATE: ' || dt_START_DATE);

        --- SELECT EMPLOYEE DETAILS FOR GROUP
        OPEN c_EMP_DET FOR
          SELECT DISTINCT EMP.*
            FROM HR_EMPLOYEES EMP
           INNER JOIN TM_EMP_GROUP_LINKS EGL ON EGL.TM_EMP_ID = EMP.EMP_ID
           WHERE EGL.TM_GRP_CODE = rec_TM_GROUP_SCH.TM_GRP_CODE
             AND EMP.EMP_ORG_ID = p_comp_id
             AND ((dt_START_DATE BETWEEN EGL.TM_START_DATE AND
                 EGL.TM_END_DATE) OR (EGL.TM_START_DATE <= dt_START_DATE AND
                 EGL.TM_END_DATE IS NULL));
        LOOP
          FETCH c_EMP_DET
            INTO rec_EMP_DET;
          EXIT WHEN c_EMP_DET%NOTFOUND;
          DBMS_OUTPUT.put_line('EMPLOYEE ID : ' || rec_EMP_DET.EMP_ID);

          IF (str_FROM_SESSION = 'PM' AND str_TO_SESSION = 'PM') THEN
            str_FDT := to_char(dt_START_DATE) || ' ' ||
                       TO_CHAR((int_FTIME - 5)) || ':00:00';
          ELSE
            IF (int_FTIME - 5 < 0) THEN
              str_FDT := to_char(dt_START_DATE - 1) || ' ' ||
                         TO_CHAR(24 + (int_FTIME - 5)) || ':00:00';
            ELSE
              str_FDT := to_char(dt_START_DATE) || ' ' ||
                         TO_CHAR((int_FTIME - 5)) || ':00:01';
            END IF;
          END IF;

          IF (str_FROM_SESSION = 'PM' AND str_TO_SESSION = 'AM') THEN
            str_TDT := to_char(dt_START_DATE + 1) || ' ' ||
                       TO_CHAR((int_TTIME + 5)) || ':00:01';
          ELSE
            IF (int_TTIME + 5 > 24) THEN
              str_TDT := to_char(dt_START_DATE + 1) || ' ' ||
                         TO_CHAR((int_TTIME + 5) - 24) || ':00:00';
            ELSE
              str_TDT := to_char(dt_START_DATE) || ' ' ||
                         TO_CHAR((int_TTIME + 5)) || ':00:01';
            END IF;

          END IF;
          DBMS_OUTPUT.put_line('ATTENDANCE TAKEN FROM TIME :' || str_FDT ||
                               ' TO TIME : ' || str_TDT);
          OPEN c_EMP_TRANS FOR
            SELECT *
              FROM TM_EMP_TRANS ET
             WHERE ET.TM_EMP_ID = rec_EMP_DET.EMP_ID
               AND TM_TIME >= TO_DATE(str_FDT, 'dd-MM-yy hh24:mi:ss')
               AND TM_TIME <= TO_DATE(str_TDT, 'dd-MM-yy hh24:mi:ss')
             ORDER BY TM_TIME;
          bol_FIN       := false;
          int_GraceLate := 0;
          int_OT        := 0;
          int_TOTALHOUR := 0;

          LOOP
            FETCH c_EMP_TRANS
              INTO rec_EMP_TRANS;
            EXIT WHEN c_EMP_TRANS%NOTFOUND;
            DBMS_OUTPUT.put_line('PUNCHED DATA ' ||
                                 rec_EMP_TRANS.TM_EMP_ID || ' -- ' ||
                                 rec_EMP_TRANS.TM_MODE || ' -- ' ||
                                 TO_CHAR(rec_EMP_TRANS.TM_TIME,
                                         'dd-MON-yy hh24:mi:ss'));

            IF (bol_FIN = FALSE AND rec_EMP_TRANS.TM_MODE = 'I') THEN
              bol_FIN       := true;
              int_GraceLate := datediff('MI',
                                        TO_DATE(TO_CHAR(rec_EMP_TRANS.TM_DATE,
                                                        'dd-MON-yy') || ' ' ||
                                                str_FROM_TIME_24,
                                                'dd-MON-yy hh24:mi:ss'),
                                        rec_EMP_TRANS.TM_TIME);
            END IF;
            IF (rec_EMP_TRANS.TM_MODE = 'O') THEN
              int_OT := datediff('MI',
                                 TO_DATE(TO_CHAR(rec_EMP_TRANS.TM_DATE,
                                                 'dd-MON-yy') || ' ' ||
                                         str_TO_TIME_24,
                                         'dd-MON-yy hh24:mi:ss'),
                                 rec_EMP_TRANS.TM_TIME);
            END IF;

            IF (rec_EMP_TRANS.TM_MODE = 'I') THEN
              START_PUNCH := rec_EMP_TRANS.TM_TIME;
            END IF;
            IF (rec_EMP_TRANS.TM_MODE = 'O') THEN
              IF (START_PUNCH IS NOT NULL) THEN

                END_PUNCH     := rec_EMP_TRANS.TM_TIME;
                int_TOTALHOUR := int_TOTALHOUR +
                                 datediff('MI', START_PUNCH, END_PUNCH);
                START_PUNCH   := NULL;
              END IF;
            END IF;
          END LOOP;
          DBMS_OUTPUT.put_line('Time Grace Late : ' || int_GraceLate);
          DBMS_OUTPUT.put_line('Over Time : ' || int_OT);
          IF (c_EMP_TRANS%ROWCOUNT > 0) THEN

            IF (int_GraceLate > int_GRACE_TIME) THEN
              DBMS_OUTPUT.put_line('EMPLOYEE LATE ON MIN : ' ||
                                   int_GraceLate);
            END IF;

            IF (int_OT > int_GRACE_TIME) THEN
              DBMS_OUTPUT.put_line('EMPLOYEE OVER TIME MIN : ' || int_OT);
              INSERT INTO TMP_TM_EMP_OVERTIME
                (OVERTIME_DATE,
                 OVERTIME_EMP_ID,
                 OVERTIME_HOURS,
                 LOGIN_EMP_ID)
              VALUES
                (dt_START_DATE,
                 rec_EMP_DET.EMP_ID,
                 int_GraceLate,
                 p_EMP_ID);
              commit;
            END IF;
            DBMS_OUTPUT.put_line('EMPLOYEE TOTAL WORKING MIN : ' ||
                                 int_TOTALHOUR);
          END IF;
        END LOOP;
        --       DBMS_OUTPUT.PUT_LINE('Total number of rows : ' || c_EMP_DET%ROWCOUNT);
        CLOSE c_EMP_DET;
      END LOOP;

      DBMS_OUTPUT.put_line('');

      dt_START_DATE := dt_START_DATE + 1;
    END LOOP;
  END Generate_OverTime;

  FUNCTION get_emp_leave_Avail(p_fiscal_year IN VARCHAR2,
                               p_org_id      IN VARCHAR2,
                               p_dept_id     IN VARCHAR2,
                               p_emp_id      IN VARCHAR2,
                               p_leave_id    IN VARCHAR2) RETURN NUMBER IS
    v_leave_bal NUMBER := 0;
  BEGIN
    SELECT XP.LDR_LEAVE_AVAIL
      INTO v_leave_bal
      FROM HR_LEAVE_LEDGER XP
     WHERE XP.LDR_EMP_ID = p_emp_id --'EMP_ID-0000000034'
       AND XP.LDR_FISCAL_YEAR = p_fiscal_year
       AND XP.LDR_ORG_ID = p_org_id
       AND XP.LDR_DEPT_ID = p_dept_id
       AND XP.LDR_LEAVE_ID = p_leave_id
       AND XP.LDR_ID = (SELECT MAX(LDR_ID)
                          FROM HR_LEAVE_LEDGER
                         WHERE LDR_EMP_ID = p_emp_id --'EMP_ID-0000000034'
                           AND LDR_FISCAL_YEAR = p_fiscal_year
                           AND LDR_ORG_ID = p_org_id
                           AND LDR_DEPT_ID = p_dept_id
                           AND LDR_LEAVE_ID = p_leave_id);
    RETURN v_leave_bal;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN 0;
  END get_emp_leave_Avail;

end HR_TA;
/
