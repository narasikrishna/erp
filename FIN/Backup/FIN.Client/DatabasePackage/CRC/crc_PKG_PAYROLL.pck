CREATE OR REPLACE PACKAGE PKG_PAYROLL IS

  --- ***************************************************************************************************************
  --- $Header: PKG_GENERAL 1.0 2013-08-1 $
  --- ***************************************************************************************************************
  --- Program      :   PKG_PAYROLL
  --- Description        :   Package comprises of Payroll related procedures and functions - ERP
  --- Author       :   C.S.Ilampooranan ,VMV Systems Pvt Ltd.,
  --- Date   :   01\08\2013
  --- Notes                :
  --- ****************************************************************************************************************
  --- Modification Log
  --- ------------------------------------
  --- Ver    Date         Author                   Description
  --- ---    ----------   ------------------------ ---------------------------------------------------------
  --- 1.0    27\08\2013  Ilam
  --- ***************************************************************************************************************
  FUNCTION get_dept_id(p_emp_id IN VARCHAR2) RETURN VARCHAR2;

  FUNCTION get_desig_id(p_org_id  IN VARCHAR2,
                        p_dept_id IN VARCHAR2,
                        p_emp_id  IN VARCHAR2) RETURN VARCHAR2;

  FUNCTION get_basic_amt(p_org_id IN VARCHAR2, p_emp_id IN VARCHAR2)
    RETURN NUMBER;

  FUNCTION get_element_str(p_org_id     IN VARCHAR2,
                           p_dept_id    IN VARCHAR2,
                           p_desig_id   IN VARCHAR2,
                           p_element_id IN VARCHAR2) RETURN VARCHAR2;

  FUNCTION get_element_value(p_org_id     IN VARCHAR2,
                             p_dept_id    IN VARCHAR2,
                             p_desig_id   IN VARCHAR2,
                             p_emp_id     IN VARCHAR2,
                             p_element_id IN VARCHAR2) RETURN NUMBER;

  FUNCTION get_pay_element_calc(p_org_id   IN VARCHAR2,
                                p_dept_id  IN VARCHAR2,
                                p_desig_id IN VARCHAR2,
                                p_emp_id   IN VARCHAR2,
                                p_str      IN VARCHAR2) RETURN NUMBER;

  FUNCTION get_no_workingdays(p_org_id      IN NUMBER,
                              p_fiscal_year IN VARCHAR2,
                              p_from_date   IN DATE,
                              p_to_date     IN DATE) RETURN NUMBER;

  FUNCTION get_present_days(p_org_id      NUMBER,
                            p_fiscal_year IN VARCHAR2,
                            p_from_date   IN DATE,
                            p_to_date     IN DATE,
                            p_staff_id    IN NUMBER) RETURN NUMBER;

  /*
    FUNCTION get_absent_days(p_org_id NUMBER,
                              p_fiscal_year IN VARCHAR2,
                p_from_date IN DATE,
                p_to_date IN DATE,
                p_staff_id IN NUMBER) RETURN NUMBER;
  
  */

  FUNCTION get_loan_deduction(p_org_id  IN VARCHAR2,
                              p_dept_id IN VARCHAR2,
                              p_emp_id  IN VARCHAR2,
                              p_from_dt IN DATE,
                              p_to_dt   IN DATE) RETURN NUMBER;

  FUNCTION get_nr_deduction(p_org_id     IN VARCHAR2,
                            p_pay_period IN VARCHAR2,
                            p_element_id IN VARCHAR2,
                            p_dept_id    IN VARCHAR2,
                            p_emp_id     IN VARCHAR2) RETURN NUMBER;

  FUNCTION get_fiscal_year(p_org_id IN VARCHAR2, p_year IN date) --p_year IN VARCHAR2)
   RETURN VARCHAR2;

  FUNCTION get_mgrp_amt(p_org_id IN VARCHAR2,
                        p_emp_id IN VARCHAR2,
                        p_year   IN VARCHAR2) RETURN NUMBER;

  PROCEDURE get_LOP_days(p_org_id      VARCHAR2,
                         p_fiscal_year IN VARCHAR2,
                         p_dept_id     IN VARCHAR2,
                         p_staff_id    IN VARCHAR2,
                         p_leave_id    IN VARCHAR2,
                         p_from_date   IN DATE,
                         p_to_date     IN DATE,
                         p_level       IN VARCHAR2,
                         p_lop_factor  OUT NUMBER,
                         p_lop_days    OUT NUMBER);

  FUNCTION get_days(p_from_dt DATE, p_to_dt DATE) RETURN NUMBER;
  FUNCTION get_leave_days(p_org_id VARCHAR2, p_from_dt DATE, p_to_dt DATE)
    RETURN NUMBER;

  FUNCTION get_last_pay_dt(p_org_id IN VARCHAR2, p_emp_id IN VARCHAR2)
    RETURN DATE;

  FUNCTION get_emp_leave_bal(p_fiscal_year IN VARCHAR2,
                             p_org_id      IN VARCHAR2,
                             p_dept_id     IN VARCHAR2,
                             p_emp_id      IN VARCHAR2,
                             p_leave_id    IN VARCHAR2) RETURN NUMBER;

  PROCEDURE get_element_stop_dtl(p_emp_id       IN VARCHAR2,
                                 p_element_id   IN VARCHAR2,
                                 p_from_dt      IN DATE,
                                 p_to_dt        IN DATE,
                                 p_exclude_fd   IN CHAR DEFAULT '0',
                                 p_payroll_days OUT NUMBER,
                                 p_skip_element OUT CHAR,
                                 p_ignore_emp   OUT CHAR);

  --start
  PROCEDURE get_element_stop_dtl_ls(p_emp_id       IN VARCHAR2,
                                    p_element_id   IN VARCHAR2,
                                    p_from_dt      IN DATE,
                                    p_to_dt        IN DATE,
                                    p_payroll_days OUT NUMBER,
                                    p_skip_element OUT CHAR,
                                    p_ignore_emp   OUT CHAR);
  --end

  FUNCTION get_dept_query(p_dept_list VARCHAR2, p_group_id VARCHAR2)
    RETURN VARCHAR2;

  FUNCTION get_csv_to_list(p_dept_list VARCHAR2) RETURN VARCHAR2;

  FUNCTION get_emp_leave_ldr_exists(p_fiscal_year IN VARCHAR2,
                                    p_emp_id      IN VARCHAR2,
                                    p_org_id      IN VARCHAR2,
                                    p_leave_id    IN VARCHAR2,
                                    p_dept_id     IN VARCHAR2) RETURN NUMBER;

  FUNCTION get_emp_curr_leave_bal(p_fiscal_year IN VARCHAR2,
                                  p_emp_id      IN VARCHAR2,
                                  p_org_id      IN VARCHAR2,
                                  p_leave_id    IN VARCHAR2,
                                  p_dept_id     IN VARCHAR2) RETURN NUMBER;

  FUNCTION get_holidays(p_org_id      IN VARCHAR2,
                        p_fiscal_year IN VARCHAR2,
                        p_from_dt     IN DATE,
                        p_to_dt       IN DATE) RETURN NUMBER;

  PROCEDURE emp_leave_txn_ledger(p_tran_type   IN VARCHAR2,
                                 p_tran_id     IN VARCHAR2,
                                 p_txn_type    IN VARCHAR2,
                                 p_trn_date    DATE,
                                 p_trn_month   IN NUMBER,
                                 p_fiscal_year IN VARCHAR2,
                                 p_emp_id      IN VARCHAR2,
                                 p_org_id      IN VARCHAR2,
                                 p_leave_id    IN VARCHAR2,
                                 p_dept_id     IN VARCHAR2,
                                 p_days        IN NUMBER);

  PROCEDURE Leave_Definition_Year_End(p_org_id        IN VARCHAR2,
                                      p_curr_fin_year VARCHAR2,
                                      p_prev_fin_year VARCHAR2);

  PROCEDURE LEAVE_DEPT_YEAR_END(p_org_id        IN VARCHAR2,
                                p_curr_fin_year VARCHAR2,
                                p_prev_fin_year VARCHAR2);

  PROCEDURE Leave_Staff_YEAR_END(p_org_id    IN VARCHAR2,
                                 p_curr_year VARCHAR2,
                                 p_prev_year VARCHAR2);

  PROCEDURE loan_deduction_update_proc(p_org_id IN VARCHAR2,
                                       p_emp_id IN VARCHAR2,
                                       p_date   IN DATE,
                                       p_amt    IN NUMBER);

  PROCEDURE payroll_process(p_pay_group  IN VARCHAR2,
                            p_pay_period IN VARCHAR2,
                            p_dept_list  IN VARCHAR2,
                            p_emp_id     IN VARCHAR2 DEFAULT NULL,
                            p_org_id     IN VARCHAR2);

  PROCEDURE payroll_finalrun_process(p_org_id     IN VARCHAR2,
                                     p_payroll_id IN VARCHAR2);

  FUNCTION leave_salary_old(p_pay_period IN VARCHAR2,
                            p_dept_id    IN VARCHAR2,
                            p_emp_id     IN VARCHAR2 DEFAULT NULL,
                            p_org_id     IN VARCHAR2,
                            p_from_dt    IN DATE,
                            p_to_dt      IN DATE) RETURN NUMBER;
  PROCEDURE get_AL_days(p_org_id       VARCHAR2,
                        p_fiscal_year  IN VARCHAR2,
                        p_dept_id      IN VARCHAR2,
                        p_staff_id     IN VARCHAR2,
                        p_leave_id     IN VARCHAR2,
                        p_from_date    IN DATE,
                        p_to_date      IN DATE,
                        p_level        IN VARCHAR2,
                        p_lop_factor   OUT NUMBER,
                        p_lop_days     OUT NUMBER,
                        p_leave_req_id IN VARCHAR2);

  PROCEDURE payroll_report_trial(p_org_id     IN VARCHAR2,
                                 p_pay_period IN VARCHAR2);

  PROCEDURE payroll_social_trial(p_org_id     IN VARCHAR2,
                                 p_pay_period IN VARCHAR2);

  PROCEDURE payroll_process_na(p_pay_group  IN VARCHAR2,
                               p_pay_period IN VARCHAR2,
                               p_dept_list  IN VARCHAR2,
                               p_emp_id     IN VARCHAR2 DEFAULT NULL,
                               p_org_id     IN VARCHAR2);

  FUNCTION get_annual_amt(p_org_id IN VARCHAR2, p_emp_id IN VARCHAR2)
    RETURN NUMBER;

  FUNCTION leave_salary(p_pay_period IN VARCHAR2,
                        p_dept_id    IN VARCHAR2,
                        p_emp_id     IN VARCHAR2 DEFAULT NULL,
                        p_org_id     IN VARCHAR2,
                        p_from_dt    IN DATE,
                        p_to_dt      IN DATE) RETURN NUMBER;
END PKG_PAYROLL;

 
 
/
CREATE OR REPLACE PACKAGE BODY PKG_PAYROLL AS

  FUNCTION get_holidays(p_org_id      IN VARCHAR2,
                        p_fiscal_year IN VARCHAR2,
                        p_from_dt     IN DATE,
                        p_to_dt       IN DATE) RETURN NUMBER IS
    v_holidays NUMBER := 0;
  BEGIN
    SELECT COUNT(*)
      INTO v_holidays
      FROM HR_HOLIDAYS_MASTER
     WHERE ORG_ID = p_org_id
          -- AND FISCAL_YEAR = p_fiscal_year
       AND HOLIDAY_DATE >= p_from_dt
       AND HOLIDAY_DATE <= p_to_dt;
  
    RETURN v_holidays;
  END get_holidays;

  FUNCTION get_dept_id(p_emp_id IN VARCHAR2) RETURN VARCHAR2 IS
    v_dept_id VARCHAR2(50);
  BEGIN
    SELECT EMP_DEPT_ID
      INTO v_dept_id
      FROM HR_EMP_WORK_DTLS
     WHERE EMP_ID = p_emp_id
       AND EFFECTIVE_TO_DT IS NULL
       AND ENABLED_FLAG = '1';
  
    RETURN v_dept_id;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN ' ';
  END get_dept_id;

  FUNCTION get_desig_id(p_org_id  IN VARCHAR2,
                        p_dept_id IN VARCHAR2,
                        p_emp_id  IN VARCHAR2) RETURN VARCHAR2 IS
    v_desig VARCHAR2(50);
  BEGIN
    SELECT EMP_DESIG_ID
      INTO v_desig
      FROM HR_EMP_WORK_DTLS
     WHERE EMP_DEPT_ID = p_dept_id
       AND EMP_ID = p_emp_id
       AND EMP_ORG_ID = p_org_id
       AND ENABLED_FLAG = '1'
       AND WORKFLOW_COMPLETION_STATUS = '1'
       AND EFFECTIVE_TO_DT IS NULL;
  
    RETURN v_desig;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN NULL;
  END get_desig_id;

  FUNCTION get_basic_amt(p_org_id IN VARCHAR2, p_emp_id IN VARCHAR2)
    RETURN NUMBER IS
    l_bas_amt    NUMBER := 0;
    l_sys_option SSM_SYSTEM_OPTIONS.HR_BASIC_ELEMENT_CODE%TYPE;
  BEGIN
    BEGIN
      SELECT X.HR_BASIC_ELEMENT_CODE
        INTO l_sys_option
        FROM SSM_SYSTEM_OPTIONS X
       WHERE X.MODULE_CODE = 'HR'
         AND X.HR_EFFECTIVE_TO_DATE IS NULL;
    EXCEPTION
      WHEN OTHERS THEN
        ----dbms_output.put_line('Err sys '||sqlerrm);
        l_sys_option := NULL;
    END;
  
    SELECT EVAL.PAY_AMOUNT
      INTO l_bas_amt
      FROM PAY_ELEMENTS ELE, PAY_EMP_ELEMENT_VALUE EVAL
     WHERE ELE.PAY_ELEMENT_ID = EVAL.PAY_ELEMENT_ID
       AND ELE.PAY_ORG_ID = EVAL.PAY_ORG_ID
       AND ELE.PAY_ORG_ID = p_org_id
       AND EVAL.PAY_EMP_ID = p_emp_id
       AND EVAL.EFFECTIVE_TO_DT IS NULL
       AND ELE.PAY_ELEMENT_ID = l_sys_option;
    --AND LTRIM(RTRIM(ELE.PAY_ELEMENT_CODE))=LTRIM(RTRIM(NVL(l_sys_option,ELE.PAY_ELEMENT_CODE)));
  
    RETURN l_bas_amt;
  EXCEPTION
    WHEN OTHERS THEN
      ----dbms_output.put_line('Error here111111'||sqlerrm);
      RETURN 0;
  END get_basic_amt;

  FUNCTION get_annual_amt(p_org_id IN VARCHAR2, p_emp_id IN VARCHAR2)
    RETURN NUMBER IS
    l_bas_amt    NUMBER := 0;
    l_sys_option SSM_SYSTEM_OPTIONS.HR_BASIC_ELEMENT_CODE%TYPE;
  BEGIN
  
    SELECT sum(nvl(EVAL.PAY_AMOUNT, 0))
      INTO l_bas_amt
      FROM PAY_ELEMENTS ELE, PAY_EMP_ELEMENT_VALUE EVAL
     WHERE ELE.PAY_ELEMENT_ID = EVAL.PAY_ELEMENT_ID
       AND ELE.PAY_ORG_ID = EVAL.PAY_ORG_ID
       AND ELE.PAY_ORG_ID = p_org_id
       AND EVAL.PAY_EMP_ID = p_emp_id
       AND EVAL.EFFECTIVE_FROM_DT =
           (select max(effective_from_dt)
              from PAY_EMP_ELEMENT_VALUE PEEV
             where PEEV.PAY_EMP_ID = eval.pay_emp_id
               and peev.PAY_ELEMENT_ID = eval.PAY_ELEMENT_ID
               and peev.paid_for_annual_leave = '1')
       and eval.paid_for_annual_leave = '1'
       and EVAL.Enabled_Flag = '1'
       and EVAL.Workflow_Completion_Status = '1';
    --AND LTRIM(RTRIM(ELE.PAY_ELEMENT_CODE))=LTRIM(RTRIM(NVL(l_sys_option,ELE.PAY_ELEMENT_CODE)));
  
    RETURN l_bas_amt;
  EXCEPTION
    WHEN OTHERS THEN
      ----dbms_output.put_line('Error here111111'||sqlerrm);
      RETURN 0;
  END get_annual_amt;

  FUNCTION get_element_value(p_org_id     IN VARCHAR2,
                             p_dept_id    IN VARCHAR2,
                             p_desig_id   IN VARCHAR2,
                             p_emp_id     IN VARCHAR2,
                             p_element_id IN VARCHAR2) RETURN NUMBER IS
    v_element_value NUMBER := 0;
  
  BEGIN
    /*
       SELECT NVL(PEC_AMOUNT,0)
     INTO v_element_value
     FROM PAY_ELEMENT_CALC
     WHERE PEC_ORG_ID=p_org_id
       AND PEC_DEPT_ID=p_dept_id
     AND PEC_DESIG_ID=p_desig_id
     AND PEC_ELEMENT_ID=p_element_id;
    */
    SELECT PAY_AMOUNT
      INTO v_element_value
      FROM PAY_EMP_ELEMENT_VALUE
     WHERE PAY_EMP_ID = p_emp_id
       AND PAY_ELEMENT_ID = p_element_id
       AND PAY_ORG_ID = p_org_id
       AND PAY_EMP_DEPT_ID = p_dept_id;
  
    RETURN v_element_value;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN 0;
  END get_element_value;

  FUNCTION get_element_str(p_org_id     IN VARCHAR2,
                           p_dept_id    IN VARCHAR2,
                           p_desig_id   IN VARCHAR2,
                           p_element_id IN VARCHAR2) RETURN VARCHAR2 IS
    v_str VARCHAR2(200);
  BEGIN
    SELECT PEC_FORMULA
      INTO v_str
      FROM PAY_ELEMENT_CALC
     WHERE PEC_ORG_ID = p_org_id
       AND PEC_DEPT_ID = p_dept_id
       AND PEC_DESIG_ID = p_desig_id
       AND PEC_ELEMENT_ID = p_element_id
       AND ENABLED_FLAG = '1'
       AND WORKFLOW_COMPLETION_STATUS = '1';
  
    RETURN v_str;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN NULL;
  END get_element_str;

  FUNCTION get_pay_element_calc(p_org_id   IN VARCHAR2,
                                p_dept_id  IN VARCHAR2,
                                p_desig_id IN VARCHAR2,
                                p_emp_id   IN VARCHAR2,
                                p_str      IN VARCHAR2) RETURN NUMBER IS
  
    --Cursor main_cur is SELECT * FROM PAY_ELEMENT_CALC WHERE PEC_CALC_TYPE='INDEPENDANT' AND PEC_DESIG_ID='DT_DG_ID-0000000041';
    --Cursor dtl_cur(c_desig_id VARCHAR2) is SELECT * FROM PAY_ELEMENT_CALC WHERE PEC_DESIG_ID=c_desig_id AND PEC_CALC_TYPE='DEPENDANT' AND PEC_ELEMENT_ID='PYE_ID-0000000004';
  
    v_str            VARCHAR2(2000);
    v_len            NUMBER := 0;
    v_counter        NUMBER := 0;
    i                NUMBER(10) := 1;
    v_tmp_element_id VARCHAR2(50);
    v_tmp            NUMBER := 0;
    -- v_org_id VARCHAR2(50):='ORG-0000000066';
    --v_dept_id VARCHAR2(50):='DEPT_ID-0000000012';
    --v_desig_id VARCHAR2(50):='DT_DG_ID-0000000041';
  
    TYPE pay_rec IS RECORD(
      rec_pec_id     PAY_ELEMENT_CALC.PEC_ID%TYPE,
      rec_element_id PAY_ELEMENT_CALC.PEC_ELEMENT_ID%TYPE,
      rec_amount     NUMBER);
  
    TYPE tab_pay_rec IS TABLE OF pay_rec INDEX BY BINARY_INTEGER;
  
    v_tab_pay_rec tab_pay_rec;
  BEGIN
    ----dbms_output.put_line('STart of the program ');
    v_counter := 0;
    i         := 1;
    v_str     := p_str; --'0.2*#PYE_ID-0000000002#+#PYE_ID-0000000003#/10+500';
    IF INSTR(v_str, '#', 1, 1) > 0 THEN
      v_len := LENGTH(LTRIM(RTRIM(v_str)));
      --dbms_output.put_line('Length of string ' || v_len);
      WHILE v_len > 0 LOOP
        IF SUBSTR(v_str, v_len, 1) = '#' THEN
          v_counter := v_counter + 1;
        END IF;
        v_len := v_len - 1;
      END LOOP;
      ----dbms_output.put_line('No of # ='||v_counter);
      WHILE v_counter > 0 LOOP
        ----dbms_output.put_line('Extracted string '||n.pec_id||' -'||' - '||n.pec_org_id||' - '||n.pec_dept_id||' - '||n.pec_desig_id||' - '||n.pec_element_id||' - '||substr(v_str,instr(v_str,'#',1,(v_counter-1)),(instr(v_str,'#',1,v_counter)-instr(v_str,'#',1,(v_counter-1)))));
        v_tmp_element_id := LTRIM(substr(v_str,
                                         instr(v_str,
                                               '#',
                                               1,
                                               (v_counter - 1)),
                                         (instr(v_str, '#', 1, v_counter) -
                                         instr(v_str,
                                                '#',
                                                1,
                                                (v_counter - 1)))),
                                  '#');
        v_tab_pay_rec(i).rec_pec_id := NULL; --n.pec_id;
        v_tab_pay_rec(i).rec_element_id := LTRIM(substr(v_str,
                                                        instr(v_str,
                                                              '#',
                                                              1,
                                                              (v_counter - 1)),
                                                        (instr(v_str,
                                                               '#',
                                                               1,
                                                               v_counter) -
                                                        instr(v_str,
                                                               '#',
                                                               1,
                                                               (v_counter - 1)))),
                                                 '#');
        v_tab_pay_rec(i).rec_amount := get_element_value(p_org_id     => p_org_id, --n.pec_org_id,
                                                         p_dept_id    => p_dept_id, --n.pec_dept_id,
                                                         p_desig_id   => p_desig_id, --n.pec_desig_id,
                                                         p_element_id => v_tmp_element_id,
                                                         p_emp_id     => p_emp_id);
        v_counter := v_counter - 2;
        i := i + 1;
      END LOOP;
      ----dbms_output.put_line(' No. of values in table '||NVL(v_tab_pay_rec.count,0));
      FOR t in 1 .. v_tab_pay_rec.count LOOP
        ----dbms_output.put_line('Values '||v_tab_pay_rec(t).rec_pec_id||' - '||v_tab_pay_rec(t).rec_element_id||' - '||v_tab_pay_rec(t).rec_amount);
        v_str := REPLACE(v_str,
                         ('#' || v_tab_pay_rec(t).rec_element_id || '#'),
                         v_tab_pay_rec(t).rec_amount);
        ----dbms_output.put_line('Nourised '||v_str);
      END LOOP;
      EXECUTE IMMEDIATE 'SELECT ' || v_str || ' FROM DUAL'
        INTO v_tmp;
      ----dbms_output.put_line('Calculated value '||v_tmp);
    END IF;
    RETURN v_tmp;
    --end loop;
    --end loop;
    --dbms_output.put_line('End of the program');
  EXCEPTION
    WHEN OTHERS THEN
      --dbms_output.put_line('Error 23456 ' || sqlerrm);
      RAISE_APPLICATION_ERROR(-20000, 'Please check the formula entered..');
      RETURN 0;
  END get_pay_element_calc;

  FUNCTION get_no_workingdays(p_org_id      IN NUMBER,
                              p_fiscal_year IN VARCHAR2,
                              p_from_date   IN DATE,
                              p_to_date     IN DATE) RETURN NUMBER IS
    v_days_count NUMBER(10) := 0;
  BEGIN
    SELECT COUNT(*)
      INTO v_days_count
      FROM HR_HOLIDAYS_MASTER
     WHERE ORG_ID = p_org_id
       AND FISCAL_YEAR = p_fiscal_year
       AND HOLIDAY_DATE >= p_from_date
       AND HOLIDAY_DATE <= p_to_date;
  
    Return v_days_count;
  END get_no_workingdays;

  FUNCTION get_present_days(p_org_id      NUMBER,
                            p_fiscal_year IN VARCHAR2,
                            p_from_date   IN DATE,
                            p_to_date     IN DATE,
                            p_staff_id    IN NUMBER) RETURN NUMBER IS
    v_present NUMBER(10) := 0;
  BEGIN
    SELECT COUNT(*)
      INTO v_present
      FROM HR_STAFF_ATTENDANCE
     WHERE ORG_ID = p_org_id
       AND FISCAL_YEAR = p_fiscal_year
       AND STAFF_ID = p_staff_id
       AND ATTENDANCE_TYPE IN ('PRESENT', 'PERMISSION')
       AND ATTENDANCE_DATE >= p_from_date
       AND ATTENDANCE_DATE <= p_to_date
       AND ENABLED_FLAG = '1';
  
    RETURN v_present;
  
  END get_present_days;

  /*
  FUNCTION get_absent_days(p_org_id NUMBER,
                              p_fiscal_year IN VARCHAR2,
                p_from_date IN DATE,
                p_to_date IN DATE,
                p_staff_id IN NUMBER) RETURN NUMBER IS
   v_absent NUMBER(10):=0;
  BEGIN
     SELECT COUNT(*)
       INTO v_absent
     FROM HR_STAFF_ATTENDANCE a,
          HR_LEAVE_APPLICATIONS b,
            HR_LEAVE_DEFINITIONS c
     WHERE a.ORG_ID=p_org_id
      AND a.FISCAL_YEAR=p_fiscal_year
      AND a.LEAVE_REQ_ID=B.LEAVE_REQ_ID
      AND B.LEAVE_ID=C.LEAVE_ID
      AND C.WITH_PAY_YN='Y'
      AND a.STAFF_ID=p_staff_id
      AND a.ATTENDANCE_TYPE IN ('ABSENT','LEAVE')
      AND a.ATTENDANCE_DATE>=p_from_date
      AND a.ATTENDANCE_DATE<=p_to_date
      AND a.ENABLED_FLAG='1';
  
      RETURN v_absent;
  
  END get_absent_days;
  */

  PROCEDURE get_LOP_days(p_org_id      VARCHAR2,
                         p_fiscal_year IN VARCHAR2,
                         p_dept_id     IN VARCHAR2,
                         p_staff_id    IN VARCHAR2,
                         p_leave_id    IN VARCHAR2,
                         p_from_date   IN DATE,
                         p_to_date     IN DATE,
                         p_level       IN VARCHAR2,
                         p_lop_factor  OUT NUMBER,
                         p_lop_days    OUT NUMBER) IS
  
    Cursor leave_condition(c_leave_id VARCHAR2,
                           c_staff_id VARCHAR2,
                           c_level    VARCHAR2 DEFAULT NULL) IS
      SELECT B.LC_TYPE,
             B.LC_LOW_VALUE,
             B.LC_HIGH_VALUE,
             B.LC_VALUE,
             B.LC_CONDITION,
             C.LEAVE_QUOTA,
             C.NO_OF_DAYS,
             C.LSD_ID,
             C.LEAVE_BALANCE
        FROM HR_LEAVE_CONDITIONS_HDR   A,
             HR_LEAVE_CONDITIONS_DTL   B,
             HR_LEAVE_STAFF_DEFINTIONS C
       WHERE A.LC_HDR_ID = B.LC_HDR_ID
         AND A.LC_LEAVE_ID = C.LEAVE_ID
            --AND  B.LC_LOW_VALUE>=v_lop_days AND B.LC_HIGH_VALUE<=v_lop_days
         AND A.LC_LEAVE_ID = C_leave_id
         AND C.STAFF_ID = c_staff_id
         AND NVL(B.LC_CONDITION, '1') =
             NVL(c_level, NVL(B.LC_CONDITION, '1'))
         AND A.ENABLED_FLAG = '1'
         AND A.WORKFLOW_COMPLETION_STATUS = '1'
         AND B.ENABLED_FLAG = '1'
         AND B.WORKFLOW_COMPLETION_STATUS = '1'
       ORDER BY B.LC_LOW_VALUE;
  
    v_total_leave_days NUMBER(10) := 0;
    v_leave_for_month  NUMBER(10) := 0;
    v_last_month_leave NUMBER(10) := 0;
    v_leave_bal        NUMBER := 0;
    v_lop_days         NUMBER(10) := 0;
    v_lc_type          HR_LEAVE_CONDITIONS_DTL.LC_TYPE%TYPE;
    v_lc_value         HR_LEAVE_CONDITIONS_DTL.LC_VALUE%TYPE := 1;
    v_lc_condition     HR_LEAVE_CONDITIONS_DTL.LC_CONDITION%TYPE;
    v_not_exists       CHAR(1) := '0';
    v_week_day_off1    varchar2(50);
    v_week_day_off2    varchar2(50);
    --PRAGMA AUTONOMOUS_TRANSACTION;
  BEGIN
  
    v_week_day_off1 := ssm.get_parameter_value('WEEKOFF1');
    v_week_day_off2 := ssm.get_parameter_value('WEEKOFF2');
  
    BEGIN
      SELECT '1'
        INTO v_not_exists
        FROM DUAL
       WHERE NOT EXISTS
       (SELECT '1'
                FROM HR_LEAVE_CONDITIONS_HDR A, HR_LEAVE_CONDITIONS_DTL B
               WHERE B.LC_HDR_ID = A.LC_HDR_ID
                 AND A.ENABLED_FLAG = '1'
                 AND A.WORKFLOW_COMPLETION_STATUS = '1'
                 AND A. LC_LEAVE_ID = P_LEAVE_ID --'EL'
                 AND A.EFFECTIVE_TO_DT IS NULL
                 AND ROWNUM <= 1);
    EXCEPTION
      WHEN OTHERS THEN
        v_not_exists := '0';
    END;
    ----dbms_output.put_line('Exists '||v_not_exists);
    IF v_not_exists = '0' THEN
      ----dbms_output.put_line('Dates '||to_char(p_from_date)||' - '||to_char(p_to_date));
      SELECT COUNT(*)
        INTO v_total_leave_days
        FROM HR_STAFF_ATTENDANCE   a,
             HR_LEAVE_APPLICATIONS b,
             HR_LEAVE_DEFINITIONS  c
       WHERE a.LEAVE_REQ_ID = B.LEAVE_REQ_ID
         AND B.LEAVE_ID = C.LEAVE_ID
         AND C.WITH_PAY_YN = '1'
            --         AND UPPER(LTRIM(RTRIM(a.ATTENDANCE_TYPE))) IN ('ABSENT', 'LEAVE')
         AND a.ENABLED_FLAG = '1'
         AND a.ORG_ID = p_org_id
         AND a.FISCAL_YEAR = p_fiscal_year
         AND a.STAFF_ID = p_staff_id
         AND c.leave_id = p_leave_id
         AND a.ATTENDANCE_DATE <= p_to_date
         AND trim(upper(to_char(a.attendance_date, 'Day'))) not in
             (v_week_day_off1, v_week_day_off2);
    
      SELECT COUNT(*)
        INTO v_last_month_leave
        FROM HR_STAFF_ATTENDANCE   a,
             HR_LEAVE_APPLICATIONS b,
             HR_LEAVE_DEFINITIONS  c
       WHERE a.LEAVE_REQ_ID = B.LEAVE_REQ_ID
         AND B.LEAVE_ID = C.LEAVE_ID
         AND C.WITH_PAY_YN = '1'
            --         AND UPPER(LTRIM(RTRIM(a.ATTENDANCE_TYPE))) IN ('ABSENT', 'LEAVE')
         AND a.ENABLED_FLAG = '1'
         AND a.ORG_ID = p_org_id
         AND a.FISCAL_YEAR = p_fiscal_year
         AND a.STAFF_ID = p_staff_id
         AND c.leave_id = p_leave_id
         AND a.ATTENDANCE_DATE <= (last_day(add_months(p_from_date, -1)))
            
         AND trim(upper(to_char(a.attendance_date, 'Day'))) not in
             (v_week_day_off1, v_week_day_off2);
      --AND a.ATTENDANCE_DATE>=p_from_date
      --AND a.ATTENDANCE_DATE<=p_to_date;
      ----dbms_output.put_line('Leave upto lastday-VAR1  '||v_total_leave_days||' For emp '||p_staff_id||' Leave ID '||p_leave_id);
      ----dbms_output.put_line('Leave upto previous-VAR2 '||v_last_month_leave||' For emp '||p_staff_id||' Leave ID '||p_leave_id);
    
      FOR l_cur IN leave_condition(p_leave_id, p_staff_id, p_level) LOOP
        ----dbms_output.put_line('Records '||l_cur.lc_type||' - '||' - '||l_cur.lc_low_value||' - '||l_cur.lc_high_value||' - '||l_cur.lc_value||' - '||l_cur.lc_condition);
        IF l_cur.lc_type = 'N-A' THEN
          v_lc_value := 1;
        ELSIF l_cur.lc_type IN ('SLAB', 'CONDITION') THEN
          IF l_cur.lc_high_value IS NOT NULL THEN
            IF v_total_leave_days BETWEEN l_cur.lc_low_value AND
               l_cur.lc_high_value THEN
              v_lc_value := l_cur.lc_value / 100;
              p_lop_days := 0;
              EXIT;
            END IF;
          ELSE
            v_leave_for_month := 0;
            v_lc_value        := 0;
            SELECT COUNT(*)
              INTO v_leave_for_month
              FROM HR_STAFF_ATTENDANCE   a,
                   HR_LEAVE_APPLICATIONS b,
                   HR_LEAVE_DEFINITIONS  c
             WHERE a.LEAVE_REQ_ID = B.LEAVE_REQ_ID
               AND B.LEAVE_ID = C.LEAVE_ID
               AND C.WITH_PAY_YN = '1'
                  --               AND UPPER(LTRIM(RTRIM(a.ATTENDANCE_TYPE))) IN
                  --                   ('ABSENT', 'LEAVE')
               AND a.ENABLED_FLAG = '1'
               AND a.ORG_ID = p_org_id
               AND a.FISCAL_YEAR = p_fiscal_year
               AND a.STAFF_ID = p_staff_id
               AND c.leave_id = p_leave_id
               AND a.ATTENDANCE_DATE >= p_from_date
               AND a.ATTENDANCE_DATE <= p_to_date
               AND trim(upper(to_char(a.attendance_date, 'Day'))) not in
                   (v_week_day_off1, v_week_day_off2);
          
            ----dbms_output.put_line('Leave for the month '||v_leave_for_month|| ' For Staff '||p_staff_id||' Leave ID '||p_leave_id);
          
            IF v_last_month_leave <= l_cur.no_of_days THEN
            
              v_lop_days := v_total_leave_days - l_cur.no_of_days;
              p_lop_days := v_lop_days;
            ELSE
            
              v_lop_days := v_leave_for_month;
              p_lop_days := v_lop_days;
            END IF;
            ----dbms_output.put_line('Actual LOP days '||v_lop_days);
          END IF;
        END IF;
      END LOOP;
      ----dbms_output.put_line('v_lc_value11 '||v_lc_value);
      p_lop_factor := v_lc_value;
    
    ELSIF v_not_exists = '1' THEN
      p_lop_factor := 1;
      v_leave_bal  := pkg_payroll.get_emp_curr_leave_bal(p_fiscal_year => p_fiscal_year,
                                                         p_emp_id      => p_staff_id,
                                                         p_org_id      => p_org_id,
                                                         p_leave_id    => p_leave_id,
                                                         p_dept_id     => p_dept_id);
    
      SELECT COUNT(*)
        INTO v_leave_for_month
        FROM HR_STAFF_ATTENDANCE   a,
             HR_LEAVE_APPLICATIONS b,
             HR_LEAVE_DEFINITIONS  c
       WHERE a.LEAVE_REQ_ID = B.LEAVE_REQ_ID
         AND B.LEAVE_ID = C.LEAVE_ID
         AND C.WITH_PAY_YN = '1'
            --         AND UPPER(LTRIM(RTRIM(a.ATTENDANCE_TYPE))) IN ('ABSENT', 'LEAVE')
         AND a.ENABLED_FLAG = '1'
         AND a.ORG_ID = p_org_id
         AND a.FISCAL_YEAR = p_fiscal_year
         AND a.STAFF_ID = p_staff_id
         AND c.leave_id = p_leave_id
         AND a.ATTENDANCE_DATE >= p_from_date
         AND a.ATTENDANCE_DATE <= p_to_date;
    
      v_total_leave_days := v_leave_for_month - ABS(v_leave_bal);
    
      IF v_leave_bal >= 0 THEN
        p_lop_days := 0;
      ELSIF v_total_leave_days > 0 THEN
        p_lop_days := ABS(v_leave_bal);
      ELSIF v_total_leave_days < 0 THEN
        p_lop_days := NVL(v_leave_for_month, 0);
      END IF;
    END IF;
  END get_LOP_days;

  PROCEDURE get_AL_days(p_org_id       VARCHAR2,
                        p_fiscal_year  IN VARCHAR2,
                        p_dept_id      IN VARCHAR2,
                        p_staff_id     IN VARCHAR2,
                        p_leave_id     IN VARCHAR2,
                        p_from_date    IN DATE,
                        p_to_date      IN DATE,
                        p_level        IN VARCHAR2,
                        p_lop_factor   OUT NUMBER,
                        p_lop_days     OUT NUMBER,
                        p_leave_req_id IN VARCHAR2) IS
  
    v_total_leave_days NUMBER(10) := 0;
    v_leave_for_month  NUMBER(10) := 0;
    v_last_month_leave NUMBER(10) := 0;
    v_leave_bal        NUMBER := 0;
    v_lop_days         NUMBER(10) := 0;
    v_lc_type          HR_LEAVE_CONDITIONS_DTL.LC_TYPE%TYPE;
    v_lc_value         HR_LEAVE_CONDITIONS_DTL.LC_VALUE%TYPE := 1;
    v_lc_condition     HR_LEAVE_CONDITIONS_DTL.LC_CONDITION%TYPE;
    v_not_exists       CHAR(1) := '0';
    v_week_day_off1    varchar2(50);
    v_week_day_off2    varchar2(50);
    v_full_leave       number;
    v_query            varchar2(5000);
    --PRAGMA AUTONOMOUS_TRANSACTION;
  BEGIN
  
    v_week_day_off1 := ssm.get_parameter_value('WEEKOFF1');
    v_week_day_off2 := ssm.get_parameter_value('WEEKOFF2');
  
    select count('x')
      into v_full_leave
      from hr_leave_applications hla
     where hla.leave_date_from <= p_from_date
       and hla.leave_date_to >= p_to_date
       and hla.staff_id = p_staff_id
       and hla.app_status = 'Approved'
       and hla.enabled_flag = '1'
       and hla.workflow_completion_status = '1'
       and hla.leave_id = p_leave_id
       and hla.leave_req_id = p_leave_req_id;
  
    if v_full_leave = 1 then
      v_total_leave_days := get_days(p_from_dt => p_from_date,
                                     p_to_dt   => p_to_date);
    else
      --debug_proc('start of get_AL_days');
      SELECT COUNT(*)
        INTO v_total_leave_days
        FROM HR_STAFF_ATTENDANCE   a,
             HR_LEAVE_APPLICATIONS b,
             HR_LEAVE_DEFINITIONS  c
       WHERE a.LEAVE_REQ_ID = B.LEAVE_REQ_ID
         AND B.LEAVE_ID = C.LEAVE_ID
         AND C.WITH_PAY_YN = '1'
         AND a.ENABLED_FLAG = '1'
         AND a.ORG_ID = p_org_id
         AND a.STAFF_ID = p_staff_id
         AND c.leave_id = p_leave_id
         AND a.ATTENDANCE_DATE >= p_from_date
         AND a.ATTENDANCE_DATE <= p_to_date
         and a.leave_req_id = p_leave_req_id
         AND trim(upper(to_char(a.attendance_date, 'Day'))) not in
             (upper(v_week_day_off1), upper(v_week_day_off2))
         and exists (select *
                from hr_leave_encashment hle
               where hle.trans_id = b.leave_req_id)
         and not exists
       (select *
                from hr_holidays_master hhm
               where hhm.holiday_date = a.attendance_date
               and hhm.created_date <= b.application_date);

/*v_query := '      SELECT COUNT(*)
        INTO v_total_leave_days
        FROM HR_STAFF_ATTENDANCE   a,
             HR_LEAVE_APPLICATIONS b,
             HR_LEAVE_DEFINITIONS  c
       WHERE a.LEAVE_REQ_ID = B.LEAVE_REQ_ID
         AND B.LEAVE_ID = C.LEAVE_ID
         AND C.WITH_PAY_YN = ''1''
         AND a.ENABLED_FLAG = ''1''
         AND a.ORG_ID = '||p_org_id||'
            -- AND a.FISCAL_YEAR = p_fiscal_year
         AND a.STAFF_ID = '||p_staff_id||'
         AND c.leave_id = '||p_leave_id||'
         AND a.ATTENDANCE_DATE >= '||p_from_date||'
         AND a.ATTENDANCE_DATE <= '||p_to_date||'
         and a.leave_req_id = '||p_leave_req_id||'
         AND trim(upper(to_char(a.attendance_date, ''Day''))) not in
             (upper(v_week_day_off1), upper(v_week_day_off2))
         and exists (select *
                from hr_leave_encashment hle
               where hle.trans_id = b.leave_req_id)
         and not exists
       (select *
                from hr_holidays_master hhm
               where hhm.holiday_date = a.attendance_date
               and hhm.created_date <= b.application_date);';
 dbms_output.put_line(v_query);
*/
   end if;
    p_lop_days   := v_total_leave_days;
    p_lop_factor := 1;
  END get_AL_days;

  --FUNCTION get_fiscal_year(p_org_id IN VARCHAR2, p_year IN VARCHAR2)
  FUNCTION get_fiscal_year(p_org_id IN VARCHAR2, p_year IN date)
    RETURN VARCHAR2 IS
    v_cal GL_ACCT_CALENDAR_DTL.CAL_DTL_ID%TYPE;
    -- v_year date;
  BEGIN
    --v_year := '01/jan/'||p_year;
    /*
      SELECT y.comp_cal_id
        INTO v_cal
    FROM gl_companies_hdr x,
    gl_companies_dtl y
    WHERE y.comp_id=x.comp_id
    and x.enabled_flag            ='1'
    AND x.workflow_completion_status='1'
    and y.enabled_flag            ='1'
    AND y.workflow_completion_status='1'
    AND y.effective_end_dt         IS NULL
    AND x.comp_id=p_org_id;
    */
    SELECT B.CAL_DTL_ID
      INTO v_cal
      FROM GL_ACCT_CALENDAR_HDR      A,
           GL_ACCT_CALENDAR_DTL      B,
           GL_COMP_ACCT_CALENDAR_DTL C
     WHERE B.CAL_ID = A.CAL_ID
       AND C.CAL_ID = A.CAL_ID
       AND C.CAL_DTL_ID = B.CAL_DTL_ID
       AND C.CAL_ACCT_YEAR = B.CAL_ACCT_YEAR
       AND C.COMP_ID = p_org_id
          --AND B.CAL_ACCT_YEAR = p_year
          -- AND B.CAL_EFF_START_DT>=p_year
          -- AND B.CALL_EFF_END_DT<=p_year
       AND p_year BETWEEN B.CAL_EFF_START_DT AND B.CALL_EFF_END_DT
       AND A.ENABLED_FLAG = '1'
       AND A.WORKFLOW_COMPLETION_STATUS = '1'
       AND B.ENABLED_FLAG = '1'
       AND B.WORKFLOW_COMPLETION_STATUS = '1'
       AND C.ENABLED_FLAG = '1'
       AND C.WORKFLOW_cOMPLETION_STATUS = '1';
  
    RETURN v_cal;
  EXCEPTION
    WHEN OTHERS THEN
      DBMS_OUTPUT.PUT_LINE('ERR IN FISCAL YEAR ' || SQLERRM);
      RETURN '';
  END get_fiscal_year;

  FUNCTION get_mgrp_amt(p_org_id IN VARCHAR2,
                        p_emp_id IN VARCHAR2,
                        p_year   IN VARCHAR2) RETURN NUMBER IS
    l_mgrp_amt NUMBER := 0;
  BEGIN
    SELECT EMP_MGRP_AMT
      INTO l_mgrp_amt
      FROM HR_EMP_MGRP_DTLS
     WHERE EMP_ID = p_emp_id
       AND EMP_MGRP_YEAR = p_year
       AND EMP_MGRP_ORG_ID = p_org_id
       AND ENABLED_FLAG = '1'
       AND WORKFLOW_COMPLETION_STATUS = '1';
  
    RETURN l_mgrp_amt;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN 0;
  END get_mgrp_amt;

  FUNCTION get_loan_deduction(p_org_id  IN VARCHAR2,
                              p_dept_id IN VARCHAR2,
                              p_emp_id  IN VARCHAR2,
                              p_from_dt IN DATE,
                              p_to_dt   IN DATE) RETURN NUMBER IS
    v_loan_amt NUMBER := 0;
  BEGIN
    SELECT SUM(B.REPAY_AMT)
      INTO v_loan_amt
      FROM HR_ADVANCE_REPAY_PLAN B, HR_ADVANCE_REQ A
     WHERE B.ENABLED_FLAG = '1'
       AND B.WORKFLOW_COMPLETION_STATUS = '1'
       AND B.REPAY_PAIDYN = '0'
       AND A.ENABLED_FLAG = '1'
       AND A.WORKFLOW_COMPLETION_STATUS = '1'
       AND A.REQ_ID = B.RES_ID
       AND A.REQ_ORG_ID = p_org_id
       AND A.REQ_DEPT_ID = p_dept_id
       AND A.REQ_EMP_ID = p_emp_id
       AND B.REPAY_DT <= p_to_dt;
    --AND A.REQ_ID='LRID-0000000013'
    RETURN NVL(v_loan_amt, 0);
  END get_loan_deduction;

  FUNCTION get_nr_deduction(p_org_id     IN VARCHAR2,
                            p_pay_period IN VARCHAR2,
                            p_element_id IN VARCHAR2,
                            p_dept_id    IN VARCHAR2,
                            p_emp_id     IN VARCHAR2) RETURN NUMBER IS
    v_ded_amt NUMBER := 0;
  BEGIN
    SELECT sum(b.pay_ded_amount)
      INTO v_ded_amt
      FROM pay_nr_elements_hdr a, pay_nr_elements_dtl b
     WHERE NVL(a.pay_period_status, 'XXXX') != 'PROCESSED'
       AND a.pay_nr_id = b.pay_nr_id
       AND a.pay_period_id = p_pay_period
       AND a.pay_element_id = p_element_id
       AND b.pay_emp_dept_id = p_dept_id
       AND b.pay_emp_id = p_emp_id
       AND a.enabled_flag = '1'
       AND a.workflow_completion_status = '1'
       AND b.enabled_flag = '1'
       AND b.workflow_completion_status = '1';
  
    RETURN v_ded_amt;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN 0;
  END get_nr_deduction;

  FUNCTION get_days(p_from_dt DATE, p_to_dt DATE) RETURN NUMBER IS
    v_days         NUMBER := 0;
    v_from_dt      DATE := p_from_dt;
    v_weekly_off_1 varchar2(100) := ssm.get_parameter_value('WEEKOFF1');
    v_weekly_off_2 varchar2(100) := ssm.get_parameter_value('WEEKOFF2');
    v_day          varchar2(100);
  BEGIN
    --v_days:=p_to_dt-p_from_dt;
    WHILE v_from_dt <= p_to_dt LOOP
      -- --dbms_output.put_line('Days '||TO_CHAR(v_from_dt,'DAY'));
      -- --dbms_output.put_line(' v_from_dt '||v_from_dt);
      v_day := (LTRIM(RTRIM(TO_CHAR(v_from_dt, 'DAY'))));
      IF (v_day = v_weekly_off_1 or v_day = v_weekly_off_2) THEN
        v_days := v_days + 0;
      else
        v_days := v_days + 1;
      
      END IF;
      v_from_dt := v_from_dt + 1;
    END LOOP;
    RETURN v_days;
  END get_days;

  FUNCTION get_leave_days(p_org_id VARCHAR2, p_from_dt DATE, p_to_dt DATE)
    RETURN NUMBER IS
    v_days         NUMBER := 0;
    v_from_dt      DATE := p_from_dt;
    v_weekly_off_1 varchar2(100) := ssm.get_parameter_value('WEEKOFF1');
    v_weekly_off_2 varchar2(100) := ssm.get_parameter_value('WEEKOFF2');
    v_day          varchar2(100);
    v_holidays     number := 0;
  BEGIN
    --v_days:=p_to_dt-p_from_dt;
    WHILE v_from_dt <= p_to_dt LOOP
      -- --dbms_output.put_line('Days '||TO_CHAR(v_from_dt,'DAY'));
      -- --dbms_output.put_line(' v_from_dt '||v_from_dt);
      v_day := (LTRIM(RTRIM(TO_CHAR(v_from_dt, 'DAY'))));
      IF (v_day = v_weekly_off_1 or v_day = v_weekly_off_2) THEN
        v_days := v_days + 0;
      else
        v_days := v_days + 1;
      
      END IF;
      v_from_dt := v_from_dt + 1;
    END LOOP;
    v_holidays := get_holidays(p_org_id      => p_org_id,
                               p_fiscal_year => null,
                               p_from_dt     => p_from_dt,
                               p_to_dt       => p_to_dt);
    RETURN v_days - v_holidays;
  END get_leave_days;

  FUNCTION get_last_pay_dt(p_org_id IN VARCHAR2, p_emp_id IN VARCHAR2)
    RETURN DATE IS
    v_last_pay_dt DATE;
  BEGIN
    SELECT max(EMP_LAST_SALARY_DT)
      INTO v_last_pay_dt
      FROM HR_EMP_WORK_DTLS
     WHERE EMP_ID = p_emp_id
       AND EMP_ORG_ID = p_org_id; /*
          AND ENABLED_FLAG='1'
        AND WORKFLOW_COMPLETION_STATUS='1'
        AND EFFECTIVE_TO_DT IS NULL;*/
  
    RETURN v_last_pay_dt;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN NULL;
  END get_last_pay_dt;
  FUNCTION get_emp_leave_bal(p_fiscal_year IN VARCHAR2,
                             p_org_id      IN VARCHAR2,
                             p_dept_id     IN VARCHAR2,
                             p_emp_id      IN VARCHAR2,
                             p_leave_id    IN VARCHAR2) RETURN NUMBER IS
    v_leave_bal NUMBER := 0;
  BEGIN
    SELECT XP.LDR_LEAVE_BAL
      INTO v_leave_bal
      FROM HR_LEAVE_LEDGER XP
     WHERE XP.LDR_EMP_ID = p_emp_id --'EMP_ID-0000000034'
       AND XP.LDR_FISCAL_YEAR = p_fiscal_year
       AND XP.LDR_ORG_ID = p_org_id
       AND XP.LDR_DEPT_ID = p_dept_id
       AND XP.LDR_LEAVE_ID = p_leave_id
       AND XP.LDR_ID = (SELECT MAX(LDR_ID)
                          FROM HR_LEAVE_LEDGER
                         WHERE LDR_EMP_ID = p_emp_id --'EMP_ID-0000000034'
                           AND LDR_FISCAL_YEAR = p_fiscal_year
                           AND LDR_ORG_ID = p_org_id
                           AND LDR_DEPT_ID = p_dept_id
                           AND LDR_LEAVE_ID = p_leave_id);
    RETURN v_leave_bal;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN 0;
  END get_emp_leave_bal;

  PROCEDURE get_element_stop_dtl(p_emp_id       IN VARCHAR2,
                                 p_element_id   IN VARCHAR2,
                                 p_from_dt      IN DATE,
                                 p_to_dt        IN DATE,
                                 p_exclude_fd   IN CHAR DEFAULT '0',
                                 p_payroll_days OUT NUMBER,
                                 p_skip_element OUT CHAR,
                                 p_ignore_emp   OUT CHAR) IS
  
    v_period_from_dt DATE := p_from_dt; --'01-JAN-14';
    v_period_to_dt   DATE := p_to_dt; --'31-JAN-14';
  
    CURSOR stop_res(c_emp_id VARCHAR2, c_element_id VARCHAR2) IS
      SELECT *
        FROM PAY_EMP_STOP_RESUME_PAY
       WHERE EMP_ID = c_emp_id
            --AND ELEMENT_ID=c_element_id
         AND NVL(ELEMENT_ID, '1') = NVL(c_element_id, NVL(ELEMENT_ID, '1'))
         AND ENABLED_FLAG = '1'
         AND WORKFLOW_COMPLETION_STATUS = '1'
       ORDER BY EMP_STOP_FROM_DT;
  
    v_ded_days      NUMBER := 0;
    v_payroll_days  NUMBER := 0;
    v_ignore_emp    CHAR(1) := '0';
    v_all_elements  CHAR(1);
    v_skip_element  CHAR(1) := '0';
    v_stop_n_resume CHAR(1) := '0';
    v_fixed_days    number;
  BEGIN
    --dbms_output.put_line('get_element_stop_dtl-Payroll dates ' || v_period_from_dt || ' -  ' || v_period_to_dt);
    v_fixed_days := ssm.get_parameter_value(p_param_code => 'FIXEDDAYS');
    if p_exclude_fd = '1' then
      v_fixed_days := 0;
    end if;
    if v_fixed_days > 0 then
      v_payroll_days := v_fixed_Days;
    else
      v_payroll_days := get_days(p_from_dt => v_period_from_dt,
                                 p_to_dt   => v_period_to_dt);
    end if;
    --    v_payroll_days := pkg_payroll.get_days(v_period_from_dt, v_period_to_dt);
    FOR m2 IN stop_res(p_emp_id, p_element_id) LOOP
      --('EMP_ID-0000000038','PYE_ID-0000000004') LOOP
      v_stop_n_resume := m2.stop_resume;
      --dbms_output.put_line('Records ' || m2.pay_stop_id || ' - ' ||
      -- m2.emp_stop_from_dt || ' - ' ||
      -- m2.emp_stop_to_dt || ' - ' || m2.resume_date);
      IF nvl(m2.emp_all_elements, '0') = '0' THEN
        IF m2.emp_stop_to_dt IS NULL and NVL(m2.resume_flag, '0') = '0' and
           m2.emp_stop_from_dt < v_period_from_dt THEN
          v_skip_element := '1';
        ELSIF m2.emp_stop_to_dt IS NULL and NVL(m2.resume_flag, '0') = '0' and
              m2.emp_stop_from_dt > v_period_from_dt THEN
          --dbms_output.put_line('One');
          v_ded_days     := v_ded_days +
                            pkg_payroll.get_days(v_period_from_dt,
                                                 m2.emp_stop_from_dt);
          v_skip_element := '0'; --Change done here
        ELSE
          IF v_period_from_dt > m2.emp_stop_from_dt AND
             v_period_from_dt <= m2.emp_stop_to_dt THEN
            --dbms_output.put_line('v_period_from_dt ' ||
            -- to_char(v_period_from_dt) ||
            -- ' -emp_stop_From_dt ' ||
            -- to_char(m2.emp_stop_from_dt) ||
            -- ' - emp_stop_to_dt ' ||
            -- to_char(m2.emp_stop_to_dt));
            v_ded_days := v_ded_days +
                          pkg_payroll.get_days(v_period_from_dt,
                                               m2.emp_stop_to_dt); --changed here
            --v_ded_days := v_ded_days + pkg_payroll.get_days(m2.emp_stop_to_dt,v_period_to_dt);
            --dbms_output.put_line('Ded1 ' || v_ded_days);
          ELSIF v_period_to_dt > m2.emp_stop_from_dt AND
                v_period_to_dt < m2.emp_stop_to_dt THEN
            v_ded_days := v_ded_days +
                          pkg_payroll.get_days(v_period_to_dt,
                                               m2.emp_stop_to_dt); --changed here
            --v_ded_days := v_ded_days +pkg_payroll.get_days(v_period_from_dt,m2.emp_stop_from_dt);
            --dbms_output.put_line('Ded2 ' || v_ded_days);
          ELSIF (LTRIM(RTRIM(TO_CHAR(TO_DATE(v_period_to_dt, 'DD-MON-YY'),
                                     'MON-YY'))) =
                LTRIM(RTRIM(TO_CHAR(m2.emp_stop_from_dt, 'MON-YY')))) OR
                (LTRIM(RTRIM(TO_CHAR(TO_DATE(v_period_to_dt, 'DD-MON-YY'),
                                     'MON-YY'))) =
                LTRIM(RTRIM(TO_CHAR(m2.emp_stop_to_dt, 'MON-YY')))) THEN
            v_ded_days := v_ded_days +
                          pkg_payroll.get_days(m2.emp_stop_from_dt,
                                               m2.emp_stop_to_dt);
            --dbms_output.put_line('Ded3 ' || v_ded_days);
          END IF;
        END IF;
      ELSIF m2.emp_all_elements = '1' THEN
        --Ignore employee becuase of stop all elements and its dependant logic goes here
        IF m2.emp_stop_to_dt IS NULL and NVL(m2.resume_flag, '0') = '0' and
           m2.emp_stop_from_dt <= v_period_from_dt THEN
          v_ignore_emp := '1';
          --start
        ELSIF m2.emp_stop_to_dt IS NULL and NVL(m2.resume_flag, '0') = '0' and
              m2.emp_stop_from_dt > v_period_from_dt THEN
          ----dbms_output.put_line('One');
          v_ded_days   := v_ded_days +
                          pkg_payroll.get_days(v_period_from_dt,
                                               m2.emp_stop_from_dt);
          v_ignore_emp := '1'; --Change done here
        ELSE
          IF v_period_from_dt > m2.emp_stop_from_dt AND
             v_period_from_dt < m2.emp_stop_to_dt THEN
            ----dbms_output.put_line('v_period_from_dt '||to_char(v_period_from_dt)||' -emp_stop_From_dt '||to_char(m2.emp_stop_from_dt)||' - emp_stop_to_dt '||to_char(m2.emp_stop_to_dt));
            v_ded_days   := v_ded_days +
                            pkg_payroll.get_days(v_period_from_dt,
                                                 m2.emp_stop_to_dt);
            v_ignore_emp := '1';
            ----dbms_output.put_line('Ded1 '||v_ded_days);
          ELSIF v_period_to_dt > m2.emp_stop_from_dt AND
                v_period_to_dt < m2.emp_stop_to_dt THEN
            v_ded_days := v_ded_days +
                          pkg_payroll.get_days(v_period_to_dt,
                                               m2.emp_stop_to_dt);
            ----dbms_output.put_line('Ded2 '||v_ded_days);
            v_ignore_emp := '1';
          ELSIF (LTRIM(RTRIM(TO_CHAR(TO_DATE(v_period_to_dt, 'DD-MON-YY'),
                                     'MON-YY'))) =
                LTRIM(RTRIM(TO_CHAR(m2.emp_stop_from_dt, 'MON-YY')))) OR
                (LTRIM(RTRIM(TO_CHAR(TO_DATE(v_period_to_dt, 'DD-MON-YY'),
                                     'MON-YY'))) =
                LTRIM(RTRIM(TO_CHAR(m2.emp_stop_to_dt, 'MON-YY')))) THEN
            v_ded_days := v_ded_days +
                          pkg_payroll.get_days(m2.emp_stop_from_dt,
                                               m2.emp_stop_to_dt);
            ----dbms_output.put_line('Ded3 '||v_ded_days);
            v_ignore_emp := '1';
          END IF;
          --end
        END IF;
      END IF;
    END LOOP;
    IF v_skip_element = '0' THEN
      ----dbms_output.put_line('Payroll days '||v_payroll_days);
      ----dbms_output.put_line('Deduction days '||v_ded_days);
      ----dbms_output.put_line('Payable days '||(v_payroll_days-v_ded_days));
      ----dbms_output.put_line(' v_stop_n_resume '||v_stop_n_resume);
      IF v_stop_n_resume = '1' THEN
        ----dbms_output.put_line('Payable days '||v_ded_days);
        p_payroll_days := v_ded_days;
      ELSE
        ----dbms_output.put_line('Payable days '||(v_payroll_days-v_ded_days));
        p_payroll_days := (v_payroll_days - v_ded_days);
      END IF;
    
    ELSIF v_skip_element = '1' THEN
      --dbms_output.put_line('Element skipped');
      p_payroll_days := 0;
    END IF;
    p_skip_element := v_skip_element;
    p_ignore_emp   := v_ignore_emp;
  END get_element_stop_dtl;

  --start
  PROCEDURE get_element_stop_dtl_ls(p_emp_id       IN VARCHAR2,
                                    p_element_id   IN VARCHAR2,
                                    p_from_dt      IN DATE,
                                    p_to_dt        IN DATE,
                                    p_payroll_days OUT NUMBER,
                                    p_skip_element OUT CHAR,
                                    p_ignore_emp   OUT CHAR) IS
  
    v_period_from_dt DATE := p_from_dt; --'01-JAN-14';
    v_period_to_dt   DATE := p_to_dt; --'31-JAN-14';
  
    CURSOR stop_res(c_emp_id VARCHAR2, c_element_id VARCHAR2) IS
      SELECT *
        FROM PAY_EMP_STOP_RESUME_PAY
       WHERE EMP_ID = c_emp_id
            --AND ELEMENT_ID=c_element_id
         AND NVL(ELEMENT_ID, '1') = NVL(c_element_id, NVL(ELEMENT_ID, '1'))
         AND ENABLED_FLAG = '1'
         AND WORKFLOW_COMPLETION_STATUS = '1'
       ORDER BY EMP_STOP_FROM_DT;
  
    v_ded_days      NUMBER := 0;
    v_payroll_days  NUMBER := 0;
    v_ignore_emp    CHAR(1) := '0';
    v_all_elements  CHAR(1);
    v_skip_element  CHAR(1) := '0';
    v_stop_n_resume CHAR(1) := '0';
    v_fixed_days    number;
  BEGIN
    --dbms_output.put_line('Payroll dates ' || v_period_from_dt || ' -  ' ||
    --v_period_to_dt);
    v_fixed_days := ssm.get_parameter_value(p_param_code => 'FIXEDDAYS');
    --if v_fixed_days > 0 then
    --v_payroll_days:=v_fixed_Days;
    --else
    v_payroll_days := get_days(p_from_dt => v_period_from_dt,
                               p_to_dt   => v_period_to_dt);
    --end if;
    --    v_payroll_days := pkg_payroll.get_days(v_period_from_dt, v_period_to_dt);
    FOR m2 IN stop_res(p_emp_id, p_element_id) LOOP
      --('EMP_ID-0000000038','PYE_ID-0000000004') LOOP
      v_stop_n_resume := m2.stop_resume;
      --dbms_output.put_line('Records ' || m2.pay_stop_id || ' - ' ||
      --m2.emp_stop_from_dt || ' - ' ||
      --m2.emp_stop_to_dt || ' - ' || m2.resume_date);
      IF nvl(m2.emp_all_elements, '0') = '0' THEN
        IF m2.emp_stop_to_dt IS NULL and NVL(m2.resume_flag, '0') = '0' and
           m2.emp_stop_from_dt < v_period_from_dt THEN
          v_skip_element := '1';
        ELSIF m2.emp_stop_to_dt IS NULL and NVL(m2.resume_flag, '0') = '0' and
              m2.emp_stop_from_dt > v_period_from_dt THEN
          --dbms_output.put_line('One');
          v_ded_days     := v_ded_days +
                            pkg_payroll.get_days(v_period_from_dt,
                                                 m2.emp_stop_from_dt);
          v_skip_element := '0'; --Change done here
        ELSE
          IF v_period_from_dt > m2.emp_stop_from_dt AND
             v_period_from_dt <= m2.emp_stop_to_dt THEN
            --dbms_output.put_line('v_period_from_dt ' ||
            -- to_char(v_period_from_dt) ||
            -- ' -emp_stop_From_dt ' ||
            -- to_char(m2.emp_stop_from_dt) ||
            -- ' - emp_stop_to_dt ' ||
            -- to_char(m2.emp_stop_to_dt));
            v_ded_days := v_ded_days +
                          pkg_payroll.get_days(v_period_from_dt,
                                               m2.emp_stop_to_dt);
            --dbms_output.put_line('Ded1 ' || v_ded_days);
          ELSIF v_period_to_dt > m2.emp_stop_from_dt AND
                v_period_to_dt < m2.emp_stop_to_dt THEN
            v_ded_days := v_ded_days +
                          pkg_payroll.get_days(v_period_to_dt,
                                               m2.emp_stop_to_dt);
            --dbms_output.put_line('Ded2 ' || v_ded_days);
          ELSIF (LTRIM(RTRIM(TO_CHAR(TO_DATE(v_period_to_dt, 'DD-MON-YY'),
                                     'MON-YY'))) =
                LTRIM(RTRIM(TO_CHAR(m2.emp_stop_from_dt, 'MON-YY')))) OR
                (LTRIM(RTRIM(TO_CHAR(TO_DATE(v_period_to_dt, 'DD-MON-YY'),
                                     'MON-YY'))) =
                LTRIM(RTRIM(TO_CHAR(m2.emp_stop_to_dt, 'MON-YY')))) THEN
            v_ded_days := v_ded_days +
                          pkg_payroll.get_days(m2.emp_stop_from_dt,
                                               m2.emp_stop_to_dt);
            --dbms_output.put_line('Ded3 ' || v_ded_days);
          END IF;
        END IF;
      ELSIF m2.emp_all_elements = '1' THEN
        --Ignore employee becuase of stop all elements and its dependant logic goes here
        IF m2.emp_stop_to_dt IS NULL and NVL(m2.resume_flag, '0') = '0' and
           m2.emp_stop_from_dt <= v_period_from_dt THEN
          v_ignore_emp := '1';
          --start
        ELSIF m2.emp_stop_to_dt IS NULL and NVL(m2.resume_flag, '0') = '0' and
              m2.emp_stop_from_dt > v_period_from_dt THEN
          ----dbms_output.put_line('One');
          v_ded_days   := v_ded_days +
                          pkg_payroll.get_days(v_period_from_dt,
                                               m2.emp_stop_from_dt);
          v_ignore_emp := '1'; --Change done here
        ELSE
          IF v_period_from_dt > m2.emp_stop_from_dt AND
             v_period_from_dt < m2.emp_stop_to_dt THEN
            ----dbms_output.put_line('v_period_from_dt '||to_char(v_period_from_dt)||' -emp_stop_From_dt '||to_char(m2.emp_stop_from_dt)||' - emp_stop_to_dt '||to_char(m2.emp_stop_to_dt));
            v_ded_days   := v_ded_days +
                            pkg_payroll.get_days(v_period_from_dt,
                                                 m2.emp_stop_to_dt);
            v_ignore_emp := '1';
            ----dbms_output.put_line('Ded1 '||v_ded_days);
          ELSIF v_period_to_dt > m2.emp_stop_from_dt AND
                v_period_to_dt < m2.emp_stop_to_dt THEN
            v_ded_days := v_ded_days +
                          pkg_payroll.get_days(v_period_to_dt,
                                               m2.emp_stop_to_dt);
            ----dbms_output.put_line('Ded2 '||v_ded_days);
            v_ignore_emp := '1';
          ELSIF (LTRIM(RTRIM(TO_CHAR(TO_DATE(v_period_to_dt, 'DD-MON-YY'),
                                     'MON-YY'))) =
                LTRIM(RTRIM(TO_CHAR(m2.emp_stop_from_dt, 'MON-YY')))) OR
                (LTRIM(RTRIM(TO_CHAR(TO_DATE(v_period_to_dt, 'DD-MON-YY'),
                                     'MON-YY'))) =
                LTRIM(RTRIM(TO_CHAR(m2.emp_stop_to_dt, 'MON-YY')))) THEN
            v_ded_days := v_ded_days +
                          pkg_payroll.get_days(m2.emp_stop_from_dt,
                                               m2.emp_stop_to_dt);
            ----dbms_output.put_line('Ded3 '||v_ded_days);
            v_ignore_emp := '1';
          END IF;
          --end
        END IF;
      END IF;
    END LOOP;
    IF v_skip_element = '0' THEN
      --dbms_output.put_line('Payroll days ' || v_payroll_days);
      --dbms_output.put_line('Deduction days ' || v_ded_days);
      --dbms_output.put_line('Payable days ' ||
      -- (v_payroll_days - v_ded_days));
      --dbms_output.put_line(' v_stop_n_resume ' || v_stop_n_resume);
      IF v_stop_n_resume = '1' THEN
        ----dbms_output.put_line('Payable days '||v_ded_days);
        p_payroll_days := v_ded_days;
      ELSE
        ----dbms_output.put_line('Payable days '||(v_payroll_days-v_ded_days));
        p_payroll_days := (v_payroll_days - v_ded_days);
      END IF;
    
    ELSIF v_skip_element = '1' THEN
      --dbms_output.put_line('Element skipped');
      p_payroll_days := 0;
    END IF;
    p_skip_element := v_skip_element;
    p_ignore_emp   := v_ignore_emp;
  END get_element_stop_dtl_ls;

  --end

  FUNCTION get_dept_query(p_dept_list VARCHAR2, p_group_id VARCHAR2)
    RETURN VARCHAR2 IS
  
    TYPE v_dept_tab IS TABLE OF VARCHAR2(50) INDEX BY BINARY_INTEGER;
  
    v_list v_dept_tab;
    v_idx  NUMBER := 1;
  
    v_dept_id VARCHAR2(4000) := p_dept_list;
    --v_dept_id VARCHAR2(4000):='DEPT_ID-0000000005,DEPT_ID-0000000006,DEPT_ID-0000000007,DEPT_ID-0000000008,DEPT_ID-0000000012';
    --v_dept2 VARCHAR2(4000):='DEPT_ID-0000000005','DEPT_ID-0000000006','DEPT_ID-0000000007','DEPT_ID-0000000008','DEPT_ID-0000000012';
    v_temp VARCHAR2(100);
    v_str  VARCHAR2(5000) := 'SELECT
  distinct eval.pay_emp_dept_id as pay_emp_dept_id
FROM pay_group_element_mapping grp1,
  pay_elements ele,
  pay_group_dtls grp,
  pay_emp_element_value eval
WHERE grp1.pay_element_id=ele.pay_element_id
AND grp.pay_group_id     =grp1.pay_group_id
AND eval.pay_element_id  =grp1.pay_element_id
AND eval.pay_group_id=grp.pay_group_id
AND grp1.pay_element_id=eval.pay_element_id
AND grp.PAY_GROUP_ID= ' || '''' ||
                             nvl(p_group_id, 'grp.PAY_GROUP_ID') || '''';
  
  BEGIN
    if p_group_id is null then
      v_str := 'SELECT
  distinct eval.pay_emp_dept_id as pay_emp_dept_id
FROM pay_group_element_mapping grp1,
  pay_elements ele,
  pay_group_dtls grp,
  pay_emp_element_value eval
WHERE grp1.pay_element_id=ele.pay_element_id
AND grp.pay_group_id     =grp1.pay_group_id
AND eval.pay_element_id  =grp1.pay_element_id
AND eval.pay_group_id=grp.pay_group_id
AND grp1.pay_element_id=eval.pay_element_id
AND grp.PAY_GROUP_ID= grp.PAY_GROUP_ID';
    else
    
      v_str := 'SELECT
  distinct eval.pay_emp_dept_id as pay_emp_dept_id
FROM pay_group_element_mapping grp1,
  pay_elements ele,
  pay_group_dtls grp,
  pay_emp_element_value eval
WHERE grp1.pay_element_id=ele.pay_element_id
AND grp.pay_group_id     =grp1.pay_group_id
AND eval.pay_element_id  =grp1.pay_element_id
AND eval.pay_group_id=grp.pay_group_id
AND grp1.pay_element_id=eval.pay_element_id
AND grp.PAY_GROUP_ID= ' || '''' || p_group_id || '''';
    
    end if;
  
    IF v_dept_id != 'ALL' THEN
      v_dept_id := ltrim(rtrim(v_dept_id));
      ----dbms_output.put_line('String value '||v_str);
      IF instr(v_dept_id, ',', 1, 1) > 0 THEN
        LOOP
          v_temp := substr(v_dept_id, 1, instr(v_dept_id, ',', 1, 1));
          v_temp := ltrim(rtrim(v_temp, ','));
          ----dbms_output.put_line('temp '||v_temp);
          v_list(v_idx) := v_temp;
          v_idx := v_idx + 1;
          v_dept_id := ltrim(ltrim(v_dept_id, v_temp), ',');
          ----dbms_output.put_line('Dept id '||v_dept_id);
          IF instr(v_dept_id, ',', 1) <= 0 THEN
            v_temp := v_dept_id;
            EXIT;
          END IF;
        END LOOP;
        v_list(v_idx) := v_temp;
        ----dbms_output.put_line('temp1 '||v_temp);
        ----dbms_output.put_line('No of Values '||v_list.count);
        v_str := v_str || ' AND eval.pay_emp_dept_id IN ( ';
        FOR i IN 1 .. v_list.count LOOP
          ----dbms_output.put_line('Department '||v_list(i));
          v_str := v_str || '''' || v_list(i) || '''' || ',';
        END LOOP;
        v_str := rtrim(v_str, ',') || ' )';
        ----dbms_output.put_line('Dept string '||v_str);
      ELSE
        -- v_str:=v_str||'  AND eval.pay_emp_dept_id='||''''||v_dept_id||'''';
        dbms_output.put_line('Else v_str ' || v_str);
      END IF;
    ELSE
      dbms_output.put_line('Else v_str ' || v_str);
    END IF;
    RETURN v_str;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN ' ';
  END get_dept_query;

  FUNCTION get_csv_to_list(p_dept_list VARCHAR2) RETURN VARCHAR2 IS
    TYPE v_dept_tab IS TABLE OF VARCHAR2(50) INDEX BY BINARY_INTEGER;
  
    v_list v_dept_tab;
    v_idx  NUMBER := 1;
  
    v_dept_id VARCHAR2(4000) := p_dept_list;
    --v_dept_id VARCHAR2(4000):='DEPT_ID-0000000005,DEPT_ID-0000000006,DEPT_ID-0000000007,DEPT_ID-0000000008,DEPT_ID-0000000012';
    --v_dept2 VARCHAR2(4000):='DEPT_ID-0000000005','DEPT_ID-0000000006','DEPT_ID-0000000007','DEPT_ID-0000000008','DEPT_ID-0000000012';
    v_temp VARCHAR2(100);
    v_str  VARCHAR2(5000);
  BEGIN
    v_dept_id := ltrim(rtrim(v_dept_id));
    --dbms_output.put_line('String value ' || v_str);
    IF instr(v_dept_id, ',', 1, 1) > 0 THEN
      LOOP
        v_temp := substr(v_dept_id, 1, instr(v_dept_id, ',', 1, 1));
        v_temp := ltrim(rtrim(v_temp, ','));
        ----dbms_output.put_line('temp '||v_temp);
        v_list(v_idx) := v_temp;
        v_idx := v_idx + 1;
        v_dept_id := ltrim(ltrim(v_dept_id, v_temp), ',');
        ----dbms_output.put_line('Dept id '||v_dept_id);
        IF instr(v_dept_id, ',', 1) <= 0 THEN
          v_temp := v_dept_id;
          EXIT;
        END IF;
      END LOOP;
      v_list(v_idx) := v_temp;
      ----dbms_output.put_line('temp1 '||v_temp);
      ----dbms_output.put_line('No of Values '||v_list.count);
      -- v_str:=v_str||' AND eval.pay_emp_dept_id IN ( ';
      FOR i IN 1 .. v_list.count LOOP
        ----dbms_output.put_line('Department '||v_list(i));
        v_str := v_str || '''' || v_list(i) || '''' || ',';
      END LOOP;
      v_str := rtrim(v_str, ',');
      ----dbms_output.put_line('Dept string '||v_str);
    ELSE
      v_str := '''' || v_dept_id || '''';
      --dbms_output.put_line('Else v_str ' || v_str);
    END IF;
    /*ELSE
     --dbms_output.put_line('Else v_str '||v_str);
    END IF;*/
    RETURN v_str;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN ' ';
  END get_csv_to_list;

  FUNCTION get_emp_leave_ldr_exists(p_fiscal_year IN VARCHAR2,
                                    p_emp_id      IN VARCHAR2,
                                    p_org_id      IN VARCHAR2,
                                    p_leave_id    IN VARCHAR2,
                                    p_dept_id     IN VARCHAR2) RETURN NUMBER IS
    v_exists NUMBER := 0;
  BEGIN
    SELECT COUNT(*)
      INTO v_exists
      FROM HR_LEAVE_LEDGER
     WHERE LDR_EMP_ID = p_emp_id
       AND LDR_LEAVE_ID = p_leave_id
       AND LDR_YEAR = p_fiscal_year
       AND LDR_ORG_ID = p_org_id
       AND LDR_DEPT_ID = p_dept_id;
  
    RETURN v_exists;
  END get_emp_leave_ldr_exists;

  FUNCTION get_emp_curr_leave_bal(p_fiscal_year IN VARCHAR2,
                                  p_emp_id      IN VARCHAR2,
                                  p_org_id      IN VARCHAR2,
                                  p_leave_id    IN VARCHAR2,
                                  p_dept_id     IN VARCHAR2) RETURN NUMBER IS
  
    v_leave_bal NUMBER := 0;
  BEGIN
    BEGIN
      SELECT ldr.LDR_LEAVE_BAL
        INTO v_leave_bal
        FROM HR_LEAVE_LEDGER ldr
       WHERE ldr.LDR_YEAR_CLOSED = '0'
         AND ldr.LDR_ID =
             (SELECT MAX(ld.LDR_ID)
                FROM HR_LEAVE_LEDGER ld
               WHERE ld.LDR_EMP_ID = p_emp_id
                 AND ld.LDR_ORG_ID = p_org_id
                 AND ld.LDR_DEPT_ID = p_dept_id
                 AND ld.LDR_LEAVE_ID = p_leave_id
                 AND ld.LDR_FISCAL_YEAR = p_fiscal_year);
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        v_leave_bal := 0;
      WHEN OTHERS THEN
        v_leave_bal := 0;
    END;
    RETURN v_leave_bal;
  END get_emp_curr_leave_bal;

  PROCEDURE emp_leave_txn_ledger(p_tran_type   IN VARCHAR2,
                                 p_tran_id     IN VARCHAR2,
                                 p_txn_type    IN VARCHAR2,
                                 p_trn_date    DATE,
                                 p_trn_month   IN NUMBER,
                                 p_fiscal_year IN VARCHAR2,
                                 p_emp_id      IN VARCHAR2,
                                 p_org_id      IN VARCHAR2,
                                 p_leave_id    IN VARCHAR2,
                                 p_dept_id     IN VARCHAR2,
                                 p_days        IN NUMBER) IS
    v_tran_type      VARCHAR2(50) := p_tran_type;
    v_leave_bal      NUMBER := 0;
    v_leave_open_bal NUMBER := 0;
  BEGIN
    /*
    debug_proc('Start of Leave Ledger');
    debug_proc('p_tran_type '||p_tran_type);
    debug_proc('p_tran_id '||p_tran_id);
    debug_proc('p_txn_type '||p_txn_type);
    debug_proc('p_trn_date '||p_trn_date);
    debug_proc('p_trn_month '||p_trn_month);
    debug_proc('p_fiscal_year '||p_fiscal_year);
    debug_proc('p_emp_id '||p_emp_id);
    debug_proc('p_org_id '||p_org_id);
    debug_proc('p_leave_id '||p_leave_id);
    debug_proc('p_dept_id '||p_dept_id);
    debug_proc('p_days '||p_days);
    debug_proc('1');*/
    v_leave_open_bal := get_emp_curr_leave_bal(p_fiscal_year,
                                               p_emp_id,
                                               p_org_id,
                                               p_leave_id,
                                               p_dept_id);
  
    ----dbms_output.put_line(' v_leave_open_bal '||v_leave_open_bal);
  
    IF p_tran_type = 'OPENBAL' THEN
      --debug_proc('3');
      v_leave_open_bal := 0;
      v_leave_bal      := p_days + v_leave_open_bal;
    
    ELSIF p_tran_type = 'LTAKEN' THEN
      --debug_proc('4');
      v_leave_bal := v_leave_open_bal - p_days;
    
    ELSIF p_tran_type = 'LENCASH' THEN
      --debug_proc('4');
      v_leave_bal := v_leave_open_bal - p_days;
    
    ELSIF p_tran_type = 'LCANCEL' THEN
      --debug_proc('5');
      v_leave_bal := v_leave_open_bal + p_days;
    
    ELSIF p_tran_type = 'LADJ' THEN
      --debug_proc('5');
      v_leave_bal := v_leave_open_bal + p_days;
    
    END IF;
  
    BEGIN
      --debug_proc('6');
      --debug_proc('6A' ||v_leave_open_bal);
      --debug_proc('6B'||v_leave_bal);
    
      INSERT INTO HR_LEAVE_LEDGER
        (PK_ID,
         CHILD_ID,
         LDR_ID,
         LDR_EMP_ID,
         LDR_LEAVE_ID,
         LDR_OPEN_BALANCE,
         LDR_LEAVE_BAL,
         LDR_LEAVE_AVAIL,
         LDR_ORG_ID,
         LDR_DEPT_ID,
         LDR_TYPE,
         LDR_TYPE_ID,
         LDR_TXN_TYPE,
         LDR_DATE,
         LDR_FISCAL_YEAR,
         LDR_MONTH,
         LDR_ENCASHED,
         LDR_EMP_SEPERATED,
         LDR_REMARKS,
         LDR_YEAR_CLOSED,
         ENABLED_FLAG,
         WORKFLOW_COMPLETION_STATUS,
         CREATED_BY,
         CREATED_DATE)
      VALUES
        (1,
         1,
         ssm.get_next_sequence('LDR', 'EN'),
         p_emp_id,
         p_leave_id,
         v_leave_open_bal,
         v_leave_bal,
         p_days,
         p_org_id,
         p_dept_id,
         p_tran_type,
         p_tran_id,
         p_txn_type,
         p_trn_date,
         p_fiscal_year,
         p_trn_month,
         '0',
         '0',
         'Leave Ledger for ' || p_tran_type,
         '0',
         '1',
         '1',
         'Admin',
         sysdate);
      --debug_proc('7');
      commit;
    
    EXCEPTION
      WHEN OTHERS THEN
        --  debug_proc('8' ||substr(sqlerrm,1,500));
        DBMS_OUTPUT.PUT_LINE('Ex1 ' || sqlerrm);
    END;
    --debug_proc('End of Leave Ledger');
  END emp_leave_txn_ledger;

  PROCEDURE Leave_Definition_Year_End(p_org_id        IN VARCHAR2,
                                      p_curr_fin_year VARCHAR2,
                                      p_prev_fin_year VARCHAR2) IS
  
    CURSOR LEAVE_DEFENITION_CUR IS
      SELECT *
        FROM HR_LEAVE_DEFINITIONS HLD
       WHERE HLD.Enabled_Flag = '1'
         AND HLD.workflow_completion_status = '1'
         AND HLD.Fiscal_Year = ltrim(rtrim(P_PREV_FIN_YEAR))
         AND HLD.Effective_To_Dt IS NULL;
  
  BEGIN
  
    FOR S_LEAVE_CUR IN LEAVE_DEFENITION_CUR LOOP
      ----dbms_output.put_line(S_LEAVE_CUR.LEAVE_ID ||' - '||S_LEAVE_CUR.LEAVE_DESC||' - '||S_LEAVE_CUR.ORG_ID);
      BEGIN
        INSERT INTO HR_LEAVE_DEFINITIONS
          (PK_ID,
           LEAVE_ID,
           LEAVE_DESC,
           ORG_ID,
           FISCAL_YEAR,
           LEAVE_TYPE,
           WITH_PAY_YN,
           CARRY_OVER_YN,
           NO_OF_DAYS,
           EFFECTIVE_FROM_DT,
           EFFECTIVE_TO_DT,
           ENABLED_FLAG,
           WORKFLOW_COMPLETION_STATUS,
           CREATED_BY,
           CREATED_DATE,
           CARRY_OVER_TYPE)
        VALUES
          (HR_LEAVE_DEFINITIONS_SEQ.NEXTVAL,
           S_LEAVE_CUR.LEAVE_ID,
           S_LEAVE_CUR.LEAVE_DESC,
           S_LEAVE_CUR.ORG_ID,
           P_CURR_FIN_YEAR,
           S_LEAVE_CUR.LEAVE_TYPE,
           S_LEAVE_CUR.WITH_PAY_YN,
           S_LEAVE_CUR.CARRY_OVER_YN,
           S_LEAVE_CUR.NO_OF_DAYS,
           S_LEAVE_CUR.EFFECTIVE_FROM_DT,
           S_LEAVE_CUR.EFFECTIVE_TO_DT,
           S_LEAVE_CUR.ENABLED_FLAG,
           S_LEAVE_CUR.WORKFLOW_COMPLETION_STATUS,
           S_LEAVE_CUR.CREATED_BY,
           sysdate,
           S_LEAVE_CUR.CARRY_OVER_TYPE);
      EXCEPTION
        WHEN OTHERS THEN
          DBMS_OUTPUT.PUT_LINE('ERR1 ' || SQLERRM);
      END;
    END LOOP;
    COMMIT;
  EXCEPTION
    WHEN OTHERS THEN
      DBMS_OUTPUT.PUT_LINE('Encounted an exception -' || sqlerrm);
  END Leave_Definition_Year_End;

  PROCEDURE Leave_Dept_Year_End(p_org_id        IN VARCHAR2,
                                p_curr_fin_year VARCHAR2,
                                p_prev_fin_year VARCHAR2) IS
  
    CURSOR LEAVE_DEPT_CUR IS
      SELECT DISTINCT HD.DEPT_ID AS DEPT_ID
        FROM HR_LEAVE_DEPT HD
       WHERE HD.Enabled_Flag = '1'
         AND HD.Fiscal_Year = ltrim(rtrim(P_PREV_FIN_YEAR))
         AND HD.Effective_To_Dt IS NULL;
  
    CURSOR LEAVE_DEF_CUR IS
      SELECT *
        FROM HR_LEAVE_DEFINITIONS
       WHERE ORG_ID = p_org_id
         AND FISCAL_YEAR = ltrim(rtrim(p_curr_fin_year))
         AND ENABLED_FLAG = '1'
         AND WORKFLOW_COMPLETION_STATUS = '1'
         AND EFFECTIVE_TO_DT IS NULL;
  
    v_from_dt DATE;
    v_to_dt   DATE;
  BEGIN
  
    FOR S_DEPT_CUR IN LEAVE_DEPT_CUR LOOP
    
      FOR S_DEF_CUR IN LEAVE_DEF_CUR LOOP
        BEGIN
          /*
           SELECT CAL_EFF_START_DT,
                CAL_EFF_END_DT
            INTO v_from_dt,
               v_to_dt
            FROM GL_ACCT_CALENDAR_HDR
          WHERE CAL_ID=p_curr_fin_year
          AND ENABLED_FLAG='1'
          AND WORKFLOW_COMPLETION_STATUS='1';
          */
          SELECT MIN(PERIOD_FROM_DT), MAX(PERIOD_TO_DT)
            INTO v_from_dt, v_to_dt
            FROM GL_ACCT_PERIOD_DTL
           WHERE CAL_DTL_ID = p_curr_fin_year
             AND PERIOD_TYPE = 'ACTPERIOD';
        EXCEPTION
          WHEN OTHERS THEN
            v_from_dt := TRUNC(SYSDATE);
            v_to_dt   := NULL;
        END;
        BEGIN
          INSERT INTO HR_LEAVE_DEPT
            (DEPT_LEAVE_ID,
             LEAVE_ID,
             DEPT_ID,
             FISCAL_YEAR,
             EFFECTIVE_FROM_DT,
             EFFECTIVE_TO_DT,
             ENABLED_FLAG,
             WORKFLOW_COMPLETION_STATUS,
             CREATED_BY,
             CREATED_DATE)
          VALUES
            (SSM.get_next_sequence('HR_020', 'EN'),
             S_DEF_CUR.LEAVE_ID, --PK_ID,
             S_DEPT_CUR.DEPT_ID,
             p_curr_fin_year,
             v_from_dt,
             NULL,
             S_DEF_CUR.ENABLED_FLAG,
             S_DEF_CUR.WORKFLOW_COMPLETION_STATUS,
             S_DEF_CUR.CREATED_BY,
             sysdate);
        EXCEPTION
          WHEN OTHERS THEN
            DBMS_OUTPUT.PUT_LINE('ERR ' || SQLERRM);
        END;
      END LOOP;
    END LOOP;
    COMMIT;
  EXCEPTION
    WHEN OTHERS THEN
      NULL;
  END LEAVE_DEPT_YEAR_END;

  PROCEDURE Leave_Staff_YEAR_END(p_org_id    IN VARCHAR2,
                                 p_curr_year VARCHAR2,
                                 p_prev_year VARCHAR2) IS
  
    Cursor lsd_cur(p_fiscal_year VARCHAR2) is
      select ld.carry_over_yn, lsd.*
        from hr_leave_staff_defintions lsd, hr_leave_definitions ld
       where ld.leave_id = lsd.leave_id
            --and ld.fiscal_year=lsd.fiscal_year  --this condition to be added when there is fiscal year based leave definitions. Currently we dont
         and ld.org_id = lsd.org_id
         and ld.enabled_flag = '1'
         and ld.workflow_completion_status = '1'
            --and ld.carry_over_yn='1'
         and lsd.fiscal_year = p_fiscal_year;
  
    v_leave_bal NUMBER := 0;
    v_left_flag NUMBER(1) := 0;
    v_no_days   NUMBER := 0;
    v_seq       VARCHAR2(50);
    v_skip      CHAR(1) := '1';
    /*
    p_prev_year VARCHAR2(50):='CAL-0000000175';
     p_curr_year VARCHAR2(50):='CAL-0000000176';
     p_org_id VARCHAR2(50):='ORG-0000000066';*/
  
  BEGIN
    FOR m in lsd_cur(p_prev_year) LOOP
      ----dbms_output.put_line('Employee Leave dtls '||m.lsd_id||' - '||m.staff_id ||' - '||m.leave_id||' - '||m.dept_id ||' -Bal leave '||v_leave_bal);
      --Balance leave for the employee to be calculated
      v_leave_bal := 0;
      v_no_days   := 0;
      v_skip      := '1';
      IF m.carry_over_yn = '1' THEN
        v_leave_bal := pkg_payroll.get_emp_leave_bal(p_fiscal_year => m.fiscal_year,
                                                     p_org_id      => m.org_id,
                                                     p_dept_id     => m.dept_id,
                                                     p_emp_id      => m.staff_id,
                                                     p_leave_id    => m.leave_id);
      END IF;
      --dbms_output.put_line('Employee Leave dtls ' || m.lsd_id || ' - ' ||
      -- m.staff_id || ' - ' || ' - ' || m.leave_id ||
      -- ' - ' || m.dept_id || ' -Bal leave ' ||
      -- v_leave_bal);
      --Check employee left the company
      select count('1')
        into v_left_flag
        FROM HR_LEAVE_LEDGER XP
       WHERE XP.LDR_EMP_ID = m.staff_id --'EMP_ID-0000000034'
         AND XP.LDR_FISCAL_YEAR = m.fiscal_year
         AND XP.LDR_ORG_ID = m.org_id
         AND XP.LDR_DEPT_ID = m.dept_id
            --AND XP.LDR_LEAVE_ID=m.leave_id
         AND XP.ldr_emp_seperated = '1';
    
      --Check for number of days encashed
      --no of days fo the new financial year for the department for the org
      /*      Begin
                SELECT   def.no_of_days
                INTO v_no_days
                FROM hr_leave_definitions def,
                hr_leave_dept dept
                WHERE def.enabled_flag            ='1'
                AND def.workflow_completion_status='1'
                AND dept.enabled_flag            ='1'
                AND dept.workflow_completion_status='1'
                AND dept.effective_to_dt          IS NULL
                AND def.effective_to_dt          IS NULL
                AND dept.leave_id=def.leave_id
                --AND dept.fiscal_year=def.fiscal_year --this condition to be added when there is fiscal year based leave definitions. Currently we dont
                AND dept.fiscal_year=p_curr_year--m.fiscal_year
                AND def.org_id=m.org_id
                AND dept.leave_id=m.leave_id
                AND dept.dept_id=m.dept_id;
                --dbms_output.put_line('Curr year '||p_curr_year ||' - '||m.leave_id||' -Days '||v_no_days);
           EXCEPTION
              WHEN OTHERS THEN
                --dbms_output.put_line('Curr year '||p_curr_year ||' - '||m.leave_id||' -not Defined '||sqlerrm);
                v_no_days:=0;
            v_skip:='0';
           END;
      */
    
      IF v_left_flag > 0 THEN
        --dbms_output.put_line('Employee Left the company');
        v_left_flag := 0;
      ELSE
        IF v_skip = '1' THEN
          BEGIN
            v_no_days := M.NO_OF_DAYS + v_leave_bal;
            v_seq     := ssm.get_next_sequence('HR_021', 'EN');
            INSERT INTO HR_LEAVE_STAFF_DEFINTIONS
              (PK_ID,
               CHILD_ID,
               LSD_ID,
               ORG_ID,
               FISCAL_YEAR,
               STAFF_ID,
               LEAVE_ID,
               DEPT_ID,
               LEAVE_AVAILED,
               LEAVE_BALANCE,
               ENABLED_FLAG,
               WORKFLOW_COMPLETION_STATUS,
               CREATED_BY,
               CREATED_DATE,
               NO_OF_DAYS)
            VALUES
              (1,
               1,
               v_seq,
               m.org_id,
               p_curr_year,
               m.staff_id,
               m.leave_id,
               m.dept_id,
               0,
               v_no_days,
               '1',
               '1',
               'ADMIN1',
               SYSDATE,
               M.NO_OF_DAYS);
          
            --Leave ledger program call start
            pkg_payroll.emp_leave_txn_ledger(p_tran_type   => 'OPENBAL',
                                             p_tran_id     => v_seq,
                                             p_txn_type    => 'LSD',
                                             p_trn_date    => SYSDATE,
                                             p_trn_month   => TO_NUMBER(TO_CHAR(SYSDATE,
                                                                                'MM')),
                                             p_fiscal_year => p_curr_year,
                                             p_emp_id      => m.STAFF_ID,
                                             p_org_id      => m.ORG_ID,
                                             p_leave_id    => m.LEAVE_ID,
                                             p_dept_id     => m.DEPT_ID,
                                             p_days        => v_no_days);
            --Leave ledger program call end
            --Leave ledger program to close previous year transactions start
            UPDATE HR_LEAVE_LEDGER
               SET LDR_YEAR_CLOSED = '1'
             WHERE LDR_EMP_ID = m.STAFF_ID
               AND LDR_FISCAL_YEAR = p_prev_year;
            --Leave ledger program to close previous year transactions end
          
          EXCEPTION
            WHEN OTHERS THEN
              DBMS_OUTPUT.PUT_LINE('ERR2 ' || SQLERRM);
          END;
        END IF;
      END IF;
    END LOOP;
    COMMIT;
  END Leave_Staff_YEAR_END;

  PROCEDURE loan_deduction_update_proc(p_org_id IN VARCHAR2,
                                       p_emp_id IN VARCHAR2,
                                       p_date   IN DATE,
                                       p_amt    IN NUMBER) IS
  
    v_loan_amt NUMBER := p_amt;
    v_repay_dt DATE := p_date;
  
    CURSOR loan_cur(c_emp_id VARCHAR2) is
      select x.req_id,
             x.req_dept_id,
             x.req_desig_id,
             x.req_emp_id,
             x.req_amount,
             x.req_advance_type,
             y.repay_id,
             y.res_id,
             y.repay_amt,
             y.installment_no,
             y.repay_dt,
             y.repay_actual_dt,
             y.repay_paidyn
        from hr_advance_req x, hr_advance_repay_plan y
       where y.res_id = x.req_id
         and y.repay_paidyn = '0'
         and x.req_emp_id = c_emp_id
       order by y.installment_no;
  
  BEGIN
  
    FOR m in loan_cur(p_emp_id) LOOP
      IF v_loan_amt > 0 THEN
        ----dbms_output.put_line('Records '||m.req_id||' - '||m.res_id||' - '||m.repay_id||' - '||m.installment_no);
        IF (m.repay_amt - v_loan_amt) < 0 THEN
        
          UPDATE HR_ADVANCE_REPAY_PLAN
             SET repay_amt       = 0,
                 REPAY_PAIDYN    = '1',
                 repay_actual_dt = v_repay_dt
           where repay_id = m.repay_id;
          v_loan_amt := v_loan_amt - m.repay_amt;
          --dbms_output.put_line('1 ' || v_loan_amt);
        ELSIF (m.repay_amt - v_loan_amt) > 0 THEN
        
          UPDATE HR_ADVANCE_REPAY_PLAN
             SET repay_amt      =
                 (m.repay_amt - v_loan_amt),
                 repay_actual_dt = v_repay_dt
           where repay_id = m.repay_id;
        
          v_loan_amt := v_loan_amt - m.repay_amt;
          --dbms_output.put_line('2 ' || v_loan_amt);
        ELSIF (m.repay_amt - v_loan_amt) = 0 THEN
        
          UPDATE HR_ADVANCE_REPAY_PLAN
             SET repay_amt       = 0,
                 REPAY_PAIDYN    = '1',
                 repay_actual_dt = v_repay_dt
           where repay_id = m.repay_id;
        
          v_loan_amt := v_loan_amt - m.repay_amt;
          --dbms_output.put_line('3 ' || v_loan_amt);
        END IF;
      END IF;
    
      IF v_loan_amt <= 0 THEN
        UPDATE HR_ADVANCE_REPAY_PLAN
           SET repay_actual_dt = v_repay_dt, repay_dt = v_repay_dt
         where repay_id = m.repay_id;
        v_repay_dt := ADD_MONTHS(v_repay_dt, 1);
      END IF;
    END LOOP;
  
  END loan_deduction_update_proc;

  PROCEDURE payroll_process(p_pay_group  IN VARCHAR2,
                            p_pay_period IN VARCHAR2,
                            p_dept_list  IN VARCHAR2,
                            p_emp_id     IN VARCHAR2 DEFAULT NULL,
                            p_org_id     IN VARCHAR2) IS
    v_fixed_days          number;
    v_working_days        number;
    v_AL_BMONTH           char(1) := '0';
    v_AL_TOT_Days         number := 0;
    v_previous_month_days number := 0;
    v_dep_num             number;
    CURSOR cur_pay_period(c_pay_period IN VARCHAR2) IS
      SELECT *
        FROM PAY_PERIODS
       WHERE ENABLED_FLAG = '1'
         AND WORKFLOW_COMPLETION_STATUS = '1'
         AND PAY_PERIOD_ID = c_pay_period;
  
    CURSOR group_cur IS
      SELECT *
        FROM PAY_GROUP_DTLS
       WHERE ENABLED_FLAG = '1'
         AND EFFECTIVE_TO_DT IS NULL
         AND WORKFLOW_COMPLETION_STATUS = '1'
         AND PAY_GROUP_ID = NVL(p_pay_group, PAY_GROUP_ID);
  
    CURSOR dept_cur IS
      SELECT * FROM HR_DEPARTMENTS WHERE ENABLED_FLAG = '1';
  
    CURSOR pay_cur_dept(c_grp_id IN VARCHAR2) IS
      SELECT distinct eval.pay_emp_dept_id as pay_emp_dept_id
        FROM pay_group_element_mapping grp1,
             pay_elements              ele,
             pay_group_dtls            grp,
             pay_emp_element_value     eval
       WHERE grp1.pay_element_id = ele.pay_element_id
         AND grp.pay_group_id = grp1.pay_group_id
         AND eval.pay_element_id = grp1.pay_element_id
         AND eval.pay_group_id = grp.pay_group_id
         AND grp1.pay_element_id = eval.pay_element_id
         AND grp.PAY_GROUP_ID = c_grp_id
         AND ele.ENABLED_FLAG = '1';
    --AND grp.pay_group_code='EMPPAYROLL';
  
    CURSOR pay_cur(c_grp_id        IN VARCHAR2,
                   c_dept_id       IN VARCHAR2,
                   c_emp_id        IN VARCHAR2,
                   c_pay_to_date   IN DATE,
                   c_pay_from_date IN DATE) IS
      SELECT
      --grp1.pg_ele_id,
      --grp1.pay_group_id,
      --grp1.pay_element_id,
       eval.effective_from_dt,
       eval.effective_to_dt,
       emp.termination_date,
       emp.emp_doj,
       emp.nationality,
       emp.ssd_flag,
       emp.stop_paytoll,
       ele.pay_element_id,
       ele.pay_element_code,
       ele.pay_element_desc,
       ele.pay_ele_class,
       NVL(ele.pay_ele_recurring, '1') pay_ele_recurring,
       NVL(ele.pay_element_prorate, '1') pay_element_prorate,
       NVL(eval.pay_element_prorate, '1') emp_prorate,
       grp.pay_group_id,
       grp.pay_payroll_type,
       grp.pay_group_code,
       grp.pay_group_desc,
       eval.pay_emp_element_id,
       eval.pay_emp_id,
       NVL(ele.unpaid_leave, '1') ele_unpaid_leave,
       NVL(eval.unpaid_leave, '1') eval_unpaid_leave,
       --eval.pay_element_id,
       eval.pay_amount,
       eval.pay_emp_dept_id,
       eval.pay_period_id,
       nvl(eval.paid_for_annual_leave, '1') ele_annual_leave,
       to_number(emp.emp_no),
       hd.dept_id,
       emp.emp_no emp_number
      
        FROM --pay_group_element_mapping grp1,
             pay_elements          ele,
             pay_group_dtls        grp,
             pay_emp_element_value eval,
             hr_employees          emp,
             hr_departments        hd,
             hr_emp_work_dtls      hewd
       WHERE eval.pay_group_id = grp.pay_group_id
         AND eval.pay_element_id = ele.pay_element_id
         AND emp.emp_id = eval.pay_emp_id
         AND ele.enabled_flag = '1'
            -- AND emp.EMP_INTERNAL_EXTERNAL = 'I'
         AND grp.PAY_GROUP_ID = c_grp_id
            --AND eval.pay_emp_dept_id = c_dept_id
         AND (nvl(eval.effective_to_dt, c_pay_to_date) >= c_pay_to_date OR
             nvl(eval.effective_to_dt, c_pay_to_date) >= c_pay_from_date)
         AND eval.pay_emp_id = NVL(c_emp_id, eval.pay_emp_id)
         and emp.emp_id = hewd.emp_id
         and hewd.emp_dept_id = hd.dept_id
         and hewd.effective_from_dt =
             (select max(hewdd.effective_from_dt)
                from hr_emp_work_dtls hewdd
               where hewd.emp_id = hewdd.emp_id)
       ORDER BY to_number(emp.emp_no), eval.pay_amount;
  
    CURSOR pay_cur1 IS
      SELECT PAY_PERIOD_ID,
             PAY_STATUS,
             PAY_GROUP_ID,
             sum(tp.pay_amount) PAY_TOTAL_PAY_AMOUNT,
             sum(0) PAY_TOTAL_DED_AMOUNT,
             IS_LAST_REC,
             PROCESSED_FLAG
        FROM TMP_PAYROLL tp
       group by PAY_PERIOD_ID,
                PAY_STATUS,
                PAY_GROUP_ID,
                IS_LAST_REC,
                PROCESSED_FLAG;
  
    CURSOR pay_cur2(c_group_id VARCHAR2) IS
      SELECT sum(tp.pay_amount) PAY_DEPT_PAY_AMT,
             sum(0) PAY_DEPT_DED_AMT,
             tp.pay_dept_id
        FROM TMP_PAYROLL tp
       where PAY_GROUP_ID = c_group_id
       group by tp.pay_dept_id;
  
    CURSOR pay_cur3(c_dept_id VARCHAR2) IS
      SELECT *
        FROM TMP_PAYROLL
       where upper(pay_remarks) = 'EMPLOYEE DETAILS'
         and PAY_DEPT_ID = c_dept_id;
  
    CURSOR leave_cur_non_AL(c_fiscal_year VARCHAR2,
                            c_org_id      VARCHAR2,
                            c_staff_id    VARCHAR2,
                            c_date        DATE) IS
      SELECT distinct c.leave_id leave_id, B.ATTRIBUTE1
        FROM HR_STAFF_ATTENDANCE   a,
             HR_LEAVE_APPLICATIONS b,
             HR_LEAVE_DEFINITIONS  c,
             ssm_system_options    sso
       WHERE a.LEAVE_REQ_ID = B.LEAVE_REQ_ID
         AND B.LEAVE_ID = C.LEAVE_ID
         AND C.WITH_PAY_YN = '1'
            --AND UPPER(LTRIM(RTRIM(a.ATTENDANCE_TYPE))) IN ('ABSENT', 'LEAVE')
         AND a.ENABLED_FLAG = '1'
         AND a.ORG_ID = c_org_id
         AND a.STAFF_ID = c_staff_id
         AND a.ATTENDANCE_DATE <= c_date
         AND c.leave_id != sso.hr_earn_leave; --'31-DEC-14';
  
    CURSOR leave_cur_AL(c_fiscal_year VARCHAR2,
                        c_org_id      VARCHAR2,
                        c_staff_id    VARCHAR2,
                        c_date        DATE) IS
      SELECT distinct c.leave_id leave_id,
                      B.ATTRIBUTE1,
                      b.leave_req_id,
                      to_char(b.leave_date_from, 'mm') leave_from,
                      to_char(b.leave_date_to, 'mm') leave_to,
                      b.no_of_days
        FROM HR_STAFF_ATTENDANCE   a,
             HR_LEAVE_APPLICATIONS b,
             HR_LEAVE_DEFINITIONS  c,
             ssm_system_options    sso
       WHERE a.LEAVE_REQ_ID = B.LEAVE_REQ_ID
         AND B.LEAVE_ID = C.LEAVE_ID
         AND C.WITH_PAY_YN = '1'
         AND a.ENABLED_FLAG = '1'
         AND b.enabled_flag = '1'
         and b.workflow_completion_status = '1'
         AND a.ORG_ID = c_org_id
         AND a.STAFF_ID = c_staff_id
         AND a.ATTENDANCE_DATE <= c_date
         AND c.leave_id = sso.hr_earn_leave
         AND exists (select *
                from hr_leave_encashment hle
               where b.leave_req_id = hle.trans_id)
            --and a.staff_id not in ('EMP_ID-0000003507','EMP_ID-0000003354','EMP_ID-0000003149','EMP_ID-0000003393')
         and not exists
       (select *
                from hr_holidays_master hhm
               where hhm.holiday_date = a.attendance_date); --'31-DEC-14';
  
    Cursor leave_adj(c_payroll_period VARCHAR2, c_emp_id VARCHAR2) IS --earnings
      SELECT C.PAY_EMP_ID, C.PAY_AMOUNT, D.PAY_ELEMENT_ID
        FROM PAY_FINAL_RUN_HDR     A,
             PAY_FINAL_RUN_DTL     B,
             PAY_FINAL_RUN_DTL_DTL C,
             PAY_ELEMENTS          D
       WHERE B.PAYROLL_ID = A.PAYROLL_ID
         AND B.PAYROLL_DTL_ID = C.PAYROLL_DTL_ID
         AND C.PAY_EMP_ELEMENT_ID = D.PAY_ELEMENT_ID
         AND A.ENABLED_FLAG = '1'
         AND A.WORKFLOW_COMPLETION_STATUS = '1'
         AND B.ENABLED_FLAG = '1'
         AND B.WORKFLOW_COMPLETION_STATUS = '1'
         AND C.ENABLED_FLAG = '1'
         AND C.WORKFLOW_COMPLETION_STATUS = '1'
         AND D.ENABLED_FLAG = '1'
         AND D.WORKFLOW_COMPLETION_STATUS = '1'
         AND D.PAID_FOR_ANNUAL_LEAVE = '1'
         AND A.PAYROLL_PERIOD = c_payroll_period --'PP_ID-0000000015'
         AND C.PAY_EMP_ID = c_emp_id;
  
    Cursor leave_adj_ded(c_payroll_period VARCHAR2, c_emp_id VARCHAR2) IS --deductions
      SELECT C.PAY_EMP_ID, C.PAY_AMOUNT, D.PAY_ELEMENT_ID
        FROM PAY_FINAL_RUN_HDR     A,
             PAY_FINAL_RUN_DTL     B,
             PAY_FINAL_RUN_DTL_DTL C,
             PAY_ELEMENTS          D
       WHERE B.PAYROLL_ID = A.PAYROLL_ID
         AND B.PAYROLL_DTL_ID = C.PAYROLL_DTL_ID
         AND C.PAY_EMP_ELEMENT_ID = D.PAY_ELEMENT_ID
         AND A.ENABLED_FLAG = '1'
         AND A.WORKFLOW_COMPLETION_STATUS = '1'
         AND B.ENABLED_FLAG = '1'
         AND B.WORKFLOW_COMPLETION_STATUS = '1'
         AND C.ENABLED_FLAG = '1'
         AND C.WORKFLOW_COMPLETION_STATUS = '1'
         AND D.ENABLED_FLAG = '1'
         AND D.WORKFLOW_COMPLETION_STATUS = '1'
            --AND D.PAID_FOR_ANNUAL_LEAVE='1'
         AND A.PAYROLL_PERIOD = c_payroll_period --'PP_ID-0000000015'
         AND C.PAY_EMP_ID = c_emp_id;
  
    TYPE DEPT_REF_CUR IS REF CURSOR;
    v_dept_cur DEPT_REF_CUR;
  
    TYPE tab_rec IS RECORD(
      tab_v_payroll_dtl_id PAY_TRIAL_RUN_DTL.PAYROLL_DTL_ID%TYPE,
      tab_v_dept_id        PAY_TRIAL_RUN_DTL.PAY_DEPT_ID%TYPE);
  
    TYPE tab_payroll_dtl_id IS TABLE OF tab_rec INDEX BY BINARY_INTEGER;
  
    v_tab_payroll_dtl_id tab_payroll_dtl_id;
  
    v_tot_pay      NUMBER := 0;
    v_tot_ded      NUMBER := 0;
    v_dept_tot_pay NUMBER := 0;
    v_dept_tot_ded NUMBER := 0;
    v_grp_tot_pay  NUMBER := 0;
    v_grp_tot_ded  NUMBER := 0;
    v_loan_ded     NUMBER := 0;
    -- v_dept_id         VARCHAR2(50);
    v_hdr_seq         VARCHAR2(50);
    v_dtl_seq         VARCHAR2(50);
    v_dtl_dtl_seq     VARCHAR2(50);
    v_period_from_dt  DATE;
    v_period_to_dt    DATE;
    v_cut_off_dt      DATE;
    v_last_pay_dt     DATE;
    v_payroll_days    NUMBER;
    v_holidays        NUMBER := 0;
    v_multiply_factor NUMBER := 1;
    v_actual_days     NUMBER := 0;
    v_pay_amt         NUMBER := 0;
    v_ded_amt         NUMBER := 0;
    v_nr_amt          NUMBER := 0;
    v_basic_sal_amt   NUMBER := 0;
    v_remarks         VARCHAR2(100);
    v_desig           VARCHAR2(50);
    v_formula         VARCHAR2(200);
    v_pay_period_prev VARCHAR2(50);
    v_leave_for_month NUMBER;
    tmp_adj_amt       NUMBER := 0;
    tmp_day_count     NUMBER;
    --v_mgrp_emp_ded NUMBER:=0;
    v_tmp_to_date  date;
    v_ignore_emp   CHAR(1) := '0';
    v_all_elements CHAR(1);
    v_skip_element CHAR(1) := '0';
    v_stop_resume  CHAR(1);
    v_resume_flag  CHAR(1);
    v_mid_month    CHAR(1) := '0';
    v_exclude_fd   CHAR(1) := '0';
    v_stop_dt      DATE;
    v_resume_dt    DATE;
    v_leave_bal    NUMBER := 0;
    v_err_flag     CHAR(1);
    v_query_str    VARCHAR2(5000);
  
    v_exists_flag CHAR(1) := 'N';
    v_payroll_id  PAY_TRIAL_RUN_HDR.PAYROLL_ID%TYPE;
    v_fiscal_year GL_ACCT_CALENDAR_HDR.CAL_ID%TYPE;
  
    v_sys_option1  SSM_SYSTEM_OPTIONS.HR_PAY_ELEMENT_CODE%TYPE;
    v_sys_option2  SSM_SYSTEM_OPTIONS.HR_BASIC_ELEMENT_CODE%TYPE;
    v_sys_option3  SSM_SYSTEM_OPTIONS.HR_MGRP_EMP_ELEMENT_CODE%TYPE;
    v_sys_option4  SSM_SYSTEM_OPTIONS.HR_MGRP_COMP_ELEMENT_CODE%TYPE;
    v_sys_option5  SSM_SYSTEM_OPTIONS.HR_EARN_LEAVE%TYPE;
    v_sys_option6  SSM_SYSTEM_OPTIONS.LEAVE_ADJ_CODE%TYPE;
    v_sys_option7  SSM_SYSTEM_OPTIONS.LEAVE_ADJ_EARN%TYPE;
    v_sys_option8  SSM_SYSTEM_OPTIONS.HR_MOBI_INVOICE_ELEMENT_CODE%TYPE;
    v_sys_option9  SSM_SYSTEM_OPTIONS.HR_MOBI_DEDUCT_ELEMENT_CODE%TYPE;
    v_sys_option10 SSM_SYSTEM_OPTIONS.HR_MEDICAL_DEDUCT_ELEMENT_CODE%TYPE;
    v_sys_option11 SSM_SYSTEM_OPTIONS.HR_RENT_DEDUCT_ELEMENT_CODE%TYPE;
  
    v_sys_option3_val SSM_SYSTEM_OPTIONS.SSDP_EMPLOYEE%TYPE;
    v_sys_option4_val SSM_SYSTEM_OPTIONS.SSDP_EMPLOYER%TYPE;
  
    v_mgrp_emp_amt NUMBER := 0;
  
    v_exist_payroll   VARCHAR2(5000);
    v_lop_factor      NUMBER := 1;
    tmp_lop_days      NUMBER := 0;
    v_lop_days        NUMBER := 0;
    tmp_al_days       NUMBER := 0;
    v_al_days         NUMBER := 0;
    v_idx             NUMBER(10) := 1;
    v_no_fridays      NUMBER := 0;
    v_no_working_days NUMBER := 0;
    v_no_holidays     NUMBER := 0;
    v_skip_emp        CHAR(1) := '0';
    tmp_dt            DATE;
    v_up_days         number;
  BEGIN
    DBMS_OUTPUT.PUT_LINE('START OF THE PROGRAM');
    -- social secrity calculation
    payroll_social_trial(p_org_id     => p_org_id,
                         p_pay_period => p_pay_period);
    commit;
    -- v_payroll_days:= get_days(p_from_dt =>v_period_from_dt,p_to_dt=>v_period_to_dt)+1;
    ----dbms_output.put_line('Payroll Days '||v_payroll_days);
    --System option1 for Loan Deduction Element
    BEGIN
      SELECT X.HR_PAY_ELEMENT_CODE,
             X.HR_BASIC_ELEMENT_CODE,
             X.HR_MGRP_EMP_ELEMENT_CODE,
             X.HR_MGRP_COMP_ELEMENT_CODE,
             X.HR_EARN_LEAVE,
             X.LEAVE_ADJ_CODE,
             X.LEAVE_ADJ_EARN,
             X.HR_MOBI_INVOICE_ELEMENT_CODE,
             X.HR_MOBI_DEDUCT_ELEMENT_CODE,
             X.HR_MEDICAL_DEDUCT_ELEMENT_CODE,
             X.HR_RENT_DEDUCT_ELEMENT_CODE,
             X.SSDP_EMPLOYEE,
             X.SSDP_EMPLOYER
        INTO v_sys_option1,
             v_sys_option2,
             v_sys_option3,
             v_sys_option4,
             v_sys_option5,
             v_sys_option6,
             v_sys_option7,
             v_sys_option8,
             v_sys_option9,
             v_sys_option10,
             v_sys_option11,
             v_sys_option3_val,
             v_sys_option4_val
        FROM SSM_SYSTEM_OPTIONS X
       WHERE X.HR_EFFECTIVE_TO_DATE IS NULL;
      --AND X.MODULE_CODE = 'HR';
    EXCEPTION
      WHEN OTHERS THEN
        ----dbms_output.put_line('Err sys '||sqlerrm);
        v_sys_option1     := NULL;
        v_sys_option2     := NULL;
        v_sys_option3     := NULL;
        v_sys_option4     := NULL;
        v_sys_option5     := NULL;
        v_sys_option3_val := NULL;
        v_sys_option4_val := NULL;
    END;
    --dbms_output.put_line('System options ' || v_sys_option1 || ' - ' ||v_sys_option2 || ' - ' || v_sys_option3 || ' - ' ||v_sys_option4 || ' - ' || v_sys_option5 || ' - ' ||v_sys_option6 || ' - ' || v_sys_option7 || ' - ' ||v_sys_option3_val || ' - ' || v_sys_option4_val);
  
    FOR pd_cur IN cur_pay_period(p_pay_period) LOOP
      v_period_from_dt  := pd_cur.PAY_FROM_DT;
      v_period_to_dt    := pd_cur.PAY_TO_DT;
      v_no_fridays      := pd_cur.NO_FRIDAYS;
      v_no_working_days := pd_cur.NO_WORKING_DAYS;
      v_no_holidays     := pd_cur.NO_HOLIDAYS;
      --v_cut_off_dt:=pd_cur.PAYROLL_CUT_OFF_DT;
    END LOOP;
    --v_fiscal_year := get_fiscal_year(p_org_id,to_char(v_period_from_dt, 'YYYY'));
    v_fiscal_year := get_fiscal_year(p_org_id, v_period_from_dt);
  
    v_query_str := 'SELECT
  distinct eval.pay_emp_dept_id as pay_emp_dept_id,to_number(hd.attribute1)
FROM pay_group_element_mapping grp1,
  pay_elements ele,
  pay_group_dtls grp,
  pay_emp_element_value eval,
  hr_departments hd
WHERE grp1.pay_element_id=ele.pay_element_id
AND eval.pay_emp_dept_id=hd.dept_id
AND grp.pay_group_id     =grp1.pay_group_id
AND eval.pay_element_id  =grp1.pay_element_id
AND eval.pay_group_id=grp.pay_group_id
AND grp1.pay_element_id=eval.pay_element_id
AND grp.PAY_GROUP_ID= ''PG_ID-0000001035''
order by to_number(hd.attribute1)'; --get_dept_query(p_dept_list, p_pay_group);
    ----dbms_output.put_line('Query string '||v_query_str);
    --debug_proc('Query string '||v_query_str);
  
    DELETE TMP_PAYROLL;
    delete from pay_trial_run_dtl_dtl;
    delete from pay_trial_run_dtl;
    delete from pay_trial_run_hdr;
    COMMIT;
    FOR g_cur IN group_cur LOOP
      --dbms_output.put_line('Pay group ' || g_cur.pay_group_id || ' - ' ||g_cur.pay_group_desc);
      --FOR m1 IN pay_cur_dept(g_cur.PAY_GROUP_ID) LOOP
      ----dbms_output.put_line(' v_query_str  '||v_query_str);
      /*     OPEN v_dept_cur FOR v_query_str;
      
            LOOP
              FETCH v_dept_cur
                INTO v_dept_id,v_dep_num;
              EXIT WHEN v_dept_cur%NOTFOUND;
      */
      FOR m2 IN pay_cur(g_cur.PAY_GROUP_ID,
                        null,
                        p_emp_id,
                        v_period_to_dt,
                        v_period_from_dt) LOOP
      
        if m2.pay_element_id = 'PYE_ID-0000000035' then
          null;
        end if;
      
        --dbms_output.put_line('Payroll ' || m2.pay_group_id || '- ' ||m2.pay_element_id || '- ' || m2.pay_emp_id ||' - ' || m2.pay_element_code || '-' ||m2.pay_element_prorate || ' - ' ||m2.emp_prorate || ' - ' ||upper(m2.pay_ele_class) || ' - ' ||m2.pay_amount || ' -Department ' ||m2.dept_id || ' -Designation ' ||get_desig_id(p_org_id,m2.dept_id,m2.pay_emp_id)); -- m1.pay_emp_dept_id);
        --  debug_proc('Payroll '||m2.pay_group_id||'- '||m2.pay_element_id||'- '||m2.pay_emp_id||' - '||m2.pay_element_code||' - '||upper(m2.pay_ele_class)||' - '||m2.pay_amount||' -Department '||m1.pay_emp_dept_id);
        v_desig := get_desig_id(p_org_id, m2.dept_id, m2.pay_emp_id);
      
        v_formula := NVL(get_element_str(p_org_id,
                                         m2.dept_id,
                                         v_desig,
                                         m2.pay_element_id),
                         m2.pay_amount);
        ----dbms_output.put_line('Formula '||NVL(get_element_str(p_org_id,v_dept_id,v_desig,m2.pay_element_id),m2.pay_amount));
        ----dbms_output.put_line('Formula value '||get_pay_element_calc(p_org_id,v_dept_id,v_desig,m2.pay_emp_id,v_formula));
      
        v_ded_amt := 0;
        v_pay_amt := 0;
        --v_mgrp_emp_ded:=0;
        v_skip_element        := '0';
        v_mid_month           := '0';
        v_exclude_fd          := '0';
        v_lop_factor          := 1;
        tmp_lop_days          := 0;
        v_lop_days            := 0;
        tmp_dt                := NULL;
        v_loan_ded            := 0;
        tmp_AL_days           := 0;
        v_AL_days             := 0;
        v_previous_month_days := 0;
        v_skip_emp            := '0';
        v_up_days             := 0;
      
        FOR pd_cur IN cur_pay_period(p_pay_period) LOOP
          v_period_from_dt := pd_cur.PAY_FROM_DT;
          v_period_to_dt   := pd_cur.PAY_TO_DT;
          --v_cut_off_dt:=pd_cur.PAYROLL_CUT_OFF_DT;
        END LOOP;
        v_last_pay_dt := get_last_pay_dt(p_org_id => p_org_id,
                                         p_emp_id => m2.pay_emp_id);
      
        --dbms_output.put_line('Payroll dates1 '||v_period_from_dt ||' - '||  v_period_to_dt||' Employee Doj '||m2.emp_doj);
        --dbms_output.put_line('Last pay date for '||m2.pay_emp_id ||' Is '||v_last_pay_dt);
      
        IF m2.emp_doj BETWEEN v_period_from_dt AND v_period_to_dt THEN
          --v_period_from_dt:=m2.emp_doj;
          --dbms_output.put_line('1');
          v_mid_month  := '1';
          v_exclude_fd := '1';
          --v_period_from_dt:=m2.emp_doj;
          /*v_actual_days := get_days(p_from_dt => m2.emp_doj,
          p_to_dt   => v_period_to_dt);*/
        ELSIF v_last_pay_dt IS NULL AND m2.emp_doj <= v_period_from_dt THEN
          --dbms_output.put_line('2');
          v_exclude_fd          := '1';
          v_previous_month_days := 0;
          v_previous_month_days := get_days(p_from_dt => m2.emp_doj,
                                            p_to_dt   => v_period_from_dt - 1);
        ELSIF m2.emp_doj > v_period_to_dt OR m2.stop_paytoll = '1' THEN
          --dbms_output.put_line('3');
          v_skip_emp := '1';
        END IF;
      
        IF nvl(m2.termination_date, v_period_to_dt + 1) BETWEEN
           v_period_from_dt AND v_period_to_dt THEN
          v_tmp_to_date := m2.termination_date;
        else
          v_tmp_to_date := v_period_to_dt;
        end if;
      
        IF v_skip_emp = '0' THEN
        
          IF v_mid_month = '0' THEN
            tmp_dt := v_period_from_dt;
          ELSE
            tmp_dt := m2.emp_doj;
          END IF;
          get_element_stop_dtl(p_emp_id       => m2.pay_emp_id,
                               p_element_id   => m2.pay_element_id,
                               p_from_dt      => tmp_dt, --v_period_from_dt,
                               p_to_dt        => v_tmp_to_date,
                               p_exclude_fd   => v_exclude_fd,
                               p_payroll_days => v_multiply_factor,
                               p_skip_element => v_skip_element,
                               p_ignore_emp   => v_ignore_emp);
        
          v_fixed_days := ssm.get_parameter_value(p_param_code => 'FIXEDDAYS');
        
          if tmp_dt < m2.effective_from_dt then
            tmp_dt := m2.effective_from_dt;
          end if;
        
          if v_tmp_to_date > nvl(m2.effective_to_dt, v_period_to_dt) then
            v_tmp_to_date := m2.effective_to_dt;
          end if;
        
          v_working_days := get_days(p_from_dt => tmp_dt,
                                     p_to_dt   => v_tmp_to_date);
        
          if v_fixed_days > 0 then
            v_payroll_days := v_fixed_Days;
          else
            v_payroll_days := get_days(p_from_dt => v_period_from_dt,
                                       p_to_dt   => v_period_to_dt);
          end if;
        
          if v_working_days < v_payroll_days and v_mid_month = '0' and
             v_fixed_days > 0 then
            v_working_days := v_payroll_days;
          
          end if;
        
          begin
          
            if m2.pay_element_id = 'PYE_ID-0000000030' then
              null;
            end if;
          
            for i in (SELECT a.*
                        FROM HR_STAFF_ATTENDANCE   a,
                             HR_LEAVE_APPLICATIONS b,
                             HR_LEAVE_DEFINITIONS  c,
                             ssm_system_options    sso
                       WHERE a.LEAVE_REQ_ID = B.LEAVE_REQ_ID
                         AND B.LEAVE_ID = C.LEAVE_ID
                         AND C.WITH_PAY_YN = '0'
                         AND a.ENABLED_FLAG = '1'
                         AND a.ORG_ID = p_org_id
                         AND a.STAFF_ID = m2.pay_emp_id
                         AND a.ATTENDANCE_DATE <= v_period_to_dt
                         and a.attendance_date >= v_period_from_dt
                         AND c.leave_id != sso.hr_earn_leave) loop
            
              tmp_lop_days := get_days(p_from_dt => i.attendance_date,
                                       p_to_dt   => i.attendance_date);
            
              v_up_days := v_up_days + NVL(tmp_lop_days, 0);
            
            end loop;
          
          end;
          v_holidays := 0; /*get_holidays(p_org_id      => p_org_id,
                                                       p_fiscal_year => v_fiscal_year,
                                                       p_from_dt     => v_period_from_dt,
                                                       p_to_dt       => v_period_to_dt);*/
        
          v_payroll_days := v_payroll_days - v_holidays;
        
          ----dbms_output.put_line('Payroll Days ' || v_payroll_days ||' Multiply Factor before ' ||v_multiply_factor);
        
          --Below for loop for LOP factor and LOP days
          ----dbms_output.put_line('DD '||v_fiscal_year||' - '||p_org_id||' - '||m2.pay_emp_id||' - '||v_period_to_dt);
          FOR l_cur IN leave_cur_non_AL(v_fiscal_year,
                                        p_org_id,
                                        m2.pay_emp_id,
                                        v_period_to_dt) LOOP
            ----dbms_output.put_line('Leave taken by '||m2.pay_emp_id ||' -> '||l_cur.leave_id);
            tmp_lop_days := 0;
            get_LOP_days(p_org_id      => p_org_id,
                         p_fiscal_year => v_fiscal_year,
                         p_dept_id     => m2.dept_id,
                         p_staff_id    => m2.pay_emp_id,
                         p_leave_id    => l_cur.leave_id,
                         p_from_date   => tmp_dt, --v_period_from_dt,
                         p_to_date     => v_tmp_to_date,
                         p_level       => l_cur.attribute1,
                         p_lop_factor  => v_lop_factor,
                         p_lop_days    => tmp_lop_days);
            ----dbms_output.put_line('tmp_lop_days  '||tmp_lop_days);
            v_lop_days := v_lop_days + NVL(tmp_lop_days, 0);
          
          END LOOP;
        
          v_AL_BMONTH   := '0';
          v_AL_TOT_Days := 0;
          --debug_proc(' v_fiscal_year '||v_fiscal_year||' - '||' p_org_id '||p_org_id||' - '||m2.pay_emp_id||' - '||v_period_to_dt);
          FOR l_cur IN leave_cur_AL(v_fiscal_year,
                                    p_org_id,
                                    m2.pay_emp_id,
                                    v_period_to_dt) LOOP
            ----dbms_output.put_line('Leave taken by '||m2.pay_emp_id ||' -> '||l_cur.leave_id);
            get_AL_days(p_org_id       => p_org_id,
                        p_fiscal_year  => v_fiscal_year,
                        p_dept_id      => m2.dept_id,
                        p_staff_id     => m2.pay_emp_id,
                        p_leave_id     => l_cur.leave_id,
                        p_from_date    => tmp_dt, --v_period_from_dt,
                        p_to_date      => v_tmp_to_date,
                        p_level        => l_cur.attribute1,
                        p_lop_factor   => v_lop_factor,
                        p_lop_days     => tmp_AL_days,
                        p_leave_req_id => l_cur.leave_req_id);
            ----dbms_output.put_line('tmp_lop_days  '||tmp_lop_days);
            v_AL_days     := v_AL_days + NVL(tmp_AL_days, 0);
            v_AL_TOT_Days := v_AL_TOT_Days + l_cur.no_of_days;
            if l_cur.leave_from != l_cur.leave_to then
              v_AL_BMONTH := '1';
            end if;
          END LOOP;
        
          v_multiply_factor := v_payroll_days - v_al_days - v_up_days;
          --natesh
          /*          if m2.pay_emp_id = 'EMP_ID-0000003186' and p_pay_period='PP_ID-0000001016'then
                      v_multiply_factor:=25;
                    end if;
          */ --debug_proc('v_AL_days  '||v_AL_days);
          --debug_proc('v_multiply_factor '||v_multiply_factor);
          ----dbms_output.put_line('Total LOP Days for the month '||v_lop_days);
        
          --get previous pay period
          select max(pay_period_id)
            into v_pay_period_prev
            from pay_emp_periods
           where pay_period_id < p_pay_period; --'PP_ID-0000001023'
          --previous payroll period cut off dt
          IF v_pay_period_prev IS NOT NULL THEN
          
            BEGIN
              select payroll_cut_off_dt
                into v_cut_off_dt
                from pay_periods
               where pay_period_id = v_pay_period_prev;
            EXCEPTION
              WHEN OTHERS THEN
                v_cut_off_dt := NULL;
            END;
          
            --start
            ----dbms_output.put_line('v_cut_off_dt  '||to_char(v_cut_off_dt,'DD-MON-YY')||' - '||v_period_from_dt||' - '||v_fiscal_year);
            --Annual leave days taken after cut off date for previous payroll period
            SELECT COUNT(*)
              INTO v_leave_for_month
              FROM HR_STAFF_ATTENDANCE   a,
                   HR_LEAVE_APPLICATIONS b,
                   HR_LEAVE_DEFINITIONS  c
             WHERE a.LEAVE_REQ_ID = B.LEAVE_REQ_ID
               AND B.LEAVE_ID = C.LEAVE_ID
               AND C.WITH_PAY_YN = '1'
                  --AND UPPER(LTRIM(RTRIM(a.ATTENDANCE_TYPE))) IN('ABSENT', 'LEAVE')
               AND a.ENABLED_FLAG = '1'
               AND a.ORG_ID = p_org_id
                  --AND a.FISCAL_YEAR = v_fiscal_year
               AND a.STAFF_ID = m2.pay_emp_id
               AND c.leave_id = v_sys_option5
               AND a.ATTENDANCE_DATE >= v_cut_off_dt
               AND a.ATTENDANCE_DATE <= (v_period_from_dt - 1);
            ----dbms_output.put_line('v_leave_for_month ' ||v_leave_for_month);
            --end
            FOR pd_cur IN cur_pay_period(v_pay_period_prev) LOOP
              tmp_day_count := get_days(p_from_dt => pd_cur.PAY_FROM_DT,
                                        p_to_dt   => pd_cur.PAY_TO_DT);
              ----dbms_output.put_line('Dates '||to_char(pd_cur.PAY_FROM_DT,'DD-MON-YY')||' - '||to_char(pd_cur.PAY_TO_DT,'DD-MON-YY'));
            END LOOP;
            ----dbms_output.put_line('tmp_day_count '||tmp_day_count);
          
            IF LTRIM(RTRIM(m2.pay_element_id)) =
               LTRIM(RTRIM(v_sys_option6)) THEN
              --Leave adjustment earnings
              tmp_adj_amt := 0;
              FOR ladj_cur IN leave_adj(v_pay_period_prev, m2.pay_emp_id) LOOP
                --C.PAY_AMOUNT,D.PAY_ELEMENT_ID
                ----dbms_output.put_line('Leave adjustment components '||ladj_cur.PAY_ELEMENT_ID || ' - '||ladj_cur.PAY_AMOUNT);
                tmp_adj_amt := tmp_adj_amt +
                               (ladj_cur.PAY_AMOUNT * v_leave_for_month /
                               tmp_day_count);
                --dbms_output.put_line('tmp_adj_amt ' || tmp_adj_amt);
              END LOOP;
            
              v_ded_amt      := tmp_adj_amt;
              v_tot_ded      := v_tot_ded + nvl(v_ded_amt, 0);
              v_dept_tot_ded := v_dept_tot_ded + nvl(v_ded_amt, 0);
              v_grp_tot_ded  := v_grp_tot_ded + nvl(v_ded_amt, 0);
            ELSIF LTRIM(RTRIM(m2.pay_element_id)) =
                  LTRIM(RTRIM(v_sys_option7)) THEN
              --Leave adjustment deductions
              tmp_adj_amt := 0;
              /*              FOR ladj_cur IN leave_adj_ded(v_pay_period_prev,
                                                          m2.pay_emp_id) LOOP
                              --C.PAY_AMOUNT,D.PAY_ELEMENT_ID
                              ----dbms_output.put_line('Leave adjustment components '||ladj_cur.PAY_ELEMENT_ID || ' - '||ladj_cur.PAY_AMOUNT);
                              tmp_adj_amt := tmp_adj_amt +
                                             (ladj_cur.PAY_AMOUNT * v_leave_for_month /
                                             tmp_day_count);
                              --dbms_output.put_line('tmp_adj_amt ' || tmp_adj_amt);
                            END LOOP;
              */
            END IF;
          END IF;
        
          IF m2.eval_unpaid_leave = '1' THEN
            --v_payroll_days:=v_payroll_days-v_lop_days;
            v_multiply_factor := v_multiply_factor - v_lop_days;
          END IF;
        
          IF m2.ele_annual_leave = '1' THEN
            v_multiply_factor := v_multiply_factor;
          END IF;
        
          ----dbms_output.put_line('Pay days '||v_payroll_days||' Multiply '||v_multiply_factor||' - LOP Factor '||v_lop_factor);
          ----dbms_output.put_line('Deciding factors '||' Igonore emp '||v_ignore_emp||' - Skip Element '||v_skip_element);
          IF v_ignore_emp = '0' THEN
          
            IF m2.pay_ele_recurring = '1' THEN
              IF UPPER(RTRIM(LTRIM(m2.pay_ele_class))) = 'EARNING' THEN
                -- debug_proc('Earning ='||'Department '||m1.pay_emp_dept_id||'-'||upper(m2.pay_ele_class)||'-'||v_dept_tot_pay||' For '||m2.pay_amount);
                IF v_skip_element = '0' THEN
                  IF m2.pay_element_prorate = '1' THEN
                    IF m2.emp_prorate = '1' THEN
                      --Below block for pro-rate the element
                      IF LTRIM(RTRIM(m2.pay_element_id)) =
                         LTRIM(RTRIM(v_sys_option2)) THEN
                        --BAS
                        v_pay_amt := m2.pay_amount * v_multiply_factor *
                                     v_lop_factor / v_payroll_days;
                      ELSE
                        v_pay_amt := m2.pay_amount * v_multiply_factor /
                                     v_payroll_days;
                      END IF;
                      --dbms_output.put_line('Pro-rate1' || '-Element ' ||
                      --        m2.pay_element_id || ' - ' ||
                      --        v_pay_amt || ' Multiply ' ||
                      --        v_multiply_factor ||
                      --       ' Payroll days ' ||
                      --       v_payroll_days);
                      v_tot_pay      := v_tot_pay + nvl(v_pay_amt, 0);
                      v_dept_tot_pay := v_dept_tot_pay + nvl(v_pay_amt, 0);
                      v_grp_tot_pay  := v_grp_tot_pay + nvl(v_pay_amt, 0);
                    ELSE
                      --below block for not to pro-rate the element
                      IF LTRIM(RTRIM(m2.pay_element_id)) =
                         LTRIM(RTRIM(v_sys_option2)) THEN
                        --BAS
                        v_pay_amt := m2.pay_amount * v_lop_factor;
                      ELSE
                        v_pay_amt := m2.pay_amount;
                      END IF;
                      --dbms_output.put_line('NO Pro-rate1' || '-Element ' ||
                      --                m2.pay_element_id || ' - ' ||
                      --              v_pay_amt || ' Multiply ' ||
                      --            v_multiply_factor ||
                      --          ' Payroll days ' ||
                      --        v_payroll_days);
                      v_tot_pay      := v_tot_pay + nvl(v_pay_amt, 0);
                      v_dept_tot_pay := v_dept_tot_pay + nvl(v_pay_amt, 0);
                      v_grp_tot_pay  := v_grp_tot_pay + nvl(v_pay_amt, 0);
                    END IF;
                  ELSE
                    --v_pay_amt:=m2.pay_amount*v_multiply_factor;
                    IF m2.emp_prorate = '1' THEN
                      --Below block for pro-rate the element
                      IF LTRIM(RTRIM(m2.pay_element_id)) =
                         LTRIM(RTRIM(v_sys_option2)) THEN
                        --BAS
                        v_pay_amt := m2.pay_amount * v_multiply_factor *
                                     v_lop_factor / v_payroll_days;
                      ELSE
                        v_pay_amt := m2.pay_amount * v_multiply_factor /
                                     v_payroll_days;
                      END IF;
                      --dbms_output.put_line('Pro-rate2' || '-Element ' ||
                      --                   m2.pay_element_id || ' - ' ||
                      --                 v_pay_amt || ' Multiply ' ||
                      --               v_multiply_factor ||
                      --             ' Payroll days ' ||
                      --           v_payroll_days);
                      v_tot_pay      := v_tot_pay + nvl(v_pay_amt, 0);
                      v_dept_tot_pay := v_dept_tot_pay + nvl(v_pay_amt, 0);
                      v_grp_tot_pay  := v_grp_tot_pay + nvl(v_pay_amt, 0);
                    ELSE
                      --below block for not to pro-rate the element
                      IF LTRIM(RTRIM(m2.pay_element_id)) =
                         LTRIM(RTRIM(v_sys_option2)) THEN
                        --BAS
                        v_pay_amt := m2.pay_amount * v_lop_factor;
                      ELSE
                        v_pay_amt := m2.pay_amount;
                      END IF;
                      ----dbms_output.put_line('NO Pro-rate2' ||'-Element '||m2.pay_element_id||' - '||v_pay_amt||' Multiply '||v_multiply_factor||' Payroll days '||v_payroll_days);
                      v_tot_pay      := v_tot_pay + nvl(v_pay_amt, 0);
                      v_dept_tot_pay := v_dept_tot_pay + nvl(v_pay_amt, 0);
                      v_grp_tot_pay  := v_grp_tot_pay + nvl(v_pay_amt, 0);
                    END IF;
                  END IF;
                END IF;
              ELSIF UPPER(RTRIM(LTRIM(m2.pay_ele_class))) = 'DEDUCTION' THEN
                --  debug_proc('Ded ='||'Department '||m1.pay_emp_dept_id||'-'||upper(m2.pay_ele_class)||'-'||v_dept_tot_ded||' For '||m2.pay_amount);
                ----dbms_output.put_line(' Recurring Deduction' ||'-Element '||m2.pay_element_id||' - '||v_pay_amt||' Multiply '||v_multiply_factor||' Payroll days '||v_payroll_days);
                --dbms_output.put_line('Recurring Deduction sys ' ||v_sys_option1 || ' - ' ||v_sys_option3 || ' - ' ||v_sys_option4);
                IF m2.pay_element_prorate = '1' THEN
                  IF v_skip_element = '0' THEN
                    IF v_sys_option1 IS NOT NULL AND
                       LTRIM(RTRIM(v_sys_option1)) =
                       LTRIM(RTRIM(m2.pay_element_id)) THEN
                      ----dbms_output.put_line('System option test1 '||v_sys_option1 ||' -Ele id '||m2.pay_element_id);
                      --Below IF ELSE ENDIF for Loan Deduction
                      --dbms_output.put_line('Loan parameters ' ||p_org_id || ' - ' ||m2.dept_id || ' - ' ||m2.pay_emp_id || ' - ' ||v_period_from_dt || ' - ' ||v_period_to_dt);
                      v_ded_amt := get_loan_deduction(p_org_id  => p_org_id,
                                                      p_dept_id => m2.dept_id,
                                                      p_emp_id  => m2.pay_emp_id,
                                                      p_from_dt => v_period_from_dt,
                                                      p_to_dt   => v_period_to_dt);
                      --dbms_output.put_line('Loan Deduction1 ' ||v_ded_amt);
                      v_ded_amt      := nvl(v_ded_amt, 0) *
                                        v_multiply_factor / v_payroll_days;
                      v_tot_ded      := v_tot_ded + nvl(v_ded_amt, 0);
                      v_dept_tot_ded := v_dept_tot_ded + nvl(v_ded_amt, 0);
                      v_grp_tot_ded  := v_grp_tot_ded + nvl(v_ded_amt, 0);
                    ELSIF v_sys_option8 IS NOT NULL AND
                          LTRIM(RTRIM(v_sys_option8)) =
                          LTRIM(RTRIM(m2.pay_element_id)) THEN
                      --start
                      --Below IF ELSE ENDIF for Mobile Invoice Deduction
                      --dbms_output.put_line('Loan parameters ' ||p_org_id || ' - ' ||m2.dept_id || ' - ' ||m2.pay_emp_id || ' - ' ||v_period_from_dt || ' - ' ||v_period_to_dt);
                      v_ded_amt := get_loan_deduction(p_org_id  => p_org_id,
                                                      p_dept_id => m2.dept_id,
                                                      p_emp_id  => m2.pay_emp_id,
                                                      p_from_dt => v_period_from_dt,
                                                      p_to_dt   => v_period_to_dt);
                      --dbms_output.put_line('Loan Deduction1 ' ||v_ded_amt);
                      v_ded_amt      := nvl(v_ded_amt, 0) *
                                        v_multiply_factor / v_payroll_days;
                      v_tot_ded      := v_tot_ded + nvl(v_ded_amt, 0);
                      v_dept_tot_ded := v_dept_tot_ded + nvl(v_ded_amt, 0);
                      v_grp_tot_ded  := v_grp_tot_ded + nvl(v_ded_amt, 0);
                      --end
                    ELSIF v_sys_option9 IS NOT NULL AND
                          LTRIM(RTRIM(v_sys_option9)) =
                          LTRIM(RTRIM(m2.pay_element_id)) THEN
                      --start
                      --Below IF ELSE ENDIF for Mobile  Deduction
                      --dbms_output.put_line('Loan parameters ' ||p_org_id || ' - ' ||m2.dept_id || ' - ' ||m2.pay_emp_id || ' - ' ||v_period_from_dt || ' - ' ||v_period_to_dt);
                      v_ded_amt := get_loan_deduction(p_org_id  => p_org_id,
                                                      p_dept_id => m2.dept_id,
                                                      p_emp_id  => m2.pay_emp_id,
                                                      p_from_dt => v_period_from_dt,
                                                      p_to_dt   => v_period_to_dt);
                      --dbms_output.put_line('Loan Deduction1 ' ||v_ded_amt);
                      v_ded_amt      := nvl(v_ded_amt, 0) *
                                        v_multiply_factor / v_payroll_days;
                      v_tot_ded      := v_tot_ded + nvl(v_ded_amt, 0);
                      v_dept_tot_ded := v_dept_tot_ded + nvl(v_ded_amt, 0);
                      v_grp_tot_ded  := v_grp_tot_ded + nvl(v_ded_amt, 0);
                      --end
                    ELSIF v_sys_option10 IS NOT NULL AND
                          LTRIM(RTRIM(v_sys_option10)) =
                          LTRIM(RTRIM(m2.pay_element_id)) THEN
                      --start
                      --Below IF ELSE ENDIF for Medical Deduction
                      --dbms_output.put_line('Loan parameters ' ||p_org_id || ' - ' ||m2.dept_id || ' - ' ||m2.pay_emp_id || ' - ' ||v_period_from_dt || ' - ' ||v_period_to_dt);
                      v_ded_amt := get_loan_deduction(p_org_id  => p_org_id,
                                                      p_dept_id => m2.dept_id,
                                                      p_emp_id  => m2.pay_emp_id,
                                                      p_from_dt => v_period_from_dt,
                                                      p_to_dt   => v_period_to_dt);
                      --dbms_output.put_line('Loan Deduction1 ' ||v_ded_amt);
                      v_ded_amt      := nvl(v_ded_amt, 0) *
                                        v_multiply_factor / v_payroll_days;
                      v_tot_ded      := v_tot_ded + nvl(v_ded_amt, 0);
                      v_dept_tot_ded := v_dept_tot_ded + nvl(v_ded_amt, 0);
                      v_grp_tot_ded  := v_grp_tot_ded + nvl(v_ded_amt, 0);
                      --end
                    ELSIF v_sys_option11 IS NOT NULL AND
                          LTRIM(RTRIM(v_sys_option11)) =
                          LTRIM(RTRIM(m2.pay_element_id)) THEN
                      --start
                      --Below IF ELSE ENDIF for Mobile Invoice Deduction
                      --dbms_output.put_line('Loan parameters ' ||p_org_id || ' - ' ||m2.dept_id || ' - ' ||m2.pay_emp_id || ' - ' ||v_period_from_dt || ' - ' ||v_period_to_dt);
                      v_ded_amt := get_loan_deduction(p_org_id  => p_org_id,
                                                      p_dept_id => m2.dept_id,
                                                      p_emp_id  => m2.pay_emp_id,
                                                      p_from_dt => v_period_from_dt,
                                                      p_to_dt   => v_period_to_dt);
                      --dbms_output.put_line('Loan Deduction1 ' ||v_ded_amt);
                      v_ded_amt      := nvl(v_ded_amt, 0) *
                                        v_multiply_factor / v_payroll_days;
                      v_tot_ded      := v_tot_ded + nvl(v_ded_amt, 0);
                      v_dept_tot_ded := v_dept_tot_ded + nvl(v_ded_amt, 0);
                      v_grp_tot_ded  := v_grp_tot_ded + nvl(v_ded_amt, 0);
                      --end
                    ELSIF v_sys_option3 IS NOT NULL AND
                          LTRIM(RTRIM(v_sys_option3)) =
                          LTRIM(RTRIM(m2.pay_element_id)) THEN
                      --Below MGRP EMP deduction calculation
                      /*                      IF upper(ltrim(rtrim(m2.nationality))) = 'KUWAIT' AND
                             m2.ssd_flag = '1' THEN
                            --dbms_output.put_line('MGRP Parameters1 ' ||
                            --                v_sys_option3 || ' - ' ||
                            --              v_sys_option3_val || ' - ' ||
                            --            m2.pay_element_id);
                            v_mgrp_emp_amt := get_mgrp_amt(p_org_id => p_org_id,
                                                           p_emp_id => m2.pay_emp_id,
                                                           p_year   => v_fiscal_year);
                          
                            v_basic_sal_amt := get_basic_amt(p_org_id => p_org_id,
                                                             p_emp_id => m2.pay_emp_id);
                          
                      
                            v_ded_amt := (v_mgrp_emp_amt + v_basic_sal_amt) *
                                         v_sys_option3_val / 100;
                            v_ded_amt := nvl(v_ded_amt, 0) * v_multiply_factor /
                                         v_payroll_days;
                            --dbms_output.put_line('Employee Contribution MGRP -1 ' ||
                            --        v_ded_amt);
                      */
                      begin
                        select nvl(pesd.emp_indemnity, 0) +
                               nvl(pesd.emp_bas_supp, 0) +
                               nvl(pesd.emp_inc_pension, 0) +
                               nvl(pesd.emp_unempl_support, 0)
                          into v_ded_amt
                          from pay_employee_ssd_details pesd
                         where pesd.emp_id = m2.pay_emp_id
                           and pesd.org_id = p_org_id
                           and pesd.pay_period = p_pay_period;
                      exception
                        when others then
                          v_ded_amt := 0;
                      end;
                      v_tot_ded      := v_tot_ded + nvl(v_ded_amt, 0);
                      v_dept_tot_ded := v_dept_tot_ded + nvl(v_ded_amt, 0);
                      v_grp_tot_ded  := v_grp_tot_ded + nvl(v_ded_amt, 0);
                      --          END IF;
                    ELSIF v_sys_option4 IS NOT NULL AND
                          LTRIM(RTRIM(v_sys_option4)) =
                          LTRIM(RTRIM(m2.pay_element_id)) THEN
                      --Below MGRP EPR deduction calculation
                      IF upper(ltrim(rtrim(m2.nationality))) = 'KUWAIT' AND
                         m2.ssd_flag = '1' THEN
                        --dbms_output.put_line('MGRP Parameters1-A ' ||
                        --                    v_sys_option4 || ' - ' ||
                        --                  v_sys_option4_val || ' - ' ||
                        --                m2.pay_element_id);
                        v_mgrp_emp_amt := get_mgrp_amt(p_org_id => p_org_id,
                                                       p_emp_id => m2.pay_emp_id,
                                                       p_year   => v_fiscal_year);
                      
                        v_basic_sal_amt := get_basic_amt(p_org_id => p_org_id,
                                                         p_emp_id => m2.pay_emp_id);
                      
                        v_ded_amt := (v_mgrp_emp_amt + v_basic_sal_amt) *
                                     v_sys_option4_val / 100;
                        v_ded_amt := nvl(v_ded_amt, 0) * v_multiply_factor /
                                     v_payroll_days;
                        --dbms_output.put_line('Employee Contribution MGRP -1A ' ||
                        --                    v_ded_amt);
                        v_tot_ded      := v_tot_ded + nvl(v_ded_amt, 0);
                        v_dept_tot_ded := v_dept_tot_ded +
                                          nvl(v_ded_amt, 0);
                        v_grp_tot_ded  := v_grp_tot_ded + nvl(v_ded_amt, 0);
                      END IF;
                    ELSE
                      --dbms_output.put_line(' Else Deduction1' ||
                      --                  '-Element ' ||
                      --                m2.pay_element_id);
                      v_ded_amt      := m2.pay_amount * v_multiply_factor /
                                        v_payroll_days;
                      v_tot_ded      := v_tot_ded + nvl(v_ded_amt, 0);
                      v_dept_tot_ded := v_dept_tot_ded + nvl(v_ded_amt, 0);
                      v_grp_tot_ded  := v_grp_tot_ded + nvl(v_ded_amt, 0);
                    END IF;
                  END IF;
                ELSE
                  IF v_skip_element = '0' THEN
                    IF v_sys_option1 IS NOT NULL AND
                       LTRIM(RTRIM(v_sys_option1)) =
                       LTRIM(RTRIM(m2.pay_element_id)) THEN
                      --Below IF ELSE ENDIF for Loan Deduction
                      v_ded_amt := get_loan_deduction(p_org_id  => p_org_id,
                                                      p_dept_id => m2.dept_id,
                                                      p_emp_id  => m2.pay_emp_id,
                                                      p_from_dt => v_period_from_dt,
                                                      p_to_dt   => v_period_to_dt);
                      --dbms_output.put_line('Loan Deduction2 ' || v_ded_amt);
                      v_tot_ded      := v_tot_ded + nvl(v_ded_amt, 0);
                      v_dept_tot_ded := v_dept_tot_ded + nvl(v_ded_amt, 0);
                      v_grp_tot_ded  := v_grp_tot_ded + nvl(v_ded_amt, 0);
                    ELSIF v_sys_option8 IS NOT NULL AND
                          LTRIM(RTRIM(v_sys_option8)) =
                          LTRIM(RTRIM(m2.pay_element_id)) THEN
                      --Below IF ELSE ENDIF for Mobile Invoice Deduction
                      v_ded_amt := get_loan_deduction(p_org_id  => p_org_id,
                                                      p_dept_id => m2.dept_id,
                                                      p_emp_id  => m2.pay_emp_id,
                                                      p_from_dt => v_period_from_dt,
                                                      p_to_dt   => v_period_to_dt);
                      --dbms_output.put_line('Loan Deduction2 ' || v_ded_amt);
                      v_tot_ded      := v_tot_ded + nvl(v_ded_amt, 0);
                      v_dept_tot_ded := v_dept_tot_ded + nvl(v_ded_amt, 0);
                      v_grp_tot_ded  := v_grp_tot_ded + nvl(v_ded_amt, 0);
                    ELSIF v_sys_option9 IS NOT NULL AND
                          LTRIM(RTRIM(v_sys_option9)) =
                          LTRIM(RTRIM(m2.pay_element_id)) THEN
                      --Below IF ELSE ENDIF for Mobile  Deduction
                      v_ded_amt := get_loan_deduction(p_org_id  => p_org_id,
                                                      p_dept_id => m2.dept_id,
                                                      p_emp_id  => m2.pay_emp_id,
                                                      p_from_dt => v_period_from_dt,
                                                      p_to_dt   => v_period_to_dt);
                      --dbms_output.put_line('Loan Deduction2 ' || v_ded_amt);
                      v_tot_ded      := v_tot_ded + nvl(v_ded_amt, 0);
                      v_dept_tot_ded := v_dept_tot_ded + nvl(v_ded_amt, 0);
                      v_grp_tot_ded  := v_grp_tot_ded + nvl(v_ded_amt, 0);
                    ELSIF v_sys_option10 IS NOT NULL AND
                          LTRIM(RTRIM(v_sys_option10)) =
                          LTRIM(RTRIM(m2.pay_element_id)) THEN
                      --Below IF ELSE ENDIF for Medical Deduction
                      v_ded_amt := get_loan_deduction(p_org_id  => p_org_id,
                                                      p_dept_id => m2.dept_id,
                                                      p_emp_id  => m2.pay_emp_id,
                                                      p_from_dt => v_period_from_dt,
                                                      p_to_dt   => v_period_to_dt);
                      --dbms_output.put_line('Loan Deduction2 ' || v_ded_amt);
                      v_tot_ded      := v_tot_ded + nvl(v_ded_amt, 0);
                      v_dept_tot_ded := v_dept_tot_ded + nvl(v_ded_amt, 0);
                      v_grp_tot_ded  := v_grp_tot_ded + nvl(v_ded_amt, 0);
                    ELSIF v_sys_option11 IS NOT NULL AND
                          LTRIM(RTRIM(v_sys_option11)) =
                          LTRIM(RTRIM(m2.pay_element_id)) THEN
                      --Below IF ELSE ENDIF for Rent Deduction
                      v_ded_amt := get_loan_deduction(p_org_id  => p_org_id,
                                                      p_dept_id => m2.dept_id,
                                                      p_emp_id  => m2.pay_emp_id,
                                                      p_from_dt => v_period_from_dt,
                                                      p_to_dt   => v_period_to_dt);
                      --dbms_output.put_line('Loan Deduction2 ' || v_ded_amt);
                      v_tot_ded      := v_tot_ded + nvl(v_ded_amt, 0);
                      v_dept_tot_ded := v_dept_tot_ded + nvl(v_ded_amt, 0);
                      v_grp_tot_ded  := v_grp_tot_ded + nvl(v_ded_amt, 0);
                    ELSIF v_sys_option3 IS NOT NULL AND
                          LTRIM(RTRIM(v_sys_option3)) =
                          LTRIM(RTRIM(m2.pay_element_id)) THEN
                      --Below MGRP EMP deduction calculation
                      /*                      IF upper(ltrim(rtrim(m2.nationality))) = 'KUWAIT' AND
                                               m2.ssd_flag = '1' THEN
                                              --dbms_output.put_line('MGRP Parameters2 ' ||
                                              --                  v_sys_option3 || ' - ' ||
                                              --                  ' - ' || v_sys_option3_val ||
                                              --                  ' - ' || m2.pay_element_id);
                                              v_mgrp_emp_amt := get_mgrp_amt(p_org_id => p_org_id,
                                                                             p_emp_id => m2.pay_emp_id,
                                                                             p_year   => v_fiscal_year);
                                            
                                              v_basic_sal_amt := get_basic_amt(p_org_id => p_org_id,
                                                                               p_emp_id => m2.pay_emp_id);
                                            
                                              v_ded_amt := (v_mgrp_emp_amt + v_basic_sal_amt) *
                                                           v_sys_option3_val / 100;
                                              --dbms_output.put_line('Employee Contribution MGRP -2 ' ||
                                              --                    v_ded_amt || ' - ' ||
                                              --                  v_basic_sal_amt);
                      */
                      begin
                        select nvl(pesd.emp_indemnity, 0) +
                               nvl(pesd.emp_bas_supp, 0) +
                               nvl(pesd.emp_inc_pension, 0) +
                               nvl(pesd.emp_unempl_support, 0)
                          into v_ded_amt
                          from pay_employee_ssd_details pesd
                         where pesd.emp_id = m2.pay_emp_id
                           and pesd.org_id = p_org_id
                           and pesd.pay_period = p_pay_period;
                      exception
                        when others then
                          v_ded_amt := 0;
                      end;
                      v_tot_ded      := v_tot_ded + nvl(v_ded_amt, 0);
                      v_dept_tot_ded := v_dept_tot_ded + nvl(v_ded_amt, 0);
                      v_grp_tot_ded  := v_grp_tot_ded + nvl(v_ded_amt, 0);
                      --                      END IF;
                    ELSIF v_sys_option4 IS NOT NULL AND
                          LTRIM(RTRIM(v_sys_option4)) =
                          LTRIM(RTRIM(m2.pay_element_id)) THEN
                      --Below MGRP EPR deduction calculation
                      IF upper(ltrim(rtrim(m2.nationality))) = 'KUWAIT' AND
                         m2.ssd_flag = '1' THEN
                        --dbms_output.put_line('MGRP Parameters2A ' ||
                        --                v_sys_option4 || ' - ' ||
                        --               ' - ' || v_sys_option4_val ||
                        --              ' - ' || m2.pay_element_id);
                        v_mgrp_emp_amt := get_mgrp_amt(p_org_id => p_org_id,
                                                       p_emp_id => m2.pay_emp_id,
                                                       p_year   => v_fiscal_year);
                      
                        v_basic_sal_amt := get_basic_amt(p_org_id => p_org_id,
                                                         p_emp_id => m2.pay_emp_id);
                      
                        v_ded_amt := (v_mgrp_emp_amt + v_basic_sal_amt) *
                                     v_sys_option4_val / 100;
                        --dbms_output.put_line('Employer Contribution MGRP -2 ' ||
                        --                v_ded_amt);
                        v_tot_ded      := v_tot_ded + nvl(v_ded_amt, 0);
                        v_dept_tot_ded := v_dept_tot_ded +
                                          nvl(v_ded_amt, 0);
                        v_grp_tot_ded  := v_grp_tot_ded + nvl(v_ded_amt, 0);
                      END IF;
                    ELSE
                      --dbms_output.put_line(' Else Deduction2' ||
                      --                 '-Element ' ||
                      --               m2.pay_element_id);
                      v_ded_amt      := m2.pay_amount;
                      v_tot_ded      := v_tot_ded + nvl(v_ded_amt, 0);
                      v_dept_tot_ded := v_dept_tot_ded + nvl(v_ded_amt, 0);
                      v_grp_tot_ded  := v_grp_tot_ded + nvl(v_ded_amt, 0);
                    END IF;
                  END IF;
                END IF;
              END IF;
            ELSIF m2.pay_ele_recurring = '0' THEN
              --Start Non-Recurring
              --dbms_output.put_line('Start of nr');
              IF m2.pay_period_id = p_pay_period THEN
                IF UPPER(RTRIM(LTRIM(m2.pay_ele_class))) = 'EARNING' THEN
                  -- debug_proc('Earning ='||'Department '||m1.pay_emp_dept_id||'-'||upper(m2.pay_ele_class)||'-'||v_dept_tot_pay||' For '||m2.pay_amount);
                  IF m2.pay_element_prorate = '1' THEN
                    IF v_skip_element = '0' THEN
                      v_pay_amt := m2.pay_amount * v_multiply_factor /
                                   v_payroll_days;
                      --dbms_output.put_line('Pro-rate' || '-Element ' ||
                      --                m2.pay_element_id || ' - ' ||
                      --              v_pay_amt || ' Multiply ' ||
                      --            v_multiply_factor ||
                      --          ' Payroll days ' ||
                      --        v_payroll_days);
                      v_tot_pay      := v_tot_pay + nvl(v_pay_amt, 0);
                      v_dept_tot_pay := v_dept_tot_pay + nvl(v_pay_amt, 0);
                      v_grp_tot_pay  := v_grp_tot_pay + nvl(v_pay_amt, 0);
                    END IF;
                  ELSE
                    --v_pay_amt:=m2.pay_amount*v_multiply_factor;
                    IF v_skip_element = '0' THEN
                      v_pay_amt := m2.pay_amount;
                      --dbms_output.put_line('no Pro-rate' || '-Element ' ||
                      --                     m2.pay_element_id || ' - ' ||
                      --                   v_pay_amt || ' Multiply ' ||
                      --                 v_multiply_factor ||
                      --               ' Payroll days ' ||
                      --             v_payroll_days);
                      v_tot_pay      := v_tot_pay + nvl(v_pay_amt, 0);
                      v_dept_tot_pay := v_dept_tot_pay + nvl(v_pay_amt, 0);
                      v_grp_tot_pay  := v_grp_tot_pay + nvl(v_pay_amt, 0);
                    END IF;
                  END IF;
                
                ELSIF UPPER(RTRIM(LTRIM(m2.pay_ele_class))) = 'DEDUCTION' THEN
                  --  debug_proc('Ded ='||'Department '||m1.pay_emp_dept_id||'-'||upper(m2.pay_ele_class)||'-'||v_dept_tot_ded||' For '||m2.pay_amount);
                  ----dbms_output.put_line('Non Recurring Deduction' ||'-Element '||m2.pay_element_id||' - '||v_pay_amt||' Multiply '||v_multiply_factor||' Payroll days '||v_payroll_days);
                  --dbms_output.put_line('Non Recurring Deduction sys ' ||v_sys_option1 || ' - ' ||v_sys_option3);
                  IF m2.pay_element_prorate = '1' THEN
                    IF v_skip_element = '0' THEN
                      IF v_sys_option1 IS NOT NULL AND
                         LTRIM(RTRIM(v_sys_option1)) =
                         LTRIM(RTRIM(m2.pay_element_id)) THEN
                        --Below IF ELSE ENDIF for Loan Deduction
                        v_ded_amt := get_loan_deduction(p_org_id  => p_org_id,
                                                        p_dept_id => m2.dept_id,
                                                        p_emp_id  => m2.pay_emp_id,
                                                        p_from_dt => v_period_from_dt,
                                                        p_to_dt   => v_period_to_dt);
                        --dbms_output.put_line('Loan Deduction3 ' ||v_ded_amt);
                        v_ded_amt      := nvl(v_ded_amt, 0) *
                                          v_multiply_factor /
                                          v_payroll_days;
                        v_tot_ded      := v_tot_ded + nvl(v_ded_amt, 0);
                        v_dept_tot_ded := v_dept_tot_ded +
                                          nvl(v_ded_amt, 0);
                        v_grp_tot_ded  := v_grp_tot_ded + nvl(v_ded_amt, 0);
                      
                      ELSIF v_sys_option8 IS NOT NULL AND
                            LTRIM(RTRIM(v_sys_option8)) =
                            LTRIM(RTRIM(m2.pay_element_id)) THEN
                        --Below IF ELSE ENDIF for Mobile Invoice Deduction
                        v_ded_amt := get_loan_deduction(p_org_id  => p_org_id,
                                                        p_dept_id => m2.dept_id,
                                                        p_emp_id  => m2.pay_emp_id,
                                                        p_from_dt => v_period_from_dt,
                                                        p_to_dt   => v_period_to_dt);
                        --dbms_output.put_line('Loan Deduction3 ' ||v_ded_amt);
                        v_ded_amt      := nvl(v_ded_amt, 0) *
                                          v_multiply_factor /
                                          v_payroll_days;
                        v_tot_ded      := v_tot_ded + nvl(v_ded_amt, 0);
                        v_dept_tot_ded := v_dept_tot_ded +
                                          nvl(v_ded_amt, 0);
                        v_grp_tot_ded  := v_grp_tot_ded + nvl(v_ded_amt, 0);
                      ELSIF v_sys_option9 IS NOT NULL AND
                            LTRIM(RTRIM(v_sys_option9)) =
                            LTRIM(RTRIM(m2.pay_element_id)) THEN
                        --Below IF ELSE ENDIF for Mobile  Deduction
                        v_ded_amt := get_loan_deduction(p_org_id  => p_org_id,
                                                        p_dept_id => m2.dept_id,
                                                        p_emp_id  => m2.pay_emp_id,
                                                        p_from_dt => v_period_from_dt,
                                                        p_to_dt   => v_period_to_dt);
                        --dbms_output.put_line('Loan Deduction3 ' ||v_ded_amt);
                        v_ded_amt      := nvl(v_ded_amt, 0) *
                                          v_multiply_factor /
                                          v_payroll_days;
                        v_tot_ded      := v_tot_ded + nvl(v_ded_amt, 0);
                        v_dept_tot_ded := v_dept_tot_ded +
                                          nvl(v_ded_amt, 0);
                        v_grp_tot_ded  := v_grp_tot_ded + nvl(v_ded_amt, 0);
                      ELSIF v_sys_option10 IS NOT NULL AND
                            LTRIM(RTRIM(v_sys_option10)) =
                            LTRIM(RTRIM(m2.pay_element_id)) THEN
                        --Below IF ELSE ENDIF for Medical  Deduction
                        v_ded_amt := get_loan_deduction(p_org_id  => p_org_id,
                                                        p_dept_id => m2.dept_id,
                                                        p_emp_id  => m2.pay_emp_id,
                                                        p_from_dt => v_period_from_dt,
                                                        p_to_dt   => v_period_to_dt);
                        --dbms_output.put_line('Loan Deduction3 ' ||v_ded_amt);
                        v_ded_amt      := nvl(v_ded_amt, 0) *
                                          v_multiply_factor /
                                          v_payroll_days;
                        v_tot_ded      := v_tot_ded + nvl(v_ded_amt, 0);
                        v_dept_tot_ded := v_dept_tot_ded +
                                          nvl(v_ded_amt, 0);
                        v_grp_tot_ded  := v_grp_tot_ded + nvl(v_ded_amt, 0);
                      ELSIF v_sys_option11 IS NOT NULL AND
                            LTRIM(RTRIM(v_sys_option11)) =
                            LTRIM(RTRIM(m2.pay_element_id)) THEN
                        --Below IF ELSE ENDIF for Rent  Deduction
                        v_ded_amt := get_loan_deduction(p_org_id  => p_org_id,
                                                        p_dept_id => m2.dept_id,
                                                        p_emp_id  => m2.pay_emp_id,
                                                        p_from_dt => v_period_from_dt,
                                                        p_to_dt   => v_period_to_dt);
                        --dbms_output.put_line('Loan Deduction3 ' ||v_ded_amt);
                        v_ded_amt      := nvl(v_ded_amt, 0) *
                                          v_multiply_factor /
                                          v_payroll_days;
                        v_tot_ded      := v_tot_ded + nvl(v_ded_amt, 0);
                        v_dept_tot_ded := v_dept_tot_ded +
                                          nvl(v_ded_amt, 0);
                        v_grp_tot_ded  := v_grp_tot_ded + nvl(v_ded_amt, 0);
                      ELSIF v_sys_option3 IS NOT NULL AND
                            LTRIM(RTRIM(v_sys_option3)) =
                            LTRIM(RTRIM(m2.pay_element_id)) THEN
                        --Below MGRP EMP deduction calculation
                        /*                        IF upper(ltrim(rtrim(m2.nationality))) = 'KUWAIT' AND
                                                  m2.ssd_flag = '1' THEN
                                                 --dbms_output.put_line('MGRP Parameters3 ' ||
                                                 --                         v_sys_option3 || ' - ' ||
                                                 --                       ' - ' ||
                                                 --                     v_sys_option3_val ||
                                                 --                   ' - ' ||
                                                 --                 m2.pay_element_id);
                                                 v_mgrp_emp_amt := get_mgrp_amt(p_org_id => p_org_id,
                                                                                p_emp_id => m2.pay_emp_id,
                                                                                p_year   => v_fiscal_year);
                                               
                                                 v_basic_sal_amt := get_basic_amt(p_org_id => p_org_id,
                                                                                  p_emp_id => m2.pay_emp_id);
                                               
                                                 v_ded_amt := (v_mgrp_emp_amt + v_basic_sal_amt) *
                                                              v_sys_option3_val / 100;
                                                 v_ded_amt := nvl(v_ded_amt, 0) *
                                                              v_multiply_factor / v_payroll_days;
                                                 --dbms_output.put_line('Employee Contribution MGRP -3 ' ||
                                                 --                           v_ded_amt);
                        */
                        begin
                          select nvl(pesd.emp_indemnity, 0) +
                                 nvl(pesd.emp_bas_supp, 0) +
                                 nvl(pesd.emp_inc_pension, 0) +
                                 nvl(pesd.emp_unempl_support, 0)
                            into v_ded_amt
                            from pay_employee_ssd_details pesd
                           where pesd.emp_id = m2.pay_emp_id
                             and pesd.org_id = p_org_id
                             and pesd.pay_period = p_pay_period;
                        exception
                          when others then
                            v_ded_amt := 0;
                        end;
                        v_tot_ded      := v_tot_ded + nvl(v_ded_amt, 0);
                        v_dept_tot_ded := v_dept_tot_ded +
                                          nvl(v_ded_amt, 0);
                        v_grp_tot_ded  := v_grp_tot_ded + nvl(v_ded_amt, 0);
                        --   END IF;
                      ELSIF v_sys_option4 IS NOT NULL AND
                            LTRIM(RTRIM(v_sys_option4)) =
                            LTRIM(RTRIM(m2.pay_element_id)) THEN
                        --Below MGRP EPR deduction calculation
                        IF upper(ltrim(rtrim(m2.nationality))) = 'KUWAIT' AND
                           m2.ssd_flag = '1' THEN
                          --dbms_output.put_line('MGRP Parameters3A ' ||
                          --                           v_sys_option4 || ' - ' ||
                          --                         v_sys_option4_val ||
                          --                       ' - ' ||
                          --                     m2.pay_element_id);
                          v_mgrp_emp_amt := get_mgrp_amt(p_org_id => p_org_id,
                                                         p_emp_id => m2.pay_emp_id,
                                                         p_year   => v_fiscal_year);
                        
                          v_basic_sal_amt := get_basic_amt(p_org_id => p_org_id,
                                                           p_emp_id => m2.pay_emp_id);
                        
                          v_ded_amt := (v_mgrp_emp_amt + v_basic_sal_amt) *
                                       v_sys_option4_val / 100;
                          v_ded_amt := nvl(v_ded_amt, 0) *
                                       v_multiply_factor / v_payroll_days;
                          --dbms_output.put_line('Employee Contribution MGRP -3 ' ||
                          --                           v_ded_amt);
                          v_tot_ded      := v_tot_ded + nvl(v_ded_amt, 0);
                          v_dept_tot_ded := v_dept_tot_ded +
                                            nvl(v_ded_amt, 0);
                          v_grp_tot_ded  := v_grp_tot_ded +
                                            nvl(v_ded_amt, 0);
                        END IF;
                      ELSE
                        --dbms_output.put_line(' Else Deduction3' ||
                        --                         '-Element ' ||
                        --                       m2.pay_element_id);
                        v_ded_amt      := m2.pay_amount * v_multiply_factor /
                                          v_payroll_days;
                        v_tot_ded      := v_tot_ded + nvl(v_ded_amt, 0);
                        v_dept_tot_ded := v_dept_tot_ded +
                                          nvl(v_ded_amt, 0);
                        v_grp_tot_ded  := v_grp_tot_ded + nvl(v_ded_amt, 0);
                      END IF;
                    END IF;
                  ELSE
                    IF v_skip_element = '0' THEN
                      IF v_sys_option1 IS NOT NULL AND
                         LTRIM(RTRIM(v_sys_option1)) =
                         LTRIM(RTRIM(m2.pay_element_id)) THEN
                        --Below IF ELSE ENDIF for Loan Deduction
                        v_ded_amt := get_loan_deduction(p_org_id  => p_org_id,
                                                        p_dept_id => m2.dept_id,
                                                        p_emp_id  => m2.pay_emp_id,
                                                        p_from_dt => v_period_from_dt,
                                                        p_to_dt   => v_period_to_dt);
                        --dbms_output.put_line('Loan Deduction4 ' ||v_ded_amt);
                        v_tot_ded      := v_tot_ded + nvl(v_ded_amt, 0);
                        v_dept_tot_ded := v_dept_tot_ded +
                                          nvl(v_ded_amt, 0);
                        v_grp_tot_ded  := v_grp_tot_ded + nvl(v_ded_amt, 0);
                      ELSIF v_sys_option8 IS NOT NULL AND
                            LTRIM(RTRIM(v_sys_option8)) =
                            LTRIM(RTRIM(m2.pay_element_id)) THEN
                        --Below IF ELSE ENDIF for Mobile Invoice Deduction
                        v_ded_amt := get_loan_deduction(p_org_id  => p_org_id,
                                                        p_dept_id => m2.dept_id,
                                                        p_emp_id  => m2.pay_emp_id,
                                                        p_from_dt => v_period_from_dt,
                                                        p_to_dt   => v_period_to_dt);
                        --dbms_output.put_line('Loan Deduction4 ' ||v_ded_amt);
                        v_tot_ded      := v_tot_ded + nvl(v_ded_amt, 0);
                        v_dept_tot_ded := v_dept_tot_ded +
                                          nvl(v_ded_amt, 0);
                        v_grp_tot_ded  := v_grp_tot_ded + nvl(v_ded_amt, 0);
                      ELSIF v_sys_option9 IS NOT NULL AND
                            LTRIM(RTRIM(v_sys_option9)) =
                            LTRIM(RTRIM(m2.pay_element_id)) THEN
                        --Below IF ELSE ENDIF for Mobile  Deduction
                        v_ded_amt := get_loan_deduction(p_org_id  => p_org_id,
                                                        p_dept_id => m2.dept_id,
                                                        p_emp_id  => m2.pay_emp_id,
                                                        p_from_dt => v_period_from_dt,
                                                        p_to_dt   => v_period_to_dt);
                        --dbms_output.put_line('Loan Deduction4 ' ||v_ded_amt);
                        v_tot_ded      := v_tot_ded + nvl(v_ded_amt, 0);
                        v_dept_tot_ded := v_dept_tot_ded +
                                          nvl(v_ded_amt, 0);
                        v_grp_tot_ded  := v_grp_tot_ded + nvl(v_ded_amt, 0);
                      ELSIF v_sys_option10 IS NOT NULL AND
                            LTRIM(RTRIM(v_sys_option10)) =
                            LTRIM(RTRIM(m2.pay_element_id)) THEN
                        --Below IF ELSE ENDIF for Medical Deduction
                        v_ded_amt := get_loan_deduction(p_org_id  => p_org_id,
                                                        p_dept_id => m2.dept_id,
                                                        p_emp_id  => m2.pay_emp_id,
                                                        p_from_dt => v_period_from_dt,
                                                        p_to_dt   => v_period_to_dt);
                        --dbms_output.put_line('Loan Deduction4 ' ||v_ded_amt);
                        v_tot_ded      := v_tot_ded + nvl(v_ded_amt, 0);
                        v_dept_tot_ded := v_dept_tot_ded +
                                          nvl(v_ded_amt, 0);
                        v_grp_tot_ded  := v_grp_tot_ded + nvl(v_ded_amt, 0);
                      ELSIF v_sys_option11 IS NOT NULL AND
                            LTRIM(RTRIM(v_sys_option11)) =
                            LTRIM(RTRIM(m2.pay_element_id)) THEN
                        --Below IF ELSE ENDIF for Rent Deduction
                        v_ded_amt := get_loan_deduction(p_org_id  => p_org_id,
                                                        p_dept_id => m2.dept_id,
                                                        p_emp_id  => m2.pay_emp_id,
                                                        p_from_dt => v_period_from_dt,
                                                        p_to_dt   => v_period_to_dt);
                        --dbms_output.put_line('Loan Deduction4 ' ||v_ded_amt);
                        v_tot_ded      := v_tot_ded + nvl(v_ded_amt, 0);
                        v_dept_tot_ded := v_dept_tot_ded +
                                          nvl(v_ded_amt, 0);
                        v_grp_tot_ded  := v_grp_tot_ded + nvl(v_ded_amt, 0);
                      ELSIF v_sys_option3 IS NOT NULL AND
                            LTRIM(RTRIM(v_sys_option3)) =
                            LTRIM(RTRIM(m2.pay_element_id)) THEN
                        --Below MGRP EMP deduction calculation
                        /*                        IF upper(ltrim(rtrim(m2.nationality))) = 'KUWAIT' AND
                                                   m2.ssd_flag = '1' THEN
                                                  --dbms_output.put_line('MGRP Parameters3 ' ||
                                                  --                         v_sys_option3 || ' - ' ||
                                                  --                       v_sys_option3_val ||
                                                  --                     ' - ' ||
                                                  --                   m2.pay_element_id);
                                                  v_mgrp_emp_amt  := get_mgrp_amt(p_org_id => p_org_id,
                                                                                  p_emp_id => m2.pay_emp_id,
                                                                                  p_year   => v_fiscal_year);
                                                  v_basic_sal_amt := get_basic_amt(p_org_id => p_org_id,
                                                                                   p_emp_id => m2.pay_emp_id);
                                                
                                                  v_ded_amt := (v_mgrp_emp_amt + v_basic_sal_amt) *
                                                               v_sys_option3_val / 100;
                                                  --dbms_output.put_line('Employee Contribution MGRP -4 ' ||
                                                  --                        v_ded_amt);
                        */
                        begin
                          select nvl(pesd.emp_indemnity, 0) +
                                 nvl(pesd.emp_bas_supp, 0) +
                                 nvl(pesd.emp_inc_pension, 0) +
                                 nvl(pesd.emp_unempl_support, 0)
                            into v_ded_amt
                            from pay_employee_ssd_details pesd
                           where pesd.emp_id = m2.pay_emp_id
                             and pesd.org_id = p_org_id
                             and pesd.pay_period = p_pay_period;
                        exception
                          when others then
                            v_ded_amt := 0;
                        end;
                        v_tot_ded      := v_tot_ded + nvl(v_ded_amt, 0);
                        v_dept_tot_ded := v_dept_tot_ded +
                                          nvl(v_ded_amt, 0);
                        v_grp_tot_ded  := v_grp_tot_ded + nvl(v_ded_amt, 0);
                        --   END IF;
                      ELSIF v_sys_option4 IS NOT NULL AND
                            LTRIM(RTRIM(v_sys_option4)) =
                            LTRIM(RTRIM(m2.pay_element_id)) THEN
                        --Below MGRP EPR deduction calculation
                        IF upper(ltrim(rtrim(m2.nationality))) = 'KUWAIT' AND
                           m2.ssd_flag = '1' THEN
                          --dbms_output.put_line('MGRP Parameters3A ' ||
                          --                        v_sys_option4 || ' - ' ||
                          --                     v_sys_option4_val ||
                          --                     ' - ' ||
                          --                   m2.pay_element_id);
                          v_mgrp_emp_amt  := get_mgrp_amt(p_org_id => p_org_id,
                                                          p_emp_id => m2.pay_emp_id,
                                                          p_year   => v_fiscal_year);
                          v_basic_sal_amt := get_basic_amt(p_org_id => p_org_id,
                                                           p_emp_id => m2.pay_emp_id);
                        
                          v_ded_amt := (v_mgrp_emp_amt + v_basic_sal_amt) *
                                       v_sys_option4_val / 100;
                          --dbms_output.put_line('Employee Contribution MGRP -4 ' ||
                          --                     v_ded_amt);
                          v_tot_ded      := v_tot_ded + nvl(v_ded_amt, 0);
                          v_dept_tot_ded := v_dept_tot_ded +
                                            nvl(v_ded_amt, 0);
                          v_grp_tot_ded  := v_grp_tot_ded +
                                            nvl(v_ded_amt, 0);
                        END IF;
                      ELSE
                        --dbms_output.put_line(' Else Deduction4' ||
                        --                  '-Element ' ||
                        --                m2.pay_element_id);
                        v_ded_amt      := m2.pay_amount;
                        v_tot_ded      := v_tot_ded + nvl(v_ded_amt, 0);
                        v_dept_tot_ded := v_dept_tot_ded +
                                          nvl(v_ded_amt, 0);
                        v_grp_tot_ded  := v_grp_tot_ded + nvl(v_ded_amt, 0);
                      END IF;
                    END IF;
                  END IF;
                END IF;
              ELSE
                --ilam commented here
                --start
                --dbms_output.put_line('NR Table extraction here');
                IF UPPER(RTRIM(LTRIM(m2.pay_ele_class))) = 'EARNING' THEN
                  --Non-Recurring Deduction logic with no pay_period_id goes here
                  IF m2.pay_element_prorate = '1' THEN
                    --dbms_output.put_line('From NR table ' || p_org_id ||
                    --              ' - ' || p_pay_period || ' - ' ||
                    --              m2.pay_element_id || ' - ' ||
                    --              m2.dept_id || ' - ' ||
                    --              m2.pay_emp_id);
                    IF v_skip_element = '0' THEN
                      v_nr_amt := get_nr_deduction(p_org_id     => p_org_id,
                                                   p_pay_period => p_pay_period,
                                                   p_element_id => m2.pay_element_id,
                                                   p_dept_id    => m2.dept_id,
                                                   p_emp_id     => m2.pay_emp_id);
                    
                      v_pay_amt      := v_nr_amt; -- * v_multiply_factor /v_payroll_days;
                      v_tot_pay      := v_tot_pay + nvl(v_pay_amt, 0);
                      v_dept_tot_pay := v_dept_tot_pay + nvl(v_pay_amt, 0);
                      v_grp_tot_pay  := v_grp_tot_pay + nvl(v_pay_amt, 0);
                    END IF;
                  ELSE
                    --dbms_output.put_line('From NR table No prorate' ||
                    --              p_org_id || ' - ' ||
                    --             p_pay_period || ' - ' ||
                    --           m2.pay_element_id || ' - ' ||
                    --         m2.dept_id || ' - ' ||
                    --       m2.pay_emp_id);
                    IF v_skip_element = '0' THEN
                      v_nr_amt := get_nr_deduction(p_org_id     => p_org_id,
                                                   p_pay_period => p_pay_period,
                                                   p_element_id => m2.pay_element_id,
                                                   p_dept_id    => m2.dept_id,
                                                   p_emp_id     => m2.pay_emp_id);
                    
                      v_pay_amt      := v_nr_amt;
                      v_tot_pay      := v_tot_pay + nvl(v_pay_amt, 0);
                      v_dept_tot_pay := v_dept_tot_pay + nvl(v_pay_amt, 0);
                      v_grp_tot_pay  := v_grp_tot_pay + nvl(v_pay_amt, 0);
                    END IF;
                  END IF;
                ELSIF UPPER(RTRIM(LTRIM(m2.pay_ele_class))) = 'DEDUCTION' THEN
                  --Non-Recurring Deduction logic with no pay_period_id goes here
                  IF m2.pay_element_prorate = '1' THEN
                    --dbms_output.put_line('From NR table ' || p_org_id ||
                    --                     ' - ' || p_pay_period || ' - ' ||
                    --                   m2.pay_element_id || ' - ' ||
                    --                 m2.dept_id || ' - ' ||
                    --               m2.pay_emp_id);
                    IF v_skip_element = '0' THEN
                      v_ded_amt      := get_nr_deduction(p_org_id     => p_org_id,
                                                         p_pay_period => p_pay_period,
                                                         p_element_id => m2.pay_element_id,
                                                         p_dept_id    => m2.dept_id,
                                                         p_emp_id     => m2.pay_emp_id);
                      v_ded_amt      := nvl(v_ded_amt, 0) *
                                        v_multiply_factor / v_payroll_days;
                      v_tot_ded      := v_tot_ded + nvl(v_ded_amt, 0);
                      v_dept_tot_ded := v_dept_tot_ded + nvl(v_ded_amt, 0);
                      v_grp_tot_ded  := v_grp_tot_ded + nvl(v_ded_amt, 0);
                    END IF;
                  ELSE
                    --dbms_output.put_line('From NR table No prorate' ||
                    --              p_org_id || ' - ' ||
                    --            p_pay_period || ' - ' ||
                    --          m2.pay_element_id || ' - ' ||
                    --        m2.dept_id || ' - ' ||
                    --      m2.pay_emp_id);
                    IF v_skip_element = '0' THEN
                      v_ded_amt := get_nr_deduction(p_org_id     => p_org_id,
                                                    p_pay_period => p_pay_period,
                                                    p_element_id => m2.pay_element_id,
                                                    p_dept_id    => m2.dept_id,
                                                    p_emp_id     => m2.pay_emp_id);
                    
                      v_tot_ded      := v_tot_ded + nvl(v_ded_amt, 0);
                      v_dept_tot_ded := v_dept_tot_ded + nvl(v_ded_amt, 0);
                      v_grp_tot_ded  := v_grp_tot_ded + nvl(v_ded_amt, 0);
                    END IF;
                  END IF;
                END IF;
                --end
              END IF;
              --End Non-Recurring
            END IF;
            ----dbms_output.put_line('For Employee '||m2.pay_emp_id||' -Payment '||v_tot_pay||' -Deduction '|| v_tot_ded);
            IF v_tot_ded > (0.1 * v_basic_sal_amt) THEN
              v_remarks := ssm.get_err_message('EN', 'ERR_103');
            END IF;
            --/*
            --Emp start
            if nvl(v_pay_amt, 0) <= 0 then
              v_pay_amt := 0;
            end if;
            if nvl(v_ded_amt, 0) <= 0 then
              v_ded_amt := 0;
            end if;
            --natesh  
            /*  if m2.pay_element_id='PYE_ID-0000000030' and m2.pay_emp_id='EMP_ID-0000003186' and p_pay_period='PP_ID-0000001016' then
                v_pay_amt:=4326.923;
                elsif m2.pay_element_id='PYE_ID-0000000030' and m2.pay_emp_id in ('EMP_ID-0000003471','EMP_ID-0000003494') and p_pay_period='PP_ID-0000001016' then
                  v_pay_amt:=0;
              end if;
            */
            BEGIN
            
              INSERT INTO TMP_PAYROLL
                (PAY_PERIOD_ID,
                 PAY_DAYSHRS,
                 PAY_STATUS,
                 PAY_GROUP_ID,
                 PAY_TOTAL_PAY_AMOUNT,
                 PAY_TOTAL_DED_AMOUNT,
                 PAY_DEPT_ID,
                 PAY_DEPT_PAY_AMT,
                 PAY_DEPT_DED_AMT,
                 PAY_EMP_ELEMENT_ID,
                 PAY_EMP_ID,
                 PAY_AMOUNT,
                 IS_LAST_REC,
                 PROCESSED_FLAG,
                 PAY_REMARKS,
                 TMP_EXCEPTION,
                 NO_FRIDAYS,
                 NO_WORKING_DAYS,
                 NO_HOLIDAYS,
                 NO_ANNUAL_LEAVE)
              VALUES
                (p_pay_period, --'PP_ID-0000000009',
                 0,
                 'APPROVED',
                 m2.pay_group_id,
                 0,
                 0,
                 m2.dept_id, --m1.pay_emp_dept_id,
                 round(v_dept_tot_pay, 3),
                 round(v_dept_tot_ded, 3),
                 m2.pay_element_id,
                 m2.pay_emp_id,
                 decode(UPPER(RTRIM(LTRIM(m2.pay_ele_class))),
                        'EARNING',
                        nvl(round(v_pay_amt,3), 0),
                        nvl(round(v_ded_amt,3), 0)), --m2.pay_amount,
                 '0',
                 '0',
                 'Employee Details',
                 v_remarks,
                 v_no_fridays,
                 v_multiply_factor,
                 nvl(v_AL_TOT_Days, 0),
                 v_al_days);
            
            EXCEPTION
              WHEN OTHERS THEN
                DBMS_OUTPUT.PUT_LINE('ERR Emp' || SQLERRM);
            END;
            --*/
          ELSIF v_ignore_emp = '1' THEN
            dbms_output.put_line('Logic for Ignore the employee goes here');
            /*
            IF UPPER(RTRIM(LTRIM(m2.pay_ele_class)))='EARNING' THEN
                -- debug_proc('Earning ='||'Department '||m1.pay_emp_dept_id||'-'||upper(m2.pay_ele_class)||'-'||v_dept_tot_pay||' For '||m2.pay_amount);
            
                   IF m2.pay_element_prorate='1' THEN
                       IF v_skip_element='0' THEN
                       --lop days to be calculated here and substract it from v_multiply_factor
                       v_pay_amt:=m2.pay_amount*v_multiply_factor/v_payroll_days;
                       --dbms_output.put_line('Pro-rate' ||'-Element '||m2.pay_element_id||' - '||v_pay_amt);
                       v_tot_pay:=v_tot_pay+v_pay_amt;
                       v_dept_tot_pay:=v_dept_tot_pay+v_pay_amt;
                       v_grp_tot_pay:=v_grp_tot_pay+v_pay_amt;
                    END IF;
                     ELSE
                     --v_pay_amt:=m2.pay_amount*v_multiply_factor;
                     IF v_skip_element='0' THEN
                       --lop days to be calculated here and substract it from v_multiply_factor
                       v_pay_amt:=m2.pay_amount;
                       --dbms_output.put_line('No Pro-rate' ||'-Element '||m2.pay_element_id||' - '||v_pay_amt);
                       v_tot_pay:=v_tot_pay+v_pay_amt;
                       v_dept_tot_pay:=v_dept_tot_pay+v_pay_amt;
                       v_grp_tot_pay:=v_grp_tot_pay+v_pay_amt;
                     END IF;
                    END IF;
                ELSIF UPPER(RTRIM(LTRIM(m2.pay_ele_class)))='DEDUCTION' THEN
               --  debug_proc('Ded ='||'Department '||m1.pay_emp_dept_id||'-'||upper(m2.pay_ele_class)||'-'||v_dept_tot_ded||' For '||m2.pay_amount);
                  v_ded_amt:=m2.pay_amount*v_multiply_factor/v_payroll_days;
                v_tot_ded:=v_tot_ded+v_ded_amt;
                v_dept_tot_ded:=v_dept_tot_ded+v_ded_amt;
                v_grp_tot_ded:=v_grp_tot_ded+v_ded_amt;
                END IF;
            
                --Emp start
               BEGIN
                 INSERT INTO TMP_PAYROLL(PAY_PERIOD_ID,
                             PAY_DAYSHRS,
                             PAY_STATUS,
                             PAY_GROUP_ID,
                             PAY_TOTAL_PAY_AMOUNT,
                             PAY_TOTAL_DED_AMOUNT,
                             PAY_DEPT_ID,
                             PAY_DEPT_PAY_AMT,
                             PAY_DEPT_DED_AMT,
                             PAY_EMP_ELEMENT_ID,
                             PAY_EMP_ID,
                             PAY_AMOUNT,
                             IS_LAST_REC,
                             PROCESSED_FLAG,
                             PAY_REMARKS)
                        VALUES(p_pay_period,--'PP_ID-0000000009',
                               0,
                             'APPROVED',
                             m2.pay_group_id,
                             0,
                             0,
                             v_dept_id,--m1.pay_emp_dept_id,
                             v_dept_tot_pay,
                             v_dept_tot_ded,
                             m2.pay_element_id,
                             m2.pay_emp_id,
                             decode(UPPER(RTRIM(LTRIM(m2.pay_ele_class))),'EARNING',v_pay_amt,v_ded_amt),--m2.pay_amount,
                             '0',
                             '0',
                             'Employee Details');
              EXCEPTION
                 WHEN OTHERS THEN
                   DBMS_OUTPUT.PUT_LINE('ERR Emp'||SQLERRM);
              END;
              --*/
          END IF;
        END IF;
        --END SKIP EMP
      --dbms_output.put_line('v_tot_pay  ' || v_tot_pay ||
      --' - v_tot_ded ' || v_tot_ded);
      --Emp end
      
      END LOOP;
    
      /*for dept_id in (select * from hr_departments) loop        --\*
              --Dept start
              BEGIN
                INSERT INTO TMP_PAYROLL
                  (PAY_PERIOD_ID,
                   PAY_DAYSHRS,
                   PAY_STATUS,
                   PAY_GROUP_ID,
                   PAY_TOTAL_PAY_AMOUNT,
                   PAY_TOTAL_DED_AMOUNT,
                   PAY_DEPT_ID,
                   PAY_DEPT_PAY_AMT,
                   PAY_DEPT_DED_AMT,
                   PAY_EMP_ELEMENT_ID,
                   PAY_EMP_ID,
                   PAY_AMOUNT,
                   IS_LAST_REC,
                   PROCESSED_FLAG,
                   PAY_REMARKS,
                   NO_FRIDAYS,
                   NO_WORKING_DAYS,
                   NO_HOLIDAYS)
                VALUES
                  (p_pay_period, --'PP_ID-0000000009',
                   0,
                   'APPROVED',
                   g_cur.PAY_GROUP_ID,
                   0,
                   0,
                   dept_id.dept_id, --m1.pay_emp_dept_id,
                   round(nvl(0,0), 5),
                   round(nvl(0,0), 5),
                   NULL,
                   NULL,
                   NULL,
                   '0',
                   '0',
                   'Department Details',
                   v_no_fridays,
                   nvl(v_payroll_days, 0) - nvl(v_lop_days, 0),
                   nvl(v_AL_TOT_Days, 0));
              EXCEPTION
                WHEN OTHERS THEN
                  DBMS_OUTPUT.PUT_LINE('ERR DEpt' || SQLERRM);
              END;
              
              end loop;
      */
      --Dept end
      --*/
      --DBMS_OUTPUT.PUT_LINE('Department Details ' || v_dept_id ||' Group ID ' || g_cur.PAY_GROUP_ID ||'=> Payment ' || v_dept_tot_pay ||' -Deduction ' || v_dept_tot_ded);
      --debug_proc('Department Details '||v_dept_id||' Group ID '||g_cur.PAY_GROUP_ID||'=> Payment '||v_dept_tot_pay||' -Deduction '||v_dept_tot_ded);
      v_dept_tot_pay := 0;
      v_dept_tot_ded := 0;
      --      END LOOP;
      DBMS_OUTPUT.PUT_LINE('Group Details ' || g_cur.PAY_GROUP_ID || '-' ||
                           g_cur.PAY_GROUP_CODE || ' - ' ||
                           g_cur.PAY_GROUP_DESC || ' -Payment ' ||
                           v_grp_tot_pay || ' -Deduction ' ||
                           v_grp_tot_ded);
      --debug_proc('Group Details '||g_cur.PAY_GROUP_ID||'-'||g_cur.PAY_GROUP_CODE||' - '||g_cur.PAY_GROUP_DESC||' -Payment '||v_grp_tot_pay||' -Deduction '||v_grp_tot_ded);
      --/*
      --Group start
      /*      BEGIN
              INSERT INTO TMP_PAYROLL
                (PAY_PERIOD_ID,
                 PAY_DAYSHRS,
                 PAY_STATUS,
                 PAY_GROUP_ID,
                 PAY_TOTAL_PAY_AMOUNT,
                 PAY_TOTAL_DED_AMOUNT,
                 PAY_DEPT_ID,
                 PAY_DEPT_PAY_AMT,
                 PAY_DEPT_DED_AMT,
                 PAY_EMP_ELEMENT_ID,
                 PAY_EMP_ID,
                 PAY_AMOUNT,
                 IS_LAST_REC,
                 PROCESSED_FLAG,
                 PAY_REMARKS,
                 NO_FRIDAYS,
                 NO_WORKING_DAYS,
                 NO_HOLIDAYS)
              VALUES
                (p_pay_period, --'PP_ID-0000000009',
                 0,
                 'APPROVED',
                 g_cur.PAY_GROUP_ID,
                 round(nvl(0,0), 5),
                 round(nvl(0,0), 5),
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 NULL,
                 '1',
                 '0',
                 'Group Details',
                 v_no_fridays,
                 nvl(v_payroll_days, 0) - nvl(v_lop_days, 0),
                 nvl(v_AL_TOT_Days, 0));
            EXCEPTION
              WHEN OTHERS THEN
                DBMS_OUTPUT.PUT_LINE('ERR GRP' || SQLERRM);
            END;
      */ --Group end
      --*/
      v_grp_tot_pay := 0;
      v_grp_tot_ded := 0;
    END LOOP;
    commit;
    DBMS_OUTPUT.PUT_LINE('Total Payment ' || v_tot_pay || ' -Deduction ' ||
                         v_tot_ded);
    v_tot_pay := 0;
    v_tot_ded := 0;
    --  CLOSE v_dept_cur;
    --Check payroll run already for this period and for this group
    BEGIN
      SELECT 'Y', PAYROLL_ID
        INTO v_exists_flag, v_payroll_id
        FROM PAY_TRIAL_RUN_HDR
       WHERE PAYROLL_PERIOD = p_pay_period
         AND PAY_GROUP_ID = p_pay_group;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        v_exists_flag := 'N';
        v_payroll_id  := NULL;
    END;
  
    --/*
    IF v_exists_flag = 'N' THEN
    
      FOR t_m1 IN pay_cur1 LOOP
        v_hdr_seq := SSM.get_next_sequence('PER_012', 'EN');
        BEGIN
          INSERT INTO PAY_TRIAL_RUN_HDR
            (PK_ID,
             CHILD_ID,
             PAYROLL_ID,
             PAYROLL_PERIOD,
             PAYROLL_DAYS_HRS,
             PAYROLL_STATUS,
             PAYROLL_DATE,
             PAYROLL_TOTAL_PAYMENT,
             PAYROLL_TOTAL_DEDUCTION,
             PAYROLL_REMARKS,
             PAY_GROUP_ID,
             ENABLED_FLAG,
             WORKFLOW_COMPLETION_STATUS,
             CREATED_BY,
             CREATED_DATE)
          VALUES
            (1,
             1,
             v_hdr_seq,
             t_m1.PAY_PERIOD_ID,
             0,
             'PENDING',
             SYSDATE,
             round(t_m1.PAY_TOTAL_PAY_AMOUNT, 3),
             round(t_m1.PAY_TOTAL_DED_AMOUNT, 3),
             'GROUP',
             t_m1.PAY_GROUP_ID,
             '1',
             '0',
             'ADMIN',
             SYSDATE);
        EXCEPTION
          WHEN OTHERS THEN
            DBMS_OUTPUT.PUT_LINE('ERHDR1' || SQLERRM);
        END;
      
        FOR t_m2 IN pay_cur2(t_m1.PAY_GROUP_ID) LOOP
          v_dtl_seq := SSM.get_next_sequence('PER_013', 'EN');
          BEGIN
            INSERT INTO PAY_TRIAL_RUN_DTL
              (PK_ID,
               CHILD_ID,
               PAYROLL_DTL_ID,
               PAYROLL_ID,
               PAY_AMOUNT,
               PAY_DED_AMOUNT,
               PAY_DEPT_ID,
               ENABLED_FLAG,
               WORKFLOW_COMPLETION_STATUS,
               CREATED_BY,
               CREATED_DATE)
            VALUES
              (1,
               1,
               v_dtl_seq,
               v_hdr_seq,
               round(t_m2.PAY_DEPT_PAY_AMT, 3),
               round(t_m2.PAY_DEPT_DED_AMT, 3),
               t_m2.PAY_DEPT_ID,
               '1',
               '1',
               'ADMIN',
               SYSDATE);
          EXCEPTION
            WHEN OTHERS THEN
              DBMS_OUTPUT.PUT_LINE('ERDTL1' || SQLERRM);
          END;
        
          FOR t_m3 IN pay_cur3(t_m2.PAY_DEPT_ID) LOOP
            v_dtl_dtl_seq := SSM.get_next_sequence('PER_014', 'EN');
            BEGIN
              INSERT INTO PAY_TRIAL_RUN_DTL_DTL
                (PK_ID,
                 CHILD_ID,
                 PAYROLL_DTL_DTL_ID,
                 PAYROLL_DTL_ID,
                 PAY_EMP_ELEMENT_ID,
                 PAY_AMOUNT,
                 PAY_EMP_ID,
                 ENABLED_FLAG,
                 WORKFLOW_COMPLETION_STATUS,
                 CREATED_BY,
                 CREATED_DATE,
                 ATTRIBUTE1,
                 PAY_EMP_NO_DAYS,
                 PAY_EMP_AL_DAYS)
              VALUES
                (1,
                 1,
                 v_dtl_dtl_seq,
                 v_dtl_seq,
                 t_m3.PAY_EMP_ELEMENT_ID,
                 round(t_m3.PAY_AMOUNT, 3),
                 t_m3.PAY_EMP_ID,
                 '1',
                 '1',
                 'ADMIN',
                 SYSDATE,
                 t_m3.TMP_EXCEPTION,
                 t_m3.no_working_days,
                 t_m3.no_annual_leave);
            EXCEPTION
              WHEN OTHERS THEN
                DBMS_OUTPUT.PUT_LINE('ERDTL2' || SQLERRM);
            END;
          END LOOP;
          v_dtl_seq := NULL;
        END LOOP;
        v_hdr_seq := NULL;
      END LOOP;
    ELSE
      v_grp_tot_pay   := 0;
      v_grp_tot_ded   := 0;
      v_exist_payroll := 'SELECT PAYROLL_DTL_ID,PAY_DEPT_ID FROM  PAY_TRIAL_RUN_DTL WHERE PAYROLL_ID=' || '''' ||
                         v_payroll_id || '''' || ' AND PAY_DEPT_ID IN (' ||
                         get_csv_to_list(p_dept_list) || ' )';
      --dbms_output.put_line('v_exist_payroll ' || v_exist_payroll);
      OPEN v_dept_cur FOR v_exist_payroll;
    
      LOOP
        FETCH v_dept_cur
          INTO v_tab_payroll_dtl_id(v_idx).tab_v_payroll_dtl_id,
               v_tab_payroll_dtl_id(v_idx).tab_v_dept_id;
        v_idx := v_idx + 1;
        EXIT WHEN v_dept_cur%NOTFOUND;
      END LOOP;
      --dbms_output.put_line(' v_tab_payroll_dtl_id.COUNT ' ||
      --    v_tab_payroll_dtl_id.COUNT);
      FOR i IN 1 .. v_tab_payroll_dtl_id.COUNT LOOP
        --dbms_output.put_line('Payroll dtl id ' || v_tab_payroll_dtl_id(i)
        --.tab_v_payroll_dtl_id);
        DELETE PAY_TRIAL_RUN_DTL_DTL
         WHERE PAYROLL_DTL_ID = v_tab_payroll_dtl_id(i)
              .tab_v_payroll_dtl_id;
      END LOOP;
    
      v_exist_payroll := 'DELETE PAY_TRIAL_RUN_DTL WHERE PAYROLL_ID=' || '''' ||
                         v_payroll_id || '''' || ' AND PAY_DEPT_ID IN (' ||
                         get_csv_to_list(p_dept_list) || ' )';
      --dbms_output.put_line('v_exist_payroll -1' || v_exist_payroll);
      EXECUTE IMMEDIATE v_exist_payroll;
      --start
      FOR t_m2 IN pay_cur2(p_pay_group) LOOP
        v_dtl_seq := SSM.get_next_sequence('PER_013', 'EN');
        BEGIN
          v_grp_tot_pay := v_grp_tot_pay + t_m2.PAY_DEPT_PAY_AMT;
          v_grp_tot_ded := v_grp_tot_ded + t_m2.PAY_DEPT_DED_AMT;
        
          INSERT INTO PAY_TRIAL_RUN_DTL
            (PK_ID,
             CHILD_ID,
             PAYROLL_DTL_ID,
             PAYROLL_ID,
             PAY_AMOUNT,
             PAY_DED_AMOUNT,
             PAY_DEPT_ID,
             ENABLED_FLAG,
             WORKFLOW_COMPLETION_STATUS,
             CREATED_BY,
             CREATED_DATE)
          VALUES
            (1,
             1,
             v_dtl_seq,
             v_payroll_id,
             round(t_m2.PAY_DEPT_PAY_AMT, 3),
             round(t_m2.PAY_DEPT_DED_AMT, 3),
             t_m2.PAY_DEPT_ID,
             '1',
             '0',
             'ADMIN',
             SYSDATE);
        EXCEPTION
          WHEN OTHERS THEN
            DBMS_OUTPUT.PUT_LINE('ERDTL1' || SQLERRM);
        END;
      
        FOR i IN 1 .. v_tab_payroll_dtl_id.COUNT LOOP
          --dbms_output.put_line('Detail detail dept id ' || v_tab_payroll_dtl_id(i)
          --.tab_v_dept_id);
          FOR t_m3 IN pay_cur3(v_tab_payroll_dtl_id(i).tab_v_dept_id) LOOP
            v_dtl_dtl_seq := SSM.get_next_sequence('PER_014', 'EN');
            BEGIN
              INSERT INTO PAY_TRIAL_RUN_DTL_DTL
                (PK_ID,
                 CHILD_ID,
                 PAYROLL_DTL_DTL_ID,
                 PAYROLL_DTL_ID,
                 PAY_EMP_ELEMENT_ID,
                 PAY_AMOUNT,
                 PAY_EMP_ID,
                 ENABLED_FLAG,
                 WORKFLOW_COMPLETION_STATUS,
                 CREATED_BY,
                 CREATED_DATE,
                 ATTRIBUTE1,
                 PAY_EMP_NO_DAYS,
                 PAY_EMP_AL_DAYS)
              VALUES
                (1,
                 1,
                 v_dtl_dtl_seq,
                 v_dtl_seq,
                 t_m3.PAY_EMP_ELEMENT_ID,
                 round(t_m3.PAY_AMOUNT, 3),
                 t_m3.PAY_EMP_ID,
                 '1',
                 '1',
                 'ADMIN',
                 SYSDATE,
                 t_m3.TMP_EXCEPTION,
                 t_m3.no_working_days,
                 t_m3.no_holidays);
            EXCEPTION
              WHEN OTHERS THEN
                DBMS_OUTPUT.PUT_LINE('ERDTL2' || SQLERRM);
            END;
          END LOOP;
          v_dtl_seq := NULL;
        END LOOP;
      END LOOP;
      v_hdr_seq := NULL;
    
      UPDATE PAY_TRIAL_RUN_HDR
         SET PAYROLL_TOTAL_PAYMENT   = v_grp_tot_pay,
             PAYROLL_TOTAL_DEDUCTION = v_grp_tot_ded
       WHERE PAYROLL_ID = v_payroll_id;
      COMMIT;
      --end
    END IF;
    --*/
  
    payroll_report_trial(p_org_id, p_pay_period);
  
  Exception
    When others then
      dbms_output.put_line('Final exception ' || sqlerrm);
  END payroll_process;

  PROCEDURE payroll_finalrun_process(p_org_id     IN VARCHAR2,
                                     p_payroll_id IN VARCHAR2) IS
    v_payroll_period varchar2(50);
    Cursor c_hdr1 is
      select *
        from pay_trial_run_hdr
       where enabled_flag = '1'
         and workflow_completion_status = '1'
         and upper(payroll_status) = 'APPROVED'
         and payroll_id = p_payroll_id;
  
    Cursor c_dtl1(t_hdr_id VARCHAR2) IS
      select * from pay_trial_run_dtl where payroll_id = t_hdr_id;
  
    Cursor c_dtl2(t_hdr_id VARCHAR2) IS
      select * from pay_trial_run_dtl_dtl where payroll_dtl_id = t_hdr_id;
  
    Cursor c_dtl3(t_hdr_id VARCHAR2) IS
      select distinct pay_emp_id pay_emp_id
        from pay_trial_run_dtl_dtl
       where payroll_dtl_id = t_hdr_id;
  
    v_hdr_seq     VARCHAR2(50);
    v_last_pay_dt DATE;
    v_sys_option1 SSM_SYSTEM_OPTIONS.HR_PAY_ELEMENT_CODE%TYPE;
  
    TYPE t_emp IS TABLE OF VARCHAR2(50) INDEX BY BINARY_INTEGER;
  
    emp_list  t_emp;
    l_counter NUMBER := 1;
  BEGIN
  
    BEGIN
      SELECT X.HR_PAY_ELEMENT_CODE
        INTO v_sys_option1
        FROM SSM_SYSTEM_OPTIONS X
       WHERE X.MODULE_CODE = 'HR'
         AND X.HR_EFFECTIVE_TO_DATE IS NULL;
    EXCEPTION
      WHEN OTHERS THEN
        ----dbms_output.put_line('Err sys '||sqlerrm);
        v_sys_option1 := NULL;
    END;
    for m in c_hdr1 Loop
      --dbms_output.put_line('Header1 ' || m.payroll_id);
      Begin
        SELECT PAY_TO_DT
          INTO v_last_pay_dt
          FROM PAY_PERIODS
         WHERE PAY_PERIOD_ID = m.payroll_period;
      Exception
        when others then
          v_last_pay_dt := NULL;
      END;
    
      Begin
      
        v_payroll_period := m.payroll_period;
      
        --Final Run Header Insert
        --v_hdr_seq:=ssm.get_next_sequence('PER_015','EN');
        INSERT INTO PAY_FINAL_RUN_HDR
          (PAYROLL_ID,
           PAYROLL_PERIOD,
           PAYROLL_DAYS_HRS,
           PAYROLL_STATUS,
           PAY_GROUP_ID,
           PAYROLL_DATE,
           PAYROLL_TOTAL_PAYMENT,
           PAYROLL_TOTAL_DEDUCTION,
           PAYROLL_REMARKS,
           ENABLED_FLAG,
           WORKFLOW_COMPLETION_STATUS,
           CREATED_BY,
           CREATED_DATE)
        VALUES
          (m.payroll_id,
           m.payroll_period,
           m.payroll_days_hrs,
           m.payroll_status,
           m.pay_group_id,
           sysdate,
           round(m.payroll_total_payment, 5),
           round(m.payroll_total_deduction, 5),
           'Final run ' || to_char(sysdate),
           '1',
           '1',
           'ADMIN',
           sysdate);
      Exception
        When others then
          dbms_output.put_line('Err1 ' || sqlerrm);
      End;
    
      for n in c_dtl1(m.payroll_id) loop
        --dbms_output.put_line('Detail 1' || n.payroll_dtl_id || ' - ' ||
        --  m.payroll_id);
        --Final Run Detail Insert
        Begin
          INSERT INTO PAY_FINAL_RUN_DTL
            (PAYROLL_DTL_ID,
             PAYROLL_ID,
             PAY_DEPT_ID,
             PAY_AMOUNT,
             PAY_DED_AMOUNT,
             ENABLED_FLAG,
             WORKFLOW_COMPLETION_STATUS,
             CREATED_BY,
             CREATED_DATE)
          VALUES
            (n.payroll_dtl_id,
             m.payroll_id,
             n.pay_dept_id,
             round(n.pay_amount, 5),
             round(n.pay_ded_amount, 5),
             '1',
             '1',
             'ADMIN',
             sysdate);
        Exception
          When others then
            dbms_output.put_line('Err2 ' || sqlerrm);
        End;
        for o in c_dtl2(n.payroll_dtl_id) loop
          --dbms_output.put_line('Detail 2' || o.payroll_dtl_dtl_id ||
          -- n.payroll_dtl_id || ' - ' || m.payroll_id);
          --Final Run Detail Detail Insert
          Begin
            INSERT INTO PAY_FINAL_RUN_DTL_DTL
              (PAYROLL_DTL_DTL_ID,
               PAYROLL_DTL_ID,
               PAY_EMP_ELEMENT_ID,
               PAY_EMP_ID,
               PAY_AMOUNT,
               ENABLED_FLAG,
               WORKFLOW_COMPLETION_STATUS,
               CREATED_BY,
               CREATED_DATE,
               PAY_EMP_NO_DAYS,
               PAY_EMP_AL_DAYS)
            VALUES
              (o.payroll_dtl_dtl_id,
               n.payroll_dtl_id,
               o.pay_emp_element_id,
               o.pay_emp_id,
               round(o.pay_amount, 5),
               '1',
               '1',
               'ADMIN',
               sysdate,
               o.pay_emp_no_days,
               o.pay_emp_al_days);
          Exception
            When others then
              dbms_output.put_line('Err3 ' || sqlerrm);
          End;
        
          UPDATE PAY_EMP_ELEMENT_VALUE
             SET PAY_LAST_PAY_DT = v_last_pay_dt
           WHERE PAY_EMP_ID = o.pay_emp_id
             AND PAY_ELEMENT_ID = o.pay_emp_element_id
             AND PAY_EMP_DEPT_ID = n.pay_dept_id
             AND PAY_GROUP_ID = m.pay_group_id
             AND ENABLED_FLAG = '1'
             AND WORKFLOW_COMPLETION_STATUS = '1'
             AND EFFECTIVE_TO_DT IS NULL;
        
          UPDATE HR_EMP_WORK_DTLS
             SET EMP_LAST_SALARY_DT = v_last_pay_dt
           WHERE EMP_DEPT_ID = n.pay_dept_id
             AND EMP_ID = o.pay_emp_id;
        
          IF v_sys_option1 IS NOT NULL AND
             LTRIM(RTRIM(v_sys_option1)) =
             LTRIM(RTRIM(o.pay_emp_element_id)) THEN
            loan_deduction_update_proc(p_org_id => p_org_id,
                                       p_emp_id => o.pay_emp_id,
                                       p_date   => v_last_pay_dt,
                                       p_amt    => o.pay_amount);
          END IF;
        
        end loop;
        for s in c_dtl3(n.payroll_dtl_id) loop
          emp_list(l_counter) := s.pay_emp_id;
          l_counter := l_counter + 1;
        end loop;
      end loop;
      FOR qt in 1 .. emp_list.count LOOP
        BEGIN
          --
          INSERT INTO PAY_EMP_PERIODS
            (EMP_PERIOD_ID,
             PAY_EMP_ID,
             PAY_PERIOD_ID,
             ENABLED_FLAG,
             WORKFLOW_COMPLETION_STATUS,
             CREATED_BY,
             CREATED_DATE)
          VALUES
            (SSM.get_next_sequence('PR_021_PP', 'EN'),
             emp_list(qt),
             m.payroll_period,
             '1',
             '1',
             user,
             sysdate);
        EXCEPTION
          WHEN OTHERS THEN
            dbms_output.put_line('Error pay_emp_period ' || sqlerrm);
        END;
      END LOOP;
    end loop;
    Commit;
  
    delete from pay_final_run_emp_dtl where payroll_id = p_payroll_id;
  
    insert into pay_final_run_emp_dtl
      select *
        from pay_trial_run_emp_dtl ptred
       where ptred.payroll_id = p_payroll_id;
    commit;
  
    /*    select ptrh.payroll_period
        into v_payroll_period
        from pay_trial_run_hdr ptrh
       where ptrh.payroll_id = p_payroll_id;
    */
    -- payroll_report_final(p_org_id, v_payroll_period);
  
  END payroll_finalrun_process;

  FUNCTION leave_salary_old(p_pay_period IN VARCHAR2,
                            p_dept_id    IN VARCHAR2,
                            p_emp_id     IN VARCHAR2 DEFAULT NULL,
                            p_org_id     IN VARCHAR2,
                            p_from_dt    IN DATE,
                            p_to_dt      IN DATE) RETURN NUMBER IS
  
    CURSOR pay_cur(c_dept_id IN VARCHAR2, c_emp_id IN VARCHAR2) IS
      SELECT
      --grp1.pg_ele_id,
      --grp1.pay_group_id,
      --grp1.pay_element_id,
       emp.emp_doj,
       ele.pay_element_id,
       ele.pay_element_code,
       ele.pay_element_desc,
       ele.pay_ele_class,
       NVL(ele.pay_ele_recurring, '1') pay_ele_recurring,
       NVL(ele.pay_element_prorate, '1') pay_element_prorate,
       NVL(eval.pay_element_prorate, '1') emp_prorate,
       eval.pay_emp_element_id,
       eval.pay_emp_id,
       NVL(ele.unpaid_leave, '1') ele_unpaid_leave,
       NVL(eval.unpaid_leave, '1') eval_unpaid_leave,
       --eval.pay_element_id,
       eval.pay_amount,
       eval.pay_emp_dept_id,
       eval.pay_period_id
        FROM --pay_group_element_mapping grp1,
             pay_elements          ele,
             pay_emp_element_value eval,
             hr_employees          emp
       WHERE eval.pay_element_id = ele.pay_element_id
         AND emp.emp_id = eval.pay_emp_id
         AND eval.pay_emp_dept_id = c_dept_id
         AND eval.pay_emp_id = NVL(c_emp_id, eval.pay_emp_id)
         AND ele.paid_for_annual_leave = '1'
         AND eval.enabled_flag = '1'
       ORDER BY eval.pay_emp_element_id;
  
    CURSOR leave_cur(c_fiscal_year VARCHAR2,
                     c_org_id      VARCHAR2,
                     c_staff_id    VARCHAR2,
                     c_date        DATE) IS
      SELECT distinct c.leave_id leave_id, B.ATTRIBUTE1
        FROM HR_STAFF_ATTENDANCE   a,
             HR_LEAVE_APPLICATIONS b,
             HR_LEAVE_DEFINITIONS  c
       WHERE a.LEAVE_REQ_ID = B.LEAVE_REQ_ID
         AND B.LEAVE_ID = C.LEAVE_ID
         AND C.WITH_PAY_YN = '1'
            --   AND UPPER(LTRIM(RTRIM(a.ATTENDANCE_TYPE))) IN ('ABSENT', 'LEAVE')
         AND a.ENABLED_FLAG = '1'
         AND a.ORG_ID = c_org_id
         AND a.FISCAL_YEAR = c_fiscal_year
         AND a.STAFF_ID = c_staff_id
         AND a.ATTENDANCE_DATE <= c_date; --'31-DEC-14';
  
    v_tot_pay         NUMBER := 0;
    v_tot_ded         NUMBER := 0;
    v_dept_tot_pay    NUMBER := 0;
    v_dept_tot_ded    NUMBER := 0;
    v_grp_tot_pay     NUMBER := 0;
    v_grp_tot_ded     NUMBER := 0;
    v_final_amt       NUMBER := 0;
    v_loan_ded        NUMBER := 0;
    v_dept_id         VARCHAR2(50);
    v_hdr_seq         VARCHAR2(50);
    v_dtl_seq         VARCHAR2(50);
    v_dtl_dtl_seq     VARCHAR2(50);
    v_period_from_dt  DATE;
    v_period_to_dt    DATE;
    v_payroll_days    NUMBER;
    v_holidays        NUMBER := 0;
    v_multiply_factor NUMBER := 1;
    v_actual_days     NUMBER := 0;
    v_pay_amt         NUMBER := 0;
    v_ded_amt         NUMBER := 0;
    v_basic_sal_amt   NUMBER := 0;
    --v_mgrp_emp_ded NUMBER:=0;
  
    v_ignore_emp   CHAR(1) := '0';
    v_all_elements CHAR(1);
    v_skip_element CHAR(1) := '0';
    v_stop_resume  CHAR(1);
    v_resume_flag  CHAR(1);
    v_mid_month    CHAR(1) := '0';
    v_stop_dt      DATE;
    v_resume_dt    DATE;
    v_leave_bal    NUMBER := 0;
    v_err_flag     CHAR(1);
    v_query_str    VARCHAR2(5000);
  
    v_exists_flag CHAR(1) := 'N';
    v_payroll_id  PAY_TRIAL_RUN_HDR.PAYROLL_ID%TYPE;
    v_fiscal_year GL_ACCT_CALENDAR_HDR.CAL_ID%TYPE;
  
    v_sys_option1     SSM_SYSTEM_OPTIONS.HR_PAY_ELEMENT_CODE%TYPE;
    v_sys_option2     SSM_SYSTEM_OPTIONS.HR_BASIC_ELEMENT_CODE%TYPE;
    v_sys_option3     SSM_SYSTEM_OPTIONS.HR_MGRP_EMP_ELEMENT_CODE%TYPE;
    v_sys_option4     SSM_SYSTEM_OPTIONS.HR_MGRP_COMP_ELEMENT_CODE%TYPE;
    v_sys_option3_val SSM_SYSTEM_OPTIONS.SSDP_EMPLOYEE%TYPE;
    v_sys_option4_val SSM_SYSTEM_OPTIONS.SSDP_EMPLOYER%TYPE;
    v_mgrp_emp_amt    NUMBER := 0;
  
    v_exist_payroll VARCHAR2(5000);
    v_lop_factor    NUMBER := 1;
    tmp_lop_days    NUMBER := 0;
    v_lop_days      NUMBER := 0;
    v_idx           NUMBER(10) := 1;
    v_skip_emp      CHAR(1) := '0';
    tmp_dt          DATE;
    v_fixed_days    number := 0;
  BEGIN
    DBMS_OUTPUT.PUT_LINE('START OF THE PROGRAM');
  
    -- v_payroll_days:= get_days(p_from_dt =>v_period_from_dt,p_to_dt=>v_period_to_dt)+1;
    --dbms_output.put_line('Payroll Days '||v_payroll_days);
    --System option1 for Loan Deduction Element
    BEGIN
      SELECT X.HR_PAY_ELEMENT_CODE,
             X.HR_BASIC_ELEMENT_CODE,
             X.HR_MGRP_EMP_ELEMENT_CODE,
             X.HR_MGRP_COMP_ELEMENT_CODE,
             X.SSDP_EMPLOYEE,
             X.SSDP_EMPLOYER
        INTO v_sys_option1,
             v_sys_option2,
             v_sys_option3,
             v_sys_option4,
             v_sys_option3_val,
             v_sys_option4_val
        FROM SSM_SYSTEM_OPTIONS X
       WHERE X.MODULE_CODE = 'HR'
         AND X.HR_EFFECTIVE_TO_DATE IS NULL;
    EXCEPTION
      WHEN OTHERS THEN
        --dbms_output.put_line('Err sys '||sqlerrm);
        v_sys_option1     := NULL;
        v_sys_option2     := NULL;
        v_sys_option3     := NULL;
        v_sys_option4     := NULL;
        v_sys_option3_val := NULL;
        v_sys_option4_val := NULL;
    END;
    dbms_output.put_line('System options ' || v_sys_option1 || ' - ' ||
                         v_sys_option2 || ' - ' || v_sys_option3 || ' - ' ||
                         v_sys_option4 || ' - ' || v_sys_option3_val ||
                         ' - ' || v_sys_option4_val);
    dbms_output.put_line('0');
    v_period_from_dt := p_from_dt;
    v_period_to_dt   := p_to_dt;
    dbms_output.put_line('Dates ' || v_period_from_dt || ' - ' ||
                         v_period_to_dt);
  
    --v_fiscal_year := pkg_payroll.get_fiscal_year(p_org_id,to_char(v_period_from_dt,'YYYY'));
    dbms_output.put_line('1');
    v_fiscal_year := pkg_payroll.get_fiscal_year(p_org_id, v_period_from_dt);
    v_dept_id     := p_dept_id;
    dbms_output.put_line('2');
    FOR m2 IN pay_cur(v_dept_id, p_emp_id) LOOP
      dbms_output.put_line('Payroll ' || '- ' || m2.pay_element_id || '- ' ||
                           m2.pay_emp_id || ' - ' || m2.pay_element_code || '-' ||
                           m2.pay_element_prorate || ' - ' ||
                           m2.emp_prorate || ' - ' ||
                           upper(m2.pay_ele_class) || ' - ' ||
                           m2.pay_amount || ' -Department ' || v_dept_id); -- m1.pay_emp_dept_id);
    
      v_ded_amt := 0;
      v_pay_amt := 0;
      --v_mgrp_emp_ded:=0;
      v_skip_element := '0';
      v_mid_month    := '0';
      v_lop_factor   := 1;
      tmp_lop_days   := 0;
      v_lop_days     := 0;
      tmp_dt         := NULL;
      v_loan_ded     := 0;
    
      IF m2.emp_doj BETWEEN v_period_from_dt AND v_period_to_dt THEN
        --v_period_from_dt:=m2.emp_doj;
        v_mid_month   := '1';
        v_actual_days := pkg_payroll.get_days(p_from_dt => m2.emp_doj,
                                              p_to_dt   => v_period_to_dt);
      ELSIF m2.emp_doj > v_period_to_dt THEN
        v_skip_emp := '1';
      END IF;
      --START SKIP EMP
      --dbms_output.put_line('v_skip_emp  '||v_skip_emp);
      IF v_skip_emp = '0' THEN
      
        IF v_mid_month = '0' THEN
          tmp_dt := v_period_from_dt;
        ELSE
          tmp_dt := m2.emp_doj;
        END IF;
        dbms_output.put_line('Ilam23 ' || tmp_dt || ' - ' ||
                             v_period_to_dt);
      
        pkg_payroll.get_element_stop_dtl_ls(p_emp_id     => m2.pay_emp_id,
                                            p_element_id => m2.pay_element_id,
                                            p_from_dt    => tmp_dt, --v_period_from_dt,
                                            p_to_dt      => v_period_to_dt,
                                            --p_exclude_fd   =>'0',
                                            p_payroll_days => v_multiply_factor,
                                            p_skip_element => v_skip_element,
                                            p_ignore_emp   => v_ignore_emp);
      
        v_fixed_days := ssm.get_parameter_value(p_param_code => 'FIXEDDAYS');
        if v_fixed_days > 0 then
          v_payroll_days := v_fixed_Days;
        else
          v_payroll_days := get_days(p_from_dt => trunc(v_period_from_dt,
                                                        'mm'),
                                     p_to_dt   => last_day(trunc(v_period_from_dt,
                                                                 'mm')));
        end if;
        /*
        v_payroll_days := pkg_payroll.get_days(p_from_dt => v_period_from_dt,
                                               p_to_dt   => v_period_to_dt);
        */
        v_holidays := pkg_payroll.get_holidays(p_org_id      => p_org_id,
                                               p_fiscal_year => v_fiscal_year,
                                               p_from_dt     => v_period_from_dt,
                                               p_to_dt       => v_period_to_dt);
      
        v_payroll_days := v_payroll_days - v_holidays;
        dbms_output.put_line('Payroll Days ' || v_payroll_days ||
                             ' Multiply Factor before ' ||
                             v_multiply_factor);
      
        --Below for loop for LOP factor and LOP days
        --dbms_output.put_line('DD '||v_fiscal_year||' - '||p_org_id||' - '||m2.pay_emp_id||' - '||v_period_to_dt);
        /*
            FOR l_cur IN leave_cur(v_fiscal_year,
                                   p_org_id,
                                   m2.pay_emp_id,
                                   v_period_to_dt) LOOP
              --dbms_output.put_line('Leave taken by '||m2.pay_emp_id ||' -> '||l_cur.leave_id);
              pkg_payroll.get_LOP_days(p_org_id      => p_org_id,
                                       p_fiscal_year => v_fiscal_year,
                                       p_dept_id     => v_dept_id,
                                       p_staff_id    => m2.pay_emp_id,
                                       p_leave_id    => l_cur.leave_id,
                                       p_from_date   => tmp_dt, --v_period_from_dt,
                                       p_to_date     => v_period_to_dt,
                                       p_level       => l_cur.attribute1,
                                       p_lop_factor  => v_lop_factor,
                                       p_lop_days    => tmp_lop_days);
              --dbms_output.put_line('tmp_lop_days  '||tmp_lop_days);
              v_lop_days := v_lop_days + NVL(tmp_lop_days, 0);
            END LOOP;
        */
        --dbms_output.put_line('Total LOP Days for the month '||v_lop_days);
      
        IF m2.ele_unpaid_leave = '1' THEN
          IF m2.eval_unpaid_leave = '1' THEN
            --v_payroll_days:=v_payroll_days-v_lop_days;
            v_multiply_factor := v_multiply_factor - v_lop_days;
          END IF;
        ELSIF m2.ele_unpaid_leave = '0' THEN
          IF m2.eval_unpaid_leave = '1' THEN
            --v_payroll_days:=v_payroll_days-v_lop_days;
            v_multiply_factor := v_multiply_factor - v_lop_days;
          END IF;
        END IF;
        dbms_output.put_line(' v_lop_factor ' || v_lop_factor);
        --dbms_output.put_line('Pay days '||v_payroll_days||' Multiply '||v_multiply_factor||' - LOP Factor '||v_lop_factor);
        --dbms_output.put_line('Deciding factors '||' Igonore emp '||v_ignore_emp||' - Skip Element '||v_skip_element);
        IF v_ignore_emp = '0' THEN
        
          IF m2.pay_ele_recurring = '1' THEN
            IF UPPER(RTRIM(LTRIM(m2.pay_ele_class))) = 'EARNING' THEN
              -- debug_proc('Earning ='||'Department '||m1.pay_emp_dept_id||'-'||upper(m2.pay_ele_class)||'-'||v_dept_tot_pay||' For '||m2.pay_amount);
              IF v_skip_element = '0' THEN
                IF m2.pay_element_prorate = '1' THEN
                  IF m2.emp_prorate = '1' THEN
                    --Below block for pro-rate the element
                    IF LTRIM(RTRIM(m2.pay_element_id)) =
                       LTRIM(RTRIM(v_sys_option2)) THEN
                      --BAS
                      dbms_output.put_line('Ilam1 -v_multiply_factor' ||
                                           v_multiply_factor ||
                                           ' -v_lop_factor ' ||
                                           v_lop_factor ||
                                           '-v_payroll_days ' ||
                                           v_payroll_days);
                      v_pay_amt := m2.pay_amount * v_multiply_factor *
                                   v_lop_factor / v_payroll_days;
                    ELSE
                      dbms_output.put_line('Ilam2 -v_multiply_factor' ||
                                           v_multiply_factor ||
                                           ' -v_lop_factor ' ||
                                           v_lop_factor ||
                                           '-v_payroll_days ' ||
                                           v_payroll_days);
                      v_pay_amt := m2.pay_amount * v_multiply_factor /
                                   v_payroll_days;
                    END IF;
                    dbms_output.put_line('Pro-rate1 Ilam' || '-Element ' ||
                                         m2.pay_element_id || ' - ' ||
                                         v_pay_amt || ' Multiply ' ||
                                         v_multiply_factor ||
                                         ' Payroll days ' ||
                                         v_payroll_days);
                    v_tot_pay      := v_tot_pay + v_pay_amt;
                    v_dept_tot_pay := v_dept_tot_pay + v_pay_amt;
                    v_grp_tot_pay  := v_grp_tot_pay + v_pay_amt;
                  ELSE
                    --below block for not to pro-rate the element
                    IF LTRIM(RTRIM(m2.pay_element_id)) =
                       LTRIM(RTRIM(v_sys_option2)) THEN
                      --BAS
                      v_pay_amt := m2.pay_amount * v_lop_factor;
                    ELSE
                      v_pay_amt := m2.pay_amount;
                    END IF;
                    dbms_output.put_line('NO Pro-rate1' || '-Element ' ||
                                         m2.pay_element_id || ' - ' ||
                                         v_pay_amt || ' Multiply ' ||
                                         v_multiply_factor ||
                                         ' Payroll days ' ||
                                         v_payroll_days);
                    v_tot_pay      := v_tot_pay + v_pay_amt;
                    v_dept_tot_pay := v_dept_tot_pay + v_pay_amt;
                    v_grp_tot_pay  := v_grp_tot_pay + v_pay_amt;
                  END IF;
                ELSE
                  --v_pay_amt:=m2.pay_amount*v_multiply_factor;
                  IF m2.emp_prorate = '1' THEN
                    --Below block for pro-rate the element
                    IF LTRIM(RTRIM(m2.pay_element_id)) =
                       LTRIM(RTRIM(v_sys_option2)) THEN
                      --BAS
                      v_pay_amt := m2.pay_amount * v_multiply_factor *
                                   v_lop_factor / v_payroll_days;
                    ELSE
                      v_pay_amt := m2.pay_amount * v_multiply_factor /
                                   v_payroll_days;
                    END IF;
                    dbms_output.put_line('Pro-rate2' || '-Element ' ||
                                         m2.pay_element_id || ' - ' ||
                                         v_pay_amt || ' Multiply ' ||
                                         v_multiply_factor ||
                                         ' Payroll days ' ||
                                         v_payroll_days);
                    v_tot_pay      := v_tot_pay + v_pay_amt;
                    v_dept_tot_pay := v_dept_tot_pay + v_pay_amt;
                    v_grp_tot_pay  := v_grp_tot_pay + v_pay_amt;
                  ELSE
                    --below block for not to pro-rate the element
                    IF LTRIM(RTRIM(m2.pay_element_id)) =
                       LTRIM(RTRIM(v_sys_option2)) THEN
                      --BAS
                      v_pay_amt := m2.pay_amount * v_lop_factor;
                    ELSE
                      v_pay_amt := m2.pay_amount;
                    END IF;
                    dbms_output.put_line('NO Pro-rate2' || '-Element ' ||
                                         m2.pay_element_id || ' - ' ||
                                         v_pay_amt || ' Multiply ' ||
                                         v_multiply_factor ||
                                         ' Payroll days ' ||
                                         v_payroll_days);
                    v_tot_pay      := v_tot_pay + v_pay_amt;
                    v_dept_tot_pay := v_dept_tot_pay + v_pay_amt;
                    v_grp_tot_pay  := v_grp_tot_pay + v_pay_amt;
                  END IF;
                END IF;
              END IF;
            ELSIF UPPER(RTRIM(LTRIM(m2.pay_ele_class))) = 'DEDUCTION' THEN
              --  debug_proc('Ded ='||'Department '||m1.pay_emp_dept_id||'-'||upper(m2.pay_ele_class)||'-'||v_dept_tot_ded||' For '||m2.pay_amount);
              --dbms_output.put_line(' Recurring Deduction' ||'-Element '||m2.pay_element_id||' - '||v_pay_amt||' Multiply '||v_multiply_factor||' Payroll days '||v_payroll_days);
              dbms_output.put_line('Recurring Deduction sys ' ||
                                   v_sys_option1 || ' - ' || v_sys_option3 ||
                                   ' - ' || v_sys_option4);
              IF m2.pay_element_prorate = '1' THEN
                IF v_skip_element = '0' THEN
                  IF v_sys_option1 IS NOT NULL AND
                     LTRIM(RTRIM(v_sys_option1)) =
                     LTRIM(RTRIM(m2.pay_element_id)) THEN
                    --dbms_output.put_line('System option test1 '||v_sys_option1 ||' -Ele id '||m2.pay_element_id);
                    --Below IF ELSE ENDIF for Loan Deduction
                    dbms_output.put_line('Loan parameters ' || p_org_id ||
                                         ' - ' || v_dept_id || ' - ' ||
                                         m2.pay_emp_id || ' - ' ||
                                         v_period_from_dt || ' - ' ||
                                         v_period_to_dt);
                    v_ded_amt := pkg_payroll.get_loan_deduction(p_org_id  => p_org_id,
                                                                p_dept_id => v_dept_id,
                                                                p_emp_id  => m2.pay_emp_id,
                                                                p_from_dt => v_period_from_dt,
                                                                p_to_dt   => v_period_to_dt);
                    dbms_output.put_line('Loan Deduction1 ' || v_ded_amt);
                    v_ded_amt      := v_ded_amt * v_multiply_factor /
                                      v_payroll_days;
                    v_tot_ded      := v_tot_ded + v_ded_amt;
                    v_dept_tot_ded := v_dept_tot_ded + v_ded_amt;
                    v_grp_tot_ded  := v_grp_tot_ded + v_ded_amt;
                  ELSIF v_sys_option3 IS NOT NULL AND
                        LTRIM(RTRIM(v_sys_option3)) =
                        LTRIM(RTRIM(m2.pay_element_id)) THEN
                    --Below MGRP EMP deduction calculation
                    /*                    dbms_output.put_line('MGRP Parameters1 ' ||
                                                             v_sys_option3 || ' - ' ||
                                                             v_sys_option3_val || ' - ' ||
                                                             m2.pay_element_id);
                                        v_mgrp_emp_amt := pkg_payroll.get_mgrp_amt(p_org_id => p_org_id,
                                                                                   p_emp_id => m2.pay_emp_id,
                                                                                   p_year   => TO_CHAR(v_period_from_dt,
                                                                                                       'YYYY'));
                                      
                                        v_basic_sal_amt := pkg_payroll.get_basic_amt(p_org_id => p_org_id,
                                                                                     p_emp_id => m2.pay_emp_id);
                                      
                                        v_ded_amt := (v_mgrp_emp_amt + v_basic_sal_amt) *
                                                     v_sys_option3_val / 100;
                                        v_ded_amt := v_ded_amt * v_multiply_factor /
                                                     v_payroll_days;
                                        dbms_output.put_line('Employee Contribution MGRP -1 ' ||
                                                             v_ded_amt);
                    */
                    begin
                      select nvl(pesd.emp_indemnity, 0) +
                             nvl(pesd.emp_bas_supp, 0) +
                             nvl(pesd.emp_inc_pension, 0) +
                             nvl(pesd.emp_unempl_support, 0)
                        into v_ded_amt
                        from pay_employee_ssd_details pesd
                       where pesd.emp_id = m2.pay_emp_id
                         and pesd.org_id = p_org_id
                         and pesd.pay_period = p_pay_period;
                    exception
                      when others then
                        v_ded_amt := 0;
                    end;
                    v_tot_ded      := v_tot_ded + v_ded_amt;
                    v_dept_tot_ded := v_dept_tot_ded + v_ded_amt;
                    v_grp_tot_ded  := v_grp_tot_ded + v_ded_amt;
                  ELSIF v_sys_option4 IS NOT NULL AND
                        LTRIM(RTRIM(v_sys_option4)) =
                        LTRIM(RTRIM(m2.pay_element_id)) THEN
                    --Below MGRP EPR deduction calculation
                    dbms_output.put_line('MGRP Parameters1-A ' ||
                                         v_sys_option4 || ' - ' ||
                                         v_sys_option4_val || ' - ' ||
                                         m2.pay_element_id);
                    v_mgrp_emp_amt := pkg_payroll.get_mgrp_amt(p_org_id => p_org_id,
                                                               p_emp_id => m2.pay_emp_id,
                                                               p_year   => TO_CHAR(v_period_from_dt,
                                                                                   'YYYY'));
                  
                    v_basic_sal_amt := pkg_payroll.get_basic_amt(p_org_id => p_org_id,
                                                                 p_emp_id => m2.pay_emp_id);
                  
                    v_ded_amt := (v_mgrp_emp_amt + v_basic_sal_amt) *
                                 v_sys_option4_val / 100;
                    v_ded_amt := v_ded_amt * v_multiply_factor /
                                 v_payroll_days;
                    dbms_output.put_line('Employee Contribution MGRP -1A ' ||
                                         v_ded_amt);
                    v_tot_ded      := v_tot_ded + v_ded_amt;
                    v_dept_tot_ded := v_dept_tot_ded + v_ded_amt;
                    v_grp_tot_ded  := v_grp_tot_ded + v_ded_amt;
                  ELSE
                    dbms_output.put_line(' Else Deduction1' || '-Element ' ||
                                         m2.pay_element_id);
                    v_ded_amt      := m2.pay_amount * v_multiply_factor /
                                      v_payroll_days;
                    v_tot_ded      := v_tot_ded + v_ded_amt;
                    v_dept_tot_ded := v_dept_tot_ded + v_ded_amt;
                    v_grp_tot_ded  := v_grp_tot_ded + v_ded_amt;
                  END IF;
                END IF;
              ELSE
                IF v_skip_element = '0' THEN
                  IF v_sys_option1 IS NOT NULL AND
                     LTRIM(RTRIM(v_sys_option1)) =
                     LTRIM(RTRIM(m2.pay_element_id)) THEN
                    --Below IF ELSE ENDIF for Loan Deduction
                    v_ded_amt := pkg_payroll.get_loan_deduction(p_org_id  => p_org_id,
                                                                p_dept_id => v_dept_id,
                                                                p_emp_id  => m2.pay_emp_id,
                                                                p_from_dt => v_period_from_dt,
                                                                p_to_dt   => v_period_to_dt);
                    dbms_output.put_line('Loan Deduction2 ' || v_ded_amt);
                    v_tot_ded      := v_tot_ded + v_ded_amt;
                    v_dept_tot_ded := v_dept_tot_ded + v_ded_amt;
                    v_grp_tot_ded  := v_grp_tot_ded + v_ded_amt;
                  ELSIF v_sys_option3 IS NOT NULL AND
                        LTRIM(RTRIM(v_sys_option3)) =
                        LTRIM(RTRIM(m2.pay_element_id)) THEN
                    --Below MGRP EMP deduction calculation
                    /*                    dbms_output.put_line('MGRP Parameters2 ' ||
                                                            v_sys_option3 || ' - ' || ' - ' ||
                                                            v_sys_option3_val || ' - ' ||
                                                            m2.pay_element_id);
                                     
                                       v_mgrp_emp_amt := pkg_payroll.get_mgrp_amt(p_org_id => p_org_id,
                                                                                  p_emp_id => m2.pay_emp_id,
                                                                                  p_year   => TO_CHAR(v_period_from_dt,
                                                                                                      'YYYY'));
                                     
                                       v_basic_sal_amt := pkg_payroll.get_basic_amt(p_org_id => p_org_id,
                                                                                    p_emp_id => m2.pay_emp_id);
                                     
                                       v_ded_amt := (v_mgrp_emp_amt + v_basic_sal_amt) *
                                                    v_sys_option3_val / 100;
                                       dbms_output.put_line('Employee Contribution MGRP -2 ' ||
                                                            v_ded_amt || ' - ' ||
                                                            v_basic_sal_amt);
                    */
                    begin
                      select nvl(pesd.emp_indemnity, 0) +
                             nvl(pesd.emp_bas_supp, 0) +
                             nvl(pesd.emp_inc_pension, 0) +
                             nvl(pesd.emp_unempl_support, 0)
                        into v_ded_amt
                        from pay_employee_ssd_details pesd
                       where pesd.emp_id = m2.pay_emp_id
                         and pesd.org_id = p_org_id
                         and pesd.pay_period = p_pay_period;
                    exception
                      when others then
                        v_ded_amt := 0;
                    end;
                    v_tot_ded      := v_tot_ded + v_ded_amt;
                    v_dept_tot_ded := v_dept_tot_ded + v_ded_amt;
                    v_grp_tot_ded  := v_grp_tot_ded + v_ded_amt;
                  ELSIF v_sys_option4 IS NOT NULL AND
                        LTRIM(RTRIM(v_sys_option4)) =
                        LTRIM(RTRIM(m2.pay_element_id)) THEN
                    --Below MGRP EPR deduction calculation
                    dbms_output.put_line('MGRP Parameters2A ' ||
                                         v_sys_option4 || ' - ' || ' - ' ||
                                         v_sys_option4_val || ' - ' ||
                                         m2.pay_element_id);
                  
                    v_mgrp_emp_amt := pkg_payroll.get_mgrp_amt(p_org_id => p_org_id,
                                                               p_emp_id => m2.pay_emp_id,
                                                               p_year   => TO_CHAR(v_period_from_dt,
                                                                                   'YYYY'));
                  
                    v_basic_sal_amt := pkg_payroll.get_basic_amt(p_org_id => p_org_id,
                                                                 p_emp_id => m2.pay_emp_id);
                  
                    v_ded_amt := (v_mgrp_emp_amt + v_basic_sal_amt) *
                                 v_sys_option4_val / 100;
                    dbms_output.put_line('Employer Contribution MGRP -2 ' ||
                                         v_ded_amt);
                    v_tot_ded      := v_tot_ded + v_ded_amt;
                    v_dept_tot_ded := v_dept_tot_ded + v_ded_amt;
                    v_grp_tot_ded  := v_grp_tot_ded + v_ded_amt;
                  ELSE
                    dbms_output.put_line(' Else Deduction2' || '-Element ' ||
                                         m2.pay_element_id);
                    v_ded_amt      := m2.pay_amount;
                    v_tot_ded      := v_tot_ded + v_ded_amt;
                    v_dept_tot_ded := v_dept_tot_ded + v_ded_amt;
                    v_grp_tot_ded  := v_grp_tot_ded + v_ded_amt;
                  END IF;
                END IF;
              END IF;
            END IF;
          ELSIF m2.pay_ele_recurring = '0' THEN
            --Start Non-Recurring
            IF m2.pay_period_id = p_pay_period THEN
              IF UPPER(RTRIM(LTRIM(m2.pay_ele_class))) = 'EARNING' THEN
                -- debug_proc('Earning ='||'Department '||m1.pay_emp_dept_id||'-'||upper(m2.pay_ele_class)||'-'||v_dept_tot_pay||' For '||m2.pay_amount);
                IF m2.pay_element_prorate = '1' THEN
                  IF v_skip_element = '0' THEN
                    v_pay_amt := m2.pay_amount * v_multiply_factor /
                                 v_payroll_days;
                    dbms_output.put_line('Pro-rate' || '-Element ' ||
                                         m2.pay_element_id || ' - ' ||
                                         v_pay_amt || ' Multiply ' ||
                                         v_multiply_factor ||
                                         ' Payroll days ' ||
                                         v_payroll_days);
                    v_tot_pay      := v_tot_pay + v_pay_amt;
                    v_dept_tot_pay := v_dept_tot_pay + v_pay_amt;
                    v_grp_tot_pay  := v_grp_tot_pay + v_pay_amt;
                  END IF;
                ELSE
                  --v_pay_amt:=m2.pay_amount*v_multiply_factor;
                  IF v_skip_element = '0' THEN
                    v_pay_amt := m2.pay_amount;
                    dbms_output.put_line('Pro-rate' || '-Element ' ||
                                         m2.pay_element_id || ' - ' ||
                                         v_pay_amt || ' Multiply ' ||
                                         v_multiply_factor ||
                                         ' Payroll days ' ||
                                         v_payroll_days);
                    v_tot_pay      := v_tot_pay + v_pay_amt;
                    v_dept_tot_pay := v_dept_tot_pay + v_pay_amt;
                    v_grp_tot_pay  := v_grp_tot_pay + v_pay_amt;
                  END IF;
                END IF;
              
              ELSIF UPPER(RTRIM(LTRIM(m2.pay_ele_class))) = 'DEDUCTION' THEN
                --  debug_proc('Ded ='||'Department '||m1.pay_emp_dept_id||'-'||upper(m2.pay_ele_class)||'-'||v_dept_tot_ded||' For '||m2.pay_amount);
                --dbms_output.put_line('Non Recurring Deduction' ||'-Element '||m2.pay_element_id||' - '||v_pay_amt||' Multiply '||v_multiply_factor||' Payroll days '||v_payroll_days);
                dbms_output.put_line('Non Recurring Deduction sys ' ||
                                     v_sys_option1 || ' - ' ||
                                     v_sys_option3);
                IF m2.pay_element_prorate = '1' THEN
                  IF v_skip_element = '0' THEN
                    IF v_sys_option1 IS NOT NULL AND
                       LTRIM(RTRIM(v_sys_option1)) =
                       LTRIM(RTRIM(m2.pay_element_id)) THEN
                      --Below IF ELSE ENDIF for Loan Deduction
                    
                      v_ded_amt := pkg_payroll.get_loan_deduction(p_org_id  => p_org_id,
                                                                  p_dept_id => v_dept_id,
                                                                  p_emp_id  => m2.pay_emp_id,
                                                                  p_from_dt => v_period_from_dt,
                                                                  p_to_dt   => v_period_to_dt);
                      dbms_output.put_line('Loan Deduction3 ' || v_ded_amt);
                      v_ded_amt      := v_ded_amt * v_multiply_factor /
                                        v_payroll_days;
                      v_tot_ded      := v_tot_ded + v_ded_amt;
                      v_dept_tot_ded := v_dept_tot_ded + v_ded_amt;
                      v_grp_tot_ded  := v_grp_tot_ded + v_ded_amt;
                    
                    ELSIF v_sys_option3 IS NOT NULL AND
                          LTRIM(RTRIM(v_sys_option3)) =
                          LTRIM(RTRIM(m2.pay_element_id)) THEN
                      --Below MGRP EMP deduction calculation
                      /*                      dbms_output.put_line('MGRP Parameters3 ' ||
                                                                 v_sys_option3 || ' - ' || ' - ' ||
                                                                 v_sys_option3_val || ' - ' ||
                                                                 m2.pay_element_id);
                                            v_mgrp_emp_amt := pkg_payroll.get_mgrp_amt(p_org_id => p_org_id,
                                                                                       p_emp_id => m2.pay_emp_id,
                                                                                       p_year   => TO_CHAR(v_period_from_dt,
                                                                                                           'YYYY'));
                                          
                                            v_basic_sal_amt := pkg_payroll.get_basic_amt(p_org_id => p_org_id,
                                                                                         p_emp_id => m2.pay_emp_id);
                                          
                                            v_ded_amt := (v_mgrp_emp_amt + v_basic_sal_amt) *
                                                         v_sys_option3_val / 100;
                                            v_ded_amt := v_ded_amt * v_multiply_factor /
                                                         v_payroll_days;
                                            dbms_output.put_line('Employee Contribution MGRP -3 ' ||
                                                                 v_ded_amt);
                      */
                      begin
                        select nvl(pesd.emp_indemnity, 0) +
                               nvl(pesd.emp_bas_supp, 0) +
                               nvl(pesd.emp_inc_pension, 0) +
                               nvl(pesd.emp_unempl_support, 0)
                          into v_ded_amt
                          from pay_employee_ssd_details pesd
                         where pesd.emp_id = m2.pay_emp_id
                           and pesd.org_id = p_org_id
                           and pesd.pay_period = p_pay_period;
                      exception
                        when others then
                          v_ded_amt := 0;
                      end;
                      v_tot_ded      := nvl(v_tot_ded, 0) +
                                        nvl(v_ded_amt, 0);
                      v_dept_tot_ded := nvl(v_dept_tot_ded, 0) +
                                        nvl(v_ded_amt, 0);
                      v_grp_tot_ded  := nvl(v_grp_tot_ded, 0) +
                                        nvl(v_ded_amt, 0);
                    ELSIF v_sys_option4 IS NOT NULL AND
                          LTRIM(RTRIM(v_sys_option4)) =
                          LTRIM(RTRIM(m2.pay_element_id)) THEN
                      --Below MGRP EPR deduction calculation
                      dbms_output.put_line('MGRP Parameters3A ' ||
                                           v_sys_option4 || ' - ' ||
                                           v_sys_option4_val || ' - ' ||
                                           m2.pay_element_id);
                      v_mgrp_emp_amt := pkg_payroll.get_mgrp_amt(p_org_id => p_org_id,
                                                                 p_emp_id => m2.pay_emp_id,
                                                                 p_year   => TO_CHAR(v_period_from_dt,
                                                                                     'YYYY'));
                    
                      v_basic_sal_amt := pkg_payroll.get_basic_amt(p_org_id => p_org_id,
                                                                   p_emp_id => m2.pay_emp_id);
                    
                      v_ded_amt := (v_mgrp_emp_amt + v_basic_sal_amt) *
                                   v_sys_option4_val / 100;
                      v_ded_amt := v_ded_amt * v_multiply_factor /
                                   v_payroll_days;
                      dbms_output.put_line('Employee Contribution MGRP -3 ' ||
                                           v_ded_amt);
                      v_tot_ded      := nvl(v_tot_ded, 0) +
                                        nvl(v_ded_amt, 0);
                      v_dept_tot_ded := nvl(v_dept_tot_ded, 0) +
                                        nvl(v_ded_amt, 0);
                      v_grp_tot_ded  := nvl(v_grp_tot_ded, 0) +
                                        nvl(v_ded_amt, 0);
                    ELSE
                      dbms_output.put_line(' Else Deduction3' ||
                                           '-Element ' ||
                                           m2.pay_element_id);
                      v_ded_amt      := m2.pay_amount * v_multiply_factor /
                                        v_payroll_days;
                      v_tot_ded      := nvl(v_tot_ded, 0) +
                                        nvl(v_ded_amt, 0);
                      v_dept_tot_ded := nvl(v_dept_tot_ded, 0) +
                                        nvl(v_ded_amt, 0);
                      v_grp_tot_ded  := nvl(v_grp_tot_ded, 0) +
                                        nvl(v_ded_amt, 0);
                    END IF;
                  END IF;
                ELSE
                  IF v_skip_element = '0' THEN
                    IF v_sys_option1 IS NOT NULL AND
                       LTRIM(RTRIM(v_sys_option1)) =
                       LTRIM(RTRIM(m2.pay_element_id)) THEN
                      --Below IF ELSE ENDIF for Loan Deduction
                      v_ded_amt := pkg_payroll.get_loan_deduction(p_org_id  => p_org_id,
                                                                  p_dept_id => v_dept_id,
                                                                  p_emp_id  => m2.pay_emp_id,
                                                                  p_from_dt => v_period_from_dt,
                                                                  p_to_dt   => v_period_to_dt);
                      dbms_output.put_line('Loan Deduction4 ' || v_ded_amt);
                      v_tot_ded      := nvl(v_tot_ded, 0) +
                                        nvl(v_ded_amt, 0);
                      v_dept_tot_ded := nvl(v_dept_tot_ded, 0) +
                                        nvl(v_ded_amt, 0);
                      v_grp_tot_ded  := nvl(v_grp_tot_ded, 0) +
                                        nvl(v_ded_amt, 0);
                    ELSIF v_sys_option3 IS NOT NULL AND
                          LTRIM(RTRIM(v_sys_option3)) =
                          LTRIM(RTRIM(m2.pay_element_id)) THEN
                      --Below MGRP EMP deduction calculation
                      /*                     dbms_output.put_line('MGRP Parameters3 ' ||
                                                                v_sys_option3 || ' - ' ||
                                                                v_sys_option3_val || ' - ' ||
                                                                m2.pay_element_id);
                                           v_mgrp_emp_amt  := pkg_payroll.get_mgrp_amt(p_org_id => p_org_id,
                                                                                       p_emp_id => m2.pay_emp_id,
                                                                                       p_year   => TO_CHAR(v_period_from_dt,
                                                                                                           'YYYY'));
                                           v_basic_sal_amt := pkg_payroll.get_basic_amt(p_org_id => p_org_id,
                                                                                        p_emp_id => m2.pay_emp_id);
                                         
                                           v_ded_amt := (v_mgrp_emp_amt + v_basic_sal_amt) *
                                                        v_sys_option3_val / 100;
                                           dbms_output.put_line('Employee Contribution MGRP -4 ' ||
                                                                v_ded_amt);
                      */
                      begin
                        select nvl(pesd.emp_indemnity, 0) +
                               nvl(pesd.emp_bas_supp, 0) +
                               nvl(pesd.emp_inc_pension, 0) +
                               nvl(pesd.emp_unempl_support, 0)
                          into v_ded_amt
                          from pay_employee_ssd_details pesd
                         where pesd.emp_id = m2.pay_emp_id
                           and pesd.org_id = p_org_id
                           and pesd.pay_period = p_pay_period;
                      exception
                        when others then
                          v_ded_amt := 0;
                      end;
                      v_tot_ded      := v_tot_ded + v_ded_amt;
                      v_dept_tot_ded := v_dept_tot_ded + v_ded_amt;
                      v_grp_tot_ded  := v_grp_tot_ded + v_ded_amt;
                    ELSIF v_sys_option4 IS NOT NULL AND
                          LTRIM(RTRIM(v_sys_option4)) =
                          LTRIM(RTRIM(m2.pay_element_id)) THEN
                      --Below MGRP EPR deduction calculation
                      dbms_output.put_line('MGRP Parameters3A ' ||
                                           v_sys_option4 || ' - ' ||
                                           v_sys_option4_val || ' - ' ||
                                           m2.pay_element_id);
                      v_mgrp_emp_amt  := pkg_payroll.get_mgrp_amt(p_org_id => p_org_id,
                                                                  p_emp_id => m2.pay_emp_id,
                                                                  p_year   => TO_CHAR(v_period_from_dt,
                                                                                      'YYYY'));
                      v_basic_sal_amt := pkg_payroll.get_basic_amt(p_org_id => p_org_id,
                                                                   p_emp_id => m2.pay_emp_id);
                    
                      v_ded_amt := (v_mgrp_emp_amt + v_basic_sal_amt) *
                                   v_sys_option4_val / 100;
                      dbms_output.put_line('Employee Contribution MGRP -4 ' ||
                                           v_ded_amt);
                      v_tot_ded      := nvl(v_tot_ded, 0) +
                                        nvl(v_ded_amt, 0);
                      v_dept_tot_ded := nvl(v_dept_tot_ded, 0) +
                                        nvl(v_ded_amt, 0);
                      v_grp_tot_ded  := nvl(v_grp_tot_ded, 0) +
                                        nvl(v_ded_amt, 0);
                    ELSE
                      dbms_output.put_line(' Else Deduction4' ||
                                           '-Element ' ||
                                           m2.pay_element_id);
                      v_ded_amt      := m2.pay_amount;
                      v_tot_ded      := nvl(v_tot_ded, 0) +
                                        nvl(v_ded_amt, 0);
                      v_dept_tot_ded := nvl(v_dept_tot_ded, 0) +
                                        nvl(v_ded_amt, 0);
                      v_grp_tot_ded  := nvl(v_grp_tot_ded, 0) +
                                        nvl(v_ded_amt, 0);
                    END IF;
                  END IF;
                END IF;
              END IF;
            ELSE
              --start
              --Non-Recurring Deduction logic with no pay_period_id goes here
              IF m2.pay_element_prorate = '1' THEN
                dbms_output.put_line('From NR table ' || p_org_id || ' - ' ||
                                     p_pay_period || ' - ' ||
                                     m2.pay_element_id || ' - ' ||
                                     v_dept_id || ' - ' || m2.pay_emp_id);
                IF v_skip_element = '0' THEN
                  v_ded_amt      := pkg_payroll.get_nr_deduction(p_org_id     => p_org_id,
                                                                 p_pay_period => p_pay_period,
                                                                 p_element_id => m2.pay_element_id,
                                                                 p_dept_id    => v_dept_id,
                                                                 p_emp_id     => m2.pay_emp_id);
                  v_ded_amt      := nvl(v_ded_amt, 0) * v_multiply_factor /
                                    v_payroll_days;
                  v_tot_ded      := nvl(v_tot_ded, 0) + nvl(v_ded_amt, 0);
                  v_dept_tot_ded := nvl(v_dept_tot_ded, 0) +
                                    nvl(v_ded_amt, 0);
                  v_grp_tot_ded  := nvl(v_grp_tot_ded, 0) +
                                    nvl(v_ded_amt, 0);
                END IF;
              ELSE
                IF v_skip_element = '0' THEN
                  v_ded_amt := pkg_payroll.get_nr_deduction(p_org_id     => p_org_id,
                                                            p_pay_period => p_pay_period,
                                                            p_element_id => m2.pay_element_id,
                                                            p_dept_id    => v_dept_id,
                                                            p_emp_id     => m2.pay_emp_id);
                
                  v_tot_ded      := nvl(v_tot_ded, 0) + nvl(v_ded_amt, 0);
                  v_dept_tot_ded := nvl(v_dept_tot_ded, 0) +
                                    nvl(v_ded_amt, 0);
                  v_grp_tot_ded  := nvl(v_grp_tot_ded, 0) +
                                    nvl(v_ded_amt, 0);
                END IF;
              
              END IF;
              --end
            END IF;
            --End Non-Recurring
          END IF;
          --dbms_output.put_line('For Employee '||m2.pay_emp_id||' -Payment '||v_tot_pay||' -Deduction '|| v_tot_ded);
        ELSIF v_ignore_emp = '1' THEN
          dbms_output.put_line('Logic for Ignore the employee goes here');
        END IF;
      END IF;
      --END SKIP EMP
    --Emp end
    END LOOP;
    v_final_amt := nvl(v_tot_pay, 0) - nvl(v_tot_ded, 0);
    DBMS_OUTPUT.PUT_LINE('Total Payment ' || v_tot_pay || ' -Deduction ' ||
                         v_tot_ded);
    v_tot_pay := 0;
    v_tot_ded := 0;
    RETURN v_final_amt;
  Exception
    When others then
      dbms_output.put_line('Final exception ' || sqlerrm);
      RETURN 0;
  END leave_salary_old;

  PROCEDURE payroll_report_trial(p_org_id     IN VARCHAR2,
                                 p_pay_period IN VARCHAR2) IS
  
    v_emp_bank_id     varchar2(500);
    v_from_date       date;
    v_to_date         date;
    v_salary          number := 0;
    v_wds             number := 0;
    v_als             number := 0;
    v_os              number := 0;
    v_ols             number := 0;
    v_allo            number := 0;
    v_vmm             number := 0;
    v_er              number := 0;
    v_tot_sal         number := 0;
    v_dedi            number := 0;
    v_dedr            number := 0;
    v_deds            number := 0;
    v_dedls           number := 0;
    v_dedrec          number := 0;
    v_netdue          number := 0;
    v_bank            varchar2(500) := null;
    v_dept_id         varchar2(500);
    v_dept_name       varchar2(500);
    v_dept_desig_id   varchar2(500);
    v_desig_name      varchar2(500);
    v_mon_days        number;
    v_dept_name_ol    varchar2(500);
    v_desig_name_ol   varchar2(500);
    v_bank_name       varchar2(500);
    v_bank_name_ol    varchar2(500);
    v_no_fridays      number;
    v_no_working_days number;
    v_leave           number;
    v_tot_leave       number;
    v_holiday         number;
    v_up_leave        number;
    v_tot_up_leave    number;
    v_al_leave        number;
    v_tot_al_leave    number;
  begin
  
    delete from pay_trial_run_emp_dtl ptred
     where ptred.payroll_period = p_pay_period;
    commit;
  
    select pp.pay_from_dt, pp.pay_to_dt, pp.no_fridays, pp.no_working_days
      into v_from_date, v_to_date, v_no_fridays, v_no_working_days
      from pay_periods pp
     where pp.pay_period_id = p_pay_period
       and pp.pay_org_id = p_org_id;
  
    v_mon_days := to_char(last_day(v_to_date), 'DD');
  
    for cur_emp in (select distinct he.emp_id,
                                    ptrh.payroll_id,
                                    he.emp_no,
                                    he.emp_first_name || ' ' ||
                                    he.emp_middle_name || ' ' ||
                                    he.emp_last_name emp_name,
                                    he.emp_first_name_ol || ' ' ||
                                    he.emp_middle_name_ol || ' ' ||
                                    he.emp_last_name_ol emp_name_ol,
                                    ptrdd.pay_emp_no_days,
                                    ptrdd.pay_emp_al_days,
                                    to_number(he.emp_no)
                      from hr_employees          he,
                           pay_trial_run_dtl     ptrd,
                           pay_trial_run_dtl_dtl ptrdd,
                           PAY_TRIAL_RUN_HDR     PTRH
                     where he.emp_org_id = p_org_id
                       and ptrh.payroll_id = ptrd.payroll_id
                       and ptrh.payroll_period = p_pay_period
                       and ptrd.payroll_dtl_id = ptrdd.payroll_dtl_id
                       and ptrdd.pay_emp_id = he.emp_id
                     order by to_number(he.emp_no)) loop
    
      v_salary       := 0;
      v_wds          := 0;
      v_als          := 0;
      v_os           := 0;
      v_ols          := 0;
      v_allo         := 0;
      v_vmm          := 0;
      v_er           := 0;
      v_tot_sal      := 0;
      v_dedi         := 0;
      v_dedr         := 0;
      v_deds         := 0;
      v_dedls        := 0;
      v_dedrec       := 0;
      v_netdue       := 0;
      v_leave        := 0;
      v_tot_leave    := 0;
      v_tot_up_leave := 0;
      v_tot_al_leave := 0;
      if cur_emp.emp_id = 'EMP_ID-0000003471' then
        null;
      end if;
      select hd.dept_id,
             hd.dept_name,
             hdd.dept_desig_id,
             hdd.desig_name,
             hd.dept_name_ol,
             hdd.desig_name_ol
        into v_dept_id,
             v_dept_name,
             v_dept_desig_id,
             v_desig_name,
             v_dept_name_ol,
             v_desig_name_ol
        from hr_emp_work_dtls     hewd,
             hr_departments       hd,
             hr_dept_designations hdd
       where hewd.emp_wh_id =
             (select max(hewdm.emp_wh_id)
                from hr_emp_work_dtls hewdm
               where hewdm.emp_id = cur_emp.emp_id
                 and nvl(hewdm.effective_to_dt, v_to_date) <= v_to_Date)
         and hewd.emp_dept_id = hd.dept_id
         and hewd.emp_desig_id = hdd.dept_desig_id
         and hd.dept_id = hdd.dept_id;
    
      begin
        for i in (select hsa.*
                    from hr_staff_attendance hsa,
                         ssm_system_options  sso,
                         hr_leave_encashment hle
                   where hsa.staff_id = cur_emp.emp_id
                     and hsa.attendance_type = sso.hr_earn_leave
                     and hsa.leave_req_id = hle.trans_id
                     and hsa.attendance_date between v_from_date and
                         v_to_date) loop
          v_leave   := 0;
          v_holiday := 0;
          select count('x')
            into v_holiday
            from hr_holidays_master hhm
           where hhm.holiday_date = i.attendance_date;
          if v_holiday = 0 then
          
            v_leave := get_days(p_from_dt => i.attendance_date,
                                p_to_dt   => i.attendance_date);
          
          end if;
        
          v_tot_leave := v_tot_leave + v_leave;
        end loop;
      exception
        when others then
          v_tot_leave := 0;
      end;
    
      begin
        for i in (select hsa.*
                    from hr_staff_attendance hsa, ssm_system_options sso
                   where hsa.staff_id = cur_emp.emp_id
                     and hsa.attendance_type = sso.hr_earn_leave
                     and hsa.attendance_date between v_from_date and
                         v_to_date
                     and exists
                   (select *
                            from hr_leave_encashment hle
                           where hsa.leave_req_id = hle.trans_id
                             and hle.lc_pay_period_id = p_pay_period)) loop
          v_al_leave := 0;
        
          v_al_leave := get_days(p_from_dt => i.attendance_date,
                                 p_to_dt   => i.attendance_date);
        
          v_tot_al_leave := v_tot_al_leave + v_al_leave;
        end loop;
      exception
        when others then
          v_tot_leave := 0;
      end;
    
      begin
        for i in (select hsa.*
                    from hr_staff_attendance hsa
                   where hsa.staff_id = cur_emp.emp_id
                     and hsa.attendance_type = 'Unpaid Leave'
                     and hsa.attendance_date between v_from_date and
                         v_to_date) loop
          v_up_leave := 0;
          v_holiday  := 0;
        
          v_up_leave := get_days(p_from_dt => i.attendance_date,
                                 p_to_dt   => i.attendance_date);
        
          v_tot_up_leave := v_tot_up_leave + v_up_leave;
        end loop;
      exception
        when others then
          v_tot_leave := 0;
      end;
    
      begin
        select cb.bank_name, cb.bank_name_ol, hebd.pk_id
          into v_bank_name, v_bank_name_ol, v_emp_bank_id
          from hr_emp_bank_dtls hebd, ca_bank cb
         where hebd.pk_id =
               (select max(hebdm.pk_id)
                  from hr_emp_bank_dtls hebdm
                 where hebdm.emp_id = cur_emp.emp_id
                   and nvl(hebdm.effective_to_dt, v_to_date) <= v_to_date)
           and hebd.emp_bank_code = cb.bank_id;
      exception
        when others then
          v_bank_name    := null;
          v_bank_name_ol := null;
      end;
      select sum(nvl(peev.pay_amount, 0))
        into v_salary
        from pay_emp_element_value peev
       where peev.pay_emp_id = cur_emp.emp_id
         and peev.effective_from_dt <= v_to_date
         and nvl(peev.effective_to_dt, v_to_date) >= v_to_date
         and exists (select *
                from ssm_code_masters scm
               where scm.parent_code = 'SAL'
                 and scm.code = peev.pay_element_id);
    
      select sum(ptrdd.pay_amount)
        into v_wds
        from pay_trial_run_dtl     ptrd,
             pay_trial_run_dtl_dtl ptrdd,
             pay_trial_run_hdr     ptrh
       where ptrd.payroll_dtl_id = ptrdd.payroll_dtl_id
         and ptrh.payroll_id = ptrd.payroll_id
         and ptrh.payroll_period = p_pay_period
         and ptrdd.pay_emp_id = cur_emp.emp_id
         and exists
       (select *
                from ssm_code_masters scm
               where scm.parent_code = 'WDS'
                 and scm.code = ptrdd.pay_emp_element_id);
    
      select sum(ptrdd.pay_amount)
        into v_als
        from pay_trial_run_dtl     ptrd,
             pay_trial_run_dtl_dtl ptrdd,
             pay_trial_run_hdr     ptrh
       where ptrd.payroll_dtl_id = ptrdd.payroll_dtl_id
         and ptrh.payroll_id = ptrd.payroll_id
         and ptrh.payroll_period = p_pay_period
         and ptrdd.pay_emp_id = cur_emp.emp_id
         and exists
       (select *
                from ssm_code_masters scm
               where scm.parent_code = 'ALS'
                 and scm.code = ptrdd.pay_emp_element_id);
    
      select sum(ptrdd.pay_amount)
        into v_os
        from pay_trial_run_dtl     ptrd,
             pay_trial_run_dtl_dtl ptrdd,
             pay_trial_run_hdr     ptrh
       where ptrd.payroll_dtl_id = ptrdd.payroll_dtl_id
         and ptrh.payroll_id = ptrd.payroll_id
         and ptrh.payroll_period = p_pay_period
         and ptrdd.pay_emp_id = cur_emp.emp_id
         and exists
       (select *
                from ssm_code_masters scm
               where scm.parent_code = 'OS'
                 and scm.code = ptrdd.pay_emp_element_id);
    
      select sum(ptrdd.pay_amount)
        into v_ols
        from pay_trial_run_dtl     ptrd,
             pay_trial_run_dtl_dtl ptrdd,
             pay_trial_run_hdr     ptrh
       where ptrd.payroll_dtl_id = ptrdd.payroll_dtl_id
         and ptrh.payroll_id = ptrd.payroll_id
         and ptrh.payroll_period = p_pay_period
         and ptrdd.pay_emp_id = cur_emp.emp_id
         and exists
       (select *
                from ssm_code_masters scm
               where scm.parent_code = 'OLS'
                 and scm.code = ptrdd.pay_emp_element_id);
    
      select sum(ptrdd.pay_amount)
        into v_allo
        from pay_trial_run_dtl     ptrd,
             pay_trial_run_dtl_dtl ptrdd,
             pay_trial_run_hdr     ptrh
       where ptrd.payroll_dtl_id = ptrdd.payroll_dtl_id
         and ptrh.payroll_id = ptrd.payroll_id
         and ptrh.payroll_period = p_pay_period
         and ptrdd.pay_emp_id = cur_emp.emp_id
         and exists
       (select *
                from ssm_code_masters scm
               where scm.parent_code = 'ALLO'
                 and scm.code = ptrdd.pay_emp_element_id);
    
      select sum(ptrdd.pay_amount)
        into v_vmm
        from pay_trial_run_dtl     ptrd,
             pay_trial_run_dtl_dtl ptrdd,
             pay_trial_run_hdr     ptrh
       where ptrd.payroll_dtl_id = ptrdd.payroll_dtl_id
         and ptrh.payroll_id = ptrd.payroll_id
         and ptrh.payroll_period = p_pay_period
         and ptrdd.pay_emp_id = cur_emp.emp_id
         and exists
       (select *
                from ssm_code_masters scm
               where scm.parent_code = 'VMM'
                 and scm.code = ptrdd.pay_emp_element_id);
    
      select sum(ptrdd.pay_amount)
        into v_er
        from pay_trial_run_dtl     ptrd,
             pay_trial_run_dtl_dtl ptrdd,
             pay_trial_run_hdr     ptrh
       where ptrd.payroll_dtl_id = ptrdd.payroll_dtl_id
         and ptrh.payroll_id = ptrd.payroll_id
         and ptrh.payroll_period = p_pay_period
         and ptrdd.pay_emp_id = cur_emp.emp_id
         and exists
       (select *
                from ssm_code_masters scm
               where scm.parent_code = 'ER'
                 and scm.code = ptrdd.pay_emp_element_id);
    
      select sum(ptrdd.pay_amount)
        into v_dedi
        from pay_trial_run_dtl     ptrd,
             pay_trial_run_dtl_dtl ptrdd,
             pay_trial_run_hdr     ptrh
       where ptrd.payroll_dtl_id = ptrdd.payroll_dtl_id
         and ptrh.payroll_id = ptrd.payroll_id
         and ptrh.payroll_period = p_pay_period
         and ptrdd.pay_emp_id = cur_emp.emp_id
         and exists
       (select *
                from ssm_code_masters scm
               where scm.parent_code = 'DEDI'
                 and scm.code = ptrdd.pay_emp_element_id);
    
      select sum(ptrdd.pay_amount)
        into v_dedr
        from pay_trial_run_dtl     ptrd,
             pay_trial_run_dtl_dtl ptrdd,
             pay_trial_run_hdr     ptrh
       where ptrd.payroll_dtl_id = ptrdd.payroll_dtl_id
         and ptrh.payroll_id = ptrd.payroll_id
         and ptrh.payroll_period = p_pay_period
         and ptrdd.pay_emp_id = cur_emp.emp_id
         and exists
       (select *
                from ssm_code_masters scm
               where scm.parent_code = 'DEDR'
                 and scm.code = ptrdd.pay_emp_element_id);
    
      select sum(ptrdd.pay_amount)
        into v_deds
        from pay_trial_run_dtl     ptrd,
             pay_trial_run_dtl_dtl ptrdd,
             pay_trial_run_hdr     ptrh
       where ptrd.payroll_dtl_id = ptrdd.payroll_dtl_id
         and ptrh.payroll_id = ptrd.payroll_id
         and ptrh.payroll_period = p_pay_period
         and ptrdd.pay_emp_id = cur_emp.emp_id
         and exists
       (select *
                from ssm_code_masters scm
               where scm.parent_code = 'DEDS'
                 and scm.code = ptrdd.pay_emp_element_id);
    
      select sum(ptrdd.pay_amount)
        into v_dedls
        from pay_trial_run_dtl     ptrd,
             pay_trial_run_dtl_dtl ptrdd,
             pay_trial_run_hdr     ptrh
       where ptrd.payroll_dtl_id = ptrdd.payroll_dtl_id
         and ptrh.payroll_id = ptrd.payroll_id
         and ptrh.payroll_period = p_pay_period
         and ptrdd.pay_emp_id = cur_emp.emp_id
         and exists
       (select *
                from ssm_code_masters scm
               where scm.parent_code = 'DEDLS'
                 and scm.code = ptrdd.pay_emp_element_id);
    
      select sum(ptrdd.pay_amount)
        into v_dedrec
        from pay_trial_run_dtl     ptrd,
             pay_trial_run_dtl_dtl ptrdd,
             pay_trial_run_hdr     ptrh
       where ptrd.payroll_dtl_id = ptrdd.payroll_dtl_id
         and ptrh.payroll_id = ptrd.payroll_id
         and ptrh.payroll_period = p_pay_period
         and ptrdd.pay_emp_id = cur_emp.emp_id
         and exists
       (select *
                from ssm_code_masters scm
               where scm.parent_code = 'DEDREC'
                 and scm.code = ptrdd.pay_emp_element_id);
    
      IF nvl(v_als, 0) <= 0 then
        v_tot_leave := 0;
      else
        v_tot_leave := cur_emp.pay_emp_al_days;
      end if;
    
      INSERT INTO PAY_TRIAL_RUN_EMP_DTL
      VALUES
        (cur_emp.payroll_id,
         p_pay_period,
         cur_emp.emp_id,
         round(v_salary, 5),
         v_mon_days,
         v_no_working_days,
         cur_emp.pay_emp_no_days, --v_no_working_days-v_tot_leave-v_tot_up_leave-v_tot_al_leave,
         v_tot_leave,
         round(v_wds, 3),
         round(v_als, 3),
         round(v_os, 3),
         round(v_ols, 3),
         round(v_allo, 3),
         round(v_VMM, 3),
         round(v_er, 3),
         round(nvl(v_wds, 0) + nvl(v_als, 0) + nvl(v_os, 0) + nvl(v_ols, 0) +
               nvl(v_allo, 0) + nvl(v_vmm, 0) + nvl(v_er, 0),
               3),
         round(v_DEDI, 3),
         round(v_DEDR, 3),
         round(v_DEDS, 3),
         round(v_DEDLS, 3),
         round(v_DEDREC, 3),
         round((nvl(v_wds, 0) + nvl(v_als, 0) + nvl(v_os, 0) +
               nvl(v_ols, 0) + nvl(v_allo, 0) + nvl(v_vmm, 0) +
               nvl(v_er, 0)) -
               (nvl(v_dedi, 0) + nvl(v_dedr, 0) + nvl(v_deds, 0) +
               nvl(v_dedls, 0) + nvl(v_dedrec, 0)),
               3),
         cur_emp.emp_name,
         v_dept_name,
         v_dept_id,
         v_desig_name,
         v_dept_desig_id,
         v_no_fridays,
         v_bank_name,
         cur_emp.emp_no,
         cur_emp.emp_name_ol,
         v_dept_name_ol,
         v_desig_name_ol,
         v_bank_name_ol,
         v_emp_bank_id);
    end loop;
  
    commit;
  
  end;

  Procedure payroll_process_na(p_pay_group  IN VARCHAR2,
                               p_pay_period IN VARCHAR2,
                               p_dept_list  IN VARCHAR2,
                               p_emp_id     IN VARCHAR2 DEFAULT NULL,
                               p_org_id     IN VARCHAR2) Is
  
    cursor group_list(c_pay_group IN VARCHAR2) IS
      select *
        from pay_group_dtls pgd
       where pgd.enabled_flag = '1'
         and pgd.workflow_completion_status = '1'
         and pgd.pay_group_id = nvl(p_pay_group, pgd.pay_group_id);
  
    cursor dept_list(c_dept_list IN VARCHAR2) IS
      select *
        from hr_departments hd, hr_employees
       where hd.enabled_flag = '1'
         and hd.workflow_completion_status = '1'
         and hd.dept_id in nvl(p_dept_list, hd.dept_id);
  
    cursor emp_ele_list(c_to_date   IN DATE,
                        c_dept_id   IN VARCHAR2,
                        c_pay_group IN VARCHAR2) IS
      select *
        from hr_employees he, pay_emp_element_value peev
       where he.enabled_flag = '1'
         and he.workflow_completion_status = '1'
         and nvl(he.termination_date, c_to_date) >= c_to_date
         and he.emp_id = peev.pay_emp_id
         and peev.pay_emp_dept_id = c_dept_id
         and peev.pay_group_id = c_pay_group
         and nvl(peev.effective_to_dt, c_to_date) <= c_to_date;
  
    cursor emp_leaves(c_emp_id        IN VARCHAR2,
                      c_from_date     IN DATE,
                      c_to_date       IN DATE,
                      c_week_day_off1 IN VARCHAR2,
                      c_week_day_off2 IN VARCHAR2) IS
      select *
        from hr_staff_attendance hsa
       where hsa.staff_id = c_emp_id
         and hsa.attendance_date >= c_from_date
         and hsa.attendance_date <= c_to_date
         and trim(upper(to_char(hsa.attendance_date, 'Day'))) not in
             (c_week_day_off1, c_week_day_off2);
  
    v_week_day_off1 varchar2(50);
    v_week_day_off2 varchar2(50);
  
  Begin
  
    v_week_day_off1 := ssm.get_parameter_value('WEEKOFF1');
    v_week_day_off2 := ssm.get_parameter_value('WEEKOFF2');
  
  End;
  FUNCTION leave_salary(p_pay_period IN VARCHAR2,
                        p_dept_id    IN VARCHAR2,
                        p_emp_id     IN VARCHAR2 DEFAULT NULL,
                        p_org_id     IN VARCHAR2,
                        p_from_dt    IN DATE,
                        p_to_dt      IN DATE) RETURN NUMBER IS
  
    CURSOR pay_cur(c_dept_id IN VARCHAR2, c_emp_id IN VARCHAR2) IS
      SELECT
      --grp1.pg_ele_id,
      --grp1.pay_group_id,
      --grp1.pay_element_id,
       emp.emp_doj,
       ele.pay_element_id,
       ele.pay_element_code,
       ele.pay_element_desc,
       ele.pay_ele_class,
       NVL(ele.pay_ele_recurring, '1') pay_ele_recurring,
       NVL(ele.pay_element_prorate, '1') pay_element_prorate,
       NVL(eval.pay_element_prorate, '1') emp_prorate,
       eval.pay_emp_element_id,
       eval.pay_emp_id,
       NVL(ele.unpaid_leave, '1') ele_unpaid_leave,
       NVL(eval.unpaid_leave, '1') eval_unpaid_leave,
       --eval.pay_element_id,
       eval.pay_amount,
       eval.pay_emp_dept_id,
       eval.pay_period_id,
       eval.pay_last_pay_dt
        FROM --pay_group_element_mapping grp1,
             pay_elements          ele,
             pay_emp_element_value eval,
             hr_employees          emp
       WHERE eval.pay_element_id = ele.pay_element_id
         AND emp.emp_id = eval.pay_emp_id
         AND eval.pay_emp_dept_id = c_dept_id
         AND eval.pay_emp_id = NVL(c_emp_id, eval.pay_emp_id)
         AND ele.paid_for_annual_leave = '1'
         AND eval.enabled_flag = '1'
       ORDER BY eval.pay_emp_element_id;
    v_day_sal      number := 0;
    v_is_holiday   number := 0;
    v_working_days number := 0;
    v_leave_sal    number := 0;
    v_from_dt      date;
    v_to_dt        date;
    v_weekly_off_1 varchar2(100) := ssm.get_parameter_value('WEEKOFF1');
    v_weekly_off_2 varchar2(100) := ssm.get_parameter_value('WEEKOFF2');
    v_day_comp     varchar2(50);
  begin
    v_from_dt := p_from_dt;
    v_to_dt   := p_to_dt;
  
    WHILE v_from_dt <= v_to_dt LOOP
      v_day_sal    := 0;
      v_is_holiday := 0;
    
      select count('x')
        into v_is_holiday
        from hr_holidays_master hhm
       where hhm.holiday_date = v_from_dt
         and hhm.enabled_flag = '1'
         and hhm.workflow_completion_status = '1'
         and hhm.org_id = p_org_id;
    
      v_day_comp := trim(upper(to_char((v_from_dt), 'day')));
    
      if v_is_holiday > 0 or v_day_comp = v_weekly_off_1 or
         v_day_comp = v_weekly_off_2 then
      
        null;
      else
        for leave_sal in pay_cur(p_dept_id, p_emp_id) loop
          --       if leave_sal.pay_last_pay_dt < v_from_dt then
        
          v_working_days := get_days(p_from_dt => trunc(v_from_dt, 'mm'),
                                     p_to_dt   => last_day(v_from_dt));
        
          v_day_sal := v_day_sal + leave_sal.pay_amount / 26; --v_working_days;
        --  end if;
        end loop;
      
      end if;
    
      v_leave_sal := v_leave_sal + v_day_sal;
      v_from_dt   := v_from_dt + 1;
    end loop;
    return round(v_leave_sal, 3);
  end leave_salary;
  PROCEDURE payroll_social_trial(p_org_id     IN VARCHAR2,
                                 p_pay_period IN VARCHAR2) IS
    v_pay_from      date;
    v_cal_id        varchar2(50);
    v_basic_amt     number;
    v_mgrp_amt      number;
    v_max_ins_sal   number;
    v_sup_sal       number;
    v_tot_sal       number;
    v_ins_sal       number;
    v_emp_per       number;
    v_emr_per       number;
    v_bas_emp       number;
    v_bas_emr       number;
    v_sup_emp       number;
    v_sup_emr       number;
    v_bas_sup_emp   number;
    v_bas_sup_emr   number;
    v_pension_emp   number;
    v_pension_emr   number;
    v_unemploy_emp  number;
    v_unemploy_emr  number;
    v_indemnity_emp number;
  BEGIN
  
    delete pay_employee_ssd_details pesd
     where pesd.pay_period = p_pay_period;
    commit;
  
    select pp.pay_from_dt
      into v_pay_from
      from pay_periods pp
     where pp.pay_period_id = p_pay_period;
  
    select gcacd.cal_dtl_id
      into v_cal_id
      from gl_comp_acct_calendar_dtl gcacd, gl_acct_calendar_dtl gacd
     where gcacd.cal_dtl_id = gacd.cal_dtl_id
       and v_pay_from between gacd.cal_eff_start_dt and
           gacd.call_eff_end_dt
       and gcacd.comp_id = p_org_id;
    for emp_dtl in (select he.emp_id,
                           he.emp_no,
                           he.emp_first_name,
                           he.emp_middle_name,
                           he.emp_last_name,
                           he.emp_first_name_ol,
                           he.emp_middle_name_ol,
                           he.emp_last_name_ol,
                           hdd.desig_name
                      from hr_employees         he,
                           hr_emp_work_dtls     hewd,
                           hr_dept_designations hdd
                     where he.enabled_flag = '1'
                       and he.workflow_completion_status = '1'
                       and he.ssd_flag = '1'
                       and he.emp_id = hewd.emp_id
                       and hewd.effective_to_dt is null
                       and hewd.enabled_flag = '1'
                       and hewd.workflow_completion_status = '1'
                       and hewd.emp_desig_id = hdd.dept_desig_id
                       and nvl(he.stop_paytoll, '0') = '0') loop
    
      v_basic_amt := get_basic_amt(p_org_id => p_org_id,
                                   p_emp_id => emp_dtl.emp_id);
    
      v_mgrp_amt := get_mgrp_amt(p_org_id => p_org_id,
                                 p_emp_id => emp_dtl.emp_id,
                                 p_year   => v_cal_id);
    
      v_tot_sal := nvl(v_basic_amt, 0) + nvl(v_mgrp_amt, 0);
    
      v_max_ins_sal := to_number(ssm.get_parameter_value(p_param_code => 'MAX_INS_SAL'));
    
      if v_tot_sal > v_max_ins_sal then
      
        v_ins_sal := v_max_ins_sal;
        v_sup_sal := v_tot_sal - v_max_ins_sal;
      
      else
        v_ins_sal := v_tot_sal;
        v_sup_sal := 0;
      end if;
    
      select sso.ssdp_employee, sso.ssdp_employer
        into v_emp_per, v_emr_per
        from ssm_system_options sso;
    
      v_bas_emp := v_ins_sal * (v_emp_per / 100);
      v_bas_emr := v_ins_sal * (v_emr_per / 100);
    
      v_sup_emp := v_sup_sal * (v_emp_per / 100);
      v_sup_emr := v_sup_sal * (v_emr_per / 100);
    
      v_bas_sup_emp := v_tot_sal * (v_emp_per / 100);
      v_bas_sup_emr := v_tot_sal * (v_emr_per / 100);
    
      v_pension_emp := v_bas_sup_emp * 0.50;
      v_pension_emr := v_bas_sup_emr * 0.10;
    
      v_unemploy_emp := v_bas_sup_emp * 0.10;
      v_unemploy_emr := v_bas_sup_emr * 0.05;
    
      v_indemnity_emp := v_ins_sal * (2.5 / 100);
    
      insert into pay_employee_ssd_details
        (emp_no,
         emp_name,
         emp_name_ol,
         job_title,
         basic_salary,
         family_allowance,
         max_insur_basic,
         max_insur_supp,
         bas_insur_salary,
         bas_emp_share,
         bas_comp_share,
         sup_insur_salary,
         sup_emp_share,
         sup_comp_share,
         emp_bas_supp,
         emp_inc_pension,
         emp_unempl_support,
         comp_bas_supp,
         comp_inc_pension,
         comp_unempl_support,
         ssdp_emp_per,
         ssdp_comp_per,
         pay_period,
         emp_id,
         org_id,
         emp_indemnity)
      values
        (emp_dtl.emp_no,
         emp_dtl.emp_first_name || ' ' || emp_dtl.emp_middle_name || ' ' ||
         emp_dtl.emp_last_name,
         emp_dtl.emp_first_name_ol || ' ' || emp_dtl.emp_middle_name_ol || ' ' ||
         emp_dtl.emp_last_name_ol,
         emp_dtl.desig_name,
         v_basic_amt,
         v_mgrp_amt,
         v_ins_sal,
         v_sup_sal,
         v_ins_sal,
         v_bas_emp,
         v_bas_emr,
         v_sup_sal,
         v_sup_emp,
         v_sup_emr,
         v_bas_sup_emp,
         v_pension_emp,
         v_unemploy_emp,
         v_bas_sup_emr,
         v_pension_emr,
         v_unemploy_emr,
         v_emp_per,
         v_emr_per,
         p_pay_period,
         emp_dtl.emp_id,
         p_org_id,
         v_indemnity_emp);
    end loop;
  
  END;
END PKG_PAYROLL;
/
