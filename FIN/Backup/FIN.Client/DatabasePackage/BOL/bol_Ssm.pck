CREATE OR REPLACE PACKAGE Ssm IS

  PROCEDURE get_next_sequence(screencode VARCHAR2,
                              ERRMESG    OUT VARCHAR2,
                              TXNCODE    OUT VARCHAR2);

  FUNCTION get_next_sequence(screencode VARCHAR2, lang VARCHAR2)
    RETURN VARCHAR2;

  FUNCTION get_parameter_value(p_param_code VARCHAR2) RETURN VARCHAR2;

  FUNCTION Get_Err_Message(v_langcode VARCHAR2, msg_code VARCHAR2)
    RETURN VARCHAR2;

  FUNCTION get_currency_format(p_currency VARCHAR2) RETURN VARCHAR2;

  FUNCTION get_list_query(p_screen_code IN VARCHAR2, P_ORG_ID VARCHAR2)
    RETURN VARCHAR2;

  FUNCTION get_rep_list_query(p_screen_code IN VARCHAR2, P_ORG_ID VARCHAR2)
    RETURN VARCHAR2;

  FUNCTION GET_FILTER_LIST_QUERY(P_SCREEN_CODE IN VARCHAR2,
                                 P_ORG_ID      VARCHAR2,
                                 P_COLNAME     VARCHAR2,
                                 P_COLVALUE    VARCHAR2) RETURN VARCHAR2;
    procedure get_next_sequence_gl(screencode VARCHAR2, lang VARCHAR2,P_PERIOD_ID VARCHAR2,P_MODULE VARCHAR2,P_ORG_ID IN VARCHAR2,P_SEQ_CODE OUT VARCHAR2);

end SSM;
/
CREATE OR REPLACE PACKAGE BODY Ssm AS
  PROCEDURE get_next_sequence(screencode VARCHAR2,
                              ERRMESG    OUT VARCHAR2,
                              TXNCODE    OUT VARCHAR2) IS
    prefix_vr                  VARCHAR2(20);
    start_with_vr              NUMBER(25);
    increment_by_vr            NUMBER(3);
    current_document_number_vr NUMBER(25);
    code                       VARCHAR2(50);
    status_fg                  NUMBER(2) := 0;
    v_width                    VARCHAR2(50);
  BEGIN
    BEGIN
      SELECT sequence_format,
             start_with,
             --increment_by,
             current_document_number,
             width
        INTO prefix_vr,
             start_with_vr,
             --increment_by_vr,
             current_document_number_vr,
             v_width
        FROM SSM_SEQUENCES
       WHERE sequence_code = screencode
         AND workflow_completion_status = 1
         AND enabled_flag = 1;

      status_fg := 1;
    EXCEPTION
      WHEN TOO_MANY_ROWS THEN
        SELECT sequence_format,
               start_with,
               --increment_by,
               current_document_number,
               width
          INTO prefix_vr,
               start_with_vr,
               --`increment_by_vr,
               current_document_number_vr,
               v_width
          FROM SSM_SEQUENCES SS
         WHERE sequence_code = screencode
           AND workflow_completion_status = 1
           AND enabled_flag = 1
           AND SS.CREATED_BY = 'ADMIN';

        status_fg := 1;
      WHEN NO_DATA_FOUND THEN
        errmesg   := 'Seed Data not available in Sequence Master for the Screen Code : ' ||
                     screencode;
        status_fg := 0;
    END;

    IF NVL(status_fg, 0) = 1 THEN
      IF INSTR(prefix_vr, 'YYYY') > 0 THEN
        SELECT REPLACE(prefix_vr, 'YYYY', TO_CHAR(SYSDATE, 'YYYY'))
          INTO prefix_vr
          FROM DUAL;
      END IF;

      code := prefix_vr || '-' ||
              LPAD(LTRIM(RTRIM(NVL(current_document_number_vr, 0) +
                               NVL(increment_by_vr, 0))),
                   v_width,
                   '0');

      --DBMS_OUTPUT.PUT_LINE('Code '||code);
      -- Removed on 14/06/2006
      /*  if screencode in ('SSMF0009','SSMF0007') then
        code := prefix_vr||'_'||LTRIM(RTRIM(TO_CHAR(NVL(current_document_number_vr,0)+NVL(increment_by_vr,0),'009')));
      else
            code := prefix_vr||'_'||LTRIM(RTRIM(TO_CHAR(NVL(current_document_number_vr,0)+NVL(increment_by_vr,0),'0000000009')));
      end if; */
      UPDATE SSM_SEQUENCES
         SET current_document_number = NVL(current_document_number, 0) + 1
       WHERE sequence_code = screencode;

      TXNCODE := code;
      errmesg := 0;
    END IF;
  EXCEPTION
    WHEN OTHERS THEN
      errmesg := 'Unable TO generate code , Please contact administrator';
  END; -- ***get_next_sequence

  FUNCTION get_next_sequence(screencode VARCHAR2, lang VARCHAR2)
    RETURN VARCHAR2 IS
    prefix_vr                  VARCHAR2(20);
    start_with_vr              NUMBER(25);
    increment_by_vr            NUMBER(3);
    current_document_number_vr NUMBER(25);
    code                       VARCHAR2(50);
    status_fg                  NUMBER(2) := 0;
    V_WIDTH                    VARCHAR2(50);
    errmesg                    VARCHAR2(1000);
  BEGIN

    -- LOCK TABLE SSM_SEQUENCES IN EXCLUSIVE MODE;
    BEGIN
      SELECT sequence_format,
             start_with,
             --increment_by,
             current_document_number,
             width
        INTO prefix_vr,
             start_with_vr,
             --increment_by_vr,
             current_document_number_vr,
             v_width
        FROM SSM_SEQUENCES
       WHERE sequence_code = screencode
         AND workflow_completion_status = 1
         AND enabled_flag = 1
         FOR UPDATE;

      status_fg := 1;
    EXCEPTION
      WHEN TOO_MANY_ROWS THEN
        SELECT sequence_format,
               start_with,
               --increment_by,
               current_document_number,
               width
          INTO prefix_vr,
               start_with_vr,
               --increment_by_vr,
               current_document_number_vr,
               v_width
          FROM SSM_SEQUENCES SS
         WHERE sequence_code = screencode
           AND workflow_completion_status = 1
           AND enabled_flag = 1
           AND SS.CREATED_BY = 'ADMIN'
           FOR UPDATE;

        status_fg := 1;
      WHEN NO_DATA_FOUND THEN
        errmesg   := 'Seed Data not available in Sequence Master for the Screen Code : ' ||
                     screencode;
        status_fg := 0;
    END;

    IF NVL(status_fg, 0) = 1 THEN
      IF INSTR(prefix_vr, 'YYYY') > 0 THEN
        SELECT REPLACE(prefix_vr, 'YYYY', TO_CHAR(SYSDATE, 'YYYY'))
          INTO prefix_vr
          FROM DUAL;
      END IF;

      code := prefix_vr || '-' ||
              LPAD(LTRIM(RTRIM(NVL(current_document_number_vr, 0) +
                               NVL(increment_by_vr, 0))),
                   v_width,
                   '0');

      --       code := prefix_vr||'_'||LTRIM(RTRIM(NVL(current_document_number_vr,0)+NVL(increment_by_vr,0)));--,v_width,'0');
      --DBMS_OUTPUT.PUT_LINE('Code '||code);
      -- Removed on 14/06/2006
      /*  if screencode in ('SSMF0009','SSMF0007') then
        code := prefix_vr||'_'||LTRIM(RTRIM(TO_CHAR(NVL(current_document_number_vr,0)+NVL(increment_by_vr,0),'009')));
      else
            code := prefix_vr||'_'||LTRIM(RTRIM(TO_CHAR(NVL(current_document_number_vr,0)+NVL(increment_by_vr,0),'0000000009')));
      end if; */
      UPDATE SSM_SEQUENCES
         SET current_document_number = NVL(current_document_number, 0) + 1
       WHERE sequence_code = screencode;

      --            TXNCODE := code;
      RETURN CODE;
      errmesg := 0;
    END IF;

    RETURN 'Error';
  EXCEPTION
    WHEN OTHERS THEN
      ERRMESG := 'Unable TO generate code , Please contact administrator'; --||substr(sqlerrm,1,200);
      RETURN errmesg;
  END; -- ***get_next_sequence

  procedure get_next_sequence_gl(screencode VARCHAR2, lang VARCHAR2,P_PERIOD_ID VARCHAR2,P_MODULE VARCHAR2,P_ORG_ID IN VARCHAR2,P_SEQ_CODE OUT VARCHAR2)
    IS
    prefix_vr                  VARCHAR2(20);
    start_with_vr              NUMBER(25);
    increment_by_vr            NUMBER(3) :=1;
    current_document_number_vr NUMBER(25);
    code                       VARCHAR2(50);
    status_fg                  NUMBER(2) := 0;
    V_WIDTH                    VARCHAR2(50);
    errmesg                    VARCHAR2(1000);
    v_period_name varchar2(50);
  BEGIN

    -- LOCK TABLE SSM_SEQUENCES IN EXCLUSIVE MODE;
    BEGIN
      SELECT sequence_format,
             start_with,
             --increment_by,
             current_document_number,
             width
        INTO prefix_vr,
             start_with_vr,
             --increment_by_vr,
             current_document_number_vr,
             v_width
        FROM SSM_SEQUENCES
       WHERE sequence_code = screencode
         AND workflow_completion_status = 1
         AND enabled_flag = 1
         for update;

      status_fg := 1;
    EXCEPTION
      WHEN TOO_MANY_ROWS THEN
        SELECT sequence_format,
               start_with,
               --increment_by,
               current_document_number,
               width
          INTO prefix_vr,
               start_with_vr,
               --increment_by_vr,
               current_document_number_vr,
               v_width
          FROM SSM_SEQUENCES SS
         WHERE sequence_code = screencode
           AND workflow_completion_status = 1
           AND enabled_flag = 1
           AND SS.CREATED_BY = 'ADMIN'
           for update;

        status_fg := 1;
      WHEN NO_DATA_FOUND THEN
        errmesg   := 'Seed Data not available in Sequence Master for the Screen Code : ' ||
                     screencode;
        status_fg := 0;
    END;

    IF NVL(status_fg, 0) = 1 THEN
      IF INSTR(prefix_vr, 'YYYY') > 0 THEN
        SELECT REPLACE(prefix_vr, 'YYYY', TO_CHAR(SYSDATE, 'YYYY'))
          INTO prefix_vr
          FROM DUAL;
      END IF;

begin
select count('x')
into current_document_number_vr
from gl_journal_hdr gjh
where gjh.je_comp_id=P_ORG_ID;
exception when others then
  current_document_number_vr:=0;
  end;

  begin
    select gcapd.period_name into v_period_name from gl_comp_acct_period_dtl gcapd where gcapd.period_id=P_PERIOD_ID;
    exception when others then v_period_name:=null;
    end;
      code := prefix_vr || '-' ||v_period_name||'-'||
              LPAD(LTRIM(RTRIM(NVL(current_document_number_vr, 0) +
                               NVL(increment_by_vr, 0))),
                   v_width,
                   '0')||'/'||P_MODULE;

      --       code := prefix_vr||'_'||LTRIM(RTRIM(NVL(current_document_number_vr,0)+NVL(increment_by_vr,0)));--,v_width,'0');
      --DBMS_OUTPUT.PUT_LINE('Code '||code);
      -- Removed on 14/06/2006
      /*  if screencode in ('SSMF0009','SSMF0007') then
        code := prefix_vr||'_'||LTRIM(RTRIM(TO_CHAR(NVL(current_document_number_vr,0)+NVL(increment_by_vr,0),'009')));
      else
            code := prefix_vr||'_'||LTRIM(RTRIM(TO_CHAR(NVL(current_document_number_vr,0)+NVL(increment_by_vr,0),'0000000009')));
      end if; */
      --            TXNCODE := code;
      P_SEQ_CODE:= CODE;
      errmesg := 0;
    END IF;


  EXCEPTION
    WHEN OTHERS THEN
      ERRMESG := 'Unable TO generate code , Please contact administrator'; --||substr(sqlerrm,1,200);

  END; -- ***get_next_sequence

  FUNCTION get_parameter_value(P_PARAM_CODE VARCHAR2) RETURN VARCHAR2 IS
    PARAMETER_VALUE VARCHAR2(100);
  BEGIN
    SELECT PARAM_VALUE
      INTO PARAMETER_VALUE
      FROM SSM_SYSTEM_PARAMETERS
     WHERE PARAM_CODE = P_PARAM_CODE
       AND ENABLED_FLAG = '1'
       AND WORKFLOW_COMPLETION_STATUS = '1';

    RETURN PARAMETER_VALUE;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN 'Not Define';
  END;

  FUNCTION Get_Err_Message(v_langcode VARCHAR2, msg_code VARCHAR2)
    RETURN VARCHAR2 IS
    v_errmsg VARCHAR2(200);
    --v_langcode varchar2(5) :=
  BEGIN
    SELECT ERROR_MESSAGE_DESCRIPTION
      INTO v_errmsg
      FROM SSM_ERROR_MESSAGES
     WHERE ERROR_MESSAGE_CODE = msg_code
       AND LANGUAGE_CODE = v_langcode
       AND WORKFLOW_COMPLETION_STATUS = 1;

    -- RETURN msg_code || ' -: ' || v_errmsg;
    RETURN v_errmsg;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      v_errmsg := msg_code || ' -: ' || 'Not Defined';
      RETURN v_errmsg;
    WHEN TOO_MANY_ROWS THEN
      v_errmsg := msg_code || ' -: ' ||
                  'Too Many Error message for same Code';
      RETURN v_errmsg;
  END;

  FUNCTION get_currency_format(P_CURRENCY VARCHAR2) RETURN VARCHAR2 IS
    V_STANDARD_PRECISION NUMBER := 4;
    V_FORMAT             VARCHAR2(100);
    V_CURRENCY           VARCHAR2(50) := P_CURRENCY;
  BEGIN
    IF V_CURRENCY IS NULL THEN
      V_CURRENCY := Ssm.GET_PARAMETER_VALUE('SP052');
    END IF;

    BEGIN
      SELECT STANDARD_PRECISION
        INTO V_STANDARD_PRECISION
        FROM SSM_CURRENCIES
       WHERE CURRENCY_CODE = V_CURRENCY;
    EXCEPTION
      WHEN OTHERS THEN
        V_STANDARD_PRECISION := 0;
    END;

    v_format := NULL;

    FOR I IN 1 .. V_STANDARD_PRECISION LOOP
      v_format := v_format || 0;
    END LOOP;

    v_format := 'fm999,999,999,999,999,999.' || v_format;
    RETURN v_format;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN 'fm999,999,999,999,999,999';
  END GET_CURRENCY_FORMAT;

  FUNCTION GET_LIST_QUERY(P_SCREEN_CODE IN VARCHAR2, P_ORG_ID VARCHAR2)
    RETURN VARCHAR2 IS
    CURSOR LIST_CUR(c_screen_code IN VARCHAR2) IS
      SELECT a.LIST_HDR_ID,
             a.LIST_SCREEN_CODE,
             a.LIST_SCREEN_NAME,
             a.LIST_TABLE_NAME,
             b.list_dtl_id,
             b.list_column_name,
             b.list_display,
             B.MASTER_TABLE,
             B.HDR_DTL_LINK,
             b.COLUMN_DISPLAY_FORMAT,
             --  b.COLUMN_DISPLAY_FORMAT,a.org_id_column_name
             (CASE
               WHEN a.org_id_column_name IS NULL THEN
                NULL
               ELSE
                a.LIST_TABLE_NAME || '.' || a.org_id_column_name
             END) AS org_id_column_name
             ,a.attribute1
        FROM SSM_LIST_HDR a, SSM_LIST_DTL B
       WHERE a.LIST_HDR_ID = B.LIST_HDR_ID
         AND a.list_screen_code = C_SCREEN_CODE
         AND b.list_display = '1'
         AND b.display_column_nm IS NOT NULL
         AND a.LIST_TABLE_NAME IS NOT NULL
       ORDER BY b.order_no ASC;

    V_QUERY              VARCHAR2(5000);
    v_tab_name           VARCHAR2(300);
    v_MASTER_TABLE       VARCHAR2(500);
    v_HDR_DTL_LINK_TABLE VARCHAR2(500);
    v_Column_Name        VARCHAR2(500);
    v_org_column_name    VARCHAR2(500);
    V_EXTA_TABLE         VARCHAR2(50);
  BEGIN
    V_QUERY := 'SELECT DISTINCT ';

    FOR L_CUR IN LIST_CUR(P_SCREEN_CODE) LOOP
      v_org_column_name := L_CUR.Org_Id_Column_Name;
      V_TAB_NAME        := L_CUR.LIST_TABLE_NAME;
     V_EXTA_TABLE :=L_CUR.ATTRIBUTE1;

      IF L_CUR.COLUMN_DISPLAY_FORMAT IS NOT NULL THEN
        v_Column_Name := 'to_char(' || L_CUR.LIST_COLUMN_NAME ||
                         ',''dd/MM/yyyy'') as ' || L_CUR.LIST_COLUMN_NAME;
      ELSE
        v_Column_Name := L_CUR.LIST_COLUMN_NAME;
      END IF;

      IF L_CUR.MASTER_TABLE IS NOT NULL THEN
        V_QUERY := V_QUERY || L_CUR.MASTER_TABLE || '.' || v_Column_Name || ',';

        v_MASTER_TABLE := v_MASTER_TABLE || L_CUR.MASTER_TABLE || ',';

        v_HDR_DTL_LINK_TABLE := v_HDR_DTL_LINK_TABLE || L_CUR.HDR_DTL_LINK ||
                                ' and ';

        DBMS_OUTPUT.put_line('v_MASTER_TABLE :' || CHR(10) ||
                             v_MASTER_TABLE);
      ELSIF L_CUR.MASTER_TABLE IS NULL AND L_CUR.HDR_DTL_LINK IS NOT NULL THEN
        V_QUERY := V_QUERY || v_Column_Name || ',';

        v_HDR_DTL_LINK_TABLE := v_HDR_DTL_LINK_TABLE || L_CUR.HDR_DTL_LINK ||
                                ' and ';
      ELSE
        V_QUERY := V_QUERY || v_Column_Name || ' , ';
      END IF;
    END LOOP;

    V_QUERY := V_QUERY || '''' || 'MODIFYDELETE' || '''';
    V_QUERY := RTRIM(V_QUERY, ',');
    V_QUERY := V_QUERY || ' FROM ' || V_TAB_NAME;

 if LENGTH(V_EXTA_TABLE) >3 then
       V_QUERY := V_QUERY || ',' ||V_EXTA_TABLE;
      end if ;

    IF v_MASTER_TABLE IS NOT NULL THEN
      v_HDR_DTL_LINK_TABLE := SUBSTR(v_HDR_DTL_LINK_TABLE,
                                     0,
                                     LENGTH(v_HDR_DTL_LINK_TABLE) - 4);
      v_MASTER_TABLE       := SUBSTR(v_MASTER_TABLE,
                                     0,
                                     LENGTH(v_MASTER_TABLE) - 1);
      V_QUERY              := V_QUERY || ',' || v_MASTER_TABLE || ' WHERE ' ||
                              v_HDR_DTL_LINK_TABLE;
    ELSIF v_MASTER_TABLE IS NULL AND v_HDR_DTL_LINK_TABLE IS NOT NULL THEN
      v_HDR_DTL_LINK_TABLE := SUBSTR(v_HDR_DTL_LINK_TABLE,
                                     0,
                                     LENGTH(v_HDR_DTL_LINK_TABLE) - 4);

      V_QUERY := V_QUERY || ' WHERE ' || v_HDR_DTL_LINK_TABLE;
    END IF;

    IF NVL(v_org_column_name, 'NVL') = 'NVL' THEN
      IF NVL(v_HDR_DTL_LINK_TABLE, 'NVL') = 'NVL' THEN

        IF P_SCREEN_CODE <> 'AP_014' THEN
          v_query := v_query || ' where ' || V_TAB_NAME ||
                     '.workflow_completion_status=1';
        END IF;

      ELSE

        IF P_SCREEN_CODE <> 'AP_014' THEN
          v_query := v_query || ' and ' || V_TAB_NAME ||
                     '.workflow_completion_status=1';
        END IF;
      END IF;

    ELSE

      IF NVL(v_HDR_DTL_LINK_TABLE, 'NVL') = 'NVL' THEN
        v_query := v_query || ' where ' || v_org_column_name || ' = ''' ||
                   p_org_id || '''';

        IF P_SCREEN_CODE <> 'AP_014' AND P_SCREEN_CODE <> 'GL_015' THEN
          v_query := v_query || ' and ' || V_TAB_NAME ||
                     '.workflow_completion_status=1';
        END IF;

      ELSE

        v_query := v_query || ' and ' || v_org_column_name || ' = ''' ||
                   p_org_id || '''';
        IF P_SCREEN_CODE <> 'AP_014' AND P_SCREEN_CODE <> 'GL_015' THEN
          v_query := v_query || ' and ' || V_TAB_NAME ||
                     '.workflow_completion_status=1';
        END IF;

      END IF;
    END IF;

    V_QUERY := V_QUERY || '  order by DeleteURL desc';
    RETURN v_query;
  END GET_LIST_QUERY;

  FUNCTION GET_REP_LIST_QUERY(P_SCREEN_CODE IN VARCHAR2, P_ORG_ID VARCHAR2)
    RETURN VARCHAR2 IS
    CURSOR LIST_CUR(c_screen_code IN VARCHAR2) IS
      SELECT a.LIST_HDR_ID,
             a.LIST_SCREEN_CODE,
             a.LIST_SCREEN_NAME,
             a.LIST_TABLE_NAME,
             b.list_dtl_id,
             b.list_column_name,
             b.list_display,
             B.MASTER_TABLE,
             B.HDR_DTL_LINK,
             b.COLUMN_DISPLAY_FORMAT,
             --  b.COLUMN_DISPLAY_FORMAT,a.org_id_column_name
             (CASE
               WHEN a.org_id_column_name IS NULL THEN
                NULL
               ELSE
                a.LIST_TABLE_NAME || '.' || a.org_id_column_name
             END) AS org_id_column_name

        FROM SSM_REP_LIST_HDR a, SSM_REP_LIST_DTL B
       WHERE a.LIST_HDR_ID = B.LIST_HDR_ID
         AND a.LIST_HDR_ID = C_SCREEN_CODE
         AND b.list_display = '1'
         AND b.display_column_nm IS NOT NULL
         AND a.LIST_TABLE_NAME IS NOT NULL
       ORDER BY b.pk_id ASC;

    V_QUERY              VARCHAR2(5000);
    v_tab_name           VARCHAR2(300);
    v_MASTER_TABLE       VARCHAR2(500);
    v_HDR_DTL_LINK_TABLE VARCHAR2(500);
    v_Column_Name        VARCHAR2(500);
    v_org_column_name    VARCHAR2(500);
  BEGIN
    V_QUERY := 'SELECT ';

    FOR L_CUR IN LIST_CUR(P_SCREEN_CODE) LOOP
      v_org_column_name := L_CUR.Org_Id_Column_Name;
      V_TAB_NAME        := L_CUR.LIST_TABLE_NAME;

      IF L_CUR.COLUMN_DISPLAY_FORMAT IS NOT NULL THEN
        v_Column_Name := 'to_char(' || L_CUR.LIST_COLUMN_NAME ||
                         ',''dd/MM/yyyy'') as ' || L_CUR.LIST_COLUMN_NAME;
      ELSE
        v_Column_Name := L_CUR.LIST_COLUMN_NAME;
      END IF;

      IF L_CUR.MASTER_TABLE IS NOT NULL THEN
        V_QUERY := V_QUERY || L_CUR.MASTER_TABLE || '.' || v_Column_Name || ',';

        v_MASTER_TABLE := v_MASTER_TABLE || L_CUR.MASTER_TABLE || ',';

        v_HDR_DTL_LINK_TABLE := v_HDR_DTL_LINK_TABLE || L_CUR.HDR_DTL_LINK ||
                                ' and ';

        DBMS_OUTPUT.put_line('v_MASTER_TABLE :' || CHR(10) ||
                             v_MASTER_TABLE);
      ELSIF L_CUR.MASTER_TABLE IS NULL AND L_CUR.HDR_DTL_LINK IS NOT NULL THEN
        V_QUERY := V_QUERY || v_Column_Name || ',';

        v_HDR_DTL_LINK_TABLE := v_HDR_DTL_LINK_TABLE || L_CUR.HDR_DTL_LINK ||
                                ' and ';
      ELSE
        V_QUERY := V_QUERY || v_Column_Name || ' , ';
      END IF;
    END LOOP;

    V_QUERY := V_QUERY || '''' || 'MODIFYDELETE' || '''';
    V_QUERY := RTRIM(V_QUERY, ',');
    V_QUERY := V_QUERY || ' FROM ' || V_TAB_NAME;

    IF v_MASTER_TABLE IS NOT NULL THEN
      v_HDR_DTL_LINK_TABLE := SUBSTR(v_HDR_DTL_LINK_TABLE,
                                     0,
                                     LENGTH(v_HDR_DTL_LINK_TABLE) - 4);
      v_MASTER_TABLE       := SUBSTR(v_MASTER_TABLE,
                                     0,
                                     LENGTH(v_MASTER_TABLE) - 1);
      V_QUERY              := V_QUERY || ',' || v_MASTER_TABLE || ' WHERE ' ||
                              v_HDR_DTL_LINK_TABLE;
    ELSIF v_MASTER_TABLE IS NULL AND v_HDR_DTL_LINK_TABLE IS NOT NULL THEN
      v_HDR_DTL_LINK_TABLE := SUBSTR(v_HDR_DTL_LINK_TABLE,
                                     0,
                                     LENGTH(v_HDR_DTL_LINK_TABLE) - 4);

      V_QUERY := V_QUERY || ' WHERE ' || v_HDR_DTL_LINK_TABLE;
    END IF;

    IF NVL(v_org_column_name, 'NVL') = 'NVL' THEN
      IF NVL(v_HDR_DTL_LINK_TABLE, 'NVL') = 'NVL' THEN
        v_query := v_query || ' where ' || V_TAB_NAME ||
                   '.workflow_completion_status=1';
      ELSE
        v_query := v_query || ' and ' || V_TAB_NAME ||
                   '.workflow_completion_status=1';
      END IF;

    ELSE
      IF NVL(v_HDR_DTL_LINK_TABLE, 'NVL') = 'NVL' THEN
        v_query := v_query || ' where ' || v_org_column_name || ' = ''' ||
                   p_org_id || ''' and ' || V_TAB_NAME ||
                   '.workflow_completion_status=1';
      ELSE
        v_query := v_query || ' and ' || v_org_column_name || ' = ''' ||
                   p_org_id || ''' and ' || V_TAB_NAME ||
                   '.workflow_completion_status=1';
      END IF;
    END IF;

    V_QUERY := V_QUERY || '  order by DeleteURL desc';
    RETURN v_query;
  END GET_REP_LIST_QUERY;

  FUNCTION GET_FILTER_LIST_QUERY(P_SCREEN_CODE IN VARCHAR2,
                                 P_ORG_ID      VARCHAR2,
                                 P_COLNAME     VARCHAR2,
                                 P_COLVALUE    VARCHAR2) RETURN VARCHAR2 IS
    CURSOR LIST_CUR(c_screen_code IN VARCHAR2) IS
      SELECT a.LIST_HDR_ID,
             a.LIST_SCREEN_CODE,
             a.LIST_SCREEN_NAME,
             a.LIST_TABLE_NAME,
             b.list_dtl_id,
             b.list_column_name,
             b.list_display,
             B.MASTER_TABLE,
             B.HDR_DTL_LINK,
             b.COLUMN_DISPLAY_FORMAT,
             --  b.COLUMN_DISPLAY_FORMAT,a.org_id_column_name
             (CASE
               WHEN a.org_id_column_name IS NULL THEN
                NULL
               ELSE
                a.LIST_TABLE_NAME || '.' || a.org_id_column_name
             END) AS org_id_column_name
              ,a.attribute1
        FROM SSM_LIST_HDR a, SSM_LIST_DTL B
       WHERE a.LIST_HDR_ID = B.LIST_HDR_ID
         AND a.list_screen_code = C_SCREEN_CODE
         AND b.list_display = '1'
         AND b.display_column_nm IS NOT NULL
         AND a.LIST_TABLE_NAME IS NOT NULL
       ORDER BY b.ORDER_NO ASC;

    V_QUERY              VARCHAR2(5000);
    v_tab_name           VARCHAR2(300);
    v_MASTER_TABLE       VARCHAR2(500);
    v_HDR_DTL_LINK_TABLE VARCHAR2(500);
    v_Column_Name        VARCHAR2(500);
    v_org_column_name    VARCHAR2(500);
    V_EXTA_TABLE         VARCHAR2(50);
  BEGIN
    V_QUERY := 'SELECT DISTINCT ';

    FOR L_CUR IN LIST_CUR(P_SCREEN_CODE) LOOP
      v_org_column_name := L_CUR.Org_Id_Column_Name;
      V_TAB_NAME        := L_CUR.LIST_TABLE_NAME;
  V_EXTA_TABLE :=L_CUR.ATTRIBUTE1;
      IF L_CUR.COLUMN_DISPLAY_FORMAT IS NOT NULL THEN
        v_Column_Name := 'to_char(' || L_CUR.LIST_COLUMN_NAME ||
                         ',''dd/MM/yyyy'') as ' || L_CUR.LIST_COLUMN_NAME;
      ELSE
        v_Column_Name := L_CUR.LIST_COLUMN_NAME;
      END IF;

      IF L_CUR.MASTER_TABLE IS NOT NULL THEN
        V_QUERY := V_QUERY || L_CUR.MASTER_TABLE || '.' || v_Column_Name || ',';

        v_MASTER_TABLE := v_MASTER_TABLE || L_CUR.MASTER_TABLE || ',';

        v_HDR_DTL_LINK_TABLE := v_HDR_DTL_LINK_TABLE || L_CUR.HDR_DTL_LINK ||
                                ' and ';

        IF P_COLNAME = L_CUR.LIST_COLUMN_NAME THEN
          IF L_CUR.COLUMN_DISPLAY_FORMAT IS NULL THEN
            v_HDR_DTL_LINK_TABLE := v_HDR_DTL_LINK_TABLE || 'UPPER(' ||
                                    L_CUR.MASTER_TABLE || '.' ||
                                    L_CUR.LIST_COLUMN_NAME || ')' ||
                                    ' LIKE(''%' || P_COLVALUE ||
                                    '%'') and ';
          END IF;
        END IF;
        DBMS_OUTPUT.put_line('v_MASTER_TABLE :' || CHR(10) ||
                             v_MASTER_TABLE);
      ELSIF L_CUR.MASTER_TABLE IS NULL AND L_CUR.HDR_DTL_LINK IS NOT NULL THEN
        V_QUERY := V_QUERY || v_Column_Name || ',';

        v_HDR_DTL_LINK_TABLE := v_HDR_DTL_LINK_TABLE || L_CUR.HDR_DTL_LINK ||
                                ' and ';

        IF P_COLNAME = L_CUR.LIST_COLUMN_NAME THEN
          IF L_CUR.COLUMN_DISPLAY_FORMAT IS NULL THEN
            v_HDR_DTL_LINK_TABLE := v_HDR_DTL_LINK_TABLE || 'UPPER(' ||
                                    L_CUR.LIST_COLUMN_NAME || ')' ||
                                    ' LIKE(''%' || P_COLVALUE ||
                                    '%'') and ';
          END IF;
        END IF;
      ELSE
        V_QUERY := V_QUERY || v_Column_Name || ' , ';

        IF P_COLNAME = L_CUR.LIST_COLUMN_NAME THEN
          IF L_CUR.COLUMN_DISPLAY_FORMAT IS NULL THEN
            v_HDR_DTL_LINK_TABLE := v_HDR_DTL_LINK_TABLE || 'UPPER(' ||
                                    L_CUR.LIST_COLUMN_NAME || ')' ||
                                    ' LIKE(''%' || P_COLVALUE ||
                                    '%'') and ';
          END IF;
        END IF;
      END IF;
    END LOOP;

    V_QUERY := V_QUERY || '''' || 'MODIFYDELETE' || '''';
    V_QUERY := RTRIM(V_QUERY, ',');
    V_QUERY := V_QUERY || ' FROM ' || V_TAB_NAME;

if LENGTH(V_EXTA_TABLE) >3 then
       V_QUERY := V_QUERY || ',' ||V_EXTA_TABLE;
      end if ;

    IF v_MASTER_TABLE IS NOT NULL THEN
      v_HDR_DTL_LINK_TABLE := SUBSTR(v_HDR_DTL_LINK_TABLE,
                                     0,
                                     LENGTH(v_HDR_DTL_LINK_TABLE) - 4);
      v_MASTER_TABLE       := SUBSTR(v_MASTER_TABLE,
                                     0,
                                     LENGTH(v_MASTER_TABLE) - 1);
      V_QUERY              := V_QUERY || ',' || v_MASTER_TABLE || ' WHERE ' ||
                              v_HDR_DTL_LINK_TABLE;
    ELSIF v_MASTER_TABLE IS NULL AND v_HDR_DTL_LINK_TABLE IS NOT NULL THEN
      v_HDR_DTL_LINK_TABLE := SUBSTR(v_HDR_DTL_LINK_TABLE,
                                     0,
                                     LENGTH(v_HDR_DTL_LINK_TABLE) - 4);

      V_QUERY := V_QUERY || ' WHERE ' || v_HDR_DTL_LINK_TABLE;
    END IF;

    IF NVL(v_org_column_name, 'NVL') = 'NVL' THEN
      IF NVL(v_HDR_DTL_LINK_TABLE, 'NVL') = 'NVL' THEN
        v_query := v_query || ' where ' || V_TAB_NAME ||
                   '.workflow_completion_status=1';
      ELSE
        v_query := v_query || ' and ' || V_TAB_NAME ||
                   '.workflow_completion_status=1';
      END IF;

    ELSE
      IF NVL(v_HDR_DTL_LINK_TABLE, 'NVL') = 'NVL' THEN
        v_query := v_query || ' where ' || v_org_column_name || ' = ''' ||
                   p_org_id || '''';

        IF P_SCREEN_CODE <> 'AP_014' AND P_SCREEN_CODE <> 'GL_015' THEN
          v_query := v_query || ' and ' || V_TAB_NAME ||
                     '.workflow_completion_status=1';
        END IF;

      ELSE

        v_query := v_query || ' and ' || v_org_column_name || ' = ''' ||
                   p_org_id || '''';
        IF P_SCREEN_CODE <> 'AP_014' AND P_SCREEN_CODE <> 'GL_015' THEN
          v_query := v_query || ' and ' || V_TAB_NAME ||
                     '.workflow_completion_status=1';
        END IF;

      END IF;
    END IF;

if P_SCREEN_CODE ='GL_015' and P_COLNAME='Status' THEN
   if P_COLVALUE='POSTED' then
      v_query := v_query || ' and ' || V_TAB_NAME ||'.workflow_completion_status=1';
   else
      v_query := v_query || ' and ' || V_TAB_NAME ||'.workflow_completion_status=0';
   end if;
END IF;

    V_QUERY := V_QUERY || '  order by DeleteURL desc';
    RETURN v_query;
  END GET_FILTER_LIST_QUERY;

END Ssm;
/
