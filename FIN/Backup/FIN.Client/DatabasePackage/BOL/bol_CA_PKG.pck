CREATE OR REPLACE PACKAGE CA_PKG IS
  --- ***************************************************************************************************************
  --- $Body: CA_PKG 1.0 2014-04-01 $
  --- ***************************************************************************************************************
  --- Program      :   ICA_PKG
  --- Description          :   All  Cash Managment related codes goes here
  --- Author       :   Ram A ,VMV Systems Pvt Ltd
  --- Date   :   13\05\2014
  --- Notes                :
  --- ****************************************************************************************************************
  --- Modification Log
  --- ------------------------------------
  --- Ver    Date                    Author                   Description
  --- ---    ----------   ------------------------ ---------------------------------------------------------
  --- 1.0    13\05\2014          Ram
  --- ***************************************************************************************************************

  PROCEDURE CA_CHEQUE_CREATION_PRO(P_BANK_ID        IN VARCHAR2,
                                   P_BANK_BRANCH_ID IN VARCHAR2,
                                   P_ACCOUNT_NO     IN VARCHAR2,
                                   P_CHEQUE_START   IN NUMERIC,
                                   P_NO_OF_CHEQUE   IN VARCHAR2,
                                   ERRMESG          OUT VARCHAR2,
                                   TXNCODE          OUT VARCHAR2);
END CA_PKG;
 
/
CREATE OR REPLACE PACKAGE BODY CA_PKG IS

  --- ***************************************************************************************************************
  --- $Body: CA_PKG 1.0 2014-04-01 $
  --- ***************************************************************************************************************
  --- Program      :   ICA_PKG
  --- Description          :   All  Cash Managment related codes goes here
  --- Author       :   Ram A ,VMV Systems Pvt Ltd
  --- Date   :   13\05\2014
  --- Notes                :
  --- ****************************************************************************************************************
  --- Modification Log
  --- ------------------------------------
  --- Ver    Date                    Author                   Description
  --- ---    ----------   ------------------------ ---------------------------------------------------------
  --- 1.0    13\05\2014          Ram
  --- ***************************************************************************************************************

  PROCEDURE CA_CHEQUE_CREATION_PRO(P_BANK_ID        IN VARCHAR2,
                                   P_BANK_BRANCH_ID IN VARCHAR2,
                                   P_ACCOUNT_NO     IN VARCHAR2,
                                   P_CHEQUE_START   IN NUMERIC,
                                   P_NO_OF_CHEQUE   IN VARCHAR2,
                                   ERRMESG          OUT VARCHAR2,
                                   TXNCODE          OUT VARCHAR2)

   IS

    ---------------------------------------------------------------------------------
    -- Filename    :  CA_CHEQUE_CREATION_PRO.prc
    ---------------------------------------------------------------------------------
    -- Programmer  :  ramachandran.ammasaigounder
    -- Date        :  13-May-2014
    -- Language    :  PL/SQL
    -- Location    :  CUSTOM_PACKAGE
    -- Purpose     :  This procedure generating cheque numbers for given bank&account number using PL/SQL
    ---------------------------------------------------------------------------------
    -- Description :
    --    Takes a string (P_BANK_ID,P_BANK_BRANCH_ID,P_ACCOUNT_NO, P_NO_OF_CHEQUE ) are input parameters.
    --    Generating cheque numbers defaultly start with 1 for newly generating cheques
    --    Generating and follow continues cheque numbers for already generated cheques
    ----------------------------------------------------------------------------------

    V_CHEQUE_COUNT  NUMBER;
    V_TEMP_ID       NUMBER;
    V_LINE_NO       NUMBER;
    V_CHEQUE_NUMBER NUMBER;
    V_CHEQUE_DATE   DATE;
    V_PAY_TO        VARCHAR2(200);
    V_AMOUNT        NUMBER;
    V_CHEQUE_STATUS VARCHAR2(50);

  BEGIN
  IF P_CHEQUE_START > 0  THEN
    V_CHEQUE_COUNT :=P_CHEQUE_START -1;
  ELSE
    SELECT MAX(CCD.CHECK_NUMBER)
      INTO V_CHEQUE_COUNT
      FROM CA_CHECK_HDR CCH,CA_CHECK_DTL CCD
     WHERE CCH.WORKFLOW_COMPLETION_STATUS = 1
       AND CCH.CHECK_HDR_ID= CCD.CHECK_HDR_ID
       AND CCH.ENABLED_FLAG = 1
       AND CCH.CHECK_BANK_ID = P_BANK_ID
       AND CCH.CHECK_BRANCH_ID = P_BANK_BRANCH_ID
       AND CCH.ACCOUNT_ID = P_ACCOUNT_NO;
END IF;
    IF V_CHEQUE_COUNT > 0 THEN

      DELETE FROM CA_CHEQUE_CREATION_TEMP_TABLE;

      FOR I IN 1 .. P_NO_OF_CHEQUE LOOP

        V_TEMP_ID       := NVL(V_TEMP_ID, 0) + 1;
        V_LINE_NO       := NVL(V_LINE_NO, 0) + 1;
        V_CHEQUE_NUMBER := NVL(V_CHEQUE_NUMBER, V_CHEQUE_COUNT) + 1;
        V_CHEQUE_DATE   := NULL;
        V_PAY_TO        := NULL;
        V_AMOUNT        := NULL;
        V_CHEQUE_STATUS := 'UNUSED';

        INSERT INTO CA_CHEQUE_CREATION_TEMP_TABLE
        VALUES
          (V_LINE_NO,
           V_CHEQUE_NUMBER,
           V_CHEQUE_DATE,
           V_PAY_TO,
           V_AMOUNT,
           V_CHEQUE_STATUS);

      END LOOP;

    ELSE
      DELETE FROM CA_CHEQUE_CREATION_TEMP_TABLE;

      V_CHEQUE_COUNT := P_NO_OF_CHEQUE;

      FOR I IN 1 .. P_NO_OF_CHEQUE LOOP

        V_TEMP_ID       := NVL(V_TEMP_ID, 0) + 1;
        V_LINE_NO       := NVL(V_LINE_NO, 0) + 1;
        V_CHEQUE_NUMBER := NVL(V_CHEQUE_NUMBER, 0) + 1;
        V_CHEQUE_DATE   := NULL;
        V_PAY_TO        := NULL;
        V_AMOUNT        := NULL;
        V_CHEQUE_STATUS := 'UNUSED';

        INSERT INTO CA_CHEQUE_CREATION_TEMP_TABLE
        VALUES
          (V_LINE_NO,
           V_CHEQUE_NUMBER,
           V_CHEQUE_DATE,
           V_PAY_TO,
           V_AMOUNT,
           V_CHEQUE_STATUS);

      END LOOP;

    END IF;
    COMMIT;
  EXCEPTION
    WHEN OTHERS THEN
      ERRMESG := SQLERRM;
      TXNCODE := SQLCODE;
  END CA_CHEQUE_CREATION_PRO;
END CA_PKG;
/
