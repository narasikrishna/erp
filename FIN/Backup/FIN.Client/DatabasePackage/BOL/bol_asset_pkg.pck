CREATE OR REPLACE PACKAGE asset_pkg is

  -- Author  : SAKTHIVEL
  -- Created : 8/14/2013 9:51:34 AM
  -- Purpose : Assest process

PROCEDURE asset_depreciation_process(P_ASSET_ID IN NUMBER,P_DPRN_METHOD_ID IN NUMBER);
FUNCTION Is_asset_Discarded(P_ASSET_ID IN NUMBER) RETURN BOOLEAN;
end asset_pkg;
 
/
CREATE OR REPLACE PACKAGE BODY asset_pkg is

FUNCTION Is_asset_Discarded(P_ASSET_ID IN NUMBER) RETURN BOOLEAN IS
v_count NUMBER:=0;
BEGIN
 SELECT COUNT(*)
   INTO v_count
   FROM AST_ASSET_DISCARD_DTL
   WHERE ASSET_MST_ID=P_ASSET_ID;
   IF v_count=0 THEN
    RETURN FALSE;
   ELSE
    RETURN TRUE;
   END IF;
END Is_Asset_Discarded;


PROCEDURE asset_depreciation_process(P_ASSET_ID IN NUMBER,
                                     P_DPRN_METHOD_ID IN NUMBER) IS

  /*  CURSOR  cdm(c_dprn_method_mst_id NUMBER)   IS
                  SELECT   dmm.dprn_method_mst_id,
                           dmm.dprn_rate
                  FROM     ast_depreciation_method_mst dmm
          WHERE dmm.dprn_method_mst_id=c_dprn_method_mst_id;*/

CURSOR  cdm(c_dprn_method_mst_id NUMBER)   IS
  SELECT B.DPRN_CATEGORY_ID,
       B.MAJOR_CATEGORY_ID,
       B.DPRN_METHOD_MST_ID,
       --a.DPRN_METHOD_MST_ID,
       B.EFFECT_FROM ,
       a.DPRN_NAME,
       a.DPRN_METHOD_ID,
       a.DPRN_RATE,
       a.DPRN_TYPE_DURATION_ID,
       a.DPRN_TYPE_DURATION,
       a.DPRN_SALVAGE
  FROM AST_DPRN_CATEGORY_CONFIG B,
    AST_DEPRECIATION_METHOD_MST a
  WHERE a.dprn_method_mst_id=b.dprn_method_mst_id
  AND B.EFFECT_FROM         =
    (SELECT MAX(EFFECT_FROM)
    FROM AST_DPRN_CATEGORY_CONFIG
    WHERE major_category_id=b.major_category_id
    )
  AND A.DPRN_METHOD_MST_ID=c_dprn_method_mst_id;



/*    CURSOR c_aam(c_asset_mst_id) IS
                  SELECT aam.asset_mst_id,
                         aam.asset_name,
                         aam.orginal_cost
                  FROM   ast_asset_mst aam
          WHERE aam.asset_mst_id=c_asset_mst_id;
*/

CURSOR c_aam(c_asset_mst_id IN NUMBER,c_MAJORE_CATEGORY_ID IN NUMBER) IS
    SELECT X.ASSET_CATEGORY_ID,
      X.ASSET_MST_ID,
    --  Y.ASSET_MST_ID,
      X.MAJORE_CATEGORY_ID,
      X.MINOR_CATEGORY_ID,
      X.EFFECT_FROM,
      y.ASSET_NAME,
      y.ORGINAL_COST,
      y.DATE_IN_SERVICE
    FROM AST_ASSET_CATEGORY_DTL X ,
      AST_ASSET_MST Y
    WHERE x.asset_mst_id=y.asset_mst_id
     AND y.DEPRECIABLE='Y'
     AND y.asset_in_use='Y'
     AND y.asset_mst_id=c_asset_mst_id
     AND x.MAJORE_CATEGORY_ID=c_MAJORE_CATEGORY_ID;



    v_asset_depreciation_id              NUMBER := 0;
    v_asset_cost                         NUMBER := 0;
    v_error_msg                          VARCHAR2(2000);
  v_date_in_service                    DATE;
  v_to_date               DATE;
  v_week_count             NUMBER:=0;
BEGIN
    --ASSET_DEPRECIATION_SEQ.NEXTVAL
    DELETE FROM ast_asset_depreciation_dtl;

    v_asset_depreciation_id := 0;

    FOR rdm IN cdm(P_DPRN_METHOD_ID) LOOP
     dbms_output.put_line('Depreciation details '||rdm.DPRN_METHOD_MST_ID || ' - '||rdm.DPRN_NAME||' - '||rdm.DPRN_RATE ||' - '||rdm.MAJOR_CATEGORY_ID);

    FOR r_aam IN c_aam(P_ASSET_ID,rdm.MAJOR_CATEGORY_ID) LOOP

    v_asset_depreciation_id := v_asset_depreciation_id  + 1;
  dbms_output.put_line('Asset Details '||r_aam.asset_mst_id ||' - '||r_aam.asset_name||' - '||r_aam.ORGINAL_COST||' - '||TO_CHAR(r_aam.DATE_IN_SERVICE));
  v_date_in_service:=r_aam.DATE_IN_SERVICE;
   --v_date_in_service:=add_months(v_date_in_service,12);
   v_asset_cost:=r_aam.ORGINAL_COST -NVL(rdm.DPRN_SALVAGE,0);
    IF UPPER(rdm.DPRN_NAME)='YEARLY' THEN
      dbms_output.put_line('One');
    v_to_date:=ROUND(SYSDATE,'YYYY');

  WHILE v_date_in_service<=v_to_date LOOP
    dbms_output.put_line('Depreciation Details '||to_char(v_date_in_service)||' - '||to_char(v_asset_cost));
    v_date_in_service:=add_months(v_date_in_service,12);

       --start
     IF v_asset_cost>=0 THEN
     BEGIN
       INSERT INTO AST_ASSET_DEPRECIATION_DTL  (ASSET_DEPRECIATION_ID,
                          ASSET_MST_ID,
                                                DPRN_CATEGORY_ID,
                                                DPRN_METHOD_MST_ID,
                                                PROCESSED_ON,
                                                DPRN_RATE,
                                              ASSET_COST,
                                              GL_TRANSFER_STATUS,
                                              GL_AMOUNT,
                                              GL_ACCOUNT,
                                              GL_TRANSFER_DATE,
                                              ENABLED_FLAG,
                                              WORKFLOW_COMPLETION_STATUS,
                                              CREATE_BY,
                                              CREATED_DATE,
                                              MODIFIED_BY,
                                              MODIFIED_DATE,
                                              ATTRIBUTE1,
                                              ATTRIBUTE2,
                                              ATTRIBUTE3,
                                              ATTRIBUTE4,
                                              ATTRIBUTE5,
                                              ATTRIBUTE6,
                                              ATTRIBUTE7,
                                              ATTRIBUTE8,
                                              ATTRIBUTE9,
                                              ATTRIBUTE10,
                                              ORGANIZATION_MASTER_ID,
                                              CALENDAR_ID
                          )
                                            VALUES(ASSET_DEPRECIATION_SEQ.NEXTVAL,--ASSET_DEPRECIATION_ID
                           R_AAM.ASSET_MST_ID,--ASSET_MST_ID
                           rdm.DPRN_CATEGORY_ID,--DPRN_CATEGORY_ID
                           RDM.DPRN_METHOD_MST_ID,--DPRN_METHOD_MST_ID
                           SYSDATE,--PROCESSED_ON
                           RDM.DPRN_RATE,--DPRN_RATE
                           V_ASSET_COST,--ASSET_COST
                           '1',--GL_TRANSFER_STATUS
                           NULL,--GL_AMOUNT
                           NULL,--GL_ACCOUNT
                                                   NULL,--GL_TRANSFER_DATE
                                                   '1',--ENABLED_FLAG
                                                   '1',--WORKFLOW_COMPLETION_STATUS
                           USER,--'SAKTHIVEL',--CREATE_BY
                           SYSDATE,--CREATED_DATE
                                               NULL,--MODIFIED_BY
                                               NULL,--MODIFIED_DATE
                                               NULL,--ATTRIBUTE1
                                               NULL,--ATTRIBUTE2
                                               NULL,--ATTRIBUTE3
                                               NULL,--ATTRIBUTE4
                                               NULL,--ATTRIBUTE5
                                               NULL,--ATTRIBUTE6
                                               NULL,--ATTRIBUTE7
                                               NULL,--ATTRIBUTE8
                                               NULL,--ATTRIBUTE9
                                               NULL,--ATTRIBUTE10
                                               29,--ORGANIZATION_MASTER_ID
                                               1--CALENDAR_ID
                           );
     EXCEPTION
       WHEN OTHERS THEN
       dbms_output.put_line('Error 1 '||sqlerrm);
     END;
     END IF;
    v_asset_cost:=v_asset_cost-rdm.DPRN_RATE;
     --end
  END LOOP;
  COMMIT;
  ELSIF UPPER(rdm.DPRN_NAME)='WEEKLY' THEN
       dbms_output.put_line('Two');
     BEGIN
       SELECT
      --t.start_date,t.end_date,
          to_number(to_char(t.end_date, 'WW')) -
           to_number(to_char(t.start_date, 'WW')) +
           52 * (to_number(to_char(t.end_date, 'YYYY')) -
                 TO_NUMBER(TO_CHAR(T.START_DATE, 'YYYY'))) DIFF_WEEK
     INTO v_week_count
      FROM (SELECT v_date_in_service start_date, SYSDATE end_date FROM dual) t;
    EXCEPTION
      WHEN OTHERS THEN
    --dbms_output.put_line('Error in week '||sqlerrm);
     v_week_count:=0;
    END;
     FOR w_count IN 1..v_week_count LOOP
       --start
     IF v_asset_cost>=0 THEN
     BEGIN
       INSERT INTO AST_ASSET_DEPRECIATION_DTL  (ASSET_DEPRECIATION_ID,
                          ASSET_MST_ID,
                                                DPRN_CATEGORY_ID,
                                                DPRN_METHOD_MST_ID,
                                                PROCESSED_ON,
                                                DPRN_RATE,
                                              ASSET_COST,
                                              GL_TRANSFER_STATUS,
                                              GL_AMOUNT,
                                              GL_ACCOUNT,
                                              GL_TRANSFER_DATE,
                                              ENABLED_FLAG,
                                              WORKFLOW_COMPLETION_STATUS,
                                              CREATE_BY,
                                              CREATED_DATE,
                                              MODIFIED_BY,
                                              MODIFIED_DATE,
                                              ATTRIBUTE1,
                                              ATTRIBUTE2,
                                              ATTRIBUTE3,
                                              ATTRIBUTE4,
                                              ATTRIBUTE5,
                                              ATTRIBUTE6,
                                              ATTRIBUTE7,
                                              ATTRIBUTE8,
                                              ATTRIBUTE9,
                                              ATTRIBUTE10,
                                              ORGANIZATION_MASTER_ID,
                                              CALENDAR_ID
                          )
                                            VALUES(ASSET_DEPRECIATION_SEQ.NEXTVAL,--ASSET_DEPRECIATION_ID
                           R_AAM.ASSET_MST_ID,--ASSET_MST_ID
                           rdm.DPRN_CATEGORY_ID,--DPRN_CATEGORY_ID
                           RDM.DPRN_METHOD_MST_ID,--DPRN_METHOD_MST_ID
                           SYSDATE,--PROCESSED_ON
                           RDM.DPRN_RATE,--DPRN_RATE
                           V_ASSET_COST,--ASSET_COST
                           '1',--GL_TRANSFER_STATUS
                           NULL,--GL_AMOUNT
                           NULL,--GL_ACCOUNT
                                                   NULL,--GL_TRANSFER_DATE
                                                   '1',--ENABLED_FLAG
                                                   '1',--WORKFLOW_COMPLETION_STATUS
                           USER,--'SAKTHIVEL',--CREATE_BY
                           SYSDATE,--CREATED_DATE
                                               NULL,--MODIFIED_BY
                                               NULL,--MODIFIED_DATE
                                               NULL,--ATTRIBUTE1
                                               NULL,--ATTRIBUTE2
                                               NULL,--ATTRIBUTE3
                                               NULL,--ATTRIBUTE4
                                               NULL,--ATTRIBUTE5
                                               NULL,--ATTRIBUTE6
                                               NULL,--ATTRIBUTE7
                                               NULL,--ATTRIBUTE8
                                               NULL,--ATTRIBUTE9
                                               NULL,--ATTRIBUTE10
                                               29,--ORGANIZATION_MASTER_ID
                                               1--CALENDAR_ID
                           );
     EXCEPTION
       WHEN OTHERS THEN
       dbms_output.put_line('Error 1 '||sqlerrm);
     END;
     END IF;
    v_asset_cost:=v_asset_cost-rdm.DPRN_RATE;
     --end
     END LOOP;
     COMMIT;
  ELSIF UPPER(rdm.DPRN_NAME)='DAILY' THEN
      WHILE v_date_in_service<=TRUNC(SYSDATE) LOOP

        --start
     IF v_asset_cost>=0 THEN
     BEGIN
       INSERT INTO AST_ASSET_DEPRECIATION_DTL  (ASSET_DEPRECIATION_ID,
                          ASSET_MST_ID,
                                                DPRN_CATEGORY_ID,
                                                DPRN_METHOD_MST_ID,
                                                PROCESSED_ON,
                                                DPRN_RATE,
                                              ASSET_COST,
                                              GL_TRANSFER_STATUS,
                                              GL_AMOUNT,
                                              GL_ACCOUNT,
                                              GL_TRANSFER_DATE,
                                              ENABLED_FLAG,
                                              WORKFLOW_COMPLETION_STATUS,
                                              CREATE_BY,
                                              CREATED_DATE,
                                              MODIFIED_BY,
                                              MODIFIED_DATE,
                                              ATTRIBUTE1,
                                              ATTRIBUTE2,
                                              ATTRIBUTE3,
                                              ATTRIBUTE4,
                                              ATTRIBUTE5,
                                              ATTRIBUTE6,
                                              ATTRIBUTE7,
                                              ATTRIBUTE8,
                                              ATTRIBUTE9,
                                              ATTRIBUTE10,
                                              ORGANIZATION_MASTER_ID,
                                              CALENDAR_ID
                          )
                                            VALUES(ASSET_DEPRECIATION_SEQ.NEXTVAL,--ASSET_DEPRECIATION_ID
                           R_AAM.ASSET_MST_ID,--ASSET_MST_ID
                           rdm.DPRN_CATEGORY_ID,--DPRN_CATEGORY_ID
                           RDM.DPRN_METHOD_MST_ID,--DPRN_METHOD_MST_ID
                           SYSDATE,--PROCESSED_ON
                           RDM.DPRN_RATE,--DPRN_RATE
                           V_ASSET_COST,--ASSET_COST
                           '1',--GL_TRANSFER_STATUS
                           NULL,--GL_AMOUNT
                           NULL,--GL_ACCOUNT
                                                   NULL,--GL_TRANSFER_DATE
                                                   '1',--ENABLED_FLAG
                                                   '1',--WORKFLOW_COMPLETION_STATUS
                           USER,--'SAKTHIVEL',--CREATE_BY
                           SYSDATE,--CREATED_DATE
                                               NULL,--MODIFIED_BY
                                               NULL,--MODIFIED_DATE
                                               NULL,--ATTRIBUTE1
                                               NULL,--ATTRIBUTE2
                                               NULL,--ATTRIBUTE3
                                               NULL,--ATTRIBUTE4
                                               NULL,--ATTRIBUTE5
                                               NULL,--ATTRIBUTE6
                                               NULL,--ATTRIBUTE7
                                               NULL,--ATTRIBUTE8
                                               NULL,--ATTRIBUTE9
                                               NULL,--ATTRIBUTE10
                                               29,--ORGANIZATION_MASTER_ID
                                               1--CALENDAR_ID
                           );
     EXCEPTION
       WHEN OTHERS THEN
       dbms_output.put_line('Error 1 '||sqlerrm);
     END;
     END IF;
    v_asset_cost:=v_asset_cost-rdm.DPRN_RATE;
    v_date_in_service:=v_date_in_service+1;
     --end
    END LOOP;
      COMMIT;
  END IF;
  END LOOP;
  END LOOP;
  --  COMMIT;
END asset_depreciation_process;

END asset_pkg;
/
