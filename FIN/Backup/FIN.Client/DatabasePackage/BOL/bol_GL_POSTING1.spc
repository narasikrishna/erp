CREATE OR REPLACE PACKAGE GL_POSTING1 IS
  procedure AP_POSTING(P_RECEIPT_ID VARCHAR2);
  PROCEDURE GL_INSERT(P_JE_HDR_ID              IN VARCHAR2,
                      p_JE_COMP_ID             IN VARCHAR2,
                      P_JE_DATE                IN DATE,
                      P_JE_GLOBAL_SEGMENT_ID   IN VARCHAR2,
                      P_JE_NUMBER              IN VARCHAR2,
                      P_JE_TYPE                IN VARCHAR2,
                      P_JE_CURRENCY_ID         IN VARCHAR2,
                      P_JE_DESC                IN VARCHAR2,
                      P_JE_CURRENCY_RATE_ID    IN VARCHAR2,
                      P_JE_STATUS              IN VARCHAR2,
                      P_JE_CURR_VALUE          IN NUMBER,
                      P_JE_PERIOD_ID           IN VARCHAR2,
                      P_JE_REFERENCE           IN VARCHAR2,
                      P_JE_EXCHANGE_RATE_VALUE IN VARCHAR2,
                      P_JE_TOT_DR_AMOUNT       IN VARCHAR2,
                      P_JE_TOT_CR_AMOUNT       IN VARCHAR2,
                      P_JE_RECURRING_END_DATE  IN DATE,
                      P_JE_RECURRING_FREQUENCY IN VARCHAR2,
                      P_JE_SOURCE              IN VARCHAR2,
                      P_JOURNAL_REVERSED       IN VARCHAR2,
                      P_ACTUAL_JOURNAL_ID      IN VARCHAR2);

  PROCEDURE GL_INSERT_JR_DTL(P_JE_DTL_ID           IN VARCHAR2,
                             P_JE_HDR_ID           IN VARCHAR2,
                             P_JE_ACCT_CODE_ID     IN VARCHAR2,
                             P_JE_PARTICULARS      VARCHAR2,
                             P_JE_CREDIT_DEBIT     IN CHAR,
                             P_JE_AMOUNT_CR        IN NUMBER,
                             P_JE_ACCOUNTED_AMT_CR IN NUMBER,
                             P_JE_SEGMENT_ID_1     IN VARCHAR2,
                             P_JE_SEGMENT_ID_2     IN VARCHAR2,
                             P_JE_SEGMENT_ID_3     IN VARCHAR2,
                             P_JE_SEGMENT_ID_4     IN VARCHAR2,
                             P_JE_SEGMENT_ID_5     IN VARCHAR2,
                             P_JE_SEGMENT_ID_6     IN VARCHAR2,
                             P_JE_AMOUNT_DR        IN NUMBER,
                             P_JE_ACCOUNTED_AMT_DR IN NUMBER);

END GL_POSTING1;
 
/
