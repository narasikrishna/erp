﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.BLL;
using FIN.BLL.SSM;
using VMVServices.Web;
using FIN.DAL.SSM;

namespace FIN.Client.SSM
{
    public partial class WorkflowEmployeeEntry : PageBase
    {
        DataTable dtGridData = new DataTable();
        WFM_WORKFLOW_CODE_MASTERS WFM_WORKFLOW_CODE_MASTERS = new WFM_WORKFLOW_CODE_MASTERS();
        WFM_WORKFLOW_LEVELS WFM_WORKFLOW_LEVELS = new WFM_WORKFLOW_LEVELS();
        Boolean bol_rowVisiable;
        Boolean savedBool;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AOBJ_PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }
        private void Startup()
        {
            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));


            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
        }

        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>
        /// 
        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                EntityData = null;
                FillCombo();

                dtGridData = DBMethod.ExecuteQuery(WorkflowDAL.GetWorkFlowEmpData(Master.RecordID)).Tables[0];
                BindGrid(dtGridData);

                if (Master.Mode != FINAppConstants.Add && Master.RecordID > 0)
                {
                    WFM_WORKFLOW_CODE_MASTERS = WorkflowBLL.getClassEntity(Master.RecordID);
                    EntityData = WFM_WORKFLOW_CODE_MASTERS;
                    
                    ddlModuleCode.SelectedValue = WFM_WORKFLOW_CODE_MASTERS.ATTRIBUTE1;//module code
                    BindModulename();


                    Screens_BLL.GetScreenName(ref ddlWorkFlowCode);

                    ddlWorkFlowCode.SelectedValue = WFM_WORKFLOW_CODE_MASTERS.WORKFLOW_CODE;
                    BindScreenName();
                    txtWorkflowDescription.Text = WFM_WORKFLOW_CODE_MASTERS.DESCRIPTION;
                    ddlEmployee.SelectedValue = WFM_WORKFLOW_CODE_MASTERS.EMP_ID;
                    Session["scrdesc"] = WFM_WORKFLOW_CODE_MASTERS.DESCRIPTION;
                    ddlWorkFlowCode.Enabled = false;
                    ddlModuleCode.Enabled = false;
                   // divmodule.Visible = false;
                   
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AOBJ_ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr[FINColumnConstants.ENABLED_FLAG] = FINAppConstants.FALSEFLAG;
                    dr["EXCEPTION_FLAG"] = FINAppConstants.FALSEFLAG;
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();


                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AOBJ_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session[FINSessionConstants.GridData], "Workflow Employee ");

                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                AssignToBE();

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Saave", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {

                    WFM_WORKFLOW_CODE_MASTERS.PK_ID = Convert.ToInt32(dtGridData.Rows[iLoop]["PK_ID"].ToString());
                    DBMethod.DeleteEntity<WFM_WORKFLOW_CODE_MASTERS>(WFM_WORKFLOW_CODE_MASTERS);
                }


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AaOBJ_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {

        }
        protected void ddlRoleCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                BindRolename(sender);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AaOBJ_ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void BindRolename(object sender)
        {
            try
            {
                ErrorCollection.Clear();

                DataTable dtData = new DataTable();
                GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
                TextBox txtRoleDescription = gvr.FindControl("txtRoleDescription") as TextBox;
                DropDownList ddlRoleCode = gvr.FindControl("ddlRoleCode") as DropDownList;

                dtData = DBMethod.ExecuteQuery(Role_DAL.GetRole(ddlRoleCode.SelectedValue.ToString())).Tables[0];
                if (dtData != null)
                {
                    if (dtData.Rows.Count > 0)
                    {
                        txtRoleDescription.Text = dtData.Rows[0]["role_name"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AaOBJ_ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        protected void ddlModuleCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                BindModulename();
                BindScreen();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AOaBJ_ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        protected void ddlWorkFlowCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                BindScreenName();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AOafghBJ_ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void BindScreenName()
        {
            try
            {
                ErrorCollection.Clear();

                DataTable dtData = new DataTable();
                dtData = DBMethod.ExecuteQuery(Screens_DAL.GetScreenName(ddlModuleCode.SelectedValue.ToString(), ddlWorkFlowCode.SelectedValue.ToString())).Tables[0];
                if (dtData != null)
                {
                    if (dtData.Rows.Count > 0)
                    {
                        txtWorkflowDescription.Text = dtData.Rows[0]["SCREEN_NAME"].ToString();
                        Session["scrdesc"] = dtData.Rows[0]["screen_desc"].ToString();

                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AOaBJ_ATfdgOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void BindScreen()
        {
            Screens_BLL.GetScreenName(ref ddlWorkFlowCode, ddlModuleCode.SelectedValue);
        }
        private void BindModulename()
        {
            DataTable dtData = new DataTable();
            //GridViewRow tmpgvr = gvData.FooterRow;

            //DropDownList ddlModuleCode = tmpgvr.FindControl("ddlModuleCode") as DropDownList;
            //TextBox txtModuleDescription = tmpgvr.FindControl("txtModuleDescription") as TextBox;


            dtData = DBMethod.ExecuteQuery(Screens_DAL.GetModuleList(ddlModuleCode.SelectedValue.ToString())).Tables[0];
            if (dtData != null)
            {
                if (dtData.Rows.Count > 0)
                {
                    txtModuleDescription.Text = dtData.Rows[0]["description"].ToString();
                }
            }

        }
        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    WFM_WORKFLOW_CODE_MASTERS = (WFM_WORKFLOW_CODE_MASTERS)EntityData;
                }
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                WFM_WORKFLOW_CODE_MASTERS.ATTRIBUTE1 = ddlModuleCode.SelectedValue;
                WFM_WORKFLOW_CODE_MASTERS.WORKFLOW_CODE = ddlWorkFlowCode.SelectedValue;
                WFM_WORKFLOW_CODE_MASTERS.DESCRIPTION = Session["scrdesc"].ToString();// txtWorkflowDescription.Text;
                WFM_WORKFLOW_CODE_MASTERS.ENABLED_FLAG = "1";
                WFM_WORKFLOW_CODE_MASTERS.FORM_OPEN_MODE = "QUERY";
                WFM_WORKFLOW_CODE_MASTERS.EMP_ID = ddlEmployee.SelectedValue;

                if (Master.Mode != FINAppConstants.Add && Master.RecordID > 0)
                {
                    WFM_WORKFLOW_CODE_MASTERS.MODIFIED_BY = this.LoggedUserName;
                    WFM_WORKFLOW_CODE_MASTERS.MODIFIED_DATE = DateTime.Today;
                }
                else
                {
                    WFM_WORKFLOW_CODE_MASTERS.PK_ID = DBMethod.GetPrimaryKeyValue(FINSequenceConstant.WFM_WORKFLOW_CODE_MASTERS_SEQ);

                    WFM_WORKFLOW_CODE_MASTERS.EFFECTIVE_DATE = DateTime.Today;
                    WFM_WORKFLOW_CODE_MASTERS.CREATED_BY = this.LoggedUserName;
                    WFM_WORKFLOW_CODE_MASTERS.CREATED_DATE = DateTime.Today;
                }
                WFM_WORKFLOW_CODE_MASTERS.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, WFM_WORKFLOW_CODE_MASTERS.PK_ID.ToString());



               // int countVal = 0;
               // countVal = DBMethod.GetIntValue(FINSQL.IsValueExists("wfm_workflow_code_masters", "PK_ID",WFM_WORKFLOW_CODE_MASTERS.PK_ID.ToString(),"WORKFLOW_CODE", ddlWorkFlowCode.SelectedValue));
               //if(countVal!=null)
               //{
               //    if (countVal.ToString().Length > 0)
               //    {
               //        ErrorCollection.Add("dup_error","Already
               //    }
               //}

                var tmpChildEntity = new List<Tuple<object, string>>();

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    WFM_WORKFLOW_LEVELS = new WFM_WORKFLOW_LEVELS();
                    if (int.Parse(dtGridData.Rows[iLoop]["PK_ID"].ToString()) > 0)
                    {
                        using (IRepository<WFM_WORKFLOW_LEVELS> userCtx = new DataRepository<WFM_WORKFLOW_LEVELS>())
                        {
                            WFM_WORKFLOW_LEVELS = userCtx.Find(r =>
                                (r.PK_ID == int.Parse(dtGridData.Rows[iLoop]["PK_ID"].ToString()))
                                ).SingleOrDefault();
                        }
                    }

                    WFM_WORKFLOW_LEVELS.CHILD_ID = WFM_WORKFLOW_CODE_MASTERS.PK_ID;
                    WFM_WORKFLOW_LEVELS.EMP_ID = dtGridData.Rows[iLoop][FINColumnConstants.EMP_ID].ToString();
                    WFM_WORKFLOW_LEVELS.ROLE_CODE = dtGridData.Rows[iLoop][FINColumnConstants.EMP_ID].ToString();                 

                    WFM_WORKFLOW_LEVELS.LEVEL_CODE = short.Parse((iLoop + 1).ToString());
                    WFM_WORKFLOW_LEVELS.IS_MANDATORY = "1";
                    WFM_WORKFLOW_LEVELS.FORM_OPEN_MODE = "QUERY";
                    WFM_WORKFLOW_LEVELS.BRANCH_CODE = "HQ";
                    WFM_WORKFLOW_LEVELS.WORKFLOW_CCID = ddlWorkFlowCode.SelectedValue.ToString() + '_' + (iLoop + 1).ToString();
                    WFM_WORKFLOW_LEVELS.WORKFLOW_CODE = ddlWorkFlowCode.SelectedValue;

                    if (dtGridData.Rows[iLoop][FINColumnConstants.effective_date] != DBNull.Value)
                    {
                        WFM_WORKFLOW_LEVELS.EFFECTIVE_DATE = DateTime.Parse(dtGridData.Rows[iLoop][FINColumnConstants.effective_date].ToString());
                    }
                    if (dtGridData.Rows[iLoop][FINColumnConstants.end_date].ToString().Length>0)
                    {
                        WFM_WORKFLOW_CODE_MASTERS.END_DATE = DateTime.Parse(dtGridData.Rows[iLoop][FINColumnConstants.end_date].ToString());
                    }

                    WFM_WORKFLOW_LEVELS.REMARKS = dtGridData.Rows[iLoop][FINColumnConstants.REMARKS].ToString();

                    WFM_WORKFLOW_LEVELS.ENABLED_FLAG = dtGridData.Rows[iLoop][FINColumnConstants.ENABLED_FLAG].ToString() == FINAppConstants.TRUEFLAG ? FINAppConstants.Y : FINAppConstants.N;
                    WFM_WORKFLOW_LEVELS.EXCEPTION_FLAG = dtGridData.Rows[iLoop]["EXCEPTION_FLAG"].ToString() == FINAppConstants.TRUEFLAG ? FINAppConstants.Y : FINAppConstants.N;
                    
                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        tmpChildEntity.Add(new Tuple<object, string>(WFM_WORKFLOW_LEVELS, "D"));
                    }
                    else
                    {
                        if (dtGridData.Rows[iLoop][FINColumnConstants.PK_ID].ToString() != "0" && dtGridData.Rows[iLoop][FINColumnConstants.PK_ID].ToString() != string.Empty)
                        {
                            WFM_WORKFLOW_LEVELS.PK_ID = CommonUtils.ConvertStringToInt(dtGridData.Rows[iLoop]["PK_ID"].ToString());
                            WFM_WORKFLOW_LEVELS.WORKFLOW_COMPLETION_STATUS = "1";
                            WFM_WORKFLOW_LEVELS.MODIFIED_BY = this.LoggedUserName;
                            WFM_WORKFLOW_LEVELS.MODIFIED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(WFM_WORKFLOW_LEVELS, FINAppConstants.Update));
                        }
                        else
                        {
                            WFM_WORKFLOW_LEVELS.PK_ID = DBMethod.GetPrimaryKeyValue(FINSequenceConstant.WFM_WORKFLOW_LEVELS_SEQ);
                            WFM_WORKFLOW_LEVELS.WORKFLOW_COMPLETION_STATUS = "1";
                            WFM_WORKFLOW_LEVELS.CREATED_BY = this.LoggedUserName;
                            WFM_WORKFLOW_LEVELS.CREATED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(WFM_WORKFLOW_LEVELS, FINAppConstants.Add));
                        }
                    }
                }

                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            CommonUtils.SavePCEntity<WFM_WORKFLOW_CODE_MASTERS, WFM_WORKFLOW_LEVELS>(WFM_WORKFLOW_CODE_MASTERS, tmpChildEntity, WFM_WORKFLOW_LEVELS);
                            savedBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {
                            CommonUtils.SavePCEntity<WFM_WORKFLOW_CODE_MASTERS, WFM_WORKFLOW_LEVELS>(WFM_WORKFLOW_CODE_MASTERS, tmpChildEntity, WFM_WORKFLOW_LEVELS, true);
                            savedBool = true;
                            break;
                        }
                }
                if (ErrorCollection.Count > 0)
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AOBJ_ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void FillCombo()
        {
            try
            {
                ErrorCollection.Clear();
                Screens_BLL.GetModuleList(ref ddlModuleCode);
                FIN.BLL.HR.Employee_BLL.GetEmployeeId(ref ddlEmployee);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AOBJ_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();
                DropDownList ddlRoleCode = tmpgvr.FindControl("ddlRoleCode") as DropDownList;

               // Role_BLL.GetRole(ref ddlRoleCode);

                FIN.BLL.HR.Employee_BLL.GetEmployeeId(ref ddlRoleCode);

                if (gvData.EditIndex >= 0)
                {
                    ddlRoleCode.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.ROLE_CODE].ToString();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AOBJ_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        #endregion

        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            try
            {
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = -1;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("COUNTRY", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("COUNTRY", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {
            System.Collections.SortedList slControls = new System.Collections.SortedList();

            DropDownList ddlRoleCode = gvr.FindControl("ddlRoleCode") as DropDownList;
            TextBox txtRoleDescription = gvr.FindControl("txtRoleDescription") as TextBox;
            CheckBox chkAct = gvr.FindControl("chkAct") as CheckBox;

            CheckBox chkExFlag = gvr.FindControl("chkExFlag") as CheckBox;
            TextBox txtRemarks = gvr.FindControl("txtRemarks") as TextBox;
            TextBox dtpStartDate = gvr.FindControl("dtpStartDate") as TextBox;
            TextBox dtpEndDate = gvr.FindControl("dtpEndDate") as TextBox;
            
            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();

            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList["PK_ID"] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);
            }

            slControls[0] = ddlRoleCode;
            slControls[1] = dtpStartDate;
            slControls[2] = dtpStartDate;
            slControls[3] = dtpEndDate;

            ErrorCollection.Clear();
            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/SSM_" + Session["Sel_Lng"].ToString() + ".properties"));
            string strCtrlTypes = "DropDownList~TextBox~DateTime~DateRangeValidate";
            string strMessage = Prop_File_Data["Employee_P"] + " ~ " + Prop_File_Data["Effective_Start_Date_P"] + " ~ " + Prop_File_Data["Effective_Start_Date_P"] + " ~ " + Prop_File_Data["End_Date_P"] + "";
            //string strMessage = "Role Code~Effective Date ~ Effective Date ~ End Date";

            ErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

            if (ErrorCollection.Count > 0)
                return drList;

            string strCondition = string.Empty;

            strCondition = "emp_id='" + ddlRoleCode.SelectedValue + "'";
            strMessage = FINMessageConstatns.RecordAlreadyExists;

            ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            if (ErrorCollection.Count > 0)
            {
                return drList;
            }

            drList[FINColumnConstants.EMP_ID] = ddlRoleCode.SelectedValue;
            drList[FINColumnConstants.EMP_NAME] = ddlRoleCode.SelectedItem.Text;


            drList[FINColumnConstants.effective_date] = DBMethod.ConvertStringToDate(dtpStartDate.Text.ToString());
            if (dtpEndDate.Text.Length > 0)
            {
                drList[FINColumnConstants.end_date] = DBMethod.ConvertStringToDate(dtpEndDate.Text.ToString());
            }

            drList[FINColumnConstants.ENABLED_FLAG] = chkAct.Checked == true ? FINAppConstants.TRUEFLAG : FINAppConstants.FALSEFLAG;
            drList["EXCEPTION_FLAG"] = chkExFlag.Checked == true ? FINAppConstants.TRUEFLAG : FINAppConstants.FALSEFLAG;
            drList[FINColumnConstants.REMARKS] = txtRemarks.Text;
            
            drList[FINColumnConstants.DELETED] = FINAppConstants.N;

            return drList;

        }

        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AOBJ_ROW_UP", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AOBJ_ROW_DEL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AOBJ_ROW_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AOBJ_RowCreated", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AOBJ_RDB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion
    }
}