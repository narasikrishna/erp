﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.BLL;
using FIN.BLL.SSM;
using FIN.DAL.SSM;
using VMVServices.Web;
using FIN.DAL.SSM;

namespace FIN.Client.SSM
{
    public partial class UserEntry : PageBase
    {
        SSM_USERS sSM_USERS = new SSM_USERS();
        SSM_USER_ORG_INTERSECTS sSM_USER_ORG_INTERSECTS = new SSM_USER_ORG_INTERSECTS();
        SSM_USER_ROLE_INTERSECTS sSM_USER_ROLE_INTERSECTS = new SSM_USER_ROLE_INTERSECTS();
        DataTable dtGridData = new DataTable();
        DataTable dtURGridData = new DataTable();

        Boolean bol_rowVisiable;
        Boolean savedBool;
        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    ChangeLanguage();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        private void ChangeLanguage()
        {
            try
            {
                ErrorCollection.Clear();
                if (VMVServices.Web.Utils.LanguageCode.ToString().Length > 0)
                {
                    Dictionary<string, string> Prop_File_Data;
                    Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));
                    RBG.Items[0].Text = Prop_File_Data["Male_P"];
                    RBG.Items[1].Text = Prop_File_Data["Female_P"];
                    
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("MisReceiptChangeLang", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
            }

            if (VMVServices.Web.Utils.AddAllowed != FINAppConstants.Y && Master.Mode == FINAppConstants.Add)
            {
                btnSave.Visible = false;
            }

        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();

                EntityData = null;
                dtGridData = DBMethod.ExecuteQuery(User_DAL.GetUserOrgIntersectData(Master.StrRecordId)).Tables[0];
                dtURGridData = DBMethod.ExecuteQuery(User_DAL.GetUserRoleIntersectData(Master.StrRecordId)).Tables[0];
                BindGrid(dtGridData);
                BindGridUR(dtURGridData);

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    // divpwd.Visible = false;

                    txtUsercode.Enabled = false;
                    using (IRepository<SSM_USERS> userCtx = new DataRepository<SSM_USERS>())
                    {
                        sSM_USERS = userCtx.Find(r =>
                            (r.USER_CODE == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = sSM_USERS;

                    txtUsercode.Text = sSM_USERS.USER_CODE;
                    txtFirstName.Text = sSM_USERS.FIRST_NAME;
                    txtUsercodeol.Text = sSM_USERS.USER_CODE_OL;
                    txtFirstNameol.Text = sSM_USERS.FIRST_NAME_OL;
                    txtLastName.Text = sSM_USERS.LAST_NAME;
                    txtMidName.Text = sSM_USERS.MIDDLE_NAME;
                    txtSurName.Text = sSM_USERS.SURNAME;
                    txtEffdate.Text = DBMethod.ConvertDateToString(sSM_USERS.EFFECTIVE_DATE.ToString());
                    if (sSM_USERS.ENABLED_FLAG == "1")
                    {
                        chkActive.Checked = true;
                    }
                    else
                    {
                        chkActive.Checked = false;
                    }
                    if (sSM_USERS.USER_PASSWORD != null)
                    {
                        txtUCPassword.Text = sSM_USERS.USER_PASSWORD;
                        hfPassword.Value = EnCryptDecrypt.CryptorEngine.Decrypt(sSM_USERS.USER_PASSWORD.ToString(),true);
                        txtUCPassword.Attributes.Add("Value", hfPassword.Value.ToString());
                    }
                        

                    RBG.SelectedValue = sSM_USERS.GENDER;
                    ddlTitle.SelectedValue = sSM_USERS.TITLE;

                    if (sSM_USERS.DATE_OF_BIRTH != null)
                    {
                        txtDateofBirth.Text = DBMethod.ConvertDateToString(sSM_USERS.DATE_OF_BIRTH.ToString());
                    }
                    ddlNationality.SelectedValue = sSM_USERS.NATIONALITY;
                    ddlEmpid.SelectedValue = sSM_USERS.PASSPORT_NUMBER_OR_EMP_ID;
                    if (sSM_USERS.CHANGE_PASSWORD_ON_NEXT_LOGIN == "1")
                    {
                        chkChangePasswordonNextLogin.Checked = true;
                    }
                    else
                    {
                        chkChangePasswordonNextLogin.Checked = false;
                    }
                    if (sSM_USERS.CHANGE_PASSWORD_INTERVAL == "1")
                    {
                        chkChangePasswordInterval.Checked = true;
                    }
                    else
                    {
                        chkChangePasswordInterval.Checked = false;
                    }

                    if (sSM_USERS.PASSWORD_LOCK == "1")
                    {
                        chkPasswordLock.Checked = true;
                    }
                    else
                    {
                        chkPasswordLock.Checked = false;
                    }
                    //ddlSupervisor.SelectedValue = sSM_USERS.SUPERVISOR_CODE;
                    //ddlgroup.SelectedValue = sSM_USERS.GROUP_ID;
                    txtAddress1.Text = sSM_USERS.ADDRESS_LINE_1;
                    txtAddress2.Text = sSM_USERS.ADDRESS_LINE_2;
                    ddlCountry.SelectedValue = sSM_USERS.COUNTRY;
                    ddlState.SelectedValue = sSM_USERS.STATE;
                    ddlCity.SelectedValue = sSM_USERS.CITY;
                    txtPhone.Text = sSM_USERS.PHONE;
                    txtMobile.Text = sSM_USERS.MOBILE_NO;
                    txtFax.Text = sSM_USERS.FAX_NUMBER;
                    txtEmailID.Text = sSM_USERS.EMAIL_ID;
                    txtTelexNo.Text = sSM_USERS.TELEX_NO;



                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    sSM_USERS = (SSM_USERS)EntityData;
                }


                sSM_USERS.USER_CODE = txtUsercode.Text;
                sSM_USERS.FIRST_NAME = txtFirstName.Text;
                sSM_USERS.USER_CODE_OL = txtUsercodeol.Text;
                sSM_USERS.FIRST_NAME_OL = txtFirstNameol.Text;
                sSM_USERS.LAST_NAME = txtLastName.Text;
                sSM_USERS.MIDDLE_NAME = txtMidName.Text;
                sSM_USERS.SURNAME = txtSurName.Text;
                sSM_USERS.EFFECTIVE_DATE = DBMethod.ConvertStringToDate(txtEffdate.Text.ToString());
                if (chkActive.Checked == true)
                {
                    sSM_USERS.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                }
                else
                {
                    sSM_USERS.ENABLED_FLAG = FINAppConstants.DisabledFlag;
                }
                sSM_USERS.USER_PASSWORD = EnCryptDecrypt.CryptorEngine.Encrypt(hfPassword.Value.ToString(), true);
                sSM_USERS.GENDER = RBG.SelectedValue;
                sSM_USERS.TITLE = ddlTitle.SelectedValue;

                if (txtDateofBirth.Text != string.Empty)
                {
                    sSM_USERS.DATE_OF_BIRTH = DBMethod.ConvertStringToDate(txtDateofBirth.Text);
                }
                sSM_USERS.NATIONALITY = ddlNationality.SelectedValue;
                sSM_USERS.PASSPORT_NUMBER_OR_EMP_ID = ddlEmpid.SelectedValue;
                if (chkChangePasswordonNextLogin.Checked == true)
                {
                    sSM_USERS.CHANGE_PASSWORD_ON_NEXT_LOGIN = FINAppConstants.EnabledFlag;
                }
                else
                {
                    sSM_USERS.CHANGE_PASSWORD_ON_NEXT_LOGIN = FINAppConstants.DisabledFlag;
                }

                if (chkChangePasswordInterval.Checked == true)
                {
                    sSM_USERS.CHANGE_PASSWORD_INTERVAL = FINAppConstants.EnabledFlag;
                }
                else
                {
                    sSM_USERS.CHANGE_PASSWORD_INTERVAL = FINAppConstants.DisabledFlag;
                }

                if (chkPasswordLock.Checked == true)
                {
                    sSM_USERS.PASSWORD_LOCK = FINAppConstants.EnabledFlag;

                }
                else
                {
                    sSM_USERS.PASSWORD_LOCK = FINAppConstants.DisabledFlag;
                }
                //sSM_USERS.SUPERVISOR_CODE = ddlSupervisor.SelectedValue;
                //sSM_USERS.GROUP_ID = ddlgroup.SelectedValue;
                sSM_USERS.ADDRESS_LINE_1 = txtAddress1.Text;
                sSM_USERS.ADDRESS_LINE_2 = txtAddress2.Text;
                sSM_USERS.COUNTRY = ddlCountry.SelectedValue;
                sSM_USERS.STATE = ddlState.SelectedValue;
                sSM_USERS.CITY = ddlCity.SelectedValue;
                sSM_USERS.PHONE = txtPhone.Text;
                sSM_USERS.MOBILE_NO = txtMobile.Text;
                sSM_USERS.FAX_NUMBER = txtFax.Text;
                sSM_USERS.EMAIL_ID = txtEmailID.Text;
                sSM_USERS.TELEX_NO = txtTelexNo.Text;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    sSM_USERS.MODIFIED_BY = this.LoggedUserName;
                    sSM_USERS.MODIFIED_DATE = DateTime.Today;

                }
                else
                {
                    // sSM_USERS.RESIG_HDR_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_041.ToString(), false, true);
                    sSM_USERS.PK_ID = DBMethod.GetPrimaryKeyValue("SSM_USERS_SEQ");
                    sSM_USERS.ATTRIBUTE1 = "USER";
                    sSM_USERS.CREATED_BY = this.LoggedUserName;
                    sSM_USERS.CREATED_DATE = DateTime.Today;
                }
                sSM_USERS.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, sSM_USERS.USER_CODE);



                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }



                ErrorCollection = CommonUtils.IsEmptyGrid(dtGridData, "User Organization");
                if (ErrorCollection.Count > 0)
                {
                    return;
                }


                var tmpChildEntity = new List<Tuple<object, string>>();


                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    sSM_USER_ORG_INTERSECTS = new SSM_USER_ORG_INTERSECTS();
                    if (dtGridData.Rows[iLoop][FINColumnConstants.PK_ID].ToString() != "0")
                    {
                        using (IRepository<SSM_USER_ORG_INTERSECTS> userCtx = new DataRepository<SSM_USER_ORG_INTERSECTS>())
                        {
                            sSM_USER_ORG_INTERSECTS = userCtx.Find(r =>
                                (r.PK_ID == int.Parse(dtGridData.Rows[iLoop][FINColumnConstants.PK_ID].ToString()))
                                ).SingleOrDefault();
                        }
                    }



                    sSM_USER_ORG_INTERSECTS.ORG_ID = dtGridData.Rows[iLoop]["COMP_ID"].ToString();
                    if (dtGridData.Rows[iLoop]["EFFECTIVE_DATE"] != DBNull.Value)
                    {
                        sSM_USER_ORG_INTERSECTS.EFFECTIVE_DATE = DateTime.Parse(dtGridData.Rows[iLoop]["EFFECTIVE_DATE"].ToString());
                    }

                    if (dtGridData.Rows[iLoop]["END_DATE"] != DBNull.Value)
                    {
                        sSM_USER_ORG_INTERSECTS.END_DATE = DateTime.Parse(dtGridData.Rows[iLoop]["END_DATE"].ToString());
                    }
                    else
                    {
                        sSM_USER_ORG_INTERSECTS.END_DATE = null;
                    }
                    sSM_USER_ORG_INTERSECTS.ENABLED_FLAG = (dtGridData.Rows[iLoop][FINColumnConstants.ENABLED_FLAG].ToString() == FINAppConstants.TRUEFLAG ? FINAppConstants.EnabledFlag : FINAppConstants.DisabledFlag);

                    sSM_USER_ORG_INTERSECTS.WORKFLOW_COMPLETION_STATUS = "1";
                    sSM_USER_ORG_INTERSECTS.USER_CODE = txtUsercode.Text;

                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        sSM_USER_ORG_INTERSECTS.PK_ID = int.Parse(dtGridData.Rows[iLoop][FINColumnConstants.PK_ID].ToString());
                        tmpChildEntity.Add(new Tuple<object, string>(sSM_USER_ORG_INTERSECTS, FINAppConstants.Delete));
                    }
                    else
                    {

                        if (dtGridData.Rows[iLoop][FINColumnConstants.PK_ID].ToString() != "0" && dtGridData.Rows[iLoop][FINColumnConstants.PK_ID].ToString() != string.Empty)
                        {
                            sSM_USER_ORG_INTERSECTS.PK_ID = int.Parse(dtGridData.Rows[iLoop][FINColumnConstants.PK_ID].ToString());
                            sSM_USER_ORG_INTERSECTS.MODIFIED_BY = this.LoggedUserName;
                            sSM_USER_ORG_INTERSECTS.MODIFIED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(sSM_USER_ORG_INTERSECTS, FINAppConstants.Update));

                        }
                        else
                        {
                            sSM_USER_ORG_INTERSECTS.PK_ID = DBMethod.GetPrimaryKeyValue(FINSequenceConstant.SSM_USER_ORG_INTERSECTS_SEQ);
                            sSM_USER_ORG_INTERSECTS.CREATED_BY = this.LoggedUserName;
                            sSM_USER_ORG_INTERSECTS.CREATED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(sSM_USER_ORG_INTERSECTS, FINAppConstants.Add));
                        }
                    }

                }


                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            CommonUtils.SavePCEntity<SSM_USERS, SSM_USER_ORG_INTERSECTS>(sSM_USERS, tmpChildEntity, sSM_USER_ORG_INTERSECTS);
                            savedBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {
                            CommonUtils.SavePCEntity<SSM_USERS, SSM_USER_ORG_INTERSECTS>(sSM_USERS, tmpChildEntity, sSM_USER_ORG_INTERSECTS, true);
                            savedBool = true;
                            break;
                        }
                }

                if (Session[FINSessionConstants.GridDataUR] != null)
                {
                    dtURGridData = (DataTable)Session[FINSessionConstants.GridDataUR];
                }


                ErrorCollection = CommonUtils.IsEmptyGrid(dtURGridData, "User Role ");
                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                for (int iLoop = 0; iLoop < dtURGridData.Rows.Count; iLoop++)
                {
                    sSM_USER_ROLE_INTERSECTS = new SSM_USER_ROLE_INTERSECTS();
                    if (dtURGridData.Rows[iLoop]["PK_ID"].ToString() != "0")
                    {
                        using (IRepository<SSM_USER_ROLE_INTERSECTS> userCtx = new DataRepository<SSM_USER_ROLE_INTERSECTS>())
                        {
                            sSM_USER_ROLE_INTERSECTS = userCtx.Find(r =>
                                (r.PK_ID == int.Parse(dtURGridData.Rows[iLoop]["PK_ID"].ToString()))
                                ).SingleOrDefault();
                        }
                    }
                    sSM_USER_ROLE_INTERSECTS.ROLE_CODE = dtURGridData.Rows[iLoop]["ROLE_CODE"].ToString();
                    if (dtURGridData.Rows[iLoop]["EFFECTIVE_DATE"] != DBNull.Value)
                    {
                        sSM_USER_ROLE_INTERSECTS.EFFECTIVE_DATE = DateTime.Parse(dtURGridData.Rows[iLoop]["EFFECTIVE_DATE"].ToString());
                    }

                    if (dtURGridData.Rows[iLoop]["END_DATE"] != DBNull.Value)
                    {
                        sSM_USER_ROLE_INTERSECTS.END_DATE = DateTime.Parse(dtURGridData.Rows[iLoop]["END_DATE"].ToString());
                    }
                    else
                    {
                        sSM_USER_ROLE_INTERSECTS.END_DATE = null;
                    }
                    sSM_USER_ROLE_INTERSECTS.ENABLED_FLAG = (dtURGridData.Rows[iLoop][FINColumnConstants.ENABLED_FLAG].ToString() == FINAppConstants.TRUEFLAG ? FINAppConstants.EnabledFlag : FINAppConstants.DisabledFlag);

                    sSM_USER_ROLE_INTERSECTS.WORKFLOW_COMPLETION_STATUS = "1";
                    sSM_USER_ROLE_INTERSECTS.USER_CODE = txtUsercode.Text;






                    if (dtURGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {

                        tmpChildEntity.Add(new Tuple<object, string>(sSM_USER_ROLE_INTERSECTS, "D"));
                    }
                    else
                    {

                        // Duplicate Validation Through Backend Package PKG_VALIDATIONS

                        //ProReturn = FIN.DAL.HR.Category_DAL.GetSPFOR_ERR_MGR_CATEGORY(hR_CATEGORIES.CATEGORY_CODE, hR_CATEGORIES.CATEGORY_ID);

                        //if (ProReturn != string.Empty)
                        //{
                        //    if (ProReturn != "0")
                        //    {
                        //        ErrorCollection.Add("CATEGORY", ProReturn);
                        //        if (ErrorCollection.Count > 0)
                        //        {
                        //            return;
                        //        }
                        //    }
                        //}



                        if (dtURGridData.Rows[iLoop]["PK_ID"].ToString() != "0")
                        {
                            sSM_USER_ROLE_INTERSECTS.PK_ID = int.Parse(dtURGridData.Rows[iLoop][FINColumnConstants.PK_ID].ToString());
                            sSM_USER_ROLE_INTERSECTS.MODIFIED_BY = this.LoggedUserName;
                            sSM_USER_ROLE_INTERSECTS.MODIFIED_DATE = DateTime.Today;
                            DBMethod.SaveEntity<SSM_USER_ROLE_INTERSECTS>(sSM_USER_ROLE_INTERSECTS, true);


                        }
                        else
                        {

                            sSM_USER_ROLE_INTERSECTS.PK_ID = DBMethod.GetPrimaryKeyValue(FINSequenceConstant.SSM_USER_ROLE_INTERSECTS_SEQ);
                            sSM_USER_ROLE_INTERSECTS.CREATED_BY = this.LoggedUserName;
                            sSM_USER_ROLE_INTERSECTS.CREATED_DATE = DateTime.Today;
                            DBMethod.SaveEntity<SSM_USER_ROLE_INTERSECTS>(sSM_USER_ROLE_INTERSECTS);

                        }


                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void FillComboBox()
        {

            Lookup_BLL.GetLookUpValues(ref ddlTitle, "TITLE");
           // Country_BLL.getCountryName(ref ddlNationality);
            Lookup_BLL.GetLookUpValues(ref ddlNationality, "NATIONALITY");
            FIN.BLL.HR.Employee_BLL.GetEmployeeName(ref ddlEmpid);
            //FIN.BLL.HR.Employee_BLL.GetEmployeeName(ref ddlSupervisor);
            //FIN.BLL.GL.AccountingGroups_BLL.fn_getAccountGroup(ref ddlgroup);
            Country_BLL.getCountryName(ref ddlCountry);
            States_BLL.getStateDetails(ref ddlState);
            TownCity_BLL.getCity(ref ddlCity);

        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                System.Collections.SortedList slControls = new System.Collections.SortedList();

                slControls[0] = txtUsercode;
                slControls[1] = txtFirstName;
                slControls[2] = txtSurName;
                slControls[3] = txtEffdate;
                slControls[4] = txtAddress1;
                slControls[5] = ddlCountry;
                slControls[6] = ddlState;
                slControls[7] = ddlCity;
                slControls[8] = txtMobile;
                slControls[9] = txtEmailID;
                Dictionary<string, string> Prop_File_Data;
                Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/SSM_" + Session["Sel_Lng"].ToString() + ".properties"));

                string strCtrlTypes = "TextBox~TextBox~TextBox~TextBox~TextBox~DropDownList~DropDownList~DropDownList~TextBox~TextBox";
                string strMessage = Prop_File_Data["User_Code_P"] + " ~ " + Prop_File_Data["First_Name_P"] + " ~ " + Prop_File_Data["SurName_P"] + " ~ " + Prop_File_Data["Effective_Date_P"] + " ~ " + Prop_File_Data["Address_1_P"] + " ~ " + Prop_File_Data["Country_P"] + " ~ " + Prop_File_Data["State_P"] + " ~ " + Prop_File_Data["City_P"] + " ~ " + Prop_File_Data["Mobile_P"] + " ~ " + Prop_File_Data["Email_ID_P"] + "";
                //string strMessage = "User code ~ First Name ~ Sur Name ~ Effective Date~Address1~Country~State~City~Mobile~Email";

                ErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                AssignToBE();

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion




        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                DBMethod.DeleteEntity<SSM_USERS>(sSM_USERS);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void btnLoginDetails_Click(object sender, EventArgs e)
        {
            
            divTabLoginDet.Visible = true;
            divTabAddDet.Visible = false;
            divTabOrgDet.Visible = false;
            divTabUserRole.Visible = false;
            txtUCPassword.Attributes.Add("Value", hfPassword.Value.ToString());
        }

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr["ENABLED_FLAG"] = "FALSE";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("USER_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void BindGridUR(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridDataUR] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr["ENABLED_FLAG"] = "FALSE";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvDataUR.DataSource = dt_tmp;
                gvDataUR.DataBind();
                GridViewRow gvr = gvDataUR.FooterRow;
                FillFooterGridURCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("USER_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>


        #endregion

        //  # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>

        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = -1;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("UserOrg_Row_cnl", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void gvData_URRowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtURGridData = (DataTable)Session[FINSessionConstants.GridDataUR];
                }
                gvDataUR.EditIndex = -1;

                BindGridUR(dtURGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("UserOrg_URRow_cnl", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }

                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    else
                    {
                        dtGridData.Rows.Add(drList);
                        BindGrid(dtGridData);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("UserOrg_Row_Cmd", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }

        protected void gvData_URRowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridDataUR] != null)
                {
                    dtURGridData = (DataTable)Session[FINSessionConstants.GridDataUR];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvDataUR.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }

                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControlUR(gvr, dtURGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    else
                    {
                        dtURGridData.Rows.Add(drList);
                        BindGridUR(dtURGridData);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("UserOrg_Row_CmdUR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {

            System.Collections.SortedList slControls = new System.Collections.SortedList();
            DropDownList ddlorg = gvr.FindControl("ddlorg") as DropDownList;
            TextBox dtpStartDate = gvr.FindControl("dtpStartDate") as TextBox;
            TextBox dtpEndDate = gvr.FindControl("dtpEndDate") as TextBox;
            CheckBox chkact = gvr.FindControl("chkact") as CheckBox;

            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList[FINColumnConstants.PK_ID] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }

            slControls[0] = ddlorg;
            slControls[1] = dtpStartDate;
            slControls[2] = dtpStartDate;
            slControls[3] = dtpEndDate;
            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/SSM_" + Session["Sel_Lng"].ToString() + ".properties"));

            ErrorCollection.Clear();
            string strCtrlTypes = "DropDownList~TextBox~DateTime~DateRangeValidate";
            string strMessage = Prop_File_Data["Organization_P"] + " ~ " + Prop_File_Data["Effective_Date_P"] + " ~ " + Prop_File_Data["Effective_Date_P"] + " ~ " + Prop_File_Data["End_Date_P"] +  "";
            //string strMessage = "Organization ~ Effective Date ~ Effective Date ~ End Date";
            ErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);
            if (ErrorCollection.Count > 0)
                return drList;


            //DateTime PSGDate = DBMethod.ConvertStringToDate(dtpPeriodStartDateGrid.Text);
            //if (dtpPeriodEndDateGrid.Text.ToString().Length > 0)
            //{
            //    DateTime PEGDate = DBMethod.ConvertStringToDate(dtpPeriodEndDateGrid.Text);

            //    if ((PSGDate - PEGDate).TotalDays > 0)
            //    {
            //        ErrorCollection.Add("FromTODate", "From Date Must Be Greater then To Date");
            //        return drList;
            //    }

            //}

            //string strCondition = "RA_EMP_ID='" + ddlEmployee.Text.Trim().ToUpper() + "'";
            //strMessage = FINMessageConstatns.RecordAlreadyExists;
            //ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            //if (ErrorCollection.Count > 0)
            //{

            //    return drList;
            //}

            if (ddlorg.SelectedItem != null)
            {
                drList["COMP_ID"] = ddlorg.SelectedItem.Value;
                drList["COMP_INTERNAL_NAME"] = ddlorg.SelectedItem.Text;
            }


            if (dtpStartDate.Text.ToString().Length > 0)
            {
                drList["EFFECTIVE_DATE"] = DBMethod.ConvertStringToDate(dtpStartDate.Text.ToString());
            }

            if (dtpEndDate.Text.ToString().Length > 0)
            {

                drList["END_DATE"] = DBMethod.ConvertStringToDate(dtpEndDate.Text.ToString());
            }
            else
            {
                drList["END_DATE"] = DBNull.Value;
            }



            if (chkact.Checked)
            {
                drList[FINColumnConstants.ENABLED_FLAG] = "TRUE";
            }
            else
            {
                drList[FINColumnConstants.ENABLED_FLAG] = "FALSE";
            }
            drList[FINColumnConstants.DELETED] = FINAppConstants.N;

            return drList;
        }


        private DataRow AssignToGridControlUR(GridViewRow gvr, DataTable tmpdtURGridData, string GMode, int rowindex)
        {

            System.Collections.SortedList slControls = new System.Collections.SortedList();
            DropDownList ddlRole = gvr.FindControl("ddlRole") as DropDownList;
            TextBox dtpStartDateUR = gvr.FindControl("dtpStartDateUR") as TextBox;
            TextBox dtpEndDateUR = gvr.FindControl("dtpEndDateUR") as TextBox;
            CheckBox chkactUR = gvr.FindControl("chkactUR") as CheckBox;

            DataRow drList;
            DataTable dt_tmp = tmpdtURGridData.Copy();
            if (GMode == "A")
            {
                drList = dtURGridData.NewRow();
                drList[FINColumnConstants.PK_ID] = "0";
            }
            else
            {
                drList = dtURGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }

            slControls[0] = ddlRole;
            slControls[1] = dtpStartDateUR;
            slControls[2] = dtpStartDateUR;
            slControls[3] = dtpEndDateUR;
            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/SSM_" + Session["Sel_Lng"].ToString() + ".properties"));

            ErrorCollection.Clear();
            string strCtrlTypes = "DropDownList~TextBox~DateTime~DateRangeValidate";
            string strMessage = Prop_File_Data["Role_P"] + " ~ " + Prop_File_Data["Effective_Date_P"] + " ~ " + Prop_File_Data["Effective_Date_P"] + " ~ " + Prop_File_Data["End_Date_P"] + "";
            //string strMessage = "Organization ~ Effective Date ~ Effective Date ~ End Date";
            ErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);
            if (ErrorCollection.Count > 0)
                return drList;


            //DateTime PSGDate = DBMethod.ConvertStringToDate(dtpPeriodStartDateGrid.Text);
            //if (dtpPeriodEndDateGrid.Text.ToString().Length > 0)
            //{
            //    DateTime PEGDate = DBMethod.ConvertStringToDate(dtpPeriodEndDateGrid.Text);

            //    if ((PSGDate - PEGDate).TotalDays > 0)
            //    {
            //        ErrorCollection.Add("FromTODate", "From Date Must Be Greater then To Date");
            //        return drList;
            //    }

            //}

            //string strCondition = "RA_EMP_ID='" + ddlEmployee.Text.Trim().ToUpper() + "'";
            //strMessage = FINMessageConstatns.RecordAlreadyExists;
            //ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            //if (ErrorCollection.Count > 0)
            //{

            //    return drList;
            //}

            if (ddlRole.SelectedItem != null)
            {
                drList["ROLE_CODE"] = ddlRole.SelectedItem.Value;
                drList["ROLE_NAME"] = ddlRole.SelectedItem.Text;
            }


            if (dtpStartDateUR.Text.ToString().Length > 0)
            {
                drList["EFFECTIVE_DATE"] = DBMethod.ConvertStringToDate(dtpStartDateUR.Text.ToString());
            }

            if (dtpEndDateUR.Text.ToString().Length > 0)
            {

                drList["END_DATE"] = DBMethod.ConvertStringToDate(dtpEndDateUR.Text.ToString());
            }
            else
            {
                drList["END_DATE"] = DBNull.Value;
            }



            if (chkactUR.Checked)
            {
                drList[FINColumnConstants.ENABLED_FLAG] = "TRUE";
            }
            else
            {
                drList[FINColumnConstants.ENABLED_FLAG] = "FALSE";
            }
            drList[FINColumnConstants.DELETED] = FINAppConstants.N;

            return drList;
        }
        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                //else if (FacilityMasterBLL.ErrorCollection.Count > 0)
                //{
                //    ErrorCollection = FacilityMasterBLL.ErrorCollection;
                //    return;
                //}
                else
                {
                    gvData.EditIndex = -1;
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("UserOrg_Row_upd", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }



        protected void gvData_URRowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvDataUR.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridDataUR] != null)
                {
                    dtURGridData = (DataTable)Session[FINSessionConstants.GridDataUR];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControlUR(gvr, dtURGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                //else if (FacilityMasterBLL.ErrorCollection.Count > 0)
                //{
                //    ErrorCollection = FacilityMasterBLL.ErrorCollection;
                //    return;
                //}
                else
                {
                    gvDataUR.EditIndex = -1;
                    BindGridUR(dtURGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("UserOrg_Row_updUR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row delete mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewDeleteEventArgs indicates which row's delete button was clicked. </param>

        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                dtGridData.Rows[e.RowIndex][FINColumnConstants.DELETED] = FINAppConstants.Y;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("UserOrg_Row_del", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }
        protected void gvData_URRowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridDataUR] != null)
                {
                    dtURGridData = (DataTable)Session[FINSessionConstants.GridDataUR];
                }
                dtURGridData.Rows[e.RowIndex][FINColumnConstants.DELETED] = FINAppConstants.Y;
                BindGridUR(dtURGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("UserOrg_Row_delUR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>
        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("UserOrg_Row_Edt", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void gvData_URRowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridDataUR] != null)
                {
                    dtURGridData = (DataTable)Session[FINSessionConstants.GridDataUR];
                }
                gvDataUR.EditIndex = e.NewEditIndex;
                BindGridUR(dtURGridData);
                GridViewRow gvr = gvDataUR.Rows[e.NewEditIndex];
                FillFooterGridURCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("UserOrg_Row_EdtUR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>
        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("UserOrgRC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }

        protected void gvData_URRowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvDataUR.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("UserOrgRCUR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }

        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddlorg = tmpgvr.FindControl("ddlorg") as DropDownList;

                FIN.BLL.GL.Organisation_BLL.getCompanyNM(ref ddlorg);


                if (gvData.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                {
                    ddlorg.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["COMP_ID"].ToString();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void FillFooterGridURCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddlRole = tmpgvr.FindControl("ddlRole") as DropDownList;

                FIN.BLL.SSM.Role_BLL.GetRole(ref ddlRole);


                if (gvDataUR.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                {
                    ddlRole.SelectedValue = gvDataUR.DataKeys[gvDataUR.EditIndex].Values["ROLE_CODE"].ToString();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("FillFootGridUR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (bol_rowVisiable)
                    e.Row.Visible = false;

                if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                {
                    e.Row.Visible = false;
                }

            }
        }

        protected void gvData_URRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (bol_rowVisiable)
                    e.Row.Visible = false;

                if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                {
                    e.Row.Visible = false;
                }

            }
        }

        protected void btnAddrsdtls_Click(object sender, EventArgs e)
        {

            //if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
            //{

            //    sSM_USERS.MODIFIED_BY = this.LoggedUserName;
            //    sSM_USERS.MODIFIED_DATE = DateTime.Today;

            //    DBMethod.SaveEntity<SSM_USERS>(sSM_USERS, true);

            //    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
            //}
        }

        protected void btnAddLoginClear_Click(object sender, EventArgs e)
        {
            //sSM_USERS.USER_PASSWORD = "";
            //sSM_USERS.GENDER = "";
            //sSM_USERS.TITLE = "0";


            //sSM_USERS.DATE_OF_BIRTH = null;

            //sSM_USERS.NATIONALITY = "0";
            //sSM_USERS.PASSPORT_NUMBER_OR_EMP_ID = "0";

            //sSM_USERS.SUPERVISOR_CODE = "0";
            //sSM_USERS.GROUP_ID = "0";
        }

        protected void btnAddrsdtlsClear_Click(object sender, EventArgs e)
        {
            //sSM_USERS.ADDRESS_LINE_1 = "";
            //sSM_USERS.ADDRESS_LINE_2 = "";
            //sSM_USERS.COUNTRY = "0";
            //sSM_USERS.STATE = "0";
            //sSM_USERS.CITY = "0";
            //sSM_USERS.PHONE = "";
            //sSM_USERS.MOBILE_NO = "";
            //sSM_USERS.FAX_NUMBER = "";
            //sSM_USERS.EMAIL_ID = "";
            //sSM_USERS.TELEX_NO = "";

        }

        protected void btnAddress_Click(object sender, EventArgs e)
        {
            divTabLoginDet.Visible = false;
            divTabAddDet.Visible = true;
            divTabOrgDet.Visible = false;
            divTabUserRole.Visible = false;
        }

        protected void btnsav_Click(object sender, EventArgs e)
        {
            //    try
            //    {

            //        ErrorCollection.Clear();

            //        AssignToBE();


            //        switch (Master.Mode)
            //        {
            //            case FINAppConstants.Add:
            //                {
            //                    DBMethod.SaveEntity<SSM_USERS>(sSM_USERS);
            //                    //  DisplaySaveCompleteMessage(Master.ListPageToOpen);
            //                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
            //                    break;
            //                }
            //            case FINAppConstants.Update:
            //                {

            //                    DBMethod.SaveEntity<SSM_USERS>(sSM_USERS, true);
            //                    //  DisplaySaveCompleteMessage(Master.ListPageToOpen);
            //                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
            //                    break;

            //                }
            //        }
            //    }
            //    catch (Exception ex)
            //    {
            //        ErrorCollection.Add("Sve", ex.Message);
            //    }
            //    finally
            //    {
            //        if (ErrorCollection.Count > 0)
            //        {
            //            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
            //        }
            //    }
        }

        protected void btnAddLogindtls_Click(object sender, EventArgs e)
        {
            //if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
            //{



            //    sSM_USERS.MODIFIED_BY = this.LoggedUserName;
            //    sSM_USERS.MODIFIED_DATE = DateTime.Today;

            //    DBMethod.SaveEntity<SSM_USERS>(sSM_USERS, true);

            //    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);

            //}

        }

        protected void btnUserOrg_Click(object sender, EventArgs e)
        {
            divTabLoginDet.Visible = false;
            divTabAddDet.Visible = false;
            divTabOrgDet.Visible = true;
            divTabUserRole.Visible = false;
        }

        protected void btnUserRole_Click(object sender, EventArgs e)
        {
            divTabLoginDet.Visible = false;
            divTabAddDet.Visible = false;
            divTabOrgDet.Visible = false;
            divTabUserRole.Visible = true;
        }

        protected void txtPassword_TextChanged(object sender, EventArgs e)
        {
            hfPassword.Value = txtUCPassword.Text;
            txtUCPassword.Attributes.Add("value", hfPassword.Value.ToString());
        }



    }
}
