﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.DAL.SSM;
using FIN.BLL;

using FIN.BLL.SSM;
using VMVServices.Web;
namespace FIN.Client.SSM
{
    public partial class TableColumnStructEntry : PageBase
    {
        TAB_COL_STRUCT_DTL tAB_COL_STRUCT_DTL = new TAB_COL_STRUCT_DTL();
        Boolean bol_rowVisiable;
        DataTable dtGridData = new DataTable();
        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
            }

            if (VMVServices.Web.Utils.AddAllowed != FINAppConstants.Y && Master.Mode == FINAppConstants.Add)
            {
                btnSave.Visible = false;
            }

        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();

                EntityData = null;
                dtGridData = DBMethod.ExecuteQuery(TableColumnStruct_DAL.GetTabStructdtl_tcsid(Master.StrRecordId)).Tables[0];
                BindGrid(dtGridData);

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    FillData();
                }
                else
                {
                    if (Session["TCS_SCREEN_CODE"] != null)
                    {
                        ddlscreen.SelectedValue = Session["TCS_SCREEN_CODE"].ToString();
                        FILLTableNM();
                        Session["TCS_SCREEN_CODE"] = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        private void FillData()
        {
            using (IRepository<TAB_COL_STRUCT_DTL> userCtx = new DataRepository<TAB_COL_STRUCT_DTL>())
            {
                tAB_COL_STRUCT_DTL = userCtx.Find(r =>
                    (r.TAB_COL_STRUCT_ID == Master.StrRecordId)
                    ).SingleOrDefault();
            }

            EntityData = tAB_COL_STRUCT_DTL;


            if (tAB_COL_STRUCT_DTL.SCREEN_CODE != null)
            {
                ddlscreen.SelectedValue = tAB_COL_STRUCT_DTL.SCREEN_CODE;
                FILLTableNM();
            }
            ddltable.SelectedValue = tAB_COL_STRUCT_DTL.TABLE_NAME;

            fillcol();
            ddlcolumn.SelectedValue = tAB_COL_STRUCT_DTL.COLUMN_NAME;
            ddlorg.SelectedValue = tAB_COL_STRUCT_DTL.ORG_ID_COLUMN_NAME;
            txtdisplayName.Text = tAB_COL_STRUCT_DTL.DISPLAY_NAME;
            txtdisplayNameol.Text = tAB_COL_STRUCT_DTL.DISPLAY_NAME_OL;
            txtwidth.Text = tAB_COL_STRUCT_DTL.WIDTH.ToString();
            if (tAB_COL_STRUCT_DTL.COL_TYPE != null)
            {
                ddlType.SelectedValue = tAB_COL_STRUCT_DTL.COL_TYPE;
            }
            if (tAB_COL_STRUCT_DTL.MASTER_TABLE_NAME != null)
            {
                ddlmastab.SelectedValue = tAB_COL_STRUCT_DTL.MASTER_TABLE_NAME;
            }
            filldispcol();
            filllinkcol();

            if (tAB_COL_STRUCT_DTL.MASTER_LINK_COLUMN_NAME != null)
            {
                ddlmaslinkcol.SelectedValue = tAB_COL_STRUCT_DTL.MASTER_LINK_COLUMN_NAME;
            }

            if (tAB_COL_STRUCT_DTL.MASTER_DISP_COLUMN_NAME != null)
            {
                ddlMasterdiscol.SelectedValue = tAB_COL_STRUCT_DTL.MASTER_DISP_COLUMN_NAME;
            }

            if (tAB_COL_STRUCT_DTL.FLAG_MASTER == FINAppConstants.EnabledFlag)
            {
                chkmaster.Checked = true;
            }
            else
            {
                chkmaster.Checked = false;
            }
            filltabcol();

            ddlType_SelectedIndexChanged(ddlType, new EventArgs());
            txtFormula.Text = tAB_COL_STRUCT_DTL.COL_FORMULA;
        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>

        private void FillComboBox()
        {
            TableColumnStruct_BLL.GetTableName(ref ddltable);

            TableColumnStruct_BLL.GetTableName(ref ddlmastab);
            TableColumnStruct_BLL.GetScreenName(ref ddlscreen);
        }


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    tAB_COL_STRUCT_DTL = (TAB_COL_STRUCT_DTL)EntityData;
                }

                if (ddlscreen.SelectedValue.Length > 0)
                {
                    tAB_COL_STRUCT_DTL.SCREEN_CODE = ddlscreen.SelectedValue;
                }
                tAB_COL_STRUCT_DTL.TABLE_NAME = ddltable.SelectedValue;
                tAB_COL_STRUCT_DTL.COLUMN_NAME = ddlcolumn.SelectedValue;
                tAB_COL_STRUCT_DTL.ORG_ID_COLUMN_NAME = ddlorg.SelectedValue;
                if (chkmaster.Checked == true)
                {
                    tAB_COL_STRUCT_DTL.MASTER_TABLE_NAME = ddlmastab.SelectedValue;
                }
                if (chkmaster.Checked == true)
                {
                    tAB_COL_STRUCT_DTL.MASTER_DISP_COLUMN_NAME = ddlMasterdiscol.SelectedValue;
                }
                if (chkmaster.Checked == true)
                {
                    tAB_COL_STRUCT_DTL.MASTER_LINK_COLUMN_NAME = ddlmaslinkcol.SelectedValue;
                }
                tAB_COL_STRUCT_DTL.DISPLAY_NAME = txtdisplayName.Text;
                tAB_COL_STRUCT_DTL.DISPLAY_NAME_OL = txtdisplayNameol.Text;
                tAB_COL_STRUCT_DTL.WIDTH = int.Parse(txtwidth.Text);
                tAB_COL_STRUCT_DTL.COL_TYPE = ddlType.SelectedValue.ToString();
                tAB_COL_STRUCT_DTL.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;
                tAB_COL_STRUCT_DTL.FLAG_MASTER = chkmaster.Checked == true ? FINAppConstants.EnabledFlag : FINAppConstants.DisabledFlag;
                tAB_COL_STRUCT_DTL.COL_FORMULA = txtFormula.Text;
                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    //tAB_COL_STRUCT_DTL.MODIFIED_BY = this.LoggedUserName;
                    //tAB_COL_STRUCT_DTL.MODIFIED_DATE = DateTime.Today;

                }
                else
                {
                    tAB_COL_STRUCT_DTL.TAB_COL_STRUCT_ID = FINSP.GetSPFOR_SEQCode("SSM_021".ToString(), false, true);

                    //tAB_COL_STRUCT_DTL.CREATED_BY = this.LoggedUserName;
                    //tAB_COL_STRUCT_DTL.CREATED_DATE = DateTime.Today;

                }
                VMVServices.Web.Utils.SavedRecordId = "";

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            //try
            //{

            //    ErrorCollection.Clear();

            //    AssignToBE();


            //    switch (Master.Mode)
            //    {

            //        case FINAppConstants.Add:
            //            {
            //                DBMethod.SaveEntity<TAB_COL_STRUCT_DTL>(tAB_COL_STRUCT_DTL);
            //                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
            //                break;
            //            }
            //        case FINAppConstants.Update:
            //            {

            //                DBMethod.SaveEntity<TAB_COL_STRUCT_DTL>(tAB_COL_STRUCT_DTL, true);
            //                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
            //                break;

            //            }
            //    }
            //}
            //catch (Exception ex)
            //{
            //    ErrorCollection.Add("Save", ex.Message);
            //}
            //finally
            //{
            //    if (ErrorCollection.Count > 0)
            //    {
            //        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
            //    }
            //}

        }
        #endregion




        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                DBMethod.DeleteEntity<TAB_COL_STRUCT_DTL>(tAB_COL_STRUCT_DTL);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }



        protected void ddltable_SelectedIndexChanged(object sender, EventArgs e)
        {
            fillcol();
            dtGridData = DBMethod.ExecuteQuery(TableColumnStruct_DAL.GetTabStructdtl_Tabname(ddltable.SelectedValue)).Tables[0];
            BindGrid(dtGridData);
        }

        private void fillcol()
        {
            TableColumnStruct_BLL.GetColName(ref ddlcolumn, ddltable.SelectedValue);
            TableColumnStruct_BLL.GetColName(ref ddlorg, ddltable.SelectedValue);
        }
        protected void ddlmastab_SelectedIndexChanged(object sender, EventArgs e)
        {
            filldispcol();
            filllinkcol();
        }
        private void filldispcol()
        {

            TableColumnStruct_BLL.GetColName(ref ddlMasterdiscol, ddlmastab.SelectedValue);
        }
        private void filllinkcol()
        {
            TableColumnStruct_BLL.GetColName(ref ddlmaslinkcol, ddlmastab.SelectedValue);
        }

        protected void btnadd_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                AssignToBE();


                switch (Master.Mode)
                {

                    case FINAppConstants.Add:
                        {
                            DBMethod.SaveEntity<TAB_COL_STRUCT_DTL>(tAB_COL_STRUCT_DTL);
                            Session["TCS_SCREEN_CODE"] = ddlscreen.SelectedValue.ToString();
                            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                            break;
                        }
                    case FINAppConstants.Update:
                        {
                            DBMethod.SaveEntity<TAB_COL_STRUCT_DTL>(tAB_COL_STRUCT_DTL, true);
                            Session["TCS_SCREEN_CODE"] = ddlscreen.SelectedValue.ToString();
                            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                            break;

                        }
                }



            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        private void BindGrid(DataTable dtData)
        {


            bol_rowVisiable = false;
            Session[FINSessionConstants.GridData] = dtData;
            DataTable dt_tmp = dtData.Copy();
            if (dt_tmp.Rows.Count == 0)
            {
                DataRow dr = dt_tmp.NewRow();
                dr[0] = "0";

                dt_tmp.Rows.Add(dr);
                bol_rowVisiable = true;
            }
            gvData.DataSource = dt_tmp;
            gvData.DataBind();


        }

        protected void chkmaster_CheckedChanged(object sender, EventArgs e)
        {
            filltabcol();
        }

        private void filltabcol()
        {
            if (chkmaster.Checked == true)
            {
                div_MasterDet.Visible = true;
                ddlmastab.Enabled = true;
                ddlMasterdiscol.Enabled = true;
                ddlmaslinkcol.Enabled = true;
            }
            else
            {
                div_MasterDet.Visible = false;
                ddlmastab.Enabled = false;
                ddlMasterdiscol.Enabled = false;
                ddlmaslinkcol.Enabled = false;
            }

        }

        protected void ddlscreen_SelectedIndexChanged(object sender, EventArgs e)
        {
            FILLTableNM();
        }
        private void FILLTableNM()
        {
            TableColumnStruct_BLL.GetTableNM_BasedScreencode(ref ddltable, ddlscreen.SelectedValue);
            DataTable dt_screenDet = DBMethod.ExecuteQuery(FIN.DAL.SSM.TableColumnStruct_DAL.GetTabStructdtl_Screen(ddlscreen.SelectedValue)).Tables[0];
            if (dt_screenDet.Rows.Count > 0)
            {
                ddltable.SelectedValue = dt_screenDet.Rows[0]["Table_name"].ToString();
                fillcol();
                ddlorg.SelectedValue = dt_screenDet.Rows[0]["org_id_column_name"].ToString();
                BindGrid(dt_screenDet);
            }
            else
            {
                if (ddltable.Items.Count > 1)
                {
                    ddltable.SelectedIndex = 1;
                    fillcol();
                    DataTable dt_orgId = DBMethod.ExecuteQuery(FINSQL.GetVideopath(Session["FormCode"].ToString())).Tables[0];
                    ddlorg.SelectedValue = dt_orgId.Rows[0]["ORG_ID"].ToString();

                }
            }
        }

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            Master.Mode = FINAppConstants.Update;
            Master.StrRecordId = gvData.DataKeys[e.NewEditIndex].Values["TAB_COL_STRUCT_ID"].ToString();
            dtGridData = DBMethod.ExecuteQuery(TableColumnStruct_DAL.GetTabStructdtl_tcsid(Master.StrRecordId)).Tables[0];
            BindGrid(dtGridData);
            FillData();
        }

        protected void ddlType_SelectedIndexChanged(object sender, EventArgs e)
        {
            div_formula.Visible = false;
            if (ddlType.SelectedValue == "FORMULA")
            {
                div_formula.Visible = true;
            }
        }



    }
}