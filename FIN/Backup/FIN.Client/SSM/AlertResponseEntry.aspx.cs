﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.DAL.SSM;
using FIN.BLL;
using FIN.BLL.SSM;
using VMVServices.Web;

namespace FIN.Client.SSM
{
    public partial class AlertResponseEntry : PageBase
    {
        SSM_ALERT_USER_LEVELS sSM_ALERT_USER_LEVELS = new SSM_ALERT_USER_LEVELS();



        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
            }

            if (VMVServices.Web.Utils.AddAllowed != FINAppConstants.Y && Master.Mode == FINAppConstants.Add)
            {
                btnSave.Visible = false;
            }

        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();

                EntityData = null;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    using (IRepository<SSM_ALERT_USER_LEVELS> userCtx = new DataRepository<SSM_ALERT_USER_LEVELS>())
                    {
                        sSM_ALERT_USER_LEVELS = userCtx.Find(r =>
                            (r.ALERT_USER_LEVEL_CODE == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = sSM_ALERT_USER_LEVELS;

                    ddlAlertCode.SelectedValue = sSM_ALERT_USER_LEVELS.ALERT_CODE;
                    txtNoofAlerts.Text = sSM_ALERT_USER_LEVELS.NO_OF_ALERTS_DISPLAYED.ToString();
                    ddlLanguageCode.SelectedValue = sSM_ALERT_USER_LEVELS.LANGUAGE_CODE;
                    ddlUserCode.SelectedValue = sSM_ALERT_USER_LEVELS.USER_CODE;

                    if (sSM_ALERT_USER_LEVELS.EFFECTIVE_DATE != null)
                    {
                        txtEffdate.Text = DBMethod.ConvertDateToString(sSM_ALERT_USER_LEVELS.EFFECTIVE_DATE.ToString());
                    }
                    if (sSM_ALERT_USER_LEVELS.END_DATE != null)
                    {
                        txtenddate.Text = DBMethod.ConvertDateToString(sSM_ALERT_USER_LEVELS.END_DATE.ToString());
                    }
                    if (sSM_ALERT_USER_LEVELS.LAST_DISPLAYED_TIME != null)
                    {
                        txtlastdisptime.Text = DBMethod.ConvertDateToString(sSM_ALERT_USER_LEVELS.LAST_DISPLAYED_TIME.ToString());
                    }
                    txtremark.Text = sSM_ALERT_USER_LEVELS.REMARKS;
                    txtalertmsg.Text = sSM_ALERT_USER_LEVELS.ALERT_MESSAGE;

                    if (sSM_ALERT_USER_LEVELS.ENABLED_FLAG == FINAppConstants.EnabledFlag)
                    {
                        chkActive.Checked = true;
                    }
                    else
                    {
                        chkActive.Checked = false;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>

        private void FillComboBox()
        {
            Alert_BLL.GetAlertName(ref ddlAlertCode);
            Language_BLL.GetLanguageDetails(ref ddlLanguageCode);
            User_BLL.GetUserData(ref ddlUserCode);
        }


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    sSM_ALERT_USER_LEVELS = (SSM_ALERT_USER_LEVELS)EntityData;
                }



                sSM_ALERT_USER_LEVELS.ALERT_CODE = ddlAlertCode.SelectedValue;
                sSM_ALERT_USER_LEVELS.NO_OF_ALERTS_DISPLAYED = short.Parse(txtNoofAlerts.Text);
                sSM_ALERT_USER_LEVELS.LANGUAGE_CODE = ddlLanguageCode.SelectedValue;
                sSM_ALERT_USER_LEVELS.USER_CODE = ddlUserCode.SelectedValue;

                if (txtEffdate.Text != string.Empty)
                {
                    sSM_ALERT_USER_LEVELS.EFFECTIVE_DATE = DBMethod.ConvertStringToDate(txtEffdate.Text.ToString());
                }
                if (txtenddate.Text != string.Empty)
                {
                    sSM_ALERT_USER_LEVELS.END_DATE = DBMethod.ConvertStringToDate(txtenddate.Text.ToString());
                }
                sSM_ALERT_USER_LEVELS.REMARKS = txtremark.Text;
                sSM_ALERT_USER_LEVELS.ALERT_MESSAGE = txtalertmsg.Text;

                sSM_ALERT_USER_LEVELS.ENABLED_FLAG = chkActive.Checked == true ? FINAppConstants.EnabledFlag : FINAppConstants.DisabledFlag;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    sSM_ALERT_USER_LEVELS.MODIFIED_BY = this.LoggedUserName;
                    sSM_ALERT_USER_LEVELS.MODIFIED_DATE = DateTime.Today;

                }
                else
                {
                    sSM_ALERT_USER_LEVELS.ALERT_USER_LEVEL_CODE = FINSP.GetSPFOR_SEQCode(FINAppConstants.SSM_019.ToString(), false, true);

                    sSM_ALERT_USER_LEVELS.CREATED_BY = this.LoggedUserName;
                    sSM_ALERT_USER_LEVELS.CREATED_DATE = DateTime.Today;

                }

                sSM_ALERT_USER_LEVELS.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, sSM_ALERT_USER_LEVELS.ALERT_USER_LEVEL_CODE);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                AssignToBE();

                // Duplicate Check

                //ProReturn = FINSP.GetSPFOR_ERR_MGR_ITEM_CATOG(txtCategoryName.Text, sSM_ALERT_USER_LEVELS.ITEM_CATEGORY_ID, txtEffectiveStartDate.Text, txtEffectiveEnddate.Text);

                //if (ProReturn != string.Empty)
                //{

                //    if (ProReturn != "0")
                //    {
                //        ErrorCollection.Add("ITEMCATEGORYNAME", ProReturn);
                //        if (ErrorCollection.Count > 0)
                //        {
                //            return;
                //        }
                //    }
                //}


                switch (Master.Mode)
                {

                    case FINAppConstants.Add:
                        {
                            DBMethod.SaveEntity<SSM_ALERT_USER_LEVELS>(sSM_ALERT_USER_LEVELS);
                            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                            break;
                        }
                    case FINAppConstants.Update:
                        {

                            DBMethod.SaveEntity<SSM_ALERT_USER_LEVELS>(sSM_ALERT_USER_LEVELS, true);
                            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                            break;

                        }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion




        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                DBMethod.DeleteEntity<SSM_ALERT_USER_LEVELS>(sSM_ALERT_USER_LEVELS);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void txtEffectiveEnddate_TextChanged(object sender, EventArgs e)
        {

        }

        protected void ddlCategoryParent_SelectedIndexChanged(object sender, EventArgs e)
        {

        }


    }
}
       