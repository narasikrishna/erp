﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="MenuEntry.aspx.cs" Inherits="FIN.Client.SSM.MenuEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 500px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblMenuCode">
                Menu Code
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtMenuCode" MaxLength="50" CssClass="validate[required] RequiredField txtBox"
                    runat="server" TabIndex="1"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblFormCode">
                Form Code
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtFormCode" MaxLength="50" CssClass="validate[required] RequiredField txtBox"
                    runat="server" TabIndex="2"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblFormLable">
                Form Label
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtFormLabel" MaxLength="50" CssClass="validate[required] RequiredField txtBox_en"
                    runat="server" TabIndex="3"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblFormLabelol">
                Form Label(Arabic)
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtFormLabelol" MaxLength="50" CssClass="validate[required] RequiredField txtBox_ol"
                    runat="server" TabIndex="3"></asp:TextBox>
            </div>
        </div>
       
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblLanguageCode">
                Language Code
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 153px">
                <asp:DropDownList ID="ddlLanguageCode" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    TabIndex="4">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblMenuURL">
                Menu URL
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtMenuURL" MaxLength="50" CssClass="validate[required] RequiredField txtBox"
                    runat="server" TabIndex="5"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblReportURL">
                Report URL
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtReportURL" MaxLength="50" CssClass="validate[required] RequiredField txtBox"
                    runat="server" TabIndex="6"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblReportCode">
                Report Code
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtReportCode" MaxLength="50" CssClass="validate[required] RequiredField txtBox"
                    runat="server" TabIndex="7"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblPKColumnName">
                Primary Key Column Name
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtPKColumnName" MaxLength="50" CssClass="validate[required] RequiredField txtBox"
                    runat="server" TabIndex="8"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblOrderNo">
                Order No
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtOrderNo" MaxLength="50" CssClass="validate[required] RequiredField txtBox"
                    runat="server" TabIndex="9"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblActive">
                Active
            </div>
            <div class="divChkbox" style=" width: 150px">
                <asp:CheckBox ID="chkActive" runat="server" Checked="True" TabIndex="10" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                            TabIndex="5" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="6" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="7" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="8" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table width="100%">
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/SSM/SSMChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
