﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="LetterTemplateEntry.aspx.cs" Inherits="FIN.Client.SSM.LetterTemplateEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 750px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 110px" id="lblAlertCode">
                Header Left
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 190px">
                <asp:TextBox ID="txtHeadLeft" CssClass="validate[required] RequiredField txtBox"
                    Height="90px" runat="server" TabIndex="2"></asp:TextBox>
            </div>
            <div class="lblBox  LNOrient" style=" width: 110px" id="lblNoofAlerts">
                Header Right
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 190px">
                <asp:TextBox ID="txtHeadright" CssClass="validate[required] RequiredField txtBox"
                    Height="90px" runat="server" TabIndex="2"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 110px" id="lblLanguageCode">
                Header Center
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 510px">
                <asp:TextBox ID="txtHeadCenter" TextMode="MultiLine" CssClass="validate[required] RequiredField txtBox"
                    Height="60px" runat="server" TabIndex="3"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 110px" id="lblEffectiveStartDate">
                Letter Descrption
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 510px">
                <asp:TextBox ID="txtLetterDesc" TextMode="MultiLine" CssClass="validate[required] RequiredField txtBox"
                    Height="60px" runat="server" TabIndex="4"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 110px" id="lblLastDisplayedTime">
                Reference Number
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 160px">
                <asp:TextBox ID="txtRefNum" CssClass="validate[]  txtBox"
                    runat="server" TabIndex="5"></asp:TextBox>
            </div>
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblActive">
                Date Line
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 175px">
              <asp:TextBox ID="txtDateline" CssClass="validate[]  txtBox"
                    runat="server" TabIndex="6"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 110px" id="lblRemarks">
                To Address
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 510px">
                <asp:TextBox ID="txtAddress" CssClass="validate[] RequiredField txtBox" Height="60" TextMode="MultiLine" runat="server" TabIndex="7"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 110px" id="Div2">
                Sub Line
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 190px">
                <asp:TextBox ID="txtSubline" CssClass="validate[]  txtBox"
                    Height="90px" runat="server" TabIndex="8"></asp:TextBox>
            </div>
            <div class="lblBox  LNOrient" style=" width: 110px" id="Div3">
                Reference Line
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 190px">
                <asp:TextBox ID="txtRefline" CssClass="validate[]  txtBox"
                    Height="90px" runat="server" TabIndex="9"></asp:TextBox>
            </div>
        </div>
         <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 110px" id="Div1">
                Letter Content 1
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 510px">
                <asp:TextBox ID="txtLettercnt1" TextMode="MultiLine" CssClass="validate[required] RequiredField txtBox"
                    Height="60px" runat="server" TabIndex="10"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 110px" id="Div4">
                Letter Content 2
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 510px">
                <asp:TextBox ID="txtLettercnt2" TextMode="MultiLine" CssClass="validate[]  txtBox"
                    Height="60px" runat="server" TabIndex="11"></asp:TextBox>
            </div>
        </div>
         <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 110px" id="Div5">
                Footer Left
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 190px">
                <asp:TextBox ID="txtFooterleft" CssClass="validate[required] RequiredField txtBox"
                    Height="90px" runat="server" TabIndex="12"></asp:TextBox>
            </div>
            <div class="lblBox  LNOrient" style=" width: 110px" id="Div6">
                Footer Right
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 190px">
                <asp:TextBox ID="txtFooterright" CssClass="validate[required] RequiredField txtBox"
                    Height="90px" runat="server" TabIndex="13"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 110px" id="Div7">
                Footer Center
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 510px">
                <asp:TextBox ID="txtFootercenter" TextMode="MultiLine" CssClass="validate[required] RequiredField txtBox"
                    Height="60px" runat="server" TabIndex="14"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 110px" id="Div8">
                Signature Title
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 510px">
                <asp:TextBox ID="txtSigntitle" TextMode="MultiLine" CssClass="validate[required] RequiredField txtBox"
                    Height="40px" runat="server" TabIndex="15"></asp:TextBox>
            </div>
        </div>
         <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 110px" id="Div9">
               Signatory
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 160px">
                <asp:TextBox ID="txtSignatory" CssClass="validate[required] RequiredField txtBox"
                    runat="server" TabIndex="16"></asp:TextBox>
            </div>
            <div class="lblBox  LNOrient" style=" width: 150px" id="Div10">
                Title
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 175px">
              <asp:TextBox ID="txtTitle" CssClass="validate[]  txtBox"
                    runat="server" TabIndex="17"></asp:TextBox>
            </div>
        </div>
        <div clas
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" OnClick="btnSave_Click"
                            TabIndex="10" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="11" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="12" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="13" />
                    </td>
                </tr>
            </table>
        </div>
        <div class="divClear_10">
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table width="100%">
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
     <script src="../LanguageScript/SSM/SSMChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
