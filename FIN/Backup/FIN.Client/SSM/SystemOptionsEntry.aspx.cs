﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.DAL.SSM;
using FIN.BLL;
using FIN.BLL.SSM;
using VMVServices.Web;

namespace FIN.Client.SSM
{
    public partial class SystemOptionsEntry : PageBase
    {
        SSM_SYSTEM_OPTIONS sSM_SYSTEM_OPTIONS = new SSM_SYSTEM_OPTIONS();

        Boolean savedBool;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    divAR.Visible = false;
                    divHR.Visible = false;
                    divAP.Visible = false;
                    divFA.Visible = false;
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
            }

            if (VMVServices.Web.Utils.AddAllowed != FINAppConstants.Y && Master.Mode == FINAppConstants.Add)
            {
                btnSave.Visible = false;
            }

        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();

                EntityData = null;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    using (IRepository<SSM_SYSTEM_OPTIONS> userCtx = new DataRepository<SSM_SYSTEM_OPTIONS>())
                    {
                        sSM_SYSTEM_OPTIONS = userCtx.Find(r =>
                            (r.SYS_OPT_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = sSM_SYSTEM_OPTIONS;

                    ddlModule.SelectedValue = sSM_SYSTEM_OPTIONS.MODULE_CODE;
                    fillModData();
                    //if (sSM_SYSTEM_OPTIONS.MODULE_CODE == "AP")
                    //{
                    //    divAR.Visible = false;
                    //    divHR.Visible = false;
                    //    divFA.Visible = false;
                    //    divAP.Visible = true;
                    //    ddlAPliabilityAcct.SelectedValue = sSM_SYSTEM_OPTIONS.AP_LIABILITY_ACCT;
                    //    ddlAPadvanceacct.SelectedValue = sSM_SYSTEM_OPTIONS.AP_ADVANCE_ACCT;
                    //    ddlAPretentionacct.SelectedValue = sSM_SYSTEM_OPTIONS.AP_RETENTION_ACCT;
                    //    ddlAPdiscountacct.SelectedValue = sSM_SYSTEM_OPTIONS.AP_DISCOUNT_ACCT;
                    //    ddlAPexchangegain.SelectedValue = sSM_SYSTEM_OPTIONS.AP_EXCHANGE_GAIN_ACCT;
                    //    ddlAPexchangeloss.SelectedValue = sSM_SYSTEM_OPTIONS.AP_EXCHANGE_LOSS_ACCT;
                    //    txtAPpaymentterms.Text = sSM_SYSTEM_OPTIONS.AP_PAYMENT_TERMS.ToString();



                    //    if (sSM_SYSTEM_OPTIONS.AP_DEFAULT_CASH_ACCT_CODE != null)
                    //    {
                    //        ddlAPDefaultCash.SelectedValue = sSM_SYSTEM_OPTIONS.AP_DEFAULT_CASH_ACCT_CODE;
                    //    }

                    //    if (sSM_SYSTEM_OPTIONS.AP_START_DATE != null)
                    //    {
                    //        txtAPstartdate.Text = DBMethod.ConvertDateToString(sSM_SYSTEM_OPTIONS.AP_START_DATE.ToString());
                    //    }
                    //    if (sSM_SYSTEM_OPTIONS.AP_END_DATE != null)
                    //    {
                    //        txtAPenddate.Text = DBMethod.ConvertDateToString(sSM_SYSTEM_OPTIONS.AP_END_DATE.ToString());
                    //    }



                    //    if (sSM_SYSTEM_OPTIONS.AP_ACTIVE == FINAppConstants.EnabledFlag)
                    //    {
                    //        chkActive.Checked = true;
                    //    }
                    //    else
                    //    {
                    //        chkActive.Checked = false;
                    //    }
                    //}

                    //if (sSM_SYSTEM_OPTIONS.MODULE_CODE == "AR")
                    //{
                    //    divAP.Visible = false;
                    //    divHR.Visible = false;
                    //    divFA.Visible = false;
                    //    divAR.Visible = true;
                    //    ddlARdebitoracct.SelectedValue = sSM_SYSTEM_OPTIONS.AR_DEBITOR_ACCT;
                    //    ddlARrevenueacct.SelectedValue = sSM_SYSTEM_OPTIONS.AR_REVENUE_ACCT;
                    //    ddlARdiscountacct.SelectedValue = sSM_SYSTEM_OPTIONS.AR_DISCOUNT_ACCT;
                    //    ddlARexchangegain.SelectedValue = sSM_SYSTEM_OPTIONS.AR_EXCHANGE_GAIN_ACCT;
                    //    ddlARexchangeloss.SelectedValue = sSM_SYSTEM_OPTIONS.AR_EXCHANGE_LOSS_ACCT;
                    //    txtARpaymentterms.Text = sSM_SYSTEM_OPTIONS.AR_PAYMENT_TERMS.ToString();

                    //    if (sSM_SYSTEM_OPTIONS.AR_DEFAULT_CASH_ACCT_CODE != null)
                    //    {
                    //        ddlARDefaultCash.SelectedValue = sSM_SYSTEM_OPTIONS.AR_DEFAULT_CASH_ACCT_CODE;
                    //    }

                    //    if (sSM_SYSTEM_OPTIONS.AR_START_DATE != null)
                    //    {
                    //        txtARstartdate.Text = DBMethod.ConvertDateToString(sSM_SYSTEM_OPTIONS.AR_START_DATE.ToString());
                    //    }
                    //    if (sSM_SYSTEM_OPTIONS.AR_END_DATE != null)
                    //    {
                    //        txtARenddate.Text = DBMethod.ConvertDateToString(sSM_SYSTEM_OPTIONS.AR_END_DATE.ToString());
                    //    }


                    //    if (sSM_SYSTEM_OPTIONS.AR_ACTIVE == FINAppConstants.EnabledFlag)
                    //    {
                    //        ARchkact.Checked = true;
                    //    }
                    //    else
                    //    {
                    //        ARchkact.Checked = false;
                    //    }

                    //}

                    //if (sSM_SYSTEM_OPTIONS.MODULE_CODE == "HR")
                    //{
                    //    divAP.Visible = false;
                    //    divAR.Visible = false;
                    //    divFA.Visible = false;
                    //    divHR.Visible = true;
                    //    if (sSM_SYSTEM_OPTIONS.SSDP_EMPLOYEE != null)
                    //    {
                    //        txtssdpemplee.Text = sSM_SYSTEM_OPTIONS.SSDP_EMPLOYEE.ToString();
                    //    }
                    //    if (sSM_SYSTEM_OPTIONS.SSDP_EMPLOYER != null)
                    //    {
                    //        txtssdpemployer.Text = sSM_SYSTEM_OPTIONS.SSDP_EMPLOYER.ToString();
                    //    }
                    //    if (sSM_SYSTEM_OPTIONS.LOAN_ELIGIBLE_YEAR != null)
                    //    {
                    //        txtlneligibleyr.Text = sSM_SYSTEM_OPTIONS.LOAN_ELIGIBLE_YEAR.ToString();
                    //    }
                    //    if (sSM_SYSTEM_OPTIONS.LOAN_REELIGIBLE_YEAR != null)
                    //    {
                    //        txtlnreeligibleyr.Text = sSM_SYSTEM_OPTIONS.LOAN_REELIGIBLE_YEAR.ToString();
                    //    }
                    //    if (sSM_SYSTEM_OPTIONS.PERMISSION_HOURS != null)
                    //    {
                    //        txtHRpermissionhours.Text = sSM_SYSTEM_OPTIONS.PERMISSION_HOURS.ToString();
                    //    }
                    //    if (sSM_SYSTEM_OPTIONS.HR_PAY_ELEMENT_CODE != null)
                    //    {
                    //        ddlHRElement.SelectedValue = sSM_SYSTEM_OPTIONS.HR_PAY_ELEMENT_CODE.ToString();
                    //    }

                    //    if (sSM_SYSTEM_OPTIONS.HR_MEDICAL_DEDUCT_ELEMENT_CODE != null)
                    //    {
                    //        ddlHRMedical.SelectedValue = sSM_SYSTEM_OPTIONS.HR_MEDICAL_DEDUCT_ELEMENT_CODE;
                    //    }
                    //    if (sSM_SYSTEM_OPTIONS.HR_MOBI_DEDUCT_ELEMENT_CODE != null)
                    //    {
                    //        ddlHRMobile.SelectedValue = sSM_SYSTEM_OPTIONS.HR_MOBI_DEDUCT_ELEMENT_CODE;
                    //    }
                    //    if (sSM_SYSTEM_OPTIONS.HR_MOBI_INVOICE_ELEMENT_CODE != null)
                    //    {
                    //        ddlHRMobileInvoice.SelectedValue = sSM_SYSTEM_OPTIONS.HR_MOBI_INVOICE_ELEMENT_CODE;
                    //    }
                    //    if (sSM_SYSTEM_OPTIONS.HR_RENT_DEDUCT_ELEMENT_CODE != null)
                    //    {
                    //        ddlHRRent.SelectedValue = sSM_SYSTEM_OPTIONS.HR_RENT_DEDUCT_ELEMENT_CODE;
                    //    }

                    //    if (sSM_SYSTEM_OPTIONS.HR_BASIC_ELEMENT_CODE != null)
                    //    {
                    //        ddlBasicelementcode.SelectedValue = sSM_SYSTEM_OPTIONS.HR_BASIC_ELEMENT_CODE;
                    //    }
                    //    if (sSM_SYSTEM_OPTIONS.HR_MGRP_EMP_ELEMENT_CODE != null)
                    //    {
                    //        ddlMGRPEmpElement.SelectedValue = sSM_SYSTEM_OPTIONS.HR_MGRP_EMP_ELEMENT_CODE;
                    //    }
                    //    if (sSM_SYSTEM_OPTIONS.HR_MGRP_COMP_ELEMENT_CODE != null)
                    //    {
                    //        ddlMGRPCompElement.SelectedValue = sSM_SYSTEM_OPTIONS.HR_MGRP_COMP_ELEMENT_CODE;
                    //    }
                    //    if (sSM_SYSTEM_OPTIONS.HR_EARN_LEAVE != null)
                    //    {
                    //        ddlHREarnLeave.SelectedValue = sSM_SYSTEM_OPTIONS.HR_EARN_LEAVE;
                    //    }

                    //    if (sSM_SYSTEM_OPTIONS.HR_LEAVE_SAL != null)
                    //    {
                    //        ddlLeavesal.SelectedValue = sSM_SYSTEM_OPTIONS.HR_LEAVE_SAL;
                    //    }
                    //    if (sSM_SYSTEM_OPTIONS.HR_LEAVE_ENCASH != null)
                    //    {
                    //        ddlLeavencashment.SelectedValue = sSM_SYSTEM_OPTIONS.HR_LEAVE_ENCASH;
                    //    }


                    //    if (sSM_SYSTEM_OPTIONS.HR_EFFECTIVE_FROM_DATE != null)
                    //    {
                    //        txtHREfffromdate.Text = DBMethod.ConvertDateToString(sSM_SYSTEM_OPTIONS.HR_EFFECTIVE_FROM_DATE.ToString());
                    //    }
                    //    if (sSM_SYSTEM_OPTIONS.HR_EFFECTIVE_TO_DATE != null)
                    //    {
                    //        txtHREnddaate.Text = DBMethod.ConvertDateToString(sSM_SYSTEM_OPTIONS.HR_EFFECTIVE_TO_DATE.ToString());
                    //    }
                    //    if (sSM_SYSTEM_OPTIONS.LEAVE_ADJ_CODE != null)
                    //    {
                    //        ddlleaveadjustcode.SelectedValue = sSM_SYSTEM_OPTIONS.LEAVE_ADJ_CODE;
                    //    }
                    //    if (sSM_SYSTEM_OPTIONS.LEAVE_ADJ_EARN != null)
                    //    {
                    //        ddlleaveadjustEarnings.SelectedValue = sSM_SYSTEM_OPTIONS.LEAVE_ADJ_EARN;
                    //    }
                    //    if (sSM_SYSTEM_OPTIONS.HR_LOAN_PAYMENT != null)
                    //    {
                    //        ddlLoanPayment.SelectedValue = sSM_SYSTEM_OPTIONS.HR_LOAN_PAYMENT;
                    //    }
                    //    if (sSM_SYSTEM_OPTIONS.HR_LEAVE_INDM_SUSP_ACC != null)
                    //    {
                    //        ddlleaveindmsuspeacc.SelectedValue = sSM_SYSTEM_OPTIONS.HR_LEAVE_INDM_SUSP_ACC;
                    //    }
                    //    if (sSM_SYSTEM_OPTIONS.HR_OTHER_SUSP_ACC != null)
                    //    {
                    //        ddlothersuspenseacc.SelectedValue = sSM_SYSTEM_OPTIONS.HR_OTHER_SUSP_ACC;
                    //    }
                    //    if (sSM_SYSTEM_OPTIONS.HR_LEAVE_ADJUSTMENT_MONTH != null)
                    //    {
                    //        txtLeavePlan.Text = sSM_SYSTEM_OPTIONS.HR_LEAVE_ADJUSTMENT_MONTH.ToString();
                    //    }

                    //}
                    //if (sSM_SYSTEM_OPTIONS.MODULE_CODE == "FA")
                    //{
                    //    divAP.Visible = false;
                    //    divHR.Visible = false;
                    //    divAR.Visible = false;
                    //    divFA.Visible = true;
                    //    ddlFAAssetCostAcc.SelectedValue = sSM_SYSTEM_OPTIONS.FA_ASSET_CST_ACC;
                    //    ddlFADepriciationAcc.SelectedValue = sSM_SYSTEM_OPTIONS.FA_DEPRICIATION_ACC;
                    //    ddlFAAccumDepreciaAcc.SelectedValue = sSM_SYSTEM_OPTIONS.FA_ACCUM_DEPRICIATION_ACC;
                    //    ddlFADepreciationRun.SelectedValue = sSM_SYSTEM_OPTIONS.FA_DEPRICIATION_RUN;
                    //}

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>

        private void FillComboBox()
        {
            Lookup_BLL.GetModuleName(ref ddlModule, Session[FINSessionConstants.Sel_Lng].ToString());


            FIN.BLL.GL.AccountCodes_BLL.fn_getAccount(ref ddlARdebitoracct);
            FIN.BLL.GL.AccountCodes_BLL.fn_getAccount(ref ddlARrevenueacct);
            FIN.BLL.GL.AccountCodes_BLL.fn_getAccount(ref ddlARdiscountacct);
            FIN.BLL.GL.AccountCodes_BLL.fn_getAccount(ref ddlARexchangegain);
            FIN.BLL.GL.AccountCodes_BLL.fn_getAccount(ref ddlARexchangeloss);
            FIN.BLL.GL.AccountCodes_BLL.fn_getAccount(ref ddlARDefaultCash);
            FIN.BLL.GL.AccountCodes_BLL.fn_getAccount(ref ddlARClearanceAcc);

            FIN.BLL.GL.AccountCodes_BLL.fn_getAccount(ref ddlFAAssetCostAcc);
            FIN.BLL.GL.AccountCodes_BLL.fn_getAccount(ref ddlFADepriciationAcc);
            FIN.BLL.GL.AccountCodes_BLL.fn_getAccount(ref ddlFAAccumDepreciaAcc);
            Lookup_BLL.GetLookUpValues(ref ddlFADepreciationRun, "DEPR_RUN");



            FIN.BLL.PER.PayrollElements_BLL.getPayElements_deductiononly(ref ddlHRElement);
            FIN.BLL.PER.PayrollElements_BLL.getPayElements_deductiononly(ref  ddlHRMedical);
            FIN.BLL.PER.PayrollElements_BLL.getPayElements_deductiononly(ref ddlHRMobile);
            FIN.BLL.PER.PayrollElements_BLL.getPayElements_deductiononly(ref ddlHRMobileInvoice);
            FIN.BLL.PER.PayrollElements_BLL.getPayElements_deductiononly(ref ddlHRRent);
            FIN.BLL.HR.LeaveDefinition_BLL.GetLeave(ref ddlHREarnLeave);
            FIN.BLL.PER.PayrollElements_BLL.getPayElements_Earningonly(ref ddlLeavesal);
            FIN.BLL.PER.PayrollElements_BLL.getPayElements_Earningonly(ref ddlLeavencashment);

            FIN.BLL.PER.PayrollElements_BLL.getPayElements_Earningonly(ref ddlBasicelementcode);
            FIN.BLL.PER.PayrollElements_BLL.getPayElements_deductiononly(ref ddlMGRPCompElement);
            FIN.BLL.PER.PayrollElements_BLL.getPayElements_deductiononly(ref ddlMGRPEmpElement);
            FIN.BLL.PER.PayrollElements_BLL.getPayElements_Earningonly(ref ddlleaveadjustcode);
            FIN.BLL.PER.PayrollElements_BLL.getPayElements_Earningonly(ref ddlleaveadjustEarnings);
            FIN.BLL.PER.PayrollElements_BLL.getPayElements_Earningonly(ref ddlLoanPayment);


            FIN.BLL.GL.AccountCodes_BLL.fn_getAccount(ref ddlleaveindmsuspeacc);
            FIN.BLL.GL.AccountCodes_BLL.fn_getAccount(ref ddlothersuspenseacc);
            FIN.BLL.GL.AccountCodes_BLL.fn_getAccount(ref ddlLeaveSuspenseAcc);
            FIN.BLL.GL.AccountCodes_BLL.fn_getAccount(ref ddlIndemExpenseAcc);

            //Alert_BLL.GetAlertName(ref ddlAlertCode);
            //Language_BLL.GetLanguageDetails(ref ddlLanguageCode);
            //User_BLL.GetUserData(ref ddlUserCode);
        }


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                DataTable dtpk = new DataTable();
                dtpk = DBMethod.ExecuteQuery(FIN.DAL.SSM.SystemOptions_DAL.getSysoptID()).Tables[0];

                if (dtpk.Rows.Count > 0)
                {
                    using (IRepository<SSM_SYSTEM_OPTIONS> userCtx = new DataRepository<SSM_SYSTEM_OPTIONS>())
                    {
                        sSM_SYSTEM_OPTIONS = userCtx.Find(r =>
                            (r.SYS_OPT_ID == dtpk.Rows[0]["SYS_OPT_ID"].ToString())
                            ).SingleOrDefault();
                    }

                }



                sSM_SYSTEM_OPTIONS.MODULE_CODE = ddlModule.SelectedValue;
                if (ddlModule.SelectedValue == "AP")
                {
                    if (ddlAPliabilityAcct.SelectedValue.ToString().Trim().Length > 0)
                    {
                        sSM_SYSTEM_OPTIONS.AP_LIABILITY_ACCT = ddlAPliabilityAcct.SelectedValue;
                    }
                    else
                    {
                        sSM_SYSTEM_OPTIONS.AP_LIABILITY_ACCT = null;
                    }

                    if (ddlAPadvanceacct.SelectedValue.ToString().Trim().Length > 0)
                    {
                        sSM_SYSTEM_OPTIONS.AP_ADVANCE_ACCT = ddlAPadvanceacct.SelectedValue;
                    }
                    else
                    {
                        sSM_SYSTEM_OPTIONS.AP_ADVANCE_ACCT = null;
                    }

                    if (ddlAPretentionacct.SelectedValue.ToString().Trim().Length > 0)
                    {
                        sSM_SYSTEM_OPTIONS.AP_RETENTION_ACCT = ddlAPretentionacct.SelectedValue;
                    }
                    else
                    {
                        sSM_SYSTEM_OPTIONS.AP_RETENTION_ACCT = null;
                    }

                    if (ddlAPdiscountacct.SelectedValue.ToString().Trim().Length > 0)
                    {
                        sSM_SYSTEM_OPTIONS.AP_DISCOUNT_ACCT = ddlAPdiscountacct.SelectedValue;
                    }
                    else
                    {
                        sSM_SYSTEM_OPTIONS.AP_DISCOUNT_ACCT = null;
                    }

                    if (ddlAPexchangegain.SelectedValue.ToString().Trim().Length > 0)
                    {
                        sSM_SYSTEM_OPTIONS.AP_EXCHANGE_GAIN_ACCT = ddlAPexchangegain.SelectedValue;
                    }
                    else
                    {
                        sSM_SYSTEM_OPTIONS.AP_EXCHANGE_GAIN_ACCT = null;
                    }

                    if (ddlAPexchangeloss.SelectedValue.ToString().Trim().Length > 0)
                    {
                        sSM_SYSTEM_OPTIONS.AP_EXCHANGE_LOSS_ACCT = ddlAPexchangeloss.SelectedValue;
                    }
                    else
                    {
                        sSM_SYSTEM_OPTIONS.AP_EXCHANGE_LOSS_ACCT = null;
                    }


                    if (txtAPpaymentterms.Text.ToString().Trim().Length > 0)
                    {
                        sSM_SYSTEM_OPTIONS.AP_PAYMENT_TERMS = int.Parse(txtAPpaymentterms.Text);
                    }
                    else
                    {
                        sSM_SYSTEM_OPTIONS.AP_PAYMENT_TERMS = null;
                    }


                    if (ddlAPDefaultCash.SelectedValue.ToString().Trim().Length > 0)
                    {
                        sSM_SYSTEM_OPTIONS.AP_DEFAULT_CASH_ACCT_CODE = ddlAPDefaultCash.SelectedValue;
                    }
                    else
                    {
                        sSM_SYSTEM_OPTIONS.AP_DEFAULT_CASH_ACCT_CODE = null;
                    }



                    if (txtAPstartdate.Text != string.Empty)
                    {
                        sSM_SYSTEM_OPTIONS.AP_START_DATE = DBMethod.ConvertStringToDate(txtAPstartdate.Text.ToString());
                    }
                    if (txtAPenddate.Text != string.Empty)
                    {
                        sSM_SYSTEM_OPTIONS.AP_END_DATE = DBMethod.ConvertStringToDate(txtAPenddate.Text.ToString());
                    }
                    else
                    {
                        sSM_SYSTEM_OPTIONS.AP_END_DATE = null;
                    }

                    sSM_SYSTEM_OPTIONS.AP_ACTIVE = chkActive.Checked == true ? FINAppConstants.EnabledFlag : FINAppConstants.DisabledFlag;
                }

                if (ddlModule.SelectedValue == "AR")
                {

                    if (ddlARdebitoracct.SelectedValue.ToString().Trim().Length > 0)
                    {
                        sSM_SYSTEM_OPTIONS.AR_DEBITOR_ACCT = ddlARdebitoracct.SelectedValue;
                    }
                    else
                    {
                        sSM_SYSTEM_OPTIONS.AR_DEBITOR_ACCT = null;
                    }


                    if (ddlARrevenueacct.SelectedValue.ToString().Trim().Length > 0)
                    {
                        sSM_SYSTEM_OPTIONS.AR_REVENUE_ACCT = ddlARrevenueacct.SelectedValue;
                    }
                    else
                    {
                        sSM_SYSTEM_OPTIONS.AR_REVENUE_ACCT = null;
                    }


                    if (ddlARdiscountacct.SelectedValue.ToString().Trim().Length > 0)
                    {
                        sSM_SYSTEM_OPTIONS.AR_DISCOUNT_ACCT = ddlARdiscountacct.SelectedValue;
                    }
                    else
                    {
                        sSM_SYSTEM_OPTIONS.AR_DISCOUNT_ACCT = null;
                    }

                    if (ddlARexchangegain.SelectedValue.ToString().Trim().Length > 0)
                    {
                        sSM_SYSTEM_OPTIONS.AR_EXCHANGE_GAIN_ACCT = ddlARexchangegain.SelectedValue;
                    }
                    else
                    {
                        sSM_SYSTEM_OPTIONS.AR_EXCHANGE_GAIN_ACCT = null;
                    }


                    if (ddlARexchangeloss.SelectedValue.ToString().Trim().Length > 0)
                    {
                        sSM_SYSTEM_OPTIONS.AR_EXCHANGE_LOSS_ACCT = ddlARexchangeloss.SelectedValue;
                    }
                    else
                    {
                        sSM_SYSTEM_OPTIONS.AR_EXCHANGE_LOSS_ACCT = null;
                    }


                    if (txtARpaymentterms.Text.ToString().Trim().Length > 0)
                    {
                        sSM_SYSTEM_OPTIONS.AR_PAYMENT_TERMS = int.Parse(txtARpaymentterms.Text);
                    }
                    else
                    {
                        sSM_SYSTEM_OPTIONS.AR_PAYMENT_TERMS = null;
                    }

                    if (ddlARDefaultCash.SelectedValue.ToString().Trim().Length > 0)
                    {
                        sSM_SYSTEM_OPTIONS.AR_DEFAULT_CASH_ACCT_CODE = ddlARDefaultCash.SelectedValue;
                    }
                    else
                    {
                        sSM_SYSTEM_OPTIONS.AR_DEFAULT_CASH_ACCT_CODE = null;
                    }
                    if (ddlARClearanceAcc.SelectedValue.ToString().Trim().Length > 0)
                    {
                        sSM_SYSTEM_OPTIONS.AR_CLEARANCE_ACC = ddlARClearanceAcc.SelectedValue;
                    }
                    else
                    {
                        sSM_SYSTEM_OPTIONS.AR_CLEARANCE_ACC = null;
                    }
                  

                    if (txtARstartdate.Text != string.Empty)
                    {
                        sSM_SYSTEM_OPTIONS.AR_START_DATE = DBMethod.ConvertStringToDate(txtARstartdate.Text.ToString());
                    }
                    if (txtARenddate.Text != string.Empty)
                    {
                        sSM_SYSTEM_OPTIONS.AR_END_DATE = DBMethod.ConvertStringToDate(txtARenddate.Text.ToString());
                    }
                    else
                    {
                        sSM_SYSTEM_OPTIONS.AR_END_DATE = null;
                    }


                    sSM_SYSTEM_OPTIONS.AR_ACTIVE = ARchkact.Checked == true ? FINAppConstants.EnabledFlag : FINAppConstants.DisabledFlag;
                }

                if (ddlModule.SelectedValue == "HR")
                {

                    if (txtssdpemplee.Text.ToString().Trim().Length > 0)
                    {
                        sSM_SYSTEM_OPTIONS.SSDP_EMPLOYEE = decimal.Parse(txtssdpemplee.Text.ToString());
                    }
                    else
                    {
                        sSM_SYSTEM_OPTIONS.SSDP_EMPLOYEE = null;
                    }


                    if (txtssdpemployer.Text.ToString().Trim().Length > 0)
                    {
                        sSM_SYSTEM_OPTIONS.SSDP_EMPLOYER = decimal.Parse(txtssdpemployer.Text.ToString());
                    }
                    else
                    {
                        sSM_SYSTEM_OPTIONS.SSDP_EMPLOYER = null;
                    }


                    if (txtlneligibleyr.Text.ToString().Trim().Length > 0)
                    {
                        sSM_SYSTEM_OPTIONS.LOAN_ELIGIBLE_YEAR = decimal.Parse(txtlneligibleyr.Text.ToString());
                    }
                    else
                    {
                        sSM_SYSTEM_OPTIONS.LOAN_ELIGIBLE_YEAR = null;
                    }


                    if (txtlnreeligibleyr.Text.ToString().Trim().Length > 0)
                    {
                        sSM_SYSTEM_OPTIONS.LOAN_REELIGIBLE_YEAR = decimal.Parse(txtlnreeligibleyr.Text.ToString());
                    }
                    else
                    {
                        sSM_SYSTEM_OPTIONS.LOAN_REELIGIBLE_YEAR = null;
                    }

                    if (txtHRpermissionhours.Text.ToString().Trim().Length > 0)
                    {
                        sSM_SYSTEM_OPTIONS.PERMISSION_HOURS = decimal.Parse(txtHRpermissionhours.Text.ToString());
                    }
                    else
                    {
                        sSM_SYSTEM_OPTIONS.PERMISSION_HOURS = null;
                    }


                    if (ddlHRElement.SelectedValue.ToString().Trim().Length > 0)
                    {
                        sSM_SYSTEM_OPTIONS.HR_PAY_ELEMENT_CODE = ddlHRElement.SelectedValue;
                    }
                    else
                    {
                        sSM_SYSTEM_OPTIONS.HR_PAY_ELEMENT_CODE = null;
                    }


                    if (ddlHRMobile.SelectedValue.ToString().Trim().Length > 0)
                    {
                        sSM_SYSTEM_OPTIONS.HR_MOBI_DEDUCT_ELEMENT_CODE = ddlHRMobile.SelectedValue;
                    }
                    else
                    {
                        sSM_SYSTEM_OPTIONS.HR_MOBI_DEDUCT_ELEMENT_CODE = null;
                    }

                    if (ddlHRMobileInvoice.SelectedValue.ToString().Trim().Length > 0)
                    {
                        sSM_SYSTEM_OPTIONS.HR_MOBI_INVOICE_ELEMENT_CODE = ddlHRMobileInvoice.SelectedValue;
                    }
                    else
                    {
                        sSM_SYSTEM_OPTIONS.HR_MOBI_INVOICE_ELEMENT_CODE = null;
                    }

                    if (ddlHRMedical.SelectedValue.ToString().Trim().Length > 0)
                    {
                        sSM_SYSTEM_OPTIONS.HR_MEDICAL_DEDUCT_ELEMENT_CODE = ddlHRMedical.SelectedValue;
                    }
                    else
                    {
                        sSM_SYSTEM_OPTIONS.HR_MEDICAL_DEDUCT_ELEMENT_CODE = null;
                    }

                    if (ddlHRRent.SelectedValue.ToString().Trim().Length > 0)
                    {
                        sSM_SYSTEM_OPTIONS.HR_RENT_DEDUCT_ELEMENT_CODE = ddlHRRent.SelectedValue;
                    }
                    else
                    {
                        sSM_SYSTEM_OPTIONS.HR_RENT_DEDUCT_ELEMENT_CODE = null;
                    }

                    if (ddlBasicelementcode.SelectedValue.ToString().Trim().Length > 0)
                    {
                        sSM_SYSTEM_OPTIONS.HR_BASIC_ELEMENT_CODE = ddlBasicelementcode.SelectedValue;
                    }
                    else
                    {
                        sSM_SYSTEM_OPTIONS.HR_BASIC_ELEMENT_CODE = null;
                    }

                    if (ddlMGRPEmpElement.SelectedValue.ToString().Trim().Length > 0)
                    {
                        sSM_SYSTEM_OPTIONS.HR_MGRP_EMP_ELEMENT_CODE = ddlMGRPEmpElement.SelectedValue;
                    }
                    else
                    {
                        sSM_SYSTEM_OPTIONS.HR_MGRP_EMP_ELEMENT_CODE = null;
                    }

                    if (ddlMGRPCompElement.SelectedValue.ToString().Trim().Length > 0)
                    {
                        sSM_SYSTEM_OPTIONS.HR_MGRP_COMP_ELEMENT_CODE = ddlMGRPCompElement.SelectedValue;
                    }
                    else
                    {
                        sSM_SYSTEM_OPTIONS.HR_MGRP_COMP_ELEMENT_CODE = null;
                    }

                    if (ddlHREarnLeave.SelectedValue.ToString().Trim().Length > 0)
                    {
                        sSM_SYSTEM_OPTIONS.HR_EARN_LEAVE = ddlHREarnLeave.SelectedValue;
                    }
                    else
                    {
                        sSM_SYSTEM_OPTIONS.HR_EARN_LEAVE = null;
                    }

                    if (ddlLeavesal.SelectedValue.ToString().Trim().Length > 0)
                    {
                        sSM_SYSTEM_OPTIONS.HR_LEAVE_SAL = ddlLeavesal.SelectedValue;
                    }
                    else
                    {
                        sSM_SYSTEM_OPTIONS.HR_LEAVE_SAL = null;
                    }


                    if (ddlLeavencashment.SelectedValue.ToString().Trim().Length > 0)
                    {
                        sSM_SYSTEM_OPTIONS.HR_LEAVE_ENCASH = ddlLeavencashment.SelectedValue;
                    }
                    else
                    {
                        sSM_SYSTEM_OPTIONS.HR_LEAVE_ENCASH = null;
                    }
                    

                    if (txtHREfffromdate.Text != string.Empty)
                    {
                        sSM_SYSTEM_OPTIONS.HR_EFFECTIVE_FROM_DATE = DBMethod.ConvertStringToDate(txtHREfffromdate.Text.ToString());
                    }
                    if (txtHREnddaate.Text != string.Empty)
                    {
                        sSM_SYSTEM_OPTIONS.HR_EFFECTIVE_TO_DATE = DBMethod.ConvertStringToDate(txtHREnddaate.Text.ToString());
                    }
                    else
                    {
                        sSM_SYSTEM_OPTIONS.HR_EFFECTIVE_TO_DATE = null;
                    }

                    if (ddlleaveadjustcode.SelectedValue.ToString().Trim().Length > 0)
                    {
                        sSM_SYSTEM_OPTIONS.LEAVE_ADJ_CODE = ddlleaveadjustcode.SelectedValue;
                    }
                    else
                    {
                        sSM_SYSTEM_OPTIONS.LEAVE_ADJ_CODE = null;
                    }

                    if (ddlleaveadjustEarnings.SelectedValue.ToString().Trim().Length > 0)
                    {
                        sSM_SYSTEM_OPTIONS.LEAVE_ADJ_EARN = ddlleaveadjustEarnings.SelectedValue;
                    }
                    else
                    {
                        sSM_SYSTEM_OPTIONS.LEAVE_ADJ_EARN = null;
                    }

                    if (ddlLoanPayment.SelectedValue.ToString().Trim().Length > 0)
                    {
                        sSM_SYSTEM_OPTIONS.HR_LOAN_PAYMENT = ddlLoanPayment.SelectedValue;
                    }
                    else
                    {
                        sSM_SYSTEM_OPTIONS.HR_LOAN_PAYMENT = null;
                    }

                    if (ddlleaveindmsuspeacc.SelectedValue.ToString().Trim().Length > 0)
                    {
                        sSM_SYSTEM_OPTIONS.HR_LEAVE_INDM_SUSP_ACC = ddlleaveindmsuspeacc.SelectedValue;
                    }
                    else
                    {
                        sSM_SYSTEM_OPTIONS.HR_LEAVE_INDM_SUSP_ACC = null;
                    }

                    if (ddlothersuspenseacc.SelectedValue.ToString().Trim().Length > 0)
                    {
                        sSM_SYSTEM_OPTIONS.HR_OTHER_SUSP_ACC = ddlothersuspenseacc.SelectedValue;
                    }
                    else
                    {
                        sSM_SYSTEM_OPTIONS.HR_OTHER_SUSP_ACC = null;
                    }

                    if (txtLeavePlan.Text.ToString().Trim().Length > 0)
                    {
                        sSM_SYSTEM_OPTIONS.HR_LEAVE_ADJUSTMENT_MONTH = txtLeavePlan.Text;
                    }
                    else
                    {
                        sSM_SYSTEM_OPTIONS.HR_LEAVE_ADJUSTMENT_MONTH = null;
                    }



                    if (ddlIndemExpenseAcc.SelectedValue.ToString().Trim().Length > 0)
                    {
                        sSM_SYSTEM_OPTIONS.HR_INDEM_EXPENSE_AC = ddlIndemExpenseAcc.SelectedValue;
                    }
                    else
                    {
                        sSM_SYSTEM_OPTIONS.HR_INDEM_EXPENSE_AC = null;
                    }

                    if (ddlLeaveSuspenseAcc.SelectedValue.ToString().Trim().Length > 0)
                    {
                        sSM_SYSTEM_OPTIONS.HR_LEAVE_SUSPENS_AC = ddlLeaveSuspenseAcc.SelectedValue;
                    }
                    else
                    {
                        sSM_SYSTEM_OPTIONS.HR_LEAVE_SUSPENS_AC = null;
                    }
                    
                }

                if (ddlModule.SelectedValue == "FA")
                {
                    if (ddlFAAssetCostAcc.SelectedValue.ToString().Trim().Length > 0)
                    {
                        sSM_SYSTEM_OPTIONS.FA_ASSET_CST_ACC = ddlFAAssetCostAcc.SelectedValue;
                    }
                    else
                    {
                        sSM_SYSTEM_OPTIONS.FA_ASSET_CST_ACC = null;
                    }

                    if (ddlFADepriciationAcc.SelectedValue.ToString().Trim().Length > 0)
                    {
                        sSM_SYSTEM_OPTIONS.FA_DEPRICIATION_ACC = ddlFADepriciationAcc.SelectedValue;
                    }
                    else
                    {
                        sSM_SYSTEM_OPTIONS.FA_DEPRICIATION_ACC = null;
                    }

                    if (ddlFAAccumDepreciaAcc.SelectedValue.ToString().Trim().Length > 0)
                    {
                        sSM_SYSTEM_OPTIONS.FA_ACCUM_DEPRICIATION_ACC = ddlFAAccumDepreciaAcc.SelectedValue;
                    }
                    else
                    {
                        sSM_SYSTEM_OPTIONS.FA_ACCUM_DEPRICIATION_ACC = null;
                    }

                    if (ddlFADepreciationRun.SelectedValue.ToString().Trim().Length > 0)
                    {
                        sSM_SYSTEM_OPTIONS.FA_DEPRICIATION_RUN = ddlFADepreciationRun.SelectedValue;
                    }
                    else
                    {
                        sSM_SYSTEM_OPTIONS.FA_DEPRICIATION_RUN = null;
                    }
                    
                   

                }

                sSM_SYSTEM_OPTIONS.ORG_ID = VMVServices.Web.Utils.OrganizationID;



                if (dtpk.Rows.Count > 0)
                {
                    //if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                    //{
                    sSM_SYSTEM_OPTIONS.SYS_OPT_ID = dtpk.Rows[0]["SYS_OPT_ID"].ToString();
                    sSM_SYSTEM_OPTIONS.MODIFIED_BY = this.LoggedUserName;
                    sSM_SYSTEM_OPTIONS.MODIFIED_DATE = DateTime.Today;
                    sSM_SYSTEM_OPTIONS.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, sSM_SYSTEM_OPTIONS.SYS_OPT_ID);
                    DBMethod.SaveEntity<SSM_SYSTEM_OPTIONS>(sSM_SYSTEM_OPTIONS, true);
                    savedBool = true;
                }
                else
                {
                    sSM_SYSTEM_OPTIONS.SYS_OPT_ID = FINSP.GetSPFOR_SEQCode("SSM_024".ToString(), false, true);

                    sSM_SYSTEM_OPTIONS.CREATED_BY = this.LoggedUserName;
                    sSM_SYSTEM_OPTIONS.CREATED_DATE = DateTime.Today;
                    sSM_SYSTEM_OPTIONS.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, sSM_SYSTEM_OPTIONS.SYS_OPT_ID);
                    DBMethod.SaveEntity<SSM_SYSTEM_OPTIONS>(sSM_SYSTEM_OPTIONS);
                    savedBool = true;

                }



            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                AssignToBE();

                // Duplicate Check

                //ProReturn = FINSP.GetSPFOR_ERR_MGR_ITEM_CATOG(txtCategoryName.Text, sSM_SYSTEM_OPTIONS.ITEM_CATEGORY_ID, txtEffectiveStartDate.Text, txtEffectiveEnddate.Text);

                //if (ProReturn != string.Empty)
                //{

                //    if (ProReturn != "0")
                //    {
                //        ErrorCollection.Add("ITEMCATEGORYNAME", ProReturn);
                //        if (ErrorCollection.Count > 0)
                //        {
                //            return;
                //        }
                //    }
                //}
                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);
                }

                //switch (Master.Mode)
                //{

                //    case FINAppConstants.Add:
                //        {
                //            DBMethod.SaveEntity<SSM_SYSTEM_OPTIONS>(sSM_SYSTEM_OPTIONS);
                //            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                //            break;
                //        }
                //    case FINAppConstants.Update:
                //        {

                //            DBMethod.SaveEntity<SSM_SYSTEM_OPTIONS>(sSM_SYSTEM_OPTIONS, true);
                //            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                //            break;

                //        }
                //}
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion




        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                DBMethod.DeleteEntity<SSM_SYSTEM_OPTIONS>(sSM_SYSTEM_OPTIONS);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void ddlModule_SelectedIndexChanged(object sender, EventArgs e)
        {
            fillModData();
        }

        private void fillModData()
        {
            // FillComboBox();
            try
            {
                DataTable dtpk = new DataTable();
                dtpk = DBMethod.ExecuteQuery(FIN.DAL.SSM.SystemOptions_DAL.getSysoptID()).Tables[0];

                if (dtpk.Rows.Count > 0)
                {
                    using (IRepository<SSM_SYSTEM_OPTIONS> userCtx = new DataRepository<SSM_SYSTEM_OPTIONS>())
                    {
                        sSM_SYSTEM_OPTIONS = userCtx.Find(r =>
                            (r.SYS_OPT_ID == dtpk.Rows[0]["SYS_OPT_ID"].ToString())
                            ).SingleOrDefault();
                    }

                }
                if (dtpk.Rows.Count > 0)
                {
                    if (ddlModule.SelectedValue == "AP")
                    {

                        FIN.BLL.GL.AccountCodes_BLL.fn_getAccount(ref ddlAPliabilityAcct);
                        FIN.BLL.GL.AccountCodes_BLL.fn_getAccount(ref ddlAPadvanceacct);
                        FIN.BLL.GL.AccountCodes_BLL.fn_getAccount(ref ddlAPretentionacct);
                        FIN.BLL.GL.AccountCodes_BLL.fn_getAccount(ref ddlAPdiscountacct);
                        FIN.BLL.GL.AccountCodes_BLL.fn_getAccount(ref ddlAPexchangegain);
                        FIN.BLL.GL.AccountCodes_BLL.fn_getAccount(ref ddlAPexchangeloss);
                        FIN.BLL.GL.AccountCodes_BLL.fn_getAccount(ref ddlAPDefaultCash);

                        divAR.Visible = false;
                        divHR.Visible = false;
                        divFA.Visible = false;
                        divAP.Visible = true;

                        if (sSM_SYSTEM_OPTIONS.AP_LIABILITY_ACCT != null)
                        {
                            ddlAPliabilityAcct.SelectedValue = sSM_SYSTEM_OPTIONS.AP_LIABILITY_ACCT;
                        }
                        if (sSM_SYSTEM_OPTIONS.AP_ADVANCE_ACCT != null)
                        {
                            ddlAPadvanceacct.SelectedValue = sSM_SYSTEM_OPTIONS.AP_ADVANCE_ACCT;
                        }
                        if (sSM_SYSTEM_OPTIONS.AP_RETENTION_ACCT != null)
                        {
                            ddlAPretentionacct.SelectedValue = sSM_SYSTEM_OPTIONS.AP_RETENTION_ACCT;
                        }
                        if (sSM_SYSTEM_OPTIONS.AP_DISCOUNT_ACCT != null)
                        {
                            ddlAPdiscountacct.SelectedValue = sSM_SYSTEM_OPTIONS.AP_DISCOUNT_ACCT;
                        }
                        if (sSM_SYSTEM_OPTIONS.AP_EXCHANGE_GAIN_ACCT != null)
                        {
                            ddlAPexchangegain.SelectedValue = sSM_SYSTEM_OPTIONS.AP_EXCHANGE_GAIN_ACCT;
                        }
                        if (sSM_SYSTEM_OPTIONS.AP_EXCHANGE_LOSS_ACCT != null)
                        {
                            ddlAPexchangeloss.SelectedValue = sSM_SYSTEM_OPTIONS.AP_EXCHANGE_LOSS_ACCT;
                        }
                        if (sSM_SYSTEM_OPTIONS.AP_PAYMENT_TERMS != null)
                        {
                            txtAPpaymentterms.Text = sSM_SYSTEM_OPTIONS.AP_PAYMENT_TERMS.ToString();
                        }

                        if (sSM_SYSTEM_OPTIONS.AP_DEFAULT_CASH_ACCT_CODE != null)
                        {
                            ddlAPDefaultCash.SelectedValue = sSM_SYSTEM_OPTIONS.AP_DEFAULT_CASH_ACCT_CODE;
                        }

                        if (sSM_SYSTEM_OPTIONS.AP_START_DATE != null)
                        {
                            txtAPstartdate.Text = DBMethod.ConvertDateToString(sSM_SYSTEM_OPTIONS.AP_START_DATE.ToString());
                        }
                        if (sSM_SYSTEM_OPTIONS.AP_END_DATE != null)
                        {
                            txtAPenddate.Text = DBMethod.ConvertDateToString(sSM_SYSTEM_OPTIONS.AP_END_DATE.ToString());
                        }

                        if (sSM_SYSTEM_OPTIONS.AP_ACTIVE == FINAppConstants.EnabledFlag)
                        {
                            chkActive.Checked = true;
                        }
                        else
                        {
                            chkActive.Checked = false;

                        }
                    }
                    else if (ddlModule.SelectedValue == "AR")
                    {
                        divAP.Visible = false;
                        divHR.Visible = false;
                        divFA.Visible = false;
                        divAR.Visible = true;

                        if (sSM_SYSTEM_OPTIONS.AR_DEBITOR_ACCT != null)
                        {
                            ddlARdebitoracct.SelectedValue = sSM_SYSTEM_OPTIONS.AR_DEBITOR_ACCT;
                        }

                        if (sSM_SYSTEM_OPTIONS.AR_REVENUE_ACCT != null)
                        {
                            ddlARrevenueacct.SelectedValue = sSM_SYSTEM_OPTIONS.AR_REVENUE_ACCT;
                        }

                        if (sSM_SYSTEM_OPTIONS.AR_DISCOUNT_ACCT != null)
                        {
                            ddlARdiscountacct.SelectedValue = sSM_SYSTEM_OPTIONS.AR_DISCOUNT_ACCT;
                        }

                        if (sSM_SYSTEM_OPTIONS.AR_EXCHANGE_GAIN_ACCT != null)
                        {
                            ddlARexchangegain.SelectedValue = sSM_SYSTEM_OPTIONS.AR_EXCHANGE_GAIN_ACCT;
                        }

                        if (sSM_SYSTEM_OPTIONS.AR_EXCHANGE_LOSS_ACCT != null)
                        {
                            ddlARexchangeloss.SelectedValue = sSM_SYSTEM_OPTIONS.AR_EXCHANGE_LOSS_ACCT;
                        }

                        if (sSM_SYSTEM_OPTIONS.AR_PAYMENT_TERMS != null)
                        {
                            txtARpaymentterms.Text = sSM_SYSTEM_OPTIONS.AR_PAYMENT_TERMS.ToString();
                        }



                        if (sSM_SYSTEM_OPTIONS.AR_DEFAULT_CASH_ACCT_CODE != null)
                        {
                            ddlARDefaultCash.SelectedValue = sSM_SYSTEM_OPTIONS.AR_DEFAULT_CASH_ACCT_CODE;
                        }
                        if (sSM_SYSTEM_OPTIONS.AR_CLEARANCE_ACC != null)
                        {
                            ddlARClearanceAcc.SelectedValue = sSM_SYSTEM_OPTIONS.AR_CLEARANCE_ACC;
                        }

                        if (sSM_SYSTEM_OPTIONS.AR_START_DATE != null)
                        {
                            txtARstartdate.Text = DBMethod.ConvertDateToString(sSM_SYSTEM_OPTIONS.AR_START_DATE.ToString());
                        }
                        if (sSM_SYSTEM_OPTIONS.AR_END_DATE != null)
                        {
                            txtARenddate.Text = DBMethod.ConvertDateToString(sSM_SYSTEM_OPTIONS.AR_END_DATE.ToString());
                        }


                        if (sSM_SYSTEM_OPTIONS.AR_ACTIVE == FINAppConstants.EnabledFlag)
                        {
                            ARchkact.Checked = true;
                        }
                        else
                        {
                            ARchkact.Checked = false;
                        }

                    }
                    else if (ddlModule.SelectedValue == "HR")
                    {
                        divAP.Visible = false;
                        divAR.Visible = false;
                        divFA.Visible = false;
                        divHR.Visible = true;



                        if (sSM_SYSTEM_OPTIONS.SSDP_EMPLOYEE != null)
                        {
                            txtssdpemplee.Text = sSM_SYSTEM_OPTIONS.SSDP_EMPLOYEE.ToString();
                        }
                        if (sSM_SYSTEM_OPTIONS.SSDP_EMPLOYER != null)
                        {
                            txtssdpemployer.Text = sSM_SYSTEM_OPTIONS.SSDP_EMPLOYER.ToString();
                        }
                        if (sSM_SYSTEM_OPTIONS.LOAN_ELIGIBLE_YEAR != null)
                        {
                            txtlneligibleyr.Text = sSM_SYSTEM_OPTIONS.LOAN_ELIGIBLE_YEAR.ToString();
                        }
                        if (sSM_SYSTEM_OPTIONS.LOAN_REELIGIBLE_YEAR != null)
                        {
                            txtlnreeligibleyr.Text = sSM_SYSTEM_OPTIONS.LOAN_REELIGIBLE_YEAR.ToString();
                        }
                        if (sSM_SYSTEM_OPTIONS.PERMISSION_HOURS != null)
                        {
                            txtHRpermissionhours.Text = sSM_SYSTEM_OPTIONS.PERMISSION_HOURS.ToString();
                        }
                        if (sSM_SYSTEM_OPTIONS.HR_PAY_ELEMENT_CODE != null)
                        {
                            ddlHRElement.SelectedValue = sSM_SYSTEM_OPTIONS.HR_PAY_ELEMENT_CODE.ToString();
                        }

                        if (sSM_SYSTEM_OPTIONS.HR_MEDICAL_DEDUCT_ELEMENT_CODE != null)
                        {
                            ddlHRMedical.SelectedValue = sSM_SYSTEM_OPTIONS.HR_MEDICAL_DEDUCT_ELEMENT_CODE;
                        }
                        if (sSM_SYSTEM_OPTIONS.HR_MOBI_DEDUCT_ELEMENT_CODE != null)
                        {
                            ddlHRMobile.SelectedValue = sSM_SYSTEM_OPTIONS.HR_MOBI_DEDUCT_ELEMENT_CODE;
                        }
                        if (sSM_SYSTEM_OPTIONS.HR_MOBI_INVOICE_ELEMENT_CODE != null)
                        {
                            ddlHRMobileInvoice.SelectedValue = sSM_SYSTEM_OPTIONS.HR_MOBI_INVOICE_ELEMENT_CODE;
                        }
                        if (sSM_SYSTEM_OPTIONS.HR_RENT_DEDUCT_ELEMENT_CODE != null)
                        {
                            ddlHRRent.SelectedValue = sSM_SYSTEM_OPTIONS.HR_RENT_DEDUCT_ELEMENT_CODE;
                        }

                        if (sSM_SYSTEM_OPTIONS.HR_BASIC_ELEMENT_CODE != null)
                        {
                            ddlBasicelementcode.SelectedValue = sSM_SYSTEM_OPTIONS.HR_BASIC_ELEMENT_CODE;
                        }
                        if (sSM_SYSTEM_OPTIONS.HR_MGRP_EMP_ELEMENT_CODE != null)
                        {
                            ddlMGRPEmpElement.SelectedValue = sSM_SYSTEM_OPTIONS.HR_MGRP_EMP_ELEMENT_CODE;
                        }
                        if (sSM_SYSTEM_OPTIONS.HR_MGRP_COMP_ELEMENT_CODE != null)
                        {
                            ddlMGRPCompElement.SelectedValue = sSM_SYSTEM_OPTIONS.HR_MGRP_COMP_ELEMENT_CODE;
                        }
                        if (sSM_SYSTEM_OPTIONS.HR_EARN_LEAVE != null)
                        {
                            ddlHREarnLeave.SelectedValue = sSM_SYSTEM_OPTIONS.HR_EARN_LEAVE;
                        }

                        if (sSM_SYSTEM_OPTIONS.HR_LEAVE_SAL != null)
                        {
                            ddlLeavesal.SelectedValue = sSM_SYSTEM_OPTIONS.HR_LEAVE_SAL;
                        }


                        if (sSM_SYSTEM_OPTIONS.HR_LEAVE_ENCASH != null)
                        {
                            ddlLeavencashment.SelectedValue = sSM_SYSTEM_OPTIONS.HR_LEAVE_ENCASH;
                        }

                        if (sSM_SYSTEM_OPTIONS.HR_EFFECTIVE_FROM_DATE != null)
                        {
                            txtHREfffromdate.Text = DBMethod.ConvertDateToString(sSM_SYSTEM_OPTIONS.HR_EFFECTIVE_FROM_DATE.ToString());
                        }
                        if (sSM_SYSTEM_OPTIONS.HR_EFFECTIVE_TO_DATE != null)
                        {
                            txtHREnddaate.Text = DBMethod.ConvertDateToString(sSM_SYSTEM_OPTIONS.HR_EFFECTIVE_TO_DATE.ToString());
                        }
                        if (sSM_SYSTEM_OPTIONS.LEAVE_ADJ_CODE != null)
                        {
                            ddlleaveadjustcode.SelectedValue = sSM_SYSTEM_OPTIONS.LEAVE_ADJ_CODE;
                        }
                        if (sSM_SYSTEM_OPTIONS.LEAVE_ADJ_EARN != null)
                        {
                            ddlleaveadjustEarnings.SelectedValue = sSM_SYSTEM_OPTIONS.LEAVE_ADJ_EARN;
                        }
                        if (sSM_SYSTEM_OPTIONS.HR_LOAN_PAYMENT != null)
                        {
                            ddlLoanPayment.SelectedValue = sSM_SYSTEM_OPTIONS.HR_LOAN_PAYMENT;
                        }
                        if (sSM_SYSTEM_OPTIONS.HR_LEAVE_INDM_SUSP_ACC != null)
                        {
                            ddlleaveindmsuspeacc.SelectedValue = sSM_SYSTEM_OPTIONS.HR_LEAVE_INDM_SUSP_ACC;
                        }

                        if (sSM_SYSTEM_OPTIONS.HR_OTHER_SUSP_ACC != null)
                        {
                            ddlothersuspenseacc.SelectedValue = sSM_SYSTEM_OPTIONS.HR_OTHER_SUSP_ACC;
                        }

                        if (sSM_SYSTEM_OPTIONS.HR_LEAVE_ADJUSTMENT_MONTH != null)
                        {
                            txtLeavePlan.Text = sSM_SYSTEM_OPTIONS.HR_LEAVE_ADJUSTMENT_MONTH.ToString();
                        }



                        if (sSM_SYSTEM_OPTIONS.HR_LEAVE_SUSPENS_AC != null)
                        {
                            ddlLeaveSuspenseAcc.SelectedValue = sSM_SYSTEM_OPTIONS.HR_LEAVE_SUSPENS_AC;
                        }
                        if (sSM_SYSTEM_OPTIONS.HR_INDEM_EXPENSE_AC != null)
                        {
                            ddlIndemExpenseAcc.SelectedValue = sSM_SYSTEM_OPTIONS.HR_INDEM_EXPENSE_AC;
                        }

                    }
                    else if (ddlModule.SelectedValue == "FA")
                    {
                        divAP.Visible = false;
                        divHR.Visible = false;
                        divAR.Visible = false;
                        divFA.Visible = true;

                        if (sSM_SYSTEM_OPTIONS.FA_ASSET_CST_ACC != null)
                        {
                            ddlFAAssetCostAcc.SelectedValue = sSM_SYSTEM_OPTIONS.FA_ASSET_CST_ACC;
                        }

                        if (sSM_SYSTEM_OPTIONS.FA_DEPRICIATION_ACC != null)
                        {
                            ddlFADepriciationAcc.SelectedValue = sSM_SYSTEM_OPTIONS.FA_DEPRICIATION_ACC;
                        }

                        if (sSM_SYSTEM_OPTIONS.FA_ACCUM_DEPRICIATION_ACC != null)
                        {
                            ddlFAAccumDepreciaAcc.SelectedValue = sSM_SYSTEM_OPTIONS.FA_ACCUM_DEPRICIATION_ACC;
                        }

                        if (sSM_SYSTEM_OPTIONS.FA_DEPRICIATION_RUN != null)
                        {
                            ddlFADepreciationRun.SelectedValue = sSM_SYSTEM_OPTIONS.FA_DEPRICIATION_RUN;
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }



        }


    }
}
