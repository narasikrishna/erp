﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.DAL.SSM;
using FIN.BLL;
using FIN.BLL.SSM;
using VMVServices.Web;

namespace FIN.Client.SSM
{
    public partial class MasterReportEntry : PageBase
    {
        SSM_REP_LIST_HDR sSM_REP_LIST_HDR = new SSM_REP_LIST_HDR();
        SSM_REP_LIST_DTL sSM_REP_LIST_DTL = new SSM_REP_LIST_DTL();
        DataTable dtGridData = new DataTable();
        Boolean bol_rowVisiable;
        Boolean savedBool;
        string ProReturn = null;
        string ProReturn_Valdn = null;
        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("SSM_MR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
            //if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            //{
            //    VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
            //}

            //if (VMVServices.Web.Utils.AddAllowed != FINAppConstants.Y && Master.Mode == FINAppConstants.Add)
            //{
            //    btnSave.Visible = false;
            //}

        }

        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }


        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();

                FillComboBox();
                EntityData = null;

             

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    using (IRepository<SSM_REP_LIST_HDR> userCtx = new DataRepository<SSM_REP_LIST_HDR>())
                    {
                        sSM_REP_LIST_HDR = userCtx.Find(r =>
                            (r.LIST_HDR_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = sSM_REP_LIST_HDR;

                    ddlscreencode.SelectedValue = sSM_REP_LIST_HDR.LIST_SCREEN_CODE;
                    fillscreenname();
                    txtscreenname.Text = sSM_REP_LIST_HDR.LIST_SCREEN_NAME;
                    ddltablename.SelectedValue = sSM_REP_LIST_HDR.LIST_TABLE_NAME;
                    txtReportName.Text = sSM_REP_LIST_HDR.ATTRIBUTE1;
                    FillScreenColName();
                    ddlOBColName.SelectedValue = sSM_REP_LIST_HDR.ATTRIBUTE3;
                    if (sSM_REP_LIST_HDR.ENABLED_FLAG == "1")
                    {
                        chkActive.Checked = true;
                    }
                    else
                    {
                        chkActive.Checked = false;
                    }

                    dtGridData = DBMethod.ExecuteQuery(FIN.DAL.SSM.MasterReport_DAL.getMasterrepdtl(Master.StrRecordId,ddltablename.SelectedValue)).Tables[0];
                    BindGrid(dtGridData);
                    ddlscreencode.Enabled = false;
                }

              

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("MR_ATC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>

        private void FillComboBox()
        {

            FIN.BLL.SSM.TableColumnStruct_BLL.GetScreenNameinTSD(ref ddlscreencode);          

        }

        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    sSM_REP_LIST_HDR = (SSM_REP_LIST_HDR)EntityData;
                }

                sSM_REP_LIST_HDR.LIST_SCREEN_CODE = ddlscreencode.SelectedValue;
                sSM_REP_LIST_HDR.LIST_SCREEN_NAME = txtscreenname.Text;
                sSM_REP_LIST_HDR.LIST_TABLE_NAME = ddltablename.SelectedValue;
                
                sSM_REP_LIST_HDR.ATTRIBUTE1 = txtReportName.Text;
                sSM_REP_LIST_HDR.ATTRIBUTE2 = ddlOBColName.SelectedItem.Text;
                sSM_REP_LIST_HDR.ATTRIBUTE3 = ddlOBColName.SelectedValue;
                //  gL_SEGMENTS.WORKFLOW_COMPLETION_STATUS = "1";


                if (chkActive.Checked == true)
                {
                    sSM_REP_LIST_HDR.ENABLED_FLAG = FINAppConstants.Y;
                }
                else
                {
                    sSM_REP_LIST_HDR.ENABLED_FLAG = FINAppConstants.N;
                }

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    sSM_REP_LIST_HDR.MODIFIED_BY = this.LoggedUserName;
                    sSM_REP_LIST_HDR.MODIFIED_DATE = DateTime.Today;

                }
                else
                {

                    sSM_REP_LIST_HDR.LIST_HDR_ID = FINSP.GetSPFOR_SEQCode("SSM_022_M".ToString(), false, true);
                    sSM_REP_LIST_HDR.CREATED_BY = this.LoggedUserName;
                    sSM_REP_LIST_HDR.CREATED_DATE = DateTime.Today;


                }
                sSM_REP_LIST_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, sSM_REP_LIST_HDR.LIST_HDR_ID);
                // SAVE DETAILS

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                var tmpChildEntity = new List<Tuple<object, string>>();

                for (int iLoop = 0; iLoop < gvData.Rows.Count; iLoop++)
                {

                    CheckBox chkSelect = (CheckBox)gvData.Rows[iLoop].FindControl("chkSelect");

                    sSM_REP_LIST_DTL = new SSM_REP_LIST_DTL();
                    if (chkSelect.Checked == true)
                    {

                        if (gvData.DataKeys[iLoop].Values["LIST_DTL_ID"].ToString() != "0")
                        {

                            using (IRepository<SSM_REP_LIST_DTL> userCtx = new DataRepository<SSM_REP_LIST_DTL>())
                            {
                                sSM_REP_LIST_DTL = userCtx.Find(r =>
                                    (r.LIST_DTL_ID == gvData.DataKeys[iLoop].Values["LIST_DTL_ID"].ToString())
                                    ).SingleOrDefault();
                            }
                        }

                        sSM_REP_LIST_DTL.DISPLAY_COLUMN_NM = gvData.DataKeys[iLoop].Values["DISPLAY_NAME"].ToString();
                        sSM_REP_LIST_DTL.COLUMN_WIDTH = short.Parse(gvData.DataKeys[iLoop].Values["WIDTH"].ToString());
                        sSM_REP_LIST_DTL.COLUMN_DISPLAY_FORMAT = gvData.DataKeys[iLoop].Values["COL_TYPE"].ToString();
                        sSM_REP_LIST_DTL.MASTER_TABLE = gvData.DataKeys[iLoop].Values["MASTER_TABLE_NAME"].ToString();
                        sSM_REP_LIST_DTL.HDR_DTL_LINK = gvData.DataKeys[iLoop].Values["HDR_DTL_LINK"].ToString();


                        if (gvData.DataKeys[iLoop].Values["FLAG_MASTER"].ToString() == "1")
                        {
                            sSM_REP_LIST_DTL.LIST_COLUMN_NAME = gvData.DataKeys[iLoop].Values["MASTER_DISP_COLUMN_NAME"].ToString();
                        }
                        else
                        {
                            sSM_REP_LIST_DTL.LIST_COLUMN_NAME = gvData.DataKeys[iLoop].Values["COLUMN_NAME"].ToString();
                        }

                        sSM_REP_LIST_DTL.LIST_DISPLAY = FINAppConstants.Y;


                        sSM_REP_LIST_DTL.WORKFLOW_COMPLETION_STATUS = "1";
                        sSM_REP_LIST_DTL.COLUMN_ALIGNMENT = short.Parse("1".ToString());
                        sSM_REP_LIST_DTL.ENABLED_FLAG = "1";
                        sSM_REP_LIST_DTL.LIST_HDR_ID = sSM_REP_LIST_HDR.LIST_HDR_ID;

                        if (gvData.DataKeys[iLoop].Values["DELETED"].ToString() == FINAppConstants.Y)
                        {
                            tmpChildEntity.Add(new Tuple<object, string>(sSM_REP_LIST_DTL, FINAppConstants.Delete));
                        }
                        else
                        {
                            {
                                if (gvData.DataKeys[iLoop].Values["LIST_DTL_ID"].ToString() != "0" && gvData.DataKeys[iLoop].Values["LIST_DTL_ID"].ToString() != string.Empty)
                                {
                                    sSM_REP_LIST_DTL.LIST_DTL_ID = gvData.DataKeys[iLoop].Values["LIST_DTL_ID"].ToString();
                                    sSM_REP_LIST_DTL.MODIFIED_BY = this.LoggedUserName;
                                    sSM_REP_LIST_DTL.MODIFIED_DATE = DateTime.Today;
                                    tmpChildEntity.Add(new Tuple<object, string>(sSM_REP_LIST_DTL, FINAppConstants.Update));
                                }
                                else
                                {
                                    sSM_REP_LIST_DTL.LIST_DTL_ID = FINSP.GetSPFOR_SEQCode("SSM_022_D".ToString(), false, true);
                                    sSM_REP_LIST_DTL.CREATED_BY = this.LoggedUserName;
                                    sSM_REP_LIST_DTL.CREATED_DATE = DateTime.Today;
                                    tmpChildEntity.Add(new Tuple<object, string>(sSM_REP_LIST_DTL, FINAppConstants.Add));
                                }
                            }

                        }
                    }
                    else
                    {
                        if (gvData.DataKeys[iLoop].Values["LIST_DTL_ID"].ToString() != "0" && gvData.DataKeys[iLoop].Values["LIST_DTL_ID"].ToString() != string.Empty)
                        {
                            using (IRepository<SSM_REP_LIST_DTL> userCtx = new DataRepository<SSM_REP_LIST_DTL>())
                            {
                                sSM_REP_LIST_DTL = userCtx.Find(r =>
                                    (r.LIST_DTL_ID == gvData.DataKeys[iLoop].Values["LIST_DTL_ID"].ToString())
                                    ).SingleOrDefault();
                            }
                            DBMethod.DeleteEntity<SSM_REP_LIST_DTL>(sSM_REP_LIST_DTL);
                        }
                    }
                }
                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {


                            CommonUtils.SavePCEntity<SSM_REP_LIST_HDR, SSM_REP_LIST_DTL>(sSM_REP_LIST_HDR, tmpChildEntity, sSM_REP_LIST_DTL);
                            savedBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {


                            CommonUtils.SavePCEntity<SSM_REP_LIST_HDR, SSM_REP_LIST_DTL>(sSM_REP_LIST_HDR, tmpChildEntity, sSM_REP_LIST_DTL, true);
                            savedBool = true;
                            break;
                        }
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("MR_ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        private void BindGrid(DataTable dtData)
        {
            bol_rowVisiable = false;
            Session[FINSessionConstants.GridData] = dtData;
            DataTable dt_tmp = dtData.Copy();
            if (dt_tmp.Rows.Count == 0)
            {
                DataRow dr = dt_tmp.NewRow();
                dr[0] = "0";
                dr["LIST_DISPLAY"] = "FALSE";
                dt_tmp.Rows.Add(dr);
                bol_rowVisiable = true;
            }
            gvData.DataSource = dt_tmp;
            gvData.DataBind();
            //GridViewRow gvr = gvData.FooterRow;
            //FillFooterGridCombo(gvr);
        }







        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>


        #endregion




        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATADELETED);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Seg_btndel", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }



        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                AssignToBE();


                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);
                }


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void ddlscreencode_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                fillscreenname();
                FillScreenColName();
                //ClearGrid();
                //DUPLCHK();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("MR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void FillScreenColName()
        {
            FIN.BLL.SSM.TableColumnStruct_BLL.getColName4ScreenCode(ref ddlOBColName, ddlscreencode.SelectedValue);
        }
        private void DUPLCHK()
        {
            try
            {
                Dictionary<string, string> Prop_File_Data;
                Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/SSM_" + Session["Sel_Lng"].ToString() + ".properties"));
                DataTable dtscreencode = new DataTable();
                dtscreencode = DBMethod.ExecuteQuery(FIN.DAL.SSM.MasterReport_DAL.getScreenCde(ddlscreencode.SelectedValue)).Tables[0];


                if (dtscreencode.Rows.Count > 0)
                {
                    ddlscreencode.SelectedIndex = 0;

                    gvData.Visible = false;
                    ErrorCollection.Add("RecordAlreadyExists", Prop_File_Data["RecordAlreadyExists_P"]);

                }
                else
                {
                    gvData.Visible = true;

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("MR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        private void fillscreenname()
        {
            DataTable dtscreennm = new DataTable();
            dtscreennm = DBMethod.ExecuteQuery(FIN.DAL.SSM.MasterReport_DAL.getScreenName(ddlscreencode.SelectedValue)).Tables[0];
            txtscreenname.Text = dtscreennm.Rows[0]["SCREEN_NAME"].ToString();
            FIN.BLL.SSM.TableColumnStruct_BLL.GetTableNM_BasedScreencode(ref ddltablename, ddlscreencode.SelectedValue);
            DataTable dt_screenDet = DBMethod.ExecuteQuery(FIN.DAL.SSM.TableColumnStruct_DAL.GetTabStructdtl_Screen(ddlscreencode.SelectedValue)).Tables[0];
            if (dt_screenDet.Rows.Count > 0)
            {
                ddltablename.SelectedValue = dt_screenDet.Rows[0]["Table_name"].ToString();
                hf_OrgId.Value = dt_screenDet.Rows[0]["org_id_column_name"].ToString();
                ClearGrid();
                dtGridData = DBMethod.ExecuteQuery(FIN.DAL.SSM.MasterReport_DAL.getDisplaycolumn(ddltablename.SelectedValue)).Tables[0];
                BindGrid(dtGridData);
            }
        }

        protected void ddltablename_SelectedIndexChanged(object sender, EventArgs e)
        {
            dtGridData = DBMethod.ExecuteQuery(FIN.DAL.SSM.MasterReport_DAL.getDisplaycolumn(ddltablename.SelectedValue)).Tables[0];
            BindGrid(dtGridData);


        }

        private void ClearGrid()
        {
            if (Session["dtGridData"] != null)
            {
                dtGridData = (DataTable)Session["dtGridData"];
            }
            dtGridData.Rows.Clear();
            gvData.DataSource = dtGridData;
            gvData.DataBind();
        }



    }
}