﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.BLL;
using FIN.BLL.SSM;
using VMVServices.Web;

namespace FIN.Client.SSM
{
    public partial class AlertsEntry : PageBase
    {
        SSM_ALERT_MASTERS sSM_ALERT_MASTERS = new SSM_ALERT_MASTERS();



        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.RecordID = int.Parse(Server.HtmlEncode(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()])));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
            }

            if (VMVServices.Web.Utils.AddAllowed != FINAppConstants.Y && Master.Mode == FINAppConstants.Add)
            {
                btnSave.Visible = false;
            }

        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();

                EntityData = null;

                if (Master.Mode != FINAppConstants.Add && Master.RecordID > 0)
                {

                    using (IRepository<SSM_ALERT_MASTERS> userCtx = new DataRepository<SSM_ALERT_MASTERS>())
                    {
                        sSM_ALERT_MASTERS = userCtx.Find(r =>
                            (r.PK_ID == Master.RecordID)
                            ).SingleOrDefault();
                    }

                    EntityData = sSM_ALERT_MASTERS;

                    txtAlertcode.Text = sSM_ALERT_MASTERS.ALERT_CODE;
                    txtAlertDescription.Text = sSM_ALERT_MASTERS.ALERT_DESC;
                    txtAlertmsg.Text = sSM_ALERT_MASTERS.ALERT_MESSAGE;
                    ddlAlertType.SelectedValue = sSM_ALERT_MASTERS.ALERT_TYPE;
                    ddlLanguageCode.SelectedValue = sSM_ALERT_MASTERS.LANGUAGE_CODE;
                    if (sSM_ALERT_MASTERS.SCREEN_CODE != null)
                    {
                        ddlScreenCode.SelectedValue = sSM_ALERT_MASTERS.SCREEN_CODE;
                    }
                    txtAlertlevelcode.Text = sSM_ALERT_MASTERS.ALERT_LEVEL;
                    if (sSM_ALERT_MASTERS.ROLE_CODE != null)
                    {
                        ddlrole.SelectedValue = sSM_ALERT_MASTERS.ROLE_CODE;
                    }
                    txtformopenmode.Text = sSM_ALERT_MASTERS.FORM_OPEN_MODE;
                    txtQuery.Text = sSM_ALERT_MASTERS.QUERY_DISPLAY;
                    txtProcessCode.Text = sSM_ALERT_MASTERS.PROCESS_CODE;
                    txtURL.Text = sSM_ALERT_MASTERS.URL;
                    txtAlerttableName.Text = sSM_ALERT_MASTERS.ALERT_TABLE_NAME;
                    txtAlertcolumname.Text = sSM_ALERT_MASTERS.ALERT_COLUMN_NAME;
                    txtPreviousFieldName.Text = sSM_ALERT_MASTERS.PREVIOUS_FIELD_NAME;
                    txtfreqofdisplay.Text = sSM_ALERT_MASTERS.FREQUENCY_OF_DISPLAY.ToString();
                    txtfromformcode.Text = sSM_ALERT_MASTERS.FROM_FORM_CODE;

                    if (sSM_ALERT_MASTERS.SOUND_ENABLED == FINAppConstants.EnabledFlag)
                    {
                        chkSoundEnabled.Checked = true;
                    }
                    else
                    {
                        chkSoundEnabled.Checked = false;
                    }
                    if (sSM_ALERT_MASTERS.DELIVER_BY_EMAIL == FINAppConstants.EnabledFlag)
                    {
                        chkDelbyemail.Checked = true;
                    }
                    else
                    {
                        chkDelbyemail.Checked = false;
                    }
                    if (sSM_ALERT_MASTERS.DELIVER_ON_DASHBOARD == FINAppConstants.EnabledFlag)
                    {
                        chkDeliveryondashboard.Checked = true;
                    }
                    else
                    {
                        chkDeliveryondashboard.Checked = false;
                    }
                    if (sSM_ALERT_MASTERS.DELIVER_AS_MESSAGE_BOX == FINAppConstants.EnabledFlag)
                    {
                        chkdeliveryasmessagebox.Checked = true;
                    }
                    else
                    {
                        chkdeliveryasmessagebox.Checked = false;
                    }

                    if (sSM_ALERT_MASTERS.ATTRIBUTE1 != null)
                    {
                        if (sSM_ALERT_MASTERS.ATTRIBUTE1 == FINAppConstants.EnabledFlag)
                        {
                            chkSchedule.Checked = true;
                        }
                        else
                        {
                            chkSchedule.Checked = false;
                        }
                    }

                    if (sSM_ALERT_MASTERS.ATTRIBUTE2 != null)
                    {
                        ddlFreqSchedule.SelectedValue = sSM_ALERT_MASTERS.ATTRIBUTE2;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>

        private void FillComboBox()
        {
            Language_BLL.GetLanguageDetails(ref ddlLanguageCode);
            Screens_BLL.GetScreenName(ref ddlScreenCode);
            Role_BLL.GetRole(ref ddlrole);
            FIN.BLL.Lookup_BLL.GetLookUpValues(ref ddlAlertType, "ARTT");
            FIN.BLL.Lookup_BLL.GetLookUpValues(ref ddlFreqSchedule, "Schedule Frequency");

        }


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    sSM_ALERT_MASTERS = (SSM_ALERT_MASTERS)EntityData;
                }



                sSM_ALERT_MASTERS.ALERT_CODE = txtAlertcode.Text;
                sSM_ALERT_MASTERS.ALERT_DESC = txtAlertDescription.Text;
                sSM_ALERT_MASTERS.ALERT_MESSAGE = txtAlertmsg.Text;
                sSM_ALERT_MASTERS.ALERT_TYPE = ddlAlertType.SelectedValue.ToString();
                sSM_ALERT_MASTERS.LANGUAGE_CODE = ddlLanguageCode.SelectedValue;
                if (ddlScreenCode.SelectedValue != string.Empty)
                {
                    sSM_ALERT_MASTERS.SCREEN_CODE = ddlScreenCode.SelectedValue;
                }
                sSM_ALERT_MASTERS.ALERT_LEVEL = txtAlertlevelcode.Text;
                if (ddlrole.SelectedValue != string.Empty)
                {
                    sSM_ALERT_MASTERS.ROLE_CODE = ddlrole.SelectedValue;
                }
                sSM_ALERT_MASTERS.FORM_OPEN_MODE = txtformopenmode.Text;
                sSM_ALERT_MASTERS.QUERY_DISPLAY = txtQuery.Text;
                sSM_ALERT_MASTERS.PROCESS_CODE = txtProcessCode.Text;
                sSM_ALERT_MASTERS.URL = txtURL.Text;
                sSM_ALERT_MASTERS.ALERT_TABLE_NAME = txtAlerttableName.Text;
                sSM_ALERT_MASTERS.ALERT_COLUMN_NAME = txtAlertcolumname.Text;
                sSM_ALERT_MASTERS.PREVIOUS_FIELD_NAME = txtPreviousFieldName.Text;
                sSM_ALERT_MASTERS.FROM_FORM_CODE = txtfromformcode.Text;
                if (txtfreqofdisplay.Text.Length > 0)
                {
                    sSM_ALERT_MASTERS.FREQUENCY_OF_DISPLAY = short.Parse(txtfreqofdisplay.Text);
                }

                sSM_ALERT_MASTERS.SOUND_ENABLED = chkSoundEnabled.Checked == true ? FINAppConstants.EnabledFlag : FINAppConstants.DisabledFlag;
                sSM_ALERT_MASTERS.DELIVER_BY_EMAIL = chkDelbyemail.Checked == true ? FINAppConstants.EnabledFlag : FINAppConstants.DisabledFlag;
                sSM_ALERT_MASTERS.DELIVER_ON_DASHBOARD = chkDeliveryondashboard.Checked == true ? FINAppConstants.EnabledFlag : FINAppConstants.DisabledFlag;
                sSM_ALERT_MASTERS.DELIVER_AS_MESSAGE_BOX = chkdeliveryasmessagebox.Checked == true ? FINAppConstants.EnabledFlag : FINAppConstants.DisabledFlag;

                sSM_ALERT_MASTERS.ENABLED_FLAG = FINAppConstants.EnabledFlag;

                sSM_ALERT_MASTERS.ATTRIBUTE1 = chkSchedule.Checked == true ? FINAppConstants.EnabledFlag : FINAppConstants.DisabledFlag; //schedule required or not
                sSM_ALERT_MASTERS.ATTRIBUTE2 = ddlFreqSchedule.SelectedValue;// freqency of schedule is running




                if (Master.Mode != FINAppConstants.Add && Master.RecordID > 0)
                {
                    sSM_ALERT_MASTERS.MODIFIED_BY = this.LoggedUserName;
                    sSM_ALERT_MASTERS.MODIFIED_DATE = DateTime.Today;

                }
                else
                {
                    sSM_ALERT_MASTERS.PK_ID = DBMethod.GetPrimaryKeyValue(FINSequenceConstant.SSM_ALERT_MASTERS_SEQ);
                    sSM_ALERT_MASTERS.ALERT_CODE = FINSP.GetSPFOR_SEQCode(FINAppConstants.SSM_020.ToString(), false, true);
                    sSM_ALERT_MASTERS.CREATED_BY = this.LoggedUserName;
                    sSM_ALERT_MASTERS.CREATED_DATE = DateTime.Today;

                }

                sSM_ALERT_MASTERS.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, sSM_ALERT_MASTERS.PK_ID.ToString());

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                AssignToBE();

                // Duplicate Check

                //ProReturn = FINSP.GetSPFOR_ERR_MGR_ITEM_CATOG(txtCategoryName.Text, sSM_ALERT_MASTERS.ITEM_CATEGORY_ID, txtEffectiveStartDate.Text, txtEffectiveEnddate.Text);

                //if (ProReturn != string.Empty)
                //{

                //    if (ProReturn != "0")
                //    {
                //        ErrorCollection.Add("ITEMCATEGORYNAME", ProReturn);
                //        if (ErrorCollection.Count > 0)
                //        {
                //            return;
                //        }
                //    }
                //}


                switch (Master.Mode)
                {

                    case FINAppConstants.Add:
                        {
                            DBMethod.SaveEntity<SSM_ALERT_MASTERS>(sSM_ALERT_MASTERS);
                            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                            break;
                        }
                    case FINAppConstants.Update:
                        {

                            DBMethod.SaveEntity<SSM_ALERT_MASTERS>(sSM_ALERT_MASTERS, true);
                            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                            break;

                        }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion




        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                DBMethod.DeleteEntity<SSM_ALERT_MASTERS>(sSM_ALERT_MASTERS);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void txtEffectiveEnddate_TextChanged(object sender, EventArgs e)
        {

        }

        protected void ddlCategoryParent_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void chkSchedule_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (chkSchedule.Checked == true)
                {
                    ddlFreqSchedule.Enabled = true;
                }
                else
                {
                    ddlFreqSchedule.Enabled = false;
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


    }

}
