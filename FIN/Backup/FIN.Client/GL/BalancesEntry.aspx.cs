﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.DAL.GL;
using FIN.BLL;
using FIN.BLL.GL;
using VMVServices.Web;

namespace FIN.Client.GL
{
    public partial class BalancesEntry : PageBase
    {


        DataTable dtGridData = new DataTable();
        DataTable dtAccBaldtl = new DataTable();
        DataTable dtAccBal_currwise = new DataTable();
        DataTable dtAccBal_Daywise = new DataTable();
        string ProReturn = null;
        Boolean bol_rowVisiable;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BPL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();

        }


        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        /// <summary>
        /// To assign the values to Controls and fetch the StrRecordId,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();


                Startup();

                FillComboBox();

                EntityData = null;

                dtGridData = DBMethod.ExecuteQuery(Balances_DAL.getBeginBalDtl(ddlperiod.SelectedValue, ddlsegment.SelectedValue, ddlCompany.SelectedValue, ddlAcctcode.SelectedValue)).Tables[0];
                BindGrid(dtGridData);


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        private void FillComboBox()
        {
            Organisation_BLL.getCompanyNM(ref ddlCompany);
            // Balances_BLL.fn_getPeriod(ref ddlperiod);
            //    Balances_BLL.fn_getSegment(ref ddlsegment);

            ddlCompany.SelectedValue = VMVServices.Web.Utils.OrganizationID;
            ddlCompany.Enabled = false;

            if (ddlCompany.SelectedValue != string.Empty)
            {
                Segments_BLL.GetGlobalSegmentvalues(ref ddlsegment, ddlCompany.SelectedValue.ToString());
            }
            AccountingCalendar_BLL.GetAccPeriodBasedOrg(ref ddlperiod);
            AccountCodes_BLL.getAccCodes(ref ddlAcctcode);

        }


        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>


        #endregion

        private void BindGrid(DataTable dtData)
        {

            if (dtData.Rows.Count > 0)
            {
                dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("Begin_Balance_Acct_Cr", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("Begin_Balance_Acct_Cr"))));
                dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("Begin_Balance_Acct_Dr", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("Begin_Balance_Acct_Dr"))));
                dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("Accounted_Cr", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("Accounted_Cr"))));
                dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("Accounted_Dr", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("Accounted_Dr"))));
                dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("Closing_Balance_Acct_Cr", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("Closing_Balance_Acct_Cr"))));
                dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("Closing_Balance_Acct_Dr", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("Closing_Balance_Acct_Dr"))));
                dtData.AcceptChanges();
            }


            bol_rowVisiable = false;
            Session[FINSessionConstants.GridData] = dtData;
            DataTable dt_tmp = dtData.Copy();
            if (dt_tmp.Rows.Count == 0)
            {
                DataRow dr = dt_tmp.NewRow();
                dr[0] = "0";

                dt_tmp.Rows.Add(dr);
                bol_rowVisiable = true;
            }
            gvData.DataSource = dt_tmp;
            gvData.DataBind();


        }

        private void BindGrid_AcctBeginBal(DataTable dtAcctbbData)
        {

            if (dtAcctbbData.Rows.Count > 0)
            {
                dtAcctbbData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("Begin_Balance_Cr", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("Begin_Balance_Cr"))));
                dtAcctbbData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("Begin_Balance_Dr", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("Begin_Balance_Dr"))));
                dtAcctbbData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("Amount_Cr", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("Amount_Cr"))));
                dtAcctbbData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("Amount_Dr", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("Amount_Dr"))));
                dtAcctbbData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("Closing_Balance_Cr", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("Closing_Balance_Cr"))));
                dtAcctbbData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("Closing_Balance_Dr", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("Closing_Balance_Dr"))));
                dtAcctbbData.AcceptChanges();
            }
            bol_rowVisiable = false;
            Session["AcctBegnBal"] = dtAcctbbData;
            //DataTable dt_tmp1 = dtperiodData.Copy();
            //if (dt_tmp1.Rows.Count == 0)
            //{
            //    DataRow dr = dt_tmp1.NewRow();
            //    dr[0] = "0";

            //    dt_tmp1.Rows.Add(dr);
            //    bol_rowVisiable = true;
            //}
            gvABB.DataSource = dtAcctbbData;
            gvABB.DataBind();


        }

        private void BindGrid_AcctBal_Currwise(DataTable dtAcctbcurrwiseData)
        {

            if (dtAcctbcurrwiseData.Rows.Count > 0)
            {
                dtAcctbcurrwiseData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("amount_cr", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("amount_cr"))));
                dtAcctbcurrwiseData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("amount_dr", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("amount_dr"))));
                dtAcctbcurrwiseData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("accounted_cr", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("accounted_cr"))));
                dtAcctbcurrwiseData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("accounted_dr", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("accounted_dr"))));
                dtAcctbcurrwiseData.AcceptChanges();
            }
            bol_rowVisiable = false;
            Session["AcctBal_Currwise"] = dtAcctbcurrwiseData;
            //DataTable dt_tmp1 = dtperiodData.Copy();
            //if (dt_tmp1.Rows.Count == 0)
            //{
            //    DataRow dr = dt_tmp1.NewRow();
            //    dr[0] = "0";

            //    dt_tmp1.Rows.Add(dr);
            //    bol_rowVisiable = true;
            //}
            gvAB_CW.DataSource = dtAcctbcurrwiseData;
            gvAB_CW.DataBind();


        }

        private void BindGrid_AcctBal_Daywise(DataTable dtAcctbDaywiseData)
        {

            if (dtAcctbDaywiseData.Rows.Count > 0)
            {
                dtAcctbDaywiseData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("je_amount_cr", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("je_amount_cr"))));
                dtAcctbDaywiseData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("je_amount_dr", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("je_amount_dr"))));
                dtAcctbDaywiseData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("je_accounted_amt_cr", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("je_accounted_amt_cr"))));
                dtAcctbDaywiseData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("je_accounted_amt_dr", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("je_accounted_amt_dr"))));
                dtAcctbDaywiseData.AcceptChanges();
            }
            bol_rowVisiable = false;
            Session["AcctBal_Daywise"] = dtAcctbDaywiseData;
            //DataTable dt_tmp1 = dtperiodData.Copy();
            //if (dt_tmp1.Rows.Count == 0)
            //{
            //    DataRow dr = dt_tmp1.NewRow();
            //    dr[0] = "0";

            //    dt_tmp1.Rows.Add(dr);
            //    bol_rowVisiable = true;
            //}
            gvAB_DW.DataSource = dtAcctbDaywiseData;
            gvAB_DW.DataBind();


        }

        //protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        //{
        //    if (e.Row.RowType == DataControlRowType.DataRow)
        //    {
        //        if (bol_rowVisiable)
        //            e.Row.Visible = false;

        //        if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
        //        {
        //            e.Row.Visible = false;
        //        }

        //    }
        //}










        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();


                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("GradeEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void btnSave_Click1(object sender, EventArgs e)
        {

            try
            {

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ACT_CAL_btn_sve", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }



        protected void ddlsegment_SelectedIndexChanged(object sender, EventArgs e)
        {
            //  FIN.BLL.GL.AccountCodes_BLL.fn_getAccCodeBasedSegment_frall(ref ddlAcctcode, ddlsegment.SelectedValue);
        }
        protected void btnCWBack_Click(object sender, EventArgs e)
        {
            ModalPopupExtender4.Show();
        }
        protected void btnDaywiseBack_Click(object sender, EventArgs e)
        {
            MPCurrencywise.Show();
        }
        protected void btnJVviewBack_Click(object sender, EventArgs e)
        {
            MPDaywise.Show();
        }
        
        protected void btnDetails_Click(object sender, EventArgs e)
        {
            ModalPopupExtender4.Show();
            GridViewRow gvrP = (GridViewRow)((Control)sender).Parent.Parent;
            if (chkUnposted.Checked)
            {
                dtAccBaldtl = DBMethod.ExecuteQuery(FIN.DAL.GL.Balances_DAL.getAcctBeginBal_UP(gvData.DataKeys[gvrP.RowIndex].Values["ACCT_CODE"].ToString(), ddlCompany.SelectedValue, ddlperiod.SelectedItem.Text, ddlsegment.SelectedItem.Text)).Tables[0];
            }
            else
            {
                dtAccBaldtl = DBMethod.ExecuteQuery(FIN.DAL.GL.Balances_DAL.getAcctBeginBal(gvData.DataKeys[gvrP.RowIndex].Values["ACCT_CODE"].ToString(), ddlCompany.SelectedValue, ddlperiod.SelectedItem.Text, ddlsegment.SelectedItem.Text)).Tables[0];
            }
            BindGrid_AcctBeginBal(dtAccBaldtl);
        }
        protected void ddlAcctcode_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (chkUnposted.Checked)
            {
                dtGridData = DBMethod.ExecuteQuery(Balances_DAL.getBeginBalDtl_UP(ddlperiod.SelectedItem.Text, ddlsegment.SelectedValue, ddlCompany.SelectedValue, ddlAcctcode.SelectedValue)).Tables[0];
            }
            else
            {
                dtGridData = DBMethod.ExecuteQuery(Balances_DAL.getBeginBalDtl(ddlperiod.SelectedItem.Text, ddlsegment.SelectedValue, ddlCompany.SelectedValue, ddlAcctcode.SelectedValue)).Tables[0];
            }
            
            BindGrid(dtGridData);

        }

        protected void btnDetails2_Click(object sender, EventArgs e)
        {
            MPCurrencywise.Show();
            GridViewRow gvrP = (GridViewRow)((Control)sender).Parent.Parent;
            string str_Unposted = "0";
            if (chkUnposted.Checked)
            {
                str_Unposted = "1";
            }
            dtAccBal_currwise = DBMethod.ExecuteQuery(FIN.DAL.GL.Balances_DAL.getAcctBal_Currencywise(gvABB.DataKeys[gvrP.RowIndex].Values["ACCT_CODE"].ToString(), gvABB.DataKeys[gvrP.RowIndex].Values["CURRENCY_CODE"].ToString(), ddlCompany.SelectedValue, ddlperiod.SelectedItem.Text, ddlsegment.SelectedItem.Text, str_Unposted)).Tables[0];
            BindGrid_AcctBal_Currwise(dtAccBal_currwise);
        }

        protected void btnDetails3_Click(object sender, EventArgs e)
        {
            MPDaywise.Show();
            GridViewRow gvrP = (GridViewRow)((Control)sender).Parent.Parent;
            string str_Unposted = "0";
            if (chkUnposted.Checked)
            {
                str_Unposted = "1";
            }
            TextBox txtPeriodDay = gvrP.FindControl("txtPeriodDay") as TextBox;
            dtAccBal_Daywise = DBMethod.ExecuteQuery(FIN.DAL.GL.Balances_DAL.getAcctBal_Daywise(DBMethod.ConvertDateToString(txtPeriodDay.Text.ToString()), gvAB_CW.DataKeys[gvrP.RowIndex].Values["ACCT_CODE"].ToString(), gvAB_CW.DataKeys[gvrP.RowIndex].Values["CURRENCY_CODE"].ToString(), ddlCompany.SelectedValue, ddlperiod.SelectedValue, ddlsegment.SelectedValue, str_Unposted)).Tables[0];
            BindGrid_AcctBal_Daywise(dtAccBal_Daywise);

        }
        protected void btnViewJV_Click(object sender, EventArgs e)
        {
            string str_JVID = "0";
            GridViewRow gvrP = (GridViewRow)((Control)sender).Parent.Parent;
            str_JVID = gvAB_DW.DataKeys[gvrP.RowIndex].Values["JE_HDR_ID"].ToString();
            frm_jv_View.Attributes.Add("src", "" + "JournalEntry.aspx?QueryMode=YES&ProgramID=13&Mode=U&ID=" + str_JVID + "&AddFlag=1&UpdateFlag=1&DeleteFlag=1&QueryFlag=1&ReportName=GL_Reports/RPTJournalVoucher.rpt&ReportName_OL=");
            mpeJEView.Show();
        }
        
        protected void btnView_Click(object sender, EventArgs e)
        {
            if (chkUnposted.Checked)
            {
                dtGridData = DBMethod.ExecuteQuery(Balances_DAL.getBeginBalDtl_UP(ddlperiod.SelectedItem.Text, ddlsegment.SelectedValue, ddlCompany.SelectedValue, ddlAcctcode.SelectedValue)).Tables[0];
            }
            else
            {
                dtGridData = DBMethod.ExecuteQuery(Balances_DAL.getBeginBalDtl(ddlperiod.SelectedItem.Text, ddlsegment.SelectedValue, ddlCompany.SelectedValue, ddlAcctcode.SelectedValue)).Tables[0];
            }

            BindGrid(dtGridData);
        }

    }
}