﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="AccountStructureEntry.aspx.cs" Inherits="FIN.Client.GL.AccountStructureEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 700px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 200px" id="lblStructureName">
                Structure Name
            </div>
            <div class="divtxtBox LNOrient" style="  width: 370px">
                <asp:TextBox ID="txtStructureName" MaxLength="250" CssClass="validate[required] RequiredField txtBox"
                    runat="server" TabIndex="1"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 200px" id="lblStructureNameAR">
                Structure Name(Arabic)
            </div>
            <div class="divtxtBox LNOrient" style="  width: 370px">
                <asp:TextBox ID="txtStructureNameAR" MaxLength="240" CssClass="txtBox_ol"
                    runat="server" TabIndex="2"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 200px" id="lblEffectiveDate">
                Effective Date
            </div>
            <div class="divtxtBox LNOrient" style="  width: 110px">
              <asp:TextBox runat="server" ID="txtEffectiveDate" CssClass="validate[required, custom[ReqDateDDMMYYY],dateRange[dg1]]  RequiredField  txtBox"
                    TabIndex="3"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender2" TargetControlID="txtEffectiveDate"
                    OnClientDateSelectionChanged="checkDate" />
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtEffectiveDate" />
            </div>
             <div class="colspace  LNOrient" >
                &nbsp</div>
             <div class="lblBox LNOrient" style="  width: 125px" id="lblEndDate">
                End Date
            </div>
            <div class="divtxtBox LNOrient" style="  width: 110px">
                <asp:TextBox ID="txtEndDate" 
                    CssClass="validate[, custom[ReqDateDDMMYYY],dateRange[dg1]]  txtBox"
                    runat="server" ontextchanged="txtEndDate_TextChanged" TabIndex="4"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtEndDate" OnClientDateSelectionChanged="checkDate"/>
            </div> 
        </div>
        <div class="divClear_10">
        </div>
        
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 200px" id="lblActive">
                Active
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:CheckBox ID="chkActive" runat="server" Checked="True" TabIndex="5" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="LNOrient">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="DisplayFont Grid"
                OnRowCancelingEdit="gvData_RowCancelingEdit" OnRowCommand="gvData_RowCommand"
                OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound" OnRowDeleting="gvData_RowDeleting"
                OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating" 
                ShowFooter="True" DataKeyNames="SEGMENT_ID">
                <Columns>
                    <asp:TemplateField HeaderText="Segment Name" FooterStyle-Width="300px">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlSegment" runat="server"  CssClass="ddlStype" TabIndex="6">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlSegment" runat="server" CssClass="ddlStype" TabIndex="6">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblSegmentName" runat="server" Text='<%# Eval("SEGMENT_NAME") %>'
                               ></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" Width="300px" />
                    </asp:TemplateField>
                   <asp:TemplateField HeaderText="Active">
                        <EditItemTemplate>
                            <asp:CheckBox ID="chkact" runat="server"  Checked='<%# Convert.ToBoolean(Eval("ENABLED_FLAG")) %>' TabIndex="7" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:CheckBox ID="chkact" Checked="true" runat="server" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkact" runat="server" Checked='<%# Convert.ToBoolean(Eval("ENABLED_FLAG")) %>' TabIndex="7" Enabled="false"/>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" runat="server" AlternateText="Edit" CausesValidation="false" ToolTip="Edit" TabIndex="8"
                                CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                            <asp:ImageButton ID="ibtnDelete" runat="server" AlternateText="Delete" CausesValidation="false" ToolTip="Delete" TabIndex="9"
                                CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibtnUpdate" runat="server" AlternateText="Update" CommandName="Update" ToolTip="Update" TabIndex="10"
                                Height="20px" ImageUrl="~/Images/Update.png" />
                            <asp:ImageButton ID="ibtnCancel" runat="server" AlternateText="Cancel" CausesValidation="false" ToolTip="Cancel" TabIndex="11"
                                CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="ibtnInsert" runat="server" AlternateText="Add" CommandName="FooterInsert" ToolTip="Add"
                                ImageUrl="~/Images/Add.jpg" TabIndex="12" />
                        </FooterTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" 
                            OnClick="btnSave_Click" TabIndex="14" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" 
                            TabIndex="15" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" 
                            TabIndex="16" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" 
                            TabIndex="17" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table width="100%">
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/GL/GLChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
       
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
