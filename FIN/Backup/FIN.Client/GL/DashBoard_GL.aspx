﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="DashBoard_GL.aspx.cs" Inherits="FIN.Client.GL.DashBoard_GL" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>
<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function fn_CloseBrowser() {
            window.close();
        }

        function fn_ShowParam() {
            $("#DivParam").toggle("slow");
        }

        function fn_showGBFilter() {
            $("#divGBFilter").fadeToggle(1000);
        }

        function fn_GBLevel() {
            $("#div_GBLevel").fadeToggle(1000);
        }
        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 100%;">
        <table width="120%">
            <tr>
                <td style="width: 32%" valign="top">
                    <table width="100%">
                        <tr>
                            <td valign="top">
                                <div id="divGLTran" style="width: 100%">
                                    <div class="GridHeader" style="  width: 100%">
                                        <div style="float: left">
                                            GL Transaction
                                        </div>
                                        <div style="width: 120px; float: right; padding-right: 10px">
                                            <asp:DropDownList ID="ddl_GL_Trans_Period" runat="server" CssClass="ddlStype" AutoPostBack="True"
                                                OnSelectedIndexChanged="ddl_GL_Trans_Period_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                        <div style="width: 120px; float: right; padding-right: 10px">
                                            <asp:DropDownList ID="ddl_GL_Trans_Year" runat="server" CssClass="ddlStype" AutoPostBack="True"
                                                OnSelectedIndexChanged="ddl_GL_Trans_Year_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="divClear_10" style="width: 50%">
                                    </div>
                                    <div style="  width: 100%">
                                        <asp:Chart ID="chrt_GLtrans" runat="server" Height="250px" Width="500px">
                                            <Series>
                                                <asp:Series Name="S_Dept_Count" ChartType="Pie" XValueMember="SOURCE_CODE" YValueMembers="TOT_AMT"
                                                    IsValueShownAsLabel="true" LabelAngle="60" CustomProperties="PieLabelStyle=Outside"
                                                    Legend="Legend1" LegendText="#VALX ( #VAL )">
                                                </asp:Series>
                                            </Series>
                                            <ChartAreas>
                                                <asp:ChartArea Name="ChartArea1" Area3DStyle-Enable3D="true" Area3DStyle-Inclination="20">
                                                    <Area3DStyle Enable3D="True" Inclination="20"></Area3DStyle>
                                                </asp:ChartArea>
                                            </ChartAreas>
                                            <Legends>
                                                <asp:Legend Docking="Bottom" Name="Legend1" Alignment="Near" IsDockedInsideChartArea="False">
                                                </asp:Legend>
                                            </Legends>
                                        </asp:Chart>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <div id="div_IDGB" style="width: 100%">
                                    <div class="GridHeader" style="  width: 100%">
                                        <div style="float: left">
                                            Group Balances
                                        </div>
                                        <div style="float: right; padding: 10px">
                                            <img src="../Images/DashBoardImage/ViewList.png" width="20px" height="20px" onclick="fn_GBLevel()" />
                                        </div>
                                        <div style="width: 120px; float: right; padding-right: 10px">
                                            <asp:DropDownList ID="ddl_GB_FINPeriod" runat="server" CssClass="ddlStype" AutoPostBack="True"
                                                OnSelectedIndexChanged="ddl_GB_FINPeriod_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                        <div style="width: 120px; float: right; padding-right: 10px">
                                            <asp:DropDownList ID="ddl_GB_FINYear" runat="server" CssClass="ddlStype" AutoPostBack="True"
                                                OnSelectedIndexChanged="ddl_GB_FINYear_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div id="div_GBLevel" style="width: 32%; background-color: ThreeDFace; position: absolute;
                                        top: 330px; left: 20px; display: none">
                                        <div id="div_Level1" runat="server" visible="true">
                                            <asp:GridView ID="GV_L1" runat="server" CssClass="Grid" Width="100%" DataKeyNames="GB_ID1"
                                                AutoGenerateColumns="False">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Department">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnk_GB1_Name" runat="server" Text='<%# Eval("GBName") %>' OnClick="lnk_GB1_Name_Click"></asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="GB" HeaderText="Balance">
                                                        <ItemStyle HorizontalAlign="Right" />
                                                    </asp:BoundField>
                                                </Columns>
                                                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                                <HeaderStyle CssClass="GridHeader" />
                                                <AlternatingRowStyle CssClass="GrdAltRow" />
                                            </asp:GridView>
                                        </div>
                                        <div id="div_Level2" runat="server" visible="true">
                                            <table width="100%">
                                                <tr>
                                                    <td align="right">
                                                        <asp:ImageButton ID="imgL2" runat="server" ImageUrl="~/Images/DashBoardImage/back.png"
                                                            Width="20px" Height="20px" OnClick="imgL2_Click" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:GridView ID="GV_L2" runat="server" CssClass="Grid" Width="100%" DataKeyNames="GB_ID2"
                                                            AutoGenerateColumns="False">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Department">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="lnk_GB2_Name" runat="server" Text='<%# Eval("GBName") %>' OnClick="lnk_GB2_Name_Click"></asp:LinkButton>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField DataField="GB" HeaderText="Balance">
                                                                    <ItemStyle HorizontalAlign="Right" />
                                                                </asp:BoundField>
                                                            </Columns>
                                                            <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                                            <HeaderStyle CssClass="GridHeader" />
                                                            <AlternatingRowStyle CssClass="GrdAltRow" />
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div id="div_Level3" runat="server" visible="true">
                                            <table width="100%">
                                                <tr>
                                                    <td align="right">
                                                        <asp:ImageButton ID="imgL3" runat="server" ImageUrl="~/Images/DashBoardImage/back.png"
                                                            Width="20px" Height="20px" OnClick="imgL3_Click" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:GridView ID="GV_L3" runat="server" CssClass="Grid" Width="100%" DataKeyNames="GB_ID3"
                                                            AutoGenerateColumns="False">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Department">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="lnk_GB3_Name" runat="server" Text='<%# Eval("GBName") %>' OnClick="lnk_GB3_Name_Click"></asp:LinkButton>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField DataField="GB" HeaderText="Balance">
                                                                    <ItemStyle HorizontalAlign="Right" />
                                                                </asp:BoundField>
                                                            </Columns>
                                                            <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                                            <HeaderStyle CssClass="GridHeader" />
                                                            <AlternatingRowStyle CssClass="GrdAltRow" />
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div id="div_Level4" runat="server" visible="true">
                                            <table width="100%">
                                                <tr>
                                                    <td align="right">
                                                        <asp:ImageButton ID="imgL4" runat="server" ImageUrl="~/Images/DashBoardImage/back.png"
                                                            Width="20px" Height="20px" OnClick="imgL4_Click" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:GridView ID="GV_L4" runat="server" CssClass="Grid" Width="100%" DataKeyNames="GB_ID4"
                                                            AutoGenerateColumns="False">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Department">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="lnk_GB4_Name" runat="server" Text='<%# Eval("GBName") %>' OnClick="lnk_GB4_Name_Click"></asp:LinkButton>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField DataField="GB" HeaderText="Balance">
                                                                    <ItemStyle HorizontalAlign="Right" />
                                                                </asp:BoundField>
                                                            </Columns>
                                                            <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                                            <HeaderStyle CssClass="GridHeader" />
                                                            <AlternatingRowStyle CssClass="GrdAltRow" />
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div id="div_Level5" runat="server" visible="true">
                                            <table width="100%">
                                                <tr>
                                                    <td align="right">
                                                        <asp:ImageButton ID="imgL5" runat="server" ImageUrl="~/Images/DashBoardImage/back.png"
                                                            Width="20px" Height="20px" OnClick="imgL5_Click" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:GridView ID="GV_L5" runat="server" CssClass="Grid" Width="100%" DataKeyNames="GB_ID4"
                                                            AutoGenerateColumns="False">
                                                            <Columns>
                                                                <asp:BoundField DataField="GBName" HeaderText="Account Code">
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="GB" HeaderText="Balance">
                                                                    <ItemStyle HorizontalAlign="Right" />
                                                                </asp:BoundField>
                                                            </Columns>
                                                            <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                                            <HeaderStyle CssClass="GridHeader" />
                                                            <AlternatingRowStyle CssClass="GrdAltRow" />
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <div id="divGBFilter" style="display: none; position: fixed; background-color: ThreeDFace;
                                        left: 10px; top: 315px; width: 450px;">
                                        <div class="divRowContainer">
                                            <div class="lblBox LNOrient" style="  width: 100px;" id="lblFirstName">
                                                From Date
                                            </div>
                                            <div class="divtxtBox LNOrient" style="  width: 100px">
                                                <asp:TextBox ID="txtFromDate" runat="server" ToolTip="From Date" TabIndex="1" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]]  RequiredField txtBox"></asp:TextBox>
                                                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calExtender1" TargetControlID="txtFromDate"
                                                    OnClientDateSelectionChanged="checkDate" />
                                            </div>
                                            <div class="lblBox LNOrient" style="  width: 100px;" id="Div13">
                                                To Date
                                            </div>
                                            <div class="divtxtBox LNOrient" style="  width: 100px">
                                                <asp:TextBox ID="txtToDate" runat="server" ToolTip="To Date" TabIndex="4" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]]  RequiredField txtBox"></asp:TextBox>
                                                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtToDate"
                                                    OnClientDateSelectionChanged="checkDate" />
                                            </div>
                                        </div>
                                        <div class="divClear_10" style="width: 50%">
                                        </div>
                                        <div class="divRowContainer">
                                            <div class="lblBox LNOrient" style="  width: 100px;" id="Div14">
                                                Global Segment
                                            </div>
                                            <div class="divtxtBox LNOrient" style="  width: 300px">
                                                <asp:DropDownList ID="ddlGlobalSegment" runat="server" TabIndex="1" CssClass=" ddlStype">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="divClear_10" style="width: 50%">
                                        </div>
                                        <div class="divRowContainer">
                                            <div class="lblBox LNOrient" style="  width: 100px;" id="Div15">
                                                GroupName
                                            </div>
                                            <div class="divtxtBox LNOrient" style="  width: 300px">
                                                <asp:DropDownList ID="ddlGroupName" runat="server" TabIndex="2" CssClass=" ddlStype">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="divClear_10" style="width: 50%">
                                        </div>
                                        <div style="width: 80px; float: right; display: none; padding-right: 10px">
                                            <asp:DropDownList ID="ddlAccountCode" runat="server" TabIndex="3" CssClass=" ddlStype">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="divClear_10" style="width: 50%">
                                        </div>
                                        <div class="divRowContainer">
                                            <div class="divtxtBox LNOrient" style="float: right; width: 400px">
                                                <asp:ImageButton ID="imgbtnGBFilter" runat="server" ImageUrl="~/Images/DashBoardImage/agt_back.png"
                                                    OnClick="imgBtnGenChart_Click" Width="20px" Height="20px" Style="border: 0px" />
                                            </div>
                                        </div>
                                        <div class="divClear_10" style="width: 50%">
                                        </div>
                                        <div class="divRowContainer">
                                        </div>
                                    </div>
                                    <div class="divClear_10" style="width: 70%">
                                    </div>
                                    <div style="  width: 100%">
                                        <asp:Chart ID="ChartGroupGL" runat="server" Width="400px" Height="400px">
                                            <Series>
                                                <asp:Series ChartArea="ChartArea1" Name="s_GB" IsValueShownAsLabel="True" XValueMember="GBName"
                                                    YValueMembers="GB">
                                                </asp:Series>
                                            </Series>
                                            <ChartAreas>
                                                <asp:ChartArea Name="ChartArea1">
                                                    <Area3DStyle Enable3D="false" Inclination="25" Rotation="40"></Area3DStyle>
                                                    <AxisX>
                                                        <MajorGrid Enabled="false" />
                                                    </AxisX>
                                                    <AxisY>
                                                        <MajorGrid Enabled="false" />
                                                    </AxisY>
                                                </asp:ChartArea>
                                            </ChartAreas>
                                        </asp:Chart>
                                    </div>
                                    <div class="divClear_10" style="width: 50%">
                                    </div>
                                    <div id="divempGrid" style="width: 98%; display: none; height: 100px; overflow: auto">
                                        <div id="divEmpDept" runat="server" visible="true">
                                            <asp:GridView ID="gvDeptDet" runat="server" CssClass="Grid" Width="100%" DataKeyNames="acct_group_ID"
                                                AutoGenerateColumns="False">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Department">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="account_group_description" runat="server" Text='<%# Eval("account_group_description") %>'
                                                                OnClick="lnk_Group_Click"></asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="opening_balance" HeaderText="Opening Balance" SortExpression="opening_balance">
                                                        <ItemStyle HorizontalAlign="Right" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="transaction_debit" HeaderText="Debit" SortExpression="transaction_debit">
                                                        <ItemStyle HorizontalAlign="Right" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="transaction_credit" HeaderText="Credit" SortExpression="transaction_credit">
                                                        <ItemStyle HorizontalAlign="Right" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="closing_balance" HeaderText="Closing Balance" SortExpression="closing_balance">
                                                        <ItemStyle HorizontalAlign="Right" />
                                                    </asp:BoundField>
                                                </Columns>
                                                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                                <HeaderStyle CssClass="GridHeader" />
                                                <AlternatingRowStyle CssClass="GrdAltRow" />
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="width: 66%">
                    <table width="100%">
                        <tr>
                            <td valign="top">
                                <div id="dividTB" style="width: 100%">
                                    <div class="GridHeader" style="  width: 100%">
                                        <div style=" ">
                                            Trail Balance
                                        </div>
                                        <div style="width: 120px; float: right; padding-right: 10px">
                                            <asp:DropDownList ID="ddl_TBFinPeriod" runat="server" CssClass="ddlStype" AutoPostBack="True"
                                                OnSelectedIndexChanged="ddl_TBFinPeriod_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                        <div style="width: 120px; float: right; padding-right: 10px">
                                            <asp:DropDownList ID="ddl_TBFinYear" runat="server" CssClass="ddlStype" AutoPostBack="True"
                                                OnSelectedIndexChanged="ddl_TBFinYear_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                        <div id="divFilter" style="display: none">
                                            <div style="width: 10px; float: right; padding-right: 10px">
                                                <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Images/agt_back.png"
                                                    OnClick="imgBtnGenTBChart_Click" Width="20px" Height="20px" Style="border: 0px" />
                                            </div>
                                            <div style="width: 55px; float: right; padding-right: 10px">
                                                <asp:DropDownList ID="ddlTBCode" runat="server" TabIndex="3" CssClass=" ddlStype">
                                                </asp:DropDownList>
                                            </div>
                                            <div style="width: 55px; float: right; padding-right: 10px">
                                                <asp:DropDownList ID="ddlTBGlobal" runat="server" TabIndex="1" CssClass=" ddlStype"
                                                    OnSelectedIndexChanged="ddlTBAccountGroup_SelectedIndexChanged" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                            <div style="width: 40px; float: right; padding-right: 10px">
                                                <asp:TextBox ID="txtTBTo" runat="server" ToolTip="To Date" TabIndex="4" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]]  RequiredField txtBox"></asp:TextBox>
                                                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender3" TargetControlID="txtTBTo"
                                                    OnClientDateSelectionChanged="checkDate" />
                                            </div>
                                            <div style="width: 40px; float: right; padding-right: 10px">
                                                <asp:TextBox ID="txtTBFrom" runat="server" ToolTip="From Date" TabIndex="4" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]]  RequiredField txtBox"></asp:TextBox>
                                                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender2" TargetControlID="txtTBFrom"
                                                    OnClientDateSelectionChanged="checkDate" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="divClear_10" style="width: 58%">
                                    </div>
                                    <div id="div8" style="height: 300px; width: 98%; overflow: auto">
                                        <asp:GridView ID="gvTrailBal" runat="server" CssClass="Grid" Width="100%" DataKeyNames="ACCT_CODE_ID"
                                            AutoGenerateColumns="False">
                                            <Columns>
                                                <asp:BoundField DataField="ACCT_CODE" HeaderText="Code"></asp:BoundField>
                                                <asp:BoundField DataField="ACCT_CODE_DESC" HeaderText="Name"></asp:BoundField>
                                                <asp:BoundField DataField="OP_BAL" HeaderText="Opening Balance">
                                                    <ItemStyle HorizontalAlign="Right" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="TRANS_DR" HeaderText="Debit">
                                                    <ItemStyle HorizontalAlign="Right" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="TRANS_CR" HeaderText="Credit">
                                                    <ItemStyle HorizontalAlign="Right" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="CLOS_BAL" HeaderText="Closing Balance">
                                                    <ItemStyle HorizontalAlign="Right" />
                                                </asp:BoundField>
                                            </Columns>
                                            <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                            <HeaderStyle CssClass="GridHeader" />
                                            <AlternatingRowStyle CssClass="GrdAltRow" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <table width="100%">
                                    <tr>
                                        <td valign="top" style="width:50%">
                                            <table width="100%">
                                                <tr>
                                                    <td valign="top">
                                                        <div id="dividExpenses" style="width: 100%">
                                                            <div class="GridHeader" style="  width: 100%">
                                                                <div style="float: left">
                                                                    Top 5 Expenses
                                                                </div>
                                                                <div style="width: 120px; float: right; padding-right: 10px">
                                                                    <asp:DropDownList ID="ddl_E_Period" runat="server" CssClass="ddlStype" AutoPostBack="True"
                                                                        OnSelectedIndexChanged="ddl_E_Period_SelectedIndexChanged">
                                                                    </asp:DropDownList>
                                                                </div>
                                                                <div style="width: 120px; float: right; padding-right: 10px">
                                                                    <asp:DropDownList ID="ddl_E_Year" runat="server" CssClass="ddlStype" AutoPostBack="True"
                                                                        OnSelectedIndexChanged="ddl_E_Year_SelectedIndexChanged">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="divClear_10" style="width: 58%">
                                                            </div>
                                                            <div id="dividExpenChrt" style="width: 98%;">
                                                                <asp:Chart ID="chartExpenses" runat="server" Width="700px" Height="400px">
                                                                    <Series>
                                                                        <asp:Series ChartArea="ChartArea1" Name="BALANCE_AMT" IsValueShownAsLabel="false"
                                                                            XValueMember="group_name" YValueMembers="BALANCE_AMT" CustomProperties="PointWidth=.2">
                                                                        </asp:Series>
                                                                    </Series>
                                                                    <ChartAreas>
                                                                        <asp:ChartArea Name="ChartArea1">
                                                                            <AxisX>
                                                                                <MajorGrid Enabled="false" />
                                                                            </AxisX>
                                                                            <AxisY>
                                                                                <MajorGrid Enabled="false" />
                                                                            </AxisY>
                                                                        </asp:ChartArea>
                                                                    </ChartAreas>
                                                                </asp:Chart>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top">
                                                        <div id="dividRevenue" style="width: 100%">
                                                            <div class="GridHeader" style="  width: 100%">
                                                                <div style="float: left">
                                                                    Top 5 Revenue
                                                                </div>
                                                                <div style="width: 120px; float: right; padding-right: 10px">
                                                                    <asp:DropDownList ID="ddl_R_Period" runat="server" CssClass="ddlStype" AutoPostBack="True"
                                                                        OnSelectedIndexChanged="ddl_R_Period_SelectedIndexChanged">
                                                                    </asp:DropDownList>
                                                                </div>
                                                                <div style="width: 120px; float: right; padding-right: 10px">
                                                                    <asp:DropDownList ID="ddl_R_Year" runat="server" CssClass="ddlStype" AutoPostBack="True"
                                                                        OnSelectedIndexChanged="ddl_R_Year_SelectedIndexChanged">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="divClear_10" style="width: 58%">
                                                            </div>
                                                            <div id="dividRevChrt" style="width: 98%;">
                                                                <asp:Chart ID="chartRevenue" runat="server" Width="700px" Height="400px">
                                                                    <Series>
                                                                        <asp:Series ChartArea="ChartArea1" Name="BALANCE_AMT" IsValueShownAsLabel="false"
                                                                            XValueMember="group_name" YValueMembers="BALANCE_AMT" CustomProperties="PointWidth=.2">
                                                                        </asp:Series>
                                                                    </Series>
                                                                    <ChartAreas>
                                                                        <asp:ChartArea Name="ChartArea1">
                                                                            <AxisX>
                                                                                <MajorGrid Enabled="false" />
                                                                            </AxisX>
                                                                            <AxisY>
                                                                                <MajorGrid Enabled="false" />
                                                                            </AxisY>
                                                                        </asp:ChartArea>
                                                                    </ChartAreas>
                                                                </asp:Chart>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td valign="top" style="width:50%">
                                            <table width="100%">
                                                <tr>
                                                    <td valign="top">
                                                        <div id="divIdBB" style="width: 100%">
                                                            <div class="GridHeader" style="  width: 100%">
                                                                Bank Balances
                                                            </div>
                                                        </div>
                                                        <div class="divClear_10" style="width: 50%">
                                                        </div>
                                                        <div id="divIdBBchrt" style="width: 98%;">
                                                            <asp:Chart ID="chartBankBal" runat="server" Width="300px" Height="250px">
                                                                <Series>
                                                                    <asp:Series ChartArea="ChartArea1" Name="BALANCE_AMT" IsValueShownAsLabel="True"
                                                                        XValueMember="BANK_NAME" YValueMembers="BALANCE_AMT" CustomProperties="PointWidth=.2">
                                                                    </asp:Series>
                                                                </Series>
                                                                <ChartAreas>
                                                                    <asp:ChartArea Name="ChartArea1">
                                                                        <AxisX>
                                                                            <MajorGrid Enabled="false" />
                                                                        </AxisX>
                                                                        <AxisY>
                                                                            <MajorGrid Enabled="false" />
                                                                        </AxisY>
                                                                    </asp:ChartArea>
                                                                </ChartAreas>
                                                                <%--  <Legends>
                                <asp:Legend IsDockedInsideChartArea="false" Alignment="Center" Name="Legend1" Docking="Bottom">
                                </asp:Legend>
                            </Legends>--%>
                                                            </asp:Chart>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top">
                                                        <div id="dividCB" style="width: 100%">
                                                            <div class="GridHeader" style="  width: 100%">
                                                                Cash Balance
                                                            </div>
                                                            <div class="divClear_10" style="width: 58%">
                                                            </div>
                                                            <div id="dividcbGrid" style="width: 98%; height: 100px">
                                                                <asp:GridView ID="gvCashBal" runat="server" CssClass="Grid" Width="100%" DataKeyNames="acct_code_id"
                                                                    AutoGenerateColumns="False">
                                                                    <Columns>
                                                                        <%--<asp:TemplateField HeaderText="Account Name">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="ACCOUNT_NAME" runat="server" Text='<%# Eval("ACCOUNT_NAME") %>'></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                                                                        <asp:BoundField DataField="ACCOUNT_NAME" HeaderText="Account Name" SortExpression="ACCOUNT_NAME">
                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="BALANCE_AMT" HeaderText="Balance Amount" SortExpression="BALANCE_AMT">
                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                        </asp:BoundField>
                                                                    </Columns>
                                                                    <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                                                    <HeaderStyle CssClass="GridHeader" />
                                                                    <AlternatingRowStyle CssClass="GrdAltRow" />
                                                                </asp:GridView>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <div class="divFormcontainer" style="width: 100%; display: none;" id="divMainContainer">
        <div style="width: 1200px">
            <div class="divClear_10">
            </div>
            <div class="divClear_10" style="width: 98%">
            </div>
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="  display: none; width: 50px" id="Div16">
                    <img src="../Images/agt_back.png" alt="Parameters" title="Parameters" onclick="fn_ShowParam()"
                        width="25px" height="25px" />
                </div>
            </div>
            <div class="divClear_10" style="width: 98%">
            </div>
            <div class="lblBox LNOrient" style="  display: none;" id="DivParam">
                <div class="divRowContainer">
                    <div class="lblBox LNOrient" style="  width: 80px" id="lblGlobalSegment">
                        Global Segment
                    </div>
                    <div class="divtxtBox LNOrient" style="  width: 100px">
                    </div>
                </div>
                <div class="divClear_10">
                </div>
                <div class="divRowContainer">
                    <div class="lblBox LNOrient" style="  width: 80px" id="Div1">
                        Group
                    </div>
                    <div class="divtxtBox LNOrient" style="  width: 100px">
                    </div>
                </div>
                <div class="divClear_10">
                </div>
                <div class="divRowContainer">
                    <div class="lblBox LNOrient" style="  width: 80px" id="Div2">
                        Code
                    </div>
                    <div class="divtxtBox LNOrient" style="  width: 100px">
                    </div>
                </div>
                <div class="divClear_10">
                </div>
                <div class="divRowContainer">
                    <div class="lblBox LNOrient" style="  width: 80px" id="lblDate">
                        From Date
                    </div>
                    <div class="divtxtBox LNOrient" style="  width: 98px">
                    </div>
                </div>
                <div class="divClear_10">
                </div>
                <div class="divRowContainer">
                    <div class="lblBox LNOrient" style="  width: 80px" id="Div6">
                        To Date
                    </div>
                    <div class="divtxtBox LNOrient" style="  width: 98px">
                    </div>
                </div>
                <div class="divClear_10">
                </div>
                <div class="divRowContainer">
                    <div class="lblBox LNOrient" style="  width: 50px" id="Div10">
                        <asp:Button ID="btnGenChart" runat="server" Text="Generate Chart" OnClick="btnGenerateChart_Click"
                            Visible="false" CssClass="btn" TabIndex="8" />
                    </div>
                </div>
                <div class="divClear_10">
                </div>
                <div class="divRowContainer">
                    <div class="lblBox LNOrient" style="  width: 250px" id="Div11">
                    </div>
                </div>
            </div>
            <div>
                <div class="divRowContainer">
                    <div class="lblBox LNOrient" style="  width: 80%;" id="Div9">
                        <div class="divRowContainer" style="width: 250px;">
                            <div class="lblBox LNOrient" style="  width: 250px; height: 200px" id="Div7">
                                <asp:Chart ID="pieJNT" runat="server" BackGradientStyle="LeftRight" BorderlineWidth="0"
                                    Height="200px" Palette="None" Width="240px" BorderlineColor="64, 0, 64">
                                    <Titles>
                                        <asp:Title Name="Items" Text="GL Transaction" />
                                    </Titles>
                                    <Legends>
                                        <asp:Legend Alignment="Center" Docking="Bottom" IsTextAutoFit="False" Name="Default"
                                            LegendStyle="Row" />
                                    </Legends>
                                    <Series>
                                        <asp:Series Name="Default" />
                                    </Series>
                                    <ChartAreas>
                                        <asp:ChartArea Name="ChartArea1" BorderWidth="0" />
                                    </ChartAreas>
                                </asp:Chart>
                            </div>
                            <div class="colspace" style=" ">
                                &nbsp</div>
                            <div class="lblBox LNOrient" style="  width: 350px; display: none;" id="Div12">
                                <asp:GridView ID="gvTrail" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                                    CssClass="Grid" Width="80%" PageSize="10">
                                    <Columns>
                                        <asp:BoundField DataField="account_group_description" HeaderText="Group" ReadOnly="True"
                                            SortExpression="account_group_description" />
                                        <asp:BoundField DataField="opening_balance" HeaderText="Opening Balance" SortExpression="opening_balance" />
                                        <asp:BoundField DataField="transaction_debit" HeaderText="Debit" SortExpression="transaction_debit" />
                                        <asp:BoundField DataField="transaction_credit" HeaderText="Credit" SortExpression="transaction_credit" />
                                        <asp:BoundField DataField="closing_balance" HeaderText="Closing Balance" SortExpression="closing_balance" />
                                    </Columns>
                                    <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                    <HeaderStyle CssClass="GridHeader" />
                                    <AlternatingRowStyle CssClass="GrdAltRow" />
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                    <div class="divClear_10">
                    </div>
                    <div class="divRowContainer">
                        <div class="lblBox LNOrient" style="  width: 50px" id="Div3">
                            <img src="../Images/agt_back.png" alt="Parameters" title="Parameters" onclick="fn_ShowParam()"
                                width="25px" height="25px" />
                        </div>
                    </div>
                    <div class="divClear_10">
                    </div>
                    <div class="colspace" style=" ">
                        &nbsp</div>
                </div>
                <div class="lblBox LNOrient" style="  display: none; width: 550px" id="Div4">
                    <asp:Chart ID="ChartGL" runat="server" Width="450px" Height="450px">
                        <Titles>
                            <asp:Title Name="Global Segment and Group wise Balances" />
                        </Titles>
                        <Series>
                            <asp:Series Name="GLCreditSeries">
                            </asp:Series>
                            <asp:Series Name="GLDebitSeries">
                            </asp:Series>
                        </Series>
                        <Legends>
                            <asp:Legend Name="Default">
                            </asp:Legend>
                        </Legends>
                        <ChartAreas>
                            <asp:ChartArea Name="ChartAreaGL">
                            </asp:ChartArea>
                        </ChartAreas>
                    </asp:Chart>
                </div>
                <div class="colspace" style=" ">
                    &nbsp</div>
                <div class="lblBox LNOrient" style="  width: 350px;" id="Div5">
                    <asp:GridView ID="gvGridCostCentre" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                        CssClass="Grid" Width="80%" PageSize="10">
                        <Columns>
                            <asp:BoundField DataField="account_group_description" HeaderText="Group" ReadOnly="True"
                                SortExpression="account_group_description" />
                            <asp:BoundField DataField="opening_balance" HeaderText="Opening Balance" SortExpression="opening_balance" />
                            <asp:BoundField DataField="transaction_debit" HeaderText="Debit" SortExpression="transaction_debit" />
                            <asp:BoundField DataField="transaction_credit" HeaderText="Credit" SortExpression="transaction_credit" />
                            <asp:BoundField DataField="closing_balance" HeaderText="Closing Balance" SortExpression="closing_balance" />
                        </Columns>
                        <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                        <HeaderStyle CssClass="GridHeader" />
                        <AlternatingRowStyle CssClass="GrdAltRow" />
                    </asp:GridView>
                </div>
            </div>
        </div>
    </div>
    <%--   <div style="width: 366px">
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="  width: 200px" id="lblGroupName">
                    Group Name
                </div>
                <div class="divtxtBox LNOrient" style="  width: 150px">
                    <asp:DropDownList ID="ddlGroupName" runat="server" TabIndex="3" Width="150px" CssClass=" ddlStype">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <asp:Button ID="btnGenerateGroupChart" runat="server" Text="Generate Group Chart" OnClick="btnGenerateGroupChart_Click"
                    CssClass="btn" TabIndex="4" />
            </div>
            <div class="divClear_10">
            </div>
            <asp:Chart ID="ChartGroupGL" runat="server" Width="200" Height="350">
                <Series>
                    <asp:Series Name="Series1">
                    </asp:Series>
                    <asp:Series Name="Series2">
                    </asp:Series>
                    <asp:Series Name="Series3">
                    </asp:Series>
                    <asp:Series Name="Series4">
                    </asp:Series>
                    <asp:Series Name="Series5">
                    </asp:Series>
                    <asp:Series Name="Series6">
                    </asp:Series>
                </Series>
                <Legends>
                    <asp:Legend Name="Default">
                    </asp:Legend>
                </Legends>
                <ChartAreas>
                    <asp:ChartArea Name="ChartAreaGroupGL">
                    </asp:ChartArea>
                </ChartAreas>
            </asp:Chart>
        </div>--%>
    <%-- <div style="width: 366px">
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="  width: 200px" id="Div3">
                    Global Segment
                </div>
                <div class="divtxtBox LNOrient" style="  width: 150px">
                    <asp:DropDownList ID="DropDownList2" runat="server" TabIndex="3" Width="150px" CssClass=" ddlStype">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="  width: 200px" id="Div4">
                    Date
                </div>
                <div class="divtxtBox LNOrient" style="  width: 150px">
                    <asp:TextBox ID="TextBox2" runat="server" TabIndex="2" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]]  RequiredField txtBox"
                        Width="150px"></asp:TextBox>
                    <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender2" TargetControlID="txtDate"
                        OnClientDateSelectionChanged="checkDate" />
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <asp:Button ID="Button2" runat="server" Text="Generate Chart" OnClick="btnGenerateChart_Click"
                    CssClass="btn" TabIndex="4" />
            </div>
            <div class="divClear_10">
            </div>
            <asp:Chart ID="Chart2" runat="server" Width="200" Height="350">
                <Series>
                    <asp:Series Name="Series1">
                    </asp:Series>
                    <asp:Series Name="Series2">
                    </asp:Series>
                </Series>
                <Legends>
                    <asp:Legend Name="Default">
                    </asp:Legend>
                </Legends>
                <ChartAreas>
                    <asp:ChartArea Name="ChartAreaGL">
                    </asp:ChartArea>
                </ChartAreas>
            </asp:Chart>
        </div>--%>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/GL/GLChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () { $("#form1").validationEngine(); });
        $("#FINContent_btnSave").click(function () {
            if ($("#form1").validationEngine('validate') == false)
                return false;
        });
    </script>
</asp:Content>
