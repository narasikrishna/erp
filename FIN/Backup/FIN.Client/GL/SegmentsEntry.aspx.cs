﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using System.Data;
using FIN.BLL;
using VMVServices.Web;

namespace FIN.Client.GL
{
    public partial class SegmentsEntry : PageBase
    {
        GL_SEGMENTS gL_SEGMENTS = new GL_SEGMENTS();
        GL_SEGMENT_VALUES gL_SEGMENT_VALUES = new GL_SEGMENT_VALUES();
        DataTable dtGridData = new DataTable();
        Boolean bol_rowVisiable;
        Boolean savedBool;
        string ProReturn = null;
        string ProReturn_Valdn = null;
        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();
                    txtSegmentName.Focus();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("GL_seg", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
            //if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            //{
            //    VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
            //}

            //if (VMVServices.Web.Utils.AddAllowed != FINAppConstants.Y && Master.Mode == FINAppConstants.Add)
            //{
            //    btnSave.Visible = false;
            //}

        }

        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }


        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void fillParentSegment()
        {
            FIN.BLL.GL.Segments_BLL.GetGlobalSegmentName(ref ddlParentSegment);
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();


                
                EntityData = null;
                dtGridData = DBMethod.ExecuteQuery(FIN.DAL.GL.Segments_DAL.GetSegmentvalues(Master.StrRecordId)).Tables[0];

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    using (IRepository<GL_SEGMENTS> userCtx = new DataRepository<GL_SEGMENTS>())
                    {
                        gL_SEGMENTS = userCtx.Find(r =>
                            (r.SEGMENT_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = gL_SEGMENTS;

                    txtSegmentName.Text = gL_SEGMENTS.SEGMENT_NAME;
                    if (gL_SEGMENTS.SEGMENT_NAME_OL != null || gL_SEGMENTS.SEGMENT_NAME_OL != string.Empty)
                    {
                        txtSegmentNameAR.Text = gL_SEGMENTS.SEGMENT_NAME_OL;
                    }
                    txtDescription.Text = gL_SEGMENTS.SEGMENT_DESC;
                    txtEffectiveDate.Text = DBMethod.ConvertDateToString(gL_SEGMENTS.EFFECTIVE_START_DT.ToString());
                    if (gL_SEGMENTS.EFFECTIVE_END_DT != null)
                    {
                        txtEndDate.Text = DBMethod.ConvertDateToString(gL_SEGMENTS.EFFECTIVE_END_DT.ToString());
                    }

                    if (gL_SEGMENTS.ENABLED_FLAG == "1")
                    {
                        chkActive.Checked = true;
                    }
                    else
                    {
                        chkActive.Checked = false;
                    }
                    chkdependent.Checked = false;
                    if (gL_SEGMENTS.IS_DEPENDENT.ToString().Trim() == FINAppConstants.Y)
                    {                        
                        chkdependent.Checked = true;                        
                        fillParentSegment();
                        ddlParentSegment.SelectedValue = gL_SEGMENTS.PARENT_SEGMENT_ID;
                    }
                }

                fn_DependentChange();
                BindGrid(dtGridData);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("SEG_ATC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    gL_SEGMENTS = (GL_SEGMENTS)EntityData;
                }

                gL_SEGMENTS.SEGMENT_NAME = txtSegmentName.Text;
                if (txtSegmentNameAR.Text != null)
                {
                    gL_SEGMENTS.SEGMENT_NAME_OL = txtSegmentNameAR.Text;
                }
                gL_SEGMENTS.SEGMENT_DESC = txtDescription.Text;
                gL_SEGMENTS.EFFECTIVE_START_DT = DBMethod.ConvertStringToDate(txtEffectiveDate.Text.ToString());
                if (txtEndDate.Text.ToString().Trim().Length > 0)
                {
                    gL_SEGMENTS.EFFECTIVE_END_DT = DBMethod.ConvertStringToDate(txtEndDate.Text.ToString());
                }
                else
                {
                    gL_SEGMENTS.EFFECTIVE_END_DT = null;
                }

                //  gL_SEGMENTS.WORKFLOW_COMPLETION_STATUS = "1";

                if (chkdependent.Checked)
                {
                    gL_SEGMENTS.IS_DEPENDENT = FINAppConstants.Y;
                    gL_SEGMENTS.PARENT_SEGMENT_ID = ddlParentSegment.SelectedValue.ToString();
                }
                else
                {
                    gL_SEGMENTS.PARENT_SEGMENT_ID = null;
                    gL_SEGMENTS.IS_DEPENDENT = FINAppConstants.N;
                }

                if (chkActive.Checked == true)
                {
                    gL_SEGMENTS.ENABLED_FLAG = FINAppConstants.Y;
                }
                else
                {
                    gL_SEGMENTS.ENABLED_FLAG = FINAppConstants.N;
                }

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    gL_SEGMENTS.MODIFIED_BY = this.LoggedUserName;
                    gL_SEGMENTS.MODIFIED_DATE = DateTime.Today;

                }
                else
                {

                    gL_SEGMENTS.SEGMENT_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.GL_006_M.ToString(), false, true);
                    gL_SEGMENTS.CREATED_BY = this.LoggedUserName;
                    gL_SEGMENTS.CREATED_DATE = DateTime.Today;


                }
                gL_SEGMENTS.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, gL_SEGMENTS.SEGMENT_ID);
                gL_SEGMENTS.SEGMENT_ORG_ID = VMVServices.Web.Utils.OrganizationID;
                // SAVE DETAILS

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                var tmpChildEntity = new List<Tuple<object, string>>();

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    gL_SEGMENT_VALUES = new GL_SEGMENT_VALUES();
                    if (dtGridData.Rows[iLoop][FINColumnConstants.SEGMENT_VALUE_ID].ToString() != "0")
                    {
                        using (IRepository<GL_SEGMENT_VALUES> userCtx = new DataRepository<GL_SEGMENT_VALUES>())
                        {
                            gL_SEGMENT_VALUES = userCtx.Find(r =>
                                (r.SEGMENT_VALUE_ID == dtGridData.Rows[iLoop][FINColumnConstants.SEGMENT_VALUE_ID].ToString())
                                ).SingleOrDefault();
                        }
                    }

                    if (dtGridData.Rows[iLoop][FINColumnConstants.EFFECTIVE_START_DT] != DBNull.Value)
                    {
                        gL_SEGMENT_VALUES.EFFECTIVE_START_DT = DateTime.Parse(dtGridData.Rows[iLoop][FINColumnConstants.EFFECTIVE_START_DT].ToString());
                    }

                    if (dtGridData.Rows[iLoop][FINColumnConstants.EFFECTIVE_END_DT] != DBNull.Value)
                    {
                        gL_SEGMENT_VALUES.EFFECTIVE_END_DT = DateTime.Parse(dtGridData.Rows[iLoop][FINColumnConstants.EFFECTIVE_END_DT].ToString());

                    }
                    else
                    {
                        gL_SEGMENT_VALUES.EFFECTIVE_END_DT = null;
                    }


                    if (dtGridData.Rows[iLoop][FINColumnConstants.ENABLED_FLAG].ToString().ToUpper() == "TRUE")
                    {
                        gL_SEGMENT_VALUES.ENABLED_FLAG = FINAppConstants.Y;
                    }
                    else
                    {
                        gL_SEGMENT_VALUES.ENABLED_FLAG = FINAppConstants.N;
                    }
                    // gL_COMPANIES_DTL.COMP_BASE_CURRENCY = "CURR_01";
                    gL_SEGMENT_VALUES.SEGMENT_VALUE = dtGridData.Rows[iLoop][FINColumnConstants.SEGMENT_VALUE].ToString();
                    gL_SEGMENT_VALUES.SEGMENT_VALUE_OL = dtGridData.Rows[iLoop][FINColumnConstants.SEGMENT_VALUE_OL].ToString();
                    if (dtGridData.Rows[iLoop][FINColumnConstants.PARENT_SEGMENT_VALUE_ID] != null)
                        gL_SEGMENT_VALUES.PARENT_SEGMENT_VALUE_ID = dtGridData.Rows[iLoop][FINColumnConstants.PARENT_SEGMENT_VALUE_ID].ToString();
                    else
                        gL_SEGMENT_VALUES.PARENT_SEGMENT_VALUE_ID = null;

                    gL_SEGMENT_VALUES.WORKFLOW_COMPLETION_STATUS = "1";
                    gL_SEGMENT_VALUES.SEGMENT_ID = gL_SEGMENTS.SEGMENT_ID;

                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        tmpChildEntity.Add(new Tuple<object, string>(gL_SEGMENT_VALUES, FINAppConstants.Delete));
                    }
                    else
                    {
                        {
                            if (dtGridData.Rows[iLoop][FINColumnConstants.SEGMENT_VALUE_ID].ToString() != "0" && dtGridData.Rows[iLoop][FINColumnConstants.SEGMENT_VALUE_ID].ToString() != string.Empty)
                            {
                                gL_SEGMENT_VALUES.SEGMENT_VALUE_ID = dtGridData.Rows[iLoop][FINColumnConstants.SEGMENT_VALUE_ID].ToString();
                                gL_SEGMENT_VALUES.MODIFIED_BY = this.LoggedUserName;
                                gL_SEGMENT_VALUES.MODIFIED_DATE = DateTime.Today;
                                tmpChildEntity.Add(new Tuple<object, string>(gL_SEGMENT_VALUES, FINAppConstants.Update));
                            }
                            else
                            {
                                gL_SEGMENT_VALUES.SEGMENT_VALUE_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.GL_006_D.ToString(), false, true);
                                gL_SEGMENT_VALUES.CREATED_BY = this.LoggedUserName;
                                gL_SEGMENT_VALUES.CREATED_DATE = DateTime.Today;
                                tmpChildEntity.Add(new Tuple<object, string>(gL_SEGMENT_VALUES, FINAppConstants.Add));
                            }
                        }
                    }
                }

                ProReturn = FINSP.GetSPFOR_ERR_MGR_CATEGORY(Master.FormCode, gL_SEGMENTS.SEGMENT_ID.ToUpper(), gL_SEGMENTS.SEGMENT_NAME.ToUpper());
                if (ProReturn != string.Empty)
                {
                    if (ProReturn != "0")
                    {
                        ErrorCollection.Add("SEGMENTS", ProReturn);
                        if (ErrorCollection.Count > 0)
                        {
                            return;
                        }
                    }
                }

                ProReturn_Valdn = FINSP.GetSPFOR_ACTIVE_FROM_END_DATE(Master.FormCode, gL_SEGMENTS.SEGMENT_ID.ToString(), gL_SEGMENTS.EFFECTIVE_START_DT.ToString("dd/MMM/yyyy"), gL_SEGMENTS.EFFECTIVE_END_DT.ToString(), int.Parse(gL_SEGMENTS.ENABLED_FLAG.ToString()));

                if (ProReturn_Valdn != string.Empty)
                {
                    if (ProReturn_Valdn != "0")
                    {

                        ErrorCollection.Add("SEGMENTNAME", ProReturn_Valdn);
                        if (ErrorCollection.Count > 0)
                        {
                            return;
                        }
                    }

                }

                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {

                            ProReturn = FINSP.GetSPFOR_ERR_MGR_SEGMENT(txtSegmentName.Text, gL_SEGMENTS.SEGMENT_ID, txtEffectiveDate.Text, txtEndDate.Text);

                            if (ProReturn != string.Empty)
                            {

                                if (ProReturn != "0")
                                {
                                    ErrorCollection.Add("SEGNAME", ProReturn);
                                    if (ErrorCollection.Count > 0)
                                    {
                                        return;
                                    }
                                }
                            }

                            CommonUtils.SavePCEntity<GL_SEGMENTS, GL_SEGMENT_VALUES>(gL_SEGMENTS, tmpChildEntity, gL_SEGMENT_VALUES);
                            savedBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {

                            ProReturn = FINSP.GetSPFOR_ERR_MGR_SEGMENT(txtSegmentName.Text, gL_SEGMENTS.SEGMENT_ID, txtEffectiveDate.Text, txtEndDate.Text);

                            if (ProReturn != string.Empty)
                            {

                                if (ProReturn != "0")
                                {
                                    ErrorCollection.Add("SEGNAME", ProReturn);
                                    if (ErrorCollection.Count > 0)
                                    {
                                        return;
                                    }
                                }
                            }


                            CommonUtils.SavePCEntity<GL_SEGMENTS, GL_SEGMENT_VALUES>(gL_SEGMENTS, tmpChildEntity, gL_SEGMENT_VALUES, true);
                            savedBool = true;
                            break;
                        }
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("SEG_ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        private void validateEnddate()
        {
            for (int iLoop = 0; iLoop < gvData.Rows.Count; iLoop++)
            {
                TextBox dtpStartDate = (TextBox)gvData.Rows[iLoop].FindControl("dtpStartDate");
                TextBox dtpEndDate = (TextBox)gvData.Rows[iLoop].FindControl("dtpEndDate");
                if (DBMethod.ConvertStringToDate(dtpEndDate.Text.ToString()) != null)
                {
                    if (DBMethod.ConvertStringToDate(txtEffectiveDate.Text.ToString()) > DBMethod.ConvertStringToDate(dtpStartDate.Text.ToString()) && DBMethod.ConvertStringToDate(dtpEndDate.Text.ToString()) > DBMethod.ConvertStringToDate(txtEndDate.Text.ToString()))
                    {
                        ErrorCollection.Add("Invalid date", "Details start and end Date range should not exceed  master's start and end date range");
                        return;
                    }
                }
            }

        }


        private void BindGrid(DataTable dtData)
        {
            bol_rowVisiable = false;
            Session[FINSessionConstants.GridData] = dtData;
            DataTable dt_tmp = dtData.Copy();
            if (dt_tmp.Rows.Count == 0)
            {
                DataRow dr = dt_tmp.NewRow();
                dr[0] = "0";
                dr["ENABLED_FLAG"] = "FALSE";
                dt_tmp.Rows.Add(dr);
                bol_rowVisiable = true;
            }
            gvData.DataSource = dt_tmp;
            gvData.DataBind();
            GridViewRow gvr = gvData.FooterRow;
            FillFooterGridCombo(gvr);
        }
        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            if (chkdependent.Checked)
            {
                DropDownList ddl_ParentSegmentValue = (DropDownList)tmpgvr.FindControl("ddlParentSegmentValue");
                FIN.BLL.GL.Segments_BLL.GetSegmentvaluesBaseDSegment(ref ddl_ParentSegmentValue, ddlParentSegment.SelectedValue.ToString());
                if (gvData.EditIndex >= 0 && tmpgvr.RowType != DataControlRowType.Footer)
                {
                    ddl_ParentSegmentValue.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.PARENT_SEGMENT_VALUE_ID].ToString();
                }
            }
        }


        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = -1;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("SEG_Row_cnl", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (bol_rowVisiable)
                {
                    e.Row.Visible = false;
                }
                else
                {
                    CheckBox chkact = (CheckBox)e.Row.FindControl("chkact");

                    if (gvData.DataKeys[e.Row.RowIndex].Values["ENABLED_FLAG"] != null)
                    {
                        if (gvData.DataKeys[e.Row.RowIndex].Values["ENABLED_FLAG"].ToString() == "1")
                        {
                            chkact.Checked = true;
                        }
                        else
                        {
                            chkact.Checked = false;
                        }
                    }
                }

                if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                {
                    e.Row.Visible = false;
                }

            }
        }


        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }

                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    else
                    {
                        dtGridData.Rows.Add(drList);
                        BindGrid(dtGridData);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("SEG_Row_Cmd", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }



        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {

            System.Collections.SortedList slControls = new System.Collections.SortedList();


            TextBox txtSegval = gvr.FindControl("txtSegval") as TextBox;
            TextBox dtpStartDate = gvr.FindControl("dtpStartDate") as TextBox;
            TextBox dtpEndDate = gvr.FindControl("dtpEndDate") as TextBox;
            TextBox txtSegvalAR = gvr.FindControl("txtSegvalAR") as TextBox;
            CheckBox chkact = gvr.FindControl("chkact") as CheckBox;
            DropDownList ddl_ParentSegmentValue = (DropDownList)gvr.FindControl("ddlParentSegmentValue");
            
            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList[FINColumnConstants.PK_ID] = "0";
                drList[FINColumnConstants.SEGMENT_VALUE_ID] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }

            slControls[0] = txtSegval;
            slControls[1] = dtpStartDate;
            slControls[2] = dtpStartDate;
            slControls[3] = dtpEndDate;

            if (chkdependent.Checked)
            {
                slControls[4] = ddl_ParentSegmentValue;
            }



            ErrorCollection.Clear();
            string strCtrlTypes = "TextBox~TextBox~DateTime~DateRangeValidate";
            if (chkdependent.Checked)
            {
                strCtrlTypes += "~" + FINAppConstants.DROP_DOWN_LIST;
            }

            string strMessage = "Segment value ~ Effective Date ~ Effective Date ~ End Date";

            if (chkdependent.Checked)
            {
                strMessage += " ~ Parent Segment Value ";
            }

            EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

            if (EmptyErrorCollection.Count > 0)
            {
                ErrorCollection = EmptyErrorCollection;
                return drList;
            }


            //DateTime PSGDate = DBMethod.ConvertStringToDate(dtpPeriodStartDateGrid.Text);
            //if (dtpPeriodEndDateGrid.Text.ToString().Length > 0)
            //{
            //    DateTime PEGDate = DBMethod.ConvertStringToDate(dtpPeriodEndDateGrid.Text);

            //    if ((PSGDate - PEGDate).TotalDays > 0)
            //    {
            //        ErrorCollection.Add("FromTODate", "From Date Must Be Greater then To Date");
            //        return drList;
            //    }

            //}

            //string strCondition = "VALUE_KEY_ID='" + ddlFacilityCode.Text.Trim().ToUpper() + "'";
            //strMessage = iAcademeMessageConstatns.RecordAlreadyExists;
            //ErrorCollection = UserUtility.DataDuplication(dt_tmp, strCondition, strMessage);
            //if (ErrorCollection.Count > 0)
            //{

            //    return drList;
            //}

            drList[FINColumnConstants.SEGMENT_VALUE] = txtSegval.Text;

            if (txtSegvalAR.Text != null)
            {
                drList[FINColumnConstants.SEGMENT_VALUE_OL] = txtSegvalAR.Text;
            }
            if (chkdependent.Checked)
            {
                drList[FINColumnConstants.PARENT_SEGMENT_VALUE_ID] = ddl_ParentSegmentValue.SelectedValue;
                drList["PARENT_SEGMENT_VALUE"] = ddl_ParentSegmentValue.SelectedItem.Text.ToString();
            }
            else
            {
                drList[FINColumnConstants.PARENT_SEGMENT_VALUE_ID] = null;
                drList["PARENT_SEGMENT_VALUE"] = null;
            }

            if (dtpStartDate.Text.ToString().Length > 0)
            {
                drList[FINColumnConstants.EFFECTIVE_START_DT] = DBMethod.ConvertStringToDate(dtpStartDate.Text.ToString());
                if(DBMethod.ConvertStringToDate(txtEffectiveDate.Text.ToString()) > DBMethod.ConvertStringToDate(dtpStartDate.Text.ToString()))
                {
                    ErrorCollection.Add("Invalid date", "Details Start Date  should not less Than  master's start date.");
                    return drList;
                }
            }

            if (dtpEndDate.Text.ToString().Length > 0)
            {

                drList[FINColumnConstants.EFFECTIVE_END_DT] = DBMethod.ConvertStringToDate(dtpEndDate.Text.ToString());

                if (DBMethod.ConvertStringToDate(dtpEndDate.Text.ToString()) > DBMethod.ConvertStringToDate(txtEndDate.Text.ToString()))
                {
                    ErrorCollection.Add("Invalid date", "Details end Date  should not Grater Than  master's end date.");
                    return drList;
                }

            }
            else
            {
                drList[FINColumnConstants.EFFECTIVE_END_DT] = DBNull.Value;
            }
            if (chkact.Checked)
            {
                drList[FINColumnConstants.ENABLED_FLAG] = "TRUE";
            }
            else
            {
                drList[FINColumnConstants.ENABLED_FLAG] = "FALSE";
            }



            drList[FINColumnConstants.DELETED] = FINAppConstants.N;

            return drList;
        }

        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                //else if (FacilityMasterBLL.ErrorCollection.Count > 0)
                //{
                //    ErrorCollection = FacilityMasterBLL.ErrorCollection;
                //    return;
                //}
                else
                {
                    gvData.EditIndex = -1;
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("SEG_Row_upd", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row delete mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewDeleteEventArgs indicates which row's delete button was clicked. </param>


        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                dtGridData.Rows[e.RowIndex][FINColumnConstants.DELETED] = FINAppConstants.Y;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("SEG_Row_del", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Row_Edt", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("SEG_Row_crt", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }



        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>


        #endregion




        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {

                    gL_SEGMENT_VALUES.PK_ID = Convert.ToInt32(dtGridData.Rows[iLoop]["PK_ID"].ToString());
                    DBMethod.DeleteEntity<GL_SEGMENT_VALUES>(gL_SEGMENT_VALUES);
                }

                gL_SEGMENTS.PK_ID = short.Parse(Master.StrRecordId.ToString());
                DBMethod.DeleteEntity<GL_SEGMENTS>(gL_SEGMENTS);
                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATADELETED);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Seg_btndel", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }



        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session[FINSessionConstants.GridData], "Segment Details");

                if (ErrorCollection.Count > 0)
                {
                    return;
                }



                AssignToBE();
                //validateEnddate();
                //if (ErrorCollection.Count > 0)
                //{
                //    return;
                //}
               
                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);
                }


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void chkdependent_CheckedChanged(object sender, EventArgs e)
        {
            fn_DependentChange();
        }
        private void fn_DependentChange()
        {
            if (chkdependent.Checked)
            {
                fillParentSegment();                
                ddlParentSegment.Enabled = true;
                gvData.Columns[0].Visible = true;
            }
            else
            {
                ddlParentSegment.Enabled = false;
                gvData.Columns[0].Visible = false;
            }
        }

        protected void ddlParentSegment_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow gvr = gvData.FooterRow;
            FillFooterGridCombo(gvr);
        }


    }
}