﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="OrganisationEntry.aspx.cs" Inherits="FIN.Client.GL.OrganisationEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1000px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="lblInternalName">
                Internal Name
            </div>
            <div class="divtxtBox LNOrient" style="width: 250px">
                <asp:TextBox ID="txtInternalName" MaxLength="250" TabIndex="1" CssClass="validate[required] RequiredField txtBox"
                    runat="server"></asp:TextBox>
            </div>
            <div class="colspace" style="float: left;">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 170px" id="lblExternalName">
                External Name
            </div>
            <div class="divtxtBox LNOrient" style="width: 250px">
                <asp:TextBox ID="txtExternalName" MaxLength="250" CssClass="validate[required] RequiredField txtBox"
                    TabIndex="2" runat="server" OnTextChanged="txtExternalName_TextChanged"></asp:TextBox>
            </div>
            <div class="colspace" style="float: left;">
                &nbsp</div>
            <%--                   <div class="adminFormFieldHeading">
                         <asp:Label ID="lblLogo" runat="server" Width = "50px" CssClass="DisplayFont lblBox" Text="Logo"></asp:Label>
                    </div>
               <div class="colspace" style="float: left;">
                &nbsp</div>--%>
            <div>
                <asp:ImageButton ID="img_Photo" Width="70px" Height="40px" ImageUrl="~/Images/UploadLogo.jpg"
                    runat="server" OnClick="img_Photo_Click" ToolTip="Click to select Logo" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="lblAddress1">
                Address 1
            </div>
            <div class="divtxtBox LNOrient" style="width: 250px">
                <asp:TextBox ID="txtAddress1" MaxLength="250" CssClass="validate[required] RequiredField txtBox"
                    TabIndex="3" runat="server"></asp:TextBox>
            </div>
            <div class="colspace" style="float: left;">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 170px" id="lblAddress2">
                Address 2
            </div>
            <div class="divtxtBox LNOrient" style="width: 250px">
                <asp:TextBox ID="txtAddress2" MaxLength="250" CssClass="txtBox" TabIndex="4" runat="server"
                    OnTextChanged="txtAddress2_TextChanged"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="lblAddress3">
                Address 3
            </div>
            <div class="divtxtBox LNOrient" style="width: 250px">
                <asp:TextBox ID="txtAddress3" MaxLength="250" CssClass="txtBox" TabIndex="5" runat="server"></asp:TextBox>
            </div>
            <div class="colspace" style="float: left;">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 170px" id="lblAddress4">
                Address 4
            </div>
            <div class="divtxtBox LNOrient" style="width: 250px">
                <asp:TextBox ID="txtAddress4" CssClass="txtBox" MaxLength="250" TabIndex="6" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="lblCountry">
                Country
            </div>
            <div class="divtxtBox LNOrient" style="width: 250px">
                <asp:DropDownList ID="ddlCountry" TabIndex="7" CssClass="validate[required] RequiredField ddlStype"
                    runat="server" Width="253px" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div class="colspace" style="float: left;">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 170px" id="lblCompanyShortName">
                Company Short Name
            </div>
            <div class="divtxtBox LNOrient" style="width: 250px">
                <asp:TextBox ID="txtCompshortname" TabIndex="8" MaxLength="10" CssClass="validate[required] RequiredField txtBox"
                    runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="lblGlobalSegmentBalancingSegment">
                Global Segment Mandatory
            </div>
            <div class="divtxtBox LNOrient" style="width: 250px">
                <asp:RadioButtonList ID="rblYN" runat="server" TabIndex="9" CssClass="lblBox" RepeatDirection="Horizontal"
                    AutoPostBack="True" OnSelectedIndexChanged="rblYN_SelectedIndexChanged">
                    <asp:ListItem Selected="True" Value="1">Yes</asp:ListItem>
                    <asp:ListItem Value="0">No</asp:ListItem>
                </asp:RadioButtonList>
            </div>
            <div class="colspace" style="float: left;">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 170px" id="lblSegmentType">
                Dependent Segment
            </div>
            <div class="divChkbox LNOrient" style="width: 250px">
                <asp:CheckBox ID="chkDependent" runat="server" Checked="false" TabIndex="10" AutoPostBack="True"
                    OnCheckedChanged="chkDependent_CheckedChanged" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="lblGlobalSegmentName">
                Global Segment
            </div>
            <div class="divtxtBox LNOrient" style="width: 250px">
                <asp:DropDownList ID="ddlGlobseg" MaxLength="50" TabIndex="11" CssClass="validate[required] RequiredField ddlStype"
                    runat="server" Width="250px">
                </asp:DropDownList>
            </div>
            <div class="colspace" style="float: left;">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 170px" id="lblInternalNameAR">
                Internal Name(Arabic)
            </div>
            <div class="divtxtBox LNOrient" style="width: 250px">
                <asp:TextBox ID="txtInternalNameAR" MaxLength="240" TabIndex="12" CssClass="txtBox_ol"
                    runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 145px" id="lblActive">
                Active
            </div>
            <div class="divtxtBox LNOrient" style="width: 250px">
                <asp:CheckBox ID="chkActive" TabIndex="13" runat="server" Checked="true" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="LNOrient">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                DataKeyNames="ACCT_STRUCT_ID,CURRENCY_ID,CAL_ID,COMP_DTL_ID,PK_ID,DELETED" OnRowCancelingEdit="gvData_RowCancelingEdit"
                OnRowCommand="gvData_RowCommand" OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound"
                OnRowDeleting="gvData_RowDeleting" OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating"
                ShowFooter="True">
                <Columns>
                    <asp:TemplateField HeaderText="Base Currency" FooterStyle-Width="280px">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlBasecurrency" runat="server" CssClass="EntryFont RequiredField ddlStype"
                                Width="98%" TabIndex="14">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlBasecurrency" runat="server" CssClass="EntryFont RequiredField ddlStype"
                                Width="98%" TabIndex="14">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblBasecurrency" CssClass="adminFormFieldHeading" Width="98%" runat="server"
                                Text='<%# Eval("CURRENCY_NAME") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Accounting Structure" FooterStyle-Width="280px" ItemStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <asp:Label ID="lblAcctStruct" runat="server" Width="100%" Text='<%# Eval("ACCT_STRUCT_NAME") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlAcctStruct" Width="100%" runat="server" CssClass="RequiredField ddlStype"
                                TabIndex="15">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlAcctStruct" Width="100%" runat="server" CssClass="RequiredField ddlStype"
                                TabIndex="15">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Accounting Calendar" ItemStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <asp:Label ID="lblAcctcal" runat="server" Width="100%" Text='<%# Eval("CAL_DESC") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlAcctcal" runat="server" Width="100%" CssClass="RequiredField ddlStype"
                                TabIndex="16">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlAcctcal" runat="server" Width="100%" CssClass="RequiredField ddlStype"
                                TabIndex="16">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Effective Date">
                        <EditItemTemplate>
                            <asp:TextBox ID="dtpStartDate" runat="server" MaxLength="12" Text='<%# Eval("EFFECTIVE_START_DT","{0:dd/MM/yyyy}") %>'
                                TabIndex="17" Onkeypress="return DateKeyCheck(event,this);" CssClass="EntryFont validate[custom[ReqDateDDMMYYY]] RequiredField txtBox"
                                Width="100px"></asp:TextBox>
                            <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calExtender1" TargetControlID="dtpStartDate" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="dtpStartDate" MaxLength="12" Width="100px" runat="server" Onkeypress="return DateKeyCheck(event,this);"
                                TabIndex="17" CssClass="EntryFont validate[custom[ReqDateDDMMYYY]] RequiredField txtBox"></asp:TextBox>
                            <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calExtender1" TargetControlID="dtpStartDate" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblDate" runat="server" Text='<%#Eval("EFFECTIVE_START_DT","{0:dd/MM/yyyy}") %>'
                                Width="98%"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="End Date">
                        <EditItemTemplate>
                            <asp:TextBox ID="dtpEndDate" runat="server" CssClass="EntryFont  txtBox" TabIndex="18"
                                Width="100px" Text='<%#  Eval("EFFECTIVE_END_DT","{0:dd/MM/yyyy}") %>'></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender10" runat="server" Format="dd/MM/yyyy"
                                TargetControlID="dtpEndDate">
                            </cc2:CalendarExtender>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="dtpEndDate" runat="server" CssClass="EntryFont  txtBox" TabIndex="18"
                                Width="100px"></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender10" runat="server" Format="dd/MM/yyyy"
                                TargetControlID="dtpEndDate">
                            </cc2:CalendarExtender>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblEndDate" runat="server" Text='<%# Eval("EFFECTIVE_END_DT","{0:dd/MM/yyyy}") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Active">
                        <EditItemTemplate>
                            <asp:CheckBox ID="chkact" runat="server" Checked='<%# Convert.ToBoolean(Eval("ENABLED_FLAG")) %>'
                                TabIndex="19" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:CheckBox ID="chkact" runat="server" Checked="true" TabIndex="19" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkact" runat="server" Checked='<%# Convert.ToBoolean(Eval("ENABLED_FLAG")) %>'
                                Enabled="false" TabIndex="19" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Year">
                        <ItemTemplate>
                            <asp:Button ID="btnYR" runat="server" CssClass="button" Text="Year" TabIndex="20"
                                Enabled="false" CommandName="Select" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:Button ID="btnYR" runat="server" CssClass="button" Text="Year" TabIndex="20"
                                CommandName="Select" OnClick="btnYR_Click" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:Button ID="btnYR" runat="server" CssClass="button" Text="Year" TabIndex="20"
                                CommandName="Select" OnClick="btnYR_Click" />
                        </FooterTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Period" Visible="false">
                        <ItemTemplate>
                            <asp:Button ID="btnPeriod" runat="server" CssClass="button" Text="Period" TabIndex="21"
                                Enabled="false" CommandName="Select" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:Button ID="btnPeriod" runat="server" CssClass="button" Text="Period" TabIndex="21"
                                CommandName="Select" OnClick="btnPeriod_Click" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:Button ID="btnPeriod" runat="server" CssClass="button" Text="Period" TabIndex="21"
                                CommandName="Select" OnClick="btnPeriod_Click" />
                        </FooterTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" runat="server" AlternateText="Edit" CausesValidation="false"
                                ToolTip="edit" TabIndex="23" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                            <asp:ImageButton ID="ibtnDelete" runat="server" AlternateText="Delete" CausesValidation="false"
                                ToolTip="delete" TabIndex="24" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibtnUpdate" runat="server" AlternateText="Update" CommandName="Update"
                                ToolTip="update" TabIndex="25" Height="20px" ImageUrl="~/Images/Update.png" />
                            <asp:ImageButton ID="ibtnCancel" runat="server" AlternateText="Cancel" CausesValidation="false"
                                ToolTip="cancel" TabIndex="26" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="ibtnInsert" runat="server" AlternateText="Add" CommandName="FooterInsert"
                                TabIndex="22" ImageUrl="~/Images/Add.jpg" />
                        </FooterTemplate>
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div>
            <asp:HiddenField ID="btnYR" runat="server" />
            <cc2:ModalPopupExtender ID="mpeCalDet" runat="server" TargetControlID="btnYR" PopupControlID="Panel1"
                BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="Panel1" runat="server" Height="300px" Width="300px" BackColor="White"
                ScrollBars="Vertical">
                <div class="ConfirmForm" style="overflow: auto">
                    <div class="divClear_10">
                    </div>
                    <table width="100%">
                        <tr>
                            <td colspan="4" class="PopUpHeader">
                                <div class="lblBox" id="divlblAccCalYear">
                                    Accounting Calendar Year
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <br />
                            </td>
                        </tr>
                        <tr class="divFormcontainer">
                            <td colspan="4" style="margin-left: 40px">
                                <asp:GridView ID="gvCalDet" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                                    DataKeyNames="PK_ID,CAL_ID,CAL_DTL_ID,SELECTED" OnRowDataBound="gvCalDet_RowDataBound">
                                    <Columns>
                                        <asp:BoundField DataField="CAL_ACCT_YEAR" HeaderText="Accounting Year" />
                                        <asp:TemplateField HeaderText="Select">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkSelected" runat="server" Checked='<%# Convert.ToBoolean(Eval("SELECTED")) %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="divClear_10">
                                </div>
                                <asp:Button runat="server" CssClass="DisplayFont button" ID="Button1" Text="Ok" Width="60px"
                                    OnClick="Button1_Click" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="DisplayFont button" ID="btnCLosePopUp" Text="Close"
                                    Width="60px" />
                                <asp:HiddenField ID="HiddenField4" runat="server" />
                                <asp:HiddenField ID="HiddenField5" runat="server" />
                                <asp:HiddenField ID="hf_Acctcal" runat="server" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" OnClick="btnSave_Click1"
                            TabIndex="23" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="24" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="25" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="26" />
                    </td>
                </tr>
            </table>
        </div>
        <div runat="server" id="divCalPeriods" Visible="false">
            <asp:HiddenField ID="btnGeneratePeriod" runat="server" />
            <asp:HiddenField ID="hfPeriodDetId" runat="server" />
            <cc2:ModalPopupExtender ID="mpeCalPeriod" runat="server" TargetControlID="btnGeneratePeriod"
                PopupControlID="pnlPeriod" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlPeriod" runat="server" Height="450px" Width="90%" BackColor="White"
                ScrollBars="Vertical">
                <div class="ConfirmForm" style="overflow: auto">
                    <table width="100%">
                        <tr>
                            <td colspan="4" class="PopUpHeader">
                                <div class="lblBox" id="div1">
                                    Accounting Calendar Periods
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="right">
                                <div class="lblBox OppLNOrient" id="lblCalendarYear">
                                    Calendar Year
                                </div>
                            </td>
                            <td colspan="2">
                                <div class="divtxtBox" style="float: left; width: 253px">
                                    <asp:DropDownList ID="ddlCalendarYear" MaxLength="50" TabIndex="8" CssClass="validate[required] RequiredField  NRddlStype"
                                        Width="200px" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlCalendarYear_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </div>
                            </td>
                        </tr>
                        <tr class="divFormcontainer">
                            <td colspan="4" align="center">
                                <asp:GridView ID="gvperiods" runat="server" AutoGenerateColumns="False" CssClass="DisplayFont  Grid"
                                    DataKeyNames="PERIOD_ID,LOOKUP_ID,CAL_DTL_ID" OnRowDataBound="gvperiod_RowDataBound"
                                    OnRowCommand="gvperiodData_RowCommand" Width="90%">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Period Name">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="TextBox1" Text='<%# Eval("PERIOD_NAME") %>' runat="server"></asp:TextBox>
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:TextBox ID="TextBox1" Text='<%# Eval("PERIOD_NAME") %>' runat="server"></asp:TextBox>
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblPeriodName" runat="server" Text='<%# Eval("PERIOD_NAME") %>' Width="80px"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Period Type">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtPeriodtype" Text='<%# Eval("PERIOD_TYPE") %>' runat="server"></asp:TextBox>
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:TextBox ID="txtPeriodtype" Text='<%# Eval("PERIOD_TYPE") %>' runat="server"></asp:TextBox>
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblPeriodtype" runat="server" Text='<%# Eval("PERIOD_TYPE") %>' Width="80px"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="From Date">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="dtfromdate" runat="server" CssClass="txtBox" Text='' MaxLength="10"></asp:TextBox>
                                                <cc2:CalendarExtender ID="CalendarExtfromdate" runat="server" Format="dd/MM/yyyy"
                                                    TargetControlID="dtfromdate">
                                                </cc2:CalendarExtender>
                                                <cc2:FilteredTextBoxExtender ID="Fillfromdte" runat="server" ValidChars="/" FilterType="Numbers,Custom"
                                                    TargetControlID="dtfromdate" />
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:TextBox ID="dtfromdate" runat="server" CssClass="txtBox" Text='' MaxLength="10"></asp:TextBox>
                                                <cc2:CalendarExtender ID="CalendarExtenderfromdate" runat="server" Format="dd/MM/yyyy"
                                                    TargetControlID="dtfromdate">
                                                </cc2:CalendarExtender>
                                                <cc2:FilteredTextBoxExtender ID="Filteredfromdate" runat="server" ValidChars="/"
                                                    FilterType="Numbers,Custom" TargetControlID="dtfromdate" />
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblQbeExamDate" runat="server" Text='<%# Eval("PERIOD_FROM_DT","{0:dd/MM/yyyy}") %>'
                                                    Width="100px"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="To Date">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="dttodate" runat="server" CssClass="EntryFont RequiredField txtBox"
                                                    Text='' MaxLength="10"></asp:TextBox>
                                                <cc2:CalendarExtender ID="CalendarExtendertodate" runat="server" Format="dd/MM/yyyy"
                                                    TargetControlID="dttodate">
                                                </cc2:CalendarExtender>
                                                <cc2:FilteredTextBoxExtender ID="Filteredtodate" runat="server" ValidChars="/" FilterType="Numbers,Custom"
                                                    TargetControlID="dttodate" />
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:TextBox ID="dttodate" runat="server" CssClass="txtBox" Text='' MaxLength="10"></asp:TextBox>
                                                <cc2:CalendarExtender ID="CalendarExttodate" runat="server" Format="dd/MM/yyyy" TargetControlID="dttodate">
                                                </cc2:CalendarExtender>
                                                <cc2:FilteredTextBoxExtender ID="FilteredtoDate" runat="server" ValidChars="/" FilterType="Numbers,Custom"
                                                    TargetControlID="dttodate" />
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lbltoDate" runat="server" Text='<%# Eval("PERIOD_TO_DT","{0:dd/MM/yyyy}") %>'
                                                    Width="100px"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Period Status">
                                            <EditItemTemplate>
                                                <asp:DropDownList ID="ddlStatus" runat="server" CssClass="NRddlStype" Width="80px">
                                                </asp:DropDownList>
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:DropDownList ID="ddlStatus" runat="server" CssClass="NRddlStype" Width="80px">
                                                </asp:DropDownList>
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:DropDownList ID="ddlStatus" runat="server" CssClass="NRddlStype" Width="80px"
                                                    AutoPostBack="true" OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                    <HeaderStyle CssClass="GridHeader" />
                                    <AlternatingRowStyle CssClass="GrdAltRow" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Button runat="server" CssClass="DisplayFont button" ID="btnPeriodOk" Text="Ok"
                                    Width="60px" OnClick="btnPeriodOk_Click" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="DisplayFont button" ID="btnPeriodClose" Text="Close"
                                    Width="60px" />
                                <asp:HiddenField ID="hfCalYear" runat="server" />
                                <asp:HiddenField ID="HiddenField2" runat="server" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table width="100%">
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
        <div>
            <asp:HiddenField ID="hfAttachments" runat="server" />
            <cc2:ModalPopupExtender ID="meUpload" runat="server" TargetControlID="hfAttachments"
                BackgroundCssClass="ConfirmBackground" PopupControlID="pnlAttachments">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlAttachments" runat="server" Width="500px" BackColor="#99ccff" BorderWidth="1">
                <table width="500px">
                    <tr>
                        <td align="right">
                            <asp:Button ID="btnCloseUpload" runat="server" Text="Close" CssClass="DisplayFont button"
                                OnClick="btnCloseUpload_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <iframe id="ifrmupload" style="float: left; width: 486px;" name="uploadfrm" class="uploadfrm"
                                frameborder="0" src="../UploadFile/PhotoUpload.aspx"></iframe>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/SSM/SSMChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

    </script>
</asp:Content>
