﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="BudgetVarianceEntry.aspx.cs" Inherits="FIN.Client.GL.BudgetVarianceEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 870px" id="divMainContainer">
          <div class="divRowContainer" style="border: 4; border-color: rgb(216,216,216); height: 30px;
            background-color: rgb(216,216,216);">
            <div align="center" class="lblBox LNOrient" style="  width: 780px; text-align: center;"
                id="lblGlobalSegment">
               Budget Variance
            </div>
            <div class="lblBox LNOrient" align="left" style="  width: 70px" id="Div2">
               <%--<asp:ImageButton ID="btnExport" runat="server" ImageUrl="~/Images/iconExport.png" CommandName="PDF"
                            AlternateText="Export To Pdf" ToolTip="Export To Pdf"  
                    Width="28px" onclick="btnExport_Click" />--%>&nbsp;
                    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../Images/show-report-icon.png" TabIndex="12"
                    Width="35px" Height="25px" OnClick="btnSave_Click" />
            </div>
        </div>
        <div class="divClear_10">
        </div> <div class="divClear_10">
        </div> <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 150px" id="Div1">
                Budget Year
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:DropDownList ID="ddlBudgerYr" runat="server" CssClass="ddlStype validate[required] RequiredField "
                    AutoPostBack="true" TabIndex="1" OnSelectedIndexChanged="ddlBudgerYr_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
             <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="  width: 150px" id="lblAdjPeriod">
                Account Group
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:DropDownList ID="ddlAccGroup" runat="server" CssClass="ddlStype validate[]  "
                    AutoPostBack="true" TabIndex="2" OnSelectedIndexChanged="ddlAccGroup_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divClear_10">
        </div>
        <div class="LNOrient">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="DisplayFont  Grid" OnRowDataBound="gvData_RowDataBound"
                DataKeyNames="BUDGET_DTL_ID" ShowFooter="true" EmptyDataText="No Record to Found"
                Width="100%">
                <Columns>
                    <asp:BoundField DataField="Account_Code" HeaderText="Account No" />
                    <asp:BoundField DataField="acct_code_desc" HeaderText="Account Description" />
                    <asp:BoundField DataField="Budget_Amount"   ItemStyle-HorizontalAlign="Right"   HeaderText="Budget Amount" />
                    <asp:BoundField DataField="Previous_Amount" ItemStyle-HorizontalAlign="Right" HeaderText="Actual Amount" />
                    <asp:BoundField DataField="Difference" ItemStyle-HorizontalAlign="Right"  HeaderText="Variance" />
                    <asp:TemplateField HeaderText="Details">
                        <ItemTemplate>
                            <asp:Button ID="btnDetails" runat="server" CssClass="button" Text="Details" TabIndex="3"
                                CommandName="Select" OnClick="btnDetails_Click" />
                        </ItemTemplate>                                      
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" TabIndex="16" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="17" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="18" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="19" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="dvPeriods">
            <asp:HiddenField ID="btnPeriods1" runat="server" />
            <cc2:ModalPopupExtender ID="mpePeriods" runat="server" TargetControlID="btnPeriods1"
                PopupControlID="pnlPeriods" CancelControlID="btnPeriodOk" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlPeriods" runat="server" BackColor="Violet" Width="45%" Height="85%"
                ScrollBars="Auto">
                <div class="ConfirmForm">
                    <div class="divFormcontainer" style="width: 100%;" id="div4">
                        <div class="divRowContainer" style="border: 4; border-color: rgb(216,216,216); height: 28px;
                            background-color: rgb(216,216,216);">
                            <div align="center" class="lblBox LNOrient" style="  width: 100%; text-align: left;
                                font-weight: bold;" id="Div5">
                                Monthly Budget Amount
                            </div>
                        </div>
                        <table width="100%" >
                            <tr class="ConfirmHeading">
                                <td colspan="2">
                                    <asp:Label ID="Label15" runat="server" Text="Current Year" Style="text-align: center;"
                                        Width="175px"></asp:Label>
                                </td>
                            </tr>
                            <tr class="ConfirmHeading">
                                <td>
                                    <asp:Label ID="lblPeriod1" runat="server" Text="Period 1" Width="75px"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtPeriodPop1" runat="server" TabIndex="1" CssClass="EntryFont RequiredField txtBox" ReadOnly="true"
                                        Width="100px"></asp:TextBox>
                                    <cc2:FilteredTextBoxExtender ID="a1FilteredTextBoxExtender20" runat="server" FilterType="Numbers,Custom"
                                        ValidChars="." TargetControlID="txtPeriodPop1" />
                                </td>
                            </tr>
                            <tr class="ConfirmHeading">
                                <td>
                                    <asp:Label ID="lblPeriod2" runat="server" Text="Period 2" Width="75px"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtPeriodPop2" runat="server" TabIndex="2" CssClass="EntryFont RequiredField txtBox" ReadOnly="true"
                                        Width="100px"></asp:TextBox>
                                    <cc2:FilteredTextBoxExtender ID="a3FilteredTextBoxExtender21" runat="server" FilterType="Numbers,Custom"
                                        ValidChars="." TargetControlID="txtPeriodPop2" />
                                </td>
                            </tr>
                            <tr class="ConfirmHeading">
                                <td>
                                    <asp:Label ID="lblPeriod3" runat="server" Text="Period 3" Width="75px"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtPeriodPop3" runat="server" TabIndex="3" CssClass="EntryFont RequiredField txtBox" ReadOnly="true"
                                        Width="100px"></asp:TextBox>
                                    <cc2:FilteredTextBoxExtender ID="a5FilteredTextBoxExtender27" runat="server" FilterType="Numbers,Custom"
                                        ValidChars="." TargetControlID="txtPeriodPop3" />
                                </td>
                            </tr>
                            <tr class="ConfirmHeading">
                                <td>
                                    <asp:Label ID="lblPeriod4" runat="server" Text="Period 4" Width="70px"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtPeriodPop4" runat="server" TabIndex="4" CssClass="EntryFont RequiredField txtBox" ReadOnly="true"
                                        Width="100px"></asp:TextBox>
                                    <cc2:FilteredTextBoxExtender ID="a7FilteredTextBoxExtender37" runat="server" FilterType="Numbers,Custom"
                                        ValidChars="." TargetControlID="txtPeriodPop4" />
                                </td>
                            </tr>
                            <tr class="ConfirmHeading">
                                <td>
                                    <asp:Label ID="lblPeriod5" runat="server" Text="Period 5" Width="75px"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtPeriodPop5" runat="server" TabIndex="5" CssClass="EntryFont RequiredField txtBox" ReadOnly="true"
                                        Width="100px"></asp:TextBox>
                                    <cc2:FilteredTextBoxExtender ID="a9FilteredTextBoxExtender47" runat="server" FilterType="Numbers,Custom"
                                        ValidChars="." TargetControlID="txtPeriodPop5" />
                                </td>
                            </tr>
                            <tr class="ConfirmHeading">
                                <td>
                                    <asp:Label ID="lblPeriod6" runat="server" Text="Period 6" Width="75px"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtPeriodPop6" runat="server" TabIndex="6" CssClass="EntryFont RequiredField txtBox" ReadOnly="true"
                                        Width="100px"></asp:TextBox>
                                    <cc2:FilteredTextBoxExtender ID="asFilteredTextBoxExtender57" runat="server" FilterType="Numbers,Custom"
                                        ValidChars="." TargetControlID="txtPeriodPop6" />
                                </td>
                            </tr>
                            <tr class="ConfirmHeading">
                                <td>
                                    <asp:Label ID="lblPeriod7" runat="server" Text="Period 7" Width="75px"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtPeriodPop7" runat="server" TabIndex="7" CssClass="EntryFont RequiredField txtBox" ReadOnly="true"
                                        Width="100px"></asp:TextBox>
                                    <cc2:FilteredTextBoxExtender ID="fFilteredTextBoxExtender67" runat="server" FilterType="Numbers,Custom"
                                        ValidChars="." TargetControlID="txtPeriodPop7" />
                                </td>
                            </tr>
                            <tr class="ConfirmHeading">
                                <td>
                                    <asp:Label ID="lblPeriod8" runat="server" Text="Period 8" Width="75px"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtPeriodPop8" runat="server" TabIndex="8" CssClass="EntryFont RequiredField txtBox" ReadOnly="true"
                                        Width="100px"></asp:TextBox>
                                    <cc2:FilteredTextBoxExtender ID="hFilteredTextBoxExtender77" runat="server" FilterType="Numbers,Custom"
                                        ValidChars="." TargetControlID="txtPeriodPop8" />
                                </td>
                            </tr>
                            <tr class="ConfirmHeading">
                                <td>
                                    <asp:Label ID="lblPeriod9" runat="server" Text="Period 9" Width="75px"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtPeriodPop9" runat="server" TabIndex="9" CssClass="EntryFont RequiredField txtBox" ReadOnly="true"
                                        Width="100px"></asp:TextBox>
                                    <cc2:FilteredTextBoxExtender ID="kFilteredTextBoxExtender87" runat="server" FilterType="Numbers,Custom"
                                        ValidChars="." TargetControlID="txtPeriodPop9" />
                                </td>
                            </tr>
                            <tr class="ConfirmHeading">
                                <td>
                                    <asp:Label ID="lblPeriod10" runat="server" Text="Period 10" Width="75px"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtPeriodPop10" runat="server" TabIndex="10" CssClass="EntryFont RequiredField txtBox" ReadOnly="true"
                                        Width="100px"></asp:TextBox>
                                    <cc2:FilteredTextBoxExtender ID="zFilteredTextBoxExtender97" runat="server" FilterType="Numbers,Custom"
                                        ValidChars="." TargetControlID="txtPeriodPop10" />
                                </td>
                            </tr>
                            <tr class="ConfirmHeading">
                                <td>
                                    <asp:Label ID="lblPeriod11" runat="server" Text="Period 11" Width="70px"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtPeriodPop11" runat="server" TabIndex="11" CssClass="EntryFont RequiredField txtBox" ReadOnly="true"
                                        Width="100px"></asp:TextBox>
                                    <cc2:FilteredTextBoxExtender ID="cFilteredTextBoxExtender31" runat="server" FilterType="Numbers,Custom"
                                        ValidChars="." TargetControlID="txtPeriodPop11" />
                                </td>
                            </tr>
                            <tr class="ConfirmHeading">
                                <td>
                                    <asp:Label ID="lblPeriod12" runat="server" Text="Period 12" Width="70px"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtPeriodPop12" runat="server" TabIndex="12" CssClass="EntryFont RequiredField txtBox" ReadOnly="true"
                                        Width="100px"></asp:TextBox>
                                    <cc2:FilteredTextBoxExtender ID="bFilteredTextBoxExtender32" runat="server" FilterType="Numbers,Custom"
                                        ValidChars="." TargetControlID="txtPeriodPop12" />
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="4">
                                    <asp:Button runat="server" CssClass="button" ID="btnPeriodOk" Text="Close" Width="160px"
                                        TabIndex="13" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/GL/GLChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
