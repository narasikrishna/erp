﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="FundTransferEntry.aspx.cs" Inherits="FIN.Client.GL.FundTransfer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1000px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 150px" id="lblOrganisation">
                Organization
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:TextBox ID="txtOrganisation" CssClass="txtBox" runat="server" TabIndex="1"></asp:TextBox>
            </div>
            <div class="lblBox" style="float: left; width: 150px" id="lblDate">
                Date
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:TextBox ID="txtDate" CssClass="txtBox" runat="server" TabIndex="2"></asp:TextBox>
            </div>
            <div class="lblBox" style="float: left; width: 150px" id="lblGlobalSegment">
                Global Segment
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:DropDownList ID="ddlGlobalSegment" runat="server" TabIndex="3">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 150px" id="lblJournalType">
                Journal Type
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:DropDownList ID="ddlJournalType" runat="server" TabIndex="4">
                </asp:DropDownList>
            </div>
            <div class="lblBox" style="float: left; width: 150px" id="lblCurrency">
                Currency
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:TextBox ID="txtCurrency" CssClass="txtBox" runat="server" TabIndex="5"></asp:TextBox>
            </div>
            <div class="lblBox" style="float: left; width: 150px" id="lblExchangeRate">
                Exchange Rate
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:TextBox ID="txtExchangeRate" CssClass="txtBox" runat="server" TabIndex="6"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 150px" id="lblAnnualBudgetAmount">
                Narration
            </div>
            <div class="divtxtBox" style="float: left; width: 750px">
                <asp:TextBox ID="txtAnnualBudgetAmount" CssClass="LongtxtBox" runat="server" 
                    TabIndex="7"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" 
                            TabIndex="7" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" 
                            TabIndex="8" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" 
                            TabIndex="9" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" 
                            TabIndex="10" />
                    </td>
                </tr>
            </table>
        </div>
        <div class="divClear_10">
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/GL/FundTransfer.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });
    </script>
</asp:Content>
