﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.BLL;
using VMVServices.Web;

namespace FIN.Client.GL
{
    public partial class AccountCodes : PageBase
    {
        
        GL_ACCT_CODES gL_ACCT_CODES = new GL_ACCT_CODES();
        GL_ACCT_CODE_SEGMENTS gL_ACCT_CODE_SEGMENTS = new GL_ACCT_CODE_SEGMENTS();
        string ProReturn = null;
        string ProReturn_Valdn = null;
        DataTable dtGridData = new DataTable();
        Boolean bol_rowVisiable;

        Boolean savedBool;
        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();
                    ddlStructureName.Focus();
                    ChangeLanguage();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("GL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        private void ChangeLanguage()
        {
            try
            {
                ErrorCollection.Clear();
                if (VMVServices.Web.Utils.LanguageCode.ToString().Length > 0)
                {
                    Dictionary<string, string> Prop_File_Data;
                    Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/GL_" + Session["Sel_Lng"].ToString() + ".properties"));
                    rbAccType.Items[0].Text = Prop_File_Data["BalanceSheet_P"];
                    rbAccType.Items[1].Text = Prop_File_Data["Profit_/Loss_P"];

                    rbControlAcc.Items[0].Text = Prop_File_Data["Yes_P"];
                    rbControlAcc.Items[1].Text = Prop_File_Data["No_P"];
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("MisReceiptChangeLang", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));


            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
        }

        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>
        /// 

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void FillComboBox()
        {
            ComboFilling.getStructure(ref ddlStructureName);
            FIN.BLL.GL.AccountingGroupDefaultSegment_BLL.fn_getGroupName(ref ddlGroupName,Master.Mode);
            if (rbControlAcc.SelectedValue == "Y")
            {
                Lookup_BLL.GetLookUpValues(ref ddlControlAccountCategory, "CAC");
            }
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();

                FillComboBox();

                EntityData = null;

                dtGridData = FIN.BLL.GL.AccountCodes_BLL.getChildEntityDet(Master.StrRecordId);
                BindGrid(dtGridData);

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    using (IRepository<GL_ACCT_CODES> userCtx = new DataRepository<GL_ACCT_CODES>())
                    {
                        gL_ACCT_CODES = userCtx.Find(r =>
                            (r.ACCT_CODE_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = gL_ACCT_CODES;

                    ddlStructureName.SelectedValue = gL_ACCT_CODES.ACCT_STRUCT_ID.ToString();
                    txtAccountCode.Text = gL_ACCT_CODES.ACCT_CODE.ToString();
                    ddlGroupName.SelectedValue = gL_ACCT_CODES.ACCT_GRP_ID.ToString();
                    txtAccountCodeDescription.Text = gL_ACCT_CODES.ACCT_CODE_DESC.ToString();
                    
                    if (gL_ACCT_CODES.ACCT_CODE_DESC_AR != null)
                    {
                        txtAccountCodeDescriptionAR.Text = gL_ACCT_CODES.ACCT_CODE_DESC_AR.ToString();
                    }
                    else if (gL_ACCT_CODES.ACCT_CODE_DESC_OL != null)
                    {
                        txtAccountCodeDescriptionAR.Text = gL_ACCT_CODES.ACCT_CODE_DESC_OL.ToString();
                    }

                    txtoldAcctcde.Text = gL_ACCT_CODES.ATTRIBUTE10;
                    if (gL_ACCT_CODES.ENABLED_FLAG.ToString() == FINAppConstants.Y)
                    {

                        chkActive.Checked = true;
                    }
                    else
                    {
                        chkActive.Checked = false;
                    }
                    rbAccType.SelectedValue = gL_ACCT_CODES.ACCT_TYPE.ToString();
                    rbControlAcc.SelectedValue = gL_ACCT_CODES.IS_CONTROL_ACCT.ToString();
                    if (gL_ACCT_CODES.IS_CONTROL_ACCT == "Y")
                    {

                        if (gL_ACCT_CODES.CONTROL_ACCT_CATEGROY != null)
                        {
                            ddlControlAccountCategory.SelectedValue = gL_ACCT_CODES.CONTROL_ACCT_CATEGROY.ToString();
                        }
                    }

                    txtEffectiveDate.Text = DBMethod.ConvertDateToString(gL_ACCT_CODES.EFFECTIVE_START_DT.ToString());
                    if (gL_ACCT_CODES.EFFECTIVE_END_DT != null)
                    {
                        txtEndDate.Text = DBMethod.ConvertDateToString(gL_ACCT_CODES.EFFECTIVE_END_DT.ToString());
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    gL_ACCT_CODES = (GL_ACCT_CODES)EntityData;
                }

                gL_ACCT_CODES.ACCT_STRUCT_ID = ddlStructureName.SelectedValue.ToString();
                gL_ACCT_CODES.ACCT_CODE = (txtAccountCode.Text);
                gL_ACCT_CODES.ACCT_GRP_ID = ddlGroupName.SelectedValue.ToString();
                gL_ACCT_CODES.ACCT_CODE_DESC = txtAccountCodeDescription.Text;

                if (txtAccountCodeDescriptionAR.Text != null)
                {
                    gL_ACCT_CODES.ACCT_CODE_DESC_AR = txtAccountCodeDescriptionAR.Text;
                    gL_ACCT_CODES.ACCT_CODE_DESC_OL = txtAccountCodeDescriptionAR.Text;
                }

                gL_ACCT_CODES.ACCT_TYPE = rbAccType.SelectedValue.ToString();
                gL_ACCT_CODES.ATTRIBUTE10 = txtoldAcctcde.Text;
                gL_ACCT_CODES.IS_CONTROL_ACCT = rbControlAcc.SelectedValue.ToString();
                if (rbControlAcc.SelectedValue == "Y")
                {
                    gL_ACCT_CODES.CONTROL_ACCT_CATEGROY = ddlControlAccountCategory.SelectedValue;
                }

                //  gL_ACCT_CODES.ENABLED_FLAG = FINAppConstants.EnabledFlag;

                if (chkActive.Checked)
                {
                    gL_ACCT_CODES.ENABLED_FLAG = FINAppConstants.Y;
                }
                else
                {
                    gL_ACCT_CODES.ENABLED_FLAG = FINAppConstants.N;
                }

                gL_ACCT_CODES.EFFECTIVE_START_DT = DBMethod.ConvertStringToDate(txtEffectiveDate.Text.ToString());
                if (txtEndDate.Text != null && txtEndDate.Text != string.Empty && txtEndDate.Text.ToString().Trim().Length > 0)
                {
                    gL_ACCT_CODES.EFFECTIVE_END_DT = DBMethod.ConvertStringToDate(txtEndDate.Text.ToString());
                }
                else
                {
                    gL_ACCT_CODES.EFFECTIVE_END_DT = null;
                }
                //  gL_ACCT_CODES.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;


                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    gL_ACCT_CODES.MODIFIED_BY = this.LoggedUserName;
                    gL_ACCT_CODES.MODIFIED_DATE = DateTime.Today;

                }
                else
                {
                    gL_ACCT_CODES.ACCT_CODE_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.ACTCODE_M.ToString(), false, true);
                    // gL_ACCT_CODES.PK_ID = DBMethod.GetPrimaryKeyValue(FINSequenceConstant.GL_ACCT_CODES_SEQ);
                    gL_ACCT_CODES.CREATED_BY = this.LoggedUserName;
                    gL_ACCT_CODES.CREATED_DATE = DateTime.Today;


                }

                gL_ACCT_CODES.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, gL_ACCT_CODES.ACCT_CODE_ID);

                gL_ACCT_CODES.ACCT_ORG_ID = VMVServices.Web.Utils.OrganizationID;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                //ErrorCollection = CommonUtils.IsEmptyGrid(dtGridData, "Account Code ");
                //if (ErrorCollection.Count > 0)
                //{
                //    return;
                //}


                var tmpChildEntity = new List<Tuple<object, string>>();
                GL_ACCT_CODE_SEGMENTS gL_ACCT_CODE_SEGMENTS = new GL_ACCT_CODE_SEGMENTS();
                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    gL_ACCT_CODE_SEGMENTS = new GL_ACCT_CODE_SEGMENTS();
                    if (dtGridData.Rows[iLoop][FINColumnConstants.ACCT_CODE_SEG_ID].ToString() != "0")
                    {
                        using (IRepository<GL_ACCT_CODE_SEGMENTS> userCtx = new DataRepository<GL_ACCT_CODE_SEGMENTS>())
                        {
                            gL_ACCT_CODE_SEGMENTS = userCtx.Find(r =>
                                (r.ACCT_CODE_SEG_ID == dtGridData.Rows[iLoop][FINColumnConstants.ACCT_CODE_SEG_ID].ToString())
                                ).SingleOrDefault();
                        }
                    }
                    gL_ACCT_CODE_SEGMENTS.ACCT_CODE_SEGMENT_ID = dtGridData.Rows[iLoop][FINColumnConstants.SEGMENT_ID].ToString();
                    gL_ACCT_CODE_SEGMENTS.ACCT_CODE_ID = gL_ACCT_CODES.ACCT_CODE_ID;
                    //gL_ACCT_CODE_SEGMENTS.ACCT_CODE_SEG_ID = "1";
                    gL_ACCT_CODE_SEGMENTS.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;

                    if (dtGridData.Rows[iLoop][FINColumnConstants.EFFECTIVE_START_DT] != DBNull.Value)
                    {
                        gL_ACCT_CODE_SEGMENTS.EFFECTIVE_START_DT = DateTime.Parse(dtGridData.Rows[iLoop][FINColumnConstants.EFFECTIVE_START_DT].ToString());
                    }

                    if (dtGridData.Rows[iLoop][FINColumnConstants.EFFECTIVE_END_DT] != DBNull.Value)
                    {
                        gL_ACCT_CODE_SEGMENTS.EFFECTIVE_END_DT = DateTime.Parse(dtGridData.Rows[iLoop][FINColumnConstants.EFFECTIVE_END_DT].ToString());
                    }
                    else
                    {
                        gL_ACCT_CODE_SEGMENTS.EFFECTIVE_END_DT = null;
                    }
                    //   gL_ACCT_CODE_SEGMENTS.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                    if (dtGridData.Rows[iLoop][FINColumnConstants.ENABLED_FLAG].ToString().ToUpper() == "TRUE")
                    {
                        gL_ACCT_CODE_SEGMENTS.ENABLED_FLAG = FINAppConstants.Y;
                    }
                    else
                    {
                        gL_ACCT_CODE_SEGMENTS.ENABLED_FLAG = FINAppConstants.N;
                    }

                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        // gL_ACCT_CODE_SEGMENTS.PK_ID = Convert.ToInt32(dtGridData.Rows[iLoop][FINColumnConstants.PK_ID].ToString());
                        // DBMethod.DeleteEntity<GL_ACCT_CODE_SEGMENTS>(gL_ACCT_CODE_SEGMENTS);
                        tmpChildEntity.Add(new Tuple<object, string>(gL_ACCT_CODE_SEGMENTS, "D"));
                    }
                    else
                    {
                        if (dtGridData.Rows[iLoop][FINColumnConstants.ACCT_CODE_SEG_ID].ToString() != "0")
                        {
                            // gL_ACCT_CODE_SEGMENTS.PK_ID = Convert.ToInt32(dtGridData.Rows[iLoop][FINColumnConstants.PK_ID].ToString());
                            gL_ACCT_CODE_SEGMENTS.MODIFIED_BY = this.LoggedUserName;
                            gL_ACCT_CODE_SEGMENTS.MODIFIED_DATE = DateTime.Today;
                            // DBMethod.SaveEntity<GL_ACCT_CODE_SEGMENTS>(gL_ACCT_CODE_SEGMENTS, true);
                            tmpChildEntity.Add(new Tuple<object, string>(gL_ACCT_CODE_SEGMENTS, "U"));

                        }
                        else
                        {
                            // gL_ACCT_CODE_SEGMENTS.PK_ID = DBMethod.GetPrimaryKeyValue(FINSequenceConstant.GL_ACCT_CODES_SEQ);
                            gL_ACCT_CODE_SEGMENTS.ACCT_CODE_SEG_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.ACTCODE_D.ToString(), false, true);
                            gL_ACCT_CODE_SEGMENTS.CREATED_BY = this.LoggedUserName;
                            gL_ACCT_CODE_SEGMENTS.CREATED_DATE = DateTime.Today;
                            //DBMethod.SaveEntity<GL_ACCT_CODE_SEGMENTS>(gL_ACCT_CODE_SEGMENTS);
                            tmpChildEntity.Add(new Tuple<object, string>(gL_ACCT_CODE_SEGMENTS, "A"));
                        }
                    }

                }

                //ProReturn = FINSP.GetSPFOR_ERR_MGR_CATEGORY_TWO(Master.FormCode, gL_ACCT_CODES.ACCT_CODE_ID, gL_ACCT_CODES.ACCT_STRUCT_ID, gL_ACCT_CODES.ACCT_GRP_ID);
                //if (ProReturn != string.Empty)
                //{
                //    if (ProReturn != "0")
                //    {
                //        ErrorCollection.Add("ACCOUNTCODES", ProReturn);
                //        if (ErrorCollection.Count > 0)
                //        {
                //            return;
                //        }
                //    }
                //}
                //ProReturn_Valdn = FINSP.GetSPFOR_ACTIVE_FROM_END_DATE(Master.FormCode, gL_ACCT_CODES.ACCT_CODE_ID.ToString(), gL_ACCT_CODES.EFFECTIVE_START_DT.ToString("dd/MMM/yyyy"), gL_ACCT_CODES.EFFECTIVE_END_DT.ToString(), int.Parse(gL_ACCT_CODES.ENABLED_FLAG.ToString()));

                //if (ProReturn_Valdn != string.Empty)
                //{
                //    if (ProReturn_Valdn != "0")
                //    {

                //        ErrorCollection.Add("ACCOUNTCODES", ProReturn_Valdn);
                //        if (ErrorCollection.Count > 0)
                //        {
                //            return;
                //        }
                //    }

                //}
                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {

                            ProReturn = FINSP.GetSPFOR_ERR_MGR_ACC_CODE(ddlStructureName.SelectedValue, ddlGroupName.SelectedValue, txtAccountCode.Text, gL_ACCT_CODES.ACCT_CODE_ID, txtEffectiveDate.Text, txtEndDate.Text);

                            if (ProReturn != string.Empty)
                            {
                                if (ProReturn != "0")
                                {
                                    ErrorCollection.Add("ACCCODE", ProReturn);
                                    if (ErrorCollection.Count > 0)
                                    {
                                        return;
                                    }
                                }
                            }


                            CommonUtils.SavePCEntity<GL_ACCT_CODES, GL_ACCT_CODE_SEGMENTS>(gL_ACCT_CODES, tmpChildEntity, gL_ACCT_CODE_SEGMENTS);
                            savedBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {

                            ProReturn = FINSP.GetSPFOR_ERR_MGR_ACC_CODE(ddlStructureName.SelectedValue, ddlGroupName.SelectedValue, txtAccountCode.Text, gL_ACCT_CODES.ACCT_CODE_ID, txtEffectiveDate.Text, txtEndDate.Text);

                            if (ProReturn != string.Empty)
                            {
                                if (ProReturn != "0")
                                {
                                    ErrorCollection.Add("ACCCODE", ProReturn);
                                    if (ErrorCollection.Count > 0)
                                    {
                                        return;
                                    }
                                }
                            }




                            CommonUtils.SavePCEntity<GL_ACCT_CODES, GL_ACCT_CODE_SEGMENTS>(gL_ACCT_CODES, tmpChildEntity, gL_ACCT_CODE_SEGMENTS, true);
                            savedBool = true;
                            break;
                        }
                }
                if (ErrorCollection.Count > 0)
                {
                    return;
                }



            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddl_Segment = tmpgvr.FindControl("ddlSegment") as DropDownList;
                ComboFilling.getSegments(ref ddl_Segment);


                if (gvData.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                {
                    ddl_Segment.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.SEGMENT_ID].ToString();
                    if (gvData.DataKeys[tmpgvr.RowIndex].Values["ACCT_CODE_SEG_ID"].ToString() !="0")
                    {
                        ddl_Segment.Enabled = false;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session[FINSessionConstants.GridData], "Segment Details");

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                int count = 0;
                for (int iLoop = 0; iLoop < gvData.Rows.Count; iLoop++)
                {
                    CheckBox chkact = (CheckBox)gvData.Rows[iLoop].FindControl("chkact");

                    if (chkact.Checked == true)
                    {
                        count = count + 1;
                    }
                }
                if (count > 6)
                {
                    ErrorCollection.Add("AcctCodeChk", "Maximum only 6 segments can be active for a particular account code");
                }
                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                ErrorCollection.Clear();

                AssignToBE();
               
                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        #endregion

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr["ENABLED_FLAG"] = "FALSE";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountCodesEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            if (Session[FINSessionConstants.GridData] != null)
            {
                dtGridData = (DataTable)Session[FINSessionConstants.GridData];
            }
            gvData.EditIndex = -1;

            BindGrid(dtGridData);

        }
        #endregion

        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CalendarEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {


            System.Collections.SortedList slControls = new System.Collections.SortedList();
            DropDownList ddl_Segment = gvr.FindControl("ddlSegment") as DropDownList;
            TextBox dtp_FromDate = gvr.FindControl("dtpFromDate") as TextBox;
            TextBox dtp_ToDate = gvr.FindControl("dtpToDate") as TextBox;
            CheckBox chkact = gvr.FindControl("chkact") as CheckBox;

            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList[FINColumnConstants.ACCT_CODE_SEG_ID] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }

            //if (dt_tmp.Select("DELETED='" + FINAppConstants.N + "'").Length >= 6)
            //{
            //    ErrorCollection.Clear();
            //    ErrorCollection.Add("LengthExced", "Only 6 Segment able to add");
            //    return drList;

            //}

            slControls[0] = ddl_Segment;
            slControls[1] = dtp_FromDate;
            slControls[2] = dtp_FromDate;
            slControls[3] = dtp_ToDate;

            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/GL_" + Session["Sel_Lng"].ToString() + ".properties"));
          
            ErrorCollection.Clear();
            string strCtrlTypes = "DropDownList~TextBox~DateTime~DateRangeValidate";

            string strMessage = Prop_File_Data["Segment_Name_P"] + " ~ " + Prop_File_Data["From_Date_P"] + " ~ " + Prop_File_Data["From_Date_P"] + " ~ " + Prop_File_Data["To_Date_P"] + "";

            EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

            if (EmptyErrorCollection.Count > 0)
            {
                ErrorCollection = EmptyErrorCollection;
                return drList;
            }


            //ErrorCollection = UserUtility_BLL.DateRangeValidate(DBMethod.ConvertStringToDate(dtp_FromDate.Text), DBMethod.ConvertStringToDate(dtp_ToDate.Text), "chk");
            //if (ErrorCollection.Count > 0)
            //    return drList;

            //string strCondition = "SEGMENT_ID='" + ddl_Segment.SelectedValue + "'";
            //strMessage = FINMessageConstatns.SegmentNameAlreadyExists;
            //ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            //if (ErrorCollection.Count > 0)
            //{

            //    return drList;
            //}

            //DataTable dt_segData = FIN.BLL.GL.Segments_BLL.getSegmentData(ddl_Segment.SelectedValue.ToString());
            //if (dt_segData.Rows.Count > 0)
            //{
            //    if (dt_segData.Rows[0]["PARENT_SEGMENT_ID"].ToString().Length > 0)
            //    {
            //        strCondition = "SEGMENT_ID='" + dt_segData.Rows[0]["PARENT_SEGMENT_ID"].ToString() + "'";
            //        strMessage = "Selected Segment is Dependent Segment,First Select Dependent Segment";
            //        ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            //        if (ErrorCollection.Count == 0)
            //        {
            //            ErrorCollection.Add("segmenterror", "Selected Segment is Dependent Segment,First Select Dependent Segment");
            //            return drList;
            //        }
            //        else
            //        {
            //            ErrorCollection.Clear();
            //        }
            //    }
            //}

            if (dtp_FromDate.Text.ToString().Length > 0)
            {               
                if (DBMethod.ConvertStringToDate(txtEffectiveDate.Text.ToString()) > DBMethod.ConvertStringToDate(dtp_FromDate.Text.ToString()))
                {
                    ErrorCollection.Add("Invalid from date22", "Details start Date should not less than master's start date.");
                    return drList;
                }
            }

            if (dtp_ToDate.Text.ToString().Length > 0)
            {             
                
                if (DBMethod.ConvertStringToDate(dtp_ToDate.Text.ToString()) > DBMethod.ConvertStringToDate(txtEndDate.Text.ToString()))
                {
                    ErrorCollection.Add("Invalid to date11", "Details end Date should not greater than master's end date.");
                    return drList;
                }

            }
            string strCondition = "SEGMENT_ID='" + ddl_Segment.SelectedValue.ToString() + "'";
            strMessage = FINMessageConstatns.SegmentNameAlreadyExists;
            ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            if (ErrorCollection.Count > 0)
            {

                return drList;
            }

            int int_segRow = 0;
            if (GMode == "A")
            {
                int_segRow = dt_tmp.Rows.Count;
            }
            else
            {
                int_segRow = rowindex;
            }
            DataTable dt_segData;
            if (int_segRow > 0)
            {
                dt_segData = FIN.BLL.GL.Segments_BLL.getSegmentData(ddl_Segment.SelectedValue.ToString());
                if (dt_segData.Rows.Count > 0)
                {
                    if (dt_segData.Rows[0]["PARENT_SEGMENT_ID"].ToString().Length > 0)
                    {
                        //strCondition = "SEGMENT_ID='" + dt_segData.Rows[0]["PARENT_SEGMENT_ID"].ToString() + "'";
                        //strMessage = "Selected Segment is Dependent Segment,First Select Dependent Segment";
                        //ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
                        //if (ErrorCollection.Count == 0)
                        //{
                        //    ErrorCollection.Add("segmenterror", "Selected Segment is Dependent Segment,First Select independent Segment");
                        //    return drList;
                        //}
                        //else
                        //{
                        //    ErrorCollection.Clear();
                        //}
                        ErrorCollection.Clear();

                        if (dt_tmp.Rows[int_segRow - 1]["SEGMENT_ID"].ToString() != dt_segData.Rows[0]["PARENT_SEGMENT_ID"].ToString())
                        {
                            ErrorCollection.Add("segmenterror",Prop_File_Data["Selected_Segment_dependent_P"]+ Prop_File_Data["First_select_independent_segment_P"]);
                                
                            return drList;
                        }

                    }
                }
            }
            ErrorCollection.Clear();
            if (GMode != "A")
            {
                if (int_segRow < dt_tmp.Rows.Count)
                {
                    dt_segData = FIN.BLL.GL.Segments_BLL.getSegmentData(dt_tmp.Rows[int_segRow]["SEGMENT_ID"].ToString());
                    if (dt_segData.Rows.Count > 0)
                    {
                        if (dt_segData.Rows[0]["PARENT_SEGMENT_ID"].ToString().Length > 0)
                        {
                            if (ddl_Segment.SelectedValue != dt_segData.Rows[0]["PARENT_SEGMENT_ID"].ToString())
                            {
                                ErrorCollection.Add("segmenterror", Prop_File_Data["Next_Segment_Dependent_P"]);
                                   
                                return drList;
                            }
                        }
                    }
                }
            }


            //drList[FINColumnConstants.ACCT_CODE_SEGMENT_ID] = ddl_Segment.SelectedItem.Text;
            drList[FINColumnConstants.SEGMENT_ID] = ddl_Segment.SelectedValue.ToString();
            drList[FINColumnConstants.SEGMENT_NAME] = ddl_Segment.SelectedItem.Text;
            drList[FINColumnConstants.EFFECTIVE_START_DT] = DBMethod.ConvertStringToDate(dtp_FromDate.Text.ToString());
            if (dtp_ToDate.Text.ToString().Length > 0)
            {
                drList[FINColumnConstants.EFFECTIVE_END_DT] = DBMethod.ConvertStringToDate(dtp_ToDate.Text.ToString());
            }
            else
            {
                drList[FINColumnConstants.EFFECTIVE_END_DT] = DBNull.Value;
            }
            if (chkact.Checked)
            {
                drList[FINColumnConstants.ENABLED_FLAG] = "TRUE";
            }
            else
            {
                drList[FINColumnConstants.ENABLED_FLAG] = "FALSE";
            }
            drList[FINColumnConstants.DELETED] = FINAppConstants.N;

            
            return drList;

        }

        public void DataDuplication(System.Data.DataTable dtGridData, string strCondition, string strMessage)
        {
            try
            {
                // SortedList ErrorCollection = new SortedList();
                if (dtGridData.Select(strCondition).Length > 0)
                {
                    ErrorCollection.Add(strMessage, strMessage);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountCodesEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
            //return ErrorCollection;
        }

        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountCodesEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountCodesEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountCodesEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountCodesEntry_RowCreated", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountCodesEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                // COURSE_GROUP.COURSE_GROUP_ID = short.Parse(Master.RecordID.ToString());
                DBMethod.DeleteEntity<GL_ACCT_CODES>(gL_ACCT_CODES);
                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATADELETED);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountCodesEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void txtAccountCode_TextChanged(object sender, EventArgs e)
        {

        }

        protected void ddlStructureName_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void ddlGroupName_SelectedIndexChanged(object sender, EventArgs e)
        {

            //fillSegmentName();

        }

        private void fillSegmentName()
        {
            dtGridData = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountCodes_DAL.getSegment_BasedonGroup(ddlGroupName.SelectedValue)).Tables[0];
            dtGridData.Columns.Add(FINColumnConstants.DELETED);
           
            BindGrid(dtGridData);

        }

        protected void rbControlAcc_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rbControlAcc.SelectedValue == "Y")
            {
                ddlControlAccountCategory.Enabled = true;
            }
            else
            {
                ddlControlAccountCategory.Enabled = false;
            }
        }

        protected void gvData_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void txtAccountCodeDescription_TextChanged(object sender, EventArgs e)
        {

        }



    }
}