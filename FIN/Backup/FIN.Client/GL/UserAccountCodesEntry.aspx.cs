﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.BLL.AP;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.BLL;
using VMVServices.Web;
namespace FIN.Client.GL
{
    public partial class UserAccountCodesEntry : PageBase
    {
        GL_PETTY_CASH_USER_ACCT_CODES gL_PETTY_CASH_USER_ACCT_CODES = new GL_PETTY_CASH_USER_ACCT_CODES();
        DataTable dtGridData = new DataTable();
        System.Collections.SortedList slControls = new System.Collections.SortedList();
        //string ProReturn = null;
        Boolean bol_rowVisiable;
        Boolean savedBool;
        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                //pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                //pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";

            }

            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
            }

            if (VMVServices.Web.Utils.AddAllowed != FINAppConstants.Y && Master.Mode == FINAppConstants.Add)
            {
                btnSave.Visible = false;
            }

        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                //ddlWeightUOM.Enabled = false;
                //ddlLengthUOM.Enabled = false;
                //ddlAreaUOM.Enabled = false;
                //ddlVolumeUOM.Enabled = false;


                Startup();
                FillComboBox();


                EntityData = null;

                dtGridData = FIN.BLL.GL.UserAccountCodes_BLL.getChildEntityDet(Master.StrRecordId);
                BindGrid(dtGridData);


                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    using (IRepository<GL_PETTY_CASH_USER_ACCT_CODES> userCtx = new DataRepository<GL_PETTY_CASH_USER_ACCT_CODES>())
                    {
                        gL_PETTY_CASH_USER_ACCT_CODES = userCtx.Find(r =>
                            (r.GL_PC_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = gL_PETTY_CASH_USER_ACCT_CODES;

                    ddlUser.SelectedValue = gL_PETTY_CASH_USER_ACCT_CODES.GL_USER_ID;
                    FillUserName();

                   // ddlAccountCode.SelectedValue = gL_PETTY_CASH_USER_ACCT_CODES.GL_ACCT_CODE_ID;
                   // FillAcctDesc();
                    if (gL_PETTY_CASH_USER_ACCT_CODES.GL_EFFECTIVE_FROM_DT != null)
                    {
                        txtFromDate.Text = DBMethod.ConvertDateToString(gL_PETTY_CASH_USER_ACCT_CODES.GL_EFFECTIVE_FROM_DT.ToString());
                    }
                    if (gL_PETTY_CASH_USER_ACCT_CODES.GL_EFFECTIVE_TO_DT != null)
                    {
                        txtToDate.Text = DBMethod.ConvertDateToString(gL_PETTY_CASH_USER_ACCT_CODES.GL_EFFECTIVE_TO_DT.ToString());
                    }

                   // chKEnabledFlag.Checked = gL_PETTY_CASH_USER_ACCT_CODES.ENABLED_FLAG == FINAppConstants.EnabledFlag ? true : false;
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>

        private void FillComboBox()
        {
            FIN.BLL.GL.AccountCodes_BLL.fn_getUserdtls(ref ddlUser);
          //  FIN.BLL.GL.AccountCodes_BLL.fn_getAccountdtls(ref ddlAccountCode);

            //FIN.BLL.HR.EmployeeContractAssignment_BLL.fn_ContractName(ref ddlContractName);
            //ComboFilling.fn_getDepartment(ref ddlDepartment);

            //Lookup_BLL.GetLookUpValues(ref ddlType, "VAC_TYPE");


        }

        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                //if (EntityData != null)
                //{
                //    gL_PETTY_CASH_USER_ACCT_CODES = (GL_PETTY_CASH_USER_ACCT_CODES)EntityData;
                //}

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                var tmpChildEntity = new List<Tuple<object, string>>();


                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    gL_PETTY_CASH_USER_ACCT_CODES = new GL_PETTY_CASH_USER_ACCT_CODES();
                    if (dtGridData.Rows[iLoop]["GL_PC_ID"].ToString() != "0")
                    {
                        using (IRepository<GL_PETTY_CASH_USER_ACCT_CODES> userCtx = new DataRepository<GL_PETTY_CASH_USER_ACCT_CODES>())
                        {
                            gL_PETTY_CASH_USER_ACCT_CODES = userCtx.Find(r =>
                                (r.GL_PC_ID == dtGridData.Rows[iLoop]["GL_PC_ID"].ToString())
                                ).SingleOrDefault();
                        }
                    }
                    gL_PETTY_CASH_USER_ACCT_CODES.GL_ACCT_CODE = dtGridData.Rows[iLoop]["CODE_ID"].ToString();


                    gL_PETTY_CASH_USER_ACCT_CODES.GL_ORG_ID = VMVServices.Web.Utils.OrganizationID;

                    gL_PETTY_CASH_USER_ACCT_CODES.GL_USER_ID = ddlUser.SelectedValue.ToString();

                    

                    if (txtFromDate.Text != string.Empty)
                    {
                        gL_PETTY_CASH_USER_ACCT_CODES.GL_EFFECTIVE_FROM_DT = DBMethod.ConvertStringToDate(txtFromDate.Text.ToString());
                    }
                    if (txtToDate.Text != string.Empty)
                    {
                        gL_PETTY_CASH_USER_ACCT_CODES.GL_EFFECTIVE_TO_DT = DBMethod.ConvertStringToDate(txtToDate.Text.ToString());
                    }

                    if (dtGridData.Rows[iLoop][FINColumnConstants.ENABLED_FLAG].ToString().ToUpper() == "TRUE")
                    {
                        gL_PETTY_CASH_USER_ACCT_CODES.ENABLED_FLAG = FINAppConstants.Y;
                    }
                    else
                    {
                        gL_PETTY_CASH_USER_ACCT_CODES.ENABLED_FLAG = FINAppConstants.N;
                    }

                    



                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        DBMethod.DeleteEntity<GL_PETTY_CASH_USER_ACCT_CODES>(gL_PETTY_CASH_USER_ACCT_CODES);
                    }
                    else
                    {



                        if (dtGridData.Rows[iLoop]["GL_PC_ID"].ToString() != "0")
                        {
                            gL_PETTY_CASH_USER_ACCT_CODES.GL_PC_ID = dtGridData.Rows[iLoop]["GL_PC_ID"].ToString();
                            gL_PETTY_CASH_USER_ACCT_CODES.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, gL_PETTY_CASH_USER_ACCT_CODES.GL_PC_ID);
                            gL_PETTY_CASH_USER_ACCT_CODES.MODIFIED_BY = this.LoggedUserName;
                            gL_PETTY_CASH_USER_ACCT_CODES.MODIFIED_DATE = DateTime.Today;
                            DBMethod.SaveEntity<GL_PETTY_CASH_USER_ACCT_CODES>(gL_PETTY_CASH_USER_ACCT_CODES, true);
                            

                        }
                        else
                        {

                            gL_PETTY_CASH_USER_ACCT_CODES.GL_PC_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.GL_020.ToString(), false, true);
                            gL_PETTY_CASH_USER_ACCT_CODES.PK_ID = DBMethod.GetPrimaryKeyValue(FINSequenceConstant.HR_JOB_RESPONSIBILITY_SEQ);
                            gL_PETTY_CASH_USER_ACCT_CODES.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, gL_PETTY_CASH_USER_ACCT_CODES.GL_PC_ID);
                            gL_PETTY_CASH_USER_ACCT_CODES.CREATED_BY = this.LoggedUserName;
                            gL_PETTY_CASH_USER_ACCT_CODES.CREATED_DATE = DateTime.Today;
                            DBMethod.SaveEntity<GL_PETTY_CASH_USER_ACCT_CODES>(gL_PETTY_CASH_USER_ACCT_CODES);
                            
                        }
                    }

                }


                             
               
                //if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                //{
                //    gL_PETTY_CASH_USER_ACCT_CODES.MODIFIED_BY = this.LoggedUserName;
                //    gL_PETTY_CASH_USER_ACCT_CODES.MODIFIED_DATE = DateTime.Today;

                //}
                //else
                //{
                //    gL_PETTY_CASH_USER_ACCT_CODES.GL_PC_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.GL_020.ToString(), false, true);
                //    //iNV_ITEM_MASTER.WORKFLOW_COMPLETION_STATUS = "1";
                //    gL_PETTY_CASH_USER_ACCT_CODES.CREATED_BY = this.LoggedUserName;
                //    gL_PETTY_CASH_USER_ACCT_CODES.CREATED_DATE = DateTime.Today;

                //}

                

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }



        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddlAccountCode = tmpgvr.FindControl("ddlAccountCode") as DropDownList;
                FIN.BLL.GL.AccountCodes_BLL.fn_getAccountdtls(ref ddlAccountCode);


                if (gvData.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                {
                    ddlAccountCode.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["CODE_ID"].ToString();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("UAC_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                AssignToBE();


                //emptyvalid();
                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);

                //switch (Master.Mode)
                //{
                //    case FINAppConstants.Add:
                //        {
                //            DBMethod.SaveEntity<GL_PETTY_CASH_USER_ACCT_CODES>(gL_PETTY_CASH_USER_ACCT_CODES);
                //            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                //            break;
                //        }
                //    case FINAppConstants.Update:
                //        {

                //            DBMethod.SaveEntity<GL_PETTY_CASH_USER_ACCT_CODES>(gL_PETTY_CASH_USER_ACCT_CODES, true);
                //            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                //            break;

                //        }
                //}
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr["ENABLED_FLAG"] = "FALSE";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("UAC_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            try
            {
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = -1;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("UAC_ROW_CNCL_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion

        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("UAC_ROW_CMD", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {


            System.Collections.SortedList slControls = new System.Collections.SortedList();

            DropDownList ddlAccountCode = gvr.FindControl("ddlAccountCode") as DropDownList;
            TextBox txtdesc = gvr.FindControl("txtdesc") as TextBox;

            CheckBox chkact = gvr.FindControl("chkact") as CheckBox;


            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList["GL_PC_ID"] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }

            slControls[0] = ddlAccountCode;
           

            ErrorCollection.Clear();
            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/SSM_" + Session["Sel_Lng"].ToString() + ".properties"));
            string strCtrlTypes = "DropDownList";
            string strMessage = Prop_File_Data["Account_Code_P"] + "";
            //string strMessage = "Account Code";
            ErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);
            if (ErrorCollection.Count > 0)
                return drList;

            //Duplicate Validation through Backend
            ErrorCollection.Clear();
            string ProReturn = null;
            ProReturn = FINSP.GetSPFOR_ERR_MGR_CATEGORY(Master.FormCode, drList["GL_PC_ID"].ToString(), ddlUser.SelectedValue.ToString());
            if (ProReturn != string.Empty)
            {
                if (ProReturn != "0")
                {
                    ErrorCollection.Add("USERACCOUNTCODES", ProReturn);
                    if (ErrorCollection.Count > 0)
                    {
                        return drList;
                    }
                }
            }

            //  string strCondition = " VALUE_KEY_ID='" + ddlResTyp.SelectedValue + "' AND JR_NAME= '" + txtdesc.Text + "' AND JOB_ID= '" + ddlJobCode.SelectedValue + "'";


            //string strCondition = " VALUE_KEY_ID='" + ddlResTyp.SelectedValue + "'";

            //strMessage = FINMessageConstatns.ResponsibilityType1;
            //ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            //if (ErrorCollection.Count > 0)
            //{

            //    return drList;
            //}





            if (ddlAccountCode.SelectedItem != null)
            {
                drList[FINColumnConstants.CODE_ID] = ddlAccountCode.SelectedItem.Value;
                drList[FINColumnConstants.CODE_NAME] = ddlAccountCode.SelectedItem.Text;
            }

            drList["CODE_NAME"] = txtdesc.Text;

                      
            if (chkact.Checked)
            {
                drList[FINColumnConstants.ENABLED_FLAG] = "TRUE";
            }
            else
            {
                drList[FINColumnConstants.ENABLED_FLAG] = "FALSE";
            }



            drList[FINColumnConstants.DELETED] = FINAppConstants.N;


            return drList;



        }



        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("UAC_ROW_UP", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("UAC_ROW_DEL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("UAC_ROW_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("UAC_RowCreated", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("UAC_RDB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                DBMethod.DeleteEntity<GL_PETTY_CASH_USER_ACCT_CODES>(gL_PETTY_CASH_USER_ACCT_CODES);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

       

        protected void ddlUser_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillUserName();
        }
        private void FillUserName()
        {
            DataTable dtUsername = new DataTable();
            dtUsername = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountCodes_DAL.getUserNamedtls(ddlUser.SelectedValue)).Tables[0];
            txtUserName.Text = dtUsername.Rows[0]["USER_NAMES"].ToString();
        }
        protected void ddlAccountCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            DropDownList ddlAccountCode = gvr.FindControl("ddlAccountCode") as DropDownList;
            TextBox txtdesc = gvr.FindControl("txtdesc") as TextBox;

            DataTable dtAcctDesc = new DataTable();
            dtAcctDesc = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountCodes_DAL.getAccountdtls(ddlAccountCode.SelectedValue)).Tables[0];
            txtdesc.Text = dtAcctDesc.Rows[0]["CODE_NAME"].ToString();
            
        }
        
    }
    
}