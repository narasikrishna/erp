﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using FIN.DAL;
using FIN.BLL;
using FIN.BLL.GL;
using VMVServices.Web;

namespace FIN.Client.GL
{
    public partial class AccountingGroupsEntry : PageBase
    {
        GL_ACCT_GROUPS gL_ACCT_GROUPS = new GL_ACCT_GROUPS();

        string ProReturn = null;
        string ProReturn_Valdn = null;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();
                    txtGroupName.Focus();
                    ChangeLanguage();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL_AGE", ex.Message);
            }
            finally
            {
                // ErrorCollection.Add("SS", "Success");
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        private void ChangeLanguage()
        {
            try
            {
                ErrorCollection.Clear();
                if (VMVServices.Web.Utils.LanguageCode.ToString().Length > 0)
                {
                    Dictionary<string, string> Prop_File_Data;
                    Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/GL_" + Session["Sel_Lng"].ToString() + ".properties"));
                    rbFundTransfer.Items[0].Text = Prop_File_Data["Yes_P"];
                    rbFundTransfer.Items[1].Text = Prop_File_Data["No_P"];

                    rbLFGYN.Items[0].Text = Prop_File_Data["Yes_P"];
                    rbLFGYN.Items[1].Text = Prop_File_Data["No_P"];
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("MisReceiptChangeLang", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }



            UserRightsChecking();



        }

        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();


                EntityData = null;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    gL_ACCT_GROUPS = AccountingGroups_BLL.getClassEntity(Master.StrRecordId);

                    EntityData = gL_ACCT_GROUPS;

                    txtGroupName.Text = gL_ACCT_GROUPS.ACCT_GRP_DESC;
                    if (gL_ACCT_GROUPS.ACCT_GRP_DESC_OL != null || gL_ACCT_GROUPS.ACCT_GRP_DESC_OL != string.Empty)
                    {
                        txtGroupNameAR.Text = gL_ACCT_GROUPS.ACCT_GRP_DESC_OL;
                    }

                    txtAlternateName.Text = gL_ACCT_GROUPS.ACCT_GRP_ALT_NAME;
                    txtEffStartdate.Text = DBMethod.ConvertDateToString(gL_ACCT_GROUPS.EFFECTIVE_START_DT.ToString());
                    if (gL_ACCT_GROUPS.EFFECTIVE_END_DT != null)
                    {
                        txtEndDate.Text = DBMethod.ConvertDateToString(gL_ACCT_GROUPS.EFFECTIVE_END_DT.ToString());
                    }

                    if (gL_ACCT_GROUPS.ACCT_FINANCIAL_GRP == FINAppConstants.Y)
                    {
                        rbLFGYN.SelectedValue = "Y";
                    }
                    else
                    {
                        rbLFGYN.SelectedValue = "N";
                    }


                    chkActive.Checked = true;
                    if (gL_ACCT_GROUPS.ENABLED_FLAG == FINAppConstants.N)
                    {
                        chkActive.Checked = false;
                    }
                    rbFundTransfer.SelectedValue = "Y";
                    if (gL_ACCT_GROUPS.ACCT_FUND_TRANSFER != null)
                    {
                        if (gL_ACCT_GROUPS.ACCT_FUND_TRANSFER == FINAppConstants.N)
                        {
                            rbFundTransfer.SelectedValue = "N";
                        }
                    }

                    //if (gL_ACCT_GROUPS.ENABLED_FLAG == FINAppConstants.Y)
                    //{
                    //    chkActive.Checked = true;
                    //}
                    //else
                    //{
                    //    chkActive.Checked = false;
                    //}

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    gL_ACCT_GROUPS = (GL_ACCT_GROUPS)EntityData;
                }
                gL_ACCT_GROUPS.ACCT_GRP_DESC = txtGroupName.Text.Trim();
                if (txtGroupNameAR.Text != null)
                {
                    gL_ACCT_GROUPS.ACCT_GRP_DESC_OL = txtGroupNameAR.Text.Trim();
                }
                gL_ACCT_GROUPS.ACCT_GRP_ALT_NAME = txtAlternateName.Text.Trim();

                gL_ACCT_GROUPS.EFFECTIVE_START_DT = DBMethod.ConvertStringToDate(txtEffStartdate.Text.ToString());
                if (txtEndDate.Text.ToString().Length > 0)
                {
                    gL_ACCT_GROUPS.EFFECTIVE_END_DT = DBMethod.ConvertStringToDate(txtEndDate.Text.ToString());
                }
                else
                {
                    gL_ACCT_GROUPS.EFFECTIVE_END_DT = null;
                }

                //gL_ACCT_GROUPS.WORKFLOW_COMPLETION_STATUS = FINAppConstants.Y;

                if (rbLFGYN.SelectedValue == "Y")
                {
                    gL_ACCT_GROUPS.ACCT_FINANCIAL_GRP = FINAppConstants.Y;
                }
                if (rbLFGYN.SelectedValue == "N")
                {
                    gL_ACCT_GROUPS.ACCT_FINANCIAL_GRP = FINAppConstants.N;
                }

                if (rbFundTransfer.SelectedValue == "Y")
                {
                    gL_ACCT_GROUPS.ACCT_FUND_TRANSFER = FINAppConstants.Y;
                }
                if (rbFundTransfer.SelectedValue == "N")
                {
                    gL_ACCT_GROUPS.ACCT_FUND_TRANSFER = FINAppConstants.N;
                }

                if (chkActive.Checked == true)
                {
                    gL_ACCT_GROUPS.ENABLED_FLAG = FINAppConstants.Y;
                }
                else
                {
                    gL_ACCT_GROUPS.ENABLED_FLAG = FINAppConstants.N;
                }

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    gL_ACCT_GROUPS.MODIFIED_BY = this.LoggedUserName;
                    gL_ACCT_GROUPS.MODIFIED_DATE = DateTime.Today;

                }
                else
                {
                    gL_ACCT_GROUPS.ACCT_GRP_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.GL_ACT_GRP.ToString(), false, true);
                    //gL_ACCT_GROUPS.PK_ID = DBMethod.GetPrimaryKeyValue(FINSequenceConstant.GL_ACCT_GROUPS_SEQ);
                    gL_ACCT_GROUPS.CREATED_BY = this.LoggedUserName;
                    gL_ACCT_GROUPS.CREATED_DATE = DateTime.Today;


                }
                gL_ACCT_GROUPS.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, gL_ACCT_GROUPS.ACCT_GRP_ID);
                gL_ACCT_GROUPS.ACCT_ORG_ID = VMVServices.Web.Utils.OrganizationID;
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>


        #endregion




        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                AccountingGroups_BLL.DeleteEntity(Master.StrRecordId);
                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATADELETED);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("GradeEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                String Start_date;
                String End_date;
                ErrorCollection.Clear();
                AssignToBE();

                //Error Message Handler

                //ProReturn = FINSP.GetSPFOR_ERR_MGR_ACC_GROUP(txtGroupName.Text, gL_ACCT_GROUPS.ACCT_GRP_ID, txtEffStartdate.Text, txtEndDate.Text);

                //if (ProReturn != string.Empty)
                //{
                //    if (ProReturn != "0")
                //    {

                //        ErrorCollection.Add("GRPNAME", ProReturn);
                //        if (ErrorCollection.Count > 0)
                //        {
                //            return;
                //        }
                //    }

                //}



                //if (ErrorCollection.Count > 0)
                //{
                //    return;
                //}

                ErrorCollection.Clear();
                ProReturn = FINSP.GetSPFOR_ERR_MGR_CATEGORY_TWO(Master.FormCode, gL_ACCT_GROUPS.ACCT_GRP_ID, gL_ACCT_GROUPS.ACCT_GRP_DESC, gL_ACCT_GROUPS.ACCT_GRP_ALT_NAME);
                if (ProReturn != string.Empty)
                {
                    if (ProReturn != "0")
                    {
                        ErrorCollection.Add("ACCOUNTGROUPS", ProReturn);
                        if (ErrorCollection.Count > 0)
                        {
                            return;
                        }
                    }
                }
                ProReturn_Valdn = FINSP.GetSPFOR_ACTIVE_FROM_END_DATE(Master.FormCode, gL_ACCT_GROUPS.ACCT_GRP_ID.ToString(), gL_ACCT_GROUPS.EFFECTIVE_START_DT.ToString("dd/MMM/yyyy"), gL_ACCT_GROUPS.EFFECTIVE_END_DT.ToString(), int.Parse(gL_ACCT_GROUPS.ENABLED_FLAG.ToString()));

                if (ProReturn_Valdn != string.Empty)
                {
                    if (ProReturn_Valdn != "0")
                    {

                        ErrorCollection.Add("GRPNAME", ProReturn_Valdn);
                        if (ErrorCollection.Count > 0)
                        {
                            return;
                        }
                    }

                }



                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                string str_Error = AccountingGroups_BLL.ValidateEntity(gL_ACCT_GROUPS);
                if (str_Error.Length > 0)
                {
                    ErrorCollection.Add("EntityValidation_AG", str_Error);
                    return;
                }
                AccountingGroups_BLL.SaveEntity(gL_ACCT_GROUPS, Master.Mode, true);
                //DisplaySaveCompleteMessage(Master.ListPageToOpen);
                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);



            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save_AG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);

                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void rbLFGYN_SelectedIndexChanged(object sender, EventArgs e)
        {

        }





    }
}