﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using FIN.DAL;
using FIN.BLL;
using FIN.BLL.GL;
using VMVServices.Web;

namespace FIN.Client.GL
{
    public partial class OrganisationEntry : PageBase
    {
        GL_COMPANIES_HDR gL_COMPANIES_HDR = new GL_COMPANIES_HDR();
        GL_COMPANIES_DTL gL_COMPANIES_DTL = new GL_COMPANIES_DTL();
        GL_COMP_ACCT_CALENDAR_DTL gL_COMP_ACCT_CALENDAR_DTL = new GL_COMP_ACCT_CALENDAR_DTL();
        DataTable dtGridData = new DataTable();
        string ProReturn = null;
        Boolean bol_rowVisiable;
        Boolean savedBool;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                if (!IsPostBack)
                {
                    Session["CompCalDet"] = null;
                    Session["CompCalPeriodDet"] = null;
                    AssignToControl();
                    EntryLoadHeader();
                    ChangeLanguage();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();
                    txtInternalName.Focus();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Pg_Load", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }


        }

        private void ChangeLanguage()
        {
            try
            {
                ErrorCollection.Clear();
                if (VMVServices.Web.Utils.LanguageCode.ToString().Length > 0)
                {
                    Dictionary<string, string> Prop_File_Data;
                    Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/GL_" + Session["Sel_Lng"].ToString() + ".properties"));
                   
                    rblYN.Items[0].Text = Prop_File_Data["Yes_P"];
                    rblYN.Items[1].Text = Prop_File_Data["No_P"];
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("MisReceiptChangeLang", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        #endregion

        protected void img_Photo_Click(object sender, ImageClickEventArgs e)
        {
            meUpload.Show();
        }

        protected void btnCloseUpload_Click(object sender, EventArgs e)
        {
            if (Session[FINSessionConstants.Attachments] != null)
            {
                System.Collections.SortedList fu_tmp = (System.Collections.SortedList)Session[FINSessionConstants.Attachments];
                //img_Photo.ImageUrl = "~/TmpResume/" + fu_tmp.GetByIndex(0) + "."+ fu_tmp.GetByIndex(1);

                byte[] imgByte = File.ReadAllBytes(Server.MapPath("~/TmpResume/" + fu_tmp.GetByIndex(0) + "." + fu_tmp.GetByIndex(1))).ToArray();
                //byte[] imgByte = gL_COMPANIES_HDR.COMP_LOGO.ToArray();
                string base64String = Convert.ToBase64String(imgByte, 0, imgByte.Length);
                img_Photo.ImageUrl = "data:image/png;base64," + base64String;
            }
        }


        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //  div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }


            UserRightsChecking();

            //if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            //{
            //    VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
            //}

            //if (VMVServices.Web.Utils.AddAllowed != FINAppConstants.Y && Master.Mode == FINAppConstants.Add)
            //{
            //    btnSave.Visible = false;
            //}

        }
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        /// <summary>
        /// To assign the values to Controls and fetch the StrRecordId,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();
                EntityData = null;

                dtGridData = DBMethod.ExecuteQuery(FIN.DAL.GL.Organisation_DAL.GetCompanydtls(Master.StrRecordId)).Tables[0];
                // dtGridData = DBMethod.ExecuteQuery(FIN.DAL.GL.Organisation_DAL.GetCompanydtls(0)).Tables[0];

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {



                    using (IRepository<GL_COMPANIES_HDR> userCtx = new DataRepository<GL_COMPANIES_HDR>())
                    {
                        gL_COMPANIES_HDR = userCtx.Find(r =>
                            (r.COMP_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = gL_COMPANIES_HDR;

                    txtInternalName.Text = gL_COMPANIES_HDR.COMP_INTERNAL_NAME;
                    if (gL_COMPANIES_HDR.COMP_INTERNAL_NAME_OL != null || gL_COMPANIES_HDR.COMP_INTERNAL_NAME_OL != string.Empty)
                    {
                        txtInternalNameAR.Text = gL_COMPANIES_HDR.COMP_INTERNAL_NAME_OL;
                    }
                    txtExternalName.Text = gL_COMPANIES_HDR.COMP_EXTERNAL_NAME;
                    txtAddress1.Text = gL_COMPANIES_HDR.COMP_ADDRESS1;
                    txtAddress2.Text = gL_COMPANIES_HDR.COMP_ADDRESS2;
                    txtAddress3.Text = gL_COMPANIES_HDR.COMP_ADDRESS3;
                    txtAddress4.Text = gL_COMPANIES_HDR.COMP_ADDRESS4;
                    ddlCountry.SelectedValue = gL_COMPANIES_HDR.COMP_COUNTRY;
                    txtCompshortname.Text = gL_COMPANIES_HDR.COMP_SHORT_NAME;
                    if (gL_COMPANIES_HDR.ATTRIBUTE1 == FINAppConstants.Y)
                    {
                        chkDependent.Checked = true;
                    }
                    else
                    {
                        chkDependent.Checked = false;
                    }
                    LoadSegmentName();
                    ddlGlobseg.SelectedValue = gL_COMPANIES_HDR.COMP_GLOBAL_SEGMENT_ID;
                    if (gL_COMPANIES_HDR.ENABLED_FLAG == "1")
                    {
                        chkActive.Checked = true;
                    }
                    else
                    {
                        chkActive.Checked = false;
                    }
                    rblYN.SelectedValue = gL_COMPANIES_HDR.GLOBAL_SEGMENT_BALANCED;
                    validate_rb_glblseg();

                    if (gL_COMPANIES_HDR.COMP_LOGO != null)
                    {
                        byte[] imgByte = gL_COMPANIES_HDR.COMP_LOGO.ToArray();
                        string base64String = Convert.ToBase64String(imgByte, 0, imgByte.Length);
                        img_Photo.ImageUrl = "data:image/png;base64," + base64String;
                    }
                }

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("FacilityMasterEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Course master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    gL_COMPANIES_HDR = (GL_COMPANIES_HDR)EntityData;
                }



                gL_COMPANIES_HDR.COMP_INTERNAL_NAME = txtInternalName.Text;
                gL_COMPANIES_HDR.COMP_EXTERNAL_NAME = txtExternalName.Text;
                gL_COMPANIES_HDR.COMP_ADDRESS1 = txtAddress1.Text;
                gL_COMPANIES_HDR.COMP_ADDRESS2 = txtAddress2.Text;
                gL_COMPANIES_HDR.COMP_ADDRESS3 = txtAddress3.Text;
                gL_COMPANIES_HDR.COMP_ADDRESS4 = txtAddress4.Text;
                gL_COMPANIES_HDR.COMP_SHORT_NAME = txtCompshortname.Text;
                gL_COMPANIES_HDR.COMP_COUNTRY = ddlCountry.SelectedValue;
                if (chkDependent.Checked)
                {
                    gL_COMPANIES_HDR.ATTRIBUTE1 = FINAppConstants.Y;
                }
                else
                {
                    gL_COMPANIES_HDR.ATTRIBUTE1 = FINAppConstants.N;
                }
                gL_COMPANIES_HDR.COMP_GLOBAL_SEGMENT_ID = ddlGlobseg.SelectedValue;

                if (txtInternalNameAR.Text != null)
                {
                    gL_COMPANIES_HDR.COMP_INTERNAL_NAME_OL = txtInternalNameAR.Text;
                }

                if (rblYN.SelectedValue == "1")
                {
                    gL_COMPANIES_HDR.GLOBAL_SEGMENT_BALANCED = FINAppConstants.Y;
                }
                else
                {
                    gL_COMPANIES_HDR.GLOBAL_SEGMENT_BALANCED = FINAppConstants.N;
                }
                if (chkActive.Checked == true)
                {
                    gL_COMPANIES_HDR.ENABLED_FLAG = FINAppConstants.Y;
                }
                else
                {
                    gL_COMPANIES_HDR.ENABLED_FLAG = FINAppConstants.N;
                }



                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != string.Empty)
                {
                    gL_COMPANIES_HDR.MODIFIED_BY = this.LoggedUserName;
                    gL_COMPANIES_HDR.MODIFIED_DATE = DateTime.Today;

                }
                else
                {

                    gL_COMPANIES_HDR.CREATED_BY = this.LoggedUserName;
                    gL_COMPANIES_HDR.CREATED_DATE = DateTime.Today;
                    gL_COMPANIES_HDR.COMP_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.GL_ORGN_M.ToString(), false, true);

                }
                gL_COMPANIES_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, gL_COMPANIES_HDR.COMP_ID);

                //if (Session[FINSessionConstants.Attachments] != null)
                //{
                //    System.Collections.SortedList fu_tmp = (System.Collections.SortedList)Session[FINSessionConstants.Attachments];
                //    if (System.IO.File.Exists(Server.MapPath("~/UploadFile/ORG_LOGO/" + gL_COMPANIES_HDR.COMP_ID + ".jpg")))
                //        System.IO.File.Delete(Server.MapPath("~/UploadFile/ORG_LOGO/" + gL_COMPANIES_HDR.COMP_ID + ".jpg"));

                //    System.IO.File.Copy(Server.MapPath("~/TmpResume/" + fu_tmp.GetByIndex(0) + "." + fu_tmp.GetByIndex(1)), Server.MapPath("~/UploadFile/ORG_LOGO/" + gL_COMPANIES_HDR.COMP_ID + ".jpg"));

                //}
                if (Session[FINSessionConstants.Attachments] != null)
                {
                    System.Collections.SortedList fu_tmp = (System.Collections.SortedList)Session[FINSessionConstants.Attachments];
                    if (System.IO.File.Exists(System.Web.Configuration.WebConfigurationManager.AppSettings["OrgLogoFolder"].ToString() + gL_COMPANIES_HDR.COMP_ID + ".jpg"))
                        System.IO.File.Delete(System.Web.Configuration.WebConfigurationManager.AppSettings["OrgLogoFolder"].ToString() + gL_COMPANIES_HDR.COMP_ID + ".jpg");

                    System.IO.File.Copy(Server.MapPath("~/TmpResume/" + fu_tmp.GetByIndex(0) + "." + fu_tmp.GetByIndex(1)), System.Web.Configuration.WebConfigurationManager.AppSettings["OrgLogoFolder"].ToString() + gL_COMPANIES_HDR.COMP_ID + ".jpg");
                    gL_COMPANIES_HDR.COMP_LOGO = File.ReadAllBytes(System.Web.Configuration.WebConfigurationManager.AppSettings["OrgLogoFolder"].ToString() + gL_COMPANIES_HDR.COMP_ID + ".jpg");
                }



                //Save Detail Table


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                ErrorCollection = CommonUtils.IsEmptyGrid(dtGridData, "Organization ");
                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                var tmpChildEntity = new List<Tuple<object, string>>();


                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    gL_COMPANIES_DTL = new GL_COMPANIES_DTL();
                    if (dtGridData.Rows[iLoop][FINColumnConstants.COMP_DTL_ID].ToString() != "0")
                    {
                        using (IRepository<GL_COMPANIES_DTL> userCtx = new DataRepository<GL_COMPANIES_DTL>())
                        {
                            gL_COMPANIES_DTL = userCtx.Find(r =>
                                (r.COMP_DTL_ID == dtGridData.Rows[iLoop][FINColumnConstants.COMP_DTL_ID].ToString())
                                ).SingleOrDefault();
                        }
                    }

                    if (dtGridData.Rows[iLoop][FINColumnConstants.COMP_DTL_ID].ToString() != "0")
                    {
                        FIN.BLL.GL.Organisation_BLL.getClassEntity(dtGridData.Rows[iLoop][FINColumnConstants.COMP_DTL_ID].ToString());
                    }
                    //gL_COMPANIES_DTL.EFFECTIVE_START_DT = DBMethod.ConvertStringToDate( Text.ToString());
                    //if (dtpEndDate.Text.ToString().Length > 0)
                    //{
                    //    gL_COMPANIES_DTL.EFFECTIVE_END_DT = DBMethod.ConvertStringToDate(dtpEndDate.Text.ToString());
                    //}
                    //else
                    //{
                    //    gL_COMPANIES_DTL.EFFECTIVE_END_DT = null;
                    //}
                    if (dtGridData.Rows[iLoop][FINColumnConstants.EFFECTIVE_START_DT] != DBNull.Value)
                    {
                        gL_COMPANIES_DTL.EFFECTIVE_START_DT = DateTime.Parse(dtGridData.Rows[iLoop][FINColumnConstants.EFFECTIVE_START_DT].ToString());
                    }

                    if (dtGridData.Rows[iLoop][FINColumnConstants.EFFECTIVE_END_DT] != DBNull.Value)
                    {
                        gL_COMPANIES_DTL.EFFECTIVE_END_DT = DateTime.Parse(dtGridData.Rows[iLoop][FINColumnConstants.EFFECTIVE_END_DT].ToString());
                    }
                    gL_COMPANIES_DTL.ENABLED_FLAG = FINAppConstants.N;

                    if (dtGridData.Rows[iLoop][FINColumnConstants.ENABLED_FLAG].ToString().ToUpper() == "TRUE")
                    {
                        gL_COMPANIES_DTL.ENABLED_FLAG = FINAppConstants.Y;
                    }
                    // gL_COMPANIES_DTL.COMP_BASE_CURRENCY = "CURR_01";
                    gL_COMPANIES_DTL.COMP_BASE_CURRENCY = dtGridData.Rows[iLoop][FINColumnConstants.CURRENCY_ID].ToString();
                    gL_COMPANIES_DTL.COMP_ACCT_STRUCT_ID = dtGridData.Rows[iLoop][FINColumnConstants.ACCT_STRUCT_ID].ToString();
                    gL_COMPANIES_DTL.COMP_CAL_ID = dtGridData.Rows[iLoop][FINColumnConstants.CAL_ID].ToString();
                    gL_COMPANIES_DTL.WORKFLOW_COMPLETION_STATUS = "1";
                    gL_COMPANIES_DTL.COMP_ID = gL_COMPANIES_HDR.COMP_ID;

                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        gL_COMPANIES_DTL.COMP_DTL_ID = dtGridData.Rows[iLoop][FINColumnConstants.COMP_DTL_ID].ToString();
                        tmpChildEntity.Add(new Tuple<object, string>(gL_COMPANIES_DTL, FINAppConstants.Delete));
                    }
                    else
                    {

                        if (dtGridData.Rows[iLoop][FINColumnConstants.COMP_DTL_ID].ToString() != "0" && dtGridData.Rows[iLoop][FINColumnConstants.COMP_DTL_ID].ToString() != string.Empty)
                        {
                            gL_COMPANIES_DTL.COMP_DTL_ID = dtGridData.Rows[iLoop][FINColumnConstants.COMP_DTL_ID].ToString();
                            gL_COMPANIES_DTL.MODIFIED_BY = this.LoggedUserName;
                            gL_COMPANIES_DTL.MODIFIED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(gL_COMPANIES_DTL, FINAppConstants.Update));

                        }
                        else
                        {
                            gL_COMPANIES_DTL.COMP_DTL_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.GL_ORGN_D.ToString(), false, true);
                            gL_COMPANIES_DTL.CREATED_BY = this.LoggedUserName;
                            gL_COMPANIES_DTL.CREATED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(gL_COMPANIES_DTL, FINAppConstants.Add));
                        }
                    }



                }
                //ProReturn = FINSP.GetSPFOR_ERR_MGR_CATEGORY(Master.FormCode, gL_COMPANIES_HDR.COMP_ID, gL_COMPANIES_HDR.);
                //if (ProReturn != string.Empty)
                //{
                //    if (ProReturn != "0")
                //    {
                //        ErrorCollection.Add("ACCOUNTCODES", ProReturn);
                //        if (ErrorCollection.Count > 0)
                //        {
                //            return;
                //        }
                //    }
                //}
                //ProReturn_Valdn = FINSP.GetSPFOR_ACTIVE_FROM_END_DATE(Master.FormCode, gL_ACCT_CODES.ACCT_CODE_ID.ToString(), gL_ACCT_CODES.EFFECTIVE_START_DT.ToString("dd/MMM/yyyy"), gL_ACCT_CODES.EFFECTIVE_END_DT.ToString(), int.Parse(gL_ACCT_CODES.ENABLED_FLAG.ToString()));

                //if (ProReturn_Valdn != string.Empty)
                //{
                //    if (ProReturn_Valdn != "0")
                //    {

                //        ErrorCollection.Add("ACCOUNTCODES", ProReturn_Valdn);
                //        if (ErrorCollection.Count > 0)
                //        {
                //            return;
                //        }
                //    }

                //}

                ProReturn = FINSP.GetSPFOR_ERR_MGR_CATEGORY_TWO(Master.FormCode, gL_COMPANIES_HDR.COMP_ID, gL_COMPANIES_HDR.COMP_INTERNAL_NAME, gL_COMPANIES_HDR.COMP_COUNTRY);
                if (ProReturn != string.Empty)
                {
                    if (ProReturn != "0")
                    {
                        ErrorCollection.Add("CMPNAME", ProReturn);
                        if (ErrorCollection.Count > 0)
                        {
                            return;
                        }
                    }
                }
                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            CommonUtils.SavePCEntity<GL_COMPANIES_HDR, GL_COMPANIES_DTL>(gL_COMPANIES_HDR, tmpChildEntity, gL_COMPANIES_DTL);
                            savedBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {

                            CommonUtils.SavePCEntity<GL_COMPANIES_HDR, GL_COMPANIES_DTL>(gL_COMPANIES_HDR, tmpChildEntity, gL_COMPANIES_DTL, true);
                            savedBool = true;
                            break;
                        }
                }

                if (Session["CompCalDet"] == null)
                    return;

                DataTable dt_CalDet = (DataTable)Session["CompCalDet"];

                DataTable dt_comp_CalDet = (DataTable)Session["CompCalPeriodDet"];
                for (int iLoop = 0; iLoop < dt_CalDet.Rows.Count; iLoop++)
                {
                    GL_COMP_ACCT_CALENDAR_DTL gL_COMP_ACCT_CALENDAR_DTL = new GL_COMP_ACCT_CALENDAR_DTL();
                    GL_COMP_ACCT_PERIOD_DTL gL_COMP_ACCT_PERIOD_DTL = new GL_COMP_ACCT_PERIOD_DTL();

                    if (dt_CalDet.Rows[iLoop]["PK_ID"].ToString() != "0")
                    {
                        using (IRepository<GL_COMP_ACCT_CALENDAR_DTL> userCtx = new DataRepository<GL_COMP_ACCT_CALENDAR_DTL>())
                        {
                            gL_COMP_ACCT_CALENDAR_DTL = userCtx.Find(r =>
                                (r.PK_ID == int.Parse(dt_CalDet.Rows[iLoop]["PK_ID"].ToString()))
                                ).SingleOrDefault();
                        }
                        if (dt_CalDet.Rows[iLoop]["SELECTED"].ToString() == "FALSE")
                        {
                            DBMethod.DeleteEntity<GL_COMP_ACCT_CALENDAR_DTL>(gL_COMP_ACCT_CALENDAR_DTL);
                        }
                        else
                        {

                            if (dt_comp_CalDet != null)
                            {
                                DataRow[] dr = dt_comp_CalDet.Select("CAL_DTL_ID='" + dt_CalDet.Rows[iLoop]["CAL_DTL_ID"].ToString() + "'");
                                if (dr == null)
                                {
                                    if (dr.Length == 0)
                                    {
                                        DataTable dt_tmp_calperdet = DBMethod.ExecuteQuery(FIN.DAL.GL.Organisation_DAL.getOrgCalPeriodDetails(Master.StrRecordId, dt_CalDet.Rows[iLoop]["CAL_DTL_ID"].ToString())).Tables[0];
                                        dr = dt_comp_CalDet.Select("CAL_DTL_ID='" + dt_CalDet.Rows[iLoop]["CAL_DTL_ID"].ToString() + "'");
                                    }
                                }
                                for (int kLoop = 0; kLoop < dr.Length; kLoop++)
                                {
                                    //gL_COMP_ACCT_PERIOD_DTL.PK_ID = DBMethod.GetPrimaryKeyValue("COMPANYCALPERDTL");

                                    gL_COMP_ACCT_PERIOD_DTL.COMP_ID = gL_COMPANIES_HDR.COMP_ID;
                                    gL_COMP_ACCT_PERIOD_DTL.PERIOD_ID = dr[kLoop]["PERIOD_ID"].ToString();
                                    gL_COMP_ACCT_PERIOD_DTL.PERIOD_NAME = dr[kLoop]["PERIOD_NAME"].ToString();
                                    gL_COMP_ACCT_PERIOD_DTL.PERIOD_TYPE = dr[kLoop]["PERIOD_TYPE"].ToString();
                                    gL_COMP_ACCT_PERIOD_DTL.PERIOD_FROM_DT = Convert.ToDateTime(dr[kLoop]["PERIOD_FROM_DT"].ToString());
                                    gL_COMP_ACCT_PERIOD_DTL.PERIOD_TO_DT = Convert.ToDateTime(dr[kLoop]["PERIOD_TO_DT"].ToString());
                                    gL_COMP_ACCT_PERIOD_DTL.PERIOD_STATUS = dr[kLoop]["LOOKUP_ID"].ToString();
                                    gL_COMP_ACCT_PERIOD_DTL.CAL_DTL_ID = dr[kLoop]["CAL_DTL_ID"].ToString();
                                    gL_COMP_ACCT_PERIOD_DTL.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                                    gL_COMP_ACCT_PERIOD_DTL.WORKFLOW_COMPLETION_STATUS = FINAppConstants.Y;
                                    gL_COMP_ACCT_PERIOD_DTL.CREATED_BY = this.LoggedUserName;
                                    gL_COMP_ACCT_PERIOD_DTL.CREATED_DATE = DateTime.Now;
                                    gL_COMP_ACCT_PERIOD_DTL.PERIOD_NUMBER = decimal.Parse(dr[kLoop]["PERIOD_NUMBER"].ToString());
                                    if (dr[kLoop]["PK_ID"].ToString() == "0")
                                    {
                                        DBMethod.SaveEntity<GL_COMP_ACCT_PERIOD_DTL>(gL_COMP_ACCT_PERIOD_DTL);
                                    }
                                    else
                                    {
                                        DBMethod.SaveEntity<GL_COMP_ACCT_PERIOD_DTL>(gL_COMP_ACCT_PERIOD_DTL, true);
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        if (dt_CalDet.Rows[iLoop]["SELECTED"].ToString() == "TRUE")
                        {
                            gL_COMP_ACCT_CALENDAR_DTL.PK_ID = DBMethod.GetPrimaryKeyValue("COMPANYCALDTL");
                            gL_COMP_ACCT_CALENDAR_DTL.COMP_ID = gL_COMPANIES_HDR.COMP_ID;
                            gL_COMP_ACCT_CALENDAR_DTL.CREATED_BY = this.LoggedUserName;
                            gL_COMP_ACCT_CALENDAR_DTL.CREATED_DATE = DateTime.Now;
                            gL_COMP_ACCT_CALENDAR_DTL.ENABLED_FLAG = FINAppConstants.Y;
                            gL_COMP_ACCT_CALENDAR_DTL.WORKFLOW_COMPLETION_STATUS = FINAppConstants.Y;
                            gL_COMP_ACCT_CALENDAR_DTL.CAL_ACCT_YEAR = dt_CalDet.Rows[iLoop]["CAL_ACCT_YEAR"].ToString();
                            gL_COMP_ACCT_CALENDAR_DTL.CAL_ID = dt_CalDet.Rows[iLoop]["CAL_ID"].ToString();
                            gL_COMP_ACCT_CALENDAR_DTL.CAL_DTL_ID = dt_CalDet.Rows[iLoop]["CAL_DTL_ID"].ToString();
                            DBMethod.SaveEntity<GL_COMP_ACCT_CALENDAR_DTL>(gL_COMP_ACCT_CALENDAR_DTL);

                            if (dt_comp_CalDet != null)
                            {
                                DataRow[] dr = dt_comp_CalDet.Select("CAL_DTL_ID='" + dt_CalDet.Rows[iLoop]["CAL_DTL_ID"].ToString() + "'");
                                if (dr == null)
                                {
                                    if (dr.Length == 0)
                                    {
                                        DataTable dt_tmp_calperdet = DBMethod.ExecuteQuery(FIN.DAL.GL.Organisation_DAL.getOrgCalPeriodDetails(Master.StrRecordId, dt_CalDet.Rows[iLoop]["CAL_DTL_ID"].ToString())).Tables[0];
                                        dr = dt_comp_CalDet.Select("CAL_DTL_ID='" + dt_CalDet.Rows[iLoop]["CAL_DTL_ID"].ToString() + "'");
                                    }
                                }
                                for (int kLoop = 0; kLoop < dr.Length; kLoop++)
                                {
                                    gL_COMP_ACCT_PERIOD_DTL.PK_ID = DBMethod.GetPrimaryKeyValue("COMPANYCALPERDTL");
                                    gL_COMP_ACCT_PERIOD_DTL.COMP_ID = gL_COMPANIES_HDR.COMP_ID;
                                    gL_COMP_ACCT_PERIOD_DTL.PERIOD_ID = dr[kLoop]["PERIOD_ID"].ToString();
                                    gL_COMP_ACCT_PERIOD_DTL.PERIOD_NAME = dr[kLoop]["PERIOD_NAME"].ToString();
                                    gL_COMP_ACCT_PERIOD_DTL.PERIOD_TYPE = dr[kLoop]["PERIOD_TYPE"].ToString();
                                    gL_COMP_ACCT_PERIOD_DTL.PERIOD_FROM_DT = Convert.ToDateTime(dr[kLoop]["PERIOD_FROM_DT"].ToString());
                                    gL_COMP_ACCT_PERIOD_DTL.PERIOD_TO_DT = Convert.ToDateTime(dr[kLoop]["PERIOD_TO_DT"].ToString());
                                    gL_COMP_ACCT_PERIOD_DTL.PERIOD_STATUS = dr[kLoop]["LOOKUP_ID"].ToString();
                                    gL_COMP_ACCT_PERIOD_DTL.CAL_DTL_ID = dr[kLoop]["CAL_DTL_ID"].ToString();
                                    gL_COMP_ACCT_PERIOD_DTL.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                                    gL_COMP_ACCT_PERIOD_DTL.WORKFLOW_COMPLETION_STATUS = FINAppConstants.Y;
                                    gL_COMP_ACCT_PERIOD_DTL.CREATED_BY = this.LoggedUserName;
                                    gL_COMP_ACCT_PERIOD_DTL.CREATED_DATE = DateTime.Now;
                                    gL_COMP_ACCT_PERIOD_DTL.PERIOD_NUMBER = decimal.Parse(dr[kLoop]["PERIOD_NUMBER"].ToString());
                                    DBMethod.SaveEntity<GL_COMP_ACCT_PERIOD_DTL>(gL_COMP_ACCT_PERIOD_DTL);
                                }
                            }

                        }
                    }
                }


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_ATB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }


        private void FillComboBox()
        {
            LoadSegmentName();
            //  ComboFilling.fn_getSegmentName(ref ddlGlobseg);
            ComboFilling.getCountry(ref ddlCountry);

        }



        /// <summary>
        /// Bind the records into grid voew
        /// </summary>
        /// <param name="dtData">Contains the database entities and correspoding records which is used in the grid view</param>

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr["ENABLED_FLAG"] = "FALSE";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WRI_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>


        #endregion

        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>

        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = -1;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Row_cnl", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }

                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    else
                    {
                        dtGridData.Rows.Add(drList);
                        BindGrid(dtGridData);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Row_Cmd", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }



        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {

            System.Collections.SortedList slControls = new System.Collections.SortedList();
            DropDownList ddlBasecurrency = gvr.FindControl("ddlBasecurrency") as DropDownList;
            DropDownList ddlAcctStruct = gvr.FindControl("ddlAcctStruct") as DropDownList;
            DropDownList ddlAcctcal = gvr.FindControl("ddlAcctcal") as DropDownList;

            TextBox dtpStartDate = gvr.FindControl("dtpStartDate") as TextBox;
            TextBox dtpEndDate = gvr.FindControl("dtpEndDate") as TextBox;
            CheckBox chkact = gvr.FindControl("chkact") as CheckBox;

            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList[FINColumnConstants.PK_ID] = "0";
                drList[FINColumnConstants.COMP_DTL_ID] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }

            slControls[0] = ddlBasecurrency;
            slControls[1] = ddlAcctStruct;
            slControls[2] = ddlAcctcal;
            slControls[3] = dtpStartDate;
            slControls[4] = dtpStartDate;
            slControls[5] = dtpEndDate;

            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/GL_" + Session["Sel_Lng"].ToString() + ".properties"));
            ErrorCollection.Clear();
            string strCtrlTypes = "DropDownList~DropDownList~DropDownList~TextBox~DateTime~DateRangeValidate";  // "TextBox~TextBox~DateTime";
            string strMessage = Prop_File_Data["Base_Currency_P"] + " ~ " + Prop_File_Data["Account_Structure_P"] + " ~ " + Prop_File_Data["Accounting_Calendar_P"] + " ~ " + Prop_File_Data["Effective_Date_P"] + " ~ " + Prop_File_Data["End_Date_P"] + "";
            //string strMessage = "Base Currency ~ Account Structure ~ Accounting Calendar ~ Effective Date ~ Effective Date ~ End Date";

            EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

            if (EmptyErrorCollection.Count > 0)
            {
                ErrorCollection = EmptyErrorCollection;
                return drList;
            }



            //DateTime PSGDate = DBMethod.ConvertStringToDate(dtpPeriodStartDateGrid.Text);
            //if (dtpPeriodEndDateGrid.Text.ToString().Length > 0)
            //{
            //    DateTime PEGDate = DBMethod.ConvertStringToDate(dtpPeriodEndDateGrid.Text);

            //    if ((PSGDate - PEGDate).TotalDays > 0)
            //    {
            //        ErrorCollection.Add("FromTODate", "From Date Must Be Greater then To Date");
            //        return drList;
            //    }

            //}
            string strCondition = FINColumnConstants.CURRENCY_ID + "='" + ddlBasecurrency.SelectedValue.ToString() + "'";
            strCondition += " AND " + FINColumnConstants.ACCT_STRUCT_ID + "='" + ddlAcctStruct.SelectedValue.ToString() + "'";
            strCondition += " AND " + FINColumnConstants.CAL_ID + "='" + ddlAcctcal.SelectedItem.Value.ToString() + "'";

            strMessage = FINMessageConstatns.RecordAlreadyExists;
            ErrorCollection = Organisation_BLL.DataDuplication(dt_tmp, strCondition, strMessage, DBMethod.ConvertStringToDate(dtpStartDate.Text), DBMethod.ConvertStringToDate(dtpEndDate.Text), ddlBasecurrency.SelectedValue.ToString(), ddlAcctStruct.SelectedValue.ToString(), ddlAcctcal.SelectedValue.ToString());
            if (ErrorCollection.Count > 0)
            {
                return drList;
            }



            //string strCondition = "EFFECTIVE_END_DT IS NULL";
            //strMessage = FINMessageConstatns.RecordAlreadyExists;
            //ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            //if (ErrorCollection.Count > 0)
            //{

            //    return drList;
            //}

            if (ddlAcctcal.SelectedItem != null)
            {
                drList[FINColumnConstants.CURRENCY_ID] = ddlBasecurrency.SelectedItem.Value;
                drList[FINColumnConstants.CURRENCY_NAME] = ddlBasecurrency.SelectedItem.Text;
            }

            if (ddlAcctStruct.SelectedItem != null)
            {
                drList[FINColumnConstants.ACCT_STRUCT_ID] = ddlAcctStruct.SelectedItem.Value;
                drList[FINColumnConstants.ACCT_STRUCT_NAME] = ddlAcctStruct.SelectedItem.Text;
            }
            if (ddlAcctcal.SelectedItem != null)
            {
                drList[FINColumnConstants.CAL_ID] = ddlAcctcal.SelectedItem.Value;
                drList[FINColumnConstants.CAL_DESC] = ddlAcctcal.SelectedItem.Text;
            }


            if (dtpStartDate.Text.ToString().Length > 0)
            {

                drList[FINColumnConstants.EFFECTIVE_START_DT] = dtpStartDate.Text;

            }
            else
            {
                drList[FINColumnConstants.EFFECTIVE_START_DT] = DBNull.Value;
            }



            if (dtpEndDate.Text.ToString().Length > 0)
            {

                drList[FINColumnConstants.EFFECTIVE_END_DT] = dtpEndDate.Text;
            }
            else
            {
                drList[FINColumnConstants.EFFECTIVE_END_DT] = DBNull.Value;
            }
            if (chkact.Checked)
            {
                drList[FINColumnConstants.ENABLED_FLAG] = "TRUE";
            }
            else
            {
                drList[FINColumnConstants.ENABLED_FLAG] = "FALSE";
            }



            drList[FINColumnConstants.DELETED] = FINAppConstants.N;

            return drList;
        }

        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                //else if (FacilityMasterBLL.ErrorCollection.Count > 0)
                //{
                //    ErrorCollection = FacilityMasterBLL.ErrorCollection;
                //    return;
                //}
                else
                {
                    gvData.EditIndex = -1;
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Row_upd", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row delete mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewDeleteEventArgs indicates which row's delete button was clicked. </param>


        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                dtGridData.Rows[e.RowIndex][FINColumnConstants.DELETED] = FINAppConstants.Y;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Row_del", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Row_Edt", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("FacilityMasterEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }


        /// <summary>
        /// Used to delete the exam master and detail table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {


                    if (dtGridData.Rows[iLoop][FINColumnConstants.SEGMENT_VALUE_ID].ToString() != "0")
                    {
                        using (IRepository<GL_COMPANIES_DTL> userCtx = new DataRepository<GL_COMPANIES_DTL>())
                        {
                            gL_COMPANIES_DTL = userCtx.Find(r =>
                                (r.COMP_DTL_ID == dtGridData.Rows[iLoop][FINColumnConstants.COMP_DTL_ID].ToString())
                                ).SingleOrDefault();
                        }
                    }


                    gL_COMPANIES_DTL.COMP_DTL_ID = dtGridData.Rows[iLoop]["COMP_DTL_ID"].ToString();
                    DBMethod.DeleteEntity<GL_COMPANIES_DTL>(gL_COMPANIES_DTL);
                }

                gL_COMPANIES_HDR.PK_ID = short.Parse(Master.StrRecordId.ToString());
                DBMethod.DeleteEntity<GL_COMPANIES_HDR>(gL_COMPANIES_HDR);
                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATADELETED);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Btn_ys_clik", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }

        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddlAcctStruct = tmpgvr.FindControl("ddlAcctStruct") as DropDownList;
                ComboFilling.getStructName(ref ddlAcctStruct);

                DropDownList ddlAcctcal = tmpgvr.FindControl("ddlAcctcal") as DropDownList;
                ComboFilling.getCalendar(ref ddlAcctcal);

                DropDownList ddlBasecurrency = tmpgvr.FindControl("ddlBasecurrency") as DropDownList;
                ComboFilling.fn_getCurrencyDetails(ref ddlBasecurrency);

                if (gvData.EditIndex >= 0 && tmpgvr.RowType != DataControlRowType.Footer)
                {
                    ddlAcctStruct.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.ACCT_STRUCT_ID].ToString();
                    ddlAcctcal.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.CAL_ID].ToString();
                    ddlBasecurrency.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.CURRENCY_ID].ToString();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void cmbCalendarTypeId_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (bol_rowVisiable)
                    e.Row.Visible = false;

                if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                {
                    e.Row.Visible = false;
                }

            }
        }

        #endregion

        protected void btnSave_Click1(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                AssignToBE();

                //ProReturn_Valdn = FINSP.GetSPFOR_ACTIVE_FROM_END_DATE(Master.FormCode, gL_ACCT_CODES.ACCT_CODE_ID.ToString(), gL_ACCT_CODES.EFFECTIVE_START_DT.ToString("dd/MMM/yyyy"), gL_ACCT_CODES.EFFECTIVE_END_DT.ToString(), int.Parse(gL_ACCT_CODES.ENABLED_FLAG.ToString()));

                //if (ProReturn_Valdn != string.Empty)
                //{
                //    if (ProReturn_Valdn != "0")
                //    {

                //        ErrorCollection.Add("ACCOUNTCODES", ProReturn_Valdn);
                //        if (ErrorCollection.Count > 0)
                //        {
                //            return;
                //        }
                //    }

                //}
                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);
                }

                //switch (Master.Mode)
                //{
                //    case FINAppConstants.Add:
                //        {
                //            DBMethod.SaveEntity<GL_COMPANIES_HDR>(gL_COMPANIES_HDR);
                //            break;
                //        }
                //    case FINAppConstants.Update:
                //        {

                //            DBMethod.SaveEntity<GL_COMPANIES_HDR>(gL_COMPANIES_HDR, true);
                //            break;
                //        }
                //}
            }


            catch (Exception ex)
            {
                ErrorCollection.Add("Org_btn_sve", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void txtExternalName_TextChanged(object sender, EventArgs e)
        {

        }

        protected void txtAddress2_TextChanged(object sender, EventArgs e)
        {

        }

        protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void btnYearClick(GridViewRow gvr)
        {
            try
            {
                DropDownList ddl_Acctcal = (DropDownList)gvr.FindControl("ddlAcctcal");


                if (ddl_Acctcal != null)
                {
                    if (ddl_Acctcal.SelectedValue.ToString().Length > 0)
                    {
                        hf_Acctcal.Value = ddl_Acctcal.SelectedValue.ToString();

                        DataTable dtAccYr = new DataTable();
                        DataTable dt_CompCal = new DataTable();
                        //if (gvr.RowType == DataControlRowType.Footer)
                        //{
                        //    dtAccYr = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountingCalendar_DAL.GetAccountYear(Master.StrRecordId, ddl_Acctcal.SelectedValue.ToString())).Tables[0];
                        //}
                        //else
                        //{
                        DataRow[] dr = null;
                        if (Session["CompCalDet"] != null)
                        {
                            dt_CompCal = (DataTable)Session["CompCalDet"];
                            if (dt_CompCal != null)
                            {
                                if (dt_CompCal.Rows.Count > 0)
                                    dr = dt_CompCal.Select("CAL_ID='" + ddl_Acctcal.SelectedValue.ToString() + "'");
                            }
                        }
                        if (dr == null)
                        {
                            dtAccYr = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountingCalendar_DAL.GetAccountYear(Master.StrRecordId, ddl_Acctcal.SelectedValue.ToString())).Tables[0];
                        }
                        else if (dr.Length == 0)
                        {
                            dtAccYr = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountingCalendar_DAL.GetAccountYear(Master.StrRecordId, ddl_Acctcal.SelectedValue.ToString())).Tables[0];
                        }
                        //}
                        if (Session["CompCalDet"] == null)
                        {
                            Session["CompCalDet"] = dtAccYr;
                            dt_CompCal = dtAccYr;
                        }
                        else
                        {
                            dt_CompCal = (DataTable)Session["CompCalDet"];
                            if (dtAccYr.Rows.Count > 0)
                            {
                                dt_CompCal.Merge(dtAccYr);
                            }
                        }
                        gvCalDet.DataSource = dt_CompCal;
                        gvCalDet.DataBind();
                        mpeCalDet.Show();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        protected void btnYR_Click(object sender, EventArgs e)
        {

            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            btnYearClick(gvr);

        }

        private void PeriodClick(GridViewRow gvr)
        {
            try
            {

                DropDownList ddl_Acctcal = (DropDownList)gvr.FindControl("ddlAcctcal");


                if (ddl_Acctcal != null)
                {
                    if (ddl_Acctcal.SelectedValue.ToString().Length > 0)
                    {
                        if (Session["CompCalDet"] == null)
                        {
                            // ErrorCollection.Add("InvalidCalDet", "Please Select The Year");
                            //return;
                            btnYearClick(gvr);
                        }
                        DataTable dt_CompCal = (DataTable)Session["CompCalDet"];
                        DataRow[] dr = null;
                        if (dt_CompCal != null)
                        {
                            if (dt_CompCal.Rows.Count > 0)
                                dr = dt_CompCal.Select("CAL_ID='" + ddl_Acctcal.SelectedValue.ToString() + "' AND SELECTED='TRUE'");
                        }
                        if (dr != null)
                        {
                            if (dr.Length > 0)
                            {
                                string str_CalDetId = "";
                                ddlCalendarYear.Items.Clear();
                                for (int iLoop = 0; iLoop < dr.Length; iLoop++)
                                {
                                    str_CalDetId += "'" + dr[iLoop]["CAL_DTL_ID"].ToString() + "',";
                                    ddlCalendarYear.Items.Add(new ListItem(dr[iLoop]["CAL_ACCT_YEAR"].ToString(), dr[iLoop]["CAL_DTL_ID"].ToString()));

                                }
                                ddlCalendarYear_SelectedIndexChanged(ddlCalendarYear, new EventArgs());
                                str_CalDetId = str_CalDetId + '0';
                                //AccountingCalendar_BLL.GetFinancialYear(ref ddlCalendarYear);
                                mpeCalPeriod.Show();
                            }

                        }
                        else
                        {
                            btnYearClick(gvr);
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void btnPeriod_Click(object sender, EventArgs e)
        {
            try
            {
                GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
                PeriodClick(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        protected void gvCalDet_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DataRowView tmp_erv = (DataRowView)e.Row.DataItem;
                    if ((tmp_erv.Row["CAL_ID"].ToString() != hf_Acctcal.Value.ToString()))
                    {
                        e.Row.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        protected void Button1_Click(object sender, EventArgs e)
        {

            DataTable dt_InvTax = (DataTable)Session["CompCalDet"];
            for (int iLoop = 0; iLoop < gvCalDet.Rows.Count; iLoop++)
            {
                CheckBox chk_Selected = (CheckBox)gvCalDet.Rows[iLoop].FindControl("chkSelected");

                DataRow[] dr = dt_InvTax.Select("CAL_ID='" + hf_Acctcal.Value + "'  and CAL_DTL_ID='" + gvCalDet.DataKeys[iLoop].Values["CAL_DTL_ID"].ToString() + "'");
                if (chk_Selected.Checked)
                {

                    dr[0]["SELECTED"] = "TRUE";

                }
                else
                {
                    dr[0]["SELECTED"] = "FALSE";
                }
            }
            Session["CompCalDet"] = dt_InvTax;
            mpeCalDet.Hide();
        }

        protected void ddlCalendarYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            ErrorCollection.Clear();
            mpeCalPeriod.Show();


            if (ddlCalendarYear.SelectedValue.ToString().Length > 0)
            {

                DataTable dtAccYr = new DataTable();
                DataTable dt_CompCal = new DataTable();
                //if (gvr.RowType == DataControlRowType.Footer)
                //{
                //    dtAccYr = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountingCalendar_DAL.GetAccountYear(Master.StrRecordId, ddl_Acctcal.SelectedValue.ToString())).Tables[0];
                //}
                //else
                //{
                hfCalYear.Value = ddlCalendarYear.SelectedValue.ToString();
                DataRow[] dr = null;
                if (Session["CompCalPeriodDet"] != null)
                {
                    dt_CompCal = (DataTable)Session["CompCalPeriodDet"];
                    if (dt_CompCal != null)
                    {
                        if (dt_CompCal.Rows.Count > 0)
                            dr = dt_CompCal.Select("CAL_DTL_ID='" + ddlCalendarYear.SelectedValue.ToString() + "'");
                    }
                }
                if (dr == null)
                {
                    dtAccYr = DBMethod.ExecuteQuery(FIN.DAL.GL.Organisation_DAL.getOrgCalPeriodDetails(Master.StrRecordId, ddlCalendarYear.SelectedValue.ToString())).Tables[0];
                }
                else if (dr.Length == 0)
                {
                    dtAccYr = DBMethod.ExecuteQuery(FIN.DAL.GL.Organisation_DAL.getOrgCalPeriodDetails(Master.StrRecordId, ddlCalendarYear.SelectedValue.ToString())).Tables[0];
                }
                //}
                if (Session["CompCalPeriodDet"] == null)
                {
                    Session["CompCalPeriodDet"] = dtAccYr;
                    dt_CompCal = dtAccYr;
                }
                else
                {
                    dt_CompCal = (DataTable)Session["CompCalPeriodDet"];
                    if (dtAccYr.Rows.Count > 0)
                    {
                        dt_CompCal.Merge(dtAccYr);
                    }
                }


                gvperiods.DataSource = dt_CompCal;
                gvperiods.DataBind();

                mpeCalPeriod.Show();
            }

        }



        protected void gvperiodData_RowCommand(object sender, GridViewCommandEventArgs e)
        {

        }

        protected void gvperiod_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (bol_rowVisiable)
                    e.Row.Visible = false;

                if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                {
                    e.Row.Visible = false;
                }
                if (((DataRowView)e.Row.DataItem).Row["CAL_DTL_ID"].ToString() != hfCalYear.Value.ToString())
                {
                    e.Row.Visible = false;
                }
                DropDownList ddlStatus = e.Row.FindControl("ddlStatus") as DropDownList;
                Lookup_BLL.GetLookUpValues(ref ddlStatus, "CPS");
                ddlStatus.SelectedValue = gvperiods.DataKeys[e.Row.RowIndex].Values["LOOKUP_ID"].ToString();
                if (gvperiods.DataKeys[e.Row.RowIndex].Values["LOOKUP_ID"].ToString() == "AUDITED")
                {
                    ddlStatus.Enabled = false;
                }
                if (gvperiods.DataKeys[e.Row.RowIndex].Values["LOOKUP_ID"].ToString() == "OPEN")
                {
                    ddlStatus.Items.Remove(new ListItem("NEVEROPEN", "NOP"));
                }
            }
        }

        protected void ddlStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            DropDownList ddl_Status = (DropDownList)gvr.FindControl("ddlStatus");
            string str_period_id = gvperiods.DataKeys[gvr.RowIndex].Values["PERIOD_ID"].ToString();
            DataTable dt_PeriodDet = (DataTable)Session["CompCalPeriodDet"];
            DataRow[] dr = dt_PeriodDet.Select("PERIOD_ID='" + str_period_id + "'  and CAL_DTL_ID='" + ddlCalendarYear.SelectedValue.ToString() + "'");
            if (dr != null)
                if (dr.Length > 0)
                {
                    dr[0]["LOOKUP_ID"] = ddl_Status.SelectedValue.ToString();
                    dr[0]["LOOKUP_NAME"] = ddl_Status.SelectedItem.Text.ToString();
                }

            mpeCalPeriod.Show();

        }



        protected void btnPeriodOk_Click(object sender, EventArgs e)
        {

        }

        protected void chkDependent_CheckedChanged(object sender, EventArgs e)
        {
            LoadSegmentName();
        }
        private void LoadSegmentName()
        {
            if (chkDependent.Checked)
            {
                Segments_BLL.getSegmentName4Type(ref ddlGlobseg, FINAppConstants.Y, Master.Mode);
            }
            else
            {
                Segments_BLL.getSegmentName4Type(ref ddlGlobseg, FINAppConstants.N, Master.Mode);
            }
        }

        protected void rblYN_SelectedIndexChanged(object sender, EventArgs e)
        {
            validate_rb_glblseg();
        }

        private void validate_rb_glblseg()
        {
            if (rblYN.SelectedValue == "1")
            {
                ddlGlobseg.Enabled = true;
            }
            else
            {
                ddlGlobseg.Enabled = false;
                ddlGlobseg.SelectedValue = "";
            }


            DataTable dtglobalseg = new DataTable();
            dtglobalseg = DBMethod.ExecuteQuery(FIN.DAL.GL.Organisation_DAL.getJournalGlobalSegment()).Tables[0];

            if (int.Parse(dtglobalseg.Rows[0][0].ToString()) > 0)
            {
                ddlGlobseg.Enabled = false;
            }
            else
            {
                ddlGlobseg.Enabled = true;
            }


        }

    }
}
