﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using FIN.DAL;
using FIN.BLL.CA;
using FIN.BLL;
using FIN.BLL.GL;
using VMVServices.Web;
using FIN.DAL.GL;
using VMVServices.Services.Data;
namespace FIN.Client.GL
{
    public partial class Payment_GL : PageBase
    {

        //PO_HEADERS PO_HEADERS = new PO_HEADERS(); 
        //PO_LINES PO_LINES = new PO_LINES();
        GL_PAYMENT_HDR gL_PAYMENT_HDR = new GL_PAYMENT_HDR();
        GL_PAYMENT_DTL gL_PAYMENT_DTL = new GL_PAYMENT_DTL();
        DataTable dtGridData = new DataTable();
        Boolean bol_rowVisiable;
        Boolean savedBool;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }
            UserRightsChecking();

        }
        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }
        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>
        private void FillComboBox()
        {
            Bank_BLL.fn_getBankName(ref ddlBank);
            Bank_BLL.fn_getBankBranchAcctNo(ref ddlBankAccount);
            Segments_BLL.GetGlobalSegmentvalues(ref ddlCostCenter, VMVServices.Web.Utils.OrganizationID);
        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>
        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();

                Session["PaymentNo"] = null;
                EntityData = null;
                Session[FINSessionConstants.GridData] = null;
                txtPaydate.Text = DBMethod.ConvertDateToString(DateTime.Now.ToShortDateString());

                dtGridData = DBMethod.ExecuteQuery(FIN.DAL.GL.PaymentGL_DAL.getGLPaymentdtl(Master.StrRecordId)).Tables[0];

                imgBtnPost.Visible = false;
                imgBtnJVPrint.Visible = false;
                btnPrintReport.Visible = false;
                imgbtnCheckPrint.Visible = false;
                btnStopPayment.Visible = false;
                imgBtnStopPayJV.Visible = false;




                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    using (IRepository<GL_PAYMENT_HDR> userCtx = new DataRepository<GL_PAYMENT_HDR>())
                    {
                        gL_PAYMENT_HDR = userCtx.Find(r =>
                            (r.PAY_HDR_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = gL_PAYMENT_HDR;
                    btnPrintReport.Visible = true;

                    txtPayNo.Text = gL_PAYMENT_HDR.PAYMENT_NO;
                    if (gL_PAYMENT_HDR.PAYMENT_DATE != null)
                    {
                        txtPaydate.Text = DBMethod.ConvertDateToString(gL_PAYMENT_HDR.PAYMENT_DATE.ToString());
                    }

                    ddlBankAccount.SelectedValue = gL_PAYMENT_HDR.ACCT_NUMBER.ToString();
                    ddlBank.SelectedValue = gL_PAYMENT_HDR.BANK_NAME.ToString();
                    FillBankDetails();
                    ddlBranch.SelectedValue = gL_PAYMENT_HDR.BRANCH.ToString();
                    FillBranchDetails();
                    ddlAcctNo.SelectedValue = gL_PAYMENT_HDR.ACCT_NUMBER.ToString();
                    ddlCostCenter.SelectedValue = gL_PAYMENT_HDR.COST_CENTRE;
                    txtAmt.Text = gL_PAYMENT_HDR.AMOUNT.ToString();
                    txtAmt.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtAmt.Text);
                    txtRemarks.Text = gL_PAYMENT_HDR.REMARKS;
                    hf_posted.Value = FINAppConstants.N;

                    Session["PaymentNo"] = gL_PAYMENT_HDR.PAYMENT_NO;

                    Cheque_BLL.GetUnusedCheck(ref ddlChkNo, ddlBank.SelectedValue, ddlBranch.SelectedValue, ddlAcctNo.SelectedValue, Master.Mode);

                    ddlChkNo.SelectedValue = gL_PAYMENT_HDR.CHECK_DTL_ID;
                    if (gL_PAYMENT_HDR.CHECK_DATE != null)
                        txtChequeDate.Text = DBMethod.ConvertDateToString(gL_PAYMENT_HDR.CHECK_DATE.ToString());

                    if (gL_PAYMENT_HDR.WORKFLOW_COMPLETION_STATUS == "1")
                    {

                        imgBtnPost.Visible = true;
                        lblPosted.Visible = false;
                        lblCancelled.Visible = false;
                        btnStopPayment.Visible = false;
                    }


                    if (gL_PAYMENT_HDR.POSTED_FLAG == FINAppConstants.Y)
                    {
                        btnSave.Visible = false;
                        hf_posted.Value = FINAppConstants.Y;
                        imgBtnPost.Visible = false;
                        imgBtnJVPrint.Visible = true;
                        imgbtnCheckPrint.Visible = true;
                        btnStopPayment.Visible = true;

                        lblPosted.Visible = true;
                        lblCancelled.Visible = false;

                        pnlgridview.Enabled = false;
                        pnltdHeader.Enabled = false;
                    }
                    else
                    {
                        imgBtnPost.Visible = true;
                    }
                    ddlChkNo.Enabled = false;

                    if (gL_PAYMENT_HDR.REV_FLAG == FINAppConstants.Y)
                    {
                        btnStopPayment.Visible = false;
                        imgbtnCheckPrint.Visible = false;
                        imgBtnStopPayJV.Visible = true;

                        lblCancelled.Visible = true;


                    }
                }
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;

                if (dtData.Rows.Count > 0)
                {
                    txtAmt.Text = CommonUtils.CalculateTotalAmount(dtData, "AMOUNT");
                    txtAmt.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtAmt.Text);
                }

                if (dtData.Rows.Count > 0)
                {
                    dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("AMOUNT", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("AMOUNT"))));
                    dtData.AcceptChanges();
                }
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);
                if (hf_posted.Value == FINAppConstants.Y)
                {
                    gvData.FooterRow.Visible = false;
                    gvData.Columns[0].Visible = false;
                }


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("SQEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddlChqNo = tmpgvr.FindControl("ddlChqNo") as DropDownList;

                DropDownList ddlAccCode = tmpgvr.FindControl("ddlAccCode") as DropDownList;

                FIN.BLL.CA.PettyCashExpenditure_BLL.GetAccountdtls(ref ddlAccCode);


                //PettyCashAllocation_BLL.GetChequeNo(ref ddlChqNo);

                Cheque_BLL.GetUnusedCheck(ref ddlChqNo, ddlBank.SelectedValue, ddlBranch.SelectedValue, ddlAcctNo.SelectedValue, FINAppConstants.Add);




                if (gvData.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                {

                    ddlChqNo.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["CHECK_DTL_ID"].ToString();
                    ddlAccCode.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.ACCT_CODE_ID].ToString();
                    if (gvData.DataKeys[gvData.EditIndex].Values["PAY_DTL_ID"].ToString() != "0" && gvData.DataKeys[gvData.EditIndex].Values["PAY_DTL_ID"].ToString() != string.Empty)
                    {
                        Cheque_BLL.GetUnusedCheck(ref ddlChqNo, ddlBank.SelectedValue, ddlBranch.SelectedValue, ddlAcctNo.SelectedValue, FINAppConstants.Update);
                        ddlChqNo.Enabled = false;
                    }
                    else
                    {
                        Cheque_BLL.GetUnusedCheck(ref ddlChqNo, ddlBank.SelectedValue, ddlBranch.SelectedValue, ddlAcctNo.SelectedValue, FINAppConstants.Add);
                    }
                    BindSegment(tmpgvr);
                    if (gvData.DataKeys[gvData.EditIndex].Values["JE_SEGMENT_ID_1"].ToString() != "0" && gvData.DataKeys[gvData.EditIndex].Values["JE_SEGMENT_ID_1"].ToString() != string.Empty)
                    {
                        DropDownList ddl_Segment1 = tmpgvr.FindControl("ddlSegment1") as DropDownList;
                        ddl_Segment1.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["JE_SEGMENT_ID_1"].ToString();
                        LoadSegmentValues(ddl_Segment1);
                    }

                    if (gvData.DataKeys[gvData.EditIndex].Values["JE_SEGMENT_ID_2"].ToString() != "0" && gvData.DataKeys[gvData.EditIndex].Values["JE_SEGMENT_ID_2"].ToString() != string.Empty)
                    {
                        DropDownList ddl_Segment2 = tmpgvr.FindControl("ddlSegment2") as DropDownList;
                        ddl_Segment2.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["JE_SEGMENT_ID_2"].ToString();
                        LoadSegmentValues(ddl_Segment2);
                    }

                    if (gvData.DataKeys[gvData.EditIndex].Values["JE_SEGMENT_ID_3"].ToString() != "0" && gvData.DataKeys[gvData.EditIndex].Values["JE_SEGMENT_ID_3"].ToString() != string.Empty)
                    {
                        DropDownList ddl_Segment3 = tmpgvr.FindControl("ddlSegment3") as DropDownList;
                        ddl_Segment3.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["JE_SEGMENT_ID_3"].ToString();
                        LoadSegmentValues(ddl_Segment3);
                    }



                    if (gvData.DataKeys[gvData.EditIndex].Values["JE_SEGMENT_ID_4"].ToString() != "0" && gvData.DataKeys[gvData.EditIndex].Values["JE_SEGMENT_ID_4"].ToString() != string.Empty)
                    {
                        DropDownList ddl_Segment4 = tmpgvr.FindControl("ddlSegment4") as DropDownList;
                        ddl_Segment4.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["JE_SEGMENT_ID_4"].ToString();
                        LoadSegmentValues(ddl_Segment4);
                    }
                    if (gvData.DataKeys[gvData.EditIndex].Values["JE_SEGMENT_ID_5"].ToString() != "0" && gvData.DataKeys[gvData.EditIndex].Values["JE_SEGMENT_ID_5"].ToString() != string.Empty)
                    {
                        DropDownList ddl_Segment5 = tmpgvr.FindControl("ddlSegment5") as DropDownList;
                        ddl_Segment5.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["JE_SEGMENT_ID_5"].ToString();
                        LoadSegmentValues(ddl_Segment5);
                    }
                    if (gvData.DataKeys[gvData.EditIndex].Values["JE_SEGMENT_ID_6"].ToString() != "0" && gvData.DataKeys[gvData.EditIndex].Values["JE_SEGMENT_ID_6"].ToString() != string.Empty)
                    {
                        DropDownList ddl_Segment6 = tmpgvr.FindControl("ddlSegment6") as DropDownList;
                        ddl_Segment6.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["JE_SEGMENT_ID_6"].ToString();
                        LoadSegmentValues(ddl_Segment6);
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("SQ_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            if (Session[FINSessionConstants.GridData] != null)
            {
                dtGridData = (DataTable)Session[FINSessionConstants.GridData];
            }
            gvData.EditIndex = -1;

            BindGrid(dtGridData);

        }
        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {
            System.Collections.SortedList slControls = new System.Collections.SortedList();

            TextBox dtpCheqDate = gvr.FindControl("dtpCheqDate") as TextBox;
            DropDownList ddlChqNo = gvr.FindControl("ddlChqNo") as DropDownList;
            TextBox txtAmount = gvr.FindControl("txtAmount") as TextBox;
            DropDownList ddlAccCode = gvr.FindControl("ddlAccCode") as DropDownList;

            TextBox txtBenfName = gvr.FindControl("txtBenfName") as TextBox;
            TextBox txtReasonfrpayment = gvr.FindControl("txtReasonfrpayment") as TextBox;


            DropDownList ddl_Segment1 = gvr.FindControl("ddlSegment1") as DropDownList;
            DropDownList ddl_Segment2 = gvr.FindControl("ddlSegment2") as DropDownList;
            DropDownList ddl_Segment3 = gvr.FindControl("ddlSegment3") as DropDownList;
            DropDownList ddl_Segment4 = gvr.FindControl("ddlSegment4") as DropDownList;
            DropDownList ddl_Segment5 = gvr.FindControl("ddlSegment5") as DropDownList;
            DropDownList ddl_Segment6 = gvr.FindControl("ddlSegment6") as DropDownList;


            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList["PAY_DTL_ID"] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }


            // slControls[0] = ddlChqNo;
            //slControls[1] = dtpCheqDate;
            slControls[0] = ddlAccCode;
            slControls[1] = txtAmount;
            slControls[2] = txtBenfName;
            slControls[3] = ddl_Segment1;
            // slControls[6] = ddl_Segment2;

            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/GL_" + Session["Sel_Lng"].ToString() + ".properties"));
            ErrorCollection.Clear();
            string strCtrlTypes = FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.DROP_DOWN_LIST;//+ "~" + FINAppConstants.DROP_DOWN_LIST;
            string strMessage = Prop_File_Data["Account_Code_P"] + " ~ " + Prop_File_Data["Amount_P"] + " ~ " + Prop_File_Data["Beneficiary_Name_P"] + "~" + " Segment1";// +"~" + " Segment2";
            // string strMessage = "Lot No ~ Quantity";

            EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

            if (EmptyErrorCollection.Count > 0)
            {
                ErrorCollection = EmptyErrorCollection;
                return drList;
            }

            //string strCondition = "LOT_ID='" + ddlLotNumber.SelectedValue.ToString() + "'";
            //strMessage = FINMessageConstatns.RecordAlreadyExists;
            //ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            //if (ErrorCollection.Count > 0)
            //{
            //    return drList;
            //}




            drList["ACCT_CODE_ID"] = ddlAccCode.SelectedValue.ToString();
            drList["ACCT_CODE"] = ddlAccCode.SelectedItem.Text.ToString();

            drList["CHECK_DTL_ID"] = ddlChqNo.SelectedValue.ToString();
            drList["CHECK_NUMBER"] = ddlChqNo.SelectedItem.Text.ToString();
            if (dtpCheqDate.Text.ToString().Length > 0)
            {
                drList["CHEQUE_DATE"] = DBMethod.ConvertStringToDate(dtpCheqDate.Text.ToString());
            }
            else
            {
                drList["CHEQUE_DATE"] = DBNull.Value;
            }
            drList["AMOUNT"] = txtAmount.Text;
            drList["BENEFICIARY_NAME"] = txtBenfName.Text;
            drList["REASON_FOR_PAYMENT"] = txtReasonfrpayment.Text;

            if (ddl_Segment1.SelectedValue.ToString().Length > 0)
            {
                drList["JE_SEGMENT_ID_1"] = ddl_Segment1.SelectedValue.ToString();
                drList["SEGMENT_1_TEXT"] = ddl_Segment1.SelectedItem.Text.ToString();
            }
            else
            {
                drList["JE_SEGMENT_ID_1"] = null;
                drList["SEGMENT_1_TEXT"] = null;
            }

            if (ddl_Segment2.SelectedValue.ToString().Length > 0)
            {
                drList["JE_SEGMENT_ID_2"] = ddl_Segment2.SelectedValue.ToString();
                drList["SEGMENT_2_TEXT"] = ddl_Segment2.SelectedItem.Text.ToString();
            }
            else
            {
                drList["JE_SEGMENT_ID_2"] = null;
                drList["SEGMENT_2_TEXT"] = null;
            }
            if (ddl_Segment3.SelectedValue.ToString().Length > 0)
            {
                drList["JE_SEGMENT_ID_3"] = ddl_Segment3.SelectedValue.ToString();
                drList["SEGMENT_3_TEXT"] = ddl_Segment3.SelectedItem.Text.ToString();
            }
            else
            {
                drList["JE_SEGMENT_ID_3"] = null;
                drList["SEGMENT_3_TEXT"] = null;
            }
            if (ddl_Segment4.SelectedValue.ToString().Length > 0)
            {
                drList["JE_SEGMENT_ID_4"] = ddl_Segment4.SelectedValue.ToString();
                drList["SEGMENT_4_TEXT"] = ddl_Segment4.SelectedItem.Text.ToString();
            }
            else
            {
                drList["JE_SEGMENT_ID_4"] = null;
                drList["SEGMENT_4_TEXT"] = null;
            }
            if (ddl_Segment5.SelectedValue.ToString().Length > 0)
            {
                drList["JE_SEGMENT_ID_5"] = ddl_Segment5.SelectedValue.ToString();
                drList["SEGMENT_5_TEXT"] = ddl_Segment5.SelectedItem.Text.ToString();
            }
            else
            {
                drList["JE_SEGMENT_ID_5"] = null;
                drList["SEGMENT_5_TEXT"] = null;
            }
            if (ddl_Segment6.SelectedValue.ToString().Length > 0)
            {
                drList["JE_SEGMENT_ID_6"] = ddl_Segment6.SelectedValue.ToString();
                drList["SEGMENT_6_TEXT"] = ddl_Segment6.SelectedItem.Text.ToString();
            }
            else
            {
                drList["JE_SEGMENT_ID_6"] = null;
                drList["SEGMENT_6_TEXT"] = null;
            }
            drList[FINColumnConstants.DELETED] = FINAppConstants.N;


            return drList;

        }



        private void BindSegment(GridViewRow gvr)
        {

            DropDownList ddl_AccountCodes = (DropDownList)gvr.FindControl("ddlAccCode");

            if (ddl_AccountCodes.SelectedValue != null)
            {
                if (ddl_AccountCodes.SelectedValue != string.Empty)
                {
                    DataTable dtdataAc = DBMethod.ExecuteQuery(Segments_DAL.GetSegmentBasedAccCode(ddl_AccountCodes.SelectedValue.ToString(), "")).Tables[0];
                    Session["AccCode_Seg"] = dtdataAc;
                    for (int iLoop = 0; iLoop < dtdataAc.Rows.Count; iLoop++)
                    {

                        if (iLoop == 0)
                        {
                            DropDownList ddlSegment = (DropDownList)gvr.FindControl("ddlSegment" + (iLoop + 1));
                            Segments_BLL.GetSegmentvalues(ref ddlSegment, dtdataAc.Rows[iLoop]["SEGMENT_ID"].ToString());
                        }
                        if (gvData.EditIndex >= 0)
                        {
                            DropDownList ddlSegment = (DropDownList)gvr.FindControl("ddlSegment" + (iLoop + 1));
                            ddlSegment.SelectedValue = gvData.DataKeys[gvr.RowIndex].Values["JE_SEGMENT_ID_" + (iLoop + 1)].ToString();
                            //LoadSegmentValues(ddlSegment);
                        }
                    }
                }
            }
        }

        protected void ddlSegment_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadSegmentValues(sender);


        }
        private void LoadSegmentValues(object sender)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            DropDownList ddl_Seg = (DropDownList)sender;
            int int_ddlNo = int.Parse(ddl_Seg.ID.ToString().Replace("ddlSegment", ""));
            DataTable dtdataAc = (DataTable)Session["AccCode_Seg"];
            DropDownList ddlSegment = (DropDownList)gvr.FindControl("ddlSegment" + (int_ddlNo + 1));
            if (ddlSegment != null && int_ddlNo < dtdataAc.Rows.Count)
            {
                if (dtdataAc.Rows[int_ddlNo]["IS_DEPENDENT"].ToString().Trim() == "0")
                {
                    Segments_BLL.GetSegmentvalues4ParentSeg(ref ddlSegment, dtdataAc.Rows[int_ddlNo]["SEGMENT_ID"].ToString(), "0");
                }
                else
                {
                    Segments_BLL.GetSegmentvalues4ParentSeg(ref ddlSegment, dtdataAc.Rows[int_ddlNo]["SEGMENT_ID"].ToString(), ddl_Seg.SelectedValue.ToString());
                }
            }
        }
        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("SQEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];

                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        #endregion

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                //ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session[FINSessionConstants.GridData], "Payment Details");

                //if (ErrorCollection.Count > 0)
                //{
                //    return;
                //}

                System.Collections.SortedList slControls = new System.Collections.SortedList();

                //slControls[0] = ddlDCNumber;
                //slControls[1] = txtDCDate;

                //Dictionary<string, string> Prop_File_Data;
                //Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/AR_" + Session["Sel_Lng"].ToString() + ".properties"));
                //ErrorCollection.Clear();

                //string strCtrlTypes = FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DATE_TIME;
                //string strMessage = Prop_File_Data["DC_Number_P"] + " ~ " + Prop_File_Data["DC_Date_P"] + "";
                ////string strMessage = "Quote Date ~ Quote Type ~ Customer Name ~ Description ~ Status";

                //EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

                //if (EmptyErrorCollection.Count > 0)
                //{
                //    ErrorCollection = EmptyErrorCollection;
                //    return;
                //}
                //if (ErrorCollection.Count > 0)
                //{
                //    return;
                //}


                AssignToBE();

                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    gL_PAYMENT_HDR = (GL_PAYMENT_HDR)EntityData;
                }

                //PO_HEADERS.PO_HEADER_ID = txtPONumber.Text;



                gL_PAYMENT_HDR.PAYMENT_NO = txtPayNo.Text;
                if (txtPaydate.Text != string.Empty)
                {
                    gL_PAYMENT_HDR.PAYMENT_DATE = DBMethod.ConvertStringToDate(txtPaydate.Text.ToString());
                }

                gL_PAYMENT_HDR.BANK_NAME = ddlBank.SelectedValue;
                gL_PAYMENT_HDR.BRANCH = ddlBranch.SelectedValue;
                gL_PAYMENT_HDR.ACCT_NUMBER = ddlAcctNo.SelectedValue;
                gL_PAYMENT_HDR.AMOUNT = CommonUtils.ConvertStringToDecimal(txtAmt.Text);
                gL_PAYMENT_HDR.COST_CENTRE = ddlCostCenter.SelectedValue;
                gL_PAYMENT_HDR.REMARKS = txtRemarks.Text;
                gL_PAYMENT_HDR.ORG_ID = VMVServices.Web.Utils.OrganizationID;


                gL_PAYMENT_HDR.POSTED_FLAG = FINAppConstants.N;
                gL_PAYMENT_HDR.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                //PO_HEADERS.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;
                gL_PAYMENT_HDR.CHECK_DTL_ID = ddlChkNo.SelectedValue.ToString();
                gL_PAYMENT_HDR.CHECK_DATE = DBMethod.ConvertStringToDate(txtChequeDate.Text);

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    gL_PAYMENT_HDR.MODIFIED_BY = this.LoggedUserName;
                    gL_PAYMENT_HDR.MODIFIED_DATE = DateTime.Today;
                }
                else
                {
                    gL_PAYMENT_HDR.PAY_HDR_ID = FINSP.GetSPFOR_SEQCode("AP_031_M".ToString(), false, true);
                    gL_PAYMENT_HDR.PAYMENT_NO = FINSP.GetSPFOR_SEQCode("AP_031_P".ToString(), false, true);
                    gL_PAYMENT_HDR.CREATED_BY = this.LoggedUserName;
                    gL_PAYMENT_HDR.CREATED_DATE = DateTime.Today;
                }
                gL_PAYMENT_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, gL_PAYMENT_HDR.PAY_HDR_ID);


                //Save Detail Table


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                ErrorCollection = CommonUtils.IsEmptyGrid(dtGridData, "Payment Details ");
                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                var tmpChildEntity = new List<Tuple<object, string>>();

                GL_PAYMENT_DTL gL_PAYMENT_DTL = new GL_PAYMENT_DTL();

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    gL_PAYMENT_DTL = new GL_PAYMENT_DTL();

                    if (dtGridData.Rows[iLoop]["PAY_DTL_ID"].ToString() != "0" && dtGridData.Rows[iLoop]["PAY_DTL_ID"].ToString() != string.Empty)
                    {
                        using (IRepository<GL_PAYMENT_DTL> userCtx = new DataRepository<GL_PAYMENT_DTL>())
                        {
                            gL_PAYMENT_DTL = userCtx.Find(r =>
                                (r.PAY_DTL_ID == dtGridData.Rows[iLoop]["PAY_DTL_ID"].ToString())
                                ).SingleOrDefault();
                        }
                    }

                    gL_PAYMENT_DTL.CHEQUE_NO = (dtGridData.Rows[iLoop]["CHECK_DTL_ID"].ToString());
                    if (dtGridData.Rows[iLoop]["CHEQUE_DATE"] != DBNull.Value)
                    {
                        gL_PAYMENT_DTL.CHEQUE_DATE = DateTime.Parse(dtGridData.Rows[iLoop]["CHEQUE_DATE"].ToString());
                    }
                    gL_PAYMENT_DTL.ACCT_CODE = dtGridData.Rows[iLoop]["ACCT_CODE_ID"].ToString();
                    gL_PAYMENT_DTL.AMOUNT = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop]["AMOUNT"].ToString());
                    gL_PAYMENT_DTL.BENEFICIARY_NAME = dtGridData.Rows[iLoop]["BENEFICIARY_NAME"].ToString();
                    gL_PAYMENT_DTL.REASON_FOR_PAYMENT = dtGridData.Rows[iLoop]["REASON_FOR_PAYMENT"].ToString();

                    gL_PAYMENT_DTL.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;
                    gL_PAYMENT_DTL.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                    gL_PAYMENT_DTL.PAY_HDR_ID = gL_PAYMENT_HDR.PAY_HDR_ID;

                    if (dtGridData.Rows[iLoop]["JE_SEGMENT_ID_1"].ToString().Length > 0)
                    {
                        gL_PAYMENT_DTL.JE_SEGMENT_ID_1 = dtGridData.Rows[iLoop]["JE_SEGMENT_ID_1"].ToString();
                    }
                    else
                    {
                        gL_PAYMENT_DTL.JE_SEGMENT_ID_1 = null;
                    }

                    if (dtGridData.Rows[iLoop]["JE_SEGMENT_ID_2"].ToString().Length > 0)
                    {
                        gL_PAYMENT_DTL.JE_SEGMENT_ID_2 = dtGridData.Rows[iLoop]["JE_SEGMENT_ID_2"].ToString();
                    }
                    else
                    {
                        gL_PAYMENT_DTL.JE_SEGMENT_ID_2 = null;
                    }


                    if (dtGridData.Rows[iLoop]["JE_SEGMENT_ID_3"].ToString().Length > 0)
                    {
                        gL_PAYMENT_DTL.JE_SEGMENT_ID_3 = dtGridData.Rows[iLoop]["JE_SEGMENT_ID_3"].ToString();
                    }
                    else
                    {
                        gL_PAYMENT_DTL.JE_SEGMENT_ID_3 = null;
                    }

                    if (dtGridData.Rows[iLoop]["JE_SEGMENT_ID_4"].ToString().Length > 0)
                    {
                        gL_PAYMENT_DTL.JE_SEGMENT_ID_4 = dtGridData.Rows[iLoop]["JE_SEGMENT_ID_4"].ToString();
                    }
                    else
                    {
                        gL_PAYMENT_DTL.JE_SEGMENT_ID_4 = null;
                    }


                    if (dtGridData.Rows[iLoop]["JE_SEGMENT_ID_5"].ToString().Length > 0)
                    {
                        gL_PAYMENT_DTL.JE_SEGMENT_ID_5 = dtGridData.Rows[iLoop]["JE_SEGMENT_ID_5"].ToString();
                    }
                    else
                    {
                        gL_PAYMENT_DTL.JE_SEGMENT_ID_5 = null;
                    }

                    if (dtGridData.Rows[iLoop]["JE_SEGMENT_ID_6"].ToString().Length > 0)
                    {
                        gL_PAYMENT_DTL.JE_SEGMENT_ID_6 = dtGridData.Rows[iLoop]["JE_SEGMENT_ID_6"].ToString();
                    }
                    else
                    {
                        gL_PAYMENT_DTL.JE_SEGMENT_ID_6 = null;
                    }
                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        tmpChildEntity.Add(new Tuple<object, string>(gL_PAYMENT_DTL, FINAppConstants.Delete));
                    }
                    else
                    {
                        if (dtGridData.Rows[iLoop]["PAY_DTL_ID"].ToString() != "0" && dtGridData.Rows[iLoop]["PAY_DTL_ID"].ToString() != string.Empty)
                        {
                            gL_PAYMENT_DTL.MODIFIED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(gL_PAYMENT_DTL, FINAppConstants.Update));
                        }
                        else
                        {
                            gL_PAYMENT_DTL.PAY_DTL_ID = FINSP.GetSPFOR_SEQCode("AP_031_D".ToString(), false, true);
                            gL_PAYMENT_DTL.CREATED_BY = this.LoggedUserName;
                            gL_PAYMENT_DTL.CREATED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(gL_PAYMENT_DTL, FINAppConstants.Add));
                        }
                    }
                }



                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            CommonUtils.SavePCEntity<GL_PAYMENT_HDR, GL_PAYMENT_DTL>(gL_PAYMENT_HDR, tmpChildEntity, gL_PAYMENT_DTL);
                            savedBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {
                            CommonUtils.SavePCEntity<GL_PAYMENT_HDR, GL_PAYMENT_DTL>(gL_PAYMENT_HDR, tmpChildEntity, gL_PAYMENT_DTL, true);
                            savedBool = true;
                            break;
                        }
                }

                //for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                //{


                //    CA_CHECK_DTL cA_CHECK_DTL = new CA_CHECK_DTL();
                //    using (IRepository<CA_CHECK_DTL> userCtx = new DataRepository<CA_CHECK_DTL>())
                //    {
                //        cA_CHECK_DTL = userCtx.Find(r =>
                //            (r.CHECK_DTL_ID == dtGridData.Rows[iLoop]["CHECK_DTL_ID"].ToString())
                //            ).SingleOrDefault();
                //    }
                //    if (cA_CHECK_DTL != null)
                //    {

                //        cA_CHECK_DTL.CHECK_PAY_TO = dtGridData.Rows[iLoop]["BENEFICIARY_NAME"].ToString();

                //        if (dtGridData.Rows[iLoop]["CHEQUE_DATE"] != DBNull.Value)
                //        {
                //            cA_CHECK_DTL.CHECK_DT = DateTime.Parse(dtGridData.Rows[iLoop]["CHEQUE_DATE"].ToString());
                //        }
                //        cA_CHECK_DTL.CHECK_AMT = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop]["AMOUNT"].ToString());
                //        cA_CHECK_DTL.CHECK_STATUS = "USED";
                //        cA_CHECK_DTL.CHECK_CURRENCY_CODE = Session[FINSessionConstants.ORGCurrency].ToString();
                //        cA_CHECK_DTL.MODIFIED_BY = this.LoggedUserName;
                //        cA_CHECK_DTL.MODIFIED_DATE = DateTime.Today;
                //        DBMethod.SaveEntity<CA_CHECK_DTL>(cA_CHECK_DTL, true);
                //    }




                CA_CHECK_DTL cA_CHECK_DTL = new CA_CHECK_DTL();
                using (IRepository<CA_CHECK_DTL> userCtx = new DataRepository<CA_CHECK_DTL>())
                {
                    cA_CHECK_DTL = userCtx.Find(r =>
                        (r.CHECK_DTL_ID == ddlChkNo.SelectedValue)
                        ).SingleOrDefault();
                }
                if (cA_CHECK_DTL != null)
                {

                    cA_CHECK_DTL.CHECK_PAY_TO = "GL PAYMENT ";

                    if (txtChequeDate.Text.ToString().Length > 0)
                        cA_CHECK_DTL.CHECK_DT = DBMethod.ConvertStringToDate(txtChequeDate.Text);

                    cA_CHECK_DTL.CHECK_AMT = CommonUtils.ConvertStringToDecimal(txtAmt.Text);
                    cA_CHECK_DTL.CHECK_STATUS = "USED";
                    cA_CHECK_DTL.CHECK_CURRENCY_CODE = Session[FINSessionConstants.ORGCurrency].ToString();
                    cA_CHECK_DTL.MODIFIED_BY = this.LoggedUserName;
                    cA_CHECK_DTL.MODIFIED_DATE = DateTime.Today;
                    DBMethod.SaveEntity<CA_CHECK_DTL>(cA_CHECK_DTL, true);
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                // DBMethod.DeleteEntity<PO_HEADERS>(PO_HEADERS);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }







        #endregion





        private void FillBankDetails()
        {
            BankBranch_BLL.fn_getBranchName(ref ddlBranch, ddlBank.SelectedValue);

        }

        protected void ddlBankAccount_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                DataTable dtBankBranchAcctNo = new DataTable();
                dtBankBranchAcctNo = DBMethod.ExecuteQuery(FIN.DAL.CA.Bank_DAL.getBankBranchAcctNo(ddlBankAccount.SelectedValue)).Tables[0];
                if (dtBankBranchAcctNo.Rows.Count > 0)
                {
                    ddlBank.SelectedValue = dtBankBranchAcctNo.Rows[0]["BANK_ID"].ToString();
                    FillBankDetails();
                    ddlBranch.SelectedValue = dtBankBranchAcctNo.Rows[0]["BANK_BRANCH_ID"].ToString();
                    FillBranchDetails();
                    ddlAcctNo.SelectedValue = dtBankBranchAcctNo.Rows[0]["ACCOUNT_ID"].ToString();
                    GridViewRow gvr = gvData.FooterRow;
                    FillFooterGridCombo(gvr);


                    Cheque_BLL.GetUnusedCheck(ref ddlChkNo, ddlBank.SelectedValue, ddlBranch.SelectedValue, ddlAcctNo.SelectedValue, FINAppConstants.Add);


                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }

        protected void ddlBank_SelectedIndexChanged1(object sender, EventArgs e)
        {
            {
                try
                {
                    ErrorCollection.Clear();
                    FillBankDetails();
                }
                catch (Exception ex)
                {
                    ErrorCollection.Add("POR", ex.Message);
                }
                finally
                {
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                    }
                }
            }
        }

        protected void ddlBranch_SelectedIndexChanged(object sender, EventArgs e)
        {
            {
                try
                {
                    ErrorCollection.Clear();
                    FillBranchDetails();
                }
                catch (Exception ex)
                {
                    ErrorCollection.Add("POR", ex.Message);
                }
                finally
                {
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                    }
                }
            }
        }

        private void FillBranchDetails()
        {

            BankAccounts_BLL.fn_getBankAccount(ref ddlAcctNo, ddlBank.SelectedValue, ddlBranch.SelectedValue);

        }

        protected void ddlAcctNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow gvr = gvData.FooterRow;
            FillFooterGridCombo(gvr);
        }
        protected void ddlAccCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            BindSegment(gvr);
        }


        protected void imgBtnJVPrint_Click(object sender, ImageClickEventArgs e)
        {

            if (EntityData != null)
            {
                gL_PAYMENT_HDR = (GL_PAYMENT_HDR)EntityData;
            }

            if (gL_PAYMENT_HDR != null)
            {
                Hashtable htParameters = new Hashtable();
                Hashtable htHeadingParameters = new Hashtable();
                Hashtable htFilterParameter = new Hashtable();

                ReportFile = "GL_REPORTS//RPTJournalVoucher.rpt";// "../Reports/GL/RPTJournalVoucher.rpt";


                ReportData = DBMethod.ExecuteQuery(FIN.DAL.GL.JournalEntry_DAL.GetJournalVoucherDetailsRPT("", gL_PAYMENT_HDR.JE_HDR_ID));


                Session["ProgramName"] = "Journal Voucher";
                if (VMVServices.Web.Utils.LanguageCode.ToString().Length > 0)
                {
                    Session["ProgramName"] = "مجلة قسيمة";
                }
                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());



                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;


                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('../RPTCrystalReportViewer.aspx?Id?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);

            }
        }
        protected void imgBtnPost_Click(object sender, ImageClickEventArgs e)
        {
            imgBtnPost.Visible = false;
            try
            {
                ErrorCollection.Clear();


                if (EntityData != null)
                {
                    gL_PAYMENT_HDR = (GL_PAYMENT_HDR)EntityData;
                }
                if (gL_PAYMENT_HDR.WORKFLOW_COMPLETION_STATUS == "1")
                {
                    FINSP.GetSP_GL_Posting(gL_PAYMENT_HDR.PAY_HDR_ID, "AP_031");
                }

                //using (IRepository<GL_PAYMENT_HDR> userCtx = new DataRepository<GL_PAYMENT_HDR>())
                //{
                //    gL_PAYMENT_HDR = userCtx.Find(r =>
                //        (r.PAY_HDR_ID == Master.StrRecordId)
                //        ).SingleOrDefault();
                //}
                //gL_PAYMENT_HDR.POSTED_FLAG = FINAppConstants.Y;
                //gL_PAYMENT_HDR.POSTED_DATE = DateTime.Now;
                //DBMethod.SaveEntity<GL_PAYMENT_HDR>(gL_PAYMENT_HDR, true);
                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.POSTED);

                imgBtnPost.Visible = false;
                imgBtnJVPrint.Visible = true;
                btnSave.Visible = false;


                imgBtnPost.Visible = false;
                lblPosted.Visible = true;

                imgBtnJVPrint.Visible = true;

                pnlgridview.Enabled = false;
                pnltdHeader.Enabled = false;



                ErrorCollection.Clear();

                Hashtable htParameters = new Hashtable();
                Hashtable htHeadingParameters = new Hashtable();
                Hashtable htFilterParameter = new Hashtable();


                ReportFile = "GL_REPORTS//RPTJournalVoucher.rpt";// "../Reports/GL/RPTJournalVoucher.rpt";


                ReportData = DBMethod.ExecuteQuery(FIN.DAL.GL.JournalEntry_DAL.GetJournalVoucherDetailsRPT("", gL_PAYMENT_HDR.JE_HDR_ID));


                Session["ProgramName"] = "Journal Voucher";
                if (VMVServices.Web.Utils.LanguageCode.ToString().Length > 0)
                {
                    Session["ProgramName"] = "مجلة قسيمة";
                }
                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());



                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;


                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('../RPTCrystalReportViewer.aspx?Id?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);
            }
            catch (Exception ex)
            {
                imgBtnPost.Visible = true;
                ErrorCollection.Add("posting", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    imgBtnPost.Visible = true;
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }


        protected void imgBtnChequePrint_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;


                Hashtable htFilterParameter = new Hashtable();
                Hashtable htHeadingParameters = new Hashtable();
                ErrorCollection.Clear();
                ReportFile = "AP_REPORTS/RPTChequePrintout_B.rpt";

                if (gvData.DataKeys[gvr.RowIndex].Values["PAY_DTL_ID"] != "0")
                {
                    htFilterParameter.Add("CHEQUEID", gvData.DataKeys[gvr.RowIndex].Values["CHECK_DTL_ID"].ToString());

                    NumberToWord.ToWord toword = new NumberToWord.ToWord(decimal.Parse(gvData.DataKeys[gvr.RowIndex].Values["AMOUNT"].ToString()));
                    if (VMVServices.Web.Utils.LanguageCode.ToString().Length > 0)
                    {
                        htFilterParameter.Add("AMTINWORD", toword.ConvertToArabic());
                    }
                    else
                    {
                        htFilterParameter.Add("AMTINWORD", toword.ConvertToEnglish());
                    }

                    VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
                    VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;


                    ReportData = FIN.BLL.CA.Cheque_BLL.GetChequesReportData();

                    htHeadingParameters.Add("ReportName", "Cheque");
                    if (VMVServices.Web.Utils.LanguageCode.ToString().Length > 0)
                    {
                        htHeadingParameters.Add("ReportName", "الاختيار");
                    }
                    ReportFormulaParameter = htHeadingParameters;

                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerPath + "?Id=" + Master.Mode + "','','" + VMVServices.Services.Data.DbConsts.ReportProperties + "');", true);
                }
                else
                {
                    ErrorCollection.Add("NOCheckPrint", "With Out Save the Record ,You are not able to Take Print out ");
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("APAgingAnalysisReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void btnPrintReport_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                Hashtable htParameters = new Hashtable();
                Hashtable htHeadingParameters = new Hashtable();
                Hashtable htFilterParameter = new Hashtable();

                ReportFile = "GL_REPORTS//RPTPaymentGL.rpt";

                if (Session["PaymentNo"] != null)
                {
                    htFilterParameter.Add("PAYMENT_NUMBER", Session["PaymentNo"].ToString());
                    ReportData = DBMethod.ExecuteQuery(PaymentGL_DAL.GetPaymentDetailsRPT(Session["PaymentNo"].ToString()));
                }

                Session["ProgramName"] = "GL Payment";
                if (VMVServices.Web.Utils.LanguageCode.ToString().Length > 0)
                {
                    Session["ProgramName"] = "دفتر الأستاذ العام الدفع";
                }
                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());

                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;
                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('../RPTCrystalReportViewer.aspx?Id?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYCrpt", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void imgbtnCheckPrint_Click(object sender, ImageClickEventArgs e)
        {
            try
            {

                Hashtable htFilterParameter = new Hashtable();
                Hashtable htHeadingParameters = new Hashtable();
                ErrorCollection.Clear();
                ReportFile = "AP_REPORTS/RPTChequePrintout_B.rpt";

                if (ddlChkNo.SelectedValue.ToString().Length > 0)
                {
                    htFilterParameter.Add("CHEQUEID", ddlChkNo.SelectedValue.ToString());

                    NumberToWord.ToWord toword = new NumberToWord.ToWord(decimal.Parse(txtAmt.Text));
                    if (VMVServices.Web.Utils.LanguageCode.ToString().Length > 0)
                    {
                        htFilterParameter.Add("AMTINWORD", toword.ConvertToArabic());
                    }
                    else
                    {
                        htFilterParameter.Add("AMTINWORD", toword.ConvertToEnglish());
                    }

                    VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
                    VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;


                    ReportData = FIN.BLL.CA.Cheque_BLL.GetChequesReportData();

                    htHeadingParameters.Add("ReportName", "Cheque");
                    if (VMVServices.Web.Utils.LanguageCode.ToString().Length > 0)
                    {
                        htHeadingParameters.Add("ReportName", "الاختيار");
                    }

                    ReportFormulaParameter = htHeadingParameters;

                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerPath + "?Id=" + Master.Mode + "','','" + VMVServices.Services.Data.DbConsts.ReportProperties + "');", true);
                }
                else
                {
                    ErrorCollection.Add("NOCheckPrint", "With Out Save the Record ,You are not able to Take Print out ");
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("APAgingAnalysisReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void imgStopPayment_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                imgBtnPost.Visible = false;
                using (IRepository<GL_PAYMENT_HDR> userCtx = new DataRepository<GL_PAYMENT_HDR>())
                {
                    gL_PAYMENT_HDR = userCtx.Find(r =>
                        (r.PAY_HDR_ID == Master.StrRecordId)
                        ).SingleOrDefault();
                }

                if (gL_PAYMENT_HDR.WORKFLOW_COMPLETION_STATUS == "1")
                {
                    FINSP.GetSP_GL_Posting(gL_PAYMENT_HDR.JE_HDR_ID, "AP_031_R", this.LoggedUserName);
                }
                btnStopPayment.Visible = false;

                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.POSTED);

                using (IRepository<GL_PAYMENT_HDR> userCtx = new DataRepository<GL_PAYMENT_HDR>())
                {
                    gL_PAYMENT_HDR = userCtx.Find(r =>
                        (r.PAY_HDR_ID == Master.StrRecordId)
                        ).SingleOrDefault();
                }

                Hashtable htParameters = new Hashtable();
                Hashtable htHeadingParameters = new Hashtable();
                Hashtable htFilterParameter = new Hashtable();

                imgBtnStopPayJV.Visible = true;

                lblPosted.Visible = true;
                imgBtnPost.Visible = false;
                lblCancelled.Visible = true;


                btnStopPayment.Visible = false;

                pnlgridview.Enabled = false;
                pnltdHeader.Enabled = false;




                ReportFile = "GL_REPORTS//RPTJournalVoucher.rpt";


                ReportData = DBMethod.ExecuteQuery(FIN.DAL.GL.JournalEntry_DAL.GetJournalVoucherDetailsRPT("", gL_PAYMENT_HDR.REV_JE_HDR_ID));


                Session["ProgramName"] = "Journal Voucher";
                if (VMVServices.Web.Utils.LanguageCode.ToString().Length > 0)
                {
                    Session["ProgramName"] = "مجلة قسيمة";
                }
                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());

                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;

                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('../RPTCrystalReportViewer.aspx?Id?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("posting", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    imgBtnPost.Visible = true;
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void imgBtnStopPayJV_Click(object sender, ImageClickEventArgs e)
        {
            using (IRepository<GL_PAYMENT_HDR> userCtx = new DataRepository<GL_PAYMENT_HDR>())
            {
                gL_PAYMENT_HDR = userCtx.Find(r =>
                    (r.PAY_HDR_ID == Master.StrRecordId)
                    ).SingleOrDefault();
            }
            if (gL_PAYMENT_HDR != null)
            {
                Hashtable htParameters = new Hashtable();
                Hashtable htHeadingParameters = new Hashtable();
                Hashtable htFilterParameter = new Hashtable();

                ReportFile = "GL_REPORTS//RPTJournalVoucher.rpt";// "../Reports/GL/RPTJournalVoucher.rpt";


                ReportData = DBMethod.ExecuteQuery(FIN.DAL.GL.JournalEntry_DAL.GetJournalVoucherDetailsRPT("", gL_PAYMENT_HDR.REV_JE_HDR_ID));


                Session["ProgramName"] = "Journal Voucher";
                if (VMVServices.Web.Utils.LanguageCode.ToString().Length > 0)
                {
                    Session["ProgramName"] = "مجلة قسيمة";
                }

                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());



                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;


                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('../RPTCrystalReportViewer.aspx?Id?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);



            }

        }

    }
}