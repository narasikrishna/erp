﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.BLL;
using FIN.BLL.GL;
using FIN.DAL.GL;
using VMVServices.Web;

namespace FIN.Client.GL
{
    public partial class ActualandProjectedBudget : PageBase
    {
        GL_BUDGET_HDR gL_BUDGET_HDR = new GL_BUDGET_HDR();
        GL_BUDGET_DTL gL_BUDGET_DTL = new GL_BUDGET_DTL();
        DataTable dtGridData = new DataTable();
        Boolean bol_rowVisiable;
        Boolean savedBool;
        string ProReturn = null;
        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();

                    ChnageLanguage();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion
        private void ChnageLanguage()
        {
            try
            {
                ErrorCollection.Clear();
                if (VMVServices.Web.Utils.LanguageCode.ToString().Length > 0)
                {
                    Dictionary<string, string> Prop_File_Data;
                    Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/GL_" + Session["Sel_Lng"].ToString() + ".properties"));
                

                    lblSegment1.Text = Prop_File_Data["Segment_1_P"];
                    lblSegment2.Text = Prop_File_Data["Segment_2_P"];
                    lblSegment3.Text = Prop_File_Data["Segment_3_P"];
                    lblSegment4.Text = Prop_File_Data["Segment_4_P"];
                    lblSegment5.Text = Prop_File_Data["Segment_5_P"];
                    lblSegment6.Text = Prop_File_Data["Segment_6_P"];

                   


                
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("MisReceiptChangeLang", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();

            //if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            //{
            //    VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
            //}

            //if (VMVServices.Web.Utils.AddAllowed != FINAppConstants.Y && Master.Mode == FINAppConstants.Add)
            //{
            //    btnSave.Visible = false;
            //}

        }

        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }



        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();
                Session["singlePeriod"] = null;
                Session[FINSessionConstants.GridData] = null;
                EntityData = null;

                dtGridData = FIN.BLL.GL.Budget_BLL.getActualAndProjectedBudgetDetails(Master.StrRecordId);
                BindGrid(dtGridData);

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    using (IRepository<GL_BUDGET_HDR> userCtx = new DataRepository<GL_BUDGET_HDR>())
                    {
                        gL_BUDGET_HDR = userCtx.Find(r =>
                            (r.BUDGET_CODE == Master.StrRecordId)
                            ).SingleOrDefault();
                    }
                    EntityData = gL_BUDGET_HDR;

                    txtBudgetName.Text = gL_BUDGET_HDR.BUDGET_NAME;
                    ddlCompanyName.SelectedValue = gL_BUDGET_HDR.BUDGET_COMP;
                    ddlBudgetType.SelectedValue = gL_BUDGET_HDR.BUDGET_TYPE;
                    ddlGlobalSegment.SelectedValue = gL_BUDGET_HDR.ATTRIBUTE1;
                    if (gL_BUDGET_HDR.BUDGET_AMOUNT != null)
                    {
                        if (gL_BUDGET_HDR.BUDGET_AMOUNT > 0)
                        {
                            txtAnnualBudgetAmount.Text = DBMethod.GetAmtDecimalCommaSeparationValue(gL_BUDGET_HDR.BUDGET_AMOUNT.ToString());
                        }
                    }
                    if (gL_BUDGET_HDR.BUDGET_PERIOD_START_DT.ToString() != string.Empty)
                    {
                        txtFromDate.Text = DBMethod.ConvertDateToString(gL_BUDGET_HDR.BUDGET_PERIOD_START_DT.ToString());
                    }
                    if (gL_BUDGET_HDR.BUDGET_PERIOD_END_DT.ToString() != string.Empty)
                    {
                        txtToDate.Text = DBMethod.ConvertDateToString(gL_BUDGET_HDR.BUDGET_PERIOD_END_DT.ToString());
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    gL_BUDGET_HDR = (GL_BUDGET_HDR)EntityData;
                }



                gL_BUDGET_HDR.BUDGET_NAME = txtBudgetName.Text;
                gL_BUDGET_HDR.BUDGET_COMP = ddlCompanyName.SelectedValue;
                gL_BUDGET_HDR.BUDGET_TYPE = ddlBudgetType.SelectedValue;
                gL_BUDGET_HDR.ATTRIBUTE1 = ddlGlobalSegment.SelectedValue;

                if (txtAnnualBudgetAmount.Text != null)
                {
                    if (CommonUtils.ConvertStringToDecimal(txtAnnualBudgetAmount.Text) > 0)
                    {
                        gL_BUDGET_HDR.BUDGET_AMOUNT = CommonUtils.ConvertStringToDecimal(txtAnnualBudgetAmount.Text.ToString());
                    }
                }



                gL_BUDGET_HDR.BUDGET_PERIOD_START_DT = DBMethod.ConvertStringToDate(txtFromDate.Text.ToString());
                gL_BUDGET_HDR.BUDGET_PERIOD_END_DT = DBMethod.ConvertStringToDate(txtToDate.Text.ToString());
                //gL_BUDGET_HDR.WORKFLOW_COMPLETION_STATUS = "1";
                gL_BUDGET_HDR.ENABLED_FLAG = "1";

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    gL_BUDGET_HDR.MODIFIED_BY = this.LoggedUserName;
                    gL_BUDGET_HDR.MODIFIED_DATE = DateTime.Today;

                }
                else
                {
                    gL_BUDGET_HDR.BUDGET_CODE = FINSP.GetSPFOR_SEQCode(FINAppConstants.GL_0003.ToString(), false, true);

                    gL_BUDGET_HDR.CREATED_BY = this.LoggedUserName;
                    gL_BUDGET_HDR.CREATED_DATE = DateTime.Today;


                }
                gL_BUDGET_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, gL_BUDGET_HDR.BUDGET_CODE);
                gL_BUDGET_HDR.ORG_ID = VMVServices.Web.Utils.OrganizationID;

                //Save Detail Table


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                ErrorCollection = CommonUtils.IsEmptyGrid(dtGridData, "Budget Entry");

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                var tmpChildEntity = new List<Tuple<object, string>>();

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    gL_BUDGET_DTL = new GL_BUDGET_DTL();
                    if (dtGridData.Rows[iLoop][FINColumnConstants.BUDGET_DTL_ID].ToString() != "0")
                    {
                        using (IRepository<GL_BUDGET_DTL> userCtx = new DataRepository<GL_BUDGET_DTL>())
                        {
                            gL_BUDGET_DTL = userCtx.Find(r =>
                                (r.BUDGET_DTL_ID == dtGridData.Rows[iLoop][FINColumnConstants.BUDGET_DTL_ID].ToString())
                                ).SingleOrDefault();
                        }
                    }

                    gL_BUDGET_DTL.BUDGET_ACCT_GRP_ID = (dtGridData.Rows[iLoop][FINColumnConstants.GROUP_ID].ToString());
                    gL_BUDGET_DTL.BUDGET_ACCT_CODE_ID = (dtGridData.Rows[iLoop][FINColumnConstants.CODE_ID].ToString());
                    if (dtGridData.Rows[iLoop]["PERCENTAGE"].ToString().Length > 0)
                    {
                        gL_BUDGET_DTL.PERCENTAGE = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop]["PERCENTAGE"].ToString());
                    }
                    if (dtGridData.Rows[iLoop]["ACTUAL_PREVIOUS_AMOUNT"].ToString().Length > 0)
                    {
                        gL_BUDGET_DTL.ACTUAL_PREVIOUS_AMOUNT = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop]["ACTUAL_PREVIOUS_AMOUNT"].ToString());
                    }


                    if (dtGridData.Rows[iLoop][FINColumnConstants.BUDGET_MONTH_AMT_01] != DBNull.Value)
                    {
                        gL_BUDGET_DTL.BUDGET_MONTH_AMT_01 = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop][FINColumnConstants.BUDGET_MONTH_AMT_01].ToString());
                    }

                    if (dtGridData.Rows[iLoop][FINColumnConstants.BUDGET_MONTH_AMT_02] != DBNull.Value)
                    {
                        gL_BUDGET_DTL.BUDGET_MONTH_AMT_02 = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop][FINColumnConstants.BUDGET_MONTH_AMT_02].ToString());
                    }


                    if (dtGridData.Rows[iLoop][FINColumnConstants.BUDGET_MONTH_AMT_03] != DBNull.Value)
                    {
                        gL_BUDGET_DTL.BUDGET_MONTH_AMT_03 = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop][FINColumnConstants.BUDGET_MONTH_AMT_03].ToString());
                    }


                    if (dtGridData.Rows[iLoop][FINColumnConstants.BUDGET_MONTH_AMT_04] != DBNull.Value)
                    {
                        gL_BUDGET_DTL.BUDGET_MONTH_AMT_04 = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop][FINColumnConstants.BUDGET_MONTH_AMT_04].ToString());
                    }


                    if (dtGridData.Rows[iLoop][FINColumnConstants.BUDGET_MONTH_AMT_05] != DBNull.Value)
                    {
                        gL_BUDGET_DTL.BUDGET_MONTH_AMT_05 = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop][FINColumnConstants.BUDGET_MONTH_AMT_05].ToString());
                    }


                    if (dtGridData.Rows[iLoop][FINColumnConstants.BUDGET_MONTH_AMT_06] != DBNull.Value)
                    {
                        gL_BUDGET_DTL.BUDGET_MONTH_AMT_06 = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop][FINColumnConstants.BUDGET_MONTH_AMT_06].ToString());
                    }


                    if (dtGridData.Rows[iLoop][FINColumnConstants.BUDGET_MONTH_AMT_07] != DBNull.Value)
                    {
                        gL_BUDGET_DTL.BUDGET_MONTH_AMT_07 = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop][FINColumnConstants.BUDGET_MONTH_AMT_07].ToString());
                    }


                    if (dtGridData.Rows[iLoop][FINColumnConstants.BUDGET_MONTH_AMT_08] != DBNull.Value)
                    {
                        gL_BUDGET_DTL.BUDGET_MONTH_AMT_08 = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop][FINColumnConstants.BUDGET_MONTH_AMT_08].ToString());
                    }


                    if (dtGridData.Rows[iLoop][FINColumnConstants.BUDGET_MONTH_AMT_09] != DBNull.Value)
                    {
                        gL_BUDGET_DTL.BUDGET_MONTH_AMT_09 = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop][FINColumnConstants.BUDGET_MONTH_AMT_09].ToString());
                    }


                    if (dtGridData.Rows[iLoop][FINColumnConstants.BUDGET_MONTH_AMT_10] != DBNull.Value)
                    {
                        gL_BUDGET_DTL.BUDGET_MONTH_AMT_10 = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop][FINColumnConstants.BUDGET_MONTH_AMT_10].ToString());
                    }


                    if (dtGridData.Rows[iLoop][FINColumnConstants.BUDGET_MONTH_AMT_11] != DBNull.Value)
                    {
                        gL_BUDGET_DTL.BUDGET_MONTH_AMT_11 = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop][FINColumnConstants.BUDGET_MONTH_AMT_11].ToString());
                    }


                    if (dtGridData.Rows[iLoop][FINColumnConstants.BUDGET_MONTH_AMT_12] != DBNull.Value)
                    {
                        gL_BUDGET_DTL.BUDGET_MONTH_AMT_12 = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop][FINColumnConstants.BUDGET_MONTH_AMT_12].ToString());
                    }


                    if (dtGridData.Rows[iLoop][FINColumnConstants.BUDGET_SEGMENT_ID_1] != DBNull.Value)
                    {
                        gL_BUDGET_DTL.BUDGET_SEGMENT_ID_1 = (dtGridData.Rows[iLoop][FINColumnConstants.BUDGET_SEGMENT_ID_1].ToString());
                    }


                    if (dtGridData.Rows[iLoop][FINColumnConstants.BUDGET_SEGMENT_ID_2] != DBNull.Value)
                    {
                        gL_BUDGET_DTL.BUDGET_SEGMENT_ID_2 = (dtGridData.Rows[iLoop][FINColumnConstants.BUDGET_SEGMENT_ID_2].ToString());
                    }


                    if (dtGridData.Rows[iLoop][FINColumnConstants.BUDGET_SEGMENT_ID_3] != DBNull.Value)
                    {
                        gL_BUDGET_DTL.BUDGET_SEGMENT_ID_3 = (dtGridData.Rows[iLoop][FINColumnConstants.BUDGET_SEGMENT_ID_3].ToString());
                    }



                    if (dtGridData.Rows[iLoop][FINColumnConstants.BUDGET_SEGMENT_ID_4] != DBNull.Value)
                    {
                        gL_BUDGET_DTL.BUDGET_SEGMENT_ID_4 = (dtGridData.Rows[iLoop][FINColumnConstants.BUDGET_SEGMENT_ID_4].ToString());
                    }


                    if (dtGridData.Rows[iLoop][FINColumnConstants.BUDGET_SEGMENT_ID_5] != DBNull.Value)
                    {
                        gL_BUDGET_DTL.BUDGET_SEGMENT_ID_5 = (dtGridData.Rows[iLoop][FINColumnConstants.BUDGET_SEGMENT_ID_5].ToString());
                    }



                    if (dtGridData.Rows[iLoop][FINColumnConstants.BUDGET_SEGMENT_ID_6] != DBNull.Value)
                    {
                        gL_BUDGET_DTL.BUDGET_SEGMENT_ID_6 = (dtGridData.Rows[iLoop][FINColumnConstants.BUDGET_SEGMENT_ID_6].ToString());
                    }

                    gL_BUDGET_DTL.ENABLED_FLAG = FINAppConstants.EnabledFlag;

                    gL_BUDGET_DTL.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;

                    gL_BUDGET_DTL.BUDGET_CODE = gL_BUDGET_HDR.BUDGET_CODE;

                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        tmpChildEntity.Add(new Tuple<object, string>(gL_BUDGET_DTL, FINAppConstants.Delete));
                    }
                    else
                    {
                        if (dtGridData.Rows[iLoop][FINColumnConstants.BUDGET_DTL_ID].ToString() != "0" && dtGridData.Rows[iLoop][FINColumnConstants.BUDGET_DTL_ID].ToString() != string.Empty)
                        {
                            gL_BUDGET_DTL.MODIFIED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(gL_BUDGET_DTL, FINAppConstants.Update));
                        }
                        else
                        {
                            gL_BUDGET_DTL.BUDGET_DTL_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.GL_0003_D.ToString(), false, true);
                            gL_BUDGET_DTL.CREATED_BY = this.LoggedUserName;
                            gL_BUDGET_DTL.CREATED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(gL_BUDGET_DTL, FINAppConstants.Add));
                        }
                    }
                }
                ProReturn = FINSP.GetSPFOR_ERR_MGR_CATEGORY_THREE(Master.FormCode, gL_BUDGET_HDR.BUDGET_CODE, gL_BUDGET_HDR.BUDGET_COMP, gL_BUDGET_HDR.BUDGET_NAME, gL_BUDGET_HDR.ATTRIBUTE1);
                if (ProReturn != string.Empty)
                {
                    if (ProReturn != "0")
                    {
                        ErrorCollection.Add("BUDGET", ProReturn);
                        if (ErrorCollection.Count > 0)
                        {
                            return;
                        }
                    }
                }

                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            CommonUtils.SavePCEntity<GL_BUDGET_HDR, GL_BUDGET_DTL>(gL_BUDGET_HDR, tmpChildEntity, gL_BUDGET_DTL);
                            savedBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {
                            CommonUtils.SavePCEntity<GL_BUDGET_HDR, GL_BUDGET_DTL>(gL_BUDGET_HDR, tmpChildEntity, gL_BUDGET_DTL, true);
                            savedBool = true;
                            break;
                        }
                }
                if (ErrorCollection.Count > 0)
                {
                    return;
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }





        private void FillComboBox()
        {
            ComboFilling.fn_getOrganisationDetails(ref ddlCompanyName);
            // ComboFilling.fn_getGlobalSegment(ref ddlGlobalSegment);

            ComboFilling.fn_getBudgetType(ref ddlBudgetType);

            ddlCompanyName.SelectedValue = VMVServices.Web.Utils.OrganizationID;
            ddlCompanyName.Enabled = false;

            if (ddlCompanyName.SelectedValue != string.Empty)
            {
                Segments_BLL.GetGlobalSegmentvalues(ref ddlGlobalSegment, ddlCompanyName.SelectedValue.ToString());
            }

        }

        private void FillComboJEDetails()
        {
            ComboFilling.fn_getSegment1Details(ref ddlSegment1);
            ComboFilling.fn_getSegment2Details(ref ddlSegment2);
            ComboFilling.fn_getSegment3Details(ref ddlSegment3);
            //ComboFilling.fn_getSegment4Details(ref ddlSegment4);
            //ComboFilling.fn_getSegment5Details(ref ddlSegment5);
            //ComboFilling.fn_getSegment6Details(ref ddlSegment6);
        }


        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                //FillComboJEDetails();

                DropDownList ddlAccountGroup = tmpgvr.FindControl("ddlAccountGroup") as DropDownList;
                DropDownList ddlAccount = tmpgvr.FindControl("ddlAccount") as DropDownList;


                FIN.BLL.GL.AccountingGroups_BLL.fn_getAccountGroup(ref ddlAccountGroup);
                FIN.BLL.GL.AccountCodes_BLL.fn_getAccount(ref ddlAccount);


                if (gvData.EditIndex >= 0)
                {
                    ddlAccountGroup.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.GROUP_ID].ToString();
                    ddlAccount.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.CODE_ID].ToString();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                if (dtData.Rows.Count > 0)
                {


                    dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("ACTUAL_PREVIOUS_AMOUNT", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("ACTUAL_PREVIOUS_AMOUNT"))));



                    dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("BUDGET_MONTH_AMT_01", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("BUDGET_MONTH_AMT_01"))));



                    dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("BUDGET_MONTH_AMT_02", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("BUDGET_MONTH_AMT_02"))));

                    dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("BUDGET_MONTH_AMT_03", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("BUDGET_MONTH_AMT_03"))));
                    dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("BUDGET_MONTH_AMT_04", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("BUDGET_MONTH_AMT_04"))));
                    dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("BUDGET_MONTH_AMT_05", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("BUDGET_MONTH_AMT_05"))));
                    dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("BUDGET_MONTH_AMT_06", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("BUDGET_MONTH_AMT_06"))));
                    dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("BUDGET_MONTH_AMT_07", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("BUDGET_MONTH_AMT_07"))));
                    dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("BUDGET_MONTH_AMT_08", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("BUDGET_MONTH_AMT_08"))));
                    dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("BUDGET_MONTH_AMT_09", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("BUDGET_MONTH_AMT_09"))));
                    dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("BUDGET_MONTH_AMT_10", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("BUDGET_MONTH_AMT_10"))));
                    dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("BUDGET_MONTH_AMT_11", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("BUDGET_MONTH_AMT_11"))));
                    dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("BUDGET_MONTH_AMT_12", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("BUDGET_MONTH_AMT_12"))));

                    dtData.AcceptChanges();

                }


                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);
            }

            catch (Exception ex)
            {
                ErrorCollection.Add("BudgetEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            if (Session[FINSessionConstants.GridData] != null)
            {
                dtGridData = (DataTable)Session[FINSessionConstants.GridData];
            }
            gvData.EditIndex = -1;

            BindGrid(dtGridData);

        }
        #endregion

        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BudgetEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {


            System.Collections.SortedList slControls = new System.Collections.SortedList();
            DropDownList ddlAccountGroup = gvr.FindControl("ddlAccountGroup") as DropDownList;
            DropDownList ddlAccount = gvr.FindControl("ddlAccount") as DropDownList;
            TextBox txtActualPreviousAmt = gvr.FindControl("txtActualPreviousAmt") as TextBox;
            TextBox txtPercentage = gvr.FindControl("txtPercentage") as TextBox;

            TextBox txtPeriod01 = gvr.FindControl("txtPeriod01") as TextBox;
            TextBox txtPeriod02 = gvr.FindControl("txtPeriod02") as TextBox;
            TextBox txtPeriod03 = gvr.FindControl("txtPeriod03") as TextBox;
            TextBox txtPeriod04 = gvr.FindControl("txtPeriod04") as TextBox;
            TextBox txtPeriod05 = gvr.FindControl("txtPeriod05") as TextBox;
            TextBox txtPeriod06 = gvr.FindControl("txtPeriod06") as TextBox;
            TextBox txtPeriod07 = gvr.FindControl("txtPeriod07") as TextBox;
            TextBox txtPeriod08 = gvr.FindControl("txtPeriod08") as TextBox;
            TextBox txtPeriod09 = gvr.FindControl("txtPeriod09") as TextBox;
            TextBox txtPeriod10 = gvr.FindControl("txtPeriod10") as TextBox;
            TextBox txtPeriod11 = gvr.FindControl("txtPeriod11") as TextBox;
            TextBox txtPeriod12 = gvr.FindControl("txtPeriod12") as TextBox;
            DropDownList ddlSegment1 = gvr.FindControl("ddlSegment1") as DropDownList;
            DropDownList ddlSegment2 = gvr.FindControl("ddlSegment2") as DropDownList;
            DropDownList ddlSegment3 = gvr.FindControl("ddlSegment3") as DropDownList;
            DropDownList ddlSegment4 = gvr.FindControl("ddlSegment4") as DropDownList;
            DropDownList ddlSegment5 = gvr.FindControl("ddlSegment5") as DropDownList;
            DropDownList ddlSegment6 = gvr.FindControl("ddlSegment6") as DropDownList;

            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList[FINColumnConstants.BUDGET_DTL_ID] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }

            if (Session["singlePeriod"] != null && Session["singlePeriod"] != string.Empty)
            {
                txtPeriod01.Text = Session["singlePeriod"].ToString();
                txtPeriod02.Text = Session["singlePeriod"].ToString();
                txtPeriod03.Text = Session["singlePeriod"].ToString();
                txtPeriod04.Text = Session["singlePeriod"].ToString();
                txtPeriod05.Text = Session["singlePeriod"].ToString();
                txtPeriod06.Text = Session["singlePeriod"].ToString();
                txtPeriod07.Text = Session["singlePeriod"].ToString();
                txtPeriod08.Text = Session["singlePeriod"].ToString();
                txtPeriod09.Text = Session["singlePeriod"].ToString();
                txtPeriod10.Text = Session["singlePeriod"].ToString();
                txtPeriod11.Text = Session["singlePeriod"].ToString();
                txtPeriod12.Text = Session["singlePeriod"].ToString();

            }


            slControls[0] = ddlAccountGroup;
            slControls[1] = ddlAccount;
            slControls[2] = txtPeriod01;
            slControls[3] = txtPeriod02;
            slControls[4] = txtPeriod03;
            slControls[5] = txtPeriod04;
            slControls[6] = txtPeriod05;
            slControls[7] = txtPeriod06;
            slControls[8] = txtPeriod07;
            slControls[9] = txtPeriod08;
            slControls[10] = txtPeriod09;
            slControls[11] = txtPeriod10;
            slControls[12] = txtPeriod11;
            slControls[13] = txtPeriod12;
            //slControls[14] = ddlSegment1;
            //slControls[15] = ddlSegment2;
            //slControls[16] = ddlSegment3;
            //slControls[17] = ddlSegment4;
            //slControls[18] = ddlSegment5;
            //slControls[19] = ddlSegment6;


            ErrorCollection.Clear();
            string strCtrlTypes = FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX;

            string strMessage = "Account Group ~ Account ~ Period 1 ~ Period 2 ~ Period 3 ~ Period 4 ~ Period 5 ~ Period 6 ~ Period 7 ~ Period 8 ~ Period 9 ~ Period 10 ~ Period 11 ~ Period 12 ";


            //string strCtrlTypes = FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST;

            //string strMessage = "Account Group ~ Account ~ Period 1 ~ Period 2 ~ Period 3 ~ Period 4 ~ Period 5 ~ Period 6 ~ Period 7 ~ Period 8 ~ Period 9 ~ Period 10 ~ Period 11 ~ Period 12 ~ Segment 1 ~ Segment 2 ~ Segment 3 ~ Segment 4 ~ Segment 5 ~ Segment 6 ";

            EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

            if (EmptyErrorCollection.Count > 0)
            {
                ErrorCollection = EmptyErrorCollection;
                return drList;
            }


            string strCondition = "BUDGET_CODE='" + ddlAccountGroup.SelectedValue.ToString() + "'";
            strMessage = FINMessageConstatns.RecordAlreadyExists;
            ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            if (ErrorCollection.Count > 0)
            {
                return drList;
            }

            drList[FINColumnConstants.GROUP_ID] = ddlAccountGroup.SelectedValue.ToString();
            drList[FINColumnConstants.GROUP_NAME] = ddlAccountGroup.SelectedItem.Text.ToString();
            drList[FINColumnConstants.CODE_ID] = ddlAccount.SelectedValue.ToString();
            drList[FINColumnConstants.CODE_NAME] = ddlAccount.SelectedItem.Text.ToString();
            if (txtActualPreviousAmt.Text.ToString().Length > 0)
            {
                drList["ACTUAL_PREVIOUS_AMOUNT"] = txtActualPreviousAmt.Text;
            }
            if (txtPercentage.Text.ToString().Length > 0)
            {
                drList["PERCENTAGE"] = txtPercentage.Text;
            }

            drList[FINColumnConstants.BUDGET_MONTH_AMT_01] = txtPeriod01.Text;
            drList[FINColumnConstants.BUDGET_MONTH_AMT_02] = txtPeriod02.Text;
            drList[FINColumnConstants.BUDGET_MONTH_AMT_03] = txtPeriod03.Text;
            drList[FINColumnConstants.BUDGET_MONTH_AMT_04] = txtPeriod04.Text;
            drList[FINColumnConstants.BUDGET_MONTH_AMT_05] = txtPeriod05.Text;
            drList[FINColumnConstants.BUDGET_MONTH_AMT_06] = txtPeriod06.Text;
            drList[FINColumnConstants.BUDGET_MONTH_AMT_07] = txtPeriod07.Text;
            drList[FINColumnConstants.BUDGET_MONTH_AMT_08] = txtPeriod08.Text;
            drList[FINColumnConstants.BUDGET_MONTH_AMT_09] = txtPeriod09.Text;
            drList[FINColumnConstants.BUDGET_MONTH_AMT_10] = txtPeriod10.Text;
            drList[FINColumnConstants.BUDGET_MONTH_AMT_11] = txtPeriod11.Text;
            drList[FINColumnConstants.BUDGET_MONTH_AMT_12] = txtPeriod12.Text;
            //drList[FINColumnConstants.BUDGET_SEGMENT_ID_1] = ddlSegment1.SelectedValue.ToString();
            //drList[FINColumnConstants.BUDGET_SEGMENT_ID_2] = ddlSegment2.SelectedValue.ToString();
            //drList[FINColumnConstants.BUDGET_SEGMENT_ID_3] = ddlSegment3.SelectedValue.ToString();
            //drList[FINColumnConstants.BUDGET_SEGMENT_ID_4] = ddlSegment4.SelectedValue.ToString();
            //drList[FINColumnConstants.BUDGET_SEGMENT_ID_5] = ddlSegment5.SelectedValue.ToString();
            //drList[FINColumnConstants.BUDGET_SEGMENT_ID_6] = ddlSegment6.SelectedValue.ToString();

            return drList;

        }

        //public void DataDuplication(System.Data.DataTable dtGridData, string strCondition, string strMessage)
        //{
        //    try
        //    {
        //        // SortedList ErrorCollection = new SortedList();
        //        if (dtGridData.Select(strCondition).Length > 0)
        //        {
        //            ErrorCollection.Add(strMessage, strMessage);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorCollection.Add("AccountingGroupLinksEntry", ex.Message);
        //    }
        //    finally
        //    {
        //        if (ErrorCollection.Count > 0)
        //        {
        //            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
        //            // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
        //        }
        //    }
        //    //return ErrorCollection;
        //}

        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BudgetEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BudgetEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BudgetEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BudgetEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BudgetEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        private void BindSegment()
        {
            try
            {
                ErrorCollection.Clear();
                DataTable dtdataAc = new DataTable();
                DropDownList ddlAccountCodes = new DropDownList();
                ddlAccountCodes.ID = "ddlAccount";

                if (gvData.FooterRow != null)
                {
                    if (gvData.EditIndex < 0)
                    {

                        ddlAccountCodes = (DropDownList)gvData.FooterRow.FindControl("ddlAccount");
                    }
                    else
                    {
                        ddlAccountCodes = (DropDownList)gvData.Rows[gvData.EditIndex].FindControl("ddlAccount");
                    }
                }
                else
                {
                    ddlAccountCodes = (DropDownList)gvData.Controls[0].Controls[1].FindControl("ddlAccount");
                }


                if (ddlAccountCodes != null)
                {
                    if (Master.Mode != FINAppConstants.Add)
                    {
                        if (Session["Accountcode"] != null)
                        {
                            ddlAccountCodes.SelectedValue = Session["Accountcode"].ToString();
                        }
                    }
                    if (ddlAccountCodes.SelectedValue != null)
                    {
                        dtdataAc = DBMethod.ExecuteQuery(Segments_DAL.GetSegmentBasedAccCode(ddlAccountCodes.SelectedValue.ToString())).Tables[0];
                        if (dtdataAc != null)
                        {
                            if (dtdataAc.Rows.Count > 0)
                            {
                                int j = dtdataAc.Rows.Count;

                                for (int i = 0; i < dtdataAc.Rows.Count; i++)
                                {
                                    if (i == 0)
                                    {
                                        //segment1
                                        if (dtdataAc.Rows[i]["segment_id"].ToString() != string.Empty)
                                        {
                                            Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment1, ddlAccountCodes.SelectedValue.ToString(), dtdataAc.Rows[i]["segment_id"].ToString());
                                        }
                                        else
                                        {
                                            Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment1, ddlAccountCodes.SelectedValue.ToString(), "0");
                                        }

                                    }
                                    if (i == 1)
                                    {
                                        //segment2
                                        if (dtdataAc.Rows[i]["segment_id"].ToString() != string.Empty)
                                        {
                                            Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment2, ddlAccountCodes.SelectedValue.ToString(), dtdataAc.Rows[i]["segment_id"].ToString());
                                        }
                                        else
                                        {
                                            Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment2, ddlAccountCodes.SelectedValue.ToString(), "0");
                                        }
                                    }
                                    if (i == 2)
                                    {
                                        //Segments3
                                        if (dtdataAc.Rows[i]["segment_id"].ToString() != string.Empty)
                                        {
                                            Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment3, ddlAccountCodes.SelectedValue.ToString(), dtdataAc.Rows[i]["segment_id"].ToString());
                                        }
                                        else
                                        {
                                            Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment3, ddlAccountCodes.SelectedValue.ToString(), "0");
                                        }
                                    }
                                    if (i == 3)
                                    {
                                        //segment4
                                        if (dtdataAc.Rows[i]["segment_id"].ToString() != string.Empty)
                                        {
                                            Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment4, ddlAccountCodes.SelectedValue.ToString(), dtdataAc.Rows[i]["segment_id"].ToString());
                                        }
                                        else
                                        {
                                            Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment4, ddlAccountCodes.SelectedValue.ToString(), "0");
                                        }
                                    }
                                    if (i == 4)
                                    {
                                        //segment5
                                        if (dtdataAc.Rows[i]["segment_id"].ToString() != string.Empty)
                                        {
                                            Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment5, ddlAccountCodes.SelectedValue.ToString(), dtdataAc.Rows[i]["segment_id"].ToString());
                                        }
                                        else
                                        {
                                            Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment5, ddlAccountCodes.SelectedValue.ToString(), "0");
                                        }
                                    }
                                    if (i == 5)
                                    {
                                        //segment6
                                        if (dtdataAc.Rows[i]["segment_id"].ToString() != string.Empty)
                                        {
                                            Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment6, ddlAccountCodes.SelectedValue.ToString(), dtdataAc.Rows[i]["segment_id"].ToString());
                                        }
                                        else
                                        {
                                            Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment6, ddlAccountCodes.SelectedValue.ToString(), "0");
                                        }
                                    }
                                }
                                if (j == 1)
                                {
                                    Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment2, ddlAccountCodes.SelectedValue.ToString(), "0");
                                    Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment3, ddlAccountCodes.SelectedValue.ToString(), "0");
                                    Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment4, ddlAccountCodes.SelectedValue.ToString(), "0");
                                    Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment5, ddlAccountCodes.SelectedValue.ToString(), "0");
                                    Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment6, ddlAccountCodes.SelectedValue.ToString(), "0");
                                }
                                else if (j == 2)
                                {
                                    Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment3, ddlAccountCodes.SelectedValue.ToString(), "0");
                                    Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment4, ddlAccountCodes.SelectedValue.ToString(), "0");
                                    Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment5, ddlAccountCodes.SelectedValue.ToString(), "0");
                                    Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment6, ddlAccountCodes.SelectedValue.ToString(), "0");
                                }
                                else if (j == 3)
                                {
                                    // Segments_BLL.GetGlobalSegmentvalues(ref ddlSegment4, VMVServices.Web.Utils.OrganizationID);

                                    Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment4, ddlAccountCodes.SelectedValue.ToString(), "0");
                                    Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment5, ddlAccountCodes.SelectedValue.ToString(), "0");
                                    Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment6, ddlAccountCodes.SelectedValue.ToString(), "0");
                                }
                                else if (j == 4)
                                {
                                    Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment5, ddlAccountCodes.SelectedValue.ToString(), "0");
                                    Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment6, ddlAccountCodes.SelectedValue.ToString(), "0");
                                }
                                else if (j == 5)
                                {
                                    Segments_BLL.GetSegmentvaluesBasedAccSegment(ref ddlSegment6, ddlAccountCodes.SelectedValue.ToString(), "0");
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BdYCs", ex.Message);
                //throw ex;
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void BindSegmentValues(int rowIndexValue)
        {
            if (Master.Mode != FINAppConstants.Add)
            {
                if (gvData.Rows.Count > 0)
                {
                    //Segments_BLL.GetGlobalSegmentvalues(ref ddlSegment1);
                    //Segments_BLL.GetGlobalSegmentvalues(ref ddlSegment2);
                    //Segments_BLL.GetGlobalSegmentvalues(ref ddlSegment3);
                    //Segments_BLL.GetGlobalSegmentvalues(ref ddlSegment4);
                    //Segments_BLL.GetGlobalSegmentvalues(ref ddlSegment5);
                    //Segments_BLL.GetGlobalSegmentvalues(ref ddlSegment6);

                    //for (int iLoop = 0; iLoop < gvData.Rows.Count; iLoop++)
                    //{
                    int iLoop = 0;
                    iLoop = rowIndexValue;

                    Session["Accountcode"] = gvData.DataKeys[iLoop].Values["ACCT_CODE_ID"].ToString();

                    if (Master.Mode != FINAppConstants.Add)
                    {
                        BindSegment();

                        string seg1 = gvData.DataKeys[iLoop].Values[FINColumnConstants.JE_SEGMENT_ID_1].ToString();
                        string seg2 = gvData.DataKeys[iLoop].Values[FINColumnConstants.JE_SEGMENT_ID_2].ToString();
                        string seg3 = gvData.DataKeys[iLoop].Values[FINColumnConstants.JE_SEGMENT_ID_3].ToString();
                        string seg4 = gvData.DataKeys[iLoop].Values[FINColumnConstants.JE_SEGMENT_ID_4].ToString();
                        string seg5 = gvData.DataKeys[iLoop].Values[FINColumnConstants.JE_SEGMENT_ID_5].ToString();
                        string seg6 = gvData.DataKeys[iLoop].Values[FINColumnConstants.JE_SEGMENT_ID_6].ToString();

                        ddlSegment1.SelectedValue = seg1;
                        ddlSegment2.SelectedValue = seg2;
                        ddlSegment3.SelectedValue = seg3;
                        ddlSegment4.SelectedValue = seg4;
                        ddlSegment5.SelectedValue = seg5;
                        ddlSegment6.SelectedValue = seg6;
                    }
                    //}
                }
            }
        }
        protected void btnClick_Click(object sender, EventArgs e)
        {

            GridViewRow gridViewRow = (GridViewRow)(sender as Control).Parent.Parent;
            int index = gridViewRow.RowIndex;
            BindSegmentValues(index);

            //if (Master.Mode == FINAppConstants.Update)
            //{
            //    DataTable dtperiod = new DataTable();
            //    dtperiod = DBMethod.ExecuteQuery(FIN.DAL.GL.JournalEntry_DAL.getSegmentDetails(Master.StrRecordId)).Tables[0];

            //    if (dtperiod != null)
            //    {
            //        if (dtperiod.Rows.Count > 0)
            //        {
            //            ddlSegment1.SelectedItem.Value = dtperiod.Rows[0]["segment_value"].ToString();
            //            ddlSegment2.SelectedItem.Value = dtperiod.Rows[1]["segment_value"].ToString();
            //            ddlSegment3.SelectedItem.Value = dtperiod.Rows[2]["segment_value"].ToString();
            //            ddlSegment4.SelectedItem.Value = dtperiod.Rows[3]["segment_value"].ToString();
            //            ddlSegment5.SelectedItem.Value = dtperiod.Rows[4]["segment_value"].ToString();
            //            ddlSegment6.SelectedItem.Value = dtperiod.Rows[5]["segment_value"].ToString();
            //        }
            //    }
            //}

            // GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;

            //ImageButton btndetails = sender as ImageButton;
            //GridViewRow gvrow = (GridViewRow)btndetails.NamingContainer;
            //lblID.Text = gvData.DataKeys[gvrow.RowIndex].Value.ToString();


            ModalPopupExtender2.Show();
        }





        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>


        #endregion




        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                DBMethod.DeleteEntity<GL_BUDGET_HDR>(gL_BUDGET_HDR);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session[FINSessionConstants.GridData], "Budget Entry Details");

                if (ErrorCollection.Count > 0)
                {
                    return;
                }


                AssignToBE();

                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                }


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        protected void btnOkay_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                System.Collections.SortedList slControls = new System.Collections.SortedList();

                slControls[0] = ddlSegment1;
                slControls[1] = ddlSegment2;
                slControls[2] = ddlSegment3;
                slControls[3] = ddlSegment4;
                slControls[4] = ddlSegment5;
                slControls[5] = ddlSegment6;

                ErrorCollection.Clear();
                string strCtrlTypes = FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST;

                string strMessage = "Segment1 ~ Segment2 ~ Segment3~ Segment4 ~ Segment5 ~ Segment6";

                EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

                //if (EmptyErrorCollection.Count > 0)
                //{
                //    ErrorCollection = EmptyErrorCollection;
                //    return;
                //}
                //else
                //{
                ModalPopupExtender2.Hide();
                // }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void btnDistributetoperiods_Click(object sender, EventArgs e)
        {
            if (txtAnnualBudgetAmount.Text.Trim() != string.Empty)
            {

                Session["singlePeriod"] = Math.Round(CommonUtils.ConvertStringToDecimal(txtAnnualBudgetAmount.Text) / 12, int.Parse(VMVServices.Web.Utils.DecimalPrecision));

            }
            else
            {
                ErrorCollection.Add("BYC1", " Anual budjet amount Can't be Empty");
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }

            }
        }

        protected void gvData_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void gvData_SelectedIndexChanged1(object sender, EventArgs e)
        {

        }

        protected void ddlAccount_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindSegment();
        }

        protected void ddlAccountGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtPreviousAmount = new DataTable();
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;

            DropDownList ddlAccountGroup = gvr.FindControl("ddlAccountGroup") as DropDownList;
            TextBox txtActualPreviousAmt = gvr.FindControl("txtActualPreviousAmt") as TextBox;

            dtPreviousAmount = DBMethod.ExecuteQuery(FIN.DAL.GL.ActualandProjectedBudget_DAL.getPreviousYrAmt(ddlAccountGroup.SelectedValue).ToString()).Tables[0];
            if (dtPreviousAmount.Rows.Count > 0)
            {
                txtActualPreviousAmt.Text = dtPreviousAmount.Rows[0]["ACTUAL_PREVIOUS_AMOUNT"].ToString();
            }
        }
    }
}