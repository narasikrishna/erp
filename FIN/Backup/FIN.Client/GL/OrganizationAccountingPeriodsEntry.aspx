﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="OrganizationAccountingPeriodsEntry.aspx.cs" Inherits="FIN.Client.GL.OrganizationAccountingPeriodsEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 700px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style=" " id="lblCalendarYear">
                Calendar Year
            </div>  <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="divtxtBox LNOrient" style="  width: 250px">
                <asp:DropDownList ID="ddlCalendarYear" MaxLength="50" TabIndex="1" CssClass="validate[required] RequiredField ddlStype"
                    Width="200px" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlCalendarYear_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div  class="LNOrient">
            <asp:GridView ID="gvperiods" runat="server" AutoGenerateColumns="False" CssClass="DisplayFont  Grid"
                DataKeyNames="PERIOD_ID,LOOKUP_ID,CAL_DTL_ID,PERIOD_TYPE,PERIOD_NAME" OnRowDataBound="gvperiod_RowDataBound"
                Width="100%">
                <Columns>
                    <asp:TemplateField HeaderText="Period Name">
                        <EditItemTemplate>
                            <asp:TextBox ID="TextBox1" Text='<%# Eval("PERIOD_NAME") %>' runat="server"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="TextBox1" Text='<%# Eval("PERIOD_NAME") %>' runat="server"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblPeriodName" runat="server" Text='<%# Eval("PERIOD_NAME") %>' Width="80px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Period Type">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtPeriodtype" Text='<%# Eval("PERIOD_TYPE") %>' runat="server"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtPeriodtype" Text='<%# Eval("PERIOD_TYPE") %>' runat="server"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblPeriodtype" runat="server" Text='<%# Eval("PERIOD_TYPE") %>' Width="80px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="From Date">
                        <EditItemTemplate>
                            <asp:TextBox ID="dtfromdate" runat="server" CssClass="txtBox" Text='' MaxLength="10"></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtfromdate" runat="server" Format="dd/MM/yyyy"
                                TargetControlID="dtfromdate">
                            </cc2:CalendarExtender>
                            <cc2:FilteredTextBoxExtender ID="Fillfromdte" runat="server" ValidChars="/" FilterType="Numbers,Custom"
                                TargetControlID="dtfromdate" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="dtfromdate" runat="server" CssClass="txtBox" Text='' MaxLength="10"></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtenderfromdate" runat="server" Format="dd/MM/yyyy"
                                TargetControlID="dtfromdate">
                            </cc2:CalendarExtender>
                            <cc2:FilteredTextBoxExtender ID="Filteredfromdate" runat="server" ValidChars="/"
                                FilterType="Numbers,Custom" TargetControlID="dtfromdate" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblQbeExamDate" runat="server" Text='<%# Eval("PERIOD_FROM_DT","{0:dd/MM/yyyy}") %>'
                                Width="100px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="To Date">
                        <EditItemTemplate>
                            <asp:TextBox ID="dttodate" runat="server" CssClass="EntryFont RequiredField txtBox"
                                Text='' MaxLength="10"></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtendertodate" runat="server" Format="dd/MM/yyyy"
                                TargetControlID="dttodate">
                            </cc2:CalendarExtender>
                            <cc2:FilteredTextBoxExtender ID="Filteredtodate" runat="server" ValidChars="/" FilterType="Numbers,Custom"
                                TargetControlID="dttodate" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="dttodate" runat="server" CssClass="txtBox" Text='' MaxLength="10"></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExttodate" runat="server" Format="dd/MM/yyyy" TargetControlID="dttodate">
                            </cc2:CalendarExtender>
                            <cc2:FilteredTextBoxExtender ID="FilteredtoDate" runat="server" ValidChars="/" FilterType="Numbers,Custom"
                                TargetControlID="dttodate" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lbltoDate" runat="server" Text='<%# Eval("PERIOD_TO_DT","{0:dd/MM/yyyy}") %>'
                                Width="100px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Period Status" >
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlStatus" runat="server" CssClass="NRddlStype" Width="120px">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlStatus" runat="server" CssClass="NRddlStype" Width="120px">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:DropDownList ID="ddlStatus" runat="server" CssClass="NRddlStype" Width="120px"
                                AutoPostBack="true" OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged">
                            </asp:DropDownList>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" OnClick="btnSave_Click"
                            TabIndex="23" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="24" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="25" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="26" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table width="100%">
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" CssClass="button" ID="btnYes" Text="Yes" Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/GL/GLChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

    </script>
</asp:Content>
