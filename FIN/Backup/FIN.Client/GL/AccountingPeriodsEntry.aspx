﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="AccountingPeriodsEntry.aspx.cs" Inherits="FIN.Client.GL.AccountingPeriodsEntry" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 700px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 150px" id="lblCalendarName">
                Calendar Name
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:TextBox ID="txtCalendarName" CssClass="RequiredField txtBox" runat="server" TabIndex="1"></asp:TextBox>
            </div>
            <div class="lblBox" style="float: left; width: 200px" id="lblEffectiveDate">
                Effective Date
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:TextBox ID="txtEffectiveDate" CssClass="RequiredField txtBox" runat="server" 
                    TabIndex="2"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 150px" id="lblAdjPeriod">
                Adjuesting Period
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:TextBox ID="txtAdjPeriod" CssClass="RequiredField txtBox" runat="server" 
                    ontextchanged="txtAdjPeriod_TextChanged" TabIndex="3"></asp:TextBox>
            </div>
            <div class="lblBox" style="float: left; width: 200px" id="lblEndDate">
                End Date
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:TextBox ID="txtEndDate" CssClass="RequiredField txtBox" runat="server" TabIndex="4"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 150px" id="lblAccountingYear">
                Accounting Year
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:TextBox ID="txtAdjustingYear" CssClass="RequiredField txtBox" runat="server" 
                    TabIndex="5"></asp:TextBox>
            </div>
            <div class="lblBox" style="float: left; width: 200px" id="lblAccoutingbeginDate">
                Accounting Begin Date
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:TextBox ID="txtAccoutingBeginDate" CssClass="RequiredField txtBox" runat="server" 
                    TabIndex="6"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
          
            <div class="lblBox" style="float: left; width: 150px" id="Div2">
                Accounting End Date
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:TextBox ID="TextBox2" CssClass="txtBox" runat="server" TabIndex="7"></asp:TextBox>
            </div>
        </div>
        <div >
        <div align="center">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                DataKeyNames="FACILITY_DETAILS_ID,VALUE_KEY_ID,DELETED" OnRowCancelingEdit="gvData_RowCancelingEdit"
                OnRowCommand="gvData_RowCommand" OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound"
                OnRowDeleting="gvData_RowDeleting" OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating"
                ShowFooter="True">
              <Columns>
                                        <asp:TemplateField HeaderText="Period Name">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="TextBox1" Text='<%# Eval("PERIOD_NAME") %>' runat="server"></asp:TextBox>
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:TextBox ID="TextBox1" Text='<%# Eval("PERIOD_NAME") %>' runat="server"></asp:TextBox>
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblPeriodName" runat="server" Text='<%# Eval("PERIOD_NAME") %>' Width="80px"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Period Type">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtPeriodtype" Text='<%# Eval("PERIOD_TYPE") %>' runat="server"></asp:TextBox>
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:TextBox ID="txtPeriodtype" Text='<%# Eval("PERIOD_TYPE") %>' runat="server"></asp:TextBox>
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblPeriodtype" runat="server" Text='<%# Eval("PERIOD_TYPE") %>' Width="80px"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="From Date">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="dtfromdate" runat="server" CssClass="txtBox" Text='' MaxLength="10"></asp:TextBox>
                                                <cc2:CalendarExtender ID="CalendarExtfromdate" runat="server" Format="dd/MM/yyyy"
                                                    TargetControlID="dtfromdate">
                                                </cc2:CalendarExtender>
                                                <cc2:FilteredTextBoxExtender ID="Fillfromdte" runat="server" ValidChars="/" FilterType="Numbers,Custom"
                                                    TargetControlID="dtfromdate" />
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:TextBox ID="dtfromdate" runat="server" CssClass="txtBox" Text='' MaxLength="10"></asp:TextBox>
                                                <cc2:CalendarExtender ID="CalendarExtenderfromdate" runat="server" Format="dd/MM/yyyy"
                                                    TargetControlID="dtfromdate">
                                                </cc2:CalendarExtender>
                                                <cc2:FilteredTextBoxExtender ID="Filteredfromdate" runat="server" ValidChars="/"
                                                    FilterType="Numbers,Custom" TargetControlID="dtfromdate" />
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblQbeExamDate" runat="server" Text='<%# Eval("PERIOD_FROM_DT","{0:dd/MM/yyyy}") %>'
                                                    Width="100px"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="To Date">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="dttodate" runat="server" CssClass="EntryFont RequiredField txtBox"
                                                    Text='' MaxLength="10"></asp:TextBox>
                                                <cc2:CalendarExtender ID="CalendarExtendertodate" runat="server" Format="dd/MM/yyyy"
                                                    TargetControlID="dttodate">
                                                </cc2:CalendarExtender>
                                                <cc2:FilteredTextBoxExtender ID="Filteredtodate" runat="server" ValidChars="/" FilterType="Numbers,Custom"
                                                    TargetControlID="dttodate" />
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:TextBox ID="dttodate" runat="server" CssClass="txtBox" Text='' MaxLength="10"></asp:TextBox>
                                                <cc2:CalendarExtender ID="CalendarExttodate" runat="server" Format="dd/MM/yyyy" TargetControlID="dttodate">
                                                </cc2:CalendarExtender>
                                                <cc2:FilteredTextBoxExtender ID="FilteredtoDate" runat="server" ValidChars="/" FilterType="Numbers,Custom"
                                                    TargetControlID="dttodate" />
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lbltoDate" runat="server" Text='<%# Eval("PERIOD_TO_DT","{0:dd/MM/yyyy}") %>'
                                                    Width="100px"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Period Status">
                                            <EditItemTemplate>
                                                <asp:DropDownList ID="ddlStatus" runat="server" CssClass="ddlStype" Width="80px">
                                                </asp:DropDownList>
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:DropDownList ID="ddlStatus" runat="server" CssClass="ddlStype" Width="80px">
                                                </asp:DropDownList>
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblStatus" runat="server" Text='<%# Eval("PERIOD_STATUS") %>' Width="80px"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <%-- <asp:TemplateField HeaderText="Add / Edit">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="ibtnEdit" runat="server" AlternateText="Edit" CausesValidation="false"
                                                    CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                                                <asp:ImageButton ID="ibtnDelete" runat="server" AlternateText="Delete" CausesValidation="false"
                                                    CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:ImageButton ID="ibtnUpdate" runat="server" AlternateText="Update" CommandName="Update"
                                                    Height="20px" ImageUrl="~/Images/Update.png" />
                                                <asp:ImageButton ID="ibtnCancel" runat="server" AlternateText="Cancel" CausesValidation="false"
                                                    CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:ImageButton ID="ibtnInsert" runat="server" AlternateText="Add" CommandName="FooterInsert"
                                                    ImageUrl="~/Images/Add.jpg" />
                                            </FooterTemplate>
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>--%>
                                    </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>


        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" 
                            TabIndex="7" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" 
                            TabIndex="8" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" 
                            TabIndex="9" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" 
                            TabIndex="10" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table width="100%">
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/GL/AccountingGroups.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () { $("#form1").validationEngine(); });
        $("#FINContent_btnSave").click(function () {
            if ($("#form1").validationEngine('validate') == false)
                return false;
        });
    </script>
</asp:Content>
