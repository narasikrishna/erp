﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="BalancesEntry.aspx.cs" Inherits="FIN.Client.GL.BalancesEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function printDiv(divid) {


            var divToPrint = document.getElementById(divid);
            var popupWin = window.open("", "_blank", "width=700,height=800");
            //popupWin.document.open("", "_blank", "width=700,height=800");
            popupWin.document.writeln("<html><head><title></title></head><body>" + divToPrint.innerHTML + "</body>");
            popupWin.document.close();
            popupWin.focus();
            popupWin.print();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 650px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" style="display: none">
            <div class="lblBox LNOrient" style="  width: 150px" id="lblCalendarName">
                Company
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:DropDownList ID="ddlCompany" runat="server" CssClass="ddlStype validate[required] RequiredField "
                    TabIndex="1">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 150px" id="lblAdjPeriod">
                Global Segment
            </div>
            <div class="divtxtBox LNOrient" style="  width: 470px">
                <asp:DropDownList ID="ddlsegment" runat="server" CssClass="ddlStype validate[required] RequiredField "
                    TabIndex="2" AutoPostBack="false" OnSelectedIndexChanged="ddlsegment_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 150px" id="lblEffectiveDate">
                Period
            </div>
            <div class="divtxtBox LNOrient" style="  width: 470px">
                <asp:DropDownList ID="ddlperiod" runat="server" CssClass="ddlStype validate[required] RequiredField "
                    TabIndex="3" AutoPostBack="false" OnSelectedIndexChanged="ddlsegment_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 150px" id="Div2">
                Account Description
            </div>
            <div class="divtxtBox LNOrient" style="  width: 470px">
                <asp:DropDownList ID="ddlAcctcode" runat="server" CssClass="ddlStype validate[required] RequiredField "
                    TabIndex="4" AutoPostBack="false" OnSelectedIndexChanged="ddlAcctcode_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" runat="server" id="chk" visible="false">
            <div class="lblBox LNOrient" style="  width: 150px" id="Div1">
                UnPosted
            </div>
            <div class="divtxtBox LNOrient" style="  width: 470px">
                <asp:CheckBox ID="chkUnposted" runat="server" Checked="false" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 150px" id="Div3">
            </div>
            <div class="divtxtBox LNOrient" style="float: right; width: 470px" align="right">
                <asp:ImageButton ID="btnView" runat="server" ImageUrl="~/Images/view.png" OnClick="btnView_Click"
                    Style="border: 0px" />
                <%--<asp:Button ID="btnView" runat="server" Text="View" OnClick="btnView_Click" />--%>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="DisplayFont  Grid"
                DataKeyNames="ACCT_CODE" EmptyDataText="No Record to Found" Width="100%">
                <Columns>
                    <asp:BoundField DataField="ACCT_CODE" HeaderText="Account No" />
                    <%--<asp:BoundField DataField="CURRENCY_CODE" HeaderText="Currency" />--%>
                    <asp:BoundField DataField="ACCT_CODE_DESC" HeaderText="Account Description" />
                    <asp:BoundField DataField="BEGIN_BALANCE_ACCT_CR" HeaderText="Account Begin Balance Credit" />
                    <asp:BoundField DataField="BEGIN_BALANCE_ACCT_DR" HeaderText="Account Begin Balance Debit" />
                    <asp:BoundField DataField="ACCOUNTED_CR" HeaderText="Accounted Credit" />
                    <asp:BoundField DataField="ACCOUNTED_DR" HeaderText="Accounted Debit" />
                    <asp:BoundField DataField="CLOSING_BALANCE_ACCT_CR" HeaderText="Account Closing Balance Credit" />
                    <asp:BoundField DataField="CLOSING_BALANCE_ACCT_DR" HeaderText="Account Closing Balance Debit" />
                    <asp:TemplateField HeaderText="Details">
                        <ItemTemplate>
                            <asp:ImageButton ID="btnDetails" runat="server" ImageUrl="~/Images/details.png" OnClick="btnDetails_Click"
                                TabIndex="10" Style="border: 0px" CommandName="Select" />
                            <%-- <asp:Button ID="btnDetails" runat="server" CssClass="button" Text="Details" TabIndex="10"
                                OnClick="btnDetails_Click" CommandName="Select" />--%>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="btnDetails" runat="server" ImageUrl="~/Images/details.png" OnClick="btnDetails_Click"
                                TabIndex="10" Style="border: 0px" CommandName="Select" />
                            <%-- <asp:Button ID="btnDetails" runat="server" CssClass="button" Text="Details" TabIndex="10"
                                CommandName="Select" OnClick="btnDetails_Click" />--%>
                        </EditItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                        <FooterStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divRowContainer divAction" style="display: none">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" OnClick="btnSave_Click1"
                            TabIndex="16" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="17" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="18" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="19" />
                    </td>
                </tr>
            </table>
        </div>
        <div>
            <asp:HiddenField ID="btnDetails" runat="server" />
            <cc2:ModalPopupExtender ID="ModalPopupExtender4" runat="server" TargetControlID="btnDetails"
                PopupControlID="Panel1" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="Panel1" runat="server" Height="300px" BackColor="White" ScrollBars="Auto">
                <div class="" style="overflow: auto">
                    <table width="100%">
                        <tr class="divFormcontainer">
                            <td colspan="4">
                                <div id="div_ABB">
                                    <asp:GridView ID="gvABB" runat="server" AutoGenerateColumns="False" CssClass="DisplayFont  Grid"
                                        DataKeyNames="ACCT_CODE,CURRENCY_CODE" EmptyDataText="No Record to Found" Width="100%">
                                        <Columns>
                                            <asp:BoundField DataField="CURRENCY_CODE" HeaderText="Currency" />
                                            <asp:BoundField DataField="BEGIN_BALANCE_CR" HeaderText="Begin Balance Credit" />
                                            <asp:BoundField DataField="BEGIN_BALANCE_DR" HeaderText="Begin Balance Debit" />
                                            <asp:BoundField DataField="AMOUNT_CR" HeaderText="Amount Credit" />
                                            <asp:BoundField DataField="AMOUNT_DR" HeaderText="Amount Debit" />
                                            <asp:BoundField DataField="CLOSING_BALANCE_CR" HeaderText="Closing Balance Credit" />
                                            <asp:BoundField DataField="CLOSING_BALANCE_DR" HeaderText="Closing Balance Debit" />
                                            <asp:TemplateField HeaderText="Details">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="btnDetails2" runat="server" ImageUrl="~/Images/details.png"
                                                        OnClick="btnDetails2_Click" TabIndex="10" Style="border: 0px" CommandName="Select" />
                                                    <%-- <asp:Button ID="btnDetails2" runat="server" CssClass="button" Text="Details" TabIndex="10"
                                                        OnClick="btnDetails2_Click" CommandName="Select" />--%>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:ImageButton ID="btnDetails2" runat="server" ImageUrl="~/Images/details.png"
                                                        OnClick="btnDetails2_Click" TabIndex="10" Style="border: 0px" CommandName="Select" />
                                                    <%-- <asp:Button ID="btnDetails2" runat="server" CssClass="button" Text="Details" TabIndex="10"
                                                        CommandName="Select" OnClick="btnDetails2_Click" />--%>
                                                </EditItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" />
                                                <FooterStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                        </Columns>
                                        <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                        <HeaderStyle CssClass="GridHeader" />
                                        <AlternatingRowStyle CssClass="GrdAltRow" />
                                    </asp:GridView>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div style="float: left">
                                    <asp:ImageButton ID="btnCLosePopUp" runat="server" ImageUrl="~/Images/btnClose.png"
                                        Style="border: 0px" />
                                    <%-- <asp:Button runat="server" CssClass="DisplayFont button" ID="btnCLosePopUp" Text="Close"
                                        Width="60px" />--%>
                                </div>
                                <div style="float: right">
                                    <input id="btnprint" type="image" src="~/Images/Print.png" onclick="printDiv('div_ABB')" />
                                    <%-- <input id="btnprint" type="button" value="Print" class="DisplayFont button" onclick="printDiv('div_ABB')" />--%>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
        <div>
            <asp:HiddenField ID="btnDetails2" runat="server" />
            <cc2:ModalPopupExtender ID="MPCurrencywise" runat="server" TargetControlID="btnDetails2"
                PopupControlID="Panel2" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="Panel2" runat="server" Height="300px" BackColor="White" ScrollBars="Auto">
                <div class="" style="overflow: auto">
                    <table width="100%">
                        <tr class="divFormcontainer">
                            <td colspan="4">
                                <div id="div_AB_CW">
                                    <asp:GridView ID="gvAB_CW" runat="server" AutoGenerateColumns="False" CssClass="DisplayFont  Grid"
                                        DataKeyNames="ACCT_CODE,CURRENCY_CODE,PERIOD_DAY" EmptyDataText="No Record to Found"
                                        Width="100%">
                                        <Columns>
                                            <asp:BoundField DataField="PERIOD_DAY" HeaderText="Period Date" DataFormatString="{0:dd/MM/yyyy}" />
                                            <asp:TemplateField Visible="false">
                                                <ItemTemplate>
                                                    <asp:TextBox Visible="false" Text='<%# Eval("PERIOD_DAY") %>' runat="server" ID="txtPeriodDay"
                                                        CssClass="validate[,custom[ReqDateDDMMYYY],,]  txtBox" TabIndex="1"></asp:TextBox>
                                                    <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtPeriodDay" />
                                                    <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars="/"
                                                        FilterType="Numbers,Custom" TargetControlID="txtPeriodDay" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="CURRENCY_CODE" HeaderText="Currency" />
                                            <asp:BoundField DataField="AMOUNT_CR" HeaderText="Amount Credit" />
                                            <asp:BoundField DataField="AMOUNT_DR" HeaderText="Amount Debit" />
                                            <asp:BoundField DataField="ACCOUNTED_CR" HeaderText="Accounted Credit" />
                                            <asp:BoundField DataField="ACCOUNTED_DR" HeaderText="Accounted Debit" />
                                            <asp:TemplateField HeaderText="Details3">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="btnDetails3" runat="server" ImageUrl="~/Images/details.png"
                                                        OnClick="btnDetails3_Click" TabIndex="10" Style="border: 0px" CommandName="Select" />
                                                    <%--<asp:Button ID="btnDetails3" runat="server" CssClass="button" Text="Details" TabIndex="10"
                                                        OnClick="btnDetails3_Click" CommandName="Select" />--%>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:ImageButton ID="btnDetails3" runat="server" ImageUrl="~/Images/details.png"
                                                        OnClick="btnDetails3_Click" TabIndex="10" Style="border: 0px" CommandName="Select" />
                                                    <%--<asp:Button ID="btnDetails3" runat="server" CssClass="button" Text="Details" TabIndex="10"
                                                        CommandName="Select" OnClick="btnDetails3_Click" />--%>
                                                </EditItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" />
                                                <FooterStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                        </Columns>
                                        <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                        <HeaderStyle CssClass="GridHeader" />
                                        <AlternatingRowStyle CssClass="GrdAltRow" />
                                    </asp:GridView>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div style="float: left">
                                    <asp:ImageButton ID="btnCWBack" runat="server" ImageUrl="~/Images/btnBack.png" TabIndex="10"
                                        Style="border: 0px" OnClick="btnCWBack_Click" />
                                    <%--<asp:Button runat="server" CssClass="DisplayFont button" ID="btnCWBack" Text="Back"
                                        Width="60px" OnClick="btnCWBack_Click" />--%>
                                </div>
                                <div style="float: right">
                                    <input id="btn_div_AB_CW_print" type="image" src="~/Images/Print.png" onclick="printDiv('div_AB_CW')" />
                                    <%--<input id="btn_div_AB_CW_print" type="button" class="DisplayFont button" value="Print"
                                        onclick="printDiv('div_AB_CW')" />--%>
                                    &nbsp;&nbsp;
                                    <asp:ImageButton ID="Button1" runat="server" ImageUrl="~/Images/btnClose.png" Style="border: 0px" />
                                    <%--<asp:Button runat="server" CssClass="DisplayFont button" ID="Button1" Text="Close"
                                        Width="60px" />--%>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
        <div>
            <asp:HiddenField ID="btnDetails3" runat="server" />
            <cc2:ModalPopupExtender ID="MPDaywise" runat="server" TargetControlID="btnDetails3"
                PopupControlID="Panel3" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="Panel3" runat="server" Height="300px" BackColor="White" ScrollBars="Auto">
                <div class="" style="overflow: auto">
                    <table width="100%">
                        <tr class="divFormcontainer">
                            <td colspan="4">
                                <div id="div_AB_DW">
                                    <asp:GridView ID="gvAB_DW" runat="server" AutoGenerateColumns="False" CssClass="DisplayFont  Grid"
                                        DataKeyNames="JE_HDR_ID" Width="100%">
                                        <Columns>
                                            <asp:BoundField DataField="je_number" HeaderText="JV Number" />
                                            <asp:BoundField DataField="JE_CREDIT_DEBIT" HeaderText="Credit Debit" />
                                            <asp:BoundField DataField="JE_AMOUNT_CR" HeaderText="Amount Credit" />
                                            <asp:BoundField DataField="JE_AMOUNT_DR" HeaderText="Amount Debit" />
                                            <asp:BoundField DataField="JE_ACCOUNTED_AMT_CR" HeaderText="Accounted Amount Credit" />
                                            <asp:BoundField DataField="JE_ACCOUNTED_AMT_DR" HeaderText="Accounted Amount Debit" />
                                            <asp:TemplateField HeaderText="View">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="btnViewJV" runat="server" ImageUrl="~/Images/view.png" OnClick="btnViewJV_Click"
                                                        TabIndex="10" Style="border: 0px" CommandName="Select" />
                                                    <%--<asp:Button ID="btnViewJV" runat="server" CssClass="button" Text="View" TabIndex="10"
                                                        OnClick="btnViewJV_Click" CommandName="Select" />--%>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" />
                                                <FooterStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                        </Columns>
                                        <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                        <HeaderStyle CssClass="GridHeader" />
                                        <AlternatingRowStyle CssClass="GrdAltRow" />
                                    </asp:GridView>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div style="float: left">
                                    <asp:ImageButton ID="btnDaywiseBack" runat="server" ImageUrl="~/Images/btnBack.png"
                                        TabIndex="10" Style="border: 0px" OnClick="btnDaywiseBack_Click" />
                                    <%--<asp:Button runat="server" CssClass="DisplayFont button" ID="btnDaywiseBack" Text="Back"
                                        Width="60px" OnClick="btnDaywiseBack_Click" />--%>
                                </div>
                                <div style="float: right">
                                    <input id="btn_AB_DW_print" type="image" src="~/Images/Print.png"  onclick="printDiv('div_AB_DW')" />
                                    <%--  <input ibtn_AB_DW_printd="" type="button" value="Print" class="DisplayFont button"
                                        onclick="printDiv('div_AB_DW')" />--%>
                                    &nbsp;&nbsp;
                                    <asp:ImageButton ID="Button2" runat="server" ImageUrl="~/Images/btnClose.png"
                                        Style="border: 0px" />
                                    <%-- <asp:Button runat="server" CssClass="DisplayFont button" ID="Button2" Text="Close"
                                        Width="60px" />--%>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
        <div>
            <asp:HiddenField ID="btnJVView" runat="server" />
            <cc2:ModalPopupExtender ID="mpeJEView" runat="server" TargetControlID="btnJVView"
                PopupControlID="pnlJVView" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlJVView" runat="server" Height="500px" BackColor="White" ScrollBars="None">
                <div class="" style="overflow: auto">
                    <table width="100%">
                        <tr class="divFormcontainer">
                            <td>
                                <asp:Button runat="server" CssClass="DisplayFont button" ID="btnJVviewBack" Text="Back"
                                    Width="60px" OnClick="btnJVviewBack_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div id="div_jvFrame">
                                    <iframe runat="server" id="frm_jv_View" name="jvview" class="frames" frameborder="0"
                                        height="480px" width="1000px" allowtransparency="true" style="border-style: solid;
                                        border-width: 0px; background-color: transparent;"></iframe>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table width="100%">
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/GL/GLChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
