﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.DAL.GL;
using FIN.DAL.AP;
using FIN.DAL.AR;
using FIN.BLL;
using FIN.BLL.AP;
using FIN.BLL.AR;
using FIN.BLL.HR;
using FIN.BLL.GL;
using FIN.BLL.SSM;
using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;

namespace FIN.Client.SSM_Reports
{
    public partial class AuditTrailParam : PageBase
    {
        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ExitInterviewReports", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                FillComboBox();
                FillStartDate();
                Startup();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ExitInterviewReports", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void FillStartDate()
        {

            string str_finyear = FINSP.GetSPFOR_FiscalYear(DateTime.Now.Date.ToString("dd/MM/yyyy"));
            DataTable dtDate = new DataTable();
            if (str_finyear != string.Empty)
            {
                dtDate = DBMethod.ExecuteQuery(FIN.DAL.GL.InventoryReports_DAL.getCalDate(str_finyear)).Tables[0];
                txtFromDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["cal_eff_start_dt"].ToString());
                txtToDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["call_eff_end_dt"].ToString());
            }


        }

        private void Startup()
        {

            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));

            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();

        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                ReportFile = Master.ReportName;
                if (ddlModuleName.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("MOD_CODE", ddlModuleName.SelectedItem.Value);
                    htFilterParameter.Add("MOD_NAME", ddlModuleName.SelectedItem.Text);
                }
                if (ddlFormName.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("FORM_CODE", ddlFormName.SelectedItem.Value);
                    htFilterParameter.Add("FORM_NAME", ddlFormName.SelectedItem.Text);
                }
                if (ddlUser.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("USER_CODE", ddlUser.SelectedItem.Value);
                    htFilterParameter.Add("USER_NAME", ddlUser.SelectedItem.Text);
                }
                if (txtFromDate.Text != string.Empty)
                {
                    htFilterParameter.Add("From_Date", txtFromDate.Text);
                }
                if (txtToDate.Text != string.Empty)
                {
                    htFilterParameter.Add("To_Date", txtToDate.Text);
                }
                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;
                //ReportData = FIN.DAL.HR.OverTimeSelection_DAL.GetSP_OverTime(ddlGroupcode.SelectedValue.ToString(), ddlEmployeeName.SelectedValue.ToString(), txtFromDate.Text.ToString(), FIN.BLL.HR.OverTimeSelection_BLL.GetReportData());
                ReportData = DBMethod.ExecuteQuery(FIN.DAL.SSM.User_DAL.getReportData());

                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());

                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerReportPath + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ExitInterviewReports", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }
        private void FillComboBox()
        {
            Lookup_BLL.GetModuleNam(ref ddlModuleName, Session[FINSessionConstants.Sel_Lng].ToString());
            BLL.SSM.User_BLL.GetUser(ref ddlUser);
            //FIN.BLL.HR.Employee_BLL.GetEmpName(ref ddlEmployeeName);
            //ddlEmployeeName.Items.RemoveAt(0);
            //ddlEmployeeName.Items.Insert(0, new ListItem("All", ""));
        }

        protected void ddlModuleName_SelectedIndexChanged(object sender, EventArgs e)
        {
            Lookup_BLL.GetMenuname(ref ddlFormName, ddlModuleName.SelectedValue, Session[FINSessionConstants.Sel_Lng].ToString());
        }

      
    }
}