﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.GL;
using FIN.DAL.AP;
using FIN.DAL.AR;
using FIN.BLL;
using FIN.BLL.AP;
using FIN.BLL.AR;
using FIN.BLL.HR;
using FIN.BLL.GL;
using FIN.BLL.SSM;
using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;
namespace FIN.Client.SSM_Reports
{
    public partial class RPTCurrenciesParam : PageBase
    {

        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("SupplierReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                FillComboBox();

                Startup();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("SupplierReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void Startup()
        {

            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));

            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();

        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;


                //if (ddlSupplierType.SelectedValue != string.Empty)
                //{

                //    htFilterParameter.Add(FINColumnConstants.VENDOR_TYPE, ddlSupplierType.SelectedItem.Value);
                //}
                //if (ddlCurrencyCode.SelectedValue != string.Empty)
                //{

                //    htFilterParameter.Add("CURRENCY_CODE", ddlCurrencyCode.SelectedItem.Text);
                //}
                //if (ddlCurrencyDesc.SelectedValue != string.Empty)
                //{

                //    htFilterParameter.Add("CURRENCY_ID", ddlCurrencyDesc.SelectedItem.Value);
                //}
                //if (ddlCurrencySyml.SelectedValue != string.Empty)
                //{

                //    htFilterParameter.Add("CURR_SYMBL", ddlCurrencySyml.SelectedItem.Value);
                //}
                //if (ddldecpre.SelectedValue != string.Empty)
                //{

                //    htFilterParameter.Add("DECIMAL_PRECICION", ddldecpre.SelectedItem.Value);
                //}
                //if (ddldecsep.SelectedValue != string.Empty)
                //{

                //    htFilterParameter.Add("DECIMAL_SEPERATOR", ddldecsep.SelectedItem.Value);
                //}
                //if (ddlInitialAmountSeparator.SelectedValue != string.Empty)
                //{

                //    htFilterParameter.Add("INIT_AMT_SEPERATOR", ddlInitialAmountSeparator.SelectedItem.Value);
                //}
                //if (ddlSubsequentAmountSeparator.SelectedValue != string.Empty)
                //{

                //    htFilterParameter.Add("SUB_AMT_SEPERATOR", ddlSubsequentAmountSeparator.SelectedItem.Value);
                //}



                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;

                ReportData = FIN.BLL.SSM.Country_BLL.GetCurrencyReportData();

                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());

                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerPath + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("SupplierReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }
        private void FillComboBox()
        {
            //FIN.BLL.Lookup_BLL.GetLookUpValues(ref ddlInitialAmountSeparator, "AMT_SEP");
            //FIN.BLL.Lookup_BLL.GetLookUpValues(ref ddlSubsequentAmountSeparator, "AMT_SEP");
            //FIN.BLL.Lookup_BLL.GetLookUpValues(ref ddldecpre, "IAS");
            //FIN.BLL.Lookup_BLL.GetLookUpValues(ref ddldecsep, "SAS");
            //FIN.BLL.SSM.Country_BLL.getCurrencyCode(ref ddlCurrencyCode);
            //FIN.BLL.SSM.Country_BLL.getCurrencyDescription(ref ddlCurrencyDesc);


        }

       


    }
}