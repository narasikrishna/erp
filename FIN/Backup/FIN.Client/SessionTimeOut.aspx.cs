﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using FIN.BLL;
namespace FIN.Client
{
    public partial class SessionTimeOut : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string str_Det = "";
            if (HttpContext.Current.Session["OrganizationID"] == null)
            {
                str_Det = "Organization Null ";
            }
            if (HttpContext.Current.Session["UserName"] == null)
            {
                str_Det += " User Name NULL";
            }
            if (HttpContext.Current.Session["Multilanguage"] == null)
            {
                str_Det += " MultiLanguage NULL";
            }
            if (Session[FINSessionConstants.UserName] == null)
            {
                str_Det += " Normal Session UserName NULL";
            }
            if (Request.Url.OriginalString.ToString().ToUpper().Contains("LOCALHOST"))
            {
                divSession.InnerHtml = str_Det;
            }
            //string path = @"D:\SessionDetails.txt";
            //using (StreamWriter sw = File.AppendText(path))
            //{
            //    sw.WriteLine(DateTime.Now.ToShortDateString());
            //    sw.WriteLine(str_Det);
            //}
            

        }
    }
}