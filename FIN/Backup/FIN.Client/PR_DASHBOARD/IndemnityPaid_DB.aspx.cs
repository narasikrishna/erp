﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using System.Data;
namespace FIN.Client.PR_DASHBOARD
{
    public partial class IndemnityPaid_DB : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session["EMP_INDEMNITY_LEAVE_PAID"] = null;
                FillComboBox();
                getIndemnityPaidLeave();
                LoadGraph();
            }
            //else
            //{
            //    LoadGraph();
            //}
        }

        private void FillComboBox()
        {
            string str_finyear = FINSP.GetSPFOR_FiscalYear(DateTime.Now.Date.ToString("dd/MM/yyyy"));
            FIN.BLL.GL.AccountingCalendar_BLL.GetCalAcctYear(ref ddl_IP_Year);
            if (ddl_IP_Year.Items.Count > 0)
            {
                ddl_IP_Year.SelectedValue = str_finyear;
            }
            DataTable dt_cur_period = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountingCalendar_DAL.GetPeriodId4CurrentDate()).Tables[0];
            FIN.BLL.GL.AccountingCalendar_BLL.GetFinancialPeriod4Year(ref ddl_IP_Period, ddl_IP_Year.SelectedValue);
            if (dt_cur_period.Rows.Count > 0)
            {
                ddl_IP_Period.SelectedValue = dt_cur_period.Rows[0]["PERIOD_ID"].ToString();
            }
        }

        private void LoadGraph()
        {
            if (Session["EMP_INDEMNITY_LEAVE_PAID"] != null)
            {
                chrtIndemnityLeave.DataSource = (DataTable)Session["EMP_INDEMNITY_LEAVE_PAID"];
                chrtIndemnityLeave.DataBind();
            }
        }

        private void getIndemnityPaidLeave()
        {
            DataTable dtEmpIndemnityLeavePaid = DBMethod.ExecuteQuery(FIN.DAL.PER.PayrollDashboard_DAL.getIndemLeavePaid()).Tables[0];
            Session["EMP_INDEMNITY_LEAVE_PAID"] = dtEmpIndemnityLeavePaid;
        }

        protected void ddl_ALP_Year_SelectedIndexChanged(object sender, EventArgs e)
        {
            FIN.BLL.GL.AccountingCalendar_BLL.GetFinancialPeriod4Year(ref ddl_IP_Period, ddl_IP_Year.SelectedValue);
            LoadGraph();
        }

        protected void ddl_ALP_Period_SelectedIndexChanged(object sender, EventArgs e)
        {
            getIndemnityPaidLeave();
            LoadGraph();
        }

       

    }
}