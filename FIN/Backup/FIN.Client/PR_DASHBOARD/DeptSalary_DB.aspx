﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="DeptSalary_DB.aspx.cs" Inherits="FIN.Client.PR_DASHBOARD.DeptSalary_DB" %>
    <%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div id="divEmpExpir" style="width: 100%">
        <div class="DBGridHeader" style="float: left; width: 100%; height: 25px" id="divHeader"
            runat="server">
            <div style="float: left;padding-left: 10px;padding-top: 2px">
                Salary
            </div>
            <div id="divPrintIcon" runat="server" style="float: right; padding-right: 10px; padding-top: 2px">
                <asp:ImageButton ID="btnGraphRep" runat="server" ImageUrl="../Images/show-report-icon.png"
                    Width="35px" Height="25px" OnClick="btnGraphRep_Click" />
            </div>
            <div style="width: 300px; float: right;  padding-right: 10px; padding-top: 2px">
                <asp:DropDownList ID="ddlDept" runat="server" CssClass="ddlStype" AutoPostBack="True"
                    OnSelectedIndexChanged="ddlDept_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div style="width: 100px; float: right; padding-right: 10px; padding-top: 2px">
                <asp:DropDownList ID="ddlFinYear" runat="server" CssClass="ddlStype" AutoPostBack="True"
                    OnSelectedIndexChanged="ddlFinYear_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            
        </div>
        <div class="divClear_10" style="width: 60%">
        </div>
        <div id="divSalGraph" style="width: 98%">
            <asp:Chart ID="chrt_SalCount" runat="server" Width="500px" Height="310px" EnableViewState="true">
                <Series>
                    <asp:Series ChartArea="ChartArea1" Name="s_Total_Net_Sal" IsValueShownAsLabel="True"
                        XValueMember="pay_period_desc" YValueMembers="NET_SAL" CustomProperties="PieLabelStyle=Outside">
                    </asp:Series>
                </Series>
                <ChartAreas>
                    <asp:ChartArea Name="ChartArea1">
                        <AxisX>
                            <MajorGrid Enabled="false" />
                        </AxisX>
                        <AxisY>
                            <MajorGrid Enabled="false" />
                        </AxisY>
                        <Area3DStyle Enable3D="True" Inclination="10" Rotation="20"></Area3DStyle>
                    </asp:ChartArea>
                </ChartAreas>
            </asp:Chart>
        </div>
         <div class="divClear_10" style="width: 70%">
    </div>
    <div id="divGraphData" runat="server">
        <asp:GridView ID="gvGraphdata" runat="server" CssClass="Grid" Width="100%" AutoGenerateColumns="False"
            ShowHeaderWhenEmpty="true">
            <Columns>
                <asp:BoundField DataField="pay_period_desc" HeaderText="Payroll Period"></asp:BoundField>
                <asp:BoundField DataField="NET_SAL" HeaderText="Salary">
                    <ItemStyle HorizontalAlign="Right" />
                </asp:BoundField>
            </Columns>
            <EmptyDataRowStyle CssClass="EmptyRowStyle" />
            <HeaderStyle CssClass="GridHeader" />
            <AlternatingRowStyle CssClass="GrdAltRow" />
        </asp:GridView>
    </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
</asp:Content>
