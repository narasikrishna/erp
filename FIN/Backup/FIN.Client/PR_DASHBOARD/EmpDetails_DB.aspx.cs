﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using VMVServices.Web;
using System.Collections;
namespace FIN.Client.PR_DASHBOARD
{
    public partial class EmpDetails_DB : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session["NET_SAL_EMP"] = null;
                FillComboBox();
                AssignToControl();
                DisplayEmployeeDetails();
                LoadGraph();
            }
            //else
            //{
            //    LoadGraph();
            //}
        }
        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                divGraphData.Visible = false;
                divPrintIcon.Visible = false;
                if (Request.QueryString.ToString().ToUpper().Contains("TVYN"))
                {
                    Startup();
                    divGraphData.Visible = true;
                    divPrintIcon.Visible = true;
                    divHeader.Style.Add(" background-color", "white");
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DetailedRevenueATC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }


        private void Startup()
        {
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[FIN.BLL.QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[FIN.BLL.QueryStringTags.ReportName.ToString()]));
        }
        private void FillComboBox()
        {
            string str_finyear = FINSP.GetSPFOR_FiscalYear(DateTime.Now.Date.ToString("dd/MM/yyyy"));   
            FIN.BLL.GL.AccountingCalendar_BLL.GetCalAcctYear(ref ddlFinYr);
            if (ddlFinYr.Items.Count > 0)
            {
                ddlFinYr.SelectedValue = str_finyear;
            }
            FIN.BLL.HR.Employee_BLL.GetEmployeeNam(ref ddlEmplName);
            if (ddlEmplName.Items.Count > 0)
            {
                ddlEmplName.SelectedIndex = 1;
            }
        }

        private void LoadGraph()
        {
            if (Session["NET_SAL_EMP"] != null)
            {
                chrtEmpDetails.DataSource = (DataTable)Session["NET_SAL_EMP"];
                chrtEmpDetails.DataBind();

                gvGraphdata.DataSource = (DataTable)Session["NET_SAL_EMP"];
                gvGraphdata.DataBind();
            }

        }

        private void DisplayEmployeeDetails()
        {
            DataTable dt_EmpDet = DBMethod.ExecuteQuery(FIN.DAL.PER.PayrollDashboard_DAL.GetEmployeeDetails4Dashbord(ddlEmplName.SelectedValue)).Tables[0];
            Session["EMPDET"] = dt_EmpDet;
            if (dt_EmpDet.Rows.Count > 0)
            {
                lblEmpNo.Text = dt_EmpDet.Rows[0]["EMP_NO"].ToString();
                lblNmae.Text = dt_EmpDet.Rows[0]["EMP_NAME"].ToString();
                lblDepartmet.Text = dt_EmpDet.Rows[0]["DEPT_NAME"].ToString();
                lblDesignation.Text = dt_EmpDet.Rows[0]["DESIG_NAME"].ToString();
                lblDOJ.Text = DBMethod.ConvertDateToString(dt_EmpDet.Rows[0]["DOJ"].ToString());
                hf_deptid.Value = dt_EmpDet.Rows[0]["DEPT_ID"].ToString();
                if (dt_EmpDet.Rows[0]["DOC"] != null)
                {
                    lblDOC.Text = DBMethod.ConvertDateToString(dt_EmpDet.Rows[0]["DOC"].ToString());
                }
                else
                {
                    lblDOC.Text = "";
                }

                DataTable dtNetSal = DBMethod.ExecuteQuery(FIN.DAL.PER.PayrollDashboard_DAL.getSal4FinYear(dt_EmpDet.Rows[0]["DEPT_ID"].ToString(), ddlFinYr.SelectedValue, ddlEmplName.SelectedValue)).Tables[0];
                Session["NET_SAL_EMP"] = dtNetSal;
            }
            LoadGraph();
        }

        protected void ddlEmplName_SelectedIndexChanged(object sender, EventArgs e)
        {
            DisplayEmployeeDetails();
        }
        protected void ddlFinYr_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtNetSal = DBMethod.ExecuteQuery(FIN.DAL.PER.PayrollDashboard_DAL.getSal4FinYear(hf_deptid.Value.ToString(), ddlFinYr.SelectedValue, ddlEmplName.SelectedValue)).Tables[0];
            Session["NET_SAL_EMP"] = dtNetSal;
            LoadGraph();
        }

        protected void btnGraphRep_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;

                Hashtable htParameters = new Hashtable();
                Hashtable htHeadingParameters = new Hashtable();
                Hashtable htFilterParameter = new Hashtable();

                chrtEmpDetails.SaveImage(System.Web.Configuration.WebConfigurationManager.AppSettings["ChartFileFolder"].ToString() + Master.ProgramID + ".png");
                htFilterParameter.Add("ChartPath", System.Web.Configuration.WebConfigurationManager.AppSettings["ChartFileFolder"].ToString() + Master.ProgramID + ".png");



                htFilterParameter.Add("FF_YEAR", ddlFinYr.SelectedItem.Text);


                htFilterParameter.Add("Employeeid", ddlEmplName.SelectedValue.ToString());
                htFilterParameter.Add("Employee_Name", ddlEmplName.SelectedItem.ToString());

                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;

                DataTable dt_data = (DataTable)Session["NET_SAL_EMP"];
                ReportData = new DataSet();
                DataTable dt = dt_data.Copy();
                dt.TableName = "vw_bi_employee_details";
                dt.Columns[0].ColumnName = "pay_period_desc";
                ReportData.Tables.Add(dt.Copy());


                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FIN.BLL.FINMessageConstatns.CrystalReportViewerReportPath + "?Id=" + Master.ProgramID + "','','" + VMVServices.Services.Data.DbConsts.ReportProperties + "');", true);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ddd", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
    }
}