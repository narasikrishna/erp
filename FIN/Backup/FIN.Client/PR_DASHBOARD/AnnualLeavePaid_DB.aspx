﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="AnnualLeavePaid_DB.aspx.cs" Inherits="FIN.Client.PR_DASHBOARD.AnnualLeavePaid_DB" %>
    <%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
        <div class="DBGridHeader" style="float: left; width: 100%; height: 25px" id="divHeader"
            runat="server">
        <div style="float: left;padding-left: 10px;padding-top: 2px">
            Annual Leave Paid
        </div>
        <div id="divPrintIcon" runat="server" style="float: right; padding-right: 10px; padding-top: 2px">
                <asp:ImageButton ID="btnGraphRep" runat="server" ImageUrl="../Images/show-report-icon.png"
                    Width="35px" Height="25px" OnClick="btnGraphRep_Click" />
            </div>
        <div style="width: 120px; float: right; padding-right: 10px;padding-top: 2px">
            <asp:DropDownList ID="ddl_ALP_Period" runat="server" CssClass="ddlStype" AutoPostBack="True"
                OnSelectedIndexChanged="ddl_ALP_Period_SelectedIndexChanged">
            </asp:DropDownList>
        </div>
        <div style="width: 120px; float: right; padding-right: 10px;padding-top: 2px">
            <asp:DropDownList ID="ddl_ALP_Year" runat="server" CssClass="ddlStype" AutoPostBack="True"
                OnSelectedIndexChanged="ddl_ALP_Year_SelectedIndexChanged">
            </asp:DropDownList>
        </div> 
        
    </div>
    <div class="divClear_10" style="width: 70%">
    </div>
    <div id="divAnnLeaveGraph" style="width: 100%;">
        <asp:Chart ID="chrtAnnualLeave" runat="server" Width="500px" Height="310px" EnableViewState="true">
            <Series>
                <asp:Series Name="sAnnualLeave" ChartType="Doughnut" XValueMember="DEPT_NAME" YValueMembers="tot_sal"
                    IsValueShownAsLabel="True" Legend="Legend1" LegendText="#VALX Years  #VAL" CustomProperties="PieLabelStyle=Outside">
                </asp:Series>
            </Series>
            <ChartAreas>
                <asp:ChartArea Name="ChartArea1" Area3DStyle-Enable3D="true" >
                    <AxisX>
                        <MajorGrid Enabled="false" />
                    </AxisX>
                    <AxisY>
                        <MajorGrid Enabled="false" />
                    </AxisY>
                    <Area3DStyle Enable3D="True"></Area3DStyle>
                </asp:ChartArea>
            </ChartAreas>
            <Legends>
                <asp:Legend Docking="Bottom" Name="Legend1">
                </asp:Legend>
            </Legends>
        </asp:Chart>
    </div>
    <div id="divGraphData" runat="server">
        <asp:GridView ID="gvGraphdata" runat="server" CssClass="Grid" Width="100%" AutoGenerateColumns="False"
            ShowHeaderWhenEmpty="true">
            <Columns>
                <asp:BoundField DataField="DEPT_NAME" HeaderText="Department Name"></asp:BoundField>
                <asp:BoundField DataField="tot_sal" HeaderText="Total Salary">
                    <ItemStyle HorizontalAlign="Right" />
                </asp:BoundField>
            </Columns>
            <EmptyDataRowStyle CssClass="EmptyRowStyle" />
            <HeaderStyle CssClass="GridHeader" />
            <AlternatingRowStyle CssClass="GrdAltRow" />
        </asp:GridView>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
</asp:Content>
