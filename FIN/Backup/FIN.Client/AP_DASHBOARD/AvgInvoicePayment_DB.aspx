﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="AvgInvoicePayment_DB.aspx.cs" Inherits="FIN.Client.AP_DASHBOARD.AvgInvoicePayment_DB" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function fn_ShowGRN() {
            $("#divGRN").fadeToggle(2000);
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div id="divEmpExpir" style="width: 100%">
        <div class="DBGridHeader" style="float: left; width: 100%; height: 25px" id="divHeader"
            runat="server">
            <div style="float: left; padding-left: 10px; padding-top: 2px">
                Average Invoice Payment days by number of Invoice
            </div>
            <div style="float: right; padding-right: 10px; padding-top: 2px">
                <img src="../Images/DashBoardImage/ViewList.png" width="20px" height="20px" onclick="fn_ShowGRN()" />
            </div>
            <div id="divPrintIcon" runat="server" style="float: right; padding-right: 10px; padding-top: 2px">
                <asp:ImageButton ID="btnGraphRep" runat="server" ImageUrl="../Images/show-report-icon.png"
                    Width="35px" Height="25px" OnClick="btnGraphRep_Click" />
            </div>
        </div>
        <div class="divClear_10" style="width: 60%">
        </div>
        <div id="divChrtGRN" style="width: 98%">
            <asp:Chart ID="chartGRN"  runat="server" Width="500px" Height="310px" EnableViewState="true">
               <%-- <Series>
                    <asp:Series ChartArea="ChartArea1" ChartType="Bar" Name="Days by Number of Invoice" IsValueShownAsLabel="True"
                        XValueMember="day_month" YValueMembers="num" Legend="Legend1">
                    </asp:Series>
                    
                </Series>--%>
                 <Series>
                    <asp:Series Name="ChartArea1" XValueMember="Year_Month" YValueMembers="Tot_Inv"
                        IsValueShownAsLabel="true" LabelAngle="60" Legend="Legend1">
                    </asp:Series>
                </Series>
                <ChartAreas>
                 <asp:ChartArea Name="ChartArea1" Area3DStyle-Enable3D="true" Area3DStyle-Inclination="20">
                        <Area3DStyle Enable3D="True" Inclination="20"></Area3DStyle>
                        <AxisX>
                            <MajorGrid Enabled="false" />
                        </AxisX>
                        <AxisY>
                            <MajorGrid Enabled="false" />
                        </AxisY>
                    </asp:ChartArea>
                   <%-- <asp:ChartArea Name="ChartArea1">
                    </asp:ChartArea>--%>
                </ChartAreas>
               <%-- <Legends>
                    <asp:Legend Alignment="Center" Name="Legend1" Docking="Bottom">
                    </asp:Legend>
                </Legends>--%>
            </asp:Chart>
        </div>
        <div class="divClear_10" style="width: 70%">
        </div>
        <div id="divGraphData" runat="server">
            <asp:GridView ID="gvGraphdata" runat="server" CssClass="Grid" Width="100%" AutoGenerateColumns="False"
                ShowHeaderWhenEmpty="true">
                <Columns>
                    <asp:BoundField DataField="Year_Month" HeaderText="Month"></asp:BoundField>
                    <asp:BoundField DataField="Tot_Inv" HeaderText="Days">
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:BoundField>
                   
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div id="divGRN" style="width: 100%; background-color: ThreeDFace; position: absolute;
            z-index: 5000; top: 30px; left: 5px; display: none">
            <div class="divRowContainer">
                <div class="lblBox" style="float: left; width: 80px" id="Div1">
                    Financial Year
                </div>
                <div style="width: 400px; float: right; padding-right: 10px">
                    <asp:DropDownList ID="ddlFinancialYr" runat="server" CssClass="ddlStype" 
                        AutoPostBack="True" 
                        onselectedindexchanged="ddlFinancialYr_SelectedIndexChanged">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox" style="float: left; width: 80px" id="lblInvoiceCurrency">
                    Supplier
                </div>
                <div style="width: 400px; float: right; padding-right: 10px">
                    <asp:DropDownList ID="ddlSupplier" runat="server" CssClass="ddlStype" 
                        AutoPostBack="True" onselectedindexchanged="ddlSupplier_SelectedIndexChanged">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
</asp:Content>
