﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.BLL;
using VMVServices.Web;
using System.Collections;

namespace FIN.Client.AP_DASHBOARD
{
    public partial class APTopSuppliers_DB : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session["FIVE_SUPPLIER_GRAPH"] = null;
                AssignToControl();
                DisplayFiveSupplyDetails();
                LoadGraph();
            }
            //else
            //{
            //    LoadGraph();
            //}
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                divGraphData.Visible = false;
                divPrintIcon.Visible = false;
                if (Request.QueryString.ToString().ToUpper().Contains("TVYN"))
                {
                    Startup();
                    divGraphData.Visible = true;
                    divPrintIcon.Visible = true;
                    divHeader.Style.Add(" background-color", "white");
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DetailedRevenueATC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }


        private void Startup()
        {
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[FIN.BLL.QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[FIN.BLL.QueryStringTags.ReportName.ToString()]));
        }

        private void LoadGraph()
        {
            try
            {
                ErrorCollection.Clear();
                if (Session["FIVE_SUPPLIER_GRAPH"] != null)
                {
                    chrtTop5Supply.DataSource = (DataTable)Session["FIVE_SUPPLIER_GRAPH"];
                    chrtTop5Supply.Series["S_supply_Count"].XValueMember = "vendor_name";
                    chrtTop5Supply.Series["S_supply_Count"].YValueMembers = "payment_amt";
                    chrtTop5Supply.DataBind();


                    gvGraphdata.DataSource = (DataTable)Session["FIVE_SUPPLIER_GRAPH"];
                    gvGraphdata.DataBind();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("APTopSuppliers", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        protected void btnGraphRep_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;

                Hashtable htParameters = new Hashtable();
                Hashtable htHeadingParameters = new Hashtable();
                Hashtable htFilterParameter = new Hashtable();
                chrtTop5Supply.SaveImage(System.Web.Configuration.WebConfigurationManager.AppSettings["ChartFileFolder"].ToString() + Master.ProgramID + ".png");
                htFilterParameter.Add("ChartPath", System.Web.Configuration.WebConfigurationManager.AppSettings["ChartFileFolder"].ToString() + Master.ProgramID + ".png");



                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;


                ReportData = DBMethod.ExecuteQuery(FIN.DAL.AP.Payment_DAL.GetTop5SupplierAmt());


                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FIN.BLL.FINMessageConstatns.CrystalReportViewerReportPath + "?Id=" + Master.ProgramID + "','','" + VMVServices.Services.Data.DbConsts.ReportProperties + "');", true);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ddd", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void DisplayFiveSupplyDetails()
        {
            DataTable dt_EmpDet = DBMethod.ExecuteQuery(FIN.DAL.AP.Payment_DAL.GetTop5SupplierAmt()).Tables[0];
            Session["FIVE_SUPPLIER_GRAPH"] = dt_EmpDet;
            if (dt_EmpDet.Rows.Count > 0)
            {
                DataTable dt_Dist_DEPT = dt_EmpDet.DefaultView.ToTable(true, "vendor_id", "vendor_name", "payment_amt");

                DataTable dt_Dept_List = new DataTable();
                dt_Dept_List.Columns.Add("vendor_id");
                dt_Dept_List.Columns.Add("vendor_name");
                dt_Dept_List.Columns.Add("payment_amt");

                for (int iLoop = 0; iLoop < dt_Dist_DEPT.Rows.Count; iLoop++)
                {
                    DataRow dr = dt_Dept_List.NewRow();
                    dr["vendor_id"] = dt_Dist_DEPT.Rows[iLoop]["vendor_id"].ToString();
                    dr["vendor_name"] = dt_Dist_DEPT.Rows[iLoop]["vendor_name"].ToString();
                    DataRow[] dr_count = dt_EmpDet.Select("vendor_id='" + dt_Dist_DEPT.Rows[iLoop]["vendor_id"].ToString() + "'");
                    dr["payment_amt"] = FIN.BLL.CommonUtils.ConvertStringToDecimal(dt_Dist_DEPT.Rows[iLoop]["payment_amt"].ToString());
                    dt_Dept_List.Rows.Add(dr);
                }

                Session["FIVE_SUPPLIER_GRAPH"] = dt_Dept_List;

                LoadGraph();
            }
        }

    }
}