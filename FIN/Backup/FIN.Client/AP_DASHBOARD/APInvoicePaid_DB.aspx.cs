﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using VMVServices.Web;
using System.Collections;

namespace FIN.Client.AP_DASHBOARD
{
    public partial class APInvoicePaid_DB : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session["INVOICED_PAID_GRAPH"] = null;
                AssignToControl();
                DisplayInvoicePaidDetails();
                LoadGraph();
            }
            //else
            //{
            //    LoadGraph();
            //}
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                divGraphData.Visible = false;
                divPrintIcon.Visible = false;
                if (Request.QueryString.ToString().ToUpper().Contains("TVYN"))
                {
                    Startup();
                    divGraphData.Visible = true;
                    divPrintIcon.Visible = true;
                    divHeader.Style.Add(" background-color", "white");
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DetailedRevenueATC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }


        private void Startup()
        {
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[FIN.BLL.QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[FIN.BLL.QueryStringTags.ReportName.ToString()]));
        }
        private void LoadGraph()
        {
            if (Session["INVOICED_PAID_GRAPH"] != null)
            {
                chrtInvoicedPaid.DataSource = (DataTable)Session["INVOICED_PAID_GRAPH"];
                chrtInvoicedPaid.DataBind();
                gvGraphdata.DataSource = (DataTable)Session["INVOICED_PAID_GRAPH"];
                gvGraphdata.DataBind();
            }
        }
        protected void btnGraphRep_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;

                Hashtable htParameters = new Hashtable();
                Hashtable htHeadingParameters = new Hashtable();
                Hashtable htFilterParameter = new Hashtable();


                chrtInvoicedPaid.SaveImage(System.Web.Configuration.WebConfigurationManager.AppSettings["ChartFileFolder"].ToString() + Master.ProgramID + ".png");
                htFilterParameter.Add("ChartPath", System.Web.Configuration.WebConfigurationManager.AppSettings["ChartFileFolder"].ToString() + Master.ProgramID + ".png");

                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;


                ReportData = DBMethod.ExecuteQuery(FIN.DAL.AP.Payment_DAL.GetInvoicePaidAmt());


                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FIN.BLL.FINMessageConstatns.CrystalReportViewerReportPath + "?Id=" + Master.ProgramID + "','','" + VMVServices.Services.Data.DbConsts.ReportProperties + "');", true);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ddd", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void DisplayInvoicePaidDetails()
        {
            DataTable dt_EmpDet = DBMethod.ExecuteQuery(FIN.DAL.AP.Payment_DAL.GetInvoicePaidAmt()).Tables[0];
            Session["INVOICED_PAID_GRAP_LOAD"] = dt_EmpDet;
            if (dt_EmpDet.Rows.Count > 0)
            {
                DataTable dt_Dist_DEPT = dt_EmpDet.DefaultView.ToTable(true, "vendor_id", "vendor_name", "invoice_amt", "payment_amt");

                DataTable dt_Dept_List = new DataTable();
                dt_Dept_List.Columns.Add("vendor_id");
                dt_Dept_List.Columns.Add("vendor_name");
                dt_Dept_List.Columns.Add("invoice_amt");
                dt_Dept_List.Columns.Add("payment_amt");

                for (int iLoop = 0; iLoop < dt_Dist_DEPT.Rows.Count; iLoop++)
                {
                    DataRow dr = dt_Dept_List.NewRow();
                    dr["vendor_id"] = dt_Dist_DEPT.Rows[iLoop]["vendor_id"].ToString();
                    dr["vendor_name"] = dt_Dist_DEPT.Rows[iLoop]["vendor_name"].ToString();
                    dr["invoice_amt"] = DBMethod.GetAmtDecimalCommaSeparationValue(FIN.BLL.CommonUtils.ConvertStringToDecimal(dt_Dist_DEPT.Rows[iLoop]["invoice_amt"].ToString()).ToString());
                    dr["payment_amt"] = DBMethod.GetAmtDecimalCommaSeparationValue(FIN.BLL.CommonUtils.ConvertStringToDecimal(dt_Dist_DEPT.Rows[iLoop]["payment_amt"].ToString()).ToString());
                    dt_Dept_List.Rows.Add(dr);
                }
                gvInvoicePaid.DataSource = dt_Dept_List;
                gvInvoicePaid.DataBind();
                Session["INVOICED_PAID_GRAPH"] = dt_Dept_List;

                LoadGraph();
            }
        }

        protected void lnk_InvoicePaid_Click(object sender, EventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            LoadInvoicePaidDetails(gvr);
        }

        private void LoadInvoicePaidDetails(GridViewRow gvr)
        {
            DataTable dt_EmpDet = (DataTable)Session["INVOICED_PAID_GRAP_LOAD"];
            List<DataTable> tables = dt_EmpDet.AsEnumerable()
                .Where(r => r["vendor_id"].ToString().Contains(gvInvoicePaid.DataKeys[gvr.RowIndex].Values["vendor_id"].ToString()))
                       .GroupBy(row => new
                       {
                           email = row.Field<string>("vendor_id"),
                           Name = row.Field<string>("vendor_name")
                       }).Select(g => System.Data.DataTableExtensions.CopyToDataTable(g)).ToList();

            DataTable dt_Dist_DEPT = tables[0].DefaultView.ToTable(true, "vendor_id", "vendor_name", "invoice_amt", "payment_amt");


            DataTable dt_Dept_List = new DataTable();
            dt_Dept_List.Columns.Add("vendor_id");
            dt_Dept_List.Columns.Add("vendor_name");
            dt_Dept_List.Columns.Add("invoice_amt");
            dt_Dept_List.Columns.Add("payment_amt");

            for (int iLoop = 0; iLoop < dt_Dist_DEPT.Rows.Count; iLoop++)
            {
                DataRow dr = dt_Dept_List.NewRow();
                dr["vendor_id"] = dt_Dist_DEPT.Rows[iLoop]["vendor_id"].ToString();
                dr["vendor_name"] = dt_Dist_DEPT.Rows[iLoop]["vendor_name"].ToString();
                dr["invoice_amt"] = DBMethod.GetAmtDecimalCommaSeparationValue(FIN.BLL.CommonUtils.ConvertStringToDecimal(dt_Dist_DEPT.Rows[iLoop]["invoice_amt"].ToString()).ToString());
                dr["payment_amt"] = DBMethod.GetAmtDecimalCommaSeparationValue(FIN.BLL.CommonUtils.ConvertStringToDecimal(dt_Dist_DEPT.Rows[iLoop]["payment_amt"].ToString()).ToString());
                dt_Dept_List.Rows.Add(dr);
            }
            gvInvoicePaid.DataSource = dt_Dept_List;
            gvInvoicePaid.DataBind();

            Session["INVOICED_PAID_GRAPH"] = dt_Dept_List;

            LoadGraph();

        }

    }
}