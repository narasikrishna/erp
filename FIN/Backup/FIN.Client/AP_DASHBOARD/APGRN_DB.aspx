﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="APGRN_DB.aspx.cs" Inherits="FIN.Client.AP_DASHBOARD.APGRN_DB" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function fn_ShowGRN() {
            $("#divGRN").fadeToggle(2000);
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div id="divEmpExpir" style="width: 100%">
        <div class="DBGridHeader" style="float: left; width: 100%; height: 25px" id="divHeader"
            runat="server">
            <div style="float: left; padding-left: 10px; padding-top: 2px">
                Goods Receipt Note
            </div>
            <div style="float: right; padding-right: 10px; padding-top: 2px">
                <img src="../Images/DashBoardImage/ViewList.png" width="20px" height="20px" onclick="fn_ShowGRN()" />
            </div>
            <div id="divPrintIcon" runat="server" style="float: right; padding-right: 10px; padding-top: 2px">
                <asp:ImageButton ID="btnGraphRep" runat="server" ImageUrl="../Images/show-report-icon.png"
                    Width="35px" Height="25px" OnClick="btnGraphRep_Click" />
            </div>
        </div>
        <div class="divClear_10" style="width: 60%">
        </div>
        <div id="divChrtGRN" style="width: 98%">
            <asp:Chart ID="chartGRN" runat="server" Width="500px" Height="310px" EnableViewState="true">
                <Series>
                    <asp:Series ChartArea="ChartArea1" Name="qty_received" IsValueShownAsLabel="True"
                        XValueMember="item_name" YValueMembers="qty_received" Legend="Legend1" LegendText="Received Quantity ( #TOTAL{N0} )">
                    </asp:Series>
                    <asp:Series ChartArea="ChartArea1" IsValueShownAsLabel="True" Name="qty_approved"
                        XValueMember="item_name" YValueMembers="qty_approved" Legend="Legend1" LegendText="Approved Quantity ( #TOTAL{N0} )\n">
                    </asp:Series>
                    <asp:Series ChartArea="ChartArea1" IsValueShownAsLabel="True" Name="qty_rejected"
                        XValueMember="item_name" YValueMembers="qty_rejected" Legend="Legend1" LegendText="Rejected Quantity ( #TOTAL{N0} )\n">
                    </asp:Series>
                </Series>
                <ChartAreas>
                    <asp:ChartArea Name="ChartArea1">
                    </asp:ChartArea>
                </ChartAreas>
                <Legends>
                    <asp:Legend Alignment="Center" Name="Legend1" Docking="Bottom">
                    </asp:Legend>
                </Legends>
            </asp:Chart>
        </div>
        <div class="divClear_10" style="width: 70%">
        </div>
        <div id="divGraphData" runat="server">
            <asp:GridView ID="gvGraphdata" runat="server" CssClass="Grid" Width="100%" AutoGenerateColumns="False"
                ShowHeaderWhenEmpty="true">
                <Columns>
                    <asp:BoundField DataField="item_name" HeaderText="Item"></asp:BoundField>
                    <asp:BoundField DataField="qty_received" HeaderText="Received Quantity">
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:BoundField>
                    <asp:BoundField DataField="qty_approved" HeaderText="Approved Quantity">
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:BoundField>
                    <asp:BoundField DataField="qty_rejected" HeaderText="Rejected Quantity">
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:BoundField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div id="divGRN" style="width: 100%; background-color: ThreeDFace; position: absolute;
            z-index: 5000; top: 30px; left: 5px; display: none">
            <div class="divRowContainer">
                <div class="lblBox" style="float: left; width: 80px" id="lblInvoiceCurrency">
                    Supplier
                </div>
                <div style="width: 400px; float: right; padding-right: 10px">
                    <asp:DropDownList ID="ddlSupplier" runat="server" CssClass="ddlStype" AutoPostBack="True"
                        OnSelectedIndexChanged="ddlSupplier_SelectedIndexChanged">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox" style="float: left; width: 80px" id="Div2">
                    Item
                </div>
                <div style="width: 400px; float: right; padding-right: 10px">
                    <asp:DropDownList ID="ddlItem" runat="server" CssClass="ddlStype" AutoPostBack="True"
                        OnSelectedIndexChanged="ddlItem_SelectedIndexChanged">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
</asp:Content>
