﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="APAgingAnalysis_DB.aspx.cs" Inherits="FIN.Client.AP_DASHBOARD.APAgingAnalysis_DB" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
 <script type="text/javascript">
     function fn_ShowGRN() {
         $("#divGRN").fadeToggle(2000);
     }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div id="divempGraph" style="float: left; width: 100%;">
        <div class="DBGridHeader" style="float: left; width: 100%; height: 25px;" id="divHeader"
            runat="server">
            <div style="float: left; padding-left: 10px">
                Aging Analysis
            </div>
            <div style="float: right; padding-right: 10px; padding-top: 2px">
                <img src="../Images/DashBoardImage/ViewList.png" width="20px" height="20px" onclick="fn_ShowGRN()" />
            </div>
            <div id="divPrintIcon" runat="server" style="float: right; padding-right: 10px; padding-top: 2px">
                <asp:ImageButton ID="btnGraphRep" runat="server" ImageUrl="../Images/show-report-icon.png"
                    Width="35px" Height="25px" OnClick="btnGraphRep_Click" />
            </div>
        </div>
        <div class="divClear_10" style="width: 70%">
        </div>
        <div id="divempAgeGraph">
            <asp:Chart ID="chrtEmpAge" runat="server" Height="310px" Width="500px" EnableViewState="true">
                <Series>
                    <asp:Series Name="s_EmpAge" ChartType="Bar" XValueMember="AgeDiff" YValueMembers="AMOUNT_PAID"
                        IsValueShownAsLabel="True" YValuesPerPoint="1" Legend="Legend1">
                    </asp:Series>
                </Series>
                <ChartAreas>
                    <asp:ChartArea Name="ChartArea1" Area3DStyle-Enable3D="true">
                        <AxisX>
                            <MajorGrid Enabled="false" />
                        </AxisX>
                        <AxisY>
                            <MajorGrid Enabled="false" />
                        </AxisY>
                        <Area3DStyle Enable3D="True"></Area3DStyle>
                    </asp:ChartArea>
                </ChartAreas>
                <Legends>
                    <asp:Legend Name="Legend1">
                    </asp:Legend>
                </Legends>
            </asp:Chart>
        </div>
        <div class="divClear_10" style="width: 70%">
        </div>
        <div id="divGraphData" runat="server">
            <asp:GridView ID="gvGraphdata" runat="server" CssClass="Grid" Width="100%" AutoGenerateColumns="False"
                ShowHeaderWhenEmpty="true">
                <Columns>
                    <asp:BoundField DataField="AgeDiff" HeaderText="Difference"></asp:BoundField>
                    <asp:BoundField DataField="AMOUNT_PAID" HeaderText="Amount">
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:BoundField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div id="divGRN" style="width: 100%; background-color: ThreeDFace; position: absolute;
            z-index: 5000; top: 30px; left: 5px; display: none">
             <div class="divRowContainer">
                <div class="lblBox" style="float: left; width: 80px" id="Div4">
                    Supplier
                </div>
                <div style="width: 400px; float: right; padding-right: 10px">
                    <asp:DropDownList ID="ddlSupplier" runat="server" CssClass="ddlStype" AutoPostBack="True"
                       >
                    </asp:DropDownList>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox" style="float: left; width: 80px" id="lblInvoiceCurrency">
                    Aging Bucket1
                </div>
                <div style="width: 400px; float: right; padding-right: 10px">
                    <asp:TextBox ID="txt30" runat="server" TabIndex="16" MaxLength="15" CssClass="validate[required] RequiredField txtBox_N"
                        Width="150px"></asp:TextBox>
                    <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Numbers,Custom"
                        ValidChars=".," TargetControlID="txt30" />
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox" style="float: left; width: 80px" id="Div1">
                    Aging Bucket2
                </div>
                <div style="width: 400px; float: right; padding-right: 10px">
                    <asp:TextBox ID="txt60" runat="server" TabIndex="16" MaxLength="15" CssClass="validate[required] RequiredField txtBox_N"
                        Width="150px" ></asp:TextBox>
                    <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers,Custom"
                        ValidChars=".," TargetControlID="txt60" />
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox" style="float: left; width: 80px" id="Div2">
                    Aging Bucket3
                </div>
                <div style="width: 400px; float: right; padding-right: 10px">
                    <asp:TextBox ID="txt90" runat="server" TabIndex="16" MaxLength="15" CssClass="validate[required] RequiredField txtBox_N"
                        Width="150px"></asp:TextBox>
                    <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers,Custom"
                        ValidChars=".," TargetControlID="txt90" />
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox" style="float: left; width: 80px" id="Div3">
                    Date
                </div>
                <div style="width: 400px; float: right; padding-right: 10px">
                    <asp:TextBox ID="txtFromDate" runat="server" TabIndex="2" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]]  RequiredField txtBox" Width="150px"
                        AutoPostBack="True"></asp:TextBox>
                    <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calExtender1" TargetControlID="txtFromDate"
                        OnClientDateSelectionChanged="checkDate" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
</asp:Content>
