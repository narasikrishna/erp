﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using FIN.BLL.AP;
using FIN.BLL.HR;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.BLL;
using VMVServices.Web;

namespace FIN.Client.UploadFile
{
    public partial class BankStatementUpload : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnFileUpload_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (fuAttachments.HasFile)
                {
                    string fileExt = System.IO.Path.GetExtension(fuAttachments.FileName);

                    if (fileExt.ToUpper() != "XLS" || fileExt.ToUpper() != "XLSX")
                    {                        
                        System.Collections.SortedList slfiledet = new System.Collections.SortedList();
                        TMP_FILE_DET tMP_FILE_DET = new TMP_FILE_DET();
                        tMP_FILE_DET.FILE_NAME = System.IO.Path.GetFileNameWithoutExtension(fuAttachments.FileName);
                        tMP_FILE_DET.FILE_TYPE = fuAttachments.PostedFile.ContentType;
                        tMP_FILE_DET.CONTENT_LENGTH = fuAttachments.PostedFile.ContentLength;
                        tMP_FILE_DET.FILE_EXTENSTION = System.IO.Path.GetExtension(fuAttachments.FileName).Replace(".", "");
                        tMP_FILE_DET.CREATED_BY = this.LoggedUserName;
                        tMP_FILE_DET.CREATED_DATE = DateTime.Now;
                        tMP_FILE_DET.TMP_FILE_ID = DBMethod.GetPrimaryKeyValue(FINSequenceConstant.TMP_FILE_DET_SEQ);
                        DBMethod.SaveEntity<TMP_FILE_DET>(tMP_FILE_DET);
                        fuAttachments.SaveAs(Server.MapPath("~/UploadFile/DocFile/" + tMP_FILE_DET.TMP_FILE_ID.ToString() + "." + tMP_FILE_DET.FILE_EXTENSTION.ToString().Replace(".", "")));
                        slfiledet.Add(slfiledet.Count + 1.ToString(), tMP_FILE_DET.TMP_FILE_ID.ToString());
                        slfiledet.Add(slfiledet.Count + 1.ToString(), tMP_FILE_DET.FILE_EXTENSTION.ToString().Replace(".", ""));
                        slfiledet.Add(slfiledet.Count + 1.ToString(), tMP_FILE_DET.FILE_NAME.ToString());
                        lblFileDet.Visible = true;
                        lblFileDet.Text = fuAttachments.FileName + " Upload Successfully ";
                        Session[FINSessionConstants.Attachments] = slfiledet;
                    }
                    else
                    {
                        ErrorCollection.Add("InvalidFileFormat", ".XLS,.XLSX File Format only Accept");
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Error on upload", ex.ToString());
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
    }
}