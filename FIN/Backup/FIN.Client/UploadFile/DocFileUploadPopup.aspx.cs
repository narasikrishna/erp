﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.IO;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.BLL;
using FIN.BLL.HR;
using VMVServices.Web;

namespace FIN.Client.MainPage
{
    public partial class DocFileUploadPopup : PageBase
    {
        DataTable dt_DocDet = new DataTable();
        string websiteURL = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["FormCode"] != null && Session["Mode"] != null && Session["StrRecordId"] != null)
                {
                    hf_ScreenId.Value = Session["StrRecordId"].ToString();
                    hf_ProgId.Value = Session["ProgramID"].ToString();
                    BindGrid();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(btnFileUpload, typeof(Button), "CLose", "window.parent.fn_uploaddownloadclose()", true);
                }
            }
        }
        protected void btnFileUpload_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();


                if (fuAttachments.HasFile)
                {
                    string fileExt = System.IO.Path.GetExtension(fuAttachments.FileName);

                    //if (fileExt.ToUpper() == ".JPG" || fileExt.ToUpper() == ".PNG" || fileExt.ToUpper() == ".DOC" || fileExt.ToUpper() == ".DOCX" || fileExt.ToUpper() == ".PDF"
                    //    || fileExt.ToUpper() == ".XLS" || fileExt.ToUpper() == ".XLSX" || fileExt.ToUpper() == ".TXT")
                    {
                        if ((fuAttachments.PostedFile.ContentLength / 1024) > 5120)
                        {
                            ErrorCollection.Add("Invalid File Size", "File Size Must Be Less then 5MB");
                            return;
                        }

                        System.Collections.SortedList slfiledet = new System.Collections.SortedList();

                        //string fileName = System.IO.Path.GetFileNameWithoutExtension(fuAttachments.FileName);
                        //string fileType = fuAttachments.PostedFile.ContentType;
                        //int conLen = fuAttachments.PostedFile.ContentLength;
                        //string fileExtt = System.IO.Path.GetExtension(fuAttachments.FileName);

                        //string fileNameP = Path.GetFileName(fuAttachments.FileName);
                        //Session["UploadImageFileName"] = fuAttachments.FileName;
                        //Session["PathUploadImageFileName"] = fileNameP;

                        //long fileKeyId = (GetKeyID("TMP_FILE_ID", "TMP_FILE_DET"));
                        //slfiledet.Add(slfiledet.Count + 1.ToString(), fileKeyId.ToString());
                        //fileKeyId = fileKeyId + 1;
                        string screenCode = string.Empty;
                        string progID = hf_ProgId.Value.ToString();
                        string transID = hf_ScreenId.Value.ToString();
                        //Session["UploadDate"] = DateTime.Now.ToShortDateString().ToString();

                        DataTable dtData = DBMethod.ExecuteQuery(FINSQL.GetMenuDtl(int.Parse(progID.ToString()))).Tables[0];
                        if (dtData != null)
                        {
                            if (dtData.Rows.Count > 0)
                            {
                                screenCode = dtData.Rows[0]["form_code"].ToString();
                            }
                        }
                        string createdBy = this.LoggedUserName;
                        DateTime createdDate = DateTime.Today;

                        if (hf_ProgId.Value != "0" && Session["Mode"] != FINAppConstants.Add)
                        {
                            string folderPath = System.Web.Configuration.WebConfigurationManager.AppSettings["FileUploadFolder"].ToString();

                            TMP_FILE_DET tMP_FILE_DET = new TMP_FILE_DET();
                            if (txtFileName.Text.ToString().Length > 0)
                            {
                                tMP_FILE_DET.FILE_NAME = txtFileName.Text;
                            }
                            else
                            {

                                tMP_FILE_DET.FILE_NAME = System.IO.Path.GetFileNameWithoutExtension(fuAttachments.FileName);
                            }
                            tMP_FILE_DET.FILE_TYPE = fuAttachments.PostedFile.ContentType;
                            tMP_FILE_DET.CONTENT_LENGTH = fuAttachments.PostedFile.ContentLength;
                            tMP_FILE_DET.FILE_EXTENSTION = System.IO.Path.GetExtension(fuAttachments.FileName).ToString().Replace(".", "");
                            tMP_FILE_DET.CREATED_BY = this.LoggedUserName;
                            tMP_FILE_DET.CREATED_DATE = DateTime.Now;
                            tMP_FILE_DET.TMP_FILE_ID = DBMethod.GetPrimaryKeyValue(FINSequenceConstant.TMP_FILE_DET_SEQ);
                            tMP_FILE_DET.TRANSACTION_ID = transID;
                            tMP_FILE_DET.SCREEN_CODE = screenCode;
                            DBMethod.SaveEntity<TMP_FILE_DET>(tMP_FILE_DET);
                            //string squery = "insert into TMP_FILE_DET (TMP_FILE_ID,FILE_NAME,FILE_TYPE,CONTENT_LENGTH,FILE_EXTENSTION,CREATED_BY,CREATED_DATE,TRANSACTION_ID,SCREEN_CODE) values ('" + fileKeyId + "','" + newFileName + "','" + fileType + "','" + conLen + "','" + fileExt + "','" + createdBy + "',sysdate, " + "'" + transID + "','" + screenCode + "')";
                            //DBMethod.ExecuteQuery(squery);
                            if (System.IO.File.Exists(folderPath + tMP_FILE_DET.TMP_FILE_ID + "." + tMP_FILE_DET.FILE_EXTENSTION))
                            {
                                System.IO.File.Delete(folderPath + tMP_FILE_DET.TMP_FILE_ID + "." + tMP_FILE_DET.FILE_EXTENSTION);
                            }
                            if (fuAttachments != null)
                            {
                                Directory.CreateDirectory(folderPath);
                                fuAttachments.SaveAs(folderPath + tMP_FILE_DET.TMP_FILE_ID + "." + tMP_FILE_DET.FILE_EXTENSTION);
                            }
                        }
                        else
                        {
                            //lblFileDet.Visible = true;
                            //lblFileDet.Text = fuAttachments.FileName + "Upload Failed. Please upload the document in Edit mode";
                            ErrorCollection.Add("Error on upload", "Upload Failed. Please upload the document in Edit mode");
                            return;
                        }

                        //slfiledet.Add(slfiledet.Count + 1.ToString(), fileKeyId.ToString());
                        // lblFileDet.Visible = true;
                        // lblFileDet.Text = fuAttachments.FileName + "Uploaded Successfully";
                        Session["FileAttachments"] = slfiledet;
                        Session["StrRecordId"] = null;
                    }
                    //else
                    //{
                    //    ErrorCollection.Add("InvalidFileFormat", "Invalid File Format!");
                    //}
                    BindGrid();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Error on upload", ex.ToString());
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void BindGrid()
        {
            DataTable dt_DocDet = DBMethod.ExecuteQuery(FINSQL.GetUploadedDocList(hf_ScreenId.Value.ToString())).Tables[0];
            websiteURL = System.Web.Configuration.WebConfigurationManager.AppSettings["WBSITEURL"].ToString();

            Session["dt_DocDet"] = dt_DocDet;
            gvProj.DataSource = Session["dt_DocDet"];
            gvProj.DataBind();

        }
        protected void btnFileDownload_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                // lblFileDet.Visible = false;

                string screenCode = string.Empty;
                string fname = string.Empty;
                string fextention = string.Empty;
                string folderPath = System.Web.Configuration.WebConfigurationManager.AppSettings["FileUploadFolder"].ToString();

                if (Session["StrRecordId"].ToString() != "0" && Session["Mode"].ToString() != FINAppConstants.Add)
                {
                    DataTable dtData = DBMethod.ExecuteQuery(FINSQL.GetMenuDtl(int.Parse(Session["ProgramID"].ToString()))).Tables[0];
                    if (dtData != null && dtData.Rows.Count > 0)
                    {
                        screenCode = dtData.Rows[0]["form_code"].ToString();
                        if (screenCode.Length > 0 && screenCode.ToString() != null)
                        {
                            string squery = "select * from TMP_FILE_DET where TRANSACTION_ID='" + Session["StrRecordId"].ToString() + "' and SCREEN_CODE='" + screenCode.ToString() + "' order by tmp_file_id desc";
                            DataTable dtDet = DBMethod.ExecuteQuery(squery).Tables[0];

                            if (dtDet != null && dtDet.Rows.Count > 0)
                            {
                                fname = dtDet.Rows[0]["file_name"].ToString();

                                if (System.IO.File.Exists(folderPath + fname))
                                {
                                    string linkToDownload = Server.MapPath("../UploadFile/DocFile/" + fname);
                                    Response.AppendHeader("content-disposition", "attachment; filename=" + fname);
                                    Response.TransmitFile(linkToDownload);
                                    Response.End();
                                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "DownloadAsDoc", "window.open('" + linkToDownload + "?Id=" + Mode + "','','');", true);
                                }
                            }
                        }
                    }
                }
                else
                {
                    //lblFileDet.Visible = true;
                    //lblFileDet.Text = "Download Failed. Download File in Edit mode";
                    ErrorCollection.Add("Error on download", "Download Failed. Download File in Edit mode");
                    return;
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Error on download", ex.ToString());
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        public static long GetKeyID(string ColumnName, string TableName)
        {
            try
            {
                long maxkeyid = 0;
                DataSet dsmaxkeyid = new DataSet();

                dsmaxkeyid = DBMethod.ExecuteQuery("SELECT nvl(max(" + ColumnName + "),0) AS ColumnName FROM " + TableName);

                if (dsmaxkeyid.Tables[0].Rows.Count > 0)
                {
                    maxkeyid = Convert.ToInt32(dsmaxkeyid.Tables[0].Rows[0][0].ToString());
                }
                return maxkeyid;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void gvProj_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                string folderPath = System.Web.Configuration.WebConfigurationManager.AppSettings["FileUploadFolder"].ToString();
                ErrorCollection.Clear();

                if (gvProj.Rows.Count > 0)
                {
                    GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;

                    string fileId = gvProj.DataKeys[gvr.RowIndex].Values["TMP_FILE_ID"].ToString();

                    if (Session["ProjMode"] != null)
                    {
                        if (Session["ProjMode"] == "E")
                        {
                            dt_DocDet = DBMethod.ExecuteQuery(FINSQL.GetUploadedDocList(Session["StrRecordId"].ToString(), fileId)).Tables[0];
                            string filename = "";
                            if (dt_DocDet.Rows.Count > 0)
                            {
                                filename = dt_DocDet.Rows[0]["file_name"].ToString();

                                if (System.IO.File.Exists(folderPath + filename))
                                {
                                    string linkToDownload = Server.MapPath("../UploadFile/DocFile/" + filename);
                                    Response.AppendHeader("content-disposition", "attachment; filename=" + filename);
                                    Response.TransmitFile(linkToDownload);
                                    Response.End();
                                }
                            }
                        }
                        //else if (Session["ProjMode"] == "D")
                        //{
                        //    Session["projId"] = projId;
                        //    DBMethod.ExecuteQuery(FINSQL.UpdateUploadedDocList(Session["projId"].ToString()));

                        //    dt_DocDet = DBMethod.ExecuteQuery(FINSQL.GetUploadedDocList(Session["StrRecordId"].ToString())).Tables[0];
                        //    gvProj.DataSource = dt_DocDet;
                        //    gvProj.DataBind();

                        //    Response.Redirect(Request.RawUrl);
                        //}
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ParameterDisplayError", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {

                }
            }
        }

        protected void btnDownload_Click(object sender, EventArgs e)
        {
            Session["ProjMode"] = "E";
        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            Session["ProjMode"] = "D";
        }

        protected void gvProj_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            if (Session["dt_DocDet"] != null)
            {
                gvProj.PageIndex = e.NewPageIndex;
                gvProj.DataSource = (DataTable)Session["dt_DocDet"];
                gvProj.DataBind();
            }
        }

        protected void gvProj_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    HyperLink h1 = (HyperLink)e.Row.FindControl("hyLUrl");
                    h1.NavigateUrl = websiteURL + "/UploadFile/DocFile/" + gvProj.DataKeys[e.Row.RowIndex].Values["filepath"].ToString();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TS_RDB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    // Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        //protected void gvProj_RowDataBound(object sender, GridViewPageEventArgs e)
        //{

        //}

        //public override void VerifyRenderingInServerForm(Control control)
        //{
        //    /* Verifies that the control is rendered */
        //}
    }
}