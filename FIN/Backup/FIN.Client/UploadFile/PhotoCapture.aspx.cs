﻿using System;   
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.Services;

using FIN.BLL.AP;
using FIN.BLL.HR;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.BLL;
using VMVServices.Web;

namespace FIN.Client.UploadFile
{
    public partial class PhotoCapture : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!this.IsPostBack)
            {

                //  Session["CapturedImagePath"] = null;
                if (Request.InputStream.Length > 0)
                {
                    using (StreamReader reader = new StreamReader(Request.InputStream))
                    {
                        string hexString = Server.UrlEncode(reader.ReadToEnd());
                        string imagePath = string.Empty;

                        //if (Session["opregId"] != null)
                        //{

                        string imageName = DateTime.Now.ToString("ddMMyyyyHHMMssff");
                       
                        //File.WriteAllBytes(Server.MapPath(imagePath), ConvertHexToBytes(hexString));
                        
                        //Session["CapturedImagePath"] = imageName+".png";



                        System.Collections.SortedList slfiledet = new System.Collections.SortedList();
                        TMP_FILE_DET tMP_FILE_DET = new TMP_FILE_DET();
                        tMP_FILE_DET.FILE_NAME = "CAPTURE";
                        tMP_FILE_DET.FILE_TYPE = "PNG";
                        tMP_FILE_DET.CONTENT_LENGTH = 0;
                        tMP_FILE_DET.FILE_EXTENSTION = "png";
                        tMP_FILE_DET.CREATED_BY = this.LoggedUserName;
                        tMP_FILE_DET.CREATED_DATE = DateTime.Now;
                        tMP_FILE_DET.TMP_FILE_ID = DBMethod.GetPrimaryKeyValue(FINSequenceConstant.TMP_FILE_DET_SEQ);
                        DBMethod.SaveEntity<TMP_FILE_DET>(tMP_FILE_DET);                        
                        File.WriteAllBytes(Server.MapPath("~/UploadFile/Captures/" + tMP_FILE_DET.TMP_FILE_ID.ToString() + "." + tMP_FILE_DET.FILE_EXTENSTION.ToString().Replace(".","")), ConvertHexToBytes(hexString));
                        
                        Session["CapturedImagePath"] ="~/UploadFile/Captures/" + tMP_FILE_DET.TMP_FILE_ID.ToString() + "." + tMP_FILE_DET.FILE_EXTENSTION.ToString().Replace(".","");
                        Session["CapturedImage"] = ResolveUrl(Session["CapturedImagePath"].ToString());
                        slfiledet.Add(slfiledet.Count + 1.ToString(), tMP_FILE_DET.TMP_FILE_ID.ToString());
                        slfiledet.Add(slfiledet.Count + 1.ToString(), tMP_FILE_DET.FILE_EXTENSTION.ToString().Replace(".",""));
                        slfiledet.Add(slfiledet.Count + 1.ToString(), tMP_FILE_DET.FILE_NAME.ToString());
                        lblFileDet.Visible = true;
                        lblFileDet.Text = fuAttachments.FileName + " Upload Successfully ";
                        Session[FINSessionConstants.Attachments] = slfiledet;
                        //}
                    }
                }
            }
        }

        private static byte[] ConvertHexToBytes(string hex)
        {
            byte[] bytes = new byte[hex.Length / 2];
            for (int i = 0; i < hex.Length; i += 2)
            {
                bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            }
            return bytes;
        }

        [WebMethod(EnableSession = true)]
        public static string GetCapturedImage()
        {
            string url = string.Empty;

            if (HttpContext.Current.Session["CapturedImage"] != null)
            {
                url = HttpContext.Current.Session["CapturedImage"].ToString();
                HttpContext.Current.Session["CapturedImage"] = null;
            }
            return url;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            Response.Redirect(Request.RawUrl);
        }

        protected void btnFileUpload_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                if (fuAttachments.HasFile)
                {
                    string fileExt = System.IO.Path.GetExtension(fuAttachments.FileName);

                    if (fileExt.ToUpper() != "")
                    {
                        //if ((fuAttachments.PostedFile.ContentLength/1024) > 1024)
                        //{
                        //    ErrorCollection.Add("Invalid File Size", "File Size Must Be Less then 1MB");
                        //    return;
                        //}

                        System.Collections.SortedList slfiledet = new System.Collections.SortedList();
                        TMP_FILE_DET tMP_FILE_DET = new TMP_FILE_DET();
                        tMP_FILE_DET.FILE_NAME = System.IO.Path.GetFileNameWithoutExtension(fuAttachments.FileName);
                        tMP_FILE_DET.FILE_TYPE = fuAttachments.PostedFile.ContentType;
                        tMP_FILE_DET.CONTENT_LENGTH = fuAttachments.PostedFile.ContentLength;
                        tMP_FILE_DET.FILE_EXTENSTION = System.IO.Path.GetExtension(fuAttachments.FileName).Replace(".","");
                        tMP_FILE_DET.CREATED_BY = this.LoggedUserName;
                        tMP_FILE_DET.CREATED_DATE = DateTime.Now;
                        tMP_FILE_DET.TMP_FILE_ID = DBMethod.GetPrimaryKeyValue(FINSequenceConstant.TMP_FILE_DET_SEQ);
                        DBMethod.SaveEntity<TMP_FILE_DET>(tMP_FILE_DET);
                        fuAttachments.SaveAs(Server.MapPath("~/UploadFile/Captures/" + tMP_FILE_DET.TMP_FILE_ID.ToString() + "." + tMP_FILE_DET.FILE_EXTENSTION.ToString().Replace(".","")));
                        //if (Session[iAcademeSessionConstants.Attachments] !=null)
                        //{
                        //    slfiledet = (System.Collections.SortedList)Session[iAcademeSessionenConstants.Attachments];
                        //}
                        Session["CapturedImagePath"] = "~/UploadFile/Captures/" + tMP_FILE_DET.TMP_FILE_ID.ToString() + "." + tMP_FILE_DET.FILE_EXTENSTION.ToString().Replace(".","");
                                                
                        slfiledet.Add(slfiledet.Count + 1.ToString(), tMP_FILE_DET.TMP_FILE_ID.ToString());
                        slfiledet.Add(slfiledet.Count + 1.ToString(), tMP_FILE_DET.FILE_EXTENSTION.ToString().Replace(".",""));
                        slfiledet.Add(slfiledet.Count + 1.ToString(), tMP_FILE_DET.FILE_NAME.ToString());
                        lblFileDet.Visible = true;
                        lblFileDet.Text = fuAttachments.FileName + " Upload Successfully ";
                        Session[FINSessionConstants.Attachments] = slfiledet;
                    }
                    else
                    {
                        ErrorCollection.Add("InvalidFileFormat", "'.jpg','.gif','.png','.bmp' File Format only Accept");
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Error on upload", ex.ToString());
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
    }
}
