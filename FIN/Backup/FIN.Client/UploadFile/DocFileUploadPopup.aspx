﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DocFileUploadPopup.aspx.cs"
    Inherits="FIN.Client.MainPage.DocFileUploadPopup" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <script src="../Scripts/JQuery/jquery-1.11.0.js" type="text/javascript"></script>
    <link href="../Styles/main.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/GridStyle.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">

        function fn_ShowIFrame(frmDivId, ifrmid, ifrmheight) {

            if ($("#hf_LastMenu").val() != frmDivId) {
                $("#" + $("#hf_LastMenu").val()).css("display", "none");
            }
            $("#" + frmDivId).toggle("slow");
            var iFrameID = document.getElementById(ifrmid);
            iFrameID.height = ifrmheight + "px";
            $("#hf_LastMenu").val(frmDivId);
            //$("#LeaveRequest").offset({ top: 50, left: 50 });
        }

        function fn_ShowAlert() {

            $("#divAlert").toggle("slow");

            //$("#LeaveRequest").offset({ top: 50, left: 50 });
        }

        function fn_ShowNotification() {

            $("#divNotification").toggle("slow");
        }
        //        $(document).ready(function () {
        //            OpenDetails($("#hf_DetDiv").val(), $("#hf_imgClose").val(), $("#hf_imgOpen").val());
        //        });
    </script>
</head>
<body>
    <form id="formupload" runat="server">
    <div style="background-color: Gray;">
        <table width="100%">
            <tr>
                <td>
                    <div class="lblBox" style="float: left; width: 150px" id="lblFileName">
                        File Name
                    </div>
                </td>
                <td>
                    <div class="divtxtBox" style="float: left; width: 350px">
                        <asp:TextBox ID="txtFileName" runat="server" TabIndex="1"  CssClass=" txtBox"></asp:TextBox>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="lblBox" style="float: left; width: 150px" id="Div1">
                        Select File
                    </div>
                </td>
                <td>
                    <div class="divtxtBox" style="float: left; width: 350px">
                        <asp:FileUpload ID="fuAttachments" runat="server" CssClass="EntryFont  txtBox" />
                        <asp:RequiredFieldValidator runat="server" ControlToValidate="fuAttachments" ValidationGroup="Upload"
                            CssClass="DisplayFont" ID="reqAttachemnts">*</asp:RequiredFieldValidator>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    <asp:Button ID="btnFileUpload" runat="server" Text="Upload" ValidationGroup="Upload"
                        CssClass="DisplayFont button" OnClick="btnFileUpload_Click" />
                </td>
            </tr>
            <tr style="display: none">
                <td colspan="2" align="right" style="float: left; width: 50%; display: none">
                    <asp:Button ID="btnDownload" runat="server" Text="Download" CssClass="DisplayFont button"
                        OnClick="btnFileDownload_Click" />
                </td>
            </tr>
            <tr>
                <td colspan="2" style="width: 100%">
                    <div align="left">
                        <asp:GridView ID="gvProj" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                            PagerStyle-HorizontalAlign="Right" Width="100%" PageSize="20" CssClass="Grid"
                            OnPageIndexChanging="gvProj_PageIndexChanging" DataKeyNames="filepath" OnRowCommand="gvProj_RowCommand"
                            OnRowDataBound="gvProj_RowDataBound" ShowHeaderWhenEmpty="True" 
                            EmptyDataText="No Record Found">
                            <Columns>
                                <%-- <asp:BoundField DataField="TMP_FILE_ID" HeaderText="ID" ReadOnly="True" SortExpression="TMP_FILE_ID" />--%>
                                <asp:TemplateField HeaderText="File Name">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="hyLUrl" runat="server" onClick="fn_ShowAlert();fn_ShowIFrame('docUploadDownload','UDfrm',450);"
                                            Target="_download"><%# Eval("FILE_NAME")%>
                                        </asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <%--<asp:BoundField DataField="TMP_FILE_ID" HeaderText="ID"
                                        ReadOnly="True" SortExpression="TMP_FILE_ID" />
                                    <asp:HyperLinkField DataTextField="FILE_NAME" HeaderText="File Name" SortExpression="FILE_NAME"
                                        DataNavigateUrlFields="TMP_FILE_ID" DataNavigateUrlFormatString="DocFileUploadPopup.aspx?id={0}" />--%>
                                <asp:BoundField DataField="UPLOADED_BY" HeaderText="Upload By" />
                                <asp:BoundField DataField="UPLOADED_ON" HeaderText="Uploaded On" DataFormatString="{0:dd/MM/yyyy}" />
                            </Columns>

<PagerStyle HorizontalAlign="Right"></PagerStyle>
                        </asp:GridView>
                        <%--   <asp:GridView ID="gvProj" runat="server" GridLines="None" AllowPaging="false" Style="background-color: transparent;
                            height: 450px; border: 0;" ForeColor="White" Font-Size="20px" Font-Names="Calibri"
                            AutoGenerateColumns="false" OnRowCommand="gvData_RowCommand" ShowHeader="False"
                            Width="98%" DataKeyNames="TMP_FILE_ID" EmptyDataText="No Record To Found" CssClass="Grid">
                            <Columns>
                                <asp:BoundField DataField="FILE_NAME" ItemStyle-Width="77%" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Height="50px" />
                                <asp:TemplateField HeaderText="Download" ItemStyle-Width="7%" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btnDownload" AlternateText="Download" runat="server" CausesValidation="false"
                                            OnClick="btnDownload_Click" ToolTip="Export to Word" Width="20px" Height="20px"
                                            CommandName="Download" ImageUrl="~/Images/Update.png" />
                                        <asp:Label ID="lblprojidDwnld" Visible="false" runat="server" Text='<%# Eval("TMP_FILE_ID") %>'></asp:Label></FooterTemplate>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblprojidDwnld" Visible="false" runat="server" Text='<%# Eval("TMP_FILE_ID") %>'></asp:Label></FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Delete" ItemStyle-Width="7%" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btnDelete" AlternateText="Delete" runat="server" CausesValidation="false"
                                            OnClick="btnDelete_Click" Width="20px" ToolTip="Delete" Height="20px" CommandName="Delete"
                                            ImageUrl="~/Images/Delete.JPG" />
                                        <asp:Label ID="lblprojid" Visible="false" runat="server" Text='<%# Eval("TMP_FILE_ID") %>'></asp:Label></FooterTemplate>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblprojid" Visible="false" runat="server" Text='<%# Eval("TMP_FILE_ID") %>'></asp:Label></FooterTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>--%>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <asp:HiddenField ID="hf_ScreenId" runat="server" Value="" />
      <asp:HiddenField ID="hf_ProgId" runat="server" Value="" />
    </form>
    
</body>
</html>
