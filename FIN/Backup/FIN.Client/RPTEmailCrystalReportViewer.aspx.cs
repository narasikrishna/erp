﻿using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using VMVServices.Web;
using VMVServices.Services;
using VMVServices.Services.Data;
using VMVServices.Helpers;
using System;
using CrystalDecisions.Web;
using FIN.BLL;
namespace FIN.Client
{

    public partial class RPTEmailCrystalReportViewer : PageBase
    {
        ReportDocument rdReport = new ReportDocument();
        //  ReportBLL reportBLL = new ReportBLL();
        ReportDocument cRDoc = new ReportDocument();
        ParameterFields pfields = new ParameterFields();
        DataTable dtData = new DataTable();

        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void Page_Init(object sender, EventArgs e)
        {
            ErrorCollection.Clear();
            try
            {
                string reportUrl = string.Empty, strURL = string.Empty;
                DataSet dsReport = new DataSet();
                Hashtable htParameters = new Hashtable();
                Hashtable htHeadingParameters = new Hashtable();
                bool isCrossTabReport = false;
                bool isChartObject = false;
                trNoRecords.Visible = false;

                htHeadingParameters.Remove("UserName");
                htHeadingParameters.Remove("OrgName");
                htHeadingParameters.Remove("OrgAddress1");
                htHeadingParameters.Remove("OrgAddress2");
                htHeadingParameters.Remove("OrgAddress3");
                htHeadingParameters.Remove("OrgPhone");
                htHeadingParameters.Remove("OrgMobile");
                htHeadingParameters.Remove("OrgFax");
                htHeadingParameters.Remove("ReportName");
                htHeadingParameters.Remove("RowBackGround");
                htHeadingParameters.Remove("AlternateRowBackGround");
                //htHeadingParameters.Remove("OrgEmail");


                htHeadingParameters.Add("UserName", VMVServices.Web.Utils.UserName.ToString());
                htHeadingParameters.Add("OrgName", VMVServices.Web.Utils.OrganizationName.ToString());
                htHeadingParameters.Add("RowBackGround", "255");

                htHeadingParameters.Add("AlternateRowBackGround", "255");

                //htHeadingParameters.Add("AlternateRowBackGround", "192");

                //htHeadingParameters.Add("OrgAddress1", VMVServices.Web.Utils.Address1.ToString());
                //htHeadingParameters.Add("OrgAddress2", VMVServices.Web.Utils.Address2.ToString());
                //htHeadingParameters.Add("OrgAddress3", VMVServices.Web.Utils.Address3.ToString());
                //htHeadingParameters.Add("OrgPhone", VMVServices.Web.Utils.Telephone.ToString());
                //htHeadingParameters.Add("OrgMobile", VMVServices.Web.Utils.Mobile.ToString());
                //htHeadingParameters.Add("OrgFax", VMVServices.Web.Utils.Fax.ToString());


                if (Session["ProgramID"].ToString().Length > 0)
                {
                    System.Data.DataTable dt_menulist = (System.Data.DataTable)Session[FIN.BLL.FINSessionConstants.MenuData];
                    System.Data.DataRow[] dr_list = dt_menulist.Select("MENU_KEY_ID=" + Session["ProgramID"].ToString());

                    if (dr_list.Length > 0)
                    {
                        if (VMVServices.Web.Utils.LanguageCode.ToString().Length > 0)
                        {
                            htHeadingParameters.Add("ReportName", dr_list[0]["ATTRIBUTE3"].ToString());
                        }
                        else
                        {
                            htHeadingParameters.Add("ReportName", dr_list[0]["ATTRIBUTE2"].ToString());
                        }
                    }
                }

                //  DataTable dt_data = FIN.DAL.DBMethod.ExecuteQuery(FIN.DAL.GL.Organisation_DAL.getOrganisationData(VMVServices.Web.Utils.OrganizationID)).Tables[0];
                //if (dt_data.Rows.Count == 1)
                //{
                if (System.IO.File.Exists(System.Web.Configuration.WebConfigurationManager.AppSettings["OrgLogoFolder"].ToString() + VMVServices.Web.Utils.OrganizationID + ".jpg"))
                {
                    VMVServices.Web.Utils.OrgLogo = System.Web.Configuration.WebConfigurationManager.AppSettings["OrgLogoFolder"].ToString() + VMVServices.Web.Utils.OrganizationID + ".jpg";
                }
                //dt_data.Rows[0]["comp_logo"].ToString();
                //}

                htHeadingParameters.Add("OrgLogo", VMVServices.Web.Utils.OrgLogo.ToString());

                if (System.IO.File.Exists(System.Web.Configuration.WebConfigurationManager.AppSettings["OrgLogoFolder"].ToString() + VMVServices.Web.Utils.OrganizationID + "_GOVT.jpg"))
                {
                    htHeadingParameters.Add("OrgGOVTLogo", System.Web.Configuration.WebConfigurationManager.AppSettings["OrgLogoFolder"].ToString() + VMVServices.Web.Utils.OrganizationID + "_GOVT.jpg");
                }

                // htHeadingParameters.Add("OrgEmail", RPCServices.Web.Utils.Email.ToString());

                if (Session["photoUrl"] != null)
                {
                    htHeadingParameters.Add("EmpFoto", Session["photoUrl"]);
                }

                ReportFormulaParameter = htHeadingParameters;

                if (VMVServices.Web.Utils.LanguageCode.ToString().Length > 0)
                {
                    if (ReportFile.Contains("_OL"))
                    {
                        ReportFile = ReportFile.ToString().ToUpper().Replace("_OL", "");
                    }

                    ReportFile = ReportFile.ToString().ToUpper().Replace(".RPT", "_OL.rpt");

                }


                rdReport.Load(Server.MapPath(ReportFile));
                this.ApplyLogonToTables(rdReport);


                rdReport.SetDatabaseLogon(System.Configuration.ConfigurationManager.AppSettings["UNAME"].ToString(), System.Configuration.ConfigurationManager.AppSettings["PWD"].ToString(), System.Configuration.ConfigurationManager.AppSettings["DBNAME"].ToString(), System.Configuration.ConfigurationManager.AppSettings["DBNAME"].ToString());


                if (ReportParameter == null || ReportParameter.Count == 0)
                {
                    dsReport = ReportData;
                    if (dsReport != null)
                    {
                        if (dsReport.Tables.Count > 0)
                        {
                            for (int i = 0; i < dsReport.Tables.Count; i++)
                            {
                                rdReport.SetDataSource(dsReport.Tables[i]);
                            }
                        }
                    }
                    int k = 0;

                    if (SubReportData != null && SubReportData.Tables.Count > 0)
                    {
                        dsReport = SubReportData;

                        CrystalDecisions.CrystalReports.Engine.Subreports subReports = rdReport.Subreports;

                        foreach (CrystalDecisions.CrystalReports.Engine.ReportDocument subReport in subReports)
                        {
                            subReport.SetDataSource(dsReport.Tables[k]);
                            if (dsReport.Tables.Count > 1)
                            {
                                k = k + 1;
                            }
                        }
                    }
                }

                else if (ReportParameter.Count > 0)
                {
                    IDictionaryEnumerator hsEnum = ReportParameter.GetEnumerator();
                    while (hsEnum.MoveNext())
                    {
                        rdReport.SetParameterValue(hsEnum.Key.ToString(), hsEnum.Value);
                        //rdReport.DataDefinition.ParameterFields[hsEnum.Key.ToString()].

                    }
                }

                if (Session["HeadingParameters"] != null)
                {
                    ReportFormulaParameter = (Hashtable)Session["HeadingParameters"];
                }

                if (ReportFormulaParameter.Count > 0)
                {
                    IDictionaryEnumerator hsEnum = ReportFormulaParameter.GetEnumerator();
                    while (hsEnum.MoveNext())
                    {
                        try
                        {
                            rdReport.DataDefinition.FormulaFields[hsEnum.Key.ToString()].Text = "'" + hsEnum.Value.ToString() + "'";
                        }
                        catch (Exception ex1)
                        {
                        }
                    }
                }



                if (VMVServices.Web.Utils.ReportFilterParameter.Count > 0)
                {
                    IDictionaryEnumerator hsEnum = VMVServices.Web.Utils.ReportFilterParameter.GetEnumerator();
                    while (hsEnum.MoveNext())
                    {
                        try
                        {
                            rdReport.DataDefinition.FormulaFields[hsEnum.Key.ToString()].Text = "'" + hsEnum.Value.ToString() + "'";
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                }

                isCrossTabReport = false;
                isChartObject = false;
                for (int iLoop = 0; iLoop < rdReport.ReportDefinition.ReportObjects.Count; iLoop++)
                {
                    if (rdReport.ReportDefinition.ReportObjects[iLoop].Kind == ReportObjectKind.CrossTabObject)
                    {
                        isCrossTabReport = true;
                    }


                    if (rdReport.ReportDefinition.ReportObjects[iLoop].Kind == ReportObjectKind.ChartObject)
                    {
                        isChartObject = true;
                    }
                }
                isCrossTabReport = true;
                isChartObject = true;
                rdReport.Refresh();
                rdReport.SetCssClass(ObjectScope.AllReportObjectsInPageFooterSections, "reportFooterTitle");
                CrystalReportViewer1.HasCrystalLogo = false;
                CrystalReportViewer1.ReportSource = rdReport;

                if (Request.QueryString["EmailRpt"] != null && Request.QueryString["MailId"] != null)
                {
                    if (Request.QueryString["MailId"].ToString() != string.Empty && Request.QueryString["EmailRpt"].ToString() == "Payslip")
                    {
                        rdReport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Server.MapPath("~/TmpWordDocs/Payslip.pdf"));
                        rdReport.Close();
                        string fileName = Server.MapPath("~/TmpWordDocs/Payslip.pdf");
                        CommonUtils.GenerateEmailAttachment(Request.QueryString["MailId"].ToString(), "Pay Slip", "", fileName);
                    }
                }
            }
            catch (Exception ex)
            {


            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('Validation.aspx','','" + ValidationWindowProperties + "');", true);

                }
            }
        }

        private void ApplyLogonToTables(ReportDocument rdReport)
        {

            TableLogOnInfo ConInfo = new TableLogOnInfo();
            Tables crTables = rdReport.Database.Tables;
            foreach (CrystalDecisions.CrystalReports.Engine.Table crTable in crTables)
            {
                {
                    ConInfo.ConnectionInfo.ServerName = System.Configuration.ConfigurationManager.AppSettings["DBNAME"].ToString();
                    ConInfo.ConnectionInfo.UserID = System.Configuration.ConfigurationManager.AppSettings["UNAME"].ToString();
                    ConInfo.ConnectionInfo.Password = System.Configuration.ConfigurationManager.AppSettings["PWD"].ToString();
                }
                crTable.ApplyLogOnInfo(ConInfo);
            }

            CrystalDecisions.CrystalReports.Engine.Subreports subReports = rdReport.Subreports;

            foreach (CrystalDecisions.CrystalReports.Engine.ReportDocument subReport in subReports)
            {
                TableLogOnInfo ConInfoSub = new TableLogOnInfo();
                Tables crTablesSub = subReport.Database.Tables;
                foreach (CrystalDecisions.CrystalReports.Engine.Table crTable in crTablesSub)
                {
                    {
                        ConInfoSub.ConnectionInfo.ServerName = System.Configuration.ConfigurationManager.AppSettings["DBNAME"].ToString();
                        ConInfoSub.ConnectionInfo.UserID = System.Configuration.ConfigurationManager.AppSettings["UNAME"].ToString();
                        ConInfoSub.ConnectionInfo.Password = System.Configuration.ConfigurationManager.AppSettings["PWD"].ToString();



                    }
                    crTable.ApplyLogOnInfo(ConInfoSub);
                }

            }

            rdReport.Refresh();
        }

        //protected void Page_UnLoad(object sender, EventArgs e)
        //{
        //    if (rdReport != null)
        //    {
        //        rdReport.Close();
        //        rdReport.Dispose();
        //    }
        //}

    }
}