using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using VMVServices.Web;

namespace Aon.Client
{
    public partial class Validation : PageBase
    {

        /// <summary>
        /// Page Load Events
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {

                    if ((ErrorCollection.Count == 0) || (Session["UserName"] == null))
                    {
                        ErrorCollection.Clear();
                        ErrorCollection.Add("Logout", "Your Session Has Expired,Please again login into the application");
                    }

                    if (ErrorCollection.Count > 0)
                    {
                        IDictionaryEnumerator myEnumerator = ErrorCollection.GetEnumerator();
                        while (myEnumerator.MoveNext())
                        {
                            blErrorMessage.Items.Add(myEnumerator.Value.ToString());
                        }
                    }

                    ErrorCollection.Clear();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}