﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="LeaseContractEntry.aspx.cs" Inherits="FIN.Client.PMS.LeaseContractEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 800px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="lblBankName">
                Contract No
            </div>
            <div class="divtxtBox" style="float: left; width: 155px">
                <asp:TextBox ID="txtContractNo" Enabled="false" CssClass="validate[] RequiredField txtBox"
                    MaxLength="10" runat="server" TabIndex="2"></asp:TextBox>
            </div>
            <div class="colspace" style="float: left;">
                &nbsp</div>
            <div class="lblBox" style="float: left; width: 195px" id="lblBankShortName">
                Sign Date
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:TextBox ID="txtBankShortName1" Enabled="false" CssClass="validate[] RequiredField txtBox"
                    MaxLength="10" runat="server" TabIndex="2"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="lblBranchName">
                Start Date
            </div>
            <div class="divtxtBox" style="float: left; width: 155px">
                <asp:TextBox ID="txtStartDate" Enabled="false" CssClass="validate[] RequiredField txtBox"
                    MaxLength="10" runat="server" TabIndex="2"></asp:TextBox>
            </div>
            <div class="colspace" style="float: left;">
                &nbsp</div>
            <div class="lblBox" style="float: left; width: 195px" id="lblShortName">
                End Date
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:TextBox ID="txtEnddate" CssClass="validate[required] RequiredField txtBox" MaxLength="10"
                    runat="server" TabIndex="4"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="lblSWIFTCode">
                Pmt.Frequency
            </div>
            <div class="divtxtBox" style="float: left; width: 152px">
                <asp:DropDownList ID="ddlPmtfreq" runat="server" TabIndex="7" CssClass="validate[required] RequiredField ddlStype">
                </asp:DropDownList>
            </div>
            <div class="colspace" style="float: left;">
                &nbsp</div>
            <div class="lblBox" style="float: left; width: 197px" id="lblIFSCCode">
                Contract Status
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:DropDownList ID="ddlContractStatus" runat="server" TabIndex="7" CssClass="validate[required] RequiredField ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="lblAccountType">
                Tenant ID
            </div>
            <div class="divtxtBox" style="float: left; width: 155px">
                <asp:DropDownList ID="ddlTenantID" runat="server" TabIndex="7" CssClass="validate[required] RequiredField ddlStype">
                </asp:DropDownList>
            </div>
            <div class="colspace" style="float: left;">
                &nbsp</div>
            <div class="lblBox" style="float: left; width: 195px" id="lblAccountNumber">
                Tenant Name
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:TextBox ID="txtTenantName" TabIndex="8" MaxLength="50" CssClass="validate[required] RequiredField txtBox"
                    runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="Div5">
                Agent Name
            </div>
            <div class="divtxtBox" style="float: left; width: 155px">
                <asp:DropDownList ID="ddlAgentName" runat="server" TabIndex="9" CssClass="ddlStype">
                </asp:DropDownList>
            </div>
            <div class="colspace" style="float: left;">
                &nbsp</div>
            <div class="lblBox" style="float: left; width: 195px" id="Div1">
                Sub Agent
            </div>
            <div class="divtxtBox" style="float: left; width: 152px">
                <asp:DropDownList ID="ddlSubagent" runat="server" TabIndex="9" CssClass="ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="Div2">
                Advance Amount
            </div>
            <div class="divChkbox" style="float: left; width: 150px">
                <asp:TextBox ID="txtAdvanceAmt" TabIndex="8" MaxLength="50" CssClass="validate[required] RequiredField txtBox"
                    runat="server"></asp:TextBox>
            </div>
            <div class="colspace" style="float: left;">
                &nbsp</div>
            <div class="lblBox" style="float: left; width: 200px" id="lblActive">
                Advance Period
            </div>
            <div class="divChkbox" style="float: left; width: 150px">
                <asp:DropDownList ID="ddlAdvPeriod" runat="server" TabIndex="9" CssClass="ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="Div3">
                Remarks
            </div>
            <div class="divChkbox" style="float: left; width: 530px">
                <asp:TextBox ID="txtRemarks" TabIndex="8" MaxLength="50" CssClass="validate[required] RequiredField txtBox"
                    runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                            TabIndex="12" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="13" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="14" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="15" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table>
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <%--<script src="../LanguageScript/CA/CAChangeLang.js" type="text/javascript"></script>--%>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
