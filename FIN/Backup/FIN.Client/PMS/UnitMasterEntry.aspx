﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="UnitMasterEntry.aspx.cs" Inherits="FIN.Client.PMS.UnitMasterEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 800px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="lblBankName">
                Building Code
            </div>
            <div class="divtxtBox" style="float: left; width: 155px">
                <asp:DropDownList ID="ddlBuildingCode" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    TabIndex="1">
                </asp:DropDownList>
            </div>
            <div class="colspace" style="float: left;">
                &nbsp</div>
            <div class="lblBox" style="float: left; width: 195px" id="lblBankShortName">
                Building Name
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:TextBox ID="txtBuildingNmae" Enabled="false" CssClass="validate[] RequiredField txtBox"
                    MaxLength="10" runat="server" TabIndex="2"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="lblBranchName">
                Unit
            </div>
            <div class="divtxtBox" style="float: left; width: 155px">
                <asp:TextBox ID="txtUnit" Enabled="false" CssClass="validate[] RequiredField txtBox"
                    MaxLength="10" runat="server" TabIndex="2"></asp:TextBox>
            </div>
            <div class="colspace" style="float: left;">
                &nbsp</div>
            <div class="lblBox" style="float: left; width: 195px" id="lblShortName">
                Floor
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:TextBox ID="txtFloor" CssClass="validate[required] RequiredField txtBox" MaxLength="10"
                    runat="server" TabIndex="4"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="lblSWIFTCode">
                Monthly Rent
            </div>
            <div class="divtxtBox" style="float: left; width: 152px">
                <asp:TextBox ID="txtMonthlyRent" MaxLength="50" CssClass="validate[required] RequiredField txtBox"
                    TabIndex="5" runat="server"></asp:TextBox>
            </div>
            <div class="colspace" style="float: left;">
                &nbsp</div>
            <div class="lblBox" style="float: left; width: 197px" id="lblIFSCCode">
                Unit Type Description
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:TextBox ID="txtUnitTypDesc" CssClass="validate[required] RequiredField txtBox"
                    MaxLength="50" runat="server" TabIndex="6"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="lblAccountType">
                Flat Type
            </div>
            <div class="divtxtBox" style="float: left; width: 155px">
                <asp:DropDownList ID="ddlFlatTyp" runat="server" TabIndex="7" CssClass="validate[required] RequiredField ddlStype">
                </asp:DropDownList>
            </div>
            <div class="colspace" style="float: left;">
                &nbsp</div>
            <div class="lblBox" style="float: left; width: 195px" id="lblAccountNumber">
                Unit Sqr.Feet
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:TextBox ID="txtUnitsqrfeet" TabIndex="8" MaxLength="50" CssClass="validate[required] RequiredField txtBox"
                    runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="Div5">
                Address1
            </div>
            <div class="divtxtBox" style="float: left; width: 155px">
                <asp:TextBox ID="txtAddrs1" TabIndex="8" MaxLength="50" CssClass="validate[required] RequiredField txtBox"
                    runat="server"></asp:TextBox>
            </div>
            <div class="colspace" style="float: left;">
                &nbsp</div>
            <div class="lblBox" style="float: left; width: 195px" id="Div1">
                Address2
            </div>
            <div class="divtxtBox" style="float: left; width: 152px">
                <asp:TextBox ID="TextBox1" TabIndex="8" MaxLength="50" CssClass="validate[required] RequiredField txtBox"
                    runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="Div3">
                Electricity No
            </div>
            <div class="divtxtBox" style="float: left; width: 155px">
                <asp:TextBox ID="txtElectNo" TabIndex="8" MaxLength="50" CssClass="validate[required] RequiredField txtBox"
                    runat="server"></asp:TextBox>
            </div>
            <div class="colspace" style="float: left;">
                &nbsp</div>
            <div class="lblBox" style="float: left; width: 195px" id="Div4">
                Extension No
            </div>
            <div class="divtxtBox" style="float: left; width: 152px">
                <asp:TextBox ID="txtExtensNo" TabIndex="8" MaxLength="50" CssClass="validate[required] RequiredField txtBox"
                    runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="Div6">
                City
            </div>
            <div class="divtxtBox" style="float: left; width: 155px">
                <asp:TextBox ID="txtCity" TabIndex="8" MaxLength="50" CssClass="validate[required] RequiredField txtBox"
                    runat="server"></asp:TextBox>
            </div>
            <div class="colspace" style="float: left;">
                &nbsp</div>
            <div class="lblBox" style="float: left; width: 195px" id="Div7">
                Present Rent
            </div>
            <div class="divtxtBox" style="float: left; width: 152px">
                <asp:TextBox ID="txtPresentRent" TabIndex="8" MaxLength="50" CssClass="validate[required] RequiredField txtBox"
                    runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="Div8">
                Municipality No
            </div>
            <div class="divtxtBox" style="float: left; width: 155px">
                <asp:TextBox ID="txtMunicipalityNo" TabIndex="8" MaxLength="50" CssClass="validate[required] RequiredField txtBox"
                    runat="server"></asp:TextBox>
            </div>
            <div class="colspace" style="float: left;">
                &nbsp</div>
            <div class="lblBox" style="float: left; width: 195px" id="Div9">
                Rental Type
            </div>
            <div class="divtxtBox" style="float: left; width: 152px">
                <asp:DropDownList ID="ddlRentalTyp" runat="server" TabIndex="7" CssClass="validate[required] RequiredField ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="Div10">
                Bed Rooms
            </div>
            <div class="divtxtBox" style="float: left; width: 155px">
                <asp:TextBox ID="txtBedrooms" TabIndex="8" MaxLength="50" CssClass="validate[required] RequiredField txtBox"
                    runat="server"></asp:TextBox>
            </div>
            <div class="colspace" style="float: left;">
                &nbsp</div>
            <div class="lblBox" style="float: left; width: 195px" id="Div11">
                Area Code
            </div>
            <div class="divtxtBox" style="float: left; width: 152px">
                <asp:DropDownList ID="ddlAreacode" runat="server" TabIndex="7" CssClass="validate[required] RequiredField ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="Div12">
                Bath Rooms
            </div>
            <div class="divtxtBox" style="float: left; width: 155px">
                <asp:TextBox ID="txtBathRooms" TabIndex="8" MaxLength="50" CssClass="validate[required] RequiredField txtBox"
                    runat="server"></asp:TextBox>
            </div>
            <div class="colspace" style="float: left;">
                &nbsp</div>
            <div class="lblBox" style="float: left; width: 195px" id="Div13">
                Area Name
            </div>
            <div class="divtxtBox" style="float: left; width: 152px">
                <asp:TextBox ID="txtAreaName" TabIndex="8" MaxLength="50" CssClass="validate[required] RequiredField txtBox"
                    runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="Div14">
                Parking Place
            </div>
            <div class="divtxtBox" style="float: left; width: 155px">
                <asp:TextBox ID="txtParkingplace" TabIndex="8" MaxLength="50" CssClass="validate[required] RequiredField txtBox"
                    runat="server"></asp:TextBox>
            </div>
            <div class="colspace" style="float: left;">
                &nbsp</div>
            <div class="lblBox" style="float: left; width: 195px" id="Div15">
                Active Status
            </div>
            <div class="divtxtBox" style="float: left; width: 152px">
                <asp:DropDownList ID="ddlActivestatus" runat="server" TabIndex="7" CssClass="validate[required] RequiredField ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="Div16">
                Others
            </div>
            <div class="divtxtBox" style="float: left; width: 155px">
                <asp:TextBox ID="txtOthers" TabIndex="8" MaxLength="50" CssClass="validate[required] RequiredField txtBox"
                    runat="server"></asp:TextBox>
            </div>
            <div class="colspace" style="float: left;">
                &nbsp</div>
            <div class="lblBox" style="float: left; width: 195px" id="Div17">
                Book Status
            </div>
            <div class="divtxtBox" style="float: left; width: 152px">
                <asp:DropDownList ID="ddlBookstatus" runat="server" TabIndex="7" CssClass="validate[required] RequiredField ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="Div2">
                Full-Furnished
            </div>
            <div class="divChkbox" style="float: left; width: 150px">
                <asp:CheckBox ID="chkFullfurnshd" runat="server" TabIndex="10" Checked="true" />
            </div>
            <div class="colspace" style="float: left;">
                &nbsp</div>
            <div class="lblBox" style="float: left; width: 200px" id="lblActive">
                Semi-Furnished
            </div>
            <div class="divChkbox" style="float: left; width: 150px">
                <asp:CheckBox ID="chkSemifurnished" runat="server" TabIndex="11" Checked="true" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="Div18">
                Un-Furnished
            </div>
            <div class="divChkbox" style="float: left; width: 150px">
                <asp:CheckBox ID="chkUnfurnished" runat="server" TabIndex="10" Checked="true" />
            </div>
            <div class="colspace" style="float: left;">
                &nbsp</div>
            <div class="lblBox" style="float: left; width: 200px" id="Div19">
                Others
            </div>
            <div class="divChkbox" style="float: left; width: 150px">
                <asp:CheckBox ID="chkothers" runat="server" TabIndex="11" Checked="true" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                            TabIndex="12" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="13" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="14" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="15" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table>
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <%--<script src="../LanguageScript/CA/CAChangeLang.js" type="text/javascript"></script>--%>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
