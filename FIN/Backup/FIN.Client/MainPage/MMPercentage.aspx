﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MMPercentage.aspx.cs" Inherits="FIN.Client.MainPage.MMPercentage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="shortcut icon" href="../Images/logo.ico" />
    <title>Miethree</title>
    <link href="../Styles/MainPage.css" rel="Stylesheet" type="text/css" />
    <link href="../Styles/GridStyle.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/main.css" rel="stylesheet" type="text/css" />
    <script src="../Scripts/JQuery/jquery-1.11.0.js" type="text/javascript"></script>
    <script type="text/javascript">
        function fn_CloseBrowser() {
            window.close();
        }
        function fn_Showframe() {
            $("#divFrmContainer").toggle("slow");

        }
        function fn_ModuleMouseOver(imgId, txtId, imgName) {
            $("#" + imgId).attr("src", '../Images/MMPercentage/' + imgName + '_IN.png');
            $("#" + txtId).attr("class", "divModuleTextIN");
        }
        function fn_ModuleMouseOut(imgId, txtId, imgName) {
            $("#" + imgId).attr("src", '../Images/MMPercentage/' + imgName + '_OUT.png');
            $("#" + txtId).attr("class", "divModuleText");
        }

        function fn_ModuleClick(ClickedMoudle) {

            $("#hf_MoudleName").val(ClickedMoudle);
            $("#btnModuleCLick").click();
        }
        function fn_ShowAlert() {
            $("#divAlert").toggle("slow");
        }
        function fn_ShowWorkFlow() {
            $("#divWorkFlow").toggle("slow");
        }

        $(document).ready(function () {
            var scr_height = window.screen.height - 100;
            $(".divCenter").css("height", scr_height + "px");
        });
        
    </script>
    <style type="text/css">
        body
        {
            background-image : url('../Images/Smalllogo.jpg');
            
        }
        
        .divContainer
        {
            width: 100%;            
            left: 50%;
        }
        .divTop
        {
            background: rgb(0,153,255);
            width: 100%;
            height: 35px;
        }
        .divCenter
        {
            width: 100%;
        }
        .divfooter
        {
            width: 100%;
            height: 25px; /* Height of the footer */
            background: rgb(0,153,255);
        }
        table
        {
            padding: 0px;
        }
        .divModuleText
        {
            color: Olive;
            font-family: Agency FB;
            font-size: 20px;
            padding-top: 15px;
            padding-left: 10px;
            cursor: pointer;
        }
        .divModuleTextIN
        {
            color: Blue;
            font-family: Agency FB;
            font-size: 20px;
            padding-top: 15px;
            padding-left: 10px;
            cursor: pointer;
        }
    </style>
</head>
<body style="margin: 0 0 0 0">
    <form id="form1" runat="server">
    <div align="center" >
        <div id="div_Container" class="divContainer">
            <table width="100%" cellspacing="0px" cellpadding="0px">
                <tr>
                    <td>
                        <div id="div_Top" class="divTop">
                            <div style="float: left; position: absolute; padding-top: 10px; padding-left: 20px;">
                                <img src="../Images/logo.png" alt="miethree" title="miethree" width="80px" height="40px" />
                            </div>
                            <div style="float: right; padding-right :20px;">
                                <table width="100px">
                                    <tr>
                                        <td>
                                            <img src="../Images/wf_icon.png" alt="WF" title="WorkFlow" onclick="fn_ShowWorkFlow()"
                                                width="25px" height="25px" />
                                        </td>
                                        <td>
                                            <img src="../Images/alert-icon.png" alt="Alert" title="Alert" onclick="fn_ShowAlert()"
                                                width="25px" height="25px" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="divCenter">
                        <div style="width: 500px; height: 500px; display: none; padding-left: 400px; position: absolute;"
                            id="divWorkFlow">
                            <asp:GridView ID="gvWorkFlow" runat="server" AutoGenerateColumns="False" DataKeyNames="workflow_code,transaction_code,menu_url,remarks,module_code,Level_Code,module_description,org_id,org_desc"
                                Style="width: 95%; height: 80%;" AllowPaging="true" PageSize="7" OnPageIndexChanging="gvWorkFlow_PageIndexChanging"
                                CssClass="GridModule" EmptyDataText=" No Record Found" ShowHeaderWhenEmpty="True"
                                OnRowDataBound="gvWorkFlow_RowDataBound">
                                <Columns>
                                    <asp:BoundField HeaderStyle-HorizontalAlign="Center" HeaderStyle-ForeColor="White"
                                        ItemStyle-Width="50px" ItemStyle-HorizontalAlign="Left" DataField="created_date"
                                        DataFormatString="{0:dd/MM/yyyy}" HeaderText="Workflow Date" />
                                    <asp:TemplateField HeaderText="Workflow Message" ItemStyle-HorizontalAlign="Left"
                                        HeaderStyle-HorizontalAlign="Center" HeaderStyle-ForeColor="White">
                                        <ItemTemplate>
                                            <asp:HyperLink Style="text-decoration: none;" ID="hyperMsgID" Text='<%#Eval("workflow_message") %>'
                                                Width="190px" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Level Code" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center"
                                        HeaderStyle-ForeColor="White">
                                        <ItemTemplate>
                                            <asp:HyperLink Style="text-decoration: none;" ID="hyperlevelID" Text='<%#Eval("Level_Code") %>'
                                                Width="40px" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Comments" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center"
                                        HeaderStyle-ForeColor="White">
                                        <ItemTemplate>
                                            <asp:HyperLink Style="text-decoration: none;" ID="hyperRemarksID" Text='<%#Eval("remarks") %>'
                                                Width="100px" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                <HeaderStyle CssClass="GridHeader" />
                                <PagerStyle CssClass="pgr"></PagerStyle>
                                <AlternatingRowStyle CssClass="GrdAltRow" />
                            </asp:GridView>
                        </div>
                        <div style="width: 500px; height: 500px; float: left; display: none; padding-left: 400px;
                            position: absolute;" id="divAlert">
                            <asp:GridView ID="gvAlert" runat="server" AutoGenerateColumns="False" DataKeyNames="alert_code,screen_code,transaction_code,menu_url,module_code,module_description,org_id,org_desc"
                                Style="width: 95%; height: 80%;" AllowPaging="true" OnPageIndexChanging="gvAlert_PageIndexChanging"
                                PageSize="7" EmptyDataText=" No Record Found" ShowHeaderWhenEmpty="True" OnRowDataBound="gvAlert_RowDataBound">
                                <Columns>
                                    <asp:BoundField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left"
                                        ItemStyle-Width="50px" DataField="created_date" DataFormatString="{0:dd/MM/yyyy}"
                                        HeaderStyle-ForeColor="White" HeaderText="Alert Date" />
                                    <asp:TemplateField HeaderText="Alert Message" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center"
                                        HeaderStyle-ForeColor="White">
                                        <ItemTemplate>
                                            <asp:HyperLink Style="text-decoration: none;" ID="hyperAlertMsgID" Text='<%#Eval("alert_message") %>'
                                                Width="190px" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                <HeaderStyle CssClass="GridHeader" />
                                <PagerStyle CssClass="pgr"></PagerStyle>
                                <AlternatingRowStyle CssClass="GrdAltRow" />
                            </asp:GridView>
                        </div>
                        <table border="0px" width="100%">
                            <tr style="display: none">
                                <td >
                                    <div class="lblBox" style="float: left; width: 140px" id="lblOrganization">
                                        Organization
                                    </div>
                                </td>
                                <td>
                                </td>
                                <td>
                                    <div class="divtxtBox" style="float: left; width: 250px">
                                        <asp:DropDownList ID="ddlOrg" MaxLength="50" runat="server" CssClass="ddlStype" Width="250px"
                                            AutoPostBack="True" OnSelectedIndexChanged="ddlOrg_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:30%; padding-right:50px" >
                                    <div id="divGL" onclick="fn_ModuleClick('GL')" onmouseover="fn_ModuleMouseOver('imgGL','divGLtext','GL')"
                                        onmouseout="fn_ModuleMouseOut('imgGL','divGLtext','GL')">
                                        <div id="divGLImg" style="float: right">
                                            <img id="imgGL" src="../Images/MMPercentage/GL_OUT.png" alt="GL" title="GL" />
                                        </div>
                                        <div id="divGLtext" class="divModuleText" style="float: right">
                                            General Ledger
                                        </div>
                                    </div>
                                </td>
                                <td rowspan="5" align="center" style="width:30%">
                                    <img src="../Images/MMPercentage/CenterGIF.gif" alt=" " title=" " width="100%" height="300px" />
                                </td>
                                <td  style="width:30%;padding-left:50px">
                                    <div id="divFA" onclick="fn_ModuleClick('FA')" onmouseover="fn_ModuleMouseOver('imgFA','divFAtext','FA')"
                                        onmouseout="fn_ModuleMouseOut('imgFA','divFAtext','FA')">
                                        <div id="divFAImg" style="float: left">
                                            <img id="imgFA" src="../Images/MMPercentage/FA_OUT.png" alt="FA" title="FA" />
                                        </div>
                                        <div id="divFAtext" class="divModuleText" style="float: left">
                                            Fixed Asset
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:30%; padding-right:50px">
                                    <div id="divAP" onclick="fn_ModuleClick('AP')" onmouseover="fn_ModuleMouseOver('imgAP','divAPtext','AP')"
                                        onmouseout="fn_ModuleMouseOut('imgAP','divAPtext','AP')">
                                        <div id="divAPImg" style="float: right ">
                                            <img id="imgAP" src="../Images/MMPercentage/AP_OUT.png" alt="AP" title="AP" />
                                        </div>
                                        <div id="divAPtext" class="divModuleText" style="float: right">
                                            Account Payable
                                        </div>
                                    </div>
                                </td>
                                <td  style="width:30%;padding-left:50px">
                                    <div id="divAR" onclick="fn_ModuleClick('AR')" onmouseover="fn_ModuleMouseOver('imgAR','divARtext','AR')"
                                        onmouseout="fn_ModuleMouseOut('imgAR','divARtext','AR')">
                                        <div id="divARImg" style="float: left">
                                            <img id="imgAR" src="../Images/MMPercentage/AR_OUT.png" alt="AR" title="AR" />
                                        </div>
                                        <div id="divARtext" class="divModuleText" style="float: left">
                                            Account Receivables
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:30%; padding-right:50px">
                                    <div id="divHR" onclick="fn_ModuleClick('HR')" onmouseover="fn_ModuleMouseOver('imgHR','divHRtext','HR')"
                                        onmouseout="fn_ModuleMouseOut('imgHR','divHRtext','HR')">
                                        <div id="divHRImg" style="float: right">
                                            <img id="imgHR" src="../Images/MMPercentage/HR_OUT.png" alt="HR" title="HR" />
                                        </div>
                                        <div id="divHRtext" class="divModuleText" style="float: right">
                                            Human Resource
                                        </div>
                                    </div>
                                </td>
                                <td  style="width:30%;padding-left:50px">
                                    <div id="divSS" onclick="fn_ModuleClick('SS')" onmouseover="fn_ModuleMouseOver('imgSS','divSStext','SS')"
                                        onmouseout="fn_ModuleMouseOut('imgSS','divSStext','SS')">
                                        <div id="divSSImg" style="float: left">
                                            <img id="imgSS" src="../Images/MMPercentage/SS_OUT.png" alt="SS" title="SS" />
                                        </div>
                                        <div id="divSStext" class="divModuleText" style="float: left">
                                            System Setup
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:30%; padding-right:50px">
                                    <div id="divCM" onclick="fn_ModuleClick('CM')" onmouseover="fn_ModuleMouseOver('imgCM','divCMtext','CM')"
                                        onmouseout="fn_ModuleMouseOut('imgCM','divCMtext','CM')">
                                        <div id="divCMImg" style="float: right">
                                            <img id="imgCM" src="../Images/MMPercentage/CM_OUT.png" alt="CM" title="CM" />
                                        </div>
                                        <div id="divCMtext" class="divModuleText" style="float: right">
                                            Cash Management
                                        </div>
                                    </div>
                                </td>
                                <td  style="width:30%;padding-left:50px">
                                    <div id="divLoan" onclick="fn_ModuleClick('Loan')" onmouseover="fn_ModuleMouseOver('imgLoan','divLoantext','Loan')"
                                        onmouseout="fn_ModuleMouseOut('imgLoan','divLoantext','Loan')">
                                        <div id="divLoanImg" style="float: left">
                                            <img id="imgLoan" src="../Images/MMPercentage/Loan_OUT.png" alt="Loan" title="Loan" />
                                        </div>
                                        <div id="divLoantext" class="divModuleText" style="float: left; padding-top: 15px">
                                            Loan
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:30%; padding-right:50px">
                                    <div id="divPayroll" onclick="fn_ModuleClick('Payroll')" onmouseover="fn_ModuleMouseOver('imgPayroll','divPayrolltext','Payroll')"
                                        onmouseout="fn_ModuleMouseOut('imgPayroll','divPayrolltext','Payroll')">
                                        <div id="divPayrollImg" style="float: right">
                                            <img id="imgPayroll" src="../Images/MMPercentage/Payroll_OUT.png" alt="Payroll" title="Payroll" />
                                        </div>
                                        <div id="divPayrolltext" class="divModuleText" style="float: right">
                                            Payroll
                                        </div>
                                    </div>
                                </td>
                                <td  style="width:30%;padding-left:50px">
                                    <div id="div22">
                                        <asp:Button ID="btnModuleCLick" runat="server" Text="" OnClick="btnModuleCLick_Click"
                                            Style="display: none" />
                                        <asp:HiddenField runat="server" ID="hf_MoudleName" Value="" />
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
        <div class="divfooter" style="float: right; color: White; font-family: Verdana; font-size: 10px;
            position: fixed; bottom: 0px; height: 15px;" align="right">
            Powered by VMV Systems Pvt Ltd
        </div>
    </div>
    </form>
</body>
</html>
