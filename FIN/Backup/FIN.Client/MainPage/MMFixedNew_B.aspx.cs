﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.BLL;

using FIN.DAL;
using System.Collections;
using VMVServices.Web;

namespace FIN.Client.MainPage
{
    public partial class MMFixedNew_B : PageBase
    {

        DataTable dtWorkflow = new DataTable();
        DataTable dtAlert = new DataTable();
        string navigationMode = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    VMVServices.Web.Utils.SavedRecordId = "";
                    FillComboBox();
                    Session["Tree_Link"] = "";
                    Session[FINSessionConstants.LevelCode] = null;

                    lblUName.Text = Session["FullUserName"].ToString();
                    AssignToWorkflow();
                    AssignToAlert();
                    Session[FINSessionConstants.ORGCurrency] = "";


                    if (Session[FINSessionConstants.Sel_Lng] == null)
                    {
                        Session[FINSessionConstants.Sel_Lng] = "EN";
                    }
                    imgbtnGL.Enabled = false;
                    imgbtnCA.Enabled = false;
                    imgbtnFA.Enabled = false;
                    imgbtnHR.Enabled = false;
                    imgbtnPA.Enabled = false;
                    imgbtnSSM.Enabled = false;
                    imgbtnAR.Enabled = false;
                    imgbtnAP.Enabled = false;




                    DataTable dt_ModuleList = DBMethod.ExecuteQuery(FIN.DAL.FINSQL.getModuleName4User(this.LoggedUserName)).Tables[0];
                    setVisiableModuleIcon(dt_ModuleList);

                    if (VMVServices.Web.Utils.OrganizationID != null)
                    {
                        ddlOrg.SelectedValue = VMVServices.Web.Utils.OrganizationID;
                        divModuleSel.Attributes.Remove("Disabled");
                        imgbtnGL.Enabled = true;
                        imgbtnCA.Enabled = true;
                        imgbtnFA.Enabled = true;
                        imgbtnHR.Enabled = true;
                        imgbtnPA.Enabled = true;
                        imgbtnSSM.Enabled = true;
                        imgbtnAR.Enabled = true;
                        imgbtnAP.Enabled = true;
                    }
                    else
                    {
                        divModuleSel.Attributes.Add("Disabled", "");
                    }


                    ChangeOrgDet();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("workflow", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    // Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void setVisiableModuleIcon(DataTable dt_ModuList)
        {



            for (int iLoop = 0; iLoop < dt_ModuList.Rows.Count; iLoop++)
            {
                switch (dt_ModuList.Rows[iLoop]["MODULE_CODE"].ToString())
                {
                    case "GL":
                        id_GL.Visible = true;
                        break;
                    case "AP":
                        id_AP.Visible = true;
                        break;
                    case "HR":
                        id_HR.Visible = true;
                        break;
                    case "CA":
                        id_CA.Visible = true;
                        break;
                    case "PR":
                        id_PA.Visible = true;
                        break;
                    case "FA":
                        id_FA.Visible = true;
                        break;
                    case "AR":
                        id_AR.Visible = true;
                        break;
                    case "SSM":
                        id_SSM.Visible = true;
                        break;
                    case "LN":
                        id_LN.Visible = true;
                        break;



                }
            }
        }
        private void FillComboBox()
        {
            FIN.BLL.GL.Organisation_BLL.getOrganizationDet(ref ddlOrg);
            DataTable dt_data = DBMethod.ExecuteQuery(FIN.DAL.GL.Organisation_DAL.getOrganisationDetails()).Tables[0];
            if (dt_data.Rows.Count == 1)
            {
                ddlOrg.SelectedValue = dt_data.Rows[0]["ORG_ID"].ToString();
                ChangeOrgDet();
            }

        }

        private void AssignToWorkflow()
        {
            try
            {
                ErrorCollection.Clear();
                dtWorkflow = DBMethod.ExecuteQuery(FINSQL.GetWorkFlowMonitor(VMVServices.Web.Utils.UserName)).Tables[0];
                if (dtWorkflow != null)
                {
                    //    if (dtWorkflow.Rows.Count > 0)
                    //    {
                    //  Menu_BLL.GetMenuDetail();
                    Session["dtWorkflow"] = dtWorkflow;
                    gvWorkFlow.DataSource = dtWorkflow;
                    gvWorkFlow.DataBind();

                    //ddlOrg.SelectedValue = dtWorkflow.Rows[0]["org_id"].ToString();

                    //Session[FINSessionConstants.ModuleName] = dtWorkflow.Rows[0]["module_code"].ToString();
                    //Session[FINSessionConstants.ModuleDescription] = dtWorkflow.Rows[0]["module_description"].ToString();

                    //BindOrgDetails();
                    //    }
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("workflow", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    // Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void AssignToAlert()
        {
            try
            {
                ErrorCollection.Clear();

                dtAlert = DBMethod.ExecuteQuery(FINSQL.GetAlertUserLevel(VMVServices.Web.Utils.UserName)).Tables[0];
                if (dtAlert != null)
                {
                    //if (dtAlert.Rows.Count > 0)
                    //{
                    Session["dtAlert"] = dtAlert;
                    gvAlert.DataSource = dtAlert;
                    gvAlert.DataBind();
                    // }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("alkert", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    // Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        protected void imgbtnAP_Click(object sender, ImageClickEventArgs e)
        {
            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/Generic_" + Session["Sel_Lng"].ToString() + ".properties"));

            Session[FINSessionConstants.ModuleName] = FINListConstants_BLL.AP;
            //Session[FINSessionConstants.ModuleDescription] = FINListConstants_BLL.AP_D;
            Session[FINSessionConstants.ModuleDescription] = Prop_File_Data["AP_P"];
            Response.Redirect(System.Configuration.ConfigurationManager.AppSettings["MainPage"].ToString());
        }

        protected void imgbtnHR_Click(object sender, ImageClickEventArgs e)
        {
            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/Generic_" + Session["Sel_Lng"].ToString() + ".properties"));

            Session[FINSessionConstants.ModuleName] = FINListConstants_BLL.HR;
            //Session[FINSessionConstants.ModuleDescription] = FINListConstants_BLL.HR_D;
            Session[FINSessionConstants.ModuleDescription] = Prop_File_Data["HR_P"];
            Response.Redirect(System.Configuration.ConfigurationManager.AppSettings["MainPage"].ToString());
        }

        protected void imgbtnSSM_Click(object sender, EventArgs e)
        {
            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/Generic_" + Session["Sel_Lng"].ToString() + ".properties"));

            Session[FINSessionConstants.ModuleName] = FINListConstants_BLL.SSM;
            Session[FINSessionConstants.ModuleDescription] = Prop_File_Data["SSM_P"];
            Response.Redirect(System.Configuration.ConfigurationManager.AppSettings["MainPage"].ToString());
        }

        protected void imgbtnLN_Click(object sender, EventArgs e)
        {
            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/Generic_" + Session["Sel_Lng"].ToString() + ".properties"));

            Session[FINSessionConstants.ModuleName] = FINListConstants_BLL.LN;
            Session[FINSessionConstants.ModuleDescription] = Prop_File_Data["LN_P"];
            Response.Redirect(System.Configuration.ConfigurationManager.AppSettings["MainPage"].ToString());
        }

        protected void imgbtnGL_Click(object sender, ImageClickEventArgs e)
        {
            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/Generic_" + Session["Sel_Lng"].ToString() + ".properties"));

            Session[FINSessionConstants.ModuleName] = FINListConstants_BLL.GL;
            Session[FINSessionConstants.ModuleDescription] = Prop_File_Data["GL_P"];
            Response.Redirect(System.Configuration.ConfigurationManager.AppSettings["MainPage"].ToString());
        }

        protected void imgbtnCA_Click(object sender, ImageClickEventArgs e)
        {
            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/Generic_" + Session["Sel_Lng"].ToString() + ".properties"));

            Session[FINSessionConstants.ModuleName] = FINListConstants_BLL.CA;
            Session[FINSessionConstants.ModuleDescription] = Prop_File_Data["CA_P"];
            Response.Redirect(System.Configuration.ConfigurationManager.AppSettings["MainPage"].ToString());
        }
        protected void imgbtnPA_Click(object sender, ImageClickEventArgs e)
        {
            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/Generic_" + Session["Sel_Lng"].ToString() + ".properties"));

            Session[FINSessionConstants.ModuleName] = FINListConstants_BLL.PA;
            Session[FINSessionConstants.ModuleDescription] = Prop_File_Data["PA_P"];
            Response.Redirect(System.Configuration.ConfigurationManager.AppSettings["MainPage"].ToString());
        }
        protected void imgbtnFA_Click(object sender, ImageClickEventArgs e)
        {
            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/Generic_" + Session["Sel_Lng"].ToString() + ".properties"));

            Session[FINSessionConstants.ModuleName] = FINListConstants_BLL.FA;
            Session[FINSessionConstants.ModuleDescription] = Prop_File_Data["FA_P"];
            Response.Redirect(System.Configuration.ConfigurationManager.AppSettings["MainPage"].ToString());
        }
        #region "Paging"
        /// <summary>
        /// Set the paging into Gridview
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvWorkFlow_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                gvWorkFlow.PageIndex = e.NewPageIndex;
                gvWorkFlow.DataSource = (DataTable)Session["dtWorkflow"];
                gvWorkFlow.DataBind();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("workflow", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    //  Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        protected void gvAlert_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                gvAlert.PageIndex = e.NewPageIndex;
                gvAlert.DataSource = (DataTable)Session["dtAlert"];
                gvAlert.DataBind();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Alert", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    //Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        #endregion

        protected void gvWorkFlow_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            HyperLink hFld = (HyperLink)e.Row.FindControl("hyperMsgID");
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.RowIndex >= 0)
                {
                    Session[FINSessionConstants.ModuleName] = gvWorkFlow.DataKeys[e.Row.RowIndex].Values["module_code"].ToString();
                    Session[FINSessionConstants.LevelCode] = gvWorkFlow.DataKeys[e.Row.RowIndex].Values["Level_Code"].ToString();
                    Session[FINSessionConstants.ModuleDescription] = gvWorkFlow.DataKeys[e.Row.RowIndex].Values["module_description"].ToString();

                    //HttpContext.Current.Session["OrganizationID"] = gvWorkFlow.DataKeys[e.Row.RowIndex].Values["org_id"].ToString();
                    //VMVServices.Web.Utils.OrganizationID = gvWorkFlow.DataKeys[e.Row.RowIndex].Values["org_id"].ToString();
                    //VMVServices.Web.Utils.OrganizationName = gvWorkFlow.DataKeys[e.Row.RowIndex].Values["org_desc"].ToString();

                    GetCurrency();
                    navigationMode = "WORKFLOW";

                    //hFld.NavigateUrl = "MainPageNew.aspx?WF=" + gvWorkFlow.DataKeys[e.Row.RowIndex].Values["menu_url"].ToString() + "&Id=" + gvWorkFlow.DataKeys[e.Row.RowIndex].Values["transaction_code"].ToString();
                    hFld.NavigateUrl = System.Configuration.ConfigurationManager.AppSettings["MainPage"].ToString() + "?WF=" + gvWorkFlow.DataKeys[e.Row.RowIndex].Values["workflow_code"].ToString() + "&Id=" + gvWorkFlow.DataKeys[e.Row.RowIndex].Values["transaction_code"].ToString() + "&MC=" + gvWorkFlow.DataKeys[e.Row.RowIndex].Values["module_code"].ToString() + "&LC=" + gvWorkFlow.DataKeys[e.Row.RowIndex].Values["Level_Code"].ToString() + "&MD=" + gvWorkFlow.DataKeys[e.Row.RowIndex].Values["module_description"].ToString() + "&ORG=" + gvWorkFlow.DataKeys[e.Row.RowIndex].Values["org_id"].ToString() + "&ORGD=" + gvWorkFlow.DataKeys[e.Row.RowIndex].Values["org_desc"].ToString() + "&NAV_MODE=" + navigationMode; 
                    // hFld.Attributes.Add("href", "javascript: void(0)");



                }
            }
        }

        protected void gvAlert_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            HyperLink hFld = (HyperLink)e.Row.FindControl("hyperAlertMsgID");
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.RowIndex >= 0)
                {
                    Session[FINSessionConstants.ModuleName] = gvAlert.DataKeys[e.Row.RowIndex].Values["module_code"].ToString();
                    Session[FINSessionConstants.ModuleDescription] = gvAlert.DataKeys[e.Row.RowIndex].Values["module_description"].ToString();

                    //HttpContext.Current.Session["OrganizationID"] = gvAlert.DataKeys[e.Row.RowIndex].Values["org_id"].ToString();
                    //VMVServices.Web.Utils.OrganizationID = gvAlert.DataKeys[e.Row.RowIndex].Values["org_id"].ToString();
                    //VMVServices.Web.Utils.OrganizationName = gvAlert.DataKeys[e.Row.RowIndex].Values["org_desc"].ToString();

                    if (gvAlert.DataKeys[e.Row.RowIndex].Values["screen_code"] == null)
                    {
                        ImageButton tmp = (ImageButton)(e.Row.FindControl("imgCloseAlert"));
                        tmp.Visible = true ;

                    }
                    GetCurrency();

                    navigationMode = "ALERT";
                    //hFld.NavigateUrl = "MainPageNew.aspx?WF=" + gvWorkFlow.DataKeys[e.Row.RowIndex].Values["menu_url"].ToString() + "&Id=" + gvWorkFlow.DataKeys[e.Row.RowIndex].Values["transaction_code"].ToString();
                    hFld.NavigateUrl = System.Configuration.ConfigurationManager.AppSettings["MainPage"].ToString() + "?WF=" + gvAlert.DataKeys[e.Row.RowIndex].Values["screen_code"].ToString() + "&Id=" + gvAlert.DataKeys[e.Row.RowIndex].Values["transaction_code"].ToString() + "&MC=" + gvAlert.DataKeys[e.Row.RowIndex].Values["module_code"].ToString() + "&MD=" + gvAlert.DataKeys[e.Row.RowIndex].Values["module_description"].ToString() + "&ORG=" + gvAlert.DataKeys[e.Row.RowIndex].Values["org_id"].ToString() + "&ORGD=" + gvAlert.DataKeys[e.Row.RowIndex].Values["org_desc"].ToString() + "&NAV_MODE=" + navigationMode;
                    // hFld.Attributes.Add("href", "javascript: void(0)");



                }
            }
        }
        private void GetCurrency()
        {

            DataTable dt = FIN.BLL.GL.Organisation_BLL.getCompanyCurrency(VMVServices.Web.Utils.OrganizationID);
            if (dt.Rows.Count > 0)
            {
                Session[FINSessionConstants.ORGCurrency] = dt.Rows[0][FINColumnConstants.CURRENCY_ID].ToString();
                Session[FINSessionConstants.ORGCurrencySymbol] = dt.Rows[0][FINColumnConstants.CURRENCY_SYMBOL].ToString();
            }
        }
        private void BindOrgDetails()
        {
            VMVServices.Web.Utils.OrganizationID = ddlOrg.SelectedValue.ToString();
            VMVServices.Web.Utils.OrganizationName = ddlOrg.SelectedItem.Text;
            VMVServices.Web.Utils.DecimalPrecision = "3";
            VMVServices.Web.Utils.CommaSeparation = "3";
            VMVServices.Web.Utils.OrgLogo = "";
            GetCurrency();

        }

        protected void ddlOrg_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChangeOrgDet();

        }
        private void ChangeOrgDet()
        {
            if (ddlOrg.SelectedValue.ToString().Length > 0)
            {
                divModuleSel.Attributes.Remove("Disabled");

                // ddlOrg.Enabled = false;

                BindOrgDetails();

                imgbtnGL.Enabled = true;
                imgbtnCA.Enabled = true;
                imgbtnFA.Enabled = true;
                imgbtnHR.Enabled = true;
                imgbtnPA.Enabled = true;
                imgbtnSSM.Enabled = true;
                imgbtnAR.Enabled = true;
                imgbtnAP.Enabled = true;
            }
        }
        protected void imgbtnAR_Click(object sender, ImageClickEventArgs e)
        {
            Session[FINSessionConstants.ModuleName] = FINListConstants_BLL.AR;
            Session[FINSessionConstants.ModuleDescription] = FINListConstants_BLL.AR_D;
            Response.Redirect(System.Configuration.ConfigurationManager.AppSettings["MainPage"].ToString());

        }

        protected void imgbtnSSM_Click(object sender, ImageClickEventArgs e)
        {
            Session[FINSessionConstants.ModuleName] = FINListConstants_BLL.SSM;
            Session[FINSessionConstants.ModuleDescription] = FINListConstants_BLL.SSM_D;
            Response.Redirect(System.Configuration.ConfigurationManager.AppSettings["MainPage"].ToString());
        }

        protected void imgbtnSSM_Click1(object sender, ImageClickEventArgs e)
        {
            Session[FINSessionConstants.ModuleName] = FINListConstants_BLL.SSM;
            Session[FINSessionConstants.ModuleDescription] = FINListConstants_BLL.SSM_D;
            Response.Redirect(System.Configuration.ConfigurationManager.AppSettings["MainPage"].ToString());

        }

        protected void imgCloseAlert_Click(object sender, ImageClickEventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            string str_alertcode = gvAlert.DataKeys[gvr.RowIndex].Values["ALERT_USER_LEVEL_CODE"].ToString();
            DBMethod.ExecuteNonQuery("update SSM_ALERT_USER_LEVELS set END_DATE=sysdate where ALERT_USER_LEVEL_CODE='" + str_alertcode + "'");
            gvAlert.Rows[gvr.RowIndex].Visible = false;
        }
    }
}