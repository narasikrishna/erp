﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="WorkFlowAlert.aspx.cs" Inherits="FIN.Client.WorkFlowAlert" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function fn_CallParentScript(str_FormName) {
            window.top.fn_ResizeEntryScreen(str_FormName, "inline", "inline");
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <table width="100%">
        <tr>
            <td style="width: 50%" valign="top">
                <div id="divWorkFlow">
                    <asp:GridView ID="gvWorkFlow" runat="server" AutoGenerateColumns="False" DataKeyNames="workflow_code,transaction_code,menu_url,remarks,module_code,Level_Code,module_description,org_id,org_desc"
                        Style="width: 95%; height: 80%;" AllowPaging="true" PageSize="7" OnPageIndexChanging="gvWorkFlow_PageIndexChanging"
                        CssClass="GridModule" EmptyDataText=" No Record Found" ShowHeaderWhenEmpty="True"
                        OnRowDataBound="gvWorkFlow_RowDataBound">
                        <Columns>
                            <asp:BoundField HeaderStyle-HorizontalAlign="Center" HeaderStyle-ForeColor="White"
                                ItemStyle-Width="50px" ItemStyle-HorizontalAlign="Left" DataField="created_date"
                                DataFormatString="{0:dd/MM/yyyy}" HeaderText="Workflow Date" />
                            <asp:TemplateField HeaderText="Workflow Message" ItemStyle-HorizontalAlign="Left"
                                HeaderStyle-HorizontalAlign="Center" HeaderStyle-ForeColor="White">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkFormTarget" Style="text-decoration: none;" OnClientClick='<%# "fn_CallParentScript(\"" + Eval("module_description")+ " -- > " + Eval("SCREEN_NAME") +"\")" %>'
                                        Text='<%#Eval("workflow_message") %>' runat="server" Width="190px"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Level Code" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center"
                                HeaderStyle-ForeColor="White">
                                <ItemTemplate>
                                    <asp:HyperLink Style="text-decoration: none;" ID="hyperlevelID" Text='<%#Eval("Level_Code") %>'
                                        Width="40px" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Comments" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center"
                                HeaderStyle-ForeColor="White">
                                <ItemTemplate>
                                    <asp:HyperLink Style="text-decoration: none;" ID="hyperRemarksID" Text='<%#Eval("remarks") %>'
                                        Width="100px" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                        <HeaderStyle CssClass="GridHeader" />
                        <PagerStyle CssClass="pgr"></PagerStyle>
                        <AlternatingRowStyle CssClass="GrdAltRow" />
                    </asp:GridView>
                </div>
            </td>
            <td style="width: 50%" valign="top">
                <div id="divAlert">
                    <asp:GridView ID="gvAlert" runat="server" AutoGenerateColumns="False" DataKeyNames="alert_code,screen_code,transaction_code,menu_url,module_code,module_description,org_id,org_desc"
                        Style="width: 95%; height: 80%;" AllowPaging="true" OnPageIndexChanging="gvAlert_PageIndexChanging"
                        PageSize="7" EmptyDataText=" No Record Found" ShowHeaderWhenEmpty="True" OnRowDataBound="gvAlert_RowDataBound">
                        <Columns>
                            <asp:BoundField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="50px" DataField="created_date" DataFormatString="{0:dd/MM/yyyy}"
                                HeaderStyle-ForeColor="White" HeaderText="Alert Date" />
                            <asp:TemplateField HeaderText="Alert Message" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center"
                                HeaderStyle-ForeColor="White">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkAlert" Style="text-decoration: none;" OnClientClick="fn_CallParentScript(' ')"
                                        Text='<%#Eval("alert_message") %>' runat="server" Width="190px"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                        <HeaderStyle CssClass="GridHeader" />
                        <PagerStyle CssClass="pgr"></PagerStyle>
                        <AlternatingRowStyle CssClass="GrdAltRow" />
                    </asp:GridView>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">

<script src="../LanguageScript/GL/GLChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
