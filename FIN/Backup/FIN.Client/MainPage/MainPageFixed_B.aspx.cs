﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using FIN.BLL;
using FIN.DAL;
namespace FIN.Client.MainPage
{
    public partial class MainPageFixed_B : System.Web.UI.Page
    {
        Dictionary<string, string> Prop_File_Data;
        System.Collections.SortedList ErrorCollection = new System.Collections.SortedList();

        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    if (HttpContext.Current.Session["OrganizationID"] == null || HttpContext.Current.Session["UserName"] == null || HttpContext.Current.Session["Multilanguage"] == null)
                    {
                        Response.Redirect("../SessionTimeOut.aspx");
                    }
                    hf_S_OrganizationID.Value = HttpContext.Current.Session["OrganizationID"].ToString();
                    hf_S_Multilanguage.Value = HttpContext.Current.Session["Multilanguage"].ToString();
                    hf_S_ModuleName.Value = Session[FINSessionConstants.ModuleName].ToString();
                    hf_S_ModuleDesc.Value = Session[FINSessionConstants.ModuleDescription].ToString();
                    txtUserID.Text = Session["UserName"].ToString();
                    Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/Generic_" + Session["Sel_Lng"].ToString() + ".properties"));
                    txtUserID.Enabled = false;
                    FillComboBox();

                    DataTable dt_menulist = new DataTable();

                    div_wf_query.Visible = false;
                    div_Query.Visible = true;
                    div_wf_Save.Visible = false;
                    div_Save.Visible = true;
                    div_wf_lng.Visible = false;
                    div_lng.Visible = true;
                    if (Session[FINSessionConstants.UserName] != null && Session[FINSessionConstants.PassWord] != null)
                    {
                        if (Request.QueryString["MC"] != null && Request.QueryString["MD"] != null && Request.QueryString["ORG"] != null && Request.QueryString["ORGD"] != null)
                        {
                            if (Request.QueryString["MC"] != string.Empty && Request.QueryString["MD"] != string.Empty && Request.QueryString["ORG"] != string.Empty && Request.QueryString["ORGD"] != string.Empty)
                            {
                                VMVServices.Web.Utils.OrganizationName = Request.QueryString["ORGD"].ToString();
                                VMVServices.Web.Utils.OrganizationID = Request.QueryString["ORG"].ToString();
                                Session[FINSessionConstants.ModuleDescription] = Request.QueryString["MD"].ToString();
                                Session[FINSessionConstants.ModuleName] = Request.QueryString["MC"].ToString();
                            }
                        }

                        lblOrgName.Text = VMVServices.Web.Utils.OrganizationName;
                        lblModuleName.Text = Session[FINSessionConstants.ModuleDescription].ToString();

                        lblUName.Text = Session["FullUserName"].ToString();


                        //Session["Tree_Link"] = "<a href='" + System.Configuration.ConfigurationManager.AppSettings["ModulePage"].ToString() + "' style='Color:white'> Home </a>" + " --> " + "<a href='../DashBoard.aspx' style='Color:white' target='centerfrm'> " + Session[FINSessionConstants.ModuleDescription] + " </a>";
                        Session["Tree_Link"] = "<a href='" + System.Configuration.ConfigurationManager.AppSettings["ModulePage"].ToString() + "' style='Color:white'> " + Prop_File_Data["Home_P"] + "</a>" + " --> " + "<a href='../DashBoard.aspx' style='Color:white' target='centerfrm'> " + Session[FINSessionConstants.ModuleDescription] + " </a>";
                        hf_TreeLink.Value = Session["Tree_Link"].ToString();

                        if (Session[FINSessionConstants.Sel_Lng] != null)
                        {
                            dt_menulist = DBMethod.ExecuteQuery(FINSQL.GetMenuList(Session[FINSessionConstants.UserName].ToString(), Session[FINSessionConstants.ModuleName].ToString(), Session[FINSessionConstants.Sel_Lng].ToString())).Tables[0];
                        }
                        System.Text.StringBuilder str_LinkQuery = new System.Text.StringBuilder();
                        string str_LinkWFQuery = string.Empty;

                        Boolean bol_incrI = true;
                        //str_LinkQuery.Append("<table width='100%' >");

                        string str_parent_menu = "";
                        for (int i = 0; i < dt_menulist.Rows.Count; i = i + 0)
                        {
                            str_LinkQuery.Append("<div class='divTR'>&nbsp;</div>");
                            string str_subMenuData = "";

                            for (int jLoop = 1; jLoop <= 4; jLoop++)
                            {
                                bol_incrI = true;
                                string str_LinkURL = "";


                                if (dt_menulist.Rows[i]["PARENT_MENU_CODE"].ToString().Length > 0)
                                {

                                    str_parent_menu = dt_menulist.Rows[i]["PARENT_MENU_CODE"].ToString();

                                    str_LinkURL = "<div class='divTDSpace'>&nbsp;</div><div class='divTD'><div class='SMHeader' onclick=\"ShowSubMenu('C_" + dt_menulist.Rows[i]["PARENT_MENU_CODE"].ToString() + "')\" id='" + dt_menulist.Rows[i]["PARENT_MENU_CODE"].ToString() + "' ><span> <img src='../Images/MainPage/grey.png' width='10px' height='10px' /> &nbsp;" + dt_menulist.Rows[i]["PARENT_MENU"].ToString() + "</div></div>";

                                    str_subMenuData += "<div class='SMHeaderDet' style=\"display:none\" id='C_" + dt_menulist.Rows[i]["PARENT_MENU_CODE"].ToString() + "'><div class='divTR' style='height:10px'>&nbsp;</div>";
                                    int submenucount = 1;
                                    for (int kLoop = i; kLoop < dt_menulist.Rows.Count; kLoop++)
                                    {
                                        if (str_parent_menu == dt_menulist.Rows[kLoop]["PARENT_MENU_CODE"].ToString())
                                        {
                                            if (submenucount == 5)
                                            {
                                                submenucount = 1;
                                                str_subMenuData += "<div class='divTR'>&nbsp;</div>";
                                            }
                                            str_subMenuData += "<div class='divTDSpace'>&nbsp;</div><div class='divTD'><a onClick=\"fn_ResizeEntryScreen('" + dt_menulist.Rows[kLoop]["SCREEN_NAME"].ToString() + "')\" href='" + dt_menulist.Rows[kLoop]["ENTRY_PAGE_OPEN"].ToString() + "?";
                                            str_subMenuData += "ProgramID=" + dt_menulist.Rows[kLoop]["MENU_KEY_ID"].ToString() + "&";
                                            str_subMenuData += QueryStringTags.Mode.ToString() + "=" + ProgramMode.Add.ToString().Substring(0, 1) + "&";
                                            str_subMenuData += QueryStringTags.ID.ToString() + "=0" + "&";
                                            str_subMenuData += QueryStringTags.AddFlag.ToString() + "=" + dt_menulist.Rows[kLoop]["INSERT_ALLOWED_FLAG"].ToString() + "&";
                                            str_subMenuData += QueryStringTags.UpdateFlag.ToString() + "=" + dt_menulist.Rows[kLoop]["UPDATE_ALLOWED_FLAG"].ToString() + "&";
                                            str_subMenuData += QueryStringTags.DeleteFlag.ToString() + "=" + dt_menulist.Rows[kLoop]["DELETE_ALLOWED_FLAG"].ToString() + "&";
                                            str_subMenuData += QueryStringTags.QueryFlag.ToString() + "=" + dt_menulist.Rows[kLoop]["QUERY_ALLOWED_FLAG"].ToString() + "&";
                                            str_subMenuData += QueryStringTags.ReportName.ToString() + "=" + dt_menulist.Rows[kLoop]["REPORT_NAME"].ToString() + "&";
                                            str_subMenuData += QueryStringTags.ReportName_OL.ToString() + "=" + dt_menulist.Rows[kLoop]["ATTRIBUTE3"].ToString() + "'";
                                            str_subMenuData += "  target='centerfrm'><span> <img src='../Images/MainPage/white.png' width='10px' height='10px' /> </span> &nbsp;<span>" + dt_menulist.Rows[kLoop]["SCREEN_NAME"].ToString() + "</span></a></div>";
                                            submenucount += 1;
                                            i = i + 1;
                                            bol_incrI = false;
                                            if (kLoop >= dt_menulist.Rows.Count)
                                            {

                                                break;

                                            }
                                        }
                                        else
                                        {

                                            break;

                                        }
                                    }
                                    str_subMenuData += "</div>";
                                    str_LinkQuery.Append(str_LinkURL);
                                }
                                else
                                {
                                    str_LinkURL = "<div class='divTDSpace'>&nbsp;</div><div class='divTD'><a onClick=\"fn_ResizeEntryScreen('" + dt_menulist.Rows[i]["SCREEN_NAME"].ToString() + "')\" href='" + dt_menulist.Rows[i]["ENTRY_PAGE_OPEN"].ToString() + "?";
                                    str_LinkURL += "ProgramID=" + dt_menulist.Rows[i]["MENU_KEY_ID"].ToString() + "&";
                                    str_LinkURL += QueryStringTags.Mode.ToString() + "=" + ProgramMode.Add.ToString().Substring(0, 1) + "&";
                                    str_LinkURL += QueryStringTags.ID.ToString() + "=0" + "&";
                                    str_LinkURL += QueryStringTags.AddFlag.ToString() + "=" + dt_menulist.Rows[i]["INSERT_ALLOWED_FLAG"].ToString() + "&";
                                    str_LinkURL += QueryStringTags.UpdateFlag.ToString() + "=" + dt_menulist.Rows[i]["UPDATE_ALLOWED_FLAG"].ToString() + "&";
                                    str_LinkURL += QueryStringTags.DeleteFlag.ToString() + "=" + dt_menulist.Rows[i]["DELETE_ALLOWED_FLAG"].ToString() + "&";
                                    str_LinkURL += QueryStringTags.QueryFlag.ToString() + "=" + dt_menulist.Rows[i]["QUERY_ALLOWED_FLAG"].ToString() + "&";
                                    str_LinkURL += QueryStringTags.ReportName.ToString() + "=" + dt_menulist.Rows[i]["REPORT_NAME"].ToString() + "&";
                                    str_LinkURL += QueryStringTags.ReportName_OL.ToString() + "=" + dt_menulist.Rows[i]["ATTRIBUTE3"].ToString() + "'";

                                    str_LinkURL += "  target='centerfrm'><span> <img src='../Images/MainPage/white.png' width='10px' height='10px' /> </span> &nbsp;<span>" + dt_menulist.Rows[i]["SCREEN_NAME"].ToString() + "</span></a></div>";

                                    str_LinkQuery.Append(str_LinkURL);
                                }

                                //for workflow process


                                if (Request.QueryString["WF"] != null && Request.QueryString["Id"] != null)
                                {

                                    if (dt_menulist.Rows.Count != i)
                                    {
                                        if (dt_menulist.Rows[i]["SCREEN_CODE"].ToString().Length > 0)
                                        {

                                            if (dt_menulist.Rows[i]["SCREEN_CODE"].ToString() != null && dt_menulist.Rows[i]["SCREEN_CODE"].ToString() != string.Empty)
                                            {
                                                if (dt_menulist.Rows[i]["SCREEN_CODE"].ToString().ToUpper() == Request.QueryString["WF"].ToString().ToUpper())
                                                {
                                                    str_LinkWFQuery = dt_menulist.Rows[i]["ENTRY_PAGE_OPEN"].ToString() + "?";
                                                    str_LinkWFQuery += "ProgramID=" + dt_menulist.Rows[i]["MENU_KEY_ID"].ToString() + "&";
                                                    str_LinkWFQuery += QueryStringTags.Mode.ToString() + "=" + ProgramMode.WApprove.ToString().Substring(0, 1) + "&";
                                                    str_LinkWFQuery += QueryStringTags.ID.ToString() + "=" + Request.QueryString["Id"].ToString() + "&";
                                                    str_LinkWFQuery += QueryStringTags.AddFlag.ToString() + "=" + dt_menulist.Rows[i]["INSERT_ALLOWED_FLAG"].ToString() + "&";
                                                    str_LinkWFQuery += QueryStringTags.UpdateFlag.ToString() + "=" + dt_menulist.Rows[i]["UPDATE_ALLOWED_FLAG"].ToString() + "&";
                                                    str_LinkWFQuery += QueryStringTags.DeleteFlag.ToString() + "=" + dt_menulist.Rows[i]["DELETE_ALLOWED_FLAG"].ToString() + "&";
                                                    str_LinkWFQuery += QueryStringTags.QueryFlag.ToString() + "=" + dt_menulist.Rows[i]["QUERY_ALLOWED_FLAG"].ToString() + "&";
                                                    str_LinkWFQuery += QueryStringTags.ReportName.ToString() + "=" + dt_menulist.Rows[i]["REPORT_NAME"].ToString() + "&";
                                                    str_LinkWFQuery += QueryStringTags.ReportName_OL.ToString() + "=" + dt_menulist.Rows[i]["ATTRIBUTE3"].ToString() + "'";
                                                    Session["str_LinkWFQuery"] = str_LinkWFQuery;

                                                    div_wf_query.Visible = true;
                                                    div_Query.Visible = false;
                                                    div_wf_Save.Visible = true;
                                                    div_Save.Visible = false;
                                                    imgOrg.Enabled = false;
                                                    ImgCurrency.Enabled = false;
                                                    div_wf_lng.Visible = true;
                                                    div_lng.Visible = false;
                                                    imgClear.Enabled = false;

                                                }
                                            }

                                        }
                                    }
                                }


                                //end for wf process

                                if (bol_incrI)
                                    i = i + 1;
                                if (i >= dt_menulist.Rows.Count)
                                {
                                    break;
                                }
                            }
                            //str_LinkQuery.Append("</div>");
                            if (str_subMenuData.Length > 0)
                            {
                                str_LinkQuery.Append(str_subMenuData);
                                str_subMenuData = "";
                            }

                        }

                        //str_LinkQuery.Append("</table>");

                        div_Menu.InnerHtml = str_LinkQuery.ToString();

                        FIN.Client.ClsGridBase.DataMenuList = dt_menulist;

                        Session[FINSessionConstants.MenuData] = dt_menulist;

                        if (Request.QueryString["WF"] != null && Request.QueryString["Id"] != null && Session["str_LinkWFQuery"] != null)
                        {
                            if (ifrmaster != null)
                            {
                                ifrmaster.Attributes.Add("src", "" + Session["str_LinkWFQuery"]);
                                //ifrmaster.Attributes.Add("src", "/" + Session["str_LinkWFQuery"]);

                            }
                        }
                        else
                        {
                            ifrmaster.Attributes.Add("src", "../" + Session[FINSessionConstants.ModuleName] + "/DashBoard_" + Session[FINSessionConstants.ModuleName] + ".aspx");

                          
                        }
                    }
                    else
                    {
                        Response.Redirect(System.Configuration.ConfigurationManager.AppSettings["LoginPage"].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("workflow", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    // Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }


        private void FillComboBox()
        {
            FIN.BLL.GL.Organisation_BLL.getOrganizationDet(ref ddlOrg);
            DataTable dt_data = DBMethod.ExecuteQuery(FIN.DAL.GL.Organisation_DAL.getOrganisationDetails()).Tables[0];
            if (dt_data.Rows.Count == 1)
            {
                ddlOrg.SelectedIndex = 1;
            }
        }
        protected void ChangeLanguage(object sender, EventArgs e)
        {
            LinkButton lb_lang = (LinkButton)sender;
            Session[FINSessionConstants.Sel_Lng] = lb_lang.CommandArgument.ToString();

            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/Generic_" + Session[FINSessionConstants.Sel_Lng].ToString() + ".properties"));
            Session["Tree_Link"] = "<a href='" + System.Configuration.ConfigurationManager.AppSettings["ModulePage"].ToString() + "' style='Color:white'> " + Prop_File_Data["Home_P"] + "</a>" + " --> " + "<a href='../DashBoard.aspx' style='Color:white' target='centerfrm'> " + Session[FINSessionConstants.ModuleDescription] + " </a>";
            hf_TreeLink.Value = Session["Tree_Link"].ToString();

            if (Session[FINSessionConstants.Sel_Lng] == null || Session[FINSessionConstants.Sel_Lng] == "EN")
            {
                Session[FINSessionConstants.Sel_Lng] = "EN";
                VMVServices.Web.Utils.LanguageCode = "";
            }
            else
            {
                VMVServices.Web.Utils.LanguageCode = "_OL";
            }

        }

        protected void imgClear_Click(object sender, ImageClickEventArgs e)
        {
            if (Request.QueryString["WF"] == null && Request.QueryString["Id"] == null)
            {
                //string entryFrameSrc = ifrmaster.Attributes["src"].ToString();
                //Uri tmp_uri = new Uri(entryFrameSrc);
                //string form_id = HttpUtility.ParseQueryString(tmp_uri.Query).Get("ProgramID");
                string form_id = Session["ProgramID"].ToString();
                System.Data.DataTable dt_menulist = (System.Data.DataTable)Session[FIN.BLL.FINSessionConstants.MenuData];
                System.Data.DataRow[] dr_list = dt_menulist.Select("MENU_KEY_ID=" + form_id);


                string str_LinkURL = dr_list[0]["ENTRY_PAGE_OPEN"].ToString() + "?";
                str_LinkURL += "ProgramID=" + dr_list[0]["MENU_KEY_ID"].ToString() + "&";
                str_LinkURL += QueryStringTags.Mode.ToString() + "=" + ProgramMode.Add.ToString().Substring(0, 1) + "&";
                str_LinkURL += QueryStringTags.ID.ToString() + "=0" + "&";
                str_LinkURL += QueryStringTags.AddFlag.ToString() + "=" + dr_list[0]["INSERT_ALLOWED_FLAG"].ToString() + "&";
                str_LinkURL += QueryStringTags.UpdateFlag.ToString() + "=" + dr_list[0]["UPDATE_ALLOWED_FLAG"].ToString() + "&";
                str_LinkURL += QueryStringTags.DeleteFlag.ToString() + "=" + dr_list[0]["DELETE_ALLOWED_FLAG"].ToString() + "&";
                str_LinkURL += QueryStringTags.QueryFlag.ToString() + "=" + dr_list[0]["QUERY_ALLOWED_FLAG"].ToString() + "&";
                str_LinkURL += QueryStringTags.ReportName.ToString() + "=" + dr_list[0]["REPORT_NAME"].ToString() + "&";

                ifrmaster.Attributes.Add("src", str_LinkURL);
            }
        }

        protected void ddlOrg_SelectedIndexChanged(object sender, EventArgs e)
        {
            // mpeOrg.Show();
            VMVServices.Web.Utils.OrganizationID = ddlOrg.SelectedValue.ToString();
            VMVServices.Web.Utils.OrganizationName = ddlOrg.SelectedItem.Text;
            lblOrgName.Text = VMVServices.Web.Utils.OrganizationName;
            VMVServices.Web.Utils.DecimalPrecision = "3";
            VMVServices.Web.Utils.CommaSeparation = "3";

            DataTable dt = FIN.BLL.GL.Organisation_BLL.getCompanyCurrency(ddlOrg.SelectedValue.ToString());
            if (dt.Rows.Count > 0)
            {
                Session[FINSessionConstants.ORGCurrency] = dt.Rows[0][FINColumnConstants.CURRENCY_ID].ToString();
                Session[FINSessionConstants.ORGCurrencySymbol] = dt.Rows[0][FINColumnConstants.CURRENCY_SYMBOL].ToString();

            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (rblWF.SelectedValue.ToString() != "WFST01")
            {
                if (txtRemarks.Text.ToString().Trim().Length == 0)
                {
                    lblNeedRemarks.Visible = true;
                    mpeWF.Show();
                    return;
                }
            }
            if (Request.QueryString["LC"] != null && Request.QueryString["LC"] != string.Empty)
            {
                Session[FINSessionConstants.LevelCode] = Request.QueryString["LC"].ToString();
            }
            if (Session["FormCode"] != null && Session["Mode"] != null && Session["StrRecordId"] != null && Session[FINSessionConstants.LevelCode] != null)
            {
                if (Session["FormCode"].ToString().Length > 0 && Session["Mode"].ToString() == ProgramMode.WApprove.ToString().Substring(0, 1) && Session["StrRecordId"].ToString() != "0")
                {
                    string str_Ret_Status = FINSP.UpdateWorhflowstatus(Session["FormCode"].ToString(), rblWF.SelectedValue.ToString(), Session["UserName"].ToString(), Session["StrRecordId"].ToString(), txtRemarks.Text, Session[FINSessionConstants.LevelCode].ToString());
                    Session[FINSessionConstants.LevelCode] = null;

                    //    FINSP.GetSP_GL_Posting(Session["StrRecordId"].ToString(), Session["FormCode"].ToString());

                    Response.Redirect(System.Configuration.ConfigurationManager.AppSettings["ModulePage"].ToString());
                }
            }
        }

        protected void imgWF_Click(object sender, ImageClickEventArgs e)
        {
            if (Request.QueryString["LC"] != null && Request.QueryString["LC"] != string.Empty)
            {
                Session[FINSessionConstants.LevelCode] = Request.QueryString["LC"].ToString();
            }
            if (Session["FormCode"] != null && Session["Mode"] != null && Session["StrRecordId"] != null && Session[FINSessionConstants.LevelCode] != null)
            {
                mpeWF.Show();

                string form_id = Session["ProgramID"].ToString();
                System.Data.DataTable dt_menulist = (System.Data.DataTable)Session[FIN.BLL.FINSessionConstants.MenuData];
                System.Data.DataRow[] dr_list = dt_menulist.Select("MENU_KEY_ID=" + form_id);


                string str_LinkURL = dr_list[0]["ENTRY_PAGE_OPEN"].ToString() + "?";
                str_LinkURL += "ProgramID=" + dr_list[0]["MENU_KEY_ID"].ToString() + "&";
                str_LinkURL += QueryStringTags.Mode.ToString() + "=" + Session["Mode"].ToString() + "&";
                str_LinkURL += QueryStringTags.ID.ToString() + "=" + Session["StrRecordId"].ToString() + "&";
                str_LinkURL += QueryStringTags.AddFlag.ToString() + "=" + dr_list[0]["INSERT_ALLOWED_FLAG"].ToString() + "&";
                str_LinkURL += QueryStringTags.UpdateFlag.ToString() + "=" + dr_list[0]["UPDATE_ALLOWED_FLAG"].ToString() + "&";
                str_LinkURL += QueryStringTags.DeleteFlag.ToString() + "=" + dr_list[0]["DELETE_ALLOWED_FLAG"].ToString() + "&";
                str_LinkURL += QueryStringTags.QueryFlag.ToString() + "=" + dr_list[0]["QUERY_ALLOWED_FLAG"].ToString() + "&";
                str_LinkURL += QueryStringTags.ReportName.ToString() + "=" + dr_list[0]["REPORT_NAME"].ToString() + "&";

                ifrmaster.Attributes.Add("src", str_LinkURL);
            }
        }

        protected void imgbtnABR_Click(object sender, ImageClickEventArgs e)
        {
            if (Session["FormCode"] != null && Session["Mode"] != null && Session["StrRecordId"] != null)
            {
                if (Session["FormCode"].ToString().Length > 0 && Session["StrRecordId"].ToString() != "0")
                {
                    mpeABR.Show();

                    string[] str_abtRec = new string[7];
                    str_abtRec = FINSP.GetSPFOR_AboutThisRec(Session["FormCode"].ToString(), Session["StrRecordId"].ToString());

                    string str_Det = "<Table>";
                    str_Det += "<tr><td>1.</td><td>" + str_abtRec[0] + "</td></tr>";
                    str_Det += "<tr><td>2.</td><td>" + str_abtRec[1] + "</td></tr>";
                    str_Det += "<tr><td>3.</td><td>" + str_abtRec[2] + "</td></tr>";
                    str_Det += "<tr><td>4.</td><td>" + str_abtRec[3] + "</td></tr>";
                    str_Det += "<tr><td>5.</td><td>" + str_abtRec[4] + "</td></tr>";
                    str_Det += "<tr><td>6.</td><td>" + str_abtRec[5] + "</td></tr>";
                    str_Det += "<tr><td>7.</td><td>" + str_abtRec[6] + "</td></tr>";
                    str_Det += "</Table>";

                    div_ABTREC.InnerHtml = str_Det;

                }
                if (Session["FormCode"].ToString().Length > 0)
                {
                    string form_id = Session["ProgramID"].ToString();
                    System.Data.DataTable dt_menulist = (System.Data.DataTable)Session[FIN.BLL.FINSessionConstants.MenuData];
                    System.Data.DataRow[] dr_list = dt_menulist.Select("MENU_KEY_ID=" + form_id);


                    string str_LinkURL = dr_list[0]["ENTRY_PAGE_OPEN"].ToString() + "?";
                    str_LinkURL += "ProgramID=" + dr_list[0]["MENU_KEY_ID"].ToString() + "&";
                    str_LinkURL += QueryStringTags.Mode.ToString() + "=" + Session["Mode"].ToString() + "&";
                    str_LinkURL += QueryStringTags.ID.ToString() + "=" + Session["StrRecordId"].ToString() + "&";
                    str_LinkURL += QueryStringTags.AddFlag.ToString() + "=" + dr_list[0]["INSERT_ALLOWED_FLAG"].ToString() + "&";
                    str_LinkURL += QueryStringTags.UpdateFlag.ToString() + "=" + dr_list[0]["UPDATE_ALLOWED_FLAG"].ToString() + "&";
                    str_LinkURL += QueryStringTags.DeleteFlag.ToString() + "=" + dr_list[0]["DELETE_ALLOWED_FLAG"].ToString() + "&";
                    str_LinkURL += QueryStringTags.QueryFlag.ToString() + "=" + dr_list[0]["QUERY_ALLOWED_FLAG"].ToString() + "&";
                    str_LinkURL += QueryStringTags.ReportName.ToString() + "=" + dr_list[0]["REPORT_NAME"].ToString() + "&";

                    ifrmaster.Attributes.Add("src", str_LinkURL);
                }
            }
        }

        //for export into excel query datas

        protected void ImgCurrency_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                if (Session[FINSessionConstants.GridData] != null)
                {
                    Response.Redirect("../ExportGridData.aspx?ExportId=EXCEL", true);
                }
                else
                {
                    ErrorCollection.Add("PleaseClickView", "No Data Found To Export");
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ExportClick", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {

                }
            }

        }


        protected void btnNotFoundLang_Click(object sender, EventArgs e)
        {
            string path = @"E:\NotFoundLang.txt";
            System.IO.File.Delete(path);
            using (System.IO.StreamWriter sw = System.IO.File.AppendText(path))
            {
                sw.WriteLine(hf_NotFoundLang.Value);
            }
        }

        protected void btnRelogin_Click(object sender, EventArgs e)
        {
            MPEReLogin.Show();
        }


        protected void btnYes_Click(object sender, EventArgs e)
        {

            SSM_USERS sSM_USERS = new SSM_USERS();
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/Generic_" + Session["Sel_Lng"].ToString() + ".properties"));

            try
            {
                ErrorCollection.Clear();

                lblError.Text = "";
                lblError.Visible = true;
                sSM_USERS = null;
                using (IRepository<SSM_USERS> userCtx = new DataRepository<SSM_USERS>())
                {
                    sSM_USERS = userCtx.Find(r =>
                        (r.USER_CODE.ToUpper() == txtUserID.Text.ToUpper() && r.ATTRIBUTE1 != "EMPLOYEE")
                        ).SingleOrDefault();
                }
                if (sSM_USERS != null)
                {
                    if (sSM_USERS.PASSWORD_LOCK == FINAppConstants.Y)
                    {
                        MPEReLogin.Show();
                        ErrorCollection.Add(Prop_File_Data["InvalidUser_P"], Prop_File_Data["PasswordLocked_P"]);
                        lblError.Text = Prop_File_Data["PasswordLocked_P"];
                        return;
                    }

                    if (sSM_USERS.USER_PASSWORD.ToString() == EnCryptDecrypt.CryptorEngine.Encrypt(txtPassword.Text, true))
                    {
                        Session["userID"] = int.Parse(sSM_USERS.PK_ID.ToString());
                        Session["UserName"] = sSM_USERS.USER_CODE;
                        Session[FINSessionConstants.PassWord] = sSM_USERS.USER_PASSWORD;
                        //Session[FINSessionConstants.UserGroupID] = sSM_USERS.GROUP_ID;
                        Session[FINSessionConstants.UserName] = sSM_USERS.USER_CODE;
                        Session["FullUserName"] = sSM_USERS.FIRST_NAME + " " + sSM_USERS.MIDDLE_NAME + " " + sSM_USERS.LAST_NAME;

                        VMVServices.Web.Utils.UserName = sSM_USERS.USER_CODE;
                        VMVServices.Web.Utils.Multilanguage = false;
                        hf_bol_IMStart.Value = "TRUE";
                        hf_IdelMinutes.Value = "0";
                        HttpContext.Current.Session["OrganizationID"] = hf_S_OrganizationID.Value;
                        HttpContext.Current.Session["Multilanguage"] = hf_S_Multilanguage.Value;
                        Session[FINSessionConstants.ModuleName] = hf_S_ModuleName.Value;
                        Session[FINSessionConstants.ModuleDescription] = hf_S_ModuleDesc.Value;

                    }
                    else
                    {
                        MPEReLogin.Show();
                        hf_pwdtryCount.Value = (int.Parse(hf_pwdtryCount.Value) + 1).ToString();
                        ErrorCollection.Add(Prop_File_Data["InvalidUser_P"], Prop_File_Data["InvalidPassword_P"]);
                        lblError.Text = Prop_File_Data["InvalidPassword_P"];
                        if (int.Parse(hf_pwdtryCount.Value) > 3)
                        {
                            sSM_USERS.PASSWORD_LOCK = FINAppConstants.Y;
                            DBMethod.SaveEntity<SSM_USERS>(sSM_USERS, true);
                        }
                    }
                }
                else
                {
                    ErrorCollection.Add(Prop_File_Data["InvalidUser_P"], Prop_File_Data["InvalidUserName_P"]);
                    lblError.Text = Prop_File_Data["InvalidUserName_P"];
                    MPEReLogin.Show();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Remove(Prop_File_Data["LoginError_P"]);
                ErrorCollection.Add(Prop_File_Data["LoginError_P"], ex.Message);
                lblError.Text = ex.Message;
                MPEReLogin.Show();
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('validation.aspx','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

    }

}