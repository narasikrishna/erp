﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using FIN.BLL;
using FIN.DAL;
using System.Data;
using VMVServices.Web;
namespace FIN.Client.MainPage
{
    public partial class MainPage : PageBase
    {
        DataSet dsMenuList = new DataSet();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string str_lastLoginTime = "";
                DataTable dt_menulist = new DataTable();

                if (Session[FINSessionConstants.UserName] != null && Session[FINSessionConstants.PassWord] != null)
                {
                    dt_menulist = DBMethod.ExecuteQuery(FINSQL.GetMenuList(Session[FINSessionConstants.UserName].ToString(),Session[FINSessionConstants.ModuleName].ToString(),Session[FINSessionConstants.Sel_Lng].ToString())).Tables[0];

                    System.Text.StringBuilder str_LinkQuery = new System.Text.StringBuilder();

                    string str_linkhead = "";

                    str_LinkQuery.Append("<div id='firstpane' class='menu_list' >");

                    for (int i = 0; i < dt_menulist.Rows.Count; i++)
                    {
                        if (dt_menulist.Rows[i]["ENABLED_FLAG"].ToString() == "1")
                        {
                            //string str_LinkURL = "<li><a href='../" + dt_menulist.Rows[i]["MENU_URL"].ToString() + "?ProgramID=" + dt_menulist.Rows[i]["MENU_KEY_ID"].ToString() + "&" + QueryStringTags.Mode.ToString() + "=" + ProgramMode.Add.ToString().Substring(0, 1) + "&" + QueryStringTags.ID.ToString() + "=0" + "'  target='centerfrm'><span>" + dt_menulist.Rows[i]["MENU_DESCRIPTION"].ToString() + "</span></a></li>";
                            string str_LinkURL = "<li><a href='" + dt_menulist.Rows[i]["ENTRY_PAGE_OPEN"].ToString() + "?";
                            str_LinkURL += "ProgramID=" + dt_menulist.Rows[i]["MENU_KEY_ID"].ToString() + "&";
                            str_LinkURL += QueryStringTags.Mode.ToString() + "=" + ProgramMode.Add.ToString().Substring(0, 1) + "&";
                            str_LinkURL += QueryStringTags.ID.ToString() + "=0" + "&";
                            str_LinkURL += QueryStringTags.AddFlag.ToString() + "=" + dt_menulist.Rows[i]["INSERT_ALLOWED_FLAG"].ToString() + "&";
                            str_LinkURL += QueryStringTags.UpdateFlag.ToString() + "=" + dt_menulist.Rows[i]["UPDATE_ALLOWED_FLAG"].ToString() + "&";
                            str_LinkURL += QueryStringTags.DeleteFlag.ToString() + "=" + dt_menulist.Rows[i]["DELETE_ALLOWED_FLAG"].ToString() + "&";
                            str_LinkURL += QueryStringTags.QueryFlag.ToString() + "=" + dt_menulist.Rows[i]["QUERY_ALLOWED_FLAG"].ToString() + "&";
                            str_LinkURL += QueryStringTags.ReportName.ToString() + "=" + dt_menulist.Rows[i]["REPORT_NAME"].ToString() + "'";
                            str_LinkURL += "  target='centerfrm'><span>" + dt_menulist.Rows[i]["SCREEN_NAME"].ToString() + "</span></a></li>";

                            if (str_linkhead == dt_menulist.Rows[i]["MODULENAME"].ToString().Trim())
                            {
                                str_LinkQuery.Append(str_LinkURL);

                            }
                            else
                            {
                                if (str_linkhead.Length != 0)
                                {
                                    str_LinkQuery.Append("</ul>");
                                    str_LinkQuery.Append("</div>");
                                }

                                str_LinkQuery.Append(" <p class='menu_head'>" + dt_menulist.Rows[i]["MODULENAME"].ToString() + "</p>");
                                str_LinkQuery.Append("<div class='menu_body'><ul>");

                                // str_LinkQuery.Append("<li><a href='../" + dt_menulist.Rows[i]["MENU_URL"].ToString() + "?ProgramID=" + dt_menulist.Rows[i]["MENU_KEY_ID"].ToString() + "' target='centerfrm'><span>" + dt_menulist.Rows[i]["MENU_DESCRIPTION"].ToString() + "</span></a></li>");
                                str_LinkQuery.Append(str_LinkURL);

                                str_linkhead = dt_menulist.Rows[i]["MODULENAME"].ToString();
                            }
                        }

                    }

                    str_LinkQuery.Append("</ul>");
                    str_LinkQuery.Append("</div>");
                    str_LinkQuery.Append("</div>");

                    div_Menu.InnerHtml = str_LinkQuery.ToString();

                    FIN.Client.ClsGridBase.DataMenuList = dt_menulist;

                    Session[FINSessionConstants.MenuData] = dt_menulist;
                }


                //using (IRepository<LOGIN_HISTORY> userctx = new DataRepository<LOGIN_HISTORY>())
                //{
                //    str_lastLoginTime = userctx.GetAll().Max(x => x.LOGIN_DATE).ToString();
                //}

                //if (Session["website_hit_id"] != null)
                //{
                //    LOGIN_HISTORY LOG_HISTORY = new LOGIN_HISTORY();
                //    LOG_HISTORY.HIT_ID = Convert.ToInt16(Session["website_hit_id"].ToString());
                //    LOG_HISTORY.USERID = Convert.ToInt32(this.LoggedOnUserID);
                //    LOG_HISTORY.LOGIN_DATE = DateTime.Now;
                //    LOG_HISTORY.LOGIN_TIME = DateTime.Now;
                //    DBMethod.SaveEntity<LOGIN_HISTORY>(LOG_HISTORY);
                //}
                AssignToControl();
                // lbluserName.Text = this.LoggedUserName;
                //  string str_currentlogintime = DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss");
                // lblCurrentLogin.Text = "Login Time : " + str_currentlogintime;
                //if (str_lastLoginTime.Length > 0)
                //{//
                //  //  lblLastLogin.Text = "Last Login Time : " + UserUtility.fn_convertdatetime(str_lastLoginTime);
                //}
                //else
                //{
                //    //lblLastLogin.Text = "Last Login Time : " + str_currentlogintime;
                //}
            }
        }

        public void AssignToControl()
        {
            //if (Session[FINSessionConstants.UserID] != null && Session[FINSessionConstants.UserGroupID] != null)
            //{

            //    string str_menuhead = "";
            //    System.Text.StringBuilder str_MenuDesign = new System.Text.StringBuilder();
            //    dsMenuList = DBMethod.ExecuteQuery(FINSQL.GetMenuList(long.Parse(Session[FINSessionConstants.UserGroupID].ToString())));
            //    if (dsMenuList.Tables[0].Rows.Count > 0)
            //    {
            //        str_MenuDesign.Append("<ul id='navigation-1'>");
            //        for (int i = 0; i < dsMenuList.Tables[0].Rows.Count; i++)
            //        {

            //            if (dsMenuList.Tables[0].Rows[i]["ENABLED_FLAG"].ToString() == "Y")
            //            {
            //                if (str_menuhead == dsMenuList.Tables[0].Rows[i]["Process_Name"].ToString().Trim())
            //                {
            //                    str_MenuDesign.Append("<li><a href='" + dsMenuList.Tables[0].Rows[i]["LIST_URL"].ToString() + "?ProgramID=" + dsMenuList.Tables[0].Rows[i]["sub_process_id"].ToString() + "' target='centerfrm'>" + dsMenuList.Tables[0].Rows[i]["sub_Process_Name"].ToString() + "</a></li>");

            //                }
            //                else
            //                {
            //                    if (str_menuhead.Length != 0)
            //                    {
            //                        str_MenuDesign.Append("</ul>");
            //                        str_MenuDesign.Append("</li>");
            //                    }

            //                    str_MenuDesign.Append("<li><a href='#' >" + dsMenuList.Tables[0].Rows[i]["Process_Name"].ToString() + "</a>");
            //                    str_MenuDesign.Append("<ul class='navigation-2'>");

            //                    str_MenuDesign.Append("<li><a href='" + dsMenuList.Tables[0].Rows[i]["List_URL"].ToString() + "?ProgramID=" + dsMenuList.Tables[0].Rows[i]["sub_process_id"].ToString() + "' target='centerfrm'>" + dsMenuList.Tables[0].Rows[i]["sub_Process_Name"].ToString() + "</a></li>");

            //                    str_menuhead = dsMenuList.Tables[0].Rows[i]["Process_Name"].ToString();
            //                }
            //            }

            //        }

            //    }
            //    str_MenuDesign.Append("</ul>");
            //    str_MenuDesign.Append("</li>");
            //    str_MenuDesign.Append("</ul>");
            //    divmenu.InnerHtml = str_MenuDesign.ToString();

            //    Session[FINSessionConstants.MenuData] = dsMenuList;



            //}


        }



        protected void imgbutlogoff_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("../Login.aspx");
        }

        protected void ibQuery_Click(object sender, ImageClickEventArgs e)
        {
            if (Session["ProgramID"] != null)
            {
                DataTable dt_menulist = new DataTable();
                dt_menulist = (DataTable)Session[FINSessionConstants.MenuData];
                DataRow[] dr = dt_menulist.Select("MENU_KEY_ID=" + Session["ProgramID"].ToString());
                if (dr.Length > 0)
                {
                     
                    
                    //this.Searchfrm.Attributes["src"] = "../QueryForm/QueryForm.aspx";
                    //if (dr[0]["MODULECODE"].ToString() == FIN.BLL.FINListConstants.GL)
                    //{
                    //    FN_GLListScreen(dr[0]["SCREEN_CODE"].ToString());
                    //}
                    
                }

            }
        }
       
    }
}