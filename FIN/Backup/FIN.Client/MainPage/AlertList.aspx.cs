﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using VMVServices.Web;
using FIN.DAL;
using FIN.BLL;

namespace FIN.Client.MainPage
{
    public partial class AlertList : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                AssignToAlert();
            }
        }



        private void AssignToAlert()
        {
            try
            {
                ErrorCollection.Clear();

                DataTable dtAlert = DBMethod.ExecuteQuery(FINSQL.GetAlertUserLevel(VMVServices.Web.Utils.UserName)).Tables[0];
                if (dtAlert != null)
                {

                    Session["dtAlert"] = dtAlert;
                    gvAlert.DataSource = dtAlert;
                    gvAlert.DataBind();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("alkert", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    // Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }




        protected void gvAlert_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            LinkButton lnk_Alert = (LinkButton)e.Row.FindControl("lnkAlert");

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.RowIndex >= 0)
                {
                    string navigationMode = "ALERT";

                    lnk_Alert.PostBackUrl = gvAlert.DataKeys[e.Row.RowIndex].Values["menu_url"].ToString() + "&" + QueryStringTags.AddFlag + "=0&" + "&" + QueryStringTags.DeleteFlag + "=0&" + QueryStringTags.UpdateFlag + "=0&" + QueryStringTags.ReportName + "=''&" + QueryStringTags.Mode + "=" + ProgramMode.WApprove.ToString().Substring(0, 1) + "&WF=" + gvAlert.DataKeys[e.Row.RowIndex].Values["screen_code"].ToString() + "&Id=" + gvAlert.DataKeys[e.Row.RowIndex].Values["transaction_code"].ToString() + "&MC=" + gvAlert.DataKeys[e.Row.RowIndex].Values["module_code"].ToString() + "&LC=0&MD=" + gvAlert.DataKeys[e.Row.RowIndex].Values["module_description"].ToString() + "&ORG=" + gvAlert.DataKeys[e.Row.RowIndex].Values["org_id"].ToString() + "&ORGD=" + gvAlert.DataKeys[e.Row.RowIndex].Values["org_desc"].ToString() + "&NAV_MODE=" + navigationMode;

                }
            }
        }

        protected void gvAlert_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                gvAlert.PageIndex = e.NewPageIndex;
                gvAlert.DataSource = (DataTable)Session["dtAlert"];
                gvAlert.DataBind();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Alert", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    //Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
    }
}