﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.BLL;

using FIN.DAL;
using System.Collections;
using VMVServices.Web;


namespace FIN.Client.MainPage
{
    public partial class ModuleMasterFixed : PageBase
    {

        DataTable dtWorkflow = new DataTable();
        DataTable dtAlert = new DataTable();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                FillComboBox();
                Session["Tree_Link"] = "";

                AssignToWorkflow();
                AssignToAlert();
                Session[FINSessionConstants.ORGCurrency] = "";
                divModuleSel.Attributes.Add("Disabled", "");
                VMVServices.Web.Utils.SavedRecordId = "";

            }
        }

        private void FillComboBox()
        {
            FIN.BLL.GL.Organisation_BLL.getOrganizationDet(ref ddlOrg);
        }

        private void AssignToWorkflow()
        {
            try
            {
                ErrorCollection.Clear();
                dtWorkflow = DBMethod.ExecuteQuery(FINSQL.GetWorkFlowMonitor(VMVServices.Web.Utils.UserName)).Tables[0];
                if (dtWorkflow != null)
                {
                    if (dtWorkflow.Rows.Count > 0)
                    {
                        //  Menu_BLL.GetMenuDetail();
                        gvWorkFlow.DataSource = dtWorkflow;
                        gvWorkFlow.DataBind();
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("workflow", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    // Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void AssignToAlert()
        {
            try
            {
                ErrorCollection.Clear();

                dtAlert = DBMethod.ExecuteQuery(FINSQL.GetAlertUserLevel(VMVServices.Web.Utils.UserName)).Tables[0];
                if (dtAlert != null)
                {
                    if (dtAlert.Rows.Count > 0)
                    {
                        gvAlert.DataSource = dtAlert;
                        gvAlert.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("alkert", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    // Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        protected void imgbtnAP_Click(object sender, ImageClickEventArgs e)
        {
            Session[FINSessionConstants.ModuleName] = FINListConstants_BLL.AP;
            Response.Redirect("MainPageFixed.aspx");
        }

        protected void imgbtnHR_Click(object sender, ImageClickEventArgs e)
        {
            Session[FINSessionConstants.ModuleName] = FINListConstants_BLL.HR;
            Response.Redirect("MainPageFixed.aspx");
        }

        protected void btnSSM_Click(object sender, EventArgs e)
        {
            Session[FINSessionConstants.ModuleName] = FINListConstants_BLL.SSM;

            Response.Redirect("MainPageFixed.aspx");
        }

        protected void imgbtnGL_Click(object sender, ImageClickEventArgs e)
        {
            Session[FINSessionConstants.ModuleName] = FINListConstants_BLL.GL;

            Response.Redirect("MainPageFixed.aspx");
        }

        protected void imgbtnCA_Click(object sender, ImageClickEventArgs e)
        {
            Session[FINSessionConstants.ModuleName] = FINListConstants_BLL.CA;

            Response.Redirect("MainPageFixed.aspx");
        }
        protected void imgbtnPA_Click(object sender, ImageClickEventArgs e)
        {
            Session[FINSessionConstants.ModuleName] = FINListConstants_BLL.PA;

            Response.Redirect("MainPageFixed.aspx");
        }
        protected void imgbtnFA_Click(object sender, ImageClickEventArgs e)
        {
            Session[FINSessionConstants.ModuleName] = FINListConstants_BLL.FA;

            Response.Redirect("MainPageFixed.aspx");
        }
        #region "Paging"
        /// <summary>
        /// Set the paging into Gridview
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvWorkFlow_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                gvWorkFlow.PageIndex = e.NewPageIndex;
                gvWorkFlow.Columns.Clear();
                //  gvWorkFlow = Master.SetupGridnew(gvWorkFlow, Session[ViewStateParam.gridData.ToString()] as DataTable, Master.KeyName);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("workflow", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    // Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        protected void gvAlert_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                gvAlert.PageIndex = e.NewPageIndex;
                gvAlert.Columns.Clear();
                //  gvWorkFlow = Master.SetupGridnew(gvWorkFlow, Session[ViewStateParam.gridData.ToString()] as DataTable, Master.KeyName);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Alert", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    // Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        #endregion

        protected void gvWorkFlow_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            HyperLink hFld = (HyperLink)e.Row.FindControl("hyperMsgID");
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.RowIndex >= 0)
                {
                    Session[FINSessionConstants.ModuleName] = gvWorkFlow.DataKeys[e.Row.RowIndex].Values["module_code"].ToString();
                    //hFld.NavigateUrl = "MainPageNew.aspx?WF=" + gvWorkFlow.DataKeys[e.Row.RowIndex].Values["menu_url"].ToString() + "&Id=" + gvWorkFlow.DataKeys[e.Row.RowIndex].Values["transaction_code"].ToString();
                    hFld.NavigateUrl = "MainPageFixed.aspx?WF=" + gvWorkFlow.DataKeys[e.Row.RowIndex].Values["workflow_code"].ToString() + "&Id=" + gvWorkFlow.DataKeys[e.Row.RowIndex].Values["transaction_code"].ToString();
                    // hFld.Attributes.Add("href", "javascript: void(0)");

                }
            }
        }

        protected void gvAlert_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            // ((HyperLink)e.Row.Cells[0].Controls[0]).NavigateUrl += "&missing_info=account";
        }
        protected void ddlOrg_SelectedIndexChanged(object sender, EventArgs e)
        {
            divModuleSel.Attributes.Remove("Disabled");
            VMVServices.Web.Utils.OrganizationID = ddlOrg.SelectedValue.ToString();
            VMVServices.Web.Utils.OrganizationName = ddlOrg.SelectedItem.Text;

            DataTable dt = FIN.BLL.GL.Organisation_BLL.getCompanyCurrency(ddlOrg.SelectedValue.ToString());
            if (dt.Rows.Count > 0)
            {
                Session[FINSessionConstants.ORGCurrency] = dt.Rows[0][FINColumnConstants.CURRENCY_ID].ToString();
            }
        }
    }
}