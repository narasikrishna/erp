﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using FIN.BLL;
namespace FIN.Client.MainPage
{
    public partial class ModuleMasterNew : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session["Tree_Link"] = "";
                VMVServices.Web.Utils.SavedRecordId = "";
                
            }
        }
                
        protected void imgbtnAP_Click(object sender, ImageClickEventArgs e)
        {
            Session[FINSessionConstants.ModuleName] = FINListConstants_BLL.AP;
            Response.Redirect("MainPageFixed.aspx");
        }

        protected void imgbtnHR_Click(object sender, ImageClickEventArgs e)
        {
            Session[FINSessionConstants.ModuleName] = FINListConstants_BLL.HR;
            Response.Redirect("MainPageFixed.aspx");
        }

        protected void btnSSM_Click(object sender, EventArgs e)
        {
            Session[FINSessionConstants.ModuleName] = FINListConstants_BLL.SSM;

            Response.Redirect("MainPageFixed.aspx");
        }

        protected void imgbtnGL_Click(object sender, ImageClickEventArgs e)
        {
            Session[FINSessionConstants.ModuleName] = FINListConstants_BLL.GL;

            Response.Redirect("MainPageFixed.aspx");
        }

        protected void imgbtnCA_Click(object sender, ImageClickEventArgs e)
        {
            Session[FINSessionConstants.ModuleName] = FINListConstants_BLL.CA;

            Response.Redirect("MainPageFixed.aspx");
        }
       
    }
}