﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using FIN.BLL;


namespace FIN.Client.MainPage
{
    public partial class MenuModule : System.Web.UI.Page
    {
        System.Collections.SortedList ErrorCollection = new System.Collections.SortedList();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadMenuItems();
            }
        }
        private void LoadMenuItems()
        {
            Session[FINSessionConstants.UserName] = "ADMIN";
            DataTable dt_menulist = FIN.DAL.DBMethod.ExecuteQuery(FIN.DAL.FINSQL.GetMenuListFull(Session[FINSessionConstants.UserName].ToString())).Tables[0];

            DataTable dt_copy = dt_menulist.Copy();
            DataTable dt_ModuleList = dt_menulist.DefaultView.ToTable(true, "MODULECODE");
            string str_ModuleMenu = "";
            string str_SubMenu = "";
            string str_topSubMenu = "";
            int Menu_Per_Row = 4;
            int int_Count = 0;
            Session[FINSessionConstants.MenuData] = dt_menulist;
            for (int iLoop = 0; iLoop < dt_ModuleList.Rows.Count; iLoop++)
            {



                DataTable dt_tmp_ParentMenu = new DataTable();
                dt_tmp_ParentMenu.Columns.Add("PARENT_MENU");
                dt_tmp_ParentMenu.Columns.Add("PARENT_MENU_CODE");
                dt_tmp_ParentMenu.Columns.Add("MENU_LIST_ORDER");
                dt_tmp_ParentMenu.Columns.Add("PARENT_MENU_IMG_URL");
                

                DataTable dt_ParentMenu = dt_menulist.AsEnumerable()
                    .Where(r => r["MODULECODE"].ToString().ToUpper().Contains(dt_ModuleList.Rows[iLoop][0].ToString().ToUpper()) && r["PARENT_MENU_CODE"] != null)
                    .Select(r =>
                    {
                        DataRow newRow = dt_tmp_ParentMenu.NewRow();
                        newRow["PARENT_MENU"] = r["PARENT_MENU"];
                        newRow["PARENT_MENU_CODE"] = r["PARENT_MENU_CODE"];
                        newRow["MENU_LIST_ORDER"] = r["MENU_LIST_ORDER"];
                        newRow["PARENT_MENU_IMG_URL"] = r["PARENT_MENU_IMG_URL"];
                        return newRow;

                    }
                    ).OrderBy(r => r["MENU_LIST_ORDER"]).Distinct(DataRowComparer.Default).CopyToDataTable();



                str_ModuleMenu += "<div id='div_" + dt_ModuleList.Rows[iLoop][0].ToString() + "' style='display:none' >";
                str_ModuleMenu += "<table cellpadding='10'>";
                for (int kLoop = 0; kLoop < dt_ParentMenu.Rows.Count; kLoop++)
                {
                    DataTable dt_SubMenu = new DataTable();
                    var tmp_menulist = dt_copy.AsEnumerable()
                        .Where(r => r["PARENT_MENU_CODE"].ToString().Contains(dt_ParentMenu.Rows[kLoop]["PARENT_MENU_CODE"].ToString()) && r["MODULECODE"].ToString() == dt_ModuleList.Rows[iLoop][0].ToString())
                        .OrderBy(r => r["ORDER_NO"]);
                    if (tmp_menulist.Any())
                    {
                        dt_SubMenu = tmp_menulist.CopyToDataTable();
                    }

                    string str_tmpid = "div_" + dt_ModuleList.Rows[iLoop][0].ToString() + "_" + dt_ParentMenu.Rows[kLoop]["PARENT_MENU_CODE"].ToString();
                    str_ModuleMenu += "<tr>";
                    str_ModuleMenu += "<td>";
                    if (dt_ParentMenu.Rows[kLoop]["PARENT_MENU_IMG_URL"].ToString().Trim().Length > 0)
                    {
                        str_ModuleMenu += " <div style='float:left; width:30px'><img src='" + dt_ParentMenu.Rows[kLoop]["PARENT_MENU_IMG_URL"].ToString() + "' width='20px' height='20px' /></div> ";
                    }
                    else
                    {
                        str_ModuleMenu += " <div style='float:left; width:30px'><img src='/Images/MENUICONS/MNEU.png' width='20px' height='20px' /></div> ";
                    }
                    str_ModuleMenu += "<div style='float:left' onClick=\"fn_ShowSubMenu('" + str_tmpid + "','" + dt_ParentMenu.Rows[kLoop]["PARENT_MENU"].ToString() +"')\">" + dt_ParentMenu.Rows[kLoop]["PARENT_MENU"].ToString() + "</div>";
                    str_ModuleMenu += "</td>";
                    str_ModuleMenu += "</tr>";

                    str_SubMenu += "<div id='" + str_tmpid + "' style='display:none' >";
                    str_topSubMenu += "<div id='TOP_" + str_tmpid + "' style='display:none' >";

                    str_SubMenu += "<table border='1' style='border:1px solid black' cellpadding='10'><tr>";
                    str_topSubMenu += "<table style='border:1px solid black' cellpadding='10'><tr>";
                    int_Count = 0;
                    for (int rLoop = 0; rLoop < dt_SubMenu.Rows.Count; rLoop++)
                    {

                        if (int_Count == Menu_Per_Row)
                        {
                            str_SubMenu += "</tr>"; str_topSubMenu += "</tr>";
                            int_Count = 0;
                            str_SubMenu += "<tr>"; str_topSubMenu += "<tr>";
                        }
                        if (dt_SubMenu.Rows[rLoop]["ACCESS_FLAG"].ToString() == "0")
                        {
                            str_SubMenu += "<td align='Center' class='menuNoAccess' style='width :200px'>";
                            str_SubMenu += "<div><img src='../Images/MMPercentage/GL_OUT.png' width='20px' height='20px' /></div>";
                            str_SubMenu += "<div style='Clear:both'></div>";
                            str_SubMenu += "<div>" + dt_SubMenu.Rows[rLoop]["SCREEN_NAME"].ToString() + "</div>";
                            str_SubMenu += "</td>";

                            str_topSubMenu += "<td align='Center' class='menuNoAccess' style='width :200px'>";
                            str_topSubMenu += "<div>" + dt_SubMenu.Rows[rLoop]["SCREEN_NAME"].ToString() + "</div>";
                            str_topSubMenu += "</td>";


                        }
                        else
                        {

                            string str_link = "  <a onClick=\"fn_hidemenu('" + dt_SubMenu.Rows[rLoop]["SCREEN_NAME"].ToString() + "')\"  href='" + dt_SubMenu.Rows[rLoop]["ENTRY_PAGE_OPEN"].ToString() + "?";
                            str_link += "ProgramID=" + dt_SubMenu.Rows[rLoop]["MENU_KEY_ID"].ToString() + "&";
                            str_link += QueryStringTags.Mode.ToString() + "=" + ProgramMode.Add.ToString().Substring(0, 1) + "&";
                            str_link += QueryStringTags.ID.ToString() + "=0" + "&";
                            str_link += QueryStringTags.AddFlag.ToString() + "=" + dt_SubMenu.Rows[rLoop]["INSERT_ALLOWED_FLAG"].ToString() + "&";
                            str_link += QueryStringTags.UpdateFlag.ToString() + "=" + dt_SubMenu.Rows[rLoop]["UPDATE_ALLOWED_FLAG"].ToString() + "&";
                            str_link += QueryStringTags.DeleteFlag.ToString() + "=" + dt_SubMenu.Rows[rLoop]["DELETE_ALLOWED_FLAG"].ToString() + "&";
                            str_link += QueryStringTags.QueryFlag.ToString() + "=" + dt_SubMenu.Rows[rLoop]["QUERY_ALLOWED_FLAG"].ToString() + "&";
                            str_link += QueryStringTags.ReportName.ToString() + "=" + dt_SubMenu.Rows[rLoop]["REPORT_NAME"].ToString() + "&";
                            str_link += QueryStringTags.ReportName_OL.ToString() + "=" + dt_SubMenu.Rows[rLoop]["ATTRIBUTE3"].ToString() + "'";
                            str_link += "  target='centerfrm' >" + dt_SubMenu.Rows[rLoop]["SCREEN_NAME"].ToString() +"</a>";

                            str_SubMenu += "<td align='Center' class='menuAccess' style='width :200px'>";
                            str_SubMenu += "<div><img src='" + dt_SubMenu.Rows[rLoop]["MENU_IMAGE_URL"].ToString() + "' width='20px' height='20px' /></div>";
                            str_SubMenu += "<div style='Clear:both'></div>";
                            str_SubMenu += "<div>" + str_link +"</div>";
                            str_SubMenu += "</td>";

                            str_topSubMenu += "<td align='Center' class='menuAccess' style='width :200px'>";
                            str_topSubMenu += "<div>" + str_link + "</div>";
                            str_topSubMenu += "</td>";
                        }

                        str_SubMenu += "<td align='Center' style='width :40px'>";
                        str_SubMenu += "<div><img src='../Images/MENUICONS/Forward_Arrow.png' width='20px' height='20px' /></div>";
                        str_SubMenu += "</td>";

                        int_Count += 1;


                    }
                    str_SubMenu += "</tr></table>";
                    str_SubMenu += "</div>";

                    str_topSubMenu += "</tr></table>";
                    str_topSubMenu += "</div>";
                }
                str_ModuleMenu += "</table>";
                str_ModuleMenu += "</div>";
            }
            div_MainMenu.InnerHtml = str_ModuleMenu;
            div_submenu.InnerHtml = str_SubMenu;
            div_top_subMenu.InnerHtml = str_topSubMenu;
        }
        protected void btnModuleCLick_Click(object sender, EventArgs e)
        {
        }

        protected void ChangeLanguage(object sender, EventArgs e)
        {
            LinkButton lb_lang = (LinkButton)sender;
            Session[FINSessionConstants.Sel_Lng] = lb_lang.CommandArgument.ToString();

            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/Generic_" + Session[FINSessionConstants.Sel_Lng].ToString() + ".properties"));
            Session["Tree_Link"] = "<a href='" + System.Configuration.ConfigurationManager.AppSettings["ModulePage"].ToString() + "' style='Color:white'> " + Prop_File_Data["Home_P"] + "</a>" + " --> " + "<a href='../DashBoard.aspx' style='Color:white' target='centerfrm'> " + Session[FINSessionConstants.ModuleDescription] + " </a>";
           //hf_TreeLink.Value = Session["Tree_Link"].ToString();

            if (Session[FINSessionConstants.Sel_Lng] == null || Session[FINSessionConstants.Sel_Lng] == "EN")
            {
                Session[FINSessionConstants.Sel_Lng] = "EN";
                VMVServices.Web.Utils.LanguageCode = "";
            }
            else
            {
                VMVServices.Web.Utils.LanguageCode = "_OL";
            }

        }

        protected void imgClear_Click(object sender, ImageClickEventArgs e)
        {
            if (Request.QueryString["WF"] == null && Request.QueryString["Id"] == null)
            {
                
                string form_id = Session["ProgramID"].ToString();
                System.Data.DataTable dt_menulist = (System.Data.DataTable)Session[FIN.BLL.FINSessionConstants.MenuData];
                System.Data.DataRow[] dr_list = dt_menulist.Select("MENU_KEY_ID=" + form_id);


                string str_LinkURL = dr_list[0]["ENTRY_PAGE_OPEN"].ToString() + "?";
                str_LinkURL += "ProgramID=" + dr_list[0]["MENU_KEY_ID"].ToString() + "&";
                str_LinkURL += QueryStringTags.Mode.ToString() + "=" + ProgramMode.Add.ToString().Substring(0, 1) + "&";
                str_LinkURL += QueryStringTags.ID.ToString() + "=0" + "&";
                str_LinkURL += QueryStringTags.AddFlag.ToString() + "=" + dr_list[0]["INSERT_ALLOWED_FLAG"].ToString() + "&";
                str_LinkURL += QueryStringTags.UpdateFlag.ToString() + "=" + dr_list[0]["UPDATE_ALLOWED_FLAG"].ToString() + "&";
                str_LinkURL += QueryStringTags.DeleteFlag.ToString() + "=" + dr_list[0]["DELETE_ALLOWED_FLAG"].ToString() + "&";
                str_LinkURL += QueryStringTags.QueryFlag.ToString() + "=" + dr_list[0]["QUERY_ALLOWED_FLAG"].ToString() + "&";
                str_LinkURL += QueryStringTags.ReportName.ToString() + "=" + dr_list[0]["REPORT_NAME"].ToString() + "&";

                ifrmaster.Attributes.Add("src", str_LinkURL);
            }
        }

        protected void ImgCurrency_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                if (Session[FINSessionConstants.GridData] != null)
                {
                    Response.Redirect("../ExportGridData.aspx?ExportId=EXCEL", true);
                }
                else
                {
                    ErrorCollection.Add("PleaseClickView", "No Data Found To Export");
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ExportClick", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {

                }
            }

        }

        protected void imgWF_Click(object sender, ImageClickEventArgs e)
        {
            if (Request.QueryString["LC"] != null && Request.QueryString["LC"] != string.Empty)
            {
                Session[FINSessionConstants.LevelCode] = Request.QueryString["LC"].ToString();
            }
            if (Session["FormCode"] != null && Session["Mode"] != null && Session["StrRecordId"] != null && Session[FINSessionConstants.LevelCode] != null)
            {
                mpeWF.Show();

                string form_id = Session["ProgramID"].ToString();
                System.Data.DataTable dt_menulist = (System.Data.DataTable)Session[FIN.BLL.FINSessionConstants.MenuData];
                System.Data.DataRow[] dr_list = dt_menulist.Select("MENU_KEY_ID=" + form_id);


                string str_LinkURL = dr_list[0]["ENTRY_PAGE_OPEN"].ToString() + "?";
                str_LinkURL += "ProgramID=" + dr_list[0]["MENU_KEY_ID"].ToString() + "&";
                str_LinkURL += QueryStringTags.Mode.ToString() + "=" + Session["Mode"].ToString() + "&";
                str_LinkURL += QueryStringTags.ID.ToString() + "=" + Session["StrRecordId"].ToString() + "&";
                str_LinkURL += QueryStringTags.AddFlag.ToString() + "=" + dr_list[0]["INSERT_ALLOWED_FLAG"].ToString() + "&";
                str_LinkURL += QueryStringTags.UpdateFlag.ToString() + "=" + dr_list[0]["UPDATE_ALLOWED_FLAG"].ToString() + "&";
                str_LinkURL += QueryStringTags.DeleteFlag.ToString() + "=" + dr_list[0]["DELETE_ALLOWED_FLAG"].ToString() + "&";
                str_LinkURL += QueryStringTags.QueryFlag.ToString() + "=" + dr_list[0]["QUERY_ALLOWED_FLAG"].ToString() + "&";
                str_LinkURL += QueryStringTags.ReportName.ToString() + "=" + dr_list[0]["REPORT_NAME"].ToString() + "&";

                ifrmaster.Attributes.Add("src", str_LinkURL);
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (rblWF.SelectedValue.ToString() != "WFST01")
            {
                if (txtRemarks.Text.ToString().Trim().Length == 0)
                {
                    lblNeedRemarks.Visible = true;
                    mpeWF.Show();
                    return;
                }
            }
            if (Request.QueryString["LC"] != null && Request.QueryString["LC"] != string.Empty)
            {
                Session[FINSessionConstants.LevelCode] = Request.QueryString["LC"].ToString();
            }
            if (Session["FormCode"] != null && Session["Mode"] != null && Session["StrRecordId"] != null && Session[FINSessionConstants.LevelCode] != null)
            {
                if (Session["FormCode"].ToString().Length > 0 && Session["Mode"].ToString() == ProgramMode.WApprove.ToString().Substring(0, 1) && Session["StrRecordId"].ToString() != "0")
                {
                    string str_Ret_Status =FIN.DAL.FINSP.UpdateWorhflowstatus(Session["FormCode"].ToString(), rblWF.SelectedValue.ToString(), Session["UserName"].ToString(), Session["StrRecordId"].ToString(), txtRemarks.Text, Session[FINSessionConstants.LevelCode].ToString());
                    Session[FINSessionConstants.LevelCode] = null;

                    //    FINSP.GetSP_GL_Posting(Session["StrRecordId"].ToString(), Session["FormCode"].ToString());

                    Response.Redirect(System.Configuration.ConfigurationManager.AppSettings["ModulePage"].ToString());
                }
            }
        }

        protected void imgbtnABR_Click(object sender, ImageClickEventArgs e)
        {
            if (Session["FormCode"] != null && Session["Mode"] != null && Session["StrRecordId"] != null)
            {
                if (Session["FormCode"].ToString().Length > 0 && Session["StrRecordId"].ToString() != "0")
                {
                    mpeABR.Show();

                    string[] str_abtRec = new string[7];
                    str_abtRec = FIN.DAL.FINSP.GetSPFOR_AboutThisRec(Session["FormCode"].ToString(), Session["StrRecordId"].ToString());

                    string str_Det = "<Table>";
                    str_Det += "<tr><td>1.</td><td>" + str_abtRec[0] + "</td></tr>";
                    str_Det += "<tr><td>2.</td><td>" + str_abtRec[1] + "</td></tr>";
                    str_Det += "<tr><td>3.</td><td>" + str_abtRec[2] + "</td></tr>";
                    str_Det += "<tr><td>4.</td><td>" + str_abtRec[3] + "</td></tr>";
                    str_Det += "<tr><td>5.</td><td>" + str_abtRec[4] + "</td></tr>";
                    str_Det += "<tr><td>6.</td><td>" + str_abtRec[5] + "</td></tr>";
                    str_Det += "<tr><td>7.</td><td>" + str_abtRec[6] + "</td></tr>";
                    str_Det += "</Table>";

                    div_ABTREC.InnerHtml = str_Det;

                }
                if (Session["FormCode"].ToString().Length > 0)
                {
                    string form_id = Session["ProgramID"].ToString();
                    System.Data.DataTable dt_menulist = (System.Data.DataTable)Session[FIN.BLL.FINSessionConstants.MenuData];
                    System.Data.DataRow[] dr_list = dt_menulist.Select("MENU_KEY_ID=" + form_id);


                    string str_LinkURL = dr_list[0]["ENTRY_PAGE_OPEN"].ToString() + "?";
                    str_LinkURL += "ProgramID=" + dr_list[0]["MENU_KEY_ID"].ToString() + "&";
                    str_LinkURL += QueryStringTags.Mode.ToString() + "=" + Session["Mode"].ToString() + "&";
                    str_LinkURL += QueryStringTags.ID.ToString() + "=" + Session["StrRecordId"].ToString() + "&";
                    str_LinkURL += QueryStringTags.AddFlag.ToString() + "=" + dr_list[0]["INSERT_ALLOWED_FLAG"].ToString() + "&";
                    str_LinkURL += QueryStringTags.UpdateFlag.ToString() + "=" + dr_list[0]["UPDATE_ALLOWED_FLAG"].ToString() + "&";
                    str_LinkURL += QueryStringTags.DeleteFlag.ToString() + "=" + dr_list[0]["DELETE_ALLOWED_FLAG"].ToString() + "&";
                    str_LinkURL += QueryStringTags.QueryFlag.ToString() + "=" + dr_list[0]["QUERY_ALLOWED_FLAG"].ToString() + "&";
                    str_LinkURL += QueryStringTags.ReportName.ToString() + "=" + dr_list[0]["REPORT_NAME"].ToString() + "&";

                    ifrmaster.Attributes.Add("src", str_LinkURL);
                }
            }
        }
    }
}