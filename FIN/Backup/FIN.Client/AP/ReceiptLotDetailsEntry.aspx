﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="ReceiptLotDetailsEntry.aspx.cs" Inherits="FIN.Client.AP.ReceiptLotDetailsEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 840px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblLotNumber">
                Lot Number
            </div>
            <div class="divtxtBox LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtLotNumber" CssClass="validate[required] RequiredField txtBox"
                    MaxLength="50" runat="server" TabIndex="1"></asp:TextBox>
            </div>
            <div class="colspace LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 200px" id="lblGRNNumber">
                Goods Received Note Number
            </div>
            <div class="divtxtBox LNOrient" style="width: 200px">
                <asp:DropDownList ID="ddlGRNNumber" CssClass="validate[required]  RequiredField ddlStype"
                    runat="server" TabIndex="2" AutoPostBack="True" OnSelectedIndexChanged="ddlGRNNumber_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblLineNumber">
                Line Number
            </div>
            <div class="divtxtBox LNOrient" style="width: 570px">
                <asp:DropDownList ID="ddlLineNumber" CssClass="validate[required]  RequiredField ddlStype"
                    runat="server" TabIndex="3" AutoPostBack="True" OnSelectedIndexChanged="ddlLineNumber_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblPONumber">
                Goods Received Note Date
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:TextBox ID="txtGRNDate" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]] RequiredField txtBox"
                    runat="server" TabIndex="4"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtGRNDate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
            <div class="colspace LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 200px" id="lblItemDescription">
                Purchase Order Number
            </div>
            <div class="divtxtBox LNOrient" style="width: 198px">
                <asp:TextBox ID="txtPONumber" CssClass="txtBox" runat="server" TabIndex="5"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="Div3">
                Item Description
            </div>
            <div class="divtxtBox LNOrient" style="width: 575px">
                <asp:TextBox ID="txtItemDescription" CssClass="txtBox" runat="server" TabIndex="6"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblPOLineNumber">
                Purchase Order Line Number
            </div>
            <div class="divtxtBox LNOrient" style=" width: 150px; height: 18px;">
                <asp:TextBox ID="txtPOLineNumber" CssClass=" RequiredField txtBox" runat="server"
                    TabIndex="7"></asp:TextBox>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 200px" id="Div2">
                Approved Quantity for this Line Number
            </div>
            <div class="divtxtBox LNOrient" style="width: 198px">
                <asp:TextBox ID="txtTotQuantity" CssClass="validate[required]  RequiredField txtBox_N"
                    Enabled="false" MaxLength="10" runat="server" TabIndex="8"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Numbers"
                    TargetControlID="txtTotQuantity" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style=" width: 200px" id="Div1">
                Received Quantity for this Lot
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:TextBox ID="txtReceivedQuantity" CssClass="txtBox_N" Enabled="false" MaxLength="50"
                    runat="server" TabIndex="9"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers"
                    TargetControlID="txtReceivedQuantity" />
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblQuantityforthislot">
                Quantity for this Lot
            </div>
            <div class="divtxtBox LNOrient" style="width: 198px">
                <asp:TextBox ID="txtQuantityforthislot" CssClass="validate[required]  RequiredField txtBox_N"
                    MaxLength="10" runat="server" TabIndex="10"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers"
                    TargetControlID="txtQuantityforthislot" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer LNOrient" >
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                Width="500px" DataKeyNames="PK_ID,LOT_ID,CODE,DELETED" OnRowCancelingEdit="gvData_RowCancelingEdit"
                OnRowCommand="gvData_RowCommand" OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound"
                OnRowDeleting="gvData_RowDeleting" OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating"
                ShowFooter="True">
                <Columns>
                    <%-- <asp:TemplateField HeaderText="Attribute Type">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtAttributeType" MaxLength="50" Width="270px" runat="server" CssClass="EntryFont RequiredField txtBox"
                                Text='<%# Eval("ATTRIBUTE_TYPE") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtAttributeType" MaxLength="50" Width="270px" runat="server" CssClass="EntryFont RequiredField txtBox"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblAttributeType" Width="270px" runat="server" Text='<%# Eval("ATTRIBUTE_TYPE") %>'></asp:Label>
                        </ItemTemplate>
                        <ControlStyle Height="25px"></ControlStyle>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>--%>
                    <asp:TemplateField HeaderText="Attribute Type">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlAttributetyp" runat="server" CssClass="EntryFont RequiredField ddlStype"
                                Width="270px" TabIndex="11">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlAttributetyp" runat="server" CssClass="EntryFont RequiredField ddlStype"
                                Width="270px" TabIndex="11">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblAttributeType" Width="270px" runat="server" Text='<%# Eval("CODE") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Attribute Value">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtAttributeValue" MaxLength="50" Width="98%" runat="server" CssClass="EntryFont RequiredField txtBox"
                                TabIndex="12" Text='<%# Eval("ATTRIBUTE_VALUE") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtAttributeValue" MaxLength="50" Width="98%" runat="server" CssClass="EntryFont RequiredField txtBox"
                                TabIndex="12"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblAttributeValue" Width="98%" runat="server" Text='<%# Eval("ATTRIBUTE_VALUE") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" runat="server" AlternateText="Edit" CausesValidation="false"
                                TabIndex="14" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                            <asp:ImageButton ID="ibtnDelete" runat="server" AlternateText="Delete" CausesValidation="false"
                                TabIndex="15" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibtnUpdate" runat="server" AlternateText="Update" CommandName="Update"
                                TabIndex="16" Height="20px" ImageUrl="~/Images/Update.png" />
                            <asp:ImageButton ID="ibtnCancel" runat="server" AlternateText="Cancel" CausesValidation="false"
                                TabIndex="17" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="ibtnInsert" runat="server" AlternateText="Add" CommandName="FooterInsert"
                                TabIndex="18" ImageUrl="~/Images/Add.jpg" />
                        </FooterTemplate>
                        <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <asp:HiddenField ID="hid_Item_Id" runat="server" />
        <asp:HiddenField ID="hid_lotno" runat="server" />
        <asp:HiddenField ID="hf_qty" runat="server" />
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" ToolTip="Save"
                            CssClass="btn" TabIndex="19" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" ToolTip="Delete"
                            OnClick="btnDelete_Click" TabIndex="20" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" ToolTip="Cancel"
                            TabIndex="21" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" ToolTip="Back"
                            TabIndex="22" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table width="100%">
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/AP/APChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
