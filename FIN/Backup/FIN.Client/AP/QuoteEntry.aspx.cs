﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.AP;
using FIN.BLL;
using FIN.BLL.GL;
using FIN.BLL.AP;
using FIN.BLL.HR;
using VMVServices.Web;
using System.Data;

namespace FIN.Client.AP
{
    public partial class QuoteEntry : PageBase
    {

        PO_RFQ_HDR PO_RFQ_HDR = new PO_RFQ_HDR();
        PO_RFQ_DTL PO_RFQ_DTL = new PO_RFQ_DTL();
        PO_RFQ_ADDITIONAL_DTLS PO_RFQ_ADDITIONAL_DTLS = new PO_RFQ_ADDITIONAL_DTLS();

        DataTable dtGridData = new DataTable();
        DataTable dtAdditionalGridData = new DataTable();
        Boolean bol_rowVisiable;
        Boolean savedBool;
        string ProReturn = null;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }
        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }
            UserRightsChecking();

        }
        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }
        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void FillComboBox()
        {

            DraftRFQ_BLL.GetSupplierName(ref ddlSupplierName);
            Lookup_BLL.GetLookUpValues(ref ddlStatus, "RFQ_STATUS");
            Currency_BLL.getCurrencyDetails(ref ddlCurrency);
        }
        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>
        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();
                //FillItemDtls();
                EntityData = null;
                Session[FINSessionConstants.GridData] = null;

                dtGridData = DBMethod.ExecuteQuery(RFQ_DAL.GetRFQDtls(Master.StrRecordId)).Tables[0];
                BindGrid(dtGridData);

                dtAdditionalGridData = DBMethod.ExecuteQuery(RFQ_DAL.GetRFQAdditionalData(Master.StrRecordId)).Tables[0];
                BindAdditionalGrid(dtAdditionalGridData);

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    PO_RFQ_HDR obj_PO_RFQ_HDR = new PO_RFQ_HDR();
                    using (IRepository<PO_RFQ_HDR> userCtx = new DataRepository<PO_RFQ_HDR>())
                    {
                        obj_PO_RFQ_HDR = userCtx.Find(r =>
                            (r.RFQ_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }


                    PO_RFQ_HDR = obj_PO_RFQ_HDR;

                    EntityData = PO_RFQ_HDR;

                    txtRFQNumber.Text = PO_RFQ_HDR.RFQ_NUMBER;
                    if (PO_RFQ_HDR.RFQ_DT != null)
                    {
                        txtRFQDate.Text = DBMethod.ConvertDateToString(PO_RFQ_HDR.RFQ_DT.ToString());
                    }
                    if (PO_RFQ_HDR.RFQ_DUE_DT != null)
                    {
                        txtRFQDueDate.Text = DBMethod.ConvertDateToString(PO_RFQ_HDR.RFQ_DUE_DT.ToString());
                    }
                    txtRemarks.Text = PO_RFQ_HDR.RFQ_REMARKS;
                    if (PO_RFQ_HDR.RFQ_CURRENCY != null)
                    {
                        ddlCurrency.SelectedValue = PO_RFQ_HDR.RFQ_CURRENCY.ToString();
                    }
                    if (PO_RFQ_HDR.RFQ_STATUS != null)
                    {
                        ddlStatus.SelectedValue = PO_RFQ_HDR.RFQ_STATUS.ToString();
                    }

                    if (PO_RFQ_HDR.RFQ_SUPPLIER != string.Empty)
                    {
                        ddlSupplierName.SelectedValue = PO_RFQ_HDR.RFQ_SUPPLIER.ToString();
                        // DraftRFQ_BLL.GetDraftRFQBasedSupplier(ref ddlDraftRFQNUmber, PO_RFQ_HDR.RFQ_SUPPLIER, "", Master.Mode);

                        if (ddlSupplierName.SelectedValue != string.Empty)
                        {
                            DraftRFQ_BLL.GetDraftRFQNo(ref ddlDraftRFQNUmber, ddlSupplierName.SelectedValue);
                            RFQ_BLL.getQuotationNumber(ref ddlQuoteNo, ddlSupplierName.SelectedValue);
                        }
                    }
                    if (PO_RFQ_HDR.RFQ_ID != string.Empty)
                    {
                        ddlQuoteNo.SelectedValue = PO_RFQ_HDR.RFQ_ID;
                        Session["RfqID"] = PO_RFQ_HDR.RFQ_ID;
                    }
                    ddlDraftRFQNUmber.Enabled = false;
                    if (PO_RFQ_HDR.RFQ_TMP_RFQ_ID != string.Empty)
                    {
                        ddlDraftRFQNUmber.SelectedValue = PO_RFQ_HDR.RFQ_TMP_RFQ_ID;
                    }
                    txtSupplierRefNo.Text = PO_RFQ_HDR.VENDOR_REF_NUMBER;
                    txtQuoteNo.Text = PO_RFQ_HDR.QUOTE_NUMBER;
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        private void BindAdditionalGrid(DataTable dtAddData)
        {
            try
            {
                ErrorCollection.Clear();

                bol_rowVisiable = false;
                Session["AddGridData"] = dtAddData;

                DataTable dt_tmp = dtAddData.Copy();

                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }

                gvAdditional.DataSource = dt_tmp;
                gvAdditional.DataBind();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountCodsdfesEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;

                if (dtData.Rows.Count > 0)
                {
                    if (dtData.Rows[0]["RFQ_AMOUNT"].ToString().Length > 0)
                    {
                        //  txtTotalAmt.Text = CommonUtils.CalculateTotalAmount(dtData, "RFQ_AMOUNT");
                        dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("RFQ_ITEM_PRICE", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("RFQ_ITEM_PRICE"))));
                        dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("RFQ_AMOUNT", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("RFQ_AMOUNT"))));
                        dtData.AcceptChanges();
                    }
                }

                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountCodesEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                //ErrorCollection.Clear();

                //DropDownList ddlItemName = tmpgvr.FindControl("ddlItemName") as DropDownList;
                //DropDownList ddlUOM = tmpgvr.FindControl("ddlUOM") as DropDownList;
                //DropDownList ddlSupplier = tmpgvr.FindControl("ddlSupplier") as DropDownList;

                //Item_BLL.GetItemName(ref ddlItemName);
                //UOMMasters_BLL.GetUOMDetails(ref ddlUOM);
                //Supplier_BLL.GetSupplierName(ref ddlSupplier);

                //if (gvData.EditIndex >= 0)
                //{
                //    ddlItemName.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.ITEM_ID].ToString();
                //    ddlUOM.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.PO_REQ_UOM].ToString();
                //    ddlSupplier.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.PO_REQ_UOM].ToString();
                //}

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                if (Session["AddGridData"] != null)
                {
                    dtAdditionalGridData = (DataTable)Session["AddGridData"];
                }
                gvAdditional.EditIndex = -1;

                BindAdditionalGrid(dtAdditionalGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountCodesEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }
        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session["AddGridData"] != null)
                {
                    dtAdditionalGridData = (DataTable)Session["AddGridData"];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvAdditional.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtAdditionalGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        return;
                    }
                    dtAdditionalGridData.Rows.Add(drList);
                    BindAdditionalGrid(dtAdditionalGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {
            System.Collections.SortedList slControls = new System.Collections.SortedList();

            TextBox txtLineNo = gvr.FindControl("txtLineNo") as TextBox;
            TextBox txtDescription = gvr.FindControl("txtDescription") as TextBox;

            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtAdditionalGridData.NewRow();
                drList["RFQ_ADD_ID"] = "0";
            }
            else
            {
                drList = dtAdditionalGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }

            txtLineNo.Text = (rowindex + 1).ToString();

            slControls[0] = txtDescription;

            string strCtrlTypes = FINAppConstants.TEXT_BOX;
            string strMessage = "Description";

            EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

            if (EmptyErrorCollection.Count > 0)
            {
                ErrorCollection = EmptyErrorCollection;
                return drList;
            }

            string strCondition = "RFQ_ADD_DESC='" + txtDescription.Text + "'";
            strMessage = FINMessageConstatns.RecordAlreadyExists;
            ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            if (ErrorCollection.Count > 0)
            {
                return drList;
            }

            drList[FINColumnConstants.LINE_NUM] = txtLineNo.Text;
            drList["RFQ_ADD_DESC"] = txtDescription.Text;
            drList[FINColumnConstants.DELETED] = FINAppConstants.N;

            return drList;

        }

        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvAdditional.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session["AddGridData"] != null)
                {
                    dtAdditionalGridData = (DataTable)Session["AddGridData"];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtAdditionalGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    return;
                }
                gvAdditional.EditIndex = -1;
                BindAdditionalGrid(dtAdditionalGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountCodesEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session["AddGridData"] != null)
                {
                    dtAdditionalGridData = (DataTable)Session["AddGridData"];
                }
                DataRow drList = null;
                drList = dtAdditionalGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;
                BindAdditionalGrid(dtAdditionalGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session["AddGridData"] != null)
                {
                    dtAdditionalGridData = (DataTable)Session["AddGridData"];
                }
                gvAdditional.EditIndex = e.NewEditIndex;
                BindAdditionalGrid(dtAdditionalGridData);
                GridViewRow gvr = gvAdditional.Rows[e.NewEditIndex];

                //  FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvAdditional.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                DropDownList ddlUOM = e.Row.FindControl("ddlUOM") as DropDownList;


                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    UOMMasters_BLL.GetUOMDetails(ref ddlUOM);
                    ddlUOM.SelectedValue = gvData.DataKeys[e.Row.RowIndex].Values[FINColumnConstants.UOM_ID].ToString();
                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        protected void gvAdditional_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                AssignToBE();
                ////ProReturn = FINSP.GetSPFOR_ERR_MGR_CATEGORY_THREE(Master.FormCode, PO_RFQ_HDR.PO_HEADER_ID, PO_RFQ_HDR.PO_NUM, PO_RFQ_HDR.PO_VENDOR_ID, PO_RFQ_HDR.VENDOR_LOC_ID);
                //if (ProReturn != string.Empty)
                //{
                //    if (ProReturn != "0")
                //    {
                //        ErrorCollection.Add("PURCHASEORDER", ProReturn);
                //        if (ErrorCollection.Count > 0)
                //        {
                //            return;
                //        }
                //    }
                //}
                if (savedBool)
                {
                    VMVServices.Web.Utils.RecordMsg = FINMessageConstatns.PO_Number + Session["PO_NUM"];
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    PO_RFQ_HDR = (PO_RFQ_HDR)EntityData;
                }

                if (Session["RfqID"].ToString() != string.Empty || Session["RfqID"].ToString() != "")
                {
                    PO_RFQ_HDR obj_PO_RFQ_HDR = new PO_RFQ_HDR();
                    using (IRepository<PO_RFQ_HDR> userCtx = new DataRepository<PO_RFQ_HDR>())
                    {
                        obj_PO_RFQ_HDR = userCtx.Find(r =>
                            (r.RFQ_ID == Session["RfqID"].ToString())
                            ).SingleOrDefault();
                    }
                    PO_RFQ_HDR = obj_PO_RFQ_HDR;
                }

                PO_RFQ_HDR.RFQ_NUMBER = txtRFQNumber.Text;

                if (txtRFQDate.Text != string.Empty)
                {
                    PO_RFQ_HDR.RFQ_DT = DBMethod.ConvertStringToDate(txtRFQDate.Text.ToString());
                }
                if (txtRFQDueDate.Text != string.Empty)
                {
                    PO_RFQ_HDR.RFQ_DUE_DT = DBMethod.ConvertStringToDate(txtRFQDueDate.Text.ToString());
                }
                PO_RFQ_HDR.RFQ_REMARKS = txtRemarks.Text;
                PO_RFQ_HDR.RFQ_CURRENCY = ddlCurrency.SelectedValue.ToString();
                PO_RFQ_HDR.RFQ_STATUS = ddlStatus.SelectedValue.ToString();
                PO_RFQ_HDR.RFQ_TMP_RFQ_ID = ddlDraftRFQNUmber.SelectedValue.ToString();
                PO_RFQ_HDR.RFQ_SUPPLIER = ddlSupplierName.SelectedValue;

                if (Session["tmp_rfq_dtl_id"] != null)
                {
                    PO_RFQ_HDR.ATTRIBUTE1 = Session["tmp_rfq_dtl_id"].ToString();
                }

                PO_RFQ_HDR.ENABLED_FLAG = "1";

                PO_RFQ_HDR.RFQ_ID = PO_RFQ_HDR.RFQ_ID;
                PO_RFQ_HDR.MODIFIED_BY = this.LoggedUserName;
                PO_RFQ_HDR.MODIFIED_DATE = DateTime.Today;

                if (txtQuoteNo.Text.Length > 0)
                {
                    PO_RFQ_HDR.QUOTE_NUMBER = txtQuoteNo.Text;
                }
                else
                {
                    PO_RFQ_HDR.QUOTE_NUMBER = FINSP.GetSPFOR_SEQCode(FINAppConstants.AP_26_Q.ToString(), false, true);
                }


                PO_RFQ_HDR.VENDOR_REF_NUMBER = txtSupplierRefNo.Text;

                PO_RFQ_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, PO_RFQ_HDR.RFQ_ID);
                PO_RFQ_HDR.RFQ_ORG_ID = VMVServices.Web.Utils.OrganizationID;

                //Save Detail Table

                var tmpChildEntity = new List<Tuple<object, string>>();
                var tmpChildEntity1 = new List<Tuple<object, string>>();
                var tmpChildEntity2 = new List<Tuple<object, string>>();

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                ErrorCollection = CommonUtils.IsEmptyGrid(dtGridData, "Item ");
                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                if (Session["AddGridData"] != null)
                {
                    dtAdditionalGridData = (DataTable)Session["AddGridData"];
                }
                ErrorCollection = CommonUtils.IsEmptyGrid(dtAdditionalGridData, "Additional Details ");
                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                if (gvData.Rows.Count > 0)
                {
                    for (int i = 0; i < gvData.Rows.Count; i++)
                    {
                        PO_RFQ_DTL = new PO_RFQ_DTL();

                        if (gvData.DataKeys[i].Values["RFQ_DTL_ID"].ToString() != "0" && gvData.DataKeys[i].Values["RFQ_DTL_ID"].ToString() != string.Empty)
                        {
                            PO_RFQ_DTL obj_PO_RFQ_DTL = new PO_RFQ_DTL();
                            using (IRepository<PO_RFQ_DTL> userCtx = new DataRepository<PO_RFQ_DTL>())
                            {
                                obj_PO_RFQ_DTL = userCtx.Find(r =>
                                    (r.RFQ_DTL_ID == gvData.DataKeys[i].Values["RFQ_DTL_ID"].ToString())
                                    ).SingleOrDefault();
                            }
                            PO_RFQ_DTL = obj_PO_RFQ_DTL;

                        }

                        PO_RFQ_DTL.RFQ_ID = PO_RFQ_HDR.RFQ_ID;
                        PO_RFQ_DTL.RFQ_ITEM_ID = gvData.DataKeys[i].Values["tmp_rfq_item_id"].ToString();
                        PO_RFQ_DTL.RFQ_ITEM_UOM = gvData.DataKeys[i].Values["tmp_rfq_item_uom"].ToString();

                        //TextBox txtUnitPrice = gvData.Rows[i].FindControl("txtUnitPrice") as TextBox;
                        //PO_RFQ_DTL.RFQ_ITEM_PRICE = CommonUtils.ConvertStringToDecimal(txtUnitPrice.Text);
                        // PO_RFQ_DTL.RFQ_ITEM_PRICE = 0;

                        TextBox txtItemType = gvData.Rows[i].FindControl("txtItemType") as TextBox;
                        PO_RFQ_DTL.RFQ_TYPE = (txtItemType.Text);

                        TextBox txtQuantity = gvData.Rows[i].FindControl("txtQuantity") as TextBox;
                        PO_RFQ_DTL.RFQ_ITEM_QTY = CommonUtils.ConvertStringToDecimal(txtQuantity.Text);

                        TextBox txtAmount = gvData.Rows[i].FindControl("txtAmount") as TextBox;
                        PO_RFQ_DTL.RFQ_AMOUNT = CommonUtils.ConvertStringToDecimal(txtAmount.Text);

                        TextBox txtUnitPrice = gvData.Rows[i].FindControl("txtUnitPrice") as TextBox;
                        
                        ErrorCollection.Remove("txtUnitPrice");

                        if (CommonUtils.ConvertStringToDecimal(txtUnitPrice.Text) == 0)
                        {
                            ErrorCollection.Add("txtUnitPrice", "Unit Price cannot be empty - Row No " + (i + 1));
                            return;
                        }
                        PO_RFQ_DTL.RFQ_ITEM_PRICE = CommonUtils.ConvertStringToDecimal(txtUnitPrice.Text);

                        PO_RFQ_DTL.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                        PO_RFQ_DTL.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;

                        PO_RFQ_DTL.RFQ_DTL_ID = gvData.DataKeys[i].Values["RFQ_DTL_ID"].ToString();
                        PO_RFQ_DTL.MODIFIED_BY = this.LoggedUserName;
                        PO_RFQ_DTL.MODIFIED_DATE = DateTime.Today;
                        tmpChildEntity.Add(new Tuple<object, string>(PO_RFQ_DTL, FINAppConstants.Update));


                        //if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                        //{
                        //    PO_RFQ_DTL.RFQ_DTL_ID = gvData.DataKeys[i].Values["RFQ_DTL_ID"].ToString();
                        //    PO_RFQ_DTL.MODIFIED_BY = this.LoggedUserName;
                        //    PO_RFQ_DTL.MODIFIED_DATE = DateTime.Today;
                        //    tmpChildEntity.Add(new Tuple<object, string>(PO_RFQ_DTL, FINAppConstants.Update));
                        //}
                        //else
                        //{
                        //    PO_RFQ_DTL.RFQ_DTL_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.AP_026_D.ToString(), false, true);
                        //    PO_RFQ_DTL.CREATED_BY = this.LoggedUserName;
                        //    PO_RFQ_DTL.CREATED_DATE = DateTime.Today;
                        //    tmpChildEntity.Add(new Tuple<object, string>(PO_RFQ_DTL, FINAppConstants.Add));
                        //}
                    }
                }



                if (dtAdditionalGridData != null)
                {
                    if (dtAdditionalGridData.Rows.Count > 0)
                    {
                        for (int iLoop = 0; iLoop < dtAdditionalGridData.Rows.Count; iLoop++)
                        {
                            PO_RFQ_ADDITIONAL_DTLS = new PO_RFQ_ADDITIONAL_DTLS();
                            if (dtAdditionalGridData.Rows[iLoop]["RFQ_ADD_ID"].ToString() != "0" && dtAdditionalGridData.Rows[iLoop]["RFQ_ADD_ID"].ToString() != string.Empty)
                            {
                                PO_RFQ_ADDITIONAL_DTLS = RFQ_BLL.getClassEntity(dtAdditionalGridData.Rows[iLoop]["RFQ_ADD_ID"].ToString());
                            }

                            PO_RFQ_ADDITIONAL_DTLS.ATTRIBUTE1 = (iLoop + 1).ToString();
                            PO_RFQ_ADDITIONAL_DTLS.RFQ_ID = PO_RFQ_HDR.RFQ_ID;
                            PO_RFQ_ADDITIONAL_DTLS.RFQ_ADD_DESC = (dtAdditionalGridData.Rows[iLoop]["RFQ_ADD_DESC"].ToString());

                            PO_RFQ_ADDITIONAL_DTLS.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                            PO_RFQ_ADDITIONAL_DTLS.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;

                            if (dtAdditionalGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                            {
                                tmpChildEntity1.Add(new Tuple<object, string>(PO_RFQ_ADDITIONAL_DTLS, FINAppConstants.Delete));
                            }
                            else
                            {
                                if (dtAdditionalGridData.Rows[iLoop]["RFQ_ADD_ID"].ToString() != "0" && dtAdditionalGridData.Rows[iLoop]["RFQ_ADD_ID"].ToString() != string.Empty)
                                {
                                    PO_RFQ_ADDITIONAL_DTLS.RFQ_ADD_ID = dtAdditionalGridData.Rows[iLoop]["RFQ_ADD_ID"].ToString();
                                    PO_RFQ_ADDITIONAL_DTLS.MODIFIED_BY = this.LoggedUserName;
                                    PO_RFQ_ADDITIONAL_DTLS.MODIFIED_DATE = DateTime.Today;
                                    tmpChildEntity1.Add(new Tuple<object, string>(PO_RFQ_ADDITIONAL_DTLS, FINAppConstants.Update));
                                }
                                else
                                {
                                    PO_RFQ_ADDITIONAL_DTLS.RFQ_ADD_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.AP_026_A.ToString(), false, true);
                                    PO_RFQ_ADDITIONAL_DTLS.CREATED_BY = this.LoggedUserName;
                                    PO_RFQ_ADDITIONAL_DTLS.CREATED_DATE = DateTime.Today;
                                    tmpChildEntity1.Add(new Tuple<object, string>(PO_RFQ_ADDITIONAL_DTLS, FINAppConstants.Add));
                                }
                            }
                        }
                    }
                }

                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                    case FINAppConstants.Update:
                        {
                            CommonUtils.SaveMultipleEntity<PO_RFQ_HDR, PO_RFQ_DTL, PO_RFQ_ADDITIONAL_DTLS>(PO_RFQ_HDR, tmpChildEntity, PO_RFQ_DTL, tmpChildEntity1, PO_RFQ_ADDITIONAL_DTLS, true);
                            savedBool = true;
                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("sdPOR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                // DBMethod.DeleteEntity<PO_RFQ_HDR>(PO_RFQ_HDR);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        protected void ddlRequisitionNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                // dtGridData = DBMethod.ExecuteQuery(PurchaseOrder_DAL.GetPODetails_BasedonReq(ddlRequisitionNumber.SelectedValue.ToString())).Tables[0];
                BindGrid(dtGridData);


                DataTable dtDesc = new DataTable();
                // dtDesc = DBMethod.ExecuteQuery(PurchaseOrder_DAL.GetRequsitionDesc(ddlRequisitionNumber.SelectedValue.ToString())).Tables[0];
                // lblDescription.Text = dtDesc.Rows[0]["PO_REQ_DESC"].ToString();

                if (gvData.Rows.Count > 0)
                {

                    ////GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;

                    ////Label lblUnitPrice = gvr.FindControl("lblUnitPrice") as Label;
                    ////Label lblQuantity = gvr.FindControl("lblQuantity") as Label;
                    ////Label lblAmount = gvr.FindControl("lblAmount") as Label;

                    ////if (lblUnitPrice != null && lblQuantity != null)
                    ////{
                    ////    lblAmount.Text = (CommonUtils.ConvertStringToDecimal(lblUnitPrice.Text) * CommonUtils.ConvertStringToDecimal(lblQuantity.Text)).ToString();

                    ////    if (txtPOTotalAmount.Text.Trim() == string.Empty)
                    ////    {
                    ////        txtPOTotalAmount.Text = "0";
                    ////    }
                    ////    if (CommonUtils.ConvertStringToDecimal(txtPOTotalAmount.Text) >= 0 && CommonUtils.ConvertStringToDecimal(lblAmount.Text) >= 0)
                    ////    {
                    ////        txtPOTotalAmount.Text = (CommonUtils.ConvertStringToDecimal(lblAmount.Text) + CommonUtils.ConvertStringToDecimal(txtPOTotalAmount.Text)).ToString();
                    ////    }
                    ////}
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        protected void txtUnitPrice_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
                TextBox txtUnitPrice = (TextBox)gvr.FindControl("txtUnitPrice");
                TextBox txtAmount = (TextBox)gvr.FindControl("txtAmount");
                TextBox txtQuantity = (TextBox)gvr.FindControl("txtQuantity");

                if (txtUnitPrice != null && txtAmount != null && txtQuantity != null)
                {
                    txtAmount.Text = (CommonUtils.ConvertStringToDecimal(txtUnitPrice.Text) * CommonUtils.ConvertStringToDecimal(txtQuantity.Text)).ToString();
                }
                if (txtAmount.Text != string.Empty)
                {
                    txtAmount.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtAmount.Text);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void fn_UnitPricetxtChanged()
        {
            try
            {
                ErrorCollection.Clear();

                TextBox txtUnitPrice = new TextBox();
                TextBox txtQuantity = new TextBox();
                TextBox txtPOAmount = new TextBox();

                txtUnitPrice.ID = "txtUnitPrice";
                txtQuantity.ID = "txtQuantity";
                txtPOAmount.ID = "txtPOAmount";

                Label lblUnitPrice = new Label();
                Label lblQuantity = new Label();
                Label lblAmount = new Label();

                lblUnitPrice.ID = "lblUnitPrice";
                lblQuantity.ID = "lblQuantity";
                lblAmount.ID = "lblAmount";

                if (gvData.FooterRow != null)
                {
                    if (gvData.EditIndex < 0)
                    {
                        txtUnitPrice = (TextBox)gvData.FooterRow.FindControl("txtUnitPrice");
                        txtQuantity = (TextBox)gvData.FooterRow.FindControl("txtQuantity");
                        txtPOAmount = (TextBox)gvData.FooterRow.FindControl("txtPOAmount");

                        lblUnitPrice = (Label)gvData.FooterRow.FindControl("lblUnitPrice");
                        lblQuantity = (Label)gvData.FooterRow.FindControl("lblQuantity");
                        lblAmount = (Label)gvData.FooterRow.FindControl("lblAmount");
                    }
                    else
                    {
                        txtUnitPrice = (TextBox)gvData.Rows[gvData.EditIndex].FindControl("txtUnitPrice");
                        txtQuantity = (TextBox)gvData.Rows[gvData.EditIndex].FindControl("txtQuantity");
                        txtPOAmount = (TextBox)gvData.Rows[gvData.EditIndex].FindControl("txtPOAmount");

                        lblUnitPrice = (Label)gvData.Rows[gvData.EditIndex].FindControl("lblUnitPrice");
                        lblQuantity = (Label)gvData.Rows[gvData.EditIndex].FindControl("lblQuantity");
                        lblAmount = (Label)gvData.Rows[gvData.EditIndex].FindControl("lblAmount");
                    }
                }
                else
                {
                    txtUnitPrice = (TextBox)gvData.Controls[0].Controls[1].FindControl("txtUnitPrice");
                    txtQuantity = (TextBox)gvData.Controls[0].Controls[1].FindControl("txtQuantity");
                    txtPOAmount = (TextBox)gvData.Controls[0].Controls[1].FindControl("txtPOAmount");

                    lblUnitPrice = (Label)gvData.Controls[0].Controls[1].FindControl("lblUnitPrice");
                    lblQuantity = (Label)gvData.Controls[0].Controls[1].FindControl("lblQuantity");
                    lblAmount = (Label)gvData.Controls[0].Controls[1].FindControl("lblAmount");
                }

                if (txtUnitPrice != null && txtQuantity != null)
                {
                    if (txtUnitPrice.Text.Trim() != string.Empty && txtQuantity.Text.Trim() != string.Empty)
                    {
                        if (double.Parse(txtUnitPrice.Text.ToString()) > 0 && double.Parse(txtQuantity.Text.ToString()) > 0)
                        {
                            txtPOAmount.Text = (CommonUtils.ConvertStringToDecimal(txtUnitPrice.Text) * CommonUtils.ConvertStringToDecimal(txtQuantity.Text)).ToString();
                            txtPOAmount.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtPOAmount.Text);

                            //if (txtPOTotalAmount.Text.Trim() == string.Empty)
                            //{
                            //    txtPOTotalAmount.Text = "0";
                            //}
                            //if ( CommonUtils.ConvertStringToDecimal(txtPOAmount.Text) >= 0)
                            //{
                            //    double editpoamt;
                            //    if (Session["Poeditvalue"] != null)
                            //    {
                            //        editpoamt = double.Parse(Session["Poeditvalue"].ToString());
                            //        Session["Poeditvalue"] = null;
                            //    }
                            //    else
                            //    {
                            //        editpoamt = 0;
                            //    }
                            //        txtPOTotalAmount.Text = ((CommonUtils.ConvertStringToDecimal(txtPOAmount.Text) - CommonUtils.ConvertStringToDecimal(editpoamt.ToString())) + CommonUtils.ConvertStringToDecimal(txtPOTotalAmount.Text)).ToString();

                            //}
                        }
                    }
                }

                //if (lblUnitPrice != null && lblQuantity != null)
                //{
                //    if (lblUnitPrice.Text.Trim() != string.Empty && lblQuantity.Text.Trim() != string.Empty)
                //    {
                //        if (int.Parse(lblUnitPrice.Text.ToString()) > 0 && int.Parse(lblQuantity.Text.ToString()) > 0)
                //        {
                //            lblAmount.Text = (CommonUtils.ConvertStringToDecimal(lblUnitPrice.Text) * CommonUtils.ConvertStringToDecimal(lblQuantity.Text)).ToString();

                //            if (txtPOTotalAmount.Text.Trim() == string.Empty)
                //            {
                //                txtPOTotalAmount.Text = "0";
                //            }
                //            if (CommonUtils.ConvertStringToDecimal(txtPOTotalAmount.Text) >= 0 && CommonUtils.ConvertStringToDecimal(lblAmount.Text) >= 0)
                //            {
                //                // txtPOTotalAmount.Text = (CommonUtils.ConvertStringToDecimal(lblAmount.Text) + CommonUtils.ConvertStringToDecimal(txtPOTotalAmount.Text)).ToString();
                //            }
                //        }
                //    }
                //}

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion
        protected void btnDelete_Click(object sender, EventArgs e)
        {

        }

        protected void ddlItemName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;

                DropDownList ddlItemName = gvr.FindControl("ddlItemName") as DropDownList;
                DropDownList ddlUnitPrice = gvr.FindControl("ddlUnitPrice") as DropDownList;

                TextBox txtUnitPrice = gvr.FindControl("txtUnitPrice") as TextBox;

                PurchaseOrder_BLL.GetItemUnitBasedSupplierprice(ref ddlUnitPrice, ddlItemName.SelectedValue);

                DataTable dtunitprice = new DataTable();
                dtunitprice = DBMethod.ExecuteQuery(PurchaseOrder_DAL.GetItemUnitBasedSupplierprice(ddlItemName.SelectedValue)).Tables[0];
                if (dtunitprice.Rows.Count > 0)
                {
                    txtUnitPrice.Text = CommonUtils.ConvertStringToDecimal(dtunitprice.Rows[0]["PO_ITEM_UNIT_PRICE"].ToString()).ToString();
                }


                fn_UnitPricetxtChanged();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void txtQuantity_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                fn_UnitPricetxtChanged();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void ddlSupplierPrice_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                fn_UnitPricetxtChanged();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void ddlSupplierName_SelectedIndexChanged1(object sender, EventArgs e)
        {
            FillItemDtls();
        }

        private void FillItemDtls()
        {
            try
            {
                ErrorCollection.Clear();

                if (ddlSupplierName.SelectedValue != string.Empty)
                {
                    DraftRFQ_BLL.GetDraftRFQNo(ref ddlDraftRFQNUmber, ddlSupplierName.SelectedValue);
                    RFQ_BLL.getQuotationNumber(ref ddlQuoteNo, ddlSupplierName.SelectedValue);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void ClearGrid()
        {
            if (Session[FINSessionConstants.GridData] != null)
            {
                dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                dtGridData.Rows.Clear();
                gvData.DataSource = dtGridData;
                gvData.DataBind();

                dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                BindGrid(dtGridData);

            }
        }
        protected void ddlSupplierName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("1245POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        protected void ddlDraftRFQNUmber_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                DataTable dtData = new DataTable();

                ClearGrid();

                if (ddlDraftRFQNUmber.SelectedValue != string.Empty)
                {
                    dtData = DBMethod.ExecuteQuery(DraftRFQ_DAL.GetDraftRFQBasedSupplier(ddlSupplierName.SelectedValue, ddlDraftRFQNUmber.SelectedValue)).Tables[0];

                    if (dtData.Rows.Count > 0)
                    {
                        txtRFQNumber.Text = dtData.Rows[0]["tmp_rfq_num"].ToString();

                        if (dtData.Rows[0]["tmp_rfq_dt"].ToString() != string.Empty)
                        {
                            txtRFQDate.Text = DBMethod.ConvertDateToString(dtData.Rows[0]["tmp_rfq_dt"].ToString());
                            Session["tmp_rfq_dtl_id"] = dtData.Rows[0]["tmp_rfq_dtl_id"].ToString();
                        }

                        if (dtData.Rows[0]["tmp_rfq_due_dt"].ToString() != string.Empty)
                        {
                            txtRFQDueDate.Text = DBMethod.ConvertDateToString(dtData.Rows[0]["tmp_rfq_due_dt"].ToString());
                        }

                        Lookup_BLL.GetLookUpValues(ref ddlStatus, "RFQ_STATUS");
                        Currency_BLL.getCurrencyDetails(ref ddlCurrency);

                        if (dtData.Rows[0]["tmp_rfq_currency"].ToString() != string.Empty)
                        {
                            ddlCurrency.SelectedValue = dtData.Rows[0]["tmp_rfq_currency"].ToString();
                        }
                        //if (dtData.Rows[0]["tmp_rfq_status"].ToString() != string.Empty)
                        //{
                        //    ddlStatus.SelectedValue = dtData.Rows[0]["tmp_rfq_status"].ToString();
                        //}

                        dtGridData = DBMethod.ExecuteQuery(DraftRFQ_DAL.GetDraftRFQData(ddlDraftRFQNUmber.SelectedValue, ddlSupplierName.SelectedValue)).Tables[0];
                        BindGrid(dtGridData);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("1245POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        protected void ddlQuoteNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                DataTable dtDataDtlsQuoteNo = new DataTable();
                DataTable dtDataAddDtlsQuoteNo = new DataTable();
                DataTable dtData = new DataTable();
                //DataTable dtDataDtls = new DataTable();

                if (ddlQuoteNo.SelectedValue != string.Empty)
                {
                    dtDataDtlsQuoteNo = DBMethod.ExecuteQuery(RFQ_DAL.getDtlsBasedOnQuoteNo(ddlQuoteNo.SelectedValue.ToString())).Tables[0];
                    dtDataAddDtlsQuoteNo = DBMethod.ExecuteQuery(RFQ_DAL.getAddDtlsBasedOnQuoteNo(ddlQuoteNo.SelectedValue.ToString())).Tables[0];

                    //dtDataDtls = DBMethod.ExecuteQuery(RFQ_DAL.GetRFQDtls(ddlQuoteNo.SelectedValue.ToString())).Tables[0];

                    if (dtDataDtlsQuoteNo.Rows.Count > 0)
                    {
                        ddlSupplierName.SelectedValue = dtDataDtlsQuoteNo.Rows[0]["RFQ_SUPPLIER"].ToString();

                        DraftRFQ_BLL.GetDraftRFQNo(ref ddlDraftRFQNUmber, ddlSupplierName.SelectedValue);
                        ddlDraftRFQNUmber.SelectedValue = dtDataDtlsQuoteNo.Rows[0]["rfq_tmp_rfq_id"].ToString();
                        txtRFQNumber.Text = dtDataDtlsQuoteNo.Rows[0]["rfq_number"].ToString();
                        txtRFQDate.Text = DBMethod.ConvertDateToString(dtDataDtlsQuoteNo.Rows[0]["RFQ_DT"].ToString().ToString());
                        txtRFQDueDate.Text = DBMethod.ConvertDateToString(dtDataDtlsQuoteNo.Rows[0]["RFQ_DUE_DT"].ToString());
                        txtRemarks.Text = dtDataDtlsQuoteNo.Rows[0]["RFQ_REMARKS"].ToString();
                        ddlCurrency.SelectedValue = dtDataDtlsQuoteNo.Rows[0]["RFQ_CURRENCY"].ToString();
                        ddlStatus.SelectedValue = dtDataDtlsQuoteNo.Rows[0]["RFQ_STATUS"].ToString();
                        chkActive.Checked = dtDataDtlsQuoteNo.Rows[0]["ENABLED_FLAG"].ToString() == FINAppConstants.EnabledFlag ? true : false;
                        Session["RfqID"] = dtDataDtlsQuoteNo.Rows[0]["RFQ_ID"].ToString();
                    }

                    dtData = DBMethod.ExecuteQuery(DraftRFQ_DAL.GetDraftRFQBasedSupplier(ddlSupplierName.SelectedValue, ddlDraftRFQNUmber.SelectedValue)).Tables[0];
                    if (dtData.Rows.Count > 0)
                    {
                        // txtRFQNumber.Text = dtData.Rows[0]["tmp_rfq_num"].ToString();
                    }

                    BindGrid(dtDataDtlsQuoteNo);
                    BindAdditionalGrid(dtDataAddDtlsQuoteNo);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("QuoteEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
    }
}