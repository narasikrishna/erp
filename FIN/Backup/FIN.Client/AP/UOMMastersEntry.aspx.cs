﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL.AP;
using FIN.DAL;
using FIN.BLL;
using VMVServices.Web;
using System.Data;
namespace FIN.Client.AP
{
    public partial class UOMMasters : PageBase
    {
        INV_UOM_MASTER iNV_UOM_MASTER = new INV_UOM_MASTER();
        string ProReturn = null;
        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
            }

            if (VMVServices.Web.Utils.AddAllowed != FINAppConstants.Y && Master.Mode == FINAppConstants.Add)
            {
                btnSave.Visible = false;
            }

        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();

                EntityData = null;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    using (IRepository<INV_UOM_MASTER> userCtx = new DataRepository<INV_UOM_MASTER>())
                    {
                        iNV_UOM_MASTER = userCtx.Find(r =>
                            (r.UOM_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }
                    chkBaseunit.Enabled = false;

                    EntityData = iNV_UOM_MASTER;

                    txtUOMCode.Text = iNV_UOM_MASTER.UOM_CODE;
                    txtUOMCodeOL.Text = iNV_UOM_MASTER.UOM_CODE_OL;

                    txtUOMDescription.Text = "";
                    if (iNV_UOM_MASTER.UOM_DESC != null)
                    {
                        txtUOMDescription.Text = iNV_UOM_MASTER.UOM_DESC.ToString();
                    }

                    txtUOMDescriptionOL.Text = iNV_UOM_MASTER.UOM_DESC_OL;

                    ddlUOMClass.SelectedValue = iNV_UOM_MASTER.UOM_CLASS;

                    if (iNV_UOM_MASTER.BASE_UNIT == FINAppConstants.EnabledFlag)
                    {
                        chkBaseunit.Checked = true;
                    }
                    else
                    {
                        chkBaseunit.Checked = false;
                    }

                    if (iNV_UOM_MASTER.ENABLED_FLAG == FINAppConstants.EnabledFlag)
                    {
                        chkActive.Checked = true;
                    }
                    else
                    {
                        chkActive.Checked = false;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();

                ValidateBaseClass();

                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                if (EntityData != null)
                {
                    iNV_UOM_MASTER = (INV_UOM_MASTER)EntityData;
                }



                iNV_UOM_MASTER.UOM_CODE = txtUOMCode.Text;
                iNV_UOM_MASTER.UOM_DESC = txtUOMDescription.Text;

                iNV_UOM_MASTER.UOM_CODE_OL = txtUOMCodeOL.Text;
                iNV_UOM_MASTER.UOM_DESC_OL = txtUOMDescriptionOL.Text;


                iNV_UOM_MASTER.UOM_CLASS = ddlUOMClass.SelectedValue.ToString();
                iNV_UOM_MASTER.BASE_UNIT = chkBaseunit.Checked == true ? FINAppConstants.EnabledFlag : FINAppConstants.DisabledFlag;
                iNV_UOM_MASTER.ENABLED_FLAG = chkActive.Checked == true ? FINAppConstants.EnabledFlag : FINAppConstants.DisabledFlag;
                iNV_UOM_MASTER.ORG_ID = VMVServices.Web.Utils.OrganizationID;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    iNV_UOM_MASTER.MODIFIED_BY = this.LoggedUserName;
                    iNV_UOM_MASTER.MODIFIED_DATE = DateTime.Today;

                }
                else
                {
                    iNV_UOM_MASTER.UOM_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.AP_0006.ToString(), false, true);

                    iNV_UOM_MASTER.CREATED_BY = this.LoggedUserName;
                    iNV_UOM_MASTER.CREATED_DATE = DateTime.Today;

                }
                iNV_UOM_MASTER.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, iNV_UOM_MASTER.UOM_ID);


                if (ErrorCollection.Count > 0)
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    //  Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void FillComboBox()
        {
            ComboFilling.fn_getUOMMaster(ref ddlUOMClass);
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                AssignToBE();


                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                // Duplicate Check

                ProReturn = null;

                DataTable dtData = new DataTable();
                int count = 0;
                dtData=DBMethod.ExecuteQuery(FINSQL.IsValueExists("INV_UOM_MASTER", "UOM_ID", iNV_UOM_MASTER.UOM_ID.ToUpper(), "UOM_CODE", iNV_UOM_MASTER.UOM_CODE.ToUpper())).Tables[0];

                if (dtData != null)
                {
                    if (dtData.Rows.Count > 0)
                    {
                        count = CommonUtils.ConvertStringToInt(dtData.Rows[0]["count"].ToString());
                    }
                }
                if (count > 0)
                {
                    ErrorCollection.Add("UOMMASTER1", "UOM code already exists");
                }

                //ProReturn = FINSP.GetSPFOR_ERR_MGR_CATEGORY(Master.FormCode, iNV_UOM_MASTER.UOM_ID.ToUpper(), iNV_UOM_MASTER.UOM_CODE.ToUpper());
                //if (ProReturn != string.Empty)
                //{
                //    if (ProReturn != "0")
                //    {
                //        ErrorCollection.Add("UOMMASTER1", ProReturn);
                //        if (ErrorCollection.Count > 0)
                //        {
                //            return;
                //        }
                //    }
                //}
                ProReturn = FINSP.GetSPFOR_ERR_MGR_CATEGORY_THREE(Master.FormCode, iNV_UOM_MASTER.UOM_ID.ToUpper(), iNV_UOM_MASTER.UOM_CODE.ToUpper(), iNV_UOM_MASTER.UOM_DESC.ToUpper(), iNV_UOM_MASTER.BASE_UNIT.ToUpper());
                if (ProReturn != string.Empty)
                {
                    if (ProReturn != "0")
                    {
                        ErrorCollection.Add("UOMMASTER", ProReturn);
                        if (ErrorCollection.Count > 0)
                        {
                            return;
                        }
                    }
                }


                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                // ProReturn = FINSP.GetSPFOR_ERR_MGR_UOM_MST(txtUOMCode.Text, iNV_UOM_MASTER.BASE_UNIT, iNV_UOM_MASTER.ORG_ID, iNV_UOM_MASTER.UOM_ID);

                //if (ProReturn != string.Empty)
                //{

                //    if (ProReturn != "0")
                //    {
                //        ErrorCollection.Remove("ITEMCATEGORYNAME");

                //        ErrorCollection.Add("ITEMCATEGORYNAME", ProReturn);
                //        if (ErrorCollection.Count > 0)
                //        {
                //            return;
                //        }
                //    }
                //}



                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            DBMethod.SaveEntity<INV_UOM_MASTER>(iNV_UOM_MASTER);
                            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                            break;
                        }
                    case FINAppConstants.Update:
                        {

                            DBMethod.SaveEntity<INV_UOM_MASTER>(iNV_UOM_MASTER, true);
                            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                            break;

                        }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion




        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                DBMethod.DeleteEntity<INV_UOM_MASTER>(iNV_UOM_MASTER);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void ddlUOMClass_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                ValidateBaseClass();

                if (ErrorCollection.Count > 0)
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void ValidateBaseClass()
        {
            try
            {
                Dictionary<string, string> Prop_File_Data;
                Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/AP_" + Session["Sel_Lng"].ToString() + ".properties"));
                ErrorCollection.Clear();
                ErrorCollection.Remove("baseunit");

                int baseUnit = 0;

                if (Master.Mode == FINAppConstants.Add)
                {
                    if (ddlUOMClass.SelectedValue != string.Empty)
                    {
                        baseUnit = DBMethod.GetIntValue(UOMMasters_DAL.IsBaseUnit(ddlUOMClass.SelectedValue));

                        if (chkBaseunit.Checked == true)
                        {
                            if (baseUnit != null)
                            {
                                if (baseUnit > 0)
                                {
                                    ErrorCollection.Remove("baseunit");
                                    ErrorCollection.Add("baseunit", Prop_File_Data["Already_base_unit_P"]);

                                    if (ErrorCollection.Count > 0)
                                    {
                                        return;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYsC", ex.Message);
            }
            finally
            {
                //if (ErrorCollection.Count > 0)
                //{
                //    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                //}
            }
        }
        protected void chkBaseunit_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ValidateBaseClass();

                if (ErrorCollection.Count > 0)
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
    }
}