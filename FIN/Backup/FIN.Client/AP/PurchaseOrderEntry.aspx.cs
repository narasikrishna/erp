﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.AP;
using FIN.BLL;
using FIN.BLL.GL;
using FIN.BLL.AP;
using FIN.BLL.HR;
using VMVServices.Web;
using System.Data;
using System.Threading.Tasks;
using VMVServices.Web;

using VMVServices.Services.Data;



namespace FIN.Client.AP
{
    public partial class PurchaseOrderEntry : PageBase
    {
        //PO_HEADERS PO_HEADERS = new PO_HEADERS(); 
        //PO_LINES PO_LINES = new PO_LINES();
        PO_HEADERS PO_HEADERS = new PO_HEADERS();
        PO_LINES PO_LINES = new PO_LINES();
        DataTable dtGridData = new DataTable();
        Boolean bol_rowVisiable;
        Boolean savedBool;
        string ProReturn = null;

        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();



        DataTable dtScopeGridData = new DataTable();
        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();
                    ChangeLanguage();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));
            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();


            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }
            UserRightsChecking();

        }
        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }
        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>
        private void FillComboBox()
        {

            Lookup_BLL.GetLookUpValues(ref ddlPOStatus, "PS");
            Lookup_BLL.GetLookUpValues(ref ddlPOType, "PT");
            Supplier_BLL.GetSupplierName(ref ddlSupplierName);
            Lookup_BLL.GetLookUpValues(ref ddlModeofTransport, "MOT");
            //   FIN.BLL.AP.Terms_BLL.getTermName(ref ddlPaymentTerm);
            Currency_BLL.getCurrencyDetails(ref ddlCurrency);
            Warehouse_BLL.GetWareHouseData(ref ddlDeliverTo);

            InventoryPriceList_BLL.GetInventoryPriceList(ref ddlPriceName);
            //ddlPriceName
        }
        private void ChangeLanguage()
        {
            try
            {
                ErrorCollection.Clear();
                if (VMVServices.Web.Utils.LanguageCode.ToString().Length > 0)
                {
                    Dictionary<string, string> Prop_File_Data;
                    Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/AP_" + Session["Sel_Lng"].ToString() + ".properties"));
                    lblPoTypeNumber.Text = Prop_File_Data["Number_P"];
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POReqChangeLang", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>
        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();


                Startup();
                FillComboBox();
                gvData.Columns[7].Visible = false;

                EntityData = null;
                Session[FINSessionConstants.GridData] = null;
                Session[FINSessionConstants.ScopeGridData + hdItemId.Value] = null;

                txtPODate.Text = DBMethod.ConvertDateToString(DateTime.Now.ToShortDateString());



                //dtGridData = DBMethod.ExecuteQuery(PurchaseOrder_DAL.GetPODetails("0")).Tables[0];
                //dtGridData.Columns["PO_QUANTITY"].DataType = System.Type.GetType("System.String");
                //dtGridData.Columns["PO_ITEM_UNIT_PRICE"].DataType = System.Type.GetType("System.String");
                //dtGridData.Columns["PO_line_Amount"].DataType = System.Type.GetType("System.String");
                //dtGridData.AcceptChanges();


                dtGridData = DBMethod.ExecuteQuery(PurchaseOrder_DAL.GetPODetails(Master.StrRecordId)).Tables[0];

                BindGrid(dtGridData);

                btnPrint.Visible = false;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    PO_HEADERS PO_HEADERS = new PO_HEADERS();
                    using (IRepository<PO_HEADERS> userCtx = new DataRepository<PO_HEADERS>())
                    {
                        PO_HEADERS = userCtx.Find(r =>
                            (r.PO_HEADER_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }
                    //  PO_HEADERS = PurchaseOrder_BLL.getClassEntity(Master.StrRecordId);

                    EntityData = PO_HEADERS;



                    if (DBMethod.GetIntValue(PurchaseOrder_DAL.ValidateGoodsReceivedOrNot(Master.StrRecordId)) > 0)
                    {
                        btnSave.Visible = false;
                        ErrorCollection.Remove("GRN Received for this");
                        ErrorCollection.Add("GRN Received for this", "Cannot edit the record,because already goods received for this PO");
                    }

                    if (PO_HEADERS.PO_NUM != null)
                    {
                        txtPONumber.Text = PO_HEADERS.PO_NUM;
                    }
                    if (PO_HEADERS.PO_DATE != null)
                    {
                        txtPODate.Text = DBMethod.ConvertDateToString(PO_HEADERS.PO_DATE.ToString());
                    }
                    if (PO_HEADERS.PO_VENDOR_ID != null)
                    {
                        ddlSupplierName.SelectedValue = PO_HEADERS.PO_VENDOR_ID.ToString();
                        SupplierBranch_BLL.fn_getSupplierSite4SupplierName(ref ddlSupplierSite, ddlSupplierName.SelectedValue);
                    }

                    if (PO_HEADERS.VENDOR_LOC_ID != null)
                    {
                        ddlSupplierSite.SelectedValue = PO_HEADERS.VENDOR_LOC_ID.ToString();
                    }
                    if (PO_HEADERS.PO_TYPE != null)
                    {
                        ddlPOType.SelectedValue = PO_HEADERS.PO_TYPE.ToString();

                        LoadPOTypeData();

                        if (ddlPOType.SelectedValue == "Quote" && PO_HEADERS.PO_REQ_ID != null)
                        {
                            btnPrint.Visible = true;
                            FIN.BLL.AP.RFQ_BLL.GetRFQQuotationNo(ref ddlRequisitionNumber, ddlSupplierName.SelectedValue);
                            ddlRequisitionNumber.SelectedValue = PO_HEADERS.PO_REQ_ID.ToString();
                        }
                        else if (PO_HEADERS.PO_REQ_ID != null)
                        {
                            ddlRequisitionNumber.SelectedValue = PO_HEADERS.PO_REQ_ID.ToString();
                        }
                    }
                    if (PO_HEADERS.PO_REMARKS != null)
                    {
                        txtRemarks.Text = PO_HEADERS.PO_REMARKS;
                    }
                    // ddlPOStatus.SelectedValue = PO_HEADERS.PO_STATUS.ToString();

                    if (PO_HEADERS.PO_AMOUNT != null)
                    {
                        txtPOTotalAmount.Text = DBMethod.GetAmtDecimalCommaSeparationValue(PO_HEADERS.PO_AMOUNT.ToString());
                    }
                    //if (PO_HEADERS.PAYMENT_TERM != null)
                    //{
                    //    ddlPaymentTerm.SelectedValue = PO_HEADERS.PAYMENT_TERM.ToString();
                    //}
                    if (PO_HEADERS.MODE_OF_TRANSPORT != null)
                    {
                        ddlModeofTransport.SelectedValue = PO_HEADERS.MODE_OF_TRANSPORT.ToString();
                    }
                    if (PO_HEADERS.NEED_BY_DATE != null)
                    {
                        txtNeedByDate.Text = DBMethod.ConvertDateToString(PO_HEADERS.NEED_BY_DATE.ToString());
                    }
                    if (PO_HEADERS.PO_CURRENCY_CODE != null)
                    {
                        ddlCurrency.SelectedValue = PO_HEADERS.PO_CURRENCY_CODE.ToString();
                    }
                    if (PO_HEADERS.PO_DESC != null)
                    {
                        txtDescription.Text = PO_HEADERS.PO_DESC.ToString();
                    }

                    ddlPriceName.SelectedValue = PO_HEADERS.ATTRIBUTE1;

                    if (PO_HEADERS.DELIVER_TO != null)
                    {
                        ddlDeliverTo.SelectedValue = PO_HEADERS.DELIVER_TO.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        protected void btnClose_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                mpeScope.Hide();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("MILotButtonClick", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }
        }
        protected void btnScope_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
                hdRowIndex.Value = gvr.RowIndex.ToString();

                //DropDownList ddlItemName = gvr.FindControl("ddlItemName") as DropDownList;

                //hdItemId.Value = ddlItemName.SelectedValue.ToString();

                string lineId = string.Empty;

                if (gvData.EditIndex >= 0)
                {
                    if (gvData.DataKeys[gvr.RowIndex].Values["PO_LINE_ID"].ToString() != string.Empty)
                    {
                        lineId = gvData.DataKeys[gvr.RowIndex].Values["PO_LINE_ID"].ToString();
                    }
                    if (gvData.DataKeys[gvr.RowIndex].Values["ITEM_ID"].ToString() != string.Empty)
                    {
                        hdItemId.Value = gvData.DataKeys[gvr.RowIndex].Values["ITEM_ID"].ToString();
                    }
                }

                if (Session[FINSessionConstants.ScopeGridData + hdItemId.Value] == null)
                {
                    dtScopeGridData = DBMethod.ExecuteQuery(PurchaseOrder_DAL.GetPOScope(lineId)).Tables[0];
                }
                else
                {
                    dtScopeGridData = (DataTable)Session[FINSessionConstants.ScopeGridData + hdItemId.Value];
                }

                BindScopeGrid(dtScopeGridData);

                mpeScope.Show();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("MILotButtonClick", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }
        }
        private void BindScopeGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();


                bol_rowVisiable = false;
                Session[FINSessionConstants.ScopeGridData + hdItemId.Value] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvLot.DataSource = dt_tmp;
                gvLot.DataBind();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RLD_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }
        }
        protected void gvLot_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            if (Session[FINSessionConstants.ScopeGridData + hdItemId.Value] != null)
            {
                dtScopeGridData = (DataTable)Session[FINSessionConstants.ScopeGridData + hdItemId.Value];
            }
            gvLot.EditIndex = -1;

            BindScopeGrid(dtScopeGridData);

        }

        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvLot_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.ScopeGridData + hdItemId.Value] != null)
                {
                    dtScopeGridData = (DataTable)Session[FINSessionConstants.ScopeGridData + hdItemId.Value];
                }


                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvLot.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }

                }

                if (Session["rowindex"] == null)
                {
                    Session["rowindex"] = 0;
                    hdRowIndex.Value = Session["rowindex"].ToString();
                }
                else
                {
                    hdRowIndex.Value = (int.Parse(Session["rowindex"].ToString()) + 1).ToString();
                }

                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToLotGridControl(gvr, dtScopeGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                        return;
                    }
                    dtScopeGridData.Rows.Add(drList);
                    BindScopeGrid(dtScopeGridData);

                    if (Session[FINSessionConstants.ScopeGridData + hdItemId.Value] != null)
                    {
                        Session[FINSessionConstants.ScopeGridData + hdRowIndex.Value] = Session[FINSessionConstants.ScopeGridData + hdItemId.Value];
                    }
                }
                mpeScope.Show();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RLD_ROW_CMD", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToLotGridControl(GridViewRow gvr, DataTable tmpdtLotGridData, string GMode, int rowindex)
        {

            System.Collections.SortedList slControls = new System.Collections.SortedList();

            TextBox txtScope = gvr.FindControl("txtScope") as TextBox;

            DataRow drList;
            DataTable dt_tmp = tmpdtLotGridData.Copy();
            if (GMode == "A")
            {
                drList = dtScopeGridData.NewRow();
                drList["PO_SCOPE_ID"] = "0";
            }
            else
            {
                drList = dtScopeGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }

            slControls[0] = txtScope;


            ErrorCollection.Clear();
            string strCtrlTypes = "TextBox";

            string strMessage = "Scope Description";

            EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

            if (EmptyErrorCollection.Count > 0)
            {
                ErrorCollection = EmptyErrorCollection;
                return drList;
            }

            string strCondition = string.Empty;

            strCondition = "PO_SCOPE_DESC='" + txtScope.Text + "'";
            strMessage = "Record Already Exists";

            ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            if (ErrorCollection.Count > 0)
            {
                return drList;
            }


            drList["PO_SCOPE_DESC"] = txtScope.Text.ToString();
            drList["PO_LINE_ID"] = hdItemId.Value.ToString();

            return drList;

        }

        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvLot_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvLot.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.LotGridData] != null)
                {
                    dtScopeGridData = (DataTable)Session[FINSessionConstants.LotGridData];
                }
                if (gvr == null)
                {
                    return;
                }
                //int rowindex = Convert.ToInt32(e.CommandArgument);

                //hdRowIndex.Value = rowindex.ToString();

                drList = AssignToLotGridControl(gvr, dtScopeGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvLot.EditIndex = -1;
                BindScopeGrid(dtScopeGridData);

                if (Session[FINSessionConstants.ScopeGridData + hdItemId.Value] == null)
                {
                    Session[FINSessionConstants.ScopeGridData + hdRowIndex.Value] = Session[FINSessionConstants.ScopeGridData + hdItemId.Value];
                }

                mpeScope.Show();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RLD_ROW_UP", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }
        }

        protected void gvLot_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.ScopeGridData + hdItemId.Value] != null)
                {
                    dtScopeGridData = (DataTable)Session[FINSessionConstants.ScopeGridData + hdItemId.Value];
                }

                BindScopeGrid(dtScopeGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RLD_ROW_DEL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }
        }

        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvLot_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.ScopeGridData + hdItemId.Value] != null)
                {
                    dtScopeGridData = (DataTable)Session[FINSessionConstants.ScopeGridData + hdItemId.Value];
                }
                gvLot.EditIndex = e.NewEditIndex;
                BindScopeGrid(dtScopeGridData);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RLD_ROW_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvLot_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvLot.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RLD_RowCreated", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }

        }

        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvLot_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;
                    else
                    {
                        //if (((DataRowView)e.Row.DataItem).Row["ITEM_ID"].ToString() != hdItemId.Value)
                        //{
                        //    e.Row.Visible = false;
                        //}
                    }
                    if (((DataRowView)e.Row.DataItem).Row["DELETED"].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RLD_RDB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                }
            }

        }
        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;

                if (dtData.Rows.Count > 0)
                {
                    if (dtData.Rows[0]["PO_ITEM_UNIT_PRICE"].ToString().Length > 0)
                    {
                        dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("PO_ITEM_UNIT_PRICE", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("PO_ITEM_UNIT_PRICE"))));
                    }
                    if (dtData.Rows[0]["PO_line_Amount"].ToString().Length > 0)
                    {
                        dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("PO_line_Amount", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("PO_line_Amount"))));
                    }
                    dtData.AcceptChanges();
                }

                DataTable dt_tmp = dtData.Copy();

                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }

                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);

                txtPOTotalAmount.Text = (CommonUtils.CalculateTotalAmount(dtData, "PO_LINE_AMOUNT"));
                if (txtPOTotalAmount.Text != string.Empty)
                {
                    txtPOTotalAmount.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtPOTotalAmount.Text);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountCodesEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddlItemName = tmpgvr.FindControl("ddlItemName") as DropDownList;
                DropDownList ddlUOM = tmpgvr.FindControl("ddlUOM") as DropDownList;
                Button btnScope = tmpgvr.FindControl("btnScope") as Button;

                //TextBox txtUnitPrice = tmpgvr.FindControl("txtUnitPrice") as TextBox;
                //TextBox txtQuantity = tmpgvr.FindControl("txtQuantity") as TextBox;
                TextBox txtPOAmount = tmpgvr.FindControl("txtPOAmount") as TextBox;


                Item_BLL.GetItemName(ref ddlItemName);
                UOMMasters_BLL.GetUOMDetails(ref ddlUOM);



                if (gvData.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                {
                    ddlItemName.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.ITEM_ID].ToString();
                    ddlUOM.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.PO_REQ_UOM].ToString();

                    Session["Poeditvalue"] = txtPOAmount.Text;

                    string ServiceOrItem = string.Empty;
                    ServiceOrItem = DBMethod.GetStringValue(PurchaseOrder_DAL.IsServiceOrItem(ddlItemName.SelectedValue));

                    if (ServiceOrItem != string.Empty)
                    {
                        if (ServiceOrItem == "Item")
                        {
                            btnScope.Visible = false;
                        }
                        else if (ServiceOrItem == "Service")
                        {
                            btnScope.Visible = true;
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            if (Session[FINSessionConstants.GridData] != null)
            {
                dtGridData = (DataTable)Session[FINSessionConstants.GridData];
            }
            gvData.EditIndex = -1;

            BindGrid(dtGridData);

        }
        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {
            System.Collections.SortedList slControls = new System.Collections.SortedList();

            TextBox txtLineNo = gvr.FindControl("txtLineNo") as TextBox;
            TextBox txtDescription = gvr.FindControl("txtDescription") as TextBox;
            TextBox txtQuantity = gvr.FindControl("txtQuantity") as TextBox;
            TextBox txtUnitPrice = gvr.FindControl("txtUnitPrice") as TextBox;
            TextBox txtPOAmount = gvr.FindControl("txtPOAmount") as TextBox;

            DropDownList ddlUOM = gvr.FindControl("ddlUOM") as DropDownList;
            // DropDownList ddlAmount = gvr.FindControl("ddlAmount") as DropDownList;
            DropDownList ddlItemName = gvr.FindControl("ddlItemName") as DropDownList;
            DropDownList ddlUnitPrice = gvr.FindControl("ddlUnitPrice") as DropDownList;


            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList[FINColumnConstants.PO_HEADER_ID] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }

            txtLineNo.Text = (rowindex + 1).ToString();

            slControls[0] = txtLineNo;
            slControls[1] = ddlItemName;
            // slControls[2] = txtDescription;
            slControls[2] = txtQuantity;
            slControls[3] = ddlUOM;
            //  slControls[4] = ddlUnitPrice;
            //slControls[6] = txtPOAmount;
            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/AP_" + Session["Sel_Lng"].ToString() + ".properties"));
            ErrorCollection.Clear();
            string strCtrlTypes = FINAppConstants.TEXT_BOX + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.DROP_DOWN_LIST;// +"~" + FINAppConstants.DROP_DOWN_LIST;
            string strMessage = Prop_File_Data["Line_No_P"] + " ~ " + Prop_File_Data["Item_Name_P"] + " ~ " + Prop_File_Data["Quantity_P"] + " ~ " + Prop_File_Data["UOM_P"];// +" ~ " + Prop_File_Data["Unit_Price_P"] + "";
            //string strMessage = "Line No ~ Item Name ~ Quantity ~ UOM ~ Unit Price";

            EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

            if (EmptyErrorCollection.Count > 0)
            {
                ErrorCollection = EmptyErrorCollection;
                return drList;
            }

            string strCondition = "ITEM_ID='" + ddlItemName.SelectedValue.ToString() + "'";
            strMessage = FINMessageConstatns.RecordAlreadyExists;
            ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            if (ErrorCollection.Count > 0)
            {
                return drList;
            }



            // txtPOTotalAmount.Text = PurchaseOrder_BLL.CalculateAmount(dtGridData, CommonUtils.ConvertStringToDecimal(txtPOAmount.Text));


            drList[FINColumnConstants.PO_REQ_LINE_NUM] = txtLineNo.Text;
            drList[FINColumnConstants.ITEM_ID] = ddlItemName.SelectedValue.ToString();
            drList[FINColumnConstants.ITEM_NAME] = ddlItemName.SelectedItem.Text.ToString();
            drList[FINColumnConstants.PO_DESCRIPTION] = txtDescription.Text;
            drList[FINColumnConstants.PO_QUANTITY] = CommonUtils.ConvertStringToDecimal(txtQuantity.Text);
            drList[FINColumnConstants.PO_REQ_UOM] = ddlUOM.SelectedValue.ToString();
            drList[FINColumnConstants.UOM_CODE] = ddlUOM.SelectedItem.Text.ToString();

            if (ddlUnitPrice.SelectedValue != string.Empty)
            {
                drList[FINColumnConstants.PO_ITEM_UNIT_PRICE] = ddlUnitPrice.SelectedValue;
            }
            else if (txtUnitPrice.Text.Trim() != string.Empty)
            {
                drList[FINColumnConstants.PO_ITEM_UNIT_PRICE] = (txtUnitPrice.Text);
            }

            //drList[FINColumnConstants.PO_REQ_LINE_NUM] = txtLineNo.Text;
            //drList[FINColumnConstants.ITEM_ID] = ddlItemName.SelectedValue.ToString();
            //drList[FINColumnConstants.PO_DESCRIPTION] = txtDescription.Text;
            //drList[FINColumnConstants.PO_QUANTITY] = txtQuantity.Text;
            //drList[FINColumnConstants.PO_REQ_UOM] = ddlUOM.SelectedValue.ToString();
            //drList[FINColumnConstants.PO_ITEM_UNIT_PRICE] = txtUnitPrice.Text;
            drList["PO_line_Amount"] = ((txtPOAmount.Text));
            drList[FINColumnConstants.DELETED] = FINAppConstants.N;


            return drList;

        }

        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountCodesEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];

                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    //String.Format("{0:0.##}", (Decimal)CommonUtils.ConvertStringToDecimal(gvData.Rows[e.Row.RowIndex].Cells[3].ToString())); 

                    TextBox txtUnitPrice = e.Row.FindControl("txtUnitPrice") as TextBox;
                    TextBox txtPOAmount = e.Row.FindControl("txtPOAmount") as TextBox;
                    DropDownList ddlItemName = e.Row.FindControl("ddlItemName") as DropDownList;
                    DropDownList ddlUnitPrice = e.Row.FindControl("ddlUnitPrice") as DropDownList;
                    Button btnScope = e.Row.FindControl("btnScope") as Button;


                    //Label lblUnitPrice =gvDat.f.Row.FindControl("lblUnitPrice") as Label;
                    //Label lblAmount = e.Row.FindControl("lblAmount") as Label;

                    if (txtUnitPrice != null && ddlItemName != null && ddlUnitPrice != null)
                    {
                        DataTable dtunitprice = new DataTable();
                        //dtunitprice = DBMethod.ExecuteQuery(InventoryPriceList_DAL.GetInventoryPriceListDtls("", ddlItemName.SelectedValue)).Tables[0];
                        dtunitprice = DBMethod.ExecuteQuery(InventoryPriceList_DAL.GetInventoryPriceListDtls_frPO("", ddlItemName.SelectedValue, DBMethod.ConvertStringToDate(txtPODate.Text))).Tables[0];

                        if (dtunitprice.Rows.Count > 0)
                        {
                            if (ddlItemName.SelectedValue != string.Empty)
                            {
                                txtUnitPrice.Text = CommonUtils.ConvertStringToDecimal(dtunitprice.Rows[0]["PO_ITEM_UNIT_PRICE"].ToString()).ToString();
                            }
                            ddlUnitPrice.Visible = false;
                            txtUnitPrice.Visible = true;
                        }
                        else
                        {
                            if (ddlItemName.SelectedValue != string.Empty)
                            {
                                txtUnitPrice.Text = CommonUtils.ConvertStringToDecimal(DBMethod.GetDecimalValue(Item_DAL.getUnitPrice(ddlItemName.SelectedValue)).ToString()).ToString();
                            }
                            txtUnitPrice.Visible = true;
                            ddlUnitPrice.Visible = false;
                        }
                        //if (lblUnitPrice != null && lblAmount != null)
                        //{
                        //    lblUnitPrice.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtUnitPrice.Text);
                        //    lblAmount.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtPOAmount.Text);
                        //}

                        string ServiceOrItem = string.Empty;
                        ServiceOrItem = DBMethod.GetStringValue(PurchaseOrder_DAL.IsServiceOrItem(ddlItemName.SelectedValue));

                        if (ServiceOrItem != string.Empty)
                        {
                            if (ServiceOrItem == "Item")
                            {
                                btnScope.Visible = false;
                            }
                            else if (ServiceOrItem == "Service")
                            {
                                btnScope.Visible = true;
                            }
                        }
                    }

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        #endregion

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                AssignToBE();
                ProReturn = FINSP.GetSPFOR_ERR_MGR_CATEGORY_THREE(Master.FormCode, PO_HEADERS.PO_HEADER_ID, PO_HEADERS.PO_NUM, PO_HEADERS.PO_VENDOR_ID, PO_HEADERS.VENDOR_LOC_ID);
                if (ProReturn != string.Empty)
                {
                    if (ProReturn != "0")
                    {
                        ErrorCollection.Add("PURCHASEORDER", ProReturn);
                        if (ErrorCollection.Count > 0)
                        {
                            return;
                        }
                    }
                }
                if (savedBool)
                {
                    VMVServices.Web.Utils.RecordMsg = FINMessageConstatns.PO_Number + Session["PO_NUM"];
                    // Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.RECORDMSG, true);
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    PO_HEADERS = (PO_HEADERS)EntityData;
                }

                //PO_HEADERS.PO_HEADER_ID = txtPONumber.Text;

                if (txtPODate.Text != string.Empty)
                {
                    PO_HEADERS.PO_DATE = DBMethod.ConvertStringToDate(txtPODate.Text.ToString());
                }
                PO_HEADERS.PO_REQ_ID = ddlRequisitionNumber.SelectedValue.ToString();
                PO_HEADERS.PO_TYPE = (ddlPOType.SelectedValue.ToString());
                PO_HEADERS.PO_REMARKS = txtRemarks.Text;
                // PO_HEADERS.PO_STATUS = ddlPOStatus.SelectedValue.ToString();
                PO_HEADERS.PO_AMOUNT = CommonUtils.ConvertStringToDecimal(txtPOTotalAmount.Text);
                PO_HEADERS.PO_VENDOR_ID = ddlSupplierName.SelectedValue.ToString();
                //  PO_HEADERS.PAYMENT_TERM = ddlPaymentTerm.SelectedValue.ToString();
                PO_HEADERS.MODE_OF_TRANSPORT = ddlModeofTransport.SelectedValue.ToString();
                PO_HEADERS.ATTRIBUTE1 = ddlPriceName.SelectedValue;
                PO_HEADERS.PO_CURRENCY_CODE = ddlCurrency.SelectedValue.ToString();
                PO_HEADERS.PO_DESC = txtDescription.Text;
                PO_HEADERS.VENDOR_LOC_ID = ddlSupplierSite.SelectedValue;
                if (txtNeedByDate.Text != string.Empty)
                {
                    PO_HEADERS.NEED_BY_DATE = DBMethod.ConvertStringToDate(txtNeedByDate.Text.ToString());
                }

                PO_HEADERS.DELIVER_TO = ddlDeliverTo.SelectedValue;

                PO_HEADERS.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                //PO_HEADERS.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;
                PO_HEADERS.ORG_ID = VMVServices.Web.Utils.OrganizationID;


                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    PO_HEADERS.MODIFIED_BY = this.LoggedUserName;
                    PO_HEADERS.MODIFIED_DATE = DateTime.Today;
                }
                else
                {
                    PO_HEADERS.PO_HEADER_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.AP_013_M.ToString(), false, true);
                    PO_HEADERS.PO_NUM = FINSP.GetSPFOR_SEQCode(FINAppConstants.AP_013_LN.ToString(), false, true);

                    PO_HEADERS.CREATED_BY = this.LoggedUserName;
                    PO_HEADERS.CREATED_DATE = DateTime.Today;
                }
                Session["PO_NUM"] = PO_HEADERS.PO_NUM;

                PO_HEADERS.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, PO_HEADERS.PO_HEADER_ID);

                //Save Detail Table


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                ErrorCollection = CommonUtils.IsEmptyGrid(dtGridData, "Item ");
                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                var tmpChildEntity = new List<Tuple<object, string>>();
                PO_LINES PO_LINES = new PO_LINES();

                var tmpChildEntity1 = new List<Tuple<object, string>>();
                PO_WORK_SCOPE PO_WORK_SCOPE = new PO_WORK_SCOPE();

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    PO_LINES = new PO_LINES();
                    if (dtGridData.Rows[iLoop][FINColumnConstants.PO_LINE_ID].ToString() != "0" && dtGridData.Rows[iLoop][FINColumnConstants.PO_LINE_ID].ToString() != string.Empty)
                    {
                        PO_LINES = PurchaseOrder_BLL.getDetailClassEntity(dtGridData.Rows[iLoop][FINColumnConstants.PO_LINE_ID].ToString());
                    }
                    PO_LINES.PO_LINE_NUM = (iLoop + 1);//CommonUtils.ConvertStringToInt(dtGridData.Rows[iLoop][FINColumnConstants.PO_REQ_LINE_NUM].ToString());
                    PO_LINES.PO_ITEM_ID = (dtGridData.Rows[iLoop][FINColumnConstants.ITEM_ID].ToString());
                    PO_LINES.PO_DESCRIPTION = (dtGridData.Rows[iLoop][FINColumnConstants.PO_DESCRIPTION].ToString());
                    PO_LINES.PO_QUANTITY = CommonUtils.ConvertStringToInt(dtGridData.Rows[iLoop][FINColumnConstants.PO_QUANTITY].ToString());
                    PO_LINES.PO_UOM = (dtGridData.Rows[iLoop][FINColumnConstants.PO_REQ_UOM].ToString());
                    PO_LINES.PO_UNIT_PRICE = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop][FINColumnConstants.PO_ITEM_UNIT_PRICE].ToString());

                    //  PO_LINES.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;
                    PO_LINES.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                    PO_LINES.PO_HEADER_ID = PO_HEADERS.PO_HEADER_ID;
                    PO_LINES.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;


                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y && dtGridData.Rows[iLoop][FINColumnConstants.PO_LINE_ID].ToString() != "0" && dtGridData.Rows[iLoop][FINColumnConstants.PO_LINE_ID].ToString() != string.Empty)
                    {
                        tmpChildEntity.Add(new Tuple<object, string>(PO_LINES, FINAppConstants.Delete));
                    }
                    else
                    {
                        if (dtGridData.Rows[iLoop][FINColumnConstants.PO_LINE_ID].ToString() != "0" && dtGridData.Rows[iLoop][FINColumnConstants.PO_LINE_ID].ToString() != string.Empty)
                        {
                            PO_LINES.MODIFIED_DATE = DateTime.Today;
                            PO_LINES.MODIFIED_BY = this.LoggedUserName;
                            tmpChildEntity.Add(new Tuple<object, string>(PO_LINES, FINAppConstants.Update));
                        }
                        else
                        {
                            PO_LINES.PO_LINE_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.AP_013_D.ToString(), false, true);
                            PO_LINES.CREATED_BY = this.LoggedUserName;
                            PO_LINES.CREATED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(PO_LINES, FINAppConstants.Add));
                        }
                    }

                   


                    if (Session[FINSessionConstants.ScopeGridData + PO_LINES.PO_ITEM_ID] != null)
                    {
                        dtScopeGridData = (DataTable)Session[FINSessionConstants.ScopeGridData + PO_LINES.PO_ITEM_ID];
                    }



                    PO_WORK_SCOPE = new PO_WORK_SCOPE();
                    if (dtScopeGridData.Rows.Count > 0)
                    {
                        for (int JLoop = 0; JLoop < dtScopeGridData.Rows.Count; JLoop++)
                        {
                            PO_WORK_SCOPE = new PO_WORK_SCOPE();

                            if (dtScopeGridData.Rows[JLoop][FINColumnConstants.PO_SCOPE_ID].ToString() != "0" && dtScopeGridData.Rows[JLoop][FINColumnConstants.PO_SCOPE_ID].ToString() != string.Empty)
                            {
                                using (IRepository<PO_WORK_SCOPE> userCtx = new DataRepository<PO_WORK_SCOPE>())
                                {
                                    PO_WORK_SCOPE = userCtx.Find(r =>
                                        (r.PO_SCOPE_ID == dtScopeGridData.Rows[JLoop][FINColumnConstants.PO_SCOPE_ID].ToString())
                                        ).SingleOrDefault();
                                }

                            }
                            PO_WORK_SCOPE.PO_SCOPE_DESC = (dtScopeGridData.Rows[JLoop][FINColumnConstants.PO_SCOPE_DESC].ToString());
                            PO_WORK_SCOPE.ENABLED_FLAG = "1";
                            PO_WORK_SCOPE.PO_LINE_ID = PO_LINES.PO_LINE_ID;

                            if (dtScopeGridData.Rows[JLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                            {
                                tmpChildEntity1.Add(new Tuple<object, string>(PO_WORK_SCOPE, FINAppConstants.Delete));
                            }
                            else
                            {
                                if (dtScopeGridData.Rows[JLoop][FINColumnConstants.PO_SCOPE_ID].ToString() != "0" && dtScopeGridData.Rows[JLoop][FINColumnConstants.PO_SCOPE_ID].ToString() != string.Empty)
                                {
                                    PO_WORK_SCOPE.MODIFIED_BY = this.LoggedUserName;
                                    PO_WORK_SCOPE.MODIFIED_DATE = DateTime.Today;
                                    tmpChildEntity1.Add(new Tuple<object, string>(PO_WORK_SCOPE, FINAppConstants.Update));
                                }
                                else
                                {
                                    PO_WORK_SCOPE.PO_SCOPE_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.AP_013_S.ToString(), false, true);
                                    PO_WORK_SCOPE.CREATED_BY = this.LoggedUserName;
                                    PO_WORK_SCOPE.CREATED_DATE = DateTime.Today;
                                    tmpChildEntity1.Add(new Tuple<object, string>(PO_WORK_SCOPE, FINAppConstants.Add));
                                }
                            }

                            PO_WORK_SCOPE.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;
                        }
                    }

                }

                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            CommonUtils.SaveMultipleEntity<PO_HEADERS, PO_LINES, PO_WORK_SCOPE>(PO_HEADERS, tmpChildEntity, PO_LINES, tmpChildEntity1, PO_WORK_SCOPE);
                            savedBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {
                            CommonUtils.SaveMultipleEntity<PO_HEADERS, PO_LINES, PO_WORK_SCOPE>(PO_HEADERS, tmpChildEntity, PO_LINES, tmpChildEntity1, PO_WORK_SCOPE, true);
                            savedBool = true;
                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                // DBMethod.DeleteEntity<PO_HEADERS>(PO_HEADERS);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        protected void ddlRequisitionNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                if (ddlPOType.SelectedValue == "Requistion")
                {
                    dtGridData = DBMethod.ExecuteQuery(PurchaseOrder_DAL.GetPODetails_BasedonReq(ddlRequisitionNumber.SelectedValue.ToString())).Tables[0];
                    BindGrid(dtGridData);

                    //dtGridData = DBMethod.ExecuteQuery(PurchaseOrder_DAL.GetPODetailsBasedReq(ddlRequisitionNumber.SelectedValue.ToString())).Tables[0];
                    //BindGrid(dtGridData);

                    DataTable dtDesc = new DataTable();
                    dtDesc = DBMethod.ExecuteQuery(PurchaseOrder_DAL.GetRequsitionDesc(ddlRequisitionNumber.SelectedValue.ToString())).Tables[0];
                    //  lblDescription.Text = dtDesc.Rows[0]["PO_REQ_DESC"].ToString();
                    txtDescription.Text = dtDesc.Rows[0]["PO_REQ_DESC"].ToString();
                }
                else if (ddlPOType.SelectedValue == "Quote")
                {
                    dtGridData = DBMethod.ExecuteQuery(PurchaseOrder_DAL.GetPODetails_BasedonQuote(ddlRequisitionNumber.SelectedValue.ToString())).Tables[0];
                    BindGrid(dtGridData);
                    btnPrint.Visible = true;
                }
                if (gvData.Rows.Count > 0)
                {

                    //GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;

                    //Label lblUnitPrice = gvr.FindControl("lblUnitPrice") as Label;
                    //Label lblQuantity = gvr.FindControl("lblQuantity") as Label;
                    //Label lblAmount = gvr.FindControl("lblAmount") as Label;

                    //if (lblUnitPrice != null && lblQuantity != null)
                    //{
                    //    lblAmount.Text = (CommonUtils.ConvertStringToDecimal(lblUnitPrice.Text) * CommonUtils.ConvertStringToDecimal(lblQuantity.Text)).ToString();

                    //    if (txtPOTotalAmount.Text.Trim() == string.Empty)
                    //    {
                    //        txtPOTotalAmount.Text = "0";
                    //    }
                    //    if (CommonUtils.ConvertStringToDecimal(txtPOTotalAmount.Text) >= 0 && CommonUtils.ConvertStringToDecimal(lblAmount.Text) >= 0)
                    //    {
                    //        txtPOTotalAmount.Text = (CommonUtils.ConvertStringToDecimal(lblAmount.Text) + CommonUtils.ConvertStringToDecimal(txtPOTotalAmount.Text)).ToString();
                    //    }
                    //}
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        protected void txtUnitPrice_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;

                DropDownList ddlItemName = gvr.FindControl("ddlItemName") as DropDownList;
                DropDownList ddlUnitPrice = gvr.FindControl("ddlUnitPrice") as DropDownList;
                TextBox txtUnitPrice = gvr.FindControl("txtUnitPrice") as TextBox;

                if (ddlUnitPrice.SelectedValue != string.Empty && ddlUnitPrice.SelectedValue != null)
                {
                    txtUnitPrice.Text = ddlUnitPrice.SelectedValue;
                }

                fn_UnitPricetxtChanged();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void fn_UnitPricetxtChanged()
        {
            try
            {
                ErrorCollection.Clear();

                TextBox txtUnitPrice = new TextBox();
                TextBox txtQuantity = new TextBox();
                TextBox txtPOAmount = new TextBox();
                DropDownList ddlItemName = new DropDownList();

                txtUnitPrice.ID = "txtUnitPrice";
                txtQuantity.ID = "txtQuantity";
                txtPOAmount.ID = "txtPOAmount";
                ddlItemName.ID = "ddlItemName";

                Label lblUnitPrice = new Label();
                Label lblQuantity = new Label();
                Label lblAmount = new Label();

                lblUnitPrice.ID = "lblUnitPrice";
                lblQuantity.ID = "lblQuantity";
                lblAmount.ID = "lblAmount";

                if (gvData.FooterRow != null)
                {
                    if (gvData.EditIndex < 0)
                    {
                        txtUnitPrice = (TextBox)gvData.FooterRow.FindControl("txtUnitPrice");
                        txtQuantity = (TextBox)gvData.FooterRow.FindControl("txtQuantity");
                        txtPOAmount = (TextBox)gvData.FooterRow.FindControl("txtPOAmount");

                        lblUnitPrice = (Label)gvData.FooterRow.FindControl("lblUnitPrice");
                        lblQuantity = (Label)gvData.FooterRow.FindControl("lblQuantity");
                        lblAmount = (Label)gvData.FooterRow.FindControl("lblAmount");
                        ddlItemName = (DropDownList)gvData.FooterRow.FindControl("ddlItemName");
                    }
                    else
                    {
                        txtUnitPrice = (TextBox)gvData.Rows[gvData.EditIndex].FindControl("txtUnitPrice");
                        txtQuantity = (TextBox)gvData.Rows[gvData.EditIndex].FindControl("txtQuantity");
                        txtPOAmount = (TextBox)gvData.Rows[gvData.EditIndex].FindControl("txtPOAmount");

                        lblUnitPrice = (Label)gvData.Rows[gvData.EditIndex].FindControl("lblUnitPrice");
                        lblQuantity = (Label)gvData.Rows[gvData.EditIndex].FindControl("lblQuantity");
                        lblAmount = (Label)gvData.Rows[gvData.EditIndex].FindControl("lblAmount");

                        ddlItemName = (DropDownList)gvData.Rows[gvData.EditIndex].FindControl("ddlItemName");
                     
                    }
                }
                else
                {
                    txtUnitPrice = (TextBox)gvData.Controls[0].Controls[1].FindControl("txtUnitPrice");
                    txtQuantity = (TextBox)gvData.Controls[0].Controls[1].FindControl("txtQuantity");
                    txtPOAmount = (TextBox)gvData.Controls[0].Controls[1].FindControl("txtPOAmount");

                    lblUnitPrice = (Label)gvData.Controls[0].Controls[1].FindControl("lblUnitPrice");
                    lblQuantity = (Label)gvData.Controls[0].Controls[1].FindControl("lblQuantity");
                    lblAmount = (Label)gvData.Controls[0].Controls[1].FindControl("lblAmount");

                    ddlItemName = (DropDownList)gvData.Controls[0].Controls[1].FindControl("ddlItemName");
                }

                if (txtUnitPrice != null && ddlItemName != null)
                {                    

                    DataTable dtunitprice = new DataTable();
                    dtunitprice = DBMethod.ExecuteQuery(InventoryPriceList_DAL.GetInventoryPriceListDtls_frPO(ddlPriceName.SelectedValue, ddlItemName.SelectedValue, DBMethod.ConvertStringToDate(txtPODate.Text))).Tables[0];

                    if (dtunitprice.Rows.Count > 0)
                    {
                        if (ddlItemName.SelectedValue != string.Empty)
                        {
                            txtUnitPrice.Text = CommonUtils.ConvertStringToDecimal(dtunitprice.Rows[0]["PO_ITEM_UNIT_PRICE"].ToString()).ToString();
                        }
                    }
                    else
                    {
                        if (ddlItemName.SelectedValue != string.Empty)
                        {
                            txtUnitPrice.Text = CommonUtils.ConvertStringToDecimal(DBMethod.GetDecimalValue(Item_DAL.getUnitPrice(ddlItemName.SelectedValue)).ToString()).ToString();
                        }
                    }
                }

                if (txtUnitPrice != null && txtQuantity != null)
                {
                    if (txtUnitPrice.Text.Trim() != string.Empty && txtQuantity.Text.Trim() != string.Empty)
                    {
                        if (double.Parse(txtUnitPrice.Text.ToString()) > 0 && double.Parse(txtQuantity.Text.ToString()) > 0)
                        {
                            txtPOAmount.Text = (CommonUtils.ConvertStringToDecimal(txtUnitPrice.Text) * CommonUtils.ConvertStringToDecimal(txtQuantity.Text)).ToString();

                            txtPOAmount.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtPOAmount.Text);

                            //if (txtPOTotalAmount.Text.Trim() == string.Empty)
                            //{
                            //    txtPOTotalAmount.Text = "0";
                            //}
                            //if ( CommonUtils.ConvertStringToDecimal(txtPOAmount.Text) >= 0)
                            //{
                            //    double editpoamt;
                            //    if (Session["Poeditvalue"] != null)
                            //    {
                            //        editpoamt = double.Parse(Session["Poeditvalue"].ToString());
                            //        Session["Poeditvalue"] = null;
                            //    }
                            //    else
                            //    {
                            //        editpoamt = 0;
                            //    }
                            //        txtPOTotalAmount.Text = ((CommonUtils.ConvertStringToDecimal(txtPOAmount.Text) - CommonUtils.ConvertStringToDecimal(editpoamt.ToString())) + CommonUtils.ConvertStringToDecimal(txtPOTotalAmount.Text)).ToString();

                            //}
                        }
                    }
                }

                //if (lblUnitPrice != null && lblQuantity != null)
                //{
                //    if (lblUnitPrice.Text.Trim() != string.Empty && lblQuantity.Text.Trim() != string.Empty)
                //    {
                //        if (int.Parse(lblUnitPrice.Text.ToString()) > 0 && int.Parse(lblQuantity.Text.ToString()) > 0)
                //        {
                //            lblAmount.Text = (CommonUtils.ConvertStringToDecimal(lblUnitPrice.Text) * CommonUtils.ConvertStringToDecimal(lblQuantity.Text)).ToString();

                //            if (txtPOTotalAmount.Text.Trim() == string.Empty)
                //            {
                //                txtPOTotalAmount.Text = "0";
                //            }
                //            if (CommonUtils.ConvertStringToDecimal(txtPOTotalAmount.Text) >= 0 && CommonUtils.ConvertStringToDecimal(lblAmount.Text) >= 0)
                //            {
                //                // txtPOTotalAmount.Text = (CommonUtils.ConvertStringToDecimal(lblAmount.Text) + CommonUtils.ConvertStringToDecimal(txtPOTotalAmount.Text)).ToString();
                //            }
                //        }
                //    }
                //}

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion
        protected void btnDelete_Click(object sender, EventArgs e)
        {

        }


        protected void ddlItemName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
                Button btnScope = gvr.FindControl("btnScope") as Button;
                DropDownList ddlItemName = gvr.FindControl("ddlItemName") as DropDownList;
                DropDownList ddlUnitPrice = gvr.FindControl("ddlUnitPrice") as DropDownList;

                TextBox txtUnitPrice = gvr.FindControl("txtUnitPrice") as TextBox;

                PurchaseOrder_BLL.GetItemUnitBasedSupplierprice(ref ddlUnitPrice, ddlItemName.SelectedValue);

                DataTable dtunitprice = new DataTable();
                //  dtunitprice = DBMethod.ExecuteQuery(InventoryPriceList_DAL.GetInventoryPriceListDtls("", ddlItemName.SelectedValue)).Tables[0];
                dtunitprice = DBMethod.ExecuteQuery(InventoryPriceList_DAL.GetInventoryPriceListDtls_frPO(ddlPriceName.SelectedValue, ddlItemName.SelectedValue, DBMethod.ConvertStringToDate(txtPODate.Text))).Tables[0];

                hdItemId.Value = ddlItemName.SelectedValue;

                txtUnitPrice.Text = string.Empty;

                if (dtunitprice.Rows.Count > 0)
                {
                    txtUnitPrice.Text = CommonUtils.ConvertStringToDecimal(dtunitprice.Rows[0]["PO_ITEM_UNIT_PRICE"].ToString()).ToString();
                    ddlUnitPrice.Visible = false;
                    txtUnitPrice.Visible = true;
                }
                else
                {
                    txtUnitPrice.Text = CommonUtils.ConvertStringToDecimal(DBMethod.GetDecimalValue(Item_DAL.getUnitPrice(ddlItemName.SelectedValue)).ToString()).ToString();
                    txtUnitPrice.Visible = true;
                    ddlUnitPrice.Visible = false;
                }
                fn_UnitPricetxtChanged();

                string ServiceOrItem = string.Empty;
                ServiceOrItem = DBMethod.GetStringValue(PurchaseOrder_DAL.IsServiceOrItem(ddlItemName.SelectedValue));

                if (ServiceOrItem != string.Empty)
                {
                    if (ServiceOrItem == "Item")
                    {
                        btnScope.Visible = false;
                    }
                    else if (ServiceOrItem == "Service")
                    {
                        btnScope.Visible = true;
                        gvData.Columns[7].Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void txtQuantity_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                fn_UnitPricetxtChanged();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void ddlSupplierPrice_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                fn_UnitPricetxtChanged();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        protected void ddlSupplierName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                SupplierBranch_BLL.fn_getSupplierSite4SupplierName(ref ddlSupplierSite, ddlSupplierName.SelectedValue);
                if (ddlPOType.SelectedValue == "Quote")
                {
                    LoadQuoteDet();
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void ddlPOType_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadPOTypeData();
        }
        private void LoadPOTypeData()
        {
            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/AP_" + Session["Sel_Lng"].ToString() + ".properties"));
            
            lblPoTypeNumber.Enabled = true;
            ddlRequisitionNumber.Enabled = true;
            ddlRequisitionNumber.Items.Clear();
            if (ddlPOType.SelectedValue == "Requistion")
            {
                lblPoTypeNumber.Text = "Requisition Number";
                if (VMVServices.Web.Utils.LanguageCode.ToString().Length > 0)
                {
                    lblPoTypeNumber.Text = Prop_File_Data["Requisition_Number_P"];
                }
                PurchaseOrderReq_BLL.GetPOReqNumber(ref ddlRequisitionNumber);
            }
            else if (ddlPOType.SelectedValue == "Quote")
            {
                lblPoTypeNumber.Text = "Quotation Number";
                if (VMVServices.Web.Utils.LanguageCode.ToString().Length > 0)
                {
                    lblPoTypeNumber.Text = Prop_File_Data["Quotation_Number_P"];
                }
                LoadQuoteDet();
            }
            else
            {
                lblPoTypeNumber.Enabled = false;
                lblPoTypeNumber.Text = "Number";
                if (VMVServices.Web.Utils.LanguageCode.ToString().Length > 0)
                {
                    lblPoTypeNumber.Text = Prop_File_Data["Number_P"];
                }
                ddlRequisitionNumber.Enabled = false;
            }
        }
        private void LoadQuoteDet()
        {
            if (ddlSupplierName.SelectedValue.Length > 0)
            {
                FIN.BLL.AP.RFQ_BLL.GetRFQQuotationNo(ref ddlRequisitionNumber, ddlSupplierName.SelectedValue);
            }
        }

        protected void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                ReportFile = Master.ReportName;

                string draftRFQId = string.Empty;
              
                if (ddlRequisitionNumber.SelectedValue != string.Empty)
                {
                   
                    PO_RFQ_HDR po_RFQ_HDR = new PO_RFQ_HDR();
                    using (IRepository<PO_RFQ_HDR> userCtx = new DataRepository<PO_RFQ_HDR>())
                    {
                        po_RFQ_HDR = userCtx.Find(r =>
                            (r.RFQ_ID == ddlRequisitionNumber.SelectedValue)
                            ).SingleOrDefault();
                    }

                    if (po_RFQ_HDR != null)
                    {
                        if (po_RFQ_HDR.RFQ_TMP_RFQ_ID != null)
                        {
                            htFilterParameter.Add("rfq_id", po_RFQ_HDR.RFQ_TMP_RFQ_ID);
                        }
                    }
                }


                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;


                ReportData = DBMethod.ExecuteQuery(FIN.DAL.AP.RFQ_DAL.getQuotationReportData());

                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());


                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerPath + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("APAgingAnalysisReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void ddlPriceName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                GridViewRow gvr = gvData.FooterRow;

                DropDownList ddlItemName = gvr.FindControl("ddlItemName") as DropDownList;
                DropDownList ddlUnitPrice = gvr.FindControl("ddlUnitPrice") as DropDownList;

                TextBox txtUnitPrice = gvr.FindControl("txtUnitPrice") as TextBox;

                InventoryPriceList_BLL.GetInventoryPriceListDtls(ref ddlUnitPrice, ddlPriceName.SelectedValue, ddlItemName.SelectedValue);

                DataTable dtunitprice = new DataTable();
                // dtunitprice = DBMethod.ExecuteQuery(InventoryPriceList_DAL.GetInventoryPriceListDtls("", ddlItemName.SelectedValue)).Tables[0];
                dtunitprice = DBMethod.ExecuteQuery(InventoryPriceList_DAL.GetInventoryPriceListDtls_frPO(ddlPriceName.SelectedValue, ddlItemName.SelectedValue, DBMethod.ConvertStringToDate(txtPODate.Text))).Tables[0];

                if (dtunitprice.Rows.Count > 0)
                {
                    if (ddlItemName.SelectedValue != string.Empty)
                    {
                        txtUnitPrice.Text = CommonUtils.ConvertStringToDecimal(dtunitprice.Rows[0]["PO_ITEM_UNIT_PRICE"].ToString()).ToString();
                    }
                    ddlUnitPrice.Visible = false;
                    txtUnitPrice.Visible = true;
                }
                else
                {
                    if (ddlItemName.SelectedValue != string.Empty)
                    {
                        txtUnitPrice.Text = CommonUtils.ConvertStringToDecimal(DBMethod.GetDecimalValue(Item_DAL.getUnitPrice(ddlItemName.SelectedValue)).ToString()).ToString();
                    }
                    txtUnitPrice.Visible = true;
                    ddlUnitPrice.Visible = false;
                }


                fn_UnitPricetxtChanged();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
    }
}