﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.BLL;
using FIN.BLL.AP;
using VMVServices.Web;

namespace FIN.Client.AP
{
    public partial class WarehouseFacilityEntry : PageBase
    {

        INV_WAREHOUSE_FACILITIES iNV_WAREHOUSE_FACILITIES = new INV_WAREHOUSE_FACILITIES();
        DataTable dtGridData = new DataTable();
        WarehouseFacility_BLL warehouseFacility_BLL = new WarehouseFacility_BLL();
        Boolean bol_rowVisiable;
        Boolean savedBool;
        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                if (!IsPostBack)
                {
                    AssignToControl();
                     EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WH_Pg_Load", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }


        }

        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //  div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }


            UserRightsChecking();


        }
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();

                txtWarehouseName.Enabled = false;

                FillComboBox();

                EntityData = null;


                dtGridData = DBMethod.ExecuteQuery(FIN.DAL.AP.WarehouseFacility_DAL.GetWarehouseFacilitydtls(Master.StrRecordId)).Tables[0];

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    using (IRepository<INV_WAREHOUSE_FACILITIES> userCtx = new DataRepository<INV_WAREHOUSE_FACILITIES>())
                    {
                        iNV_WAREHOUSE_FACILITIES = userCtx.Find(r =>
                            (r.WH_FACILITY_ID == (Master.StrRecordId.ToString()))
                            ).SingleOrDefault();
                    }

                    EntityData = iNV_WAREHOUSE_FACILITIES;
                    ddlWarehouseNumber.SelectedValue = iNV_WAREHOUSE_FACILITIES.INV_WH_ID;
                    fillWHName();
                    //INV_WAREHOUSES INV_WAREHOUSES = new INV_WAREHOUSES();
                    //using (IRepository<INV_WAREHOUSES> userCtx = new DataRepository<INV_WAREHOUSES>())
                    //{
                    //    INV_WAREHOUSES = userCtx.Find(r =>
                    //        (r.INV_WH_ID == (iNV_WAREHOUSE_FACILITIES.INV_WH_ID.ToString()))
                    //        ).SingleOrDefault();
                    //}

                    //if (INV_WAREHOUSES != null)
                    //{
                    //    txtWarehouseName.Text = INV_WAREHOUSES.INV_WH_NAME;
                    //    ddlWarehouseNumber.SelectedValue = INV_WAREHOUSES.INV_WH_NAME;
                    //}
                }

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WH_ATC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Course master table entities
        /// </summary>




        private void FillComboBox()
        {
            warehouseFacility_BLL.fn_getWarehouseName(ref ddlWarehouseNumber);

        }



        /// <summary>
        /// Bind the records into grid voew
        /// </summary>
        /// <param name="dtData">Contains the database entities and correspoding records which is used in the grid view</param>

        private void BindGrid(DataTable dtData)
        {
            bol_rowVisiable = false;
            Session[FINSessionConstants.GridData] = dtData;
            DataTable dt_tmp = dtData.Copy();
            if (dt_tmp.Rows.Count == 0)
            {
                DataRow dr = dt_tmp.NewRow();
                dr[0] = "0";
                dr["ENABLED_FLAG"] = "FALSE";
                dt_tmp.Rows.Add(dr);
                bol_rowVisiable = true;
            }
            gvData.DataSource = dt_tmp;
            gvData.DataBind();
            GridViewRow gvr = gvData.FooterRow;
            FillFooterGridCombo(gvr);
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>


        #endregion

        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>

        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = -1;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WH_Row_cnl", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }

                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    else
                    {
                        dtGridData.Rows.Add(drList);
                        BindGrid(dtGridData);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WH_Row_Cmd", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }



        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {

            System.Collections.SortedList slControls = new System.Collections.SortedList();

            DropDownList ddlFacility = gvr.FindControl("ddlFacility") as DropDownList;
            TextBox txtDescription = gvr.FindControl("txtDescription") as TextBox;
            TextBox dtpStartDate = gvr.FindControl("dtpStartDate") as TextBox;
            TextBox dtpEndDate = gvr.FindControl("dtpEndDate") as TextBox;
            CheckBox chkact = gvr.FindControl("chkact") as CheckBox;

            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
              //  drList[FINColumnConstants.PK_ID] = "0";
                drList["WH_FACILITY_ID"] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }

            slControls[0] = ddlFacility;
            // slControls[1] = txtDescription;
            slControls[1] = dtpStartDate;
            slControls[2] = dtpStartDate;
            slControls[3] = dtpEndDate;

            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/AP_" + Session["Sel_Lng"].ToString() + ".properties"));

            ErrorCollection.Clear();
            string strCtrlTypes = "DropDownList~TextBox~DateTime~DateRangeValidate";
            string strMessage = Prop_File_Data["Facility_P"] + " ~ " + Prop_File_Data["Start_Date_P"] + " ~ " + Prop_File_Data["Start_Date_P"] + " ~ " + Prop_File_Data["End_Date_P"] + "";
            //string strMessage = "Facility ~ Start Date ~ Start Date ~ End Date";
            ErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);
            if (ErrorCollection.Count > 0)
                return drList;



            string strCondition = "VALUE_KEY_ID='" + ddlFacility.SelectedValue.ToString() + "'";
            strMessage = FINMessageConstatns.RecordAlreadyExists;
            ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            if (ErrorCollection.Count > 0)
            {
                return drList;
            }


            if (Session[FINSessionConstants.GridData] != null)
            {
                dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    if (dtGridData.Rows[iLoop]["INV_WH_ID"].ToString() != "0")
                    {
                        iNV_WAREHOUSE_FACILITIES.INV_WH_ID = dtGridData.Rows[iLoop]["INV_WH_ID"].ToString();
                    }
                }
            }

            //Duplicate Validation through Backend
            ErrorCollection.Clear();
            string ProReturn = null;
            if (iNV_WAREHOUSE_FACILITIES.INV_WH_ID == null)
            {
                iNV_WAREHOUSE_FACILITIES.INV_WH_ID = string.Empty;
                ProReturn = FINSP.GetSPFOR_ERR_MGR_CATEGORY_TWO(Master.FormCode, drList["WH_FACILITY_ID"].ToString(), ddlFacility.SelectedValue.ToString(), ddlWarehouseNumber.SelectedValue);
            }
            else
            {
              //  ProReturn = FINSP.GetSPFOR_ERR_MGR_CATEGORY(Master.FormCode, iNV_WAREHOUSE_FACILITIES.INV_WH_ID.ToString(), ddlFacility.SelectedValue.ToString());
                ProReturn = FINSP.GetSPFOR_ERR_MGR_CATEGORY_TWO(Master.FormCode, drList["WH_FACILITY_ID"].ToString(), ddlFacility.SelectedValue.ToString(), ddlWarehouseNumber.SelectedValue);
            }
            if (ProReturn != string.Empty)
            {
                if (ProReturn != "0")
                {
                    ErrorCollection.Add("WAREHOUSEFACILITY", ProReturn);
                    if (ErrorCollection.Count > 0)
                    {
                        return drList;
                    }
                }
            }

            if (ddlFacility.SelectedItem != null)
            {
                drList[FINColumnConstants.VALUE_KEY_ID] = ddlFacility.SelectedItem.Value;
                drList[FINColumnConstants.VALUE_NAME] = ddlFacility.SelectedItem.Text;
            }

            drList["WH_FACILITY_DESC"] = txtDescription.Text;
            if (dtpStartDate.Text.ToString().Length > 0)
            {
                drList["WH_EFF_START_DT"] = DBMethod.ConvertStringToDate(dtpStartDate.Text.ToString());
            }

            if (dtpEndDate.Text.ToString().Length > 0)
            {

                drList["WH_EFF_END_DT"] = DBMethod.ConvertStringToDate(dtpEndDate.Text.ToString());
            }
            else
            {
                drList["WH_EFF_END_DT"] = DBNull.Value;
            }
            if (chkact.Checked)
            {
                drList[FINColumnConstants.ENABLED_FLAG] = "TRUE";
            }
            else
            {
                drList[FINColumnConstants.ENABLED_FLAG] = "FALSE";
            }



            drList[FINColumnConstants.DELETED] = FINAppConstants.N;

            return drList;
        }

        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                //else if (FacilityMasterBLL.ErrorCollection.Count > 0)
                //{
                //    ErrorCollection = FacilityMasterBLL.ErrorCollection;
                //    return;
                //}
                else
                {
                    gvData.EditIndex = -1;
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WH_Row_upd", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row delete mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewDeleteEventArgs indicates which row's delete button was clicked. </param>


        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                dtGridData.Rows[e.RowIndex][FINColumnConstants.DELETED] = FINAppConstants.Y;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WH_Row_del", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WH_Row_Edt", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WH_RowCrt", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// Used to delete the exam master and detail table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {

                    iNV_WAREHOUSE_FACILITIES.PK_ID = Convert.ToInt32(dtGridData.Rows[iLoop]["PK_ID"].ToString());
                    DBMethod.DeleteEntity<INV_WAREHOUSE_FACILITIES>(iNV_WAREHOUSE_FACILITIES);
                }


                //   DisplaySaveCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WH_Btn_ys_clik", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddlFacility = tmpgvr.FindControl("ddlFacility") as DropDownList;
               FIN.BLL.Lookup_BLL.GetLookUpValues(ref ddlFacility, "WT");


                if (gvData.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                {
                    ddlFacility.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.VALUE_KEY_ID].ToString();



                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WH_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }



        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (bol_rowVisiable)
                    e.Row.Visible = false;

                if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                {
                    e.Row.Visible = false;
                }

            }
        }

        #endregion



        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session[FINSessionConstants.GridData], "Warehouse Facility ");

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    if (dtGridData.Rows[iLoop]["WH_FACILITY_ID"].ToString() != string.Empty && dtGridData.Rows[iLoop]["WH_FACILITY_ID"].ToString() != "0")
                    {
                        using (IRepository<INV_WAREHOUSE_FACILITIES> userCtx = new DataRepository<INV_WAREHOUSE_FACILITIES>())
                        {
                            iNV_WAREHOUSE_FACILITIES = userCtx.Find(r =>
                                (r.WH_FACILITY_ID == (dtGridData.Rows[iLoop]["WH_FACILITY_ID"].ToString()))
                                ).SingleOrDefault();
                        }
                    }
                    //Master.StrRecordId.ToString()

                    if (dtGridData.Rows[iLoop]["WH_EFF_START_DT"] != DBNull.Value)
                    {
                        iNV_WAREHOUSE_FACILITIES.WH_EFF_START_DT = DateTime.Parse(dtGridData.Rows[iLoop]["WH_EFF_START_DT"].ToString());
                    }

                    if (dtGridData.Rows[iLoop]["WH_EFF_END_DT"] != DBNull.Value)
                    {
                        iNV_WAREHOUSE_FACILITIES.WH_EFF_END_DT = DateTime.Parse(dtGridData.Rows[iLoop]["WH_EFF_END_DT"].ToString());
                    }


                    if (dtGridData.Rows[iLoop][FINColumnConstants.ENABLED_FLAG].ToString().ToUpper() == "TRUE")
                    {
                        iNV_WAREHOUSE_FACILITIES.ENABLED_FLAG = FINAppConstants.Y;
                    }
                    else
                    {
                        iNV_WAREHOUSE_FACILITIES.ENABLED_FLAG = FINAppConstants.N;
                    }

                    iNV_WAREHOUSE_FACILITIES.WH_FACILITY_CODE = dtGridData.Rows[iLoop]["VALUE_KEY_ID"].ToString();
                    iNV_WAREHOUSE_FACILITIES.WH_FACILITY_DESC = dtGridData.Rows[iLoop]["WH_FACILITY_DESC"].ToString();
                    iNV_WAREHOUSE_FACILITIES.INV_WH_ID = ddlWarehouseNumber.SelectedValue.ToString();
                    iNV_WAREHOUSE_FACILITIES.WH_ORG_ID = VMVServices.Web.Utils.OrganizationID;        

                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        iNV_WAREHOUSE_FACILITIES.WH_FACILITY_ID = (dtGridData.Rows[iLoop]["WH_FACILITY_ID"].ToString());
                        DBMethod.DeleteEntity<INV_WAREHOUSE_FACILITIES>(iNV_WAREHOUSE_FACILITIES);
                    }
                    else
                    {
                        if (dtGridData.Rows[iLoop]["WH_FACILITY_ID"].ToString() !="0")
                        {
                            iNV_WAREHOUSE_FACILITIES.WH_FACILITY_ID = (dtGridData.Rows[iLoop]["WH_FACILITY_ID"].ToString());
                            iNV_WAREHOUSE_FACILITIES.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, iNV_WAREHOUSE_FACILITIES.WH_FACILITY_ID);
                            iNV_WAREHOUSE_FACILITIES.MODIFIED_BY = this.LoggedUserName;
                            iNV_WAREHOUSE_FACILITIES.MODIFIED_DATE = DateTime.Today;
                            DBMethod.SaveEntity<INV_WAREHOUSE_FACILITIES>(iNV_WAREHOUSE_FACILITIES, true);


                        }
                        else
                        {
                            iNV_WAREHOUSE_FACILITIES.WH_FACILITY_ID = FINSP.GetSPFOR_SEQCode("WF_D".ToString(), false, true);
                            iNV_WAREHOUSE_FACILITIES.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, iNV_WAREHOUSE_FACILITIES.WH_FACILITY_ID);
                            iNV_WAREHOUSE_FACILITIES.CREATED_BY = this.LoggedUserName;
                            iNV_WAREHOUSE_FACILITIES.CREATED_DATE = DateTime.Today;
                            DBMethod.SaveEntity<INV_WAREHOUSE_FACILITIES>(iNV_WAREHOUSE_FACILITIES);
                        }
                    }


                    
  

                }
                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                else
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WH_btn_sve", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void ddlWarehouseNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            fillWHName();
            getWFDetails();
        }

        private void fillWHName()
        {
            DataTable dtWHName = new DataTable();
            dtWHName = DBMethod.ExecuteQuery(FIN.DAL.AP.WarehouseFacility_DAL.getWHNm(ddlWarehouseNumber.SelectedValue)).Tables[0];
            txtWarehouseName.Text = dtWHName.Rows[0]["INV_WH_NAME"].ToString();
        }
        private void getWFDetails()
        {
            DataTable dtWFDtl = new DataTable();
            dtWFDtl = DBMethod.ExecuteQuery(FIN.DAL.AP.WarehouseFacility_DAL.getWFDetails(ddlWarehouseNumber.SelectedValue.ToString())).Tables[0];
            BindGrid(dtWFDtl);
        }

        protected void txtWarehouseName_TextChanged(object sender, EventArgs e)
        {

        }


    }
}