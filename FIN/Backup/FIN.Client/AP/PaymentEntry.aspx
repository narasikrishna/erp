﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="PaymentEntry.aspx.cs" Inherits="FIN.Client.AP.PaymentEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1200px" id="divMainContainer">
        <table style="width:100%">
            <tr>
                <td runat="server" id="tdHeader">
                    <asp:Panel runat="server" ID="pnltdHeader">
                        <div class="divRowContainer">
                            <div class="lblBox LNOrient" style="width: 150px" id="lblWarehouseNumber">
                                Payment Number
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 150px">
                                <asp:TextBox ID="txtPaymentNumber" runat="server" TabIndex="1" Enabled="false" CssClass=" txtBox"></asp:TextBox>
                            </div>
                            <div class="colspace  LNOrient" >
                                &nbsp</div>
                            <div class="lblBox LNOrient" style="width: 150px" id="lblWarehouseName">
                                Payment Date
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 150px">
                                <asp:TextBox ID="txtPaymentDate" runat="server" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]] RequiredField txtBox"
                                    TabIndex="2" OnTextChanged="txtPaymentDate_TextChanged"></asp:TextBox>
                                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calExtender1" TargetControlID="txtPaymentDate"
                                    OnClientDateSelectionChanged="checkDate" />
                            </div>
                            <div class="colspace LNOrient" >
                                &nbsp</div>
                            <div class="lblBox LNOrient" style="width: 150px" id="lblLineNumber">
                                Payment Method
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 155px">
                                <asp:DropDownList ID="ddlPaymentMode" runat="server" TabIndex="3" CssClass="validate[required] RequiredField ddlStype"
                                    OnSelectedIndexChanged="ddlPaymentMode_SelectedIndexChanged" AutoPostBack="true">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="divClear_10">
                        </div>
                        <div class="divRowContainer">
                            <div class="lblBox LNOrient" style="width: 150px" id="lblGRNNumber">
                                Supplier Number
                            </div>
                            <div class="divtxtBox LNOrient" style=" width: 310px">
                                <asp:DropDownList ID="ddlSupplierNumber" runat="server" TabIndex="4" CssClass="validate[required] RequiredField ddlStype"
                                    AutoPostBack="True" OnSelectedIndexChanged="ddlSupplierNumber_SelectedIndexChanged">
                                </asp:DropDownList>
                            </div>
                            <div class="colspace  LNOrient" >
                                &nbsp</div>
                            <div class="lblBox  LNOrient" style="width: 150px" id="lblGRNDate">
                                Supplier Name
                            </div>
                            <div class="divtxtBox  LNOrient" style="width: 320px">
                                <asp:TextBox ID="txtSupplierName" Enabled="false" runat="server" TabIndex="5" CssClass="txtBox" Width="320px"></asp:TextBox>
                            </div>
                        </div>
                        <div class="divClear_10">
                        </div>
                        <div class="divRowContainer">
                            <div class="lblBox LNOrient" style="width: 150px" id="lblPONumber">
                                Bank Name
                            </div>
                            <div class="divtxtBox  LNOrient" style="width: 155px; height: 22px;">
                                <asp:DropDownList ID="ddlBankName" runat="server" TabIndex="6" CssClass="validate[]   ddlStype"
                                    AutoPostBack="True" OnSelectedIndexChanged="ddlBankName_SelectedIndexChanged">
                                </asp:DropDownList>
                            </div>
                            <div class="colspace LNOrient" >
                                &nbsp</div>
                            <div class="lblBox LNOrient" style=" width: 150px" id="lblPOLineNumber">
                                Bank Branch
                            </div>
                            <div class="divtxtBox  LNOrient" style="width: 150px">
                                <asp:DropDownList ID="ddlBankBranch" runat="server" TabIndex="7" CssClass="validate[]   ddlStype"
                                    AutoPostBack="True" OnSelectedIndexChanged="ddlBankBranch_SelectedIndexChanged">
                                </asp:DropDownList>
                            </div>
                            <div class="colspace  LNOrient" >
                                &nbsp</div>
                            <div class="lblBox  LNOrient" style=" width: 150px" id="lblItemDescription">
                                Account Number
                            </div>
                            <div class="divtxtBox  LNOrient" style=" width: 155px">
                                <asp:DropDownList ID="ddlAccountnumber" runat="server" TabIndex="8" AutoPostBack="True"
                                    OnSelectedIndexChanged="ddlAccountnumber_SelectedIndexChanged" CssClass="validate[]   ddlStype">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="divClear_10">
                        </div>
                        <div class="divRowContainer">
                            <div class="lblBox  LNOrient" style=" width: 150px" id="Div1">
                                Cheque Number
                            </div>
                            <div class="divtxtBox  LNOrient" style="width: 155px">
                                <asp:DropDownList ID="ddlChequeNumber" runat="server" TabIndex="9" CssClass="validate[]   ddlStype"
                                    AutoPostBack="True" OnSelectedIndexChanged="ddlChequeNumber_SelectedIndexChanged">
                                </asp:DropDownList>
                            </div>
                            <div class="colspace  LNOrient">
                                &nbsp</div>
                            <div class="lblBox  LNOrient" style="width: 150px" id="Div2">
                                Value Date
                            </div>
                            <div class="divtxtBox  LNOrient" style="width: 150px">
                                <asp:TextBox ID="txtChequeDate" runat="server" CssClass="validate[] validate[custom[ReqDateDDMMYYY]]  txtBox"
                                    Enabled="false" TabIndex="10"></asp:TextBox>
                                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtChequeDate"
                                    OnClientDateSelectionChanged="checkDate" />
                            </div>
                            <div class="colspace  LNOrient" >
                                &nbsp</div>
                            <div class="lblBox  LNOrient" style="width: 150px" id="Div5">
                                Cheque Received By
                            </div>
                            <div class="divtxtBox  LNOrient" style="width: 150px">
                                <asp:TextBox ID="txtChequeReceivedBy" runat="server" TabIndex="11" CssClass="txtBox"
                                    MaxLength="100"></asp:TextBox>
                            </div>
                        </div>
                        <div class="divClear_10">
                        </div>
                        <div class="divRowContainer">
                            <div class="lblBox LNOrient" style="width: 150px" id="Div7">
                                Civil ID
                            </div>
                            <div class="divtxtBox LNOrient" style=" width: 810px">
                                <asp:TextBox ID="txtCivilId" runat="server" TabIndex="11" CssClass="txtBox" MaxLength="100"></asp:TextBox>
                            </div>
                        </div>
                        <div class="divClear_10">
                        </div>
                        <div class="divRowContainer">
                            <div class="lblBox  LNOrient" style="width: 150px" id="lblPaymentCurrency">
                                Payment Currency
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 155px">
                                <asp:DropDownList ID="ddlPaymentCurrency" runat="server" TabIndex="12" AutoPostBack="true"
                                    CssClass=" ddlStype" OnSelectedIndexChanged="ddlPaymentCurrency_SelectedIndexChanged">
                                </asp:DropDownList>
                            </div>
                            <div class="colspace  LNOrient">
                                &nbsp</div>
                            <div class="lblBox  LNOrient" style="width: 150px" id="lblExchangeRateType">
                                Exchange Rate Type
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 150px">
                                <asp:DropDownList ID="ddlExchangeRateType" runat="server" TabIndex="13" CssClass="ddlStype"
                                    OnSelectedIndexChanged="ddlExchangeRateType_TextChanged" AutoPostBack="true">
                                </asp:DropDownList>
                            </div>
                            <div class="colspace  LNOrient" >
                                &nbsp</div>
                            <div class="lblBox  LNOrient" style=" width: 150px" id="lblExchangeRate">
                                Exchange Rate
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 150px">
                                <asp:TextBox ID="txtExchangeRate" runat="server" TabIndex="14" CssClass="txtBox_N"></asp:TextBox>
                            </div>
                            <div class="colspace  LNOrient" >
                                &nbsp</div>
                        </div>
                        <div class="divClear_10">
                        </div>
                        <div class="divRowContainer">
                            <div class="lblBox  LNOrient" style="width: 150px" id="Div4">
                                Payment Amount
                            </div>
                            <div class="divtxtBox LNOrient" style=" width: 150px">
                                <asp:TextBox ID="txtTotalPaymentAmt" runat="server" CssClass="txtBox_N" Enabled="false"
                                    TabIndex="15"></asp:TextBox>
                                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender30" runat="server" FilterType="Numbers,Custom"
                                    ValidChars=".," TargetControlID="txtTotalPaymentAmt" />
                            </div>
                            <div class="lblBox LNOrient" style="width: 350px;" id="Div15">
                                <asp:Label ID="lblWaitApprove" runat="server" Text="WAITING FOR APPROVAL" Visible="false"
                                    CssClass="lblBox" Font-Size="18px" Font-Bold="true"></asp:Label></div>
                        </div>
                        <div class="divClear_10">
                        </div>
                        <div class="divRowContainer" runat="server" id="divgloacccode" visible="false">
                            <div class="lblBox" style="float: left; width: 150px" id="lblGlobalSegment">
                                Default Segment
                            </div>
                            <div class="divtxtBox  LNOrient" style=" width: 155px">
                                <asp:DropDownList ID="ddlGlobalSegment" runat="server" CssClass="validate[required] RequiredField ddlStype"
                                    TabIndex="17">
                                    <asp:ListItem Value="" Text="---Select---"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="lblBox LNOrient" style="width: 150px" id="Div3">
                                Account Code
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 155px">
                                <asp:DropDownList ID="ddlAccountCode" runat="server" TabIndex="18" CssClass=" ddlStype"
                                    AutoPostBack="True">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="divClear_10">
                        </div>
                        <asp:HiddenField runat="server" ID="HDTotPayment" Value="0" />
                        <asp:HiddenField runat="server" ID="hdTotRetnAmt" Value="0" />
                        <asp:HiddenField runat="server" ID="hdOCAmt" Value="0" />
                    </asp:Panel>
                </td>
                <td valign="top">
                    <table>
                        <tr>
                            <td>
                                <asp:ImageButton ID="btnPrint" runat="server" ImageUrl="~/Images/Print.png" OnClick="imgBtnPrint_Click"
                                    Style="border: 0px" />
                            </td>
                        </tr>
                         <tr>
                            <td>
                                  <asp:ImageButton ID="imgbtnPaymentCancel" runat="server" ImageUrl="~/Images/stopPay.png"
                                    Style="border: 0px" onclick="imgbtnPaymentCancel_Click"  />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:ImageButton ID="btnChequePrint" runat="server" ImageUrl="~/Images/printCheque.png"
                                    OnClick="imgBtnChequePrint_Click" Style="border: 0px" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:ImageButton ID="imgBtnPost" runat="server" ImageUrl="~/Images/Post.png" OnClick="imgBtnPost_Click"
                                    Style="border: 0px" OnClientClick="fn_PostVisible('FINContent_imgBtnPost')" />
                                <asp:Label ID="lblPosted" runat="server" Text="POSTED" Visible="false" CssClass="lblBox"
                                    Font-Size="18px" Font-Bold="true" Style="color: Green"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:ImageButton ID="imgBtnJVPrint" runat="server" ImageUrl="~/Images/jvPrint.png"
                                    Style="border: 0px" OnClick="imgBtnJVPrint_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:ImageButton ID="btnStopPayment" runat="server" ImageUrl="~/Images/stopPay.png"
                                    OnClick="imgStopPayment_Click" Style="border: 0px" />
                                <asp:Label ID="lblCancelled" runat="server" Text="CANCELLED" Visible="false" CssClass="lblBox"
                                    Font-Size="18px" Font-Bold="true" Style="color: Red"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:ImageButton ID="imgBtnStopPayJV" runat="server" ImageUrl="~/Images/stopPayJV.png"
                                    Style="border: 0px" OnClick="imgBtnStopPayJV_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <div class="divRowContainer">
            <div class="divtxtBox" style="float: right; width: 545px">
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer LNOrient " id="gridDiv" runat="server">
            <asp:Panel runat="server" ID="pnlgridview">
                <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="DisplayFont Grid"
                    OnRowCancelingEdit="gvData_RowCancelingEdit" OnRowCommand="gvData_RowCommand"
                    OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound" OnRowDeleting="gvData_RowDeleting"
                    OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating" ShowFooter="True"
                    DataKeyNames="line_no,pay_dtl_id,pay_invoice_id,pay_invoice_amt,inv_date">
                    <Columns>
                        <asp:TemplateField HeaderText="">
                            <ItemTemplate>
                                <asp:ImageButton ID="ibtnEdit" runat="server" AlternateText="Edit" CausesValidation="false"
                                    TabIndex="26" ToolTip="Edit" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                                <asp:ImageButton ID="ibtnDelete" runat="server" AlternateText="Delete" CausesValidation="false"
                                    TabIndex="27" ToolTip="Delete" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:ImageButton ID="ibtnUpdate" runat="server" AlternateText="Update" CommandName="Update"
                                    TabIndex="28" ToolTip="Update" Height="20px" ImageUrl="~/Images/Update.png" />
                                <asp:ImageButton ID="ibtnCancel" runat="server" AlternateText="Cancel" CausesValidation="false"
                                    TabIndex="29" ToolTip="Cancel" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:ImageButton ID="ibtnInsert" runat="server" AlternateText="Add" CommandName="FooterInsert"
                                    TabIndex="25" ToolTip="Add" ImageUrl="~/Images/Add.jpg" />
                            </FooterTemplate>
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <%--  <asp:TemplateField HeaderText="Line No">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtLineNo" runat="server" Enabled="false" Text='<%# Eval("line_no") %>'
                                    TabIndex="19" Width="50px" CssClass="EntryFont RequiredField txtBox" MaxLength="10"></asp:TextBox>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtLineNo" runat="server" MaxLength="10" Enabled="false" CssClass="EntryFont RequiredField txtBox"
                                    Width="50px" TabIndex="19"></asp:TextBox>
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblLineNumber" Visible="false" runat="server" Text='<%# Eval("line_no") %>'
                                    Width="50px"></asp:Label>
                                <asp:Label ID="lblLineNo" runat="server" Text='<%# Container.DataItemIndex + 1 %>'
                                    Width="50px"></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>--%>
                        <asp:TemplateField HeaderText="Invoice Number">
                            <EditItemTemplate>
                                <asp:DropDownList ID="ddlInvoiceNumber" runat="server" CssClass="ddlStype RequiredField"
                                    AutoPostBack="true" OnSelectedIndexChanged="ddlInvoiceNumber_SelectedIndexChanged"
                                    TabIndex="20" Width="200px">
                                </asp:DropDownList>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:DropDownList ID="ddlInvoiceNumber" runat="server" CssClass="ddlStype RequiredField"
                                    AutoPostBack="true" OnSelectedIndexChanged="ddlInvoiceNumber_SelectedIndexChanged"
                                    TabIndex="20" Width="200px">
                                </asp:DropDownList>
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblInvoiceNumber" runat="server" Text='<%# Eval("INV_NUM") %>' Width="150px"></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Invoice Date">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtInvoiceDate" MaxLength="20" runat="server" Text='<%# Eval("inv_date","{0:dd/MM/yyyy}") %>'
                                    TabIndex="21" Enabled="false" CssClass="EntryFont txtBox" Width="120px"></asp:TextBox>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtInvoiceDate" MaxLength="20" runat="server" CssClass="EntryFont txtBox"
                                    TabIndex="21" Enabled="false" Width="120px"></asp:TextBox>
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblInvoiceDate" MaxLength="250" runat="server" Text='<%# Eval("inv_date","{0:dd/MM/yyyy}") %>'
                                    Width="100px"></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Invoice Amount">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtInvoiceAmount" MaxLength="13" runat="server" Text='<%# Eval("inv_amt") %>'
                                    Enabled="false" CssClass="EntryFont RequiredField txtBox_N" Width="120px" TabIndex="19"></asp:TextBox>
                                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender31" runat="server" FilterType="Numbers,Custom"
                                    ValidChars=".," TargetControlID="txtInvoiceAmount" />
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtInvoiceAmount" MaxLength="13" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                    Enabled="false" Width="120px" TabIndex="19"></asp:TextBox>
                                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender31" runat="server" FilterType="Numbers,Custom"
                                    ValidChars=".," TargetControlID="txtInvoiceAmount" />
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblInvoiceAmount" runat="server" Text='<%# Eval("inv_amt") %>' Width="80px"></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Retention Amount">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtRetentionAmount" MaxLength="13" runat="server" Text='<%# Eval("inv_retention_amt") %>'
                                    Enabled="false" CssClass="EntryFont RequiredField txtBox_N" Width="120px" TabIndex="19"></asp:TextBox>
                                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender32" runat="server" FilterType="Numbers,Custom"
                                    ValidChars=".," TargetControlID="txtRetentionAmount" />
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtRetentionAmount" MaxLength="13" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                    Enabled="false" Width="120px" TabIndex="19"></asp:TextBox>
                                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender32" runat="server" FilterType="Numbers,Custom"
                                    ValidChars=".," TargetControlID="txtRetentionAmount" />
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblrententionAmount" runat="server" Text='<%# Eval("inv_retention_amt") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Amount Paid">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtAmountPaid" MaxLength="13" runat="server" Text='<%# Eval("INV_PAID_AMT") %>'
                                    CssClass="EntryFont RequiredField txtBox_N" Enabled="false" Width="120px" TabIndex="22">
                                </asp:TextBox><cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender21" runat="server"
                                    FilterType="Numbers,Custom" ValidChars=".," TargetControlID="txtAmountPaid" />
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtAmountPaid" MaxLength="13" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                    Width="120px" TabIndex="22" Enabled="false"></asp:TextBox><cc2:FilteredTextBoxExtender
                                        ID="FilteredTextBoxExtender21" runat="server" FilterType="Numbers,Custom" ValidChars=".,"
                                        TargetControlID="txtAmountPaid" />
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblAmountPaid" runat="server" Text='<%# Eval("INV_PAID_AMT") %>' Width="100px"></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Payment Amount">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtPaymentAmount" MaxLength="13" runat="server" Text='<%# Eval("pay_invoice_paid_amt") %>'
                                    OnTextChanged="txtPaymentAmount_TextChanged" AutoPostBack="true" CssClass="EntryFont RequiredField txtBox_N"
                                    Width="120px" TabIndex="22">
                                </asp:TextBox><cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender211" runat="server"
                                    FilterType="Numbers,Custom" ValidChars=".," TargetControlID="txtPaymentAmount" />
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtPaymentAmount" MaxLength="13" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                    OnTextChanged="txtPaymentAmount_TextChanged" AutoPostBack="true" Width="120px"
                                    TabIndex="22"></asp:TextBox><cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender211"
                                        runat="server" FilterType="Numbers,Custom" ValidChars=".," TargetControlID="txtPaymentAmount" />
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblPaymentAmount" runat="server" Text='<%# Eval("pay_invoice_paid_amt") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Retention Amount">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtPayRetentionAmount" MaxLength="13" runat="server" Text='<%# Eval("PAY_RETENTIION_AMT") %>'
                                    OnTextChanged="txtPayRetentionAmount_TextChanged" CssClass="EntryFont RequiredField txtBox_N"
                                    Width="120px" TabIndex="22">
                                </asp:TextBox><cc2:FilteredTextBoxExtender ID="ftbetxtPayRetentionAmount" runat="server"
                                    FilterType="Numbers,Custom" ValidChars=".," TargetControlID="txtPayRetentionAmount" />
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtPayRetentionAmount" MaxLength="13" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                    OnTextChanged="txtPayRetentionAmount_TextChanged" Width="120px" TabIndex="22"></asp:TextBox><cc2:FilteredTextBoxExtender
                                        ID="febetxtPayRetentionAmount1" runat="server" FilterType="Numbers,Custom" ValidChars=".,"
                                        TargetControlID="txtPayRetentionAmount" />
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblPayRetentionAmount" runat="server" Text='<%# Eval("PAY_RETENTIION_AMT") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Balance Amount">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtAmountToPay" Text='<%# Eval("pay_invoice_amt") %>' runat="server"
                                    Enabled="false" MaxLength="13" CssClass="EntryFont RequiredField txtBox_N" Width="120px"
                                    TabIndex="23"></asp:TextBox>
                                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender33" runat="server" FilterType="Numbers,Custom"
                                    ValidChars=".," TargetControlID="txtAmountToPay" />
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtAmountToPay" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                    Enabled="false" MaxLength="13" Width="120px" TabIndex="23"></asp:TextBox>
                                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender33" runat="server" FilterType="Numbers,Custom"
                                    ValidChars=".," TargetControlID="txtAmountToPay" />
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblAmountToPay" runat="server" Text='<%# Eval("pay_invoice_amt") %>'
                                    Width="100px"></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" ItemStyle-Width="50px" ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Button ID="btnView" runat="server" CssClass="btn" Text="View" OnClick="btnInvoiceOpen_Click"
                                    TabIndex="24" />
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Button ID="btnView" runat="server" CssClass="btn" Text="View" OnClick="btnInvoiceOpen_Click"
                                    TabIndex="24" />
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:Button ID="btnView" runat="server" CssClass="btn" Text="View" OnClick="btnInvoiceOpen_Click"
                                    TabIndex="24" />
                            </FooterTemplate>
                            <FooterStyle VerticalAlign="Top" />
                            <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Right"></ItemStyle>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                    <HeaderStyle CssClass="GridHeader" />
                    <AlternatingRowStyle CssClass="GrdAltRow" />
                </asp:GridView>
            </asp:Panel>
        </div>
        <div class="divClear_10">
        </div> <div class="divClear_10">
        </div>
        <div class="divRowContainer  LNOrient" >
            <div style="width: 100%">
                <div style=" padding-top: 5px;"  class="lblBox GridHeader LNOrient">
                    Additions/Deductions (if any)
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <asp:Panel runat="server" ID="pnlAcc">
                <asp:GridView ID="gv_pay_OC" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                    Width="800px" DataKeyNames="POC_ID,ACCT_CODE_ID,DELETED,AddSub_value" OnRowCancelingEdit="gv_pay_OC_RowCancelingEdit"
                    OnRowCommand="gv_pay_OC_RowCommand" OnRowCreated="gv_pay_OC_RowCreated" OnRowDataBound="gv_pay_OC_RowDataBound"
                    OnRowDeleting="gv_pay_OC_RowDeleting" OnRowEditing="gv_pay_OC_RowEditing" OnRowUpdating="gv_pay_OC_RowUpdating"
                    ShowFooter="True">
                    <Columns>
                        <asp:TemplateField HeaderText="Account Code">
                            <EditItemTemplate>
                                <asp:DropDownList ID="ddlAccountCode" TabIndex="31" runat="server" CssClass="RequiredField EntryFont ddlStype">
                                </asp:DropDownList>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:DropDownList ID="ddlAccountCode" TabIndex="31" runat="server" CssClass="RequiredField EntryFont ddlStype">
                                </asp:DropDownList>
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblAccountCode" CssClass="adminFormFieldHeading" runat="server" Width="95%"
                                    Text='<%# Eval("ACCT_CODE_DESC") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Add/Deduct">
                            <EditItemTemplate>
                                <asp:DropDownList ID="ddlAddSub" TabIndex="31" runat="server" CssClass="RequiredField EntryFont ddlStype">
                                  <asp:ListItem Text="---Select---" Value=""></asp:ListItem>
                                <asp:ListItem Text="Add" Value="Add"></asp:ListItem>
                                <asp:ListItem Text="Deduct" Value="Deduct"></asp:ListItem>
                                </asp:DropDownList>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:DropDownList ID="ddlAddSub" TabIndex="31" runat="server" CssClass="RequiredField EntryFont ddlStype">
                               <asp:ListItem Text="---Select---" Value=""></asp:ListItem>
                                <asp:ListItem Text="Add" Value="Add"></asp:ListItem>
                                <asp:ListItem Text="Deduct" Value="Deduct"></asp:ListItem>    </asp:DropDownList>
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblAddSub" CssClass="adminFormFieldHeading" runat="server" Width="95%"
                                    Text='<%# Eval("AddSub_value") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Amount">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtPOCAmount" TabIndex="32" runat="server" CssClass="  EntryFont  txtBox_N"
                                    Width="95%" Text='<%# Eval("POC_AMOUNT") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtPOCAmount" TabIndex="32" runat="server" CssClass="  EntryFont  txtBox_N"
                                    Width="95%"></asp:TextBox>
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblPOCAmount" runat="server" Width="95%" Text='<%# Eval("POC_AMOUNT") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Remarks">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtPOCRemarks" TabIndex="33" runat="server" CssClass="  EntryFont  txtBox"
                                    Width="95%" Text='<%# Eval("REMARKS") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtPOCRemarks" TabIndex="33" runat="server" CssClass="  EntryFont  txtBox"
                                    Width="95%"></asp:TextBox>
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblPOCRemarks" runat="server" Width="95%" Text='<%# Eval("REMARKS") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="">
                            <ItemTemplate>
                                <asp:ImageButton ID="ibtnEdit" runat="server" AlternateText="Edit" CausesValidation="false"
                                    TabIndex="35" ToolTip="Edit" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                                <asp:ImageButton ID="ibtnDelete" runat="server" AlternateText="Delete" CausesValidation="false"
                                    TabIndex="36" ToolTip="Delete" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:ImageButton ID="ibtnUpdate" runat="server" AlternateText="Update" CommandName="Update"
                                    TabIndex="37" ToolTip="Update" Height="20px" ImageUrl="~/Images/Update.png" />
                                <asp:ImageButton ID="ibtnCancel" runat="server" AlternateText="Cancel" CausesValidation="false"
                                    TabIndex="38" ToolTip="Cancel" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:ImageButton ID="ibtnInsert" TabIndex="34" runat="server" AlternateText="Add"
                                    ToolTip="Add" CommandName="FooterInsert" ImageUrl="~/Images/Add.jpg" />
                            </FooterTemplate>
                            <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Center" />
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                    <HeaderStyle CssClass="GridHeader" />
                    <AlternatingRowStyle CssClass="GrdAltRow" />
                </asp:GridView>
            </asp:Panel>
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                            TabIndex="9" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="10" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="11" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="12" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table width="100%">
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
        <div id="div6">
            <asp:HiddenField runat="server" ID="btnView1" />
            <cc2:ModalPopupExtender ID="ModalPopupExtender2" runat="server" TargetControlID="btnView1"
                PopupControlID="Panel1" CancelControlID="Button2" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="Panel1" runat="server">
                <div class="ConfirmForm">
                    <table width="100%">
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <iframe id="ifrmInvoice" name="centerfrm" class="frames" frameborder="0" width="1000px"
                                    height="320px" allowtransparency="true" style="border-style: solid; border-width: 0px;
                                    background-color: transparent;" src="InvoicePopup.aspx" runat="server"></iframe>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="Button2" Text="Close" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
        <div class=" LNOrient">
            <asp:HiddenField runat="server" ID="btnStopPayment1" />
            <cc2:ModalPopupExtender ID="popStopPayment" runat="server" TargetControlID="btnStopPayment1"
                PopupControlID="Panel2" CancelControlID="btnClose" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="Panel2" runat="server">
                <div class="ConfirmForm">
                    <div class="divRowContainer" runat="server" id="div8">
                        <div class="lblBox  LNOrient" style=" width: 150px" id="Div9">
                            Payment Stop Date
                        </div>
                        <div class="divtxtBox LNOrient" style="width: 150px">
                            <asp:TextBox ID="txtStopDate" runat="server" CssClass="validate[] validate[custom[ReqDateDDMMYYY]]  txtBox"
                                Width="100%" TabIndex="1"></asp:TextBox>
                            <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender2" TargetControlID="txtStopDate"
                                OnClientDateSelectionChanged="checkDate" />
                        </div>
                        <div class="lblBox LNOrient" style="width: 150px" id="Div10">
                            Payment Reason
                        </div>
                        <div class="divtxtBox LNOrient" style="width: 150px">
                            <asp:TextBox ID="txtStopPaymentReason" MaxLength="50" runat="server" CssClass="validate[]   txtBox"
                                Width="100%" TabIndex="2"></asp:TextBox>
                        </div>
                    </div>
                    <div class="divClear_10">
                    </div>
                    <div class="divRowContainer" runat="server" id="div11">
                        <div class="lblBox LNOrient" style="width: 150px" id="Div12">
                            Payment Remarks
                        </div>
                        <div class="divtxtBox  LNOrient" style=" width: 150px">
                            <asp:TextBox ID="txtStopPayRemarks" MaxLength="500" runat="server" CssClass="validate[]   txtBox"
                                Width="100%" TabIndex="3"></asp:TextBox>
                        </div>
                    </div>
                    <div class="divRowContainer" runat="server" id="div13">
                        <div class=" LNOrient" style=" width: 150px" id="Div14">
                            <asp:Button runat="server" OnClick="btnUpdateStopPayment_Click" CssClass="button"
                                ID="btnUpdateStopPayment" Text="Submit" Width="60px" />
                        </div>
                        <div class="divtxtBox  LNOrient" style=" width: 150px">
                            <asp:Button runat="server" CssClass="button" ID="btnClose" Text="Cancel" Width="60px"
                                OnClick="btnClose_Click" />
                        </div>
                    </div>
                    <div class="divClear_10">
                    </div>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/AP/APChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
       

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

    </script>
</asp:Content>
