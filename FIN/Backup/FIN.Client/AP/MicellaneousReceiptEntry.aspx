﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="MicellaneousReceiptEntry.aspx.cs" Inherits="FIN.Client.AP.MicellaneousReceiptEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 850px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style=" width: 200px" id="lblGRNNumber" runat="server">
                Goods Received Note Number
            </div>
            <div class="divtxtBox LNOrient" style=" width: 154px" id="lblGRNNumValue" runat="server">
                <asp:TextBox ID="txtGRNNumber" runat="server" MaxLength="50" Enabled="true" CssClass=" txtBox"
                    TabIndex="1"></asp:TextBox>
            </div>
            <div class="colspace LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 185px" id="lblGRNDate">
                Goods Received Note Date
            </div>
            <div class="divtxtBox LNOrient" style="width: 154px">
                <asp:TextBox ID="txtGRNDate" runat="server" TabIndex="2" CssClass="validate[required] RequiredField txtBox"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calExtender1" TargetControlID="txtGRNDate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style=" width: 200px" id="lblSupplierName ">
                Supplier Name
            </div>
            <div class="divtxtBox LNOrient" style="width: 515px">
                <asp:DropDownList ID="ddlSupplierName" runat="server" TabIndex="3" AutoPostBack="true"
                    CssClass="ddlStype RequiredField" OnSelectedIndexChanged="ddlSupplierName_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="Div1">
                Supplier Site
            </div>
            <div class="divtxtBox LNOrient" style="width: 515px">
                <asp:DropDownList ID="ddlSupplierSite" runat="server" TabIndex="4" CssClass="ddlStype ">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblEmployeeName">
                Received By
            </div>
            <div class="divtxtBox LNOrient" style=" width: 515px">
                <asp:DropDownList ID="ddlEmployeename" runat="server" Enabled="true" CssClass="ddlStype"
                    TabIndex="5">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="Div12">
                Initiated By
            </div>
            <div class="divtxtBox LNOrient" style=" width: 515px">
                <asp:DropDownList ID="ddlInitiatedBy" runat="server" Enabled="true" CssClass="ddlStype"
                    TabIndex="6">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="Div13">
                Approved By
            </div>
            <div class="divtxtBox LNOrient" style="width: 515px">
                <asp:DropDownList ID="ddlApprovedBy" runat="server" Enabled="true" CssClass="ddlStype"
                    TabIndex="7">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblModeofTransport">
                Mode of Transport
            </div>
            <div class="divtxtBox LNOrient" style=" width: 150px">
                <asp:DropDownList ID="ddlModeofTransport" runat="server" CssClass="ddlStype" TabIndex="8">
                </asp:DropDownList>
            </div>
            <div class="colspace LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 200px" id="lblInspectionRequired">
                Inspection Required
            </div>
            <div class="divtxtBox LNOrient" style=" width: 150px">
                <asp:CheckBox ID="chkInspection" runat="server" TabIndex="9" OnCheckedChanged="chkInspection_CheckedChanged"
                    AutoPostBack="true" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer LNOrient">
            <h1 class="lblBox" style="font-weight: normal; font-size: 14px;">
                Shipment Details</h1>
        </div>
        <div class="divClear_10">
        </div>
        <div class="ship-bg">
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="width: 200px" id="lblShipmentNumber">
                    Shipment Number
                </div>
                <div class="divtxtBox LNOrient" style="width: 150px">
                    <asp:TextBox ID="txtShipmentNumber" runat="server" CssClass="txtBox_N" TabIndex="10"
                        MaxLength="50" OnTextChanged="txtShipmentNumber_TextChanged"></asp:TextBox>
                </div>
                <div class="colspace LNOrient" >
                    &nbsp</div>
                <div class="lblBox LNOrient" style="width: 192px" id="lblShipmentDate">
                    Shipment Date
                </div>
                <div class="divtxtBox LNOrient" style="width: 150px">
                    <asp:TextBox ID="txtShipmentDate" CssClass="txtBox" runat="server" TabIndex="11"
                        MaxLength="50"></asp:TextBox>
                    <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="shipCal" TargetControlID="txtShipmentDate"
                        OnClientDateSelectionChanged="checkDate" />
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="width: 200px" id="lblShipmentTrackingNo">
                    Shipment Tracking Number
                </div>
                <div class="divtxtBox LNOrient" style="width: 150px">
                    <asp:TextBox ID="txtShipmentTrackingNo" CssClass="txtBox_N" runat="server" TabIndex="12"
                        MaxLength="50"></asp:TextBox>
                </div>
                <div class="colspace LNOrient" >
                    &nbsp</div>
                <div class="lblBox LNOrient" style="width: 192px" id="lblShipmentReceivedby">
                    Shipment Received By
                </div>
                <div class="divtxtBox LNOrient" style="width: 150px">
                    <asp:TextBox ID="txtShipmentReceivedby" CssClass="txtBox" runat="server" TabIndex="13"
                        MaxLength="50"></asp:TextBox>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="width: 200px" id="lblShipmentCompany">
                    Shipment Company
                </div>
                <div class="divtxtBox LNOrient" style="width: 150px">
                    <asp:TextBox ID="txtShipmentCompany" CssClass="txtBox" runat="server" TabIndex="14"
                        MaxLength="50"></asp:TextBox>
                </div>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer LNOrient " style="height: 200px; overflow: scroll">
            <asp:HiddenField runat="server" ID="hdItemId" />
            <asp:HiddenField runat="server" ID="hdLineId" />
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" Width="800px"
                CssClass="DisplayFont Grid" OnRowCancelingEdit="gvData_RowCancelingEdit" OnRowCommand="gvData_RowCommand"
                OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound" OnRowDeleting="gvData_RowDeleting"
                OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating" ShowFooter="True"
                DataKeyNames="RECEIPT_ID,RECEIPT_DTL_ID,PO_LINE_NUM,PO_HEADER_ID,PO_LINE_ID,ITEM_ID,LINE_NUM">
                <Columns>
                    <asp:TemplateField HeaderText="Line No">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtLineNo" runat="server" Enabled="false" Text='<%# Eval("LINE_NUM") %>'
                                TabIndex="15" Width="50px" CssClass="EntryFont RequiredField txtBox" MaxLength="50"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtLineNo" runat="server" MaxLength="50" Enabled="false" TabIndex="15"
                                CssClass="EntryFont RequiredField txtBox" Width="50px"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblLineNo" runat="server" Text='<%# Container.DataItemIndex + 1 %>'
                                Width="50px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                  <%--  <asp:TemplateField>
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlPoNumber" runat="server" CssClass="ddlStype" AutoPostBack="true"
                                Style="display: none;"  TabIndex="16" OnSelectedIndexChanged="ddlPoNumber_SelectedIndexChanged">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlPoNumber" runat="server" CssClass="ddlStype" AutoPostBack="true"
                                Style="display: none;"  TabIndex="16" OnSelectedIndexChanged="ddlPoNumber_SelectedIndexChanged">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblPONumber" runat="server" Text='<%# Eval("PO_NUM") %>' Style="display: none;"
                                Visible="false"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtPOLineNo" runat="server" MaxLength="250" Text='<%# Eval("PO_LINE_NUM") %>'
                                Style="display: none;" TabIndex="17" Enabled="false" CssClass="EntryFont RequiredField txtBox"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtPOLineNo" MaxLength="250" runat="server" Enabled="false" Style="display: none;"
                                TabIndex="17" CssClass="EntryFont RequiredField txtBox"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblPOLineNo" runat="server" Text='<%# Eval("PO_LINE_NUM") %>' Width="60px"
                                Visible="false"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>--%>
                    <asp:TemplateField HeaderText="Item/Description">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlItem" runat="server" CssClass="ddlStype" Width="150px" TabIndex="18">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlItem" runat="server" CssClass="ddlStype" Width="150px" TabIndex="18">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblItemName" runat="server" Text='<%# Eval("ITEM_NAME") %>' Width="250px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Purchase Order Quantity" Visible="false">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtQuantity" MaxLength="10" runat="server" Text='<%# Eval("PO_QUANTITY") %>'
                                TabIndex="19" CssClass="EntryFont  txtBox_N" Width="80px"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers"
                                TargetControlID="txtQuantity" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtQuantity" MaxLength="10" runat="server" CssClass="EntryFont txtBox_N"
                                TabIndex="19" Width="80px"></asp:TextBox><cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender66"
                                    runat="server" FilterType="Numbers" TargetControlID="txtQuantity" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblQuantity" runat="server" Text='<%# Eval("PO_QUANTITY") %>' Width="80px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Received Quantity">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtReceivedQuantity" MaxLength="10" runat="server" Text='<%# Eval("QTY_RECEIVED") %>'
                                TabIndex="20" CssClass="EntryFont RequiredField txtBox_N" Width="100px"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender27" runat="server" FilterType="Numbers"
                                TargetControlID="txtReceivedQuantity" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtReceivedQuantity" MaxLength="10" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                TabIndex="20" Width="100px"></asp:TextBox><cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender23"
                                    runat="server" FilterType="Numbers" TargetControlID="txtReceivedQuantity" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblReceivedQuantity" runat="server" Width="100px" Text='<%# Eval("QTY_RECEIVED") %>'
                                ></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Approved Quantity">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtApprovedQuantity" MaxLength="10" runat="server" Text='<%# Eval("QTY_APPROVED") %>'
                                TabIndex="21" CssClass="EntryFont RequiredField txtBox_N" Width="100px"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxE3xtender27" runat="server" FilterType="Numbers"
                                TargetControlID="txtApprovedQuantity" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtApprovedQuantity" MaxLength="10" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                TabIndex="21" Width="100px"></asp:TextBox><cc2:FilteredTextBoxExtender ID="FilteredText3BoxExtender23"
                                    runat="server" FilterType="Numbers" TargetControlID="txtApprovedQuantity" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblaPPROVEDQuantity" Width="100px" runat="server" Text='<%# Eval("QTY_APPROVED") %>'
                                ></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Rejected Quantity">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtRejectedQuantity" MaxLength="10" runat="server" Text='<%# Eval("QTY_REJECTED") %>'
                                TabIndex="22" CssClass="EntryFont RequiredField txtBox_N" Width="100px"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender28" runat="server" FilterType="Numbers"
                                TargetControlID="txtRejectedQuantity" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtRejectedQuantity" MaxLength="10" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                TabIndex="22" Width="100px"></asp:TextBox><cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender_23"
                                    runat="server" FilterType="Numbers" TargetControlID="txtRejectedQuantity" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblRejectedQuantity" runat="server" Text='<%# Eval("QTY_REJECTED") %>'
                                Width="100px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Remarks">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtRemarks" MaxLength="250" runat="server" Text='<%# Eval("REMARKS") %>'
                                TabIndex="23" CssClass="EntryFont txtBox" Width="200px"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtRemarks" MaxLength="250" runat="server" CssClass="EntryFont txtBox"
                                TabIndex="23" Width="200px"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblREMARKS" runat="server" Text='<%# Eval("REMARKS") %>' Width="200px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Add/Edit">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" runat="server" AlternateText="Edit" CausesValidation="false"
                                TabIndex="24" ToolTip="Edit" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                            <asp:ImageButton ID="ibtnDelete" runat="server" AlternateText="Delete" CausesValidation="false"
                                TabIndex="25" ToolTip="Delete" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibtnUpdate" runat="server" AlternateText="Update" CommandName="Update"
                                TabIndex="26" Height="20px" ImageUrl="~/Images/Update.png" />
                            <asp:ImageButton ID="ibtnCancel" runat="server" AlternateText="Cancel" CausesValidation="false"
                                TabIndex="27" ToolTip="Add" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="ibtnInsert" runat="server" AlternateText="Add" CommandName="FooterInsert"
                                TabIndex="28" ToolTip="Add" ImageUrl="~/Images/Add.jpg" />
                        </FooterTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                            TabIndex="20" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="21" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="22" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="13" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table width="100%">
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/AP/APChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
