﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.AP;
using FIN.BLL;
using FIN.BLL.AP;
using FIN.BLL.HR;
using VMVServices.Web;
using System.Data;

namespace FIN.Client.AP
{
    public partial class MicellaneousReceiptEntry : PageBase
    {

        INV_RECEIPT_DTLS INV_RECEIPT_DTLS = new INV_RECEIPT_DTLS();
        INV_RECEIPTS_HDR INV_RECEIPTS_HDR = new INV_RECEIPTS_HDR();

        DataTable dtGridData = new DataTable();
        Boolean bol_rowVisiable;
        Boolean savedBool;
        string ProReturn = null;
        string trnType = string.Empty;
        int trnTypeBool = 0;


        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// .RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }
            UserRightsChecking();

        }
        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }
        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>
        private void FillComboBox()
        {
            Employee_BLL.GetEmployeeName(ref ddlEmployeename);
            Employee_BLL.GetEmployeeName(ref ddlInitiatedBy);
            Employee_BLL.GetEmployeeName(ref ddlApprovedBy);

            Lookup_BLL.GetLookUpValues(ref ddlModeofTransport, "MOT");
            Supplier_BLL.GetSupplierName(ref ddlSupplierName);
        }
        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>
        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();
                ChangeLanguage();

                EntityData = null;
                Session[FINSessionConstants.GridData] = null;

                dtGridData = DBMethod.ExecuteQuery(PurchaseItemReceipt_DAL.GetReceiptDetails(Master.StrRecordId)).Tables[0];
                BindGrid(dtGridData);

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    lblGRNNumber.Visible = true;
                    lblGRNNumValue.Visible = true;

                    INV_RECEIPTS_HDR = PurchaseItemReceipt_BLL.getClassEntity(Master.StrRecordId);

                    EntityData = INV_RECEIPTS_HDR;

                    txtGRNNumber.Text = INV_RECEIPTS_HDR.GRN_NUM;
                    if (INV_RECEIPTS_HDR.RECEIPT_DATE != null)
                    {
                        txtGRNDate.Text = DBMethod.ConvertDateToString(INV_RECEIPTS_HDR.RECEIPT_DATE.ToString());
                    }
                    chkInspection.Checked = INV_RECEIPTS_HDR.INSPECTION_REQ_YN == "1" ? true : false;
                    if (INV_RECEIPTS_HDR.EMP_ID != null)
                    {
                        ddlEmployeename.SelectedValue = INV_RECEIPTS_HDR.EMP_ID.ToString();
                    }
                    if (INV_RECEIPTS_HDR.SHIPMENT_NUMBER != null)
                    {
                        txtShipmentNumber.Text = INV_RECEIPTS_HDR.SHIPMENT_NUMBER.ToString();
                    }
                    if (INV_RECEIPTS_HDR.SHIPMENT_DATE != null)
                    {
                        txtShipmentDate.Text = DBMethod.ConvertDateToString(INV_RECEIPTS_HDR.SHIPMENT_DATE.ToString());
                    }
                    txtShipmentTrackingNo.Text = INV_RECEIPTS_HDR.SHIPMENT_TRACKING_NUMBER;
                    txtShipmentReceivedby.Text = INV_RECEIPTS_HDR.SHIPMENT_RECEIVED_BY;

                    ddlModeofTransport.SelectedValue = INV_RECEIPTS_HDR.SHIPMENT_MODE_OF_TRANSPORT;
                    txtShipmentCompany.Text = INV_RECEIPTS_HDR.SHIPMENT_COMPANY;
                    ddlSupplierName.SelectedValue = INV_RECEIPTS_HDR.VENDOR_ID;

                    if (INV_RECEIPTS_HDR.VENDOR_LOC_ID != null)
                    {
                        SupplierBranch_BLL.fn_getSupplierSite4SupplierName(ref ddlSupplierSite, ddlSupplierName.SelectedValue);

                        ddlSupplierSite.SelectedValue = INV_RECEIPTS_HDR.VENDOR_LOC_ID.ToString();

                    }
                    if (INV_RECEIPTS_HDR.INTIATED_BY != null)
                    {
                        ddlInitiatedBy.SelectedValue = INV_RECEIPTS_HDR.INTIATED_BY.ToString();
                    }
                    if (INV_RECEIPTS_HDR.APPROVED_BY != null)
                    {
                        ddlApprovedBy.SelectedValue = INV_RECEIPTS_HDR.APPROVED_BY.ToString();
                    }
                    if (INV_RECEIPTS_HDR.WORKFLOW_COMPLETION_STATUS == "1")
                    {
                        btnSave.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// .RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        private void ChangeLanguage()
        {
            try
            {
                ErrorCollection.Clear();
                if (VMVServices.Web.Utils.LanguageCode.ToString().Length > 0)
                {
                    Dictionary<string, string> Prop_File_Data;
                    Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/AP_" + Session["Sel_Lng"].ToString() + ".properties"));
                    lblGRNNumber.InnerHtml = Prop_File_Data["Goods_Received_Note_Number_P"];
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("MisReceiptChangeLang", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountCodesEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                //  DropDownList ddlPoNumber = tmpgvr.FindControl("ddlPoNumber") as DropDownList;
                // PurchaseOrder_BLL.GetPODetailsBasedSupplier(ref ddlPoNumber, ddlSupplierName.SelectedValue);
                DropDownList ddlItem = tmpgvr.FindControl("ddlItem") as DropDownList;

                Item_BLL.getItemList_Without_Service(ref ddlItem);

                if (gvData.EditIndex >= 0)
                {
                    //   ddlPoNumber.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.PO_LINE_ID].ToString();
                    ddlItem.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.ITEM_ID].ToString();

                    //   fn_POData();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// .RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            if (Session[FINSessionConstants.GridData] != null)
            {
                dtGridData = (DataTable)Session[FINSessionConstants.GridData];
            }
            gvData.EditIndex = -1;

            BindGrid(dtGridData);

        }
        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// .RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// .RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {
            System.Collections.SortedList slControls = new System.Collections.SortedList();

            TextBox txtLineNo = gvr.FindControl("txtLineNo") as TextBox;
            TextBox txtPOLineNo = gvr.FindControl("txtPOLineNo") as TextBox;
            TextBox txtQuantity = gvr.FindControl("txtQuantity") as TextBox;
            TextBox txtItemName = gvr.FindControl("txtItemName") as TextBox;
            TextBox txtRemarks = gvr.FindControl("txtRemarks") as TextBox;
            TextBox txtReceivedQuantity = gvr.FindControl("txtReceivedQuantity") as TextBox;
            TextBox txtRejectedQuantity = gvr.FindControl("txtRejectedQuantity") as TextBox;
            TextBox txtApprovedQuantity = gvr.FindControl("txtApprovedQuantity") as TextBox;


            DropDownList ddlItem = gvr.FindControl("ddlItem") as DropDownList;

            Label lblLineNo = gvr.FindControl("lblLineNo") as Label;

            DropDownList ddlPoNumber = gvr.FindControl("ddlPoNumber") as DropDownList;


            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList[FINColumnConstants.RECEIPT_ID] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);
            }

            txtLineNo.Text = (rowindex + 1).ToString();

            slControls[0] = ddlItem;
            //slControls[1] = txtQuantity;
            slControls[1] = txtReceivedQuantity;
            // slControls[3] = txtRejectedQuantity;
            slControls[2] = txtApprovedQuantity;
          
            Dictionary<string, string> Prop_File_Data;
           
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/AP_" + Session["Sel_Lng"].ToString() + ".properties"));
          
            ErrorCollection.Clear();

            string strCtrlTypes = FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX;
            string strMessage = Prop_File_Data["Item_Name_P"] + " ~ " + Prop_File_Data["Received_Quantity_P"] + " ~ " + Prop_File_Data["Approved_Quantity_P"] + "";
          
            EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

            if (EmptyErrorCollection.Count > 0)
            {
                ErrorCollection = EmptyErrorCollection;
                return drList;
            }

            ErrorCollection.Clear();

            //EmptyErrorCollection = CommonUtils.IsAnySingleEntry(txtReceivedQuantity.Text, txtRejectedQuantity.Text, txtQuantity.Text);

            //if (EmptyErrorCollection.Count > 0)
            //{
            //    ErrorCollection = EmptyErrorCollection;
            //    return drList;
            //}

            string strCondition = "ITEM_NAME='" + ddlItem.SelectedItem.Text.Trim().ToUpper() + "'";
            strMessage = FINMessageConstatns.RecordAlreadyExists;
            ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            if (ErrorCollection.Count > 0)
            {
                return drList;
            }
            

            if (CommonUtils.ConvertStringToInt(txtReceivedQuantity.Text) < CommonUtils.ConvertStringToInt(txtApprovedQuantity.Text))
            {
                ErrorCollection.Add("recapp", "Approved quantity should not be greater than the Received Quantity");
            }
            if (ErrorCollection.Count > 0)
            {
                return drList;
            }

            if (CommonUtils.ConvertStringToInt(txtReceivedQuantity.Text) < CommonUtils.ConvertStringToInt(txtRejectedQuantity.Text))
            {
                ErrorCollection.Add("rejectedapp", "Rejected quantity should not be greater than the Received Quantity");
            }
            if (ErrorCollection.Count > 0)
            {
                return drList;
            }


            if (CommonUtils.ConvertStringToInt(txtReceivedQuantity.Text) != (CommonUtils.ConvertStringToInt(txtRejectedQuantity.Text) + CommonUtils.ConvertStringToInt(txtApprovedQuantity.Text)))
            {
                ErrorCollection.Add("rec", "Received quantity should be equal to Rejected quantity + Approved quantity");
            }
            if (ErrorCollection.Count > 0)
            {
                return drList;
            }

            //if (CommonUtils.ConvertStringToInt(txtQuantity.Text) < CommonUtils.ConvertStringToInt(txtRejectedQuantity.Text))
            //{
            //    ErrorCollection.Add("rec1", "Rejected quantity should not be greater than the PO Quantity");
            //}
            //if (ErrorCollection.Count > 0)
            //{
            //    return drList;
            //}
            //if (CommonUtils.ConvertStringToInt(txtQuantity.Text) < (CommonUtils.ConvertStringToInt(txtRejectedQuantity.Text) + CommonUtils.ConvertStringToInt(txtReceivedQuantity.Text)))
            //{
            //    ErrorCollection.Add("rec2", "PO Quantity should not be greater than the Received quantity + Rejected quantity");
            //}
            if (ErrorCollection.Count > 0)
            {
                return drList;
            }

            drList[FINColumnConstants.LINE_NUM] = txtLineNo.Text;

            //if (ddlPoNumber.SelectedValue != string.Empty)
            //{
            //    drList[FINColumnConstants.PO_NUM] = ddlPoNumber.SelectedItem.Text.ToString();
            //}

            //drList[FINColumnConstants.PO_HEADER_ID] = (hdLineId.Value);

            //if (hdLineId.Value != string.Empty)
            //{
            //    drList[FINColumnConstants.PO_LINE_ID] = ddlPoNumber.SelectedValue.ToString();
            //}
            //if (hdItemId.Value != string.Empty)
            //{
            drList[FINColumnConstants.ITEM_ID] = (ddlItem.SelectedValue);
            // }

            //drList[FINColumnConstants.PO_LINE_NUM] = txtPOLineNo.Text == string.Empty ? "0" : txtPOLineNo.Text;

            drList[FINColumnConstants.ITEM_NAME] = ddlItem.SelectedItem.Text;
            drList[FINColumnConstants.PO_QUANTITY] = CommonUtils.ConvertStringToInt(txtQuantity.Text);

            drList[FINColumnConstants.QTY_RECEIVED] = CommonUtils.ConvertStringToInt(txtReceivedQuantity.Text);
            drList[FINColumnConstants.QTY_REJECTED] = CommonUtils.ConvertStringToInt(txtRejectedQuantity.Text);
            drList["QTY_APPROVED"] = CommonUtils.ConvertStringToInt(txtApprovedQuantity.Text);


            drList[FINColumnConstants.REMARKS] = txtRemarks.Text;
            drList[FINColumnConstants.DELETED] = FINAppConstants.N;


            return drList;

        }


        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// .RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// .RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// .RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// .RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// .RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// .RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        #endregion

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                AssignToBE();

                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// .RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    INV_RECEIPTS_HDR = (INV_RECEIPTS_HDR)EntityData;
                }

                if (txtGRNDate.Text != string.Empty)
                {
                    INV_RECEIPTS_HDR.RECEIPT_DATE = DBMethod.ConvertStringToDate(txtGRNDate.Text.ToString());
                }

                INV_RECEIPTS_HDR.INSPECTION_REQ_YN = chkInspection.Checked == true ? "1" : "0";
                INV_RECEIPTS_HDR.EMP_ID = (ddlEmployeename.SelectedValue.ToString());

                if (txtShipmentDate.Text != string.Empty)
                {
                    INV_RECEIPTS_HDR.SHIPMENT_DATE = DBMethod.ConvertStringToDate(txtShipmentDate.Text.ToString());
                }

                INV_RECEIPTS_HDR.SHIPMENT_NUMBER = txtShipmentNumber.Text;
                INV_RECEIPTS_HDR.SHIPMENT_TRACKING_NUMBER = txtShipmentTrackingNo.Text;

                INV_RECEIPTS_HDR.SHIPMENT_RECEIVED_BY = (txtShipmentReceivedby.Text);
                INV_RECEIPTS_HDR.SHIPMENT_MODE_OF_TRANSPORT = ddlModeofTransport.SelectedValue.ToString();
                INV_RECEIPTS_HDR.SHIPMENT_COMPANY = (txtShipmentCompany.Text);

                INV_RECEIPTS_HDR.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                // INV_RECEIPTS_HDR.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;
                INV_RECEIPTS_HDR.ORG_ID = VMVServices.Web.Utils.OrganizationID;
                INV_RECEIPTS_HDR.VENDOR_ID = ddlSupplierName.SelectedValue;
                INV_RECEIPTS_HDR.VENDOR_LOC_ID = ddlSupplierSite.SelectedValue;

                INV_RECEIPTS_HDR.INTIATED_BY = ddlInitiatedBy.SelectedValue;
                INV_RECEIPTS_HDR.APPROVED_BY = ddlApprovedBy.SelectedValue;
                INV_RECEIPTS_HDR.RECEIPT_FLAG = "Micellaneous";

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    INV_RECEIPTS_HDR.MODIFIED_BY = this.LoggedUserName;
                    INV_RECEIPTS_HDR.MODIFIED_DATE = DateTime.Today;
                }
                else
                {
                    INV_RECEIPTS_HDR.RECEIPT_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.AP_014_M.ToString(), false, true);
                    INV_RECEIPTS_HDR.GRN_NUM = FINSP.GetSPFOR_SEQCode(FINAppConstants.AP_014_LN.ToString(), false, true);
                    INV_RECEIPTS_HDR.CREATED_BY = this.LoggedUserName;
                    INV_RECEIPTS_HDR.CREATED_DATE = DateTime.Today;
                }

                INV_RECEIPTS_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, INV_RECEIPTS_HDR.RECEIPT_ID);
                //Save Detail Table


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                ErrorCollection = CommonUtils.IsEmptyGrid(dtGridData, "PO ");
                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                var tmpChildEntity = new List<Tuple<object, string>>();
                INV_RECEIPT_DTLS INV_RECEIPT_DTLS = new INV_RECEIPT_DTLS();

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    INV_RECEIPT_DTLS = new INV_RECEIPT_DTLS();

                    if (dtGridData.Rows[iLoop][FINColumnConstants.RECEIPT_DTL_ID].ToString() != "0" && dtGridData.Rows[iLoop][FINColumnConstants.RECEIPT_DTL_ID].ToString() != string.Empty)
                    {
                        INV_RECEIPT_DTLS = PurchaseItemReceipt_BLL.getDetailClassEntity(dtGridData.Rows[iLoop][FINColumnConstants.RECEIPT_DTL_ID].ToString());
                    }
                    int linNo = (iLoop + 1);
                    INV_RECEIPT_DTLS.LINE_NUM = linNo.ToString();
                    INV_RECEIPT_DTLS.ITEM_ID = (dtGridData.Rows[iLoop][FINColumnConstants.ITEM_ID].ToString());
                    // INV_RECEIPT_DTLS.PO_HEADER_ID = (dtGridData.Rows[iLoop][FINColumnConstants.PO_HEADER_ID].ToString());
                    // INV_RECEIPT_DTLS.PO_LINE_ID = (dtGridData.Rows[iLoop][FINColumnConstants.PO_LINE_ID].ToString());
                    INV_RECEIPT_DTLS.QTY_RECEIVED = CommonUtils.ConvertStringToInt(dtGridData.Rows[iLoop][FINColumnConstants.QTY_RECEIVED].ToString());
                    INV_RECEIPT_DTLS.QTY_REJECTED = CommonUtils.ConvertStringToInt(dtGridData.Rows[iLoop][FINColumnConstants.QTY_REJECTED].ToString());
                    INV_RECEIPT_DTLS.QTY_APPROVED = CommonUtils.ConvertStringToInt(dtGridData.Rows[iLoop]["QTY_APPROVED"].ToString());

                    INV_RECEIPT_DTLS.INV_DESCRIPTION = (dtGridData.Rows[iLoop][FINColumnConstants.REMARKS].ToString());
                    //  INV_RECEIPT_DTLS.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;
                    INV_RECEIPT_DTLS.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                    INV_RECEIPT_DTLS.RECEIPT_ID = INV_RECEIPTS_HDR.RECEIPT_ID;


                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        tmpChildEntity.Add(new Tuple<object, string>(INV_RECEIPT_DTLS, FINAppConstants.Delete));
                    }
                    else
                    {
                        if (dtGridData.Rows[iLoop][FINColumnConstants.RECEIPT_DTL_ID].ToString() != "0" && dtGridData.Rows[iLoop][FINColumnConstants.RECEIPT_DTL_ID].ToString() != string.Empty)
                        {
                            INV_RECEIPT_DTLS.MODIFIED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(INV_RECEIPT_DTLS, FINAppConstants.Update));
                        }
                        else
                        {
                            INV_RECEIPT_DTLS.RECEIPT_DTL_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.AP_014_D.ToString(), false, true);
                            INV_RECEIPT_DTLS.CREATED_BY = this.LoggedUserName;
                            INV_RECEIPT_DTLS.CREATED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(INV_RECEIPT_DTLS, FINAppConstants.Add));
                        }
                    }

                    INV_RECEIPT_DTLS.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;

                }



                // Duplicate Check

                ProReturn = FINSP.GetSPFOR_ERR_MGR_PO_RECP(txtGRNNumber.Text, INV_RECEIPTS_HDR.RECEIPT_ID);

                if (ProReturn != string.Empty)
                {

                    if (ProReturn != "0")
                    {
                        ErrorCollection.Add("POREQ", ProReturn);
                        if (ErrorCollection.Count > 0)
                        {
                            return;
                        }
                    }
                }

                ProReturn = FINSP.GetSPFOR_ERR_MGR_CATEGORY(Master.FormCode, INV_RECEIPTS_HDR.RECEIPT_ID, INV_RECEIPTS_HDR.GRN_NUM);
                if (ProReturn != string.Empty)
                {
                    if (ProReturn != "0")
                    {
                        ErrorCollection.Add("ITEMRECEIPT", ProReturn);
                        if (ErrorCollection.Count > 0)
                        {
                            return;
                        }
                    }
                }
                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            CommonUtils.SavePCEntity<INV_RECEIPTS_HDR, INV_RECEIPT_DTLS>(INV_RECEIPTS_HDR, tmpChildEntity, INV_RECEIPT_DTLS);
                            savedBool = true;

                            //for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                            //{
                            //    trnType = "RECEIVE";

                            //    FINSP.GetSPItemtxnLedger(trnType, dtGridData.Rows[iLoop][FINColumnConstants.ITEM_ID].ToString(), CommonUtils.ConvertStringToInt(dtGridData.Rows[iLoop]["QTY_APPROVED"].ToString()), INV_RECEIPTS_HDR.WH_ID, DBMethod.ConvertDateToString(txtGRNDate.Text).ToString(), INV_RECEIPTS_HDR.RECEIPT_ID, Session["po_header_Id"].ToString(), Session["PO_LINE_ID"].ToString(), trnType + " of " + iNV_RECEIPT_ITEM_WH_HDR.ITEM_ID);
                            //}

                            break;
                        }
                    case FINAppConstants.Update:
                        {
                            CommonUtils.SavePCEntity<INV_RECEIPTS_HDR, INV_RECEIPT_DTLS>(INV_RECEIPTS_HDR, tmpChildEntity, INV_RECEIPT_DTLS, true);
                            savedBool = true;


                            break;
                        }
                }

                if (INV_RECEIPTS_HDR.WORKFLOW_COMPLETION_STATUS == "1")
                {
                   // FINSP.GetSP_GL_Posting(INV_RECEIPTS_HDR.RECEIPT_ID, "AP_024");
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// .RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                // DBMethod.DeleteEntity<INV_RECEIPTS_HDR>(INV_RECEIPTS_HDR);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// .RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void ddlPoNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                fn_POData();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// .RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void fn_POData()
        {
            try
            {
                ErrorCollection.Clear();

                //  DropDownList ddlPoNumber = new DropDownList();
                //TextBox txtItemName = new TextBox();
                TextBox txtQuantity = new TextBox();
                //  TextBox txtPOLineNo = new TextBox();
                DropDownList ddlItem = new DropDownList();

                ddlItem.ID = "ddlItem";
                //   ddlPoNumber.ID = "ddlPoNumber";

                //  txtPOLineNo.ID = "txtPOLineNo";
                txtQuantity.ID = "txtQuantity";

                // ddlPoNumber.Focus();

                if (gvData.FooterRow != null)
                {
                    if (gvData.EditIndex < 0)
                    {

                        txtQuantity = (TextBox)gvData.FooterRow.FindControl("txtQuantity");
                        //  txtPOLineNo = (TextBox)gvData.FooterRow.FindControl("txtPOLineNo");
                        //  ddlPoNumber = (DropDownList)gvData.FooterRow.FindControl("ddlPoNumber");
                        ddlItem = (DropDownList)gvData.FooterRow.FindControl("ddlItem");
                    }
                    else
                    {

                        txtQuantity = (TextBox)gvData.Rows[gvData.EditIndex].FindControl("txtQuantity");
                        //txtPOLineNo = (TextBox)gvData.Rows[gvData.EditIndex].FindControl("txtPOLineNo");
                        //ddlPoNumber = (DropDownList)gvData.Rows[gvData.EditIndex].FindControl("ddlPoNumber");
                        ddlItem = (DropDownList)gvData.Rows[gvData.EditIndex].FindControl("ddlItem");
                    }
                }
                else
                {

                    txtQuantity = (TextBox)gvData.Controls[0].Controls[1].FindControl("txtQuantity");
                    //txtPOLineNo = (TextBox)gvData.Controls[0].Controls[1].FindControl("txtPOLineNo");
                    //ddlPoNumber = (DropDownList)gvData.Controls[0].Controls[1].FindControl("ddlPoNumber");
                    ddlItem = (DropDownList)gvData.Controls[0].Controls[1].FindControl("ddlItem");
                }

                DataTable dtData = new DataTable();
                //if (ddlPoNumber.SelectedValue != string.Empty && ddlPoNumber.SelectedValue != "0")
                //{
                //    dtData = DBMethod.ExecuteQuery(PurchaseOrder_DAL.GetPOData(ddlPoNumber.SelectedValue.ToString())).Tables[0];
                //    if (dtData != null)
                //    {
                //        if (dtData.Rows.Count > 0)
                //        {

                //            txtQuantity.Text = dtData.Rows[0][FINColumnConstants.PO_QUANTITY].ToString();
                //            txtPOLineNo.Text = dtData.Rows[0][FINColumnConstants.PO_LINE_NUM].ToString();

                //            Item_BLL.GetItemName(ref ddlItem);
                //            ddlItem.SelectedValue = dtData.Rows[0][FINColumnConstants.PO_ITEM_ID].ToString();
                //            hdItemId.Value = dtData.Rows[0][FINColumnConstants.PO_ITEM_ID].ToString();
                //            hdLineId.Value = dtData.Rows[0][FINColumnConstants.PO_HEADER_ID].ToString();
                //        }
                //    }
                //}

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// .RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void chkInspection_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (chkInspection.Checked)
                {
                    ddlEmployeename.Enabled = true;
                }
                else
                {
                    ddlEmployeename.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// .RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        #endregion

        protected void txtShipmentNumber_TextChanged(object sender, EventArgs e)
        {

        }

        protected void ddlSupplierName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);
                SupplierBranch_BLL.fn_getSupplierSite4SupplierName(ref ddlSupplierSite, ddlSupplierName.SelectedValue);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// .RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }




    }
}