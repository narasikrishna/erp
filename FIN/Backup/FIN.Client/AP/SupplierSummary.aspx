﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="SupplierSummary.aspx.cs" Inherits="FIN.Client.AP.SupplierSummary" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<%--    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>--%>
    <script type="text/javascript">
        function shrinkandgrow(input) {
            var displayIcon = "img" + input;
            if ($("#" + displayIcon).attr("src") == "../Images/expand_blue.png") {
                $("#" + displayIcon).closest("tr")
			    .after("<tr><td></td><td colspan = '100%'>" + $("#" + input)
			    .html() + "</td></tr>");
                $("#" + displayIcon).attr("src", "../Images/collapse_blue.png");
            } else {
                $("#" + displayIcon).closest("tr").next().remove();
                $("#" + displayIcon).attr("src", "../Images/expand_blue.png");
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1000px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style="width: 200px" id="lblFromDate">
                From Date
            </div>
            <div class="divtxtBox LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtFromDate" runat="server" TabIndex="1" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]]  RequiredField txtBox"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calExtender1" TargetControlID="txtFromDate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style=" width: 200px" id="lblToDate">
                To Date
            </div>
            <div class="divtxtBox LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtToDate" runat="server" TabIndex="2" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]]  RequiredField txtBox"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtToDate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divFormcontainer  LNOrient"  id="div1">
            <div class="divRowContainer divReportAction">
                <table class="ReportTable">
                    <tr>
                        <td class="lblBox LNOrient">
                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/btnShow.png"
                                OnClick="btnShow_Click" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class=" LNOrient">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" Width="900px"
                CssClass="DisplayFont Grid" OnRowDataBound="gvData_RowDataBound" ShowFooter="false"
                DataKeyNames="VENDOR_ID">
                <Columns>
                    <asp:TemplateField ItemStyle-Width="40px">
                        <ItemTemplate>
                            <a href="JavaScript:shrinkandgrow('div<%# Eval("VENDOR_ID") %>');">
                                <img alt="Details" id="imgdiv<%# Eval("VENDOR_ID") %>" src="../Images/expand_blue.png" />
                            </a>
                            <div id="div<%# Eval("VENDOR_ID") %>" style="display: none;">
                                <asp:GridView ID="gvInvPayment" runat="server" AutoGenerateColumns="false" DataKeyNames="VENDOR_ID" CssClass="Grid"
                                    HeaderStyle-BackColor="Gray" HeaderStyle-ForeColor="White">
                                    <Columns>
                                        <asp:BoundField ItemStyle-Width="100px" DataFormatString="{0:dd/MM/yyyy}" DataField="INV_DATE" HeaderText="Invoice Date" />
                                        <asp:HyperLinkField ItemStyle-Width="150px" DataNavigateUrlFields="INV_ID" DataNavigateUrlFormatString="InvoiceEntry.aspx?ProgramID=32&Mode=U&ID={0}" DataTextField="INV_ID" HeaderText="Invoice Number" />
                                        <asp:BoundField ItemStyle-Width="150px" DataField="INVOICE_AMT" HeaderText="Invoice Amount" ItemStyle-HorizontalAlign="Right" />
                                        <asp:HyperLinkField ItemStyle-Width="150px" DataNavigateUrlFields="PAY_ID" DataNavigateUrlFormatString="PaymentEntry.aspx?ProgramID=82&Mode=U&ID={0}" DataTextField="PAY_ID" HeaderText="Payment Number" />
                                        <asp:BoundField ItemStyle-Width="150px" DataField="PAYMENT_AMT" HeaderText="Payment Amount" ItemStyle-HorizontalAlign="Right" />
                                        <%--<asp:BoundField ItemStyle-Width="150px" DataField="INV_ID" HeaderText="Invoice ID" DataFormatString="<a target='INV_ID' href='InvoiceEntry.aspx?ProgramID=32&Mode=U&ID={0}'>ID</a>" />--%>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="VENDOR_ID" HeaderText="Supplier Number" />
                    <asp:BoundField DataField="VENDOR_NAME" HeaderText="Supplier Name" />
                    <asp:BoundField DataField="INVOICE_AMT" HeaderText="Invoice Amount">
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:BoundField>
                    <asp:BoundField DataField="PAYMENT_AMT" HeaderText="Paid Amount">
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:BoundField>
                    <asp:BoundField DataField="BALANCE" HeaderText="Balance">
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:BoundField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table width="100%">
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
        <div id="div5">
            <asp:HiddenField runat="server" ID="hdRowIndex" />
            <asp:HiddenField runat="server" ID="hdItemId" />
            <asp:HiddenField runat="server" ID="HiddenField1" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/AP/APChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">


        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });
    </script>
</asp:Content>
