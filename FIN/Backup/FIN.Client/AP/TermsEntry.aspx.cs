﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.BLL;
using VMVServices.Web;

namespace FIN.Client.AP
{
    public partial class TermsEntry : PageBase
    {
        INV_PAY_TERMS_HDR iNV_PAY_TERMS_HDR = new INV_PAY_TERMS_HDR();
        INV_PAY_TERMS_DTL iNV_PAY_TERMS_DTL = new INV_PAY_TERMS_DTL();

        DataTable dtGridData = new DataTable();
        Boolean bol_rowVisiable;
        Boolean saveBool;
        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("APTerms", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
        }

        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                Startup();
                EntityData = null;

                dtGridData = FIN.BLL.AP.Terms_BLL.getChildEntityDet(Master.StrRecordId);
                BindGrid(dtGridData);

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    using (IRepository<INV_PAY_TERMS_HDR> userCtx = new DataRepository<INV_PAY_TERMS_HDR>())
                    {
                        iNV_PAY_TERMS_HDR = userCtx.Find(r =>
                            (r.TERM_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }


                    EntityData = iNV_PAY_TERMS_HDR;

                    txtTermName.Text = iNV_PAY_TERMS_HDR.TERM_NAME;
                    if (iNV_PAY_TERMS_HDR.TERM_NAME_OL != null)
                    {
                        txtTermNameOL.Text = iNV_PAY_TERMS_HDR.TERM_NAME_OL;
                    }
                    if (iNV_PAY_TERMS_HDR.TERM_EFF_FROM_DT != null)
                    {
                        txtEffectiveFromDate.Text = DBMethod.ConvertDateToString(iNV_PAY_TERMS_HDR.TERM_EFF_FROM_DT.ToString());
                    }

                    if (iNV_PAY_TERMS_HDR.TERM_EFF_TO_DT != null)
                    {
                        txtEffectiveToDate.Text = DBMethod.ConvertDateToString(iNV_PAY_TERMS_HDR.TERM_EFF_TO_DT.ToString());
                    }


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("APTerm", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }


        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();

                if (EntityData != null)
                {
                    iNV_PAY_TERMS_HDR = (INV_PAY_TERMS_HDR)EntityData;
                }

                iNV_PAY_TERMS_HDR.TERM_NAME = txtTermName.Text;

                iNV_PAY_TERMS_HDR.TERM_NAME_OL = txtTermNameOL.Text;


                if (txtEffectiveFromDate.Text != null && txtEffectiveFromDate.Text != string.Empty && txtEffectiveFromDate.Text.ToString().Trim().Length > 0)
                {
                    iNV_PAY_TERMS_HDR.TERM_EFF_FROM_DT = DBMethod.ConvertStringToDate(txtEffectiveFromDate.Text.ToString());
                }

                if (txtEffectiveToDate.Text != null && txtEffectiveToDate.Text != string.Empty && txtEffectiveToDate.Text.ToString().Trim().Length > 0)
                {
                    iNV_PAY_TERMS_HDR.TERM_EFF_TO_DT = DBMethod.ConvertStringToDate(txtEffectiveToDate.Text.ToString());
                }

                iNV_PAY_TERMS_HDR.ENABLED_FLAG = FINAppConstants.Y;
                // iNV_PAY_TERMS_HDR.WORKFLOW_COMPLETION_STATUS = FINAppConstants.Y;
                iNV_PAY_TERMS_HDR.ORG_ID = VMVServices.Web.Utils.OrganizationID;
                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    iNV_PAY_TERMS_HDR.MODIFIED_BY = this.LoggedUserName;
                    iNV_PAY_TERMS_HDR.MODIFIED_DATE = DateTime.Today;
                }
                else
                {
                    iNV_PAY_TERMS_HDR.TERM_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.AP_TERM_M.ToString(), false, true);

                    iNV_PAY_TERMS_HDR.CREATED_BY = this.LoggedUserName;
                    iNV_PAY_TERMS_HDR.CREATED_DATE = DateTime.Today;
                }
                iNV_PAY_TERMS_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, iNV_PAY_TERMS_HDR.TERM_ID);
                iNV_PAY_TERMS_HDR.ORG_ID = VMVServices.Web.Utils.OrganizationID;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                var tmpChildEntity = new List<Tuple<object, string>>();
                INV_PAY_TERMS_DTL iNV_PAY_TERMS_DTL = new INV_PAY_TERMS_DTL();
                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    iNV_PAY_TERMS_DTL = new INV_PAY_TERMS_DTL();
                    if (dtGridData.Rows[iLoop][FINColumnConstants.TERM_DTL_ID].ToString() != "0")
                    {
                        using (IRepository<INV_PAY_TERMS_DTL> userCtx = new DataRepository<INV_PAY_TERMS_DTL>())
                        {
                            iNV_PAY_TERMS_DTL = userCtx.Find(r =>
                                (r.TERM_DTL_ID == dtGridData.Rows[iLoop][FINColumnConstants.TERM_DTL_ID].ToString())
                                ).SingleOrDefault();
                        }
                    }
                    iNV_PAY_TERMS_DTL.TERM_ID = iNV_PAY_TERMS_HDR.TERM_ID.ToString();
                    iNV_PAY_TERMS_DTL.TERM_TYPE = dtGridData.Rows[iLoop][FINColumnConstants.VALUE_KEY_ID].ToString();
                    iNV_PAY_TERMS_DTL.TERM_CR_DAYS = int.Parse(dtGridData.Rows[iLoop][FINColumnConstants.TERM_CR_DAYS].ToString());
                    //iNV_PAY_TERMS_DTL.TERM_AMT = decimal.Parse(dtGridData.Rows[iLoop][FINColumnConstants.TERM_AMT].ToString());
                    iNV_PAY_TERMS_DTL.TERM_AMT = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop][FINColumnConstants.TERM_AMT].ToString());
                    iNV_PAY_TERMS_DTL.TERM_DISCOUNT_TYPE = dtGridData.Rows[iLoop][FINColumnConstants.TERM_DISCOUNT_TYPE].ToString();
                    iNV_PAY_TERMS_DTL.TERM_DISCOUNT_DAYS = int.Parse(dtGridData.Rows[iLoop][FINColumnConstants.TERM_DISCOUNT_DAYS].ToString());
                    iNV_PAY_TERMS_DTL.TERM_DISCOUNT_AMT = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop][FINColumnConstants.TERM_DISCOUNT_AMT].ToString());

                    iNV_PAY_TERMS_DTL.WORKFLOW_COMPLETION_STATUS = "1";
                    iNV_PAY_TERMS_DTL.ENABLED_FLAG = "1";

                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        tmpChildEntity.Add(new Tuple<object, string>(iNV_PAY_TERMS_DTL, "D"));
                    }
                    else
                    {
                        if (dtGridData.Rows[iLoop][FINColumnConstants.TERM_DTL_ID].ToString() != "0")
                        {
                            iNV_PAY_TERMS_DTL.MODIFIED_BY = this.LoggedUserName;
                            iNV_PAY_TERMS_DTL.MODIFIED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(iNV_PAY_TERMS_DTL, "U"));
                        }
                        else
                        {
                            iNV_PAY_TERMS_DTL.TERM_DTL_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.AP_TERM_D.ToString(), false, true);
                            iNV_PAY_TERMS_DTL.CREATED_BY = this.LoggedUserName;
                            iNV_PAY_TERMS_DTL.CREATED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(iNV_PAY_TERMS_DTL, "A"));
                        }
                    }
                }

                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            FIN.BLL.GL.AccountCodes_BLL.SavePCEntity<INV_PAY_TERMS_HDR, INV_PAY_TERMS_DTL>(iNV_PAY_TERMS_HDR, tmpChildEntity, iNV_PAY_TERMS_DTL);
                            saveBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {
                            FIN.BLL.GL.AccountCodes_BLL.SavePCEntity<INV_PAY_TERMS_HDR, INV_PAY_TERMS_DTL>(iNV_PAY_TERMS_HDR, tmpChildEntity, iNV_PAY_TERMS_DTL, true);
                            saveBool = true;
                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("APTerm", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                DropDownList ddlTermType = tmpgvr.FindControl("ddlTermType") as DropDownList;
                Lookup_BLL.GetLookUpValues(ref ddlTermType, "TERM_TYPE");


                if (gvData.EditIndex >= 0)
                {
                    ddlTermType.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.VALUE_KEY_ID].ToString();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("APTermFillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session[FINSessionConstants.GridData], "Term ");

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                AssignToBE();

                if (ErrorCollection.Count > 0)
                {
                    return;
                }


                if (saveBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("APTermSave", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        #endregion

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;

                if (dtData.Rows.Count > 0)
                {
                    dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("TERM_AMT", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("TERM_AMT"))));
                    dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("TERM_DISCOUNT_AMT", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("TERM_DISCOUNT_AMT"))));
                    dtData.AcceptChanges();
                }

                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("APTermEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            if (Session[FINSessionConstants.GridData] != null)
            {
                dtGridData = (DataTable)Session[FINSessionConstants.GridData];
            }

            gvData.EditIndex = -1;
            BindGrid(dtGridData);
        }
        #endregion

        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }

                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("APPayTermEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {
            System.Collections.SortedList slControls = new System.Collections.SortedList();

            //TextBox tTermType = gvr.FindControl("txtTermType") as TextBox;
            DropDownList ddlTermType = gvr.FindControl("ddlTermType") as DropDownList;
            TextBox tTermCRDays = gvr.FindControl("txtTermCRDays") as TextBox;
            TextBox tTermCRAmt = gvr.FindControl("txtTermCRAmt") as TextBox;

            TextBox tTermDiscType = gvr.FindControl("txtTermDiscType") as TextBox;
            TextBox tTermDiscDays = gvr.FindControl("txtTermDiscDays") as TextBox;
            TextBox tTermDiscAmt = gvr.FindControl("txtTermDiscAmt") as TextBox;

            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList[FINColumnConstants.TERM_DTL_ID] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);
            }

            slControls[0] = ddlTermType;
            slControls[1] = tTermCRDays;
            slControls[2] = tTermCRAmt;
            slControls[3] = tTermDiscType;
            slControls[4] = tTermDiscDays;
            slControls[5] = tTermDiscAmt;
            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/AP_" + Session["Sel_Lng"].ToString() + ".properties"));
            ErrorCollection.Clear();
            string strCtrlTypes = FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX;
            string strMessage = Prop_File_Data["Term_Type_P"] + " ~ " + Prop_File_Data["CR_Days_P"] + " ~ " + Prop_File_Data["CR_Amount_P"] + " ~ " + Prop_File_Data["Discount_Type_P"] + " ~ " + Prop_File_Data["Days_P"] + " ~ " + Prop_File_Data["Amount_P"] + "";
            // string strMessage = "Term Type ~ CR Days ~ CR Amount ~ Discount Type ~ Days ~ Amount";

            EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

            if (EmptyErrorCollection.Count > 0)
            {
                ErrorCollection = EmptyErrorCollection;
                return drList;
            }

            //string strCondition = "TERM_DTL_ID='" + ddl_Segment.SelectedItem.Text.Trim().ToUpper() + "'";
            //strMessage = FINMessageConstatns.RecordAlreadyExists;
            //ErrorCollection = UserUtility.DataDuplication(dt_tmp, strCondition, strMessage);
            //if (ErrorCollection.Count > 0)
            //{
            //    return drList;
            //}

            //   drList[FINColumnConstants.TERM_TYPE] = tTermType.Text;

            if (ddlTermType.SelectedItem != null)
            {
                drList[FINColumnConstants.VALUE_KEY_ID] = ddlTermType.SelectedItem.Value;
                drList[FINColumnConstants.VALUE_NAME] = ddlTermType.SelectedItem.Text;
            }


            drList[FINColumnConstants.TERM_CR_DAYS] = tTermCRDays.Text.ToString();
            drList[FINColumnConstants.TERM_AMT] = tTermCRAmt.Text.ToString();
            drList[FINColumnConstants.TERM_DISCOUNT_TYPE] = tTermDiscType.Text.ToString();
            drList[FINColumnConstants.TERM_DISCOUNT_DAYS] = tTermDiscDays.Text.ToString();
            drList[FINColumnConstants.TERM_DISCOUNT_AMT] = tTermDiscAmt.Text.ToString();
            drList[FINColumnConstants.DELETED] = FINAppConstants.N;

            //if (tTermCRAmt.Text.Length > 0)
            //{
            //    tTermCRAmt.Text = DBMethod.GetAmtDecimalCommaSeparationValue(tTermCRAmt.Text);
            //    drList[FINColumnConstants.TERM_AMT] = tTermCRAmt.Text.ToString(); 
            //    String.Format("{0:0,0}", decimal.Parse(drList[FINColumnConstants.TERM_AMT].ToString()));     
            //}


            return drList;
        }



        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("APTermEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("APTermEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("APTermEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("APTermEntry_RowCreated", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }
                }
                //if (e.Row.RowType == DataControlRowType.DataRow)
                //{
                //    if (e.Row.RowIndex.Equals(0))
                //        e.Row.Cells.Cast<TableCell>().ToList()
                //                    .Where(a => !string.IsNullOrEmpty(HttpUtility.HtmlDecode(a.Text).Trim())).ToList()
                //                    .ForEach(a => a.Text = String.Format("{0:N3}", Double.Parse(a.Text)));
                //    else
                //        e.Row.Cells.Cast<TableCell>().ToList()
                //                    .Where(a => !string.IsNullOrEmpty(HttpUtility.HtmlDecode(a.Text).Trim())).ToList()
                //                    .ForEach(a => a.Text = a.Text);
                //}
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("APTermEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void btnYes_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                DBMethod.DeleteEntity<INV_PAY_TERMS_HDR>(iNV_PAY_TERMS_HDR);
                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATADELETED);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("APTermEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void txtEffectiveFromDate_TextChanged(object sender, EventArgs e)
        {

        }

        protected void txtTermName_TextChanged(object sender, EventArgs e)
        {

        }

        protected void txtEffectiveToDate_TextChanged(object sender, EventArgs e)
        {

        }

    }
}