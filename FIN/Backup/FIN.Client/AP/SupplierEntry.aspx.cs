﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.BLL;
using FIN.BLL.GL;
using FIN.BLL.AP;
using FIN.DAL.AP;
using VMVServices.Web;

namespace FIN.Client.AP
{
    public partial class Supplier : PageBase
    {
        SUPPLIER_CUSTOMERS sUPPLIER_CUSTOMERS = new SUPPLIER_CUSTOMERS();
        string ProReturn = null;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
            }

            if (VMVServices.Web.Utils.AddAllowed != FINAppConstants.Y && Master.Mode == FINAppConstants.Add)
            {
                btnSave.Visible = false;
            }

        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();

                EntityData = null;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    using (IRepository<SUPPLIER_CUSTOMERS> userCtx = new DataRepository<SUPPLIER_CUSTOMERS>())
                    {
                        sUPPLIER_CUSTOMERS = userCtx.Find(r =>
                            (r.VENDOR_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = sUPPLIER_CUSTOMERS;

                    txtSupplierNumber.Text = sUPPLIER_CUSTOMERS.VENDOR_CODE;
                    txtSupplierName.Text = sUPPLIER_CUSTOMERS.VENDOR_NAME;

                    txtSupplierNameOL.Text = sUPPLIER_CUSTOMERS.VENDOR_NAME_OL;
                    txtChequePrintName.Text = sUPPLIER_CUSTOMERS.ATTRIBUTE1;
                    ddlSupplierType.SelectedValue = sUPPLIER_CUSTOMERS.VENDOR_TYPE;

                    ddlSupplierStatus.SelectedValue = sUPPLIER_CUSTOMERS.VENDOR_STATUS;
                    txtAddress1.Text = sUPPLIER_CUSTOMERS.VENDOR_ADD1;
                    ddlCountry.SelectedValue = sUPPLIER_CUSTOMERS.VENDOR_COUNTRY;
                    // Supplier_BLL.fn_getState(ref ddlState, ddlCountry.SelectedValue);
                    fillstate();

                    txtAddress2.Text = sUPPLIER_CUSTOMERS.VENDOR_ADD2;
                    ddlState.SelectedValue = sUPPLIER_CUSTOMERS.VENDOR_STATE;
                    fillcity();
                    //Supplier_BLL.fn_getCity(ref ddlCity, ddlState.SelectedValue);
                    txtAddress3.Text = sUPPLIER_CUSTOMERS.VENDOR_ADD3;
                    ddlCity.SelectedValue = sUPPLIER_CUSTOMERS.VENDOR_CITY;
                    ddlSupplierCategory.SelectedValue = sUPPLIER_CUSTOMERS.SUPPLIER_CATEGORY;
                    txtPostalCode.Text = sUPPLIER_CUSTOMERS.VENDOR_ZIP_CODE;
                    txtPhone.Text = sUPPLIER_CUSTOMERS.VENDOR_PHONE;
                    txtFax.Text = sUPPLIER_CUSTOMERS.VENDOR_FAX;
                    txtEmail.Text = sUPPLIER_CUSTOMERS.VENDOR_EMAIL;
                    txtURL.Text = sUPPLIER_CUSTOMERS.VENDOR_URL;
                    txtTaxID1.Text = sUPPLIER_CUSTOMERS.VENDOR_TAX_ID_1;
                    txtTaxID2.Text = sUPPLIER_CUSTOMERS.VENDOR_TAX_ID_2;
                    txtTaxID3.Text = sUPPLIER_CUSTOMERS.VENDOR_TAX_ID_3;
                    txtTaxID4.Text = sUPPLIER_CUSTOMERS.VENDOR_TAX_ID_4;
                    txtTaxID5.Text = sUPPLIER_CUSTOMERS.VENDOR_TAX_ID_5;

                    ddlPaymentTerm.SelectedValue = sUPPLIER_CUSTOMERS.VENDOR_TERM_ID;
                  //  txtRetAccount.Text = sUPPLIER_CUSTOMERS.VENDOR_RETENTION_ACCT_ID;
                    txtContactPerson.Text = sUPPLIER_CUSTOMERS.VENDOR_CONTACT_NAME;
                    txtAlternateName.Text = sUPPLIER_CUSTOMERS.VENDOR_ALT_NAME;

                    if (sUPPLIER_CUSTOMERS.ENABLED_FLAG == FINAppConstants.EnabledFlag)
                    {
                        chkActive.Checked = true;
                    }
                    else
                    {
                        chkActive.Checked = false;
                    }
                    ddlAPAdvance.SelectedValue = sUPPLIER_CUSTOMERS.AP_ADVANCE_ACCOUNT;
                    ddlAPLiable.SelectedValue = sUPPLIER_CUSTOMERS.AP_LIABILITY_ACCOUNT;
                    ddlRetAccount.SelectedValue = sUPPLIER_CUSTOMERS.VENDOR_RETENTION_ACCT_ID;
                    ddlARDebtors.SelectedValue = sUPPLIER_CUSTOMERS.AR_DEBTORS_ACCOUNT;
                    ddlARRevenue.SelectedValue = sUPPLIER_CUSTOMERS.AR_REVENUE_ACCOUNT;
                    ddlARAdvance.SelectedValue = sUPPLIER_CUSTOMERS.AR_ADVANCE_ACCOUNT;

                    if (sUPPLIER_CUSTOMERS.VENDOR_TYPE != null)
                    {
                        DispSupplierTypeDDL();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    sUPPLIER_CUSTOMERS = (SUPPLIER_CUSTOMERS)EntityData;
                }

                sUPPLIER_CUSTOMERS.ORG_ID = VMVServices.Web.Utils.OrganizationID;
                sUPPLIER_CUSTOMERS.VENDOR_CODE = txtSupplierNumber.Text;

                sUPPLIER_CUSTOMERS.VENDOR_NAME = txtSupplierName.Text;
                sUPPLIER_CUSTOMERS.VENDOR_NAME_OL = txtSupplierNameOL.Text;
                sUPPLIER_CUSTOMERS.ATTRIBUTE1 = txtChequePrintName.Text;


                sUPPLIER_CUSTOMERS.VENDOR_TYPE = ddlSupplierType.SelectedValue;
                sUPPLIER_CUSTOMERS.VENDOR_STATUS = ddlSupplierStatus.SelectedValue;
                sUPPLIER_CUSTOMERS.VENDOR_ADD1 = txtAddress1.Text;
                sUPPLIER_CUSTOMERS.VENDOR_COUNTRY = ddlCountry.SelectedValue;
                sUPPLIER_CUSTOMERS.VENDOR_ADD2 = txtAddress2.Text;
                sUPPLIER_CUSTOMERS.VENDOR_STATE = ddlState.SelectedValue;
                sUPPLIER_CUSTOMERS.VENDOR_ADD3 = txtAddress3.Text;
                sUPPLIER_CUSTOMERS.VENDOR_CITY = ddlCity.SelectedValue;
                sUPPLIER_CUSTOMERS.VENDOR_ZIP_CODE = txtPostalCode.Text;
                sUPPLIER_CUSTOMERS.VENDOR_PHONE = txtPhone.Text;
                sUPPLIER_CUSTOMERS.VENDOR_FAX = txtFax.Text;
                sUPPLIER_CUSTOMERS.VENDOR_EMAIL = txtEmail.Text;
                sUPPLIER_CUSTOMERS.VENDOR_URL = txtURL.Text;
                sUPPLIER_CUSTOMERS.VENDOR_TAX_ID_1 = txtTaxID1.Text;
                sUPPLIER_CUSTOMERS.VENDOR_TAX_ID_2 = txtTaxID2.Text;
                sUPPLIER_CUSTOMERS.VENDOR_TAX_ID_3 = txtTaxID3.Text;
                sUPPLIER_CUSTOMERS.VENDOR_TAX_ID_4 = txtTaxID4.Text;
                sUPPLIER_CUSTOMERS.VENDOR_TAX_ID_5 = txtTaxID5.Text;
                sUPPLIER_CUSTOMERS.ENABLED_FLAG = chkActive.Checked == true ? FINAppConstants.EnabledFlag : FINAppConstants.DisabledFlag;
              
                //sUPPLIER_CUSTOMERS.SUPPLIER_CATEGORY = ddlAPAdvance.SelectedValue;
                sUPPLIER_CUSTOMERS.ATTRIBUTE1 = ddlAPAdvance.SelectedValue;


                sUPPLIER_CUSTOMERS.AP_ADVANCE_ACCOUNT = ddlAPAdvance.SelectedValue;
                sUPPLIER_CUSTOMERS.AP_LIABILITY_ACCOUNT = ddlAPLiable.SelectedValue;
                sUPPLIER_CUSTOMERS.VENDOR_RETENTION_ACCT_ID = ddlRetAccount.SelectedValue;
                sUPPLIER_CUSTOMERS.AR_DEBTORS_ACCOUNT = ddlARDebtors.SelectedValue;
                sUPPLIER_CUSTOMERS.AR_REVENUE_ACCOUNT = ddlARRevenue.SelectedValue;
                sUPPLIER_CUSTOMERS.AR_ADVANCE_ACCOUNT = ddlARAdvance.SelectedValue;

                sUPPLIER_CUSTOMERS.SUPPLIER_CATEGORY = ddlSupplierCategory.SelectedValue;
                sUPPLIER_CUSTOMERS.VENDOR_TERM_ID = ddlPaymentTerm.SelectedValue;
               // sUPPLIER_CUSTOMERS.VENDOR_RETENTION_ACCT_ID = txtRetAccount.Text;
                sUPPLIER_CUSTOMERS.VENDOR_CONTACT_NAME = txtContactPerson.Text;
                sUPPLIER_CUSTOMERS.VENDOR_ALT_NAME = txtAlternateName.Text;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    sUPPLIER_CUSTOMERS.MODIFIED_BY = this.LoggedUserName;
                    sUPPLIER_CUSTOMERS.MODIFIED_DATE = DateTime.Today;

                }
                else
                {
                    sUPPLIER_CUSTOMERS.VENDOR_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.AP_009.ToString(), false, true);
                    sUPPLIER_CUSTOMERS.VENDOR_CODE = FINSP.GetSPFOR_SEQCode(FINAppConstants.AP_009_VC.ToString(), false, true);

                    // sUPPLIER_CUSTOMERS.ENABLED_FLAG = "1";
                    sUPPLIER_CUSTOMERS.CREATED_BY = this.LoggedUserName;
                    sUPPLIER_CUSTOMERS.CREATED_DATE = DateTime.Today;


                }
                sUPPLIER_CUSTOMERS.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, sUPPLIER_CUSTOMERS.VENDOR_ID);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void FillComboBox()
        {
            Supplier_BLL.fn_getSupplierType(ref ddlSupplierType);
            Supplier_BLL.fn_getSupplierStatus(ref ddlSupplierStatus);
            Supplier_BLL.fn_getCountry(ref ddlCountry);
            //Supplier_BLL.fn_getState(ref ddlState, ddlCountry.SelectedValue);
            //Supplier_BLL.fn_getCity(ref ddlCity, ddlState.SelectedValue);
            AccountCodes_BLL.getAccountWithControlAC(ref ddlAPAdvance);
            AccountCodes_BLL.getAccountWithControlAC(ref ddlRetAccount);
            AccountCodes_BLL.getAccountWithControlAC(ref ddlAPLiable);
            AccountCodes_BLL.getAccountWithControlAC(ref ddlARAdvance);
            AccountCodes_BLL.getAccountWithControlAC(ref ddlARDebtors);
            AccountCodes_BLL.getAccountWithControlAC(ref ddlARRevenue);

            FIN.BLL.AP.Terms_BLL.getTermName(ref ddlPaymentTerm);
            FIN.BLL.Lookup_BLL.GetLookUpValues(ref ddlSupplierCategory, "SUPP_CATEGORY");
        }

        protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            fillstate();

        }

        private void fillstate()
        {
            ddlCountry.Focus();
            Supplier_BLL.fn_getState(ref ddlState, ddlCountry.SelectedValue);
            //Supplier_BLL.fn_getCity(ref ddlCity, ddlState.SelectedValue);
        }

        protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
        {
            fillcity();

        }
        private void fillcity()
        {
            ddlState.Focus();
            Supplier_BLL.fn_getCity(ref ddlCity, ddlState.SelectedValue);
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                AssignToBE();


                // Duplicate Check
                ProReturn = FINSP.GetSPFOR_ERR_MGR_CATEGORY_TWO(Master.FormCode, sUPPLIER_CUSTOMERS.VENDOR_ID, sUPPLIER_CUSTOMERS.VENDOR_CODE, sUPPLIER_CUSTOMERS.VENDOR_NAME);
                if (ProReturn != string.Empty)
                {
                    if (ProReturn != "0")
                    {
                        ErrorCollection.Add("SUPPLIER", ProReturn);
                        if (ErrorCollection.Count > 0)
                        {
                            return;
                        }
                    }
                }

                //ProReturn = FINSP.GetSPFOR_ERR_MGR_SUPPLIER(txtSupplierNumber.Text, sUPPLIER_CUSTOMERS.VENDOR_ID);

                //if (ProReturn != string.Empty)
                //{

                //    if (ProReturn != "0")
                //    {
                //        ErrorCollection.Add("SUPPLIERENTRY", ProReturn);
                //        if (ErrorCollection.Count > 0)
                //        {
                //            return;
                //        }
                //    }
                //}



                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            DBMethod.SaveEntity<SUPPLIER_CUSTOMERS>(sUPPLIER_CUSTOMERS);
                            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                            break;
                        }
                    case FINAppConstants.Update:
                        {

                            DBMethod.SaveEntity<SUPPLIER_CUSTOMERS>(sUPPLIER_CUSTOMERS, true);
                            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                            break;

                        }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion




        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                DBMethod.DeleteEntity<SUPPLIER_CUSTOMERS>(sUPPLIER_CUSTOMERS);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void txtSupplierName_TextChanged(object sender, EventArgs e)
        {

        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {

        }

        protected void ddlSupplierType_SelectedIndexChanged(object sender, EventArgs e)
        {
            DispSupplierTypeDDL();
        }
        private void DispSupplierTypeDDL()
        {
            APAcc.Visible = false;
            ARAcc.Visible = false;
            if (ddlSupplierType.SelectedValue.ToString().ToUpper() != "BOTH")
            {
                APAcc.Visible = true;
            }
            else
            {
                APAcc.Visible = true;
                ARAcc.Visible = true;
            }
        }

    }
}