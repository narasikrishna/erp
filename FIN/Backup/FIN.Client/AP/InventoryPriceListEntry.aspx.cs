﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.DAL.GL;
using FIN.BLL;
using FIN.BLL.GL;
using FIN.BLL.HR;
using VMVServices.Web;
using System.Collections;
using VMVServices.Services.Data;

namespace FIN.Client.AP
{
    public partial class InventoryPriceListEntry : PageBase
    {
        INV_PRICE_LIST_HDR iNV_PRICE_LIST_HDR = new INV_PRICE_LIST_HDR();
        INV_PRICE_LIST_DTL iNV_PRICE_LIST_DTL = new INV_PRICE_LIST_DTL();
        DataTable dtGridData = new DataTable();
        string career_path_type;
        Boolean bol_rowVisiable;
        Boolean savedBool;
        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TAE", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));


            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
        }

        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        private void FillComboBox()
        {
            FIN.BLL.AP.InventoryPriceList_BLL.GetPriceListCurrency(ref ddlPriceListCurrency);
            Lookup_BLL.GetLookUpValues(ref ddlPricelistType, "PRICELIST_TYPE");
            FIN.BLL.AP.Supplier_BLL.GetSupplierName(ref ddlsupplier);
        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                Startup();
                FillComboBox();
                EntityData = null;
                Session[FINSessionConstants.GridData] = null;

                dtGridData = FIN.BLL.AP.InventoryPriceList_BLL.getChildEntityDet(Master.StrRecordId);
                BindGrid(dtGridData);

                btnPrint.Visible = false;
                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    btnPrint.Visible = true;
                    using (IRepository<INV_PRICE_LIST_HDR> userCtx = new DataRepository<INV_PRICE_LIST_HDR>())
                    {
                        iNV_PRICE_LIST_HDR = userCtx.Find(r =>
                            (r.PRICE_HDR_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = iNV_PRICE_LIST_HDR;
                    txtPriceListName.Text = iNV_PRICE_LIST_HDR.PRICE_LIST_NAME;
                    txtPriceListDescription.Text = iNV_PRICE_LIST_HDR.PRICE_LIST_DESC;
                    ddlPriceListCurrency.SelectedValue = iNV_PRICE_LIST_HDR.PRICE_LIST_CURRENCY;
                    ddlPricelistType.SelectedValue = iNV_PRICE_LIST_HDR.PRICE_LIST_TYPE;
                    txtPaymentTerms.Text = iNV_PRICE_LIST_HDR.PRICE_LIST_PAY_TERMS;
                    txtFrieghtTerms.Text = iNV_PRICE_LIST_HDR.PRICE_LIST_FRIEGHT_TERMS;
                    ddlsupplier.SelectedValue = iNV_PRICE_LIST_HDR.VENDOR_ID;
                    if (iNV_PRICE_LIST_HDR.PRICE_LIST_START_DT != null)
                    {
                        txtFromDate.Text = DBMethod.ConvertDateToString(iNV_PRICE_LIST_HDR.PRICE_LIST_START_DT.ToString());
                    }
                    if (iNV_PRICE_LIST_HDR.PRICE_LIST_END_DT != null)
                    {
                        txtToDate.Text = DBMethod.ConvertDateToString(iNV_PRICE_LIST_HDR.PRICE_LIST_END_DT.ToString());
                    }

                    if (iNV_PRICE_LIST_HDR.ENABLED_FLAG == FINAppConstants.EnabledFlag)
                    {
                        chkActive.Checked = true;
                    }
                    else
                    {
                        chkActive.Checked = false;
                    }

                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TAEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);

                }
            }

        }

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;

                if (dtData.Rows.Count > 0)
                {
                    if (dtData.Rows[0]["PRICE_ITEM_COST"].ToString().Length > 0)
                    {
                        dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("PRICE_ITEM_COST", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("PRICE_ITEM_COST"))));
                    }
                    dtData.AcceptChanges();
                }


                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr["ENABLED_FLAG"] = false;
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TAE_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        protected void txtItemCost_TextChanged(object sender, EventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            TextBox txtItemCost = (TextBox)gvr.FindControl("txtItemCost");
            if (CommonUtils.ConvertStringToDecimal(txtItemCost.Text) > 0)
            {
                txtItemCost.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtItemCost.Text);
            }
        }

        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();
                DropDownList ddlItemCode = tmpgvr.FindControl("ddlItemCode") as DropDownList;
                FIN.BLL.AP.InventoryPriceList_BLL.GetItemCode(ref ddlItemCode);

                //Label lblItemCost = tmpgvr.FindControl("lblItemCost") as Label;
                //if (CommonUtils.ConvertStringToDecimal(lblItemCost.Text) > 0)
                //{
                //    lblItemCost.Text = DBMethod.GetAmtDecimalCommaSeparationValue(lblItemCost.Text);
                //}

                if (gvData.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                {
                    ddlItemCode.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["ITEM_ID"].ToString();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TAEFillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        #endregion
        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                System.Collections.SortedList slControls = new System.Collections.SortedList();

                slControls[0] = ddlPriceListCurrency;
                slControls[1] = ddlPricelistType;
                //slControls[2] = ddlCareerPathName;


                string strCtrlTypes = FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST;
                string strMessage = "PriceList Currency ~ PriceList Type  ";

                EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

                if (EmptyErrorCollection.Count > 0)
                {
                    ErrorCollection = EmptyErrorCollection;
                    return;
                }

                ErrorCollection.Clear();

                ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session[FINSessionConstants.GridData], "Price List ");

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                AssignToBE();

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                if (savedBool)
                {
                    //txtAttendanceId.Text = hR_TRM_ATTEND_HDR.ATT_HDR_ID;
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    iNV_PRICE_LIST_HDR = (INV_PRICE_LIST_HDR)EntityData;
                }

                iNV_PRICE_LIST_HDR.PRICE_LIST_NAME = txtPriceListName.Text;
                iNV_PRICE_LIST_HDR.PRICE_LIST_DESC = txtPriceListDescription.Text;
                iNV_PRICE_LIST_HDR.PRICE_LIST_CURRENCY = ddlPriceListCurrency.SelectedValue.ToString();
                iNV_PRICE_LIST_HDR.PRICE_LIST_TYPE = ddlPricelistType.SelectedValue.ToString();
                iNV_PRICE_LIST_HDR.PRICE_LIST_PAY_TERMS = txtPaymentTerms.Text;
                iNV_PRICE_LIST_HDR.PRICE_LIST_FRIEGHT_TERMS = txtFrieghtTerms.Text;
                iNV_PRICE_LIST_HDR.VENDOR_ID = ddlsupplier.SelectedValue;
                if (txtFromDate.Text != string.Empty)
                {
                    iNV_PRICE_LIST_HDR.PRICE_LIST_START_DT = DBMethod.ConvertStringToDate(txtFromDate.Text.ToString());
                }
                if (txtToDate.Text != string.Empty)
                {
                    iNV_PRICE_LIST_HDR.PRICE_LIST_END_DT = DBMethod.ConvertStringToDate(txtToDate.Text.ToString());
                }

                iNV_PRICE_LIST_HDR.PRICE_ORG_ID = VMVServices.Web.Utils.OrganizationID;


                iNV_PRICE_LIST_HDR.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                iNV_PRICE_LIST_HDR.PRICE_ORG_ID = VMVServices.Web.Utils.OrganizationID;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    iNV_PRICE_LIST_HDR.MODIFIED_BY = this.LoggedUserName;
                    iNV_PRICE_LIST_HDR.MODIFIED_DATE = DateTime.Today;
                }
                else
                {
                    iNV_PRICE_LIST_HDR.PRICE_HDR_ID = FINSP.GetSPFOR_SEQCode("AP_023_M".ToString(), false, true);
                    //hR_TRM_FEEDBACK_HDR.PK_ID = DBMethod.GetPrimaryKeyValue(FINSequenceConstant.hR_TRM_SCHEDULE_HDR_SEQ);
                    iNV_PRICE_LIST_HDR.CREATED_BY = this.LoggedUserName;
                    iNV_PRICE_LIST_HDR.CREATED_DATE = DateTime.Today;
                }

                iNV_PRICE_LIST_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, iNV_PRICE_LIST_HDR.PRICE_HDR_ID);

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                var tmpChildEntity = new List<Tuple<object, string>>();

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    iNV_PRICE_LIST_DTL = new INV_PRICE_LIST_DTL();
                    if (dtGridData.Rows[iLoop]["PRICE_DTL_ID"].ToString() != "0" && dtGridData.Rows[iLoop]["PRICE_DTL_ID"].ToString() != string.Empty)
                    {
                        using (IRepository<INV_PRICE_LIST_DTL> userCtx = new DataRepository<INV_PRICE_LIST_DTL>())
                        {
                            iNV_PRICE_LIST_DTL = userCtx.Find(r =>
                                (r.PRICE_DTL_ID == dtGridData.Rows[iLoop]["PRICE_DTL_ID"].ToString())
                                ).SingleOrDefault();
                        }
                    }

                    //iNV_PRICE_LIST_DTL.PRICE_HDR_ID = dtGridData.Rows[iLoop]["PRICE_HDR_ID"].ToString();
                    iNV_PRICE_LIST_DTL.PRICE_ITEM_ID = dtGridData.Rows[iLoop]["PRICE_ITEM_ID"].ToString();
                    iNV_PRICE_LIST_DTL.PRICE_ITEM_UOM = dtGridData.Rows[iLoop]["PRICE_ITEM_UOM"].ToString();
                    iNV_PRICE_LIST_DTL.PRICE_ITEM_COST = Decimal.Parse(dtGridData.Rows[iLoop]["PRICE_ITEM_COST"].ToString());
                    //hR_CAREER_PLAN_DTL.PLAN_PROFILE_REQUIRED_VALUE = short.Parse(dtGridData.Rows[iLoop]["PLAN_PROFILE_REQUIRED_VALUE"].ToString());

                    iNV_PRICE_LIST_DTL.PRICE_HDR_ID = iNV_PRICE_LIST_HDR.PRICE_HDR_ID;
                    iNV_PRICE_LIST_DTL.WORKFLOW_COMPLETION_STATUS = "1";
                    //iNV_PRICE_LIST_DTL.ENABLED_FLAG = "1";
                    iNV_PRICE_LIST_DTL.ENABLED_FLAG = (dtGridData.Rows[iLoop]["ENABLED_FLAG"].ToString() == "TRUE" ? FINAppConstants.EnabledFlag : FINAppConstants.DisabledFlag);


                    if (dtGridData.Rows[iLoop]["PRICE_DTL_ID"].ToString() != "0" && dtGridData.Rows[iLoop]["PRICE_DTL_ID"].ToString() != string.Empty)
                    {
                        iNV_PRICE_LIST_DTL.PRICE_DTL_ID = dtGridData.Rows[iLoop]["PRICE_DTL_ID"].ToString();
                        iNV_PRICE_LIST_DTL.MODIFIED_BY = this.LoggedUserName;
                        iNV_PRICE_LIST_DTL.MODIFIED_DATE = DateTime.Today;

                        tmpChildEntity.Add(new Tuple<object, string>(iNV_PRICE_LIST_DTL, "U"));
                    }
                    else
                    {
                        iNV_PRICE_LIST_DTL.PRICE_DTL_ID = FINSP.GetSPFOR_SEQCode("AP_023_D".ToString(), false, true);
                        iNV_PRICE_LIST_DTL.CREATED_BY = this.LoggedUserName;
                        iNV_PRICE_LIST_DTL.CREATED_DATE = DateTime.Today;
                        //DBMethod.SaveEntity<GL_ACCT_CODE_SEGMENTS>(gL_ACCT_CODE_SEGMENTS);
                        tmpChildEntity.Add(new Tuple<object, string>(iNV_PRICE_LIST_DTL, "A"));
                    }

                }

                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            FIN.BLL.HR.Competency_BLL.SavePCEntity<INV_PRICE_LIST_HDR, INV_PRICE_LIST_DTL>(iNV_PRICE_LIST_HDR, tmpChildEntity, iNV_PRICE_LIST_DTL);
                            savedBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {
                            FIN.BLL.HR.Competency_BLL.SavePCEntity<INV_PRICE_LIST_HDR, INV_PRICE_LIST_DTL>(iNV_PRICE_LIST_HDR, tmpChildEntity, iNV_PRICE_LIST_DTL, true);
                            savedBool = true;
                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TAEEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }




        protected void btnYes_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    DBMethod.DeleteEntity<INV_PRICE_LIST_DTL>(iNV_PRICE_LIST_DTL);
                }
                DBMethod.DeleteEntity<INV_PRICE_LIST_HDR>(iNV_PRICE_LIST_HDR);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TAE_DELETE", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        #endregion
        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            if (Session[FINSessionConstants.GridData] != null)
            {
                dtGridData = (DataTable)Session[FINSessionConstants.GridData];
            }
            gvData.EditIndex = -1;

            BindGrid(dtGridData);

        }

        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("COM_ROW_CMD", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {


            System.Collections.SortedList slControls = new System.Collections.SortedList();
            CheckBox chkGridActive = gvr.FindControl("chkGridActive") as CheckBox;

            DropDownList ddlItemCode = gvr.FindControl("ddlItemCode") as DropDownList;

            TextBox txtItemName = gvr.FindControl("txtItemName") as TextBox;
            TextBox txtItemUOM = gvr.FindControl("txtItemUOM") as TextBox;
            TextBox txtItemCost = gvr.FindControl("txtItemCost") as TextBox;
            //TextBox txtReqValue = gvr.FindControl("txtReqValue") as TextBox;
            //CheckBox chkact = gvr.FindControl("chkActive") as CheckBox;

            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList["PRICE_DTL_ID"] = "0";
                //txtlineno.Text = (tmpdtGridData.Rows.Count + 1).ToString();
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }

            // txtlineno.Text = (txtlineno.Text + rowindex + 1).ToString();

            slControls[0] = ddlItemCode;
            slControls[1] = txtItemName;
            slControls[2] = txtItemUOM;
            slControls[3] = txtItemCost;
            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/AP_" + Session["Sel_Lng"].ToString() + ".properties"));

            ErrorCollection.Clear();
            string strCtrlTypes = "DropDownList~TextBox~TextBox~TextBox";
            string strMessage = Prop_File_Data["Item_Code_P"] + " ~ " + Prop_File_Data["Item_Name_P"] + " ~ " + Prop_File_Data["Item_UOM_P"] + " ~ " + Prop_File_Data["Item_Cost_P"] + "";
            // string strMessage = "  Item Code ~ Item Name ~ Item UOM ~ Item Cost";

            EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

            if (EmptyErrorCollection.Count > 0)
            {
                ErrorCollection = EmptyErrorCollection;
                return drList;
            }

            if (chkGridActive.Checked)
            {
                drList[FINColumnConstants.ENABLED_FLAG] = "TRUE";
            }
            else
            {
                drList[FINColumnConstants.ENABLED_FLAG] = "FALSE";
            }


            drList["ITEM_CODE"] = ddlItemCode.SelectedItem.Text;
            drList["PRICE_ITEM_ID"] = ddlItemCode.SelectedValue;

            drList["Item_Name"] = txtItemName.Text;

            drList["PRICE_ITEM_UOM"] = txtItemUOM.Text;
            drList["PRICE_ITEM_COST"] = DBMethod.GetAmtDecimalCommaSeparationValue(CommonUtils.ConvertStringToDecimal(txtItemCost.Text).ToString());

            drList[FINColumnConstants.DELETED] = FINAppConstants.N;

            return drList;

        }



        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("COM_ROW_UP", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("COM_ROW_DEL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("COM_ROW_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("COM_RowCreated", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("COM_RDB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        #endregion
        protected void btnDelete_Click(object sender, EventArgs e)
        {

        }


        protected void ddlItemCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {

                GridViewRow gvr = gvData.FooterRow;

                DropDownList ddlItemCode = (DropDownList)gvr.FindControl("ddlItemCode");
                TextBox txtItemName = (TextBox)gvr.FindControl("txtItemName");
                TextBox txtItemUOM = (TextBox)gvr.FindControl("txtItemUOM");
                TextBox txtItemCost = (TextBox)gvr.FindControl("txtItemCost");

                INV_ITEM_MASTER iNV_ITEM_MASTER = new INV_ITEM_MASTER();

                using (IRepository<INV_ITEM_MASTER> ssm = new DataRepository<INV_ITEM_MASTER>())
                {
                    iNV_ITEM_MASTER = ssm.Find(x => (x.ITEM_ID == ddlItemCode.SelectedValue.ToString())).SingleOrDefault();
                    if (iNV_ITEM_MASTER != null)
                    {
                        Session["Item Code"] = iNV_ITEM_MASTER.ITEM_CODE;
                    }
                }
                DataTable dt_std_rate = new DataTable();
                dt_std_rate = FIN.BLL.AP.InventoryPriceList_BLL.getItemDtls(ddlItemCode.SelectedValue);
                if (dt_std_rate != null)
                {
                    if (dt_std_rate.Rows.Count > 0)
                    {
                        txtItemName.Text = dt_std_rate.Rows[0]["Item_Name"].ToString();
                        txtItemUOM.Text = dt_std_rate.Rows[0]["uom_desc"].ToString();
                        txtItemCost.Text = dt_std_rate.Rows[0]["ITEM_UNIT_PRICE"].ToString();
                    }

                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {

                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        protected void btnPrint_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    htFilterParameter.Add("InvPriceListHdrID", Master.StrRecordId.ToString());
                }

                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;
                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;

                ReportData = FIN.BLL.AP.InventoryPriceList_BLL.GetInventoryPriceListReport();

                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());

                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerPath + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("InvPriceListReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
    }
}