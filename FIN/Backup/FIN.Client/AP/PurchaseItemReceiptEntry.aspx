﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="PurchaseItemReceiptEntry.aspx.cs" Inherits="FIN.Client.AP.PurchaseItemReceiptEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1000px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblGRNNumber" runat="server">
                Goods Received Note Number
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px" id="lblGRNNumValue" runat="server">
                <asp:TextBox ID="txtGRNNumber" runat="server" MaxLength="50" CssClass=" txtBox" Enabled="false"
                    TabIndex="1"></asp:TextBox>
            </div>
            <div class="colspace LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 180px" id="lblGRNDate">
                Goods Received Note Date
            </div>
            <div class="divtxtBox LNOrient" style="width: 163px">
                <asp:TextBox ID="txtGRNDate" runat="server" TabIndex="2" CssClass="validate[required] RequiredField txtBox"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calExtender1" TargetControlID="txtGRNDate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblSupplierName ">
                Supplier Name
            </div>
            <div class="divtxtBox LNOrient" style="width: 515px">
                <asp:DropDownList ID="ddlSupplierName" runat="server" TabIndex="3" AutoPostBack="true"
                    CssClass="ddlStype RequiredField validate[required]" OnSelectedIndexChanged="ddlSupplierName_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="Div1">
                Supplier Site
            </div>
            <div class="divtxtBox LNOrient" style=" width: 515px">
                <asp:DropDownList ID="ddlSupplierSite" runat="server" TabIndex="4" CssClass="ddlStype ">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblEmployeeName">
                Received By
            </div>
            <div class="divtxtBox LNOrient" style="width: 515px">
                <asp:DropDownList ID="ddlEmployeename" runat="server" Enabled="true" CssClass="ddlStype"
                    TabIndex="5">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblModeofTransport">
                Mode of Transport
            </div>
            <div class="divtxtBox LNOrient" style="width: 155px">
                <asp:DropDownList ID="ddlModeofTransport" runat="server" CssClass="ddlStype" TabIndex="6">
                </asp:DropDownList>
            </div>
            <div class="colspace LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 195px" id="lblInspectionRequired">
                Inspection Required
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:CheckBox ID="chkInspection" runat="server" TabIndex="7" OnCheckedChanged="chkInspection_CheckedChanged"
                    AutoPostBack="true" />
            </div>
             <div class="divClear_10">
            </div>
            <div id="dvApprv" class="divRowContainer" runat="server">
                <div class="lblBox LNOrient" style="width: 200px" id="lblApproverName">
                    Approver Name
                </div>
                <div class="divtxtBox LNOrient" style="width: 520px">
                    <asp:TextBox ID="txtApproverName" CssClass="txtBox" runat="server" TabIndex="8"
                        MaxLength="50"></asp:TextBox>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div id="dvRemarks" class="divRowContainer" runat="server">
                <div class="lblBox LNOrient" style="width: 200px" id="lblRemarks">
                    Remarks
                </div>
                <div class="divtxtBox LNOrient" style="width: 520px">
                    <asp:TextBox ID="txtRemarks" CssClass="txtBox" Height="50px" runat="server" TabIndex="9"
                        MaxLength="50" TextMode="MultiLine"></asp:TextBox>
                </div>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" runat="server" visible="false" id="divInspby">
            <div class="lblBox LNOrient" style="width: 200px" id="Div2">
                Inspected By
            </div>
            <div class="divtxtBox LNOrient" style="width: 515px">
                <asp:DropDownList ID="ddlInspected" runat="server" Enabled="true" CssClass="ddlStype RequiredField validate[required]"
                    TabIndex="10">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer LNOrient">
            <h1 class="lblBox" style="font-weight: bold; font-size: 14px;">
                Shipment Details</h1>
        </div>
        <div class="divClear_10">
        </div>
        <div class="ship-bg">
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="width: 200px" id="lblShipmentNumber">
                    Shipment Number
                </div>
                <div class="divtxtBox LNOrient" style="width: 150px">
                    <asp:TextBox ID="txtShipmentNumber" runat="server" CssClass="txtBox_N" TabIndex="11"
                        MaxLength="50" OnTextChanged="txtShipmentNumber_TextChanged"></asp:TextBox>
                </div>
                <div class="colspace LNOrient" >
                    &nbsp</div>
                <div class="lblBox LNOrient" style="width: 180px" id="lblShipmentDate">
                    Shipment Date
                </div>
                <div class="divtxtBox  LNOrient" style="width: 165px">
                    <asp:TextBox ID="txtShipmentDate" CssClass="txtBox" runat="server" TabIndex="12"
                        MaxLength="50"></asp:TextBox>
                    <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="shipCal" TargetControlID="txtShipmentDate"
                        OnClientDateSelectionChanged="checkDate" />
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style=" width: 200px" id="lblShipmentTrackingNo">
                    Shipment Tracking Number
                </div>
                <div class="divtxtBox LNOrient" style=" width: 150px">
                    <asp:TextBox ID="txtShipmentTrackingNo" CssClass="txtBox_N" runat="server" TabIndex="13"
                        MaxLength="50"></asp:TextBox>
                </div>
                <div class="colspace LNOrient" >
                    &nbsp</div>
                <div class="lblBox LNOrient" style="width: 180px" id="lblShipmentReceivedby">
                    Shipment Received By
                </div>
                <div class="divtxtBox LNOrient" style=" width: 165px">
                    <asp:TextBox ID="txtShipmentReceivedby" CssClass="txtBox" runat="server" TabIndex="14"
                        MaxLength="50"></asp:TextBox>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="width: 200px" id="lblShipmentCompany">
                    Shipment Company
                </div>
                <div class="divtxtBox LNOrient" style="width: 150px">
                    <asp:TextBox ID="txtShipmentCompany" CssClass="txtBox" runat="server" TabIndex="15"
                        MaxLength="50"></asp:TextBox>
                </div>
            </div>
           
        </div>
        <div class="divClear_10">
        </div>
        <div class=" LNOrient">
            <asp:HiddenField runat="server" ID="hdItemId" />
            <asp:HiddenField runat="server" ID="hdLineId" />
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" Width="800px"
                CssClass="DisplayFont Grid" OnRowCancelingEdit="gvData_RowCancelingEdit" OnRowCommand="gvData_RowCommand"
                OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound" OnRowDeleting="gvData_RowDeleting"
                OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating" ShowFooter="True"
                DataKeyNames="RECEIPT_ID,RECEIPT_DTL_ID,PO_LINE_NUM,PO_HEADER_ID,PO_LINE_ID,item_id,LINE_NUM">
                <Columns>
                    <asp:TemplateField HeaderText="Line No">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtLineNo" runat="server" Enabled="false" Text='<%# Eval("LINE_NUM") %>'
                                TabIndex="16" Width="50px" CssClass="EntryFont RequiredField txtBox" MaxLength="50"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtLineNo" runat="server" MaxLength="50" Enabled="false" TabIndex="16"
                                CssClass="EntryFont RequiredField txtBox" Width="50px"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblLineNo" runat="server" Text='<%# Container.DataItemIndex + 1 %>'
                                Width="50px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Purchase Order Number">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlPoNumber" runat="server" CssClass="ddlStype" AutoPostBack="true"
                                Width="150px" TabIndex="17" OnSelectedIndexChanged="ddlPoNumber_SelectedIndexChanged">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlPoNumber" runat="server" CssClass="ddlStype" AutoPostBack="true"
                                Width="150px" TabIndex="17" OnSelectedIndexChanged="ddlPoNumber_SelectedIndexChanged">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblPONumber" runat="server" Text='<%# Eval("PO_NUM") %>' Width="150px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Purchase Order Line No">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtPOLineNo" runat="server" MaxLength="250" Text='<%# Eval("PO_LINE_NUM") %>'
                                TabIndex="18" Enabled="false" CssClass="EntryFont RequiredField txtBox" Width="60px"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtPOLineNo" MaxLength="250" Width="60px" runat="server" Enabled="false"
                                TabIndex="18" CssClass="EntryFont RequiredField txtBox"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblPOLineNo" runat="server" Text='<%# Eval("PO_LINE_NUM") %>' Width="60px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Item/Description">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtItemName" runat="server" MaxLength="250" Enabled="false" TabIndex="19"
                                CssClass="EntryFont RequiredField txtBox"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtItemName" runat="server" MaxLength="250" Enabled="false" TabIndex="19"
                                Width="250" CssClass="EntryFont RequiredField txtBox"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblItemName" runat="server" Text='<%# Eval("ITEM_NAME") %>' Width="250px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Purchase Order Quantity">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtQuantity" MaxLength="10" Enabled="false" runat="server" Text='<%# Eval("PO_QUANTITY") %>'
                                TabIndex="20" CssClass="EntryFont RequiredField txtBox_N" Width="80px"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers"
                                TargetControlID="txtQuantity" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtQuantity" MaxLength="10" Enabled="false" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                TabIndex="20" Width="80px"></asp:TextBox><cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender66"
                                    runat="server" FilterType="Numbers" TargetControlID="txtQuantity" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblQuantity" runat="server" Text='<%# Eval("PO_QUANTITY") %>' Width="80px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:TemplateField>
                   
                      <asp:TemplateField HeaderText="Existing Received Quantity">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtAlreadyApprovedQuantity" Enabled="false" MaxLength="10" runat="server" Text='<%# Eval("QTY_ALREADY_APPROVED") %>'
                                TabIndex="21" CssClass="EntryFont RequiredField txtBox_N" Width="80px"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilaeeteredTextBoxExtender27" runat="server" FilterType="Numbers"
                                TargetControlID="txtAlreadyApprovedQuantity" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtAlreadyApprovedQuantity"  Enabled="false" MaxLength="10" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                TabIndex="21" Width="80px"></asp:TextBox><cc2:FilteredTextBoxExtender ID="FilateredTssextBoxExtender23"
                                    runat="server" FilterType="Numbers" TargetControlID="txtAlreadyApprovedQuantity" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblAlreadyQuantity" runat="server" Text='<%# Eval("QTY_ALREADY_APPROVED") %>'
                                Width="80px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Received Quantity">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtReceivedQuantity" MaxLength="10" runat="server" Text='<%# Eval("QTY_RECEIVED") %>'
                                TabIndex="21" CssClass="EntryFont RequiredField txtBox_N" Width="80px"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender27" runat="server" FilterType="Numbers"
                                TargetControlID="txtReceivedQuantity" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtReceivedQuantity" MaxLength="10" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                TabIndex="21" Width="80px"></asp:TextBox><cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender23"
                                    runat="server" FilterType="Numbers" TargetControlID="txtReceivedQuantity" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblReceivedQuantity" runat="server" Text='<%# Eval("QTY_RECEIVED") %>'
                                Width="80px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Approved Quantity">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtApprovedQuantity" MaxLength="10" runat="server" Enabled="true"
                                Text='<%# Eval("QTY_APPROVED") %>' TabIndex="22" CssClass="EntryFont RequiredField txtBox_N"
                                Width="80px"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxE3xtender27" runat="server" FilterType="Numbers"
                                TargetControlID="txtApprovedQuantity" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtApprovedQuantity" MaxLength="10" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                TabIndex="22" Width="80px"></asp:TextBox><cc2:FilteredTextBoxExtender ID="FilteredText3BoxExtender23"
                                    runat="server" FilterType="Numbers" TargetControlID="txtApprovedQuantity" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblaPPROVEDQuantity" runat="server" Text='<%# Eval("QTY_APPROVED") %>'
                                Width="80px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Rejected Quantity">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtRejectedQuantity" MaxLength="10" runat="server" Enabled="true"
                                Text='<%# Eval("QTY_REJECTED") %>' TabIndex="23" CssClass="EntryFont RequiredField txtBox_N"
                                Width="80px"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender28" runat="server" FilterType="Numbers"
                                TargetControlID="txtRejectedQuantity" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtRejectedQuantity" MaxLength="10" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                TabIndex="23" Width="80px"></asp:TextBox><cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender_23"
                                    runat="server" FilterType="Numbers" TargetControlID="txtRejectedQuantity" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblRejectedQuantity" runat="server" Text='<%# Eval("QTY_REJECTED") %>'
                                Width="80px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Remarks">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtRemarks" MaxLength="250" runat="server" Text='<%# Eval("REMARKS") %>'
                                TabIndex="24" CssClass="EntryFont txtBox" Width="100px"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtRemarks" MaxLength="250" runat="server" CssClass="EntryFont txtBox"
                                TabIndex="24" Width="200px"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblREMARKS" runat="server" Text='<%# Eval("REMARKS") %>' Width="100px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" runat="server" AlternateText="Edit" CausesValidation="false"
                                TabIndex="25" ToolTip="Edit" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                            <asp:ImageButton ID="ibtnDelete" runat="server" AlternateText="Delete" CausesValidation="false"
                                TabIndex="25" ToolTip="Delete" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibtnUpdate" runat="server" AlternateText="Update" CommandName="Update"
                                TabIndex="25" Height="20px" ImageUrl="~/Images/Update.png" />
                            <asp:ImageButton ID="ibtnCancel" runat="server" AlternateText="Cancel" CausesValidation="false"
                                TabIndex="25" ToolTip="Add" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="ibtnInsert" runat="server" AlternateText="Add" CommandName="FooterInsert"
                                TabIndex="25" ToolTip="Add" ImageUrl="~/Images/Add.jpg" />
                        </FooterTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                            TabIndex="26" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="27" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="28" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="29" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table width="100%">
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/AP/APChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
