﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.BLL;
using FIN.BLL.AP;
using VMVServices.Web;


namespace FIN.Client.AP
{
    public partial class ReceiptLotDetailsEntry : PageBase
    {

        INV_RECEIPT_LOTS_HDR iNV_RECEIPT_LOTS_HDR = new INV_RECEIPT_LOTS_HDR();
        INV_RECEIPT_LOT_DTLS iNV_RECEIPT_LOT_DTLS = new INV_RECEIPT_LOT_DTLS();
        ReceiptLotDetails_BLL ReceiptLotDetails_BLL = new ReceiptLotDetails_BLL();
        DataTable dtGridData = new DataTable();
        Boolean bol_rowVisiable;
        Boolean savedBool;
        string ProReturn = null;
        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RLD_PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));


            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
        }

        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>
        /// 

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void FillComboBox()
        {
            ReceiptLotDetails_BLL.fn_getGRNNo(ref ddlGRNNumber);
            //ReceiptLotDetails_BLL.fn_getLineNo(ref ddlLineNumber);

        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                //txtLotNumber.Enabled = false;
                txtGRNDate.Enabled = false;
                txtPONumber.Enabled = false;
                txtItemDescription.Enabled = false;
                txtPOLineNumber.Enabled = false;

                FillComboBox();

                EntityData = null;

                dtGridData = FIN.BLL.AP.ReceiptLotDetails_BLL.getChildEntityDet(Master.StrRecordId);
                BindGrid(dtGridData);


                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    txtQuantityforthislot.Enabled = false;

                    using (IRepository<INV_RECEIPT_LOTS_HDR> userCtx = new DataRepository<INV_RECEIPT_LOTS_HDR>())
                    {
                        iNV_RECEIPT_LOTS_HDR = userCtx.Find(r =>
                            (r.LOT_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = iNV_RECEIPT_LOTS_HDR;

                    txtLotNumber.Enabled = false;
                    ddlGRNNumber.Enabled = false;
                    ddlLineNumber.Enabled = false;

                    if (iNV_RECEIPT_LOTS_HDR.LOT_NUMBER != null)
                    {
                        txtLotNumber.Text = iNV_RECEIPT_LOTS_HDR.LOT_NUMBER;
                    }
                    if (iNV_RECEIPT_LOTS_HDR.RECEIPT_ID != null)
                    {
                        ddlGRNNumber.SelectedValue = iNV_RECEIPT_LOTS_HDR.RECEIPT_ID.ToString();

                        BindLineDetails();
                    }
                    if (iNV_RECEIPT_LOTS_HDR.RECEIPT_DTL_ID != null)
                    {
                        ddlLineNumber.SelectedValue = iNV_RECEIPT_LOTS_HDR.RECEIPT_DTL_ID.ToString();
                    }
                    if (iNV_RECEIPT_LOTS_HDR.LOT_QTY != null)
                    {
                        txtQuantityforthislot.Text = iNV_RECEIPT_LOTS_HDR.LOT_QTY.ToString();
                        BindPODetails();
                    }

                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RLD_ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);

                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    iNV_RECEIPT_LOTS_HDR = (INV_RECEIPT_LOTS_HDR)EntityData;
                }

                iNV_RECEIPT_LOTS_HDR.LOT_NUMBER = txtLotNumber.Text;
                iNV_RECEIPT_LOTS_HDR.RECEIPT_ID = ddlGRNNumber.SelectedValue.ToString();
                iNV_RECEIPT_LOTS_HDR.RECEIPT_DTL_ID = ddlLineNumber.SelectedValue.ToString();
                iNV_RECEIPT_LOTS_HDR.LOT_QTY = CommonUtils.ConvertStringToDecimal(txtQuantityforthislot.Text);
                iNV_RECEIPT_LOTS_HDR.ITEM_ID = hid_Item_Id.Value;
                iNV_RECEIPT_LOTS_HDR.ENABLED_FLAG = FINAppConstants.Y;

                iNV_RECEIPT_LOTS_HDR.LOT_ORG_ID = VMVServices.Web.Utils.OrganizationID;



                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    iNV_RECEIPT_LOTS_HDR.MODIFIED_BY = this.LoggedUserName;
                    iNV_RECEIPT_LOTS_HDR.MODIFIED_DATE = DateTime.Today;
                }
                else
                {
                    // iNV_RECEIPT_LOTS_HDR.LOT_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.AP_015_LN.ToString(), false, true);
                    //hid_lotno.Value = iNV_RECEIPT_LOTS_HDR.LOT_NUMBER;
                    //txtLotNumber.Text = hid_lotno.Value;
                    iNV_RECEIPT_LOTS_HDR.LOT_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.AP_015_M.ToString(), false, true);
                    iNV_RECEIPT_LOTS_HDR.PK_ID = DBMethod.GetPrimaryKeyValue(FINSequenceConstant.INV_RECEIPT_LOTS_HDR_SEQ);
                    iNV_RECEIPT_LOTS_HDR.CREATED_BY = this.LoggedUserName;
                    iNV_RECEIPT_LOTS_HDR.CREATED_DATE = DateTime.Today;
                }
                iNV_RECEIPT_LOTS_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, iNV_RECEIPT_LOTS_HDR.LOT_ID);

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                FIN.BLL.CommonUtils.DeleteData(typeof(INV_RECEIPT_LOT_DTLS).Name.ToString(), "PK_ID", iNV_RECEIPT_LOTS_HDR.PK_ID.ToString(), "", "", "");

                ErrorCollection = CommonUtils.IsEmptyGrid(dtGridData, "Attribute value ");
                if (ErrorCollection.Count > 0)
                {
                    return;
                }


                var tmpChildEntity = new List<Tuple<object, string>>();
                INV_RECEIPT_LOT_DTLS iNV_RECEIPT_LOT_DTLS = new INV_RECEIPT_LOT_DTLS();
                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    iNV_RECEIPT_LOT_DTLS = new INV_RECEIPT_LOT_DTLS();
                    if ((dtGridData.Rows[iLoop]["PK_ID"].ToString()) != "0" && dtGridData.Rows[iLoop]["PK_ID"].ToString() != string.Empty)
                    {
                        using (IRepository<INV_RECEIPT_LOT_DTLS> userCtx = new DataRepository<INV_RECEIPT_LOT_DTLS>())
                        {
                            iNV_RECEIPT_LOT_DTLS = userCtx.Find(r =>
                                (r.PK_ID == int.Parse(dtGridData.Rows[iLoop]["PK_ID"].ToString()))
                                ).SingleOrDefault();
                        }
                    }

                    iNV_RECEIPT_LOT_DTLS = new INV_RECEIPT_LOT_DTLS();

                    iNV_RECEIPT_LOT_DTLS.ATTRIBUTE_TYPE = dtGridData.Rows[iLoop]["CODE"].ToString();
                    iNV_RECEIPT_LOT_DTLS.ATTRIBUTE_VALUE = dtGridData.Rows[iLoop]["ATTRIBUTE_VALUE"].ToString();

                    iNV_RECEIPT_LOT_DTLS.LOT_ID = iNV_RECEIPT_LOTS_HDR.LOT_ID;

                    iNV_RECEIPT_LOT_DTLS.ENABLED_FLAG = "1";
                    iNV_RECEIPT_LOT_DTLS.CHILD_ID = iNV_RECEIPT_LOTS_HDR.PK_ID;



                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {

                        tmpChildEntity.Add(new Tuple<object, string>(iNV_RECEIPT_LOT_DTLS, "D"));
                        tmpChildEntity.Remove(new Tuple<object, string>(iNV_RECEIPT_LOT_DTLS, "D"));
                    }
                    else
                    {
                        if ((dtGridData.Rows[iLoop]["PK_ID"].ToString()) != "0" && dtGridData.Rows[iLoop]["PK_ID"].ToString() != string.Empty)
                        {
                            // iNV_RECEIPT_LOT_DTLS.PK_ID = int.Parse(dtGridData.Rows[iLoop]["PK_ID"].ToString());
                            iNV_RECEIPT_LOT_DTLS.PK_ID = DBMethod.GetPrimaryKeyValue(FINSequenceConstant.INV_RECEIPT_LOT_DTLS_SEQ);

                            iNV_RECEIPT_LOT_DTLS.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;

                            iNV_RECEIPT_LOT_DTLS.CREATED_BY = this.LoggedUserName;
                            iNV_RECEIPT_LOT_DTLS.CREATED_DATE = DateTime.Today;
                            iNV_RECEIPT_LOT_DTLS.MODIFIED_BY = this.LoggedUserName;
                            iNV_RECEIPT_LOT_DTLS.MODIFIED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(iNV_RECEIPT_LOT_DTLS, "U"));
                        }
                        else
                        {
                            iNV_RECEIPT_LOT_DTLS.PK_ID = DBMethod.GetPrimaryKeyValue(FINSequenceConstant.INV_RECEIPT_LOT_DTLS_SEQ);
                            iNV_RECEIPT_LOT_DTLS.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;

                            iNV_RECEIPT_LOT_DTLS.CREATED_BY = this.LoggedUserName;
                            iNV_RECEIPT_LOT_DTLS.CREATED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(iNV_RECEIPT_LOT_DTLS, "A"));
                        }
                    }
                }
                //ProReturn = FINSP.GetSPFOR_ERR_MGR_CATEGORY_TWO(Master.FormCode, iNV_RECEIPT_LOTS_HDR.LOT_ID, iNV_RECEIPT_LOTS_HDR.RECEIPT_ID, iNV_RECEIPT_LOTS_HDR.RECEIPT_DTL_ID);
                //if (ProReturn != string.Empty)
                //{
                //    if (ProReturn != "0")
                //    {
                //        ErrorCollection.Add("RECEIPTLOTDETAILS", ProReturn);
                //        if (ErrorCollection.Count > 0)
                //        {
                //            return;
                //        }
                //    }
                //}
                // Duplicate Check

                ProReturn = FINSP.GetSPFOR_ERR_MGR_RECP_LOT(txtLotNumber.Text, iNV_RECEIPT_LOTS_HDR.LOT_ID);

                if (ProReturn != string.Empty)
                {

                    if (ProReturn != "0")
                    {
                        ErrorCollection.Add("RECEIPTLOT", ProReturn);
                        if (ErrorCollection.Count > 0)
                        {
                            return;
                        }
                    }
                }


                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            FIN.BLL.GL.AccountCodes_BLL.SavePCEntity<INV_RECEIPT_LOTS_HDR, INV_RECEIPT_LOT_DTLS>(iNV_RECEIPT_LOTS_HDR, tmpChildEntity, iNV_RECEIPT_LOT_DTLS);
                            savedBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {

                            FIN.BLL.GL.AccountCodes_BLL.SavePCEntity<INV_RECEIPT_LOTS_HDR, INV_RECEIPT_LOT_DTLS>(iNV_RECEIPT_LOTS_HDR, tmpChildEntity, iNV_RECEIPT_LOT_DTLS, true);
                            savedBool = true;
                            break;

                        }
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RLD_ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddlAttributetyp = tmpgvr.FindControl("ddlAttributetyp") as DropDownList;
                Lookup_BLL.GetLookUpValues(ref ddlAttributetyp, "ATRTY");


                if (gvData.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                {
                    ddlAttributetyp.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["CODE"].ToString();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RLD_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                if (txtLotNumber.Text.Length == 0)
                {
                    ErrorCollection.Add("lotnumber", "Please Enter the Lot Number");
                    return;
                }

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                ErrorCollection = CommonUtils.IsEmptyGrid(dtGridData, "Attribute value ");
                if (ErrorCollection.Count > 0)
                {
                    return;
                }


                DataTable dtqty = new DataTable();
                dtqty = DBMethod.ExecuteQuery(FIN.DAL.AP.ReceiptLotDetails_DAL.getLotqty(ddlLineNumber.SelectedValue)).Tables[0];
                if (dtqty.Rows.Count > 0)
                {
                    hf_qty.Value = dtqty.Rows[0]["tot_qty"].ToString();
                }

                if (CommonUtils.ConvertStringToDecimal(txtQuantityforthislot.Text) > CommonUtils.ConvertStringToDecimal(hf_qty.Value))
                {
                    ErrorCollection.Add("lotqty", "Quantity Cannot be greater than purchase item received quantity");
                    return;
                }
                //if (CommonUtils.ConvertStringToDecimal(txtQuantityforthislot.Text) <= 0)
                //{
                //    ErrorCollection.Add("lotquantity", "Quantity for this lot should have atleast one lot quantity");
                //}
                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                DataTable dtlotqty = new DataTable();

                dtlotqty = DBMethod.ExecuteQuery(FIN.DAL.AP.ReceiptLotDetails_DAL.GetTotalReceivedLotqty(ddlGRNNumber.SelectedValue, ddlLineNumber.SelectedValue, hid_Item_Id.Value)).Tables[0];
                if (Master.Mode == FINAppConstants.Add && Master.StrRecordId == "0")
                {

                    if (dtlotqty.Rows.Count > 0)
                    {
                        if (CommonUtils.ConvertStringToDecimal(dtlotqty.Rows[0]["lot_qty"].ToString()) > 0)
                        {
                            decimal diffLot = CommonUtils.ConvertStringToDecimal(hf_qty.Value) - CommonUtils.ConvertStringToDecimal(dtlotqty.Rows[0]["lot_qty"].ToString());

                            if (CommonUtils.ConvertStringToDecimal(txtQuantityforthislot.Text) > Math.Abs(diffLot))
                            {
                                ErrorCollection.Add("totlotqty", "Quantity for this lot exceeding the GRN quantity.Received quantity is " + dtlotqty.Rows[0]["lot_qty"].ToString() + ".Pending Quantity is " + diffLot);
                                return;
                            }
                        }
                    }
                    if (ErrorCollection.Count > 0)
                    {
                        return;
                    }
                }

                AssignToBE();
                //ProReturn = FINSP.GetSPFOR_ERR_MGR_CATEGORY_TWO(Master.FormCode, iNV_RECEIPT_LOTS_HDR.LOT_ID, iNV_RECEIPT_LOTS_HDR.RECEIPT_ID, iNV_RECEIPT_LOTS_HDR.RECEIPT_DTL_ID);
                //if (ProReturn != string.Empty)
                //{
                //    if (ProReturn != "0")
                //    {
                //        ErrorCollection.Add("RECEIPTLOTDETAILS", ProReturn);
                //        if (ErrorCollection.Count > 0)
                //        {
                //            return;
                //        }
                //    }
                //}

                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        #endregion

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RLD_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            if (Session[FINSessionConstants.GridData] != null)
            {
                dtGridData = (DataTable)Session[FINSessionConstants.GridData];
            }
            gvData.EditIndex = -1;

            BindGrid(dtGridData);

        }
        #endregion

        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RLD_ROW_CMD", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {


            System.Collections.SortedList slControls = new System.Collections.SortedList();

            //  TextBox txtAttributeType = gvr.FindControl("txtAttributeType") as TextBox;
            TextBox txtAttributeValue = gvr.FindControl("txtAttributeValue") as TextBox;
            DropDownList ddlAttributetyp = gvr.FindControl("ddlAttributetyp") as DropDownList;

            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList["pk_id"] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }

            slControls[0] = ddlAttributetyp;
            slControls[1] = txtAttributeValue;

            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/AP_" + Session["Sel_Lng"].ToString() + ".properties"));
            ErrorCollection.Clear();
            string strCtrlTypes = "DropDownList~TextBox";
            string strMessage = Prop_File_Data["Attribute_P"] + " ~ " + Prop_File_Data["Attribute_Value_P"] + "";
            //string strMessage = "Attribute~AttributeValue";

            EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

            if (EmptyErrorCollection.Count > 0)
            {
                ErrorCollection = EmptyErrorCollection;
                return drList;
            }


            string strCondition = string.Empty;

            strCondition = "CODE='" + ddlAttributetyp.SelectedValue + "'";
            strMessage = FINMessageConstatns.RecordAlreadyExists;

            ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            if (ErrorCollection.Count > 0)
            {
                return drList;
            }

            drList["CODE"] = ddlAttributetyp.SelectedValue.ToString();
            drList["CODE"] = ddlAttributetyp.SelectedItem.Text.ToString();
            //drList["ATTRIBUTE_TYPE"] = txtAttributeType.Text;
            drList["ATTRIBUTE_VALUE"] = txtAttributeValue.Text;

            drList[FINColumnConstants.DELETED] = FINAppConstants.N;


            return drList;

        }



        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RLD_ROW_UP", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RLD_ROW_DEL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RLD_ROW_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RLD_RowCreated", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RLD_RDB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                // COURSE_GROUP.COURSE_GROUP_ID = short.Parse(Master.RecordID.ToString());
                DBMethod.DeleteEntity<INV_RECEIPT_LOTS_HDR>(iNV_RECEIPT_LOTS_HDR);
                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATADELETED);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RLD_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        private void BindPODetails()
        {
            DataTable dtpodata = new DataTable();

            if (ddlLineNumber.SelectedValue != string.Empty)
            {
                dtpodata = DBMethod.ExecuteQuery(FIN.DAL.AP.ReceiptLotDetails_DAL.getpodata(ddlLineNumber.SelectedValue)).Tables[0];
                if (dtpodata.Rows.Count > 0)
                {
                    txtPONumber.Text = dtpodata.Rows[0]["PO_NUM"].ToString();
                    txtItemDescription.Text = dtpodata.Rows[0]["ITEM_DESCRIPTION"].ToString();
                    txtPOLineNumber.Text = dtpodata.Rows[0]["PO_LINE_NUM"].ToString();
                    hid_Item_Id.Value = dtpodata.Rows[0]["ITEM_ID"].ToString();
                    txtTotQuantity.Text = dtpodata.Rows[0]["qty_approved"].ToString();
                }
                txtReceivedQuantity.Text = DBMethod.GetDecimalValue(FIN.DAL.AP.ReceiptLotDetails_DAL.GetReceivedLotQty(ddlLineNumber.SelectedValue)).ToString();
            }
        }

        protected void ddlLineNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                BindPODetails();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WRI_ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void BindLineDetails()
        {
            DataTable dtgrndata = new DataTable();

            if (ddlGRNNumber.SelectedValue != string.Empty)
            {
                dtgrndata = DBMethod.ExecuteQuery(FIN.DAL.AP.ReceiptLotDetails_DAL.getgrndata(ddlGRNNumber.SelectedValue)).Tables[0];
                if (dtgrndata.Rows.Count > 0)
                {
                    txtGRNDate.Text = DateTime.Parse(dtgrndata.Rows[0]["GRN_DATE"].ToString()).ToString("dd/MM/yyyy");
                }

                PurchaseItemReceipt_BLL.fn_GetPOItemLineData(ref ddlLineNumber, ddlGRNNumber.SelectedValue.ToString());


            }
        }
        protected void ddlGRNNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                BindLineDetails();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WRI_ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {

        }



    }
}