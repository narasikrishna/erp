﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.DAL.AP;
using FIN.DAL.GL;
using FIN.BLL;
using FIN.BLL.AP;
using VMVServices.Web;
using System.Web.UI.DataVisualization.Charting;
using System.Drawing;

namespace FIN.Client.AP
{
    public partial class DashBoard_AP : PageBase
    {

        ///// <summary>
        ///// when the pages is rendered and loaded for the first time execution goes here
        ///// </summary>
        ///// <param name="sender"></param>
        ///// <param name="e"></param>
        ///// 

        //protected void Page_Load(object sender, EventArgs e)
        //{
        //    if (!IsPostBack)
        //    {
        //        ChartAP.Visible = false;
        //        FillComboBox();
        //        ddlSupplierName.SelectedValue = "VEND_ID-0000000005";
        //        txtFromDate.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
        //        txtToDate.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");

        //        this.btnGenChart.Click += new EventHandler(btnGenerateChart_Click);

        //        //generateChart();
        //    }
        //}
        //private void FillComboBox()
        //{
        //    FIN.BLL.AP.Supplier_BLL.GetSupplierName(ref ddlSupplierName);
        //}

        //protected void btnGenerateChart_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        generateChart();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //protected void generateChart()
        //{
        //    try
        //    {
        //        DataTable dtData = new DataTable();
        //        dtData = DBMethod.ExecuteQuery(FIN.DAL.AP.Invoice_DAL.getInvPaymentAmt(ddlSupplierName.SelectedValue.ToString(), txtFromDate.Text.ToString(), txtToDate.Text.ToString())).Tables[0];

        //        ChartAP.Series["APInvoiceSeries"].ChartType = SeriesChartType.Column;
        //        ChartAP.Series["APPaymentSeries"].ChartType = SeriesChartType.Column;
        //        ChartAP.Series["APInvoiceSeries"]["DrawingStyle"] = "Default";
        //        ChartAP.Series["APPaymentSeries"]["DrawingStyle"] = "Default";
        //        ChartAP.ChartAreas["ChartAreaAP"].Area3DStyle.Enable3D = false;
        //        ChartAP.Series["APInvoiceSeries"].IsValueShownAsLabel = false;
        //        ChartAP.Series["APPaymentSeries"].IsValueShownAsLabel = false;

        //        ChartAP.Series["APInvoiceSeries"].BorderWidth = 2;
        //        ChartAP.Series["APPaymentSeries"].BorderWidth = 2;
        //        //ChartAP.Series["Series1"].IsXValueIndexed = true;
        //        //ChartAP.Series["APPaymentSeries"].IsXValueIndexed = true;

        //        ChartAP.Series["APInvoiceSeries"]["StackedGroupName"] = "Group1";
        //        ChartAP.Series["APPaymentSeries"]["StackedGroupName"] = "Group1";

        //        ChartAP.DataSource = dtData;
        //        ChartAP.DataBind();

        //        ChartAP.Series["APInvoiceSeries"].XValueMember = "Vendor_Name";
        //        ChartAP.Series["APInvoiceSeries"].YValueMembers = "Invoice_Amt";

        //        ChartAP.Series["APPaymentSeries"].XValueMember = "Vendor_Name";
        //        ChartAP.Series["APPaymentSeries"].YValueMembers = "Payment_Amt";

        //        ChartAP.Series["APInvoiceSeries"].Color = System.Drawing.Color.Brown;
        //        ChartAP.Series["APInvoiceSeries"].Legend = "Default";
        //        ChartAP.Series["APInvoiceSeries"].LegendText = "Invoice Amount";

        //        ChartAP.Series["APPaymentSeries"].Color = System.Drawing.Color.BlueViolet;
        //        ChartAP.Series["APPaymentSeries"].Legend = "Default";
        //        ChartAP.Series["APPaymentSeries"].LegendText = "Payment Amount";


        //        ChartAP.Legends["Default"].BackColor = Color.Transparent;
        //        ChartAP.Legends["Default"].LegendStyle = LegendStyle.Column;
        //        ChartAP.Legends["Default"].ForeColor = Color.Black;
        //        ChartAP.Legends["Default"].Docking = Docking.Bottom;


        //        ChartAP.Legends["Default"].BorderColor = Color.Transparent;
        //        ChartAP.Legends["Default"].BackSecondaryColor = Color.Transparent;
        //        ChartAP.Legends["Default"].BackGradientStyle = GradientStyle.None;
        //        ChartAP.Legends["Default"].BorderColor = Color.Black;
        //        ChartAP.Legends["Default"].BorderWidth = 1;
        //        ChartAP.Legends["Default"].BorderDashStyle = ChartDashStyle.Solid;
        //        ChartAP.Legends["Default"].ShadowOffset = 1;

        //        //ChartAP.Series["Series1"]["PixelPointWidth"] = "50";
        //        //ChartAP.Series["Series2"]["PixelPointWidth"] = "50";


        //        ChartAP.BackColor = Color.Transparent;
        //        ChartAP.ChartAreas["ChartAreaAP"].AxisY = new System.Web.UI.DataVisualization.Charting.Axis { LabelStyle = new LabelStyle() { Font = new Font("Verdana", 8.0f, FontStyle.Regular) } };
        //        //ChartAP.ChartAreas["ChartAreaAP"].AxisY = new System.Web.UI.DataVisualization.Charting.Axis { LabelStyle = new LabelStyle() { ForeColor = Color.Blue } };

        //        ChartAP.ChartAreas["ChartAreaAP"].AxisX = new System.Web.UI.DataVisualization.Charting.Axis { LabelStyle = new LabelStyle() { Font = new Font("Verdana", 8.0f, FontStyle.Regular) } };
        //        //ChartAP.ChartAreas["ChartAreaAP"].AxisX = new System.Web.UI.DataVisualization.Charting.Axis { LabelStyle = new LabelStyle() { ForeColor = Color.Blue } };



        //        ChartAP.ChartAreas["ChartAreaAP"].Area3DStyle.Enable3D = false;
        //        ChartAP.ChartAreas["ChartAreaAP"].BackColor = Color.Transparent;
        //        ChartAP.ChartAreas["ChartAreaAP"].AxisX.MajorGrid.Enabled = false;
        //        ChartAP.ChartAreas["ChartAreaAP"].AxisY.MajorGrid.Enabled = true;
        //        ChartAP.ChartAreas["ChartAreaAP"].Position.Width = 100;
        //        ChartAP.ChartAreas["ChartAreaAP"].Position.Height = 100;


        //        ChartAP.ChartAreas["ChartAreaAP"].AxisX.IsMarginVisible = true;
        //        ChartAP.ChartAreas["ChartAreaAP"].Position.X = 6;
        //        ChartAP.ChartAreas["ChartAreaAP"].AxisX.Title = ".\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n.";


        //        ChartAP.ChartAreas["ChartAreaAP"].AxisY.IsStartedFromZero = true;
        //        ChartAP.ChartAreas["ChartAreaAP"].AxisY.Title = "Supplier Invoice/Payment";
        //        ChartAP.ChartAreas["ChartAreaAP"].AxisX.Title = "Suppliers";
        //        ChartAP.ChartAreas["ChartAreaAP"].AxisY.LabelStyle.Format = "#,##,##0.#0";// "{900:C}";"#,##0;-#,##0;0"

        //        ChartAP.ChartAreas["ChartAreaAP"].AxisY.TitleFont = new Font("Verdana", 12, FontStyle.Regular);
        //        //ChartAP.ChartAreas["ChartAreaAP"].AxisY.TitleForeColor = Color.Blue;

        //        ChartAP.ChartAreas["ChartAreaAP"].AxisX.TitleFont = new Font("Verdana", 12, FontStyle.Regular);
        //        //ChartAP.ChartAreas["ChartAreaAP"].AxisX.TitleForeColor = Color.Blue;


        //        ChartAP.ChartAreas["ChartAreaAP"].AxisX.LabelStyle.Angle = -90;


        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //    finally
        //    {
        //        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerPath + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);
        //    }
        //}

        //protected void btnSave_Click(object sender, EventArgs e)
        //{

        //}
        //protected void btnDelete_Click(object sender, EventArgs e)
        //{

        //}
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session["FIVE_SUPPLY_GRAP"] = null;
                Session["INVOICED_PAID_GRAP"] = null;
                Session["GRNData"] = null;

                FillComboBox();

                DisplayFiveSupplyDetails();
                DisplayInvoicePaidDetails();

                getAgeDetails();
                getIssuedQuantity();

                getPOAwaited();

                getGRNDataChart();

                LoadGraph();
            }
            else
            {
                LoadGraph();
            }
        }

        private void LoadGraph()
        {
            try
            {
                ErrorCollection.Clear();
                if (Session["FIVE_SUPPLY_GRAP"] != null)
                {
                    chrtTop5Supply.DataSource = (DataTable)Session["FIVE_SUPPLY_GRAP"];
                    chrtTop5Supply.Series["S_supply_Count"].XValueMember = "vendor_name";
                    chrtTop5Supply.Series["S_supply_Count"].YValueMembers = "payment_amt";
                    chrtTop5Supply.DataBind();
                }
                if (Session["INVOICED_PAID_GRAP"] != null)
                {
                    chrtInvoicedPaid.DataSource = (DataTable)Session["INVOICED_PAID_GRAP"];
                    chrtInvoicedPaid.DataBind();
                }

                if (Session["SUPPLY_AGE"] != null)
                {
                    chrtEmpAge.DataSource = (DataTable)Session["SUPPLY_AGE"];
                    chrtEmpAge.DataBind();
                }
                if (Session["POAwait"] != null)
                {
                    chrtPOAwait.DataSource = (DataTable)Session["POAwait"];
                    chrtPOAwait.DataBind();
                }
                if (Session["GRNData"] != null)
                {
                    chartGRN.DataSource = (DataTable)Session["GRNData"];
                    chartGRN.DataBind();
                }
                if (Session["IssuedQuantity"] != null)
                {
                    chartIssuedQuantity.DataSource = (DataTable)Session["IssuedQuantity"];

                    chartIssuedQuantity.Series["IssuedQuantity"].XValueMember = "item_name";
                    chartIssuedQuantity.Series["IssuedQuantity"].YValueMembers = "issued_quantity";
                    chartIssuedQuantity.DataBind();
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountCodesEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void getPOAwaited()
        {
            DataTable dt_POAwait = DBMethod.ExecuteQuery(PurchaseOrder_DAL.GetSupplyWisePoAwaitedChart()).Tables[0];
            Session["POAwait"] = dt_POAwait;
            LoadGraph();
        }
        private void getGRNDataChart()
        {
            DataTable dt_GRNData = DBMethod.ExecuteQuery(PurchaseOrder_DAL.GetGRNDataChart()).Tables[0];
            Session["GRNData"] = dt_GRNData;
        }


        private void getIssuedQuantity()
        {
            DataTable dt_IssuedQuantity = DBMethod.ExecuteQuery(PurchaseOrder_DAL.GetIssuedQuantity()).Tables[0];
            Session["IssuedQuantity"] = dt_IssuedQuantity;
        }
        private void getAgeDetails()
        {
            DataTable dt_empAge = DBMethod.ExecuteQuery(APAgingAnalysis_DAL.GetAgingAnalysisChart()).Tables[0];
            Session["SUPPLY_AGE"] = dt_empAge;
        }

        private void FillComboBox()
        {
            Supplier_BLL.GetSupplier(ref ddlSupplier);
        }
        private void DisplayFiveSupplyDetails()
        {
            DataTable dt_EmpDet = DBMethod.ExecuteQuery(Payment_DAL.GetTop5SupplierAmt()).Tables[0];
            Session["FIVE_SUPPLY_GRAP"] = dt_EmpDet;
            if (dt_EmpDet.Rows.Count > 0)
            {
                DataTable dt_Dist_DEPT = dt_EmpDet.DefaultView.ToTable(true, "vendor_id", "vendor_name", "payment_amt");

                DataTable dt_Dept_List = new DataTable();
                dt_Dept_List.Columns.Add("vendor_id");
                dt_Dept_List.Columns.Add("vendor_name");
                dt_Dept_List.Columns.Add("payment_amt");

                for (int iLoop = 0; iLoop < dt_Dist_DEPT.Rows.Count; iLoop++)
                {
                    DataRow dr = dt_Dept_List.NewRow();
                    dr["vendor_id"] = dt_Dist_DEPT.Rows[iLoop]["vendor_id"].ToString();
                    dr["vendor_name"] = dt_Dist_DEPT.Rows[iLoop]["vendor_name"].ToString();
                    DataRow[] dr_count = dt_EmpDet.Select("vendor_id='" + dt_Dist_DEPT.Rows[iLoop]["vendor_id"].ToString() + "'");
                    dr["payment_amt"] = CommonUtils.ConvertStringToDecimal(dt_Dist_DEPT.Rows[iLoop]["payment_amt"].ToString());
                    dt_Dept_List.Rows.Add(dr);
                }

                Session["FIVE_SUPPLY_GRAP"] = dt_Dept_List;

                LoadGraph();
            }
        }

        private void DisplayInvoicePaidDetails()
        {
            DataTable dt_EmpDet = DBMethod.ExecuteQuery(Payment_DAL.GetInvoicePaidAmt()).Tables[0];
            Session["INVOICED_PAID_GRAP_LOAD"] = dt_EmpDet;
            if (dt_EmpDet.Rows.Count > 0)
            {
                DataTable dt_Dist_DEPT = dt_EmpDet.DefaultView.ToTable(true, "vendor_id", "vendor_name", "invoice_amt", "payment_amt");

                DataTable dt_Dept_List = new DataTable();
                dt_Dept_List.Columns.Add("vendor_id");
                dt_Dept_List.Columns.Add("vendor_name");
                dt_Dept_List.Columns.Add("invoice_amt");
                dt_Dept_List.Columns.Add("payment_amt");

                for (int iLoop = 0; iLoop < dt_Dist_DEPT.Rows.Count; iLoop++)
                {
                    DataRow dr = dt_Dept_List.NewRow();
                    dr["vendor_id"] = dt_Dist_DEPT.Rows[iLoop]["vendor_id"].ToString();
                    dr["vendor_name"] = dt_Dist_DEPT.Rows[iLoop]["vendor_name"].ToString();
                    dr["invoice_amt"] = DBMethod.GetAmtDecimalCommaSeparationValue(CommonUtils.ConvertStringToDecimal(dt_Dist_DEPT.Rows[iLoop]["invoice_amt"].ToString()).ToString());
                    dr["payment_amt"] = DBMethod.GetAmtDecimalCommaSeparationValue(CommonUtils.ConvertStringToDecimal(dt_Dist_DEPT.Rows[iLoop]["payment_amt"].ToString()).ToString());
                    dt_Dept_List.Rows.Add(dr);
                }
                gvInvoicePaid.DataSource = dt_Dept_List;
                gvInvoicePaid.DataBind();
                Session["INVOICED_PAID_GRAP"] = dt_Dept_List;

                LoadGraph();
            }
        }
        protected void lnk_InvoicePaid_Click(object sender, EventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            LoadInvoicePaidDetails(gvr);
        }

        private void LoadInvoicePaidDetails(GridViewRow gvr)
        {
            DataTable dt_EmpDet = (DataTable)Session["INVOICED_PAID_GRAP_LOAD"];
            List<DataTable> tables = dt_EmpDet.AsEnumerable()
                .Where(r => r["vendor_id"].ToString().Contains(gvInvoicePaid.DataKeys[gvr.RowIndex].Values["vendor_id"].ToString()))
                       .GroupBy(row => new
                       {
                           email = row.Field<string>("vendor_id"),
                           Name = row.Field<string>("vendor_name")
                       }).Select(g => System.Data.DataTableExtensions.CopyToDataTable(g)).ToList();

            DataTable dt_Dist_DEPT = tables[0].DefaultView.ToTable(true, "vendor_id", "vendor_name", "invoice_amt", "payment_amt");


            DataTable dt_Dept_List = new DataTable();
            dt_Dept_List.Columns.Add("vendor_id");
            dt_Dept_List.Columns.Add("vendor_name");
            dt_Dept_List.Columns.Add("invoice_amt");
            dt_Dept_List.Columns.Add("payment_amt");

            for (int iLoop = 0; iLoop < dt_Dist_DEPT.Rows.Count; iLoop++)
            {
                DataRow dr = dt_Dept_List.NewRow();
                dr["vendor_id"] = dt_Dist_DEPT.Rows[iLoop]["vendor_id"].ToString();
                dr["vendor_name"] = dt_Dist_DEPT.Rows[iLoop]["vendor_name"].ToString();
                dr["invoice_amt"] = DBMethod.GetAmtDecimalCommaSeparationValue(CommonUtils.ConvertStringToDecimal(dt_Dist_DEPT.Rows[iLoop]["invoice_amt"].ToString()).ToString());
                dr["payment_amt"] = DBMethod.GetAmtDecimalCommaSeparationValue(CommonUtils.ConvertStringToDecimal(dt_Dist_DEPT.Rows[iLoop]["payment_amt"].ToString()).ToString());
                dt_Dept_List.Rows.Add(dr);
            }
            gvInvoicePaid.DataSource = dt_Dept_List;
            gvInvoicePaid.DataBind();

            Session["INVOICED_PAID_GRAP"] = dt_Dept_List;

            LoadGraph();

        }


        private void LoadGRNDetails()
        {
            DataTable dt_Emp_Dist_list = DBMethod.ExecuteQuery(PurchaseOrder_DAL.GetGRNDataChart(ddlSupplier.SelectedValue, ddlItem.SelectedValue)).Tables[0];
            Session["GRNData"] = dt_Emp_Dist_list;
            LoadGraph();
        }


        protected void ddlSupplier_SelectedIndexChanged(object sender, EventArgs e)
        {
            PurchaseOrder_BLL.GetItemBasedVendor(ref ddlItem, ddlSupplier.SelectedValue);

            DataTable dt_Emp_Dist_list = DBMethod.ExecuteQuery(PurchaseOrder_DAL.GetGRNDataChart(ddlSupplier.SelectedValue)).Tables[0];
            Session["GRNData"] = dt_Emp_Dist_list;

            LoadGraph();
        }

        protected void ddlItem_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadGRNDetails();
        }
    }
}