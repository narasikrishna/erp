﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.AP;
using FIN.BLL;
using FIN.BLL.AP;
using FIN.BLL.HR;
using VMVServices.Web;
using System.Data;

namespace FIN.Client.AP
{
    public partial class ItemOtherCostEntry : PageBase
    {


        INV_ITEM_COSTING INV_ITEM_COSTING = new INV_ITEM_COSTING();
        DataTable dtGridData = new DataTable();
        Boolean bol_rowVisiable;
        Boolean savedBool;
        string ProReturn = null;
        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }
            UserRightsChecking();

        }
        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }
        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>
        private void FillComboBox()
        {
            PurchaseItemReceipt_BLL.fn_getGRNNo(ref ddlGRNNumber);
        }
        private void BindLineDetails()
        {
            DataTable dtData = new DataTable();
            dtData = DBMethod.ExecuteQuery(PurchaseItemReceipt_DAL.GetPOItemLine(ddlGRNNumber.SelectedValue.ToString())).Tables[0];
            if (dtData != null)
            {
                if (dtData.Rows.Count > 0)
                {
                    txtGRNDate.Text = DateTime.Parse(dtData.Rows[0][FINColumnConstants.RECEIPT_DATE].ToString()).ToString("dd/MM/yyyy");
                    PurchaseItemReceipt_BLL.fn_GetPOItemLineData(ref ddlLineNumber, ddlGRNNumber.SelectedValue.ToString());
                }
            }
        }
        protected void ddlGRNNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                BindLineDetails();
             
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void BindPODetails()
        {
            DataTable dtData = new DataTable();
            dtData = DBMethod.ExecuteQuery(PurchaseItemReceipt_DAL.GetPOItemLineDetails(ddlGRNNumber.SelectedValue.ToString(), ddlLineNumber.SelectedValue.ToString())).Tables[0];
            if (dtData != null)
            {
                if (dtData.Rows.Count > 0)
                {
                    txtPONumber.Text = dtData.Rows[0][FINColumnConstants.PO_NUM].ToString();
                    txtPOLineNumber.Text = dtData.Rows[0][FINColumnConstants.PO_LINE_NUM].ToString();
                    txtItemDescription.Text = dtData.Rows[0][FINColumnConstants.inv_description].ToString();
                    Session["Item_Id"] = dtData.Rows[0][FINColumnConstants.ITEM_ID].ToString();


                }
            }

          
        }
        protected void ddlLineNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                BindPODetails();
                dtGridData = DBMethod.ExecuteQuery(ItemOtherCost_DAL.GetItemOtherCostDtl_frGrn_ln_no(ddlGRNNumber.SelectedValue, ddlLineNumber.SelectedValue)).Tables[0];
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>
        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();

                EntityData = null;
                Session[FINSessionConstants.GridData] = null;

                dtGridData = DBMethod.ExecuteQuery(ItemOtherCost_DAL.GetItemOtherCostDetails(Master.StrRecordId)).Tables[0];
                BindGrid(dtGridData);

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    ddlGRNNumber.Enabled = false;
                    ddlLineNumber.Enabled = false;

                    INV_ITEM_COSTING = ItemOtherCost_BLL.getClassEntity(Master.StrRecordId);

                    EntityData = INV_ITEM_COSTING;
                    ddlGRNNumber.SelectedValue = INV_ITEM_COSTING.INV_RECEIPT_HDR_ID;
                    BindLineDetails();
                    ddlLineNumber.SelectedValue = INV_ITEM_COSTING.INV_RECEIPT_LINE_ID;
                    BindPODetails();
                    if (INV_ITEM_COSTING.WORKFLOW_COMPLETION_STATUS == "1")
                    {
                        btnSave.Visible = false;
                    }

                    //txtDescription.Text = INV_ITEM_COSTING.PO_REQ_DESC;
                    //txtRequisitionNumber.Text = INV_ITEM_COSTING.PO_REQ_ID;
                    //txtRequisitionDate.Text = DBMethod.ConvertDateToString(pO_REQUISITION_HDR.PO_REQ_DT.ToString());
                    //ddlRequisitionType.SelectedValue = pO_REQUISITION_HDR.PO_REQ_TYPE.ToString();
                    //ddlDepartmentName.SelectedValue = pO_REQUISITION_HDR.DEPT_ID.ToString();
                    //ddlItemService.SelectedValue = pO_REQUISITION_HDR.ITEM_SERVICE.ToString();
                    //lblRequisitionNumber.Visible = true;
                    //txtRequisitionNumber.Visible = true;
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;

                if (dtData.Rows.Count > 0)
                {
                    dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("INV_CHARGE_AMT", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("INV_CHARGE_AMT"))));
                    dtData.AcceptChanges();
                }

                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountCodesEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddlCostType = tmpgvr.FindControl("ddlCostType") as DropDownList;
                Lookup_BLL.GetLookUpValues(ref ddlCostType, "CT");

                if (gvData.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                {
                    ddlCostType.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.LOOKUP_ID].ToString();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            if (Session[FINSessionConstants.GridData] != null)
            {
                dtGridData = (DataTable)Session[FINSessionConstants.GridData];
            }
            gvData.EditIndex = -1;

            BindGrid(dtGridData);

        }
        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {
            System.Collections.SortedList slControls = new System.Collections.SortedList();

            TextBox txtCost = gvr.FindControl("txtCost") as TextBox;

            DropDownList ddlCostType = gvr.FindControl("ddlCostType") as DropDownList;


            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList[FINColumnConstants.INV_ITEM_COST_ID] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }


            slControls[0] = ddlCostType;
            slControls[1] = txtCost;

            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/AP_" + Session["Sel_Lng"].ToString() + ".properties"));
            ErrorCollection.Clear();
            string strCtrlTypes = FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.TEXT_BOX;
            string strMessage = Prop_File_Data["Cost_Type_P"] + " ~ " + Prop_File_Data["Cost_P"] + "";
            //string strMessage = "Cost type ~ Cost";

            EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

            if (EmptyErrorCollection.Count > 0)
            {
                ErrorCollection = EmptyErrorCollection;
                return drList;
            }


            string strCondition = "LOOKUP_ID='" + ddlCostType.SelectedValue.ToString() + "'";
            strMessage = FINMessageConstatns.RecordAlreadyExists;
            ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            if (ErrorCollection.Count > 0)
            {
                return drList;
            }

            drList[FINColumnConstants.LOOKUP_ID] = ddlCostType.SelectedValue.ToString();
            drList["LOOKUP_NAME"] = ddlCostType.SelectedItem.Text.ToString();
            drList[FINColumnConstants.INV_CHARGE_AMT] = txtCost.Text;

            drList[FINColumnConstants.DELETED] = FINAppConstants.N;


            return drList;

        }


        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountCodesEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        #endregion

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();


                AssignToBE();

                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                //if (EntityData != null)
                //{
                //    INV_ITEM_COSTING = (INV_ITEM_COSTING)EntityData;
                //}

               // INV_ITEM_COSTING.INV_ITEM_ID = ddlGRNNumber.SelectedValue.ToString();

               // INV_ITEM_COSTING.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;
                

                //Save Detail Table

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                ErrorCollection = CommonUtils.IsEmptyGrid(dtGridData, "Cost ");
                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                var tmpChildEntity = new List<Tuple<object, string>>();


                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    if (dtGridData.Rows[iLoop][FINColumnConstants.INV_ITEM_COST_ID].ToString() != "0" && dtGridData.Rows[iLoop][FINColumnConstants.INV_ITEM_COST_ID].ToString() != string.Empty)
                    {
                        INV_ITEM_COSTING = ItemOtherCost_BLL.getClassEntity(dtGridData.Rows[iLoop][FINColumnConstants.INV_ITEM_COST_ID].ToString());
                    }
                    INV_ITEM_COSTING.INV_CHARGE_AMT = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop][FINColumnConstants.INV_CHARGE_AMT].ToString());
                    INV_ITEM_COSTING.INV_ITEM_COST_TYPE = (dtGridData.Rows[iLoop][FINColumnConstants.LOOKUP_ID].ToString());

                    INV_ITEM_COSTING.INV_RECEIPT_HDR_ID = (ddlGRNNumber.SelectedValue.ToString());
                    INV_ITEM_COSTING.INV_RECEIPT_LINE_ID = ddlLineNumber.SelectedValue.ToString();
                    INV_ITEM_COSTING.INV_ORG_ID = VMVServices.Web.Utils.OrganizationID;

                    if (Session["Item_Id"] != null)
                    {
                        INV_ITEM_COSTING.INV_ITEM_ID = Session["Item_Id"].ToString();
                    }
                    INV_ITEM_COSTING.ENABLED_FLAG = FINAppConstants.EnabledFlag;


                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        tmpChildEntity.Add(new Tuple<object, string>(INV_ITEM_COSTING, FINAppConstants.Delete));
                    }
                    else
                    {
                        ProReturn = FINSP.GetSPFOR_ERR_IS_CAL_PERIOD_AVIAL(INV_ITEM_COSTING.INV_ITEM_COST_ID,txtGRNDate.Text);

                        if (ProReturn != string.Empty)
                        {

                            if (ProReturn != "0")
                            {
                                ErrorCollection.Add("POREQcaL", ProReturn);
                                if (ErrorCollection.Count > 0)
                                {
                                    return;
                                }
                            }
                        }

                        if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                        {
                            if ((dtGridData.Rows[iLoop][FINColumnConstants.INV_ITEM_COST_ID].ToString()) != "0" && dtGridData.Rows[iLoop][FINColumnConstants.INV_ITEM_COST_ID].ToString() != string.Empty)
                            {
                                INV_ITEM_COSTING.INV_ITEM_COST_ID = dtGridData.Rows[iLoop][FINColumnConstants.INV_ITEM_COST_ID].ToString();
                                INV_ITEM_COSTING.MODIFIED_DATE = DateTime.Today;
                                // tmpChildEntity.Add(new Tuple<object, string>(INV_ITEM_COSTING, FINAppConstants.Update));
                                INV_ITEM_COSTING.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, INV_ITEM_COSTING.INV_ITEM_COST_ID);
                                DBMethod.SaveEntity<INV_ITEM_COSTING>(INV_ITEM_COSTING, true);
                                savedBool = true;
                               // break;
                            }
                        }
                        else
                        {
                            if ((dtGridData.Rows[iLoop][FINColumnConstants.INV_ITEM_COST_ID].ToString()) != "0" && dtGridData.Rows[iLoop][FINColumnConstants.INV_ITEM_COST_ID].ToString() != string.Empty)
                            {
                                INV_ITEM_COSTING.INV_ITEM_COST_ID = dtGridData.Rows[iLoop][FINColumnConstants.INV_ITEM_COST_ID].ToString();
                                INV_ITEM_COSTING.MODIFIED_DATE = DateTime.Today;
                                // tmpChildEntity.Add(new Tuple<object, string>(INV_ITEM_COSTING, FINAppConstants.Update));
                                INV_ITEM_COSTING.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, INV_ITEM_COSTING.INV_ITEM_COST_ID);
                                DBMethod.SaveEntity<INV_ITEM_COSTING>(INV_ITEM_COSTING, true);
                                savedBool = true;
                                //break;
                            }
                            else
                            {
                                INV_ITEM_COSTING.INV_ITEM_COST_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.AP_012_D.ToString(), false, true);
                                INV_ITEM_COSTING.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, INV_ITEM_COSTING.INV_ITEM_COST_ID);
                                INV_ITEM_COSTING.CREATED_BY = this.LoggedUserName;
                                INV_ITEM_COSTING.CREATED_DATE = DateTime.Today;
                                DBMethod.SaveEntity<INV_ITEM_COSTING>(INV_ITEM_COSTING);
                                savedBool = true;
                                //break;
                            }


                            if (INV_ITEM_COSTING.WORKFLOW_COMPLETION_STATUS == "1")
                            {
                                FINSP.GetSP_GL_Posting(INV_ITEM_COSTING.INV_ITEM_COST_ID, "AP_017");
                            }


                            //if (ProReturn != string.Empty)
                            //{
                            //    if (ProReturn != "0")
                            //    {
                            //        ErrorCollection.Add("STAFFLEAVEDTLS", ProReturn);
                            //        if (ErrorCollection.Count > 0)
                            //        {
                            //            return;
                            //        }
                            //    }
                            //}
                        }
                        //if (dtGridData.Rows[iLoop][FINColumnConstants.INV_ITEM_COST_ID].ToString() != "0" && dtGridData.Rows[iLoop][FINColumnConstants.INV_ITEM_COST_ID].ToString() != string.Empty)
                        //{
                        //    INV_ITEM_COSTING.INV_ITEM_COST_ID = dtGridData.Rows[iLoop][FINColumnConstants.INV_ITEM_COST_ID].ToString();
                        //    INV_ITEM_COSTING.MODIFIED_DATE = DateTime.Today;
                        //    // tmpChildEntity.Add(new Tuple<object, string>(INV_ITEM_COSTING, FINAppConstants.Update));
                        //    INV_ITEM_COSTING.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, INV_ITEM_COSTING.INV_ITEM_COST_ID);
                        //    DBMethod.SaveEntity<INV_ITEM_COSTING>(INV_ITEM_COSTING, true);
                        //    savedBool = true;
                        //    break;
                        //}
                        //else
                        //{
                        //    INV_ITEM_COSTING.INV_ITEM_COST_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.AP_012_D.ToString(), false, true);
                        //    INV_ITEM_COSTING.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, INV_ITEM_COSTING.INV_ITEM_COST_ID);
                        //    INV_ITEM_COSTING.CREATED_BY = this.LoggedUserName;
                        //    INV_ITEM_COSTING.CREATED_DATE = DateTime.Today;
                        //    DBMethod.SaveEntity<INV_ITEM_COSTING>(INV_ITEM_COSTING);
                        //    savedBool = true;
                        //    break;
                        //    // tmpChildEntity.Add(new Tuple<object, string>(INV_ITEM_COSTING, FINAppConstants.Add));
                        //}

                      
                    }

                    //check whether the Accounting period is available or not
                   

                    //switch (Master.Mode)
                    //{
                    //    case FINAppConstants.Add:
                    //        {
                    //            DBMethod.SaveEntity<INV_ITEM_COSTING>(INV_ITEM_COSTING);
                    //            savedBool = true;
                    //            break;
                    //        }
                    //    case FINAppConstants.Update:
                    //        {
                    //            DBMethod.SaveEntity<INV_ITEM_COSTING>(INV_ITEM_COSTING, true);
                    //            savedBool = true;
                    //            break;
                    //        }
                    //}
                }

                if (ErrorCollection.Count > 0)
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                // DBMethod.DeleteEntity<pO_REQUISITION_HDR>(pO_REQUISITION_HDR);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void txtUnitPrice_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
              //  fn_UnitPricetxtChanged();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void fn_UnitPricetxtChanged()
        {
            try
            {
                ErrorCollection.Clear();

                TextBox txtUnitPrice = new TextBox();
                TextBox txtQuantity = new TextBox();
                TextBox txtPOAmount = new TextBox();

                txtUnitPrice.ID = "txtUnitPrice";
                txtQuantity.ID = "txtQuantity";
                txtPOAmount.ID = "txtPOAmount";

                if (gvData.FooterRow != null)
                {
                    if (gvData.EditIndex < 0)
                    {
                        txtUnitPrice = (TextBox)gvData.FooterRow.FindControl("txtUnitPrice");
                        txtQuantity = (TextBox)gvData.FooterRow.FindControl("txtQuantity");
                        txtPOAmount = (TextBox)gvData.FooterRow.FindControl("txtPOAmount");
                    }
                    else
                    {
                        txtUnitPrice = (TextBox)gvData.Rows[gvData.EditIndex].FindControl("txtUnitPrice");
                        txtQuantity = (TextBox)gvData.Rows[gvData.EditIndex].FindControl("txtQuantity");
                        txtPOAmount = (TextBox)gvData.Rows[gvData.EditIndex].FindControl("txtPOAmount");
                    }
                }
                else
                {
                    txtUnitPrice = (TextBox)gvData.Controls[0].Controls[1].FindControl("txtUnitPrice");
                    txtQuantity = (TextBox)gvData.Controls[0].Controls[1].FindControl("txtQuantity");
                    txtPOAmount = (TextBox)gvData.Controls[0].Controls[1].FindControl("txtPOAmount");
                }

                if (txtUnitPrice.Text.Trim() != string.Empty && txtQuantity.Text.Trim() != string.Empty)
                {
                    if (int.Parse(txtUnitPrice.Text.ToString()) > 0 && int.Parse(txtQuantity.Text.ToString()) > 0)
                    {
                        //txtPOAmount.Text = (CommonUtils.ConvertStringToDecimal(txtUnitPrice.Text) * CommonUtils.ConvertStringToDecimal(txtQuantity.Text)).ToString();
                        //if (txtTotalAmount.Text.Trim() == string.Empty)
                        //{
                        //    txtTotalAmount.Text = "0";
                        //}
                        //if (CommonUtils.ConvertStringToDecimal(txtTotalAmount.Text) >= 0 && CommonUtils.ConvertStringToDecimal(txtPOAmount.Text) > 0)
                        //{
                        //    txtTotalAmount.Text = (CommonUtils.ConvertStringToDecimal(txtPOAmount.Text) + CommonUtils.ConvertStringToDecimal(txtTotalAmount.Text)).ToString();
                        //}
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        protected void btnDelete_Click(object sender, EventArgs e)
        {

        }

        protected void txtPOLineNumber_TextChanged(object sender, EventArgs e)
        {

        }







    }
}