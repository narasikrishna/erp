﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="WarehouseReceiptItemEntry.aspx.cs" Inherits="FIN.Client.AP.WarehouseReceiptItemEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 800px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblWarehouseNumber">
                Warehouse Number
            </div>
            <div class="divtxtBox LNOrient" style="width: 200px">
                <asp:DropDownList ID="ddlWarehouseNumber" runat="server" OnSelectedIndexChanged="ddlWarehouseNumber_SelectedIndexChanged"
                    CssClass="validate[required] RequiredField ddlStype" AutoPostBack="True" TabIndex="1">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="Div2">
                Warehouse Name
            </div>
            <div class="divtxtBox LNOrient" style="width: 575px">
                <asp:TextBox ID="txtWarehouseName" runat="server" TabIndex="2" CssClass="validate[required] RequiredField  txtBox"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblGRNNumber">
                Goods Received Note Number
            </div>
            <div class="divtxtBox LNOrient" style="width: 200px">
                <asp:DropDownList ID="ddlGRNNumber" runat="server" TabIndex="3" CssClass="validate[required] RequiredField  ddlStype"
                    AutoPostBack="True" OnSelectedIndexChanged="ddlGRNNumber_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div class="colspace LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 195px" id="lblGRNDate">
                Goods Received Note Date
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:TextBox ID="txtGRNDate" runat="server" TabIndex="4" CssClass="validate[required] RequiredField txtBox"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblLineNumber">
                Line Number
            </div>
            <div class="divtxtBox LNOrient" style="width: 200px">
                <asp:DropDownList ID="ddlLineNumber" runat="server" TabIndex="5" CssClass="validate[required] RequiredField ddlStype"
                    OnSelectedIndexChanged="ddlLineNumber_SelectedIndexChanged" AutoPostBack="True">
                </asp:DropDownList>
            </div>
            <div class="colspace LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 195px" id="lblPONumber">
                Purchase Order Number
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:TextBox ID="txtPONumber" runat="server" TabIndex="6" CssClass="validate[required] RequiredField txtBox"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="Div3">
                Item Description
            </div>
            <div class="divtxtBox LNOrient" style="width: 575px">
                <asp:TextBox ID="txtItemDescription" runat="server" TabIndex="8" CssClass="validate[required] RequiredField txtBox"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblPOLineNumber">
                Purchase Order Line Number
            </div>
            <div class="divtxtBox LNOrient" style="width: 200px">
                <asp:TextBox ID="txtPOLineNumber" runat="server" TabIndex="7" CssClass="validate[required] RequiredField txtBox"></asp:TextBox>
            </div>
            <div class="colspace LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 195px" id="lblItemDescription">
                Warehouse Quantity
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:TextBox ID="txtWHQuantity" MaxLength="10" runat="server" Enabled="false" TabIndex="9"
                    CssClass="validate[required] RequiredField txtBox_N"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
    </div>
    <div class="divClear_10">
    </div>
    <div class="divRowContainer LNOrient">
        <asp:HiddenField runat="server" ID="hdQuantity" />
        <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
            Width="400px" DataKeyNames="PK_ID,LOT_ID,DELETED" OnRowCancelingEdit="gvData_RowCancelingEdit"
            OnRowCommand="gvData_RowCommand" OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound"
            OnRowDeleting="gvData_RowDeleting" OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating"
            ShowFooter="True">
            <Columns>
                <asp:TemplateField HeaderText="Lot Number">
                    <EditItemTemplate>
                        <asp:DropDownList ID="ddlLotNo" Width="370px" TabIndex="10" runat="server" CssClass="EntryFont ddlStype"
                            AutoPostBack="True" OnSelectedIndexChanged="ddlLotNo_SelectedIndexChanged">
                        </asp:DropDownList>
                    </EditItemTemplate>
                    <FooterTemplate>
                        <asp:DropDownList ID="ddlLotNo" Width="370px" TabIndex="10" AutoPostBack="True" OnSelectedIndexChanged="ddlLotNo_SelectedIndexChanged"
                            runat="server" CssClass="EntryFont RequiredField ddlStype">
                        </asp:DropDownList>
                    </FooterTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblLotNo" Width="370px" CssClass="adminFormFieldHeading" runat="server"
                            Text='<%# Eval("LOT_NUMBER") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Quantity">
                    <EditItemTemplate>
                        <asp:TextBox ID="txtquantity" Width="170px" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                            MaxLength="10" TabIndex="11" Text='<%# Eval("LOT_QTY") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <FooterTemplate>
                        <asp:TextBox ID="txtquantity" Width="170px" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                            MaxLength="10" TabIndex="11"></asp:TextBox>
                    </FooterTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblQuantity" Width="170px" runat="server" Text='<%# Eval("LOT_QTY") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Right" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="">
                    <ItemTemplate>
                        <asp:ImageButton ID="ibtnEdit" runat="server" AlternateText="Edit" CausesValidation="false"
                            ToolTip="Edit" TabIndex="12" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                        <asp:ImageButton ID="ibtnDelete" runat="server" AlternateText="Delete" CausesValidation="false"
                            ToolTip="Delete" TabIndex="13" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:ImageButton ID="ibtnUpdate" runat="server" AlternateText="Update" CommandName="Update"
                            ToolTip="Update" TabIndex="14" Height="20px" ImageUrl="~/Images/Update.png" />
                        <asp:ImageButton ID="ibtnCancel" runat="server" AlternateText="Cancel" CausesValidation="false"
                            ToolTip="Cancel" TabIndex="15" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                    </EditItemTemplate>
                    <FooterTemplate>
                        <asp:ImageButton ID="ibtnInsert" runat="server" AlternateText="Add" CommandName="FooterInsert"
                            ToolTip="Add" TabIndex="16" ImageUrl="~/Images/Add.jpg" />
                    </FooterTemplate>
                    <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Center" />
                </asp:TemplateField>
            </Columns>
            <EmptyDataRowStyle CssClass="EmptyRowStyle" />
            <HeaderStyle CssClass="GridHeader" />
            <AlternatingRowStyle CssClass="GrdAltRow" />
        </asp:GridView>
    </div>
    <div class="divRowContainer divAction">
        <table class="SaveTable">
            <tr>
                <td>
                    <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                        TabIndex="9" />
                </td>
                <td>
                    <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="10" />
                </td>
                <td>
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="11" />
                </td>
                <td>
                    <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="12" />
                </td>
            </tr>
        </table>
    </div>
    <div id="divDelete">
        <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
            PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
        </cc2:ModalPopupExtender>
        <asp:Panel ID="pnlConfirm" runat="server">
            <div class="ConfirmForm">
                <table width="100%">
                    <tr class="ConfirmHeading" style="width: 100%">
                        <td>
                            <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                Width="60px" />
                            &nbsp;
                            <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                        </td>
                    </tr>
                </table>
            </div>
        </asp:Panel>
    </div>
    
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/AP/APChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
