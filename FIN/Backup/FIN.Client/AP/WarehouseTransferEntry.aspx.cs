﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.BLL;
using FIN.BLL.GL;
using FIN.BLL.HR;
using FIN.BLL.AP;
using VMVServices.Web;

namespace FIN.Client.AP
{
    public partial class WarehouseTransferEntry : PageBase
    {

        INV_TRANSFER_HDR iNV_TRANSFER_HDR = new INV_TRANSFER_HDR();
        INV_TRANSFER_DTL iNV_TRANSFER_DTL = new INV_TRANSFER_DTL();

        WarehouseTransfer_BLL WarehouseTransfer_BLL = new WarehouseTransfer_BLL();

        DataTable dtGridData = new DataTable();
        Boolean bol_rowVisiable;
        Boolean saveBool;
        string ProReturn = null;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WHT_PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));


            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
        }

        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>
        /// 


        private void FillComboBox()
        {
            WarehouseTransfer_BLL.getWarehouseFROMBasedReceipt(ref ddlWarehousefrom);
            WarehouseTransfer_BLL.getWarehouseFROMBasedReceipt(ref ddlWHTO);
            Employee_BLL.GetEmployeeName(ref ddlTransferredBy);
            // Employee_BLL.GetEmployeeName(ref ddlTransferredTo);
            Employee_BLL.GetEmployeeName(ref ddlApprovedBy);
            Employee_BLL.GetEmployeeName(ref ddlReceivedBy);



        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                txtWarehouseName.Enabled = false;
                txtWHname.Enabled = false;

                txtTotalQuantity.Enabled = false;

                Session[FINSessionConstants.GridData] = null;
                FillComboBox();

                EntityData = null;

                dtGridData = FIN.BLL.AP.WarehouseTransfer_BLL.getChildEntityDet(Master.StrRecordId);


                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    using (IRepository<INV_TRANSFER_HDR> userCtx = new DataRepository<INV_TRANSFER_HDR>())
                    {
                        iNV_TRANSFER_HDR = userCtx.Find(r =>
                            (r.INV_TRANS_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = iNV_TRANSFER_HDR;

                    ddlWarehousefrom.SelectedItem.Text = iNV_TRANSFER_HDR.INV_FROM_WH_ID.ToString();
                    //  WarehouseReceiptItem_BLL.fn_getReceipt4Wh(ref ddlReceiptNumber, ddlWarehousefrom.SelectedItem.Text.ToString());
                    ddlWHTO.SelectedItem.Text = iNV_TRANSFER_HDR.INV_TO_WH_ID.ToString();

                    INV_WAREHOUSES INV_WAREHOUSES = new INV_WAREHOUSES();
                    using (IRepository<INV_WAREHOUSES> userCtx = new DataRepository<INV_WAREHOUSES>())
                    {
                        INV_WAREHOUSES = userCtx.Find(r =>
                            (r.INV_WH_ID == (iNV_TRANSFER_HDR.INV_FROM_WH_ID.ToString()))
                            ).SingleOrDefault();
                    }

                    if (INV_WAREHOUSES != null)
                    {
                        ddlWarehousefrom.SelectedValue = INV_WAREHOUSES.INV_WH_NAME;
                        txtWarehouseName.Text = ddlWarehousefrom.SelectedValue;
                    }

                    INV_WAREHOUSES INV_WAREHOUSES1 = new INV_WAREHOUSES();
                    using (IRepository<INV_WAREHOUSES> userCtx = new DataRepository<INV_WAREHOUSES>())
                    {
                        INV_WAREHOUSES1 = userCtx.Find(r =>
                            (r.INV_WH_ID == (iNV_TRANSFER_HDR.INV_TO_WH_ID.ToString()))
                            ).SingleOrDefault();
                    }

                    if (INV_WAREHOUSES1 != null)
                    {
                        ddlWHTO.SelectedValue = INV_WAREHOUSES1.INV_WH_NAME;
                        //ddlWHTO.Text = ddlWHTO.SelectedValue;
                        txtWHname.Text = ddlWHTO.SelectedValue;
                    }

                    if (iNV_TRANSFER_HDR.INV_TRANS_DATE != null)
                    {
                        txtTransferDate.Text = DBMethod.ConvertDateToString(iNV_TRANSFER_HDR.INV_TRANS_DATE.ToString());
                    }
                    //if (iNV_TRANSFER_HDR.TRANSFERED_TO != null)
                    //{
                    //    ddlTransferredTo.SelectedValue = iNV_TRANSFER_HDR.TRANSFERED_TO;
                    //}
                    if (iNV_TRANSFER_HDR.TRANSFERED_BY != null)
                    {
                        ddlTransferredBy.SelectedValue = iNV_TRANSFER_HDR.TRANSFERED_BY;
                    }
                    if (iNV_TRANSFER_HDR.APPROVED_BY != null)
                    {
                        ddlApprovedBy.SelectedValue = iNV_TRANSFER_HDR.APPROVED_BY;
                    }
                    if (iNV_TRANSFER_HDR.RECEIVE_APPROVED_BY != null)
                    {
                        ddlReceivedBy.SelectedValue = iNV_TRANSFER_HDR.RECEIVE_APPROVED_BY;
                    }

                    if (iNV_TRANSFER_HDR.TRANSFER_NUMBER != null)
                    {
                        txtTransferNumber.Text = iNV_TRANSFER_HDR.TRANSFER_NUMBER;
                    }

                    txtReason.Text = iNV_TRANSFER_HDR.INV_TRANS_REASON.ToString();
                    txtTotalQuantity.Text = iNV_TRANSFER_HDR.INV_ITEM_QTY.ToString();



                    //  WarehouseReceiptItem_BLL.fn_getItemForRecipt(ref ddlItemno, "0");

                    //INV_ITEM_MASTER INV_ITEM_MASTER = new INV_ITEM_MASTER();
                    //using (IRepository<INV_ITEM_MASTER> userCtx = new DataRepository<INV_ITEM_MASTER>())
                    //{
                    //    INV_ITEM_MASTER = userCtx.Find(r =>
                    //        (r.ITEM_ID == (iNV_TRANSFER_HDR.INV_ITEM_ID.ToString()))
                    //        ).SingleOrDefault();
                    //}

                    //if (INV_ITEM_MASTER != null)
                    //{
                    //    if (iNV_TRANSFER_HDR.INV_ITEM_ID != null)
                    //    {
                    //        ddlItemno.SelectedItem.Text = iNV_TRANSFER_HDR.INV_ITEM_ID.ToString();
                    //    }
                    //    txtitemname.Text = INV_ITEM_MASTER.ITEM_NAME;
                    //    //txtitemname.Text = ddlItemno.SelectedValue;// INV_ITEM_MASTER.ITEM_CODE.ToString();
                    //    //ddlItemno.SelectedValue = INV_ITEM_MASTER.ITEM_CODE.ToString();
                    //}



                }

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WHT_ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    iNV_TRANSFER_HDR = (INV_TRANSFER_HDR)EntityData;
                }

                iNV_TRANSFER_HDR.INV_FROM_WH_ID = ddlWarehousefrom.SelectedItem.Text;
                iNV_TRANSFER_HDR.INV_TO_WH_ID = ddlWHTO.SelectedItem.Text;
                // iNV_TRANSFER_HDR.INV_ITEM_ID = ddlItemno.SelectedValue;
                iNV_TRANSFER_HDR.INV_TRANS_REASON = txtReason.Text;
                iNV_TRANSFER_HDR.INV_ITEM_QTY = CommonUtils.ConvertStringToDecimal(txtTotalQuantity.Text);

                iNV_TRANSFER_HDR.ENABLED_FLAG = FINAppConstants.Y;
                //  iNV_TRANSFER_HDR.WORKFLOW_COMPLETION_STATUS = FINAppConstants.Y;


                iNV_TRANSFER_HDR.APPROVED_BY = ddlApprovedBy.SelectedValue;
                iNV_TRANSFER_HDR.RECEIVE_APPROVED_BY = ddlReceivedBy.SelectedValue;
                iNV_TRANSFER_HDR.TRANSFERED_BY = ddlTransferredBy.SelectedValue;
                //iNV_TRANSFER_HDR.TRANSFERED_TO = ddlTransferredTo.SelectedValue;
                iNV_TRANSFER_HDR.TRANSFER_NUMBER = txtTransferNumber.Text;

                if (txtTransferDate.Text != string.Empty)
                {
                    iNV_TRANSFER_HDR.INV_TRANS_DATE = DBMethod.ConvertStringToDate(txtTransferDate.Text.ToString());
                }

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    iNV_TRANSFER_HDR.MODIFIED_BY = this.LoggedUserName;
                    iNV_TRANSFER_HDR.MODIFIED_DATE = DateTime.Today;
                }
                else
                {
                    iNV_TRANSFER_HDR.INV_TRANS_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.AP_018_M.ToString(), false, true);
                    iNV_TRANSFER_HDR.PK_ID = DBMethod.GetPrimaryKeyValue(FINSequenceConstant.iNV_TRANSFER_HDR_SEQ);
                    iNV_TRANSFER_HDR.CREATED_BY = this.LoggedUserName;
                    iNV_TRANSFER_HDR.CREATED_DATE = DateTime.Today;
                }
                iNV_TRANSFER_HDR.INV_ORG_ID = VMVServices.Web.Utils.OrganizationID;
                iNV_TRANSFER_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, iNV_TRANSFER_HDR.INV_TRANS_ID);

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                var tmpChildEntity = new List<Tuple<object, string>>();

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    iNV_TRANSFER_DTL = new INV_TRANSFER_DTL();
                    if (dtGridData.Rows[iLoop]["INV_TRANS_DTL_ID"].ToString() != "0" && dtGridData.Rows[iLoop]["INV_TRANS_DTL_ID"].ToString() != string.Empty)
                    {
                        using (IRepository<INV_TRANSFER_DTL> userCtx = new DataRepository<INV_TRANSFER_DTL>())
                        {
                            iNV_TRANSFER_DTL = userCtx.Find(r =>
                                (r.INV_TRANS_DTL_ID == dtGridData.Rows[iLoop]["INV_TRANS_DTL_ID"].ToString())
                                ).SingleOrDefault();
                        }
                    }

                    iNV_TRANSFER_DTL.INV_ITEM_ID = dtGridData.Rows[iLoop]["ITEM_ID"].ToString();
                    iNV_TRANSFER_DTL.INV_LOT_ID = dtGridData.Rows[iLoop]["LOT_ID"].ToString();
                    iNV_TRANSFER_DTL.INV_ITEM_QTY = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop]["INV_ITEM_QTY"].ToString());
                    iNV_TRANSFER_DTL.ACCT_CODE_ID = dtGridData.Rows[iLoop]["code_id"].ToString();

                    iNV_TRANSFER_DTL.INV_TRANS_ID = iNV_TRANSFER_HDR.INV_TRANS_ID;
                    iNV_TRANSFER_DTL.CHILD_ID = iNV_TRANSFER_HDR.PK_ID;
                    iNV_TRANSFER_DTL.WORKFLOW_COMPLETION_STATUS = "1";

                    iNV_TRANSFER_DTL.ENABLED_FLAG = "1";

                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        tmpChildEntity.Add(new Tuple<object, string>(iNV_TRANSFER_DTL, "D"));
                    }
                    else
                    {
                        if (dtGridData.Rows[iLoop]["INV_TRANS_DTL_ID"].ToString() != "0" && dtGridData.Rows[iLoop]["INV_TRANS_DTL_ID"].ToString() != string.Empty)
                        {
                            iNV_TRANSFER_DTL.MODIFIED_BY = this.LoggedUserName;
                            iNV_TRANSFER_DTL.MODIFIED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(iNV_TRANSFER_DTL, "U"));
                        }
                        else
                        {

                            iNV_TRANSFER_DTL.INV_TRANS_DTL_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.AP_018_D.ToString(), false, true);
                            iNV_TRANSFER_DTL.CREATED_BY = this.LoggedUserName;
                            iNV_TRANSFER_DTL.CREATED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(iNV_TRANSFER_DTL, "A"));
                        }
                    }

                }
                //ProReturn = FINSP.GetSPFOR_ERR_MGR_CATEGORY_THREE(Master.FormCode, iNV_TRANSFER_HDR.INV_TRANS_ID, iNV_TRANSFER_HDR.INV_TRANS_FROM_ACC, iNV_TRANSFER_HDR.INV_TRANS_TO_ACC, iNV_TRANSFER_HDR.INV_ITEM_ID);
                //if (ProReturn != string.Empty)
                //{
                //    if (ProReturn != "0")
                //    {
                //        ErrorCollection.Add("WAREHOUSETRANSFER", ProReturn);
                //        if (ErrorCollection.Count > 0)
                //        {
                //            return;
                //        }
                //    }
                //}

                //   FINSP.GetSPFOR_ItemTransfer(txtTransferDate.Text.ToString(), iNV_TRANSFER_HDR.INV_FROM_WH_ID, iNV_TRANSFER_HDR.INV_TO_WH_ID, iNV_TRANSFER_HDR.INV_ITEM_QTY, dtGridData);

                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            FIN.BLL.GL.AccountCodes_BLL.SavePCEntity<INV_TRANSFER_HDR, INV_TRANSFER_DTL>(iNV_TRANSFER_HDR, tmpChildEntity, iNV_TRANSFER_DTL);
                            saveBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {

                            FIN.BLL.GL.AccountCodes_BLL.SavePCEntity<INV_TRANSFER_HDR, INV_TRANSFER_DTL>(iNV_TRANSFER_HDR, tmpChildEntity, iNV_TRANSFER_DTL, true);
                            saveBool = true;
                            break;

                        }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WHT_ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddlItemno = tmpgvr.FindControl("ddlItemno") as DropDownList;
                DropDownList ddlAcccode = tmpgvr.FindControl("ddlAcccode") as DropDownList;
                DropDownList ddlLotNo = tmpgvr.FindControl("ddlLotNo") as DropDownList;
                WarehouseReceiptItem_BLL.fn_getItemForRecipt(ref ddlItemno);
                AccountCodes_BLL.fn_getAccount(ref ddlAcccode);

                if (gvData.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                {
                    ddlItemno.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.ITEM_ID].ToString();
                    fillLot(tmpgvr);
                    ddlLotNo.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.LOT_ID].ToString();
                    ddlAcccode.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.CODE_ID].ToString();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WHT_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();


                ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session[FINSessionConstants.GridData], "Warehouse Transfer ");

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                AssignToBE();


                if (saveBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        #endregion

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);

                txtTotalQuantity.Text = CommonUtils.CalculateTotalAmount(dtData, "INV_ITEM_QTY");
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WHT_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            if (Session[FINSessionConstants.GridData] != null)
            {
                dtGridData = (DataTable)Session[FINSessionConstants.GridData];
            }
            gvData.EditIndex = -1;

            BindGrid(dtGridData);

        }
        #endregion

        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WHT_ROW_CMD", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {


            System.Collections.SortedList slControls = new System.Collections.SortedList();
            DropDownList ddlLotNo = gvr.FindControl("ddlLotNo") as DropDownList;
            DropDownList ddlItemno = gvr.FindControl("ddlItemno") as DropDownList;
            DropDownList ddlAcccode = gvr.FindControl("ddlAcccode") as DropDownList;
            TextBox txtquantity = gvr.FindControl("txtquantity") as TextBox;

            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList["INV_TRANS_DTL_ID"] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }


            slControls[0] = ddlItemno;
            slControls[1] = ddlLotNo;
            slControls[2] = ddlAcccode;
            ErrorCollection.Clear();
            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/AP_" + Session["Sel_Lng"].ToString() + ".properties"));
            string strCtrlTypes = "DropDownList ~ DropDownList";
            string strMessage = Prop_File_Data["Item_Number_P"] + " ~ " + Prop_File_Data["Lot_Number_P"] + " ~ " + Prop_File_Data["Account_Number_P"] + "";
            //string strMessage = "Item Number ~ Lot Number";

            EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

            if (EmptyErrorCollection.Count > 0)
            {
                ErrorCollection = EmptyErrorCollection;
                return drList;
            }


            string strCondition = "LOT_ID='" + ddlLotNo.SelectedValue + "'";
            strMessage = FINMessageConstatns.RecordAlreadyExists;
            ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            if (ErrorCollection.Count > 0)
            {
                return drList;
            }


            DataTable dtqty = new DataTable();
            dtqty = DBMethod.ExecuteQuery(FIN.DAL.AP.WarehouseTransfer_DAL.getQuatity(ddlLotNo.SelectedValue)).Tables[0];
            if (dtqty.Rows.Count > 0)
            {
                if (int.Parse(txtquantity.Text.ToString()) > int.Parse(dtqty.Rows[0]["LOT_QTY"].ToString()))
                {
                    ErrorCollection.Add("validqty", "Quantity should not exceed lot quantity.");
                    return drList;
                }
            }



            if (txtTotalQuantity.Text.Trim() == string.Empty)
            {
                txtTotalQuantity.Text = "0";
            }

            if (ddlItemno.SelectedItem != null)
            {
                drList[FINColumnConstants.ITEM_ID] = ddlItemno.SelectedItem.Value;
                drList[FINColumnConstants.ITEM_NAME] = ddlItemno.SelectedItem.Text;
            }

            if (ddlAcccode.SelectedItem != null)
            {
                drList[FINColumnConstants.CODE_ID] = ddlAcccode.SelectedItem.Value;
                drList[FINColumnConstants.CODE_NAME] = ddlAcccode.SelectedItem.Text;
            }

            if (ddlLotNo.SelectedItem != null)
            {
                drList[FINColumnConstants.LOT_ID] = ddlLotNo.SelectedItem.Value;
                drList["LOT_NUMBER"] = ddlLotNo.SelectedItem.Text;
            }

            drList["INV_ITEM_QTY"] = txtquantity.Text.ToString();



            drList[FINColumnConstants.DELETED] = FINAppConstants.N;


            return drList;

        }



        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WHT_ROW_UP", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WHT_ROW_DEL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WHT_ROW_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WHT_RowCreated", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WHT_RDB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                // COURSE_GROUP.COURSE_GROUP_ID = short.Parse(Master.RecordID.ToString());
                DBMethod.DeleteEntity<INV_TRANSFER_HDR>(iNV_TRANSFER_HDR);
                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATADELETED);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WHT_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }






        protected void ddlLotNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;

            DropDownList ddlLotNo = gvr.FindControl("ddlLotNo") as DropDownList;
            TextBox txtquantity = gvr.FindControl("txtquantity") as TextBox;
            DataTable dtqty = new DataTable();
            dtqty = DBMethod.ExecuteQuery(FIN.DAL.AP.WarehouseTransfer_DAL.getQuatity(ddlLotNo.SelectedValue)).Tables[0];
            if (dtqty.Rows.Count > 0)
            {
                txtquantity.Text = dtqty.Rows[0]["LOT_QTY"].ToString();
            }

            //if (txtTotalQuantity.Text.Trim() == string.Empty)
            //{
            //    txtTotalQuantity.Text = "0";
            //}
            //if (CommonUtils.ConvertStringToDecimal(txtTotalQuantity.Text) >= 0 && CommonUtils.ConvertStringToDecimal(txtquantity.Text) >= 0)
            //{
            //    txtTotalQuantity.Text = (CommonUtils.ConvertStringToDecimal(txtquantity.Text) + CommonUtils.ConvertStringToDecimal(txtTotalQuantity.Text)).ToString();
            //}


        }




        protected void ddlWarehousefrom_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtWarehouseName.Text = ddlWarehousefrom.SelectedValue;
            // WarehouseReceiptItem_BLL.fn_getReceipt4Wh(ref ddlReceiptNumber, ddlWarehousefrom.SelectedItem.Text.ToString());
        }

        protected void ddlWHTO_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtWHname.Text = ddlWHTO.SelectedValue;
        }
        private void BindItemDetails()
        {
            //DataTable dtitemname = new DataTable();
            //dtitemname = DBMethod.ExecuteQuery(FIN.DAL.AP.WarehouseTransfer_DAL.getItemname(ddlItemno.SelectedItem.Text)).Tables[0];
            //if (dtitemname != null)
            //{
            //    if (dtitemname.Rows.Count > 0)
            //    {
            //        txtitemname.Text = dtitemname.Rows[0]["item_name"].ToString();
            //    }
            //}
        }
        protected void ddlItemno_SelectedIndexChanged(object sender, EventArgs e)
        {
            //BindItemDetails();

            //dtGridData = FIN.BLL.AP.WarehouseTransfer_BLL.getChildEntityDet(Master.StrRecordId)
            //BindGrid(dtGridData);
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            fillLot(gvr);

        }

        private void fillLot(GridViewRow gvr)
        {
            DropDownList ddlLotNo = gvr.FindControl("ddlLotNo") as DropDownList;
            DropDownList ddlItemno = gvr.FindControl("ddlItemno") as DropDownList;

            //   WarehouseTransfer_BLL.fn_getLotNo(ref ddlLotNo);

            if (ddlItemno.SelectedValue != null && ddlItemno.SelectedItem != null)
            {
                WarehouseReceiptItem_BLL.fn_getLot4WHItemRecID(ref ddlLotNo, ddlWarehousefrom.SelectedItem.Text.ToString(), ddlItemno.SelectedValue.ToString());
            }
            else
            {
                WarehouseReceiptItem_BLL.fn_getLot4WHItemRecID(ref ddlLotNo, ddlWarehousefrom.SelectedItem.Text.ToString());
            }
        }


        protected void ddlReceiptNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            // WarehouseReceiptItem_BLL.fn_getItemForRecipt(ref ddlItemno, ddlReceiptNumber.SelectedValue.ToString());
        }

        protected void chkasset_CheckedChanged(object sender, EventArgs e)
        {

            if (chkasset.Checked == true)
            {
                WHTO.Visible = false;
                gvData.Columns[3].Visible = true;
            }
            else
            {
                WHTO.Visible = true;
                gvData.Columns[3].Visible = false;
            }
        }


    }
}