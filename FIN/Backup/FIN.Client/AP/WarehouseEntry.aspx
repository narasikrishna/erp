﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="WarehouseEntry.aspx.cs" Inherits="FIN.Client.AP.WarehouseEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 800px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblWarehouseNumber">
                Warehouse Number
            </div>
            <div class="divtxtBox LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtWarehouseNumber" MaxLength="50" CssClass=" txtBox" Enabled="false"
                    runat="server" OnTextChanged="txtWarehouseNumber_TextChanged" TabIndex="1"></asp:TextBox>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblWarehouseName">
                Warehouse Name
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:TextBox ID="txtWarehouseName" CssClass="validate[required] RequiredField txtBox_en"
                    MaxLength="200" runat="server" TabIndex="2" OnTextChanged="txtWarehouseName_TextChanged"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style="width: 200px" id="Div2">
                Warehouse Name (Arabic)
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:TextBox ID="txtWarehouseNameOL" CssClass="txtBox_ol" MaxLength="240" runat="server"
                    TabIndex="3"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style="width: 200px" id="lblWarehouseType">
                Warehouse Type
            </div>
            <div class="divtxtBox  LNOrient" style="width: 155px">
                <asp:DropDownList ID="ddlWarehouseType" runat="server" TabIndex="4" CssClass="validate[required]  RequiredField ddlStype"
                    OnSelectedIndexChanged="ddlWarehouseType_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient">
                &nbsp</div>
            <div class="lblBox  LNOrient" style=" width: 195px" id="lblWarehouseDescription">
                Warehouse Description
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:TextBox ID="txtWarehouseDescription" CssClass="validate txtBox" MaxLength="500"
                    TextMode="MultiLine" runat="server" TabIndex="5"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblWarehouseContact">
                Warehouse Contact Name
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtWarehouseContact" MaxLength="50" CssClass="validate[required] RequiredField txtBox_en"
                    runat="server" TabIndex="6"></asp:TextBox>
                   
            </div>
            <div class="colspace" style="float: left;">
                &nbsp</div>
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblWarehouseStatus">
                Warehouse Status
            </div>
            <div class="divtxtBox  LNOrient" style="width: 155px">
                <asp:DropDownList ID="ddlWarehouseStatus" runat="server" TabIndex="7" CssClass="validate[required]  RequiredField ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblAddress1">
                Address 1
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:TextBox ID="txtAddress1" MaxLength="200" CssClass="validate[required]  RequiredField txtBox" TextMode="MultiLine" Height="50px"
                    runat="server" TabIndex="8"></asp:TextBox>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblCountry">
                Country
            </div>
            <div class="divtxtBox  LNOrient" style="width: 155px">
                <asp:DropDownList ID="ddlCountry" AutoPostBack="True" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged"
                    runat="server" TabIndex="9" CssClass="validate[required]   RequiredField ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style="width: 200px" id="lblAddress2">
                Address 2
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtAddress2" MaxLength="200" CssClass="validate txtBox" runat="server" TextMode="MultiLine" Height="50px"
                    TabIndex="10"></asp:TextBox>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblState">
                State
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 155px">
                <asp:DropDownList ID="ddlState" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlState_SelectedIndexChanged"
                    TabIndex="11" CssClass="validate[required]   RequiredField ddlStype" MaxLength="200">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblAddress3">
                Address 3
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:TextBox ID="txtAddress3" MaxLength="200" CssClass="validate txtBox" runat="server" TextMode="MultiLine" Height="50px"
                    TabIndex="12"></asp:TextBox>
            </div>
            <div class="colspace  LNOrient">
                &nbsp</div>
            <div class="lblBox  LNOrient" style="width: 200px" id="lblCity">
                City
            </div>
            <div class="divtxtBox  LNOrient" style="width: 155px">
                <asp:DropDownList ID="ddlCity" runat="server" TabIndex="13" CssClass="validate[required]   RequiredField ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style="width: 200px" id="lblPostalCode">
                Postal Code
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:TextBox ID="txtPostalCode" MaxLength="20" CssClass="validate[required] RequiredField txtBox"
                    runat="server" TabIndex="14"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars=""
                    FilterType="Numbers,Custom" TargetControlID="txtPostalCode" />
            </div>
            <div class="colspace  LNOrient">
                &nbsp</div>
            <div class="lblBox  LNOrient" style="width: 200px" id="lblPhone">
                Phone
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtPhone" CssClass="validate[required] RequiredField  txtBox"
                    runat="server" MaxLength="20" TabIndex="15"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars="+,-"
                    FilterType="Numbers,Custom" TargetControlID="txtPhone" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style="width: 200px" id="lblActive">
                Active
            </div>
            <div class="divChkbox  LNOrient" style="width: 150px">
                <asp:CheckBox ID="chkActive" runat="server" TabIndex="16" Checked="true" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                            TabIndex="17" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="18" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="19" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="20" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table>
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/AP/APChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">


        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });
    </script>
</asp:Content>
