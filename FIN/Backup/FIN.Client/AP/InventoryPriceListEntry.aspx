﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="InventoryPriceListEntry.aspx.cs" Inherits="FIN.Client.AP.InventoryPriceListEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 800px" id="divMainContainer">
        <table width="100%">
            <tr>
                <td>
                    <asp:Panel runat="server" ID="pnltdHeader" Width="800px">
                        <div class="divRowContainer">
                            <div class="lblBox LNOrient" style="width: 200px" id="lblPriceListName">
                                Price List Name
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 548px">
                                <asp:TextBox ID="txtPriceListName" runat="server" MaxLength="100" TabIndex="1" CssClass="validate[required] RequiredField txtBox"></asp:TextBox>
                            </div>
                        </div>
                        <div class="divClear_10">
                        </div>
                        <div class="divRowContainer">
                            <div class="lblBox  LNOrient" style=" width: 200px" id="lblPriceListDescription">
                                Price List Description
                            </div>
                            <div class="divtxtBox  LNOrient" style="width: 548px">
                                <asp:TextBox ID="txtPriceListDescription" Height="30px" runat="server" TabIndex="2"
                                    MaxLength="500" TextMode="MultiLine" CssClass="txtBox"></asp:TextBox>
                            </div>
                        </div>
                        <div class="divClear_10">
                        </div>
                        <div class="divRowContainer">
                            <div class="lblBox  LNOrient" style="width: 200px" id="Div1">
                                Supplier Name
                            </div>
                            <div class="divtxtBox  LNOrient" style="width: 544px">
                                <asp:DropDownList ID="ddlsupplier" TabIndex="3" CssClass="validate[required] RequiredField ddlStype"
                                    runat="server">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="divClear_10">
                        </div>
                        <div class="divRowContainer">
                            <div class="lblBox  LNOrient" style="width: 200px" id="lblpriceListCurrency">
                                Price List Currency
                            </div>
                            <div class="divtxtBox LNOrient" style=" width: 160px">
                                <asp:DropDownList ID="ddlPriceListCurrency" TabIndex="4" CssClass="validate[required] RequiredField ddlStype"
                                    runat="server">
                                </asp:DropDownList>
                            </div>
                            <div class="colspace  LNOrient" >
                                &nbsp</div>
                            <div class="lblBox LNOrient" style=" width: 197px" id="lblPricelistType">
                                Price List Type
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 167px">
                                <asp:DropDownList ID="ddlPricelistType" TabIndex="5" CssClass="validate[required] RequiredField ddlStype"
                                    runat="server">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="divClear_10">
                        </div>
                        <div class="divRowContainer">
                            <div class="lblBox LNOrient" style="width: 200px" id="lblPaymentTerms">
                                Payment Terms
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 160px">
                                <asp:TextBox ID="txtPaymentTerms" runat="server" TabIndex="6" MaxLength="50" CssClass="txtBox"></asp:TextBox>
                            </div>
                            <div class="colspace LNOrient" >
                                &nbsp</div>
                            <div class="lblBox LNOrient" style="width: 197px" id="lblFrieghtTerms">
                                Freight Terms
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 162px">
                                <asp:TextBox ID="txtFrieghtTerms" runat="server" TabIndex="7" MaxLength="50" CssClass="txtBox"></asp:TextBox>
                            </div>
                        </div>
                        <div class="divClear_10">
                        </div>
                        <div class="divRowContainer">
                            <div class="lblBox LNOrient" style="width: 200px" id="lblFromDate">
                                Effective From Date
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 160px">
                                <asp:TextBox ID="txtFromDate" CssClass="validate[required, custom[ReqDateDDMMYYY],dateRange[dg1]]  RequiredField  txtBox"
                                    runat="server" TabIndex="8"></asp:TextBox>
                                <cc2:CalendarExtender ID="CalendarExtender8" runat="server" Format="dd/MM/yyyy" TargetControlID="txtFromDate"
                                    OnClientDateSelectionChanged="checkDate">
                                </cc2:CalendarExtender>
                                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars="/"
                                    FilterType="Numbers,Custom" TargetControlID="txtFromDate" />
                            </div>
                            <div class="colspace LNOrient" >
                                &nbsp</div>
                            <div class="lblBox LNOrient" style="width: 197px" id="lblToDate">
                                To Date
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 162px">
                                <asp:TextBox ID="txtToDate" CssClass="validate[, custom[ReqDateDDMMYYY],dateRange[dg1]]    txtBox"
                                    runat="server" TabIndex="9"></asp:TextBox>
                                <cc2:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MM/yyyy" TargetControlID="txtToDate"
                                    OnClientDateSelectionChanged="checkDate">
                                </cc2:CalendarExtender>
                                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars="/"
                                    FilterType="Numbers,Custom" TargetControlID="txtToDate" />
                            </div>
                        </div>
                        <div class="divClear_10">
                        </div>
                        <div class="divRowContainer">
                            <div class="lblBox LNOrient" style="width: 150px" id="Div4">
                                Active
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 150px">
                                <asp:CheckBox ID="chkActive" runat="server" Checked="true" TabIndex="10" />
                            </div>
                        </div>
                        <div class="divClear_10">
                        </div>
                    </asp:Panel>
                </td>
                <td valign="top">
                    <table>
                        <tr>
                            <td>
                                <asp:ImageButton ID="btnPrint" runat="server" ImageUrl="../Images/Print.png" OnClick="btnPrint_Click"
                                    Style="border: 0px" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <div class="divRowContainer  LNOrient" style="width: 750px;">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                DataKeyNames="ITEM_ID,DELETED" OnRowCancelingEdit="gvData_RowCancelingEdit" OnRowCommand="gvData_RowCommand"
                OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound" OnRowDeleting="gvData_RowDeleting"
                OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating" ShowFooter="True">
                <Columns>
                    <asp:TemplateField HeaderText="Item Code">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlItemCode" TabIndex="11" runat="server" CssClass="EntryFont RequiredField ddlStype"
                                Width="250px" AutoPostBack="True" OnSelectedIndexChanged="ddlItemCode_SelectedIndexChanged">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlItemCode" TabIndex="11" runat="server" CssClass="EntryFont RequiredField ddlStype"
                                Width="250px" AutoPostBack="True" OnSelectedIndexChanged="ddlItemCode_SelectedIndexChanged">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label Width="125px" ID="lblType" CssClass="adminFormFieldHeading" runat="server"
                                Text='<%# Eval("ITEM_CODE") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Item Name">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtItemName" Width="180px" Enabled="false" MaxLength="50" runat="server"
                                CssClass="RequiredField   txtBox" Text='<%# Eval("ITEM_NAME") %>' TabIndex="10"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtItemName" Width="180px" Enabled="false" MaxLength="50" runat="server"
                                CssClass="RequiredField   txtBox" TabIndex="10"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblGroupName2" CssClass="adminFormFieldHeading" runat="server" Text='<%# Eval("ITEM_NAME") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Item UOM">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtItemUOM" Width="180px" Enabled="false" MaxLength="50" runat="server"
                                CssClass="RequiredField   txtBox" Text='<%# Eval("uom_desc") %>' TabIndex="11"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtItemUOM" Width="180px" Enabled="false" MaxLength="50" runat="server"
                                CssClass="RequiredField   txtBox" TabIndex="11"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblGroupName1" CssClass="adminFormFieldHeading" runat="server" Text='<%# Eval("uom_desc") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Item Cost">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtItemCost" Width="180px" MaxLength="50" runat="server" CssClass="RequiredField   txtBox_N"
                                OnTextChanged="txtItemCost_TextChanged" AutoPostBack="true" Text='<%# Eval("PRICE_ITEM_COST") %>'
                                TabIndex="12"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="Filteretener28" runat="server" FilterType="Numbers,Custom"
                                ValidChars=".," TargetControlID="txtItemCost" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtItemCost" Width="180px" MaxLength="50" runat="server" CssClass="RequiredField   txtBox_N"
                                OnTextChanged="txtItemCost_TextChanged" AutoPostBack="true" TabIndex="12"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="Filtdderetener28" runat="server" FilterType="Numbers,Custom"
                                ValidChars=".," TargetControlID="txtItemCost" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblItemCost" CssClass="adminFormFieldHeading" runat="server" Text='<%# Eval("PRICE_ITEM_COST") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Active">
                        <EditItemTemplate>
                            <asp:CheckBox ID="chkGridActive" runat="server" Checked='<%# Convert.ToBoolean(Eval("ENABLED_FLAG")) %>'
                                TabIndex="13" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:CheckBox ID="chkGridActive" runat="server" Checked="True" TabIndex="13" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkGridActive" runat="server" Checked='<%# Convert.ToBoolean(Eval("ENABLED_FLAG")) %>'
                                TabIndex="13" />
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" runat="server" AlternateText="Edit" CausesValidation="false"
                                TabIndex="15" ToolTip="Edit" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                            <asp:ImageButton ID="ibtnDelete" runat="server" AlternateText="Delete" CausesValidation="false"
                                TabIndex="16" ToolTip="Delete" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibtnUpdate" runat="server" AlternateText="Update" CommandName="Update"
                                TabIndex="17" ToolTip="Update" Height="20px" ImageUrl="~/Images/Update.png" />
                            <asp:ImageButton ID="ibtnCancel" runat="server" AlternateText="Cancel" CausesValidation="false"
                                TabIndex="18" ToolTip="Cancel" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="ibtnInsert" runat="server" AlternateText="Add" CommandName="FooterInsert"
                                TabIndex="14" ToolTip="Add" ImageUrl="~/Images/Add.jpg" />
                        </FooterTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table>
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" CssClass="button" ID="btnYes" Text="Yes" Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/AP/APChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
