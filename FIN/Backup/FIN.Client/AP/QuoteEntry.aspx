﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="QuoteEntry.aspx.cs" Inherits="FIN.Client.AP.QuoteEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1000px" id="divMainContainer">
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style="width: 200px" id="Div4">
                Quote Number
            </div>
            <div class="divtxtBox LNOrient" style=" width: 250px">
                <asp:TextBox ID="txtQuoteNo" runat="server" CssClass=" txtBox" MaxLength="50" Enabled="false" TabIndex="1"></asp:TextBox>
            </div>
            <div class="colspace LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 200px" id="Div5">
                Supplier Reference Number
            </div>
            <div class="divtxtBox LNOrient" style="width: 250px">
                <asp:TextBox ID="txtSupplierRefNo" runat="server" CssClass="validate[required]  RequiredField txtBox"
                    MaxLength="50" TabIndex="2"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="Div3">
                Supplier Name
            </div>
            <div class="divtxtBox LNOrient" style="width: 700px">
                <asp:DropDownList ID="ddlSupplierName" runat="server" TabIndex="3" AutoPostBack="true"
                    OnSelectedIndexChanged="ddlSupplierName_SelectedIndexChanged1" CssClass="ddlStype RequiredField validate[required]">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblNumber">
                Request for Quote Number
            </div>
            <div class="divtxtBox LNOrient" style="width: 250px">
                <asp:DropDownList ID="ddlQuoteNo" runat="server" OnSelectedIndexChanged="ddlQuoteNo_SelectedIndexChanged"
                    AutoPostBack="true" TabIndex="4" CssClass="ddlStype">
                </asp:DropDownList>
            </div>
            <div class="colspace LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="display: none; width: 200px" id="Div2" runat="server">
                Request for Quote Number
            </div>
            <div class="divtxtBox LNOrient" style=" display: none; width: 250px">
                <asp:DropDownList ID="ddlDraftRFQNUmber" runat="server" TabIndex="5" AutoPostBack="true"
                    OnSelectedIndexChanged="ddlDraftRFQNUmber_SelectedIndexChanged" CssClass="ddlStype">
                </asp:DropDownList>
                <asp:TextBox ID="txtRFQNumber" runat="server" Visible="false"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblDate">
                Request for Quote Date
            </div>
            <div class="divtxtBox LNOrient" style="width: 250px">
                <asp:TextBox Width="150px" ID="txtRFQDate" runat="server" Enabled="false" TabIndex="6"
                    CssClass="validate[required] validate[custom[ReqDateDDMMYYY]] RequiredField txtBox"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calExtender1" TargetControlID="txtRFQDate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
            <div class="colspace LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 200px" id="lblSupplierName ">
                Request for Quote Due Date
            </div>
            <div class="divtxtBox LNOrient" style="width: 250px">
                <asp:TextBox Width="150px" ID="txtRFQDueDate" runat="server" Enabled="false" TabIndex="7"
                    CssClass="validate[required] validate[custom[ReqDateDDMMYYY]] RequiredField txtBox"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtRFQDueDate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="Div1">
                Remarks
            </div>
            <div class="divtxtBox LNOrient" style="width: 455px">
                <asp:TextBox ID="txtRemarks" runat="server" Width="455px" CssClass="txtBox" Enabled="false"
                    Height="59px" MaxLength="1000" TabIndex="8" TextMode="MultiLine"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblPOType">
                Currency
            </div>
            <div class="divtxtBox LNOrient" style="width: 250px">
                <asp:DropDownList ID="ddlCurrency" runat="server" TabIndex="9" CssClass="SemiRequired ddlStype"
                    Enabled="false" Width="150px">
                </asp:DropDownList>
            </div>
            <div class="colspace LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 200px" id="Div8">
                Status
            </div>
            <div class="divtxtBox LNOrient" style="width: 250px">
                <asp:DropDownList ID="ddlStatus" runat="server" Width="20px" TabIndex="10" CssClass="SemiRequired ddlStype"
                    Enabled="false">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblRequisitionNumber">
                Active</div>
            <div class="divtxtBox LNOrient" >
                <asp:CheckBox runat="server" ID="chkActive" Checked="true" Enabled="false" TabIndex="11" />
            </div>
              <div class="colspace LNOrient"  >
                &nbsp</div>
            <%-- <div class="lblBox" style="float: left; width: 150px" id="Div6">
                Total Amount
            </div>--%>
            <div class="divtxtBox LNOrient" style="width: 180px">
            <asp:Label ID="lblTotal" runat="server" Text=""></asp:Label>
                <asp:TextBox Visible="false" ID="txtTotalAmt" runat="server" Width="180px" CssClass="validate[required] RequiredField txtBox"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class=" LNOrient">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" Width="800px"
                CssClass="DisplayFont Grid" OnRowDataBound="gvData_RowDataBound" ShowFooter="false"
                DataKeyNames="RFQ_DTL_ID,tmp_rfq_item_id,tmp_rfq_item_uom,LINE_NUM,tmp_rfq_item_cost,INV_ITEM_TYPE,UOM_ID,UOM_NAME">
                <Columns>
                    <asp:TemplateField HeaderText="Line No">
                        <ItemTemplate>
                            <asp:Label ID="lblLineNo" runat="server" Text='<%# Container.DataItemIndex + 1 %>'
                                Width="24px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Item Name">
                        <ItemTemplate>
                            <asp:DropDownList ID="ddlItemName" runat="server" CssClass="RequiredField ddlStype"
                                Visible="false" Enabled="false" TabIndex="12" Width="400px">
                            </asp:DropDownList>
                            <asp:Label ID="lblItemName" runat="server" Text='<%# Eval("ITEM_NAME") %>' Width="400px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Item Type">
                        <ItemTemplate>
                            <asp:TextBox ID="txtItemType" Enabled="false" runat="server" Text='<%# Eval("INV_ITEM_TYPE") %>'
                                Width="150px" TabIndex="13" CssClass="EntryFont RequiredField txtBox"></asp:TextBox>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="UOM">
                        <ItemTemplate>
                            <asp:DropDownList ID="ddlUOM" Visible="false" Enabled="false" runat="server" CssClass="RequiredField ddlStype"
                                Width="150px" TabIndex="14">
                            </asp:DropDownList>
                            <asp:Label ID="lblUOM" runat="server" Text='<%# Eval("UOM_NAME") %>' Width="150px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Quantity">
                        <ItemTemplate>
                            <asp:TextBox ID="txtQuantity" MaxLength="10" Enabled="false" runat="server" Text='<%# Eval("TMP_RFQ_ITEM_QTY") %>'
                                TabIndex="15" CssClass="EntryFont RequiredField txtBox_N" Width="50px"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers"
                                TargetControlID="txtQuantity" />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Right" Width="50px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Unit Price">
                        <ItemTemplate>
                            <asp:TextBox ID="txtUnitPrice" MaxLength="13" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                OnTextChanged="txtUnitPrice_TextChanged" AutoPostBack="true" Text='<%# Eval("RFQ_ITEM_PRICE") %>'
                                TabIndex="6" Width="150px"></asp:TextBox><cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtedrnder21"
                                    runat="server" FilterType="Numbers,Custom" ValidChars=".," TargetControlID="txtUnitPrice" />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Amount">
                        <ItemTemplate>
                            <asp:TextBox ID="txtAmount" MaxLength="13" runat="server" onkeyup="Calculate();" CssClass="EntryFont RequiredField txtBox_N"
                                Text='<%# Eval("RFQ_AMOUNT") %>' TabIndex="7" Width="150px"></asp:TextBox><cc2:FilteredTextBoxExtender
                                    ID="FilteredTextBoxExtender21" runat="server" FilterType="Numbers,Custom" ValidChars=".,"
                                    TargetControlID="txtAmount" />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
             
        </div>
        <div class="divClear_10">
        </div>
        <div class="divClear_10">
        </div>
        <div class=" LNOrient">
            <asp:GridView ID="gvAdditional" runat="server" AutoGenerateColumns="False" Width="510px"
                CssClass="DisplayFont Grid" OnRowCancelingEdit="gvData_RowCancelingEdit" OnRowCommand="gvData_RowCommand"
                OnRowCreated="gvData_RowCreated" OnRowDataBound="gvAdditional_RowDataBound" OnRowDeleting="gvData_RowDeleting"
                OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating" ShowFooter="True"
                DataKeyNames="RFQ_ADD_ID,RFQ_ADD_TYPE,RFQ_ADD_DESC">
                <Columns>
                    <asp:TemplateField HeaderText="Line No">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtLineNo" runat="server" Enabled="false" Text='<%# Eval("LINE_NUM") %>'
                                Width="50px" CssClass="EntryFont RequiredField txtBox" MaxLength="10"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtLineNo" runat="server" MaxLength="10" Enabled="false" CssClass="EntryFont RequiredField txtBox"
                                Width="50px"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblLineNo" runat="server" Text='<%# Container.DataItemIndex + 1 %>'
                                Width="50px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Description">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtDescription" runat="server" Text='<%# Eval("RFQ_ADD_DESC") %>' MaxLength="500"
                                Width="1000px" TabIndex="18" CssClass="EntryFont RequiredField txtBox"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtDescription" MaxLength="500" runat="server" CssClass="EntryFont RequiredField txtBox"
                                TabIndex="18" Width="1000px"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblDescription"  runat="server" Text='<%# Eval("RFQ_ADD_DESC") %>'
                                Width="450px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" runat="server" AlternateText="Edit" CausesValidation="false"
                                TabIndex="19" ToolTip="Edit" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                            <asp:ImageButton ID="ibtnDelete" runat="server" AlternateText="Delete" CausesValidation="false"
                                TabIndex="20" ToolTip="Delete" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibtnUpdate" runat="server" AlternateText="Update" CommandName="Update"
                                TabIndex="21" ToolTip="Update" Height="20px" ImageUrl="~/Images/Update.png" />
                            <asp:ImageButton ID="ibtnCancel" runat="server" AlternateText="Cancel" CausesValidation="false"
                                TabIndex="22" ToolTip="Cancel" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="ibtnInsert" runat="server" AlternateText="Add" CommandName="FooterInsert"
                                TabIndex="23" ToolTip="Add" ImageUrl="~/Images/Add.jpg" />
                        </FooterTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table width="100%">
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/AP/APChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">


        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

        function Calculate() {
            var grid = document.getElementById('<%=gvData.ClientID %>');
            var total = 0;

            if (grid.rows.length > 0) {
                var inputs = grid.getElementsByTagName("input");


                for (var i = 0; i < inputs.length; i++) {

                    if (inputs[i].name.indexOf("txtAmount") > 1) {
                        if (inputs[i].value != "") {
                            total = parseInt(total) + parseInt(inputs[i].value);
                        }
                    }
                }
            }
           
            document.getElementById('<%= lblTotal.ClientID %>').innerHTML = parseInt(total);
        }


//        function fn_totsum() {
//            var fields = document.getElementsByClassName('totsum');
//            var sum = 0;
//            for (var i = 0; i < fields.length; ++i) {
//                var item = fields[i];
//                sum += parseInt(item.innerHTML);
//            }
//            alert(sum);
//        }

        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });
    </script>
</asp:Content>
