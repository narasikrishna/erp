﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.AP;
using FIN.BLL;
using FIN.BLL.AP;
using FIN.BLL.HR;
using VMVServices.Web;
using System.Data;

namespace FIN.Client.AP
{
    public partial class PurchaseOrderReqEntry : PageBase
    {
        PO_REQUISITION_HDR pO_REQUISITION_HDR = new PO_REQUISITION_HDR();
        PO_REQUISITION_DTL pO_REQUISITION_DTL = new PO_REQUISITION_DTL();
        DataTable dtGridData = new DataTable();
        Boolean bol_rowVisiable;
        Boolean savedBool;
        string ProReturn = null;
        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }
            UserRightsChecking();

        }
        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }
        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>
        private void FillComboBox()
        {
            Department_BLL.GetDepartmentName(ref ddlDepartmentName);
            Lookup_BLL.GetLookUpValues(ref ddlRequisitionType, "RTY");
            Lookup_BLL.GetLookUpValues(ref ddlItemService, "IS");
        }
        protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                BindEmpName();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR1", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// .RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void BindEmpName()
        {
            Employee_BLL.GetEmplNameBothIntExt(ref ddlreqby, ddlDepartmentName.SelectedValue);
        }
        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>
        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();

                ChangeLanguage();
                Session["ItemService"] = null;
                EntityData = null;
                Session[FINSessionConstants.GridData] = null;

                txtRequisitionDate.Text = DBMethod.ConvertDateToString(DateTime.Now.ToShortDateString());

                dtGridData = DBMethod.ExecuteQuery(PurchaseOrderReq_DAL.GetPurchaseReqDetails(Master.StrRecordId)).Tables[0];


                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    pO_REQUISITION_HDR = PurchaseOrderReq_BLL.getClassEntity(Master.StrRecordId);

                    EntityData = pO_REQUISITION_HDR;

                    ddlItemService.Enabled = false;
                    txtDescription.Text = pO_REQUISITION_HDR.PO_REQ_DESC;
                    txtRequisitionNumber.Text = pO_REQUISITION_HDR.PO_REQ_ID;

                    if (pO_REQUISITION_HDR.PO_REQ_DT != null)
                    {
                        txtRequisitionDate.Text = DBMethod.ConvertDateToString(pO_REQUISITION_HDR.PO_REQ_DT.ToString());
                    }
                    if (pO_REQUISITION_HDR.NEED_BY_DATE != null)
                    {
                        txtNeedByDate.Text = DBMethod.ConvertDateToString(pO_REQUISITION_HDR.NEED_BY_DATE.ToString());
                    }

                    ddlRequisitionType.SelectedValue = pO_REQUISITION_HDR.PO_REQ_TYPE.ToString();
                    ddlDepartmentName.SelectedValue = pO_REQUISITION_HDR.DEPT_ID.ToString();
                    BindEmpName();
                    ddlItemService.SelectedValue = pO_REQUISITION_HDR.ITEM_SERVICE.ToString();
                    if (pO_REQUISITION_HDR.PO_REQ_AMT != null)
                    {
                        txtTotalAmount.Text = pO_REQUISITION_HDR.PO_REQ_AMT.ToString();
                        txtTotalAmount.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtTotalAmount.Text);
                    }

                    Session["ItemService"] = pO_REQUISITION_HDR.ITEM_SERVICE.ToString();

                   
                    lblRequisitionNumber.Visible = true;
                    lblRequisitionNumberValue.Visible = true;

                    if (pO_REQUISITION_HDR.ENABLED_FLAG == FINAppConstants.EnabledFlag)
                    {
                        chkActive.Checked = true;
                    }
                    else
                    {
                        chkActive.Checked = false;
                    }
                    ddlreqby.SelectedValue = pO_REQUISITION_HDR.REQUESTED_BY;
                    fillempname();
                    //txtRequestedBy.Text = pO_REQUISITION_HDR.REQ_BY;
                }

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        private void ChangeLanguage()
        {
            try
            {
                ErrorCollection.Clear();
                if (VMVServices.Web.Utils.LanguageCode.ToString().Length > 0)
                {
                    Dictionary<string, string> Prop_File_Data;
                    Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/AP_" + Session["Sel_Lng"].ToString() + ".properties"));
                    lblRequisitionNumber.InnerHtml = Prop_File_Data["Requisition_Number_P"];
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POReqChangeLang", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                if (dtData.Rows.Count > 0)
                {
                    if (dtData.Rows[0]["PO_QUANTITY"].ToString().Length > 0)
                    {
                        dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("PO_QUANTITY", DBMethod.GetAmtDecimalSeparationValue(p.Field<String>("PO_QUANTITY"))));
                    }
                    if (dtData.Rows[0]["PO_ITEM_UNIT_PRICE"].ToString().Length > 0)
                    {
                        dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("PO_ITEM_UNIT_PRICE", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("PO_ITEM_UNIT_PRICE"))));
                    }
                    if (dtData.Rows[0]["PO_Amount"].ToString().Length > 0)
                    {
                        dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("PO_Amount", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("PO_Amount"))));
                    }
                    dtData.AcceptChanges();
                }
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);

                txtTotalAmount.Text = CommonUtils.CalculateTotalAmount(dtData, "PO_AMOUNT");
                if (txtTotalAmount.Text.Length > 0)
                {
                    txtTotalAmount.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtTotalAmount.Text);
                }
                //fn_UnitPricetxtChanged();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountCodesEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddlItemName = tmpgvr.FindControl("ddlItemName") as DropDownList;
                DropDownList ddlUOM = tmpgvr.FindControl("ddlUOM") as DropDownList;
                UOMMasters_BLL.GetUOMDetails(ref ddlUOM);

                if (Session["ItemService"] != null)
                {
                    Item_BLL.GetItemName(ref ddlItemName, Session["ItemService"].ToString());
                }
                else
                {
                    Item_BLL.GetItemName(ref ddlItemName, ddlItemService.SelectedValue.ToString());
                }
                //else if (ddlItemService.SelectedItem.Text.ToUpper() == "SERVICE")
                //{
                //    Lookup_BLL.GetLookUpValues(ref ddlItemName, "18");
                //}

                if (gvData.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                {
                    ddlItemName.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.ITEM_ID].ToString();
                    ddlUOM.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["UOM_ID"].ToString();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            if (Session[FINSessionConstants.GridData] != null)
            {
                dtGridData = (DataTable)Session[FINSessionConstants.GridData];
            }
            gvData.EditIndex = -1;

            BindGrid(dtGridData);

        }
        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {
            System.Collections.SortedList slControls = new System.Collections.SortedList();

            TextBox txtLineNo = gvr.FindControl("txtLineNo") as TextBox;
            TextBox txtGridDescription = gvr.FindControl("txtGridDescription") as TextBox;
            TextBox txtQuantity = gvr.FindControl("txtQuantity") as TextBox;
            TextBox txtUnitPrice = gvr.FindControl("txtUnitPrice") as TextBox;
            TextBox txtPOAmount = gvr.FindControl("txtPOAmount") as TextBox;

            DropDownList ddlUOM = gvr.FindControl("ddlUOM") as DropDownList;
            // DropDownList ddlAmount = gvr.FindControl("ddlAmount") as DropDownList;
            DropDownList ddlItemName = gvr.FindControl("ddlItemName") as DropDownList;


            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList[FINColumnConstants.PO_REQ_ID] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }

            txtLineNo.Text = (rowindex + 1).ToString();

            slControls[0] = txtLineNo;
            slControls[1] = ddlItemName;
            // slControls[2] = txtDescription;
            slControls[2] = txtQuantity;
            slControls[3] = ddlUOM;
            slControls[4] = txtUnitPrice;
            //slControls[6] = txtPOAmount;

            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/AP_" + Session["Sel_Lng"].ToString() + ".properties"));
            ErrorCollection.Clear();
            string strCtrlTypes = FINAppConstants.TEXT_BOX + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.TEXT_BOX;
            string strMessage = Prop_File_Data["Line_No_P"] + " ~ " + Prop_File_Data["Item_Name_P"] + " ~ " + Prop_File_Data["Quantity_P"] + " ~ " + Prop_File_Data["UOM_P"] + " ~ " + Prop_File_Data["Unit_Price_P"] + "";
            //string strMessage = "Line No ~ Item Name ~ Quantity ~ UOM ~ Unit Price";

            EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

            if (EmptyErrorCollection.Count > 0)
            {
                ErrorCollection = EmptyErrorCollection;
                return drList;
            }


            string strCondition = "ITEM_ID='" + ddlItemName.SelectedValue.ToString() + "'";
            strMessage = FINMessageConstatns.RecordAlreadyExists;
            ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            if (ErrorCollection.Count > 0)
            {
                return drList;
            }

            if (txtTotalAmount.Text.Trim() == string.Empty)
            {
                txtTotalAmount.Text = "0";
            }

            //if (Session["POAmount"] != null)
            //{
            //    if (CommonUtils.ConvertStringToDecimal(txtTotalAmount.Text) >= 0 && CommonUtils.ConvertStringToDecimal(Session["POAmount"].ToString()) > 0)
            //    {
            //txtTotalAmount.Text = PurchaseOrderReq_BLL.CalculateAmount(dtGridData, CommonUtils.ConvertStringToDecimal(txtPOAmount.Text));
            //    }
            //}

            drList[FINColumnConstants.PO_REQ_LINE_NUM] = txtLineNo.Text;
            drList[FINColumnConstants.ITEM_ID] = ddlItemName.SelectedValue.ToString();
            drList[FINColumnConstants.ITEM_NAME] = ddlItemName.SelectedItem.Text.ToString();
            drList[FINColumnConstants.PO_DESCRIPTION] = txtGridDescription.Text;
            drList[FINColumnConstants.PO_QUANTITY] = txtQuantity.Text;
            // drList[FINColumnConstants.PO_REQ_UOM] = ddlUOM.SelectedValue.ToString();
            drList[FINColumnConstants.UOM_CODE] = ddlUOM.SelectedItem.Text.ToString();
            drList[FINColumnConstants.UOM_ID] = ddlUOM.SelectedValue.ToString();



            drList[FINColumnConstants.PO_ITEM_UNIT_PRICE] = CommonUtils.AmountSeparation(txtUnitPrice.Text);
            drList[FINColumnConstants.PO_Amount] = CommonUtils.ConvertStringToDecimal(txtPOAmount.Text);
            drList[FINColumnConstants.DELETED] = FINAppConstants.N;


            return drList;

        }


        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountCodesEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }
                    //tfn_UnitPricetxtChanged();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        #endregion

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();


                AssignToBE();
                ProReturn = FINSP.GetSPFOR_ERR_MGR_CATEGORY(Master.FormCode, pO_REQUISITION_HDR.PO_REQ_ID, pO_REQUISITION_HDR.PO_REQ_ID);
                if (ProReturn != string.Empty)
                {
                    if (ProReturn != "0")
                    {
                        ErrorCollection.Add("PURCHASEREQUISITION", ProReturn);
                        if (ErrorCollection.Count > 0)
                        {
                            return;
                        }
                    }
                }
                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    pO_REQUISITION_HDR = (PO_REQUISITION_HDR)EntityData;
                }

                pO_REQUISITION_HDR.PO_REQ_ID = txtRequisitionNumber.Text;

                if (txtRequisitionDate.Text != string.Empty)
                {
                    pO_REQUISITION_HDR.PO_REQ_DT = DBMethod.ConvertStringToDate(txtRequisitionDate.Text.ToString());
                }
                if (txtNeedByDate.Text != string.Empty)
                {
                    pO_REQUISITION_HDR.NEED_BY_DATE = DBMethod.ConvertStringToDate(txtNeedByDate.Text.ToString());
                }
                pO_REQUISITION_HDR.PO_REQ_TYPE = ddlRequisitionType.SelectedValue.ToString();
                pO_REQUISITION_HDR.DEPT_ID = (ddlDepartmentName.SelectedValue.ToString());
                pO_REQUISITION_HDR.PO_REQ_DESC = txtDescription.Text;
                pO_REQUISITION_HDR.ITEM_SERVICE = ddlItemService.SelectedValue.ToString();
                pO_REQUISITION_HDR.PO_REQ_AMT = CommonUtils.ConvertStringToDecimal(txtTotalAmount.Text);

                pO_REQUISITION_HDR.ENABLED_FLAG = chkActive.Checked == true ? FINAppConstants.EnabledFlag : FINAppConstants.DisabledFlag;
                // pO_REQUISITION_HDR.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;
                pO_REQUISITION_HDR.ORG_ID = VMVServices.Web.Utils.OrganizationID;

                pO_REQUISITION_HDR.REQ_BY = ddlreqby.SelectedItem.Text;
                pO_REQUISITION_HDR.REQUESTED_BY = ddlreqby.SelectedValue;
                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    pO_REQUISITION_HDR.MODIFIED_BY = this.LoggedUserName;
                    pO_REQUISITION_HDR.MODIFIED_DATE = DateTime.Today;
                }
                else
                {
                    pO_REQUISITION_HDR.PO_REQ_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.AP_012_M.ToString(), false, true);
                    pO_REQUISITION_HDR.CREATED_BY = this.LoggedUserName;
                    pO_REQUISITION_HDR.CREATED_DATE = DateTime.Today;
                }

                pO_REQUISITION_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, pO_REQUISITION_HDR.PO_REQ_ID);
                //Save Detail Table


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                ErrorCollection = CommonUtils.IsEmptyGrid(dtGridData, "Item ");
                if (ErrorCollection.Count > 0)
                {
                    return;
                }


                var tmpChildEntity = new List<Tuple<object, string>>();
                PO_REQUISITION_DTL pO_REQUISITION_DTL = new PO_REQUISITION_DTL();

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    pO_REQUISITION_DTL = new PO_REQUISITION_DTL();
                    if (dtGridData.Rows[iLoop][FINColumnConstants.PO_REQ_LINE_ID].ToString() != "0" && dtGridData.Rows[iLoop][FINColumnConstants.PO_REQ_LINE_ID].ToString() != string.Empty)
                    {
                        pO_REQUISITION_DTL = PurchaseOrderReq_BLL.getDetailClassEntity(dtGridData.Rows[iLoop][FINColumnConstants.PO_REQ_LINE_ID].ToString());
                    }
                    pO_REQUISITION_DTL.PO_REQ_LINE_NUM = (iLoop + 1);// CommonUtils.ConvertStringToInt(dtGridData.Rows[iLoop][FINColumnConstants.PO_REQ_LINE_NUM].ToString());
                    pO_REQUISITION_DTL.ITEM_ID = (dtGridData.Rows[iLoop][FINColumnConstants.ITEM_ID].ToString());
                    pO_REQUISITION_DTL.PO_DESCRIPTION = (dtGridData.Rows[iLoop][FINColumnConstants.PO_DESCRIPTION].ToString());
                    pO_REQUISITION_DTL.PO_QUANTITY = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop][FINColumnConstants.PO_QUANTITY].ToString());
                    if (dtGridData.Rows[iLoop][FINColumnConstants.UOM_ID].ToString().Length == 0)
                    {
                        ErrorCollection.Add("uomerror1", "UOM cannot be empty -Row No:" + (iLoop + 1));
                        return;
                    }
                    pO_REQUISITION_DTL.PO_REQ_UOM = (dtGridData.Rows[iLoop][FINColumnConstants.UOM_ID].ToString());
                    pO_REQUISITION_DTL.PO_ITEM_UNIT_PRICE = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop][FINColumnConstants.PO_ITEM_UNIT_PRICE].ToString());

                    pO_REQUISITION_DTL.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;
                    pO_REQUISITION_DTL.ENABLED_FLAG = FINAppConstants.EnabledFlag;

                    pO_REQUISITION_DTL.PO_REQ_ID = pO_REQUISITION_HDR.PO_REQ_ID;

                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        tmpChildEntity.Add(new Tuple<object, string>(pO_REQUISITION_DTL, FINAppConstants.Delete));
                    }
                    else
                    {
                        if (dtGridData.Rows[iLoop][FINColumnConstants.PO_REQ_LINE_ID].ToString() != "0" && dtGridData.Rows[iLoop][FINColumnConstants.PO_REQ_LINE_ID].ToString() != string.Empty)
                        {
                            pO_REQUISITION_DTL.MODIFIED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(pO_REQUISITION_DTL, FINAppConstants.Update));
                        }
                        else
                        {
                            pO_REQUISITION_DTL.PO_REQ_LINE_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.AP_012_D.ToString(), false, true);
                            pO_REQUISITION_DTL.CREATED_BY = this.LoggedUserName;
                            pO_REQUISITION_DTL.CREATED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(pO_REQUISITION_DTL, FINAppConstants.Add));
                        }
                    }
                    pO_REQUISITION_DTL.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;

                }
                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            CommonUtils.SavePCEntity<PO_REQUISITION_HDR, PO_REQUISITION_DTL>(pO_REQUISITION_HDR, tmpChildEntity, pO_REQUISITION_DTL);
                            savedBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {
                            CommonUtils.SavePCEntity<PO_REQUISITION_HDR, PO_REQUISITION_DTL>(pO_REQUISITION_HDR, tmpChildEntity, pO_REQUISITION_DTL, true);
                            savedBool = true;
                            break;
                        }
                }
                if (ErrorCollection.Count > 0)
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                // DBMethod.DeleteEntity<pO_REQUISITION_HDR>(pO_REQUISITION_HDR);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void txtUnitPrice_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                fn_UnitPriceChanged();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        //private void txtQuantity_TextChanged(object sender, EventArgs e)
        //{
        //    fn_UnitPriceChanged(); 
        //}

        //protected void ddlItemName_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;

        //    DropDownList ddlItemName = gvr.FindControl("ddlItemName") as DropDownList;
        //    TextBox txtUnitPrice = gvr.FindControl("txtUnitPrice") as TextBox;
        //    DataTable dtunitprice = new DataTable();
        //    dtunitprice = DBMethod.ExecuteQuery(PurchaseOrder_DAL.GetItemUnitprice(ddlItemName.SelectedValue)).Tables[0];
        //    txtUnitPrice.Text = dtunitprice.Rows[0]["ITEM_UNIT_PRICE"].ToString();
        //    txtUnitPrice.Enabled = false;
        //}

        private void fn_UnitPriceChanged()
        {
            try
            {
                ErrorCollection.Clear();
                TextBox txtUnitPrice = new TextBox();
                TextBox txtQuantity = new TextBox();
                TextBox txtPOAmount = new TextBox();

                txtUnitPrice.ID = "txtUnitPrice";
                txtQuantity.ID = "txtQuantity";
                txtPOAmount.ID = "txtPOAmount";

                if (gvData.FooterRow != null)
                {
                    if (gvData.EditIndex < 0)
                    {
                        txtUnitPrice = (TextBox)gvData.FooterRow.FindControl("txtUnitPrice");
                        txtQuantity = (TextBox)gvData.FooterRow.FindControl("txtQuantity");
                        txtPOAmount = (TextBox)gvData.FooterRow.FindControl("txtPOAmount");
                    }
                    else
                    {
                        txtUnitPrice = (TextBox)gvData.Rows[gvData.EditIndex].FindControl("txtUnitPrice");
                        txtQuantity = (TextBox)gvData.Rows[gvData.EditIndex].FindControl("txtQuantity");
                        txtPOAmount = (TextBox)gvData.Rows[gvData.EditIndex].FindControl("txtPOAmount");
                    }
                }
                else
                {
                    txtUnitPrice = (TextBox)gvData.Controls[0].Controls[1].FindControl("txtUnitPrice");
                    txtQuantity = (TextBox)gvData.Controls[0].Controls[1].FindControl("txtQuantity");
                    txtPOAmount = (TextBox)gvData.Controls[0].Controls[1].FindControl("txtPOAmount");
                }
                if (txtUnitPrice != null && txtQuantity != null)
                {
                    if (txtUnitPrice.Text.Trim() != string.Empty && txtQuantity.Text.Trim() != string.Empty)
                    {
                        if (CommonUtils.ConvertStringToDecimal(txtUnitPrice.Text.ToString()) > 0 && CommonUtils.ConvertStringToDecimal(txtQuantity.Text.ToString()) > 0)
                        {
                            txtPOAmount.Text = (CommonUtils.ConvertStringToDecimal(txtUnitPrice.Text) * CommonUtils.ConvertStringToDecimal(txtQuantity.Text)).ToString();
                            txtPOAmount.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtPOAmount.Text);
                            //if (txtTotalAmount.Text.Trim() == string.Empty)
                            //{
                            //    txtTotalAmount.Text = "0";
                            //}
                        }
                        else
                        {
                            txtPOAmount.Text = "0";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        //private void fn_UnitPricetxtChanged()
        //{
        //    try
        //    {
        //        ErrorCollection.Clear();

        //        if (Session[FINSessionConstants.GridData] != null)
        //        {
        //            dtGridData = (DataTable)Session[FINSessionConstants.GridData];
        //        }
        //        if (dtGridData != null)
        //        {
        //            if (dtGridData.Rows.Count > 0)
        //            {
        //                for (int i = 0; i < dtGridData.Rows.Count; i++)
        //                {
        //                    TextBox txtUnitPrice = (TextBox)gvData.Rows[i].FindControl("txtUnitPrice");
        //                    TextBox txtQuantity = (TextBox)gvData.Rows[i].FindControl("txtQuantity");
        //                    TextBox txtPOAmount = (TextBox)gvData.Rows[i].FindControl("txtPOAmount");

        //                    if (txtUnitPrice != null && txtQuantity != null)
        //                    {
        //                        if (txtUnitPrice.Text.Trim() != string.Empty && txtQuantity.Text.Trim() != string.Empty)
        //                        {
        //                            if (int.Parse(txtUnitPrice.Text.ToString()) > 0 && int.Parse(txtQuantity.Text.ToString()) > 0)
        //                            {
        //                                txtPOAmount.Text = (CommonUtils.ConvertStringToDecimal(txtUnitPrice.Text) * CommonUtils.ConvertStringToDecimal(txtQuantity.Text)).ToString();
        //                                //if (txtTotalAmount.Text.Trim() == string.Empty)
        //                                //{
        //                                //    txtTotalAmount.Text = "0";
        //                                //}
        //                                //if (CommonUtils.ConvertStringToDecimal(txtTotalAmount.Text) >= 0 && CommonUtils.ConvertStringToDecimal(txtPOAmount.Text) > 0)
        //                                //{
        //                                //    txtTotalAmount.Text = (CommonUtils.ConvertStringToDecimal(txtPOAmount.Text) + CommonUtils.ConvertStringToDecimal(txtTotalAmount.Text)).ToString();
        //                                //}

        //                                Session["POAmount"] = txtPOAmount.Text;
        //                            }
        //                        }
        //                    }



        //                }
        //            }
        //        }


        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorCollection.Add("POR", ex.Message);
        //    }
        //    finally
        //    {
        //        if (ErrorCollection.Count > 0)
        //        {
        //            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
        //        }
        //    }
        //}
        #endregion

        /// <summary>
        /// Clear the grid view records 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlItemService_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ClearGrid();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void ClearGrid()
        {
            if (Session[FINSessionConstants.GridData] != null)
            {
                dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                dtGridData.Rows.Clear();
                gvData.DataSource = dtGridData;
                gvData.DataBind();

                dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                BindGrid(dtGridData);

                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);
            }
        }

        protected void ddlRequisitionType_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void txtQuantity_TextChanged(object sender, EventArgs e)
        {
            fn_UnitPriceChanged();
        }

        protected void ddlItemName_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;

            DropDownList ddlItemName = gvr.FindControl("ddlItemName") as DropDownList;
            DropDownList ddlUOM = gvr.FindControl("ddlUOM") as DropDownList;
            TextBox txtUnitPrice = gvr.FindControl("txtUnitPrice") as TextBox;
            DataTable dtunitprice = new DataTable();
            dtunitprice = DBMethod.ExecuteQuery(PurchaseOrder_DAL.GetItemUnitprice(ddlItemName.SelectedValue)).Tables[0];
            txtUnitPrice.Text = dtunitprice.Rows[0]["ITEM_UNIT_PRICE"].ToString();
            txtUnitPrice.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtUnitPrice.Text);
            // txtUnitPrice.Enabled = false;

            UOMMasters_BLL.getUOMBasedItem(ref ddlUOM, ddlItemName.SelectedValue);

            //DataTable dtUOM = new DataTable();
            //dtUOM = DBMethod.ExecuteQuery(PurchaseOrder_DAL.GetItemUOM(ddlItemName.SelectedValue)).Tables[0];
            //if (dtUOM.Rows.Count > 0)
            //{
            //    if (dtUOM.Rows[0]["ITEM_UOM"].ToString().Length > 0)
            //    {
            //        //UOMMasters_BLL.GetUOMDetails(ref ddlUOM);
            //        ComboFilling.fn_getUOMDetails(ref ddlUOM);
            //        ddlUOM.SelectedValue = dtUOM.Rows[0]["ITEM_UOM"].ToString();
            //    }
            //}
            fn_UnitPriceChanged();
        }

        protected void ddlreqby_SelectedIndexChanged(object sender, EventArgs e)
        {

            fillempname();

        }

        private void fillempname()
        {
            DataTable dtempname = new DataTable();
            dtempname = DBMethod.ExecuteQuery(FIN.DAL.HR.Employee_DAL.GetEmployeeName(ddlreqby.SelectedValue)).Tables[0];
            if (dtempname.Rows.Count > 0)
            {
                // txtRequestedBy.Text = dtempname.Rows[0]["EMP_NAME"].ToString(); // by deivamani
                // txtRequestedBy.Enabled = false;
            }
            else
            {
                //  txtRequestedBy.Enabled = true;
                // txtRequestedBy.Text = "";
            }
        }

    }
}