﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="SupplierEntry.aspx.cs" Inherits="FIN.Client.AP.Supplier" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1100px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style="width: 160px" id="lblSupplierNumber">
                Supplier Number
            </div>
            <div class="divtxtBox  LNOrient" style="width: 160px">
                <asp:TextBox ID="txtSupplierNumber" MaxLength="50" CssClass=" txtBox" Enabled="false"
                    runat="server" TabIndex="1"></asp:TextBox>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp;</div>
            <div class="lblBox  LNOrient" style="width: 160px" id="lblSupplierName">
                Supplier Name
            </div>
            <div class="divtxtBox  LNOrient" style="width: 510px">
                <asp:TextBox ID="txtSupplierName" CssClass="validate[required] RequiredField txtBox_en"
                    MaxLength="200" runat="server" TabIndex="2" OnTextChanged="txtSupplierName_TextChanged"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style="width: 160px" id="Div10">
                Supplier Name (Arabic)
            </div>
            <div class="divtxtBox  LNOrient" style="width: 853px">
                <asp:TextBox ID="txtSupplierNameOL" CssClass="txtBox_ol" runat="server" MaxLength="240"
                    TabIndex="3"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style="width: 160px" id="Div9">
                Cheque Print Name
            </div>
            <div class="divtxtBox  LNOrient" style="width: 853px">
                <asp:TextBox ID="txtChequePrintName" CssClass="txtBox" MaxLength="240" runat="server"
                    TabIndex="4"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style="width: 160px" id="lblSupplierType">
                Supplier Type
            </div>
            <div class="divtxtBox  LNOrient" style="width: 160px">
                <asp:DropDownList ID="ddlSupplierType" runat="server" TabIndex="5" CssClass="validate[required]  RequiredField ddlStype"
                    AutoPostBack="True" OnSelectedIndexChanged="ddlSupplierType_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp;</div>
            <div class="lblBox  LNOrient" style="width: 160px" id="lblSupplierStatus">
                Supplier Status
            </div>
            <div class="divtxtBox  LNOrient" style="width: 163px">
                <asp:DropDownList ID="ddlSupplierStatus" runat="server" TabIndex="6" CssClass=" ddlStype">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp;</div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style="width: 160px" id="Div11">
                Supplier Category
            </div>
            <div class="divtxtBox  LNOrient" style="width: 160px">
                <asp:DropDownList ID="ddlSupplierCategory" AutoPostBack="true" runat="server" TabIndex="7"
                    CssClass=" ddlStype">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp;</div>
            <div class="lblBox  LNOrient" style="width: 160px" id="lblRetentionAcc">
                Retention Account
            </div>
            <div class="divtxtBox  LNOrient" style="width: 163px">
                <asp:DropDownList ID="ddlRetAccount" runat="server" TabIndex="8" CssClass=" ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" runat="server" id="APAcc" visible="false">
            <div class="lblBox  LNOrient" style="width: 160px" id="Div5">
                AP Liability Account
            </div>
            <div class="divtxtBox  LNOrient" style="width: 160px">
                <asp:DropDownList ID="ddlAPLiable" runat="server" TabIndex="9" CssClass=" ddlStype">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp;</div>
            <div class="lblBox  LNOrient" style="width: 160px" id="Div7">
                AP Advance Account
            </div>
            <div class="divtxtBox  LNOrient" style="width: 160px">
                <asp:DropDownList ID="ddlAPAdvance" runat="server" TabIndex="10" CssClass=" ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" runat="server" id="ARAcc" visible="false">
            <div class="lblBox  LNOrient" style="width: 160px" id="Div2">
                AR Debtors Account
            </div>
            <div class="divtxtBox  LNOrient" style="width: 160px">
                <asp:DropDownList ID="ddlARDebtors" runat="server" TabIndex="11" CssClass=" ddlStype">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp;</div>
            <div class="lblBox  LNOrient" style="width: 160px" id="Div3">
                AR Advance
            </div>
            <div class="divtxtBox  LNOrient" style="width: 160px">
                <asp:DropDownList ID="ddlARAdvance" runat="server" TabIndex="11" CssClass=" ddlStype">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp;</div>
            <div class="lblBox  LNOrient" style="width: 160px" id="Div8">
                AR Revenue Account
            </div>
            <div class="divtxtBox  LNOrient" style="width: 160px">
                <asp:DropDownList ID="ddlARRevenue" runat="server" TabIndex="12" CssClass=" ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style="width: 160px" id="lblPaymentTerm">
                Payment Term
            </div>
            <div class="divtxtBox  LNOrient" style="width: 160px">
                <asp:DropDownList ID="ddlPaymentTerm" runat="server" TabIndex="13" CssClass="ddlStype">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp;</div>
            <div class="lblBox  LNOrient" style="width: 160px" id="lblAlternateName">
                Alternate Name
            </div>
            <div class="divtxtBox  LNOrient" style="width: 160px">
                <asp:TextBox ID="txtAlternateName" MaxLength="200" CssClass="txtBox" runat="server"
                    TabIndex="14"></asp:TextBox>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp;</div>
            <div class="lblBox  LNOrient" style="width: 160px" id="lblContactPerson">
                Contact Person
            </div>
            <div class="divtxtBox  LNOrient" style="width: 160px">
                <asp:TextBox ID="txtContactPerson" MaxLength="50" CssClass="txtBox" runat="server"
                    TabIndex="15"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style="width: 160px" id="lblAddress1">
                Address 1
            </div>
            <div class="divtxtBox  LNOrient" style="width: 160px">
                <asp:TextBox ID="txtAddress1" MaxLength="100" CssClass="validate[required] RequiredField txtBox"
                    Height="50px" runat="server" TabIndex="16" TextMode="MultiLine"></asp:TextBox>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp;</div>
            <div class="lblBox  LNOrient" style="width: 160px" id="lblAddress2">
                Address 2
            </div>
            <div class="divtxtBox  LNOrient" style="width: 160px">
                <asp:TextBox ID="txtAddress2" MaxLength="100" Height="50px" CssClass=" txtBox" TextMode="MultiLine"
                    runat="server" TabIndex="17"></asp:TextBox>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp;</div>
            <div class="lblBox  LNOrient" style="width: 160px" id="lblAddress3">
                Address 3
            </div>
            <div class="divtxtBox  LNOrient" style="width: 160px">
                <asp:TextBox ID="txtAddress3" MaxLength="100" CssClass=" txtBox" Height="50px" runat="server"
                    TextMode="MultiLine" TabIndex="18"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style="width: 160px" id="lblCountry">
                Country
            </div>
            <div class="divtxtBox  LNOrient" style="width: 161px">
                <asp:DropDownList ID="ddlCountry" AutoPostBack="True" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged"
                    runat="server" TabIndex="19" CssClass="validate[required]  RequiredField ddlStype">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp;</div>
            <div class="lblBox  LNOrient" style="width: 160px" id="lblState">
                State
            </div>
            <div class="divtxtBox  LNOrient" style="width: 163px">
                <asp:DropDownList ID="ddlState" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlState_SelectedIndexChanged"
                    TabIndex="20" CssClass="ddlStype">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp;</div>
            <div class="lblBox  LNOrient" style="width: 155px" id="lblCity">
                City
            </div>
            <div class="divtxtBox  LNOrient" style="width: 165px">
                <asp:DropDownList ID="ddlCity" runat="server" TabIndex="21" CssClass="ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style="width: 160px" id="lblPostalCode">
                Postal Code
            </div>
            <div class="divtxtBox  LNOrient" style="width: 160px">
                <asp:TextBox ID="txtPostalCode" MaxLength="15" CssClass="txtBox_N" runat="server"
                    TabIndex="22"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars=""
                    FilterType="Numbers,Custom" TargetControlID="txtPostalCode" />
            </div>
            <div class="colspace  LNOrient" >
                &nbsp;</div>
            <div class="lblBox  LNOrient" style="width: 160px" id="lblPhone">
                Phone
            </div>
            <div class="divtxtBox  LNOrient" style="width: 160px">
                <asp:TextBox ID="txtPhone" CssClass="txtBox_N" runat="server" MaxLength="15" TabIndex="23"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars="+,-"
                    FilterType="Numbers,Custom" TargetControlID="txtPhone" />
            </div>
            <div class="colspace  LNOrient" >
                &nbsp;</div>
            <div class="lblBox  LNOrient" style="width: 160px" id="Div1">
                Fax
            </div>
            <div class="divtxtBox  LNOrient" style="width: 160px">
                <asp:TextBox ID="txtFax" CssClass=" txtBox_N" runat="server" MaxLength="15" TabIndex="24"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" ValidChars="+,-"
                    FilterType="Numbers,Custom" TargetControlID="txtFax" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style="width: 160px" id="lblEmail">
                Email
            </div>
            <div class="divtxtBox  LNOrient" style="width: 340px">
                <asp:TextBox ID="txtEmail" MaxLength="100" CssClass="validate[custom[email]] txtBox"
                    runat="server" TabIndex="25"></asp:TextBox>
                <asp:RegularExpressionValidator ID="reqEmail" runat="server" ValidationGroup="btnSave"
                    ControlToValidate="txtEmail" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">* 
                                                    Email Id is Invalid Format</asp:RegularExpressionValidator>
            </div>
            <div class="lblBox  LNOrient" style="width: 155px" id="lblURL">
                URL
            </div>
            <div class="divtxtBox  LNOrient" style="width: 333px">
                <asp:TextBox ID="txtURL" CssClass=" txtBox" runat="server" TabIndex="26" MaxLength="200"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style="width: 160px" id="lblTaxID1">
                Tax ID 1
            </div>
            <div class="divtxtBox  LNOrient" style="width: 160px">
                <asp:TextBox ID="txtTaxID1" MaxLength="50" CssClass=" txtBox" runat="server" TabIndex="27"></asp:TextBox>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp;</div>
            <div class="lblBox  LNOrient" style="width: 160px" id="Div4">
                Tax ID 2
            </div>
            <div class="divtxtBox  LNOrient" style="width: 160px">
                <asp:TextBox ID="txtTaxID2" MaxLength="50" CssClass=" txtBox" runat="server" TabIndex="28"></asp:TextBox>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp;</div>
            <div class="lblBox  LNOrient" style="width: 160px" id="lblTaxID2">
                Tax ID 3
            </div>
            <div class="divtxtBox  LNOrient" style="width: 160px">
                <asp:TextBox ID="txtTaxID3" MaxLength="50" CssClass=" txtBox" runat="server" TabIndex="29"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style="width: 160px" id="Div6">
                Tax ID 4
            </div>
            <div class="divtxtBox  LNOrient" style="width: 160px">
                <asp:TextBox ID="txtTaxID4" MaxLength="50" CssClass="txtBox" runat="server" TabIndex="30"></asp:TextBox>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp;</div>
            <div class="lblBox  LNOrient" style="width: 160px" id="lblTaxID3">
                Tax ID 5
            </div>
            <div class="divtxtBox  LNOrient" style="width: 160px">
                <asp:TextBox ID="txtTaxID5" MaxLength="50" CssClass="txtBox" runat="server" TabIndex="31"></asp:TextBox>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp;</div>
            <div class="divRowContainer">
                <div class="lblBox  LNOrient" style="width: 160px" id="lblActive">
                    Active
                </div>
                <div class="divtxtBox  LNOrient" style="width: 160px">
                    <asp:CheckBox ID="chkActive" runat="server" Checked="True" TabIndex="32" />
                </div>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                            TabIndex="33" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" OnClick="btnDelete_Click"
                            TabIndex="34" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="35" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="36" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table>
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/AP/APChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">


        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });
    </script>
</asp:Content>
