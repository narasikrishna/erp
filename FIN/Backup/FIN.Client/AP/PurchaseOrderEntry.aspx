﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="PurchaseOrderEntry.aspx.cs" Inherits="FIN.Client.AP.PurchaseOrderEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1000px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style="width: 150px" id="lblNumber">
                Number
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:TextBox ID="txtPONumber" runat="server" CssClass=" txtBox" Enabled="false" TabIndex="1"></asp:TextBox>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox  LNOrient" style="width: 100px" id="lblDate">
                Date
            </div>
            <div class="divtxtBox  LNOrient" style="width: 120px">
                <asp:TextBox ID="txtPODate" runat="server" TabIndex="2" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]] RequiredField txtBox"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calExtender1" TargetControlID="txtPODate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox  LNOrient" style="width: 100px" id="lblPOType">
                Purchase Order Type
            </div>
            <div class="divtxtBox  LNOrient" style="width: 151px">
                <asp:DropDownList ID="ddlPOType" runat="server" TabIndex="3" CssClass="ddlStype"
                    AutoPostBack="True" OnSelectedIndexChanged="ddlPOType_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style="width: 150px" id="lblSupplierName ">
                Supplier Name
            </div>
            <div class="divtxtBox  LNOrient" style="width: 660px">
                <asp:DropDownList ID="ddlSupplierName" runat="server" TabIndex="4" AutoPostBack="true"
                    OnSelectedIndexChanged="ddlSupplierName_SelectedIndexChanged" CssClass="ddlStype RequiredField validate[required]">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style="width: 150px" id="Div1">
                Supplier Site
            </div>
            <div class="divtxtBox  LNOrient" style="width: 660px">
                <asp:DropDownList ID="ddlSupplierSite" runat="server" TabIndex="5" CssClass="ddlStype  ">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style="width: 150px" id="lblRequisitionNumber">
                <asp:Label ID="lblPoTypeNumber" runat="server" Text="Number"></asp:Label></div>
            <div class="divtxtBox  LNOrient" style="width: 660px">
                <asp:DropDownList ID="ddlRequisitionNumber" runat="server" TabIndex="6" CssClass="ddlStype"
                    OnSelectedIndexChanged="ddlRequisitionNumber_SelectedIndexChanged" AutoPostBack="True">
                    <asp:ListItem>---Select---</asp:ListItem>
                </asp:DropDownList>
                <br />
                <asp:Label ID="lblDescription" runat="server" Text='<%# Eval("PO_REQ_DESC") %>' Width="100%">
                </asp:Label>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="divtxtBox  LNOrient" >
                <asp:Button ID="btnPrint" runat="server" Text="Compare Quote" OnClick="btnPrint_Click"
                    CssClass="btn" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style="width: 150px" id="Div3">
                Description
            </div>
            <div class="divtxtBox  LNOrient" style="width: 389px">
                <asp:TextBox ID="txtDescription" runat="server" TabIndex="7" MaxLength="100" CssClass="txtBox"></asp:TextBox>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox  LNOrient" style="width: 100px" id="Div6">
                Need by Date
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:TextBox ID="txtNeedByDate" runat="server" TabIndex="8" CssClass="validate[] validate[custom[ReqDateDDMMYYY]] txtBox"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarEsxtendedr1"
                    TargetControlID="txtNeedByDate" OnClientDateSelectionChanged="checkDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <%--  <div class="lblBox  LNOrient" style="width: 150px" id="Div5">
                Payment Term
            </div>
            <div class="divtxtBox  LNOrient" style="width: 220px">
                <asp:DropDownList ID="ddlPaymentTerm" runat="server" TabIndex="9" CssClass="ddlStype">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>--%>
            <div class="lblBox  LNOrient" style="width: 150px" id="lblCurrency">
                Currency
            </div>
            <div class="divtxtBox  LNOrient" style="width: 220px">
                <asp:DropDownList ID="ddlCurrency" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    TabIndex="9">
                    <asp:ListItem Value="" Text="---Select---"></asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox  LNOrient" style="width: 145px" id="Div7">
                Deliver To
            </div>
            <div class="divtxtBox  LNOrient" style="width: 276px">
                <asp:DropDownList ID="ddlDeliverTo" runat="server" TabIndex="10" CssClass="ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style="width: 150px" id="Div4">
                Mode of Transport
            </div>
            <div class="divtxtBox  LNOrient" style="width: 220px">
                <asp:DropDownList ID="ddlModeofTransport" runat="server" CssClass="ddlStype" TabIndex="11">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox  LNOrient" style="width: 145px" id="Div8">
                Price List
            </div>
            <div class="divtxtBox  LNOrient" style="width: 276px">
                <asp:DropDownList ID="ddlPriceName" runat="server" CssClass="ddlStype" AutoPostBack="true"
                    TabIndex="12" OnSelectedIndexChanged="ddlPriceName_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style="width: 150px" id="Div2">
                Purchase Order Amount
            </div>
            <div class="divtxtBox  LNOrient" style="width: 220px">
                <asp:TextBox Enabled="false" ID="txtPOTotalAmount" runat="server" Width="120px" CssClass="validate[required] RequiredField txtBox_N"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div>
                <asp:DropDownList ID="ddlPOStatus" Visible="false" runat="server" Width="20px" TabIndex="13"
                    CssClass="ddlStype">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
        </div>
        <%-- <div class="divRowContainer">
            <div class="lblBox  LNOrient" style="width: 200px" id="Div4" runat="server">
                Supplier Price
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:DropDownList ID="ddlSupplierPrice" runat="server" Width="150px" TabIndex="9" 
                    CssClass="ddlStype" onselectedindexchanged="ddlSupplierPrice_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>--%>
        <div class="divClear_10">
        </div>
        <%-- <div class="divRowContainer">
            <div class="divtxtBox  LNOrient" style="float: right; width: 545px">
                <asp:ImageButton ID="btnPrint" runat="server" ImageUrl="../Images/print-icon.png"
                    OnClick="btnPrint_Click" Width="35px" Height="25px" Style="border: 0px" />
            </div>
        </div>--%>
        <div class="divClear_10">
        </div>
        <div class="LNOrient">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" Width="900px"
                CssClass="DisplayFont Grid" OnRowCancelingEdit="gvData_RowCancelingEdit" OnRowCommand="gvData_RowCommand"
                OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound" OnRowDeleting="gvData_RowDeleting"
                OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating" ShowFooter="True"
                DataKeyNames="PO_HEADER_ID,PO_LINE_ID,ITEM_ID,PO_REQ_UOM,UOM_code">
                <Columns>
                    <asp:TemplateField HeaderText="Line No">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtLineNo" runat="server" Enabled="false" Text='<%# Eval("PO_REQ_LINE_NUM") %>'
                                Width="95%" CssClass="EntryFont RequiredField txtBox" MaxLength="10"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtLineNo" runat="server" MaxLength="10" Enabled="false" CssClass="EntryFont RequiredField txtBox"
                                Width="95%"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblLineNo" runat="server" Text='<%# Container.DataItemIndex + 1 %>'
                                Width="95%"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Item Name">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlItemName" runat="server" CssClass="RequiredField ddlStype"
                                TabIndex="13" Width="250px" AutoPostBack="True" OnSelectedIndexChanged="ddlItemName_SelectedIndexChanged">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlItemName" runat="server" CssClass="RequiredField ddlStype"
                                TabIndex="13" Width="250px" AutoPostBack="True" OnSelectedIndexChanged="ddlItemName_SelectedIndexChanged">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblItemName" runat="server" Text='<%# Eval("ITEM_NAME") %>' Width="250px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Description">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtDescription" runat="server" Text='<%# Eval("PO_DESCRIPTION") %>'
                                Width="200px" TabIndex="14" CssClass="EntryFont  txtBox"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtDescription" MaxLength="250" runat="server" CssClass="EntryFont  txtBox"
                                TabIndex="14" Width="200px"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblDescription" MaxLength="250" runat="server" Text='<%# Eval("PO_DESCRIPTION") %>'
                                Width="200px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Quantity">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtQuantity" MaxLength="13" runat="server" Text='<%# Eval("PO_QUANTITY") %>'
                                TabIndex="15" CssClass="EntryFont RequiredField txtBox_N" Width="70px" AutoPostBack="True"
                                OnTextChanged="txtQuantity_TextChanged"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers"
                                TargetControlID="txtQuantity" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtQuantity" MaxLength="13" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                TabIndex="15" Width="70px" AutoPostBack="True" OnTextChanged="txtQuantity_TextChanged"></asp:TextBox><cc2:FilteredTextBoxExtender
                                    ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers" TargetControlID="txtQuantity" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblQuantity" runat="server" Text='<%# Eval("PO_QUANTITY") %>' Width="70px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Right" Width="50px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="UOM">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlUOM" runat="server" CssClass="RequiredField ddlStype" Width="150px"
                                TabIndex="16">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlUOM" runat="server" CssClass="RequiredField ddlStype" Width="150px"
                                TabIndex="16">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblUOM" runat="server" Text='<%# Eval("UOM_CODE") %>' Width="150px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Unit Price">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlUnitPrice" runat="server" CssClass="RequiredField ddlStype"
                                Visible="false" TabIndex="17" Width="150px">
                            </asp:DropDownList>
                            <asp:TextBox ID="txtUnitPrice" MaxLength="12" AutoPostBack="true" OnTextChanged="txtUnitPrice_TextChanged"
                                runat="server" Text='<%# Eval("PO_ITEM_UNIT_PRICE") %>' CssClass="EntryFont RequiredField txtBox_N"
                                Width="150px"></asp:TextBox><cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender21"
                                    runat="server" FilterType="Numbers,Custom" ValidChars="." TargetControlID="txtUnitPrice" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlUnitPrice" runat="server" CssClass="RequiredField ddlStype"
                                Visible="false" TabIndex="17" Width="150px">
                            </asp:DropDownList>
                            <asp:TextBox ID="txtUnitPrice" MaxLength="12" AutoPostBack="true" OnTextChanged="txtUnitPrice_TextChanged"
                                runat="server" CssClass="EntryFont RequiredField txtBox_N" Width="150px"></asp:TextBox><cc2:FilteredTextBoxExtender
                                    ID="FilteredTextBoxExtender21" runat="server" FilterType="Numbers,Custom" ValidChars="."
                                    TargetControlID="txtUnitPrice" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblUnitPrice" runat="server" Text='<%# Eval("PO_ITEM_UNIT_PRICE") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Amount">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtPOAmount" Enabled="false" Text='<%# Eval("PO_line_Amount") %>'
                                runat="server" CssClass="EntryFont RequiredField txtBox_N" Width="150px"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoddfxExtender21" runat="server" FilterType="Numbers,Custom"
                                ValidChars=".," TargetControlID="txtPOAmount" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtPOAmount" Enabled="false" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                Width="150px"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExhhghtender21" runat="server" FilterType="Numbers,Custom"
                                ValidChars=".," TargetControlID="txtPOAmount" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblAmount" runat="server" Text='<%# Eval("PO_line_Amount") %>' Width="150px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-Width="50px" ItemStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <asp:Button ID="btnScope" runat="server" CssClass="btn" Text="Scope" CommandName="btnPop"
                                Visible="false" TabIndex="12" OnClick="btnScope_Click" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:Button ID="btnScope" runat="server" CssClass="btn" Text="Scope" CommandName="btnPop"
                                Visible="false" TabIndex="12" OnClick="btnScope_Click" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:Button ID="btnScope" runat="server" CssClass="btn" Text="Scope" CommandName="btnPop"
                                Visible="false" TabIndex="12" OnClick="btnScope_Click" />
                        </FooterTemplate>
                        <FooterStyle VerticalAlign="Top" />
                        <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Right"></ItemStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" runat="server" AlternateText="Edit" CausesValidation="false"
                                TabIndex="18" ToolTip="Edit" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                            <asp:ImageButton ID="ibtnDelete" runat="server" AlternateText="Delete" CausesValidation="false"
                                TabIndex="19" ToolTip="Delete" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibtnUpdate" runat="server" AlternateText="Update" CommandName="Update"
                                TabIndex="20" ToolTip="Update" Height="20px" ImageUrl="~/Images/Update.png" />
                            <asp:ImageButton ID="ibtnCancel" runat="server" AlternateText="Cancel" CausesValidation="false"
                                TabIndex="21" ToolTip="Cancel" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="ibtnInsert" runat="server" AlternateText="Add" CommandName="FooterInsert"
                                TabIndex="22" ToolTip="Add" ImageUrl="~/Images/Add.jpg" />
                        </FooterTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer LNOrient">
            <div class="lblBox  LNOrient" style="width: 615px" id="lblRemarks">
                Remarks</div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer LNOrient">
            <div class="divtxtBox  LNOrient" style="width: 350px">
                <asp:TextBox ID="txtRemarks" runat="server" Width="350px" CssClass=" txtBox" Height="59px"
                    TabIndex="23" TextMode="MultiLine"></asp:TextBox>
            </div>
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" OnClick="btnDelete_Click" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table width="100%">
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
        <div id="div5">
            <asp:HiddenField runat="server" ID="hdRowIndex" />
            <asp:HiddenField runat="server" ID="hdItemId" />
            <asp:HiddenField runat="server" ID="HiddenField1" />
            <cc2:ModalPopupExtender ID="mpeScope" runat="server" TargetControlID="HiddenField1"
                PopupControlID="pnlpopup" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlpopup" runat="server" Width="650px" Height="400px">
                <div class="ConfirmForm" style="overflow: auto;">
                    <table>
                        <tr class="ConfirmLotHeading">
                            <td>
                                <div class="divRowContainer LNOrient" >
                                    <asp:GridView ID="gvLot" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                                        Width="100%" DataKeyNames="PO_SCOPE_ID,PO_LINE_ID,DELETED" OnRowCancelingEdit="gvLot_RowCancelingEdit"
                                        OnRowCommand="gvLot_RowCommand" OnRowCreated="gvLot_RowCreated" OnRowDataBound="gvLot_RowDataBound"
                                        OnRowDeleting="gvLot_RowDeleting" OnRowEditing="gvLot_RowEditing" OnRowUpdating="gvLot_RowUpdating"
                                        ShowFooter="True">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Scope Description">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtScope" TextMode="MultiLine" MaxLength="500" CssClass="validate[required]  RequiredField txtBox"
                                                        Width="400px" Height="100px" runat="server" TabIndex="1" Text='<%# Eval("PO_SCOPE_DESC") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <FooterTemplate>
                                                    <asp:TextBox ID="txtScope" TextMode="MultiLine" MaxLength="500" CssClass="validate[required]  RequiredField txtBox"
                                                        Width="400px" Height="100px" runat="server" TabIndex="1" Text=''></asp:TextBox>
                                                </FooterTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblScope" Width="400px" Text='<%# Eval("PO_SCOPE_DESC") %>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="ibtnEdit" runat="server" AlternateText="Edit" CausesValidation="false"
                                                        TabIndex="18" ToolTip="Edit" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                                                    <asp:ImageButton ID="ibtnDelete" runat="server" AlternateText="Delete" CausesValidation="false"
                                                        TabIndex="19" ToolTip="Delete" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:ImageButton ID="ibtnUpdate" runat="server" AlternateText="Update" CommandName="Update"
                                                        TabIndex="20" ToolTip="Update" Height="20px" ImageUrl="~/Images/Update.png" />
                                                    <asp:ImageButton ID="ibtnCancel" runat="server" AlternateText="Cancel" CausesValidation="false"
                                                        TabIndex="21" ToolTip="Cancel" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                                                </EditItemTemplate>
                                                <FooterTemplate>
                                                    <asp:ImageButton ID="ibtnInsert" runat="server" AlternateText="Add" CommandName="FooterInsert"
                                                        TabIndex="22" ToolTip="Add" ImageUrl="~/Images/Add.jpg" />
                                                </FooterTemplate>
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                        </Columns>
                                        <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                        <HeaderStyle CssClass="GridHeader" />
                                        <AlternatingRowStyle CssClass="GrdAltRow" />
                                    </asp:GridView>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblLotError" runat="server" Text="" Visible="false"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" CssClass="button" ID="btnClose" Text="Close" Width="60px"
                                    OnClick="btnClose_Click" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/AP/APChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">


        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });
    </script>
</asp:Content>
