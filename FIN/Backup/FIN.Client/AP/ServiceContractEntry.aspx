﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="ServiceContractEntry.aspx.cs" Inherits="FIN.Client.AP.ServiceContractEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1000px" id="divMainContainer">
        <table width="100%">
            <tr>
                <td>
                    <asp:Panel runat="server" ID="pnltdHeader">
                        <div class="divRowContainer">
                            <div class="lblBox LNOrient" style="width: 150px" id="Div3">
                                Contract Number
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 200px" id="lblInvoiceNoValue">
                                <asp:TextBox ID="txtContNumber" runat="server" CssClass="validate[required] RequiredField txtBox"
                                    Width="150px" MaxLength="50" TabIndex="1"></asp:TextBox>
                            </div>
                            <div class="colspace LNOrient">
                                &nbsp</div>
                            <div class="lblBox  LNOrient" style="width: 150px" id="lblInvoiceDate">
                                Contract Date
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 150px">
                                <asp:TextBox ID="txtContDate" runat="server" TabIndex="2" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]]  RequiredField txtBox"></asp:TextBox>
                                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calExtender1" TargetControlID="txtContDate"
                                    OnClientDateSelectionChanged="checkDate" />
                            </div>
                        </div>
                        <div class="divClear_10">
                        </div>
                        <div class="divRowContainer">
                            <div class="lblBox LNOrient" style="width: 150px" id="Div1">
                                Supplier Name
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 520px" id="Div2">
                                <asp:DropDownList ID="ddlSuppliername" runat="server" CssClass="validate[required] RequiredField ddlStype"
                                    TabIndex="3" AutoPostBack="True" OnSelectedIndexChanged="ddlSuppliername_SelectedIndexChanged">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="divClear_10">
                        </div>
                        <div class="divRowContainer">
                            <div class="lblBox LNOrient" style="width: 150px" id="Div5">
                                Supplier Site
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 520px">
                                <asp:DropDownList ID="ddlSupplierSite" runat="server" CssClass="validate[required] RequiredField ddlStype"
                                    TabIndex="4">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="divClear_10">
                        </div>
                        <div class="divRowContainer">
                            <div class="lblBox LNOrient" style="width: 150px" id="Div4">
                                Contract Type
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 200px">
                                <asp:DropDownList ID="ddlContType" runat="server" CssClass="validate[required] RequiredField ddlStype"
                                    Width="150px" TabIndex="5">
                                </asp:DropDownList>
                            </div>
                            <div class="colspace LNOrient">
                                &nbsp</div>
                            <div class="lblBox LNOrient" style="width: 150px" id="Div6">
                                Payment Type
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 150px">
                                <asp:DropDownList ID="ddlPayType" runat="server" CssClass="validate[required] RequiredField ddlStype"
                                    TabIndex="6" AutoPostBack="True" OnSelectedIndexChanged="ddlPayType_SelectedIndexChanged">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="divClear_10">
                        </div>
                        <div class="divRowContainer">
                            <div class="lblBox LNOrient" style="width: 150px" id="Div7">
                                Contract From
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 200px" id="Div8">
                                <asp:TextBox ID="txtContFrom" runat="server" TabIndex="7" Width="150px" CssClass="validate[required, custom[ReqDateDDMMYYY],dateRange[dg1]]  RequiredField  txtBox"></asp:TextBox>
                                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender2" TargetControlID="txtContFrom"
                                    OnClientDateSelectionChanged="checkDate" />
                            </div>
                            <div class="colspace LNOrient">
                                &nbsp</div>
                            <div class="lblBox LNOrient" style="width: 150px" id="Div9">
                                Contract To
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 150px">
                                <asp:TextBox ID="txtContTo" runat="server" TabIndex="8" CssClass="validate[required, custom[ReqDateDDMMYYY],dateRange[dg1]]  RequiredField  txtBox"></asp:TextBox>
                                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtContTo"
                                    OnClientDateSelectionChanged="checkDate" />
                            </div>
                        </div>
                        <div class="divClear_10">
                        </div>
                        <div class="divRowContainer">
                            <div class="lblBox LNOrient" style="width: 150px" id="Div10">
                                Contract Value
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 200px">
                                <asp:TextBox ID="txtContValue" runat="server" MaxLength="13" Width="150px" TabIndex="9"
                                    CssClass="validate[required]  RequiredField txtBox_N"></asp:TextBox>
                                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtsdfender7" runat="server" ValidChars=".,"
                                    FilterType="Numbers,Custom" TargetControlID="txtContValue" />
                            </div>
                            <div class="colspace LNOrient">
                                &nbsp</div>
                        </div>
                        <div class="divClear_10">
                        </div>
                        <div class="divRowContainer">
                            <div class="lblBox LNOrient" style="width: 150px" id="Div11">
                                Remarks
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 530px">
                                <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine" Height="50px" TabIndex="10"
                                    MaxLength="240" CssClass="txtBox"></asp:TextBox>
                            </div>
                        </div>
                        <div class="divClear_10">
                        </div>
                    </asp:Panel>
                </td>
                <td valign="top">
                    <table>
                        <tr>
                            <td>
                                <asp:ImageButton ID="btnPrint" runat="server" ImageUrl="../Images/Print.png" OnClick="btnPrint_Click"
                                    Style="border: 0px" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <div class=" LNOrient">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                DataKeyNames="serv_contr_dtl_id,DELETED" OnRowCancelingEdit="gvData_RowCancelingEdit"
                OnRowCommand="gvData_RowCommand" OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound"
                OnRowDeleting="gvData_RowDeleting" OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating"
                ShowFooter="True">
                <Columns>
                    <asp:TemplateField HeaderText="From Date">
                        <EditItemTemplate>
                            <asp:TextBox ID="dtpFromDate" MaxLength="10" runat="server" CssClass="RequiredField EntryFont  txtBox"
                                TabIndex="11" Text='<%#  Eval("SERV_CONTR_DTL_FROM_DATE","{0:dd/MM/yyyy}") %>'
                                Width="130px" Onkeypress="return DateKeyCheck(event,this);"></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender8" runat="server" Format="dd/MM/yyyy" TargetControlID="dtpFromDate">
                            </cc2:CalendarExtender>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" ValidChars="/"
                                FilterType="Numbers,Custom" TargetControlID="dtpFromDate" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="dtpFromDate" TabIndex="12" MaxLength="10" runat="server" CssClass="RequiredField EntryFont  txtBox"
                                Onkeypress="return DateKeyCheck(event,this);" Width="130px"></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender9" runat="server" Format="dd/MM/yyyy" TargetControlID="dtpFromDate">
                            </cc2:CalendarExtender>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" ValidChars="/"
                                FilterType="Numbers,Custom" TargetControlID="dtpFromDate" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblFromDate" Width="100px" runat="server" Text='<%# Eval("SERV_CONTR_DTL_FROM_DATE","{0:dd/MM/yyyy}") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="To Date">
                        <EditItemTemplate>
                            <asp:TextBox ID="dtpToDate" MaxLength="10" runat="server" CssClass="RequiredField EntryFont  txtBox"
                                TabIndex="13" Text='<%#  Eval("SERV_CONTR_DTL_TO_DATE","{0:dd/MM/yyyy}") %>'
                                Width="130px" Onkeypress="return DateKeyCheck(event,this);"></asp:TextBox>
                            <cc2:CalendarExtender ID="dtpToDate8" runat="server" Format="dd/MM/yyyy" TargetControlID="dtpToDate">
                            </cc2:CalendarExtender>
                            <cc2:FilteredTextBoxExtender ID="dtpToDate7" runat="server" ValidChars="/" FilterType="Numbers,Custom"
                                TargetControlID="dtpToDate" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="dtpToDate" TabIndex="14" MaxLength="10" runat="server" CssClass="RequiredField EntryFont  txtBox"
                                Onkeypress="return DateKeyCheck(event,this);" Width="130px"></asp:TextBox>
                            <cc2:CalendarExtender ID="dtpToDater9" runat="server" Format="dd/MM/yyyy" TargetControlID="dtpToDate">
                            </cc2:CalendarExtender>
                            <cc2:FilteredTextBoxExtender ID="dtpToDater8" runat="server" ValidChars="/" FilterType="Numbers,Custom"
                                TargetControlID="dtpToDate" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblToDate" Width="100px" runat="server" Text='<%# Eval("SERV_CONTR_DTL_TO_DATE","{0:dd/MM/yyyy}") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Percentage">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtPerCentage" Width="130px" MaxLength="4" runat="server" CssClass=" RequiredField  txtBox_N"
                                Text='<%# Eval("SERV_CONTR_PERCENTAGE") %>' TabIndex="15"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FTEtxtdPerCentage1" runat="server" ValidChars="%"
                                FilterType="Numbers" TargetControlID="txtPerCentage" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtPerCentage" TabIndex="16" Width="130px" MaxLength="4" runat="server"
                                CssClass="RequiredField   txtBox_N"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FTEtxtPersCentage2" runat="server" ValidChars="%"
                                FilterType="Numbers" TargetControlID="txtPerCentage" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblPercentage" Width="100px" runat="server" Text='<%# Eval("SERV_CONTR_PERCENTAGE") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Days">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtdays" Width="130px" MaxLength="10" runat="server" CssClass=" RequiredField  txtBox_N"
                                Text='<%# Eval("SERV_CONTR_DTL_DAYS") %>' TabIndex="17"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers"
                                TargetControlID="txtdays" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtdays" TabIndex="18" Width="130px" MaxLength="10" runat="server"
                                CssClass="RequiredField   txtBox_N"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtejknder1" runat="server" FilterType="Numbers"
                                TargetControlID="txtdays" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblDays" Width="100px" runat="server" Text='<%# Eval("SERV_CONTR_DTL_DAYS") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Generate Invoice">
                        <EditItemTemplate>
                            <asp:Button ID="btnGenerateInvoice" runat="server" Text="Generate Invoice" Enabled="false"
                                OnClick="btnGenerateInvoice_Click" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:Button ID="btnGenerateInvoice" runat="server" Text="Generate Invoice" Enabled="false" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Button ID="btnGenerateInvoice" runat="server" Text="Generate Invoice" Enabled="false" />
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Invoice Status">
                        <EditItemTemplate>
                            <asp:Label ID="lblInvStatus" Width="130px" runat="server" Text='<%# Eval("SERV_CONTR_INV_STATUS") %>'></asp:Label>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="lblInvStatus" Width="130px" runat="server" Text='Pending'></asp:Label>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblInvStatus" Width="130px" runat="server" Text='<%# Eval("SERV_CONTR_INV_STATUS") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Payment Status">
                        <EditItemTemplate>
                            <asp:Label ID="lblPayStatus" Width="130px" runat="server" Text='<%# Eval("SERV_CONTR_PAY_STATUS") %>'></asp:Label>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="lblPayStatus" Width="130px" runat="server" Text='Pending'></asp:Label>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblPayStatus" Width="130px" runat="server" Text='<%# Eval("SERV_CONTR_PAY_STATUS") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" TabIndex="19" runat="server" AlternateText="Edit"
                                CausesValidation="false" ToolTip="Edit" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                            <asp:ImageButton ID="ibtnDelete" TabIndex="20" runat="server" AlternateText="Delete"
                                CausesValidation="false" ToolTip="Delete" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibtnUpdate" TabIndex="21" runat="server" AlternateText="Update"
                                CommandName="Update" ToolTip="Update" Height="20px" ImageUrl="~/Images/Update.png" />
                            <asp:ImageButton ID="ibtnCancel" TabIndex="22" runat="server" AlternateText="Cancel"
                                CausesValidation="false" ToolTip="Cancel" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="ibtnInsert" TabIndex="23" runat="server" AlternateText="Add"
                                CommandName="FooterInsert" ToolTip="Add" ImageUrl="~/Images/Add.jpg" />
                        </FooterTemplate>
                        <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                            TabIndex="17" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="24" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="25" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="26" />
                    </td>
                </tr>
            </table>
        </div>
        <div class=" LNOrient" id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table>
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/AP/APChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
