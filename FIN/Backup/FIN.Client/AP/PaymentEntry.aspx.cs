﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.DAL.GL;
using FIN.DAL.AP;
using FIN.DAL.CA;
using FIN.BLL.GL;
using FIN.BLL;
using FIN.BLL.AP;
using FIN.BLL.CA;
using VMVServices.Web;
using VMVServices.Services.Data;
using System.Data;
using System.Data.SqlClient;

namespace FIN.Client.AP
{
    public partial class PaymentEntry : PageBase
    {
        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();
        INV_PAYMENTS_HDR INV_PAYMENTS_HDR = new INV_PAYMENTS_HDR();
        INV_PAYMENTS_DTL INV_PAYMENTS_DTL = new INV_PAYMENTS_DTL();

        DataTable dtGridData = new DataTable();
        Boolean bol_rowVisiable;
        Boolean savedBool;
        Boolean stopPayBool;
        string ProReturn = null;
        string trnType = string.Empty;
        int trnTypeBool = 0;

        bool lineNum = false;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                if (!IsPostBack)
                {
                    Session["PAY_OC"] = null;
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WRI_PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));

            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
        }

        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }
        #endregion


        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void FillComboBox()
        {
            object s = new object();
            EventArgs e = new EventArgs();
            //Supplier_BLL.GetSupplierNumber_Name(ref ddlSupplierNumber);
            Supplier_BLL.GetSupplierNumber(ref ddlSupplierNumber);
            Lookup_BLL.GetLookUpValues(ref ddlPaymentMode, "PAYMENT_MODE");
            Bank_BLL.fn_getBankName(ref ddlBankName);
            // Segments_BLL.GetGlobalSegmentvalues(ref ddlGlobalSegment, VMVServices.Web.Utils.OrganizationID);
            FIN.BLL.GL.Segments_BLL.GetGlobalSegmentvaluesByOrgName(ref ddlGlobalSegment);
            ddlGlobalSegment.Items.RemoveAt(0);
            AccountCodes_BLL.getAccCodeBasedOrg(ref  ddlAccountCode, VMVServices.Web.Utils.OrganizationID);


            Currency_BLL.GetPaymentCurrency(ref ddlPaymentCurrency);
            ddlPaymentCurrency.SelectedValue = Session[FINSessionConstants.ORGCurrency].ToString();
            ddlPaymentCurrency_SelectedIndexChanged(s, e);
        }
        protected void ddlSupplierNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            //txtSupplierName.Text = ddlSupplierNumber.SelectedValue.ToString();
            txtSupplierName.Text = ddlSupplierNumber.SelectedItem.Text.ToString();
            GridViewRow gvr = gvData.FooterRow;
            FillFooterGridCombo(gvr);
        }
        protected void ddlBankName_SelectedIndexChanged(object sender, EventArgs e)
        {
            BankBranch_BLL.fn_getBranchName(ref ddlBankBranch, ddlBankName.SelectedValue.ToString());
        }
        protected void ddlBankBranch_SelectedIndexChanged(object sender, EventArgs e)
        {
            BankAccounts_BLL.fn_getBankAccount(ref ddlAccountnumber, ddlBankName.SelectedValue, ddlBankBranch.SelectedValue);
        }
        protected void ddlAccountnumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlPaymentMode.SelectedValue.ToString().ToUpper() != "EFT")
            {
                Cheque_BLL.GetUnusedCheckNumberDetails(ref ddlChequeNumber, ddlBankName.SelectedValue, ddlBankBranch.SelectedValue, ddlAccountnumber.SelectedValue, Master.Mode);
                if (ddlChequeNumber.Items.Count > 1)
                {
                    ddlChequeNumber.SelectedIndex = 1;
                    //ddlChequeNumber.Enabled = false;
                }
            }

        }
        protected void ddlInvoiceNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            DropDownList ddlInvoiceNumber = gvr.FindControl("ddlInvoiceNumber") as DropDownList;
            TextBox txtInvoiceDate = gvr.FindControl("txtInvoiceDate") as TextBox;
            TextBox txtInvoiceAmount = gvr.FindControl("txtInvoiceAmount") as TextBox;
            TextBox txtRetentionAmount = gvr.FindControl("txtRetentionAmount") as TextBox;
            TextBox txt_AmountPaid = gvr.FindControl("txtAmountPaid") as TextBox;
            TextBox txt_AmountToPay = gvr.FindControl("txtAmountToPay") as TextBox;
            TextBox txt_PayRetentionAmount = gvr.FindControl("txtPayRetentionAmount") as TextBox;

            TextBox txt_PaymentAmount = (TextBox)gvr.FindControl("txtPaymentAmount");

            txt_PayRetentionAmount.Enabled = false;
            txt_PaymentAmount.Text = "";
            txtInvoiceDate.Text = "";
            txtInvoiceAmount.Text = "";
            txtRetentionAmount.Text = "";
            txt_AmountPaid.Text = "";
            txt_AmountToPay.Text = "";
            txt_PayRetentionAmount.Text = "";

            DataTable dtData = new DataTable();

            Session["INVNUM"] = ddlInvoiceNumber.SelectedValue;
            dtData = DBMethod.ExecuteQuery(Invoice_DAL.getInvoiceHeaderDetails(ddlInvoiceNumber.SelectedValue)).Tables[0];
            if (dtData != null)
            {
                if (dtData.Rows.Count > 0)
                {
                    txtInvoiceDate.Text = DBMethod.ConvertDateToString(dtData.Rows[0][FINColumnConstants.INV_DATE].ToString());
                    txtInvoiceAmount.Text = (dtData.Rows[0][FINColumnConstants.INV_AMT].ToString());
                    if (dtData.Rows[0]["INV_RETENTION_AMT"].ToString().Trim().Length > 0)
                    {
                        txtRetentionAmount.Text = (dtData.Rows[0]["INV_RETENTION_AMT"].ToString());
                        if (CommonUtils.ConvertStringToDecimal(txtRetentionAmount.Text) > 0)
                            txt_PayRetentionAmount.Enabled = true;

                    }
                    DataTable dt_totalPayment = DBMethod.ExecuteQuery(Payment_DAL.getTotalPaymentAmt4Invoice(ddlInvoiceNumber.SelectedValue)).Tables[0];
                    txt_AmountPaid.Text = dt_totalPayment.Rows[0][0].ToString();
                    txt_AmountToPay.Text = (CommonUtils.ConvertStringToDecimal(txtInvoiceAmount.Text)  - CommonUtils.ConvertStringToDecimal(txt_AmountPaid.Text)).ToString();


                    if (CommonUtils.ConvertStringToDecimal(txt_AmountToPay.Text) > 0)
                    {
                        txt_AmountToPay.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txt_AmountToPay.Text);
                    }
                    if (CommonUtils.ConvertStringToDecimal(txt_AmountPaid.Text) > 0)
                    {
                        txt_AmountPaid.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txt_AmountPaid.Text);
                    }

                    if (CommonUtils.ConvertStringToDecimal(txtRetentionAmount.Text) > 0)
                    {
                        txtRetentionAmount.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtRetentionAmount.Text);
                    }
                    if (CommonUtils.ConvertStringToDecimal(txtInvoiceAmount.Text) > 0)
                    {
                        txtInvoiceAmount.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtInvoiceAmount.Text);
                    }
                }
            }

        }
        protected void txtPayRetentionAmount_TextChanged(object sender, EventArgs e)
        {
        }

        protected void txtPaymentAmount_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
                TextBox txtAmountPaid = (TextBox)gvr.FindControl("txtAmountPaid");
                TextBox txtAmountToPay = (TextBox)gvr.FindControl("txtAmountToPay");
                TextBox txtRetentionAmount = (TextBox)gvr.FindControl("txtRetentionAmount");
                TextBox txtInvoiceAmount = (TextBox)gvr.FindControl("txtInvoiceAmount");
                TextBox txt_PaymentAmount = (TextBox)gvr.FindControl("txtPaymentAmount");

                ErrorCollection.Remove("InvalidAmt");
                if (CommonUtils.ConvertStringToDecimal(txt_PaymentAmount.Text) > (CommonUtils.ConvertStringToDecimal(txtInvoiceAmount.Text) - CommonUtils.ConvertStringToDecimal(txtRetentionAmount.Text) - CommonUtils.ConvertStringToDecimal(txtAmountPaid.Text)))
                {
                    ErrorCollection.Add("InvalidAmt", "Payment amount must be less than the balance amount");
                    //txt_PaymentAmount.Text = "";
                    //txt_PaymentAmount.Focus();
                    return;
                }
                else
                {
                    if (CommonUtils.ConvertStringToDecimal(txtInvoiceAmount.Text) > 0 && CommonUtils.ConvertStringToDecimal(txt_PaymentAmount.Text) > 0)
                    {
                        txtAmountToPay.Text = (CommonUtils.ConvertStringToDecimal(txtInvoiceAmount.Text) - CommonUtils.ConvertStringToDecimal(txtRetentionAmount.Text) - CommonUtils.ConvertStringToDecimal(txtAmountPaid.Text) - CommonUtils.ConvertStringToDecimal(txt_PaymentAmount.Text)).ToString();
                    }
                }
                if (CommonUtils.ConvertStringToDecimal(txtAmountToPay.Text) > 0)
                {
                    txtAmountToPay.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtAmountToPay.Text);
                }
                if (CommonUtils.ConvertStringToDecimal(txt_PaymentAmount.Text) > 0)
                {
                    txt_PaymentAmount.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txt_PaymentAmount.Text);
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void PaymentCtrlEnableDisable()
        {
            Bank_BLL.fn_getBankName(ref ddlBankName);
            //BankBranch_BLL.fn_getBranchName(ref ddlBankBranch, "0");
            //BankAccounts_BLL.fn_getBankAccount(ref ddlAccountnumber, "0", "0");
            //Cheque_BLL.fn_getChequeNumber(ref ddlChequeNumber, "0", "0", "0");


            if (ddlPaymentMode.SelectedItem.Text == "Cash")
            {
                ddlBankName.Enabled = false;
                ddlBankBranch.Enabled = false;
                ddlAccountnumber.Enabled = false;

                ddlChequeNumber.Enabled = false;
                txtChequeDate.Enabled = false;
                txtChequeReceivedBy.Enabled = false;
                txtCivilId.Enabled = false;
            }
            else if (ddlPaymentMode.SelectedItem.Text == "Cheque")
            {
                ddlBankName.Enabled = true;
                ddlBankBranch.Enabled = true;
                ddlAccountnumber.Enabled = true;
                txtChequeReceivedBy.Enabled = true;
                txtCivilId.Enabled = true;

                ddlChequeNumber.Enabled = true;
                txtChequeDate.Enabled = true;
            }
            else if (ddlPaymentMode.SelectedItem.Text == "EFT")
            {
                ddlBankName.Enabled = true;
                ddlBankBranch.Enabled = true;
                ddlAccountnumber.Enabled = true;

                txtChequeReceivedBy.Enabled = false;
                txtCivilId.Enabled = false;
                ddlChequeNumber.Enabled = false;
                txtChequeDate.Enabled = false;
            }
        }
        protected void ddlPaymentMode_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                PaymentCtrlEnableDisable();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("wri_ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        protected void ddlChequeNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            //DataTable dtData = new DataTable();

            //dtData = DBMethod.ExecuteQuery(Cheque_DAL.GetCheckNumberDetails(ddlChequeNumber.SelectedValue)).Tables[0];
            //if (dtData != null)
            //{
            //    if (dtData.Rows.Count > 0)
            //    {
            //        txtChequeDate.Text = dtData.Rows[0][FINColumnConstants.CHECK_DT].ToString();
            //    }
            //}
            if (ddlChequeNumber.SelectedValue != null)
            {
                txtChequeDate.Enabled = true;
            }

        }
        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();

                FillComboBox();


                Session["InvoiceProgramID"] = null;
                Session["InvoiceMode"] = null;
                Session["InvoiceID"] = null;
                Session["StopPayment"] = null;


                ddlBankName.Enabled = false;
                ddlBankBranch.Enabled = false;
                ddlAccountnumber.Enabled = false;

                ddlChequeNumber.Enabled = false;
                txtChequeDate.Enabled = false;
                txtChequeReceivedBy.Enabled = false;
                txtCivilId.Enabled = false;

                EntityData = null;
                Session[FINSessionConstants.GridData] = null;
                txtPaymentDate.Text = DBMethod.ConvertDateToString(DateTime.Now.ToShortDateString());

                dtGridData = DBMethod.ExecuteQuery(Payment_DAL.getPaymentDetails(Master.StrRecordId)).Tables[0];

                BindGrid(dtGridData);

                DataTable dt_PAY_OC = DBMethod.ExecuteQuery(Payment_DAL.getPaymentOtherCharges(Master.StrRecordId)).Tables[0];
                BindGrid_OC(dt_PAY_OC);

                imgBtnPost.Visible = false;
                btnChequePrint.Visible = false;
                btnStopPayment.Visible = false;
                btnPrint.Visible = false;
                imgBtnJVPrint.Visible = false;
                imgBtnStopPayJV.Visible = false;
                imgbtnPaymentCancel.Visible = false;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    INV_PAYMENTS_HDR = Payment_BLL.getClassEntity(Master.StrRecordId);

                    EntityData = INV_PAYMENTS_HDR;
                    imgbtnPaymentCancel.Visible = true ;


                    btnStopPayment.Visible = true;
                    btnPrint.Visible = true;
                    txtPaymentNumber.Text = INV_PAYMENTS_HDR.PAY_ID;

                    if (INV_PAYMENTS_HDR.PAY_DATE != null)
                    {
                        txtPaymentDate.Text = DBMethod.ConvertDateToString(INV_PAYMENTS_HDR.PAY_DATE.ToString());
                    }
                    // Supplier_BLL.GetSupplierNumber_Name(ref ddlSupplierNumber);

                    ddlSupplierNumber.SelectedValue = INV_PAYMENTS_HDR.PAY_VENDOR_ID.ToString();

                    if (INV_PAYMENTS_HDR.ATTRIBUTE1 != null)
                    {
                        //ddlSupplierNumber.SelectedValue = INV_PAYMENTS_HDR.ATTRIBUTE1.ToString();
                        //ddlSupplierNumber.SelectedItem.Text = INV_PAYMENTS_HDR.ATTRIBUTE1.ToString();
                        txtSupplierName.Text = INV_PAYMENTS_HDR.ATTRIBUTE1;
                    }

                    ddlPaymentMode.SelectedValue = INV_PAYMENTS_HDR.PAY_MODE;

                    ddlBankName.SelectedValue = INV_PAYMENTS_HDR.PAY_BANK_ID;
                    BankBranch_BLL.fn_getBranchName(ref ddlBankBranch, ddlBankName.SelectedValue.ToString());

                    ddlBankBranch.SelectedValue = INV_PAYMENTS_HDR.PAY_BANK_BRANCH_ID;
                    BankAccounts_BLL.fn_getBankAccount(ref ddlAccountnumber, ddlBankName.SelectedValue, ddlBankBranch.SelectedValue);

                    ddlAccountnumber.SelectedValue = INV_PAYMENTS_HDR.PAY_ACCOUNT_ID;
                    Cheque_BLL.fn_getChequeNumber(ref ddlChequeNumber, ddlBankName.SelectedValue, ddlBankBranch.SelectedValue, ddlAccountnumber.SelectedValue);


                    if (INV_PAYMENTS_HDR.PAY_CHECK_NUMBER != null)
                    {
                        ddlChequeNumber.SelectedValue = INV_PAYMENTS_HDR.PAY_CHECK_NUMBER;
                    }
                    if (INV_PAYMENTS_HDR.PAY_CHECK_DATE != null)
                    {
                        txtChequeDate.Text = DBMethod.ConvertDateToString(INV_PAYMENTS_HDR.PAY_CHECK_DATE.ToString());
                    }

                    txtCivilId.Text = INV_PAYMENTS_HDR.PAY_CHECK_RECEV_CIVIL_ID;
                    //txtTotalPaymentAmt.Text = INV_PAYMENTS_HDR.PAYMENT_AMT.ToString();
                    txtTotalPaymentAmt.Text = DBMethod.GetAmtDecimalCommaSeparationValue(INV_PAYMENTS_HDR.PAYMENT_AMT.ToString());

                    if (INV_PAYMENTS_HDR.GLOBAL_SEGMENT_ID != null)
                    {
                        ddlGlobalSegment.SelectedValue = INV_PAYMENTS_HDR.GLOBAL_SEGMENT_ID.ToString();
                    }
                    if (INV_PAYMENTS_HDR.EXC_RATE_TYPE_ID != null)
                    {
                        ddlExchangeRateType.SelectedValue = INV_PAYMENTS_HDR.EXC_RATE_TYPE_ID.ToString();
                    }
                    if (INV_PAYMENTS_HDR.EXC_RATE_VALUE != null)
                    {
                        txtExchangeRate.Text = INV_PAYMENTS_HDR.EXC_RATE_VALUE.ToString();
                    }
                    if (INV_PAYMENTS_HDR.PAYMENT_CURRENCY != null)
                    {
                        ddlPaymentCurrency.SelectedValue = INV_PAYMENTS_HDR.PAYMENT_CURRENCY.ToString();
                    }
                    if (INV_PAYMENTS_HDR.PAY_CHECK_RECEIVED_BY != null)
                    {
                        txtChequeReceivedBy.Text = INV_PAYMENTS_HDR.PAY_CHECK_RECEIVED_BY.ToString();
                        if (INV_PAYMENTS_HDR.PAY_CHECK_RECEIVED_BY.ToString().Length > 0)
                        {
                            txtChequeReceivedBy.Enabled = false;
                            txtCivilId.Enabled = false;

                            CA_CHECK_DTL cA_CHECK_DTL = new CA_CHECK_DTL();
                            using (IRepository<CA_CHECK_DTL> userCtx = new DataRepository<CA_CHECK_DTL>())
                            {
                                cA_CHECK_DTL = userCtx.Find(r =>
                                    (r.CHECK_DTL_ID == ddlChequeNumber.SelectedValue.ToString())
                                    ).SingleOrDefault();
                            }
                            if (cA_CHECK_DTL != null)
                            {
                                if (cA_CHECK_DTL.CHECK_STATUS.ToString() == "USED")
                                {
                                    btnStopPayment.Enabled = false;
                                }
                            }
                        }
                    }
                    PaymentCtrlEnableDisable();
                    ddlChequeNumber.Enabled = false;

                    if (INV_PAYMENTS_HDR.WORKFLOW_COMPLETION_STATUS == "1")
                    {
                        btnChequePrint.Visible = true;
                        imgBtnPost.Visible = true;
                        lblPosted.Visible = false;
                        lblCancelled.Visible = false;
                        btnStopPayment.Visible = false;
                    }

                    if (INV_PAYMENTS_HDR.REV_FLAG != null)
                    {
                        if (INV_PAYMENTS_HDR.REV_FLAG.ToString() == FINAppConstants.Y)
                        {
                            btnSave.Visible = false;
                            imgBtnPost.Visible = false;
                            imgbtnPaymentCancel.Visible = false;
                            btnChequePrint.Visible = false;
                            lblCancelled.Visible = true;
                        }
                    }
                    btnStopPayment.Visible = false;
                    if (INV_PAYMENTS_HDR.POSTED_FLAG != null)
                    {
                        if (INV_PAYMENTS_HDR.POSTED_FLAG.Trim() == FINAppConstants.Y)
                        {

                            btnSave.Visible = false;
                            imgBtnPost.Visible = false;
                            imgbtnPaymentCancel.Visible = false;
                            imgBtnJVPrint.Visible = true;
                            btnStopPayment.Visible = true;
                            lblPosted.Visible = true;
                            lblCancelled.Visible = false;

                            pnlgridview.Enabled = false;
                            pnltdHeader.Enabled = false;
                            pnlAcc.Enabled = false;
                        }
                    }
                 

                    if (INV_PAYMENTS_HDR.REV_JE_HDR_ID != null)
                    {
                        if (INV_PAYMENTS_HDR.REV_JE_HDR_ID.ToString().Length > 0)
                        {
                            lblCancelled.Visible = true;
                            btnStopPayment.Visible = false;
                            imgBtnStopPayJV.Visible = true;
                        }
                    }


                    if (FINSP.Is_workflow_defined("AP_022"))
                    {
                        if (INV_PAYMENTS_HDR.WORKFLOW_COMPLETION_STATUS == "0")
                        {
                            lblWaitApprove.Visible = true;
                        }
                        else
                        {
                            lblWaitApprove.Visible = false;
                        }
                    }
                    else
                    {
                        lblWaitApprove.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("wri_ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>
        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();

                if (EntityData != null)
                {
                    INV_PAYMENTS_HDR = (INV_PAYMENTS_HDR)EntityData;
                }
                if (txtPaymentDate.Text != string.Empty)
                {
                    INV_PAYMENTS_HDR.PAY_DATE = DBMethod.ConvertStringToDate(txtPaymentDate.Text.ToString());
                }

                //INV_PAYMENTS_HDR.PAY_VENDOR_ID = ddlSupplierNumber.SelectedItem.Text;
                INV_PAYMENTS_HDR.PAY_VENDOR_ID = ddlSupplierNumber.SelectedValue;
                INV_PAYMENTS_HDR.ATTRIBUTE1 = txtSupplierName.Text;
                INV_PAYMENTS_HDR.PAY_MODE = ddlPaymentMode.SelectedValue;
                INV_PAYMENTS_HDR.PAY_BANK_ID = ddlBankName.SelectedValue;
                INV_PAYMENTS_HDR.PAY_BANK_BRANCH_ID = ddlBankBranch.SelectedValue;
                INV_PAYMENTS_HDR.PAY_ACCOUNT_ID = ddlAccountnumber.SelectedValue;
                INV_PAYMENTS_HDR.PAY_CHECK_NUMBER = ddlChequeNumber.SelectedValue;
                INV_PAYMENTS_HDR.PAY_CHECK_RECEV_CIVIL_ID = txtCivilId.Text;
                if (txtChequeDate.Text != string.Empty)
                {
                    INV_PAYMENTS_HDR.PAY_CHECK_DATE = DBMethod.ConvertStringToDate(txtChequeDate.Text.ToString());
                }

                INV_PAYMENTS_HDR.PAYMENT_AMT = CommonUtils.ConvertStringToDecimal(txtTotalPaymentAmt.Text);
                INV_PAYMENTS_HDR.PAY_CHECK_RECEIVED_BY = txtChequeReceivedBy.Text;

                INV_PAYMENTS_HDR.PAYMENT_CURRENCY = ddlPaymentCurrency.SelectedValue;
                INV_PAYMENTS_HDR.EXC_RATE_TYPE_ID = ddlExchangeRateType.SelectedValue;
                INV_PAYMENTS_HDR.EXC_RATE_VALUE = CommonUtils.ConvertStringToDecimal(txtExchangeRate.Text.ToString());


                INV_PAYMENTS_HDR.ORG_ID = VMVServices.Web.Utils.OrganizationID;

                INV_PAYMENTS_HDR.ENABLED_FLAG = FINAppConstants.Y;
                //INV_PAYMENTS_HDR.WORKFLOW_COMPLETION_STATUS = FINAppConstants.Y;

                INV_PAYMENTS_HDR.GLOBAL_SEGMENT_ID = ddlGlobalSegment.SelectedValue;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    INV_PAYMENTS_HDR.MODIFIED_BY = this.LoggedUserName;
                    INV_PAYMENTS_HDR.MODIFIED_DATE = DateTime.Today;

                    if (Session["StopPayment"] != null || Session["StopPayment"] != string.Empty)
                    {
                        if (txtStopDate.Text != string.Empty)
                        {
                            INV_PAYMENTS_HDR.PAY_STOP_DATE = DBMethod.ConvertStringToDate(txtStopDate.Text.ToString());
                        }
                        INV_PAYMENTS_HDR.PAY_STOP_REASON = txtStopPaymentReason.Text;
                        INV_PAYMENTS_HDR.PAY_STOP_REMARKS = txtStopPayRemarks.Text;

                    }
                }
                else
                {
                    INV_PAYMENTS_HDR.PAY_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.AP_022_M.ToString(), false, true);
                    INV_PAYMENTS_HDR.POSTED_FLAG = FINAppConstants.N;
                    INV_PAYMENTS_HDR.CREATED_BY = this.LoggedUserName;
                    INV_PAYMENTS_HDR.CREATED_DATE = DateTime.Today;
                }

                INV_PAYMENTS_HDR.WORKFLOW_COMPLETION_STATUS = "0";

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                ErrorCollection = CommonUtils.IsEmptyGrid(dtGridData, "Payment ");
                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                var tmpChildEntity = new List<Tuple<object, string>>();


                string str_Cur_id = "";
                if (Session[FINSessionConstants.ORGCurrency].ToString() == ddlPaymentCurrency.SelectedValue)
                {
                    for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                    {
                        if (dtGridData.Rows[iLoop]["DELETED"].ToString() != FINAppConstants.Y)
                        {
                            DataTable dtData = DBMethod.ExecuteQuery(Invoice_DAL.getInvoiceHeaderDetails(dtGridData.Rows[iLoop][FINColumnConstants.PAY_INVOICE_ID].ToString())).Tables[0];
                            if (dtData.Rows.Count > 0)
                            {


                                if (dtData.Rows[0]["INV_PAY_CURR_CODE"].ToString() != ddlPaymentCurrency.SelectedValue)
                                {
                                    if (str_Cur_id.Length == 0)
                                    {
                                        str_Cur_id = dtData.Rows[0]["INV_PAY_CURR_CODE"].ToString();
                                    }
                                    else
                                    {
                                        if (dtData.Rows[0]["INV_PAY_CURR_CODE"].ToString() != str_Cur_id)
                                        {
                                            ErrorCollection.Add("ONLY ONE OTHER CURRENCY IS ALLOWED", "Only One Other Currency Invoice Allowed");
                                            return;
                                        }
                                    }
                                }
                            }


                        }
                    }
                }

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    INV_PAYMENTS_DTL = new INV_PAYMENTS_DTL();

                    if ((dtGridData.Rows[iLoop]["PAY_DTL_ID"].ToString()) != "0" && dtGridData.Rows[iLoop]["PAY_DTL_ID"].ToString() != string.Empty)
                    {
                        using (IRepository<INV_PAYMENTS_DTL> userCtx = new DataRepository<INV_PAYMENTS_DTL>())
                        {
                            INV_PAYMENTS_DTL = userCtx.Find(r =>
                                (r.PAY_DTL_ID == (dtGridData.Rows[iLoop]["PAY_DTL_ID"].ToString()))
                                ).SingleOrDefault();
                        }
                    }

                    INV_PAYMENTS_DTL.PAY_ID = INV_PAYMENTS_HDR.PAY_ID;
                    INV_PAYMENTS_DTL.ATTRIBUTE1 = (iLoop + 1).ToString();

                    INV_PAYMENTS_DTL.PAY_INVOICE_ID = dtGridData.Rows[iLoop][FINColumnConstants.PAY_INVOICE_ID].ToString();

                    INV_PAYMENTS_DTL.ATTRIBUTE2 = dtGridData.Rows[iLoop]["inv_date"].ToString();
                    INV_PAYMENTS_DTL.ATTRIBUTE3 = dtGridData.Rows[iLoop]["inv_amt"].ToString();
                    INV_PAYMENTS_DTL.ATTRIBUTE4 = dtGridData.Rows[iLoop]["INV_PAID_AMT"].ToString();
                    INV_PAYMENTS_DTL.PAY_INVOICE_PAID_AMT = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop]["pay_invoice_paid_amt"].ToString());
                    INV_PAYMENTS_DTL.PAY_INVOICE_AMT = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop]["pay_invoice_amt"].ToString());
                    INV_PAYMENTS_DTL.INV_RETENTION_AMT = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop]["INV_RETENTION_AMT"].ToString());
                    INV_PAYMENTS_DTL.PAY_RETENTIION_AMT = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop]["PAY_RETENTIION_AMT"].ToString());

                    INV_PAYMENTS_DTL.WORKFLOW_COMPLETION_STATUS = "1";
                    INV_PAYMENTS_DTL.ENABLED_FLAG = "1";

                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        if ((dtGridData.Rows[iLoop]["PAY_DTL_ID"].ToString()) != "0" && dtGridData.Rows[iLoop]["PAY_DTL_ID"].ToString() != string.Empty)
                        {
                            tmpChildEntity.Add(new Tuple<object, string>(INV_PAYMENTS_DTL, "D"));
                        }
                    }
                    else
                    {
                        if ((dtGridData.Rows[iLoop]["PAY_DTL_ID"].ToString()) != "0" && dtGridData.Rows[iLoop]["PAY_DTL_ID"].ToString() != string.Empty)
                        {

                            INV_PAYMENTS_DTL.MODIFIED_BY = this.LoggedUserName;
                            INV_PAYMENTS_DTL.MODIFIED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(INV_PAYMENTS_DTL, "U"));
                        }
                        else
                        {
                            INV_PAYMENTS_DTL.PAY_DTL_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.AP_022_D.ToString(), false, true);
                            INV_PAYMENTS_DTL.CREATED_BY = this.LoggedUserName;
                            INV_PAYMENTS_DTL.CREATED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(INV_PAYMENTS_DTL, "A"));
                        }
                    }
                    INV_PAYMENTS_DTL.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;

                }
                //check whether the Accounting period is available or not
                ProReturn = FINSP.GetSPFOR_ERR_IS_CAL_PERIOD_AVIAL(INV_PAYMENTS_HDR.PAY_ID, txtPaymentDate.Text);

                if (ProReturn != string.Empty)
                {

                    if (ProReturn != "0")
                    {
                        ErrorCollection.Add("POREQcaL", ProReturn);
                        if (ErrorCollection.Count > 0)
                        {
                            return;
                        }
                    }
                }



                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            FIN.BLL.GL.AccountCodes_BLL.SavePCEntity<INV_PAYMENTS_HDR, INV_PAYMENTS_DTL>(INV_PAYMENTS_HDR, tmpChildEntity, INV_PAYMENTS_DTL);
                            savedBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {
                            FIN.BLL.GL.AccountCodes_BLL.SavePCEntity<INV_PAYMENTS_HDR, INV_PAYMENTS_DTL>(INV_PAYMENTS_HDR, tmpChildEntity, INV_PAYMENTS_DTL, true);
                            savedBool = true;
                            break;
                        }
                }

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                INV_PAYMENTS_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, INV_PAYMENTS_HDR.PAY_ID);
                DBMethod.SaveEntity<INV_PAYMENTS_HDR>(INV_PAYMENTS_HDR, true);
                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                DataTable dt_POC = new DataTable();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dt_POC = (DataTable)Session["PAY_OC"];
                }



                if (dt_POC != null)
                {
                    for (int iLoop = 0; iLoop < dt_POC.Rows.Count; iLoop++)
                    {
                        INV_PAYMENTS_OTHER_CHARGES iNV_PAYMENTS_OTHER_CHARGES = new INV_PAYMENTS_OTHER_CHARGES();

                        if ((dt_POC.Rows[iLoop]["POC_ID"].ToString()) != "0" && dt_POC.Rows[iLoop]["POC_ID"].ToString() != string.Empty)
                        {
                            using (IRepository<INV_PAYMENTS_OTHER_CHARGES> userCtx = new DataRepository<INV_PAYMENTS_OTHER_CHARGES>())
                            {
                                iNV_PAYMENTS_OTHER_CHARGES = userCtx.Find(r =>
                                    (r.POC_ID == (dt_POC.Rows[iLoop]["POC_ID"].ToString()))
                                    ).SingleOrDefault();
                            }
                        }

                        iNV_PAYMENTS_OTHER_CHARGES.PAY_ID = INV_PAYMENTS_HDR.PAY_ID;
                        iNV_PAYMENTS_OTHER_CHARGES.ATTRIBUTE1 = dt_POC.Rows[iLoop]["ACCT_CODE_ID"].ToString();
                        iNV_PAYMENTS_OTHER_CHARGES.ATTRIBUTE2 = dt_POC.Rows[iLoop]["AddSub_value"].ToString();
                        iNV_PAYMENTS_OTHER_CHARGES.POC_AMOUNT = CommonUtils.ConvertStringToDecimal(dt_POC.Rows[iLoop]["POC_AMOUNT"].ToString());

                        iNV_PAYMENTS_OTHER_CHARGES.REMARKS = dt_POC.Rows[iLoop]["REMARKS"].ToString();

                        iNV_PAYMENTS_OTHER_CHARGES.ORG_ID = VMVServices.Web.Utils.OrganizationID;
                        iNV_PAYMENTS_OTHER_CHARGES.WORKFLOW_COMPLETION_STATUS = "1";
                        iNV_PAYMENTS_OTHER_CHARGES.ENABLED_FLAG = "1";

                        if (dt_POC.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                        {
                            if ((dt_POC.Rows[iLoop]["POC_ID"].ToString()) != "0" && dt_POC.Rows[iLoop]["POC_ID"].ToString() != string.Empty)
                            {
                                DBMethod.DeleteEntity<INV_PAYMENTS_OTHER_CHARGES>(iNV_PAYMENTS_OTHER_CHARGES);
                            }


                        }
                        else
                        {
                            if ((dt_POC.Rows[iLoop]["POC_ID"].ToString()) != "0" && dt_POC.Rows[iLoop]["POC_ID"].ToString() != string.Empty)
                            {
                                iNV_PAYMENTS_OTHER_CHARGES.MODIFIED_BY = this.LoggedUserName;
                                iNV_PAYMENTS_OTHER_CHARGES.MODIFIED_DATE = DateTime.Today;
                                DBMethod.SaveEntity<INV_PAYMENTS_OTHER_CHARGES>(iNV_PAYMENTS_OTHER_CHARGES, true);
                            }
                            else
                            {
                                iNV_PAYMENTS_OTHER_CHARGES.POC_ID = FINSP.GetSPFOR_SEQCode("AP_022_OC".ToString(), false, true);
                                iNV_PAYMENTS_OTHER_CHARGES.CREATED_BY = this.LoggedUserName;
                                iNV_PAYMENTS_OTHER_CHARGES.CREATED_DATE = DateTime.Today;
                                DBMethod.SaveEntity<INV_PAYMENTS_OTHER_CHARGES>(iNV_PAYMENTS_OTHER_CHARGES);
                            }
                        }

                    }
                }
                if (ddlPaymentMode.SelectedItem.Text == "Cheque")
                {
                    CA_CHECK_DTL cA_CHECK_DTL = new CA_CHECK_DTL();
                    using (IRepository<CA_CHECK_DTL> userCtx = new DataRepository<CA_CHECK_DTL>())
                    {
                        cA_CHECK_DTL = userCtx.Find(r =>
                            (r.CHECK_DTL_ID == ddlChequeNumber.SelectedValue.ToString())
                            ).SingleOrDefault();
                    }
                    if (cA_CHECK_DTL != null)
                    {
                        SUPPLIER_CUSTOMERS sUPPLIER_CUSTOMERS = new SUPPLIER_CUSTOMERS();
                        using (IRepository<SUPPLIER_CUSTOMERS> userCtx = new DataRepository<SUPPLIER_CUSTOMERS>())
                        {
                            sUPPLIER_CUSTOMERS = userCtx.Find(r =>
                                (r.VENDOR_ID == ddlSupplierNumber.SelectedValue.ToString())
                                ).SingleOrDefault();
                        }
                        if (sUPPLIER_CUSTOMERS != null)
                        {
                            cA_CHECK_DTL.CHECK_PAY_TO = sUPPLIER_CUSTOMERS.ATTRIBUTE1;
                        }
                        else
                        {
                            cA_CHECK_DTL.CHECK_PAY_TO = txtSupplierName.Text;
                        }
                        cA_CHECK_DTL.CHECK_DT = DBMethod.ConvertStringToDate(txtPaymentDate.Text);
                        cA_CHECK_DTL.CHECK_AMT = decimal.Parse(txtTotalPaymentAmt.Text);
                        cA_CHECK_DTL.CHECK_STATUS = "USED";
                        cA_CHECK_DTL.CHECK_CURRENCY_CODE = ddlPaymentCurrency.SelectedValue;
                        cA_CHECK_DTL.MODIFIED_BY = this.LoggedUserName;
                        cA_CHECK_DTL.MODIFIED_DATE = DateTime.Today;
                        DBMethod.SaveEntity<CA_CHECK_DTL>(cA_CHECK_DTL, true);

                    }
                }
                if (INV_PAYMENTS_HDR.WORKFLOW_COMPLETION_STATUS == "1")
                {
                    FIN.BLL.SSM.Alert_BLL.GenerateEmail(FINMessageConstatns.PAYMENT_ALERT);
                    if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0" && savedBool == true)
                    {
                        if (VMVServices.Web.Utils.IsAlert == "1")
                        {
                            FINSQL.UpdateAlertUserLevel();
                        }
                    }
                }
                if (ErrorCollection.Count > 0)
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WRI_ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }

        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddlInvoiceNumber = tmpgvr.FindControl("ddlInvoiceNumber") as DropDownList;

                string str_currecny = "";
                if (ddlPaymentCurrency.SelectedValue.ToString().Length > 0)
                {
                    if (Session[FINSessionConstants.ORGCurrency].ToString() == ddlPaymentCurrency.SelectedValue)
                    {
                        str_currecny = "";
                    }
                    else
                    {
                        str_currecny = ddlPaymentCurrency.SelectedValue;
                    }
                }
                Invoice_BLL.getInvoice4Payment(ref ddlInvoiceNumber, ddlSupplierNumber.SelectedValue, Master.Mode, str_currecny);

                if (gvData.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                {
                    if (gvData.DataKeys[gvData.EditIndex].Values["pay_invoice_id"] != null)
                    {
                        ddlInvoiceNumber.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["pay_invoice_id"].ToString();
                    }
                    if (Master.Mode == FINAppConstants.Update)
                    {
                        ddlInvoiceNumber.Enabled = false;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WRI_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                if (Master.Mode == FINAppConstants.WF)
                {
                    return;
                }
                if (ddlPaymentMode.SelectedItem.Text == "Cheque")
                {
                    if (ddlChequeNumber.SelectedValue == string.Empty || txtChequeDate.Text == string.Empty)
                    {
                        ErrorCollection.Add("cheque", "Please select the cheque details(Cheque Number and Value Date");
                    }
                }
                else if (ddlPaymentMode.SelectedItem.Text == "EFT")
                {
                    if (ddlBankName.SelectedValue == string.Empty || ddlBankBranch.SelectedValue == string.Empty || ddlAccountnumber.SelectedValue == string.Empty)
                    {
                        ErrorCollection.Add("eft", "Please select the bank details (Bank Name,Bank Branch and Account Number");
                    }
                }

                AssignToBE();

                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }



        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                if (dtData.Rows.Count > 0)
                {

                    //txtTotalPaymentAmt.Text = CommonUtils.CalculateTotalAmount(dtData, "pay_invoice_paid_amt");
                    HDTotPayment.Value = CommonUtils.CalculateTotalAmount(dtData, "pay_invoice_paid_amt");
                    hdTotRetnAmt.Value = CommonUtils.CalculateTotalAmount(dtData, "PAY_RETENTIION_AMT");
                    //txtTotalPaymentAmt.Text = (CommonUtils.ConvertStringToDecimal(HDTotPayment.Value.ToString()) + CommonUtils.ConvertStringToDecimal(hdTotRetnAmt.Value.ToString())).ToString();

                    txtTotalPaymentAmt.Text = (CommonUtils.ConvertStringToDecimal(HDTotPayment.Value.ToString()) + CommonUtils.ConvertStringToDecimal(hdTotRetnAmt.Value.ToString()) + CommonUtils.ConvertStringToDecimal(hdOCAmt.Value)).ToString();
                    //txtTotalPaymentAmt.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtToe talPaymentAmt.Text);
                    Session["ToTPayment"] = txtTotalPaymentAmt.Text;
                }

                if (dtData.Rows.Count > 0)
                {
                    dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("inv_amt", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("inv_amt"))));
                    dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("inv_retention_amt", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("inv_retention_amt"))));
                    dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("pay_invoice_paid_amt", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("pay_invoice_paid_amt"))));
                    dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("pay_invoice_amt", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("pay_invoice_amt"))));
                    dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("INV_PAID_AMT", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("INV_PAID_AMT"))));
                    dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("PAY_RETENTIION_AMT", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("PAY_RETENTIION_AMT"))));
                    dtData.AcceptChanges();
                }

                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WRI_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            if (Session[FINSessionConstants.GridData] != null)
            {
                dtGridData = (DataTable)Session[FINSessionConstants.GridData];
            }
            gvData.EditIndex = -1;

            BindGrid(dtGridData);

        }
        #endregion

        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WRI_ROW_CMD", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {
            System.Collections.SortedList slControls = new System.Collections.SortedList();

            DropDownList ddlInvoiceNumber = gvr.FindControl("ddlInvoiceNumber") as DropDownList;
            //TextBox txtLineNo = gvr.FindControl("txtLineNo") as TextBox;
            TextBox txtInvoiceDate = gvr.FindControl("txtInvoiceDate") as TextBox;
            TextBox txtInvoiceAmount = gvr.FindControl("txtInvoiceAmount") as TextBox;
            TextBox txtAmountPaid = gvr.FindControl("txtAmountPaid") as TextBox;
            TextBox txtAmountToPay = gvr.FindControl("txtAmountToPay") as TextBox;
            TextBox txtRetentionAmount = gvr.FindControl("txtRetentionAmount") as TextBox;
            TextBox txt_PaymentAmount = gvr.FindControl("txtPaymentAmount") as TextBox;
            TextBox txt_PayRetentionAmount = gvr.FindControl("txtPayRetentionAmount") as TextBox;




            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();

            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList[FINColumnConstants.PAY_DTL_ID] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);
            }



            if (txt_PaymentAmount.Text.ToString().Trim().Length == 0 && txt_PayRetentionAmount.Text.ToString().Trim().Length == 0)
            {
                ErrorCollection.Add("pleaseEnteramount", "Please Enter payment amount or Retention Amount");
                return drList;
            }



            if (txt_PaymentAmount.Text.ToString().Trim().Length == 0)
                txt_PaymentAmount.Text = "0";
            if (txt_PayRetentionAmount.Text.ToString().Trim().Length == 0)
                txt_PayRetentionAmount.Text = "0";



            if (CommonUtils.ConvertStringToDecimal(txt_PaymentAmount.Text) > (CommonUtils.ConvertStringToDecimal(txtInvoiceAmount.Text) - CommonUtils.ConvertStringToDecimal(txtRetentionAmount.Text) - CommonUtils.ConvertStringToDecimal(txtAmountPaid.Text)))
            {
                ErrorCollection.Add("InvalidAmt", "Payment amount must be less than the balance amount");
                //txt_PaymentAmount.Text = "";
                //txt_PaymentAmount.Focus();
                return drList;
            }
            else
            {
                if (CommonUtils.ConvertStringToDecimal(txtInvoiceAmount.Text) > 0 && CommonUtils.ConvertStringToDecimal(txt_PaymentAmount.Text) > 0)
                {
                    txtAmountToPay.Text = (CommonUtils.ConvertStringToDecimal(txtInvoiceAmount.Text) - (CommonUtils.ConvertStringToDecimal(txtRetentionAmount.Text) + CommonUtils.ConvertStringToDecimal(txtAmountPaid.Text) + CommonUtils.ConvertStringToDecimal(txt_PaymentAmount.Text))).ToString();
                }
            }

            if (txt_PaymentAmount.Text.ToString().Trim().Length == 0)
                txt_PaymentAmount.Text = "0";
            if (txt_PayRetentionAmount.Text.ToString().Trim().Length == 0)
                txt_PayRetentionAmount.Text = "0";


            //if (lineNum)
            //{
            //    txtLineNo.Text = (rowindex - 1).ToString();
            //}
            //else
            //{
            //    txtLineNo.Text = (rowindex + 1).ToString();
            //}

            lineNum = false;

            ErrorCollection.Clear();

            // slControls[0] = txtLineNo;
            slControls[0] = ddlInvoiceNumber;
            slControls[1] = txtInvoiceDate;
            slControls[2] = txtInvoiceAmount;
            slControls[3] = txt_PaymentAmount;
            slControls[4] = txtAmountToPay;
            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/AP_" + Session["Sel_Lng"].ToString() + ".properties"));
            ErrorCollection.Clear();
            string strCtrlTypes = FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX;
            string strMessage = Prop_File_Data["Invoice_Number_P"] + " ~ " + Prop_File_Data["Invoice_Date_P"] + " ~ " + Prop_File_Data["Invoice_Amount_P"] + " ~ " + Prop_File_Data["Amount_Paid_P"] + " ~ " + Prop_File_Data["Amount_To_Pay_P"] + "";
            //string strMessage = "Line No ~ Invoice Number ~ Invoice Date ~ Invoice Amount ~ Amount Paid ~ Amount to Pay";

            EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

            if (EmptyErrorCollection.Count > 0)
            {
                ErrorCollection = EmptyErrorCollection;
                return drList;
            }


            string strCondition = "PAY_INVOICE_ID='" + ddlInvoiceNumber.SelectedValue + "'";
            strMessage = FINMessageConstatns.RecordAlreadyExists;
            ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            if (ErrorCollection.Count > 0)
            {
                return drList;
            }

            if (txtRetentionAmount.Text.ToString().Trim().Length > 0)
            {
                if (CommonUtils.ConvertStringToDecimal(txt_PayRetentionAmount.Text) > CommonUtils.ConvertStringToDecimal(txtRetentionAmount.Text))
                {
                    ErrorCollection.Add("ren_amt_toPay1", "Payment Rentention should be less than the Invoice Retention Amount");
                    return drList;
                }
            }

            if (CommonUtils.ConvertStringToDecimal(txtInvoiceAmount.Text) > 0)
            {
                if ((CommonUtils.ConvertStringToDecimal(txtInvoiceAmount.Text) - CommonUtils.ConvertStringToDecimal(txtRetentionAmount.Text)) < CommonUtils.ConvertStringToDecimal(txt_PaymentAmount.Text.ToString()))
                {
                    ErrorCollection.Add("ren_amt_toPay", "Amount Paid should be less than the Invoice Amount");
                    return drList;
                }
            }


            //if (CommonUtils.ConvertStringToDecimal(txtInvoiceAmount.Text) == (CommonUtils.ConvertStringToDecimal(txt_PaymentAmount.Text) + CommonUtils.ConvertStringToDecimal(txtRetentionAmount.Text)))
            //{
            //    ErrorCollection.Add("amtpaidalready", "Amount already paid for the invoice.");
            //    return drList;
            //}
            //else if (CommonUtils.ConvertStringToDecimal(txt_PaymentAmount.Text) == 0)
            //{
            //    ErrorCollection.Add("invldpayment", "Invalid Payment Amount.");
            //    return drList;
            //}


            //txtTotalPaymentAmt.Text = CommonUtils.CalculateTotalAmount(dtGridData, "pay_invoice_paid_amt");
            //PurchaseOrder_BLL.CalculateAmount(dtGridData, CommonUtils.ConvertStringToDecimal(txt_PaymentAmount.Text));
            // txtTotalPaymentAmt.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtTotalPaymentAmt.Text);

            //    drList["line_no"] = txtLineNo.Text;
            drList[FINColumnConstants.PAY_INVOICE_ID] = ddlInvoiceNumber.SelectedValue.ToString();
            drList["INV_NUM"] = ddlInvoiceNumber.SelectedItem.Text.ToString();
            drList["inv_date"] = DBMethod.ConvertStringToDate(txtInvoiceDate.Text.ToString());

            drList["inv_amt"] = txtInvoiceAmount.Text;
            drList["INV_PAID_AMT"] = txtAmountPaid.Text;
            drList["pay_invoice_paid_amt"] = txt_PaymentAmount.Text;
            drList["pay_invoice_amt"] = txtAmountToPay.Text;
            drList["INV_RETENTION_AMT"] = txtRetentionAmount.Text == string.Empty ? "0" : txtRetentionAmount.Text;
            drList["PAY_RETENTIION_AMT"] = txt_PayRetentionAmount.Text == string.Empty ? "0" : txt_PayRetentionAmount.Text;
            drList[FINColumnConstants.DELETED] = FINAppConstants.N;


            return drList;

        }



        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WRI_ROW_UP", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WRI_ROW_DEL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WRI_ROW_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WRI_RowCreated", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        lineNum = true;
                        e.Row.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WRI_RDB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        protected void btnUpdateStopPayment_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                if (EntityData != null)
                {
                    INV_PAYMENTS_HDR = (INV_PAYMENTS_HDR)EntityData;
                }
                if (txtStopDate.Text != string.Empty)
                {
                    INV_PAYMENTS_HDR.PAY_STOP_DATE = DBMethod.ConvertStringToDate(txtStopDate.Text.ToString());
                }
                INV_PAYMENTS_HDR.PAY_STOP_REASON = txtStopPaymentReason.Text;
                INV_PAYMENTS_HDR.PAY_STOP_REMARKS = txtStopPayRemarks.Text;
                DBMethod.SaveEntity<INV_PAYMENTS_HDR>(INV_PAYMENTS_HDR, true);

                CA_CHECK_DTL cA_CHECK_DTL = new CA_CHECK_DTL();
                using (IRepository<CA_CHECK_DTL> userCtx = new DataRepository<CA_CHECK_DTL>())
                {
                    cA_CHECK_DTL = userCtx.Find(r =>
                        (r.CHECK_DTL_ID == ddlChequeNumber.SelectedValue.ToString())
                        ).SingleOrDefault();
                }
                if (cA_CHECK_DTL != null)
                {
                    SUPPLIER_CUSTOMERS sUPPLIER_CUSTOMERS = new SUPPLIER_CUSTOMERS();
                    using (IRepository<SUPPLIER_CUSTOMERS> userCtx = new DataRepository<SUPPLIER_CUSTOMERS>())
                    {
                        sUPPLIER_CUSTOMERS = userCtx.Find(r =>
                            (r.VENDOR_ID == ddlSupplierNumber.SelectedValue.ToString())
                            ).SingleOrDefault();
                    }
                    if (cA_CHECK_DTL.CHECK_STATUS.ToString() == "USED")
                    {
                        cA_CHECK_DTL.CHECK_STATUS = "CANCEL";
                        cA_CHECK_DTL.CHECK_CANCEL_REMARKS = txtStopPayRemarks.Text + " " + txtStopPaymentReason.Text;
                    }
                    DBMethod.SaveEntity<CA_CHECK_DTL>(cA_CHECK_DTL, true);

                }
                stopPayBool = true;
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WRI_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        protected void btnStopPayment_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                popStopPayment.Show();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WRI_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        protected void btnClose_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();


                //Session["StopPayment"] = "stop";
                popStopPayment.Hide();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WRI_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        protected void btnInvoiceOpen_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                string url = Server.MapPath("~/AP/InvoicePopup.aspx");
                Session["InvoiceProgramID"] = "30";
                Session["InvoiceMode"] = "U";
                Session["InvoiceID"] = Session["INVNUM"].ToString();

                ModalPopupExtender2.Show();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WRI_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        protected void btnYes_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                // COURSE_GROUP.COURSE_GROUP_ID = short.Parse(Master.RecordID.ToString());
                DBMethod.DeleteEntity<INV_PAYMENTS_HDR>(INV_PAYMENTS_HDR);
                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATADELETED);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WRI_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }




        protected void ddlLineNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WRI_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void ddlLotNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;

                DropDownList ddlLotNo = gvr.FindControl("ddlLotNo") as DropDownList;
                TextBox txtquantity = gvr.FindControl("txtquantity") as TextBox;
                DataTable dtqty = new DataTable();
                dtqty = DBMethod.ExecuteQuery(FIN.DAL.AP.WarehouseReceiptItem_DAL.getQuatity(ddlLotNo.SelectedValue)).Tables[0];
                txtquantity.Text = dtqty.Rows[0]["LOT_QTY"].ToString();


                //if (txtWHQuantity.Text.Trim() == string.Empty)
                //{
                //    txtWHQuantity.Text = "0";
                //}
                //if (CommonUtils.ConvertStringToDecimal(txtWHQuantity.Text) >= 0 && CommonUtils.ConvertStringToDecimal(txtquantity.Text) >= 0)
                //{
                //    txtWHQuantity.Text = (CommonUtils.ConvertStringToDecimal(txtquantity.Text) + CommonUtils.ConvertStringToDecimal(txtWHQuantity.Text)).ToString();
                //}

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WRI_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void ddlWarehouseNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                //  txtWarehouseName.Text = ddlWarehouseNumber.SelectedValue;
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void FillLineNumberDetails()
        {
            DataTable dtgrndata = new DataTable();

            //dtgrndata = DBMethod.ExecuteQuery(FIN.DAL.AP.WarehouseReceiptItem_DAL.getgrndata(ddlGRNNumber.SelectedValue)).Tables[0];
            //txtGRNDate.Text = DateTime.Parse(dtgrndata.Rows[0]["GRN_DATE"].ToString()).ToString("dd/MM/yyyy");
            //PurchaseItemReceipt_BLL.fn_GetPOItemLine(ref ddlLineNumber, ddlGRNNumber.SelectedValue.ToString());


        }
        protected void ddlGRNNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                FillLineNumberDetails();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void txtPaymentDate_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                FillExchangeRate();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void FillExchangeRate()
        {
            DataTable dtDat = new DataTable();
            dtDat = DBMethod.ExecuteQuery(Currency_DAL.getCurrencyBasedOrg(VMVServices.Web.Utils.OrganizationID, txtPaymentDate.Text)).Tables[0];

            if (dtDat != null)
            {
                if (dtDat.Rows.Count > 0)
                {
                    if (ddlPaymentCurrency.SelectedValue == dtDat.Rows[0][FINColumnConstants.CURRENCY_ID].ToString())
                    {
                        txtExchangeRate.Enabled = false;
                        txtExchangeRate.Text = "1";
                    }
                    else
                    {

                        txtExchangeRate.Enabled = true;
                        DataTable dtDat1 = new DataTable();
                        dtDat1 = DBMethod.ExecuteQuery(ExchangeRate_DAL.GetExchangeRate(txtPaymentDate.Text, ddlPaymentCurrency.SelectedValue.ToString())).Tables[0];

                        if (dtDat1 != null)
                        {
                            if (dtDat1.Rows.Count > 0)
                            {
                                if (ddlExchangeRateType.SelectedItem.Text.ToLower() == "standard")
                                {
                                    txtExchangeRate.Text = dtDat1.Rows[0]["CURRENCY_STD_RATE"].ToString();
                                }
                                else if (ddlExchangeRateType.SelectedItem.Text.ToLower() == "selling")
                                {
                                    txtExchangeRate.Text = dtDat1.Rows[0]["CURRENCY_SEL_RATE"].ToString();
                                }
                                else if (ddlExchangeRateType.SelectedItem.Text.ToLower() == "buying")
                                {
                                    txtExchangeRate.Text = dtDat1.Rows[0]["CURRENCY_BUY_RATE"].ToString();
                                }
                            }
                            else
                            {
                                txtExchangeRate.Text = "1";
                            }
                        }
                        else
                        {
                            txtExchangeRate.Text = "1";
                        }
                    }
                }
            }
        }
        protected void ddlExchangeRateType_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                FillExchangeRate();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Savde", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }

        protected void ddlPaymentCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DataTable dtData = new DataTable();

                Lookup_BLL.GetLookUpValues(ref ddlExchangeRateType, "EXRATETYPE");
                if (ddlExchangeRateType.Items.Count >= 1)
                {
                    ddlExchangeRateType.SelectedIndex = 1;
                }
                txtExchangeRate.Text = "1";
                txtExchangeRate.Enabled = true;


                GridViewRow gvr = gvData.FooterRow;
                DropDownList ddlInvoiceNumber = gvr.FindControl("ddlInvoiceNumber") as DropDownList;
                TextBox txtRetentionAmount = gvr.FindControl("txtRetentionAmount") as TextBox;

                Segments_BLL.GetGlobalSegmentvalues(ref ddlGlobalSegment, VMVServices.Web.Utils.OrganizationID);

                //  AccountCodes_BLL.getAccCodeBasedOrg(ref  ddlAccountCode, VMVServices.Web.Utils.OrganizationID);

                if (ddlPaymentCurrency.SelectedValue != string.Empty)
                {
                    //Payment_BLL.getInvoiceDetails(ref ddlInvoiceNumber, ddlPaymentCurrency.SelectedValue.ToString(), ddlSupplierNumber.SelectedValue);

                    string str_currecny = "";
                    if (ddlPaymentCurrency.SelectedValue.ToString().Length > 0)
                    {
                        if (Session[FINSessionConstants.ORGCurrency].ToString() == ddlPaymentCurrency.SelectedValue)
                        {
                            str_currecny = "";
                        }
                        else
                        {
                            str_currecny = ddlPaymentCurrency.SelectedValue;
                        }
                    }
                    Invoice_BLL.getInvoice4Payment(ref ddlInvoiceNumber, ddlSupplierNumber.SelectedValue, Master.Mode, str_currecny);

                    Payment_BLL.getAccCodeDetails(ref ddlAccountCode, ddlPaymentCurrency.SelectedValue.ToString(), ddlPaymentCurrency.SelectedValue);

                    dtData = DBMethod.ExecuteQuery(Payment_DAL.getInvoiceDetails(ddlPaymentCurrency.SelectedValue.ToString())).Tables[0];
                    if (dtData != null)
                    {
                        if (dtData.Rows.Count > 0)
                        {
                            ddlGlobalSegment.SelectedValue = dtData.Rows[0]["global_segment_id"].ToString();
                            ddlAccountCode.SelectedValue = dtData.Rows[0]["inv_acct_code_id"].ToString();
                            txtRetentionAmount.Text = dtData.Rows[0]["inv_retention_amt"].ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Savde", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void OpenWindow(object sender, EventArgs e)
        {
            string url = Server.MapPath("~/AP/InvoicePopup.aspx");
            Session["InvoiceProgramID"] = "30";
            Session["InvoiceMode"] = "U";
            Session["InvoiceID"] = Session["INVNUM"].ToString();

            //string s = "window.open('" + url + "', 'popup_window', 'width=300,height=100,left=100,top=100,resizable=yes');";
            //ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
            ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + url + "','','" + DbConsts.ReportProperties + "');", true);

        }

        #endregion






        //Payment Other Charges


        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>

        protected void gv_pay_OC_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session["PAY_OC"] != null)
                {
                    dtGridData = (DataTable)Session["PAY_OC"];
                }
                gv_pay_OC.EditIndex = -1;

                BindGrid_OC(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WH_Row_cnl", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gv_pay_OC_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session["PAY_OC"] != null)
                {
                    dtGridData = (DataTable)Session["PAY_OC"];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gv_pay_OC.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }

                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl_OC(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    else
                    {
                        dtGridData.Rows.Add(drList);
                        BindGrid_OC(dtGridData);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WH_Row_Cmd", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }



        private DataRow AssignToGridControl_OC(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {

            System.Collections.SortedList slControls = new System.Collections.SortedList();

            DropDownList ddl_AccountCode = gvr.FindControl("ddlAccountCode") as DropDownList;
            DropDownList ddlAddSub = gvr.FindControl("ddlAddSub") as DropDownList;
            TextBox txt_POCAmount = gvr.FindControl("txtPOCAmount") as TextBox;
            TextBox txt_POCRemarks = gvr.FindControl("txtPOCRemarks") as TextBox;

            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList["POC_ID"] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }

            slControls[0] = ddl_AccountCode;
            slControls[1] = txt_POCAmount;
            slControls[2] = ddlAddSub;

            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/AP_" + Session["Sel_Lng"].ToString() + ".properties"));

            ErrorCollection.Clear();
            string strCtrlTypes = "DropDownList~TextBox~DropDownList";
            string strMessage = "Account Code" + " ~ " + "Amount" + " ~ " + "Add or Deduct";
            //string strMessage = "Facility ~ Start Date ~ Start Date ~ End Date";
            ErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);
            if (ErrorCollection.Count > 0)
                return drList;



            string strCondition = "ACCT_CODE_ID='" + ddl_AccountCode.SelectedValue.ToString() + "'";
            strMessage = FINMessageConstatns.RecordAlreadyExists;
            ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            if (ErrorCollection.Count > 0)
            {
                return drList;
            }




            drList["ACCT_CODE_ID"] = ddl_AccountCode.SelectedItem.Value;
            drList["ACCT_CODE_DESC"] = ddl_AccountCode.SelectedItem.Text;

            drList["AddSub_value"] = ddlAddSub.SelectedValue;
            drList["POC_AMOUNT"] = txt_POCAmount.Text;

            drList["REMARKS"] = txt_POCRemarks.Text;


            drList[FINColumnConstants.DELETED] = FINAppConstants.N;

            return drList;
        }

        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gv_pay_OC_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gv_pay_OC.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session["PAY_OC"] != null)
                {
                    dtGridData = (DataTable)Session["PAY_OC"];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl_OC(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }

                else
                {
                    gv_pay_OC.EditIndex = -1;
                    BindGrid_OC(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WH_Row_upd", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row delete mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewDeleteEventArgs indicates which row's delete button was clicked. </param>


        protected void gv_pay_OC_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session["PAY_OC"] != null)
                {
                    dtGridData = (DataTable)Session["PAY_OC"];
                }
                dtGridData.Rows[e.RowIndex][FINColumnConstants.DELETED] = FINAppConstants.Y;
                BindGrid_OC(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WH_Row_del", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gv_pay_OC_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session["PAY_OC"] != null)
                {
                    dtGridData = (DataTable)Session["PAY_OC"];
                }
                gv_pay_OC.EditIndex = e.NewEditIndex;
                BindGrid_OC(dtGridData);
                GridViewRow gvr = gv_pay_OC.Rows[e.NewEditIndex];
                FillFooterGridCombo_OC(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WH_Row_Edt", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gv_pay_OC_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gv_pay_OC.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WH_RowCrt", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// Used to delete the exam master and detail table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>



        private void FillFooterGridCombo_OC(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddl_AccountCode = tmpgvr.FindControl("ddlAccountCode") as DropDownList;
                AccountCodes_BLL.fn_getAccount(ref ddl_AccountCode);

                if (gv_pay_OC.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                {
                    ddl_AccountCode.SelectedValue = gv_pay_OC.DataKeys[gv_pay_OC.EditIndex].Values["ACCT_CODE_ID"].ToString();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WH_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }



        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gv_pay_OC_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (bol_rowVisiable)
                    e.Row.Visible = false;

                if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                {
                    e.Row.Visible = false;
                }

            }
        }

        private void BindGrid_OC(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session["PAY_OC"] = dtData;

                if (dtData.Rows.Count > 0)
                {

                    //   hdOCAmt.Value = CommonUtils.CalculateTotalAmount(dtData, "POC_AMOUNT");
                    //txtTotalPaymentAmt.Text = (CommonUtils.ConvertStringToDecimal(HDTotPayment.Value.ToString()) + CommonUtils.ConvertStringToDecimal(hdTotRetnAmt.Value.ToString()) + CommonUtils.ConvertStringToDecimal(hdOCAmt.Value)).ToString();


                    dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("POC_AMOUNT", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("POC_AMOUNT"))));
                    dtData.AcceptChanges();
                }

                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gv_pay_OC.DataSource = dt_tmp;
                gv_pay_OC.DataBind();
                GridViewRow gvr = gv_pay_OC.FooterRow;
                FillFooterGridCombo_OC(gvr);

                //DataTable dtDatas = new System.Data.DataTable();
                //DataTable dtSubDatas = new System.Data.DataTable();
                //decimal totValue = 0;
                //decimal totSubValue = 0;

                //if (txtTotalPaymentAmt.Text.Length > 0)
                //{
                //    var addValue = dt_tmp.AsEnumerable().Where(r => r["AddSub_value"].ToString().Contains("Add"));
                //    var subValue = dt_tmp.AsEnumerable().Where(r => r["AddSub_value"].ToString().Contains("Deduct"));

                //    if (addValue.Any())
                //    {
                //        dtDatas = System.Data.DataTableExtensions.CopyToDataTable(addValue);
                //        string totAmt = CommonUtils.CalculateTotalAmounts(dtDatas, "POC_AMOUNT", "");
                //        totValue = CommonUtils.ConvertStringToDecimal(totAmt);
                //    }
                //    if (subValue.Any())
                //    {
                //        dtSubDatas = System.Data.DataTableExtensions.CopyToDataTable(subValue);
                //        string totAmt = CommonUtils.CalculateTotalAmounts(dtSubDatas, "POC_AMOUNT", "");
                //        totSubValue = CommonUtils.ConvertStringToDecimal(totAmt);
                //    }

                //    txtTotalPaymentAmt.Text = ((CommonUtils.ConvertStringToDecimal(Session["ToTPayment"].ToString()) + totValue) - totSubValue).ToString();
                //    txtTotalPaymentAmt.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtTotalPaymentAmt.Text);
                //}
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WRI_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        #endregion
        protected void imgBtnChequePrint_Click(object sender, ImageClickEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                ReportFile = "AP_REPORTS/RPTChequePrintout_B.rpt";
                CA_BANK cA_BANK = new CA_BANK();
                using (IRepository<CA_BANK> userCtx = new DataRepository<CA_BANK>())
                {
                    cA_BANK = userCtx.Find(r =>
                        (r.BANK_ID == ddlBankName.SelectedValue.ToString())
                        ).SingleOrDefault();
                }


                if (cA_BANK != null)
                {
                    if (cA_BANK.ATTRIBUTE1 != null)
                    {
                        if (cA_BANK.ATTRIBUTE1.ToString().Trim().Length > 0)
                        {
                            ReportFile = cA_BANK.ATTRIBUTE1.ToString();
                        }
                    }

                }
                if (ddlChequeNumber.SelectedValue.ToString().Length > 0)
                {
                    htFilterParameter.Add("CHEQUEID", ddlChequeNumber.SelectedValue.ToString());

                    NumberToWord.ToWord toword = new NumberToWord.ToWord(CommonUtils.ConvertStringToDecimal(txtTotalPaymentAmt.Text));

                    htFilterParameter.Add("AMTINWORD", toword.ConvertToEnglish());

                    VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
                    VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;


                    ReportData = FIN.BLL.CA.Cheque_BLL.GetChequesReportData();

                    htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());

                    ReportFormulaParameter = htHeadingParameters;

                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerPath + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("APAgingAnalysisReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        protected void btnChequePrint_Click(object sender, EventArgs e)
        {

        }

        protected void btnPost_Click(object sender, EventArgs e)
        {

        }

        protected void imgBtnPost_Click(object sender, ImageClickEventArgs e)
        {
            try
            {

                string str_sp_output = FINSP.SP_POSTING_ACCTCODE_VALIDATION("AP_022", ddlSupplierNumber.SelectedValue, Master.StrRecordId);

                if (str_sp_output != "SUCCESS")
                {
                    ErrorCollection.Add("PostingAcctErr", str_sp_output);
                    return;
                }


                DataTable dt_supp_cust = DBMethod.ExecuteQuery(Supplier_DAL.getSupplierCustomer4Id(ddlSupplierNumber.SelectedValue)).Tables[0];
                DataTable dt_ssm = DBMethod.ExecuteQuery(FIN.DAL.SSM.SystemOptions_DAL.getSysoptID()).Tables[0];


                if (dt_supp_cust.Rows[0]["AP_LIABILITY_ACCOUNT"].ToString() == string.Empty)
                {
                    if (dt_ssm.Rows[0]["AP_LIABILITY_ACCT"].ToString() == string.Empty)
                    {
                        ErrorCollection.Add("ADDLibacct", "Please Enter the Liability Account");
                        return;
                    }
                }



                if (EntityData != null)
                {
                    INV_PAYMENTS_HDR = (INV_PAYMENTS_HDR)EntityData;
                }
                if (INV_PAYMENTS_HDR.WORKFLOW_COMPLETION_STATUS == "1")
                {
                    FINSP.GetSP_GL_Posting(INV_PAYMENTS_HDR.PAY_ID, "AP_022");
                }

                INV_PAYMENTS_HDR = Payment_BLL.getClassEntityData(Master.StrRecordId);
                INV_PAYMENTS_HDR.POSTED_FLAG = FINAppConstants.Y;
                INV_PAYMENTS_HDR.POSTED_DATE = DateTime.Now;
                DBMethod.SaveEntity<INV_PAYMENTS_HDR>(INV_PAYMENTS_HDR, true);
                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.POSTED);

                imgBtnPost.Visible = false;
                imgbtnPaymentCancel.Visible = false;
                lblPosted.Visible = true;

                imgBtnJVPrint.Visible = true;

                pnlgridview.Enabled = false;
                pnltdHeader.Enabled = false;
                pnlAcc.Enabled = false;


                //Update
                string connectionStringSQL = System.Web.Configuration.WebConfigurationManager.AppSettings["connectionStringSQL"].ToString();
                //Create Connection  -- SQL

                dtGridData = (DataTable)Session[FINSessionConstants.GridData];

                SqlConnection con = new SqlConnection(connectionStringSQL);
                con.Open();
                DataSet dtSet = new DataSet();
                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    string sqlQuery = "";
                    sqlQuery += " select nvl(attribute6,0)  as attribute6 from inv_invoices_hdr where inv_id = '" + dtGridData.Rows[iLoop]["INV_ID"].ToString() + "'";
                    sqlQuery += " and attribute5='MASONET'";

                    dtSet = DBMethod.ExecuteQuery(sqlQuery);

                    if (dtSet.Tables[0].Rows.Count > 0)
                    {
                        if (dtSet.Tables[0].Rows[iLoop]["attribute6"].ToString() != "0")
                        {
                            decimal paid_amount = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop]["INV_PAID_AMT"].ToString()) + CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop]["PAY_INVOICE_PAID_AMT"].ToString());
                            sqlQuery = "";
                            if (paid_amount == CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop]["inv_amt"].ToString()))
                            {
                                sqlQuery = " update accountpayable set paidamount =" + paid_amount + ",status=4 where accountpayableid=" + dtSet.Tables[0].Rows[iLoop]["attribute6"].ToString();
                            }
                            else
                            {
                                sqlQuery = " update accountpayable set paidamount =" + paid_amount + ",status=3 where accountpayableid=" + dtSet.Tables[0].Rows[iLoop]["attribute6"].ToString();
                            }

                            SqlCommand cmd = new SqlCommand(sqlQuery, con);
                            cmd.ExecuteNonQuery();
                        }
                    }
                }
                con.Close();

                Hashtable htParameters = new Hashtable();
                Hashtable htHeadingParameters = new Hashtable();
                Hashtable htFilterParameter = new Hashtable();


                ReportFile = "GL_REPORTS//RPTJournalVoucher.rpt";// "../Reports/GL/RPTJournalVoucher.rpt";


                ReportData = DBMethod.ExecuteQuery(FIN.DAL.GL.JournalEntry_DAL.GetJournalVoucherDetailsRPT("", INV_PAYMENTS_HDR.JE_HDR_ID));


                Session["ProgramName"] = "Journal Voucher";
                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());

                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;

                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('../RPTCrystalReportViewer.aspx?Id?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("posting", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    imgBtnPost.Visible = true;
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void imgBtnPrint_Click(object sender, ImageClickEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                ReportFile = Master.ReportName;


                //if (ddlSupplierName.SelectedValue != string.Empty)
                //{
                //    htFilterParameter.Add("SUPPLIER_NAME", ddlSupplierName.SelectedValue);
                //}
                if (txtPaymentDate.Text != string.Empty)
                {
                    htFilterParameter.Add("From_Date", txtPaymentDate.Text);
                }
                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    htFilterParameter.Add("PAY_ID", Master.StrRecordId.ToString());
                }


                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;


                ReportData = FIN.BLL.AP.Payment_BLL.GetPaymentReportData();

                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());

                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerPath + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("APAgingAnalysisReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        protected void btnPrint_Click(object sender, ImageClickEventArgs e)
        {

        }
        protected void imgStopPayment_Click(object sender, ImageClickEventArgs e)
        {

            try
            {

                if (EntityData != null)
                {
                    // INV_PAYMENTS_HDR = (INV_PAYMENTS_HDR)EntityData;
                    INV_PAYMENTS_HDR = Payment_BLL.getClassEntity(Master.StrRecordId);
                }
                if (INV_PAYMENTS_HDR.WORKFLOW_COMPLETION_STATUS == "1")
                {
                    if (INV_PAYMENTS_HDR.POSTED_FLAG != null)
                    {
                        if (INV_PAYMENTS_HDR.POSTED_FLAG.Trim() == FINAppConstants.Y)
                        {
                            FINSP.GetSP_GL_Posting(INV_PAYMENTS_HDR.JE_HDR_ID, "AP_022_R", this.LoggedUserName);
                        }
                    }
                }
                btnStopPayment.Visible = false;

                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.POSTED);

                INV_PAYMENTS_HDR = Payment_BLL.getClassEntity(Master.StrRecordId);

                Hashtable htParameters = new Hashtable();
                Hashtable htHeadingParameters = new Hashtable();
                Hashtable htFilterParameter = new Hashtable();


                lblPosted.Visible = true;
                imgBtnPost.Visible = false;
                lblCancelled.Visible = true;

                imgBtnStopPayJV.Visible = true;
                btnStopPayment.Visible = false;

                pnlgridview.Enabled = false;
                pnltdHeader.Enabled = false;
                pnlAcc.Enabled = false;

                ReportFile = "GL_REPORTS//RPTJournalVoucher.rpt";


                ReportData = DBMethod.ExecuteQuery(FIN.DAL.GL.JournalEntry_DAL.GetJournalVoucherDetailsRPT("", INV_PAYMENTS_HDR.REV_JE_HDR_ID));


                Session["ProgramName"] = "Journal Voucher";
                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());

                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;

                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('../RPTCrystalReportViewer.aspx?Id?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("posting", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    imgBtnPost.Visible = true;
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void imgBtnJVPrint_Click(object sender, ImageClickEventArgs e)
        {
            INV_PAYMENTS_HDR = Payment_BLL.getClassEntity(Master.StrRecordId);

            if (INV_PAYMENTS_HDR != null)
            {
                Hashtable htParameters = new Hashtable();
                Hashtable htHeadingParameters = new Hashtable();
                Hashtable htFilterParameter = new Hashtable();

                ReportFile = "GL_REPORTS//RPTJournalVoucher.rpt";// "../Reports/GL/RPTJournalVoucher.rpt";


                ReportData = DBMethod.ExecuteQuery(FIN.DAL.GL.JournalEntry_DAL.GetJournalVoucherDetailsRPT("", INV_PAYMENTS_HDR.JE_HDR_ID));


                Session["ProgramName"] = "Journal Voucher";
                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());



                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;


                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('../RPTCrystalReportViewer.aspx?Id?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);



            }
        }

        protected void imgBtnStopPayJV_Click(object sender, ImageClickEventArgs e)
        {
            INV_PAYMENTS_HDR = Payment_BLL.getClassEntity(Master.StrRecordId);

            if (INV_PAYMENTS_HDR != null)
            {
                Hashtable htParameters = new Hashtable();
                Hashtable htHeadingParameters = new Hashtable();
                Hashtable htFilterParameter = new Hashtable();

                ReportFile = "GL_REPORTS//RPTJournalVoucher.rpt";// "../Reports/GL/RPTJournalVoucher.rpt";


                ReportData = DBMethod.ExecuteQuery(FIN.DAL.GL.JournalEntry_DAL.GetJournalVoucherDetailsRPT("", INV_PAYMENTS_HDR.REV_JE_HDR_ID));


                Session["ProgramName"] = "Journal Voucher";
                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());



                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;


                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('../RPTCrystalReportViewer.aspx?Id?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);



            }
        }

        protected void imgbtnPaymentCancel_Click(object sender, ImageClickEventArgs e)
        {
            INV_PAYMENTS_HDR = Payment_BLL.getClassEntity(Master.StrRecordId);
            INV_PAYMENTS_HDR.POSTED_FLAG = FINAppConstants.CANCELED;
            INV_PAYMENTS_HDR.REV_FLAG = FINAppConstants.Y;
            DBMethod.SaveEntity<INV_PAYMENTS_HDR>(INV_PAYMENTS_HDR, true);
            ErrorCollection.Add("Payment Canceled", "Payment Cancelled Successfully");
            imgBtnPost.Visible = false;
            imgbtnPaymentCancel.Visible = false;
            lblCancelled.Visible = true;
            btnSave.Visible = false;
            btnChequePrint.Visible = false;
            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
        }

    }
}