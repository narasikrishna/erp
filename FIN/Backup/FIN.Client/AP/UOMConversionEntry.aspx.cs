﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using FIN.DAL;
using FIN.BLL;
using VMVServices.Web;

namespace FIN.Client.AP
{
    public partial class UOMConversion : PageBase
    {
        INV_UOM_CONVERSION iNV_UOM_CONVERSION = new INV_UOM_CONVERSION();
        string ProReturn = null;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
            }

            if (VMVServices.Web.Utils.AddAllowed != FINAppConstants.Y && Master.Mode == FINAppConstants.Add)
            {
                btnSave.Visible = false;
            }

        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();

                EntityData = null;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    using (IRepository<INV_UOM_CONVERSION> userCtx = new DataRepository<INV_UOM_CONVERSION>())
                    {
                        iNV_UOM_CONVERSION = userCtx.Find(r =>
                            (r.UOM_CONV_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = iNV_UOM_CONVERSION;

                    ddlUOMFrom.SelectedValue = iNV_UOM_CONVERSION.UOM_FROM;
                    ddlUOMTo.SelectedValue = iNV_UOM_CONVERSION.UOM_TO.ToString();
                    txtUOMConversion.Text = iNV_UOM_CONVERSION.UOM_CONVERSION.ToString();
                    if (iNV_UOM_CONVERSION.ENABLED_FLAG == FINAppConstants.EnabledFlag)
                    {
                        chkActive.Checked = true;
                    }
                    else
                    {
                        chkActive.Checked = false;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                Dictionary<string, string> Prop_File_Data;
                Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/AP_" + Session["Sel_Lng"].ToString() + ".properties"));
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    iNV_UOM_CONVERSION = (INV_UOM_CONVERSION)EntityData;
                }



                iNV_UOM_CONVERSION.UOM_FROM = ddlUOMFrom.SelectedValue.ToString();
                iNV_UOM_CONVERSION.UOM_TO = ddlUOMTo.SelectedValue.ToString();
                decimal decValue;
                if (decimal.TryParse(txtUOMConversion.Text, out decValue))
                { iNV_UOM_CONVERSION.UOM_CONVERSION = decimal.Parse(txtUOMConversion.Text); }
                else
                {
                    ErrorCollection.Add("InvalidDecimal", Prop_File_Data["Please_Give_valid_UOM_P"]);
                    return;
                }
                iNV_UOM_CONVERSION.UOM_CONVERSION = decimal.Parse(txtUOMConversion.Text);
                iNV_UOM_CONVERSION.ENABLED_FLAG = chkActive.Checked == true ? FINAppConstants.EnabledFlag : FINAppConstants.DisabledFlag;
                iNV_UOM_CONVERSION.ORG_ID = VMVServices.Web.Utils.OrganizationID;
                
                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    iNV_UOM_CONVERSION.MODIFIED_BY = this.LoggedUserName;
                    iNV_UOM_CONVERSION.MODIFIED_DATE = DateTime.Today;

                }
                else
                {
                    iNV_UOM_CONVERSION.UOM_CONV_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.AP_0002.ToString(), false, true);
                    //iNV_UOM_CONVERSION.WORKFLOW_COMPLETION_STATUS = "1";
                    iNV_UOM_CONVERSION.CREATED_BY = this.LoggedUserName;
                    iNV_UOM_CONVERSION.CREATED_DATE = DateTime.Today;

                }

                iNV_UOM_CONVERSION.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, iNV_UOM_CONVERSION.UOM_CONV_ID);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void FillComboBox()
        {
            ComboFilling.fn_getUOMConversationFrom(ref ddlUOMFrom);
            ComboFilling.fn_getUOMConversationTo(ref ddlUOMTo);
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                AssignToBE();
                //if (ErrorCollection.Count > 0)
                //{
                //    return;
                //}
                // Duplicate Check

                ProReturn = null;
                ProReturn = FINSP.GetSPFOR_ERR_MGR_CATEGORY_TWO(Master.FormCode, iNV_UOM_CONVERSION.UOM_CONV_ID, iNV_UOM_CONVERSION.UOM_FROM, iNV_UOM_CONVERSION.UOM_TO);
                if (ProReturn != string.Empty)
                {
                    if (ProReturn != "0")
                    {
                        ErrorCollection.Add("UOMCONVERSION", ProReturn);
                        if (ErrorCollection.Count > 0)
                        {
                            return;
                        }
                    }
                }

                //ProReturn = FINSP.GetSPFOR_UOM_CONV_VALIDATOR(ddlUOMFrom.SelectedValue, ddlUOMTo.SelectedValue, iNV_UOM_CONVERSION.ORG_ID, iNV_UOM_CONVERSION.UOM_CONV_ID);

                //if (ProReturn != string.Empty)
                //{

                //    if (ProReturn != "0")
                //    {
                //        ErrorCollection.Add("UOMCONVERSATION", ProReturn);
                //        if (ErrorCollection.Count > 0)
                //        {
                //            return;
                //        }
                //    }
                //}



                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            DBMethod.SaveEntity<INV_UOM_CONVERSION>(iNV_UOM_CONVERSION);
                            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                            break;
                        }
                    case FINAppConstants.Update:
                        {

                            DBMethod.SaveEntity<INV_UOM_CONVERSION>(iNV_UOM_CONVERSION, true);
                            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                            break;

                        }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion




        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                DBMethod.DeleteEntity<INV_UOM_CONVERSION>(iNV_UOM_CONVERSION);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void txtUOMConversion_TextChanged(object sender, EventArgs e)
        {

        }


    }
}