﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="CustomerEntry.aspx.cs" Inherits="FIN.Client.AR.CustomerEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        function textboxMultilineMaxNumber(txt, maxLen) {

            if (txt.value.length > maxLen) {
                return false;
            }
            else {
                return true;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1200px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblSupplierNumber">
                Customer Number
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:TextBox ID="txtSupplierNumber" CssClass="txtBox" Enabled="false" MaxLength="50"
                    runat="server" TabIndex="1"></asp:TextBox>
            </div>
            <div class="colspace LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 150px" id="lblSupplierName">
                Customer Name
            </div>
            <div class="colspace LNOrient">
                &nbsp</div>
            <div class="divtxtBox LNOrient" style="width: 195px">
                <asp:TextBox ID="txtSupplierName" CssClass="validate[required] RequiredField txtBox"
                    MaxLength="200" runat="server" TabIndex="2" OnTextChanged="txtSupplierName_TextChanged"></asp:TextBox>
            </div>
            <div class="colspace LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 150px" id="lblSupplierNameAR">
                Customer Name(Arabic)
            </div>
            <div class="divtxtBox LNOrient" style="width: 200px; margin-left: 0px;">
                <asp:TextBox ID="txtSupplierNameAR" CssClass=" txtBox_ol" runat="server" TabIndex="2"
                    MaxLength="200" OnTextChanged="txtSupplierName_TextChanged"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblSupplierType">
                Customer Type
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:DropDownList ID="ddlSupplierType" runat="server" TabIndex="3" CssClass="validate[required]  RequiredField ddlStype"
                    AutoPostBack="True" OnSelectedIndexChanged="ddlSupplierType_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div class="colspace LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 200px" id="lblSupplierStatus">
                Customer Status
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:DropDownList ID="ddlSupplierStatus" runat="server" TabIndex="4" CssClass=" ddlStype">
                </asp:DropDownList>
            </div>
            <div class="colspace LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 200px" id="Div5">
                Account Receivable Liability Account
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:DropDownList ID="ddlAPLiable" runat="server" TabIndex="5" CssClass="ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" id="APACCT" runat="server">
            <div class="lblBox LNOrient" style="width: 200px" id="Div7">
                Account Receivable Advance Account
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:DropDownList ID="ddlAPAdvance" runat="server" TabIndex="6" CssClass=" ddlStype"
                    OnSelectedIndexChanged="ddlAPAdvance_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div class="colspace LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 200px" id="Div1">
                Payment Term
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:DropDownList ID="ddlPayterm" runat="server" TabIndex="10" CssClass="EntryFont ddlStype">
                </asp:DropDownList>
            </div>
            <div class="colspace LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 200px" id="Div4">
                Retention Account
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:DropDownList ID="ddlretenAcc" runat="server" TabIndex="11" CssClass="ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" style="display: none">
            <div class="lblBox LNOrient" style="width: 200px" id="Div8">
                AR Revenue Account
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:DropDownList ID="ddlARRevenue" runat="server" TabIndex="9" CssClass="ddlStype">
                </asp:DropDownList>
            </div>
            <div class="colspace LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 200px" id="Div2">
                AR Debtors Account
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:DropDownList ID="ddlARDebtors" runat="server" TabIndex="7" CssClass="ddlStype">
                </asp:DropDownList>
            </div>
            <div class="colspace LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 200px" id="lblARAdvance">
                AR Advance
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:DropDownList ID="ddlARAdvance" runat="server" TabIndex="8" CssClass=" ddlStype"
                    OnSelectedIndexChanged="ddlARAdvance_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="Div6">
                Alternate Name
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:TextBox ID="txtAltName" CssClass="validate[required] RequiredField txtBox" runat="server"
                    MaxLength="200" TabIndex="12"></asp:TextBox>
            </div>
            <div class="colspace LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 200px" id="Div9">
                Contact Person Name
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:TextBox ID="txtContactPersonName" TabIndex="24" CssClass="txtBox" MaxLength="100"
                    runat="server"></asp:TextBox>
            </div>
            <div class="colspace LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 200px" id="Div11">
                Customer Category
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:DropDownList ID="ddlCustomerCategory" AutoPostBack="true" runat="server" TabIndex="7"
                    CssClass=" ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblCountry">
                Country
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:DropDownList ID="ddlCountry" AutoPostBack="True" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged"
                    runat="server" TabIndex="13" CssClass="validate[required]  RequiredField ddlStype">
                </asp:DropDownList>
            </div>
            <div class="colspace LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 200px" id="lblState">
                State
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:DropDownList ID="ddlState" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlState_SelectedIndexChanged"
                    TabIndex="16" CssClass="ddlStype">
                </asp:DropDownList>
            </div>
            <div class="colspace LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 200px" id="lblCity">
                City
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:DropDownList ID="ddlCity" runat="server" TabIndex="19" CssClass="ddlStype">
                </asp:DropDownList>
            </div>
            <div class="colspace LNOrient">
                &nbsp</div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblAddress1">
                Address 1
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:TextBox ID="txtAddress1" MaxLength="90" CssClass="validate[required] RequiredField txtBox"
                    TextMode="MultiLine" Height="50px" runat="server" TabIndex="15" onkeypress="return textboxMultilineMaxNumber(this,85);"></asp:TextBox>
            </div>
            <div class="colspace LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 200px" id="lblAddress2">
                Address 2
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:TextBox ID="txtAddress2" MaxLength="90" CssClass=" txtBox" runat="server" TextMode="MultiLine"
                    onkeypress="return textboxMultilineMaxNumber(this,85);" Height="50px" TabIndex="18"></asp:TextBox>
            </div>
            <div class="colspace LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 200px" id="lblAddress3">
                Address 3
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:TextBox ID="txtAddress3" MaxLength="90" CssClass=" txtBox" runat="server" TextMode="MultiLine"
                    onkeypress="return textboxMultilineMaxNumber(this,85);" Height="50px" TabIndex="21"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblEmail">
                Email
            </div>
            <div class="divtxtBox LNOrient" style="width: 360px">
                <asp:TextBox ID="txtEmail" MaxLength="100" CssClass="validate[custom[email]] txtBox"
                    runat="server" TabIndex="25"></asp:TextBox>
                <asp:RegularExpressionValidator ID="reqEmail" runat="server" ValidationGroup="btnSave"
                    ControlToValidate="txtEmail" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">* 
                                                    Email Id is Invalid Format</asp:RegularExpressionValidator>
            </div>
            <div class="colspace LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 160px" id="lblURL">
                URL
            </div>
            <div class="divtxtBox LNOrient" style="width: 353px">
                <asp:TextBox ID="txtURL" CssClass=" txtBox" runat="server" TabIndex="26" MaxLength="200"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblPostalCode">
                Postal Code
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:TextBox ID="txtPostalCode" MaxLength="20" CssClass="txtBox_N" runat="server"
                    TabIndex="22"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars=""
                    FilterType="Numbers,Custom" TargetControlID="txtPostalCode" />
            </div>
            <div class="colspace LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 200px" id="lblPhone">
                Phone
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:TextBox ID="txtPhone" CssClass="txtBox" runat="server" MaxLength="13" TabIndex="23"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars="+,-"
                    FilterType="Numbers,Custom" TargetControlID="txtPhone" />
            </div>
            <div class="colspace LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 200px" id="lblFax">
                Fax
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:TextBox ID="txtFax" CssClass=" txtBox" runat="server" MaxLength="10" TabIndex="14"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" ValidChars="+,-"
                    FilterType="Numbers,Custom" TargetControlID="txtFax" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblTaxID1">
                Tax Identifier 1
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:TextBox ID="txtTaxID1" MaxLength="50" CssClass=" txtBox" runat="server" TabIndex="25"></asp:TextBox>
            </div>
            <div class="colspace LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 200px" id="lblTaxID2">
                Tax Identifier 2
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:TextBox ID="txtTaxID2" MaxLength="50" CssClass=" txtBox" runat="server" TabIndex="26"></asp:TextBox>
            </div>
            <div class="colspace LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 200px" id="lblTaxID3">
                Tax Identifier 3
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:TextBox ID="txtTaxID3" MaxLength="50" CssClass=" txtBox" runat="server" TabIndex="27"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblTaxID4">
                Tax Identifier 4
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:TextBox ID="txtTaxID4" MaxLength="50" CssClass="txtBox" runat="server" TabIndex="28"></asp:TextBox>
            </div>
            <div class="colspace LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 200px" id="lblTaxID5">
                Tax Identifier 5
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:TextBox ID="txtTaxID5" MaxLength="50" CssClass="txtBox" runat="server" TabIndex="29"></asp:TextBox>
            </div>
            <div class="colspace LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 200px" id="lblActive">
                Active
            </div>
            <div class="divChkbox LNOrient">
                <asp:CheckBox ID="chkActive" runat="server" Checked="True" TabIndex="30" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
        </div>
    </div>
    <div class="divClear_10">
    </div>
    <div class="divRowContainer divAction">
        <table class="SaveTable">
            <tr>
                <td>
                    <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                        TabIndex="31" />
                </td>
                <td>
                    <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="32" />
                </td>
                <td>
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="33" />
                </td>
                <td>
                    <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="34" />
                </td>
            </tr>
        </table>
    </div>
    <div id="divDelete">
        <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
            PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
        </cc2:ModalPopupExtender>
        <asp:Panel ID="pnlConfirm" runat="server">
            <div class="ConfirmForm">
                <table>
                    <tr class="ConfirmHeading" style="width: 100%">
                        <td>
                            <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                Width="60px" />
                            &nbsp;
                            <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                        </td>
                    </tr>
                </table>
            </div>
        </asp:Panel>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/AR/ARChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">


        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });
      
    </script>
</asp:Content>
