﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="PickReleaseEntry.aspx.cs" Inherits="FIN.Client.AR.PickReleaseEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 800px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 190px" id="lblPRNumber">
                Pick Release Number
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:TextBox ID="txtPRNO" CssClass="validate[] txtBox" runat="server" Enabled="false"
                    TabIndex="1"></asp:TextBox>
            </div>
            <div class="lblBox LNOrient" style="width: 190px" id="lblDCNumber">
                Delivery Voucher Number
            </div>
            <div class="divtxtBox LNOrient" style="width: 200px">
                <asp:DropDownList ID="ddlDCNumber" runat="server" CssClass="validate[required] RequiredField ddlStype"
                     TabIndex="2" AutoPostBack="True" OnSelectedIndexChanged="ddlDCNumber_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 190px" id="lblLineNumber">
                Line Number
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:DropDownList ID="ddlLineNumber" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    Width="150px" TabIndex="3" AutoPostBack="True" OnSelectedIndexChanged="ddlLineNumber_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div class="lblBox LNOrient" style="width: 190px" id="lblDCDate">
                Delivery Voucher Date
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:TextBox ID="txtDCDate" CssClass="validate[required] RequiredField txtBox" runat="server"
                    TabIndex="4"></asp:TextBox>
                <cc2:CalendarExtender ID="CalendarExtender9" runat="server" Format="dd/MM/yyyy" TargetControlID="txtDCDate"
                    OnClientDateSelectionChanged="checkDate">
                </cc2:CalendarExtender>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtDCDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 190px" id="lblOrderNumber">
                Order Number
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:TextBox ID="txtOrderNumber" CssClass="txtBox" runat="server"
                    Enabled="true" TabIndex="5"></asp:TextBox>
            </div>
            <div class="lblBox LNOrient" style="width: 190px" id="lblItemDescription">
                Item Description
            </div>
            <div class="divtxtBox LNOrient" style="width: 200px">
                <asp:TextBox ID="txtItemDescription" CssClass="txtBox" runat="server"
                    Enabled="true" TabIndex="6"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 190px" id="lblOrderLineNumber">
                Order Line Number
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:TextBox ID="txtOrderLineNumber" CssClass="txtBox_N" runat="server"
                    Enabled="true" TabIndex="7"></asp:TextBox>
            </div>
            <div class="lblBox LNOrient" style="width: 190px" id="lblItemService">
                Quantity for this Lot
            </div>
            <div class="divtxtBox LNOrient" style="width: 200px">
                <asp:TextBox ID="txtQuantityforthisLot" CssClass="txtBox_N" runat="server"
                    Enabled="false" TabIndex="8"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="LNOrient">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                Width="330px" DataKeyNames="LOT_ID,OM_PR_DTL_ID,INV_WH_ID,DELETED" OnRowCancelingEdit="gvData_RowCancelingEdit"
                OnRowCommand="gvData_RowCommand" OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound"
                OnRowDeleting="gvData_RowDeleting" OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating"
                ShowFooter="True">
                <Columns>
                    <asp:TemplateField HeaderText="Warehouse">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlWH" runat="server" CssClass="ddlStype" width="310px"
                                 TabIndex="13" AutoPostBack="True" 
                                onselectedindexchanged="ddlWH_SelectedIndexChanged" >
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlWH" AutoPostBack="True" 
                                onselectedindexchanged="ddlWH_SelectedIndexChanged" runat="server" CssClass="ddlStype" width="310px"
                                 TabIndex="9" >
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblWH" Width="310px" runat="server" Text='<%# Eval("INV_WH_NAME") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Lot Number">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlLotNumber" TabIndex="14" runat="server" CssClass="EntryFont RequiredField ddlStype"
                                AutoPostBack="True"  OnSelectedIndexChanged="ddlLotNumber_SelectedIndexChanged">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlLotNumber" TabIndex="10" runat="server" CssClass="EntryFont RequiredField ddlStype"
                                AutoPostBack="True"  OnSelectedIndexChanged="ddlLotNumber_SelectedIndexChanged">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblLotNumber" Width="180px" CssClass="adminFormFieldHeading" runat="server"
                                Text='<%# Eval("LOT_NUMBER") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Quantity">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtQuantity" TabIndex="15" runat="server" Text='<%# Eval("LOT_QTY") %>' Width="100px"
                                CssClass="EntryFont RequiredField txtBox_N" ></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtQuantity" TabIndex="11" runat="server" CssClass="EntryFont RequiredField txtBox_N" Width="100px"
                                ></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblQuantity" runat="server" Width="100px" Text='<%# Eval("LOT_QTY") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Add / Edit">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" TabIndex="16" runat="server" AlternateText="Edit"
                                CausesValidation="false" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                            <asp:ImageButton ID="ibtnDelete" TabIndex="17" runat="server" AlternateText="Delete"
                                CausesValidation="false" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibtnUpdate" TabIndex="18" runat="server" AlternateText="Update"
                                CommandName="Update" Height="20px" ImageUrl="~/Images/Update.png" />
                            <asp:ImageButton ID="ibtnCancel" TabIndex="19" runat="server" AlternateText="Cancel"
                                CausesValidation="false" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="ibtnInsert" TabIndex="12" runat="server" AlternateText="Add"
                                CommandName="FooterInsert" ImageUrl="~/Images/Add.jpg" />
                        </FooterTemplate>
                        <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
    </div>
     <asp:HiddenField runat="server" ID="HFShippedQty" />
    <asp:HiddenField runat="server" ID="HFlotqty" />
    <asp:HiddenField runat="server" ID="HfqtyfromPR" />
    <asp:HiddenField runat="server" ID="hforderNo" />
    <asp:HiddenField runat="server" ID="hfitem" />
    <asp:HiddenField runat="server" ID="hforderlineno" />
    <div class="divRowContainer divAction">
        <table class="SaveTable">
            <tr>
                <td>
                    <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                        TabIndex="17" />
                </td>
                <td>
                    <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn"  />
                </td>
                <td>
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn"  />
                </td>
                <td>
                    <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn"  />
                </td>
            </tr>
        </table>
    </div>
    <div id="divDelete">
        <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
            PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
        </cc2:ModalPopupExtender>
        <asp:Panel ID="pnlConfirm" runat="server">
            <div class="ConfirmForm">
                <table>
                    <tr class="ConfirmHeading" style="width: 100%">
                        <td>
                            <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                Width="60px" />
                            &nbsp;
                            <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                        </td>
                    </tr>
                </table>
            </div>
        </asp:Panel>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/AR/ARChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
