﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.DAL.AR;
using FIN.DAL.GL;
using FIN.BLL;
using VMVServices.Web;
using System.Web.UI.DataVisualization.Charting;
using System.Drawing;

namespace FIN.Client.AR
{
    public partial class DashBoard_AR : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session["FIVE_CUSTOMER_GRAPH"] = null;
                Session["INVOICE_RECEIVED_GRAPH"] = null;

                FillComboBox();

                DisplayFiveCustomerDetails();
                DisplayInvoiceReceivedDetails();
                getEmployeeAgeDetails();
                getSOAwaited();

                LoadGraph();
            }
            else
            {
                LoadGraph();
            }
        }

        private void LoadGraph()
        {
            if (Session["FIVE_CUSTOMER_GRAPH"] != null)
            {
                chrtTop5Customer.DataSource = (DataTable)Session["FIVE_CUSTOMER_GRAPH"];
                chrtTop5Customer.Series["S_Customer_Count"].XValueMember = "vendor_name";
                chrtTop5Customer.Series["S_Customer_Count"].YValueMembers = "payment_amt";
                chrtTop5Customer.DataBind();
            }
            if (Session["INVOICE_RECEIVED_GRAPH"] != null)
            {
                chrtInvoicedReceive.DataSource = (DataTable)Session["INVOICE_RECEIVED_GRAPH"];
                chrtInvoicedReceive.DataBind();
            }

            if (Session["CUST_AGE"] != null)
            {
                chrtEmpAge.DataSource = (DataTable)Session["CUST_AGE"];
                chrtEmpAge.DataBind();
            }
            
            if (Session["SOAwait"] != null)
            {
                chrtSOAwait.DataSource = (DataTable)Session["SOAwait"];
                chrtSOAwait.DataBind();
            }
        }

        private void getSOAwaited()
        {
            DataTable dt_SOAwait = DBMethod.ExecuteQuery(SalesOrder_DAL.GetCustomerSOAwaitedChart()).Tables[0];
            Session["SOAwait"] = dt_SOAwait;
            LoadGraph();
        }

        
        private void getEmployeeAgeDetails()
        {
            DataTable dt_empAge = DBMethod.ExecuteQuery(FIN.DAL.AP.APAgingAnalysis_DAL.GetAgingAnalysisChart()).Tables[0];
            Session["CUST_AGE"] = dt_empAge;

        }
        
        private void FillComboBox()
        {

        }
        private void DisplayFiveCustomerDetails()
        {
            DataTable dt_Top5Cust = DBMethod.ExecuteQuery(Invoice_DAL.getTop5CustomerAmt()).Tables[0];
            Session["FIVE_CUSTOMER_GRAPH"] = dt_Top5Cust;
            if (dt_Top5Cust.Rows.Count > 0)
            {
                DataTable dt_Dist_DEPT = dt_Top5Cust.DefaultView.ToTable(true, "vendor_id", "vendor_name", "payment_amt");

                DataTable dt_Dept_List = new DataTable();
                dt_Dept_List.Columns.Add("vendor_id");
                dt_Dept_List.Columns.Add("vendor_name");
                dt_Dept_List.Columns.Add("payment_amt");

                for (int iLoop = 0; iLoop < dt_Dist_DEPT.Rows.Count; iLoop++)
                {
                    DataRow dr = dt_Dept_List.NewRow();
                    dr["vendor_id"] = dt_Dist_DEPT.Rows[iLoop]["vendor_id"].ToString();
                    dr["vendor_name"] = dt_Dist_DEPT.Rows[iLoop]["vendor_name"].ToString();
                    DataRow[] dr_count = dt_Top5Cust.Select("vendor_id='" + dt_Dist_DEPT.Rows[iLoop]["vendor_id"].ToString() + "'");
                    dr["payment_amt"] = CommonUtils.ConvertStringToDecimal(dt_Dist_DEPT.Rows[iLoop]["payment_amt"].ToString());
                    dt_Dept_List.Rows.Add(dr);
                }

                Session["FIVE_CUSTOMER_GRAPH"] = dt_Dept_List;

                LoadGraph();
            }
        }

        private void DisplayInvoiceReceivedDetails()
        {
            DataTable dt_InvRec = DBMethod.ExecuteQuery(Invoice_DAL.GetInvoiceReceivedAmt()).Tables[0];
            Session["INVOICE_RECEIVE_GRAPH_LOAD"] = dt_InvRec;
            if (dt_InvRec.Rows.Count > 0)
            {
                DataTable dt_Dist_DEPT = dt_InvRec.DefaultView.ToTable(true, "vendor_id", "vendor_name", "invoice_amt", "cust_inv_amt");

                DataTable dt_Dept_List = new DataTable();
                dt_Dept_List.Columns.Add("vendor_id");
                dt_Dept_List.Columns.Add("vendor_name");
                dt_Dept_List.Columns.Add("invoice_amt");
                dt_Dept_List.Columns.Add("cust_inv_amt");

                for (int iLoop = 0; iLoop < dt_Dist_DEPT.Rows.Count; iLoop++)
                {
                    DataRow dr = dt_Dept_List.NewRow();
                    dr["vendor_id"] = dt_Dist_DEPT.Rows[iLoop]["vendor_id"].ToString();
                    dr["vendor_name"] = dt_Dist_DEPT.Rows[iLoop]["vendor_name"].ToString();
                    dr["invoice_amt"] = DBMethod.GetAmtDecimalCommaSeparationValue(CommonUtils.ConvertStringToDecimal(dt_Dist_DEPT.Rows[iLoop]["invoice_amt"].ToString()).ToString());
                    dr["cust_inv_amt"] = DBMethod.GetAmtDecimalCommaSeparationValue(CommonUtils.ConvertStringToDecimal(dt_Dist_DEPT.Rows[iLoop]["cust_inv_amt"].ToString()).ToString());
                    dt_Dept_List.Rows.Add(dr);
                }
                gvInvoiceReceived.DataSource = dt_Dept_List;
                gvInvoiceReceived.DataBind();
                Session["INVOICE_RECEIVED_GRAPH"] = dt_Dept_List;

                LoadGraph();
            }
        }


        protected void lnk_InvoiceReceived_Click(object sender, EventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            LoadInvoiceReceivedDetails(gvr);
        }

        private void LoadInvoiceReceivedDetails(GridViewRow gvr)
        {
            DataTable dt_EmpDet = (DataTable)Session["INVOICE_RECEIVE_GRAPH_LOAD"];
            List<DataTable> tables = dt_EmpDet.AsEnumerable()
                .Where(r => r["vendor_id"].ToString().Contains(gvInvoiceReceived.DataKeys[gvr.RowIndex].Values["vendor_id"].ToString()))
                       .GroupBy(row => new
                       {
                           email = row.Field<string>("vendor_id"),
                           Name = row.Field<string>("vendor_name")
                       }).Select(g => System.Data.DataTableExtensions.CopyToDataTable(g)).ToList();

            DataTable dt_Dist_DEPT = tables[0].DefaultView.ToTable(true, "vendor_id", "vendor_name", "invoice_amt", "cust_inv_amt");


            DataTable dt_Dept_List = new DataTable();
            dt_Dept_List.Columns.Add("vendor_id");
            dt_Dept_List.Columns.Add("vendor_name");
            dt_Dept_List.Columns.Add("invoice_amt");
            dt_Dept_List.Columns.Add("cust_inv_amt");

            for (int iLoop = 0; iLoop < dt_Dist_DEPT.Rows.Count; iLoop++)
            {
                DataRow dr = dt_Dept_List.NewRow();
                dr["vendor_id"] = dt_Dist_DEPT.Rows[iLoop]["vendor_id"].ToString();
                dr["vendor_name"] = dt_Dist_DEPT.Rows[iLoop]["vendor_name"].ToString();
                dr["invoice_amt"] = DBMethod.GetAmtDecimalCommaSeparationValue(CommonUtils.ConvertStringToDecimal(dt_Dist_DEPT.Rows[iLoop]["invoice_amt"].ToString()).ToString());
                dr["cust_inv_amt"] = DBMethod.GetAmtDecimalCommaSeparationValue(CommonUtils.ConvertStringToDecimal(dt_Dist_DEPT.Rows[iLoop]["cust_inv_amt"].ToString()).ToString());
                dt_Dept_List.Rows.Add(dr);
            }
            gvInvoiceReceived.DataSource = dt_Dept_List;
            gvInvoiceReceived.DataBind();

            Session["EMP_DESIG_GRAP"] = dt_Dept_List;

            LoadGraph();

        }

        protected void imgBack_Click(object sender, ImageClickEventArgs e)
        {
            //divEmpDept.Visible = true;
            //// divEmpDesig.Visible = false;
            //Session["EMP_DESIG_GRAP"] = null;
            //LoadGraph();
        }

        protected void imgEmp_Click_Click(object sender, ImageClickEventArgs e)
        {
            //divEmpDept.Visible = false;
            ////divEmpDesig.Visible = true;
            ////divEmpDet.Visible = false;

            //LoadGraph();
        }


        protected void lnk_Desig_Name_Click(object sender, EventArgs e)
        {
            //GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            //LoadEMPDet(gvr);
        }

        private void LoadEMPDet(GridViewRow gvr)
        {
            //LoadGraph();
        }

        protected void ddlFinYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            //getEmpCount4FinYear();
        }

 
        protected void ddlPerFinYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            //FIN.BLL.GL.AccountingCalendar_BLL.GetFinancialPeriod4Year(ref ddlPerFinPeriod, ddlPerFinYear.SelectedValue);
            //LoadGraph();
        }

        protected void ddlPerFinPeriod_SelectedIndexChanged(object sender, EventArgs e)
        {
            //getEmployeePermitDetails();
            //LoadGraph();
        }

        

        //private void FillComboBox()
        //{
        //    FIN.BLL.AR.Customer_BLL.GetCustomerName(ref ddlCustomerName);
        //}

        //protected void btnGenerateChart_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        DataTable dtData = new DataTable();
        //        dtData = DBMethod.ExecuteQuery(FIN.DAL.AP.Invoice_DAL.getInvPaymentAmt(ddlCustomerName.SelectedValue.ToString(), txtFromDate.Text.ToString(), txtToDate.Text.ToString())).Tables[0];

        //        ChartAR.Series["ARInvoiceSeries"].ChartType = SeriesChartType.Column;
        //        ChartAR.Series["ARPaymentSeries"].ChartType = SeriesChartType.Column;
        //        ChartAR.Series["ARInvoiceSeries"]["DrawingStyle"] = "Default";
        //        ChartAR.Series["ARPaymentSeries"]["DrawingStyle"] = "Default";
        //        ChartAR.ChartAreas["ChartAreaAR"].Area3DStyle.Enable3D = false;
        //        ChartAR.Series["ARInvoiceSeries"].IsValueShownAsLabel = false;
        //        ChartAR.Series["ARPaymentSeries"].IsValueShownAsLabel = false;

        //        ChartAR.Series["ARInvoiceSeries"].BorderWidth = 2;
        //        ChartAR.Series["ARPaymentSeries"].BorderWidth = 2;
        //        //ChartAR.Series["Series1"].IsXValueIndexed = true;
        //        //ChartAR.Series["ARPaymentSeries"].IsXValueIndexed = true;

        //        ChartAR.Series["ARInvoiceSeries"]["StackedGroupName"] = "Group1";
        //        ChartAR.Series["ARPaymentSeries"]["StackedGroupName"] = "Group1";

        //        ChartAR.DataSource = dtData;
        //        ChartAR.DataBind();

        //        ChartAR.Series["ARInvoiceSeries"].XValueMember = "Vendor_Name";
        //        ChartAR.Series["ARInvoiceSeries"].YValueMembers = "Invoice_Amt";

        //        ChartAR.Series["ARPaymentSeries"].XValueMember = "Vendor_Name";
        //        ChartAR.Series["ARPaymentSeries"].YValueMembers = "Payment_Amt";

        //        ChartAR.Series["ARInvoiceSeries"].Color = System.Drawing.Color.Chocolate;
        //        ChartAR.Series["ARInvoiceSeries"].Legend = "Default";
        //        ChartAR.Series["ARInvoiceSeries"].LegendText = "Invoice Amount";

        //        ChartAR.Series["ARPaymentSeries"].Color = System.Drawing.Color.DarkGray;
        //        ChartAR.Series["ARPaymentSeries"].Legend = "Default";
        //        ChartAR.Series["ARPaymentSeries"].LegendText = "Payment Amount";


        //        ChartAR.Legends["Default"].BackColor = Color.Transparent;
        //        ChartAR.Legends["Default"].LegendStyle = LegendStyle.Column;
        //        ChartAR.Legends["Default"].ForeColor = Color.Black;
        //        ChartAR.Legends["Default"].Docking = Docking.Bottom;


        //        ChartAR.Legends["Default"].BorderColor = Color.Transparent;
        //        ChartAR.Legends["Default"].BackSecondaryColor = Color.Transparent;
        //        ChartAR.Legends["Default"].BackGradientStyle = GradientStyle.None;
        //        ChartAR.Legends["Default"].BorderColor = Color.Black;
        //        ChartAR.Legends["Default"].BorderWidth = 1;
        //        ChartAR.Legends["Default"].BorderDashStyle = ChartDashStyle.Solid;
        //        ChartAR.Legends["Default"].ShadowOffset = 1;

        //        //ChartAR.Series["Series1"]["PixelPointWidth"] = "50";
        //        //ChartAR.Series["Series2"]["PixelPointWidth"] = "50";


        //        ChartAR.BackColor = Color.Transparent;
        //        ChartAR.ChartAreas["ChartAreaAR"].AxisY = new System.Web.UI.DataVisualization.Charting.Axis { LabelStyle = new LabelStyle() { Font = new Font("Verdana", 8.0f, FontStyle.Regular) } };
        //        //ChartAR.ChartAreas["ChartAreaAR"].AxisY = new System.Web.UI.DataVisualization.Charting.Axis { LabelStyle = new LabelStyle() { ForeColor = Color.Blue } };

        //        ChartAR.ChartAreas["ChartAreaAR"].AxisX = new System.Web.UI.DataVisualization.Charting.Axis { LabelStyle = new LabelStyle() { Font = new Font("Verdana", 8.0f, FontStyle.Regular) } };
        //        //ChartAR.ChartAreas["ChartAreaAR"].AxisX = new System.Web.UI.DataVisualization.Charting.Axis { LabelStyle = new LabelStyle() { ForeColor = Color.Blue } };



        //        ChartAR.ChartAreas["ChartAreaAR"].Area3DStyle.Enable3D = false;
        //        ChartAR.ChartAreas["ChartAreaAR"].BackColor = Color.Transparent;
        //        ChartAR.ChartAreas["ChartAreaAR"].AxisX.MajorGrid.Enabled = false;
        //        ChartAR.ChartAreas["ChartAreaAR"].AxisY.MajorGrid.Enabled = true;
        //        ChartAR.ChartAreas["ChartAreaAR"].Position.Width = 100;
        //        ChartAR.ChartAreas["ChartAreaAR"].Position.Height = 100;


        //        ChartAR.ChartAreas["ChartAreaAR"].AxisX.IsMarginVisible = true;
        //        ChartAR.ChartAreas["ChartAreaAR"].Position.X = 6;
        //        ChartAR.ChartAreas["ChartAreaAR"].AxisX.Title = ".\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n.";


        //        ChartAR.ChartAreas["ChartAreaAR"].AxisY.IsStartedFromZero = true;
        //        ChartAR.ChartAreas["ChartAreaAR"].AxisY.Title = "Customer Invoice/Payment";
        //        ChartAR.ChartAreas["ChartAreaAR"].AxisX.Title = "Customers";
        //        ChartAR.ChartAreas["ChartAreaAR"].AxisY.LabelStyle.Format = "#,##,##0.#0";// "{900:C}";"#,##0;-#,##0;0"

        //        ChartAR.ChartAreas["ChartAreaAR"].AxisY.TitleFont = new Font("Verdana", 12, FontStyle.Regular);
        //        //ChartAR.ChartAreas["ChartAreaAR"].AxisY.TitleForeColor = Color.Blue;

        //        ChartAR.ChartAreas["ChartAreaAR"].AxisX.TitleFont = new Font("Verdana", 12, FontStyle.Regular);
        //        //ChartAR.ChartAreas["ChartAreaAR"].AxisX.TitleForeColor = Color.Blue;


        //        ChartAR.ChartAreas["ChartAreaAR"].AxisX.LabelStyle.Angle = -90;


        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //    finally
        //    {
        //        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerPath + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);
        //    }
        //}

        protected void btnSave_Click(object sender, EventArgs e)
        {

        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {

        }
      

        

    }
}