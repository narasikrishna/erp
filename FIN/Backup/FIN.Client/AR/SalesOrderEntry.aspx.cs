﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.BLL;
using FIN.BLL.AR;
using FIN.DAL.AR;
using FIN.BLL.AP;
using FIN.DAL.AP;
using FIN.BLL.AP;
using VMVServices.Web;

namespace FIN.Client.AR
{
    public partial class SalesOrderEntry : PageBase
    {
        OM_CUSTOMER_ORDER_HDR oM_CUSTOMER_ORDER_HDR = new OM_CUSTOMER_ORDER_HDR();
        OM_CUSTOMER_ORDER_DTL oM_CUSTOMER_ORDER_DTL = new OM_CUSTOMER_ORDER_DTL();
        DataTable dtGridData = new DataTable();
        Boolean bol_rowVisiable;
        Boolean savedBool;
        string ProReturn = null;
        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("GL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();

            //if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            //{
            //    VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
            //}

            //if (VMVServices.Web.Utils.AddAllowed != FINAppConstants.Y && Master.Mode == FINAppConstants.Add)
            //{
            //    btnSave.Visible = false;
            //}

        }

        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }


        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();

                EntityData = null;

                FillComboBox();


                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    oM_CUSTOMER_ORDER_HDR = FIN.BLL.AR.SalesOrder_BLL.getClassEntity(Master.StrRecordId);

                    EntityData = oM_CUSTOMER_ORDER_HDR;


                    txtOrderNumber.Text = oM_CUSTOMER_ORDER_HDR.OM_ORDER_NUM;
                    txtOrderDate.Text = DBMethod.ConvertDateToString(oM_CUSTOMER_ORDER_HDR.OM_ORDER_DATE.ToString());

                    ddlQuoteNumber.SelectedValue = oM_CUSTOMER_ORDER_HDR.QUOTE_HDR_ID;
                    fillcurrency();
                    ddlOrderType.SelectedValue = oM_CUSTOMER_ORDER_HDR.OM_ORDER_TYPE;
                    txtRemarks.Text = oM_CUSTOMER_ORDER_HDR.REMARKS;
                    ddlOrderStatus.SelectedValue = oM_CUSTOMER_ORDER_HDR.OM_ORDER_STATUS;
                    
                    if (oM_CUSTOMER_ORDER_HDR.QUOTE_HDR_ID != null)
                    {
                        FIN.BLL.AP.Supplier_BLL.GetCustomerBasedQuote(ref ddlCustomerName, ddlQuoteNumber.SelectedValue);
                    }


                    ddlCustomerName.SelectedValue = oM_CUSTOMER_ORDER_HDR.OM_CUST_ID;
                    SupplierBranch_BLL.fn_getSupplierSite4SupplierName(ref ddlCustomerSite, ddlCustomerName.SelectedValue);


                    //txtOrderAmount.Text = oM_CUSTOMER_ORDER_HDR.or
                    txtBillingAddress.Text = oM_CUSTOMER_ORDER_HDR.OM_ORDER_BILL_TO_ADDRESS;
                    txtShippingAddress.Text = oM_CUSTOMER_ORDER_HDR.OM_ORDER_SHIP_TO_ADDRESS;
                    txtShipmentDate.Text = DBMethod.ConvertDateToString(oM_CUSTOMER_ORDER_HDR.OM_SHIPMENT_DATE.ToString());
                    txtShipmentMethod.Text = oM_CUSTOMER_ORDER_HDR.REMARKS;
                    ddlOrderCurrency.SelectedValue = oM_CUSTOMER_ORDER_HDR.OM_ORDER_CURRENCY;
                    txtComments.Text = oM_CUSTOMER_ORDER_HDR.REMARKS;
                    ddlOrderStatus.SelectedValue = oM_CUSTOMER_ORDER_HDR.OM_ORDER_STATUS;
                    
                   // ddlCustomerName.Enabled = false;
                    ddlQuoteNumber.Enabled = false;

                    ddlCustomerSite.Enabled = false;
                    if (oM_CUSTOMER_ORDER_HDR.OM_VENDOR_LOC_ID != null)
                    {
                        ddlCustomerSite.SelectedValue = oM_CUSTOMER_ORDER_HDR.OM_VENDOR_LOC_ID.ToString();
                    }

                    txtExchangeRate.Text = DBMethod.GetAmtDecimalCommaSeparationValue(oM_CUSTOMER_ORDER_HDR.OM_EXCHANGE_RATE.ToString());

                    if (oM_CUSTOMER_ORDER_HDR.OM_DELIVERY_DATE != null)
                    {
                        txtDeliveryDate.Text = DBMethod.ConvertDateToString(oM_CUSTOMER_ORDER_HDR.OM_DELIVERY_DATE.ToString());
                    }
                 
                    //if (oM_CUSTOMER_ORDER_HDR.LOT_ID != null)
                    //{
                    //    ddlLotNumber.SelectedValue = oM_CUSTOMER_ORDER_HDR.LOT_ID.ToString();
                    //}
                }
                dtGridData = FIN.BLL.AR.SalesOrder_BLL.getSalesOrderDetails(Master.StrRecordId);
                BindGrid(dtGridData);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    oM_CUSTOMER_ORDER_HDR = (OM_CUSTOMER_ORDER_HDR)EntityData;
                }


                if (txtOrderDate.Text != string.Empty)
                {
                    oM_CUSTOMER_ORDER_HDR.OM_ORDER_DATE = DBMethod.ConvertStringToDate(txtOrderDate.Text.ToString());
                }

                oM_CUSTOMER_ORDER_HDR.OM_ORDER_NUM = txtOrderNumber.Text;
                oM_CUSTOMER_ORDER_HDR.QUOTE_HDR_ID = ddlQuoteNumber.SelectedValue;

                oM_CUSTOMER_ORDER_HDR.OM_ORDER_TYPE = ddlOrderType.SelectedValue;
                oM_CUSTOMER_ORDER_HDR.REMARKS = txtRemarks.Text;

                oM_CUSTOMER_ORDER_HDR.OM_ORDER_STATUS = ddlOrderStatus.SelectedValue;
                oM_CUSTOMER_ORDER_HDR.OM_CUST_ID = ddlCustomerName.SelectedValue;
                oM_CUSTOMER_ORDER_HDR.OM_ORDER_BILL_TO_ADDRESS = txtBillingAddress.Text;
                oM_CUSTOMER_ORDER_HDR.OM_ORDER_SHIP_TO_ADDRESS = txtShippingAddress.Text;

                oM_CUSTOMER_ORDER_HDR.OM_SHIPMENT_METHOD = txtShipmentMethod.Text;
                oM_CUSTOMER_ORDER_HDR.OM_ORDER_CURRENCY = ddlOrderCurrency.SelectedValue;
                oM_CUSTOMER_ORDER_HDR.OM_ORDER_STATUS = ddlOrderStatus.SelectedValue;
                oM_CUSTOMER_ORDER_HDR.OM_ORDER_COMMENTS = txtComments.Text;

                if (txtShipmentDate.Text != string.Empty)
                {
                    oM_CUSTOMER_ORDER_HDR.OM_SHIPMENT_DATE = DBMethod.ConvertStringToDate(txtShipmentDate.Text.ToString());
                }
                oM_CUSTOMER_ORDER_HDR.ENABLED_FLAG = FINAppConstants.Y;
                oM_CUSTOMER_ORDER_HDR.OM_ORDER_ORG_ID = VMVServices.Web.Utils.OrganizationID;

                oM_CUSTOMER_ORDER_HDR.OM_VENDOR_LOC_ID = ddlCustomerSite.SelectedValue.ToString();

                oM_CUSTOMER_ORDER_HDR.OM_EXCHANGE_RATE = CommonUtils.ConvertStringToDecimal(txtExchangeRate.Text);
                if (txtDeliveryDate.Text != string.Empty)
                {
                    oM_CUSTOMER_ORDER_HDR.OM_DELIVERY_DATE = DBMethod.ConvertStringToDate(txtDeliveryDate.Text.ToString());
                }
               // oM_CUSTOMER_ORDER_HDR.LOT_ID = ddlLotNumber.SelectedValue;
                
                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    oM_CUSTOMER_ORDER_HDR.MODIFIED_BY = this.LoggedUserName;
                    oM_CUSTOMER_ORDER_HDR.MODIFIED_DATE = DateTime.Today;
                }
                else
                {
                    oM_CUSTOMER_ORDER_HDR.OM_ORDER_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.AR_005_M.ToString(), false, true);
                    oM_CUSTOMER_ORDER_HDR.OM_ORDER_NUM = FINSP.GetSPFOR_SEQCode(FINAppConstants.AR_005_ON.ToString(), false, true);
                    oM_CUSTOMER_ORDER_HDR.CREATED_BY = this.LoggedUserName;
                    oM_CUSTOMER_ORDER_HDR.CREATED_DATE = DateTime.Today;
                }
                oM_CUSTOMER_ORDER_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, oM_CUSTOMER_ORDER_HDR.OM_ORDER_ID);

                //Save Detail Table


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }



                var tmpChildEntity = new List<Tuple<object, string>>();

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    oM_CUSTOMER_ORDER_DTL = new OM_CUSTOMER_ORDER_DTL();
                    if (dtGridData.Rows[iLoop][FINColumnConstants.OM_ORDER_DTL_ID].ToString() != "0" && dtGridData.Rows[iLoop][FINColumnConstants.OM_ORDER_DTL_ID].ToString() != string.Empty)
                    {
                        using (IRepository<OM_CUSTOMER_ORDER_DTL> userCtx = new DataRepository<OM_CUSTOMER_ORDER_DTL>())
                        {
                            oM_CUSTOMER_ORDER_DTL = userCtx.Find(r =>
                                (r.OM_ORDER_DTL_ID == dtGridData.Rows[iLoop][FINColumnConstants.OM_ORDER_DTL_ID].ToString())
                                ).SingleOrDefault();
                        }
                    }

                    if (dtGridData.Rows[iLoop][FINColumnConstants.OM_ORDER_ID].ToString() != "0")
                    {
                        FIN.BLL.AR.SalesOrder_BLL.getDetailClassEntity(dtGridData.Rows[iLoop][FINColumnConstants.OM_ORDER_DTL_ID].ToString());
                    }

                    oM_CUSTOMER_ORDER_DTL.OM_ORDER_DTL_ID = (dtGridData.Rows[iLoop][FINColumnConstants.OM_ORDER_DTL_ID].ToString());

                    oM_CUSTOMER_ORDER_DTL.OM_ORDER_LINE_NUM = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop][FINColumnConstants.OM_ORDER_LINE_NUM].ToString());
                    oM_CUSTOMER_ORDER_DTL.OM_ITEM_ID = dtGridData.Rows[iLoop][FINColumnConstants.ITEM_ID].ToString();
                    oM_CUSTOMER_ORDER_DTL.OM_ORDER_DESC = dtGridData.Rows[iLoop][FINColumnConstants.OM_ORDER_DESC].ToString();
                    oM_CUSTOMER_ORDER_DTL.OM_ORDER_QTY = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop][FINColumnConstants.OM_ORDER_QTY].ToString());
                    oM_CUSTOMER_ORDER_DTL.OM_ORDER_UOM = dtGridData.Rows[iLoop][FINColumnConstants.UOM_NAME].ToString();
                    oM_CUSTOMER_ORDER_DTL.OM_ORDER_PRICE = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop][FINColumnConstants.OM_ORDER_PRICE].ToString());


                    if (dtGridData.Rows[iLoop][FINColumnConstants.ENABLED_FLAG].ToString().ToUpper() == "TRUE")
                    {
                        oM_CUSTOMER_ORDER_DTL.ENABLED_FLAG = FINAppConstants.Y;
                    }
                    else
                    {
                        oM_CUSTOMER_ORDER_DTL.ENABLED_FLAG = FINAppConstants.N;
                    }


                    oM_CUSTOMER_ORDER_DTL.OM_ORDER_ID = oM_CUSTOMER_ORDER_HDR.OM_ORDER_ID;

                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        oM_CUSTOMER_ORDER_DTL.OM_ORDER_DTL_ID = dtGridData.Rows[iLoop][FINColumnConstants.OM_ORDER_DTL_ID].ToString();
                        tmpChildEntity.Add(new Tuple<object, string>(oM_CUSTOMER_ORDER_DTL, FINAppConstants.Delete));
                    }
                    else
                    {
                        if (dtGridData.Rows[iLoop][FINColumnConstants.OM_ORDER_DTL_ID].ToString() != "0" && dtGridData.Rows[iLoop][FINColumnConstants.OM_ORDER_DTL_ID].ToString() != string.Empty)
                        {
                            oM_CUSTOMER_ORDER_DTL.OM_ORDER_DTL_ID = dtGridData.Rows[iLoop][FINColumnConstants.OM_ORDER_DTL_ID].ToString();
                            oM_CUSTOMER_ORDER_DTL.MODIFIED_DATE = DateTime.Today;
                            oM_CUSTOMER_ORDER_DTL.MODIFIED_BY = this.LoggedUserName;
                            oM_CUSTOMER_ORDER_DTL.WORKFLOW_COMPLETION_STATUS = "1";// FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, oM_CUSTOMER_ORDER_DTL.OM_ORDER_DTL_ID);

                            tmpChildEntity.Add(new Tuple<object, string>(oM_CUSTOMER_ORDER_DTL, FINAppConstants.Update));
                        }
                        else
                        {
                            oM_CUSTOMER_ORDER_DTL.OM_ORDER_DTL_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.AR_005_M.ToString(), false, true);
                            oM_CUSTOMER_ORDER_DTL.CREATED_BY = this.LoggedUserName;
                            oM_CUSTOMER_ORDER_DTL.CREATED_DATE = DateTime.Today;
                            oM_CUSTOMER_ORDER_DTL.WORKFLOW_COMPLETION_STATUS = "1";// FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, oM_CUSTOMER_ORDER_DTL.OM_ORDER_DTL_ID);

                            tmpChildEntity.Add(new Tuple<object, string>(oM_CUSTOMER_ORDER_DTL, FINAppConstants.Add));
                        }
                    }
                }

                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            CommonUtils.SavePCEntity<OM_CUSTOMER_ORDER_HDR, OM_CUSTOMER_ORDER_DTL>(oM_CUSTOMER_ORDER_HDR, tmpChildEntity, oM_CUSTOMER_ORDER_DTL);
                            savedBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {
                            CommonUtils.SavePCEntity<OM_CUSTOMER_ORDER_HDR, OM_CUSTOMER_ORDER_DTL>(oM_CUSTOMER_ORDER_HDR, tmpChildEntity, oM_CUSTOMER_ORDER_DTL, true);
                            savedBool = true;
                            break;
                        }

                }
                if (ErrorCollection.Count > 0)
                {
                    return;
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        private void FillComboBox()
        {
            try
            {
                ErrorCollection.Clear();
                FIN.BLL.AP.Supplier_BLL.GetCustomerNameDetails(ref ddlCustomerName);
                FIN.BLL.AR.SalesQuote_BLL.fn_getSalesQuote(ref ddlQuoteNumber);
                Lookup_BLL.GetLookUpValues(ref ddlOrderType, "OT");
                Lookup_BLL.GetLookUpValues(ref ddlOrderStatus, "OS");

                FIN.BLL.GL.Currency_BLL.getCurrencyDesc(ref ddlOrderCurrency);
              //  ReceiptLotDetails_BLL.GetLotNumber(ref ddlLotNumber);
               
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Salesorder", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        protected void ddlSupplierName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                SupplierBranch_BLL.fn_getSupplierSite4SupplierName(ref ddlCustomerSite, ddlCustomerName.SelectedValue);


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void ddlQuoteNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                fillcurrency();

                dtGridData = DBMethod.ExecuteQuery(SalesQuote_DAL.GetSalesQuoteBasedQN(ddlQuoteNumber.SelectedValue)).Tables[0];
                BindGrid(dtGridData);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Salesorder", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void fillcurrency()
        {
            FIN.BLL.AR.SalesOrder_BLL.fn_getCurrency(ref ddlOrderCurrency, ddlQuoteNumber.SelectedValue);
            FIN.BLL.AP.Supplier_BLL.GetCustomerBasedQuote(ref ddlCustomerName, ddlQuoteNumber.SelectedValue);
        }

        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddlItemName = tmpgvr.FindControl("ddlItemName") as DropDownList;
                FIN.BLL.AP.Item_BLL.GetItemName(ref ddlItemName);

                //DropDownList ddlUOM = tmpgvr.FindControl("ddlUOM") as DropDownList;
                //FIN.BLL.AP.Item_BLL.getUOM_frSO(ref ddlUOM);

                if (gvData.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                {
                 //   ddlUOM.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.UOM_CODE].ToString();
                    ddlItemName.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.ITEM_ID].ToString();
                }


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                if (dtData.Rows.Count > 0)
                {

                    dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("OM_ORDER_PRICE", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("OM_ORDER_PRICE"))));

                    dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("AMOUNT", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("AMOUNT"))));
                    dtData.AcceptChanges();
                }
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr["ENABLED_FLAG"] = "FALSE";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);

                txtOrderAmount.Text = FIN.BLL.CommonUtils.CalculateTotalAmount(dtData, "AMOUNT");

                if (txtOrderAmount.Text.Length > 0)
                {
                    txtOrderAmount.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtOrderAmount.Text);
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("SalesOrderEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            if (Session[FINSessionConstants.GridData] != null)
            {
                dtGridData = (DataTable)Session[FINSessionConstants.GridData];
            }
            gvData.EditIndex = -1;

            BindGrid(dtGridData);

        }
        #endregion

        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CalendarEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {


            System.Collections.SortedList slControls = new System.Collections.SortedList();
            DropDownList ddlItemName = gvr.FindControl("ddlItemName") as DropDownList;
           // DropDownList ddlUOM = gvr.FindControl("ddlUOM") as DropDownList;
            TextBox txtLineNo = gvr.FindControl("txtLineNo") as TextBox;
            TextBox txtuom = gvr.FindControl("txtuom") as TextBox;
            TextBox txtDescription = gvr.FindControl("txtDescription") as TextBox;
            TextBox txtQuantity = gvr.FindControl("txtQuantity") as TextBox;
            TextBox txtAmount = gvr.FindControl("txtAmount") as TextBox;
            TextBox txtUnitPrice = gvr.FindControl("txtUnitPrice") as TextBox;
            
            CheckBox chkact = gvr.FindControl("chkact") as CheckBox;

            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList[FINColumnConstants.OM_ORDER_DTL_ID] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }
            txtLineNo.Text = (rowindex + 1).ToString();


            slControls[0] = ddlItemName;
            slControls[1] = txtuom;
            slControls[2] = txtLineNo;
            slControls[3] = txtQuantity;
            slControls[4] = txtAmount;

            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/AR_" + Session["Sel_Lng"].ToString() + ".properties"));
            ErrorCollection.Clear();
            string strCtrlTypes = FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX;
            string strMessage = Prop_File_Data["Item_P"] + " ~ " + Prop_File_Data["UOM_P"] + " ~ " + Prop_File_Data["Line_No_P"] + " ~ " + Prop_File_Data["Quantity_P"] + " ~ " + Prop_File_Data["Amount_P"] + "";
            //string strMessage = "Item Name ~ UOM ~ Line Number ~ Quantity ~ Amount ";

            EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

            if (EmptyErrorCollection.Count > 0)
            {
                ErrorCollection = EmptyErrorCollection;
                return drList;
            }


            //   ErrorCollection = UserUtility_BLL.DateRangeValidate(DBMethod.ConvertStringToDate(dtp_StartDate.Text), DBMethod.ConvertStringToDate(dtp_EndDate.Text), Master.Mode);
            if (ErrorCollection.Count > 0)
                return drList;


            string strCondition = "ITEM_ID='" + ddlItemName.SelectedValue.ToString() + "'";
            strMessage = FINMessageConstatns.RecordAlreadyExists;
            ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            if (ErrorCollection.Count > 0)
            {
                return drList;
            }




            drList[FINColumnConstants.OM_ORDER_LINE_NUM] = txtLineNo.Text;
            drList[FINColumnConstants.ITEM_ID] = ddlItemName.SelectedValue.ToString();
            drList[FINColumnConstants.ITEM_NAME] = ddlItemName.SelectedItem.Text;
            drList[FINColumnConstants.OM_ORDER_DESC] = txtDescription.Text;
            drList[FINColumnConstants.OM_ORDER_QTY] = txtQuantity.Text;
            //drList[FINColumnConstants.UOM_CODE] = ddlUOM.SelectedValue.ToString();
            //drList["uom_name"] = ddlUOM.SelectedItem.Text;
            drList["UOM_NAME"] = txtuom.Text;
            drList["AMOUNT"] = txtAmount.Text;
            drList[FINColumnConstants.OM_ORDER_PRICE] = txtUnitPrice.Text;
           // drList[FINColumnConstants.UOM_NAME] = ddlUOM.SelectedItem.Text;

            // drList[FINColumnConstants.EFFECTIVE_END_DT] = DBMethod.ConvertStringToDate(dtp_EndDate.Text.ToString());

            if (chkact.Checked)
            {
                drList[FINColumnConstants.ENABLED_FLAG] = "TRUE";
            }
            else
            {
                drList[FINColumnConstants.ENABLED_FLAG] = "FALSE";
            }

            return drList;

        }
        protected void txtQuantity_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                fn_UnitPricetxtChanged();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void fn_UnitPricetxtChanged()
        {
            try
            {
                ErrorCollection.Clear();

                TextBox txtUnitPrice = new TextBox();
                TextBox txtQuantity = new TextBox();
                TextBox txtAmount = new TextBox();

                txtUnitPrice.ID = "txtUnitPrice";
                txtQuantity.ID = "txtQuantity";
                txtAmount.ID = "txtAmount";

                Label lblUnitPrice = new Label();
                Label lblQuantity = new Label();
                Label lblAmount = new Label();

                lblUnitPrice.ID = "lblUnitPrice";
                lblQuantity.ID = "lblQuantity";
                lblAmount.ID = "lblAmount";

                if (gvData.FooterRow != null)
                {
                    if (gvData.EditIndex < 0)
                    {
                        txtUnitPrice = (TextBox)gvData.FooterRow.FindControl("txtUnitPrice");
                        txtQuantity = (TextBox)gvData.FooterRow.FindControl("txtQuantity");
                        txtAmount = (TextBox)gvData.FooterRow.FindControl("txtAmount");

                        lblUnitPrice = (Label)gvData.FooterRow.FindControl("lblUnitPrice");
                        lblQuantity = (Label)gvData.FooterRow.FindControl("lblQuantity");
                        lblAmount = (Label)gvData.FooterRow.FindControl("lblAmount");
                    }
                    else
                    {
                        txtUnitPrice = (TextBox)gvData.Rows[gvData.EditIndex].FindControl("txtUnitPrice");
                        txtQuantity = (TextBox)gvData.Rows[gvData.EditIndex].FindControl("txtQuantity");
                        txtAmount = (TextBox)gvData.Rows[gvData.EditIndex].FindControl("txtAmount");

                        lblUnitPrice = (Label)gvData.Rows[gvData.EditIndex].FindControl("lblUnitPrice");
                        lblQuantity = (Label)gvData.Rows[gvData.EditIndex].FindControl("lblQuantity");
                        lblAmount = (Label)gvData.Rows[gvData.EditIndex].FindControl("lblAmount");
                    }
                }
                else
                {
                    txtUnitPrice = (TextBox)gvData.Controls[0].Controls[1].FindControl("txtUnitPrice");
                    txtQuantity = (TextBox)gvData.Controls[0].Controls[1].FindControl("txtQuantity");
                    txtAmount = (TextBox)gvData.Controls[0].Controls[1].FindControl("txtAmount");

                    lblUnitPrice = (Label)gvData.Controls[0].Controls[1].FindControl("lblUnitPrice");
                    lblQuantity = (Label)gvData.Controls[0].Controls[1].FindControl("lblQuantity");
                    lblAmount = (Label)gvData.Controls[0].Controls[1].FindControl("lblAmount");
                }

                if (txtUnitPrice != null && txtQuantity != null)
                {
                    if (txtUnitPrice.Text.Trim() != string.Empty && txtQuantity.Text.Trim() != string.Empty)
                    {
                        if (double.Parse(txtUnitPrice.Text.ToString()) > 0 && double.Parse(txtQuantity.Text.ToString()) > 0)
                        {
                            txtAmount.Text = (CommonUtils.ConvertStringToDecimal(txtUnitPrice.Text) * CommonUtils.ConvertStringToDecimal(txtQuantity.Text)).ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("SalesOrderEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("SalesOrdrEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("SalesOrderEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("SalesOrderEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Cells[3].HorizontalAlign = HorizontalAlign.Right;
                    e.Row.Cells[5].HorizontalAlign = HorizontalAlign.Right;
                    e.Row.Cells[6].HorizontalAlign = HorizontalAlign.Right;
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("SalesOrderEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }



        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>


        #endregion




        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();

                DBMethod.DeleteEntity<OM_CUSTOMER_ORDER_HDR>(oM_CUSTOMER_ORDER_HDR);
                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATADELETED);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("SalesOrderEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                System.Collections.SortedList slControls = new System.Collections.SortedList();

                //slControls[0] = txtOrderNumber;
                slControls[0] = txtOrderDate;
              //  slControls[2] = ddlQuoteNumber;
                slControls[1] = ddlCustomerName;

                Dictionary<string, string> Prop_File_Data;
                Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/AR_" + Session["Sel_Lng"].ToString() + ".properties"));
                ErrorCollection.Clear();

                string strCtrlTypes =  FINAppConstants.DATE_TIME + "~" + FINAppConstants.DROP_DOWN_LIST;
                string strMessage =  Prop_File_Data["Order_Date_P"] +  " ~ " + Prop_File_Data["Customer_Name_P"] + "";
           

                //string strCtrlTypes = FINAppConstants.TEXT_BOX + "~" + FINAppConstants.DATE_TIME + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST;
                //string strMessage = Prop_File_Data["Order_Number_P"] + " ~ " + Prop_File_Data["Order_Date_P"] + " ~ " + Prop_File_Data["Quote_Number_P"] + " ~ " + Prop_File_Data["Customer_Name_P"] + "";
                //string strMessage = "Order Number ~ Order Date ~ Quote Number ~ Customer Name";

                EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

                if (EmptyErrorCollection.Count > 0)
                {
                    ErrorCollection = EmptyErrorCollection;
                    return;
                }
                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                ErrorCollection = CommonUtils.IsEmptyGrid(dtGridData, "Sales Order ");

                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                AssignToBE();

                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        protected void ddlItemName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;

                DropDownList ddlItemName = gvr.FindControl("ddlItemName") as DropDownList;
                TextBox txtUnitPrice = gvr.FindControl("txtUnitPrice") as TextBox;
                DataTable dtunitprice = new DataTable();
                dtunitprice = DBMethod.ExecuteQuery(PurchaseOrder_DAL.GetItemUnitprice(ddlItemName.SelectedValue)).Tables[0];
                txtUnitPrice.Text = dtunitprice.Rows[0]["ITEM_UNIT_PRICE"].ToString();

                fn_UnitPricetxtChanged();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void txtShipmentMethod_TextChanged(object sender, EventArgs e)
        {

        }


    }
        #endregion
}