﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.AP;
using FIN.DAL.AR;
using FIN.BLL;
using FIN.BLL.GL;
using FIN.BLL.AP;
using FIN.BLL.AR;
using FIN.BLL.HR;
using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;
namespace FIN.Client.AR
{
    public partial class SalesQuoteEntry : PageBase
    {
        //PO_HEADERS PO_HEADERS = new PO_HEADERS(); 
        //PO_LINES PO_LINES = new PO_LINES();
        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();
        OM_SALE_QUOTE_HDR oM_SALE_QUOTE_HDR = new OM_SALE_QUOTE_HDR();
        OM_SALE_QUOTE_DTL oM_SALE_QUOTE_DTL = new OM_SALE_QUOTE_DTL();
        DataTable dtGridData = new DataTable();
        Boolean bol_rowVisiable;
        Boolean savedBool;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));

            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();
            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }
            UserRightsChecking();

        }
        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }
        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>
        private void FillComboBox()
        {

            FIN.BLL.AP.Supplier_BLL.GetCustomerNameDetails(ref ddlCustomerName);
            Lookup_BLL.GetLookUpValues(ref ddlQuoteType, "IS");
            Lookup_BLL.GetLookUpValues(ref ddlstatus, "STS");
            Currency_BLL.getCurrencyDetails(ref ddlCurrency);
            SalesQuote_BLL.fn_getPayterm(ref ddlPaymentTerm);
            Lookup_BLL.GetLookUpValues(ref ddlShippingInstruction, "SHIPINSTR");
            Lookup_BLL.GetLookUpValues(ref ddlCarrierService, "CARSER");

            Lookup_BLL.GetLookUpValues(ref ddlModeofTransport, "MOT");
        }
        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>
        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();

                EntityData = null;
                Session[FINSessionConstants.GridData] = null;

                dtGridData = DBMethod.ExecuteQuery(SalesQuote_DAL.GetSalesQuoteDetails(Master.StrRecordId)).Tables[0];

                btnPrint.Visible = false;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    btnPrint.Visible = true;
                    oM_SALE_QUOTE_HDR = SalesQuote_BLL.getClassEntity(Master.StrRecordId);

                    EntityData = oM_SALE_QUOTE_HDR;


                    txtQuoteNumber.Text = oM_SALE_QUOTE_HDR.QUOTE_HDR_ID;
                    txtQuoteDate.Text = DBMethod.ConvertDateToString(oM_SALE_QUOTE_HDR.QUOTE_DATE.ToString());
                    if (oM_SALE_QUOTE_HDR.QUOTE_TYPE != null)
                    {
                        ddlQuoteType.SelectedValue = oM_SALE_QUOTE_HDR.QUOTE_TYPE.ToString();

                    }
                    if (oM_SALE_QUOTE_HDR.QUOTE_CUST_ID != null)
                    {
                        ddlCustomerName.SelectedValue = oM_SALE_QUOTE_HDR.QUOTE_CUST_ID.ToString();
                        FIN.BLL.AR.Customer_Branch_BLL.GetCustomerBranch(ref  ddlSupplierSite, ddlCustomerName.SelectedValue);
                        if (oM_SALE_QUOTE_HDR.ATTRIBUTE1 != null)
                        {
                            ddlSupplierSite.SelectedValue = oM_SALE_QUOTE_HDR.ATTRIBUTE1.ToString();
                        }
                    }
                    if (oM_SALE_QUOTE_HDR.QUOTE_REQ_DATE != null)
                    {
                        txtRequestDate.Text = DBMethod.ConvertDateToString(oM_SALE_QUOTE_HDR.QUOTE_REQ_DATE.ToString());
                    }
                    if (oM_SALE_QUOTE_HDR.QUOTE_EXPIRE_DATE != null)
                    {
                        txtExpireDate.Text = DBMethod.ConvertDateToString(oM_SALE_QUOTE_HDR.QUOTE_EXPIRE_DATE.ToString());
                    }
                    txtDescription.Text = oM_SALE_QUOTE_HDR.QUOTE_NAME;
                    if (oM_SALE_QUOTE_HDR.QUOTE_CURRENCY != null)
                    {
                        ddlCurrency.SelectedValue = oM_SALE_QUOTE_HDR.QUOTE_CURRENCY;
                    }
                    if (oM_SALE_QUOTE_HDR.QUOTE_STATUS != null)
                    {
                        ddlstatus.SelectedValue = oM_SALE_QUOTE_HDR.QUOTE_STATUS;
                    }
                    if (oM_SALE_QUOTE_HDR.QUOTE_PAY_TERM_ID != null)
                    {
                        ddlPaymentTerm.SelectedValue = oM_SALE_QUOTE_HDR.QUOTE_PAY_TERM_ID;
                    }
                    if (oM_SALE_QUOTE_HDR.QUOTE_SHIP_INSTRUCTION != null)
                    {
                        ddlShippingInstruction.SelectedValue = oM_SALE_QUOTE_HDR.QUOTE_SHIP_INSTRUCTION;
                    }
                    if (oM_SALE_QUOTE_HDR.QUOTE_CARRIER_SERVICE != null)
                    {
                        ddlCarrierService.SelectedValue = oM_SALE_QUOTE_HDR.QUOTE_CARRIER_SERVICE;
                    }
                    txtNote.Text = oM_SALE_QUOTE_HDR.QUOTE_NOTE;


                    txtTotal.Text = DBMethod.GetAmtDecimalCommaSeparationValue(oM_SALE_QUOTE_HDR.QUOTE_AMOUNT.ToString());

                    if (oM_SALE_QUOTE_HDR.OM_EXPECTED_DELIVERY_DT != null)
                    {
                        txtDeleiveryDate.Text = DBMethod.ConvertDateToString(oM_SALE_QUOTE_HDR.OM_EXPECTED_DELIVERY_DT.ToString());
                    }
                    if (oM_SALE_QUOTE_HDR.OM_DELIVERY_MODE != null)
                    {
                        ddlModeofTransport.SelectedValue = oM_SALE_QUOTE_HDR.OM_DELIVERY_MODE.ToString();
                    }
                }
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                if (dtData.Rows.Count > 0)
                {
                    if (dtData.Rows[0]["QUOTE_LINE_AMT"].ToString().Length > 0)
                    {
                        dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("QUOTE_LINE_AMT", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("QUOTE_LINE_AMT"))));
                    }
                    if (dtData.Rows[0]["QUOTE_ITEM_UNIT_PRICE"].ToString().Length > 0)
                    {
                        dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("QUOTE_ITEM_UNIT_PRICE", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("QUOTE_ITEM_UNIT_PRICE"))));
                    }
                    dtData.AcceptChanges();
                }
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);

                //txtTotal.Text = DBMethod.GetAmtDecimalCommaSeparationValue(oM_SALE_QUOTE_HDR.QUOTE_AMOUNT.ToString());
                txtTotal.Text = CommonUtils.CalculateTotalAmount(dtData, "QUOTE_LINE_AMT");
                if (txtTotal.Text.Length > 0)
                {
                    txtTotal.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtTotal.Text);
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("SQEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddlItemName = tmpgvr.FindControl("ddlItemName") as DropDownList;
                //   DropDownList ddlUOM = tmpgvr.FindControl("ddlUOM") as DropDownList;

                //TextBox txtUnitPrice = tmpgvr.FindControl("txtUnitPrice") as TextBox;
                //TextBox txtQuantity = tmpgvr.FindControl("txtQuantity") as TextBox;
                TextBox txtAmount = tmpgvr.FindControl("txtAmount") as TextBox;


                Item_BLL.GetItemName(ref ddlItemName, ddlQuoteType.SelectedValue);
                //  UOMMasters_BLL.GetUOMDetails(ref ddlUOM);



                if (gvData.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                {
                    ddlItemName.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.ITEM_ID].ToString();
                    //  ddlUOM.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.UOM_ID].ToString();

                    Session["Poeditvalue"] = txtAmount.Text;


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("SQ_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            if (Session[FINSessionConstants.GridData] != null)
            {
                dtGridData = (DataTable)Session[FINSessionConstants.GridData];
            }
            gvData.EditIndex = -1;

            BindGrid(dtGridData);

        }
        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {
            System.Collections.SortedList slControls = new System.Collections.SortedList();

            TextBox txtLineNo = gvr.FindControl("txtLineNo") as TextBox;
            TextBox txtQuantity = gvr.FindControl("txtQuantity") as TextBox;
            TextBox txtUnitPrice = gvr.FindControl("txtUnitPrice") as TextBox;
            TextBox txtAmount = gvr.FindControl("txtAmount") as TextBox;
            TextBox txtUOM = gvr.FindControl("txtUOM") as TextBox;

            DropDownList ddlItemName = gvr.FindControl("ddlItemName") as DropDownList;
            // DropDownList ddlAmount = gvr.FindControl("ddlAmount") as DropDownList;
            DropDownList ddlUOM = gvr.FindControl("ddlUOM") as DropDownList;


            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList[FINColumnConstants.QUOTE_DTL_ID] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }

            txtLineNo.Text = (rowindex + 1).ToString();

            slControls[0] = txtLineNo;
            slControls[1] = ddlItemName;
            slControls[2] = txtQuantity;
            slControls[3] = txtUOM;
            slControls[4] = txtUnitPrice;

            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/AR_" + Session["Sel_Lng"].ToString() + ".properties"));
            ErrorCollection.Clear();
            string strCtrlTypes = FINAppConstants.TEXT_BOX + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX;
            string strMessage = Prop_File_Data["Line_No_P"] + " ~ " + Prop_File_Data["Item_Name_P"] + " ~ " + Prop_File_Data["Quantity_P"] + " ~ " + Prop_File_Data["UOM_P"] + " ~ " + Prop_File_Data["Unit_Price_P"] + "";
            //string strMessage = "Line No ~ Item Name ~ Quantity ~ UOM ~ Unit Price";

            EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

            if (EmptyErrorCollection.Count > 0)
            {
                ErrorCollection = EmptyErrorCollection;
                return drList;
            }

            string strCondition = "ITEM_ID='" + ddlItemName.SelectedValue.ToString() + "'";
            strMessage = FINMessageConstatns.RecordAlreadyExists;
            ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            if (ErrorCollection.Count > 0)
            {
                return drList;
            }
            ErrorCollection.Remove("amterror");

            if (CommonUtils.ConvertStringToDecimal(txtAmount.Text) == 0)
            {
                ErrorCollection.Add("amterror", "Amount cannot be empty");
            }
            if (ErrorCollection.Count > 0)
            {
                return drList;
            }
            // txtPOTotalAmount.Text = PurchaseOrder_BLL.CalculateAmount(dtGridData, CommonUtils.ConvertStringToDecimal(txtPOAmount.Text));


            drList["QUOTE_LINE_NUM"] = txtLineNo.Text;
            drList["ITEM_ID"] = ddlItemName.SelectedValue.ToString();
            drList[FINColumnConstants.ITEM_NAME] = ddlItemName.SelectedItem.Text.ToString();

            drList["QUOTE_ITEM_QTY"] = txtQuantity.Text;
            // drList["UOM_ID"] = ddlUOM.SelectedValue.ToString();
            //drList["UOM_NAME"] = ddlUOM.SelectedItem.Text.ToString();
            drList["QUOTE_ITEM_UOM"] = txtUOM.Text;
            drList["QUOTE_ITEM_UNIT_PRICE"] = txtUnitPrice.Text;

            //drList[FINColumnConstants.PO_REQ_LINE_NUM] = txtLineNo.Text;
            //drList[FINColumnConstants.ITEM_ID] = ddlItemName.SelectedValue.ToString();
            //drList[FINColumnConstants.PO_DESCRIPTION] = txtDescription.Text;
            //drList[FINColumnConstants.PO_QUANTITY] = txtQuantity.Text;
            //drList[FINColumnConstants.PO_REQ_UOM] = ddlUOM.SelectedValue.ToString();
            //drList[FINColumnConstants.PO_ITEM_UNIT_PRICE] = txtUnitPrice.Text;
            drList["QUOTE_LINE_AMT"] = txtAmount.Text;

            drList[FINColumnConstants.DELETED] = FINAppConstants.N;


            return drList;

        }

        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("SQEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];

                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    //e.Row.Cells[1].HorizontalAlign = HorizontalAlign.Right;
                    //e.Row.Cells[3].HorizontalAlign = HorizontalAlign.Right;
                    //e.Row.Cells[4].HorizontalAlign = HorizontalAlign.Right;
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        #endregion

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                System.Collections.SortedList slControls = new System.Collections.SortedList();

                slControls[0] = txtQuoteDate;
                slControls[1] = ddlQuoteType;
                slControls[2] = ddlCustomerName;
                slControls[3] = txtDescription;
                slControls[4] = ddlstatus;
                Dictionary<string, string> Prop_File_Data;
                Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/AR_" + Session["Sel_Lng"].ToString() + ".properties"));
                ErrorCollection.Clear();

                string strCtrlTypes = FINAppConstants.DATE_TIME + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.DROP_DOWN_LIST;
                string strMessage = Prop_File_Data["Quote_Date_P"] + " ~ " + Prop_File_Data["Quote_Type_P"] + " ~ " + Prop_File_Data["Customer_Name_P"] + " ~ " + Prop_File_Data["Description_P"] + " ~ " + Prop_File_Data["Status_P"] + "";
                //string strMessage = "Quote Date ~ Quote Type ~ Customer Name ~ Description ~ Status";

                EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

                if (EmptyErrorCollection.Count > 0)
                {
                    ErrorCollection = EmptyErrorCollection;
                    return;
                }
                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                AssignToBE();
                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    oM_SALE_QUOTE_HDR = (OM_SALE_QUOTE_HDR)EntityData;
                }


                // oM_SALE_QUOTE_HDR.QUOTE_HDR_ID = txtQuoteNumber.Text;
                if (txtQuoteDate.Text != string.Empty)
                {
                    oM_SALE_QUOTE_HDR.QUOTE_DATE = DBMethod.ConvertStringToDate(txtQuoteDate.Text.ToString());
                }
                oM_SALE_QUOTE_HDR.QUOTE_TYPE = ddlQuoteType.SelectedValue;
                oM_SALE_QUOTE_HDR.QUOTE_NAME = txtDescription.Text;
                oM_SALE_QUOTE_HDR.QUOTE_CUST_ID = ddlCustomerName.SelectedValue;
                oM_SALE_QUOTE_HDR.ATTRIBUTE1 = ddlSupplierSite.SelectedValue;

                oM_SALE_QUOTE_HDR.QUOTE_STATUS = ddlstatus.SelectedValue;

                if (txtRequestDate.Text != string.Empty)
                {
                    oM_SALE_QUOTE_HDR.QUOTE_REQ_DATE = DBMethod.ConvertStringToDate(txtRequestDate.Text.ToString());
                }

                if (txtExpireDate.Text != string.Empty)
                {
                    oM_SALE_QUOTE_HDR.QUOTE_EXPIRE_DATE = DBMethod.ConvertStringToDate(txtExpireDate.Text.ToString());
                }

                oM_SALE_QUOTE_HDR.QUOTE_CURRENCY = ddlCurrency.SelectedValue;
                oM_SALE_QUOTE_HDR.QUOTE_PAY_TERM_ID = ddlPaymentTerm.SelectedValue;
                oM_SALE_QUOTE_HDR.QUOTE_SHIP_INSTRUCTION = ddlShippingInstruction.SelectedValue;
                oM_SALE_QUOTE_HDR.QUOTE_CARRIER_SERVICE = ddlCarrierService.SelectedValue;
                oM_SALE_QUOTE_HDR.QUOTE_NOTE = txtNote.Text;
                oM_SALE_QUOTE_HDR.QUOTE_AMOUNT = CommonUtils.ConvertStringToDecimal(txtTotal.Text);
                oM_SALE_QUOTE_HDR.OM_DELIVERY_MODE = ddlModeofTransport.SelectedValue;
                if (txtDeleiveryDate.Text != string.Empty)
                {
                    oM_SALE_QUOTE_HDR.OM_EXPECTED_DELIVERY_DT = DBMethod.ConvertStringToDate(txtDeleiveryDate.Text.ToString());
                }

                oM_SALE_QUOTE_HDR.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                oM_SALE_QUOTE_HDR.QUOTE_ORG_ID = VMVServices.Web.Utils.OrganizationID;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    oM_SALE_QUOTE_HDR.MODIFIED_BY = this.LoggedUserName;
                    oM_SALE_QUOTE_HDR.MODIFIED_DATE = DateTime.Today;
                }
                else
                {
                    oM_SALE_QUOTE_HDR.QUOTE_HDR_ID = FINSP.GetSPFOR_SEQCode("AR_004_M".ToString(), false, true);
                    oM_SALE_QUOTE_HDR.CREATED_BY = this.LoggedUserName;
                    oM_SALE_QUOTE_HDR.CREATED_DATE = DateTime.Today;
                }
                oM_SALE_QUOTE_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, oM_SALE_QUOTE_HDR.QUOTE_HDR_ID);

                //Save Detail Table


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                ErrorCollection = CommonUtils.IsEmptyGrid(dtGridData, "Sales Quote ");
                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                var tmpChildEntity = new List<Tuple<object, string>>();

                OM_SALE_QUOTE_DTL oM_SALE_QUOTE_DTL = new OM_SALE_QUOTE_DTL();

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    oM_SALE_QUOTE_DTL = new OM_SALE_QUOTE_DTL();

                    if (dtGridData.Rows[iLoop][FINColumnConstants.QUOTE_DTL_ID].ToString() != "0" && dtGridData.Rows[iLoop][FINColumnConstants.QUOTE_DTL_ID].ToString() != string.Empty)
                    {
                        oM_SALE_QUOTE_DTL = SalesQuote_BLL.getDetailClassEntity(dtGridData.Rows[iLoop][FINColumnConstants.QUOTE_DTL_ID].ToString());
                    }
                    oM_SALE_QUOTE_DTL.QUOTE_LINE_NUM = (iLoop + 1);//CommonUtils.ConvertStringToInt(dtGridData.Rows[iLoop][FINColumnConstants.PO_REQ_LINE_NUM].ToString());
                    oM_SALE_QUOTE_DTL.QUOTE_ITEM_ID = (dtGridData.Rows[iLoop][FINColumnConstants.ITEM_ID].ToString());

                    oM_SALE_QUOTE_DTL.QUOTE_ITEM_QTY = CommonUtils.ConvertStringToInt(dtGridData.Rows[iLoop]["QUOTE_ITEM_QTY"].ToString());
                    oM_SALE_QUOTE_DTL.QUOTE_ITEM_UOM = (dtGridData.Rows[iLoop]["QUOTE_ITEM_UOM"].ToString());
                    oM_SALE_QUOTE_DTL.QUOTE_ITEM_UNIT_PRICE = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop]["QUOTE_ITEM_UNIT_PRICE"].ToString());
                    oM_SALE_QUOTE_DTL.QUOTE_LINE_AMT = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop]["QUOTE_LINE_AMT"].ToString());
                    oM_SALE_QUOTE_DTL.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;
                    oM_SALE_QUOTE_DTL.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                    oM_SALE_QUOTE_DTL.QUOTE_HDR_ID = oM_SALE_QUOTE_HDR.QUOTE_HDR_ID;


                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        tmpChildEntity.Add(new Tuple<object, string>(oM_SALE_QUOTE_DTL, FINAppConstants.Delete));
                    }
                    else
                    {
                        if (dtGridData.Rows[iLoop][FINColumnConstants.QUOTE_DTL_ID].ToString() != "0" && dtGridData.Rows[iLoop][FINColumnConstants.QUOTE_DTL_ID].ToString() != string.Empty)
                        {
                            oM_SALE_QUOTE_DTL.MODIFIED_DATE = DateTime.Today;
                            oM_SALE_QUOTE_DTL.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;

                            tmpChildEntity.Add(new Tuple<object, string>(oM_SALE_QUOTE_DTL, FINAppConstants.Update));
                        }
                        else
                        {
                            oM_SALE_QUOTE_DTL.QUOTE_DTL_ID = FINSP.GetSPFOR_SEQCode("AR_004_D".ToString(), false, true);
                            oM_SALE_QUOTE_DTL.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;

                            oM_SALE_QUOTE_DTL.CREATED_BY = this.LoggedUserName;
                            oM_SALE_QUOTE_DTL.CREATED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(oM_SALE_QUOTE_DTL, FINAppConstants.Add));
                        }
                    }
                }
                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            CommonUtils.SavePCEntity<OM_SALE_QUOTE_HDR, OM_SALE_QUOTE_DTL>(oM_SALE_QUOTE_HDR, tmpChildEntity, oM_SALE_QUOTE_DTL);
                            savedBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {
                            CommonUtils.SavePCEntity<OM_SALE_QUOTE_HDR, OM_SALE_QUOTE_DTL>(oM_SALE_QUOTE_HDR, tmpChildEntity, oM_SALE_QUOTE_DTL, true);
                            savedBool = true;
                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                // DBMethod.DeleteEntity<PO_HEADERS>(PO_HEADERS);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }



        private void fn_UnitPricetxtChanged()
        {
            try
            {
                ErrorCollection.Clear();

                TextBox txtUnitPrice = new TextBox();
                TextBox txtQuantity = new TextBox();
                TextBox txtAmount = new TextBox();

                txtUnitPrice.ID = "txtUnitPrice";
                txtQuantity.ID = "txtQuantity";
                txtAmount.ID = "txtAmount";

                Label lblUnitPrice = new Label();
                Label lblQuantity = new Label();
                Label lblAmount = new Label();

                lblUnitPrice.ID = "lblUnitPrice";
                lblQuantity.ID = "lblQuantity";
                lblAmount.ID = "lblAmount";

                if (gvData.FooterRow != null)
                {
                    if (gvData.EditIndex < 0)
                    {
                        txtUnitPrice = (TextBox)gvData.FooterRow.FindControl("txtUnitPrice");
                        txtQuantity = (TextBox)gvData.FooterRow.FindControl("txtQuantity");
                        txtAmount = (TextBox)gvData.FooterRow.FindControl("txtAmount");

                        lblUnitPrice = (Label)gvData.FooterRow.FindControl("lblUnitPrice");
                        lblQuantity = (Label)gvData.FooterRow.FindControl("lblQuantity");
                        lblAmount = (Label)gvData.FooterRow.FindControl("lblAmount");
                    }
                    else
                    {
                        txtUnitPrice = (TextBox)gvData.Rows[gvData.EditIndex].FindControl("txtUnitPrice");
                        txtQuantity = (TextBox)gvData.Rows[gvData.EditIndex].FindControl("txtQuantity");
                        txtAmount = (TextBox)gvData.Rows[gvData.EditIndex].FindControl("txtAmount");

                        lblUnitPrice = (Label)gvData.Rows[gvData.EditIndex].FindControl("lblUnitPrice");
                        lblQuantity = (Label)gvData.Rows[gvData.EditIndex].FindControl("lblQuantity");
                        lblAmount = (Label)gvData.Rows[gvData.EditIndex].FindControl("lblAmount");
                    }
                }
                else
                {
                    txtUnitPrice = (TextBox)gvData.Controls[0].Controls[1].FindControl("txtUnitPrice");
                    txtQuantity = (TextBox)gvData.Controls[0].Controls[1].FindControl("txtQuantity");
                    txtAmount = (TextBox)gvData.Controls[0].Controls[1].FindControl("txtAmount");

                    lblUnitPrice = (Label)gvData.Controls[0].Controls[1].FindControl("lblUnitPrice");
                    lblQuantity = (Label)gvData.Controls[0].Controls[1].FindControl("lblQuantity");
                    lblAmount = (Label)gvData.Controls[0].Controls[1].FindControl("lblAmount");
                }

                if (txtUnitPrice != null && txtQuantity != null)
                {
                    if (txtUnitPrice.Text.Trim() != string.Empty && txtQuantity.Text.Trim() != string.Empty)
                    {
                        if (double.Parse(txtUnitPrice.Text.ToString()) > 0 && double.Parse(txtQuantity.Text.ToString()) > 0)
                        {
                            txtAmount.Text = (CommonUtils.ConvertStringToDecimal(txtUnitPrice.Text) * CommonUtils.ConvertStringToDecimal(txtQuantity.Text)).ToString();


                        }
                    }
                }



            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion
        protected void btnDelete_Click(object sender, EventArgs e)
        {

        }





        protected void ddlItemName_SelectedIndexChanged1(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;

                DropDownList ddlItemName = gvr.FindControl("ddlItemName") as DropDownList;
                TextBox txtUnitPrice = gvr.FindControl("txtUnitPrice") as TextBox;
                TextBox txtUOM = gvr.FindControl("txtUOM") as TextBox;

                DataTable dtunitprice = new DataTable();

                dtunitprice = DBMethod.ExecuteQuery(PurchaseOrder_DAL.GetItemUnitprice(ddlItemName.SelectedValue)).Tables[0];
                txtUnitPrice.Text = dtunitprice.Rows[0]["ITEM_UNIT_PRICE"].ToString();

                DataTable dtUom = new DataTable();
                if (ddlItemName.SelectedValue != string.Empty && ddlItemName.SelectedValue != "0")
                {
                    dtUom = DBMethod.ExecuteQuery(MaterialIssue_DAL.GetItemUOM_fromUommaster(ddlItemName.SelectedValue.ToString())).Tables[0];
                    if (dtUom != null)
                    {
                        if (dtUom.Rows.Count > 0)
                        {
                            txtUOM.Text = dtUom.Rows[0]["ITEM_UOM"].ToString();
                        }
                        else
                        {
                            txtUOM.Text = "N/A";
                        }
                    }
                }


                fn_UnitPricetxtChanged();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void txtQuantity_TextChanged1(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                fn_UnitPricetxtChanged();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        protected void ddlSupplierNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                FIN.BLL.AR.Customer_Branch_BLL.GetCustomerBranch(ref  ddlSupplierSite, ddlCustomerName.SelectedValue);


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        protected void ddlQuoteType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                dtGridData = DBMethod.ExecuteQuery(SalesQuote_DAL.GetSalesQuoteDetails(Master.StrRecordId)).Tables[0];
                BindGrid(dtGridData);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void txtRequestDate_TextChanged(object sender, EventArgs e)
        {

        }
        protected void btnPrint_Click(object sender, ImageClickEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                ReportFile = Master.ReportName;


                //if ( SelectedValue != string.Empty)
                //{
                //    htFilterParameter.Add("SUPPLIER_NAME", ddlSupplierName.SelectedValue);
                //}
                if (txtQuoteNumber.Text != string.Empty)
                {
                    htFilterParameter.Add("QUOTE_HDR_ID", txtQuoteNumber.Text);
                }



                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;

                ReportData = FIN.BLL.AR.SalesQuote_BLL.GetReportData();

                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());

                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerPath + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("SalesQuoteReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
    }
}