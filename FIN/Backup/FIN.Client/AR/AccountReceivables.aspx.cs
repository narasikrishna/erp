﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.AP;
using FIN.BLL;
using FIN.BLL.GL;
using FIN.BLL.AP;
using FIN.BLL.HR;
using VMVServices.Web;
using System.Data;
using System.Threading.Tasks;
using VMVServices.Services.Data;
using System.Data;
using System.Data.SqlClient;
using Oracle.DataAccess.Client;

namespace FIN.Client.AR
{
    public partial class AccountReceivables : PageBase
    {
        DataTable dtGridData = new DataTable();
        Boolean bol_rowVisiable;

        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();

        string connectionStringSQL = System.Web.Configuration.WebConfigurationManager.AppSettings["connectionStringSQL"].ToString();
        string connectionStringOracle = System.Web.Configuration.WebConfigurationManager.AppSettings["connectionStringOracle"].ToString();
        bool postedMsg = false;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));
            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();


            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }
            UserRightsChecking();

        }
        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>
        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                Startup();
                EntityData = null;
                Session["dtData"] = null;
                Session[FINSessionConstants.GridData] = null;
                //dtGridData = DBMethod.ExecuteQuery(FIN.DAL.AP.InvoiceRegister_DAL.getAccountPayableData()).Tables[0];
                //BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountReceivablesATC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;

                if (dtData.Rows.Count > 0)
                {
                    if (dtData.Rows[0]["AMOUNT"].ToString().Length > 0)
                    {
                        dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("AMOUNT", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("AMOUNT"))));
                    }

                    dtData.AcceptChanges();
                }

                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr["MIGRATED"] = "FALSE";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }

                gvData.DataSource = dt_tmp;
                gvData.DataBind();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountReceivablesBG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        #endregion


        # region Save,Update and Delete
        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                // DBMethod.DeleteEntity<PO_HEADERS>(PO_HEADERS);
                //DisplayDeleteCompleteMessage(Master.ListPageToOpen);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountReceivablesPOR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        #endregion

        private void BindFilteredGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;


                if (dtData.Rows.Count > 0)
                {
                    if (dtData.Rows[0]["AMOUNT"].ToString().Length > 0)
                    {
                        dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("AMOUNT", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("AMOUNT"))));
                    }             
                    dtData.AcceptChanges();
                }

                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr["MIGRATED"] = "FALSE";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountPayablesBG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void btnPost_Click(object sender, EventArgs e)
        {
            try
            {
                for (int iLoop = 0; iLoop < gvData.Rows.Count; iLoop++)
                {
                    CheckBox chkSelect = (CheckBox)gvData.Rows[iLoop].FindControl("chkSelect");
                    if (chkSelect.Checked)
                    {
                        FINSP.GetSP_GL_Posting(gvData.DataKeys[iLoop].Values["AccountReceivableId"].ToString(), "SSM_031");
                        postedMsg = true;
                    }
                }

                if (postedMsg)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.POSTED);
                }

                DataTable dtData = DBMethod.ExecuteQuery(FIN.DAL.AR.SalesRegister_DAL.getAccountReceivableData()).Tables[0];
                BindGrid(dtData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountReceivablesPOST", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
                TextBox txtVendorNum = gvr.FindControl("txtVendorNum") as TextBox;
                TextBox txtVendorName = gvr.FindControl("txtVendorName") as TextBox;
                TextBox txtdescription = gvr.FindControl("txtdescription") as TextBox;
                TextBox txtInvoiceNumber = gvr.FindControl("txtInvoiceNumber") as TextBox;
                TextBox txtInvoiceDate = gvr.FindControl("txtInvoiceDate") as TextBox;
                TextBox txtDueDate = gvr.FindControl("txtDueDate") as TextBox;
                TextBox txtAMOUNT = gvr.FindControl("txtAMOUNT") as TextBox;

                if (txtVendorNum.Text.Length > 0)
                {
                    txtVendorName.Text = string.Empty;
                    txtdescription.Text = string.Empty;
                    txtInvoiceNumber.Text = string.Empty;
                    txtInvoiceDate.Text = string.Empty;
                    txtDueDate.Text = string.Empty;
                    txtAMOUNT.Text = string.Empty;
                }
                else if (txtVendorName.Text.Length > 0)
                {
                    txtVendorNum.Text = string.Empty;
                    txtdescription.Text = string.Empty;
                    txtInvoiceNumber.Text = string.Empty;
                    txtInvoiceDate.Text = string.Empty;
                    txtDueDate.Text = string.Empty;
                    txtAMOUNT.Text = string.Empty;
                }
                else if (txtdescription.Text.Length > 0)
                {
                    txtVendorName.Text = string.Empty;
                    txtVendorNum.Text = string.Empty;
                    txtInvoiceNumber.Text = string.Empty;
                    txtInvoiceDate.Text = string.Empty;
                    txtDueDate.Text = string.Empty;
                    txtAMOUNT.Text = string.Empty;
                }
                else if (txtInvoiceNumber.Text.Length > 0)
                {
                    txtVendorName.Text = string.Empty;
                    txtdescription.Text = string.Empty;
                    txtVendorNum.Text = string.Empty;
                    txtInvoiceDate.Text = string.Empty;
                    txtDueDate.Text = string.Empty;
                    txtAMOUNT.Text = string.Empty;
                }
                else if (txtInvoiceDate.Text.Length > 0)
                {
                    txtVendorName.Text = string.Empty;
                    txtdescription.Text = string.Empty;
                    txtVendorNum.Text = string.Empty;
                    txtInvoiceNumber.Text = string.Empty;
                    txtDueDate.Text = string.Empty;
                    txtAMOUNT.Text = string.Empty;
                }
                else if (txtDueDate.Text.Length > 0)
                {
                    txtVendorName.Text = string.Empty;
                    txtdescription.Text = string.Empty;
                    txtVendorNum.Text = string.Empty;
                    txtInvoiceNumber.Text = string.Empty;
                    txtInvoiceDate.Text = string.Empty;
                    txtAMOUNT.Text = string.Empty;
                }
                else if (txtAMOUNT.Text.Length > 0)
                {
                    txtVendorName.Text = string.Empty;
                    txtdescription.Text = string.Empty;
                    txtVendorNum.Text = string.Empty;
                    txtInvoiceNumber.Text = string.Empty;
                    txtInvoiceDate.Text = string.Empty;
                    txtDueDate.Text = string.Empty;
                }
                DataTable dt = new DataTable();

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dt = (DataTable)Session[FINSessionConstants.GridData];
                    if (dt.Rows.Count > 0)
                    {
                        var var_List = dt.AsEnumerable().Where(r => r["InvoiceNumber"].ToString() != string.Empty);

                        if (txtVendorNum.Text.Length > 0)
                        {
                            var_List = dt.AsEnumerable().Where(r => r["vendornumber"].ToString().ToUpper().StartsWith(txtVendorNum.Text.ToUpper()));
                        }
                        else if (txtVendorName.Text.Length > 0)
                        {
                            var_List = dt.AsEnumerable().Where(r => r["name_en"].ToString().ToUpper().StartsWith(txtVendorName.Text.ToUpper()));
                        }
                        else if (txtdescription.Text.Length > 0)
                        {
                            var_List = dt.AsEnumerable().Where(r => r["description"].ToString().ToUpper().StartsWith(txtdescription.Text.ToUpper()));
                        }
                        else if (txtInvoiceNumber.Text.Length > 0)
                        {
                            var_List = dt.AsEnumerable().Where(r => r["InvoiceNumber"].ToString().ToUpper().StartsWith(txtInvoiceNumber.Text.ToUpper()));
                        }
                        else if (txtInvoiceDate.Text.Length > 0)
                        {
                            var_List = dt.AsEnumerable().Where(r => r["INVOICEDATE"].ToString().StartsWith(txtInvoiceDate.Text));
                        }
                        else if (txtDueDate.Text.Length > 0)
                        {
                            var_List = dt.AsEnumerable().Where(r => r["DueDate"].ToString().StartsWith(txtDueDate.Text));
                        }
                        else if (txtAMOUNT.Text.Length > 0)
                        {
                            var_List = dt.AsEnumerable().Where(r => r["AMOUNT"].ToString().StartsWith(txtAMOUNT.Text));
                        }
                        if (var_List.Any())
                        {
                            dt = System.Data.DataTableExtensions.CopyToDataTable(var_List);
                            BindFilteredGrid(dt);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            if (Session[FINSessionConstants.GridData] != null)
            {
                dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                BindGrid(dtGridData);
            }
        }
        protected void btnImport_Click(object sender, EventArgs e)
        {
            string maxIdAR = "";
            string maxPersonIDMNet = "";
            try
            {
                //AccountReceivable
                //Get the MAX Id
                using (OracleConnection connMax = new OracleConnection())
                {
                    connMax.ConnectionString = connectionStringOracle;
                    connMax.Open();
                    OracleCommand cmdMax = connMax.CreateCommand();
                    string sql = "";

                    sql = "Select max(ar.AccountReceivableId) maxValueAR from AccountReceivable ar where ar.invoicedate <=to_date('" + txtFromDate.Text + "','dd/MM/yyyy')";
                    DataTable dtDataMax = new DataTable();
                    OracleDataAdapter oa = new OracleDataAdapter(cmdMax);
                    cmdMax.CommandText = sql;
                    oa.Fill(dtDataMax);
                    if (dtDataMax.Rows.Count > 0)
                    {
                        if (dtDataMax.Rows[0]["maxValueAR"].ToString().Length > 0)
                        {
                            maxIdAR = dtDataMax.Rows[0]["maxValueAR"].ToString();
                        }
                    }

                    //Customer Mapping -- Get the max Person ID
                    sql = "select max(to_number(personid)) personIDMNet from dm_intf_cust_mapping t where t.attribute10='MASONET'";
                    DataTable dtDataMaxPID = new DataTable();
                    OracleDataAdapter oaPID = new OracleDataAdapter(cmdMax);
                    cmdMax.CommandText = sql;
                    oaPID.Fill(dtDataMaxPID);
                    if (dtDataMaxPID.Rows.Count > 0)
                    {
                        if (dtDataMaxPID.Rows[0]["personIDMNet"].ToString().Length > 0)
                        {
                            maxPersonIDMNet = dtDataMaxPID.Rows[0]["personIDMNet"].ToString();
                        }
                    }

                    connMax.Close();
                }

                //Create Connection - AccountReceivable  -- SQL
                SqlConnection con = new SqlConnection(connectionStringSQL);

                string sqlQuery = " select AccountReceivableId,(TYPE*10) as TYPE,CustomerId,AMOUNT,CURRENCYID,convert(nvarchar,DueDate,103) as DueDate,ReceivedAmount,STATUS,convert(nvarchar,STATUSDATE,103) as [STATUSDATE]";
                sqlQuery += " ,REMARKS_EN,REMARKS_AR,COMPONENTID,MergedARId,SplittedARId,CUSERID,convert(nvarchar,CDATE,103) as CDATE ";
                sqlQuery += " ,MDATE, convert(nvarchar,CANCELDATE,103) as CANCELDATE, convert(nvarchar,INVOICEDATE,103) as INVOICEDATE, AccountReceivableNumber";
                sqlQuery += " from AccountReceivable where status in (1,3,4) and invoicedate > = CONVERT(datetime,'01/01/2015',103) ";
                if (maxIdAR.ToString().Length > 0)
                {
                    sqlQuery += " and AccountReceivableId > '" + maxIdAR + "'";
                }
                SqlCommand cmd = new SqlCommand(sqlQuery, con);

                con.Open();
                DataTable dtData = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter(cmd);

                da.Fill(dtData);

                //Vendor Mapping - SQL
                string sqlQueryVM = " SELECT PERSONID, NAME_EN, NAME_AR, NATIONALITYID, PERSONTYPE, CITYID, ISACTIVE, CUSTOMERNUMBER, ISPRIMARY, Address";
                sqlQueryVM += " FROM vwcustomer";
                if (maxPersonIDMNet.ToString().Length > 0)
                {
                    sqlQueryVM += " where PERSONID > '" + maxPersonIDMNet + "'";
                }

                SqlCommand cmdVM = new SqlCommand(sqlQueryVM, con);
                DataTable dtDataVM = new DataTable();
                SqlDataAdapter daVM = new SqlDataAdapter(cmdVM);

                daVM.Fill(dtDataVM);


                //Create Connection -- Oracle
                //Account Receivables
                if (dtData.Rows.Count > 0)
                {
                    using (OracleConnection connection = new OracleConnection())
                    {
                        connection.ConnectionString = connectionStringOracle;
                        connection.Open();
                        OracleCommand command = connection.CreateCommand();

                        for (int iLoop = 0; iLoop < dtData.Rows.Count; iLoop++)
                        {
                            string sql = "";
                            //  sql += " insert into accountpayable Values('";
                            sql += " insert into AccountReceivable (AccountReceivableId,type,CustomerId, amount, currencyid, duedate, ReceivedAmount, status, statusdate, remarks_en, remarks_ar, componentid, ";
                            sql += "  mergedarid, splittedarid, cuserid , cdate, mdate, canceldate, invoicedate, AccountReceivableNumber) Values ('";
                            sql += dtData.Rows[iLoop]["AccountReceivableId"].ToString() + "','";
                            sql += dtData.Rows[iLoop]["TYPE"].ToString() + "','";
                            sql += dtData.Rows[iLoop]["CustomerId"].ToString() + "','";
                            sql += dtData.Rows[iLoop]["AMOUNT"].ToString() + "','";
                            sql += dtData.Rows[iLoop]["CURRENCYID"].ToString() + "',";
                            sql += "to_date('" + dtData.Rows[iLoop]["DUEDATE"].ToString() + "','dd/MM/yyyy'),'";
                            sql += dtData.Rows[iLoop]["ReceivedAmount"].ToString() + "','";
                            sql += dtData.Rows[iLoop]["STATUS"].ToString() + "',";
                            sql += "to_date('" + dtData.Rows[iLoop]["STATUSDATE"].ToString() + "','dd/MM/yyyy'),'";
                            sql += dtData.Rows[iLoop]["REMARKS_EN"].ToString() + "','";
                            sql += dtData.Rows[iLoop]["REMARKS_AR"].ToString() + "','";
                            sql += dtData.Rows[iLoop]["COMPONENTID"].ToString() + "','";
                            sql += dtData.Rows[iLoop]["MERGEDARID"].ToString() + "','";
                            sql += dtData.Rows[iLoop]["SPLITTEDARID"].ToString() + "','";
                            sql += dtData.Rows[iLoop]["CUSERID"].ToString() + "',";
                            sql += "to_date('" + dtData.Rows[iLoop]["CDATE"].ToString() + "','dd/MM/yyyy'),";
                            sql += "NULL,";
                            sql += "to_date('" + dtData.Rows[iLoop]["CANCELDATE"].ToString() + "','dd/MM/yyyy'),";
                            sql += "to_date('" + dtData.Rows[iLoop]["INVOICEDATE"].ToString() + "','dd/MM/yyyy'),'";
                            sql += dtData.Rows[iLoop]["AccountReceivableNumber"].ToString() + "'";
                            sql += ")";

                            command.CommandText = sql;
                            command.ExecuteNonQuery();
                        }
                        connection.Close();
                        // lblImported.Text = "Data Imported";
                    }
                }

                //Vendor Mapping Insert - Oracle
                if (dtDataVM.Rows.Count > 0)
                {
                    using (OracleConnection connection = new OracleConnection())
                    {
                        connection.ConnectionString = connectionStringOracle;
                        connection.Open();
                        OracleCommand command = connection.CreateCommand();

                        for (int iLoop = 0; iLoop < dtDataVM.Rows.Count; iLoop++)
                        {
                            string sql = "";
                            sql += " insert into DM_INTF_CUST_MAPPING (PERSONID, NAME_EN, NAME_AR, NATIONALITYID, PERSONTYPE, CITYID, ISACTIVE, CUSTNUMBER,";
                            sql += " ISPRIMARY, ADDRESS, ATTRIBUTE10 ) Values ('";
                            sql += dtDataVM.Rows[iLoop]["PERSONID"].ToString() + "','";
                            sql += dtDataVM.Rows[iLoop]["NAME_EN"].ToString() + "','";
                            sql += dtDataVM.Rows[iLoop]["NAME_AR"].ToString() + "','";
                            sql += dtDataVM.Rows[iLoop]["NATIONALITYID"].ToString() + "','";
                            sql += dtDataVM.Rows[iLoop]["PERSONTYPE"].ToString() + "','";
                            sql += dtDataVM.Rows[iLoop]["CITYID"].ToString() + "','";
                            sql += dtDataVM.Rows[iLoop]["ISACTIVE"].ToString() + "','";
                            sql += dtDataVM.Rows[iLoop]["CUSTOMERNUMBER"].ToString() + "','";
                            sql += dtDataVM.Rows[iLoop]["ISPRIMARY"].ToString() + "','";
                            sql += dtDataVM.Rows[iLoop]["ADDRESS"].ToString() + "','";
                            sql += "MASONET'";
                            sql += ")";

                            command.CommandText = sql;
                            command.ExecuteNonQuery();
                        }
                        connection.Close();
                        // lblImported.Text = "Data Imported";
                    }
                }

                //for (int iLoop = 0; iLoop < dtDataVM.Rows.Count; iLoop++)
                //{
                //    FINSP.GetSP_GL_Posting("", "SSM_031_M");
                //}

                FINSP.GetSP_GL_Posting("1", "SSM_031_M");

                DataTable dtDataView = DBMethod.ExecuteQuery(FIN.DAL.AR.SalesRegister_DAL.getAccountReceivableData()).Tables[0];
                BindGrid(dtDataView);
                Session["dtData"] = dtDataView;

                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATAIMPORT);

                //Close Connection  
                con.Close();
                da.Dispose();
                //close

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountReceivablesImport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void chkSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chk_SAA = (CheckBox)sender;
            for (int rloop = 0; rloop < gvData.Rows.Count; rloop++)
            {
                CheckBox chk_tmp = (CheckBox)gvData.Rows[rloop].FindControl("chkSelect");
                chk_tmp.Checked = chk_SAA.Checked;
            }
        }

        protected void gvData_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session["dtData"] != null)
                {
                    gvData.PageIndex = e.NewPageIndex;
                    gvData.DataSource = Session["dtData"];
                    gvData.DataBind();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CourseSubjectEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

      

    }
}