﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="ARReceiptImport.aspx.cs" Inherits="FIN.Client.AR.ARReceiptImport" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1000px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblFromDate">
                Date
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:TextBox ID="txtFromDate" runat="server" TabIndex="1" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]]  RequiredField txtBox"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calExtender1" TargetControlID="txtFromDate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
            <div class="divtxtBox LNOrient">
                <asp:ImageButton ID="btnImport" runat="server" ImageUrl="~/Images/btnImport.png"
                    OnClick="btnImport_Click" ValidationGroup="btnSave" /></div>
            <div class="divtxtBox LNOrient">
                <asp:ImageButton ID="btnPost" runat="server" ImageUrl="../Images/Post.png" OnClick="btnPost_Click" /></div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" style="display: none">
            <div class="lblBox LNOrient" style="width: 200px" id="lblToDate">
                To Date
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:TextBox ID="txtToDate" runat="server" TabIndex="2" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]]  RequiredField txtBox"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtToDate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class=" LNOrient">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" Width="90%"
                CssClass="DisplayFont Grid" ShowFooter="false" DataKeyNames="CollectionId">
                <Columns>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <table width="100%">
                                <tr>
                                    <td>
                                     <div class="lblBox LNOrient" style="color:White;">
                                            Customer Number
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtVendorNum" CssClass="txtBox" OnTextChanged="txtSearch_TextChanged"
                                            AutoPostBack="true"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label runat="server" ID="vendorNum" Text='<%# Eval("vendornumber") %>'></asp:Label></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <table width="100%">
                                <tr>
                                    <td>
                                    <div class="lblBox LNOrient" style="color:White;">
                                            Customer Name
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:TextBox OnTextChanged="txtSearch_TextChanged" AutoPostBack="true" runat="server"
                                            ID="txtVendorName" CssClass="txtBox"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label runat="server" ID="name_en" Text='<%# Eval("name_en") %>'></asp:Label></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <table width="100%">
                                <tr>
                                    <td>
                                     <div class="lblBox LNOrient" style="color:White;">
                                            Invoice Number
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:TextBox OnTextChanged="txtSearch_TextChanged" AutoPostBack="true" runat="server"
                                            ID="txtInvoiceNumber" CssClass="txtBox"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label runat="server" ID="InvoiceNumber" Text='<%# Eval("InvoiceNumber") %>'></asp:Label></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <table width="100%">
                                <tr>
                                    <td>
                                   <div class="lblBox LNOrient" style="color:White;">
                                            Receipt Number
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:TextBox OnTextChanged="txtSearch_TextChanged" AutoPostBack="true" runat="server"
                                            ID="txtRecNumber" CssClass="txtBox"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label runat="server" ID="RecNumber" Text='<%# Eval("ReceiptNumber") %>'></asp:Label></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <table width="100%">
                                <tr>
                                    <td>
                                   <div class="lblBox LNOrient" style="color:White;">
                                            Amount
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:TextBox OnTextChanged="txtSearch_TextChanged" AutoPostBack="true" runat="server"
                                            ID="txtAMOUNT" CssClass="txtBox"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label runat="server" ID="AMOUNT" Style="text-align: right;" Text='<%# Eval("AMOUNT") %>'></asp:Label></ItemTemplate>
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Select">
                        <ItemTemplate>
                            <asp:CheckBox ID="chkSelect" runat="server" Checked='<%# Convert.ToBoolean(Eval("MIGRATED")) %>' />
                        </ItemTemplate>
                        <HeaderTemplate>
                            <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="True" OnCheckedChanged="chkSelectAll_CheckedChanged" />
                        </HeaderTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                        <HeaderStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Error Message" ItemStyle-Width="200px" ItemStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <asp:HyperLink Style="cursor: pointer;" runat="server" ID="linkErrorMsg" ForeColor="BlueViolet"
                                Text="View Log"></asp:HyperLink>
                            <asp:Panel ID="pnlAssign" runat="server" Width="200px">
                                <asp:Label runat="server" ID="txtDesc" Text='<%# Eval("error_message") %>' Width="200px"></asp:Label>
                            </asp:Panel>
                            <cc2:BalloonPopupExtender ID="BalloonPopupExtender4" TargetControlID="linkErrorMsg"
                                UseShadow="true" Position="Auto" BalloonPopupControlID="pnlAssign" runat="server"
                                DisplayOnClick="true" DisplayOnMouseOver="true" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Width="20px">
                        <HeaderTemplate>
                            <asp:ImageButton ID="btnShowAll" runat="server" ImageUrl="../Images/close.png" Width="20px"
                                Height="20px" OnClick="btnShowAll_Click" />
                        </HeaderTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table width="100%">
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
        <div id="div5">
            <asp:HiddenField runat="server" ID="hdRowIndex" />
            <asp:HiddenField runat="server" ID="hdItemId" />
            <asp:HiddenField runat="server" ID="HiddenField1" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/AR/ARChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">


        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });
    </script>
</asp:Content>
