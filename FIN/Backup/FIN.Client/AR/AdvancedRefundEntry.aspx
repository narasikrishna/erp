﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="AdvancedRefundEntry.aspx.cs" Inherits="FIN.Client.AR.AdvancedRefundEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1200px" id="divMainContainer">
        <table>
            <tr>
                <td runat="server" id="tdHeader">
                    <asp:Panel runat="server" ID="pnltdHeader">
                        <div class="divRowContainer">
                            <div class="lblBox LNOrient" style="width: 150px" id="lblWarehouseNumber">
                                Payment Number
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 150px">
                                <asp:TextBox ID="txtPaymentNumber" runat="server" TabIndex="1" Enabled="false" CssClass=" txtBox"></asp:TextBox>
                            </div>
                            <div class="colspace LNOrient">
                                &nbsp</div>
                            <div class="lblBox LNOrient" style=" width: 150px" id="lblWarehouseName">
                                Payment Date
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 150px">
                                <asp:TextBox ID="txtPaymentDate" runat="server" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]] RequiredField txtBox"
                                    TabIndex="2" OnTextChanged="txtPaymentDate_TextChanged"></asp:TextBox>
                                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calExtender1" TargetControlID="txtPaymentDate"
                                    OnClientDateSelectionChanged="checkDate" />
                            </div>
                            <div class="colspace LNOrient">
                                &nbsp</div>
                            <div class="lblBox LNOrient" style="width: 150px" id="lblLineNumber">
                                Payment Method
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 155px">
                                <asp:DropDownList ID="ddlPaymentMode" runat="server" TabIndex="3" CssClass="validate[required] RequiredField ddlStype"
                                    OnSelectedIndexChanged="ddlPaymentMode_SelectedIndexChanged" AutoPostBack="true">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="divClear_10">
                        </div>
                        <div class="divRowContainer">
                            <div class="lblBox LNOrient" style="width: 150px" id="lblGRNNumber">
                                Customer Number
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 310px">
                                <asp:DropDownList ID="ddlCustomerNumber" runat="server" TabIndex="4" CssClass="validate[required] RequiredField ddlStype"
                                    AutoPostBack="True" OnSelectedIndexChanged="ddlCustomerNumber_SelectedIndexChanged">
                                </asp:DropDownList>
                            </div>
                            <div class="colspace LNOrient" >
                                &nbsp</div>
                            <div class="lblBox LNOrient" style="width: 150px" id="lblGRNDate">
                                Customer Name
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 320px">
                                <asp:TextBox ID="txtCustomerName" Enabled="false" runat="server" TabIndex="5" CssClass="txtBox"></asp:TextBox>
                            </div>
                        </div>
                        <div class="divClear_10">
                        </div>
                        <div class="divRowContainer">
                            <div class="lblBox LNOrient" style="width: 150px" id="lblPONumber">
                                Bank Name
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 155px; height: 22px;">
                                <asp:DropDownList ID="ddlBankName" runat="server" TabIndex="6" CssClass="validate[]   ddlStype"
                                    AutoPostBack="True" OnSelectedIndexChanged="ddlBankName_SelectedIndexChanged">
                                </asp:DropDownList>
                            </div>
                            <div class="colspace LNOrient" >
                                &nbsp</div>
                            <div class="lblBox LNOrient" style="width: 150px" id="lblPOLineNumber">
                                Bank Branch
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 150px">
                                <asp:DropDownList ID="ddlBankBranch" runat="server" TabIndex="7" CssClass="validate[]   ddlStype"
                                    AutoPostBack="True" OnSelectedIndexChanged="ddlBankBranch_SelectedIndexChanged">
                                </asp:DropDownList>
                            </div>
                            <div class="colspace LNOrient">
                                &nbsp</div>
                            <div class="lblBox LNOrient" style="width: 150px" id="lblItemDescription">
                                Account Number
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 155px">
                                <asp:DropDownList ID="ddlAccountnumber" runat="server" TabIndex="8" AutoPostBack="True"
                                    OnSelectedIndexChanged="ddlAccountnumber_SelectedIndexChanged" CssClass="validate[]   ddlStype">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="divClear_10">
                        </div>
                        <div class="divRowContainer">
                            <div class="lblBox LNOrient" style="width: 150px" id="Div1">
                                Cheque Number
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 155px">
                                <asp:DropDownList ID="ddlChequeNumber" runat="server" TabIndex="9" CssClass="validate[]   ddlStype"
                                    AutoPostBack="True" OnSelectedIndexChanged="ddlChequeNumber_SelectedIndexChanged">
                                </asp:DropDownList>
                            </div>
                            <div class="colspace LNOrient">
                                &nbsp</div>
                            <div class="lblBox LNOrient" style="width: 150px" id="Div2">
                                Value Date
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 150px">
                                <asp:TextBox ID="txtChequeDate" runat="server" CssClass="validate[] validate[custom[ReqDateDDMMYYY]]  txtBox"
                                    Enabled="false" TabIndex="10"></asp:TextBox>
                                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtChequeDate"
                                    OnClientDateSelectionChanged="checkDate" />
                            </div>
                            <div class="colspace LNOrient">
                                &nbsp</div>
                            <div class="lblBox LNOrient" style="width: 150px" id="Div5">
                                Cheque Received By
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 150px">
                                <asp:TextBox ID="txtChequeReceivedBy" runat="server" TabIndex="11" CssClass="txtBox"
                                    MaxLength="100"></asp:TextBox>
                            </div>
                        </div>
                        <div class="divClear_10">
                        </div>
                        <div class="divRowContainer">
                            <div class="lblBox LNOrient" style="width: 150px" id="Div7">
                                Civil ID
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 808px">
                                <asp:TextBox ID="txtCivilId" runat="server" TabIndex="11" CssClass="txtBox" MaxLength="100"></asp:TextBox>
                            </div>
                        </div>
                        <div class="divClear_10">
                        </div>
                        <div class="divRowContainer">
                            <div class="lblBox LNOrient" style="width: 150px" id="lblPaymentCurrency">
                                Payment Currency
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 155px">
                                <asp:DropDownList ID="ddlPaymentCurrency" runat="server" TabIndex="12" AutoPostBack="true"
                                    CssClass=" ddlStype" OnSelectedIndexChanged="ddlPaymentCurrency_SelectedIndexChanged">
                                </asp:DropDownList>
                            </div>
                            <div class="colspace LNOrient">
                                &nbsp</div>
                            <div class="lblBox LNOrient" style="width: 150px" id="lblExchangeRateType">
                                Exchange Rate Type
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 150px">
                                <asp:DropDownList ID="ddlExchangeRateType" runat="server" TabIndex="13" CssClass="ddlStype"
                                    OnSelectedIndexChanged="ddlExchangeRateType_TextChanged" AutoPostBack="true">
                                </asp:DropDownList>
                            </div>
                            <div class="colspace LNOrient">
                                &nbsp</div>
                            <div class="lblBox LNOrient" style="width: 150px" id="lblExchangeRate">
                                Exchange Rate
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 150px">
                                <asp:TextBox ID="txtExchangeRate" runat="server" TabIndex="14" CssClass="txtBox_N"></asp:TextBox>
                            </div>
                            <div class="colspace LNOrient" >
                                &nbsp</div>
                        </div>
                        <div class="divClear_10">
                        </div>
                        <div class="divRowContainer">
                            <div class="lblBox LNOrient" style="width: 150px" id="Div4">
                                Invoice Number
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 155px">
                                <asp:DropDownList ID="ddlInvoiceNumber" runat="server" TabIndex="13" CssClass="ddlStype"
                                    OnSelectedIndexChanged="ddlInvoiceNumber_SelectedIndexChanged" AutoPostBack="true">
                                </asp:DropDownList>
                            </div>
                            <div class="colspace LNOrient">
                                &nbsp</div>
                            <div class="lblBox LNOrient" style="width: 150px" id="lblInvoiceAmount">
                                Invoice Amount
                            </div>
                            <div class="divtxtBox LNOrient" style=" width: 150px">
                                <asp:TextBox ID="txtInvoiceAmount" runat="server" CssClass="txtBox_N" Enabled="false"
                                    TabIndex="15"></asp:TextBox>
                                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender32" runat="server" FilterType="Numbers,Custom"
                                    ValidChars=".," TargetControlID="txtInvoiceAmount" />
                            </div>
                            <div class="colspace LNOrient">
                                &nbsp</div>
                            <div class="lblBox LNOrient" style=" width: 150px" id="Div3">
                                Refund Amount
                            </div>
                            <div class="divtxtBox LNOrient" style=" width: 150px">
                                <asp:TextBox ID="txtRefundAmount" runat="server" CssClass="txtBox_N" TabIndex="15"
                                    OnTextChanged="txtRefundAmount_TextChanged" AutoPostBack="true"></asp:TextBox>
                                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server" FilterType="Numbers,Custom"
                                    ValidChars=".," TargetControlID="txtRefundAmount" />
                            </div>
                            <div class="lblBox LNOrient" style=" width: 350px;" id="Div15">
                                <asp:Label ID="lblWaitApprove" runat="server" Text="WAITING FOR APPROVAL" Visible="false"
                                    CssClass="lblBox" Font-Size="18px" Font-Bold="true"></asp:Label></div>
                        </div>
                        <div class="divClear_10">
                        </div>
                        <div class="divRowContainer">
                            <div class="lblBox LNOrient" style=" width: 150px" id="Div17">
                                Amount Paid
                            </div>
                            <div class="divtxtBox LNOrient" style="width: 150px">
                                <asp:TextBox ID="txtAmtPaid" runat="server" CssClass="txtBox_N" Enabled="false" TabIndex="16"></asp:TextBox>
                                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender100" runat="server" FilterType="Numbers,Custom"
                                    ValidChars=".," TargetControlID="txtAmtPaid" />
                            </div>
                            <div class="colspace LNOrient">
                                &nbsp&nbsp</div>
                            <div class="lblBox LNOrient" style=" width: 150px" id="Div18">
                                Balance Amount
                            </div>
                            <div class="divtxtBox LNOrient" style=" width: 150px">
                                <asp:TextBox ID="txtBalanceAmt" runat="server" CssClass="txtBox_N" Enabled="false" TabIndex="17"></asp:TextBox>
                                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender101" runat="server" FilterType="Numbers,Custom"
                                    ValidChars=".," TargetControlID="txtBalanceAmt" />
                            </div>
                        </div>
                        <div class="divClear_10">
                        </div>
                        <asp:HiddenField runat="server" ID="HDTotPayment" Value="0" />
                        <asp:HiddenField runat="server" ID="hdTotRetnAmt" Value="0" />
                        <asp:HiddenField runat="server" ID="hdOCAmt" Value="0" />
                    </asp:Panel>
                </td>
                <td valign="top">
                    <table>
                        <tr>
                            <td>
                                <asp:ImageButton ID="btnPrint" runat="server" ImageUrl="~/Images/Print.png" OnClick="imgBtnPrint_Click"
                                    Style="border: 0px" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:ImageButton ID="btnChequePrint" runat="server" ImageUrl="~/Images/printCheque.png"
                                    OnClick="imgBtnChequePrint_Click" Style="border: 0px" />
                            </td>
                        </tr>
                        <tr>
                            <td style="display:none">
                                <asp:ImageButton ID="imgBtnPost" runat="server" ImageUrl="~/Images/Post.png" OnClick="imgBtnPost_Click"
                                    Style="border: 0px" OnClientClick="fn_PostVisible('FINContent_imgBtnPost')" />
                                <asp:Label ID="lblPosted" runat="server" Text="POSTED" Visible="false" CssClass="lblBox"
                                    Font-Size="18px" Font-Bold="true" Style="color: Green"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:ImageButton ID="imgBtnJVPrint" runat="server" ImageUrl="~/Images/jvPrint.png"
                                    Style="border: 0px" OnClick="imgBtnJVPrint_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:ImageButton ID="btnStopPayment" runat="server" ImageUrl="~/Images/stopPay.png"
                                    OnClick="imgStopPayment_Click" Style="border: 0px" />
                                <asp:Label ID="lblCancelled" runat="server" Text="CANCELLED" Visible="false" CssClass="lblBox"
                                    Font-Size="18px" Font-Bold="true" Style="color: Red"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:ImageButton ID="imgBtnStopPayJV" runat="server" ImageUrl="~/Images/stopPayJV.png"
                                    Style="border: 0px" OnClick="imgBtnStopPayJV_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <div class="divRowContainer">
            <div class="divtxtBox" style="float: right; width: 545px">
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer LNOrient">
            <div class="LNOrient" style="width: 100%">
                <div style=" padding-top: 5px;" class="GridHeader lblBox">
                    Deductions(if any)
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <asp:Panel runat="server" ID="pnlAcc">
                <asp:GridView ID="gv_pay_OC" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                    Width="800px" DataKeyNames="AROC_ID,ACCT_CODE_ID,DELETED,AddSub_value" OnRowCancelingEdit="gv_pay_OC_RowCancelingEdit"
                    OnRowCommand="gv_pay_OC_RowCommand" OnRowCreated="gv_pay_OC_RowCreated" OnRowDataBound="gv_pay_OC_RowDataBound"
                    OnRowDeleting="gv_pay_OC_RowDeleting" OnRowEditing="gv_pay_OC_RowEditing" OnRowUpdating="gv_pay_OC_RowUpdating"
                    ShowFooter="True">
                    <Columns>
                        <asp:TemplateField HeaderText="Account Code">
                            <EditItemTemplate>
                                <asp:DropDownList ID="ddlAccountCode" TabIndex="31" runat="server" CssClass="RequiredField EntryFont ddlStype">
                                </asp:DropDownList>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:DropDownList ID="ddlAccountCode" TabIndex="31" runat="server" CssClass="RequiredField EntryFont ddlStype">
                                </asp:DropDownList>
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblAccountCode" CssClass="adminFormFieldHeading" runat="server" Width="95%"
                                    Text='<%# Eval("ACCT_CODE_DESC") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Deduction">
                            <EditItemTemplate>
                                <asp:DropDownList ID="ddlAddSub" TabIndex="31" runat="server" CssClass="RequiredField EntryFont ddlStype">
                                    <asp:ListItem Text="---Select---" Value=""></asp:ListItem>
                                    <%--      <asp:ListItem Text="Add" Value="Add"></asp:ListItem>--%>
                                    <asp:ListItem Text="Deduct" Value="Deduct"></asp:ListItem>
                                </asp:DropDownList>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:DropDownList ID="ddlAddSub" TabIndex="31" runat="server" CssClass="RequiredField EntryFont ddlStype">
                                    <asp:ListItem Text="---Select---" Value=""></asp:ListItem>
                                    <%--   <asp:ListItem Text="Add" Value="Add"></asp:ListItem>--%>
                                    <asp:ListItem Text="Deduct" Value="Deduct"></asp:ListItem>
                                </asp:DropDownList>
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblAddSub" CssClass="adminFormFieldHeading" runat="server" Width="95%"
                                    Text='<%# Eval("AddSub_value") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Amount">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtAROCAmount" TabIndex="32" runat="server" CssClass="  EntryFont  txtBox_N"
                                    Width="95%" Text='<%# Eval("AROC_AMOUNT") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtAROCAmount" TabIndex="32" runat="server" CssClass="  EntryFont  txtBox_N"
                                    Width="95%"></asp:TextBox>
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblAROCAmount" runat="server" Width="95%" Text='<%# Eval("AROC_AMOUNT") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Remarks">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtAROCRemarks" TabIndex="33" runat="server" CssClass="  EntryFont  txtBox"
                                    Width="95%" Text='<%# Eval("REMARKS") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtAROCRemarks" TabIndex="33" runat="server" CssClass="  EntryFont  txtBox"
                                    Width="95%"></asp:TextBox>
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblAROCRemarks" runat="server" Width="95%" Text='<%# Eval("REMARKS") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="">
                            <ItemTemplate>
                                <asp:ImageButton ID="ibtnEdit" runat="server" AlternateText="Edit" CausesValidation="false"
                                    TabIndex="35" ToolTip="Edit" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                                <asp:ImageButton ID="ibtnDelete" runat="server" AlternateText="Delete" CausesValidation="false"
                                    TabIndex="36" ToolTip="Delete" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:ImageButton ID="ibtnUpdate" runat="server" AlternateText="Update" CommandName="Update"
                                    TabIndex="37" ToolTip="Update" Height="20px" ImageUrl="~/Images/Update.png" />
                                <asp:ImageButton ID="ibtnCancel" runat="server" AlternateText="Cancel" CausesValidation="false"
                                    TabIndex="38" ToolTip="Cancel" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:ImageButton ID="ibtnInsert" TabIndex="34" runat="server" AlternateText="Add"
                                    ToolTip="Add" CommandName="FooterInsert" ImageUrl="~/Images/Add.jpg" />
                            </FooterTemplate>
                            <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Center" />
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                    <HeaderStyle CssClass="GridHeader" />
                    <AlternatingRowStyle CssClass="GrdAltRow" />
                </asp:GridView>
            </asp:Panel>
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                            TabIndex="9" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="10" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="11" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="12" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table width="100%">
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
        <div id="div6">
            <asp:HiddenField runat="server" ID="btnView1" />
            <cc2:ModalPopupExtender ID="ModalPopupExtender2" runat="server" TargetControlID="btnView1"
                PopupControlID="Panel1" CancelControlID="Button2" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="Panel1" runat="server">
                <div class="ConfirmForm">
                    <table width="100%">
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <iframe id="ifrmInvoice" name="centerfrm" class="frames" frameborder="0" width="1000px"
                                    height="320px" allowtransparency="true" style="border-style: solid; border-width: 0px;
                                    background-color: transparent;" src="../AP/InvoicePopup.aspx" runat="server">
                                </iframe>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="Button2" Text="Close" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
        <div>
            <asp:HiddenField runat="server" ID="btnStopPayment1" />
            <cc2:ModalPopupExtender ID="popStopPayment" runat="server" TargetControlID="btnStopPayment1"
                PopupControlID="Panel2" CancelControlID="btnClose" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="Panel2" runat="server">
                <div class="ConfirmForm">
                    <div class="divRowContainer" runat="server" id="div8">
                        <div class="lblBox LNOrient" style=" width: 150px" id="Div9">
                            Payment Stop Date
                        </div>
                        <div class="divtxtBox LNOrient" style=" width: 150px">
                            <asp:TextBox ID="txtStopDate" runat="server" CssClass="validate[] validate[custom[ReqDateDDMMYYY]]  txtBox"
                                Width="100%" TabIndex="1"></asp:TextBox>
                            <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender2" TargetControlID="txtStopDate"
                                OnClientDateSelectionChanged="checkDate" />
                        </div>
                        <div class="lblBox LNOrient" style=" width: 150px" id="Div10">
                            Payment Reason
                        </div>
                        <div class="divtxtBox LNOrient" style=" width: 150px">
                            <asp:TextBox ID="txtStopPaymentReason" MaxLength="50" runat="server" CssClass="validate[]   txtBox"
                                Width="100%" TabIndex="2"></asp:TextBox>
                        </div>
                    </div>
                    <div class="divClear_10">
                    </div>
                    <div class="divRowContainer" runat="server" id="div11">
                        <div class="lblBox LNOrient" style=" width: 150px" id="Div12">
                            Payment Remarks
                        </div>
                        <div class="divtxtBox LNOrient" style=" width: 150px">
                            <asp:TextBox ID="txtStopPayRemarks" MaxLength="500" runat="server" CssClass="validate[]   txtBox"
                                Width="100%" TabIndex="3"></asp:TextBox>
                        </div>
                    </div>
                    <div class="divRowContainer" runat="server" id="div13">
                        <div class="LNOrient" style="width: 150px" id="Div14">
                            <asp:Button runat="server" OnClick="btnUpdateStopPayment_Click" CssClass="button"
                                ID="btnUpdateStopPayment" Text="Submit" Width="60px" />
                        </div>
                        <div class="divtxtBox LNOrient" style="width: 150px">
                            <asp:Button runat="server" CssClass="button" ID="btnClose" Text="Cancel" Width="60px"
                                OnClick="btnClose_Click" />
                        </div>
                    </div>
                    <div class="divClear_10">
                    </div>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/AR/ARChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
