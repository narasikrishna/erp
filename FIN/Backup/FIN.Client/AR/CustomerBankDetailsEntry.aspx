﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="CustomerBankDetailsEntry.aspx.cs" Inherits="FIN.Client.AR.CustomerBankDetailsEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 800px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 190px" id="lblCustomerNumber">
                Customer Number
            </div>
            <div class="divtxtBox LNOrient" style="width: 250px">
                <asp:DropDownList ID="ddlCustomerNumber" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    Width="150px" TabIndex="1" AutoPostBack="True" OnSelectedIndexChanged="ddlCustomerNumber_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <%--  <div class="lblBox" style="float: left; width: 190px" id="lblCustomerName">
                Customer Name
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:TextBox ID="txtCustomerName" CssClass="validate[required] txtBox" runat="server"
                    Enabled="true" TabIndex="2"></asp:TextBox>
            </div>--%>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 190px" id="Div3">
                Customer Name
            </div>
            <div class="divtxtBox LNOrient" style="width: 250px">
                <asp:TextBox ID="txtCustomerName" CssClass="validate[required] txtBox" runat="server"
                    Enabled="true" TabIndex="2"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 190px" id="Div1">
                Customer Site
            </div>
            <div class="divtxtBox LNOrient" style="width: 250px">
                <asp:DropDownList ID="ddlCustomersite" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    Width="150px" TabIndex="3" AutoPostBack="True" OnSelectedIndexChanged="ddlCustomersite_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
           <%-- <div class="lblBox" style="float: left; width: 190px" id="Div2">
                Site Name
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:TextBox ID="txtName" CssClass="validate[required] txtBox" runat="server" Enabled="false"
                    TabIndex="4"></asp:TextBox>
            </div>--%>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 190px" id="Div4">
                Site Name
            </div>
            <div class="divtxtBox LNOrient" style="width: 250px">
                <asp:TextBox ID="txtName" CssClass="validate[required] txtBox" runat="server" Enabled="false"
                    TabIndex="4"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="LNOrient">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                DataKeyNames="PK_ID,BANK_ID,BANK_BRANCH_ID,VALUE_KEY_ID,DELETED" OnRowCancelingEdit="gvData_RowCancelingEdit"
                OnRowCommand="gvData_RowCommand" OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound"
                OnRowDeleting="gvData_RowDeleting" OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating"
                ShowFooter="True">
                <Columns>
                    <asp:TemplateField HeaderText="Bank Name">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlBankName" runat="server" CssClass="ddlStype" TabIndex="13"
                                AutoPostBack="True" Width="100%" OnSelectedIndexChanged="ddlBankName_SelectedIndexChanged">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlBankName" runat="server" CssClass="ddlStype" TabIndex="5"
                                AutoPostBack="True" Width="100%" OnSelectedIndexChanged="ddlBankName_SelectedIndexChanged">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblBankName" CssClass="adminFormFieldHeading" runat="server" Text='<%# Eval("BANK_NAME") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Branch Name">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlBranchName" Width="100%" runat="server" TabIndex="14" CssClass="ddlStype"
                                AutoPostBack="True" OnSelectedIndexChanged="ddlBranchName_SelectedIndexChanged">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlBranchName" Width="100%" runat="server" TabIndex="6" CssClass="ddlStype"
                                AutoPostBack="True" OnSelectedIndexChanged="ddlBranchName_SelectedIndexChanged">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblBranchName" CssClass="adminFormFieldHeading" runat="server" Text='<%# Eval("BANK_BRANCH_NAME") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="IBAN">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtifsc" Enabled="false" runat="server" Text='<%# Eval("BANK_IFSC_CODE") %>'
                                TabIndex="15" CssClass="EntryFont RequiredField txtBox_N" Width="100px"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtifsc" Enabled="false" runat="server" CssClass="EntryFont RequiredField txtBox"
                                TabIndex="7" Width="100px"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblifsc" runat="server" Text='<%# Eval("BANK_IFSC_CODE") %>' Width="130px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Swift Code">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtSwift" Enabled="false" runat="server" Text='<%# Eval("BANK_SWIFT_CODE") %>'
                                TabIndex="16" CssClass="EntryFont RequiredField txtBox_N" Width="100px"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtSwift" Enabled="false" runat="server" CssClass="EntryFont RequiredField txtBox"
                                TabIndex="8" Width="100px"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblSwift" runat="server" Text='<%# Eval("BANK_SWIFT_CODE") %>' Width="130px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Account Type">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlAccountType" Width="98%" TabIndex="17" runat="server" CssClass="ddlStype">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlAccountType" Width="98%" TabIndex="9" runat="server" CssClass="ddlStype">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblAccountType" CssClass="adminFormFieldHeading" runat="server" Text='<%# Eval("VALUE_NAME") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Account Number">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtAccountNumber" runat="server" Text='<%# Eval("VENDOR_BANK_ACCOUNT_CODE") %>'
                                TabIndex="18" MaxLength="50" CssClass="EntryFont RequiredField txtBox_N" Width="150px"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtAccountNumber" runat="server" CssClass="EntryFont RequiredField txtBox"
                                TabIndex="10" MaxLength="50" Width="150px"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblAccountNumber" runat="server" Text='<%# Eval("VENDOR_BANK_ACCOUNT_CODE") %>'
                                Width="150px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <%--<asp:TemplateField HeaderText="Start Date">
                        <EditItemTemplate>
                            <asp:TextBox Width="150px" ID="dtpStartDate" MaxLength="10" runat="server" CssClass="validate[required] validate[custom[ReqDateDDMMYYY],dateRange[dg1]] RequiredField txtBox"
                                Text='<%#  Eval("EFFECTIVE_FROM_DT","{0:dd/MM/yyyy}") %>' Onkeypress="return DateKeyCheck(event,this);"></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender8" runat="server" Format="dd/MM/yyyy" TargetControlID="dtpStartDate">
                            </cc2:CalendarExtender>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars="/"
                                FilterType="Numbers,Custom" TargetControlID="dtpStartDate" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox Width="150px" ID="dtpStartDate" TabIndex="6" MaxLength="10" runat="server"
                                CssClass="RequiredField EntryFont  txtBox" Onkeypress="return DateKeyCheck(event,this);"></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender9" runat="server" Format="dd/MM/yyyy" TargetControlID="dtpStartDate">
                            </cc2:CalendarExtender>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars="/"
                                FilterType="Numbers,Custom" TargetControlID="dtpStartDate" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label Width="150px" ID="lblStartDate" runat="server" Text='<%# Eval("EFFECTIVE_FROM_DT","{0:dd/MM/yyyy}") %>'></asp:Label>
                        </ItemTemplate>
                        <ControlStyle Height="25px"></ControlStyle>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="End Date">
                        <EditItemTemplate>
                            <asp:TextBox ID="dtpEndDate" MaxLength="10" runat="server" CssClass="validate[custom[ReqDateDDMMYYY],dateRange[dg1]] txtBox"
                                Width="150px" Text='<%#  Eval("EFFECTIVE_TO_DT","{0:dd/MM/yyyy}") %>'></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender10" runat="server" Format="dd/MM/yyyy"
                                TargetControlID="dtpEndDate">
                            </cc2:CalendarExtender>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars="/"
                                FilterType="Numbers,Custom" TargetControlID="dtpEndDate" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="dtpEndDate" TabIndex="7" MaxLength="10" runat="server" CssClass="EntryFont  txtBox"
                                Width="150px"></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender10" runat="server" Format="dd/MM/yyyy"
                                TargetControlID="dtpEndDate">
                            </cc2:CalendarExtender>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" ValidChars="/"
                                FilterType="Numbers,Custom" TargetControlID="dtpEndDate" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label Width="150px" ID="lblEndDate" runat="server" Text='<%# Eval("EFFECTIVE_TO_DT","{0:dd/MM/yyyy}") %>'></asp:Label>
                        </ItemTemplate>
                        <ControlStyle Height="25px"></ControlStyle>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>--%>
                    <asp:TemplateField HeaderText="Active">
                        <EditItemTemplate>
                            <asp:CheckBox ID="chkAct" runat="server" Width="30px" TabIndex="19" Checked='<%# Convert.ToBoolean(Eval("ENABLED_FLAG")) %>' />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:CheckBox ID="chkAct" TabIndex="11" runat="server" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkAct" runat="server" Width="30px" Checked='<%# Convert.ToBoolean(Eval("ENABLED_FLAG")) %>'
                                Enabled="false" />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Add/Edit">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" runat="server" AlternateText="Edit" CausesValidation="false"
                                TabIndex="20" ToolTip="Edit" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                            <asp:ImageButton ID="ibtnDelete" runat="server" AlternateText="Delete" CausesValidation="false"
                                TabIndex="21" ToolTip="Delete" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibtnUpdate" runat="server" AlternateText="Update" CommandName="Update"
                                TabIndex="22" ToolTip="Update" Height="20px" ImageUrl="~/Images/Update.png" />
                            <asp:ImageButton ID="ibtnCancel" runat="server" AlternateText="Cancel" CausesValidation="false"
                                TabIndex="23" ToolTip="Cancel" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="ibtnInsert" runat="server" AlternateText="Add" CommandName="FooterInsert"
                                TabIndex="12" ToolTip="Add" ImageUrl="~/Images/Add.jpg" />
                        </FooterTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                            TabIndex="11" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn"  />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn"  />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table>
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/AR/ARChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();


                return $("#form1").validationEngine('validate')
            })


        }

    </script>
</asp:Content>
