﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="DeliveryChallanEntry.aspx.cs" Inherits="FIN.Client.AR.DeliveryChallanEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 800px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" align="center">
            <div class="lblBox LNOrient" style="width: 190px" id="lblDCNumber">
                Delivery Voucher Number
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:TextBox ID="txtDCNumber" CssClass="validate[required] txtBox" runat="server"
                    Enabled="false" TabIndex="1"></asp:TextBox>
            </div>
            <div class="lblBox LNOrient" style="width: 190px" id="lblDCDate">
                Delivery Voucher Date
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:TextBox ID="txtDCDate" CssClass="validate[required] RequiredField txtBox" runat="server"
                    TabIndex="2"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtDCDate"
                    OnClientDateSelectionChanged="checkDate" />
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtDCDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" align="left">
            <div class="lblBox LNOrient" style="width: 190px" id="lblInspected">
                Inspected
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:CheckBox ID="chkInspected" runat="server" TabIndex="3"  OnCheckedChanged="chkInspection_CheckedChanged" AutoPostBack="true"/>
            </div>
            <div class="lblBox LNOrient" style="width: 190px" id="lblEmployeeName">
                Employee Name
            </div>
            <div class="divtxtBox LNOrient" style="width: 203px">
                <asp:DropDownList ID="ddlEmployeeName" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    Width="150px" TabIndex="4">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" align="center">
            <div class="lblBox LNOrient" style="width: 190px" id="lblShipmentNumber">
                Shipment Number
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:TextBox ID="txtShipmentNumber" CssClass="validate[required] RequiredField txtBox" MaxLength="50"
                    runat="server" TabIndex="5"></asp:TextBox>
            </div>
            <div class="lblBox LNOrient" style="width: 190px" id="lblShipmentDate">
                Shipment Date
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:TextBox ID="txtShipmentDate" CssClass="validate[required] RequiredField txtBox"
                    runat="server" TabIndex="6"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calExtender1" TargetControlID="txtShipmentDate"
                    OnClientDateSelectionChanged="checkDate" />
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtShipmentDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" align="center">
            <div class="lblBox LNOrient" style="width: 190px" id="lblShipmentTrackingNo">
                Shipment Tracking No
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:TextBox ID="txtShipmentTrackingNo" CssClass="validate[required] RequiredField txtBox" MaxLength="50"
                    runat="server" TabIndex="7"></asp:TextBox>
            </div>
            <div class="lblBox LNOrient" style="width: 190px" id="lblShipmentReceivedBy">
                Shipment Received By
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:TextBox ID="txtShipmentReceivedBy" CssClass="validate[required] RequiredField txtBox" MaxLength="50"
                    runat="server" TabIndex="8"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" align="left">
            <div class="lblBox LNOrient" style="width: 190px" id="lblModeofTransport">
                Mode of Transport
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:DropDownList ID="ddlModeofTransport" runat="server" CssClass="ddlStype" TabIndex="9" Width="150px">
                </asp:DropDownList>
            </div>
            <div class="lblBox LNOrient" style="width: 190px" id="lblShipmentCompany">
                Shipment Company
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:TextBox ID="txtShipmentfCompany" CssClass="validate[required] RequiredField txtBox" MaxLength="50"
                    runat="server" TabIndex="10"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" align="center" runat="server" id="divsingleorder" visible="false">
            <div class="lblBox LNOrient" style="width: 190px" id="lblSingleOrder">
                Single Order
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:CheckBox ID="chkSingleOrder" runat="server" Checked="True" TabIndex="11" />
            </div>
            <div class="lblBox LNOrient" style="width: 190px" id="lblSalesOrder">
                Sales Order
            </div>
            <div class="divtxtBox LNOrient" style="width: 150px">
                <asp:DropDownList ID="ddlSalesOrder" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    Width="150px" TabIndex="12" 
                    onselectedindexchanged="ddlSalesOrder_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="LNOrient">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"  Width="1200px" 
                DataKeyNames="OM_DC_DTL_ID,OM_ORDER_ID,ITEM_ID,DELETED" OnRowCancelingEdit="gvData_RowCancelingEdit"
                OnRowCommand="gvData_RowCommand" OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound"
                OnRowDeleting="gvData_RowDeleting" OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating"
                ShowFooter="True">
                <Columns>
                    <asp:TemplateField HeaderText="Line No" Visible="false">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtLineNo" TabIndex="22" runat="server" Text='<%# Eval("OM_DC_LINE_NUM") %>'
                                CssClass="EntryFont RequiredField txtBox_N" Enabled="false" Width="95%"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtLineNo" TabIndex="13" runat="server" Enabled="false" CssClass="EntryFont RequiredField txtBox"
                                Width="95%"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblLineNo" runat="server" Text='<%# Eval("OM_DC_LINE_NUM") %>' ></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Order Number">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlOrderNumber" Width="99%" TabIndex="23" runat="server" CssClass="EntryFont RequiredField ddlStype"
                                AutoPostBack="True"  OnSelectedIndexChanged="ddlOrderNumber_SelectedIndexChanged">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlOrderNumber" TabIndex="14" runat="server" CssClass="EntryFont RequiredField ddlStype"
                                AutoPostBack="True" OnSelectedIndexChanged="ddlOrderNumber_SelectedIndexChanged"  >
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblOrderNumber"   CssClass="adminFormFieldHeading" runat="server" Text='<%# Eval("OM_ORDER_NUM") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle  HorizontalAlign="Left" />
                        <FooterStyle Width ="130px" />
                    </asp:TemplateField>
                   <%-- <asp:TemplateField HeaderText="Order Line No">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtOrderLineNo" Enabled="false" TabIndex="24" runat="server" Text='<%# Eval("OM_ORDER_LINE_NUM") %>' MaxLength="13"
                                CssClass="EntryFont RequiredField txtBox_N" Width="100px"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtOrderLineNo" Enabled="false" TabIndex="15" runat="server" CssClass="EntryFont RequiredField txtBox_N" MaxLength="13"
                                Width="100px"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblOrderLineNo" runat="server" Text='<%# Eval("OM_ORDER_LINE_NUM") %>'
                                Width="130px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>--%>
                    <asp:TemplateField HeaderText="Item/Description">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlItemName" Enabled="True" runat="server" CssClass="ddlStype RequiredField"
                                TabIndex="24" Width="150px" AutoPostBack="True" OnSelectedIndexChanged="ddlItemName_SelectedIndexChanged">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlItemName" Enabled="True" runat="server" CssClass="ddlStype RequiredField"
                                TabIndex="15" Width="150px" AutoPostBack="True" OnSelectedIndexChanged="ddlItemName_SelectedIndexChanged">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblItemName" runat="server" Text='<%# Eval("ITEM_NAME") %>' Width="150px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="Order Line No">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtOrderLineNo" Enabled="false" TabIndex="25" runat="server" Text='<%# Eval("OM_ORDER_LINE_NUM") %>' MaxLength="13"
                                CssClass="EntryFont RequiredField txtBox_N" Width="96%"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtOrderLineNo" Enabled="false" TabIndex="16" runat="server" CssClass="EntryFont RequiredField txtBox_N" MaxLength="13"
                                Width="96%"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblOrderLineNo" runat="server" Text='<%# Eval("OM_ORDER_LINE_NUM") %>'
                                ></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Quantity">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtQuantity" TabIndex="26" runat="server" Text='<%# Eval("OM_ITEM_QTY") %>' MaxLength="13"
                                CssClass="EntryFont RequiredField txtBox_N" Width="96%"></asp:TextBox>
                                 <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers"
                            TargetControlID="txtQuantity" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtQuantity" TabIndex="17" runat="server" CssClass="EntryFont RequiredField txtBox_N" MaxLength="13"
                                Enabled="false" Width="96%"></asp:TextBox>
                                 <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Numbers"
                            TargetControlID="txtQuantity" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblQuantity" runat="server" Text='<%# Eval("OM_ITEM_QTY") %>' ></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Shipped Quantity">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtShippedQuantity" TabIndex="27" runat="server" Text='<%# Eval("OM_SHIPPED_QTY") %>' MaxLength="13"
                             OnTextChanged="txtQuantity_TextChanged" AutoPostBack="true" 
                                CssClass="EntryFont RequiredField txtBox_N" Width="96%"></asp:TextBox>
                                 <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterType="Numbers"
                            TargetControlID="txtShippedQuantity" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtShippedQuantity" TabIndex="18" runat="server" CssClass="EntryFont RequiredField txtBox_N" MaxLength="13"
                             OnTextChanged="txtQuantity_TextChanged" AutoPostBack="true" 
                                Width="96%"></asp:TextBox>
                                 <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" FilterType="Numbers"
                            TargetControlID="txtShippedQuantity" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblShippedQuantity" runat="server" Text='<%# Eval("OM_SHIPPED_QTY") %>'
                                ></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Returned Quantity">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtReturnedQuantity" TabIndex="28" runat="server" Text='<%# Eval("OM_RETURN_QTY") %>' MaxLength="13"
                                OnTextChanged="txtReturnedQuantity_TextChanged" AutoPostBack="true" CssClass="EntryFont  txtBox_N"
                                Width="96%"></asp:TextBox>
                                 <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterType="Numbers"
                            TargetControlID="txtReturnedQuantity" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtReturnedQuantity" TabIndex="19" runat="server" CssClass="EntryFont  txtBox_N" MaxLength="13"
                                OnTextChanged="txtReturnedQuantity_TextChanged" AutoPostBack="true" Width="96%"></asp:TextBox>
                                 <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" FilterType="Numbers"
                            TargetControlID="txtReturnedQuantity" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblReturnedQuantity" runat="server" Text='<%# Eval("OM_RETURN_QTY") %>'
                                ></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Remarks">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtRemarks" TabIndex="29" runat="server" Text='<%# Eval("OM_REMARKS") %>' MaxLength="200"
                                CssClass="EntryFont RequiredField txtBox" Width="96%"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtRemarks" TabIndex="20"  runat="server" CssClass="EntryFont RequiredField txtBox" MaxLength="200"
                                Width="96%"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblRemarks" runat="server" Text='<%# Eval("OM_REMARKS") %>' ></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Add/Edit">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" TabIndex="32" runat="server" AlternateText="Edit"
                                CausesValidation="false" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                            <asp:ImageButton ID="ibtnDelete" TabIndex="33" runat="server" AlternateText="Delete"
                                CausesValidation="false" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibtnUpdate" TabIndex="30" runat="server" AlternateText="Update"
                                CommandName="Update" Height="20px" ImageUrl="~/Images/Update.png" />
                            <asp:ImageButton ID="ibtnCancel" TabIndex="31" runat="server" AlternateText="Cancel"
                                CausesValidation="false" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="ibtnInsert" TabIndex="21" runat="server" AlternateText="Add"
                                CommandName="FooterInsert" ImageUrl="~/Images/Add.jpg" />
                        </FooterTemplate>
                        <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divClear_10">
        </div>
        <%-- <div class="divRowContainer" align="center">
            <div class="lblBox" style="float: left; width: 600px" id="lblTotal">
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:TextBox ID="txtTotal" CssClass="validate[required] txtBox" runat="server"></asp:TextBox>
            </div>
        </div>--%>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                            TabIndex="26" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn"  />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn"  />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn"  />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table>
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/AR/ARChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
