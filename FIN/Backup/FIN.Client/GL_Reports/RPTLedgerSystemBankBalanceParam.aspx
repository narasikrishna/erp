﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="RPTLedgerSystemBankBalanceParam.aspx.cs" Inherits="FIN.Client.GL_Reports.RPTLedgerSystemBankBalanceParam" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 900px" id="div1">
        <div class="divRowContainer">
            <div class="divRowContainer" style="display:none">
                <div class="lblBox LNOrient" style="  width: 120px" id="lblDate">
                    From Date
                </div>
                <div class="divtxtBox LNOrient" style="  width: 150px">
                    <asp:TextBox ID="txtFromDate" runat="server" TabIndex="1" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]]  RequiredField txtBox"></asp:TextBox>
                    <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calExtender1" TargetControlID="txtFromDate"
                        OnClientDateSelectionChanged="checkDate" />
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer" style="display:none">
                <div class="lblBox LNOrient" style="  width: 120px" id="Div2">
                    To Date
                </div>
                <div class="divtxtBox LNOrient" style="  width: 150px">
                    <asp:TextBox ID="txtToDate" runat="server" TabIndex="2" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]]  RequiredField txtBox"></asp:TextBox>
                    <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtToDate"
                        OnClientDateSelectionChanged="checkDate" />
                </div>
            </div>
            <div class="divClear_10">
            </div>
              <div class="divRowContainer" style="display:none;">
                <div class="lblBox LNOrient" style="  width: 120px;display:none;" id="lblAdjPeriod">
                     Global Segment
                </div>
                <div class="divtxtBox LNOrient" style="  width: 540px">
                    <asp:DropDownList ID="ddlsegment" runat="server" CssClass="validate[required] ddlStype"
                        TabIndex="1">
                    </asp:DropDownList>
                </div>
            </div>
             <div class="divClear_10">
            </div>
            <div class="divRowContainer">
             <div class="lblBox LNOrient" style="  width: 120px" id="Div4">
                    Bank
                </div>
                <div class="divtxtBox LNOrient" style="  width: 200px">
                    <asp:DropDownList ID="ddlBank" runat="server" CssClass="ddlStype"
                        TabIndex="2">
                    </asp:DropDownList>
                </div>
                 <div class="colspace  LNOrient" >
                &nbsp</div>
                <div class="lblBox LNOrient" style="  width: 120px" id="lblPeriod">
                    Period
                </div>
                <div class="divtxtBox LNOrient" style="  width: 200px">
                    <asp:DropDownList ID="ddlperiod" runat="server" CssClass="ddlStype validate[required] RequiredField"
                        TabIndex="3">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="divClear_10">
            </div>
          
          <%--  <div class="divRowContainer" id="Post" runat="server" visible="false">
                <div class="lblBox LNOrient" style="  width: 120px" id="Div3">
                   Include Un Post
                </div>
                <div class="divtxtBox LNOrient" style="  width: 200px">
                    <asp:CheckBox ID="chkUnPost" runat="server" />
                </div>
                 <div class="colspace" style=" ">
                &nbsp</div>
            <div class="lblBox LNOrient" style="  width: 120px" id="Div6">
               Include With Zero
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:CheckBox ID="chkWithZero" runat="server" Text="" Checked="false" />
            </div>
            </div>
            <div class="divClear_10">
            </div>--%>
            <div class="divFormcontainer" style="width: 500px" id="divMainContainer">
                <div class="divRowContainer divReportAction">
                    <div>
                        <asp:ImageButton ID="btnSave" runat="server" ImageUrl="../Images/show-report-icon.png" TabIndex="4"
                            Width="35px" Height="25px" OnClick="btnSave_Click" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/GL/GLChangeLang.js" type="text/javascript"></script>
    <script src="../LanguageScript/GL/GLChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>