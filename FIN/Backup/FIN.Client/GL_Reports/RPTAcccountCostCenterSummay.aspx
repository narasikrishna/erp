﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="RPTAcccountCostCenterSummay.aspx.cs" Inherits="FIN.Client.GL_Reports.RPTAcccountCostCenterSummay" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1000px" id="div1">
        <div class="divRowContainer" style="display:none" >
            <div class="lblBox LNOrient" style="  width: 150px" id="lblGlobalSegment">
                Global Segment
            </div>
            <div class="divtxtBox LNOrient" style="  width: 550px">
                <asp:DropDownList ID="ddlGlobalSegment" runat="server" TabIndex="1" CssClass="validate[required] ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 150px" id="Div5">
                Cost Center
            </div>
            <div class="divtxtBox LNOrient" style="  width: 550px">
                <asp:DropDownList ID="ddlSegmentValue" runat="server" TabIndex="2" CssClass=" ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 150px">
                From Journal Number
            </div>
            <div class="divtxtBox LNOrient" style="  width: 200px">
                <asp:DropDownList ID="ddlFromJournal" runat="server" TabIndex="3" CssClass=" ddlStype">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="  width: 130px">
                To Journal Number
            </div>
            <div class="divtxtBox LNOrient" style="  width: 200px">
                <asp:DropDownList ID="ddlToJournal" runat="server" TabIndex="4" CssClass=" ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 150px" id="lblDate">
                From Date
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:TextBox ID="txtFromDate" runat="server" TabIndex="5" CssClass="txtBox"
                    AutoPostBack="true" OnTextChanged="txtToDate_TextChanged"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calExtender1" TargetControlID="txtFromDate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="  width: 130px">
                To Date
            </div>
            <div class="divtxtBox LNOrient" style="  width: 180px">
                <asp:TextBox ID="txtToDate" runat="server" TabIndex="6" CssClass="txtBox"
                    AutoPostBack="true" OnTextChanged="txtToDate_TextChanged"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtToDate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" runat="server" visible="false">
            <div class="lblBox LNOrient" style="  width: 150px">
                From Account
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:TextBox ID="txtFromAccount" runat="server" TabIndex="7" CssClass="txtBox"></asp:TextBox>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="  width: 130px">
                To Account
            </div>
            <div class="divtxtBox LNOrient" style="  width: 180px">
                <asp:TextBox ID="txtToAccount" runat="server" TabIndex="8" CssClass="txtBox"></asp:TextBox>
            </div>
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 150px">
                From Account
            </div>
            <div class="divtxtBox LNOrient" style="  width: 450px">
                <asp:DropDownList ID="ddlFromAccNumber" runat="server" CssClass=" RequiredField ddlStype"
                    TabIndex="6">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 150px">
                To Account
            </div>
            <div class="divtxtBox LNOrient" style="  width: 450px">
                <asp:DropDownList ID="ddlToAccNumber" runat="server" CssClass=" RequiredField ddlStype"
                    TabIndex="6">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 150px" id="lblAccountingYear">
                Debit From Amount
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:TextBox ID="txtFromValue" CssClass="txtBox_N" runat="server" TabIndex="9"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers,custom"
                    ValidChars=".," TargetControlID="txtFromValue" />
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="  width: 130px" id="Div7">
                Debit To Amount
            </div>
            <div class="divtxtBox LNOrient" style="  width: 180px">
                <asp:TextBox ID="txtToValue" CssClass="txtBox_N" runat="server" TabIndex="10"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers,custom"
                    ValidChars=".," TargetControlID="txtToValue" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 150px" id="Div2">
                Credit From Amount
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:TextBox ID="txtCreditFrom" CssClass="txtBox_N" runat="server" TabIndex="11"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Numbers,custom"
                    ValidChars=".," TargetControlID="txtCreditFrom" />
            </div>
             <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="  width: 130px" id="Div3">
                Credit To Amount
            </div>
            <div class="divtxtBox LNOrient" style="  width: 180px">
                <asp:TextBox ID="txtCreditTo" CssClass="txtBox_N" runat="server" TabIndex="12"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Numbers,custom"
                    ValidChars=".," TargetControlID="txtCreditTo" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" id="user" runat="server" visible="false">
            <div class="lblBox LNOrient" style="  width: 150px" id="Div8">
                User
            </div>
            <div class="divtxtBox LNOrient" style="  width: 470px">
                <asp:DropDownList ID="ddlUser" runat="server" TabIndex="11" CssClass=" ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" id="post" runat="server" style="display:none">
            <div class="lblBox LNOrient" style="  width: 150px" id="Div4">
                Include Unposted
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:CheckBox ID="chkUnPost" runat="server" Text="" Checked="false" TabIndex="12" />
                <div class="colspace" style=" ">
                    &nbsp</div>
            </div>
            <%--  <div class="lblBox LNOrient" style="  width: 150px;Display:none" id="Div2">
              Include  With Zero
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px;Display:none">
                <asp:CheckBox ID="chkWithZero" runat="server" Text="" Checked="false" TabIndex="13" />
            </div>--%>
        </div>
        <div class="divClear_10">
        </div>
         <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 120px" id="Div6">
                Journal Status
            </div>
            <div class="divtxtBox LNOrient" style="  width: 550px" align="center">
                <asp:RadioButtonList ID="rbUNPosted" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Value="P" Selected="True">Posted &nbsp; &nbsp; &nbsp;    </asp:ListItem>
                    <asp:ListItem Value="U">Unposted &nbsp; &nbsp; &nbsp; </asp:ListItem>
                    <asp:ListItem Value="B">Both &nbsp; &nbsp; &nbsp; </asp:ListItem>
                </asp:RadioButtonList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer divReportAction" style="width: 320px" align="left">
            <asp:ImageButton ID="btnSave" runat="server" ImageUrl="../Images/show-report-icon.png"
                Width="35px" Height="25px" OnClick="btnSave_Click" TabIndex="13" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/GL/GLChangeLang.js" type="text/javascript"></script>
  <%--  <script src="../LanguageScript/AP/APChangeLang.js" type="text/javascript"></script>--%>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
