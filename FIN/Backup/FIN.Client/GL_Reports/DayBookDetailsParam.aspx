﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="DayBookDetailsParam.aspx.cs" Inherits="FIN.Client.GL_Reports.DayBookDetailsParam" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1000px" id="div1">
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 120px" id="Div5">
                Account Group
            </div>
            <div class="divtxtBox LNOrient" style="  width: 620px">
                <asp:DropDownList ID="ddlAccountGroup" runat="server" TabIndex="1" CssClass=" ddlStype"
                    AutoPostBack="True" OnSelectedIndexChanged="ddlAccountGroup_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div class="divtxtBox LNOrient" style="  width: 250px; display: none">
                <asp:DropDownList ID="ddlAccCode" runat="server" CssClass="ddlStype RequiredField"
                    TabIndex="2" AutoPostBack="True">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" runat="server" visible="false">
            <div class="lblBox LNOrient" style="  width: 120PX">
                From Account
            </div>
            <div class="divtxtBox LNOrient" style="  width: 250PX">
                <asp:TextBox ID="txtFromAccount" runat="server" TabIndex="10" CssClass="txtBox"></asp:TextBox>
            </div>
            <div class="lblBox LNOrient" style="  width: 120px">
                To Account
            </div>
            <div class="divtxtBox LNOrient" style="  width: 250px">
                <asp:TextBox ID="txtToAccount" runat="server" TabIndex="3" CssClass="txtBox"></asp:TextBox>
            </div>
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 120px">
                From Account
            </div>
            <div class="divtxtBox LNOrient" style="  width: 450px">
                <asp:DropDownList ID="ddlFromAccNumber" runat="server" CssClass="ddlStype"
                    TabIndex="4">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 120px">
                To Account
            </div>
            <div class="divtxtBox LNOrient" style="  width: 450px">
                <asp:DropDownList ID="ddlToAccNumber" runat="server" CssClass="ddlStype"
                    TabIndex="11">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 120px" id="lblDate">
                From Date
            </div>
            <div class="divtxtBox LNOrient" style="  width: 250px">
                <asp:TextBox ID="txtFromDate" runat="server" TabIndex="3" CssClass="txtBox"
                    AutoPostBack="True" OnTextChanged="txtFromDate_TextChanged"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calExtender1" TargetControlID="txtFromDate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="  width: 120px" id="Div3">
                To Date
            </div>
            <div class="divtxtBox LNOrient" style="  width: 250px">
                <asp:TextBox ID="txtToDate" runat="server" TabIndex="4" CssClass="txtBox"
                    AutoPostBack="True" OnTextChanged="txtFromDate_TextChanged"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtToDate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 120px">
                From Journal Number
            </div>
            <div class="divtxtBox LNOrient" style="  width: 250px">
                <asp:DropDownList ID="ddlFromJournal" runat="server" TabIndex="5" CssClass=" ddlStype">
                </asp:DropDownList>
            </div>
             <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="  width: 120px">
                To Journal Number
            </div>
            <div class="divtxtBox LNOrient" style="  width: 250px">
                <asp:DropDownList ID="ddlToJournal" runat="server" TabIndex="6" CssClass=" ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 120px" id="Div7">
                Journal Type
            </div>
            <div class="divtxtBox LNOrient" style="  width: 250px">
                <asp:DropDownList ID="ddlJournalType" runat="server" CssClass="ddlStype RequiredField"
                    TabIndex="7">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="  width: 120px" id="Div8">
                Journal Reference
            </div>
            <div class="divtxtBox LNOrient" style="  width: 250px">
                <asp:DropDownList ID="ddlJournalRef" runat="server" CssClass="ddlStype"
                    TabIndex="8">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 120px" id="Div9">
                Segment Value
            </div>
            <div class="divtxtBox LNOrient" style="  width: 250px">
                <asp:DropDownList ID="ddlSegment" runat="server" CssClass="ddlStype"
                    TabIndex="9">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="  width: 120px" id="Div12">
                Journal Source
            </div>
            <div class="divtxtBox LNOrient" style="  width: 250px">
                <asp:DropDownList ID="ddlJournalSource" runat="server" CssClass="ddlStype"
                    TabIndex="8">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 120px" id="Div10">
                From Amount
            </div>
            <div class="divtxtBox LNOrient" style="  width: 250px">
                <asp:TextBox ID="txtFromAmount" runat="server" TabIndex="10" CssClass=" txtBox_N"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers,Custom"
                    ValidChars=".-" TargetControlID="txtFromAmount" />
            </div>
             <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="  width: 120px" id="Div11">
                To Amount
            </div>
            <div class="divtxtBox LNOrient" style="  width: 250px">
                <asp:TextBox ID="txtToAmount" runat="server" TabIndex="11" CssClass="txtBox_N"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers,Custom"
                    ValidChars=".-" TargetControlID="txtToAmount" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 120px" id="Div4">
                Journal Status
            </div>
            <div class="divtxtBox LNOrient" style="  width: 550px" align="center">
                <asp:RadioButtonList ID="rbUNPosted" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Value="P" Selected="True">Posted &nbsp; &nbsp; &nbsp;    </asp:ListItem>
                    <asp:ListItem Value="U">Unposted &nbsp; &nbsp; &nbsp; </asp:ListItem>
                    <asp:ListItem Value="B">Both &nbsp; &nbsp; &nbsp; </asp:ListItem>
                </asp:RadioButtonList>
            </div>
            <div class="divtxtBox LNOrient" style="  width: 250px">
            </div>
            <%--  <div class="colspace" style=" ">
                &nbsp</div>
            <div class="lblBox LNOrient" style="  width: 120px" id="Div2">
                With Zero
            </div>
            <div class="divtxtBox LNOrient" style="  width: 250px">
                <asp:CheckBox ID="chkWithZero" runat="server" TabIndex="13" Text="" Checked="false" />
            </div>--%>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer divReportAction">
            <table class="ReportTable">
                <tr>
                    <td>
                        <asp:ImageButton ID="btnSave" runat="server" ImageUrl="../Images/show-report-icon.png"
                            TabIndex="14" Width="35px" Height="25px" OnClick="btnSave_Click" />
                    </td>
                    <td>
                        <%--<asp:Button ID="btnPrint" runat="server" Text="Print" CssClass="btn" />--%>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/GL/GLChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();


                return $("#form1").validationEngine('validate')
            })


        }

    </script>
</asp:Content>
