﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true" CodeBehind="RPTInventoryReportsParam.aspx.cs" Inherits="FIN.Client.Reports.GL.RPTInventoryReportsParam" %>
<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 370px" id="divMainContainer">
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="lblWareHouseName">
                WareHouse Name
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:DropDownList ID="ddlWareHouseName" runat="server" TabIndex="3" CssClass="validate[required]  RequiredField ddlStype"
                    Width="150px">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer divReportAction">
            <table class="ReportTable">
                <tr>
                    <td>
                          <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../Images/show-report-icon.png"
                    Width="35px" Height="25px" OnClick="btnSave_Click" />
                    </td>
                    <td>
                       <%-- <asp:Button ID="btnPrint" runat="server" Text="Print" CssClass="btn" />--%>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>

