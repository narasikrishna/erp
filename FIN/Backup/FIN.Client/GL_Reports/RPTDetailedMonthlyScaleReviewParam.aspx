﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="RPTDetailedMonthlyScaleReviewParam.aspx.cs" Inherits="FIN.Client.GL_Reports.RPTDetailedMonthlyScaleReviewParam" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 700px" id="divMainContainer">
        <div class="divRowContainer" style="display:none;">
            <div class="lblBox LNOrient" style="  width: 150px;display:none;" id="lblGlobalSegment">
                Global Segment
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:DropDownList ID="ddlGlobalSegment" runat="server" TabIndex="1" CssClass="validate[required] ddlStype">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="  width: 130px" id="Div1">
                Include Unposted
            </div>
            <div class="lblBox LNOrient" style="  width: 100px" id="Div3">
                <asp:CheckBox ID="chkUnPosted" runat="server" Checked="false" Text=" " TabIndex="2" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" runat="server" visible="false">
            <div class="lblBox LNOrient" style="  width: 150px">
                From Account
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:TextBox ID="txtFromAccount" runat="server" TabIndex="3" CssClass="txtBox"></asp:TextBox>
            </div>
           <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="  width: 130px">
                To Account
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:TextBox ID="txtToAccount" runat="server" TabIndex="4" CssClass="txtBox"></asp:TextBox>
            </div>
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 150px">
                From Account
            </div>
            <div class="divtxtBox LNOrient" style="  width: 455px">
                <asp:DropDownList ID="ddlFromAccNumber" runat="server" CssClass=" RequiredField ddlStype"
                    TabIndex="3">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 150px">
                To Account
            </div>
            <div class="divtxtBox LNOrient" style="  width: 455px">
                <asp:DropDownList ID="ddlToAccNumber" runat="server" CssClass=" RequiredField ddlStype"
                    TabIndex="4">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divRowContainer" style="display: none">
            <div class="lblBox LNOrient" style="  width: 200px" id="Div2">
                Account Description
            </div>
            <div class="divtxtBox LNOrient" style="  width: 470px">
                <asp:DropDownList ID="ddlAccCode" runat="server" CssClass="ddlStype validate[required] RequiredField "
                    TabIndex="5" AutoPostBack="True">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 150px" id="Div8">
                Debit From Amount
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:TextBox ID="txtFromAmt" runat="server" TabIndex="6" CssClass="EntryFont txtBox_N"
                    Width="150px" FilterType="Numbers,Custom" ValidChars=".-"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers,Custom"
                    ValidChars=".-" TargetControlID="txtFromAmt" />
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="  width: 130px" id="Div9">
                Debit To Amount
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:TextBox ID="txtToAmt" runat="server" TabIndex="7" CssClass="EntryFont txtBox_N"
                    Width="150px" FilterType="Numbers,Custom" ValidChars=".-"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers,Custom"
                    ValidChars=".-" TargetControlID="txtToAmt" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 150px" id="Div5">
                Credit From Amount
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:TextBox ID="txtCreditFromAmt" runat="server" TabIndex="8" CssClass="EntryFont txtBox_N"
                    Width="150px" FilterType="Numbers,Custom" ValidChars=".-"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Numbers,Custom"
                    ValidChars=".-" TargetControlID="txtCreditFromAmt" />
            </div>
           <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="  width: 130px" id="Div6">
                Credit To Amount
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:TextBox ID="txtCreditToAmt" runat="server" TabIndex="9" CssClass="EntryFont txtBox_N"
                    Width="150px" FilterType="Numbers,Custom" ValidChars=".-"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Numbers,Custom"
                    ValidChars=".-" TargetControlID="txtCreditToAmt" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 150px" id="lblPeriod">
                Period
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:DropDownList ID="ddlperiod" runat="server" CssClass="validate[required] ddlStype  "
                    TabIndex="8">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="  width: 130px" id="Div4">
                Include Zeros
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:CheckBox ID="chkWithZero" runat="server" Text="" Checked="false" TabIndex="9" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer divReportAction">
            <table class="ReportTable">
                <tr>
                    <td>
                        <asp:ImageButton ID="btnSave" runat="server" ImageUrl="../Images/show-report-icon.png"
                            TabIndex="10" Width="35px" Height="25px" OnClick="btnSave_Click" />
                    </td>
                    <td>
                        <%--<asp:Button ID="btnPrint" runat="server" Text="Print" CssClass="btn" />--%>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/GL/GLChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
