﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.GL;
using FIN.DAL.AP;
using FIN.DAL.AR;
using FIN.BLL;
using FIN.BLL.AP;
using FIN.BLL.AR;
using FIN.BLL.HR;
using FIN.BLL.GL;
using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;
namespace FIN.Client.GL_Reports
{
    public partial class RPTToplevelAccgroupSummaryParam : PageBase
    {
        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountGroupSummaryReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                FillComboBox();
                //FillStartDate();
                Startup();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountGroupSummaryReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void Startup()
        {

            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));

            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();

        }
        private void FillStartDate()
        {

            string str_finyear = FINSP.GetSPFOR_FiscalYear(DateTime.Now.Date.ToString("dd/MM/yyyy"));
            DataTable dtDate = new DataTable();
            if (str_finyear != string.Empty)
            {
                dtDate = DBMethod.ExecuteQuery(FIN.DAL.GL.InventoryReports_DAL.getCalDate(str_finyear)).Tables[0];
                txtDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["cal_eff_start_dt"].ToString());
                txtToDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["call_eff_end_dt"].ToString());
            }



            //    //if (ddl_GB_FINYear.Items.Count > 0)
            //    //{
            //    //    // ddl_GB_FINYear.SelectedIndex = ddl_GB_FINYear.Items.Count - 1;
            //    //    ddl_GB_FINYear.SelectedValue = str_finyear;
            //    //}
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;
                DataTable dtPerioddate = new DataTable();
                dtPerioddate = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountingCalendar_DAL.GetPeriodFromToDate(ddlperiod.SelectedValue.ToString())).Tables[0];
                if (dtPerioddate.Rows.Count > 0)
                {
                    if (dtPerioddate.Rows[0]["period_from_dt"].ToString() != string.Empty)
                    {
                        txtDate.Text = DBMethod.ConvertDateToString(dtPerioddate.Rows[0]["period_from_dt"].ToString());
                    }
                    if (dtPerioddate.Rows[0]["period_to_dt"].ToString() != string.Empty)
                    {
                        txtToDate.Text = DBMethod.ConvertDateToString(dtPerioddate.Rows[0]["period_to_dt"].ToString());
                    }
                }

                if (ddlGlobalSegment.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("SEGMENT_ID", ddlGlobalSegment.SelectedItem.Text);
                }
                if (txtDate.Text != string.Empty)
                {
                    htFilterParameter.Add("From_Date", txtDate.Text);
                }
                if (txtToDate.Text != string.Empty)
                {
                    htFilterParameter.Add("To_Date", txtToDate.Text);
                }
                //if (ddlAccountGroup.SelectedValue != string.Empty)
                //{
                //    htFilterParameter.Add("ACCOUNTGROUP", ddlAccountGroup.SelectedValue);
                //}
                if (ddlperiod.SelectedValue.ToString().Length > 0)
                {
                    htFilterParameter.Add(FINColumnConstants.PERIOD_ID, ddlperiod.SelectedValue.ToString());
                    htFilterParameter.Add(FINColumnConstants.PERIOD_NAME, ddlperiod.SelectedItem.Text.ToString());
                }

                if (ddlFromGroup.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("FromGroup", ddlFromGroup.SelectedValue);
                    htFilterParameter.Add("FromGroupName", ddlFromGroup.SelectedItem.Text);
                }
                if (ddlToGroup.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("ToGroup", ddlToGroup.SelectedValue);
                    htFilterParameter.Add("ToGroupName", ddlToGroup.SelectedItem.Text);
                }


                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;
                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;

                //htFilterParameter.Add("ReportName", Session["ProgramName"].ToString());
                //htFilterParameter.Add("OrgName", VMVServices.Web.Utils.OrganizationName);
                string str_Unpost = "0";
                if (chkUnPost.Checked)
                {
                    str_Unpost = "1";
                }
                string str_GroupId = "ALL";

                DataSet dtReport = new DataSet();
                if (DBMethod.ConvertStringToDate(txtDate.Text).ToString() != string.Empty && DBMethod.ConvertStringToDate(txtToDate.Text).ToString() != string.Empty)
                {
                    //ReportData = FIN.DAL.GL.AccountingGroups_DAL.GetSP_AccountGroupSummary(ddlGlobalSegment.SelectedValue.ToString(), DBMethod.ConvertStringToDate(txtDate.Text), DBMethod.ConvertStringToDate(txtToDate.Text), str_GroupId, str_Unpost, FIN.BLL.GL.AccountingGroups_BLL.getTopAccountGroupSummaryReportData());
                    dtReport = FIN.DAL.GL.AccountingGroups_DAL.GetSP_AccountGroupSummary(ddlGlobalSegment.SelectedValue.ToString(), DBMethod.ConvertStringToDate(txtDate.Text), DBMethod.ConvertStringToDate(txtToDate.Text), str_GroupId, str_Unpost, FIN.BLL.GL.AccountingGroups_BLL.getTopAccountGroupSummaryReportData());
                    //ReportData = FIN.DAL.GL.AccountingGroups_DAL.GetSP_TopBalanceAccountGroupSummary(FIN.BLL.GL.AccountingGroups_BLL.getTopBalanceAccountGroupSummaryReportData());
                    ReportData = DBMethod.ExecuteQuery(FIN.BLL.GL.AccountingGroups_BLL.getTopBalanceAccountGroupSummaryReportData());
                }


                //ReportFormulaParameter = htHeadingParameters;



                //htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());
                //htHeadingParameters.Add("OrgName", VMVServices.Web.Utils.OrganizationName);
                //if (ddlGlobalSegment.SelectedValue != string.Empty)
                //{
                //    htHeadingParameters.Add("SEGMENT_ID", ddlGlobalSegment.SelectedItem.Text);
                //}
                //if (txtDate.Text != string.Empty)
                //{
                //    htHeadingParameters.Add("AccountSummary_Date", txtDate.Text);
                //}

                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerPath + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountGroupSummaryReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }
        private void FillComboBox()
        {
            FIN.BLL.GL.Segments_BLL.GetGlobalSegmentvaluesByOrgName(ref ddlGlobalSegment);
            ddlGlobalSegment.Items.RemoveAt(0);       
            FIN.BLL.GL.AccountingCalendar_BLL.GetAccPeriodBasedOrg4NotNOPPeriod(ref ddlperiod);
            for (int i = 1; i <= 10; i++)
            {
                ddlLevel.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }
        }

        protected void ddlLevel_SelectedIndexChanged(object sender, EventArgs e)
        {
           
            FIN.DAL.GL.AccountingGroups_DAL.GetSP_Proc_Top_Group_Level_bal(ddlLevel.SelectedValue);

            FIN.BLL.GL.AccountingGroups_BLL.getTopBalanceAccountGroups(ref ddlFromGroup);
            FIN.BLL.GL.AccountingGroups_BLL.getTopBalanceAccountGroups(ref ddlToGroup);

        }
    }
}