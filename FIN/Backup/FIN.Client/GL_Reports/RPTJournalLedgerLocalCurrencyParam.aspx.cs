﻿using System;
using System.Linq;
using System.Web;
using FIN.DAL;
using FIN.BLL;
using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;
using System.IO;
using System.Web.UI;
using System.Collections;
using FIN.DAL.GL;
using System.Web.UI.WebControls;
using FIN.BLL.GL;

namespace FIN.Client.GL_Reports
{
    public partial class RPTJournalLedgerLocalCurrencyParam : PageBase
    {
        public int doc_tablecount;
        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("JounalLedgerSystem", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                Startup();
                FillComboBox();
                FillStartDate();
                FillJournalNumber();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("JounalLedgerSystemATC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void Startup()
        {
            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));
        }
        protected void FillComboBox()
        {
            FIN.BLL.GL.Segments_BLL.GetGlobalSegmentvaluesByOrgName(ref ddlSegment);
            ddlSegment.SelectedIndex = 1;
            AccountCodes_BLL.getAccCodeBasedOrgRep(ref ddlFromAccNumber);
            AccountCodes_BLL.getAccCodeBasedOrgRep(ref ddlToAccNumber);
            //FIN.BLL.GL.Segments_BLL.GetGlobalSegmentvalues(ref ddlSegment);
            //FIN.BLL.GL.AccountCodes_BLL.getAccCodeBasedOrg(ref ddlAccCode, VMVServices.Web.Utils.OrganizationID);
            //ddlAccCode.Items.RemoveAt(0);
            //ddlAccCode.Items.Insert(0, new ListItem("ALL", ""));
        }
        private void FillStartDate()
        {

            string str_finyear = FINSP.GetSPFOR_FiscalYear(DateTime.Now.Date.ToString("dd/MM/yyyy"));
            DataTable dtDate = new DataTable();
            if (str_finyear != string.Empty)
            {
                dtDate = DBMethod.ExecuteQuery(FIN.DAL.GL.InventoryReports_DAL.getCalDate(str_finyear)).Tables[0];
                txtFromDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["cal_eff_start_dt"].ToString());
                txtToDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["call_eff_end_dt"].ToString());
            }



            //if (ddl_GB_FINYear.Items.Count > 0)
            //{
            //    // ddl_GB_FINYear.SelectedIndex = ddl_GB_FINYear.Items.Count - 1;
            //    ddl_GB_FINYear.SelectedValue = str_finyear;
            //}
        }
        protected void txtToDate_TextChanged(object sender, EventArgs e)
        {
            FillJournalNumber();
        }

        private void FillJournalNumber()
        {
            FIN.BLL.GL.JournalEntry_BLL.GetjournalNumber(ref ddlFromJournal, txtFromDate.Text, txtToDate.Text);
            FIN.BLL.GL.JournalEntry_BLL.GetjournalNumber(ref ddlToJournal, txtFromDate.Text, txtToDate.Text);
        }
        private void ParamValidation()
        {

            ErrorCollection.Clear();

            ErrorCollection = CommonUtils.ValidateDateRange(txtFromDate.Text, txtToDate.Text);
            if (ErrorCollection.Count > 0)
            {
                return;
            }

            ErrorCollection = CommonUtils.Validate2AmountsExp(txtFromAmount.Text, txtToAmount.Text);
            if (ErrorCollection.Count > 0)
            {
                return;
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;

                string posted = string.Empty;
                ErrorCollection.Remove("globseg");
                ErrorCollection.Remove("fromToDate");

                ParamValidation();
                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                if (ddlSegment.SelectedValue.ToString().Length > 0)
                {
                    if (txtFromDate.Text != string.Empty && txtToDate.Text != string.Empty)
                    {
                        htFilterParameter.Add("From_Date", txtFromDate.Text);

                        htFilterParameter.Add("To_Date", txtToDate.Text);
                    }
                    else
                    {
                        ErrorCollection.Remove("fromToDate");
                        ErrorCollection.Add("fromToDate", "Please select From Date and To Date");
                        return;
                    }
                    string str_acccode = "";
                    /* if (txtFromAccount.Text.ToString().Length > 0)
                     {
                         htFilterParameter.Add("FromAccount", txtFromAccount.Text.ToString());
                     }

                     if (txtToAccount.Text.ToString().Length > 0)
                     {
                         htFilterParameter.Add("ToAccount", txtToAccount.Text.ToString());
                     }*/

                    if (ddlFromAccNumber.SelectedValue != string.Empty)
                    {
                        htFilterParameter.Add("FromAccount", ddlFromAccNumber.SelectedValue);
                    }
                    if (ddlToAccNumber.SelectedValue != string.Empty)
                    {
                        htFilterParameter.Add("ToAccount", ddlToAccNumber.SelectedValue);
                    }


                    if (ddlFromJournal.SelectedValue != string.Empty)
                    {
                        htFilterParameter.Add("FromJournal", ddlFromJournal.SelectedValue.ToString());
                        htFilterParameter.Add("From_Journal", ddlFromJournal.SelectedItem.Text.ToString());
                    }

                    if (ddlToJournal.SelectedValue != string.Empty)
                    {
                        htFilterParameter.Add("ToJournal", ddlToJournal.SelectedValue.ToString());
                        htFilterParameter.Add("To_Journal", ddlToJournal.SelectedItem.Text.ToString());
                    }

                    if (ddlSegment.SelectedValue.ToString().Length > 0)
                    {
                        htFilterParameter.Add(FINColumnConstants.SEGMENT_VALUE_ID, ddlSegment.SelectedValue.ToString());
                    }

                    if (chkUnPost.Checked)
                    {
                        htFilterParameter.Add("UNPOST", "1");
                        posted = "1";
                    }
                    else
                    {
                        htFilterParameter.Add("UNPOST", "0");
                        posted = "0";
                    }
                    if (txtFromAmount.Text != string.Empty)
                    {
                        htFilterParameter.Add("From_Amount", txtFromAmount.Text);
                        htFilterParameter.Add("FromAmount", DBMethod.GetAmtDecimalCommaSeparationValue(txtFromAmount.Text));
                    }
                    if (txtToAmount.Text != string.Empty)
                    {
                        htFilterParameter.Add("To_Amount", txtToAmount.Text);
                        htFilterParameter.Add("ToAmount", DBMethod.GetAmtDecimalCommaSeparationValue(txtToAmount.Text));
                    }

                    VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
                    VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;

                    ReportData = FIN.DAL.GL.JournalEntry_DAL.GetSP_Statement_of_accounts(ddlSegment.SelectedValue.ToString(), DBMethod.ConvertStringToDate(txtFromDate.Text), DBMethod.ConvertStringToDate(txtToDate.Text), str_acccode, posted, FIN.DAL.GL.JournalEntry_DAL.getStatementofAccountforgnCurr());

                    //ReportData = DBMethod.ExecuteQuery(FIN.DAL.GL.JournalEntry_DAL.getJournalLedgerSystem());

                    ReportFormulaParameter = htHeadingParameters;

                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerReportPath + "?Id=" + Master.ProgramID + "','','" + DbConsts.ReportProperties + "');", true);
                }
                else
                {
                    ErrorCollection.Remove("globseg");
                    ErrorCollection.Add("globseg", "Please select Global segment");
                    return;
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("JounalLedgerSystemReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
    }
}