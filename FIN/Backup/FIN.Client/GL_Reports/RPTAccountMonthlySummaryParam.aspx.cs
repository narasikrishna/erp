﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.GL;
using FIN.DAL.AP;
using FIN.DAL.AR;
using FIN.BLL;
using FIN.BLL.AP;
using FIN.BLL.AR;
using FIN.BLL.HR;
using FIN.BLL.GL;
using FIN.BLL.SSM;
using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;

namespace FIN.Client.Reports.GL
{
    public partial class RPTAccountMonthlySummaryParam : PageBase
    {
        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountMonthlySummaryReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                Startup();
                FillStartDate();
                FillComboBox();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountMonthlySummaryReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void Startup()
        {

            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));

            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();

        }
        private void FillStartDate()
        {

            //string str_finyear = FINSP.GetSPFOR_FiscalYear(DateTime.Now.Date.ToString("dd/MM/yyyy"));
            //DataTable dtDate = new DataTable();
            //if (str_finyear != string.Empty)
            //{
            //    dtDate = DBMethod.ExecuteQuery(FIN.DAL.GL.InventoryReports_DAL.getCalDate(str_finyear)).Tables[0];
            //    txtFromDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["cal_eff_start_dt"].ToString());
            //    txtToDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["call_eff_end_dt"].ToString());
            //}



            //if (ddl_GB_FINYear.Items.Count > 0)
            //{
            //    // ddl_GB_FINYear.SelectedIndex = ddl_GB_FINYear.Items.Count - 1;
            //    ddl_GB_FINYear.SelectedValue = str_finyear;
            //}
        }
       
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;

                /*if (txtFromAccount.Text.ToString().Length > 0)
                {
                    htFilterParameter.Add("FromAccount", txtFromAccount.Text.ToString());
                }
                if (txtToAccount.Text.ToString().Length > 0)
                {
                    htFilterParameter.Add("ToAccount", txtToAccount.Text.ToString());
                }*/

               


                if (ddlFromAccNumber.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("FromAccount", ddlFromAccNumber.SelectedValue);
                }
                if (ddlToAccNumber.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("ToAccount", ddlToAccNumber.SelectedValue);
                }

                //if (ddlAccountGroup.SelectedValue != string.Empty)
                //{
                //    htFilterParameter.Add("ACCOUNTGROUP", ddlAccountGroup.SelectedValue);
                //}
                if (ddlGlobalSegment.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("SEGMENT_ID", ddlGlobalSegment.SelectedItem.Value);
                }
                if (ddlAccCode.SelectedValue.ToString().Length > 0)
                {
                    htFilterParameter.Add("AccountCode", ddlAccCode.SelectedItem.Value);
                }
                if (ddlFromAccGroup.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("AccFromGroup", ddlFromAccGroup.SelectedValue.ToString());
                    htFilterParameter.Add("Acc_From_Group", ddlFromAccGroup.SelectedItem.Text);
                }
                if (ddlToAccGroup.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("AccToGroup", ddlToAccGroup.SelectedValue.ToString());
                    htFilterParameter.Add("Acc_To_Group", ddlToAccGroup.SelectedItem.Text);
                }
                if (ddlperiod.SelectedValue.ToString().Length > 0)
                {
                    htFilterParameter.Add(FINColumnConstants.PERIOD_NAME, ddlperiod.SelectedItem.Text.ToString());
                    htFilterParameter.Add(FINColumnConstants.PERIOD_ID, ddlperiod.SelectedValue.ToString());
                }
                if (chkWithZero.Checked)
                {
                    htFilterParameter.Add("WITHZERO", "TRUE");
                }
                else
                {
                    htFilterParameter.Add("WITHZERO", "FALSE");
                }
                //if (txtFromDate.Text != string.Empty)
                //{
                //    htFilterParameter.Add("From_Date", txtFromDate.Text);
                //}
                //if (txtToDate.Text != string.Empty)
                //{
                //    htFilterParameter.Add("To_Date", txtToDate.Text);
                //}
                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;
                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
                //  ReportData = FIN.DAL.GL.AccountMonthlySummary_DAL.GetSP_AccountMonthlySummary(ddlGlobalSegment.SelectedValue.ToString(), DBMethod.ConvertStringToDate(txtFromDate.Text), DBMethod.ConvertStringToDate(txtToDate.Text), "ALL", "0", FIN.BLL.GL.AccountMonthlySummary_BLL.GetReportData());
                ReportData = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountMonthlySummary_DAL.getAccMonthSumparm());


                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());

                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerPath + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountMonthlySummaryReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }
        private void FillComboBox()
        {
            FIN.BLL.GL.Segments_BLL.GetGlobalSegmentvaluesByOrgName(ref ddlGlobalSegment);
            //Segments_BLL.GetGlobalSegmentvaluesBasedOrg(ref ddlGlobalSegment);
            FIN.BLL.GL.AccountCodes_BLL.getAccCodeBasedOrg(ref ddlAccCode, VMVServices.Web.Utils.OrganizationID);
            ddlAccCode.Items.RemoveAt(0);
            ddlAccCode.Items.Insert(0, new ListItem("All", ""));
            //FIN.BLL.GL.AccountCodes_BLL.fn_getGroupnameinaccount(ref ddlAccountGroup, false);
            FIN.BLL.GL.AccountingCalendar_BLL.GetAccPeriodBasedOrg4NotNOPPeriods(ref ddlperiod);
            FIN.BLL.GL.AccountCodes_BLL.fn_getGroupnameinaccount(ref ddlFromAccGroup, false);
            FIN.BLL.GL.AccountCodes_BLL.fn_getGroupnameinaccount(ref ddlToAccGroup, false);

            AccountCodes_BLL.getAccCodeBasedOrgRep(ref ddlFromAccNumber);
            AccountCodes_BLL.getAccCodeBasedOrgRep(ref ddlToAccNumber);
            ddlGlobalSegment.SelectedIndex = 1;

        }





    }
}