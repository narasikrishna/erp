﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.GL;
using FIN.DAL.AP;
using FIN.DAL.AR;
using FIN.BLL;
using FIN.BLL.AP;
using FIN.BLL.AR;
using FIN.BLL.HR;
using FIN.BLL.GL;
using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;

namespace FIN.Client.GL_Reports
{
    public partial class GLPeriodBalanceParam : PageBase
    {
        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();
        bool amtSep = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountGroupSummaryReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                FillComboBox();
                FillStartDate();
                Startup();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountGroupSummaryReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void Startup()
        {

            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));

            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();

        }
        private void FillStartDate()
        {

            string str_finyear = FINSP.GetSPFOR_FiscalYear(DateTime.Now.Date.ToString("dd/MM/yyyy"));
            DataTable dtDate = new DataTable();
            if (str_finyear != string.Empty)
            {
                dtDate = DBMethod.ExecuteQuery(FIN.DAL.GL.InventoryReports_DAL.getCalDate(str_finyear)).Tables[0];
                txtDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["cal_eff_start_dt"].ToString());
                txtToDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["call_eff_end_dt"].ToString());
            }



            //if (ddl_GB_FINYear.Items.Count > 0)
            //{
            //    // ddl_GB_FINYear.SelectedIndex = ddl_GB_FINYear.Items.Count - 1;
            //    ddl_GB_FINYear.SelectedValue = str_finyear;
            //}
        }

        private void FillComboBox()
        {
            FIN.BLL.GL.Segments_BLL.GetGlobalSegmentvaluesByOrgName(ref ddlGlobalSegment);
            //FIN.BLL.GL.Segments_BLL.GetGlobalSegmentvaluesBasedOrg(ref ddlGlobalSegment);
            ddlGlobalSegment.SelectedIndex = 1;

        }

        protected void ddlGroupName_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {


            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton Head_img_btn = (ImageButton)e.Row.FindControl("img_Det");
                if (gvData.DataKeys[e.Row.RowIndex]["B_TYPE"].ToString() != "EXPAND")
                {
                    Head_img_btn.CommandName = "MINUS";
                    Head_img_btn.ImageUrl = "../Images/collapse_blue.png";
                }
                else
                {
                    Head_img_btn.CommandName = "PLUS";
                    Head_img_btn.ImageUrl = "../Images/expand_blue.png";
                }
                if (gvData.DataKeys[e.Row.RowIndex]["GROUP_LEVEL"].ToString() == "0")
                {
                    Head_img_btn.Visible = false;
                }

                Label lbl_tmp = e.Row.FindControl("lblAcct_grp_Name") as Label;
                lbl_tmp.Text = lbl_tmp.Text.Replace(" ", "&nbsp;");
            }


        }

        protected void img_Det_Click(object sender, ImageClickEventArgs e)
        {


            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            string Acct_grp_id = gvData.DataKeys[gvr.RowIndex].Values["ACCT_GRP_ID"].ToString();

            ImageButton Head_img_btn = (ImageButton)gvData.Rows[gvr.RowIndex].FindControl("img_Det");

            DataTable dt = (DataTable)Session["TemplateData"];
            DataTable dt_tmp = (DataTable)Session[FINSessionConstants.GridData];


            int row_index = gvr.RowIndex;
            if (Head_img_btn.CommandName == "PLUS")
            {
                dt_tmp.Rows[row_index][0] = "COLLAPSE";
            }
            else
            {
                dt_tmp.Rows[row_index][0] = "EXPAND";
            }
            if (Head_img_btn.CommandName == "PLUS")
            {
                var var_empList = dt.AsEnumerable().Where(r => r["PARENT_GROUP_ID"].ToString().Contains(Acct_grp_id));

                if (var_empList.Any())
                {
                    DataTable dt_Group_tmp = System.Data.DataTableExtensions.CopyToDataTable(var_empList);
                    for (int kLoop = 0; kLoop < dt_Group_tmp.Rows.Count; kLoop++)
                    {
                        row_index += 1;
                        DataRow dr = dt_tmp.NewRow();

                        for (int rloop = 0; rloop < dt_Group_tmp.Columns.Count; rloop++)
                        {
                            dr[rloop] = dt_Group_tmp.Rows[kLoop][rloop];
                        }

                        // dr = dt_Group_tmp.Rows[kLoop];
                        dt_tmp.Rows.InsertAt(dr, row_index);
                        // dt_tmp.Rows.Add(dr);

                    }


                }

            }
            else
            {
                row_index += 1;


                //for (int kLoop = row_index; kLoop < dt_tmp.Rows.Count; kLoop++)
                //{
                //    if (dt_tmp.Rows[kLoop]["PARENT_GROUP_ID"].ToString() == Acct_grp_id)
                //    {
                //        dt_tmp.Rows.RemoveAt(kLoop);
                //        kLoop--;
                //    }
                //}

                //top level
                var var_Top = dt_tmp.AsEnumerable().Where(r => r["PARENT_GROUP_ID"].ToString().Contains(Acct_grp_id));

                if (var_Top.Any())
                {
                    DataTable dt_Top = System.Data.DataTableExtensions.CopyToDataTable(var_Top);
                    if (dt_Top.Rows.Count > 0)
                    {
                        for (int xLoop = 0; xLoop < dt_Top.Rows.Count; xLoop++)
                        {

                            //1 level
                            var var_empList = dt_tmp.AsEnumerable().Where(r => r["PARENT_GROUP_ID"].ToString().Contains(dt_Top.Rows[xLoop]["ACCT_GRP_ID"].ToString()));

                            if (var_empList.Any())
                            {
                                DataTable dt_Group_tmp = System.Data.DataTableExtensions.CopyToDataTable(var_empList);
                                if (dt_Group_tmp.Rows.Count > 0)
                                {
                                    for (int kLoop = 0; kLoop < dt_Group_tmp.Rows.Count; kLoop++)
                                    {
                                        //2nd level

                                        var var_Child = dt_tmp.AsEnumerable().Where(r => r["PARENT_GROUP_ID"].ToString().Contains(dt_Group_tmp.Rows[kLoop]["ACCT_GRP_ID"].ToString()));

                                        if (var_Child.Any())
                                        {
                                            DataTable dt_Child = System.Data.DataTableExtensions.CopyToDataTable(var_Child);
                                            if (dt_Child.Rows.Count > 0)
                                            {
                                                for (int jLoop = 0; jLoop < dt_Child.Rows.Count; jLoop++)
                                                {

                                                    //3rd level
                                                    var var_SubChild = dt_tmp.AsEnumerable().Where(r => r["PARENT_GROUP_ID"].ToString().Contains(dt_Child.Rows[jLoop]["ACCT_GRP_ID"].ToString()));

                                                    if (var_SubChild.Any())
                                                    {
                                                        DataTable dt_SubChild = System.Data.DataTableExtensions.CopyToDataTable(var_SubChild);
                                                        if (dt_SubChild.Rows.Count > 0)
                                                        {
                                                            for (int MLoop = 0; MLoop < dt_SubChild.Rows.Count; MLoop++)
                                                            {
                                                                //dt_SubChild.Rows.RemoveAt(MLoop);                                                               


                                                                //4level
                                                                var var_SubSubChild = dt_tmp.AsEnumerable().Where(r => r["PARENT_GROUP_ID"].ToString().Contains(dt_SubChild.Rows[MLoop]["ACCT_GRP_ID"].ToString()));

                                                                if (var_SubSubChild.Any())
                                                                {
                                                                    DataTable dt_SubSubChild = System.Data.DataTableExtensions.CopyToDataTable(var_SubSubChild);
                                                                    if (dt_SubSubChild.Rows.Count > 0)
                                                                    {
                                                                        for (int NLoop = 0; NLoop < dt_SubSubChild.Rows.Count; NLoop++)
                                                                        {
                                                                            for (int dtLoop1 = 0; dtLoop1 < dt_tmp.Rows.Count; dtLoop1++)
                                                                            {
                                                                                if (dt_SubSubChild.Rows[NLoop]["ACCT_GRP_ID"].ToString() == dt_tmp.Rows[dtLoop1]["ACCT_GRP_ID"].ToString())
                                                                                {
                                                                                    dt_tmp.Rows.RemoveAt(dtLoop1);
                                                                                    dt_tmp.AcceptChanges();
                                                                                    break;
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }

                                                                // 4 level end
                                                                for (int dtLoop = 0; dtLoop < dt_tmp.Rows.Count; dtLoop++)
                                                                {
                                                                    if (dt_SubChild.Rows[MLoop]["ACCT_GRP_ID"].ToString() == dt_tmp.Rows[dtLoop]["ACCT_GRP_ID"].ToString())
                                                                    {
                                                                        dt_tmp.Rows.RemoveAt(dtLoop);
                                                                        dt_tmp.AcceptChanges();
                                                                        break;
                                                                    }
                                                                }
                                                                //MLoop--;

                                                            }
                                                        }
                                                    }

                                                    for (int dtLoop = 0; dtLoop < dt_tmp.Rows.Count; dtLoop++)
                                                    {
                                                        if (dt_Child.Rows[jLoop]["ACCT_GRP_ID"].ToString() == dt_tmp.Rows[dtLoop]["ACCT_GRP_ID"].ToString())
                                                        {
                                                            dt_tmp.Rows.RemoveAt(dtLoop);
                                                            dt_tmp.AcceptChanges();
                                                            break;
                                                        }
                                                    }
                                                }


                                            }
                                        }

                                        //   2end                                      

                                        for (int dtLoop = 0; dtLoop < dt_tmp.Rows.Count; dtLoop++)
                                        {
                                            if (dt_Group_tmp.Rows[kLoop]["ACCT_GRP_ID"].ToString() == dt_tmp.Rows[dtLoop]["ACCT_GRP_ID"].ToString())
                                            {
                                                dt_tmp.Rows.RemoveAt(dtLoop);
                                                dt_tmp.AcceptChanges();
                                                break;
                                            }
                                        }
                                    }
                                }

                            }

                            for (int dtLoop = 0; dtLoop < dt_tmp.Rows.Count; dtLoop++)
                            {
                                if (dt_Top.Rows[xLoop]["ACCT_GRP_ID"].ToString() == dt_tmp.Rows[dtLoop]["ACCT_GRP_ID"].ToString())
                                {
                                    dt_tmp.Rows.RemoveAt(dtLoop);
                                    dt_tmp.AcceptChanges();
                                    break;
                                }
                            }


                        }
                    }


                }

            }
            amtSep = true;
            BindGrid(dt_tmp);
        }

        private void BindGrid(DataTable dt_data)
        {
            if (dt_data.Rows.Count > 0)
            {
                //ReportData.Tables[0].AsEnumerable().ToList().ForEach(p => p.SetField<String>("OPENING_BALANCE", DBMethod.GetAmtDecimalCommaSeparationValueWithNegative(p.Field<String>("OPENING_BALANCE"))));
                //ReportData.Tables[0].AsEnumerable().ToList().ForEach(p => p.SetField<String>("TRANSACTION_DEBIT", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("TRANSACTION_DEBIT"))));
                //ReportData.Tables[0].AsEnumerable().ToList().ForEach(p => p.SetField<String>("TRANSACTION_CREDIT", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("TRANSACTION_CREDIT"))));
                //ReportData.Tables[0].AsEnumerable().ToList().ForEach(p => p.SetField<String>("CLOSING_BALANCE_DEBIT", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("CLOSING_BALANCE_DEBIT"))));
                //ReportData.Tables[0].AsEnumerable().ToList().ForEach(p => p.SetField<String>("CLOSING_BALANCE_CREDIT", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("CLOSING_BALANCE_CREDIT"))));
                //ReportData.Tables[0].AsEnumerable().ToList().ForEach(p => p.SetField<String>("CLOSING_BALANCE", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("CLOSING_BALANCE"))));
                //ReportData.Tables[0].AcceptChanges();

                //dt_data.AsEnumerable().ToList().ForEach(p => p.SetField<String>("OPENING_BALANCE", DBMethod.GetAmtDecimalCommaSeparationValueWithNegative((p.Field<String>("OPENING_BALANCE")))));
                //dt_data.AsEnumerable().ToList().ForEach(p => p.SetField<String>("TRANSACTION_DEBIT", DBMethod.GetAmtDecimalCommaSeparationValueWithNegative((p.Field<String>("TRANSACTION_DEBIT")))));
                //dt_data.AsEnumerable().ToList().ForEach(p => p.SetField<String>("TRANSACTION_CREDIT", DBMethod.GetAmtDecimalCommaSeparationValueWithNegative((p.Field<String>("TRANSACTION_CREDIT")))));
                //dt_data.AsEnumerable().ToList().ForEach(p => p.SetField<String>("CLOSING_BALANCE_DEBIT", DBMethod.GetAmtDecimalCommaSeparationValueWithNegative((p.Field<String>("CLOSING_BALANCE_DEBIT")))));
                //dt_data.AsEnumerable().ToList().ForEach(p => p.SetField<String>("CLOSING_BALANCE_CREDIT", DBMethod.GetAmtDecimalCommaSeparationValueWithNegative((p.Field<String>("CLOSING_BALANCE_CREDIT")))));
                //dt_data.AsEnumerable().ToList().ForEach(p => p.SetField<String>("CLOSING_BALANCE", DBMethod.GetAmtDecimalCommaSeparationValueWithNegative((p.Field<String>("CLOSING_BALANCE")))));
                //dt_data.AcceptChanges();



                //string str_dec = VMVServices.Web.Utils.DecimalPrecision;
                //string separatedValue = "#,###,###,###,###,###,###,###,###,###,###,###,###";

                //string formattedValue = amtValue.ToString(separatedValue + ".000", System.Globalization.CultureInfo.InvariantCulture);
                //if (amtValue < 0)
                //{
                //    string formattedValue = String.Format("{0:#,##0.000;(#,##0.000);}", amtValue);
                //}
                string negativeAmt = "###,##0.000;(#,##0.000)";// "#,###,###,###,###,###,###,###,###,###,###,###,##0.000;(#,###,###,###,###,###,###,###,###,###,###,###,##0.000)";
                //string negativeAmt = "#,##,###,###,###,###,##0.000;(#,##,###,###,###,###,##0.000)";

                if (!amtSep)
                {
                    //dt_data.AsEnumerable().ToList().ForEach(p => p.SetField<String>("OPENING_BALANCE", DBMethod.GetAmtDecimalCommaSeparationValueWithNegative((p.Field<String>("OPENING_BALANCE")))));
                    //dt_data.AsEnumerable().ToList().ForEach(p => p.SetField<String>("TRANSACTION_DEBIT", DBMethod.GetAmtDecimalCommaSeparationValueWithNegative((p.Field<String>("TRANSACTION_DEBIT")))));
                    //dt_data.AsEnumerable().ToList().ForEach(p => p.SetField<String>("TRANSACTION_CREDIT", DBMethod.GetAmtDecimalCommaSeparationValueWithNegative((p.Field<String>("TRANSACTION_CREDIT")))));
                    //dt_data.AsEnumerable().ToList().ForEach(p => p.SetField<String>("CLOSING_BALANCE_DEBIT", DBMethod.GetAmtDecimalCommaSeparationValueWithNegative((p.Field<String>("CLOSING_BALANCE_DEBIT")))));
                    //dt_data.AsEnumerable().ToList().ForEach(p => p.SetField<String>("CLOSING_BALANCE_CREDIT", DBMethod.GetAmtDecimalCommaSeparationValueWithNegative((p.Field<String>("CLOSING_BALANCE_CREDIT")))));
                    //dt_data.AsEnumerable().ToList().ForEach(p => p.SetField<String>("CLOSING_BALANCE", DBMethod.GetAmtDecimalCommaSeparationValueWithNegative((p.Field<String>("CLOSING_BALANCE")))));
                    //dt_data.AcceptChanges();




                    dt_data.AsEnumerable().ToList().ForEach(p => p.SetField<String>("OPENING_BALANCE", decimal.Parse((p.Field<String>("OPENING_BALANCE"))).ToString(negativeAmt)));
                    dt_data.AsEnumerable().ToList().ForEach(p => p.SetField<String>("TRANSACTION_DEBIT", decimal.Parse(p.Field<String>("TRANSACTION_DEBIT")).ToString(negativeAmt)));
                    dt_data.AsEnumerable().ToList().ForEach(p => p.SetField<String>("TRANSACTION_CREDIT", decimal.Parse(p.Field<String>("TRANSACTION_CREDIT")).ToString(negativeAmt)));
                    dt_data.AsEnumerable().ToList().ForEach(p => p.SetField<String>("CLOSING_BALANCE_DEBIT", decimal.Parse(p.Field<String>("CLOSING_BALANCE_DEBIT")).ToString(negativeAmt)));
                    dt_data.AsEnumerable().ToList().ForEach(p => p.SetField<String>("CLOSING_BALANCE_CREDIT", decimal.Parse(p.Field<String>("CLOSING_BALANCE_CREDIT")).ToString(negativeAmt)));
                    dt_data.AsEnumerable().ToList().ForEach(p => p.SetField<String>("CLOSING_BALANCE", decimal.Parse(p.Field<String>("CLOSING_BALANCE")).ToString(negativeAmt)));
                    dt_data.AcceptChanges();
                }
                else
                {
                    for (int i = 0; i < dt_data.Rows.Count; i++)
                    {
                        if (dt_data.Rows[i]["OPENING_BALANCE"].ToString().IndexOf("(") >= 0)
                        {
                            dt_data.Rows[i]["OPENING_BALANCE"] = dt_data.Rows[i]["OPENING_BALANCE"].ToString().Replace("(", "-");
                            dt_data.AcceptChanges();
                            dt_data.Rows[i]["OPENING_BALANCE"] = dt_data.Rows[i]["OPENING_BALANCE"].ToString().Replace(")", "");
                            dt_data.AcceptChanges();
                        }
                        if (dt_data.Rows[i]["TRANSACTION_DEBIT"].ToString().IndexOf("(") >= 0)
                        {
                            dt_data.Rows[i]["TRANSACTION_DEBIT"] = dt_data.Rows[i]["TRANSACTION_DEBIT"].ToString().Replace("(", "-");
                            dt_data.AcceptChanges();
                            dt_data.Rows[i]["TRANSACTION_DEBIT"] = dt_data.Rows[i]["TRANSACTION_DEBIT"].ToString().Replace(")", "");
                            dt_data.AcceptChanges();
                        }
                        if (dt_data.Rows[i]["TRANSACTION_CREDIT"].ToString().IndexOf("(") >= 0)
                        {
                            dt_data.Rows[i]["TRANSACTION_CREDIT"] = dt_data.Rows[i]["TRANSACTION_CREDIT"].ToString().Replace("(", "-");
                            dt_data.AcceptChanges();
                            dt_data.Rows[i]["TRANSACTION_CREDIT"] = dt_data.Rows[i]["TRANSACTION_CREDIT"].ToString().Replace(")", "");
                            dt_data.AcceptChanges();
                        }
                        if (dt_data.Rows[i]["CLOSING_BALANCE_DEBIT"].ToString().IndexOf("(") >= 0)
                        {
                            dt_data.Rows[i]["CLOSING_BALANCE_DEBIT"] = dt_data.Rows[i]["CLOSING_BALANCE_DEBIT"].ToString().Replace("(", "-");
                            dt_data.AcceptChanges();
                            dt_data.Rows[i]["CLOSING_BALANCE_DEBIT"] = dt_data.Rows[i]["CLOSING_BALANCE_DEBIT"].ToString().Replace(")", "");
                            dt_data.AcceptChanges();
                        }
                        if (dt_data.Rows[i]["CLOSING_BALANCE_CREDIT"].ToString().IndexOf("(") >= 0)
                        {
                            dt_data.Rows[i]["CLOSING_BALANCE_CREDIT"] = dt_data.Rows[i]["CLOSING_BALANCE_CREDIT"].ToString().Replace("(", "-");
                            dt_data.AcceptChanges();
                            dt_data.Rows[i]["CLOSING_BALANCE_CREDIT"] = dt_data.Rows[i]["CLOSING_BALANCE_CREDIT"].ToString().Replace(")", "");
                            dt_data.AcceptChanges();
                        }
                        if (dt_data.Rows[i]["CLOSING_BALANCE"].ToString().IndexOf("(") >= 0)
                        {
                            dt_data.Rows[i]["CLOSING_BALANCE"] = dt_data.Rows[i]["CLOSING_BALANCE"].ToString().Replace("(", "-");
                            dt_data.AcceptChanges();
                            dt_data.Rows[i]["CLOSING_BALANCE"] = dt_data.Rows[i]["CLOSING_BALANCE"].ToString().Replace(")", "");
                            dt_data.AcceptChanges();
                        }
                        dt_data.AcceptChanges();
                    }
                    //dt_data.AsEnumerable().ToList().ForEach(p => p.SetField<String>("OPENING_BALANCE", DBMethod.GetAmtDecimalCommaSeparationValue((p.Field<String>("OPENING_BALANCE")))));
                    //dt_data.AsEnumerable().ToList().ForEach(p => p.SetField<String>("TRANSACTION_DEBIT", DBMethod.GetAmtDecimalCommaSeparationValue((p.Field<String>("TRANSACTION_DEBIT")))));
                    //dt_data.AsEnumerable().ToList().ForEach(p => p.SetField<String>("TRANSACTION_CREDIT", DBMethod.GetAmtDecimalCommaSeparationValue((p.Field<String>("TRANSACTION_CREDIT")))));
                    //dt_data.AsEnumerable().ToList().ForEach(p => p.SetField<String>("CLOSING_BALANCE_DEBIT", DBMethod.GetAmtDecimalCommaSeparationValue((p.Field<String>("CLOSING_BALANCE_DEBIT")))));
                    //dt_data.AsEnumerable().ToList().ForEach(p => p.SetField<String>("CLOSING_BALANCE_CREDIT", DBMethod.GetAmtDecimalCommaSeparationValue((p.Field<String>("CLOSING_BALANCE_CREDIT")))));
                    //dt_data.AsEnumerable().ToList().ForEach(p => p.SetField<String>("CLOSING_BALANCE", DBMethod.GetAmtDecimalCommaSeparationValue((p.Field<String>("CLOSING_BALANCE")))));
                    //dt_data.AcceptChanges();


                    //  string str_dec = VMVServices.Web.Utils.DecimalPrecision;
                    //  string negativeAmt = "#,##,###,###,###,###,##0.##0;(#,##,###,###,###,###,##0.##0)";

                    dt_data.AsEnumerable().ToList().ForEach(p => p.SetField<String>("OPENING_BALANCE", decimal.Parse((p.Field<String>("OPENING_BALANCE"))).ToString(negativeAmt)));
                    dt_data.AsEnumerable().ToList().ForEach(p => p.SetField<String>("TRANSACTION_DEBIT", decimal.Parse(p.Field<String>("TRANSACTION_DEBIT")).ToString(negativeAmt)));
                    dt_data.AsEnumerable().ToList().ForEach(p => p.SetField<String>("TRANSACTION_CREDIT", decimal.Parse(p.Field<String>("TRANSACTION_CREDIT")).ToString(negativeAmt)));
                    dt_data.AsEnumerable().ToList().ForEach(p => p.SetField<String>("CLOSING_BALANCE_DEBIT", decimal.Parse(p.Field<String>("CLOSING_BALANCE_DEBIT")).ToString(negativeAmt)));
                    dt_data.AsEnumerable().ToList().ForEach(p => p.SetField<String>("CLOSING_BALANCE_CREDIT", decimal.Parse(p.Field<String>("CLOSING_BALANCE_CREDIT")).ToString(negativeAmt)));
                    dt_data.AsEnumerable().ToList().ForEach(p => p.SetField<String>("CLOSING_BALANCE", decimal.Parse(p.Field<String>("CLOSING_BALANCE")).ToString(negativeAmt)));
                    dt_data.AcceptChanges();
                }


            }

            gvData.DataSource = dt_data;
            gvData.DataBind();
            Session[FINSessionConstants.GridData] = dt_data;


        }

        private void ParamValidation()
        {

            ErrorCollection.Clear();

            ErrorCollection = CommonUtils.ValidateDateRange(txtDate.Text, txtToDate.Text);
            if (ErrorCollection.Count > 0)
            {
                return;
            }
        }

        protected void btnSave_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;
                btnPrint.Visible = true;
                string str_Unpost = "0";
                if (chkUnPost.Checked)
                {
                    str_Unpost = "1";
                }


                ParamValidation();
                if (ErrorCollection.Count > 0)
                {
                    return;
                }


                ReportData = FIN.DAL.GL.AccountingGroups_DAL.getSP_GL_Period_Balance(ddlGlobalSegment.SelectedValue.ToString(), DBMethod.ConvertStringToDate(txtDate.Text), DBMethod.ConvertStringToDate(txtToDate.Text), str_Unpost, Session[FINSessionConstants.Sel_Lng].ToString());




                Session["TemplateData"] = ReportData.Tables[0];
                var var_empList = ReportData.Tables[0].AsEnumerable().Where(r => r["parent_group_id"].ToString().Length == 0);

                if (var_empList.Any())
                {
                    DataTable dt_tmp = System.Data.DataTableExtensions.CopyToDataTable(var_empList);
                    amtSep = false;
                    BindGrid(dt_tmp);
                }


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountGroupSummaryReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                Hashtable htParameters = new Hashtable();
                Hashtable htHeadingParameters = new Hashtable();
                Hashtable htFilterParameter = new Hashtable();


                ReportFile = Master.ReportName;


                ReportData = new DataSet();

                ReportData.Tables.Add(((DataTable)Session[FINSessionConstants.GridData]).Copy());




                VMVServices.Web.Utils.ReportFilterParameter = htHeadingParameters;


                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('../RPTCrystalReportViewer.aspx?Id?Id=" + Master.Mode + "','','" + VMVServices.Services.Data.DbConsts.ReportProperties + "');", true);



            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ExportClick", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
    }
}