﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="RPTProfitLoss.aspx.cs" Inherits="FIN.Client.GL_Reports.RPTProfitLoss" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 900px" id="div1">
        <div class="divRowContainer">
            <div class="divRowContainer" >
                <div class="lblBox" style="float: left; width: 120px" id="lblDate">
                    From Date
                </div>
                <div class="divtxtBox" style="float: left; width: 150px">
                    <asp:TextBox ID="txtFromDate" runat="server" TabIndex="1" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]]  RequiredField txtBox"></asp:TextBox>
                    <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calExtender1" TargetControlID="txtFromDate"
                        OnClientDateSelectionChanged="checkDate" />
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer" >
                <div class="lblBox" style="float: left; width: 120px" id="Div2">
                    To Date
                </div>
                <div class="divtxtBox" style="float: left; width: 150px">
                    <asp:TextBox ID="txtToDate" runat="server" TabIndex="2" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]]  RequiredField txtBox"></asp:TextBox>
                    <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtToDate"
                        OnClientDateSelectionChanged="checkDate" />
                </div>
            </div>
            <%--<div class="divRowContainer">
                <div class="lblBox" style="float: left; width: 120px" id="lblPeriod">
                    Period
                </div>
                <div class="divtxtBox" style="float: left; width: 150px">
                    <asp:DropDownList ID="ddlperiod" runat="server" CssClass="ddlStype validate[required] RequiredField "
                        TabIndex="3">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divFormcontainer">
                <div class="lblBox" style="float: left; width: 120px" id="Div3">
                    Un Posted
                </div>
                <div class="lblBox" style="float: left; width: 150px" id="Div4">
                    <asp:CheckBox ID="chkUnPosted" runat="server" Checked="false" Text=" " />
                </div>
            </div>--%>
            <div class="divFormcontainer" style="width: 290px" id="divMainContainer">
                <div class="divRowContainer divReportAction">
                    <div>
                        <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../Images/show-report-icon.png"
                            Width="35px" Height="25px" OnClick="btnSave_Click" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
