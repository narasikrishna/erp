﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.GL;
using FIN.DAL.AP;
using FIN.DAL.AR;
using FIN.BLL;
using FIN.BLL.AP;
using FIN.BLL.AR;
using FIN.BLL.HR;
using FIN.BLL.GL;
using FIN.BLL.SSM;
using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;

namespace FIN.Client.GL_Reports
{
    public partial class RPTTrialBalanceYearlyDetailsParam : PageBase
    {
        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountMonthlySummaryReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                Startup();
                //FillStartDate();
                FillComboBox();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountMonthlySummaryReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void Startup()
        {

            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));

            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();

        }
        private void FillStartDate()
        {

            string str_finyear = FINSP.GetSPFOR_FiscalYear(DateTime.Now.Date.ToString("dd/MM/yyyy"));
            DataTable dtDate = new DataTable();
            if (str_finyear != string.Empty)
            {
                dtDate = DBMethod.ExecuteQuery(FIN.DAL.GL.InventoryReports_DAL.getCalDate(str_finyear)).Tables[0];
                txtFromDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["cal_eff_start_dt"].ToString());
                txtToDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["call_eff_end_dt"].ToString());
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;

                /* if (txtFromAccount.Text.ToString().Length > 0)
                 {
                     htFilterParameter.Add("FromAccount", txtFromAccount.Text.ToString());
                 }
                 if (txtToAccount.Text.ToString().Length > 0)
                 {
                     htFilterParameter.Add("ToAccount", txtToAccount.Text.ToString());
                 }*/
                if (ddlFromAccNumber.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("FromAccount", ddlFromAccNumber.SelectedValue);
                }
                if (ddlToAccNumber.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("ToAccount", ddlToAccNumber.SelectedValue);
                }

                if (ddlGlobalSegment.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("SEGMENT_NAME", ddlGlobalSegment.SelectedItem.Text);
                    htFilterParameter.Add("SEGMENT_ID", ddlGlobalSegment.SelectedItem.Value);
                }

                if (ddlAccCode.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("ACCOUNT_CODE", ddlAccCode.SelectedItem.Value);
                }

                if (txtFromDate.Text != string.Empty)
                {
                    htFilterParameter.Add("From_Date", txtFromDate.Text);
                }
                //if (txtToDate.Text != string.Empty)
                //{
                //    htFilterParameter.Add("To_Date", txtToDate.Text);
                //}


                string str_acctcode = "";
                string str_unpost = "1";
                string withZero = "1";
                if (chkUnPosted.Checked)
                {
                    str_unpost = "0";
                }
                if (ddlAccCode.SelectedValue.ToString().Trim().Length == 0)
                {
                    str_acctcode = "ALL";
                }
                else
                {
                    str_acctcode = ddlAccCode.SelectedValue;
                }
                if (chkWithZero.Checked)
                {
                    withZero = "0";
                    htFilterParameter.Add("WITHZERO", "TRUE");
                }
                else
                {
                    withZero = "1";
                    htFilterParameter.Add("WITHZERO", "FALSE");
                }

                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;


                DataTable dtGetTBYearlyDetails = new DataTable();
                //As per Mey/Natesh/NSN Discussion, no need to send FromDate & ToDate. Send only 'As on Date' ie., txtFromDate..
                dtGetTBYearlyDetails = FIN.DAL.GL.AccountMonthlySummary_DAL.getGLTrialBalanceYearlyDetails(ddlGlobalSegment.SelectedValue.ToString(), "01/01/" + DBMethod.ConvertStringToDate(txtFromDate.Text).Year.ToString(), txtFromDate.Text.ToString(), txtFromAccount.Text.ToString(), txtToAccount.Text.ToString(), CommonUtils.ConvertStringToDecimal(withZero.ToString()), str_unpost);

                DataSet ds4 = new DataSet();
                ds4.Tables.Add(dtGetTBYearlyDetails);
                ds4.AcceptChanges();

                ReportData = ds4;



                //ReportData = DBMethod.ExecuteQuery(FIN.DAL.GL.TrailBalance_DAL.getTrialBalanceYearlyDtls(ddlGlobalSegment.SelectedValue.ToString(), txtFromDate.Text.ToString(), txtFromDate.Text.ToString(),str_unpost));


                #region datatableUsage
                //DataTable dtgetfsyr = new DataTable();

                //DataSet ds1 = FIN.DAL.GL.AccountMonthlySummary_DAL.GetSP_AccountMonthlySummary(ddlGlobalSegment.SelectedValue.ToString(), DBMethod.ConvertStringToDate(txtFromDate.Text), DBMethod.ConvertStringToDate(txtToDate.Text), str_acctcode, str_unpost, FIN.BLL.GL.AccountMonthlySummary_BLL.GetReportData());
                //DataSet ds2 = FIN.DAL.GL.AccountMonthlySummary_DAL.GetSP_AccountMonthlySummary(ddlGlobalSegment.SelectedValue.ToString(), DBMethod.ConvertStringToDate(txtFromDate.Text).AddYears(-1), DBMethod.ConvertStringToDate(txtToDate.Text).AddYears(-1), str_acctcode, str_unpost, FIN.BLL.GL.AccountMonthlySummary_BLL.GetReportData());

                //dtgetfsyr = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountingCalendar_DAL.GetCallYear(DBMethod.ConvertDateToString(DBMethod.ConvertStringToDate(txtFromDate.Text).AddYears(-1).ToShortDateString()))).Tables[0];
                //DataSet ds3 = FIN.DAL.GL.AccountMonthlySummary_DAL.GetSP_AccountMonthlySummary(ddlGlobalSegment.SelectedValue.ToString(), DateTime.Parse(dtgetfsyr.Rows[0]["CAL_EFF_START_DT"].ToString()), DateTime.Parse(dtgetfsyr.Rows[0]["CALL_EFF_END_DT"].ToString()), str_acctcode, str_unpost, FIN.BLL.GL.AccountMonthlySummary_BLL.GetReportData());


                //DataTable dtData1 = new DataTable("TrialBalanceTable");
                //DataColumn dc1 = new DataColumn("ACCT_CODE");
                //DataColumn dc2 = new DataColumn("CURRENT_BAL");
                //DataColumn dc3 = new DataColumn("PRE_BAL");
                //DataColumn dc4 = new DataColumn("PRV_FIN_BAL");

                //dtData1.Columns.Add(dc1);
                //dtData1.Columns.Add(dc2);
                //dtData1.Columns.Add(dc3);
                //dtData1.Columns.Add(dc4);

                //for (int iLoop = 0; iLoop < ds1.Tables[0].Rows.Count; iLoop++)
                //{
                //    DataRow row = dtData1.NewRow();

                //    row["ACCT_CODE"] = ds1.Tables[0].Rows[iLoop]["ACCOUNT_CODE"].ToString();
                //    row["CURRENT_BAL"] = double.Parse(ds1.Tables[0].Rows[iLoop]["CLOSING_BALANCE"].ToString());
                //    var ds2AcctCodeDtls = ds2.Tables[0].Rows.Cast<DataRow>().Where(x => x["ACCOUNT_CODE"] == row["ACCT_CODE"]).ToList();
                //    if (ds2AcctCodeDtls.ToString().Length > 0)
                //    {
                //        row["PRE_BAL"] = double.Parse(ds2.Tables[0].Rows[0]["CLOSING_BALANCE"].ToString());
                //    }
                //    var ds3AcctCodeDtls = ds3.Tables[0].Rows.Cast<DataRow>().Where(x => x["ACCOUNT_CODE"] == row["ACCT_CODE"]).ToList();
                //    if (ds3AcctCodeDtls.ToString().Length > 0)
                //    {
                //        row["PRV_FIN_BAL"] = double.Parse(ds3.Tables[0].Rows[0]["CLOSING_BALANCE"].ToString());
                //    }

                //    dtData1.Rows.Add(row);
                //}

                //dtData1.AcceptChanges();

                //DataSet ds4 = new DataSet();
                //ds4.Tables.Add(dtData1);
                ////ds4.AcceptChanges();

                //ReportData = ds4;
                //ReportData.Tables[0].TableName = "VW_TB_YEARLY_DETAILS";

                #endregion

                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());

                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerPath + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountMonthlySummaryReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }
        private void FillComboBox()
        {
            txtFromDate.Text = DateTime.Now.ToShortDateString();
            // Segments_BLL.GetGlobalSegmentvaluesBasedOrg(ref ddlGlobalSegment);
            FIN.BLL.GL.Segments_BLL.GetGlobalSegmentvaluesByOrgName(ref ddlGlobalSegment);
            FIN.BLL.GL.AccountCodes_BLL.getAccCodeBasedOrg(ref ddlAccCode, VMVServices.Web.Utils.OrganizationID, "", false);
            AccountCodes_BLL.getAccCodeBasedOrgRep(ref ddlFromAccNumber);
            AccountCodes_BLL.getAccCodeBasedOrgRep(ref ddlToAccNumber);

            ddlGlobalSegment.SelectedIndex = 1;
            //ddlAccCode.Items.RemoveAt(0);
            //ddlAccCode.Items.Insert(0, new ListItem("All", "All"));
        }


    }
}