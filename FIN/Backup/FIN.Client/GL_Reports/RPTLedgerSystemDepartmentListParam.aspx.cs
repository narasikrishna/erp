﻿using System;
using System.Linq;
using System.Web;
using FIN.DAL;
using FIN.BLL;
using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;
using System.IO;
using System.Web.UI;
using System.Collections;
using FIN.DAL.GL;
using System.Web.UI.WebControls;

namespace FIN.Client.GL_Reports
{
    public partial class RPTLedgerSystemDepartmentListParam : PageBase
    {
        public int doc_tablecount;
        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("LedgerSystemDepartmentList", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                Startup();
                FillComboBox();
                FillStartDate();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("LedgerSystemDepartmentListATC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void Startup()
        {
            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));
        }
        protected void FillComboBox()
        {
            Lookup_BLL.GetLookUpValue(ref ddlDepartment, "DEPT_TYP");
            FIN.BLL.SSM.User_BLL.GetCreatedby(ref ddlCreatedBy);
            FIN.BLL.SSM.User_BLL.GetCreatedby(ref ddlModifiedBy);
           
        }
        private void FillStartDate()
        {

            string str_finyear = FINSP.GetSPFOR_FiscalYear(DateTime.Now.Date.ToString("dd/MM/yyyy"));
            DataTable dtDate = new DataTable();
            if (str_finyear != string.Empty)
            {
                dtDate = DBMethod.ExecuteQuery(FIN.DAL.GL.InventoryReports_DAL.getCalDate(str_finyear)).Tables[0];
                txtFromDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["cal_eff_start_dt"].ToString());
                txtToDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["call_eff_end_dt"].ToString());
            }



            //if (ddl_GB_FINYear.Items.Count > 0)
            //{
            //    // ddl_GB_FINYear.SelectedIndex = ddl_GB_FINYear.Items.Count - 1;
            //    ddl_GB_FINYear.SelectedValue = str_finyear;
            //}
        }
        private void ParamValidation()
        {

            ErrorCollection.Clear();


            ErrorCollection = CommonUtils.ValidateDateRange(txtCreatedFromDate.Text, txtCreatedToDate.Text);
            if (ErrorCollection.Count > 0)
            {
                return;
            }
            ErrorCollection = CommonUtils.ValidateDateRange(txtModifiedFromDate.Text, txtModToDate.Text);
            if (ErrorCollection.Count > 0)
            {
                return;
            }

        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;
                ParamValidation();
                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                if (txtFromDate.Text != string.Empty)
                {
                    htFilterParameter.Add(FINColumnConstants.EFFECTIVE_FROM_DT, txtFromDate.Text);
                }
                if (txtToDate.Text != string.Empty)
                {
                    htFilterParameter.Add(FINColumnConstants.EFFECTIVE_TO_DT, txtToDate.Text);
                }
                if (ddlDepartment.SelectedValue.ToString().Length > 0)
                {
                    htFilterParameter.Add("LOOKUP_NAME", ddlDepartment.SelectedItem.Text.ToString());
                }
                if (ddlCreatedBy.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("CREATED_BY", ddlCreatedBy.SelectedValue);
                }
                if (ddlModifiedBy.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("MODIFIED_BY", ddlModifiedBy.SelectedValue);
                }
                if (txtCreatedFromDate.Text != string.Empty)
                {
                    htFilterParameter.Add("CREATED_From_Date", txtCreatedFromDate.Text);
                }
                if (txtCreatedToDate.Text != string.Empty)
                {
                    htFilterParameter.Add("CREATED_To_Date", txtCreatedToDate.Text);
                }
                if (txtModifiedFromDate.Text != string.Empty)
                {
                    htFilterParameter.Add("MODIFIED_From_Date", txtModifiedFromDate.Text);
                }
                if (txtModToDate.Text != string.Empty)
                {
                    htFilterParameter.Add("MODIFIED_To_Date", txtModToDate.Text);
                }

                //if (ddlSegment.SelectedValue.ToString().Length > 0)
                //{
                //    htFilterParameter.Add(FINColumnConstants.SEGMENT_VALUE_ID, ddlSegment.SelectedValue.ToString());
                //}

                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;

                ReportData = DBMethod.ExecuteQuery(FIN.DAL.HR.Department_DAL.getDeptList());

                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerReportPath + "?Id=" + Master.ProgramID + "','','" + DbConsts.ReportProperties + "');", true);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("LedgerSystemDepartmentListReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
    }
}