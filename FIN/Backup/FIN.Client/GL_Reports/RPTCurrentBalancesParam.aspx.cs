﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.GL;
using FIN.DAL.AP;
using FIN.DAL.AR;
using FIN.BLL;
using FIN.BLL.AP;
using FIN.BLL.AR;
using FIN.BLL.HR;
using FIN.BLL.GL;
using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;

namespace FIN.Client.Reports.GL
{
    public partial class RPTCurrentBalancesParam : PageBase
    {
        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CurrentBalanceReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                FillComboBox();

                Startup();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CurrentBalanceReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void Startup()
        {

            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));

            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();

        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;


                if (ddlGlobalSegment.SelectedValue != string.Empty)
                {

                    htFilterParameter.Add("SEGMENT_VALUE", ddlGlobalSegment.SelectedValue);
                    htFilterParameter.Add("SEGMENT_NAME", ddlGlobalSegment.SelectedItem.Text);
                }
                if (ddlPeriodName.SelectedValue != string.Empty)
                {

                    htFilterParameter.Add("PERIOD_NUMBER", ddlPeriodName.SelectedValue);
                    htFilterParameter.Add("PERIOD_NAME", ddlPeriodName.SelectedItem.Text);
                }
                /* if (txtFromAccount.Text.ToString().Length > 0)
                 {
                     htFilterParameter.Add("FromAccount", txtFromAccount.Text.ToString());
                 }

                 if (txtToAccount.Text.ToString().Length > 0)
                 {
                     htFilterParameter.Add("ToAccount", txtToAccount.Text.ToString());
                 }*/
                ParamValidation();
                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                if (ddlFromAccNumber.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("FromAccount", ddlFromAccNumber.SelectedValue);
                    htFilterParameter.Add("FromAccountName", ddlFromAccNumber.SelectedItem.Text);
                }
                if (ddlToAccNumber.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("ToAccount", ddlToAccNumber.SelectedValue);
                    htFilterParameter.Add("ToAccountName", ddlToAccNumber.SelectedItem.Text);
                }

                if (txtFromDebitAmt.Text != string.Empty)
                {
                    htFilterParameter.Add("From_DebitAmt", txtFromDebitAmt.Text);
                    htFilterParameter.Add("FromDebitAmt", DBMethod.GetAmtDecimalCommaSeparationValue(txtFromDebitAmt.Text));
                }
                if (txtDebitToAmt.Text != string.Empty)
                {
                    htFilterParameter.Add("To_DebitAmt", txtDebitToAmt.Text);
                    htFilterParameter.Add("ToDebitAmt", DBMethod.GetAmtDecimalCommaSeparationValue(txtDebitToAmt.Text));
                }
                if (txtFromCreditAmt.Text != string.Empty)
                {
                    htFilterParameter.Add("From_CreditAmt", txtFromCreditAmt.Text);
                    htFilterParameter.Add("FromCreditAmt", DBMethod.GetAmtDecimalCommaSeparationValue(txtFromCreditAmt.Text));
                }
                if (txtToCreditAmt.Text != string.Empty)
                {
                    htFilterParameter.Add("To_CreditAmt", txtToCreditAmt.Text);
                    htFilterParameter.Add("ToCreditAmt", DBMethod.GetAmtDecimalCommaSeparationValue(txtToCreditAmt.Text));

                }

                if (chkWithZero.Checked)
                {
                    htFilterParameter.Add("WITHZERO", "TRUE");
                }
                else
                {
                    htFilterParameter.Add("WITHZERO", "FALSE");
                }
                //if (txtDate.Text != string.Empty)
                //{

                //    htFilterParameter.Add("Balance_Date", txtDate.Text);
                //}
                //FIN.DAL.GL.TrailBalance_DAL.GetSP_TrailBalanceGroup(ddlGlobalSegment.SelectedValue.ToString(), DBMethod.ConvertStringToDate(txtDate.Text));


                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;


                ReportData = FIN.BLL.GL.CurrentBalances_BLL.GetReportData();
                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());
                htHeadingParameters.Add("OrgName", VMVServices.Web.Utils.OrganizationName);



                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerPath + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CurrentBalanceReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }
        private void FillComboBox()
        {
            FIN.BLL.GL.Segments_BLL.GetGlobalSegmentvaluesByOrgName(ref ddlGlobalSegment);
            //FIN.BLL.GL.CurrentBalances_BLL.GetGroupName(ref ddlGlobalSegment);
            ddlGlobalSegment.Items.RemoveAt(0);
            FIN.BLL.GL.AccountingCalendar_BLL.GetAccPeriodBasedOrg4NotNOPPeriods(ref ddlPeriodName);
            ddlPeriodName.Items.RemoveAt(0);


            AccountCodes_BLL.getAccCodeBasedOrgRep(ref ddlFromAccNumber);
            AccountCodes_BLL.getAccCodeBasedOrgRep(ref ddlToAccNumber);

        }
        private void ParamValidation()
        {
            ErrorCollection.Clear();

            ErrorCollection = CommonUtils.Validate2AmountsExp(txtFromDebitAmt.Text, txtDebitToAmt.Text);
            if (ErrorCollection.Count > 0)
            {
                return;
            }

            ErrorCollection = CommonUtils.Validate2AmountsExp(txtFromCreditAmt.Text, txtToCreditAmt.Text);

            if (ErrorCollection.Count > 0)
            {
                return;
            }
        }


    }
}