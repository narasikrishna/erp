﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="RPTAccountMonthlySummaryParam.aspx.cs" Inherits="FIN.Client.Reports.GL.RPTAccountMonthlySummaryParam" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 650px" id="divMainContainer">
        <div class="divRowContainer" style="display:none;">
            <div class="lblBox LNOrient" style="  width: 150px;display:none;" id="lblGlobalSegment">
                Global Segment
            </div>
            <div class="divtxtBox LNOrient" style="  width: 470px">
                <asp:DropDownList ID="ddlGlobalSegment" runat="server" TabIndex="1" CssClass="ddlStype validate[required] RequiredField">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 150px" id="lblPeriod">
                Period
            </div>
            <div class="divtxtBox LNOrient" style="  width: 470px">
                <asp:DropDownList ID="ddlperiod" runat="server" CssClass="ddlStype" TabIndex="2">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 150px" id="Div1">
                From Account Group
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:DropDownList ID="ddlFromAccGroup" runat="server" TabIndex="3" CssClass=" ddlStype">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="  width: 150px" id="Div5">
                To Account Group
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:DropDownList ID="ddlToAccGroup" runat="server" TabIndex="4" CssClass=" ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" style="display: none">
            <div class="lblBox LNOrient" style="  width: 150px" id="Div3">
                Account Group
            </div>
            <div class="divtxtBox LNOrient" style="  width: 450px">
                <asp:DropDownList ID="ddlAccountGroup" runat="server" TabIndex="5" CssClass=" ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" runat="server" visible="false">
            <div class="lblBox LNOrient" style="  width: 150px">
                From Account
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:TextBox ID="txtFromAccount" runat="server" TabIndex="6" CssClass="txtBox"></asp:TextBox>
            </div>
              <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="  width: 150px">
                To Account
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:TextBox ID="txtToAccount" runat="server" TabIndex="7" CssClass="txtBox"></asp:TextBox>
            </div>
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 150px">
                From Account
            </div>
            <div class="divtxtBox LNOrient" style="  width: 470px">
                <asp:DropDownList ID="ddlFromAccNumber" runat="server" CssClass="ddlStype"
                    TabIndex="6">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 150px">
                To Account
            </div>
            <div class="divtxtBox LNOrient" style="  width: 470px">
                <asp:DropDownList ID="ddlToAccNumber" runat="server" CssClass=" ddlStype"
                    TabIndex="7">
                </asp:DropDownList>
            </div>
        </div>
    <div class="divClear_10">
    </div>
    <div class="divRowContainer">
        <div class="lblBox LNOrient" style="  width: 150px" id="Div6">
            Include Zeros
        </div>
        <div class="divtxtBox LNOrient" style="  width: 150px">
            <asp:CheckBox ID="chkWithZero" runat="server" Text="" Checked="false" TabIndex="8" />
        </div>
    </div>
    <div class="divRowContainer" style="display: none">
        <div class="lblBox LNOrient" style="  width: 150px" id="Div2">
            Account Description
        </div>
        <div class="divtxtBox LNOrient" style="  width: 400px">
            <asp:DropDownList ID="ddlAccCode" runat="server" CssClass="ddlStype RequiredField"
                TabIndex="8" AutoPostBack="True">
            </asp:DropDownList>
        </div>
    </div>
    <%-- <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 150px" id="lblFromDate">
                From Date
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:TextBox ID="txtFromDate" runat="server" TabIndex="6" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]]  RequiredField txtBox"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calExtender1" TargetControlID="txtFromDate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
            <div class="colspace" style=" ">
                &nbsp;</div>
            <div class="lblBox LNOrient" style="  width: 130px" id="lblToDate">
                To Date
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:TextBox ID="txtToDate" runat="server" TabIndex="7" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]]  RequiredField txtBox"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtToDate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
        </div>--%>
    <div class="divClear_10">
    </div>
    <div class="divRowContainer divReportAction">
        <table class="ReportTable">
            <tr>
                <td>
                    <asp:ImageButton ID="btnSave" runat="server" ImageUrl="../Images/show-report-icon.png"
                        TabIndex="8" Width="35px" Height="25px" OnClick="btnSave_Click" />
                </td>
                <td>
                    <%--<asp:Button ID="btnPrint" runat="server" Text="Print" CssClass="btn" />--%>
                </td>
            </tr>
        </table>
    </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/GL/GLChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();


                return $("#form1").validationEngine('validate')
            })


        }

    </script>
</asp:Content>
