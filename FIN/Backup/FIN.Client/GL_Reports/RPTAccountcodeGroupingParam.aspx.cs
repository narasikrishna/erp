﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.GL;
using FIN.DAL.AP;
using FIN.DAL.AR;
using FIN.BLL;
using FIN.BLL.AP;
using FIN.BLL.AR;
using FIN.BLL.HR;
using FIN.BLL.GL;
using FIN.BLL.SSM;
using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;

namespace FIN.Client.Reports.GL
{
    public partial class RPTAccountcodeGroupingParam : PageBase
    {
        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();

        DataTable dtGridData = new DataTable();
        DataTable dtGS = new DataTable();
        string ProReturn = null;
        Boolean bol_rowVisiable;
        Boolean savedBool;
        DataSet dsAccCodeLevel = new DataSet();
        Boolean isAccCode;

        int i, modulecount = 0, applicationCount = 0, moduleappcount = 0, functionCount = 0, submenuCount = 0, subsubmenuCount = 0, accCodeCount = 0;
        string IsCodeORGroup = string.Empty;
        DataSet dsModuleList = new DataSet();
        DataSet dsLevel2 = new DataSet();
        DataSet dsLevel3 = new DataSet();
        DataSet dsLevel4 = new DataSet();
        DataSet dsLevel5 = new DataSet();
        DataSet dsAccCode = new DataSet();

        string previousModule = string.Empty, previousMainApplication = string.Empty, previousFunction = string.Empty, previousSubMenu = string.Empty;
        string previousSubSubMenu = string.Empty;
        string previousAccCodeMenu = string.Empty;
        TreeNode moduleNode = null;
        TreeNode applicationNode = null;
        TreeNode functionNode = null;
        TreeNode subMenuNode = null;
        TreeNode subsubMenuNode = null;
        TreeNode accCodeNodeNode = null;


        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                int selectedNode = 0, moduleNode = 0, appNode = 0, functionId = 0;

                string selectAppNode = string.Empty, moduleId = string.Empty, functionAppNode = string.Empty;
                if (tvModules != null)
                {
                    if (tvModules.Nodes.Count > 0)
                    {
                        if (Request.QueryString["Module"] != null)
                        {
                            selectedNode = int.Parse(Request.QueryString["Module"]);
                            if (tvModules.Nodes[selectedNode] != null)
                            {
                                tvModules.CollapseAll();
                                Session["ModuleNode"] = selectedNode;
                                Session["ApplicationNode"] = null;
                                Session["FunctionNode"] = null;
                            }
                        }
                        if (Request.QueryString["AppNode"] != null)
                        {
                            selectAppNode = Request.QueryString["AppNode"].ToString();
                            if (selectAppNode != string.Empty)
                            {
                                if (selectAppNode.Length >= 2)
                                {
                                    moduleNode = int.Parse(selectAppNode.Substring(0, 2));
                                    moduleId = selectAppNode.ToString().Substring(0, 2);
                                }
                                appNode = int.Parse(selectAppNode.Replace(moduleId, string.Empty));
                                if (tvModules.Nodes[moduleNode] != null)
                                {
                                    tvModules.CollapseAll();
                                    Session["ModuleNode"] = moduleNode;
                                    Session["ApplicationNode"] = appNode;
                                    Session["FunctionNode"] = null;
                                }
                            }
                        }
                        if (Request.QueryString["SubMenu"] != null)
                        {
                            functionAppNode = Request.QueryString["SubMenu"].ToString();
                            if (functionAppNode != string.Empty)
                            {
                                moduleNode = int.Parse(functionAppNode.Substring(0, 2));
                                appNode = int.Parse(functionAppNode.Substring(2, 2)) - 1;
                                functionId = int.Parse(functionAppNode.Substring(4, 2));
                                if (tvModules.Nodes[moduleNode] != null)
                                {
                                    tvModules.CollapseAll();
                                    Session["ModuleNode"] = moduleNode;
                                    Session["ApplicationNode"] = appNode;
                                    Session["FunctionNode"] = functionId;
                                }
                            }
                        }
                    }
                    if (Session["ModuleNode"] != null)
                    {
                        tvModules.Nodes[int.Parse(Session["ModuleNode"].ToString())].Expand();
                        tvModules.Nodes[int.Parse(Session["ModuleNode"].ToString())].Selected = true;
                        if (Session["ApplicationNode"] != null)
                        {
                            if (tvModules.Nodes[int.Parse(Session["ModuleNode"].ToString())].ChildNodes.Count >= int.Parse(Session["ApplicationNode"].ToString()))
                            {
                                if (tvModules.Nodes[int.Parse(Session["ModuleNode"].ToString())].ChildNodes[int.Parse(Session["ApplicationNode"].ToString())] != null)
                                {
                                    tvModules.Nodes[int.Parse(Session["ModuleNode"].ToString())].ChildNodes[int.Parse(Session["ApplicationNode"].ToString())].Expand();
                                    tvModules.Nodes[int.Parse(Session["ModuleNode"].ToString())].ChildNodes[int.Parse(Session["ApplicationNode"].ToString())].Selected = true;

                                    if (Session["FunctionNode"] != null)
                                    {
                                        if (tvModules.Nodes[int.Parse(Session["ModuleNode"].ToString())] != null)
                                        {
                                            if (tvModules.Nodes[int.Parse(Session["ModuleNode"].ToString())].ChildNodes[int.Parse(Session["ApplicationNode"].ToString())] != null)
                                            {
                                                if (tvModules.Nodes[int.Parse(Session["ModuleNode"].ToString())].ChildNodes[int.Parse(Session["ApplicationNode"].ToString())].ChildNodes[int.Parse(Session["FunctionNode"].ToString())] != null)
                                                {
                                                    tvModules.Nodes[int.Parse(Session["ModuleNode"].ToString())].ChildNodes[int.Parse(Session["ApplicationNode"].ToString())].ChildNodes[int.Parse(Session["FunctionNode"].ToString())].Expand();
                                                    tvModules.Nodes[int.Parse(Session["ModuleNode"].ToString())].ChildNodes[int.Parse(Session["ApplicationNode"].ToString())].ChildNodes[int.Parse(Session["FunctionNode"].ToString())].Selected = true;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                    }
                }
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountcodeGroupingReport ", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                Startup();
                BindTreeview();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountcodeGroupingReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void BindTreeview()
        {
            try
            {
                ErrorCollection.Clear();

                AccountingGroups_DAL.GetSP_AccountGroupLinksCodeName();
                dsModuleList = DBMethod.ExecuteQuery(AccountingGroups_DAL.getLevel1());

                if (dsModuleList.Tables[0].Rows.Count > 0)
                {
                    for (i = 0; i <= dsModuleList.Tables[0].Rows.Count - 1; i++)
                    {
                        if (dsModuleList.Tables[0].Rows[i]["level1"].ToString() != string.Empty)
                        {
                            if (dsModuleList.Tables[0].Rows[i]["level1"].ToString() != previousModule.ToString())
                            {
                                if (previousModule != string.Empty)
                                {
                                    tvModules.Nodes.Add(moduleNode);
                                }
                                moduleNode = new TreeNode();
                                moduleNode.Text = dsModuleList.Tables[0].Rows[i]["level1"].ToString();
                                moduleNode.Value = dsModuleList.Tables[0].Rows[i]["level1_Id"].ToString();
                                moduleNode.PopulateOnDemand = true;
                                modulecount += 1;
                                applicationCount = 0;
                                functionCount = 0;
                                submenuCount = 0;
                                subsubmenuCount = 0;
                                moduleNode.SelectAction = TreeNodeSelectAction.SelectExpand;
                                previousModule = (dsModuleList.Tables[0].Rows[i]["level1"].ToString());
                            }
                        }

                        // level 2
                        dsLevel2 = DBMethod.ExecuteQuery(AccountingGroups_DAL.getLevel1Child(dsModuleList.Tables[0].Rows[i]["level1_Id"].ToString()));//.Replace(dsModuleList.Tables[0].Rows[i]["level1_Id"].ToString().Substring(0, dsModuleList.Tables[0].Rows[i]["level1_Id"].ToString().IndexOf("ACC")), "")));
                        if (dsLevel2.Tables[0].Rows.Count > 0)
                        {
                            for (int i2 = 0; i2 <= dsLevel2.Tables[0].Rows.Count - 1; i2++)
                            {
                                if (dsLevel2.Tables[0].Rows[i2]["level2_Id"].ToString() != string.Empty || dsLevel2.Tables[0].Rows[i2]["level2"].ToString() == "-")
                                {
                                    if (dsLevel2.Tables[0].Rows[i2]["level2_Id"].ToString() != previousMainApplication.ToString())
                                    {
                                        applicationNode = new TreeNode();
                                        if (dsLevel2.Tables[0].Rows[i2]["level2"].ToString() == "-")
                                        {
                                            dsLevel3 = new DataSet();
                                        }
                                        else
                                        {
                                            applicationNode.Text = dsLevel2.Tables[0].Rows[i2]["level2"].ToString();
                                            applicationNode.Value = dsLevel2.Tables[0].Rows[i2]["level2_Id"].ToString();
                                            dsLevel3 = DBMethod.ExecuteQuery(AccountingGroups_DAL.getLevel2Child(dsLevel2.Tables[0].Rows[i2]["level2_Id"].ToString()));
                                        }
                                        applicationNode.PopulateOnDemand = true;

                                        moduleappcount = modulecount - 1;
                                        functionCount = 0;

                                        applicationCount += 1;
                                        applicationNode.SelectAction = TreeNodeSelectAction.SelectExpand;

                                        if (dsLevel2.Tables[0].Rows[i2]["level2"].ToString() != "-")
                                        {
                                            if (dsLevel3.Tables.Count > 0)
                                            {
                                                if (dsLevel3.Tables[0].Rows.Count > 0)
                                                {
                                                    if (dsLevel3.Tables[0].Rows[0]["level3_Id"].ToString() != "-")
                                                    {
                                                        moduleNode.ChildNodes.Add(applicationNode);
                                                    }
                                                }
                                            }
                                        }
                                        previousMainApplication = (dsLevel2.Tables[0].Rows[i2]["level2_Id"].ToString());

                                        if (dsLevel2.Tables[0].Rows[i2]["level2"].ToString() == "-")
                                        {
                                            // BindLevel2AccCode(i2, i);
                                        }

                                        //level 3 
                                        if (dsLevel3.Tables.Count > 0)
                                        {
                                            if (dsLevel3.Tables[0].Rows.Count > 0)
                                            {
                                                for (int i3 = 0; i3 <= dsLevel3.Tables[0].Rows.Count - 1; i3++)
                                                {
                                                    if (dsLevel3.Tables[0].Rows[i3]["level3_Id"].ToString() != string.Empty || dsLevel3.Tables[0].Rows[i3]["level3"].ToString() == "-")
                                                    {
                                                        if (dsLevel3.Tables[0].Rows[i3]["level3_Id"].ToString() != previousFunction.ToString())
                                                        {
                                                            functionNode = new TreeNode();

                                                            if (dsLevel3.Tables[0].Rows[i3]["level3"].ToString() == "-")
                                                            {
                                                                dsLevel4 = new DataSet();
                                                            }
                                                            else
                                                            {
                                                                functionNode.Text = dsLevel3.Tables[0].Rows[i3]["level3"].ToString();
                                                                functionNode.Value = dsLevel3.Tables[0].Rows[i3]["level3_Id"].ToString();
                                                                dsLevel4 = DBMethod.ExecuteQuery(AccountingGroups_DAL.getLevel3Child(dsLevel3.Tables[0].Rows[i3]["level3_Id"].ToString()));
                                                            }

                                                            functionNode.PopulateOnDemand = true;

                                                            functionCount += 1;
                                                            submenuCount = 0;
                                                            functionNode.SelectAction = TreeNodeSelectAction.SelectExpand;

                                                            if (dsLevel3.Tables[0].Rows[i3]["level3"].ToString() != "-")
                                                            {
                                                                if (dsLevel4.Tables.Count > 0)
                                                                {
                                                                    if (dsLevel4.Tables[0].Rows.Count > 0)
                                                                    {
                                                                        if (dsLevel4.Tables[0].Rows[0]["level4"].ToString() != "-")
                                                                        {
                                                                            applicationNode.ChildNodes.Add(functionNode);
                                                                        }
                                                                        else if (dsLevel3.Tables[0].Rows[i3]["level3"].ToString() != "-" || dsLevel4.Tables[0].Rows[0]["level4"].ToString() != "-")
                                                                        {
                                                                            applicationNode.ChildNodes.Add(functionNode);
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            previousFunction = (dsLevel3.Tables[0].Rows[i3]["level3_Id"].ToString());

                                                            if (dsLevel3.Tables[0].Rows[i3]["level3"].ToString() == "-")
                                                            {
                                                                BindLevel2AccCode(i2, i);
                                                            }
                                                            if (dsLevel4.Tables.Count > 0)
                                                            {
                                                                if (dsLevel4.Tables[0].Rows.Count > 0)
                                                                {
                                                                    if (dsLevel3.Tables[0].Rows[i3]["level3"].ToString() != "-" || dsLevel4.Tables[0].Rows[0]["level4"].ToString() != "-")
                                                                    {
                                                                        BindLevel3AccCode(i3, i3);
                                                                    }
                                                                }
                                                            }
                                                            // start level4
                                                            if (dsLevel4.Tables.Count > 0)
                                                            {
                                                                if (dsLevel4.Tables[0].Rows.Count > 0)
                                                                {
                                                                    for (int i4 = 0; i4 <= dsLevel4.Tables[0].Rows.Count - 1; i4++)
                                                                    {
                                                                        if (dsLevel4.Tables[0].Rows[i4]["level4_Id"].ToString() != string.Empty || dsLevel4.Tables[0].Rows[i4]["level4"].ToString() == "-")
                                                                        {
                                                                            if (dsLevel4.Tables[0].Rows[i4]["level4_Id"].ToString() != previousSubMenu.ToString())
                                                                            {
                                                                                subMenuNode = new TreeNode();
                                                                                dsLevel5 = new DataSet();

                                                                                if (dsLevel4.Tables[0].Rows[i4]["level4"].ToString() == "-")
                                                                                {
                                                                                    dsLevel5 = new DataSet();
                                                                                }
                                                                                else
                                                                                {
                                                                                    subMenuNode.Text = dsLevel4.Tables[0].Rows[i4]["level4"].ToString();
                                                                                    subMenuNode.Value = dsLevel4.Tables[0].Rows[i4]["level4_Id"].ToString();
                                                                                    dsLevel5 = DBMethod.ExecuteQuery(AccountingGroups_DAL.getLevel4Child(dsLevel4.Tables[0].Rows[i4]["level4_Id"].ToString()));
                                                                                }

                                                                                subMenuNode.PopulateOnDemand = true;

                                                                                submenuCount += 1;
                                                                                subsubmenuCount = 0;
                                                                                subMenuNode.SelectAction = TreeNodeSelectAction.SelectExpand;

                                                                                if (dsLevel4.Tables[0].Rows[i4]["level4"].ToString() != "-")
                                                                                {
                                                                                    if (dsLevel5.Tables.Count > 0)
                                                                                    {
                                                                                        if (dsLevel5.Tables[0].Rows.Count > 0)
                                                                                        {
                                                                                            if (dsLevel5.Tables[0].Rows[0]["level5"].ToString() != "-")
                                                                                            {
                                                                                                //applicationNode.ChildNodes.Add(functionNode);
                                                                                                functionNode.ChildNodes.Add(subMenuNode);
                                                                                            }
                                                                                            else if (dsLevel4.Tables[0].Rows[i4]["level4"].ToString() != "-" || dsLevel5.Tables[0].Rows[0]["level5"].ToString() != "-")
                                                                                            {
                                                                                                functionNode.ChildNodes.Add(subMenuNode);
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                                previousSubMenu = (dsLevel4.Tables[0].Rows[i4]["level4_Id"].ToString());


                                                                                if (dsLevel4.Tables[0].Rows[i4]["level4"].ToString() == "-")
                                                                                {
                                                                                    BindLevel3AccCode(i3, i);
                                                                                }

                                                                                //start level 5
                                                                                if (dsLevel5.Tables.Count > 0)
                                                                                {
                                                                                    if (dsLevel5.Tables[0].Rows.Count > 0)
                                                                                    {
                                                                                        for (int i5 = 0; i5 <= dsLevel5.Tables[0].Rows.Count - 1; i5++)
                                                                                        {
                                                                                            if (dsLevel5.Tables[0].Rows[i5]["level5_Id"].ToString() != string.Empty || dsLevel5.Tables[0].Rows[i5]["level5"].ToString() == "-")
                                                                                            {
                                                                                                if (dsLevel5.Tables[0].Rows[i5]["level5_Id"].ToString() != previousSubSubMenu.ToString() || dsLevel5.Tables[0].Rows[i5]["level5"].ToString() == "-")
                                                                                                {
                                                                                                    subsubMenuNode = new TreeNode();

                                                                                                    if (dsLevel5.Tables[0].Rows[i5]["level5"].ToString() == "-")
                                                                                                    {
                                                                                                    }
                                                                                                    else
                                                                                                    {
                                                                                                        subsubMenuNode.Text = dsLevel5.Tables[0].Rows[i5]["level5"].ToString();
                                                                                                        subsubMenuNode.Value = dsLevel5.Tables[0].Rows[i5]["level5_Id"].ToString();
                                                                                                    }

                                                                                                    subsubMenuNode.PopulateOnDemand = true;

                                                                                                    subsubmenuCount += 1;
                                                                                                    accCodeCount = 0;
                                                                                                    subsubMenuNode.SelectAction = TreeNodeSelectAction.SelectExpand;


                                                                                                    subMenuNode.ChildNodes.Add(subsubMenuNode);

                                                                                                    previousSubSubMenu = (dsLevel5.Tables[0].Rows[i5]["level5_Id"].ToString());

                                                                                                    //start acc code
                                                                                                    if (dsLevel5.Tables[0].Rows[i5]["level5"].ToString() == "-")
                                                                                                    {
                                                                                                        BindLevel4AccCode(i4, i3);
                                                                                                    }
                                                                                                    else
                                                                                                    {
                                                                                                        BindLevel5AccCode(i5, i4);
                                                                                                    }

                                                                                                    //if (dsAccCode.Tables.Count == 0 || dsAccCode.Tables[0].Rows.Count== 0)
                                                                                                    //{
                                                                                                    //    if (dsLevel5.Tables[0].Rows[0]["level5"].ToString() != "-")
                                                                                                    //    {
                                                                                                    //        functionNode.ChildNodes.RemoveAt(i4);
                                                                                                    //    }
                                                                                                    //    //subMenuNode.ChildNodes.Remove(i3);
                                                                                                    //}
                                                                                                    //end acc code
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        BindLevel4AccCode(i4, i3);
                                                                                    }
                                                                                }
                                                                                //// end level5
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    BindLevel3AccCode(i3, i3);
                                                                }
                                                            }
                                                            //  end level4
                                                        }
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                BindLevel2AccCode(i2, i);
                                            }
                                        }
                                        //end level3
                                    }
                                }
                            }
                        }
                        else
                        {
                            BindLevel2AccCode(i, i);
                        }
                        //end level2
                    }
                    tvModules.Nodes.Add(moduleNode);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountsscodeGroupingReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }
        private void BindLevel5AccCode(int i5, int i4)
        {
            try
            {
                if (subsubMenuNode != null)
                {
                    if (dsLevel5.Tables[0].Rows[i5]["level5"].ToString() == "-")
                    {

                    }
                    else
                    {
                        dsAccCode = DBMethod.ExecuteQuery(AccountingGroups_DAL.getAccCodes(dsLevel5.Tables[0].Rows[i5]["level5_Id"].ToString(), "l5"));
                    }
                }

                if (dsAccCode.Tables.Count > 0)
                {
                    if (dsAccCode.Tables[0].Rows.Count > 0)
                    {
                        for (int i6 = 0; i6 <= dsAccCode.Tables[0].Rows.Count - 1; i6++)
                        {
                            if (dsAccCode.Tables[0].Rows[i6]["acc_code_id"].ToString() != string.Empty)
                            {
                                if (dsAccCode.Tables[0].Rows[i6]["acc_code_id"].ToString() != previousAccCodeMenu.ToString())
                                {
                                    accCodeNodeNode = new TreeNode();

                                    accCodeNodeNode.Text = dsAccCode.Tables[0].Rows[i6]["acc_code_desc"].ToString();
                                    accCodeNodeNode.Value = dsAccCode.Tables[0].Rows[i6]["acc_code_id"].ToString();

                                    accCodeCount += 1;

                                    if (subsubMenuNode != null)
                                    {
                                        subsubMenuNode.ChildNodes.Add(accCodeNodeNode);
                                    }

                                    previousAccCodeMenu = (dsAccCode.Tables[0].Rows[i6]["acc_code_id"].ToString());
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYCbuds", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void BindLevel2AccCode(int i2, int i1)
        {
            try
            {
                if (applicationNode != null)
                {
                    if (dsLevel2.Tables[0].Rows[i2]["level2"].ToString() == "-")
                    {
                        //dsAccCode = DBMethod.ExecuteQuery(AccountingGroups_DAL.getAccCodes(dsLevel2.Tables[0].Rows[i2]["level1_Id"].ToString(), "l5"));
                    }
                    else
                    {
                        dsAccCode = DBMethod.ExecuteQuery(AccountingGroups_DAL.getAccCodes(dsLevel2.Tables[0].Rows[i2]["level2_Id"].ToString(), "l5"));
                    }
                }
                if (dsAccCode.Tables.Count > 0)
                {
                    if (dsAccCode.Tables[0].Rows.Count > 0)
                    {
                        for (int i6 = 0; i6 <= dsAccCode.Tables[0].Rows.Count - 1; i6++)
                        {
                            if (dsAccCode.Tables[0].Rows[i6]["acc_code_id"].ToString() != string.Empty)
                            {
                                if (dsAccCode.Tables[0].Rows[i6]["acc_code_id"].ToString() != previousAccCodeMenu.ToString())
                                {
                                    accCodeNodeNode = new TreeNode();

                                    accCodeNodeNode.Text = dsAccCode.Tables[0].Rows[i6]["acc_code_desc"].ToString();
                                    accCodeNodeNode.Value = dsAccCode.Tables[0].Rows[i6]["acc_code_id"].ToString();


                                    accCodeCount += 1;

                                    if (applicationNode != null)
                                    {
                                        applicationNode.ChildNodes.Add(accCodeNodeNode);
                                    }

                                    previousAccCodeMenu = (dsAccCode.Tables[0].Rows[i6]["acc_code_id"].ToString());
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYCbuds", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void BindLevel3AccCode(int i3, int i2)
        {
            try
            {
                if (functionNode != null)
                {
                    if (dsLevel3.Tables[0].Rows[i3]["level3"].ToString() == "-")
                    {
                    }
                    else
                    {
                        dsAccCode = DBMethod.ExecuteQuery(AccountingGroups_DAL.getAccCodes(dsLevel3.Tables[0].Rows[i3]["level3_Id"].ToString(), "l5"));
                    }
                }

                if (dsAccCode.Tables.Count > 0)
                {
                    if (dsAccCode.Tables[0].Rows.Count > 0)
                    {
                        for (int i6 = 0; i6 <= dsAccCode.Tables[0].Rows.Count - 1; i6++)
                        {
                            if (dsAccCode.Tables[0].Rows[i6]["acc_code_id"].ToString() != string.Empty)
                            {
                                if (dsAccCode.Tables[0].Rows[i6]["acc_code_id"].ToString() != previousAccCodeMenu.ToString())
                                {
                                    accCodeNodeNode = new TreeNode();

                                    accCodeNodeNode.Text = dsAccCode.Tables[0].Rows[i6]["acc_code_desc"].ToString();
                                    accCodeNodeNode.Value = dsAccCode.Tables[0].Rows[i6]["acc_code_id"].ToString();

                                    accCodeCount += 1;

                                    if (functionNode != null)
                                    {
                                        functionNode.ChildNodes.Add(accCodeNodeNode);
                                    }

                                    previousAccCodeMenu = (dsAccCode.Tables[0].Rows[i6]["acc_code_id"].ToString());
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYCbuds", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void BindLevel4AccCode(int i4, int i3)
        {
            try
            {
                if (subMenuNode != null)
                {
                    if (dsLevel4.Tables[0].Rows[i4]["level4"].ToString() == "-")
                    {
                        // dsAccCode = DBMethod.ExecuteQuery(AccountingGroups_DAL.getAccCodes(dsLevel3.Tables[0].Rows[i3]["level3_Id"].ToString(), "l5"));
                    }
                    else
                    {
                        dsAccCode = DBMethod.ExecuteQuery(AccountingGroups_DAL.getAccCodes(dsLevel4.Tables[0].Rows[i4]["level4_Id"].ToString(), "l5"));
                    }
                }

                if (dsAccCode.Tables.Count > 0)
                {
                    if (dsAccCode.Tables[0].Rows.Count > 0)
                    {
                        for (int i6 = 0; i6 <= dsAccCode.Tables[0].Rows.Count - 1; i6++)
                        {
                            if (dsAccCode.Tables[0].Rows[i6]["acc_code_id"].ToString() != string.Empty)
                            {
                                if (dsAccCode.Tables[0].Rows[i6]["acc_code_id"].ToString() != previousAccCodeMenu.ToString())
                                {
                                    accCodeNodeNode = new TreeNode();

                                    accCodeNodeNode.Text = dsAccCode.Tables[0].Rows[i6]["acc_code_desc"].ToString();
                                    accCodeNodeNode.Value = dsAccCode.Tables[0].Rows[i6]["acc_code_id"].ToString();
                                    // accCodeNodeNode.PopulateOnDemand = true;

                                    accCodeCount += 1;

                                    if (subMenuNode != null)
                                    {
                                        subMenuNode.ChildNodes.Add(accCodeNodeNode);
                                    }

                                    previousAccCodeMenu = (dsAccCode.Tables[0].Rows[i6]["acc_code_id"].ToString());
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYCbuds", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void BindTreeviewold()
        {
            try
            {
                ErrorCollection.Clear();
                AccountingGroups_DAL.GetSP_AccountGroupLinksCode();

                int i, modulecount = 0, applicationCount = 0, moduleappcount = 0, functionCount = 0, submenuCount = 0, subsubmenuCount = 0, accCodeCount = 0;
                DataSet dsModuleList = new DataSet();

                dsModuleList = DBMethod.ExecuteQuery(AccountingGroups_DAL.GetAccountGroupLinkWithCode());

                if (dsModuleList.Tables[0].Rows.Count > 0)
                {
                    string previousModule = string.Empty, previousMainApplication = string.Empty, previousFunction = string.Empty, previousSubMenu = string.Empty;
                    string previousSubSubMenu = string.Empty;
                    string previousAccCodeMenu = string.Empty;
                    TreeNode moduleNode = null;
                    TreeNode applicationNode = null;
                    TreeNode functionNode = null;
                    TreeNode subMenuNode = null;
                    TreeNode subsubMenuNode = null;
                    TreeNode accCodeNodeNode = null;

                    for (i = 0; i <= dsModuleList.Tables[0].Rows.Count - 1; i++)
                    {
                        if (dsModuleList.Tables[0].Rows[i]["level1"].ToString() != string.Empty)
                        {
                            if (dsModuleList.Tables[0].Rows[i]["level1"].ToString() != previousModule.ToString())
                            {
                                if (previousModule != string.Empty)
                                {
                                    tvModules.Nodes.Add(moduleNode);
                                }
                                moduleNode = new TreeNode();
                                moduleNode.Text = dsModuleList.Tables[0].Rows[i]["level1"].ToString();
                                moduleNode.Value = dsModuleList.Tables[0].Rows[i]["rownum"].ToString();
                                moduleNode.PopulateOnDemand = true;
                                modulecount += 1;
                                applicationCount = 0;
                                functionCount = 0;
                                submenuCount = 0;
                                subsubmenuCount = 0;

                                moduleNode.SelectAction = TreeNodeSelectAction.SelectExpand;
                                previousModule = (dsModuleList.Tables[0].Rows[i]["level1"].ToString());
                            }
                        }
                        if (dsModuleList.Tables[0].Rows[i]["level2"].ToString() != string.Empty)
                        {
                            if (dsModuleList.Tables[0].Rows[i]["level2"].ToString() != previousMainApplication.ToString())
                            {
                              
                                applicationNode = new TreeNode();
                                applicationNode.Text = dsModuleList.Tables[0].Rows[i]["level2"].ToString();
                                applicationNode.Value = dsModuleList.Tables[0].Rows[i]["rownum"].ToString();
                                applicationNode.PopulateOnDemand = true;
                               
                                moduleappcount = modulecount - 1;
                                functionCount = 0;
                                //submenuCount = 0;

                                applicationCount += 1;
                                applicationNode.SelectAction = TreeNodeSelectAction.SelectExpand;
                                moduleNode.ChildNodes.Add(applicationNode);
                                previousMainApplication = (dsModuleList.Tables[0].Rows[i]["level2"].ToString());
                            }
                        }
                        if (dsModuleList.Tables[0].Rows[i]["level3"].ToString() != string.Empty)
                        {
                            if (dsModuleList.Tables[0].Rows[i]["level3"].ToString() != previousFunction.ToString())
                            {
                                functionNode = new TreeNode();
                                functionNode.Text = dsModuleList.Tables[0].Rows[i]["level3"].ToString();
                                functionNode.Value = dsModuleList.Tables[0].Rows[i]["rownum"].ToString();
                                functionNode.PopulateOnDemand = true;

                                functionCount += 1;
                                submenuCount = 0;
                                functionNode.SelectAction = TreeNodeSelectAction.SelectExpand;
                                applicationNode.ChildNodes.Add(functionNode);
                                previousFunction = (dsModuleList.Tables[0].Rows[i]["level3"].ToString());
                            }
                        }
                        if (dsModuleList.Tables[0].Rows[i]["level4"].ToString() != string.Empty)
                        {
                            if (dsModuleList.Tables[0].Rows[i]["level4"].ToString() != previousSubMenu.ToString())
                            {
                                subMenuNode = new TreeNode();

                                subMenuNode.Text = dsModuleList.Tables[0].Rows[i]["level4"].ToString();
                                subMenuNode.Value = dsModuleList.Tables[0].Rows[i]["rownum"].ToString();
                                subMenuNode.PopulateOnDemand = true;

                                submenuCount += 1;
                                subsubmenuCount = 0;
                                subMenuNode.SelectAction = TreeNodeSelectAction.Expand;
                                functionNode.ChildNodes.Add(subMenuNode);
                                previousSubMenu = (dsModuleList.Tables[0].Rows[i]["level4"].ToString());
                            }
                        }
                        if (dsModuleList.Tables[0].Rows[i]["level5"].ToString() != string.Empty)
                        {
                            if (dsModuleList.Tables[0].Rows[i]["level5"].ToString() != previousSubSubMenu.ToString())
                            {
                                subsubMenuNode = new TreeNode();

                                subsubMenuNode.Text = dsModuleList.Tables[0].Rows[i]["level5"].ToString();
                                subsubMenuNode.Value = dsModuleList.Tables[0].Rows[i]["rownum"].ToString();
                                subsubMenuNode.PopulateOnDemand = true;

                                subsubmenuCount += 1;
                                accCodeCount = 0;
                                subsubMenuNode.SelectAction = TreeNodeSelectAction.Expand;
                                subMenuNode.ChildNodes.Add(subsubMenuNode);
                                previousSubSubMenu = (dsModuleList.Tables[0].Rows[i]["level5"].ToString());
                            }
                        }
                        if (dsModuleList.Tables[0].Rows[i]["column_value"].ToString() != string.Empty)
                        {
                            if (dsModuleList.Tables[0].Rows[i]["column_value"].ToString() != previousAccCodeMenu.ToString())
                            {
                                accCodeNodeNode = new TreeNode();

                                accCodeNodeNode.Text = dsModuleList.Tables[0].Rows[i]["column_value"].ToString();
                                accCodeNodeNode.Value = dsModuleList.Tables[0].Rows[i]["rownum"].ToString();
                                // accCodeNodeNode.PopulateOnDemand = true;

                                accCodeCount += 1;
                                // accCodeNodeNode.SelectAction = TreeNodeSelectAction.Expand;
                                if (subsubMenuNode != null)
                                {
                                    subsubMenuNode.ChildNodes.Add(accCodeNodeNode);
                                }
                                else if (subMenuNode != null)
                                {
                                    subMenuNode.ChildNodes.Add(accCodeNodeNode);
                                }
                                else if (functionNode != null)
                                {
                                    functionNode.ChildNodes.Add(accCodeNodeNode);
                                }
                                else if (applicationNode != null)
                                {
                                    applicationNode.ChildNodes.Add(accCodeNodeNode);
                                }
                                else if (moduleNode != null)
                                {
                                    moduleNode.ChildNodes.Add(accCodeNodeNode);
                                }
                                previousAccCodeMenu = (dsModuleList.Tables[0].Rows[i]["column_value"].ToString());
                            }
                        }
                    }
                    tvModules.Nodes.Add(moduleNode);


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountsscodeGroupingReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }
        private void Startup()
        {

            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));

            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();

        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                ReportFile = Master.ReportName;


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountsscodeGroupingReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }
        protected void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;

                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;

                ReportData = FIN.BLL.GL.AccountcodeGrouping_BLL.GetAccountcodeGroupingReportData();

                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());

                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerPath + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountcodeGroupingReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }



    }
}