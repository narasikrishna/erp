﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using VMVServices.Web;
using FIN.BLL;
using System.Collections;
using FIN.DAL;


using iTextSharp.text.pdf;
using iTextSharp.text;

namespace FIN.Client.GL_Reports
{
    public partial class FinancialTemplate_SegmentViewParam : PageBase
    {

        DataTable dt_segment = new DataTable();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    AssignToControl();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TrailBalanceDetailReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }


        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                Startup();
                FillComboBox();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TrailBalanceDetailReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void Startup()
        {
            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));

            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();
        }
        private void FillComboBox()
        {
            FIN.BLL.GL.FinancialTemplate_Segment_BLL.fn_getTemplateName(ref ddlTemplateName);
            FIN.BLL.GL.AccountingCalendar_BLL.GetCalYear(ref ddlYear);

        }
        protected void btnView_Click(object sender, EventArgs e)
        {
            try
            {
                gvData.DataSource = new DataTable();
                gvData.DataBind();

                string str_FromDate = "01/01/2015";
                string str_ToDate = "31/12/2015";
                if (ddlYear.SelectedValue.ToString().Length > 0)
                {
                    DataTable dt_yeardata = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountingCalendar_DAL.GetStartdtEnddtfrcalyear(ddlYear.SelectedValue.ToString())).Tables[0];
                    if (dt_yeardata.Rows.Count > 0)
                    {
                        str_FromDate = DBMethod.ConvertDateToString(dt_yeardata.Rows[0]["cal_eff_start_dt"].ToString());
                        str_ToDate = DBMethod.ConvertDateToString(dt_yeardata.Rows[0]["call_eff_end_dt"].ToString());
                    }

                }
                if (ddlPeriod.SelectedValue.ToString().Length > 0)
                {
                    DataTable dt_yeardata = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountingCalendar_DAL.GetPeriodFromToDate(ddlPeriod.SelectedValue.ToString())).Tables[0];
                    if (dt_yeardata.Rows.Count > 0)
                    {
                        //str_FromDate = DBMethod.ConvertDateToString(dt_yeardata.Rows[0]["period_from_dt"].ToString());
                        str_ToDate = DBMethod.ConvertDateToString(dt_yeardata.Rows[0]["period_to_dt"].ToString());
                    }
                }

                string str_WithZero = "N";
                if (chkWithZero.Checked )
                {
                    str_WithZero = "Y";
                }

                DataTable dt = FIN.DAL.GL.FinancialTemplate_Segment_DAL.get_TmpleateData(ddlTemplateName.SelectedValue.ToString(), str_FromDate, str_ToDate, str_WithZero);


                Session["TemplateData"] = dt;
                var var_empList = dt.AsEnumerable().Where(r => r["PARENT_GROUP_ID"].ToString().Contains("HEAD"));

                if (var_empList.Any())
                {
                    DataTable dt_tmp = System.Data.DataTableExtensions.CopyToDataTable(var_empList);

                    DataRow dr = dt_tmp.NewRow();

                    dr["B_TYPE"] = "EXPAND";
                    dr["BS_TMPL_ID"] = "0";
                    dr["ACCT_GRP_ID"] = "0";
                    dr["PARENT_GROUP_ID"] = "HEAD";
                    dr["ACCT_GRP_NAME"] = "Total ";
                    dr["GROUP_NUMBER"] = "0";
                    dr["GROUP_NOTE"] = "0";
                    dr["GROUP_LEVEL"] = "0";

                    for (int iLoop = 8; iLoop < dt_tmp.Columns.Count-1; iLoop++)
                    {
                        dr[iLoop] = dt_tmp.Compute("SUM(" + dt_tmp.Columns[iLoop].Caption + ")", "PARENT_GROUP_ID='HEAD'");

                    }
                    dt_tmp.Rows.Add(dr);

                    BindGrid(dt_tmp);
                }

                divExpandLevel.Visible = true;




            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TrailBalanceDetailReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }
        private void BindGrid(DataTable dt_data)
        {


            dt_segment = DBMethod.ExecuteQuery(FIN.DAL.GL.FinancialTemplate_Segment_DAL.getTemplateSegment(ddlTemplateName.SelectedValue.ToString())).Tables[0];


            //for (int sLoop = 0; sLoop < dt_segment.Rows.Count; sLoop++)
            // {
            //     dt_data.AsEnumerable().ToList().ForEach(p => p.SetField<String>(dt_segment.Rows[sLoop]["SEGMENT_VALUE_ID"].ToString(), DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>(dt_segment.Rows[sLoop]["SEGMENT_VALUE_ID"].ToString()))));
            // }
            // dt_data.AcceptChanges();

            gvData.DataSource = dt_data;
            gvData.DataBind();
            Session[FINSessionConstants.GridData] = dt_data;
            imgBtnExport.Visible = true;
            Reload_BS_Structure();



        }


        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.Header)
            {
                for (int rLoop = 2; rLoop < 10; rLoop++)
                {
                    e.Row.Cells[rLoop].Visible = false;

                }
                e.Row.Cells[e.Row.Cells.Count - 1].Visible = false;
                for (int rLoop = 10; rLoop < e.Row.Cells.Count - 1; rLoop++)
                {
                    DataRow[] dr = dt_segment.Select("SEGMENT_VALUE_ID='" + e.Row.Cells[rLoop].Text + "'");
                    if (dr.Length > 0)
                    {
                        e.Row.Cells[rLoop].Text = dr[0]["SEGMENT_VALUE"].ToString();
                        e.Row.Cells[rLoop].Width = 250;
                    }
                }
            }
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                for (int rLoop = 2; rLoop < 10; rLoop++)
                {
                    e.Row.Cells[rLoop].Visible = false;

                }
                e.Row.Cells[e.Row.Cells.Count - 1].Visible = false;
                for (int rLoop = 10; rLoop < e.Row.Cells.Count - 1; rLoop++)
                {
                    e.Row.Cells[rLoop].HorizontalAlign = HorizontalAlign.Right;
                    if (e.Row.Cells[rLoop].Text.ToString().Contains("&nbsp"))
                    {
                        e.Row.Cells[rLoop].Text = "0";
                    }
                    e.Row.Cells[rLoop].Text = DBMethod.GetAmtDecimalCommaSeparationValueWithNegative(e.Row.Cells[rLoop].Text);
                    e.Row.Cells[rLoop].Width = 250;

                }

                Label lbl_tmp = e.Row.FindControl("lblAcct_grp_Name") as Label;
                string str_Space = "";
                if (gvData.DataKeys[e.Row.RowIndex]["GROUP_LEVEL"].ToString() != "20")
                {
                    for (int iLoop = 0; iLoop < 5-int.Parse(gvData.DataKeys[e.Row.RowIndex]["GROUP_LEVEL"].ToString()); iLoop++)
                    {
                        str_Space = str_Space + "&nbsp;&nbsp;";
                    }
                }
                lbl_tmp.Text =str_Space+lbl_tmp.Text;//.Replace(" ", "&nbsp;");

                ImageButton Head_img_btn = (ImageButton)e.Row.FindControl("img_Det");
                if (gvData.DataKeys[e.Row.RowIndex]["B_TYPE"].ToString() != "EXPAND")
                {
                    Head_img_btn.CommandName = "MINUS";
                    Head_img_btn.ImageUrl = "../Images/collapse_blue.png";
                }
                else
                {
                    Head_img_btn.CommandName = "PLUS";
                    Head_img_btn.ImageUrl = "../Images/expand_blue.png";
                }
                if (gvData.DataKeys[e.Row.RowIndex]["GROUP_LEVEL"].ToString() == "0")
                {
                    Head_img_btn.Visible = false;
                }
            }

        }

        protected void img_Det_Click(object sender, ImageClickEventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            string Acct_grp_id = gvData.DataKeys[gvr.RowIndex].Values["ACCT_GRP_ID"].ToString();

            ImageButton Head_img_btn = (ImageButton)gvData.Rows[gvr.RowIndex].FindControl("img_Det");

            DataTable dt = (DataTable)Session["TemplateData"];
            DataTable dt_tmp = (DataTable)Session[FINSessionConstants.GridData];


            int row_index = gvr.RowIndex;
            if (Head_img_btn.CommandName == "PLUS")
            {
                dt_tmp.Rows[row_index][0] = "COLLAPSE";
            }
            else
            {
                dt_tmp.Rows[row_index][0] = "EXPAND";
            }
            if (Head_img_btn.CommandName == "PLUS")
            {
                var var_empList = dt.AsEnumerable().Where(r => r["PARENT_GROUP_ID"].ToString().Contains(Acct_grp_id));

                if (var_empList.Any())
                {
                    DataTable dt_Group_tmp = System.Data.DataTableExtensions.CopyToDataTable(var_empList);
                    for (int kLoop = 0; kLoop < dt_Group_tmp.Rows.Count; kLoop++)
                    {
                        row_index += 1;
                        DataRow dr = dt_tmp.NewRow();

                        for (int rloop = 0; rloop < dt_Group_tmp.Columns.Count; rloop++)
                        {
                            dr[rloop] = dt_Group_tmp.Rows[kLoop][rloop];
                        }

                        // dr = dt_Group_tmp.Rows[kLoop];
                        dt_tmp.Rows.InsertAt(dr, row_index);
                        // dt_tmp.Rows.Add(dr);

                    }


                }
            }
            else
            {
                row_index += 1;
                for (int kLoop = row_index; kLoop < dt_tmp.Rows.Count; kLoop++)
                {
                    //if ( dt_tmp.Rows[kLoop]["PARENT_GROUP_ID"].ToString() == Acct_grp_id)
                    if (Acct_grp_id.Contains(dt_tmp.Rows[kLoop]["PARENT_GROUP_ID"].ToString()))
                    {
                        Acct_grp_id += "~" + dt_tmp.Rows[kLoop]["ACCT_GRP_ID"].ToString();
                        dt_tmp.Rows.RemoveAt(kLoop);
                        kLoop--;
                    }
                }
            }



            BindGrid(dt_tmp);





        }



        private void Reload_BS_Structure()
        {
            try
            {
                DataTable dt_BS_TMPL_DATA = DBMethod.ExecuteQuery(FIN.DAL.GL.FinancialTemplate_Segment_DAL.get_GroupTemplateStructure(ddlTemplateName.SelectedValue.ToString())).Tables[0];
                string str_ORG_STR = "";


                if (dt_BS_TMPL_DATA.Rows.Count > 0)
                {
                    DataTable dt_EmpProcessList = new DataTable();
                    dt_EmpProcessList.Columns.Add("GROUP_NAME");
                    dt_EmpProcessList.Columns.Add("BS_TMPL_ID");
                    dt_EmpProcessList.Columns.Add("BS_DISPLAY_NAME");

                    dt_EmpProcessList.Columns.Add("STATUS");

                    var var_HeadEmpList = dt_BS_TMPL_DATA.AsEnumerable().Where(r => r["BS_TMPL_PARENT_GROUP_NAME"].ToString().Contains("HEAD"));
                    if (var_HeadEmpList.Any())
                    {
                        DataTable dt_data = System.Data.DataTableExtensions.CopyToDataTable(var_HeadEmpList);



                        string str_Replace_str = "";
                        string str_new_replace_str = "";
                        string str_tmp;
                        str_ORG_STR += "  <ul  id='BSTemp'  > ";
                        str_ORG_STR += "  <li><a onclick=fn_GroupAccClick('','" + ddlTemplateName.SelectedValue.ToString() + "')> " + ddlTemplateName.SelectedValue.ToString() + "</a>";
                        str_ORG_STR += " <ul>";
                        for (int iLoop = 0; iLoop < dt_data.Rows.Count; iLoop++)
                        {
                            DataRow dr = dt_EmpProcessList.NewRow();
                            dr["GROUP_NAME"] = dt_data.Rows[iLoop]["BS_GROUP_NAME"].ToString();
                            dr["BS_TMPL_ID"] = dt_data.Rows[iLoop]["BS_TMPL_ID"].ToString();
                            dr["BS_DISPLAY_NAME"] = dt_data.Rows[iLoop]["BS_DISPLAY_NAME"].ToString();
                            dr["STATUS"] = "N";
                            dt_EmpProcessList.Rows.Add(dr);

                            str_ORG_STR += "<li>";
                            str_ORG_STR += dt_data.Rows[iLoop]["BS_TMPL_ID"].ToString();
                            str_ORG_STR += "</li>";
                            DataRow[] dr_col;
                            do
                            {
                                dr_col = dt_EmpProcessList.Select("STATUS='N'");
                                if (dr_col.Length > 0)
                                {
                                    str_Replace_str = "<li>";
                                    str_Replace_str += dr_col[0]["BS_TMPL_ID"].ToString();
                                    str_Replace_str += "</li>";
                                    str_new_replace_str = "<li> <a onclick=fn_GroupAccClick('" + dr_col[0]["BS_TMPL_ID"].ToString() + "','" + dr_col[0]["BS_DISPLAY_NAME"].ToString().Replace(" ", "_") + "')> " + dr_col[0]["BS_DISPLAY_NAME"].ToString() + "</a>";
                                    str_new_replace_str += "<UL>";

                                    var var_empList = dt_BS_TMPL_DATA.AsEnumerable().Where(r => r["BS_TMPL_PARENT_ID"].ToString().Contains(dr_col[0]["BS_TMPL_ID"].ToString()));
                                    str_tmp = "";
                                    if (var_empList.Any())
                                    {
                                        DataTable dt_EMP_List = System.Data.DataTableExtensions.CopyToDataTable(var_empList);

                                        for (int jLoop = 0; jLoop < dt_EMP_List.Rows.Count; jLoop++)
                                        {
                                            str_tmp += "<li>";
                                            str_tmp += dt_EMP_List.Rows[jLoop]["BS_TMPL_ID"].ToString();
                                            str_tmp += "</li>";

                                            dr = dt_EmpProcessList.NewRow();
                                            dr["GROUP_NAME"] = dt_EMP_List.Rows[jLoop]["BS_GROUP_NAME"].ToString();
                                            dr["BS_TMPL_ID"] = dt_EMP_List.Rows[jLoop]["BS_TMPL_ID"].ToString();
                                            dr["BS_DISPLAY_NAME"] = dt_EMP_List.Rows[jLoop]["BS_DISPLAY_NAME"].ToString();
                                            dr["STATUS"] = "N";
                                            dt_EmpProcessList.Rows.Add(dr);

                                        }
                                        str_ORG_STR = str_ORG_STR.Replace(str_Replace_str, str_new_replace_str + str_tmp + "</UL></li>");
                                    }
                                    else
                                    {
                                        str_ORG_STR = str_ORG_STR.Replace(str_Replace_str, str_new_replace_str.Replace("<UL>", "</li>"));
                                    }

                                    dr_col[0]["STATUS"] = "Y";
                                    dt_EmpProcessList.AcceptChanges();
                                }
                                dr_col = dt_EmpProcessList.Select("STATUS='N'");
                            } while (dr_col.Length > 0);

                        }
                        str_ORG_STR += " </ul>";
                        str_ORG_STR += "  </li> ";
                        str_ORG_STR += "  </ul> ";

                    }
                }
                else
                {
                    str_ORG_STR += "  <ul  id='BSTemp'> ";
                    str_ORG_STR += "  <li> <div onClick=fn_GroupAccClick('','') > " + ddlTemplateName.SelectedValue.ToString() + "</div>";
                    str_ORG_STR += "  </li> ";
                    str_ORG_STR += "  </ul> ";
                }

                div_Template.InnerHtml = str_ORG_STR;
                ScriptManager.RegisterStartupScript(btnView, typeof(Button), "LoadGraph", "fn_showGraph()", true);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }


        }


        protected void btnTmpl_Click(object sender, EventArgs e)
        {
            DataTable dt = (DataTable)Session["TemplateData"];
            DataTable dt_tmp = new DataTable();



            var var_empList = dt.AsEnumerable().Where(r => r["PARENT_GROUP_ID"].ToString().Contains(hf_tmpl_id.Value.ToString()));

            if (var_empList.Any())
            {
                DataTable dt_Group_tmp = System.Data.DataTableExtensions.CopyToDataTable(var_empList);
                dt_tmp = dt_Group_tmp;
            }
            var_empList = dt.AsEnumerable().Where(r => r["ACCT_GRP_ID"].ToString().Contains(hf_tmpl_id.Value.ToString()));

            if (var_empList.Any())
            {
                DataTable dt_Group_tmp = System.Data.DataTableExtensions.CopyToDataTable(var_empList);
                DataRow dr = dt_tmp.NewRow();
                for (int rloop = 0; rloop < dt_Group_tmp.Columns.Count; rloop++)
                {
                    dr[rloop] = dt_Group_tmp.Rows[0][rloop];
                }
                dr[0] = "COLLAPSE";
                dt_tmp.Rows.InsertAt(dr, 0);

            }


            DataRow dr1 = dt_tmp.NewRow();

            dr1["B_TYPE"] = "EXPAND";
            dr1["BS_TMPL_ID"] = "0";
            dr1["ACCT_GRP_ID"] = "0";
            dr1["PARENT_GROUP_ID"] = "HEAD";
            dr1["ACCT_GRP_NAME"] = "Total ";
            dr1["GROUP_NUMBER"] = "0";
            dr1["GROUP_NOTE"] = "0";
            dr1["GROUP_LEVEL"] = "0";

            for (int iLoop = 8; iLoop < dt_tmp.Columns.Count - 1; iLoop++)
            {
                dr1[iLoop] = dt_tmp.Rows[0][iLoop];

            }
            dt_tmp.Rows.Add(dr1);

            BindGrid(dt_tmp);


        }

        protected void txtExpandLevel_TextChanged(object sender, EventArgs e)
        {

            ShowRecord(0);


        }

        private void ShowRecord(int int_groupLevel)
        {

            DataTable dt = (DataTable)Session["TemplateData"];

            DataView dv_groupLevel = dt.DefaultView;
            dv_groupLevel.Sort = "GROUP_LEVEL DESC";
            DataTable dt_GroupLevel = dv_groupLevel.ToTable(true, "GROUP_LEVEL");

            if (int.Parse(txtExpandLevel.Text) > dt_GroupLevel.Rows.Count)
            {
                txtExpandLevel.Text = (dt_GroupLevel.Rows.Count).ToString();
            }


            DataTable dt_data = new DataTable();
            DataTable dt_BindData = new DataTable();
            DataTable dt_NextLevelData = new DataTable();

            DataTable dt_tmp_bindData = new DataTable();

            var var_empList = dt.AsEnumerable().Where(r => r["GROUP_LEVEL"].ToString().Contains("20"));

            if (var_empList.Any())
            {
                DataTable dt_tmp = System.Data.DataTableExtensions.CopyToDataTable(var_empList);

                dt_data = dt_tmp.Copy();

                dt_BindData = dt_tmp.Copy();


                dt_NextLevelData = dt_tmp.Copy();


                for (int i = 1; i <= int.Parse(txtExpandLevel.Text); i++)
                {

                    dt_data = dt_NextLevelData.Copy();
                    dt_NextLevelData.Rows.Clear();


                    for (int iLoop = 0; iLoop < dt_data.Rows.Count; iLoop++)
                    {
                        dt_tmp_bindData = dt_BindData.Copy();


                        var var_selAcct_GrpData = dt.AsEnumerable().Where(r => r["PARENT_GROUP_ID"].ToString().Contains(dt_data.Rows[iLoop]["ACCT_GRP_ID"].ToString()) && int.Parse(r["GROUP_LEVEL"].ToString()) > int_groupLevel);
                        if (var_selAcct_GrpData.Any())
                        {
                            dt_BindData.Rows.Clear();
                            DataTable dt_sel_acct_data = System.Data.DataTableExtensions.CopyToDataTable(var_selAcct_GrpData);
                            DataRow dr;
                            for (int k = 0; k < dt_tmp_bindData.Rows.Count; k++)
                            {

                                dr = dt_BindData.NewRow();
                                for (int rloop = 0; rloop < dt_tmp_bindData.Columns.Count; rloop++)
                                {
                                    dr[rloop] = dt_tmp_bindData.Rows[k][rloop];
                                }

                                dt_BindData.Rows.Add(dr);
                                if (dt_tmp_bindData.Rows[k]["ACCT_GRP_ID"].ToString() == dt_data.Rows[iLoop]["ACCT_GRP_ID"].ToString())
                                {
                                    dt_BindData.Rows[dt_BindData.Rows.Count - 1][0] = "COLLAPSE";
                                    break;
                                }
                            }

                            for (int kLoop = 0; kLoop < dt_sel_acct_data.Rows.Count; kLoop++)
                            {
                                dr = dt_BindData.NewRow();
                                for (int rloop = 0; rloop < dt_sel_acct_data.Columns.Count; rloop++)
                                {
                                    dr[rloop] = dt_sel_acct_data.Rows[kLoop][rloop];
                                }
                                dt_BindData.Rows.Add(dr);
                            }

                            for (int kLoop = 0; kLoop < dt_sel_acct_data.Rows.Count; kLoop++)
                            {
                                dr = dt_NextLevelData.NewRow();
                                for (int rloop = 0; rloop < dt_sel_acct_data.Columns.Count; rloop++)
                                {
                                    dr[rloop] = dt_sel_acct_data.Rows[kLoop][rloop];
                                }
                                dt_NextLevelData.Rows.Add(dr);
                            }


                            bool bol_addData = false;
                            for (int k = 0; k < dt_tmp_bindData.Rows.Count; k++)
                            {
                                if (bol_addData)
                                {
                                    dr = dt_BindData.NewRow();
                                    for (int rloop = 0; rloop < dt_tmp_bindData.Columns.Count; rloop++)
                                    {
                                        dr[rloop] = dt_tmp_bindData.Rows[k][rloop];
                                    }
                                    dt_BindData.Rows.Add(dr);
                                }
                                if (dt_tmp_bindData.Rows[k]["ACCT_GRP_ID"].ToString() == dt_data.Rows[iLoop]["ACCT_GRP_ID"].ToString())
                                {
                                    bol_addData = true;
                                }
                            }

                        }
                    }
                }


                DataRow dr1 = dt_BindData.NewRow();

                dr1["B_TYPE"] = "EXPAND";
                dr1["BS_TMPL_ID"] = "0";
                dr1["ACCT_GRP_ID"] = "0";
                dr1["PARENT_GROUP_ID"] = "HEAD";
                dr1["ACCT_GRP_NAME"] = "Total ";
                dr1["GROUP_NUMBER"] = "0";
                dr1["GROUP_NOTE"] = "0";
                dr1["GROUP_LEVEL"] = "0";

                for (int iLoop = 8; iLoop < dt_tmp.Columns.Count - 1; iLoop++)
                {
                    dr1[iLoop] = dt_tmp.Compute("SUM(" + dt_tmp.Columns[iLoop].Caption + ")", "PARENT_GROUP_ID='HEAD'");

                }
                dt_BindData.Rows.Add(dr1);


                BindGrid(dt_BindData);
            }
        }

        protected void btnExpandAll_Click(object sender, EventArgs e)
        {

            DataTable dt = (DataTable)Session["TemplateData"];

            DataView dv_groupLevel = dt.DefaultView;
            dv_groupLevel.Sort = "GROUP_LEVEL DESC";
            DataTable dt_GroupLevel = dv_groupLevel.ToTable(true, "GROUP_LEVEL");
            txtExpandLevel.Text = (dt_GroupLevel.Rows.Count).ToString();
            ShowRecord(-1);
        }




        protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            FIN.BLL.GL.AccountingCalendar_BLL.GetFinancialPeriod4Year(ref ddlPeriod, ddlYear.SelectedValue.ToString(),false);
        }




        protected void imgBtnExport_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                DataTable dt_data = (DataTable)Session[FINSessionConstants.GridData];
                DataTable dt_Crystal_Data = new DataTable();
                dt_Crystal_Data.Columns.Add("ROW_NUMBER");
                dt_Crystal_Data.Columns.Add("GROUP_NAME");
                dt_Crystal_Data.Columns.Add("GROUP_LEVEL");
                dt_Crystal_Data.Columns.Add("SEGMENT_NAME");
                dt_Crystal_Data.Columns.Add("SEGMENT_VALUE",typeof(double));
                dt_Crystal_Data.Columns.Add("ORG_ID");

               

                for (int iLoop = 0; iLoop < gvData.Rows.Count; iLoop++)
                {
                    for (int rLoop = 8; rLoop < gvData.HeaderRow.Cells.Count; rLoop++)
                    {
                        if (gvData.HeaderRow.Cells[rLoop].Visible)
                        {
                            DataRow dr = dt_Crystal_Data.NewRow();
                            dr["ROW_NUMBER"] = iLoop;
                            dr["GROUP_NAME"] = ((Label)gvData.Rows[iLoop].FindControl("lblAcct_grp_Name")).Text.Replace("&nbsp;", " ");
                            dr["GROUP_LEVEL"] = gvData.Rows[iLoop].Cells[9].Text;
                            dr["SEGMENT_NAME"] = gvData.HeaderRow.Cells[rLoop].Text;
                            dr["SEGMENT_VALUE"] = gvData.Rows[iLoop].Cells[rLoop].Text.Replace(",","").Replace("(","-").Replace(")","") ;
                            dr["ORG_ID"] = VMVServices.Web.Utils.OrganizationID;
                            dt_Crystal_Data.Rows.Add(dr);
                        }
                    }
                }


                ErrorCollection.Clear();


                Hashtable htParameters = new Hashtable();
                Hashtable htHeadingParameters = new Hashtable();
                Hashtable htFilterParameter = new Hashtable();

                ReportFile = Master.ReportName;

                ReportData = new DataSet();
                ReportData.Tables.Add(dt_Crystal_Data.Copy());

                htHeadingParameters.Add("ReportName", ddlTemplateName.SelectedItem.Text);
                htHeadingParameters.Add("SELYEAR", ddlYear.SelectedItem.Text);
                htHeadingParameters.Add("SELPERIOD", ddlPeriod.SelectedItem.Text);

                VMVServices.Web.Utils.ReportFilterParameter = htHeadingParameters;

                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('../RPTCrystalReportViewer.aspx?Id?Id=" + Master.Mode + "','','" + VMVServices.Services.Data.DbConsts.ReportProperties + "');", true);
                
/*

                Document doc = new Document(iTextSharp.text.PageSize.A0);
                System.IO.FileStream file =
                    new System.IO.FileStream(Server.MapPath("~/PdfSample") +
                    DateTime.Now.ToString("ddMMyyHHmmss") + ".pdf",
                    System.IO.FileMode.OpenOrCreate);
                PdfWriter writer = PdfWriter.GetInstance(doc, file);
                // calling PDFFooter class to Include in document
                writer.PageEvent = new PDFFooter();
                doc.Open();

                DataTable dt_data = (DataTable)Session[FINSessionConstants.GridData];


                doc.Add(new Paragraph(" "));

                doc.Add(new Paragraph(" "));

                PdfPTable tab = new PdfPTable(dt_data.Columns.Count - 5);
                PdfPCell cell = new PdfPCell(new Phrase("Cost Center",
                                    new iTextSharp.text.Font(1, 24F)));
                cell.Colspan = dt_data.Columns.Count - 5;
                cell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                //Style
                //cell.BorderColor = new BaseColor(System.Drawing.Color.Red);
                cell.Border = iTextSharp.text.Rectangle.BOTTOM_BORDER; // | Rectangle.TOP_BORDER;
                cell.BorderWidthBottom = 3f;
                tab.AddCell(cell);


                tab.AddCell("Group Name");
                tab.AddCell("Note");

                for (int rLoop = 7; rLoop < dt_data.Columns.Count; rLoop++)
                {
                    DataRow[] dr = dt_segment.Select("SEGMENT_VALUE_ID='" + dt_data.Columns[rLoop].Caption + "'");
                    if (dr.Length > 0)
                    {
                        tab.AddCell(dr[0]["SEGMENT_VALUE"].ToString());                       
                      
                    }
                }


                for (int kLoop = 0; kLoop < dt_data.Rows.Count; kLoop++)
                {
                    tab.AddCell(dt_data.Rows[kLoop]["ACCT_GRP_NAME"].ToString());
                    tab.AddCell(dt_data.Rows[kLoop]["GROUP_NOTE"].ToString());
                    for (int rLoop = 7; rLoop < dt_data.Columns.Count; rLoop++)
                    {
                        tab.AddCell(dt_data.Rows[kLoop][rLoop].ToString());
                    }
                }

                doc.Add(tab);
                doc.Close();
                file.Close();

                Reload_BS_Structure();
 * */
                /*
                //row 1
                tab.AddCell("R1C1");
                tab.AddCell("R1C2");
                tab.AddCell("R1C3");
                //row 2
                tab.AddCell("R2C1");
                tab.AddCell("R2C2");
                tab.AddCell("R2C3");
                cell = new PdfPCell();
                cell.Colspan = 3;
                iTextSharp.text.List pdfList = new List(List.UNORDERED);
                pdfList.Add(new iTextSharp.text.ListItem(new Phrase("Unorder List 1")));
                pdfList.Add("Unorder List 2");
                pdfList.Add("Unorder List 3");
                pdfList.Add("Unorder List 4");
                cell.AddElement(pdfList);
                tab.AddCell(cell);
                 */
              

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ExportClick", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

       




    }



    public class PDFFooter : PdfPageEventHelper
    {
        // write on top of document
        public override void OnOpenDocument(PdfWriter writer, Document document)
        {
            base.OnOpenDocument(writer, document);
            PdfPTable tabFot = new PdfPTable(new float[] { 1F });
            tabFot.SpacingAfter = 10F;
            PdfPCell cell;
            tabFot.TotalWidth = 300F;
           
            iTextSharp.text.Image giff = iTextSharp.text.Image.GetInstance("F:\\VMV\\ERP\\CRCERP\\FIN.Client\\UploadFile\\ORG_LOGO\\ORG-0000000066.jpg");
            giff.Alignment = iTextSharp.text.Image.TEXTWRAP | iTextSharp.text.Image.ALIGN_LEFT;
            giff.ScaleToFit(100f, 100f);
            giff.IndentationLeft = 9f;
            giff.SpacingAfter = 9f;

            //cell = new PdfPCell(new Phrase("Header"));
            cell = new PdfPCell(giff);
            tabFot.AddCell(cell);
            tabFot.WriteSelectedRows(0, -1, 150, document.Top, writer.DirectContent);
        }

        // write on start of each page
        public override void OnStartPage(PdfWriter writer, Document document)
        {
            base.OnStartPage(writer, document);
        }

        // write on end of each page
        public override void OnEndPage(PdfWriter writer, Document document)
        {
            base.OnEndPage(writer, document);
            PdfPTable tabFot = new PdfPTable(new float[] { 1F });
            PdfPCell cell;
            tabFot.TotalWidth = 300F;
            cell = new PdfPCell(new Phrase("Footer"));
            tabFot.AddCell(cell);
            tabFot.WriteSelectedRows(0, -1, 150, document.Bottom, writer.DirectContent);
        }

        //write on close of document
        public override void OnCloseDocument(PdfWriter writer, Document document)
        {
            base.OnCloseDocument(writer, document);
        }
    }
}