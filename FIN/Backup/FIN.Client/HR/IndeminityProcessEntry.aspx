﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="IndeminityProcessEntry.aspx.cs" Inherits="FIN.Client.HR.IndeminityProcessEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1000px" id="divMainContainer" runat="server">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="lblDepartment">
                Department
            </div>
            <div class="divtxtBox  LNOrient" style="width: 480px">
                <asp:DropDownList ID="ddlDepartment" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged" AutoPostBack="true"
                    TabIndex="1">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="lblEmployeeName">
                Employee Name
            </div>
            <div class="divtxtBox  LNOrient" style="width: 480px">
                <asp:DropDownList ID="ddlStaffName" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    TabIndex="2">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="lblAssignmentDate">
                Date
            </div>
            <div class="divtxtBox  LNOrient" style="width: 152px">
                <asp:TextBox ID="txtDate" CssClass="validate[required]  RequiredField  txtBox" runat="server"
                    TabIndex="3"></asp:TextBox><cc2:CalendarExtender ID="CalendwearExtender1" runat="server"
                        Format="dd/MM/yyyy" TargetControlID="txtDate">
                    </cc2:CalendarExtender>
                <cc2:FilteredTextBoxExtender ID="FiltereswedTextBoxExtender3" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtDate" />
            </div>
           <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 150px" id="Div1">
                <asp:ImageButton ID="btnProcess" runat="server" ImageUrl="~/Images/btnCalculate.png"
                    OnClick="btnProcess_Click" TabIndex="4" ToolTip="btnProcess" Style="border: 0px;" />
                <%--<asp:Button ID="btnProcess" runat="server" Text="Calculate" ToolTip="btnProcess"
                    CssClass="btn" TabIndex="4" OnClick="btnProcess_Click" />--%>
                &nbsp;
                <asp:ImageButton ID="btnPosting" runat="server" ImageUrl="~/Images/Post.png" OnClick="btnPosting_Click"
                    TabIndex="5" Style="border: 0px;" />
                <%--<asp:Button ID="btnPosting" TabIndex="5" runat="server" Text="Post" CssClass="btn" OnClick="btnPosting_Click" />--%>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="Div2">
                Amount
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:TextBox runat="server" Enabled="false" ID="txtPayableAmt" CssClass="validate[required]  RequiredField  txtBox_N"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="Div4">
                Bank Name
            </div>
            <div class="divtxtBox  LNOrient" style="width: 152px">
                <asp:DropDownList ID="ddlBankName" runat="server" CssClass="validate[required]  RequiredField  ddlStype"
                    AutoPostBack="True" TabIndex="6">
                </asp:DropDownList>
            </div>
           <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 150px" id="Div7">
                Cheque Date
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:TextBox ID="txtChequeDate" CssClass="EntryFont txtBox" runat="server" TabIndex="7"></asp:TextBox><cc2:CalendarExtender
                    ID="CalendarExtender4" runat="server" Format="dd/MM/yyyy" TargetControlID="txtChequeDate">
                </cc2:CalendarExtender>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtChequeDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="Div5">
                Cheque Number
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:TextBox ID="txtChequeNo" TabIndex="8" MaxLength="50" runat="server" CssClass="validate[required]  RequiredField  txtBox_N"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FielteredsdfTextBoxExtender2" runat="server" FilterType="Numbers"
                    TargetControlID="txtChequeNo" />
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 150px" id="Div9">
                Cheque Amount
            </div>
            <div class="divtxtBox  LNOrient" style="width: 154px">
                <asp:TextBox ID="txtChequeAmt" TabIndex="9" runat="server" CssClass="validate[required]  RequiredField  txtBox_N"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxsfExtender1" runat="server" ValidChars="."
                    FilterType="Numbers,Custom" TargetControlID="txtChequeAmt" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="Div6">
                Remarks
            </div>
            <div class="divtxtBox  LNOrient" style="width: 480px">
                <asp:TextBox ID="txtRemarks" TabIndex="10" runat="server" Style="height: 70px;" TextMode="MultiLine"
                    MaxLength="100" CssClass="txtBox"></asp:TextBox>
            </div>
           <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 150px" id="Div3">
                <asp:ImageButton ID="btnPayment" runat="server" ImageUrl="~/Images/btnPayment.png" OnClick="btnPayment_Click"
                    TabIndex="11" Visible="false" ToolTip="Payment" Style="border: 0px;" />
                <%--<asp:Button ID="btnPayment" runat="server" Text="Payment" Visible="false" ToolTip="Payment"
                    CssClass="btn" TabIndex="11" OnClick="btnPayment_Click" />--%>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                            Visible="false" TabIndex="12" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="13" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="14" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="15" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table>
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/HR/HRChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
