﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="IndemnityEncashmentEntry.aspx.cs" Inherits="FIN.Client.HR.IndemnityEncashmentEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1000px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblBankName">
                Date
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:TextBox ID="txtDate" CssClass="validate[required]  RequiredField txtBox" runat="server"
                    TabIndex="1" Width="144px"></asp:TextBox>
                <cc2:CalendarExtender ID="CalendarExtender8" runat="server" Format="dd/MM/yyyy" TargetControlID="txtDate"
                    OnClientDateSelectionChanged="checkDate">
                </cc2:CalendarExtender>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtDate" />
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 200px" id="lblBankShortName">
                Employee
            </div>
            <div class="divtxtBox  LNOrient" style="width: 151px">
                <asp:DropDownList ID="ddlEmployee" runat="server" TabIndex="3" CssClass="validate[required] RequiredField ddlStype"
                    AutoPostBack="True" 
                    onselectedindexchanged="ddlEmployee_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="Div6">
                Indemnity Amount
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:TextBox ID="txtIndemnityamt" CssClass="validate[required]  RequiredField txtBox_N"
                 MaxLength="15"   runat="server" TabIndex="5"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" ValidChars=".,"
                    FilterType="Numbers,Custom" TargetControlID="txtIndemnityamt" />
            </div>
           <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 200px" id="lblShortName">
                Eligible Amount
            </div>
            <div class="divtxtBox  LNOrient" style="width: 148px">
                <asp:TextBox ID="txtElegamt" CssClass="validate[required]  RequiredField txtBox_N"
                    MaxLength="15" runat="server" TabIndex="5" AutoPostBack="True" 
                    ontextchanged="txtElegamt_TextChanged"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" ValidChars=".,"
                    FilterType="Numbers,Custom" TargetControlID="txtElegamt" />
            </div>
        </div>
        <div class="divClear_10">
            <asp:HiddenField ID="hf_AvailableLeave" runat="server" />
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblNoofDays">
               Loan Amount
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                 <asp:TextBox ID="txtLoanamt" CssClass="validate[required]  RequiredField txtBox_N"
                  MaxLength="15"   runat="server" TabIndex="6"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars=".,"
                    FilterType="Numbers,Custom" TargetControlID="txtLoanamt" />
            </div>
           <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 200px" id="Div7">
                 Payment Option
            </div>
            <div class="divtxtBox  LNOrient" style="width: 153px">
                <asp:DropDownList ID="ddlPaymentOption" runat="server" TabIndex="9" CssClass="validate[required]  RequiredField ddlStype"
                    AutoPostBack="True" 
                    onselectedindexchanged="ddlPaymentOption_SelectedIndexChanged1" >
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div runat="server" id="IdPayPrd">
            <div class="lblBox LNOrient" style="width: 200px" id="Div9">
                Pay Period
            </div>
            <div class="divtxtBox  LNOrient" style="width: 523px">
                <asp:DropDownList ID="ddlPayperiod" runat="server" TabIndex="10" CssClass="validate[required]  RequiredField ddlStype"
                    AutoPostBack="True">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="Div3">
                Remarks
            </div>
            <div class="divtxtBox  LNOrient" style="width: 526px">
                <asp:TextBox ID="txtRemarks" TextMode="MultiLine" Height="50px" CssClass="txtBox"
                  MaxLength="500"   runat="server" TabIndex="11"></asp:TextBox>
            </div>
        </div>
      
        <div class="divClear_10">
        </div>
        <div class="divClear_10">
        </div>
          <asp:HiddenField ID="hfEligamt" runat="server" />
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                            TabIndex="9" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="10" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="11" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="12" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table>
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
<script src="../LanguageScript/HR/HRChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
