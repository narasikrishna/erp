﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="PassportHandOverEntry.aspx.cs" Inherits="FIN.Client.HR.PassportHandOverEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 800px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 160px" id="lblEmployee">
                Employee
            </div>
            <div class="divtxtBox  LNOrient" style="width: 494px">
                <asp:DropDownList ID="ddlEmployee" runat="server" TabIndex="1" Width="494px" AutoPostBack="True"
                    CssClass="validate[required] RequiredField ddlStype" OnSelectedIndexChanged="ddlEmployee_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <%--  <div class="lblBox" style="float: left; width: 155px" id="lblName">
             Employee Name
            </div>
             <div class="divtxtBox" style="float: left; width: 152px">
                <asp:TextBox ID="txtEmployeeName" 
                    runat="server"  TabIndex="2" CssClass="txtBox" ReadOnly="True"></asp:TextBox>
               </div>--%>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 160px" id="lblPassportName">
                Passport Name
            </div>
            <div class="divtxtBox  LNOrient" style="width: 494px">
                <asp:DropDownList ID="ddlPassportName" Width="494px" CssClass="validate[required] RequiredField ddlStype"
                    runat="server" TabIndex="2">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 160px" id="lblHandoverTo">
                Handover To
            </div>
            <div class="divtxtBox  LNOrient" style="width: 494px">
                <asp:DropDownList ID="ddlHandoverTo" runat="server" TabIndex="3" AutoPostBack="True"
                    Width="494px" CssClass="validate[required] RequiredField ddlStype">
                </asp:DropDownList>
            </div>
            <%-- <div class="lblBox"  id="lblHandOverType">
                Handover Type 
            </div>
            <div class="divtxtBox" >
                <asp:DropDownList ID="ddlHandoverType" 
                    runat="server"  TabIndex="4" AutoPostBack="True" CssClass="validate[required] RequiredField ddlStype">
                </asp:DropDownList>
            </div>--%>
            <div class="colspace  LNOrient" >
                &nbsp</div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 160px" id="lblHandoverDate">
                Handover Date
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:TextBox ID="txtHandoverDate" CssClass="validate[required, custom[ReqDateDDMMYYY]]  RequiredField  txtBox"
                    runat="server" TabIndex="4"></asp:TextBox>
                <cc2:CalendarExtender ID="CalendarExtender8" runat="server" Format="dd/MM/yyyy" TargetControlID="txtHandoverDate"
                    OnClientDateSelectionChanged="checkDate">
                </cc2:CalendarExtender>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtHandoverDate" />
            </div>
            <div class="colspace" style="" id="divReturnDate" runat="server" visible="false">
                <div class="lblBox LNOrient" style=" width: 158px" id="Div1">
                    Return Date
                </div>
                <div class="divtxtBox  LNOrient" style="float: left; width: 158px">
                    <asp:TextBox ID="txtReturnDate" CssClass="validate[, custom[ReqDateDDMMYYY]] txtBox"
                        runat="server" TabIndex="5"></asp:TextBox>
                    <cc2:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MM/yyyy" TargetControlID="txtReturnDate"
                        OnClientDateSelectionChanged="checkDate">
                    </cc2:CalendarExtender>
                    <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" ValidChars="/"
                        FilterType="Numbers,Custom" TargetControlID="txtReturnDate" />
                </div>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div id="ReturnID" runat="server">
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="width: 160px" id="Div2">
                    Return To
                </div>
                <div class="divtxtBox  LNOrient" style="width: 494px">
                    <asp:DropDownList ID="ddlReturnBy" Width="494px" runat="server" TabIndex="6" AutoPostBack="True"
                        CssClass="ddlStype">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 160px" id="lblReason">
                Reason
            </div>
            <div class="divtxtBox  LNOrient" style="width: 500px">
                <asp:TextBox ID="txtReason" runat="server" TabIndex="7" TextMode="MultiLine" Height="50px"
                    CssClass="validate[required] RequiredField txtBox"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 160px" id="lblActiveFlag">
                Active
            </div>
            <div class="divtxtBox  LNOrient" style="width: 152px">
                <asp:CheckBox ID="chKEnabledFlag" runat="server" Checked="True" TabIndex="8" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" TabIndex="9" OnClick="btnSave_Click" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="10" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Reset" CssClass="btn" TabIndex="11" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="12" />
                    </td>
                </tr>
            </table>
        </div>
        <%-- <div id="divDelete">
        <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
            PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
        </cc2:ModalPopupExtender>
        <asp:Panel ID="pnlConfirm" runat="server">
            <div class="ConfirmForm">
                <table>
                    <tr class="ConfirmHeading" style="width: 100%">
                        <td>
                            <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                Width="60px" />
                            &nbsp;
                            <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                        </td>
                    </tr>
                </table>
            </div>
        </asp:Panel>
    </div>--%>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/HR/HRChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
