﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.DAL.GL;
using FIN.BLL;
using VMVServices.Web;
using System.Web.UI.DataVisualization.Charting;
using System.Drawing;

namespace FIN.Client.HR
{
    public partial class DashBoard_HR : PageBase
    {
        
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ChartHR.Visible = false;
                FillComboBox();
            }
        }

        private void FillComboBox()
        {
            FIN.BLL.HR.Department_BLL.GetDepartmentName(ref ddlDeptName);
            FIN.BLL.HR.DepartmentDetails_BLL.fn_GetDesigName(ref ddlDesignation);
            Lookup_BLL.GetLookUpValues(ref ddlBloodGroup, "BG");
        }

        protected void btnGenerateData_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dtDataEmpDtls = new DataTable();
                dtDataEmpDtls = DBMethod.ExecuteQuery(FIN.DAL.HR.Employee_DAL.GetEmployeeDeptDetails(ddlDeptName.SelectedValue.ToString(), ddlDesignation.SelectedValue.ToString(), ddlBloodGroup.SelectedValue.ToString())).Tables[0];

                GridViewHR.DataSource = dtDataEmpDtls;
                GridViewHR.DataBind();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }


        protected void btnGenerateChart_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dtDataAttend = new DataTable();
                //dtData = DBMethod.ExecuteQuery(FIN.DAL.HR.Employee_DAL.GetEmployeeDeptDetails(ddlDeptName.SelectedValue.ToString(), ddlDesignation.SelectedValue.ToString(), ddlBloodGroup.SelectedValue.ToString())).Tables[0];
                dtDataAttend = DBMethod.ExecuteQuery(FIN.DAL.HR.InterviewResult_DAL.getInterviewAttended(txtFromDate.Text.ToString(), txtToDate.Text.ToString())).Tables[0];

                DataTable dtDataSelect = new DataTable();
                dtDataSelect = DBMethod.ExecuteQuery(FIN.DAL.HR.InterviewResult_DAL.getInterviewSelected(txtFromDate.Text.ToString(), txtToDate.Text.ToString())).Tables[0];

                DataTable dtDataReject = new DataTable();
                dtDataReject = DBMethod.ExecuteQuery(FIN.DAL.HR.InterviewResult_DAL.getInterviewRejected(txtFromDate.Text.ToString(), txtToDate.Text.ToString())).Tables[0];


                ChartHR.Series["HRInterviewAttendedSeries"].ChartType = SeriesChartType.Column;
                ChartHR.Series["HRInterviewSelectedSeries"].ChartType = SeriesChartType.Column;
                ChartHR.Series["HRResignationSeries"].ChartType = SeriesChartType.Column;

                ChartHR.Series["HRInterviewAttendedSeries"]["DrawingStyle"] = "Default";
                ChartHR.Series["HRInterviewSelectedSeries"]["DrawingStyle"] = "Default";
                ChartHR.Series["HRResignationSeries"]["DrawingStyle"] = "Default";

                ChartHR.ChartAreas["ChartAreaHR"].Area3DStyle.Enable3D = false;
                ChartHR.Series["HRInterviewAttendedSeries"].IsValueShownAsLabel = false;
                ChartHR.Series["HRInterviewSelectedSeries"].IsValueShownAsLabel = false;
                ChartHR.Series["HRResignationSeries"].IsValueShownAsLabel = false;

                ChartHR.Series["HRInterviewAttendedSeries"].BorderWidth = 2;
                ChartHR.Series["HRInterviewSelectedSeries"].BorderWidth = 2;
                ChartHR.Series["HRResignationSeries"].BorderWidth = 2;

                //ChartHR.Series["Series1"].IsXValueIndexed = true;
                //ChartHR.Series["HRInterviewSelectedSeries"].IsXValueIndexed = true;

                ChartHR.Series["HRInterviewAttendedSeries"]["StackedGroupName"] = "Group1";
                ChartHR.Series["HRInterviewSelectedSeries"]["StackedGroupName"] = "Group1";
                ChartHR.Series["HRResignationSeries"]["StackedGroupName"] = "Group1";

                DataTable dtData = new DataTable();
                dtData.Columns.Add("Sno");
                dtData.Columns.Add("numberOfAttend");
                dtData.Columns.Add("numberOfSelect");
                dtData.Columns.Add("numberOfReject");

                DataRow drData;
                //0th Row
                drData = dtData.NewRow();
                drData["Sno"] = "0";
                drData["numberOfAttend"] = "0";
                drData["numberOfSelect"] = "0";
                drData["numberOfReject"] = "0";

                for (int iLoop = 0; iLoop < dtDataAttend.Rows.Count; iLoop++)
                {
                    drData = dtData.NewRow();
                    drData["Sno"] = iLoop + 1;
                    drData["numberOfAttend"] = dtDataAttend.Rows[iLoop]["numberOfAttended"].ToString();
                    drData["numberOfSelect"] = dtDataSelect.Rows[iLoop]["numberOfSelect"].ToString();
                    drData["numberOfReject"] = dtDataReject.Rows[iLoop]["numberOfReject"].ToString();
                
                    dtData.Rows.Add(drData);
                }

                ChartHR.DataSource = dtData;
                ChartHR.DataBind();

                ChartHR.Series["HRInterviewAttendedSeries"].XValueMember = "Sno";
                ChartHR.Series["HRInterviewAttendedSeries"].YValueMembers = "numberOfAttend";

                ChartHR.Series["HRInterviewSelectedSeries"].XValueMember = "Sno";
                ChartHR.Series["HRInterviewSelectedSeries"].YValueMembers = "numberOfSelect";

                ChartHR.Series["HRResignationSeries"].XValueMember = "Sno";
                ChartHR.Series["HRResignationSeries"].YValueMembers = "numberOfReject";

                ChartHR.Series["HRInterviewAttendedSeries"].Color = System.Drawing.Color.DarkGoldenrod;
                ChartHR.Series["HRInterviewAttendedSeries"].Legend = "Default";
                ChartHR.Series["HRInterviewAttendedSeries"].LegendText = "Inteview Attended";

                ChartHR.Series["HRInterviewSelectedSeries"].Color = System.Drawing.Color.DarkOrange;
                ChartHR.Series["HRInterviewSelectedSeries"].Legend = "Default";
                ChartHR.Series["HRInterviewSelectedSeries"].LegendText = "Inteview Selected";

                ChartHR.Series["HRResignationSeries"].Color = System.Drawing.Color.DarkKhaki;
                ChartHR.Series["HRResignationSeries"].Legend = "Default";
                ChartHR.Series["HRResignationSeries"].LegendText = "Interview Rejected";


                ChartHR.Legends["Default"].BackColor = Color.Transparent;
                ChartHR.Legends["Default"].LegendStyle = LegendStyle.Column;
                ChartHR.Legends["Default"].ForeColor = Color.Black;
                ChartHR.Legends["Default"].Docking = Docking.Bottom;


                ChartHR.Legends["Default"].BorderColor = Color.Transparent;
                ChartHR.Legends["Default"].BackSecondaryColor = Color.Transparent;
                ChartHR.Legends["Default"].BackGradientStyle = GradientStyle.None;
                ChartHR.Legends["Default"].BorderColor = Color.Black;
                ChartHR.Legends["Default"].BorderWidth = 1;
                ChartHR.Legends["Default"].BorderDashStyle = ChartDashStyle.Solid;
                ChartHR.Legends["Default"].ShadowOffset = 1;

                //ChartHR.Series["Series1"]["PixelPointWidth"] = "50";
                //ChartHR.Series["Series2"]["PixelPointWidth"] = "50";


                ChartHR.BackColor = Color.Transparent;
                ChartHR.ChartAreas["ChartAreaHR"].AxisY = new System.Web.UI.DataVisualization.Charting.Axis { LabelStyle = new LabelStyle() { Font = new Font("Verdana", 8.0f, FontStyle.Regular) } };
                //ChartHR.ChartAreas["ChartAreaHR"].AxisY = new System.Web.UI.DataVisualization.Charting.Axis { LabelStyle = new LabelStyle() { ForeColor = Color.Blue } };

                ChartHR.ChartAreas["ChartAreaHR"].AxisX = new System.Web.UI.DataVisualization.Charting.Axis { LabelStyle = new LabelStyle() { Font = new Font("Verdana", 8.0f, FontStyle.Regular) } };
                //ChartHR.ChartAreas["ChartAreaHR"].AxisX = new System.Web.UI.DataVisualization.Charting.Axis { LabelStyle = new LabelStyle() { ForeColor = Color.Blue } };



                ChartHR.ChartAreas["ChartAreaHR"].Area3DStyle.Enable3D = false;
                ChartHR.ChartAreas["ChartAreaHR"].BackColor = Color.Transparent;
                ChartHR.ChartAreas["ChartAreaHR"].AxisX.MajorGrid.Enabled = false;
                ChartHR.ChartAreas["ChartAreaHR"].AxisY.MajorGrid.Enabled = true;
                ChartHR.ChartAreas["ChartAreaHR"].Position.Width = 100;
                ChartHR.ChartAreas["ChartAreaHR"].Position.Height = 100;


                ChartHR.ChartAreas["ChartAreaHR"].AxisX.IsMarginVisible = true;
                ChartHR.ChartAreas["ChartAreaHR"].Position.X = 6;
                ChartHR.ChartAreas["ChartAreaHR"].AxisX.Title = ".\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n.";


                ChartHR.ChartAreas["ChartAreaHR"].AxisY.IsStartedFromZero = true;
                ChartHR.ChartAreas["ChartAreaHR"].AxisY.Title = "Number of Candidates";
                ChartHR.ChartAreas["ChartAreaHR"].AxisX.Title = "Interview Status";
                ChartHR.ChartAreas["ChartAreaHR"].AxisY.LabelStyle.Format = "#,##,##0.#0";// "{900:C}";"#,##0;-#,##0;0"

                ChartHR.ChartAreas["ChartAreaHR"].AxisY.TitleFont = new Font("Verdana", 12, FontStyle.Regular);
                //ChartHR.ChartAreas["ChartAreaHR"].AxisY.TitleForeColor = Color.Blue;

                ChartHR.ChartAreas["ChartAreaHR"].AxisX.TitleFont = new Font("Verdana", 12, FontStyle.Regular);
                //ChartHR.ChartAreas["ChartAreaHR"].AxisX.TitleForeColor = Color.Blue;


                ChartHR.ChartAreas["ChartAreaHR"].AxisX.LabelStyle.Angle = -90;


            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerPath + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {

        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {

        }

    }
}