﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.BLL;
using FIN.BLL.HR;
using VMVServices.Web;

namespace FIN.Client.HR
{
    public partial class JobsEntry : PageBase
    {

        HR_JOBS hR_JOBS = new HR_JOBS();
        DataTable dtGridData = new DataTable();
        Jobs_BLL Jobs_BLL = new Jobs_BLL();
        Grades_BLL Grades_BLL = new Grades_BLL();

        string ProReturn = null;
        Boolean bol_rowVisiable;
        Boolean savedBool;
        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("JOBS_PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));


            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
        }

        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>
        /// 

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void FillComboBox()
        {
            Categories_BLL.fn_getCategory(ref ddlCategory, Master.Mode);


        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                //   txtGradeName.Enabled = false;
                FillComboBox();

                EntityData = null;

                dtGridData = FIN.BLL.HR.Jobs_BLL.getChildEntityDet(Master.StrRecordId);
                BindGrid(dtGridData);

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    using (IRepository<HR_JOBS> userCtx = new DataRepository<HR_JOBS>())
                    {
                        hR_JOBS = userCtx.Find(r =>
                            (r.JOB_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    ddlCategory.SelectedValue = hR_JOBS.CATEGORY_ID;
                    fillJobdtls();
                    // as per customer requirement job under category instead of grade


                    //   HR_GRADES  hR_GRADES = new HR_GRADES();
                    //using (IRepository<HR_GRADES> userCtx = new DataRepository<HR_GRADES>())
                    //{
                    //    hR_GRADES = userCtx.Find(r =>
                    //        (r.GRADE_ID == hR_JOBS.GRADE_ID.ToString())
                    //        ).SingleOrDefault();
                    //}
                    //EntityData = hR_JOBS;
                    //ddlCategory.SelectedValue = hR_GRADES.CATEGORY_ID;
                    //Fn_GradeName();
                    //ddlGradeCode.SelectedValue = hR_JOBS.GRADE_ID;
                    //Fillgradedesc();

                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("JOBS_ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                var tmpChildEntity = new List<Tuple<object, string>>();

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    hR_JOBS = new HR_JOBS();
                    if (dtGridData.Rows[iLoop]["JOB_ID"].ToString() != "0")
                    {
                        using (IRepository<HR_JOBS> userCtx = new DataRepository<HR_JOBS>())
                        {
                            hR_JOBS = userCtx.Find(r =>
                                (r.JOB_ID == dtGridData.Rows[iLoop]["JOB_ID"].ToString())
                                ).SingleOrDefault();
                        }
                    }
                    hR_JOBS.JOB_CODE = dtGridData.Rows[iLoop]["JOB_CODE"].ToString();
                    hR_JOBS.JOB_DESC = dtGridData.Rows[iLoop]["JOB_DESC"].ToString();

                    hR_JOBS.JOB_CODE_OL = dtGridData.Rows[iLoop]["JOB_CODE_OL"].ToString();
                    hR_JOBS.JOB_DESC_OL = dtGridData.Rows[iLoop]["JOB_DESC_OL"].ToString();

                    hR_JOBS.CATEGORY_ID = ddlCategory.SelectedValue;
                    //  as per customer requirement job under category instead of grade
                    //hR_JOBS.GRADE_ID = ddlGradeCode.SelectedValue;
                    hR_JOBS.JOB_ORG_ID = VMVServices.Web.Utils.OrganizationID;

                    if (dtGridData.Rows[iLoop]["EFFECTIVE_FROM_DATE"] != DBNull.Value)
                    {
                        hR_JOBS.EFFECTIVE_FROM_DATE = DateTime.Parse(dtGridData.Rows[iLoop]["EFFECTIVE_FROM_DATE"].ToString());
                    }

                    if (dtGridData.Rows[iLoop]["EFFECTIVE_TO_DATE"] != DBNull.Value)
                    {
                        hR_JOBS.EFFECTIVE_TO_DATE = DateTime.Parse(dtGridData.Rows[iLoop]["EFFECTIVE_TO_DATE"].ToString());
                    }
                    else
                    {
                        hR_JOBS.EFFECTIVE_TO_DATE = null;
                    }

                    if (dtGridData.Rows[iLoop][FINColumnConstants.ENABLED_FLAG].ToString().ToUpper() == "TRUE")
                    {
                        hR_JOBS.ENABLED_FLAG = FINAppConstants.Y;
                    }
                    else
                    {
                        hR_JOBS.ENABLED_FLAG = FINAppConstants.N;
                    }




                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {

                        tmpChildEntity.Add(new Tuple<object, string>(hR_JOBS, "D"));
                    }
                    else
                    {





                        //ProReturn = FIN.DAL.HR.Jobs_DAL.GetSPFOR_ERR_MGR_JOBS(hR_JOBS.JOB_CODE, hR_JOBS.JOB_ID);

                        //if (ProReturn != string.Empty)
                        //{
                        //    if (ProReturn != "0")
                        //    {
                        //        ErrorCollection.Add("JOB", ProReturn);
                        //        if (ErrorCollection.Count > 0)
                        //        {
                        //            return;
                        //        }
                        //    }
                        //}



                        if (dtGridData.Rows[iLoop]["JOB_ID"].ToString() != "0")
                        {
                            hR_JOBS.JOB_ID = dtGridData.Rows[iLoop][FINColumnConstants.JOB_ID].ToString();
                            hR_JOBS.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_JOBS.JOB_ID);

                            hR_JOBS.MODIFIED_BY = this.LoggedUserName;
                            hR_JOBS.MODIFIED_DATE = DateTime.Today;
                            DBMethod.SaveEntity<HR_JOBS>(hR_JOBS, true);
                            //tmpChildEntity.Add(new Tuple<object, string>(hR_CATEGORIES, "U"));

                        }
                        else
                        {

                            hR_JOBS.JOB_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_003.ToString(), false, true);
                            hR_JOBS.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_JOBS.JOB_ID);

                            hR_JOBS.PK_ID = DBMethod.GetPrimaryKeyValue(FINSequenceConstant.HR_JOBS_SEQ);
                            hR_JOBS.CREATED_BY = this.LoggedUserName;
                            hR_JOBS.CREATED_DATE = DateTime.Today;
                            DBMethod.SaveEntity<HR_JOBS>(hR_JOBS);
                            // tmpChildEntity.Add(new Tuple<object, string>(hR_CATEGORIES, "A"));
                        }
                    }

                }
                //VMVServices.Web.Utils.SavedRecordId = "";

                //switch (Master.Mode)
                //{
                //    case FINAppConstants.Add:
                //        {
                //            FIN.BLL.GL.AccountCodes_BLL.SavePCEntity<INV_TRANSFER_HDR, INV_TRANSFER_DTL>(iNV_TRANSFER_HDR, tmpChildEntity, iNV_TRANSFER_DTL);
                //            break;
                //        }
                //    case FINAppConstants.Update:
                //        {

                //            FIN.BLL.GL.AccountCodes_BLL.SavePCEntity<INV_TRANSFER_HDR, INV_TRANSFER_DTL>(iNV_TRANSFER_HDR, tmpChildEntity, iNV_TRANSFER_DTL, true);
                //            break;

                //        }
                //}



            }
            catch (Exception ex)
            {
                ErrorCollection.Add("JOBS_ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                //ErrorCollection.Clear();

                //DropDownList ddlLotNo = tmpgvr.FindControl("ddlLotNo") as DropDownList;
                //WarehouseTransfer_BLL.fn_getLotNo(ref ddlLotNo);


                //if (gvData.EditIndex >= 0)
                //{
                //    ddlLotNo.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.LOT_ID].ToString();


                //}

                //CheckBox chkact = tmpgvr.FindControl("chkact") as CheckBox;
                //DataTable dtchilcat = new DataTable();
                //for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                //{
                //    hR_JOBS.JOB_ID = dtGridData.Rows[iLoop][FINColumnConstants.JOB_ID].ToString();
                //}
                //dtchilcat = DBMethod.ExecuteQuery(FIN.DAL.HR.Jobs_DAL.Get_Childdataof_Job(hR_JOBS.JOB_ID)).Tables[0];
                //if (dtchilcat.Rows.Count > 0)
                //{
                //    chkact.Enabled = false;
                //}

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("JOBS_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session[FINSessionConstants.GridData], "Jobs ");
                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                AssignToBE();

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        #endregion

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr["ENABLED_FLAG"] = "FALSE";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("JOBS_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            try
            {
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = -1;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("JOBS_ROW_CNCL_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion

        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("JOBS_ROW_CMD", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {


            System.Collections.SortedList slControls = new System.Collections.SortedList();

            TextBox txtcode = gvr.FindControl("txtcode") as TextBox;
            TextBox txtdesc = gvr.FindControl("txtdesc") as TextBox;
            TextBox txtcode_AR = gvr.FindControl("txtcode_AR") as TextBox;
            TextBox txtdesc_AR = gvr.FindControl("txtdesc_AR") as TextBox;
            TextBox dtpStartDate = gvr.FindControl("dtpStartDate") as TextBox;
            TextBox dtpEndDate = gvr.FindControl("dtpEndDate") as TextBox;
            CheckBox chkact = gvr.FindControl("chkact") as CheckBox;


            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList["JOB_ID"] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }

            slControls[0] = txtcode;
            slControls[1] = txtdesc;
            slControls[2] = dtpStartDate;
            slControls[3] = dtpStartDate;
            slControls[4] = dtpEndDate;
            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));


            ErrorCollection.Clear();
            string strCtrlTypes = "TextBox~TextBox~TextBox~DateTime~DateRangeValidate";
            string strMessage = Prop_File_Data["Job_Code_P"] + " ~ " + Prop_File_Data["Description_P"] + " ~ " + Prop_File_Data["Effective_Date_P"] + " ~ " + Prop_File_Data["Effective_Date_P"] + " ~ " + Prop_File_Data["End_Date_P"] + "";
            // string strMessage = "Job Code ~ Description ~ Start Date ~ Start Date ~ End Date";
            ErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);
            if (ErrorCollection.Count > 0)
                return drList;


            string strCondition = "JOB_CODE='" + txtcode.Text.Trim().ToUpper() + "'";
            strMessage = FINMessageConstatns.JobCode;
            ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            if (ErrorCollection.Count > 0)
            {

                return drList;
            }
            //Duplicate Validation Through Backend Package PKG_VALIDATIONS

            ProReturn = FINSP.GetSPFOR_ERR_MGR_CATEGORY_TWO(Master.FormCode, drList["JOB_ID"].ToString(), ddlCategory.SelectedValue, txtcode.Text);
            if (ProReturn != string.Empty)
            {
                if (ProReturn != "0")
                {
                    ErrorCollection.Add("GRADE", ProReturn);
                    if (ErrorCollection.Count > 0)
                    {
                        return drList;
                    }
                }
            }
            DataTable dtchildJob = new DataTable();
            dtchildJob = null;

            if (gvData.EditIndex >= 0)
            {
                hR_JOBS.JOB_ID = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.JOB_ID].ToString();

                dtchildJob = null;
                dtchildJob = DBMethod.ExecuteQuery(FIN.DAL.HR.Jobs_DAL.Get_Childdataof_Job(hR_JOBS.JOB_ID)).Tables[0];
                if (dtchildJob.Rows.Count > 0)
                {
                    if (chkact.Checked)
                    {


                    }
                    else
                    {
                        ErrorCollection.Add("chkflg", "Child record found please check the active flag");
                        return drList;
                    }
                }
            }


            drList["JOB_CODE"] = txtcode.Text;
            drList["JOB_DESC"] = txtdesc.Text;

            drList["JOB_CODE_OL"] = txtcode_AR.Text;
            drList["JOB_DESC_OL"] = txtdesc_AR.Text;

            if (dtpStartDate.Text.ToString().Length > 0)
            {
                drList["EFFECTIVE_FROM_DATE"] = DBMethod.ConvertStringToDate(dtpStartDate.Text.ToString());
            }

            if (dtpEndDate.Text.ToString().Length > 0)
            {

                drList["EFFECTIVE_TO_DATE"] = DBMethod.ConvertStringToDate(dtpEndDate.Text.ToString());
            }
            else
            {
                drList["EFFECTIVE_TO_DATE"] = DBNull.Value;
            }
            if (chkact.Checked)
            {
                drList[FINColumnConstants.ENABLED_FLAG] = "TRUE";
            }
            else
            {
                drList[FINColumnConstants.ENABLED_FLAG] = "FALSE";
            }



            drList[FINColumnConstants.DELETED] = FINAppConstants.N;


            return drList;



        }



        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("JOBS_ROW_UP", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("JOBS_ROW_DEL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("JOBS_ROW_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("JOBS_RowCreated", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }
                    if (e.Row.RowType == DataControlRowType.DataRow)
                    {
                        e.Row.Cells[2].HorizontalAlign = HorizontalAlign.Right;
                        e.Row.Cells[3].HorizontalAlign = HorizontalAlign.Right;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("JOBS_RDB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {

                    hR_JOBS.PK_ID = Convert.ToInt32(dtGridData.Rows[iLoop]["PK_ID"].ToString());
                    DBMethod.DeleteEntity<HR_JOBS>(hR_JOBS);
                }


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("JOBS_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        protected void ddlGradeCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            Fillgradedesc();
            fillJobdtls();
        }

        private void Fillgradedesc()
        {
            //DataTable dtgradename = new DataTable();
            //dtgradename = DBMethod.ExecuteQuery(FIN.DAL.HR.Jobs_DAL.GetGradename(ddlGradeCode.SelectedValue)).Tables[0];

            //if (dtgradename != null)
            //{
            //    if (dtgradename.Rows.Count > 0)
            //    {
            //        txtGradeName.Text = dtgradename.Rows[0]["GRADE_DESCRIPTION"].ToString();
            //    }
            //}
        }


        private void fillJobdtls()
        {
            try
            {
                Dictionary<string, string> Prop_File_Data;
                Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));
                dtGridData = DBMethod.ExecuteQuery(FIN.DAL.HR.Jobs_DAL.Getjobdtls4Category(ddlCategory.SelectedValue)).Tables[0];
                if (dtGridData.Rows.Count > 0)
                {
                    BindGrid(dtGridData);
                    //ddlCategory.SelectedIndex = 0;
                    //ErrorCollection.Add("Recordfound",  Prop_File_Data["Record_already_Exist_P"]);

                    //return;
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("JR_Validation", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
            //dtGridData.Columns.Add(FINColumnConstants.DELETED);

            //BindGrid(dtGridData);

        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {

        }

        protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Fn_GradeName();
            fillJobdtls();
        }
        private void Fn_GradeName()
        {
            //Jobs_BLL.fn_GetGrade(ref ddlGradeCode, ddlCategory.SelectedValue, Master.Mode);
        }
    }
}