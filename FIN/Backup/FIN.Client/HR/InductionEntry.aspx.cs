﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.BLL;
using FIN.DAL.HR;
using FIN.BLL.HR;
using VMVServices.Web;

namespace FIN.Client.HR
{
    public partial class InductionEntry : PageBase
    {

        HR_INDUCTION_HDR hR_INDUCTION_HDR = new HR_INDUCTION_HDR();
        HR_INDUCTION_DTL hR_INDUCTION_DTL = new HR_INDUCTION_DTL();
        DataTable dtGridData = new DataTable();
        Department_BLL Department_BLL = new Department_BLL();
        Boolean bol_rowVisiable;
        Boolean savedBool;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }


        }

        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //  div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                //pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                //pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }


            UserRightsChecking();

        }
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        /// <summary>
        /// To assign the values to Controls and fetch the StrRecordId,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();
                EntityData = null;

                dtGridData = DBMethod.ExecuteQuery(FIN.DAL.HR.Induction_DAL.GetInductionDetails(Master.StrRecordId)).Tables[0];

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    using (IRepository<HR_INDUCTION_HDR> userCtx = new DataRepository<HR_INDUCTION_HDR>())
                    {
                        hR_INDUCTION_HDR = userCtx.Find(r =>
                            (r.HR_IND_HDR_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = hR_INDUCTION_HDR;
                    txtInductionID.Text = hR_INDUCTION_HDR.HR_IND_HDR_ID;
                    if (hR_INDUCTION_HDR.HR_IND_DATE != null)
                    {
                        txtDOB.Text = DBMethod.ConvertDateToString(hR_INDUCTION_HDR.HR_IND_DATE.ToString());
                    }
                    ddlInductedBy.SelectedValue = hR_INDUCTION_HDR.HR_IND_EMP_ID.ToString();
                    ddlDeprtment.SelectedValue = hR_INDUCTION_HDR.HR_IND_DEPT_ID;
                    LoadDesignation();
                    ddlDesignation.SelectedValue = hR_INDUCTION_HDR.HR_IND_DESIG_ID;
                    ddlApplicantID.SelectedValue = hR_INDUCTION_HDR.APP_ID;
                    hR_INDUCTION_HDR.ENABLED_FLAG = FINAppConstants.EnabledFlag;

                }

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Induction Entry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Course master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    hR_INDUCTION_HDR = (HR_INDUCTION_HDR)EntityData;
                }

                hR_INDUCTION_HDR.HR_IND_HDR_ID = txtInductionID.Text;
                if (txtDOB.Text != string.Empty)
                {
                    hR_INDUCTION_HDR.HR_IND_DATE = DBMethod.ConvertStringToDate(txtDOB.Text.ToString());
                }
                hR_INDUCTION_HDR.HR_IND_EMP_ID = ddlInductedBy.SelectedValue.ToString();
                hR_INDUCTION_HDR.HR_IND_DEPT_ID = ddlDeprtment.SelectedValue.ToString();
                hR_INDUCTION_HDR.HR_IND_DESIG_ID = ddlDesignation.SelectedValue.ToString();
                hR_INDUCTION_HDR.APP_ID = ddlApplicantID.SelectedValue.ToString();
                hR_INDUCTION_HDR.ENABLED_FLAG = FINAppConstants.Y;
                hR_INDUCTION_HDR.HR_IND_ORG_ID = VMVServices.Web.Utils.OrganizationID;
                //  hR_PER_APP_REVIEW_ASGNMNT_HDR.WORKFLOW_COMPLETION_STATUS = FINAppConstants.Y;


                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != string.Empty)
                {
                    hR_INDUCTION_HDR.MODIFIED_BY = this.LoggedUserName;
                    hR_INDUCTION_HDR.MODIFIED_DATE = DateTime.Today;

                }
                else
                {

                    hR_INDUCTION_HDR.CREATED_BY = this.LoggedUserName;
                    hR_INDUCTION_HDR.CREATED_DATE = DateTime.Today;
                    hR_INDUCTION_HDR.HR_IND_HDR_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_048.ToString(), false, true);

                }

                hR_INDUCTION_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_INDUCTION_HDR.HR_IND_HDR_ID);
                //Save Detail Table


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }


                var tmpChildEntity = new List<Tuple<object, string>>();


                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    hR_INDUCTION_DTL = new HR_INDUCTION_DTL();
                    if (dtGridData.Rows[iLoop][FINColumnConstants.HR_IND_DTL_ID].ToString() != "0")
                    {
                        using (IRepository<HR_INDUCTION_DTL> userCtx = new DataRepository<HR_INDUCTION_DTL>())
                        {
                            hR_INDUCTION_DTL = userCtx.Find(r =>
                                (r.HR_IND_DTL_ID == dtGridData.Rows[iLoop][FINColumnConstants.HR_IND_DTL_ID].ToString())
                                ).SingleOrDefault();
                        }
                    }

                    if (dtGridData.Rows[iLoop][FINColumnConstants.HR_IND_DTL_ID].ToString() != "0")
                    {
                        FIN.BLL.HR.Induction_BLL.getClassEntity(dtGridData.Rows[iLoop][FINColumnConstants.HR_IND_DTL_ID].ToString());
                    }


                    hR_INDUCTION_DTL.ACTIVITY_CARRIED = dtGridData.Rows[iLoop]["HR_IND_ACT_ID"].ToString();
                    hR_INDUCTION_DTL.REMARKS = dtGridData.Rows[iLoop]["REMARKS"].ToString();
                    hR_INDUCTION_DTL.STATUS = dtGridData.Rows[iLoop]["STATUS"].ToString();

                    hR_INDUCTION_DTL.ENABLED_FLAG = FINAppConstants.Y;

                    hR_INDUCTION_DTL.WORKFLOW_COMPLETION_STATUS = "1";
                    hR_INDUCTION_DTL.HR_IND_HDR_ID = hR_INDUCTION_HDR.HR_IND_HDR_ID;

                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        hR_INDUCTION_DTL.HR_IND_DTL_ID = dtGridData.Rows[iLoop][FINColumnConstants.HR_IND_DTL_ID].ToString();
                        tmpChildEntity.Add(new Tuple<object, string>(hR_INDUCTION_DTL, FINAppConstants.Delete));
                    }
                    else
                    {

                        if (dtGridData.Rows[iLoop][FINColumnConstants.HR_IND_DTL_ID].ToString() != "0" && dtGridData.Rows[iLoop][FINColumnConstants.HR_IND_DTL_ID].ToString() != string.Empty)
                        {
                            hR_INDUCTION_DTL.HR_IND_DTL_ID = dtGridData.Rows[iLoop][FINColumnConstants.HR_IND_DTL_ID].ToString();
                            hR_INDUCTION_DTL.MODIFIED_BY = this.LoggedUserName;
                            hR_INDUCTION_DTL.MODIFIED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(hR_INDUCTION_DTL, FINAppConstants.Update));

                        }
                        else
                        {
                            hR_INDUCTION_DTL.HR_IND_DTL_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_048_D.ToString(), false, true);
                            hR_INDUCTION_DTL.CREATED_BY = this.LoggedUserName;
                            hR_INDUCTION_DTL.CREATED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(hR_INDUCTION_DTL, FINAppConstants.Add));
                        }
                    }

                }
                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            CommonUtils.SavePCEntity<HR_INDUCTION_HDR, HR_INDUCTION_DTL>(hR_INDUCTION_HDR, tmpChildEntity, hR_INDUCTION_DTL);
                            savedBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {
                            CommonUtils.SavePCEntity<HR_INDUCTION_HDR, HR_INDUCTION_DTL>(hR_INDUCTION_HDR, tmpChildEntity, hR_INDUCTION_DTL, true);
                            savedBool = true;
                            break;
                        }
                }
                if (ErrorCollection.Count > 0)
                {
                    return;
                }


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_ATB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }


        private void FillComboBox()
        {

            Department_BLL.fn_GetEmployeeDetails(ref ddlInductedBy);
            Department_BLL.GetDepartmentName(ref ddlDeprtment);
            // Department_BLL.GetDesignation(ref ddlDesignation);
            //Induction_BLL.fn_GetApplicantDetails(ref ddlApplicantID);
            Department_BLL.fn_GetEmployeeDetails(ref ddlApplicantID);

        }

        /// <summary>
        /// Bind the records into grid voew
        /// </summary>
        /// <param name="dtData">Contains the database entities and correspoding records which is used in the grid view</param>

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    //dr["ENABLED_FLAG"] = "FALSE";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WRI_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>


        #endregion

        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>

        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = -1;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Row_cnl", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }

                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    else
                    {
                        dtGridData.Rows.Add(drList);
                        BindGrid(dtGridData);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Row_Cmd", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }



        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {

            System.Collections.SortedList slControls = new System.Collections.SortedList();
            TextBox txtRemarks = gvr.FindControl("txtRemarks") as TextBox;
            DropDownList ddlApplicantName = gvr.FindControl("ddlApplicantName") as DropDownList;
            DropDownList ddlstatus = gvr.FindControl("ddlstatus") as DropDownList;
            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList[FINColumnConstants.HR_IND_DTL_ID] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }

            slControls[0] = ddlApplicantName;
            slControls[1] = ddlstatus;
            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));

            ErrorCollection.Clear();
            string strCtrlTypes = FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST;
            string strMessage = "Activity Carried~Status";
            //string strMessage = " Applicant Name ";
            ErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);
            if (ErrorCollection.Count > 0)
                return drList;


            //DateTime PSGDate = DBMethod.ConvertStringToDate(dtpPeriodStartDateGrid.Text);
            //if (dtpPeriodEndDateGrid.Text.ToString().Length > 0)
            //{
            //    DateTime PEGDate = DBMethod.ConvertStringToDate(dtpPeriodEndDateGrid.Text);

            //    if ((PSGDate - PEGDate).TotalDays > 0)
            //    {
            //        ErrorCollection.Add("FromTODate", "From Date Must Be Greater then To Date");
            //        return drList;
            //    }

            //}

            //string strCondition = "HR_IND_EMP_ID='" + ddlApplicantID.Text.Trim().ToUpper() + "'";
            //strMessage = FINMessageConstatns.RecordAlreadyExists;
            //ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            //if (ErrorCollection.Count > 0)
            //{

            //    return drList;
            //}


            drList["HR_IND_ACT_ID"] = ddlApplicantName.SelectedValue;
            drList["HR_ACTIVITY_DESC"] = ddlApplicantName.SelectedItem.Text;
            drList["REMARKS"] = txtRemarks.Text;

            drList["STATUS"] = ddlstatus.SelectedValue;
            drList["DESCRIPTION"] = ddlstatus.SelectedItem.Text;
            drList[FINColumnConstants.DELETED] = FINAppConstants.N;

            return drList;
        }

        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                //else if (FacilityMasterBLL.ErrorCollection.Count > 0)
                //{
                //    ErrorCollection = FacilityMasterBLL.ErrorCollection;
                //    return;
                //}
                else
                {
                    gvData.EditIndex = -1;
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Row_upd", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row delete mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewDeleteEventArgs indicates which row's delete button was clicked. </param>


        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                dtGridData.Rows[e.RowIndex][FINColumnConstants.DELETED] = FINAppConstants.Y;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Row_del", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Row_Edt", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("InductionDefineKRA", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }


        /// <summary>
        /// Used to delete the exam master and detail table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {


                    if (dtGridData.Rows[iLoop][FINColumnConstants.HR_IND_DTL_ID].ToString() != "0")
                    {
                        using (IRepository<HR_INDUCTION_DTL> userCtx = new DataRepository<HR_INDUCTION_DTL>())
                        {
                            hR_INDUCTION_DTL = userCtx.Find(r =>
                                 (r.HR_IND_DTL_ID == dtGridData.Rows[iLoop][FINColumnConstants.HR_IND_DTL_ID].ToString())
                                 ).SingleOrDefault();
                        }
                    }


                    hR_INDUCTION_DTL.HR_IND_DTL_ID = dtGridData.Rows[iLoop]["HR_IND_DTL_ID"].ToString();
                    DBMethod.DeleteEntity<HR_INDUCTION_DTL>(hR_INDUCTION_DTL);
                }

                hR_INDUCTION_DTL.HR_IND_DTL_ID = Master.StrRecordId.ToString();
                DBMethod.DeleteEntity<HR_INDUCTION_DTL>(hR_INDUCTION_DTL);
                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATADELETED);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Btn_ys_clik", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }

        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddlApplicantName = tmpgvr.FindControl("ddlApplicantName") as DropDownList;
                DropDownList ddlstatus = tmpgvr.FindControl("ddlstatus") as DropDownList;
                Induction_BLL.fn_getActivityName(ref ddlApplicantName);
                //  Induction_BLL.fn_GetApplicantDetails(ref ddl_Applicant);
                Lookup_BLL.GetLookUpValues(ref ddlstatus, "STS");

                if (gvData.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                {
                    ddlApplicantName.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["HR_IND_ACT_ID"].ToString();
                    ddlstatus.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["STATUS"].ToString();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (bol_rowVisiable)
                    e.Row.Visible = false;

                if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                {
                    e.Row.Visible = false;
                }

            }
        }

        #endregion

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                AssignToBE();
                ErrorCollection = CommonUtils.IsEmptyGrid(dtGridData, "Induction");
                if (ErrorCollection.Count > 0)
                {
                    return;
                }


                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                }

                //switch (Master.Mode)
                //{
                //    case FINAppConstants.Add:
                //        {
                //            DBMethod.SaveEntity<GL_COMPANIES_HDR>(gL_COMPANIES_HDR);
                //            break;
                //        }
                //    case FINAppConstants.Update:
                //        {

                //            DBMethod.SaveEntity<GL_COMPANIES_HDR>(gL_COMPANIES_HDR, true);
                //            break;
                //        }
            }


            catch (Exception ex)
            {
                ErrorCollection.Add("Org_btn_sve", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }



        protected void ddlApplicantID_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            fillApplicantDetails(gvr);

        }

        private void fillApplicantDetails(GridViewRow gvr)
        {

            DropDownList ddlApplicantID = gvr.FindControl("ddlApplicantID") as DropDownList;
            //DropDownList ddlApplicantName = gvr.FindControl("ddlApplicantName") as DropDownList;
            TextBox txtApplicantName = gvr.FindControl("txtApplicantName") as TextBox;

            DataTable dtApplicantNmae = new DataTable();
            dtApplicantNmae = DBMethod.ExecuteQuery(Induction_DAL.getApplicantName(ddlApplicantID.SelectedValue)).Tables[0];

            //Induction_DAL.getApplicantName(ddlApplicantID.SelectedValue);
            txtApplicantName.Text = dtApplicantNmae.Rows[0]["HR_APPLICANT_NAME"].ToString();

        }

        protected void ddlDeprtment_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadDesignation();
        }
        private void LoadDesignation()
        {
            FIN.BLL.HR.Department_BLL.GetDesignationName(ref ddlDesignation, ddlDeprtment.SelectedValue);
        }



    }
}
