﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.BLL.AP;
using FIN.BLL.HR;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.BLL;
using VMVServices.Web;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;

namespace FIN.Client.HR
{
    public partial class ApplicantEntry : PageBase
    {
        HR_APPLICANTS hR_APPLICANTS = new HR_APPLICANTS();
        HR_APPLICANTS_QUALIFICATION hR_APPLICANTS_QUALIFICATION = new HR_APPLICANTS_QUALIFICATION();
        TMP_FILE_DET TMP_FILE_DETS = new TMP_FILE_DET();
        HR_APPLICANTS_PREV_CTC hR_APPLICANTS_PREV_CTC = new HR_APPLICANTS_PREV_CTC();


        System.Collections.SortedList slControls = new System.Collections.SortedList();
        DataTable dtGridData = new DataTable();
        DataTable dtGridDataPP = new DataTable();
        Boolean bol_rowVisiable;
        Boolean savedBool;
        //string ProReturn = null;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    Session[FINSessionConstants.Attachments] = null;
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                    //if (Session["CapturedImagePath"] != null)
                    //{
                    //    imgCaptureImagediv.Visible = true;
                    //    lblCaptureImage.Visible = true;
                    //    imgPatientFoto.ImageUrl = "Captures/" + Session["CapturedImagePath"].ToString() + ".png";
                    //    Session["CapturedImagePath"] = null;
                    //}
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                //pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                //pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
            }

            if (VMVServices.Web.Utils.AddAllowed != FINAppConstants.Y && Master.Mode == FINAppConstants.Add)
            {
                btnSave.Visible = false;
            }

        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                //ddlWeightUOM.Enabled = false;
                //ddlLengthUOM.Enabled = false;
                //ddlAreaUOM.Enabled = false;
                //ddlVolumeUOM.Enabled = false;


                Startup();
                FillComboBox();
                Session[FINSessionConstants.GridData] = null;
                Session["GridDataPP"] = null;

                EntityData = null;

                dtGridData = DBMethod.ExecuteQuery(Applicant_DAL.GetApplicantQualDtls(Master.StrRecordId.ToString())).Tables[0];
                BindGrid(dtGridData);

                dtGridDataPP = DBMethod.ExecuteQuery(Applicant_DAL.GetApplicantPrevCTCDtls(Master.StrRecordId.ToString())).Tables[0];
                BindGridPP(dtGridDataPP);


                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    using (IRepository<HR_APPLICANTS> userCtx = new DataRepository<HR_APPLICANTS>())
                    {
                        hR_APPLICANTS = userCtx.Find(r =>
                            (r.APP_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }



                    EntityData = hR_APPLICANTS;
                    txtApplicantId.Text = hR_APPLICANTS.APP_ID;
                    txtFirstName.Text = hR_APPLICANTS.APP_FIRST_NAME;
                    txtMName.Text = hR_APPLICANTS.APP_MIDDLE_NAME;
                    txtLastName.Text = hR_APPLICANTS.APP_LAST_NAME;
                    txtAddress1.Text = hR_APPLICANTS.APP_ADDRESS1;
                    txtAddress2.Text = hR_APPLICANTS.APP_ADDRESS2;
                    txtContactNo.Text = hR_APPLICANTS.APP_CONTACT_NO;
                    txtMobile.Text = hR_APPLICANTS.APP_MOBILE;
                    txtState.Text = hR_APPLICANTS.APP_STATE;
                    txtpincode.Text = hR_APPLICANTS.APP_PIN;
                    ddlApplicantType.SelectedValue = hR_APPLICANTS.APP_TYPE;
                    txtReferenceName.Text = hR_APPLICANTS.APP_REFERAL_NAME;
                    ddlStatus.SelectedValue = hR_APPLICANTS.STATUS;
                    txtCity.Text = hR_APPLICANTS.APP_CITY;
                    ddlApplnJobType.SelectedValue = hR_APPLICANTS.APP_JOB_TYPE;
                    ddlGender.SelectedValue = hR_APPLICANTS.APP_GENDER;
                    txtEmail.Text = hR_APPLICANTS.APP_EMAIL;
                    //if (hR_APPLICANTS.CURRENT_CTC != null)
                    //    txtCurrentCTC.Text = hR_APPLICANTS.CURRENT_CTC.ToString();
                    if (hR_APPLICANTS.NATIONALITY != null)
                    {
                        try
                        {
                            ddlNationality.SelectedValue = hR_APPLICANTS.NATIONALITY.ToString();
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                    if (hR_APPLICANTS.APP_DOB != null)
                    {
                        txtDOB.Text = DBMethod.ConvertDateToString(hR_APPLICANTS.APP_DOB.ToString());
                    }
                    if (hR_APPLICANTS.APP_LAST_CONTACT_DT != null)
                    {
                        txtLastContactDate.Text = DBMethod.ConvertDateToString(hR_APPLICANTS.APP_LAST_CONTACT_DT.ToString());
                    }

                    txtPreviousCompany.Text = hR_APPLICANTS.PREV_COMPANY;
                    txtReferenceName.Text = hR_APPLICANTS.APP_REFERAL_NAME;
                    //txtReference.Text = hR_APPLICANTS.APP_REFERAL_DTL;
                    txtCurrentPosition.Text = hR_APPLICANTS.APP_CURRENT_POSITION;
                    if (hR_APPLICANTS.APP_PRIME_SKILLS != null)
                    {
                        txtPrimarySkill.Text = hR_APPLICANTS.APP_PRIME_SKILLS;
                    }
                    txtSecondarySkill.Text = hR_APPLICANTS.APP_SEC_SKILLS;
                    txtRejectionReason.Text = hR_APPLICANTS.APP_REJECTION_REASON;
                    txtComments.Text = hR_APPLICANTS.APP_COMMENTS;
                    if (hR_APPLICANTS.APP_VISA_VALIDITY_DT != null)
                    {
                        dtpVisaVal.Text = DBMethod.ConvertDateToString(hR_APPLICANTS.APP_VISA_VALIDITY_DT.ToString());
                    }
                    txtPassportNum.Text = hR_APPLICANTS.APP_PASSPORT_NUMBER;
                    txtVisaDtl.Text = hR_APPLICANTS.APP_VISA_DTL;
                    txtRefName1.Text = hR_APPLICANTS.REFERENCE_NAME1;
                    txtCompName1.Text = hR_APPLICANTS.COMPANY_NAME1;
                    txtContactNo1.Text = hR_APPLICANTS.CONTACT_NUMBER1;
                    txtRefName2.Text = hR_APPLICANTS.REFERENCE_NAME2;
                    txtCompName2.Text = hR_APPLICANTS.COMPANY_NAME2;
                    txtContactNo2.Text = hR_APPLICANTS.CONTACT_NUMBER2;
                    ddlStatus.SelectedValue = hR_APPLICANTS.STATUS;
                    ddlReferalType.SelectedValue = hR_APPLICANTS.REFERAL_TYPE;
                    if (hR_APPLICANTS.APP_YEARS_EXPERIENCE != null)
                    {
                        txtExp.Text = hR_APPLICANTS.APP_YEARS_EXPERIENCE.ToString();
                    }
                    lblFileName.Text = "No Resume Upload for the Applicant";
                    string resumeid = "";
                    DataTable dtresume = new DataTable();
                    if (hR_APPLICANTS.RESUME_ID != null)
                    {
                        resumeid = hR_APPLICANTS.RESUME_ID.ToString();
                        dtresume = DBMethod.ExecuteQuery(Applicant_DAL.GetResumeFileName(resumeid.ToString())).Tables[0];
                        if (dtresume.Rows.Count > 0)
                        {
                            lblFileName.Visible = true;
                            lblFileName.Text = dtresume.Rows[0]["FILE_NAME"].ToString() + "." + dtresume.Rows[0]["FILE_EXTENSTION"].ToString();
                            hlresumdDownlaod.Visible = true;
                            hlresumdDownlaod.NavigateUrl = System.Configuration.ConfigurationManager.AppSettings["WBSITEURL"].ToString() + "/UploadFile/Resume/" + hR_APPLICANTS.APP_ID.ToString() + "_RESUME" + "." + dtresume.Rows[0]["FILE_EXTENSTION"].ToString().Replace(".", "");
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>

        private void FillComboBox()
        {

            Lookup_BLL.GetLookUpValues(ref ddlGender, "APP_GENDER");

            Lookup_BLL.GetLookUpValues(ref ddlApplicantType, "APP_TYPE");

            Lookup_BLL.GetLookUpValues(ref ddlApplnJobType, "APP_JOB_TYPE");

            Lookup_BLL.GetLookUpValues(ref ddlStatus, "STATUS");

            Lookup_BLL.GetLookUpValues(ref ddlReferalType, "Referral_Type");
            // FIN.BLL.SSM.Country_BLL.getNationalityName(ref ddlNationality);
            Lookup_BLL.GetLookUpValues(ref ddlNationality, "NATIONALITY");

        }

        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    hR_APPLICANTS = (HR_APPLICANTS)EntityData;
                }
                hR_APPLICANTS.APP_ORG_ID = VMVServices.Web.Utils.OrganizationID;

                // hR_APPLICANTS.APP_ID = txtApplicantId.Text;
                hR_APPLICANTS.APP_FIRST_NAME = txtFirstName.Text;
                hR_APPLICANTS.APP_MIDDLE_NAME = txtMName.Text;
                hR_APPLICANTS.APP_LAST_NAME = txtLastName.Text;
                hR_APPLICANTS.APP_ADDRESS1 = txtAddress1.Text;
                hR_APPLICANTS.APP_ADDRESS2 = txtAddress2.Text;
                hR_APPLICANTS.APP_CONTACT_NO = txtContactNo.Text;
                hR_APPLICANTS.APP_MOBILE = txtMobile.Text;
                hR_APPLICANTS.APP_STATE = txtState.Text;
                if (ddlApplicantType.SelectedValue != null)
                {
                    hR_APPLICANTS.APP_TYPE = ddlApplicantType.SelectedValue.ToString();
                }
                hR_APPLICANTS.APP_CITY = txtCity.Text;
                if (ddlStatus.SelectedValue != null)
                {
                    hR_APPLICANTS.STATUS = ddlStatus.SelectedValue.ToString();
                }
                hR_APPLICANTS.APP_PIN = txtpincode.Text;
                if (ddlApplnJobType.SelectedValue != null)
                {
                    hR_APPLICANTS.APP_JOB_TYPE = ddlApplnJobType.SelectedValue.ToString();
                }
                if (ddlApplnJobType.SelectedValue != null)
                {
                    hR_APPLICANTS.APP_GENDER = ddlGender.SelectedValue.ToString();
                }
                //hR_APPLICANTS.APP_REFERAL_DTL = txtReference.Text;
                hR_APPLICANTS.APP_EMAIL = txtEmail.Text;
                hR_APPLICANTS.NATIONALITY = ddlNationality.SelectedValue;
                //if (txtCurrentCTC.Text.ToString().Length > 0)
                //{
                //    hR_APPLICANTS.CURRENT_CTC = CommonUtils.ConvertStringToInt(txtCurrentCTC.Text);
                //}
                //else
                //{
                //    hR_APPLICANTS.CURRENT_CTC = null;
                //}
                if (txtDOB.Text != string.Empty)
                {
                    hR_APPLICANTS.APP_DOB = DBMethod.ConvertStringToDate(txtDOB.Text.ToString());
                }
                if (txtLastContactDate.Text != string.Empty)
                {
                    hR_APPLICANTS.APP_LAST_CONTACT_DT = DBMethod.ConvertStringToDate(txtLastContactDate.Text.ToString());
                }
                hR_APPLICANTS.APP_REFERAL_NAME = txtReferenceName.Text;
                hR_APPLICANTS.APP_PRIME_SKILLS = txtPrimarySkill.Text;
                hR_APPLICANTS.APP_SEC_SKILLS = txtSecondarySkill.Text;
                hR_APPLICANTS.APP_REJECTION_REASON = txtRejectionReason.Text;
                hR_APPLICANTS.APP_COMMENTS = txtComments.Text;
                hR_APPLICANTS.PREV_COMPANY = txtPreviousCompany.Text;
                if (chkExEmp.Checked)
                {
                    hR_APPLICANTS.APP_EX_EMP = FINAppConstants.Y;
                }
                else
                {
                    hR_APPLICANTS.APP_EX_EMP = FINAppConstants.N;
                }
                hR_APPLICANTS.APP_YEARS_EXPERIENCE = CommonUtils.ConvertStringToDecimal(txtExp.Text);
                hR_APPLICANTS.APP_CURRENT_POSITION = txtCurrentPosition.Text.ToString();
                hR_APPLICANTS.APP_PASSPORT_NUMBER = txtPassportNum.Text;
                hR_APPLICANTS.APP_VISA_DTL = txtVisaDtl.Text;
                if (dtpVisaVal.Text != string.Empty)
                {
                    hR_APPLICANTS.APP_VISA_VALIDITY_DT = DBMethod.ConvertStringToDate(dtpVisaVal.Text.ToString());
                }
                hR_APPLICANTS.REFERENCE_NAME1 = txtRefName1.Text;
                hR_APPLICANTS.COMPANY_NAME1 = txtCompName1.Text;
                hR_APPLICANTS.CONTACT_NUMBER1 = txtContactNo1.Text;
                hR_APPLICANTS.REFERENCE_NAME2 = txtRefName2.Text;
                hR_APPLICANTS.COMPANY_NAME2 = txtCompName2.Text;
                hR_APPLICANTS.CONTACT_NUMBER2 = txtContactNo2.Text;
                if (ddlStatus.SelectedValue != null)
                {
                    hR_APPLICANTS.STATUS = ddlStatus.SelectedValue.ToString();
                }
                if (ddlReferalType.SelectedValue != null)
                {
                    hR_APPLICANTS.REFERAL_TYPE = ddlReferalType.SelectedValue.ToString();
                }

                hR_APPLICANTS.ENABLED_FLAG = FINAppConstants.Y;
                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    hR_APPLICANTS.MODIFIED_BY = this.LoggedUserName;
                    hR_APPLICANTS.MODIFIED_DATE = DateTime.Today;

                }
                else
                {
                    hR_APPLICANTS.APP_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_054_M.ToString(), false, true);
                    // hR_APPLICANTS.PK_ID = DBMethod.GetPrimaryKeyValue(FINSequenceConstant.hR_COMPETENCY_HDR_SEQ);
                    //iNV_ITEM_MASTER.WORKFLOW_COMPLETION_STATUS = "1";
                    hR_APPLICANTS.CREATED_BY = this.LoggedUserName;
                    hR_APPLICANTS.CREATED_DATE = DateTime.Today;

                }

                hR_APPLICANTS.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_APPLICANTS.APP_ID);

                if (Session[FINSessionConstants.Attachments] != null)
                {
                    System.Collections.SortedList fu_tmp = (System.Collections.SortedList)Session[FINSessionConstants.Attachments];
                    if (System.IO.File.Exists(Server.MapPath("~/UploadFile/Resume/" + hR_APPLICANTS.APP_ID + "_RESUME." + fu_tmp.GetByIndex(1))))
                        System.IO.File.Delete(Server.MapPath("~/UploadFile/Resume/" + hR_APPLICANTS.APP_ID + "_RESUME." + fu_tmp.GetByIndex(1)));

                    System.IO.File.Copy(Server.MapPath("~/TmpResume/" + fu_tmp.GetByIndex(0) + "." + fu_tmp.GetByIndex(1)), Server.MapPath("~/UploadFile/Resume/" + hR_APPLICANTS.APP_ID + "_RESUME." + fu_tmp.GetByIndex(1)));
                    hR_APPLICANTS.RESUME_ID = fu_tmp.GetByIndex(0).ToString();
                }

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }



                var tmpChildEntity = new List<Tuple<object, string>>();

                
                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    hR_APPLICANTS_QUALIFICATION = new HR_APPLICANTS_QUALIFICATION();
                    if (dtGridData.Rows[iLoop][FINColumnConstants.APP_QUALI_ID].ToString() != "0")
                    {
                        using (IRepository<HR_APPLICANTS_QUALIFICATION> userCtx = new DataRepository<HR_APPLICANTS_QUALIFICATION>())
                        {
                            hR_APPLICANTS_QUALIFICATION = userCtx.Find(r =>
                                (r.APP_QUALI_ID == dtGridData.Rows[iLoop][FINColumnConstants.APP_QUALI_ID].ToString())
                                ).SingleOrDefault();
                        }
                    }

                    hR_APPLICANTS_QUALIFICATION.APP_QUALI_SHORT_NAME = dtGridData.Rows[iLoop][FINColumnConstants.APP_QUALI_SHORT_NAME].ToString();

                    hR_APPLICANTS_QUALIFICATION.APP_QUALI_DESCRIPTION = dtGridData.Rows[iLoop][FINColumnConstants.APP_QUALI_DESCRIPTION].ToString();
                    hR_APPLICANTS_QUALIFICATION.APP_ISSUING_AUTHORITY = dtGridData.Rows[iLoop][FINColumnConstants.APP_ISSUING_AUTHORITY].ToString();
                    hR_APPLICANTS_QUALIFICATION.APP_QUALI_DURATION = decimal.Parse(dtGridData.Rows[iLoop][FINColumnConstants.APP_QUALI_DURATION].ToString());

                    hR_APPLICANTS_QUALIFICATION.ENABLED_FLAG = FINAppConstants.Y;

                    hR_APPLICANTS_QUALIFICATION.WORKFLOW_COMPLETION_STATUS = "1";
                    hR_APPLICANTS_QUALIFICATION.APP_ID = hR_APPLICANTS.APP_ID;

                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        hR_APPLICANTS_QUALIFICATION.APP_QUALI_ID = dtGridData.Rows[iLoop][FINColumnConstants.APP_QUALI_ID].ToString();
                        tmpChildEntity.Add(new Tuple<object, string>(hR_APPLICANTS_QUALIFICATION, FINAppConstants.Delete));
                    }
                    else
                    {

                        if (dtGridData.Rows[iLoop][FINColumnConstants.APP_QUALI_ID].ToString() != "0" && dtGridData.Rows[iLoop][FINColumnConstants.APP_QUALI_ID].ToString() != string.Empty)
                        {
                            hR_APPLICANTS_QUALIFICATION.APP_QUALI_ID = dtGridData.Rows[iLoop][FINColumnConstants.APP_QUALI_ID].ToString();
                            hR_APPLICANTS_QUALIFICATION.MODIFIED_BY = this.LoggedUserName;
                            hR_APPLICANTS_QUALIFICATION.MODIFIED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(hR_APPLICANTS_QUALIFICATION, "U"));

                        }
                        else
                        {
                            hR_APPLICANTS_QUALIFICATION.APP_QUALI_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_054_D.ToString(), false, true);
                            hR_APPLICANTS_QUALIFICATION.CREATED_BY = this.LoggedUserName;
                            hR_APPLICANTS_QUALIFICATION.CREATED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(hR_APPLICANTS_QUALIFICATION, "A"));
                        }
                    }





                    //Applicant_BLL. = hR_APPLICANTS_QUALIFICATION;

                }



                if (Session["GridDataPP"] != null)
                {
                    dtGridDataPP = (DataTable)Session["GridDataPP"];
                }


                //PREV PACK
                var tmpChildEntity1 = new List<Tuple<object, string>>();

            
                for (int jLoop = 0; jLoop < dtGridDataPP.Rows.Count; jLoop++)
                {
                    hR_APPLICANTS_PREV_CTC = new HR_APPLICANTS_PREV_CTC();
                    if (dtGridDataPP.Rows[jLoop]["APP_PREV_CTC_ID"].ToString() != "0")
                    {
                        using (IRepository<HR_APPLICANTS_PREV_CTC> userCtx = new DataRepository<HR_APPLICANTS_PREV_CTC>())
                        {
                            hR_APPLICANTS_PREV_CTC = userCtx.Find(r =>
                                (r.APP_PREV_CTC_ID == dtGridDataPP.Rows[jLoop]["APP_PREV_CTC_ID"].ToString())
                                ).SingleOrDefault();
                        }
                    }
                   
                    hR_APPLICANTS_PREV_CTC.ELEMENT_NAME = dtGridDataPP.Rows[jLoop]["ELEMENT_NAME"].ToString();

                    hR_APPLICANTS_PREV_CTC.ELEMENT_AMOUNT = decimal.Parse(dtGridDataPP.Rows[jLoop]["ELEMENT_AMOUNT"].ToString());



                    hR_APPLICANTS_PREV_CTC.ENABLED_FLAG = FINAppConstants.Y;

                    hR_APPLICANTS_PREV_CTC.WORKFLOW_COMPLETION_STATUS = "1";
                    hR_APPLICANTS_PREV_CTC.APP_ID = hR_APPLICANTS.APP_ID;

                    if (dtGridDataPP.Rows[jLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        hR_APPLICANTS_PREV_CTC.APP_PREV_CTC_ID = dtGridDataPP.Rows[jLoop]["APP_PREV_CTC_ID"].ToString();
                        tmpChildEntity.Add(new Tuple<object, string>(hR_APPLICANTS_QUALIFICATION, FINAppConstants.Delete));
                    }
                    else
                    {

                        if (dtGridDataPP.Rows[jLoop]["APP_PREV_CTC_ID"].ToString() != "0" && dtGridDataPP.Rows[jLoop]["APP_PREV_CTC_ID"].ToString() != string.Empty)
                        {
                            hR_APPLICANTS_PREV_CTC.APP_PREV_CTC_ID = dtGridDataPP.Rows[jLoop]["APP_PREV_CTC_ID"].ToString();
                            hR_APPLICANTS_PREV_CTC.MODIFIED_BY = this.LoggedUserName;
                            hR_APPLICANTS_PREV_CTC.MODIFIED_DATE = DateTime.Today;
                            tmpChildEntity1.Add(new Tuple<object, string>(hR_APPLICANTS_PREV_CTC, "U"));

                        }
                        else
                        {
                            hR_APPLICANTS_PREV_CTC.APP_PREV_CTC_ID = FINSP.GetSPFOR_SEQCode("HR_054_DD".ToString(), false, true);
                            hR_APPLICANTS_PREV_CTC.CREATED_BY = this.LoggedUserName;
                            hR_APPLICANTS_PREV_CTC.CREATED_DATE = DateTime.Today;
                            tmpChildEntity1.Add(new Tuple<object, string>(hR_APPLICANTS_PREV_CTC, "A"));
                        }
                    }





                    //Applicant_BLL. = hR_APPLICANTS_QUALIFICATION;

                }

                // PREV PACK
                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            CommonUtils.SaveMultipleEntity<HR_APPLICANTS, HR_APPLICANTS_QUALIFICATION, HR_APPLICANTS_PREV_CTC>(hR_APPLICANTS, tmpChildEntity, hR_APPLICANTS_QUALIFICATION, tmpChildEntity1, hR_APPLICANTS_PREV_CTC);
                            savedBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {
                            CommonUtils.SaveMultipleEntity<HR_APPLICANTS, HR_APPLICANTS_QUALIFICATION, HR_APPLICANTS_PREV_CTC>(hR_APPLICANTS, tmpChildEntity, hR_APPLICANTS_QUALIFICATION, tmpChildEntity1, hR_APPLICANTS_PREV_CTC, true);
                            savedBool = true;
                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                //ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session[FINSessionConstants.GridData], "Applicant ");
                //if (ErrorCollection.Count > 0)
                //{
                //    return;
                //}
                AssignToBE();


                //emptyvalid();
                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);
                }

                // Duplicate Validation Through Backend Package PKG_VALIDATIONS



                //ProReturn = FIN.DAL.AP.Item_DAL.GetSPFOR_DUPLICATE_CHECK(iNV_ITEM_MASTER.ITEM_CODE,txtItemName.Text, iNV_ITEM_MASTER.ORG_ID, iNV_ITEM_MASTER.ITEM_ID);

                //if (ProReturn != string.Empty)
                //{
                //    if (ProReturn != "0")
                //    {
                //        ErrorCollection.Add("ITEMNAME", ProReturn);
                //        if (ErrorCollection.Count > 0)
                //        {
                //            return;
                //        }
                //    }
                //}


                //switch (Master.Mode)
                //{
                //    case FINAppConstants.Add:
                //        {
                //            DBMethod.SaveEntity<HR_APPLICANTS>(hR_APPLICANTS);
                //            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                //            break;
                //        }
                //    case FINAppConstants.Update:
                //        {

                //            DBMethod.SaveEntity<HR_APPLICANTS>(hR_APPLICANTS, true);
                //            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                //            break;

                //        }
            }

            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion




        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                DBMethod.DeleteEntity<HR_APPLICANTS>(hR_APPLICANTS);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }



        protected void btnCancel_Click(object sender, EventArgs e)
        {

        }







        //private void emptyvalid()
        //{
        //    if (hR_VACANCIES.VAC_DEPT_ID == null)
        //    {

        //        if (ddlDepartment.SelectedValue == string.Empty)
        //        {
        //            ErrorCollection.Add("Department", "Department Cannot be empty");
        //        }

        //    }
        //    if (hR_VACANCIES.VAC_DESIG_ID == null)
        //    {
        //        if (ddlDesignation.SelectedValue == string.Empty)

        //            ErrorCollection.Add("Designation", "Designation Cannot be empty");
        //        }
        //    }
        //    //if (hR_VACANCIES.VAC_TYPE > 0)
        //    //{

        //    //    if (ddlType.SelectedValue == string.Empty)
        //    //    {
        //    //        ErrorCollection.Add("DepartmentType", "Department Type Cannot be empty");
        //    //    }
        //    //}

        //    //if  > 0)
        //    //{
        //    //    if (ddlVolumeUOM.SelectedValue == string.Empty)
        //    //    {

        //    //        ErrorCollection.Add("VOLEMT", "Volume UOM Cannot be empty");
        //    //    }
        //    //}

        //}

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {

            System.Collections.SortedList slControls = new System.Collections.SortedList();
            TextBox txt_ShortName = gvr.FindControl("txtShortName") as TextBox;
            TextBox txt_Description = gvr.FindControl("txtDescription") as TextBox;
            TextBox txt_IssueAuth = gvr.FindControl("txtIssueAuth") as TextBox;
            TextBox txt_Duration = gvr.FindControl("txtDuration") as TextBox;

            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList[FINColumnConstants.APP_QUALI_ID] = "0";

            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }

            slControls[0] = txt_ShortName;
            slControls[1] = txt_Description;
            slControls[2] = txt_IssueAuth;
            slControls[3] = txt_Duration;

            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));

            ErrorCollection.Clear();
            string strCtrlTypes = FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX;
            string strMessage = Prop_File_Data["Short_Name_P"] + " ~ " + Prop_File_Data["Description_P"] + " ~ " + Prop_File_Data["Issuing_Authority_P"] + " ~ " + Prop_File_Data["Duration_P"] + "";
            //string strMessage = " Short Name ~ Description ~ Issuing Authority ~ Duration ";
            ErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);
            if (ErrorCollection.Count > 0)
                return drList;




            // string strCondition = "PAY_ELEMENT_ID='" + ddl_ElementName.Text.Trim().ToUpper() + "'";
            strMessage = FINMessageConstatns.RecordAlreadyExists;
            //ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            //if (ErrorCollection.Count > 0)
            //{

            //    return drList;
            //}

            //if (ddl_ElementName.SelectedItem != null)
            //{
            //    drList[FINColumnConstants.PAY_ELEMENT_ID] = ddl_ElementName.SelectedItem.Value;
            //    drList[FINColumnConstants.PAY_ELEMENT] = ddl_ElementName.SelectedItem.Text;
            //}
            drList["APP_QUALI_SHORT_NAME"] = txt_ShortName.Text;
            drList["APP_QUALI_DESCRIPTION"] = txt_Description.Text;
            drList["APP_ISSUING_AUTHORITY"] = txt_IssueAuth.Text;
            drList["APP_QUALI_DURATION"] = txt_Duration.Text;
            drList[FINColumnConstants.ENABLED_FLAG] = FINAppConstants.EnabledFlag;
            drList[FINColumnConstants.DELETED] = FINAppConstants.N;

            return drList;
        }


        private DataRow AssignToGridControlPP(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {

            System.Collections.SortedList slControls = new System.Collections.SortedList();
            TextBox txtEleName = gvr.FindControl("txtEleName") as TextBox;
            TextBox txtAmt = gvr.FindControl("txtAmt") as TextBox;


            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridDataPP.NewRow();
                drList["APP_PREV_CTC_ID"] = "0";

            }
            else
            {
                drList = dtGridDataPP.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }

            slControls[0] = txtEleName;
            slControls[1] = txtAmt;


            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));

            ErrorCollection.Clear();
            string strCtrlTypes = FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX;
            string strMessage = Prop_File_Data["Element_Name_P"] + " ~ " + Prop_File_Data["Amount_P"] + "";
            //string strMessage = " Short Name ~ Description ~ Issuing Authority ~ Duration ";
            ErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);
            if (ErrorCollection.Count > 0)
                return drList;




            // string strCondition = "PAY_ELEMENT_ID='" + ddl_ElementName.Text.Trim().ToUpper() + "'";
            strMessage = FINMessageConstatns.RecordAlreadyExists;
            //ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            //if (ErrorCollection.Count > 0)
            //{

            //    return drList;
            //}

            //if (ddl_ElementName.SelectedItem != null)
            //{
            //    drList[FINColumnConstants.PAY_ELEMENT_ID] = ddl_ElementName.SelectedItem.Value;
            //    drList[FINColumnConstants.PAY_ELEMENT] = ddl_ElementName.SelectedItem.Text;
            //}
            drList["ELEMENT_NAME"] = txtEleName.Text;
            drList["ELEMENT_AMOUNT"] = txtAmt.Text;

            drList[FINColumnConstants.DELETED] = FINAppConstants.N;

            return drList;
        }


        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                //else if (FacilityMasterBLL.ErrorCollection.Count > 0)
                //{
                //    ErrorCollection = FacilityMasterBLL.ErrorCollection;
                //    return;
                //}
                else
                {
                    gvData.EditIndex = -1;
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Row_upd", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }


        protected void gvPrevPack_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvPrevPack.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session["GridDataPP"] != null)
                {
                    dtGridDataPP = (DataTable)Session["GridDataPP"];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControlPP(gvr, dtGridDataPP, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                //else if (FacilityMasterBLL.ErrorCollection.Count > 0)
                //{
                //    ErrorCollection = FacilityMasterBLL.ErrorCollection;
                //    return;
                //}
                else
                {
                    gvPrevPack.EditIndex = -1;
                    BindGridPP(dtGridDataPP);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Row_upd", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row delete mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewDeleteEventArgs indicates which row's delete button was clicked. </param>


                                                                                                        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                dtGridData.Rows[e.RowIndex][FINColumnConstants.DELETED] = FINAppConstants.Y;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Row_del", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }

        protected void gvPrevPack_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session["GridDataPP"] != null)
                {
                    dtGridDataPP = (DataTable)Session["GridDataPP"];
                }
                dtGridDataPP.Rows[e.RowIndex][FINColumnConstants.DELETED] = FINAppConstants.Y;
                BindGridPP(dtGridDataPP);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Row_del", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                //GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                //FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Row_Edt", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }


        protected void gvPrevPack_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session["GridDataPP"] != null)
                {
                    dtGridDataPP = (DataTable)Session["GridDataPP"];
                }
                gvPrevPack.EditIndex = e.NewEditIndex;
                BindGridPP(dtGridDataPP);
                //GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                //FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Row_Edt", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (bol_rowVisiable)
                    e.Row.Visible = false;

                if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                {
                    e.Row.Visible = false;
                }

            }
        }
        protected void gvPrevPack_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (bol_rowVisiable)
                    e.Row.Visible = false;

                if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                {
                    e.Row.Visible = false;
                }

            }
        }
        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>
        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }

                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    else
                    {
                        dtGridData.Rows.Add(drList);
                        BindGrid(dtGridData);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Row_Cmd", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }

        protected void gvPrevPack_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session["GridDataPP"] != null)
                {
                    dtGridDataPP = (DataTable)Session["GridDataPP"];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvPrevPack.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }

                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControlPP(gvr, dtGridDataPP, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    else
                    {
                        dtGridDataPP.Rows.Add(drList);
                        BindGridPP(dtGridDataPP);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Row_Cmd", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }


        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = -1;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Row_cnl", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void gvPrevPack_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session["GridDataPP"] != null)
                {
                    dtGridDataPP = (DataTable)Session["GridDataPP"];
                }
                gvPrevPack.EditIndex = -1;

                BindGridPP(dtGridDataPP);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Row_cnl", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }


        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Applicant", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }


        protected void gvPrevPack_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvPrevPack.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Applicant", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;


                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr["enabled_flag"] = "FALSE";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }

                gvData.DataSource = dt_tmp;
                gvData.DataBind();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TS_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        private void BindGridPP(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session["GridDataPP"] = dtData;
                if (dtData.Rows.Count > 0)
                {
                    dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("ELEMENT_AMOUNT", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("ELEMENT_AMOUNT"))));
                    dtData.AcceptChanges();
                }
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";                    
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvPrevPack.DataSource = dt_tmp;
                gvPrevPack.DataBind();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TS_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        protected void btnCloseUpload_Click(object sender, EventArgs e)
        {
            if (Session[FINSessionConstants.Attachments] != null)
            {
                System.Collections.SortedList fu_tmp = (System.Collections.SortedList)Session[FINSessionConstants.Attachments];
                // img_Photo.ImageUrl = "~/TmpAttachments/" + fu_tmp.GetByIndex(0) + ".doc";
                lblFileName.Visible = true;
                lblFileName.Text = fu_tmp.GetByIndex(2) + "." + fu_tmp.GetByIndex(1);
            }
        }
        protected void img_Photo_Click(object sender, ImageClickEventArgs e)
        {
            meUpload.Show();
        }

        protected void txtFirstName_TextChanged(object sender, EventArgs e)
        {

        }

        //protected void btnFoto_Click(object sender, EventArgs e)
        //{
        //    //RegisterStartupScript("Startup", "<script language='javascript'>window.open('PhotoCapturing.aspx?ID=" + Master.RecordID.ToString() + ",'','toolbar=no,menubar=no,location=no,scrollbars=yes,resizable=no,status=no,width=860px,height=660px,left=0px,Top=0px');</script>");
        //    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('PhotoCapturing.aspx?ID=" + Master.RecordID.ToString() + ",'','" + MessageConstants.ReportProperties + "');", true);

        //    //string alertScript = "javascript:alert('Alter From asp.net page')";
        //    //ClientScript.RegisterStartupScript(typeof(Page), "alertscript", alertScript);
        //    ModalPopupExtender2.Show();
        //}

        //protected void btnCloseImage_Click(object sender, EventArgs e)
        //{
        //    if (Session["CapturedImagePath"] != null)
        //    {

        //        //System.Collections.SortedList fu_tmp = (System.Collections.SortedList)Session["CapturedImagePath"];
        //        imgPatientFoto.ImageUrl = "Captures/" + Session["CapturedImagePath"].ToString() + ".png";

        //        Session["fotoLoad"] = "yes";

        //        ModalPopupExtender2.Hide();

        //        Response.Redirect(Request.RawUrl);
        //    }
        //}


    }





}
