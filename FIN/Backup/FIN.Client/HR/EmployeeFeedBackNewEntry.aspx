﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="EmployeeFeedBackNewEntry.aspx.cs" Inherits="FIN.Client.HR.EmployeeFeedBackNewEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="Server">
    <div class="divFormcontainer" style="width: 800px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 180px" id="Div1">
                Employee Name
            </div>
            <div class="divtxtBox  LNOrient" style="width: 300px">
                <asp:DropDownList ID="ddlEmpName" runat="server" CssClass="validate[required] DisplayFont RequiredField ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 180px" id="lblFeedBackTempName">
                Feedback Template Name
            </div>
            <div class="divtxtBox  LNOrient" style="width: 300px">
                <asp:DropDownList ID="ddlFeedbackTempName" runat="server" CssClass="validate[required] DisplayFont RequiredField ddlStype"
                    OnSelectedIndexChanged="ddlFeedbackTempName_SelectedIndexChanged" AutoPostBack="true">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 180px" id="lblDate">
                Feedback Date
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:TextBox ID="txtFromDate" runat="server" TabIndex="2" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]]  RequiredField txtBox"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calExtender1" TargetControlID="txtFromDate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px" align="center">
                <asp:ImageButton ID="imgBtnView" runat="server" ImageUrl="~/Images/View.png" Style="border: 0px"
                    OnClick="imgBtnView_Click" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <br />
        <div id="div_TemplQues" runat="server" visible="false">
            <div class="divRowContainer" style="background-color: Navy; height: 40px">
                <div class="lblBox" style="float: left; width: 100px; padding-left: 50px" align="center">
                    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/Previous.png"
                        Style="border: 0px" OnClick="imgPrevious_Click" />
                </div>
                <div class="lblBox" style="width: 100px" align="center">
                    <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Images/Next.png" Style="border: 0px"
                        OnClick="imgNext_Click" />
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer" align="center">
                <asp:Label ID="lblTemplateName" runat="server" Text="Template Name" CssClass="lblBox"
                    Style="font-size: 20px"></asp:Label>
            </div>
            <div class="divClear_10">
            </div>
            <br />
            <div class="divRowContainer">
                <asp:Label ID="lblTemplDesc" runat="server" Text="Description" CssClass="lblBox"
                    Style="font-size: 13px"></asp:Label>
            </div>
            <br />
            <div class="divClear_10">
            </div>
            <div class="divRowContainer" align="center" id="div_RFA" runat="server" style="font-size: 11">
                <table border="1" style="font-weight: bold">
                    <tr>
                        <td colspan="4">
                            Rate Of Attribures
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 150px">
                            1
                        </td>
                        <td style="width: 150px">
                            2
                        </td>
                        <td style="width: 150px">
                            3
                        </td>
                        <td style="width: 150px">
                            4
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Strongly Disagree
                        </td>
                        <td>
                            Disagree
                        </td>
                        <td>
                            Agree
                        </td>
                        <td>
                            Strongly Agree
                        </td>
                    </tr>
                </table>
            </div>
            <div class="divClear_10">
            </div>
            <br />
            <div class="divRowContainer">
                <asp:Label ID="lblHeading" runat="server" Text="Heading" CssClass="lblBox" Style="color: Blue;
                    font-weight: bold"></asp:Label>
            </div>
            <div class="divClear_10">
            </div>   
            <div class="divRowContainer" style="padding-left: 20px">
                <asp:Label ID="lblQuestion" runat="server" Text="Question" CssClass="lblBox"></asp:Label>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer" style="padding-left: 40px">
                <asp:RadioButtonList ID="rbAnswer" runat="server" RepeatColumns="4" CssClass="lblBox"
                    Style="font-weight: bold" RepeatDirection="Horizontal" CellPadding="10" AutoPostBack="True"
                    OnSelectedIndexChanged="rbAnswer_SelectedIndexChanged">
                </asp:RadioButtonList>
                <asp:TextBox ID="txtComments" runat="server" CssClass="txtBox" TextMode="MultiLine"
                    Height="50px" Visible="false"></asp:TextBox>
                <asp:GridView ID="gv_quesList" runat="server" AutoGenerateColumns="False" DataKeyNames="feedback_dtl_id,code_id,feedback_rating_type,SEL_RATE,TYPED_COMMENTS"
                    Width="100%" OnDataBound="gv_quesList_DataBound" 
                    OnRowDataBound="gv_quesList_RowDataBound" ShowHeader="False">
                    <Columns>
                        <asp:TemplateField HeaderText="">
                            <ItemTemplate>
                                <asp:Label ID="lbl_G_Question" runat="server" Text='<%# Eval("feedback_desc") %>'
                                    CssClass="lblBox"></asp:Label>
                                <asp:RadioButtonList ID="rb_G_Answer" runat="server" RepeatColumns="4" CssClass="lblBox"
                                    Style="font-weight: bold" RepeatDirection="Horizontal" CellPadding="10" 
                                    AutoPostBack="True" onselectedindexchanged="rb_G_Answer_SelectedIndexChanged">
                                </asp:RadioButtonList>
                                <asp:TextBox ID="txt_G_Comments" runat="server" CssClass="txtBox" TextMode="MultiLine"
                                    Height="50px" Visible="false"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                    <HeaderStyle CssClass="GridHeader" />
                    <AlternatingRowStyle CssClass="GrdAltRow" />
                </asp:GridView>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer" style="background-color: Navy; height: 40px">
                <div class="lblBox" style="float: left; width: 100px; padding-left: 50px" align="center">
                    <asp:ImageButton ID="imgPrevious" runat="server" ImageUrl="~/Images/Previous.png"
                        Style="border: 0px" OnClick="imgPrevious_Click" />
                </div>
                <div class="lblBox" style="float: right; width: 100px" align="center">
                    <asp:ImageButton ID="imgNext" runat="server" ImageUrl="~/Images/Next.png" Style="border: 0px"
                        OnClick="imgNext_Click" />
                </div>
            </div>
            <asp:HiddenField ID="hf_QuestNum" runat="server" />
            <asp:HiddenField ID="hf_QuesType" runat="server" />
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                            TabIndex="12" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="13" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="14" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="15" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/HR/HRChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_imgBtnView").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

    </script>
</asp:Content>
