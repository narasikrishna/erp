﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.DAL.GL;
using FIN.BLL;
using FIN.BLL.GL;
using FIN.BLL.HR;
using VMVServices.Web;


namespace FIN.Client.HR
{
    public partial class TrainingFeedbackEntry : PageBase
    {
        HR_TRM_FEEDBACK_HDR hR_TRM_FEEDBACK_HDR = new HR_TRM_FEEDBACK_HDR();
        HR_TRM_FEEDBACK_DTL hR_TRM_FEEDBACK_DTL = new HR_TRM_FEEDBACK_DTL();

        DataTable dtGridData = new DataTable();
        Boolean bol_rowVisiable;
        Boolean savedBool;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TFE", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));


            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
        }

        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        private void FillComboBox()
        {
            FIN.BLL.HR.TrainingSchedule_BLL.GetScheduleData(ref ddlSchedule);
            Lookup_BLL.GetLookUpValues(ref ddlSession, "Session");
            FIN.BLL.HR.Employee_BLL.GetEmployeeName(ref ddlEmployee);
        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                Startup();
                FillComboBox();
                EntityData = null;

                dtGridData = FIN.BLL.HR.TrainingFeedback_BLL.getChildEntityDet(Master.StrRecordId);
                BindGrid(dtGridData);

                txtId.Enabled = false;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    using (IRepository<HR_TRM_FEEDBACK_HDR> userCtx = new DataRepository<HR_TRM_FEEDBACK_HDR>())
                    {
                        hR_TRM_FEEDBACK_HDR = userCtx.Find(r =>
                            (r.FB_HDR_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = hR_TRM_FEEDBACK_HDR;

                    txtId.Text = hR_TRM_FEEDBACK_HDR.FB_HDR_ID.ToString();

                    if (hR_TRM_FEEDBACK_HDR.FB_DATE != null)
                    {
                        txtDate.Text = DBMethod.ConvertDateToString(hR_TRM_FEEDBACK_HDR.FB_DATE.ToString());
                    }

                    ddlSchedule.SelectedValue = hR_TRM_FEEDBACK_HDR.TRN_SCH_HDR_ID;
                    FillProgram();
                    //HR_TRM_SCHEDULE_DTL hR_TRM_SCHEDULE_DTL = new HR_TRM_SCHEDULE_DTL();
                    //using (IRepository<HR_TRM_SCHEDULE_DTL> userCtx = new DataRepository<HR_TRM_SCHEDULE_DTL>())
                    //{
                    //    hR_TRM_SCHEDULE_DTL = userCtx.Find(r =>
                    //        (r.TRN_SCH_HDR_ID == hR_TRM_FEEDBACK_HDR.TRN_SCH_HDR_ID.ToString())
                    //        ).SingleOrDefault();
                    //}

                    //ddlProgram.SelectedValue = hR_TRM_SCHEDULE_DTL.PROG_ID.ToString();
                    //ddlCourse.SelectedValue = hR_TRM_SCHEDULE_DTL.PROG_COURSE_ID.ToString();
                    //ddlSubject.SelectedValue = hR_TRM_SCHEDULE_DTL.SUBJECT_ID.ToString();
                    //ddlSession.SelectedValue = hR_TRM_SCHEDULE_DTL.TRN_SESSION.ToString();

                    //ddlSchedule.SelectedValue = hR_TRM_FEEDBACK_HDR.TRN_SCH_HDR_ID;
                    //FillProgram();

                    ddlProgram.SelectedValue = hR_TRM_FEEDBACK_HDR.PROGRAM_ID;
                    FillCourse();

                    ddlCourse.SelectedValue = hR_TRM_FEEDBACK_HDR.COURSE_ID;
                    FillSubject();
                    ddlSubject.SelectedValue = hR_TRM_FEEDBACK_HDR.SUBJECT_ID;
                    ddlSession.SelectedValue = hR_TRM_FEEDBACK_HDR.SESSION_ID;
                    ddlEmployee.SelectedValue = hR_TRM_FEEDBACK_HDR.EMP_ID;
                   // Trainer_BLL.GetTrainer(ref ddlTrainerName);
                    fillTrainer();
                    ddlTrainerName.SelectedValue = hR_TRM_FEEDBACK_HDR.TRAINER_ID;

                    if (hR_TRM_FEEDBACK_HDR.TRAINING_DATE != null)
                    {
                        txtTrainingDate.Text = DBMethod.ConvertDateToString(hR_TRM_FEEDBACK_HDR.TRAINING_DATE.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("FBEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("FB_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                TextBox txtDesc = tmpgvr.FindControl("txtDesc") as TextBox;
                TextBox txtComment = tmpgvr.FindControl("txtComment") as TextBox;
                DropDownList ddlTrainingTyp = tmpgvr.FindControl("ddlTrainingTyp") as DropDownList;
                Lookup_BLL.GetLookUpValues(ref ddlTrainingTyp, "TRAINING_TYPE");


                if (gvData.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                {
                    txtDesc.Text = gvData.DataKeys[gvData.EditIndex].Values["FB_DESC"].ToString();
                    txtComment.Text = gvData.DataKeys[gvData.EditIndex].Values["FB_COMMENTS"].ToString();
                    ddlTrainingTyp.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.LOOKUP_ID].ToString();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("FBFillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        #endregion
        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                System.Collections.SortedList slControls = new System.Collections.SortedList();

                //slControls[0] = txtId;
                slControls[0] = txtDate;
                slControls[1] = ddlSchedule;
                slControls[2] = ddlProgram;
                slControls[3] = ddlCourse;
                slControls[4] = ddlSubject;
                slControls[5] = ddlSession;
                slControls[6] = ddlEmployee;
                Dictionary<string, string> Prop_File_Data;
                Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));
                string strCtrlTypes = FINAppConstants.TEXT_BOX + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST;
                string strMessage = Prop_File_Data["Date_P"] + " ~ " + Prop_File_Data["Schedule_P"] + " ~ " + Prop_File_Data["Program_P"] + " ~ " + Prop_File_Data["Course_P"] + " ~ " + Prop_File_Data["Subject_P"] + " ~ " + Prop_File_Data["Session_P"] + " ~ " + Prop_File_Data["Employee_P"] + "";
                //string strMessage = "Date ~ Schedule ~ Program ~ Course ~ Subject ~ Session ~ Employee";

                EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

                if (EmptyErrorCollection.Count > 0)
                {
                    ErrorCollection = EmptyErrorCollection;
                    return;
                }

                ErrorCollection.Clear();
                AssignToBE();
                ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session[FINSessionConstants.GridData], "Training Feedback");

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);
                    txtId.Text = hR_TRM_FEEDBACK_HDR.FB_HDR_ID;
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }
        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    hR_TRM_FEEDBACK_HDR = (HR_TRM_FEEDBACK_HDR)EntityData;
                }

                if (txtDate.Text != string.Empty)
                {
                    hR_TRM_FEEDBACK_HDR.FB_DATE = DBMethod.ConvertStringToDate(txtDate.Text.ToString());
                }

                hR_TRM_FEEDBACK_HDR.TRN_SCH_HDR_ID = ddlSchedule.SelectedValue.ToString();
                hR_TRM_FEEDBACK_HDR.ENABLED_FLAG = FINAppConstants.EnabledFlag;

                hR_TRM_FEEDBACK_HDR.PROGRAM_ID = ddlProgram.SelectedValue.ToString();
                hR_TRM_FEEDBACK_HDR.COURSE_ID = ddlCourse.SelectedValue.ToString();
                hR_TRM_FEEDBACK_HDR.SUBJECT_ID = ddlSubject.SelectedValue.ToString();
                hR_TRM_FEEDBACK_HDR.SESSION_ID = ddlSession.SelectedValue.ToString();
                hR_TRM_FEEDBACK_HDR.EMP_ID = ddlEmployee.SelectedValue.ToString();
                hR_TRM_FEEDBACK_HDR.TRAINER_ID = ddlTrainerName.SelectedValue.ToString();
                hR_TRM_FEEDBACK_HDR.FB_ORG_ID = VMVServices.Web.Utils.OrganizationID;
                if (txtTrainingDate.Text.Length > 0)
                {
                    hR_TRM_FEEDBACK_HDR.TRAINING_DATE = DBMethod.ConvertStringToDate(txtTrainingDate.Text);
                }

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    hR_TRM_FEEDBACK_HDR.MODIFIED_BY = this.LoggedUserName;
                    hR_TRM_FEEDBACK_HDR.MODIFIED_DATE = DateTime.Today;
                }
                else
                {
                    hR_TRM_FEEDBACK_HDR.FB_HDR_ID = FINSP.GetSPFOR_SEQCode("HR_061_M".ToString(), false, true);
                    //hR_TRM_FEEDBACK_HDR.PK_ID = DBMethod.GetPrimaryKeyValue(FINSequenceConstant.hR_TRM_SCHEDULE_HDR_SEQ);
                    hR_TRM_FEEDBACK_HDR.CREATED_BY = this.LoggedUserName;
                    hR_TRM_FEEDBACK_HDR.CREATED_DATE = DateTime.Today;
                    txtId.Text = hR_TRM_FEEDBACK_HDR.FB_HDR_ID;
                }

                hR_TRM_FEEDBACK_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_TRM_FEEDBACK_HDR.FB_HDR_ID);

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                var tmpChildEntity = new List<Tuple<object, string>>();

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    hR_TRM_FEEDBACK_DTL = new HR_TRM_FEEDBACK_DTL();
                    if (dtGridData.Rows[iLoop]["FB_DTL_ID"].ToString() != "0" && dtGridData.Rows[iLoop]["FB_DTL_ID"].ToString() != string.Empty)
                    {
                        using (IRepository<HR_TRM_FEEDBACK_DTL> userCtx = new DataRepository<HR_TRM_FEEDBACK_DTL>())
                        {
                            hR_TRM_FEEDBACK_DTL = userCtx.Find(r =>
                                (r.FB_DTL_ID == dtGridData.Rows[iLoop]["FB_DTL_ID"].ToString())
                                ).SingleOrDefault();
                        }
                    }

                    hR_TRM_FEEDBACK_DTL.FB_DESC = dtGridData.Rows[iLoop]["FB_DESC"].ToString();
                    hR_TRM_FEEDBACK_DTL.FB_COMMENTS = dtGridData.Rows[iLoop]["FB_COMMENTS"].ToString();
                    hR_TRM_FEEDBACK_DTL.TRAINING_TYPE = dtGridData.Rows[iLoop]["LOOKUP_ID"].ToString();

                    hR_TRM_FEEDBACK_DTL.FB_HDR_ID = hR_TRM_FEEDBACK_HDR.FB_HDR_ID;
                    hR_TRM_FEEDBACK_DTL.WORKFLOW_COMPLETION_STATUS = "1";
                    hR_TRM_FEEDBACK_DTL.ENABLED_FLAG = FINAppConstants.Y;

                    //if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    //{
                    //    tmpChildEntity.Add(new Tuple<object, string>(hR_TRM_FEEDBACK_DTL, "D"));
                    //}
                    //else
                    //{
                    if (dtGridData.Rows[iLoop]["FB_DTL_ID"].ToString() != "0" && dtGridData.Rows[iLoop]["FB_DTL_ID"].ToString() != string.Empty)
                    {
                        hR_TRM_FEEDBACK_DTL.FB_DTL_ID = dtGridData.Rows[iLoop]["FB_DTL_ID"].ToString();
                        hR_TRM_FEEDBACK_DTL.MODIFIED_BY = this.LoggedUserName;
                        hR_TRM_FEEDBACK_DTL.MODIFIED_DATE = DateTime.Today;

                        tmpChildEntity.Add(new Tuple<object, string>(hR_TRM_FEEDBACK_DTL, "U"));
                    }
                    else
                    {
                        hR_TRM_FEEDBACK_DTL.FB_DTL_ID = FINSP.GetSPFOR_SEQCode("HR_061_D".ToString(), false, true);
                        hR_TRM_FEEDBACK_DTL.CREATED_BY = this.LoggedUserName;
                        hR_TRM_FEEDBACK_DTL.CREATED_DATE = DateTime.Today;
                        //DBMethod.SaveEntity<GL_ACCT_CODE_SEGMENTS>(gL_ACCT_CODE_SEGMENTS);
                        tmpChildEntity.Add(new Tuple<object, string>(hR_TRM_FEEDBACK_DTL, "A"));
                    }
                    //}
                }

                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            FIN.BLL.HR.Competency_BLL.SavePCEntity<HR_TRM_FEEDBACK_HDR, HR_TRM_FEEDBACK_DTL>(hR_TRM_FEEDBACK_HDR, tmpChildEntity, hR_TRM_FEEDBACK_DTL);
                            savedBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {
                            FIN.BLL.HR.Competency_BLL.SavePCEntity<HR_TRM_FEEDBACK_HDR, HR_TRM_FEEDBACK_DTL>(hR_TRM_FEEDBACK_HDR, tmpChildEntity, hR_TRM_FEEDBACK_DTL, true);
                            savedBool = true;
                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TFBEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void ddlSchedule_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                FillProgram();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TFB_SCHEDULE", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void FillProgram()
        {
            FIN.BLL.HR.TrainingSchedule_BLL.fn_GetProgram4Shedule(ref ddlProgram, ddlSchedule.SelectedValue.ToString());
        }
        protected void ddlProgram_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                FillCourse();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TFB_PROGRAM", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void FillCourse()
        {
            FIN.BLL.HR.TrainingSchedule_BLL.fn_GetCourse4SheduleProg(ref ddlCourse, ddlSchedule.SelectedValue, ddlProgram.SelectedValue.ToString());
        }
        protected void ddlCourse_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                FillSubject();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TS_ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void FillSubject()
        {
            FIN.BLL.HR.TrainingSchedule_BLL.fn_GetSubject4ScheduleprogCourse(ref ddlSubject, ddlSchedule.SelectedValue, ddlProgram.SelectedValue, ddlCourse.SelectedValue.ToString());
        }
        protected void ddlSession_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                DataTable dtData = new DataTable();

                // Trainer_BLL.GetTrainer(ref ddlTrainerName);


                dtData = DBMethod.ExecuteQuery(TrainingSchedule_DAL.GetTrngSchDtlID4ScheduleprogCourseSubjSession(ddlSchedule.SelectedValue, ddlProgram.SelectedValue, ddlCourse.SelectedValue.ToString(), ddlSubject.SelectedValue, ddlSession.SelectedValue)).Tables[0];
                if (dtData != null)
                {
                    if (dtData.Rows.Count > 0)
                    {
                        txtTrainingDate.Text = DBMethod.ConvertDateToString(dtData.Rows[0][FINColumnConstants.TRN_DATE].ToString());
                        ddlTrainerName.SelectedValue = dtData.Rows[0][FINColumnConstants.TRNR_ID].ToString();
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TS_ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        protected void btnYes_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    DBMethod.DeleteEntity<HR_TRM_FEEDBACK_HDR>(hR_TRM_FEEDBACK_HDR);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }
        #endregion


        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            try
            {
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = -1;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_ROW_CNCL_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }


        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_ROW_CMD", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {
            System.Collections.SortedList slControls = new System.Collections.SortedList();

            TextBox txtDesc1 = gvr.FindControl("txtDesc") as TextBox;
            TextBox txtComments1 = gvr.FindControl("txtComment") as TextBox;
            DropDownList ddlTrainingTyp = gvr.FindControl("ddlTrainingTyp") as DropDownList;

            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList[FINColumnConstants.FB_HDR_ID] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);
            }

            ErrorCollection.Clear();
            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));
            string strCtrlTypes = string.Empty;
            string strMessage = string.Empty;

            slControls[0] = ddlTrainingTyp;
            slControls[1] = txtDesc1;
            slControls[2] = txtComments1;

            strCtrlTypes = FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX;
            strMessage = Prop_File_Data["Training_Type_P"] + " ~ " + Prop_File_Data["Description_P"] + " ~ " + Prop_File_Data["Comment_P"] + "";
            //strMessage = "Description ~ Comment ";

            ErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);
            if (ErrorCollection.Count > 0)
                return drList;

            drList[FINColumnConstants.LOOKUP_ID] = ddlTrainingTyp.SelectedValue;
            drList[FINColumnConstants.LOOKUP_NAME] = ddlTrainingTyp.SelectedItem.Text;

            drList[FINColumnConstants.FB_DESC] = txtDesc1.Text;
            drList[FINColumnConstants.FB_COMMENTS] = txtComments1.Text;

            return drList;
        }

        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_ROW_UP", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                //drList[FINColumnConstants.DELETED] = FINAppConstants.Y;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_ROW_DEL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_ROW_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_RowCreated", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    //if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    //{
                    //    e.Row.Visible = false;
                    //}

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_RDB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }



        #endregion

        protected void ddlEmployee_SelectedIndexChanged(object sender, EventArgs e)
        {
            fillTrainer();
        }

        private void fillTrainer()
        {
            Trainer_BLL.GetTrainer_frTrainingFeedback(ref ddlTrainerName,ddlEmployee.SelectedValue);
        }
    }
}