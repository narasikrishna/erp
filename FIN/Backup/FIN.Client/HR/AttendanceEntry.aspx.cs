﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.DAL.GL;
using FIN.BLL;
using FIN.BLL.GL;
using FIN.BLL.HR;
using VMVServices.Web;

namespace FIN.Client.HR
{
    public partial class AttendanceEntry : PageBase
    {
        HR_TRM_ATTEND_HDR hR_TRM_ATTEND_HDR = new HR_TRM_ATTEND_HDR();
        HR_TRM_ATTEND_DTL hR_TRM_ATTEND_DTL = new HR_TRM_ATTEND_DTL();
        HR_TRM_SCHEDULE_DTL hR_TRM_SCHEDULE_DTL = new HR_TRM_SCHEDULE_DTL();

        DataTable dtGridData = new DataTable();
        Boolean bol_rowVisiable;
        Boolean savedBool;

        string ProReturn = null;
        string TrndSchDtlID = null;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TAE", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));


            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
        }

        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        private void FillComboBox()
        {
            FIN.BLL.HR.TrainingAttendance_BLL.fn_GetSchedule(ref ddlScheduleName);

            //FIN.BLL.HR.Employee_BLL.GetEmployeeName(ref ddlEmployee);
        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                Startup();
                FillComboBox();
                EntityData = null;
                Session[FINSessionConstants.GridData] = null;

                //dtGridData = FIN.BLL.HR.TrainingAttendance_BLL.getChildEntityDet(Master.StrRecordId);
                //BindGrid(dtGridData);

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    using (IRepository<HR_TRM_ATTEND_HDR> userCtx = new DataRepository<HR_TRM_ATTEND_HDR>())
                    {
                        hR_TRM_ATTEND_HDR = userCtx.Find(r =>
                            (r.ATT_HDR_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = hR_TRM_ATTEND_HDR;
                    txtAttendanceId.Text = hR_TRM_ATTEND_HDR.ATT_HDR_ID;

                    if (hR_TRM_ATTEND_HDR.ATT_DATE != null)
                    {
                        txtDate.Text = DBMethod.ConvertDateToString(hR_TRM_ATTEND_HDR.ATT_DATE.ToString());
                    }

                    ddlScheduleName.SelectedValue = hR_TRM_ATTEND_HDR.TRN_SCH_HDR_ID;
                    Program4Shedule();

                    using (IRepository<HR_TRM_SCHEDULE_DTL> userCtx = new DataRepository<HR_TRM_SCHEDULE_DTL>())
                    {
                        hR_TRM_SCHEDULE_DTL = userCtx.Find(r =>
                            (r.TRN_SCH_DTL_ID == hR_TRM_ATTEND_HDR.TRN_SCH_DTL_ID)
                            ).SingleOrDefault();
                    }

                    ddlProgram.SelectedValue = hR_TRM_SCHEDULE_DTL.PROG_ID;
                    Course4Shedule();
                    ddlCourse.SelectedValue = hR_TRM_SCHEDULE_DTL.PROG_COURSE_ID;
                    Subject4Shedule();
                    ddlSubject.SelectedValue = hR_TRM_SCHEDULE_DTL.SUBJECT_ID;
                    FillSession();
                    ddlSession.SelectedValue = hR_TRM_SCHEDULE_DTL.TRN_SESSION;
                    hf_TrngSchDtlId.Value = hR_TRM_ATTEND_HDR.TRN_SCH_DTL_ID;
                    getEnrollID();
                    
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TAEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);

                }
            }

        }

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TAE_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();
                DropDownList ddlEmpName = tmpgvr.FindControl("ddlEmployeeName") as DropDownList;
                FIN.BLL.HR.Employee_BLL.GetEmployeeName(ref ddlEmpName);

                if (gvData.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                {
                    ddlEmpName.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["ATT_EMP_ID"].ToString();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TAEFillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        #endregion
        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                System.Collections.SortedList slControls = new System.Collections.SortedList();

                slControls[0] = txtDate;
                slControls[1] = ddlScheduleName;
                slControls[2] = ddlProgram;
                slControls[3] = ddlCourse;
                slControls[4] = ddlSubject;
                slControls[5] = ddlSession;
                Dictionary<string, string> Prop_File_Data;
                Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));
                string strCtrlTypes = FINAppConstants.TEXT_BOX + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST;
                string strMessage = Prop_File_Data["Date_P"] + " ~ " + Prop_File_Data["Schedule_P"] + " ~ " + Prop_File_Data["Program_P"] + " ~ " + Prop_File_Data["Course_P"] + " ~ " + Prop_File_Data["Subject_P"] + " ~ " + Prop_File_Data["Session_P"] + "";
                //string strMessage = "Date ~ Schedule ~ Program ~ Course ~ Subject ~ Session";

                EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

                if (EmptyErrorCollection.Count > 0)
                {
                    ErrorCollection = EmptyErrorCollection;
                    return;
                }

                ErrorCollection.Clear();
                AssignToBE();
                //ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session[FINSessionConstants.GridData], "Training Attendance");

                //if (ErrorCollection.Count > 0)
                //{
                //    return;
                //}

                if (savedBool)
                {
                    txtAttendanceId.Text = hR_TRM_ATTEND_HDR.ATT_HDR_ID;
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    hR_TRM_ATTEND_HDR = (HR_TRM_ATTEND_HDR)EntityData;
                }

                if (txtDate.Text != string.Empty)
                {
                    hR_TRM_ATTEND_HDR.ATT_DATE = DBMethod.ConvertStringToDate(txtDate.Text.ToString());
                }

                hR_TRM_ATTEND_HDR.TRN_SCH_HDR_ID = ddlScheduleName.SelectedValue.ToString();
                hR_TRM_ATTEND_HDR.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                getTrngSchDtlID();
                hR_TRM_ATTEND_HDR.TRN_SCH_DTL_ID = hf_TrngSchDtlId.Value;
                hR_TRM_ATTEND_HDR.ATT_ORG_ID = VMVServices.Web.Utils.OrganizationID;
                hR_TRM_ATTEND_HDR.ATTRIBUTE1 = hf_ENRL_ID.Value;
                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    hR_TRM_ATTEND_HDR.MODIFIED_BY = this.LoggedUserName;
                    hR_TRM_ATTEND_HDR.MODIFIED_DATE = DateTime.Today;
                }
                else
                {
                    hR_TRM_ATTEND_HDR.ATT_HDR_ID = FINSP.GetSPFOR_SEQCode("HR_060_M".ToString(), false, true);
                    //hR_TRM_FEEDBACK_HDR.PK_ID = DBMethod.GetPrimaryKeyValue(FINSequenceConstant.hR_TRM_SCHEDULE_HDR_SEQ);
                    hR_TRM_ATTEND_HDR.CREATED_BY = this.LoggedUserName;
                    hR_TRM_ATTEND_HDR.CREATED_DATE = DateTime.Today;
                    txtAttendanceId.Text = hR_TRM_ATTEND_HDR.ATT_HDR_ID;
                }

                //ProReturn = FIN.DAL.HR.TrainingAttendance_DAL.GetSPFOR_ERR_MGR_CATEGORY(Master.FormCode, hR_TRM_ATTEND_HDR.TRN_SCH_HDR_ID, hR_TRM_ATTEND_HDR.);

                //if (ProReturn != string.Empty)
                //{
                //    if (ProReturn != "0")
                //    {
                //        ErrorCollection.Add("ITEMNAME", ProReturn);
                //        if (ErrorCollection.Count > 0)
                //        {
                //            return;
                //        }
                //    }
                //}

                hR_TRM_ATTEND_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_TRM_ATTEND_HDR.ATT_HDR_ID);

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                var tmpChildEntity = new List<Tuple<object, string>>();

                for (int iLoop = 0; iLoop < gvData.Rows.Count; iLoop++)
                {
                    hR_TRM_ATTEND_DTL = new HR_TRM_ATTEND_DTL();
                    if (gvData.DataKeys[iLoop].Values["ATT_DTL_ID"].ToString() != "0" && gvData.DataKeys[iLoop].Values["ATT_DTL_ID"].ToString() != string.Empty)
                    {
                        using (IRepository<HR_TRM_ATTEND_DTL> userCtx = new DataRepository<HR_TRM_ATTEND_DTL>())
                        {
                            hR_TRM_ATTEND_DTL = userCtx.Find(r =>
                                (r.ATT_DTL_ID == gvData.DataKeys[iLoop].Values["ATT_DTL_ID"].ToString())
                                ).SingleOrDefault();
                        }
                    }

                    CheckBox chk_attemp = (CheckBox)gvData.Rows[iLoop].FindControl("chkSelect");
                    if (chk_attemp.Checked)
                    {

                        hR_TRM_ATTEND_DTL.ATT_EMP_ID = gvData.DataKeys[iLoop].Values["EMP_ID"].ToString();

                        hR_TRM_ATTEND_DTL.ATT_HDR_ID = hR_TRM_ATTEND_HDR.ATT_HDR_ID;
                        hR_TRM_ATTEND_DTL.WORKFLOW_COMPLETION_STATUS = "1";
                        hR_TRM_ATTEND_DTL.ENABLED_FLAG = FINAppConstants.Y;

                        //if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                        //{
                        //    tmpChildEntity.Add(new Tuple<object, string>(hR_TRM_FEEDBACK_DTL, "D"));
                        //}
                        //else
                        //{
                        if (gvData.DataKeys[iLoop].Values["ATT_DTL_ID"].ToString() != "0" && gvData.DataKeys[iLoop].Values["ATT_DTL_ID"].ToString() != string.Empty)
                        {
                            hR_TRM_ATTEND_DTL.ATT_DTL_ID = gvData.DataKeys[iLoop].Values["ATT_DTL_ID"].ToString();
                            hR_TRM_ATTEND_DTL.MODIFIED_BY = this.LoggedUserName;
                            hR_TRM_ATTEND_DTL.MODIFIED_DATE = DateTime.Today;

                            tmpChildEntity.Add(new Tuple<object, string>(hR_TRM_ATTEND_DTL, "U"));
                        }
                        else
                        {
                            hR_TRM_ATTEND_DTL.ATT_DTL_ID = FINSP.GetSPFOR_SEQCode("HR_060_D".ToString(), false, true);
                            hR_TRM_ATTEND_DTL.CREATED_BY = this.LoggedUserName;
                            hR_TRM_ATTEND_DTL.CREATED_DATE = DateTime.Today;
                            //DBMethod.SaveEntity<GL_ACCT_CODE_SEGMENTS>(gL_ACCT_CODE_SEGMENTS);
                            tmpChildEntity.Add(new Tuple<object, string>(hR_TRM_ATTEND_DTL, "A"));
                        }
                    }
                    else
                    {
                        if (gvData.DataKeys[iLoop].Values["ATT_DTL_ID"].ToString() != "0" && gvData.DataKeys[iLoop].Values["ATT_DTL_ID"].ToString() != string.Empty)
                        {
                            DBMethod.DeleteEntity<HR_TRM_ATTEND_DTL>(hR_TRM_ATTEND_DTL);
                        }
                    }
                    //}
                }

                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            FIN.BLL.HR.Competency_BLL.SavePCEntity<HR_TRM_ATTEND_HDR, HR_TRM_ATTEND_DTL>(hR_TRM_ATTEND_HDR, tmpChildEntity, hR_TRM_ATTEND_DTL);
                            savedBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {
                            FIN.BLL.HR.Competency_BLL.SavePCEntity<HR_TRM_ATTEND_HDR, HR_TRM_ATTEND_DTL>(hR_TRM_ATTEND_HDR, tmpChildEntity, hR_TRM_ATTEND_DTL, true);
                            savedBool = true;
                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TAEEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }

        protected void Program4Shedule()
        {
            FIN.BLL.HR.TrainingSchedule_BLL.fn_GetProgram4Shedule(ref ddlProgram, ddlScheduleName.SelectedValue.ToString());
        }
        protected void ddlSchedule_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ////FIN.BLL.HR.TrainingAttendance_BLL.fn_GetProgram(ref ddlProgram, ddlScheduleName.SelectedValue.ToString());
                //FIN.BLL.HR.TrainingSchedule_BLL.fn_GetProgram4Shedule(ref ddlProgram, ddlScheduleName.SelectedValue.ToString());
                Program4Shedule();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TAE_SCHEDULE", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void Course4Shedule()
        {
            FIN.BLL.HR.TrainingSchedule_BLL.fn_GetCourse4SheduleProg(ref ddlCourse, ddlScheduleName.SelectedValue.ToString(), ddlProgram.SelectedValue.ToString());
        }
        protected void ddlProgram_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ////FIN.BLL.HR.TrainingAttendance_BLL.fn_GetCourse(ref ddlCourse, ddlProgram.SelectedValue.ToString());
                //FIN.BLL.HR.TrainingSchedule_BLL.fn_GetCourse4SheduleProg(ref ddlCourse, ddlScheduleName.SelectedValue.ToString(), ddlProgram.SelectedValue.ToString());
                Course4Shedule();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TAE_PROGRAM", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void Subject4Shedule()
        {
            FIN.BLL.HR.TrainingSchedule_BLL.fn_GetSubject4ScheduleprogCourse(ref ddlSubject, ddlScheduleName.SelectedValue.ToString(), ddlProgram.SelectedValue.ToString(), ddlCourse.SelectedValue.ToString());
        }
        protected void ddlCourse_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ////FIN.BLL.HR.TrainingAttendance_BLL.fn_GetSubject(ref ddlSubject, ddlCourse.SelectedValue.ToString());
                //FIN.BLL.HR.TrainingSchedule_BLL.fn_GetSubject4ScheduleprogCourse(ref ddlSubject, ddlScheduleName.SelectedValue.ToString(),ddlProgram.SelectedValue.ToString(), ddlCourse.SelectedValue.ToString());
                Subject4Shedule();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TAE_COURSE", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void getTrngSchDtlID()
        {
            try
            {
                ErrorCollection.Clear();

                DataTable dtTrngDtlID = new DataTable();
                dtTrngDtlID = DBMethod.ExecuteQuery(FIN.DAL.HR.TrainingSchedule_DAL.GetTrngSchDtlID4ScheduleprogCourseSubjSession(ddlScheduleName.SelectedValue.ToString(),
                              ddlProgram.SelectedValue.ToString(), ddlCourse.SelectedValue.ToString(), ddlSubject.SelectedValue.ToString(), ddlSession.SelectedItem.Text.ToString())).Tables[0];

                if (dtTrngDtlID.Rows.Count > 0)
                {
                    hf_TrngSchDtlId.Value = dtTrngDtlID.Rows[0]["TRN_SCH_DTL_ID"].ToString();
                    txtDate.Text = dtTrngDtlID.Rows[0]["TRN_DATE"].ToString(); 
                }
                else
                {
                    hf_TrngSchDtlId.Value = "0";
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TAE_SESSION", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }


        protected void btnYes_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    DBMethod.DeleteEntity<HR_TRM_ATTEND_HDR>(hR_TRM_ATTEND_HDR);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TAE_DELETE", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        #endregion


        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            try
            {
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = -1;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TAE_ROW_CNCL_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }


        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }

                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TAE_ROW_CMD", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {
            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));
            System.Collections.SortedList slControls = new System.Collections.SortedList();

            DropDownList ddlEmployeeName = gvr.FindControl("ddlEmployeeName") as DropDownList;

            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList[FINColumnConstants.ATT_DTL_ID] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);
            }

            ErrorCollection.Clear();

            string strCtrlTypes = string.Empty;
            string strMessage = string.Empty;

            slControls[0] = ddlEmployeeName;

            strCtrlTypes = FINAppConstants.DROP_DOWN_LIST;
            strMessage = Prop_File_Data["Employee_Name_P"] + "";
            //strMessage = "Employee Name";

            ErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);
            if (ErrorCollection.Count > 0)
                return drList;

            drList[FINColumnConstants.ATT_EMP_ID] = ddlEmployeeName.SelectedValue;
            drList[FINColumnConstants.EMP_NAME] = ddlEmployeeName.SelectedItem.Text;

            return drList;
        }

        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_ROW_UP", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }


        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                //drList[FINColumnConstants.DELETED] = FINAppConstants.Y;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_ROW_DEL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_ROW_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_RowCreated", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    //if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    //{
                    //    e.Row.Visible = false;
                    //}

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_RDB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }

        #endregion

        protected void ddlSubject_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillSession();

        }
        private void FillSession()
        {
            Lookup_BLL.GetLookUpValues(ref ddlSession, "Session");
        }


        protected void ddlSession_SelectedIndexChanged(object sender, EventArgs e)
        {
            getTrngSchDtlID();
            getEnrollID();

        }
        private void getEnrollID()
        {
            HR_TRM_ENROLL_HDR hR_TRM_ENROLL_HDR = new HR_TRM_ENROLL_HDR();
            using (IRepository<HR_TRM_ENROLL_HDR> userCtx = new DataRepository<HR_TRM_ENROLL_HDR>())
            {
                hR_TRM_ENROLL_HDR = userCtx.Find(r =>
                    (r.TRN_SCH_DTL_ID == hf_TrngSchDtlId.Value)
                    ).SingleOrDefault();
            }
            if (hR_TRM_ENROLL_HDR != null)
            {
                hf_ENRL_ID.Value = hR_TRM_ENROLL_HDR.ENRL_HDR_ID;
                LoadEnrolledEmployee();
            }
            else
            {
                hf_ENRL_ID.Value = "0";
                ErrorCollection.Add("NOTYETENROOLED", "Selected Program - Subject, Employees are not Enrolled... ");
                Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
            }
        }
        private void LoadEnrolledEmployee()
        {

            HR_TRM_ATTEND_HDR hR_TRM_ATTEND_HDR = new HR_TRM_ATTEND_HDR();
            using (IRepository<HR_TRM_ATTEND_HDR> userCtx = new DataRepository<HR_TRM_ATTEND_HDR>())
            {
                hR_TRM_ATTEND_HDR = userCtx.Find(r =>
                    (r.TRN_SCH_DTL_ID == hf_TrngSchDtlId.Value)
                    ).SingleOrDefault();
            }
            if (hR_TRM_ATTEND_HDR != null)
            {
                Master.Mode = FINAppConstants.Update;
                Master.StrRecordId = hR_TRM_ATTEND_HDR.ATT_HDR_ID;
                txtDate.Text = DBMethod.ConvertDateToString(hR_TRM_ATTEND_HDR.ATT_DATE.ToShortDateString());
                txtAttendanceId.Text = hR_TRM_ATTEND_HDR.ATT_HDR_ID;
            }

            DataTable dt_data = Attendance_BLL.fngetEnrolledEmployee(hf_ENRL_ID.Value);
            gvData.DataSource = dt_data;
            gvData.DataBind();

        }
    }
}