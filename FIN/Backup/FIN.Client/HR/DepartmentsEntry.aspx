﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true" CodeBehind="DepartmentsEntry.aspx.cs" Inherits="FIN.Client.HR.DepartmentsEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1000px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divClear_10">
        </div>
        <div class="LNOrient">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                Width="98%" DataKeyNames="DEPT_ID,VALUE_KEY_ID,SEGMENT_ID,SEGMENT_VALUE_ID,DELETED" OnRowCancelingEdit="gvData_RowCancelingEdit"
                OnRowCommand="gvData_RowCommand" OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound"
                OnRowDeleting="gvData_RowDeleting" OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating"
                ShowFooter="True">
                <Columns>
                    <asp:TemplateField HeaderText="Department Name">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtdeptName"  MaxLength="100" runat="server" CssClass="EntryFont RequiredField  txtBox_en" Width="97%"
                                Text='<%# Eval("DEPT_NAME") %>'></asp:TextBox>
                                <%-- <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars="/"
                                FilterType="LowercaseLetters,Numbers,UppercaseLetters" TargetControlID="txtdeptName" />--%>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtdeptName"  MaxLength="100" runat="server" TabIndex="1"
                                CssClass="EntryFont RequiredField   txtBox_en" Width="97%"></asp:TextBox>
                                 <%--<cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars="/"
                                FilterType="LowercaseLetters,Numbers,UppercaseLetters" TargetControlID="txtdeptName" />--%>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lbldeptName" Style="word-wrap: break-word; white-space: pre-wrap; " Width="130px" runat="server" Text='<%# Eval("DEPT_NAME") %>'></asp:Label>
                        </ItemTemplate>
                      
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" Width="200px" />
                        <FooterStyle Width="200px" />
                    </asp:TemplateField>

                      <asp:TemplateField HeaderText="Department Name(Arabic)">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtdeptNameAR"  MaxLength="100" runat="server" CssClass="EntryFont  txtBox_ol" Width="97%"
                                Text='<%# Eval("DEPT_NAME_OL") %>'></asp:TextBox>
                              
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtdeptNameAR"  MaxLength="100" runat="server" TabIndex="1"
                                CssClass="EntryFont txtBox_ol" Width="97%"></asp:TextBox>
                                
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lbldeptNameAR" Style="word-wrap: break-word; white-space: pre-wrap; " Width="130px" runat="server" Text='<%# Eval("DEPT_NAME_OL") %>'></asp:Label>
                        </ItemTemplate>
                      
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Right"  Width="200px" />
                        <FooterStyle Width="200px" />
                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="Department Group">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddldeptTyp"  runat="server" CssClass="RequiredField EntryFont ddlStype" Width="100%">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddldeptTyp" TabIndex="2" runat="server" CssClass="RequiredField EntryFont ddlStype" Width="100%">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lbldeptTyp" CssClass="adminFormFieldHeading" runat="server" Text='<%# Eval("VALUE_NAME") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                   
                   <asp:TemplateField HeaderText="Cost Segment">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlCostSeg"  runat="server" 
                                CssClass="RequiredField EntryFont ddlStype" Width="100%" AutoPostBack="True" 
                                onselectedindexchanged="ddlCostSeg_SelectedIndexChanged">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlCostSeg" TabIndex="2" runat="server" CssClass="RequiredField EntryFont ddlStype" AutoPostBack="True" 
                                onselectedindexchanged="ddlCostSeg_SelectedIndexChanged" Width="100%">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblCostSeg" CssClass="adminFormFieldHeading" runat="server" Text='<%# Eval("SEGMENT_NAME") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Cost Value">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlCostValue"  runat="server" CssClass="RequiredField EntryFont ddlStype" Width="100%">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlCostValue" TabIndex="2" runat="server" CssClass="RequiredField EntryFont ddlStype" Width="100%">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblCostValue" CssClass="adminFormFieldHeading" runat="server" Text='<%# Eval("SEGMENT_VALUE") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>

                  
                    <asp:TemplateField HeaderText="Active">
                        <EditItemTemplate>
                            <asp:CheckBox ID="chkact" runat="server"  Checked='<%# Convert.ToBoolean(Eval("ENABLED_FLAG")) %>' />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:CheckBox ID="chkact"  runat="server" TabIndex="3" Checked="true" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkact"  runat="server" Checked='<%# Convert.ToBoolean(Eval("ENABLED_FLAG")) %>' Enabled="false" />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" TabIndex="4" runat="server" AlternateText="Edit" CausesValidation="false"
                                ToolTip="Edit" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                            <asp:ImageButton ID="ibtnDelete" TabIndex="5" runat="server" AlternateText="Delete" CausesValidation="false"
                                ToolTip="Delete" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibtnUpdate" TabIndex="4" runat="server" AlternateText="Update" CommandName="Update"
                                ToolTip="Update" Height="20px" ImageUrl="~/Images/Update.png" />
                            <asp:ImageButton ID="ibtnCancel" TabIndex="5" runat="server" AlternateText="Cancel" CausesValidation="false"
                                ToolTip="Cancel" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="ibtnInsert" runat="server" TabIndex="6" AlternateText="Add"
                                CommandName="FooterInsert" ToolTip="Add" ImageUrl="~/Images/Add.jpg" />
                        </FooterTemplate>
                        <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>

      <%--  <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 500px" id="lblDept">
                *SSDP - Social Security Deduction Percentage
            </div>
        </div>--%>

        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"  TabIndex="7"  />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" 
                            TabIndex="8"  />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" OnClick="btnCancel_Click"
                            TabIndex="9" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="10" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table width="100%">
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
       <script src="../LanguageScript/HR/HRChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
