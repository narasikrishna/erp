﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="TrainingBudgetPlanEntry.aspx.cs" Inherits="FIN.Client.HR.TrainingBudgetPlanEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1000px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblappraisal">
                Appraisal
            </div>
            <div class="divtxtBox  LNOrient" style="width: 155px">
                <asp:DropDownList ID="ddlAppraisal" runat="server" TabIndex="1" CssClass="validate[required] RequiredField ddlStype"
                    AutoPostBack="True" OnSelectedIndexChanged="ddlAppraisal_SelectedIndexChanged">
                    <asp:ListItem Value="">---Select---</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="LNOrient">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                Width="1000px" DataKeyNames="TRN_BUDGET_PLAN_ID,EMP_ID,COURSE_ID,quarter_id,rating_id"
                OnRowDataBound="gvData_RowDataBound" ShowFooter="false">
                <Columns>
                    <asp:BoundField DataField="emp_name" HeaderText="Employee" ItemStyle-Width="120px" />
                    <asp:TemplateField HeaderText="Course Details">
                        <ItemTemplate>
                            <asp:DropDownList ID="ddlCourseName" CssClass="validate[required] RequiredField ddlStype"
                                Width="120px" runat="server" TabIndex="2">
                            </asp:DropDownList>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Organiser Name">
                        <ItemTemplate>
                            <asp:TextBox ID="txtOrganiserName" CssClass="validate[required]  RequiredField txtBox"
                                MaxLength="50" Width="100px" Text='<%# Eval("TRN_ORGANISER") %>' runat="server"
                                TabIndex="3"></asp:TextBox>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Quarter">
                        <ItemTemplate>
                            <asp:DropDownList ID="ddlQuarter" CssClass="validate[required] RequiredField ddlStype"
                                runat="server" TabIndex="4" >
                            </asp:DropDownList>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Extension Cost">
                        <ItemTemplate>
                            <asp:TextBox ID="txtExtensionCost" CssClass="validate[required] RequiredField txtBox_N"
                                MaxLength="13" Width="100px" Text='<%# Eval("TRN_ESTIMATION_CST") %>' runat="server"
                                TabIndex="5"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredeTexsdfetBoxExtender23" runat="server" FilterType="Numbers,Custom"
                                ValidChars=".," TargetControlID="txtExtensionCost" />
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Periods(Hrs/Days)">
                        <ItemTemplate>
                            <asp:TextBox ID="txtPeriods" CssClass="validate[required] RequiredField txtBox_N"
                                MaxLength="3" Width="100px" Text='<%# Eval("TRN_PERIODS") %>' runat="server" TabIndex="6"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FiltsdfetBoxExtender23" runat="server" FilterType="Numbers"
                                TargetControlID="txtPeriods" />
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="No of People">
                        <ItemTemplate>
                            <asp:TextBox ID="txtNoOfPeople" CssClass="validate[required] RequiredField txtBox_N"
                                Width="100px" Text='<%# Eval("TRN_NO_OF_PEOPLES") %>' MaxLength="5" runat="server"
                                TabIndex="7"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="Filtsdftessnder23" runat="server" FilterType="Numbers"
                                TargetControlID="txtNoOfPeople" />
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Covered By KFAS">
                        <ItemTemplate>
                            <asp:TextBox ID="txtCoveredByKFAS" MaxLength="13" CssClass="validate[required] RequiredField txtBox_N"
                                Width="100px" Text='<%# Eval("TRN_COVERED_BY_KFAS") %>' runat="server" TabIndex="8"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="Filted11TextBoxExtender23" runat="server" FilterType="Numbers,Custom"
                                ValidChars=".," TargetControlID="txtCoveredByKFAS" />
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Type of Trainings">
                        <ItemTemplate>
                            <asp:DropDownList ID="ddlTypeOfTraings" CssClass="validate[required] RequiredField ddlStype"
                                Width="120px" runat="server" TabIndex="9" AutoPostBack="True">
                            </asp:DropDownList>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                </Columns>
              
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" TabIndex="12"
                            OnClick="btnSave_Click" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="13" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Reset" CssClass="btn" TabIndex="14"
                            OnClick="btnCancel_Click" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="15" />
                    </td>
                </tr>
            </table>
        </div>
        <%-- <div id="divDelete">
        <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
            PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
        </cc2:ModalPopupExtender>
        <asp:Panel ID="pnlConfirm" runat="server">
            <div class="ConfirmForm">
                <table>
                    <tr class="ConfirmHeading" style="width: 100%">
                        <td>
                            <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                Width="60px" />
                            &nbsp;
                            <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                        </td>
                    </tr>
                </table>
            </div>
        </asp:Panel>
    </div>--%>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/HR/HRChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
