﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.DAL.GL;
using FIN.BLL;
using FIN.BLL.GL;
using FIN.BLL.HR;
using VMVServices.Web;


namespace FIN.Client.HR
{
    public partial class LeaveDepartmentEntry : PageBase
    {
        HR_LEAVE_DEPT HR_LEAVE_DEPT = new HR_LEAVE_DEPT();

        DataTable dtData = new DataTable();
        Boolean bol_rowVisiable;
        Boolean savedBool;
        DataTable dtGridData = new DataTable();
        string ProReturn = null;


        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));


            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
        }

        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        private void FillComboBox()
        {
            AccountingCalendar_BLL.GetCalAcctYear(ref ddlFinancialYear);
            Department_BLL.GetDepartmentName(ref ddlDepartment);
        }
        private void FillFromToDate()
        {
            try
            {
                if (ddlFinancialYear.SelectedValue != null)
                {
                    dtData = DBMethod.ExecuteQuery(AccountingCalendar_DAL.GetStartdtEnddtfrcalyear((ddlFinancialYear.SelectedValue.ToString()))).Tables[0];
                    if (dtData != null)
                    {
                        if (dtData.Rows.Count > 0)
                        {
                            if (dtData.Rows[0]["cal_eff_start_dt"].ToString().Length > 0)
                            {
                                txtFromDate.Text = DateTime.Parse(dtData.Rows[0][FINColumnConstants.CAL_EFF_START_DT].ToString()).ToString("dd/MM/yyyy");
                            }
                            if (dtData.Rows[0]["call_eff_end_dt"].ToString().Length > 0)
                            {
                                txtToDate.Text = DateTime.Parse(dtData.Rows[0][FINColumnConstants.CAL_EFF_END_DT].ToString()).ToString("dd/MM/yyyy");
                            }
                        }
                    }

                    //dtGridData = DBMethod.ExecuteQuery(LeaveDepartment_DAL.GetLeaveDepartmentDtls(Master.StrRecordId)).Tables[0];
                    //BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATGd_PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        protected void ddlFinancialYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                FillFromToDate();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// .RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();

                EntityData = null;
                // Session[FINSessionConstants.GridData] = null;

                FillComboBox();
                dtGridData = DBMethod.ExecuteQuery(LeaveDepartment_DAL.GetLeaveDepartmentDtls(Master.StrRecordId)).Tables[0];
                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    ddlFinancialYear.Enabled = false;
                    HR_LEAVE_DEPT = LeaveDepartment_BLL.getClassEntity(Master.StrRecordId);
                    EntityData = HR_LEAVE_DEPT;
                    ddlFinancialYear.SelectedValue = HR_LEAVE_DEPT.FISCAL_YEAR.ToString();
                    FillFromToDate();
                    ddlDepartment.SelectedValue = HR_LEAVE_DEPT.DEPT_ID.ToString();
                    LoadDepartment_Leave();
                    ddlFinancialYear.Enabled = false;
                    ddlDepartment.Enabled = false;
                }

                
                BindGrid(dtGridData);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Vaca_Entry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);

                }
            }

        }

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr["ENABLED_FLAG"] = "FALSE";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddlLeave = tmpgvr.FindControl("ddlLeave") as DropDownList;
                LeaveDefinition_BLL.GetLeaveBasedFiscalYear(ref ddlLeave, ddlFinancialYear.SelectedValue.ToString());
                //LeaveDepartment_BLL.GetLeaveBasedFiscalYrOrgID(ref ddlLeave, ddlFinancialYear.SelectedValue.ToString());


                if (gvData.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                {
                    ddlLeave.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.LEAVE_ID].ToString();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RLD_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion
        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session[FINSessionConstants.GridData], "Leave Department ");

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                AssignToBE();

                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);
                    btnSave.Attributes.Add("onclick", "reload();");
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                var tmpChildEntity = new List<Tuple<object, string>>();

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    HR_LEAVE_DEPT = new HR_LEAVE_DEPT();

                    if ((dtGridData.Rows[iLoop][FINColumnConstants.DEPT_LEAVE_ID].ToString()) != "0" && dtGridData.Rows[iLoop][FINColumnConstants.DEPT_LEAVE_ID].ToString() != string.Empty)
                    {
                        HR_LEAVE_DEPT = LeaveDepartment_BLL.getClassEntity(dtGridData.Rows[iLoop][FINColumnConstants.DEPT_LEAVE_ID].ToString());

                    }
                    HR_LEAVE_DEPT.LEAVE_ID = dtGridData.Rows[iLoop][FINColumnConstants.LEAVE_ID].ToString();
                    HR_LEAVE_DEPT.DEPT_ID = ddlDepartment.SelectedValue.ToString();
                    HR_LEAVE_DEPT.FISCAL_YEAR = (ddlFinancialYear.SelectedValue.ToString());

                    if (dtGridData.Rows[iLoop][FINColumnConstants.EFFECTIVE_FROM_DT] != DBNull.Value)
                    {
                        HR_LEAVE_DEPT.EFFECTIVE_FROM_DT = DateTime.Parse(dtGridData.Rows[iLoop][FINColumnConstants.EFFECTIVE_FROM_DT].ToString());
                    }

                    if (dtGridData.Rows[iLoop][FINColumnConstants.EFFECTIVE_TO_DT] != DBNull.Value)
                    {
                        HR_LEAVE_DEPT.EFFECTIVE_TO_DT = DateTime.Parse(dtGridData.Rows[iLoop][FINColumnConstants.EFFECTIVE_TO_DT].ToString());
                    }

                    HR_LEAVE_DEPT.ENABLED_FLAG = dtGridData.Rows[iLoop][FINColumnConstants.ENABLED_FLAG].ToString() == FINAppConstants.TRUEFLAG ? FINAppConstants.EnabledFlag : FINAppConstants.DisabledFlag;

                    HR_LEAVE_DEPT.ORG_ID = VMVServices.Web.Utils.OrganizationID.ToString();


                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        tmpChildEntity.Add(new Tuple<object, string>(HR_LEAVE_DEPT, "D"));
                    }
                    else
                    {


                        // Duplicate Validation Through Backend Package PKG_VALIDATIONS

                        //ProReturn = FIN.DAL.HR.LeaveDepartment_DAL.GetSPFOR_DUPLICATE_CHECK(ddlFinancialYear.SelectedValue, HR_LEAVE_DEPT.DEPT_ID, HR_LEAVE_DEPT.LEAVE_ID, HR_LEAVE_DEPT.DEPT_LEAVE_ID);

                        //if (ProReturn != string.Empty)
                        //{
                        //    if (ProReturn != "0")
                        //    {
                        //        ErrorCollection.Add("LEAVEDEPT", ProReturn);
                        //        if (ErrorCollection.Count > 0)
                        //        {
                        //            return;
                        //        }
                        //    }
                        //}



                        if (dtGridData.Rows[iLoop][FINColumnConstants.DEPT_LEAVE_ID].ToString() != "0" && dtGridData.Rows[iLoop][FINColumnConstants.DEPT_LEAVE_ID].ToString() != string.Empty)
                        {
                            HR_LEAVE_DEPT.DEPT_LEAVE_ID = dtGridData.Rows[iLoop]["DEPT_LEAVE_ID"].ToString();
                            HR_LEAVE_DEPT.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, HR_LEAVE_DEPT.DEPT_LEAVE_ID);
                            HR_LEAVE_DEPT.MODIFIED_BY = this.LoggedUserName;
                            HR_LEAVE_DEPT.MODIFIED_DATE = DateTime.Today;
                            //tmpChildEntity.Add(new Tuple<object, string>(HR_LEAVE_DEPT, FINAppConstants.Update));
                            DBMethod.SaveEntity<HR_LEAVE_DEPT>(HR_LEAVE_DEPT, true);
                            savedBool = true;

                        }
                        else
                        {
                            HR_LEAVE_DEPT.DEPT_LEAVE_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_020.ToString(), false, true);
                            HR_LEAVE_DEPT.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, HR_LEAVE_DEPT.DEPT_LEAVE_ID);
                            HR_LEAVE_DEPT.CREATED_BY = this.LoggedUserName;
                            HR_LEAVE_DEPT.CREATED_DATE = DateTime.Today;
                            //tmpChildEntity.Add(new Tuple<object, string>(HR_LEAVE_DEPT, FINAppConstants.Add));
                            DBMethod.SaveEntity<HR_LEAVE_DEPT>(HR_LEAVE_DEPT);
                            savedBool = true;
                        }


                       
                    }
                }


                //switch (Master.Mode)
                //{
                //    case FINAppConstants.Add:
                //        {
                //            CommonUtils.SaveSingleEntity<HR_LEAVE_DEPT>(tmpChildEntity);
                //            savedBool = true;
                //            break;
                //        }
                //    case FINAppConstants.Update:
                //        {
                //            CommonUtils.SaveSingleEntity<HR_LEAVE_DEPT>(tmpChildEntity, true);
                //            savedBool = true;
                //            break;
                //        }
                //}
                //if (ErrorCollection.Count > 0)
                //{
                //    return;
                //}


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {

                    HR_LEAVE_DEPT.DEPT_LEAVE_ID = (dtGridData.Rows[iLoop]["DEPT_LEAVE_ID"].ToString());
                    DBMethod.DeleteEntity<HR_LEAVE_DEPT>(HR_LEAVE_DEPT);
                }


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion


        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            try
            {
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = -1;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_ROW_CNCL_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_ROW_CMD", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {


            System.Collections.SortedList slControls = new System.Collections.SortedList();

            DropDownList ddlLeave = gvr.FindControl("ddlLeave") as DropDownList;

            TextBox dtpStartDate = gvr.FindControl("dtpStartDate") as TextBox;
            TextBox dtpEndDate = gvr.FindControl("dtpEndDate") as TextBox;

            CheckBox chkact = gvr.FindControl("chkact") as CheckBox;



            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList[FINColumnConstants.DEPT_LEAVE_ID] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }

            slControls[0] = ddlLeave;
            slControls[1] = dtpStartDate;
            slControls[2] = dtpEndDate;
            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));
            ErrorCollection.Clear();
            string strCtrlTypes = FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.TEXT_BOX +"~" + FINAppConstants.DATE_RANGE_VALIDATE;
            string strMessage = Prop_File_Data["Leave_Name_P"] + " ~ " + Prop_File_Data["Start_Date_P"] + " ~ " + Prop_File_Data["End_Date_P"] + "";
            //string strMessage = "Leave name ~ Start Date ~ End Date";
            ErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);
            if (ErrorCollection.Count > 0)
                return drList;

            //ErrorCollection = UserUtility_BLL.DateRangeValidate(DBMethod.ConvertStringToDate(dtpStartDate.Text), DBMethod.ConvertStringToDate(dtpEndDate.Text), "chk");
            //if (ErrorCollection.Count > 0)
            //    return drList;

            ProReturn = FINSP.GetSPFOR_ERR_MGR_CATEGORY_THREE(Master.FormCode, drList[FINColumnConstants.DEPT_LEAVE_ID].ToString(), ddlFinancialYear.SelectedValue, ddlDepartment.SelectedValue, ddlLeave.SelectedValue);
            if (ProReturn != string.Empty)
            {
                if (ProReturn != "0")
                {
                    ErrorCollection.Add("LEAVEDEPARTMENT", ProReturn);
                    if (ErrorCollection.Count > 0)
                    {
                        return drList;
                    }
                }
            }

            string strCondition = "LEAVE_ID='" + ddlLeave.SelectedValue.ToString() + "'";
            strMessage = FINMessageConstatns.RecordAlreadyExists;
            ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            if (ErrorCollection.Count > 0)
            {
                return drList;
            }

          

            drList[FINColumnConstants.LEAVE_ID] = ddlLeave.SelectedValue;
            drList[FINColumnConstants.LEAVE_DESC] = ddlLeave.SelectedItem.Text;

            if (dtpStartDate.Text.ToString().Length > 0)
            {
                drList[FINColumnConstants.EFFECTIVE_FROM_DT] = DBMethod.ConvertStringToDate(dtpStartDate.Text.ToString());
            }

            if (dtpEndDate.Text.ToString().Length > 0)
            {

                drList[FINColumnConstants.EFFECTIVE_TO_DT] = DBMethod.ConvertStringToDate(dtpEndDate.Text.ToString());
            }
            else
            {
                drList[FINColumnConstants.EFFECTIVE_TO_DT] = DBNull.Value;
            }

            drList[FINColumnConstants.ENABLED_FLAG] = chkact.Checked == true ? FINAppConstants.TRUEFLAG : FINAppConstants.FALSEFLAG;

            drList[FINColumnConstants.DELETED] = FINAppConstants.N;


            return drList;



        }



        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_ROW_UP", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_ROW_DEL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_ROW_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_RowCreated", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_RDB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }



        #endregion

        protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {

            LoadDepartment_Leave();

        }

        private void LoadDepartment_Leave()
        {
            dtGridData = DBMethod.ExecuteQuery(LeaveDepartment_DAL.GetLeaveDepartmentDtls_BasedDept_fsyr(ddlDepartment.SelectedValue, ddlFinancialYear.SelectedValue)).Tables[0];
            BindGrid(dtGridData);
        }
    }
}