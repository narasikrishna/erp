﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AttendanceImportEntry.aspx.cs"
    Inherits="FIN.Client.HR.AttendanceImportEntry" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link href="../Styles/main.css" rel="stylesheet" type="text/css" id="MainCss" />
    <link href="../Styles/GridStyle.css" rel="stylesheet" type="text/css" />
    <script src="../Scripts/JQuery/jquery-1.11.0.min.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="divRowContainer">
        <div>
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient">
                <asp:Label runat="server" ID="lblId" Text="Date" class="lblBox LNOrient"></asp:Label>
                <%-- <div class="lblBox LNOrient" runat="server" style="width: 100px" id="lblId">
                Date
            </div>--%>
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <%-- <asp:DropDownList ID="ddlYear" runat="server" onchange="PopulateDays()" />--%>
                <asp:TextBox ID="txtDate" CssClass="validate[required]  RequiredField  txtBox" ValidationGroup="btnimport"
                    runat="server" TabIndex="9"></asp:TextBox><cc2:CalendarExtender ID="CalendarExtender2"
                        runat="server" Format="dd/MM/yyyy" TargetControlID="txtDate">
                    </cc2:CalendarExtender>
            </div>
            <div class="lblBox LNOrient" style="width: 80px" id="Div6">
                <asp:ImageButton ID="btnimport" runat="server" ImageUrl="~/Images/btnImport.png"
                    OnClick="btnimport_Click" Style="border: 0px;" />
                <%--<asp:Button ID="btnimport" runat="server" Text="Import" CssClass="btn" OnClick="btnimport_Click" />--%>
            </div>
            <div id="IDdataImport" runat="server" visible="false" class="divRowContainer">
                <div class="lblBox" style="width: 100px" id="Div4">
                    <br />
                </div>
                <div class="lblBox LNOrient" runat="server" style="width: 300px; color: Green" id="dataImport">
                    Data Imported Successfully
                </div>
            </div>
            <div id="IDNodata" runat="server" visible="false" class="divRowContainer">
                <div class="lblBox LNOrient" style="width: 100px" id="Div8">
                    <br />
                </div>
                <div class="lblBox LNOrient" runat="server" style="width: 300px; color: Green" id="noDataImport">
                    No Data to Import
                </div>
            </div>
        </div>
        <div>
        </div>
        <%--<div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 100px" id="Div1">
                Month
            </div>
            <div class="lblBox" style="float: left; width: 150px" id="Div2">
                <asp:DropDownList ID="ddlMonth" runat="server" onchange="PopulateDays()" />
            </div>
        </div>--%>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="display: none; width: 100px" id="Div5">
                <asp:Button ID="btnshow" runat="server" Text="Show" CssClass="btn" OnClick="btnshow_Click" />
            </div>
        </div>
    </div>
    <div class="divClear_10">
    </div>
    <div class="LNOrient">
        <asp:HiddenField ID="hf_Sel_Lng" Value="" runat="server" />
        <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="DisplayFont Grid"
            DataKeyNames="FLG_LATE,NO_IN_TIME,NO_OUT_TIME" OnRowDataBound="gvData_RowDataBound"
            EmptyDataText="No Record to Found" Width="100%">
            <Columns>
                <asp:BoundField DataField="emp_no" HeaderText="Employee Number" />
                <asp:BoundField DataField="EMP_NAME" HeaderText="Employee Name" />
                <asp:BoundField DataFormatString="{0:dd/MM/yyyy}" DataField="TM_DATE" HeaderText="Date" />
                <asp:BoundField Visible="true" DataField="FLG_LATE" HeaderText="Late" />
                <asp:BoundField Visible="true" DataField="late_in" HeaderText="Late In(In Minutes)">
                    <ItemStyle HorizontalAlign="Right" />
                </asp:BoundField>
                <asp:BoundField Visible="true" DataField="early_out" HeaderText="Early Out(In Minutes)">
                    <ItemStyle HorizontalAlign="Right" />
                </asp:BoundField>
                <asp:BoundField Visible="true" DataField="emp_in_time" HeaderText="Time In" />
                <asp:BoundField Visible="true" DataField="emp_out_time" HeaderText="Time Out" />
                <asp:BoundField Visible="true" DataField="per_from_time" HeaderText="Permission From Time">
                    <ItemStyle HorizontalAlign="left" />
                </asp:BoundField>
                <asp:BoundField Visible="true" DataField="per_to_time" HeaderText="Permission To Time">
                    <ItemStyle HorizontalAlign="left" />
                </asp:BoundField>
                <asp:BoundField Visible="false" DataField="NO_IN_TIME" HeaderText="NO_IN_TIME" />
                <asp:BoundField Visible="false" DataField="NO_OUT_TIME" HeaderText="NO_OUT_TIME" />
                <%--<asp:BoundField Visible="false" DataField="TM_TIME" HeaderText="Time" />--%>
                <%--  <asp:BoundField Visible="false" DataField="TM_MODE" HeaderText="Mode" />  --%>
            </Columns>
            <EmptyDataRowStyle CssClass="EmptyRowStyle" />
            <HeaderStyle CssClass="GridHeader" />
            <AlternatingRowStyle CssClass="GrdAltRow" />
        </asp:GridView>
    </div>
    </form>
</body>

<script type="text/javascript">
    $(document).ready(function () {
        if ($("#hf_Sel_Lng").val() == "AR") {
            document.dir = "rtl",
            $("<link/>", {
                rel: "stylesheet",
                type: "text/css",
                href: "../Styles/Main_R.css"
            }).appendTo("head");
        }
    });
    
    </script>
</html>
