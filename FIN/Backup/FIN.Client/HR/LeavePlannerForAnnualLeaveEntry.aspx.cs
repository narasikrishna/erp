﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.BLL;
using FIN.BLL.HR;
using VMVServices.Web;

namespace FIN.Client.HR
{
    public partial class LeavePlannerForAnnualLeaveEntry : PageBase
    {

        HR_LEAVE_PLAN_FOR_AL hR_LEAVE_PLAN_FOR_AL = new HR_LEAVE_PLAN_FOR_AL();


        DataTable dtGridData = new DataTable();
        Boolean bol_rowVisiable;
        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CML_PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));


            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
        }

        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>
        /// 

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void FillComboBox()
        {
            FIN.BLL.GL.AccountingCalendar_BLL.GetCalYear(ref ddlFiscalYr);
            Department_BLL.GetDepartmentName(ref ddlDepartment);

        }
        protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                dtGridData = DBMethod.ExecuteQuery(FIN.DAL.HR.LeavePlannerforAL_DAL.GetLeaveplannerDtl(ddlDepartment.SelectedValue,ddlFiscalYr.SelectedValue)).Tables[0];
                if (dtGridData.Rows.Count > 0)
                {
                    BindGrid(dtGridData);
                }
                else
                {
                    dtGridData = DBMethod.ExecuteQuery(FIN.DAL.HR.Employee_DAL.GetEmpBasedDept(ddlDepartment.SelectedValue)).Tables[0];
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CML_ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();


                Startup();
                FillComboBox();

                EntityData = null;

                dtGridData = DBMethod.ExecuteQuery(FIN.DAL.HR.LeavePlannerforAL_DAL.GetLeaveplannerfrAl(Master.StrRecordId)).Tables[0];


                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    using (IRepository<HR_LEAVE_PLAN_FOR_AL> userCtx = new DataRepository<HR_LEAVE_PLAN_FOR_AL>())
                    {
                        hR_LEAVE_PLAN_FOR_AL = userCtx.Find(r =>
                            (r.LEAVE_PLAN_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = hR_LEAVE_PLAN_FOR_AL;

                    ddlFiscalYr.SelectedValue = hR_LEAVE_PLAN_FOR_AL.CAL_DTL_ID;
                    ddlDepartment.SelectedValue = hR_LEAVE_PLAN_FOR_AL.ATTRIBUTE1;
                }
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CML_ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();



                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                var tmpChildEntity = new List<Tuple<object, string>>();

                if (gvData.Rows.Count > 0)
                {
                    for (int iLoop = 0; iLoop < gvData.Rows.Count; iLoop++)
                    {
                        HR_LEAVE_PLAN_FOR_AL hR_LEAVE_PLAN_FOR_AL = new HR_LEAVE_PLAN_FOR_AL();

                        if (gvData.DataKeys[iLoop].Values["LEAVE_PLAN_ID"].ToString() != "0" && gvData.DataKeys[iLoop].Values["LEAVE_PLAN_ID"].ToString() != string.Empty)
                        {
                            using (IRepository<HR_LEAVE_PLAN_FOR_AL> userCtx = new DataRepository<HR_LEAVE_PLAN_FOR_AL>())
                            {
                                hR_LEAVE_PLAN_FOR_AL = userCtx.Find(r =>
                                    (r.LEAVE_PLAN_ID == gvData.DataKeys[iLoop].Values["LEAVE_PLAN_ID"].ToString())
                                    ).SingleOrDefault();
                            }
                        }

                        TextBox dtpStartDate = (TextBox)gvData.Rows[iLoop].FindControl("dtpStartDate");
                        TextBox dtptodate = (TextBox)gvData.Rows[iLoop].FindControl("dtptodate");



                        hR_LEAVE_PLAN_FOR_AL.CAL_DTL_ID = ddlFiscalYr.SelectedValue;
                        hR_LEAVE_PLAN_FOR_AL.EMP_ID = gvData.DataKeys[iLoop].Values["EMP_ID"].ToString();
                        if (dtpStartDate.Text != string.Empty)
                        {
                            hR_LEAVE_PLAN_FOR_AL.FROM_DATE = DBMethod.ConvertStringToDate(dtpStartDate.Text.ToString());
                        }
                        else
                        {
                            hR_LEAVE_PLAN_FOR_AL.FROM_DATE = null;
                        }
                        if (dtptodate.Text != string.Empty)
                        {
                            hR_LEAVE_PLAN_FOR_AL.TO_DATE = DBMethod.ConvertStringToDate(dtptodate.Text.ToString());
                        }
                        else
                        {
                            hR_LEAVE_PLAN_FOR_AL.TO_DATE = null;
                        }

                        hR_LEAVE_PLAN_FOR_AL.ATTRIBUTE1 = ddlDepartment.SelectedValue;

                        hR_LEAVE_PLAN_FOR_AL.ENABLED_FLAG = FINAppConstants.Y;
                        hR_LEAVE_PLAN_FOR_AL.ORG_ID = VMVServices.Web.Utils.OrganizationID;

                        if (gvData.DataKeys[iLoop].Values[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                        {

                            DBMethod.DeleteEntity<HR_LEAVE_PLAN_FOR_AL>(hR_LEAVE_PLAN_FOR_AL);
                        }
                        else
                        {
                            if (gvData.DataKeys[iLoop].Values["LEAVE_PLAN_ID"].ToString() != "0" && gvData.DataKeys[iLoop].Values["LEAVE_PLAN_ID"].ToString() != string.Empty)
                            {
                                hR_LEAVE_PLAN_FOR_AL.LEAVE_PLAN_ID = gvData.DataKeys[iLoop].Values["LEAVE_PLAN_ID"].ToString();
                                hR_LEAVE_PLAN_FOR_AL.MODIFIED_BY = this.LoggedUserName;

                                hR_LEAVE_PLAN_FOR_AL.MODIFIED_DATE = DateTime.Today;
                                hR_LEAVE_PLAN_FOR_AL.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_LEAVE_PLAN_FOR_AL.LEAVE_PLAN_ID);
                                DBMethod.SaveEntity<HR_LEAVE_PLAN_FOR_AL>(hR_LEAVE_PLAN_FOR_AL, true);

                            }
                            else
                            {

                                hR_LEAVE_PLAN_FOR_AL.LEAVE_PLAN_ID = FINSP.GetSPFOR_SEQCode("HR_112".ToString(), false, true);
                                hR_LEAVE_PLAN_FOR_AL.CREATED_BY = this.LoggedUserName;
                                hR_LEAVE_PLAN_FOR_AL.CREATED_DATE = DateTime.Today;
                                hR_LEAVE_PLAN_FOR_AL.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_LEAVE_PLAN_FOR_AL.LEAVE_PLAN_ID);
                                DBMethod.SaveEntity<HR_LEAVE_PLAN_FOR_AL>(hR_LEAVE_PLAN_FOR_AL);
                            }
                        }

                    }


                }



            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CML_ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                //DropDownList ddlLotNo = tmpgvr.FindControl("ddlLotNo") as DropDownList;
                //WarehouseTransfer_BLL.fn_getLotNo(ref ddlLotNo);


                //if (gvData.EditIndex >= 0)
                //{
                //    ddlLotNo.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.LOT_ID].ToString();


                //}
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CML_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                AssignToBE();

                //ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session[FINSessionConstants.GridData], "Competency Link");

                //if (ErrorCollection.Count > 0)
                //{
                //    return;
                //}

                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        #endregion

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    // dr["ENABLED_FLAG"] = "FALSE";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CMl_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                //ErrorCollection.Clear();
                //GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                //DataRow drList = null;
                //if (Session[FINSessionConstants.GridData] != null)
                //{
                //    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                //}

                //if (e.CommandName.Equals("FooterInsert"))
                //{
                //    gvr = gvData.FooterRow;
                //    if (gvr == null)
                //    {
                //        return;
                //    }
                //}


                //if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                //{
                //    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                //    if (ErrorCollection.Count > 0)
                //    {
                //        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                //        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                //        return;
                //    }
                //    dtGridData.Rows.Add(drList);
                //    BindGrid(dtGridData);
                //}
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CMl_ROW_CMD", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>




        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CML_RDB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                // COURSE_GROUP.COURSE_GROUP_ID = short.Parse(Master.RecordID.ToString());
                DBMethod.DeleteEntity<HR_LEAVE_PLAN_FOR_AL>(hR_LEAVE_PLAN_FOR_AL);
                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATADELETED);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("cML_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }



        private void FillCompetency()
        {
            

        }



    }
}