﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.DAL.GL;
using FIN.BLL;
using FIN.BLL.GL;
using FIN.BLL.HR;
using VMVServices.Web;

namespace FIN.Client.HR
{
    public partial class LeaveDefinitionEntry : PageBase
    {
        HR_LEAVE_DEFINITIONS HR_LEAVE_DEFINITIONS = new HR_LEAVE_DEFINITIONS();
        DataTable dtData = new DataTable();
        Boolean bol_rowVisiable;
        Boolean savedBool;
        DataTable dtGridData = new DataTable();
        string ProReturn = null;


        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));


            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
        }

        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        private void FillComboBox()
        {
         //   AccountingCalendar_BLL.GetFinancialYear(ref ddlFinancialYear);
        }
        private void FillFromToDate()
        {
            //if (ddlFinancialYear.SelectedValue != null)
            //{
            //    dtData = DBMethod.ExecuteQuery(AccountingCalendar_DAL.GetFinancialYear((ddlFinancialYear.SelectedValue.ToString()))).Tables[0];
            //    if (dtData != null)
            //    {
            //        if (dtData.Rows.Count > 0)
            //        {
            //            if (dtData.Rows[0]["cal_eff_start_dt"].ToString().Length > 0)
            //            {
            //                txtFromDate.Text = DateTime.Parse(dtData.Rows[0][FINColumnConstants.CAL_EFF_START_DT].ToString()).ToString("dd/MM/yyyy");
            //            }
            //            if (dtData.Rows[0]["cal_eff_end_dt"].ToString().Length > 0)
            //            {
            //                txtToDate.Text = DateTime.Parse(dtData.Rows[0][FINColumnConstants.CAL_EFF_END_DT].ToString()).ToString("dd/MM/yyyy");
            //            }
            //        }
            //    }
            //}
        }
        protected void ddlFinancialYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
              //  FillFromToDate();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// .RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();

                
              //  Session[FINSessionConstants.GridData] = null;
                FillComboBox();
                EntityData = null;

                dtGridData = DBMethod.ExecuteQuery(LeaveDefinition_DAL.GetLeaveDefnDtls(Master.RecordID)).Tables[0];
                BindGrid(dtGridData);

                if (Master.Mode != FINAppConstants.Add && Master.RecordID > 0)
                {

                    using (IRepository<HR_LEAVE_DEFINITIONS> userCtx = new DataRepository<HR_LEAVE_DEFINITIONS>())
                    {
                        HR_LEAVE_DEFINITIONS = userCtx.Find(r =>
                            (r.PK_ID == Master.RecordID)
                            ).SingleOrDefault();
                    }
                   // HR_LEAVE_DEFINITIONS = LeaveDefinition_BLL.getClassEntity(Master.StrRecordId);
                    EntityData = HR_LEAVE_DEFINITIONS;
                  //  ddlFinancialYear.SelectedValue = HR_LEAVE_DEFINITIONS.FISCAL_YEAR.ToString();
                    //FillFromToDate();

                   
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Vaca_Entry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);

                }
            }

        }

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr["WITH_PAY_YN"] = "FALSE";
                    dr["CARRY_OVER_YN"] = "FALSE";
                    dr["ENABLED_FLAG"] = "FALSE";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        #endregion
        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session[FINSessionConstants.GridData], "Leave Definition ");

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                AssignToBE();

                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);
                    btnSave.Attributes.Add("onclick","reload();");
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                var tmpChildEntity = new List<Tuple<object, string>>();

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                   // HR_LEAVE_DEFINITIONS = new HR_LEAVE_DEFINITIONS();

                    if (int.Parse(dtGridData.Rows[iLoop][FINColumnConstants.PK_ID].ToString()) > 0)
                    {
                       HR_LEAVE_DEFINITIONS = LeaveDefinition_BLL.getClassEntity(dtGridData.Rows[iLoop][FINColumnConstants.PK_ID].ToString());
                       // LeaveDefinition_BLL.getClassEntity(dtGridData.Rows[iLoop][FINColumnConstants.PK_ID].ToString());

                    }
                    HR_LEAVE_DEFINITIONS.LEAVE_ID = dtGridData.Rows[iLoop][FINColumnConstants.LEAVE_ID].ToString();
                    HR_LEAVE_DEFINITIONS.LEAVE_ID_OL = dtGridData.Rows[iLoop]["LEAVE_ID_OL"].ToString();
                    HR_LEAVE_DEFINITIONS.LEAVE_DESC = dtGridData.Rows[iLoop][FINColumnConstants.LEAVE_DESC].ToString();
                    HR_LEAVE_DEFINITIONS.NO_OF_DAYS = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop][FINColumnConstants.NO_OF_DAYS].ToString());
                   // HR_LEAVE_DEFINITIONS.CARRY_OVER_TYPE = dtGridData.Rows[iLoop][FINColumnConstants.CARRY_OVER_TYPE].ToString();
                   // HR_LEAVE_DEFINITIONS.FISCAL_YEAR = (ddlFinancialYear.SelectedValue.ToString());
                    if (dtGridData.Rows[iLoop][FINColumnConstants.EFFECTIVE_FROM_DT] != DBNull.Value)
                    {
                        HR_LEAVE_DEFINITIONS.EFFECTIVE_FROM_DT = DateTime.Parse(dtGridData.Rows[iLoop][FINColumnConstants.EFFECTIVE_FROM_DT].ToString());
                    }

                    if (dtGridData.Rows[iLoop][FINColumnConstants.EFFECTIVE_TO_DT] != DBNull.Value)
                    {
                        HR_LEAVE_DEFINITIONS.EFFECTIVE_TO_DT = DateTime.Parse(dtGridData.Rows[iLoop][FINColumnConstants.EFFECTIVE_TO_DT].ToString());
                    }


                    HR_LEAVE_DEFINITIONS.WITH_PAY_YN = dtGridData.Rows[iLoop][FINColumnConstants.WITH_PAY_YN].ToString() == FINAppConstants.TRUEFLAG ? FINAppConstants.EnabledFlag : FINAppConstants.DisabledFlag;
                    HR_LEAVE_DEFINITIONS.CARRY_OVER_YN = dtGridData.Rows[iLoop][FINColumnConstants.CARRY_OVER_YN].ToString() == FINAppConstants.TRUEFLAG ? FINAppConstants.EnabledFlag : FINAppConstants.DisabledFlag;
                    HR_LEAVE_DEFINITIONS.ENABLED_FLAG = dtGridData.Rows[iLoop][FINColumnConstants.ENABLED_FLAG].ToString() == FINAppConstants.TRUEFLAG ? FINAppConstants.EnabledFlag : FINAppConstants.DisabledFlag;

                    

                    HR_LEAVE_DEFINITIONS.ORG_ID = VMVServices.Web.Utils.OrganizationID.ToString();


                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        tmpChildEntity.Add(new Tuple<object, string>(HR_LEAVE_DEFINITIONS, "D"));
                    }
                    else
                    {


                        // Duplicate Validation Through Backend Package PKG_VALIDATIONS

                        //ProReturn = FIN.DAL.HR.LeaveDefinition_DAL.GetSPFOR_DUPLICATE_CHECK(HR_LEAVE_DEFINITIONS.LEAVE_DESC, HR_LEAVE_DEFINITIONS.LEAVE_ID);

                        //if (ProReturn != string.Empty)
                        //{
                        //    if (ProReturn != "0")
                        //    {
                        //        ErrorCollection.Add("LEAVEDEF", ProReturn);
                        //        if (ErrorCollection.Count > 0)
                        //        {
                        //            return;
                        //        }
                        //    }
                        //}



                        if (int.Parse(dtGridData.Rows[iLoop][FINColumnConstants.PK_ID].ToString()) > 0)
                        {
                            HR_LEAVE_DEFINITIONS.PK_ID = int.Parse(dtGridData.Rows[iLoop]["PK_ID"].ToString());
                            HR_LEAVE_DEFINITIONS.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, HR_LEAVE_DEFINITIONS.PK_ID.ToString());
                            HR_LEAVE_DEFINITIONS.MODIFIED_BY = this.LoggedUserName;
                            HR_LEAVE_DEFINITIONS.MODIFIED_DATE = DateTime.Today;
                            DBMethod.SaveEntity<HR_LEAVE_DEFINITIONS>(HR_LEAVE_DEFINITIONS,true);
                            savedBool = true;
                        }
                        else
                        {
                            HR_LEAVE_DEFINITIONS.PK_ID = DBMethod.GetPrimaryKeyValue("hr_leave_definitions_seq");
                            HR_LEAVE_DEFINITIONS.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, HR_LEAVE_DEFINITIONS.PK_ID.ToString());
                            HR_LEAVE_DEFINITIONS.CREATED_BY = this.LoggedUserName;
                            HR_LEAVE_DEFINITIONS.CREATED_DATE = DateTime.Today;
                            DBMethod.SaveEntity<HR_LEAVE_DEFINITIONS>(HR_LEAVE_DEFINITIONS);
                            savedBool = true;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {

                    HR_LEAVE_DEFINITIONS.PK_ID = Convert.ToInt32(dtGridData.Rows[iLoop]["PK_ID"].ToString());
                    DBMethod.DeleteEntity<HR_LEAVE_DEFINITIONS>(HR_LEAVE_DEFINITIONS);
                }


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion


        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            try
            {
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = -1;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_ROW_CNCL_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_ROW_CMD", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {


            System.Collections.SortedList slControls = new System.Collections.SortedList();

            TextBox txtLeaveName = gvr.FindControl("txtLeaveName") as TextBox;
            TextBox txtLeaveNameAR = gvr.FindControl("txtLeaveNameAR") as TextBox;
            TextBox txtdesc = gvr.FindControl("txtdesc") as TextBox;
            TextBox dtpStartDate = gvr.FindControl("dtpStartDate") as TextBox;
            TextBox dtpEndDate = gvr.FindControl("dtpEndDate") as TextBox;
            CheckBox chkWithPay = gvr.FindControl("chkWithPay") as CheckBox;
            CheckBox chkCarryOver = gvr.FindControl("chkCarryOver") as CheckBox;
            CheckBox chkact = gvr.FindControl("chkact") as CheckBox;
            TextBox txtNoOfDays = gvr.FindControl("txtNoOfDays") as TextBox;
            TextBox txtCarryOverType = gvr.FindControl("txtCarryOverType") as TextBox;



            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList[FINColumnConstants.PK_ID] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }

            slControls[0] = txtLeaveName;
            slControls[1] = txtdesc;
            slControls[2] = txtNoOfDays;
          //  slControls[3] = txtCarryOverType;
            slControls[3] = dtpStartDate;
            slControls[4] = dtpStartDate;
            slControls[5] = dtpEndDate;
            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));

            ErrorCollection.Clear();
            string strCtrlTypes = "TextBox~TextBox~TextBox~TextBox~DateTime~DateRangeValidate";
            string strMessage = Prop_File_Data["Leave_Name_P"] + " ~ " + Prop_File_Data["Description_P"] + " ~ " + Prop_File_Data["No_of_Days_P"] + " ~ " + Prop_File_Data["Start_Date_P"] + " ~ " + Prop_File_Data["Start_Date_P"] + " ~ " + Prop_File_Data["End_Date_P"] + "";
            //string strMessage = "Leave Name ~ Description ~ No of days ~ Start Date ~ Start Date ~ End Date";
            ErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);
            if (ErrorCollection.Count > 0)
                return drList;

            if (dtpEndDate.Text.ToString().Length > 0)
            {
                ErrorCollection = UserUtility_BLL.DateRangeValidate(DBMethod.ConvertStringToDate(dtpStartDate.Text), DBMethod.ConvertStringToDate(dtpEndDate.Text), "chk");
                if (ErrorCollection.Count > 0)
                    return drList;
            }


            string strCondition = "LEAVE_ID='" + txtLeaveName.Text + "'";
            strMessage = FINMessageConstatns.RecordAlreadyExists;
            ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            if (ErrorCollection.Count > 0)
            {
                return drList;
            }

           // ProReturn = FINSP.GetSPFOR_ERR_MGR_CATEGORY_TWO(Master.FormCode, drList[FINColumnConstants.PK_ID].ToString(), ddlFinancialYear.SelectedValue,txtLeaveName.Text);
            ProReturn = FINSP.GetSPFOR_ERR_MGR_CATEGORY(Master.FormCode, drList[FINColumnConstants.PK_ID].ToString(), txtLeaveName.Text);
            if (ProReturn != string.Empty)
            {
                if (ProReturn != "0")
                {
                    ErrorCollection.Add("LEAVEDEFINITION", ProReturn);
                    if (ErrorCollection.Count > 0)
                    {
                        return drList;
                    }
                }
            }

            drList[FINColumnConstants.LEAVE_ID] = txtLeaveName.Text;
            drList["LEAVE_ID_OL"] = txtLeaveNameAR.Text;
            drList[FINColumnConstants.LEAVE_DESC] = txtdesc.Text;
            drList[FINColumnConstants.NO_OF_DAYS] = txtNoOfDays.Text;
           // drList[FINColumnConstants.CARRY_OVER_TYPE] = txtCarryOverType.Text;

            if (dtpStartDate.Text.ToString().Length > 0)
            {
                drList[FINColumnConstants.EFFECTIVE_FROM_DT] = DBMethod.ConvertStringToDate(dtpStartDate.Text.ToString());
            }

            if (dtpEndDate.Text.ToString().Length > 0)
            {

                drList[FINColumnConstants.EFFECTIVE_TO_DT] = DBMethod.ConvertStringToDate(dtpEndDate.Text.ToString());
            }
            else
            {
                drList[FINColumnConstants.EFFECTIVE_TO_DT] = DBNull.Value;
            }

            drList[FINColumnConstants.ENABLED_FLAG] = chkact.Checked == true ? FINAppConstants.TRUEFLAG : FINAppConstants.FALSEFLAG;
            drList[FINColumnConstants.WITH_PAY_YN] = chkWithPay.Checked == true ? FINAppConstants.TRUEFLAG : FINAppConstants.FALSEFLAG;
            drList[FINColumnConstants.CARRY_OVER_YN] = chkCarryOver.Checked == true ? FINAppConstants.TRUEFLAG : FINAppConstants.FALSEFLAG;

            drList[FINColumnConstants.DELETED] = FINAppConstants.N;


            return drList;



        }



        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_ROW_UP", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_ROW_DEL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                //FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_ROW_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_RowCreated", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }
                    if (e.Row.RowType == DataControlRowType.DataRow)
                    {
                        e.Row.Cells[2].HorizontalAlign = HorizontalAlign.Right;
                       
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_RDB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }



        #endregion

        protected void btnDelete_Click(object sender, EventArgs e)
        {

        }
    }
}