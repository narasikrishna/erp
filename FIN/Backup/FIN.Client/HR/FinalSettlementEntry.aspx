﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="FinalSettlementEntry.aspx.cs" Inherits="FIN.Client.HR.FinalSettlementEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 100%;" id="divMainContainer">
        <table width="100%">
            <tr>
                <td colspan="2">
                    <div class="divRowContainer">
                        <div class="lblBox LNOrient" style="width: 150px" id="lblEmpName">
                            Employee Name
                        </div>
                        <div class="divtxtBox  LNOrient" style="width: 350px">
                            <asp:DropDownList ID="ddlEmpName" runat="server" CssClass="validate[required] RequiredField ddlStype"
                                Width="340px" TabIndex="1" AutoPostBack="True" OnSelectedIndexChanged="ddlEmpName_SelectedIndexChanged">
                            </asp:DropDownList>
                        </div>
                        <div class="colspace  LNOrient">
                            &nbsp</div>
                        <div class="lblBox LNOrient" style="width: 150px" id="lblPayrollPeriod">
                            Last Working Date
                        </div>
                        <div class="divtxtBox  LNOrient" style="width: 150px">
                            <asp:TextBox ID="txtDate" CssClass="validate[required]  RequiredField txtBox" runat="server"
                                TabIndex="2" Width="144px" AutoPostBack="True" OnTextChanged="txtDate_TextChanged"></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender8" runat="server" Format="dd/MM/yyyy" TargetControlID="txtDate"
                                OnClientDateSelectionChanged="checkDate">
                            </cc2:CalendarExtender>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars="/"
                                FilterType="Numbers,Custom" TargetControlID="txtDate" />
                        </div>
                        <div class="colspace  LNOrient">
                            &nbsp</div>
                        <div class="lblBox LNOrient" style="width: 150px" id="Div4">
                            <asp:ImageButton ID="imgBtnPost" runat="server" ImageUrl="~/Images/Post.png" OnClick="imgBtnPost_Click"
                                Style="border: 0px" />
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td style="width: 50%">
                    <div id="divPermintDet" style="width: 100%">
                        <div class="GridHeader" style="width: 100%">
                            <div style="float: left">
                                Indemnity Settlement
                            </div>
                        </div>
                        <div class="divClear_10" style="width: 70%">
                        </div>
                        <div id="divempPermitGrid">
                            <div class="divRowContainer">
                                <div class="lblBox" style="width: 120px" id="Div5">
                                    Amount
                                </div>
                                <div class="divtxtBox  LNOrient" style="width: 150px">
                                    <asp:TextBox runat="server" Enabled="false" ID="txtPayableAmt" CssClass="validate[required]  RequiredField  txtBox_N"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                </td>
                <td style="width: 50%">
                    <div class="GridHeader" style="width: 100%">
                        <div style="float: left">
                            Loan Settlement
                        </div>
                    </div>
                    <div class="divClear_10" style="width: 70%">
                    </div>
                    <div class="divRowContainer">
                        <div class="lblBox LNOrient" style="width: 120px" id="Div6">
                            Balance Amount
                        </div>
                        <div class="divtxtBox  LNOrient" style="width: 150px">
                            <asp:TextBox runat="server" Enabled="false" ID="txtLoanBalAmt" CssClass="validate[required]  RequiredField  txtBox_N"></asp:TextBox>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div class="GridHeader" style="width: 100%">
                        <div style="float: left">
                            Employee Salary Settlement
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td style="width: 50%">
                    <div class="lblBox LNOrient" style="font-weight: bold; width: 100px" id="Div8">
                        Earnings
                    </div>
                </td>
                <td style="width: 50%">
                    <div class="lblBox LNOrient" style="font-weight: bold; width: 100px" id="Div9">
                        Deduction
                    </div>
                </td>
            </tr>
            <tr>
                <td style="width: 50%" valign="top">
                    <asp:GridView ID="gvEmployeePay" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                        Width="98%" DataKeyNames="FNL_STL_DTL_ID,PAYROLL_DTL_ID,PAYROLL_DTL_DTL_ID,PAY_EMP_ELEMENT_ID,PAY_EMP_ID,pay_element_Type,DELETED"
                        OnRowDataBound="gvEmployeePay_RowDataBound" ShowFooter="false">
                        <Columns>
                            <asp:TemplateField Visible="false" HeaderText="Employee">
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtEmployeeNo" MaxLength="100" runat="server" CssClass=" RequiredField  txtBox"
                                        Text='<%# Eval("emp_name") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtEmployeeNo" Visible="false" MaxLength="100" runat="server" CssClass="RequiredField   txtBox"></asp:TextBox>
                                </FooterTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblEmployeeNo" runat="server" Text='<%# Eval("emp_name") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                            </asp:TemplateField>
                            <asp:TemplateField Visible="false" HeaderText="Type">
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtElementType" Width="130px" MaxLength="100" runat="server" CssClass=" RequiredField  txtBox"
                                        Text='<%# Eval("pay_element_Type") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtElementType" Width="130px" Visible="false" MaxLength="100" runat="server"
                                        CssClass="RequiredField   txtBox"></asp:TextBox>
                                </FooterTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblElementType" Width="130px" runat="server" Text='<%# Eval("pay_element_Type") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" Width="150px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Element">
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtElement" Width="280px" MaxLength="100" runat="server" CssClass=" RequiredField  txtBox"
                                        Text='<%# Eval("pay_element_desc") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtElement" Width="280px" Visible="false" MaxLength="100" runat="server"
                                        CssClass="RequiredField   txtBox"></asp:TextBox>
                                </FooterTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblElement" Width="280px" runat="server" Text='<%# Eval("pay_element_desc") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Amount">
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtAmount" Width="95%" MaxLength="100" onkeyup="CalculateEar();"
                                        runat="server" CssClass=" RequiredField  txtBox_N" Text='<%# Eval("pay_amount") %>'></asp:TextBox>
                                    <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" ValidChars=".,"
                                        FilterType="Numbers,Custom" TargetControlID="txtAmount" />
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtAmount" Width="95%" onkeyup="CalculateEar();" Visible="false"
                                        MaxLength="100" runat="server" CssClass="RequiredField   txtBox_N"></asp:TextBox>
                                    <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars=".,"
                                        FilterType="Numbers,Custom" TargetControlID="txtAmount" />
                                </FooterTemplate>
                                <ItemTemplate>
                                    <asp:TextBox ID="txtAmount" Width="95%" onkeyup="CalculateEar();" MaxLength="100"
                                        runat="server" CssClass=" RequiredField  txtBox_N" Text='<%# Eval("pay_amount") %>'></asp:TextBox>
                                </ItemTemplate>
                                <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                        <HeaderStyle CssClass="GridHeader" />
                        <AlternatingRowStyle CssClass="GrdAltRow" />
                    </asp:GridView>
                </td>
                <td style="width: 50%" valign="top">
                    <asp:GridView ID="gvEmployeePay2" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                        OnRowDataBound="gvEmployeePay2_RowDataBound" Width="98%" DataKeyNames="FNL_STL_DTL_ID,PAYROLL_DTL_ID,PAYROLL_DTL_DTL_ID,PAY_EMP_ELEMENT_ID,PAY_EMP_ID,pay_element_Type,DELETED"
                        ShowFooter="false">
                        <Columns>
                            <asp:TemplateField Visible="false" HeaderText="Employee">
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtEmployeeNo" Width="130px" MaxLength="100" runat="server" CssClass=" RequiredField  txtBox"
                                        Text='<%# Eval("emp_name") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtEmployeeNo" Width="130px" Visible="false" MaxLength="100" runat="server"
                                        CssClass="RequiredField   txtBox"></asp:TextBox>
                                </FooterTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblEmployeeNo" Width="130px" runat="server" Text='<%# Eval("emp_name") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                            </asp:TemplateField>
                            <asp:TemplateField Visible="false" HeaderText="Type">
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtElementType" Width="130px" MaxLength="100" runat="server" CssClass=" RequiredField  txtBox"
                                        Text='<%# Eval("pay_element_Type") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtElementType" Width="130px" Visible="false" MaxLength="100" runat="server"
                                        CssClass="RequiredField   txtBox"></asp:TextBox>
                                </FooterTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblElementType" Width="130px" runat="server" Text='<%# Eval("pay_element_Type") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" Width="150px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Element">
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtElement" Width="280px" MaxLength="100" runat="server" CssClass=" RequiredField  txtBox"
                                        Text='<%# Eval("pay_element_desc") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtElement" Width="280px" Visible="false" MaxLength="100" runat="server"
                                        CssClass="RequiredField   txtBox"></asp:TextBox>
                                </FooterTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblElement" Width="280px" runat="server" Text='<%# Eval("pay_element_desc") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Amount">
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtAmount2" Width="96%" MaxLength="100" onkeyup="CalculateDed();"
                                        runat="server" CssClass=" RequiredField  txtBox_N" Text='<%# Eval("pay_amount") %>'></asp:TextBox>
                                    <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" ValidChars=".,"
                                        FilterType="Numbers,Custom" TargetControlID="txtAmount" />
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtAmount2" Width="96%" onkeyup="CalculateDed();" Visible="false"
                                        MaxLength="100" runat="server" CssClass="RequiredField   txtBox_N"></asp:TextBox>
                                    <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" ValidChars=".,"
                                        FilterType="Numbers,Custom" TargetControlID="txtAmount" />
                                </FooterTemplate>
                                <ItemTemplate>
                                    <asp:TextBox ID="txtAmount2" Width="96%" onkeyup="CalculateDed();" MaxLength="100"
                                        runat="server" CssClass=" RequiredField  txtBox_N" Text='<%# Eval("pay_amount") %>'></asp:TextBox>
                                </ItemTemplate>
                                <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                        <HeaderStyle CssClass="GridHeader" />
                        <AlternatingRowStyle CssClass="GrdAltRow" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <div class="lblBox" style="float: left; font-weight: bold; width: 100px" id="Div16">
                        Total Earnings
                    </div>
                    <div class="lblBox" style="float: right; width: 100px; height: 30px" id="Div17">
                        <asp:Label ID="lblTotalEar" runat="server"></asp:Label>
                    </div>
                </td>
                <td align="right">
                    <div class="lblBox" style="float: left; font-weight: bold; width: 120px; height: 30px"
                        id="Div14">
                        Total Deduction
                    </div>
                    <div class="lblBox" style="float: right; width: 80px" id="Div15">
                        <asp:Label ID="lblTotDed" runat="server"></asp:Label>
                    </div>
                </td>
            </tr>
            <tr>
                <td align="right" colspan="2">
                    <div class="lblBox" style="float: left; font-weight: bold; width: 100px" id="Div11">
                        Net Amount</div>
                    <div class="lblBox" style="float: left; width: 500px" id="Div13">
                        <asp:Label ID="lblNet" runat="server" Text=""></asp:Label>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div class="GridHeader" style="float: left; width: 100%">
                        <div style="float: left;">
                            Annual Leave Settlement
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div class="divRowContainer">
                        <div class="divRowContainer">
                            <div class="lblBox" style="float: left; width: 100px" id="Div7">
                                No of Days
                            </div>
                            <div class="divtxtBox" style="float: left; width: 182px">
                                <asp:TextBox runat="server" Enabled="false" ID="txtNoOfDays" CssClass="validate[required]  RequiredField  txtBox_N"></asp:TextBox>
                            </div>
                            <div class="colspace" style="float: left;">
                                &nbsp</div>
                            <div class="lblBox" style="float: left; width: 100px" id="Div3">
                                Amount
                            </div>
                            <div class="divtxtBox" style="float: left; width: 182px">
                                <asp:TextBox runat="server" Enabled="false" ID="txtLeaveAmount" CssClass="validate[required]  RequiredField  txtBox_N"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div class="GridHeader" style="float: left; width: 100%">
                        <div style="float: left;">
                            Bank Details
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div class="divRowContainer">
                        <div class="lblBox" style="float: left; width: 100px" id="Div1">
                            Amount To Pay
                        </div>
                        <div class="divtxtBox" style="float: left; width: 180px; font-weight: bold">
                            <asp:Label ID="lblAmtTOPAy" runat="server" Text=""></asp:Label>
                        </div>
                    </div>
                    <div class="divClear_10" style="width: 60%">
                    </div>
                    <div class="divRowContainer">
                        <div class="lblBox" style="float: left; width: 100px" id="lblPONumber">
                            Bank Name
                        </div>
                        <div class="divtxtBox" style="float: left; width: 180px; height: 22px;">
                            <asp:DropDownList ID="ddlBankName" runat="server" TabIndex="14" CssClass="ddlStype"
                                AutoPostBack="True" OnSelectedIndexChanged="ddlBankName_SelectedIndexChanged">
                            </asp:DropDownList>
                        </div>
                        <div class="colspace" style="float: left;">
                            &nbsp</div>
                        <div class="lblBox" style="float: left; width: 100px" id="lblPOLineNumber">
                            Branch
                        </div>
                        <div class="divtxtBox" style="float: left; width: 180px">
                            <asp:DropDownList ID="ddlBankBranch" runat="server" TabIndex="15" CssClass="ddlStype"
                                AutoPostBack="True" OnSelectedIndexChanged="ddlBankBranch_SelectedIndexChanged">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="divClear_10" style="width: 60%">
                    </div>
                    <div class="divRowContainer" align="left">
                        <div class="lblBox" style="float: left; width: 100px" id="lblItemDescription">
                            Account No
                        </div>
                        <div class="divtxtBox" style="float: left; width: 180px">
                            <asp:DropDownList ID="ddlAccountnumber" runat="server" TabIndex="16" AutoPostBack="True"
                                OnSelectedIndexChanged="ddlAccountnumber_SelectedIndexChanged" CssClass="ddlStype">
                            </asp:DropDownList>
                        </div>
                        <div class="colspace" style="float: left;">
                            &nbsp</div>
                        <div class="lblBox" style="float: left; width: 100px">
                            Cheque No
                        </div>
                        <div class="divtxtBox" style="float: left; width: 180px">
                            <asp:DropDownList ID="ddlChequeNumber" runat="server" TabIndex="17" CssClass=" ddlStype">
                            </asp:DropDownList>
                        </div>
                    </div>
                </td>
            </tr>
        </table>
        <br />
        <br />
        <br />
        <br />
        <div class="divClear_10">
        </div>
    </div>
    <div class="divRowContainer">
        <asp:HiddenField runat="server" ID="hf_dept_id" />
        <asp:HiddenField runat="server" ID="hf_Payroll_Id" />
        <asp:HiddenField runat="server" ID="hf_No_of_days" />
        <asp:HiddenField runat="server" ID="hd_AnnualLeave_Id" />
        <asp:HiddenField runat="server" ID="hd_loan_req_id" />
    </div>
    <div class="divRowContainer divAction">
        <table class="SaveTable">
            <tr>
                <td>
                    <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                        TabIndex="17" />
                </td>
                <td>
                    <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="18" />
                </td>
                <td>
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="19" />
                </td>
                <td>
                    <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="20" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/HR/HRChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
            var scrollY = $("#FINContent_hf_ScrollPos").val()

            if (scrollY > 0) {
                window.scrollTo(0, scrollY);
            }

            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }
        function CalculateEar() {
            var grid = document.getElementById('<%=gvEmployeePay.ClientID %>');
            var total = 0;

            if (grid.rows.length > 0) {
                var inputs = grid.getElementsByTagName("input");


                for (var i = 0; i < inputs.length; i++) {

                    if (inputs[i].name.indexOf("txtAmount") > 1) {
                        if (inputs[i].value != "" && inputs[i].value != ".000") {
                            total = parseFloat(total) + parseFloat(inputs[i].value);
                        }
                    }
                }
            }

            document.getElementById('<%= lblTotalEar.ClientID %>').innerHTML = total.toFixed(3);
            calculate_time();

        }

        function CalculateDed() {
            var grid = document.getElementById('<%=gvEmployeePay2.ClientID %>');
            var total = 0;

            if (grid.rows.length > 0) {
                var inputs = grid.getElementsByTagName("input");


                for (var i = 0; i < inputs.length; i++) {

                    if (inputs[i].name.indexOf("txtAmount2") > 1) {

                        if (inputs[i].value != "" && inputs[i].value != ".000") {
                            total = parseFloat(total) + parseFloat(inputs[i].value);
                            //alert(total);
                        }
                    }
                }
            }

            document.getElementById('<%= lblTotDed.ClientID %>').innerHTML = total.toFixed(3);
            calculate_time();


        }

        function calculate_time() {
            var hour_start = document.getElementById('<%= lblTotalEar.ClientID %>').innerHTML.replace(',', '')
            // var hour_start = document.getElementById(parseInt('lblTotalEar'));                    
            var hour_end = document.getElementById('<%= lblTotDed.ClientID %>').innerHTML.replace(',', '')
            var hour_total = hour_start - hour_end;

            document.getElementById('<%=lblNet.ClientID %>').innerHTML = hour_total.toFixed(3);


            var IndemAmt = parseFloat($("#FINContent_txtPayableAmt").val().replace(',', ''));
            var LoanAmt = parseFloat($("#FINContent_txtLoanBalAmt").val().replace(',', ''));
            var leaveAmt = parseFloat($("#FINContent_txtLeaveAmount").val().replace(',', ''));

            var AmtTOApay = (IndemAmt + parseFloat(hour_total) + leaveAmt) - LoanAmt;


            document.getElementById('<%=lblAmtTOPAy.ClientID %>').innerHTML = AmtTOApay.toFixed(3);

        }
       


    </script>
</asp:Content>
