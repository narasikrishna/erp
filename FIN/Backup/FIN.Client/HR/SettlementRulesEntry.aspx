﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="SettlementRulesEntry.aspx.cs" Inherits="FIN.Client.HR.SettlementRulesEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 750px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div align="center">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                Width="600px" DataKeyNames="STATT_ID,LEAVE_REQ_ID,EMP_ID,ATTENDANCE_TYPE" OnRowCancelingEdit="gvData_RowCancelingEdit"
                OnRowCommand="gvData_RowCommand" OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound"
                OnRowDeleting="gvData_RowDeleting" OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating"
                ShowFooter="True">
                <Columns>
                    <asp:TemplateField HeaderText="Seperation Type">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlSeperationType" Width="150px" AutoPostBack="true" TabIndex="1"
                                runat="server" CssClass="RequiredField EntryFont ddlStype">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlSeperationType" Width="150px" AutoPostBack="true" TabIndex="1"
                                runat="server" CssClass="RequiredField EntryFont ddlStype">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblSeperationType" Width="130px" runat="server" Text='<%# Eval("EMP_NAME") %>'></asp:Label>
                        </ItemTemplate>
                       
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="No of Years From">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtNoofYearsFrom" CssClass="validate[required] RequiredField txtBox"
                                MaxLength="10" Width="90%" runat="server" TabIndex="3"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtNoofYearsFrom" CssClass="validate[required] RequiredField txtBox"
                                MaxLength="10" Width="90%" runat="server" TabIndex="3"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblNoofYearsFrom" Width="130px" runat="server" Text='<%# Eval("EMP_NAME") %>'></asp:Label>
                        </ItemTemplate>
                       
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="No of Years To">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtNoofYearsTo" CssClass="validate[required] RequiredField txtBox"
                                MaxLength="10" Width="90%" runat="server" TabIndex="3"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtNoofYearsTo" CssClass="validate[required] RequiredField txtBox"
                                MaxLength="10" Width="90%" runat="server" TabIndex="3"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblNoofYearsTo" Width="130px" runat="server" Text='<%# Eval("EMP_NAME") %>'></asp:Label>
                        </ItemTemplate>
                        
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="No of Days Per Year">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtNoofDays" CssClass="validate[required] RequiredField txtBox"
                                MaxLength="10" Width="90%" runat="server" TabIndex="4"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtNoofDays" CssClass="validate[required] RequiredField txtBox"
                                MaxLength="10" Width="90%" runat="server" TabIndex="4"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblNoofDays" Width="130px" runat="server" Text='<%# Eval("EMP_NAME") %>'></asp:Label>
                        </ItemTemplate>
                        
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Proportionate Years">
                        <EditItemTemplate>
                            <asp:CheckBox ID="chkPropYear" runat="server" TabIndex="5" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:CheckBox ID="chkPropYear" runat="server" TabIndex="5" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblPropYear" Width="130px" runat="server" Text='<%# Eval("EMP_NAME") %>'></asp:Label>
                        </ItemTemplate>
                        
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" TabIndex="9" runat="server" AlternateText="Edit" CausesValidation="false"
                                ToolTip="Edit" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                            <asp:ImageButton ID="ibtnDelete" TabIndex="10" runat="server" AlternateText="Delete"
                                CausesValidation="false" ToolTip="Delete" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibtnUpdate" TabIndex="11" runat="server" AlternateText="Update"
                                CommandName="Update" ToolTip="Update" Height="20px" ImageUrl="~/Images/Update.png" />
                            <asp:ImageButton ID="ibtnCancel" TabIndex="12" runat="server" AlternateText="Cancel"
                                CausesValidation="false" ToolTip="Cancel" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="ibtnInsert" TabIndex="10" runat="server" AlternateText="Add"
                                CommandName="FooterInsert" ToolTip="Add" ImageUrl="~/Images/Add.jpg" />
                        </FooterTemplate>
                        <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="1" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="2" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="3" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table width="100%">
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" CssClass="button" ID="btnYes" Text="Yes" Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
   <script src="../LanguageScript/HR/HRChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
