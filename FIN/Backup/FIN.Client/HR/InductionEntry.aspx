﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="InductionEntry.aspx.cs" Inherits="FIN.Client.HR.InductionEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 800px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 120px" id="lblInductionID">
                Induction ID
            </div>
            <div class="divtxtBox  LNOrient" style="width: 255px">
                <asp:TextBox ID="txtInductionID" CssClass="RequiredField txtBox" MaxLength="10"
                    runat="server" Enabled="false" TabIndex="1"></asp:TextBox>
            </div>
           <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 120px" id="lblDate">
                Date
            </div>
            <div class="divtxtBox  LNOrient" style="width: 255px">
                <asp:TextBox runat="server" ID="txtDOB" CssClass="validate[required,custom[ReqDateDDMMYYY]] RequiredField txtBox"
                    TabIndex="2"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtDOB" />
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtDOB" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 120px" id="lblInductedBy">
                Inducted By
            </div>
            <div class="divtxtBox  LNOrient" style="width: 255px">
                <asp:DropDownList ID="ddlInductedBy" TabIndex="3" runat="server" CssClass="validate[required] RequiredField ddlStype">
                </asp:DropDownList>
            </div>
           <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style=" width: 120px" id="lblDepartment">
                Department
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 255px">
                <asp:DropDownList ID="ddlDeprtment" TabIndex="4" runat="server" 
                    CssClass="validate[required] RequiredField ddlStype" AutoPostBack="True" 
                    onselectedindexchanged="ddlDeprtment_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 120px" id="lblDesignation">
                Designation
            </div>
            <div class="divtxtBox  LNOrient" style="width: 255px">
                <asp:DropDownList ID="ddlDesignation" TabIndex="5" CssClass="validate[required] RequiredField ddlStype"
                    runat="server">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style=" width: 120px" id="Div1">
                Employee
            </div>
            <div class="divtxtBox  LNOrient" style="width: 255px">
                <asp:DropDownList ID="ddlApplicantID" TabIndex="6" runat="server" CssClass="validate[required] RequiredField ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="LNOrient">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                Width="100%" DataKeyNames="HR_IND_DTL_ID,HR_IND_ACT_ID,STATUS" OnRowCancelingEdit="gvData_RowCancelingEdit"
                OnRowCommand="gvData_RowCommand" OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound"
                OnRowDeleting="gvData_RowDeleting" OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating"
                ShowFooter="True">
                <Columns>
                    <%--  <asp:TemplateField HeaderText="Applicant ID">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlApplicantID" TabIndex="6" runat="server" CssClass="EntryFont RequiredField ddlStype"
                                Width="200px" AutoPostBack="True" 
                                onselectedindexchanged="ddlApplicantID_SelectedIndexChanged">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlApplicantID" TabIndex="6" runat="server" CssClass="EntryFont RequiredField ddlStype"
                                Width="200px" AutoPostBack="True" 
                                onselectedindexchanged="ddlApplicantID_SelectedIndexChanged">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblType" CssClass="adminFormFieldHeading" runat="server" Text='<%# Eval("HR_APPLICANT_ID") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Active Carried">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlactcarried" TabIndex="6" runat="server" CssClass="EntryFont RequiredField ddlStype"
                                Width="200px" AutoPostBack="True">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlactcarried" TabIndex="6" runat="server" CssClass="EntryFont RequiredField ddlStype"
                                Width="200px" AutoPostBack="True">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblactcarried" CssClass="adminFormFieldHeading" runat="server" Text='<%# Eval("HR_APPLICANT_ID") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>--%>
                    <asp:TemplateField HeaderText="Activity Carried ">
                        <EditItemTemplate>
                         <asp:DropDownList ID="ddlApplicantName" Width="200px" TabIndex="13" runat="server"  CssClass="RequiredField EntryFont ddlStype">
                            </asp:DropDownList>
                           
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlApplicantName" Width="200px"  runat="server" TabIndex="7"  CssClass="RequiredField EntryFont ddlStype">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblGroupName" CssClass="adminFormFieldHeading" runat="server" Style="word-wrap: break-word;white-space: pre-wrap;" Text='<%# Eval("HR_ACTIVITY_DESC") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Remarks">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtRemarks" MaxLength="150" Enabled="true" CssClass="EntryFont txtBox"
                                Text='<%# Eval("REMARKS") %>' runat="server" TabIndex="14"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtRemarks" MaxLength="150" Enabled="true" CssClass="EntryFont txtBox" TabIndex="8"
                                 runat="server"  Wrap = "false"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblRemarks" CssClass="adminFormFieldHeading" runat="server" Style="word-wrap: break-word;white-space: pre-wrap;" Text='<%# Eval("REMARKS") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    
                    <asp:TemplateField HeaderText="Status">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlstatus" TabIndex="15" runat="server" CssClass="EntryFont RequiredField ddlStype"
                                Width="200px" AutoPostBack="True">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlstatus"  runat="server" CssClass="EntryFont RequiredField ddlStype" TabIndex="9"
                                Width="200px" AutoPostBack="True">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblstatus" CssClass="adminFormFieldHeading" runat="server" Text='<%# Eval("DESCRIPTION") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    
                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" TabIndex="11" runat="server" AlternateText="Edit"
                                CausesValidation="false" ToolTip="Edit" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                            <asp:ImageButton ID="ibtnDelete" TabIndex="12" runat="server" AlternateText="Delete"
                                CausesValidation="false" ToolTip="Delete" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibtnUpdate" runat="server" AlternateText="Update" CommandName="Update" TabIndex="16"
                                ToolTip="Update" Height="20px" ImageUrl="~/Images/Update.png" />
                            <asp:ImageButton ID="ibtnCancel" runat="server" AlternateText="Cancel" CausesValidation="false" TabIndex="17"
                                ToolTip="Cancel" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="ibtnInsert" runat="server" AlternateText="Add" TabIndex="10"
                                CommandName="FooterInsert" ToolTip="Add" ImageUrl="~/Images/Add.jpg" />
                        </FooterTemplate>
                        <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" OnClick="btnSave_Click" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" />
                    </td>
                </tr>
            </table>
        </div>
        <%-- <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table>
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" CssClass="button" ID="btnYes" Text="Yes" Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>--%>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/HR/HRChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
