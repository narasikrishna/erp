﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="Permission.aspx.cs" Inherits="FIN.Client.HR.Permission" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 900px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblDepartment">
                Department Name
            </div>
            <div class="divtxtBox  LNOrient" style="width: 580px">
                <asp:DropDownList ID="ddlDepartName" runat="server" CssClass="validate[required] RequiredField ddlStype"
                   TabIndex="1" AutoPostBack="True" OnSelectedIndexChanged="ddlDepartName_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
<%--             <div class="colspace" style="float: left;">
                &nbsp</div>--%>
            
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblEmpName">
                Employee Name
            </div>
            <div class="divtxtBox  LNOrient" style="width: 580px">
                <asp:DropDownList ID="ddlEmpName" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    TabIndex="2">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblCategory">
                Category
            </div>
            <div class="divtxtBox  LNOrient" style="width: 190px">
                <asp:DropDownList ID="ddlCategory" runat="server" CssClass="validate[required] RequiredField ddlStype"
                   TabIndex="3" OnSelectedIndexChanged="ddlCategory_SelectedIndexChanged" AutoPostBack = "true">
                </asp:DropDownList>
            </div>
             <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 182px" id="lblStatus">
                Status
            </div>
            <div class="divtxtBox  LNOrient" style="width: 190px">
                <asp:DropDownList ID="ddlStatus" runat="server" CssClass="ddlStype" 
                    TabIndex="4" >
                </asp:DropDownList>
            </div>
        </div>

        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="Div1">
               Permission Date
            </div>
            <div class="divtxtBox  LNOrient" style="width: 190px">
                <asp:TextBox ID="txtDate" CssClass="validate[required,custom[ReqDateDDMMYYY]]  RequiredField  txtBox"
                    runat="server" TabIndex="5" Width="100px"></asp:TextBox>
                <cc2:CalendarExtender ID="CalendarExtesnder1" runat="server" Format="dd/MM/yyyy"
                    TargetControlID="txtDate" OnClientDateSelectionChanged="checkDate">
                </cc2:CalendarExtender>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblFromTime">
                From Time
            </div>
            <div class="divtxtBox  LNOrient" style="width: 60px">
                <asp:DropDownList ID="ddlStartTimeHr" runat="server" CssClass="validate[required] RequiredField  ddlStype" TabIndex="6"
                    Width="45px">
                    <asp:ListItem Value="" Text="-"></asp:ListItem>
                    <asp:ListItem Value="01">01</asp:ListItem>
                    <asp:ListItem Value="02">02</asp:ListItem>
                    <asp:ListItem Value="03">03</asp:ListItem>
                    <asp:ListItem Value="04">04</asp:ListItem>
                    <asp:ListItem Value="05">05</asp:ListItem>
                    <asp:ListItem Value="06">06</asp:ListItem>
                    <asp:ListItem Value="07">07</asp:ListItem>
                    <asp:ListItem Value="08">08</asp:ListItem>
                    <asp:ListItem Value="09">09</asp:ListItem>
                    <asp:ListItem Value="10">10</asp:ListItem>
                    <asp:ListItem Value="11">11</asp:ListItem>
                    <asp:ListItem Value="12">12</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="divtxtBox  LNOrient" style="width: 60px">
                <asp:DropDownList ID="ddlStartTimeMin" runat="server" CssClass="validate[required] RequiredField  ddlStype" TabIndex="7"
                    Width="45px">
                    <asp:ListItem Value="" Text="-"></asp:ListItem>
                    <asp:ListItem Value="00">00</asp:ListItem>
                    <asp:ListItem Value="05">05</asp:ListItem>
                    <asp:ListItem Value="10">10</asp:ListItem>
                    <asp:ListItem Value="15">15</asp:ListItem>
                    <asp:ListItem Value="20">20</asp:ListItem>
                    <asp:ListItem Value="25">25</asp:ListItem>
                    <asp:ListItem Value="30">30</asp:ListItem>
                    <asp:ListItem Value="35">35</asp:ListItem>
                    <asp:ListItem Value="40">40</asp:ListItem>
                    <asp:ListItem Value="45">45</asp:ListItem>
                    <asp:ListItem Value="50">50</asp:ListItem>
                    <asp:ListItem Value="55">55</asp:ListItem>
                    <asp:ListItem Value="60">60</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="divtxtBox  LNOrient" style="width: 60px">
                <asp:DropDownList ID="ddlStartTimeAMPM" runat="server" CssClass="validate[required] RequiredField  ddlStype" TabIndex="8"
                    Width="45px">
                    <asp:ListItem Value="" Text="-"></asp:ListItem>
                    <asp:ListItem Value="AM">AM</asp:ListItem>
                    <asp:ListItem Value="PM">PM</asp:ListItem>
                </asp:DropDownList>
            </div>
                        <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 182px" id="lblToTime">
                To Time
            </div>
               <div class="divtxtBox  LNOrient" style="width: 60px">
                <asp:DropDownList ID="ddlEndTimeHr" runat="server" CssClass="validate[required] RequiredField  ddlStype" TabIndex="9"
                    Width="45px">
                    <asp:ListItem Value="" Text="-"></asp:ListItem>
                    <asp:ListItem Value="01">01</asp:ListItem>
                    <asp:ListItem Value="02">02</asp:ListItem>
                    <asp:ListItem Value="03">03</asp:ListItem>
                    <asp:ListItem Value="04">04</asp:ListItem>
                    <asp:ListItem Value="05">05</asp:ListItem>
                    <asp:ListItem Value="06">06</asp:ListItem>
                    <asp:ListItem Value="07">07</asp:ListItem>
                    <asp:ListItem Value="08">08</asp:ListItem>
                    <asp:ListItem Value="09">09</asp:ListItem>
                    <asp:ListItem Value="10">10</asp:ListItem>
                    <asp:ListItem Value="11">11</asp:ListItem>
                    <asp:ListItem Value="12">12</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="divtxtBox  LNOrient" style="width: 60px">
                <asp:DropDownList ID="ddlEndTimeMin" runat="server" CssClass="validate[required] RequiredField  ddlStype" TabIndex="10"
                    Width="45px">
                    <asp:ListItem Value="" Text="-"></asp:ListItem>
                    <asp:ListItem Value="00">00</asp:ListItem>
                    <asp:ListItem Value="05">05</asp:ListItem>
                    <asp:ListItem Value="10">10</asp:ListItem>
                    <asp:ListItem Value="15">15</asp:ListItem>
                    <asp:ListItem Value="20">20</asp:ListItem>
                    <asp:ListItem Value="25">25</asp:ListItem>
                    <asp:ListItem Value="30">30</asp:ListItem>
                    <asp:ListItem Value="35">35</asp:ListItem>
                    <asp:ListItem Value="40">40</asp:ListItem>
                    <asp:ListItem Value="45">45</asp:ListItem>
                    <asp:ListItem Value="50">50</asp:ListItem>
                    <asp:ListItem Value="55">55</asp:ListItem>
                    <asp:ListItem Value="60">60</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="divtxtBox  LNOrient" style="width: 60px">
                <asp:DropDownList ID="ddlEndTimeAMPM" runat="server" CssClass="validate[required] RequiredField  ddlStype" TabIndex="11"
                    Width="45px">
                    <asp:ListItem Value="" Text="-"></asp:ListItem>
                    <asp:ListItem Value="AM">AM</asp:ListItem>
                    <asp:ListItem Value="PM">PM</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
<%--        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            
        </div>--%>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblReason">
                Reason
            </div>
            <div class="divtxtBox  LNOrient" style="width: 345px">
                <asp:DropDownList ID="ddlreason" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    TabIndex="12" >
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblComment">
                Comment
            </div>
            <div class="divtxtBox  LNOrient" style="width: 346px">
                <asp:TextBox ID="txtComment" CssClass=" txtBox" Height = "50px" TextMode = "MultiLine" runat="server" TabIndex="13"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblActive">
                Active
            </div>
            <div class="divtxtBox  LNOrient" style="width: 250px">
                <asp:CheckBox ID="chkActive" runat="server" Checked="True" TabIndex="14" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" OnClick="btnSave_Click"
                            TabIndex="7" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="8" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="9" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="10" />
                    </td>
                </tr>
            </table>
        </div>
        <div class="divClear_10">
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table width="100%">
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/HR/HRChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
