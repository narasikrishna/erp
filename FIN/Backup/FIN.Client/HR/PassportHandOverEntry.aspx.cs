﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.BLL.AP;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.BLL;
using VMVServices.Web;

namespace FIN.Client.HR
{
    public partial class PassportHandOverEntry : PageBase
    {
        HR_EMP_PASSPORT_HANDOVER hR_EMP_PASSPORT_HANDOVER = new HR_EMP_PASSPORT_HANDOVER();
        HR_EMPLOYEES hR_EMPLOYEES = new HR_EMPLOYEES();
        System.Collections.SortedList slControls = new System.Collections.SortedList();
        //string ProReturn = null;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                //pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                //pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
                ddlReturnBy.Visible = true;
                txtReason.Visible = true;
                txtReturnDate.Visible = true;
                ddlReturnBy.Visible = true;

            }

            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
            }

            if (VMVServices.Web.Utils.AddAllowed != FINAppConstants.Y && Master.Mode == FINAppConstants.Add)
            {
                btnSave.Visible = false;
            }

        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                //ddlWeightUOM.Enabled = false;
                //ddlLengthUOM.Enabled = false;
                //ddlAreaUOM.Enabled = false;
                //ddlVolumeUOM.Enabled = false;


                Startup();
                FillComboBox();

                ReturnID.Visible = false;
                divReturnDate.Visible = false;
                EntityData = null;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    using (IRepository<HR_EMP_PASSPORT_HANDOVER> userCtx = new DataRepository<HR_EMP_PASSPORT_HANDOVER>())
                    {
                        hR_EMP_PASSPORT_HANDOVER = userCtx.Find(r =>
                            (r.PASS_HANDOVER_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = hR_EMP_PASSPORT_HANDOVER;
                    ReturnID.Visible = true;
                    divReturnDate.Visible = true;

                    ddlEmployee.SelectedValue = hR_EMP_PASSPORT_HANDOVER.PASS_EMP_ID;
                    fillPassport_handovername();

                    ddlPassportName.SelectedValue = hR_EMP_PASSPORT_HANDOVER.PASS_TXN_ID;
                    //   ddlHandoverType.SelectedValue = hR_EMP_PASSPORT_HANDOVER.PASS_HANDOVER_TYPE;
                    ddlHandoverTo.SelectedValue = hR_EMP_PASSPORT_HANDOVER.PASS_HANDOVER_TO;

                    if (hR_EMP_PASSPORT_HANDOVER.PASS_HANDOVER_DATE != null)
                    {
                        txtHandoverDate.Text = DBMethod.ConvertDateToString(hR_EMP_PASSPORT_HANDOVER.PASS_HANDOVER_DATE.ToString());
                    }
                    if (hR_EMP_PASSPORT_HANDOVER.PASS_RETURN_DATE != null)
                    {
                        txtReturnDate.Text = DBMethod.ConvertDateToString(hR_EMP_PASSPORT_HANDOVER.PASS_RETURN_DATE.ToString());
                    }
                    ddlReturnBy.SelectedValue = hR_EMP_PASSPORT_HANDOVER.PASS_RETURNED_BY;
                    txtReason.Text = hR_EMP_PASSPORT_HANDOVER.PASS_RETURN_REASON;
                    chKEnabledFlag.Checked = hR_EMP_PASSPORT_HANDOVER.ENABLED_FLAG == FINAppConstants.EnabledFlag ? true : false;
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>

        private void FillComboBox()
        {
            FIN.BLL.HR.Employee_BLL.GetEmployeeName(ref ddlEmployee);

            //Lookup_BLL.GetLookUpValues(ref ddlHandoverType, "HANDOVER_TYPE");
            FIN.BLL.HR.PassportHandover_BLL.GetReturnBy(ref ddlReturnBy);



            //FIN.BLL.HR.EmployeeContractAssignment_BLL.fn_ContractName(ref ddlContractName);
            //ComboFilling.fn_getDepartment(ref ddlDepartment);

            //Lookup_BLL.GetLookUpValues(ref ddlType, "VAC_TYPE");


        }

        private void AssignToBE()
        {
            try
            {
                // ErrorCollection.Clear();
                if (EntityData != null)
                {
                    hR_EMP_PASSPORT_HANDOVER = (HR_EMP_PASSPORT_HANDOVER)EntityData;
                }
                hR_EMP_PASSPORT_HANDOVER.PASS_EMP_ID = ddlEmployee.SelectedValue.ToString();

                hR_EMP_PASSPORT_HANDOVER.PASS_TXN_ID = ddlPassportName.SelectedValue.ToString();
                // hR_EMP_PASSPORT_HANDOVER.PASS_HANDOVER_TYPE = ddlHandoverType.SelectedValue.ToString();
                hR_EMP_PASSPORT_HANDOVER.PASS_HANDOVER_TO = ddlHandoverTo.SelectedValue.ToString();
                if (txtHandoverDate.Text != string.Empty)
                {
                    hR_EMP_PASSPORT_HANDOVER.PASS_HANDOVER_DATE = DBMethod.ConvertStringToDate(txtHandoverDate.Text.ToString());
                }
                if (txtReturnDate.Text != string.Empty)
                {
                    hR_EMP_PASSPORT_HANDOVER.PASS_RETURN_DATE = DBMethod.ConvertStringToDate(txtReturnDate.Text.ToString());
                }
                if (ddlReturnBy.SelectedValue != string.Empty || ddlReturnBy.SelectedValue != "")
                {
                    hR_EMP_PASSPORT_HANDOVER.PASS_RETURNED_BY = ddlReturnBy.SelectedValue.ToString();
                }

                if (txtReason.Text != string.Empty || txtReason.Text != "")
                {
                    hR_EMP_PASSPORT_HANDOVER.PASS_RETURN_REASON = txtReason.Text;
                }
                else
                {
                    hR_EMP_PASSPORT_HANDOVER.PASS_RETURN_REASON = null;
                }


                hR_EMP_PASSPORT_HANDOVER.PASS_HANDOVER_ORG_ID = VMVServices.Web.Utils.OrganizationID;


                hR_EMP_PASSPORT_HANDOVER.ENABLED_FLAG = FINAppConstants.Y;
                hR_EMP_PASSPORT_HANDOVER.ENABLED_FLAG = chKEnabledFlag.Checked == true ? FINAppConstants.EnabledFlag : FINAppConstants.DisabledFlag;






                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    hR_EMP_PASSPORT_HANDOVER.MODIFIED_BY = this.LoggedUserName;
                    hR_EMP_PASSPORT_HANDOVER.MODIFIED_DATE = DateTime.Today;
                    //ddlReturnBy.Visible = true;
                }
                else
                {
                    hR_EMP_PASSPORT_HANDOVER.PASS_HANDOVER_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_082.ToString(), false, true);
                    //iNV_ITEM_MASTER.WORKFLOW_COMPLETION_STATUS = "1";
                    hR_EMP_PASSPORT_HANDOVER.CREATED_BY = this.LoggedUserName;
                    hR_EMP_PASSPORT_HANDOVER.CREATED_DATE = DateTime.Today;

                }

                hR_EMP_PASSPORT_HANDOVER.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_EMP_PASSPORT_HANDOVER.PASS_HANDOVER_ID);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();


                slControls[0] = ddlEmployee;
                slControls[1] = ddlPassportName;
                slControls[2] = ddlHandoverTo;
                slControls[3] = txtHandoverDate;
                //slControls[4] = txtReturnDate;
                //slControls[5] = ddlReturnBy;
                slControls[4] = txtReason;

                Dictionary<string, string> Prop_File_Data;
                Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));
                string strCtrlTypes = FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" +
    FINAppConstants.DATE_TIME + "~" + FINAppConstants.TEXT_BOX;
                string strMessage = Prop_File_Data["Department_Name_P"] + " ~ " + Prop_File_Data["Passport_Name_P"] + " ~ " + Prop_File_Data["Handover_To_P"] + " ~ " + Prop_File_Data["Handover_Date_P"] + " ~ " + Prop_File_Data["Reason_P"] + "";
                //string strMessage = "DepartMentName ~ Desgination Name ~ Category ~ Grade ~ Job ~ Position  ~  Start Date ~  Start Date ~ Period End Date ";
                EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

                if (EmptyErrorCollection.Count > 0)
                {
                    ErrorCollection = EmptyErrorCollection;

                }

                if (Master.Mode == FINAppConstants.Update)
                {
                    if (ddlReturnBy.SelectedValue != string.Empty)
                    {
                        if (txtReturnDate.Text.ToString() == string.Empty)
                        {
                            ErrorCollection.Add("Mandatory", "Return Date Cannot be Empty");
                            return;
                        }
                    }
                }

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                if (txtReturnDate.Text.ToString().Length > 0)
                {
                    if ((DBMethod.ConvertStringToDate(txtReturnDate.Text.ToString()) < DBMethod.ConvertStringToDate(txtHandoverDate.Text.ToString())))
                    {
                        ErrorCollection.Add("Invalid Date", "Return date must be greater then or equal to handoverdate");
                        return;
                    }
                }


                AssignToBE();


                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                //emptyvalid();



                // Duplicate Validation Through Backend Package PKG_VALIDATIONS



                //ProReturn = FIN.DAL.AP.Item_DAL.GetSPFOR_DUPLICATE_CHECK(iNV_ITEM_MASTER.ITEM_CODE,txtItemName.Text, iNV_ITEM_MASTER.ORG_ID, iNV_ITEM_MASTER.ITEM_ID);

                //if (ProReturn != string.Empty)
                //{
                //    if (ProReturn != "0")
                //    {
                //        ErrorCollection.Add("ITEMNAME", ProReturn);
                //        if (ErrorCollection.Count > 0)
                //        {
                //            return;
                //        }
                //    }
                //}


                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            DBMethod.SaveEntity<HR_EMP_PASSPORT_HANDOVER>(hR_EMP_PASSPORT_HANDOVER);
                            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                            break;
                        }
                    case FINAppConstants.Update:
                        {

                            DBMethod.SaveEntity<HR_EMP_PASSPORT_HANDOVER>(hR_EMP_PASSPORT_HANDOVER, true);
                            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                            break;

                        }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion




        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                DBMethod.DeleteEntity<HR_EMP_PASSPORT_HANDOVER>(hR_EMP_PASSPORT_HANDOVER);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void ddlEmployee_SelectedIndexChanged(object sender, EventArgs e)
        {

            fillPassport_handovername();

        }
        private void fillPassport_handovername()
        {
            FIN.BLL.HR.PassportHandover_BLL.GetPassportName(ref ddlPassportName, ddlEmployee.SelectedValue.ToString());
            FIN.BLL.HR.PassportHandover_BLL.GetHandoverTo(ref ddlHandoverTo, ddlEmployee.SelectedValue);
        }

        private void FillEmployeeName()
        {
            DataTable dtemployeename = new DataTable();
            dtemployeename = DBMethod.ExecuteQuery(FIN.DAL.HR.EmployeeContractAssignment_DAL.GetEmployeeName(ddlEmployee.SelectedValue)).Tables[0];
            //   txtEmployeeName.Text = dtemployeename.Rows[0]["EMP_FIRST_NAME"].ToString();
        }

        protected void ddlHandoverTo_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void fillReturnto()
        {

        }










    }





}