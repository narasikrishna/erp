﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.DAL.GL;
using FIN.BLL;
using FIN.BLL.AP;
using FIN.BLL.GL;
using FIN.BLL.HR;
using VMVServices.Web;
using System.Data;

namespace FIN.Client.HR
{
    public partial class HolidayMasterEntry : PageBase
    {
        HR_HOLIDAYS_MASTER hR_HOLIDAYS_MASTER = new HR_HOLIDAYS_MASTER();

        DataTable dtGridData = new DataTable();
        Boolean bol_rowVisiable;
        Boolean savedBool;
        DataTable dtData = new DataTable();
        string ProReturn = null;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// .RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }
            UserRightsChecking();

        }
        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }
        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>
        private void FillComboBox()
        {
            AccountingCalendar_BLL.GetCalAcctYear(ref ddlFinancialYear);
        }
        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddlCountry = tmpgvr.FindControl("ddlCountry") as DropDownList;
                DropDownList ddlLocation = tmpgvr.FindControl("ddlLocation") as DropDownList;

                Supplier_BLL.fn_getCountry(ref ddlCountry);


                if (gvData.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                {
                    ddlCountry.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.COUNTRY_CODE].ToString();
                    filllocation(tmpgvr);
                    ddlLocation.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.LOC_ID].ToString();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RI_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void FillFromToDate()
        {
            if (ddlFinancialYear.SelectedValue != null)
            {
                dtData = DBMethod.ExecuteQuery(AccountingCalendar_DAL.GetStartdtEnddtfrcalyear((ddlFinancialYear.SelectedValue.ToString()))).Tables[0];
                if (dtData != null)
                {
                    if (dtData.Rows.Count > 0)
                    {
                        txtFromDate.Text = DateTime.Parse(dtData.Rows[0][FINColumnConstants.CAL_EFF_START_DT].ToString()).ToString("dd/MM/yyyy");

                        if (dtData.Rows[0][FINColumnConstants.CAL_EFF_END_DT].ToString().Count() > 0)
                        {
                            txtToDate.Text = DateTime.Parse(dtData.Rows[0][FINColumnConstants.CAL_EFF_END_DT].ToString()).ToString("dd/MM/yyyy");
                        }
                    }
                }
            }
        }
        protected void ddlFinancialYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                FillFromToDate();
                fillHolidaydtls();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// .RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void fillHolidaydtls()
        {

            dtGridData = DBMethod.ExecuteQuery(HolidayMaster_DAL.GetHolodayDetails_finyr(ddlFinancialYear.SelectedValue)).Tables[0];
            BindGrid(dtGridData);
        }

        protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
                filllocation(gvr);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// .RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void filllocation(GridViewRow gvr)
        {
            DropDownList ddlCountry = gvr.FindControl("ddlCountry") as DropDownList;
            DropDownList ddlLocation = gvr.FindControl("ddlLocation") as DropDownList;

            FIN.BLL.HR.Location_BLL.GetLocationDetails(ref ddlLocation, ddlCountry.SelectedValue);
        }


        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>
        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();

                EntityData = null;
                Session[FINSessionConstants.GridData] = null;

                dtGridData = DBMethod.ExecuteQuery(HolidayMaster_DAL.GetHolodayMasterDetails(Master.StrRecordId)).Tables[0];
                BindGrid(dtGridData);

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    hR_HOLIDAYS_MASTER = HolidayMaster_BLL.getClassEntity(Master.StrRecordId);
                    EntityData = hR_HOLIDAYS_MASTER;
                    ddlFinancialYear.SelectedValue = hR_HOLIDAYS_MASTER.FISCAL_YEAR.ToString();
                    FillFromToDate();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// .RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr["ENABLED_FLAG"] = "FALSE";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountCodesEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        #endregion

        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            if (Session[FINSessionConstants.GridData] != null)
            {
                dtGridData = (DataTable)Session[FINSessionConstants.GridData];
            }
            gvData.EditIndex = -1;

            BindGrid(dtGridData);

        }
        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// .RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// .RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {
            System.Collections.SortedList slControls = new System.Collections.SortedList();

            TextBox txtDate = gvr.FindControl("txtDate") as TextBox;
            TextBox txtDescription = gvr.FindControl("txtDescription") as TextBox;
            CheckBox chkact = gvr.FindControl("chkact") as CheckBox;
            DropDownList ddlCountry = gvr.FindControl("ddlCountry") as DropDownList;
            DropDownList ddlLocation = gvr.FindControl("ddlLocation") as DropDownList;


            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList[FINColumnConstants.HOLIDAY_ID] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);
            }


            ErrorCollection.Clear();

            slControls[0] = txtDate;
            slControls[1] = txtDescription;
            slControls[2] = ddlCountry;
            slControls[3] = ddlLocation;
            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));
            string strCtrlTypes = FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST;
            string strMessage = Prop_File_Data["Date_P"] + " ~ " + Prop_File_Data["Description_P"] + " ~ " + Prop_File_Data["Country_P"] + " ~ " + Prop_File_Data["Location_P"] + "";
            //string strMessage = "Date ~ Description ~ Country ~ Location";


            EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

            if (EmptyErrorCollection.Count > 0)
            {
                ErrorCollection = EmptyErrorCollection;
                return drList;
            }

            ErrorCollection.Clear();
            if (txtFromDate.Text.ToString().Length > 0)
            {
                if (DBMethod.ConvertStringToDate(txtDate.Text) >= DBMethod.ConvertStringToDate(txtFromDate.Text) && DBMethod.ConvertStringToDate(txtDate.Text) <= DBMethod.ConvertStringToDate(txtToDate.Text))
                {

                }
                else
                {
                    ErrorCollection.Add("InvalidDate", "Holiday Date Must be between selected financial Year");
                    return drList;
                }
            }
            else
            {
                ErrorCollection.Add("SelectFinancialyear", "Please Select Financial Year");
                return drList;
            }
            string strCondition = "HOLIDAY_DATE='" + DBMethod.ConvertStringToDate(txtDate.Text.ToString()) + "'";
            strMessage = FINMessageConstatns.RecordAlreadyExists;
            ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            if (ErrorCollection.Count > 0)
            {
                return drList;
            }

            drList[FINColumnConstants.HOLIDAY_DATE] = DBMethod.ConvertStringToDate(txtDate.Text.ToString());
            drList[FINColumnConstants.HOLIDAY_DESC] = txtDescription.Text;

            drList["COUNTRY_CODE"] = ddlCountry.SelectedValue;
            drList["LOC_ID"] = ddlLocation.SelectedValue;

            drList[FINColumnConstants.COUNTRY_NAME] = ddlCountry.SelectedItem.Text;
            drList[FINColumnConstants.LOC_DESC] = ddlLocation.SelectedItem.Text;

            drList[FINColumnConstants.ENABLED_FLAG] = chkact.Checked == true ? FINAppConstants.TRUEFLAG : FINAppConstants.FALSEFLAG;
            drList[FINColumnConstants.DELETED] = FINAppConstants.N;


            return drList;

        }


        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// .RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountCodesEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// .RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// .RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// .RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// .RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// .RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        #endregion

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                System.Collections.SortedList slControls = new System.Collections.SortedList();

                slControls[0] = ddlFinancialYear;
                slControls[1] = txtFromDate;
                Dictionary<string, string> Prop_File_Data;
                Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));
                string strCtrlTypes = FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.TEXT_BOX;
                string strMessage = Prop_File_Data["Financial_Year_P"] + " ~ " + Prop_File_Data["From_Date_P"] + "";
                //string strMessage = "Financial Year ~ From Date";

                EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

                if (EmptyErrorCollection.Count > 0)
                {
                    ErrorCollection = EmptyErrorCollection;
                    return;
                }

                AssignToBE();

                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// .RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();



                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                ErrorCollection = CommonUtils.IsEmptyGrid(dtGridData, "Holiday ");
                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                var tmpChildEntity = new List<Tuple<object, string>>();


                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {

                    if (dtGridData.Rows[iLoop][FINColumnConstants.HOLIDAY_ID].ToString() != "0")
                    {
                        hR_HOLIDAYS_MASTER = HolidayMaster_BLL.getClassEntity(dtGridData.Rows[iLoop][FINColumnConstants.HOLIDAY_ID].ToString());
                    }
                    if (dtGridData.Rows[iLoop][FINColumnConstants.HOLIDAY_DATE] != DBNull.Value)
                    {
                        hR_HOLIDAYS_MASTER.HOLIDAY_DATE = DateTime.Parse(dtGridData.Rows[iLoop][FINColumnConstants.HOLIDAY_DATE].ToString());
                    }
                    hR_HOLIDAYS_MASTER.FISCAL_YEAR = (ddlFinancialYear.SelectedValue.ToString());


                    hR_HOLIDAYS_MASTER.ORG_ID = VMVServices.Web.Utils.OrganizationID;

                    hR_HOLIDAYS_MASTER.HOLIDAY_DESC = (dtGridData.Rows[iLoop][FINColumnConstants.HOLIDAY_DESC].ToString());
                    hR_HOLIDAYS_MASTER.ENABLED_FLAG = (dtGridData.Rows[iLoop][FINColumnConstants.ENABLED_FLAG].ToString() == FINAppConstants.TRUEFLAG ? FINAppConstants.EnabledFlag : FINAppConstants.DisabledFlag);

                    hR_HOLIDAYS_MASTER.COUNTRY_ID = (dtGridData.Rows[iLoop]["COUNTRY_CODE"].ToString());
                    hR_HOLIDAYS_MASTER.HOLIDAY_LOC_ID = (dtGridData.Rows[iLoop]["LOC_ID"].ToString());



                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        tmpChildEntity.Add(new Tuple<object, string>(hR_HOLIDAYS_MASTER, FINAppConstants.Delete));
                    }
                    else
                    {

                        ProReturn = FIN.DAL.HR.HolidayMaster_DAL.GetSPFOR_DUPLICATE_CHECK(DateTime.Parse(hR_HOLIDAYS_MASTER.HOLIDAY_DATE.ToString()).ToString("dd/MM/yyyy"), hR_HOLIDAYS_MASTER.HOLIDAY_ID);

                        if (ProReturn != string.Empty)
                        {
                            if (ProReturn != "0")
                            {
                                ErrorCollection.Add("HOLIDAY", ProReturn);
                                if (ErrorCollection.Count > 0)
                                {
                                    return;
                                }
                            }
                        }



                        if (dtGridData.Rows[iLoop][FINColumnConstants.HOLIDAY_ID].ToString() != "0")
                        {
                            hR_HOLIDAYS_MASTER.HOLIDAY_ID = dtGridData.Rows[iLoop][FINColumnConstants.HOLIDAY_ID].ToString();
                            hR_HOLIDAYS_MASTER.MODIFIED_BY = this.LoggedUserName;
                            hR_HOLIDAYS_MASTER.MODIFIED_DATE = DateTime.Today;
                            hR_HOLIDAYS_MASTER.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_HOLIDAYS_MASTER.HOLIDAY_ID.ToString());
                            DBMethod.SaveEntity<HR_HOLIDAYS_MASTER>(hR_HOLIDAYS_MASTER, true);
                            savedBool = true;
                        }
                        else
                        {
                            hR_HOLIDAYS_MASTER.HOLIDAY_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_017.ToString(), false, true);
                            hR_HOLIDAYS_MASTER.CREATED_BY = this.LoggedUserName;
                            hR_HOLIDAYS_MASTER.CREATED_DATE = DateTime.Today;
                            hR_HOLIDAYS_MASTER.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_HOLIDAYS_MASTER.HOLIDAY_ID.ToString());
                            DBMethod.SaveEntity<HR_HOLIDAYS_MASTER>(hR_HOLIDAYS_MASTER);
                            savedBool = true;
                        }

                        DataTable dt_payPeriodDet = DBMethod.ExecuteQuery(FIN.DAL.PER.PayrollPeriods_DAL.GetPayrollPeriods4Holiday(hR_HOLIDAYS_MASTER.HOLIDAY_ID.ToString())).Tables[0];
                        if (dt_payPeriodDet.Rows.Count > 0)
                        {
                            PAY_PERIODS pAY_PERIODS = new PAY_PERIODS();
                            using (IRepository<PAY_PERIODS> userCtx = new DataRepository<PAY_PERIODS>())
                            {
                                pAY_PERIODS = userCtx.Find(r =>
                                    (r.PAY_PERIOD_ID == dt_payPeriodDet.Rows[0]["PAY_PERIOD_ID"].ToString())
                                    ).SingleOrDefault();
                            }



                            DateTime startDate = DateTime.Parse(dt_payPeriodDet.Rows[0]["PAY_FROM_DT"].ToString());
                            DateTime endDate = DateTime.Parse(dt_payPeriodDet.Rows[0]["PAY_TO_DT"].ToString());
                            int INT_Friday = ClsGridBase.CountDays(DayOfWeek.Friday, startDate, endDate);

                            TimeSpan ts = endDate - startDate;
                            DataTable dt_holidays = DBMethod.ExecuteQuery(FIN.DAL.HR.HolidayMaster_DAL.GetNoOfHolidaysBWDays(DBMethod.ConvertDateToString(startDate.ToShortDateString()), DBMethod.ConvertDateToString(endDate.ToShortDateString()))).Tables[0];
                            DataTable dt_fixedDays = DBMethod.ExecuteQuery(FIN.DAL.SSM.SystemParameters_DAL.GetFixedDays()).Tables[0];

                            pAY_PERIODS.NO_FRIDAYS = INT_Friday;
                            if (int.Parse(dt_fixedDays.Rows[0][0].ToString()) == 0)
                            {
                                pAY_PERIODS.NO_WORKING_DAYS = int.Parse(((ts.TotalDays - INT_Friday) + 1).ToString());
                            }
                            else
                            {
                                pAY_PERIODS.NO_WORKING_DAYS = int.Parse(dt_fixedDays.Rows[0][0].ToString());
                            }
                            pAY_PERIODS.NO_HOLIDAYS = int.Parse(dt_holidays.Rows[0][0].ToString()); 


                            DBMethod.SaveEntity<PAY_PERIODS>(pAY_PERIODS, true);
                        }


                    }

                }


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// .RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                // DBMethod.DeleteEntity<INV_RECEIPTS_HDR>(INV_RECEIPTS_HDR);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// .RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        #endregion

        protected void btnDelete_Click(object sender, EventArgs e)
        {

        }


    }
}