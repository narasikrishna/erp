﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.DAL.GL;
using FIN.BLL;
using FIN.BLL.GL;
using FIN.BLL.HR;
using VMVServices.Web;


namespace FIN.Client.HR
{


    public partial class Initiate_AppraisalEntry : PageBase
    {
        HR_PER_APP_INIT hR_PER_APP_INIT = new HR_PER_APP_INIT();
        
        DataTable dtData = new DataTable();
        Boolean bol_rowVisiable;
        Boolean savedBool;
        DataTable dtGridData = new DataTable();

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));


            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
        }

        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        private void FillComboBox()
        {
            //AccountingCalendar_BLL.GetFinancialYear(ref ddlFinancialYear);
            Department_BLL.GetDepartmentName(ref ddlDepartment);
            AppraisalDefineKRAEntry_BLL.fn_GetJobDetails(ref ddlJob);
            //  Employee_BLL.GetEmployeeName(ref ddlStaffName);
        }
        private void FillFromToDate()
        {
            //if (ddlFinancialYear.SelectedValue != null)
            //{
            //    dtData = DBMethod.ExecuteQuery(AccountingCalendar_DAL.GetFinancialYear((ddlFinancialYear.SelectedValue.ToString()))).Tables[0];
            //    if (dtData != null)
            //    {
            //        if (dtData.Rows.Count > 0)
            //        {
            //            if (dtData.Rows[0][FINColumnConstants.CAL_EFF_START_DT] != string.Empty)
            //            {
            //                txtFromDate.Text = DateTime.Parse(dtData.Rows[0][FINColumnConstants.CAL_EFF_START_DT].ToString()).ToString("dd/MM/yyyy");
            //            }
            //            if (dtData.Rows[0][FINColumnConstants.CAL_EFF_END_DT] != string.Empty)
            //            {
            //                txtToDate.Text = DateTime.Parse(dtData.Rows[0][FINColumnConstants.CAL_EFF_END_DT].ToString()).ToString("dd/MM/yyyy");
            //            }
            //        }
            //    }
            //}
        }
        protected void ddlFinancialYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                FillFromToDate();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// .RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void FillLeaveRequest(GridViewRow gvr)
        {
            // GridViewRow gvr = gvData.FooterRow;

            DropDownList ddlStaff = gvr.FindControl("ddlStaff") as DropDownList;
            DropDownList ddlAttendanceType = gvr.FindControl("ddlAttendanceType") as DropDownList;
            DropDownList ddlRequest = gvr.FindControl("ddlRequest") as DropDownList;

            if (ddlStaff.SelectedValue != "")
            {
                LeaveApplication_BLL.GetLeaveApplication(ref ddlRequest, ddlStaff.SelectedValue.ToString());

                if (gvData.EditIndex >= 0)
                {
                    ddlRequest.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.LEAVE_REQ_ID].ToString();
                }
            }

            if (ddlAttendanceType.SelectedItem.Text == "Leave")
            {
                ddlRequest.Enabled = true;
            }
            else
            {
                ddlRequest.Enabled = false;
            }
        }
        protected void ddlRequest_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
                FillLeaveRequest(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// .RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();
                EntityData = null;
                Session[FINSessionConstants.GridData] = null;

                

               

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    HR_PER_APP_INIT hR_PER_APP_INIT = new HR_PER_APP_INIT();
                    hR_PER_APP_INIT = InitiateApprisal_BLL.getClassEntity(Master.StrRecordId);
                    EntityData = hR_PER_APP_INIT;


                  
                    HR_PER_APP_REVIEW_ASGNMNT_HDR hR_PER_APP_REVIEW_ASGNMNT_HDR = new HR_PER_APP_REVIEW_ASGNMNT_HDR();
                    using (IRepository<HR_PER_APP_REVIEW_ASGNMNT_HDR> userCtx = new DataRepository<HR_PER_APP_REVIEW_ASGNMNT_HDR>())
                    {
                        hR_PER_APP_REVIEW_ASGNMNT_HDR = userCtx.Find(r =>
                            (r.RA_HDR_ID == hR_PER_APP_INIT.RA_HDR_ID.ToString())
                            ).SingleOrDefault();
                    }

                    ddlJob.SelectedValue = hR_PER_APP_REVIEW_ASGNMNT_HDR.RA_JOB_ID.ToString();
                    
                    ddlDepartment.SelectedValue = hR_PER_APP_REVIEW_ASGNMNT_HDR.RA_DEPT_ID.ToString();
                    FillAppraisalDefineKPI();
                    ddlAppraisal.SelectedValue = hR_PER_APP_INIT.RA_HDR_ID.ToString();
                   
                    FillFromToDate();

                    //HR_EMP_WORK_DTLS HR_EMP_WORK_DTLS = new HR_EMP_WORK_DTLS();
                    //HR_EMP_WORK_DTLS = Workdetails_BLL.getEmpWorkDetails(HR_STAFF_ATTENDANCE.STAFF_ID);
                    //if (HR_EMP_WORK_DTLS != null)
                    //{
                    //    ddlDepartment.SelectedValue = HR_EMP_WORK_DTLS.EMP_DEPT_ID;
                    //}

                    //ddlDepartment.SelectedValue = HR_STAFF_ATTENDANCE.ATTRIBUTE1;
                    if (hR_PER_APP_INIT.APP_INIT_DATE != null)
                    {
                        txtPeriodStartDate.Text = DBMethod.ConvertDateToString(hR_PER_APP_INIT.APP_INIT_DATE.ToString());
                    }
                    dtGridData = DBMethod.ExecuteQuery(InitiateApprisal_DAL.GetInitiateAppraisal(Master.StrRecordId)).Tables[0];
                    BindGrid(dtGridData);
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Vaca_Entry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);

                }
            }

        }

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr["ENABLED_FLAG"] = "FALSE";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
        
               

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
  
        #endregion
        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                //ErrorCollection.Clear();
                //AssignToBE();
                //System.Collections.SortedList slControls = new System.Collections.SortedList();

                ////slControls[0] = ddlFinancialYear;
                ////slControls[1] = txtFromDate;
                ////slControls[2] = ddlDepartment;
                ////slControls[3] = txtAttendanceDate;

                //string strCtrlTypes = FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.TEXT_BOX;
                //string strMessage = "Employee Number ~ Employee Name";

               

                ////if (DBMethod.ConvertStringToDate(txtFromDate.Text) <= DBMethod.ConvertStringToDate(txtAttendanceDate.Text) && DBMethod.ConvertStringToDate(txtToDate.Text) >= DBMethod.ConvertStringToDate(txtAttendanceDate.Text))
                ////{

                ////}
                ////else
                ////{
                ////    ErrorCollection.Add("invalidAttendance", "Attendance Date should be between From and To Date");
                ////    return;
                ////}

                ErrorCollection.Clear();

                ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session[FINSessionConstants.GridData], "Initiate Appraisal ");

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                AssignToBE();

                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                var tmpChildEntity = new List<Tuple<object, string>>();

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    hR_PER_APP_INIT = new HR_PER_APP_INIT();
                    if (dtGridData.Rows[iLoop][FINColumnConstants.PER_APP_INIT_ID].ToString() != "0")
                    {
                        hR_PER_APP_INIT = InitiateApprisal_BLL.getClassEntity(dtGridData.Rows[iLoop][FINColumnConstants.PER_APP_INIT_ID].ToString());
                    }

                    //if ((dtGridData.Rows[iLoop][FINColumnConstants.PER_APP_INIT_ID].ToString()) != "0" && dtGridData.Rows[iLoop][FINColumnConstants.PER_APP_INIT_ID].ToString() != string.Empty)
                    //{
                    //    hR_PER_APP_INIT = InitiateApprisal_BLL.getClassEntity(dtGridData.Rows[iLoop][FINColumnConstants.PER_APP_INIT_ID].ToString());

                    //}
                    hR_PER_APP_INIT.PER_APP_EMP_ID= dtGridData.Rows[iLoop][FINColumnConstants.EMP_ID].ToString();
                 //   hR_PER_APP_INIT.PER_APP_EMP_ID = dtGridData.Rows[iLoop][FINColumnConstants.EMP_FIRST_NAME].ToString();
                    hR_PER_APP_INIT.RA_HDR_ID = dtGridData.Rows[iLoop][FINColumnConstants.RA_HDR_ID].ToString();
                    //hR_PER_APP_INIT. = ddlDepartment.SelectedValue;
                    //hR_PER_APP_REVIEW_ASGNMNT_HDR.RA_JOB_ID = ddlJob.SelectedValue;
                    if (txtPeriodStartDate.Text.Trim() != string.Empty)
                    {
                        hR_PER_APP_INIT.APP_INIT_DATE = DBMethod.ConvertStringToDate(txtPeriodStartDate.Text.ToString());
                    }

                    if (dtGridData.Rows[iLoop][FINColumnConstants.ENABLED_FLAG].ToString().ToUpper() == "TRUE")
                    {
                        hR_PER_APP_INIT.ENABLED_FLAG = FINAppConstants.Y;
                    }
                    else
                    {
                        hR_PER_APP_INIT.ENABLED_FLAG = FINAppConstants.N;
                    }

                    //hR_PER_APP_INIT.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                    hR_PER_APP_INIT.PER_ORG_ID = VMVServices.Web.Utils.OrganizationID;
                    

                    //if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    //{
                    //    tmpChildEntity.Add(new Tuple<object, string>(hR_PER_APP_INIT, "D"));
                    //}
                    //else
                    //{
                        if ((dtGridData.Rows[iLoop][FINColumnConstants.PER_APP_INIT_ID].ToString()) != "0" && dtGridData.Rows[iLoop][FINColumnConstants.PER_APP_INIT_ID].ToString() != string.Empty)
                        {
                            hR_PER_APP_INIT.PER_APP_INIT_ID = dtGridData.Rows[iLoop]["PER_APP_INIT_ID"].ToString();
                            hR_PER_APP_INIT.MODIFIED_BY = this.LoggedUserName;
                            hR_PER_APP_INIT.MODIFIED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(hR_PER_APP_INIT, FINAppConstants.Update));
                        }
                        else
                        {
                            //HR_STAFF_ATTENDANCE.STATT_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_022);
                            hR_PER_APP_INIT.PER_APP_INIT_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_055.ToString(), false, true);
                            hR_PER_APP_INIT.CREATED_BY = this.LoggedUserName;
                            hR_PER_APP_INIT.CREATED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(hR_PER_APP_INIT, FINAppConstants.Add));
                        }
                        hR_PER_APP_INIT.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_PER_APP_INIT.PER_APP_INIT_ID);
                   /// }
                }
                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            CommonUtils.SaveSingleEntity<HR_PER_APP_INIT>(tmpChildEntity);
                            savedBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {
                            CommonUtils.SaveSingleEntity<HR_PER_APP_INIT>(tmpChildEntity, true);
                            savedBool = true;
                            break;
                        }
                }

                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                if (hR_PER_APP_INIT.WORKFLOW_COMPLETION_STATUS == "1")
                {
                    FIN.BLL.SSM.Alert_BLL.GenerateEmail(FINMessageConstatns.Initiate_Appraisal_Alert);
                    if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0" && savedBool == true)
                    {
                        if (VMVServices.Web.Utils.IsAlert == "1")
                        {
                            FINSQL.UpdateAlertUserLevel();
                        }
                    }
                }
                if (ErrorCollection.Count > 0)
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {

                    // HR_STAFF_ATTENDANCE.LSD_ID = (dtGridData.Rows[iLoop]["LSD_ID"].ToString());
                    DBMethod.DeleteEntity<HR_PER_APP_INIT>(hR_PER_APP_INIT);
                }


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion


        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            try
            {
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = -1;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_ROW_CNCL_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_ROW_CMD", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {


            System.Collections.SortedList slControls = new System.Collections.SortedList();

            DropDownList ddlEmployeeNumber = gvr.FindControl("ddlEmployeeNumber") as DropDownList;
            DropDownList ddlEmployeeName = gvr.FindControl("ddlEmployeeName") as DropDownList;
            CheckBoxList chkselect = gvr.FindControl("chkselect") as CheckBoxList;



            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList[FINColumnConstants.PER_APP_INIT_ID] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }

            ErrorCollection.Clear();

            string strCtrlTypes = string.Empty;
            string strMessage = string.Empty;

            if (ddlEmployeeNumber.SelectedItem.Text != FINMessageConstatns.PER_APP_EMP_ID.ToString())
            {
                slControls[0] = ddlEmployeeNumber;
                slControls[1] = ddlEmployeeName;
                Dictionary<string, string> Prop_File_Data;
                Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));
                strCtrlTypes = FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST;
                 strMessage = Prop_File_Data["Employee_Number_P"] + " ~ " + Prop_File_Data["Employee_Name_P"] + "";
                //strMessage = "Employee Number ~ Employee Name ";
            }
            //else if (chkSelect.ch == FINMessageConstatns.AttendanceType_Present.ToString())
            //{
            //    slControls[0] = ddlStaff;
            //    slControls[1] = ddlAttendanceType;
            //    slControls[3] = ddlRequest;

            //    strCtrlTypes = FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST;
            //    strMessage = "Staff Name ~ Attendance type ~ Leave Request";
            //}

            ErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);
            if (ErrorCollection.Count > 0)
                return drList;

            string strCondition = "EMP_ID='" + ddlEmployeeNumber.SelectedValue.ToString() + "'";
            strMessage = FINMessageConstatns.RecordAlreadyExists;
            ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);

            if (ErrorCollection.Count > 0)
            {
                return drList;
            }

            drList[FINColumnConstants.EMP_ID] = ddlEmployeeNumber.SelectedValue;
            drList[FINColumnConstants.EMP_NAME] = ddlEmployeeName.SelectedItem.Text;

            //if (CheckBoxSelect.Checked)
            //{
            //    drList[FINColumnConstants.ENABLED_FLAG] = "TRUE";
            //}
            //else
            //{
            //    drList[FINColumnConstants.ENABLED_FLAG] = "FALSE";
            //}
            drList[FINColumnConstants.DELETED] = FINAppConstants.N;


            return drList;



        }



        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        //{
        //    try
        //    {
        //        ErrorCollection.Clear();
        //        GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
        //        DataRow drList = null;

        //        if (Session[FINSessionConstants.GridData] != null)
        //        {
        //            dtGridData = (DataTable)Session[FINSessionConstants.GridData];
        //        }
        //        if (gvr == null)
        //        {
        //            return;
        //        }

        //        drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
        //        if (ErrorCollection.Count > 0)
        //        {
        //            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
        //            //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
        //            return;
        //        }
        //        gvData.EditIndex = -1;
        //        BindGrid(dtGridData);
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorCollection.Add("CATG_ROW_UP", ex.Message);
        //    }
        //    finally
        //    {
        //        if (ErrorCollection.Count > 0)
        //        {
        //            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
        //            //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
        //        }
        //    }
        //}


        //protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        //{
        //    try
        //    {
        //        ErrorCollection.Clear();


        //        if (Session[FINSessionConstants.GridData] != null)
        //        {
        //            dtGridData = (DataTable)Session[FINSessionConstants.GridData];
        //        }
        //        DataRow drList = null;
        //        drList = dtGridData.Rows[e.RowIndex];
        //        drList[FINColumnConstants.DELETED] = FINAppConstants.Y;

        //        BindGrid(dtGridData);
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorCollection.Add("CATG_ROW_DEL", ex.Message);
        //    }
        //    finally
        //    {
        //        if (ErrorCollection.Count > 0)
        //        {
        //            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
        //            //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
        //        }
        //    }
        //}


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        //protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        //{
        //    try
        //    {
        //        ErrorCollection.Clear();
        //        if (Session[FINSessionConstants.GridData] != null)
        //        {
        //            dtGridData = (DataTable)Session[FINSessionConstants.GridData];
        //        }
        //        gvData.EditIndex = e.NewEditIndex;
        //        BindGrid(dtGridData);
        //        GridViewRow gvr = gvData.Rows[e.NewEditIndex];
        //        //FillFooterGridCombo(gvr);


        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorCollection.Add("CATG_ROW_EDT", ex.Message);
        //    }
        //    finally
        //    {
        //        if (ErrorCollection.Count > 0)
        //        {
        //            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
        //            //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
        //        }
        //    }
        //}

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_RowCreated", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    //if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    //{
                    //    e.Row.Visible = false;
                    //}

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_RDB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }



        #endregion

        protected void ddlAppraisal_SelectedIndexChanged(object sender, EventArgs e)
        {

            dtGridData = DBMethod.ExecuteQuery(AppraisalDefineKRA_DAL.GetAppraisal_Employee(ddlAppraisal.SelectedValue.ToString())).Tables[0];
            BindGrid(dtGridData);

        }

        protected void ddlJob_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillAppraisalDefineKPI();
        }
        private void FillAppraisalDefineKPI()
        {
            AppraisalDefineKRAEntry_BLL.fn_GetAppraisalDescriptionDetails(ref ddlAppraisal, ddlDepartment.SelectedValue, ddlJob.SelectedValue);

        }

       
        // );
    }
}