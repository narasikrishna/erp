﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.DAL.GL;
using FIN.BLL;
using FIN.BLL.GL;
using FIN.BLL.HR;
using VMVServices.Web;

namespace FIN.Client.HR
{
    public partial class EmployeeContracDetailsEntry : PageBase
    {
        HR_EMP_CONTRACT_HDR hR_EMP_CONTRACT_HDR = new HR_EMP_CONTRACT_HDR();
        HR_EMP_CONTRACT_DTL hR_EMP_CONTRACT_DTL = new HR_EMP_CONTRACT_DTL();
        DataTable dtGridData = new DataTable();
        Boolean bol_rowVisiable;
        Boolean savedBool;
        string ProReturn = null;
        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TAE", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));


            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
        }

        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        private void FillComboBox()
        {
            CareerPlanDtls_BLL.GetDepartmentName(ref ddlDepartment);
            Categories_BLL.fn_getCategory(ref ddlcategory, Master.Mode);
            //CareerPlanDtls_BLL.GetCareerPath(ref ddlCareerPathName);
            //FIN.BLL.HR.Employee_BLL.GetEmployeeName(ref ddlEmployee);
        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                Startup();
                FillComboBox();
                EntityData = null;
                Session[FINSessionConstants.GridData] = null;

                dtGridData = FIN.BLL.HR.EmployeeContractDetails_BLL.getChildEntityDet(Master.StrRecordId);
                BindGrid(dtGridData);

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    using (IRepository<HR_EMP_CONTRACT_HDR> userCtx = new DataRepository<HR_EMP_CONTRACT_HDR>())
                    {
                        hR_EMP_CONTRACT_HDR = userCtx.Find(r =>
                            (r.CONTRACT_HDR_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = hR_EMP_CONTRACT_HDR;

                    txtContactDesc.Text = hR_EMP_CONTRACT_HDR.CONTRACT_DESC.ToString();

                    ddlcategory.SelectedValue = hR_EMP_CONTRACT_HDR.CONTRACT_EMP_CATEGORY.ToString();
                    filljob4Category();
                    ddlJobs.SelectedValue = hR_EMP_CONTRACT_HDR.CONTRACT_EMP_JOB.ToString();
                    fn_fillPosition4Job();
                    ddlPosition.SelectedValue = hR_EMP_CONTRACT_HDR.POSITION_ID;
                    fn_fllGrade4Position();                    
                    ddlGrade.SelectedValue= hR_EMP_CONTRACT_HDR.CONTRACT_EMP_GRADE.ToString();                    
                   
                    ddlDepartment.SelectedValue = hR_EMP_CONTRACT_HDR.CONTRACT_EMP_DEPT_ID.ToString();
                    fn_fill_Designation();
                    ddlDesignation.SelectedValue = hR_EMP_CONTRACT_HDR.CONTRACT_EMP_DESIG_ID.ToString();
                    txtStartDate.Text = DBMethod.ConvertDateToString(hR_EMP_CONTRACT_HDR.CONTRACT_EFFECTIVE_FROM_DT.ToString());

                    if (hR_EMP_CONTRACT_HDR.CONTRACT_EFFECTIVE_TO_DT != null)
                    {
                        txtEndDate.Text = DBMethod.ConvertDateToString(hR_EMP_CONTRACT_HDR.CONTRACT_EFFECTIVE_TO_DT.ToString());
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TAEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);

                }
            }

        }

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr["CONTRACT_TC_APPLICABLE"]= false;
                    dr["ENABLED_FLAG"] = false;
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TAE_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        #endregion
        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                System.Collections.SortedList slControls = new System.Collections.SortedList();

                slControls[0] = txtContactDesc;
                slControls[1] = ddlDepartment;
                slControls[2] = ddlDesignation;
                slControls[3] = txtStartDate;
                Dictionary<string, string> Prop_File_Data;
                Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));

                string strCtrlTypes = FINAppConstants.TEXT_BOX + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST +"~" + FINAppConstants.TEXT_BOX;
                string strMessage = Prop_File_Data["Contract_Description_P"] + " ~ " + Prop_File_Data["Department_P"] + " ~ " + Prop_File_Data["Description_P"] + " ~ " + Prop_File_Data["Start_Date_P"] +  "";
                //string strMessage = "Contract Description ~ Department ~ Description ~ Start Date ";

                EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

                if (EmptyErrorCollection.Count > 0)
                {
                    ErrorCollection = EmptyErrorCollection;
                    return;
                }

                ErrorCollection.Clear();
                AssignToBE();
                ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session[FINSessionConstants.GridData], Prop_File_Data["Employee_Contract_Details_P"]);

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    hR_EMP_CONTRACT_HDR = (HR_EMP_CONTRACT_HDR)EntityData;
                }

                hR_EMP_CONTRACT_HDR.CONTRACT_DESC = txtContactDesc.Text.ToString();
                hR_EMP_CONTRACT_HDR.CONTRACT_EMP_CATEGORY = ddlcategory.SelectedValue;
                hR_EMP_CONTRACT_HDR.CONTRACT_EMP_JOB = ddlJobs.SelectedValue;
                hR_EMP_CONTRACT_HDR.POSITION_ID = ddlPosition.SelectedValue;
                hR_EMP_CONTRACT_HDR.CONTRACT_EMP_GRADE = ddlGrade.SelectedValue.ToString();
                hR_EMP_CONTRACT_HDR.CONTRACT_EMP_DEPT_ID = ddlDepartment.SelectedValue.ToString();
                hR_EMP_CONTRACT_HDR.CONTRACT_EMP_DESIG_ID = ddlDesignation.SelectedValue.ToString();
                hR_EMP_CONTRACT_HDR.CONTRACT_ORG_ID = VMVServices.Web.Utils.OrganizationID;
                hR_EMP_CONTRACT_HDR.CONTRACT_EFFECTIVE_FROM_DT = DBMethod.ConvertStringToDate(txtStartDate.Text.ToString());
                if (txtEndDate.Text != string.Empty)
                {
                    hR_EMP_CONTRACT_HDR.CONTRACT_EFFECTIVE_TO_DT = DBMethod.ConvertStringToDate(txtEndDate.Text.ToString());
                }

                hR_EMP_CONTRACT_HDR.ENABLED_FLAG = FINAppConstants.EnabledFlag;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    hR_EMP_CONTRACT_HDR.MODIFIED_BY = this.LoggedUserName;
                    hR_EMP_CONTRACT_HDR.MODIFIED_DATE = DateTime.Today;
                }
                else
                {
                    hR_EMP_CONTRACT_HDR.CONTRACT_HDR_ID = FINSP.GetSPFOR_SEQCode("HR_076_M".ToString(), false, true);
                    //hR_TRM_FEEDBACK_HDR.PK_ID = DBMethod.GetPrimaryKeyValue(FINSequenceConstant.hR_TRM_SCHEDULE_HDR_SEQ);
                    hR_EMP_CONTRACT_HDR.CREATED_BY = this.LoggedUserName;
                    hR_EMP_CONTRACT_HDR.CREATED_DATE = DateTime.Today;
                }

                hR_EMP_CONTRACT_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_EMP_CONTRACT_HDR.CONTRACT_HDR_ID);


                // Duplicate Validation Through Backend Package PKG_VALIDATIONS
                ProReturn = FIN.DAL.HR.EmployeeContractDetails_DAL.GetSPFOR_ERR_MGR_CATEGORY(Master.FormCode, hR_EMP_CONTRACT_HDR.CONTRACT_HDR_ID, hR_EMP_CONTRACT_HDR.CONTRACT_DESC);

                if (ProReturn != string.Empty)
                {
                    if (ProReturn != "0")
                    {
                        ErrorCollection.Add("EMP_CONTR_DTLS", ProReturn);
                        if (ErrorCollection.Count > 0)
                        {
                            return;
                        }
                    }
                }

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                var tmpChildEntity = new List<Tuple<object, string>>();

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    hR_EMP_CONTRACT_DTL = new HR_EMP_CONTRACT_DTL();
                    if (dtGridData.Rows[iLoop]["CONTRACT_DTL_ID"].ToString() != "0" && dtGridData.Rows[iLoop]["CONTRACT_DTL_ID"].ToString() != string.Empty)
                    {
                        using (IRepository<HR_EMP_CONTRACT_DTL> userCtx = new DataRepository<HR_EMP_CONTRACT_DTL>())
                        {
                            hR_EMP_CONTRACT_DTL = userCtx.Find(r =>
                                (r.CONTRACT_DTL_ID == dtGridData.Rows[iLoop]["CONTRACT_DTL_ID"].ToString())
                                ).SingleOrDefault();
                        }
                    }

                    hR_EMP_CONTRACT_DTL.CONTRACT_LINE_NUM = short.Parse(dtGridData.Rows[iLoop]["CONTRACT_LINE_NUM"].ToString());
                    hR_EMP_CONTRACT_DTL.CONTRACT_TERM_CONDTIONS = dtGridData.Rows[iLoop]["CONTRACT_TERM_CONDTIONS"].ToString();
                    hR_EMP_CONTRACT_DTL.CONTRACT_REMARKS = dtGridData.Rows[iLoop]["CONTRACT_REMARKS"].ToString();

                    hR_EMP_CONTRACT_DTL.CONTRACT_HDR_ID = hR_EMP_CONTRACT_HDR.CONTRACT_HDR_ID;
                    hR_EMP_CONTRACT_DTL.WORKFLOW_COMPLETION_STATUS = "1";

                    if (dtGridData.Rows[iLoop]["CONTRACT_TC_APPLICABLE"].ToString() == "TRUE")
                    {
                        hR_EMP_CONTRACT_DTL.CONTRACT_TC_APPLICABLE = FINAppConstants.Y;
                    }
                    else
                    {
                        hR_EMP_CONTRACT_DTL.CONTRACT_TC_APPLICABLE = FINAppConstants.N;
                    }
                    if (dtGridData.Rows[iLoop]["ENABLED_FLAG"].ToString() == "TRUE")
                    {
                        hR_EMP_CONTRACT_DTL.ENABLED_FLAG = FINAppConstants.Y;
                    }
                    else
                    {
                        hR_EMP_CONTRACT_DTL.ENABLED_FLAG = FINAppConstants.N;
                    }

                    //if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    //{
                    //    tmpChildEntity.Add(new Tuple<object, string>(hR_TRM_FEEDBACK_DTL, "D"));
                    //}
                    //else
                    //{
                    if (dtGridData.Rows[iLoop]["CONTRACT_DTL_ID"].ToString() != "0" && dtGridData.Rows[iLoop]["CONTRACT_DTL_ID"].ToString() != string.Empty)
                    {
                        hR_EMP_CONTRACT_DTL.CONTRACT_DTL_ID = dtGridData.Rows[iLoop]["CONTRACT_DTL_ID"].ToString();
                        hR_EMP_CONTRACT_DTL.MODIFIED_BY = this.LoggedUserName;
                        hR_EMP_CONTRACT_DTL.MODIFIED_DATE = DateTime.Today;

                        tmpChildEntity.Add(new Tuple<object, string>(hR_EMP_CONTRACT_DTL, "U"));
                    }
                    else
                    {
                        hR_EMP_CONTRACT_DTL.CONTRACT_DTL_ID = FINSP.GetSPFOR_SEQCode("HR_076_D".ToString(), false, true);
                        hR_EMP_CONTRACT_DTL.CREATED_BY = this.LoggedUserName;
                        hR_EMP_CONTRACT_DTL.CREATED_DATE = DateTime.Today;
                        //DBMethod.SaveEntity<GL_ACCT_CODE_SEGMENTS>(gL_ACCT_CODE_SEGMENTS);
                        tmpChildEntity.Add(new Tuple<object, string>(hR_EMP_CONTRACT_DTL, "A"));
                    }
                    //}
                }

                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            FIN.BLL.HR.Competency_BLL.SavePCEntity<HR_EMP_CONTRACT_HDR, HR_EMP_CONTRACT_DTL>(hR_EMP_CONTRACT_HDR, tmpChildEntity, hR_EMP_CONTRACT_DTL);
                            savedBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {
                            FIN.BLL.HR.Competency_BLL.SavePCEntity<HR_EMP_CONTRACT_HDR, HR_EMP_CONTRACT_DTL>(hR_EMP_CONTRACT_HDR, tmpChildEntity, hR_EMP_CONTRACT_DTL, true);
                            savedBool = true;
                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TAEEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }


        protected void ddlDept_SelectedIndexChanged(object sender, EventArgs e)
        {
            fn_fill_Designation();
        }
        
        private void fn_fill_Designation()
        {

            AssetIssue_BLL.GetDesignationName(ref ddlDesignation, ddlDepartment.SelectedValue.ToString());
        }


        protected void btnYes_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    DBMethod.DeleteEntity<HR_EMP_CONTRACT_DTL>(hR_EMP_CONTRACT_DTL);
                }
                DBMethod.DeleteEntity<HR_EMP_CONTRACT_HDR>(hR_EMP_CONTRACT_HDR);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TAE_DELETE", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        #endregion

        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            if (Session[FINSessionConstants.GridData] != null)
            {
                dtGridData = (DataTable)Session[FINSessionConstants.GridData];
            }
            gvData.EditIndex = -1;

            BindGrid(dtGridData);

        }

        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("COM_ROW_CMD", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {


            System.Collections.SortedList slControls = new System.Collections.SortedList();


            TextBox txtLineNo = gvr.FindControl("txtLineNo") as TextBox;
            TextBox txtTerms_Conditions = gvr.FindControl("txtTerms_Conditions") as TextBox;
            TextBox txtRemarks = gvr.FindControl("txtRemarks") as TextBox;
            CheckBox chkApplicable = gvr.FindControl("chkApplicable") as CheckBox;
            CheckBox chkact = gvr.FindControl("chkActive") as CheckBox;

            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList["CONTRACT_DTL_ID"] = "0";
                txtLineNo.Text = (tmpdtGridData.Rows.Count + 1).ToString();
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }

//            txtLineNo.Text = (txtLineNo.Text + rowindex + 1).ToString();

            slControls[0] = txtTerms_Conditions;


            ErrorCollection.Clear();
            string strCtrlTypes = "TextBox";

            string strMessage = " Terms and Conditions";

            EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

            if (EmptyErrorCollection.Count > 0)
            {
                ErrorCollection = EmptyErrorCollection;
                return drList;
            }


            //string strCondition = "LOT_ID='" + ddlLotNo.SelectedValue + "'";
            //strMessage = FINMessageConstatns.RecordAlreadyExists;
            //ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            //if (ErrorCollection.Count > 0)
            //{

            //    return drList;
            //}



            drList["CONTRACT_LINE_NUM"] = txtLineNo.Text;
            drList["CONTRACT_TERM_CONDTIONS"] = txtTerms_Conditions.Text;
            drList["CONTRACT_REMARKS"] = txtRemarks.Text;
            if (chkApplicable.Checked)
            {
                drList["CONTRACT_TC_APPLICABLE"] = "TRUE";
            }
            else
            {
                drList["CONTRACT_TC_APPLICABLE"] = "FALSE";
            }

            if (chkact.Checked)
            {
                drList["ENABLED_FLAG"] = "TRUE";
            }
            else
            {
                drList["ENABLED_FLAG"] = "FALSE";
            }


            drList[FINColumnConstants.DELETED] = FINAppConstants.N;


            return drList;

        }



        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("COM_ROW_UP", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("COM_ROW_DEL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("COM_ROW_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("COM_RowCreated", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("COM_RDB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        #endregion

        protected void btnDelete_Click(object sender, EventArgs e)
        {

        }

        protected void ddlcategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            filljob4Category();
            if (ddlJobs.SelectedIndex != null || ddlJobs.SelectedIndex.ToString() != "")
            {
                ddlJobs.SelectedIndex = 0;
            }
            if (ddlPosition.SelectedIndex >= 0)
            {
                ddlPosition.SelectedIndex = 0;
            }
            if (ddlGrade.SelectedIndex >= 0)
            {
                ddlGrade.SelectedIndex = 0;
            }
        }
       
       

        private void filljob4Category()
        {
           Jobs_BLL.fn_getJob4Category(ref ddlJobs, ddlcategory.SelectedValue);
        }

        protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            fn_fill_Designation();

            if (ddlDesignation.SelectedIndex != null || ddlDesignation.SelectedIndex.ToString() != "")
            {
                ddlDesignation.SelectedIndex = 0;
            }
        }

        protected void ddlJobs_SelectedIndexChanged(object sender, EventArgs e)
        {
            fn_fillPosition4Job();

            if (ddlPosition.SelectedIndex != null || ddlPosition.SelectedIndex.ToString() != "")
            {
                ddlPosition.SelectedIndex = 0;
            }
            if (ddlGrade.SelectedIndex >= 0)
            {
                ddlGrade.SelectedIndex = 0;
            }
        }
        private void fn_fillPosition4Job()
        {
            Position_BLL.fn_GetPositionNameBasedonJob(ref ddlPosition, ddlJobs.SelectedValue);
        }

        protected void ddlPosition_SelectedIndexChanged(object sender, EventArgs e)
        {
            fn_fllGrade4Position();
            if (ddlGrade.SelectedIndex != null || ddlGrade.SelectedIndex.ToString() != "")
            {
                ddlGrade.SelectedIndex = 0;
            }
            
        }
        private void fn_fllGrade4Position()
        {
            Grades_BLL.fn_getGradeName4Position(ref ddlGrade, ddlPosition.SelectedValue);
        }
        
    }
}