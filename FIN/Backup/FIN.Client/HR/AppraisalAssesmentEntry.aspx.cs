﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.BLL;
using FIN.BLL.GL;
using FIN.DAL.GL;
using VMVServices.Web;

namespace FIN.Client.HR
{
    public partial class AppraisalAssesmentEntry : PageBase
    {
        HR_PER_APP_REVIEW_HDR hR_PER_APP_REVIEW_HDR = new HR_PER_APP_REVIEW_HDR();
        HR_PER_APP_REVIEW_DTL hR_PER_APP_REVIEW_DTL = new HR_PER_APP_REVIEW_DTL();
        DataTable dtGridData = new DataTable();

        Boolean bol_rowVisiable;
        Boolean savedBool;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();
                   // divNeeded.Visible = false;

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }


        }

        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //  div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }


            UserRightsChecking();

        }
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        /// <summary>
        /// To assign the values to Controls and fetch the StrRecordId,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();
                EntityData = null;
                gvdata2.Visible = false;
                divNeeded.Visible = false;
               
                Session["EXIST"] = null;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    using (IRepository<HR_PER_APP_REVIEW_HDR> userCtx = new DataRepository<HR_PER_APP_REVIEW_HDR>())
                    {
                        hR_PER_APP_REVIEW_HDR = userCtx.Find(r =>
                            (r.REV_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = hR_PER_APP_REVIEW_HDR;
                    txtAssessmentDate.Text = DBMethod.ConvertDateToString(hR_PER_APP_REVIEW_HDR.REV_DATE.ToString());
                    // ddlreviewType.SelectedValue = hR_PER_APP_REVIEW_HDR.REV_TYPE;
                    ddlRecommendation.SelectedValue = hR_PER_APP_REVIEW_HDR.REV_RECOMMENDATION;
                    if (hR_PER_APP_REVIEW_HDR.REV_RECOMMENDATION == "Training")
                    {
                        divNeeded.Visible = true;
                    }
                    HR_PER_APP_ASSIGNMENT_HDR hR_PER_APP_ASSIGNMENT_HDR = new HR_PER_APP_ASSIGNMENT_HDR();
                    using (IRepository<HR_PER_APP_ASSIGNMENT_HDR> userCtx = new DataRepository<HR_PER_APP_ASSIGNMENT_HDR>())
                    {
                        hR_PER_APP_ASSIGNMENT_HDR = userCtx.Find(r =>
                            (r.ASSIGN_HDR_ID == hR_PER_APP_REVIEW_HDR.ASSIGN_HDR_ID.ToString())
                            ).SingleOrDefault();
                    }

                    ddlJob.SelectedValue = hR_PER_APP_ASSIGNMENT_HDR.ASSIGN_JOB_ID.ToString();

                    ddlDepartment.SelectedValue = hR_PER_APP_ASSIGNMENT_HDR.ASSIGN_DEPT_ID.ToString();

                    FillAppraisalDefineKPI();
                    ddlAppraisal.SelectedValue = hR_PER_APP_REVIEW_HDR.ASSIGN_HDR_ID;
                    FillAppraiser();
                    //HR_PER_APP_REVIEW_ASGNMNT_HDR hR_PER_APP_REVIEW_ASGNMNT_HDR = new HR_PER_APP_REVIEW_ASGNMNT_HDR();

                    //using (IRepository<HR_PER_APP_REVIEW_ASGNMNT_HDR> userCtx = new DataRepository<HR_PER_APP_REVIEW_ASGNMNT_HDR>())
                    //{
                    //    hR_PER_APP_REVIEW_ASGNMNT_HDR = userCtx.Find(r =>
                    //        (r.ASSIGN_HDR_ID == hR_PER_APP_REVIEW_HDR.ASSIGN_HDR_ID.ToString() && r.RA_REVIEWER_ID ==hR_PER_APP_REVIEW_HDR.REV_APPRAISER_ID )
                    //        ).SingleOrDefault();
                    //}
                    if (hR_PER_APP_REVIEW_HDR.ATTRIBUTE1 != null)
                    {
                        ddlAppraiser.SelectedValue = hR_PER_APP_REVIEW_HDR.ATTRIBUTE1; //hR_PER_APP_REVIEW_HDR.REV_APPRAISER_ID;
                    }
                    FillAppraisee();
                    ddlAppraisee.SelectedValue = hR_PER_APP_REVIEW_HDR.REV_APPRAISEE_ID;
                    txtOverallAssessment.Text = hR_PER_APP_REVIEW_HDR.REV_OVERALL_ASSESMENT;
                    txtComments.Text = hR_PER_APP_REVIEW_HDR.REV_COMMENTS;
                    txtTrainingNeeded.Text = hR_PER_APP_REVIEW_HDR.REV_TRNING_NEEDED;


                    LoadKPI();
                    reviewercmnts();

                }


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AppraisalAssessment", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Course master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    hR_PER_APP_REVIEW_HDR = (HR_PER_APP_REVIEW_HDR)EntityData;
                }

                hR_PER_APP_REVIEW_HDR.ASSIGN_HDR_ID = ddlAppraisal.SelectedValue;
                if (txtAssessmentDate.Text.Length > 0)
                {
                    hR_PER_APP_REVIEW_HDR.REV_DATE = DBMethod.ConvertStringToDate(txtAssessmentDate.Text);
                }
                //  hR_PER_APP_REVIEW_HDR.REV_TYPE = ddlreviewType.SelectedValue;
                //hR_PER_APP_REVIEW_HDR.REV_STATUS 
                hR_PER_APP_REVIEW_HDR.REV_OVERALL_ASSESMENT = txtOverallAssessment.Text;
                hR_PER_APP_REVIEW_HDR.REV_RECOMMENDATION = ddlRecommendation.SelectedValue;
                hR_PER_APP_REVIEW_HDR.REV_COMMENTS = txtComments.Text;
                hR_PER_APP_REVIEW_HDR.REV_ORG_ID = VMVServices.Web.Utils.OrganizationID;
                hR_PER_APP_REVIEW_HDR.REV_APPRAISER_ID = hfAppraiserEmpId.Value;
                hR_PER_APP_REVIEW_HDR.REV_APPRAISEE_ID = ddlAppraisee.SelectedValue;
                hR_PER_APP_REVIEW_HDR.ENABLED_FLAG = FINAppConstants.Y;
                hR_PER_APP_REVIEW_HDR.ATTRIBUTE1 = ddlAppraiser.SelectedValue;
                hR_PER_APP_REVIEW_HDR.REV_TRNING_NEEDED = txtTrainingNeeded.Text;

                //  hR_PER_APP_REVIEW_HDR.WORKFLOW_COMPLETION_STATUS = FINAppConstants.Y;
                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != string.Empty)
                {
                    hR_PER_APP_REVIEW_HDR.MODIFIED_BY = this.LoggedUserName;
                    hR_PER_APP_REVIEW_HDR.MODIFIED_DATE = DateTime.Today;

                }
                else
                {
                    if (Session["EXIST"] != null)
                    {
                        hR_PER_APP_REVIEW_HDR.MODIFIED_BY = this.LoggedUserName;
                        hR_PER_APP_REVIEW_HDR.MODIFIED_DATE = DateTime.Today;
                    }
                    else
                    {
                        hR_PER_APP_REVIEW_HDR.REV_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_039_M.ToString(), false, true);
                    }
                    hR_PER_APP_REVIEW_HDR.CREATED_BY = this.LoggedUserName;
                    hR_PER_APP_REVIEW_HDR.CREATED_DATE = DateTime.Today;


                }
                hR_PER_APP_REVIEW_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_PER_APP_REVIEW_HDR.REV_ID);

                //Save Detail Table


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                //ErrorCollection = CommonUtils.IsEmptyGrid(dtGridData, "AppraisalDefineKRA");
                //if (ErrorCollection.Count > 0)
                //{
                //    return;
                //}
                var tmpChildEntity = new List<Tuple<object, string>>();

                for (int iLoop = 0; iLoop < gvData.Rows.Count; iLoop++)
                {
                    hR_PER_APP_REVIEW_DTL = new HR_PER_APP_REVIEW_DTL();
                    if (gvData.DataKeys[iLoop].Values["REV_DTL_ID"].ToString() != "0")
                    {
                        using (IRepository<HR_PER_APP_REVIEW_DTL> userCtx = new DataRepository<HR_PER_APP_REVIEW_DTL>())
                        {
                            hR_PER_APP_REVIEW_DTL = userCtx.Find(r =>
                                (r.REV_DTL_ID == gvData.DataKeys[iLoop].Values["REV_DTL_ID"].ToString())
                                ).SingleOrDefault();
                        }
                    }
                    hR_PER_APP_REVIEW_DTL.REV_ID = hR_PER_APP_REVIEW_HDR.REV_ID;
                    hR_PER_APP_REVIEW_DTL.ASSIGN_DTL_ID = gvData.DataKeys[iLoop].Values["ASSIGN_DTL_ID"].ToString();
                    TextBox txt_RevSelfRating = gvData.Rows[iLoop].FindControl("txtRevSelfRating") as TextBox;
                    hR_PER_APP_REVIEW_DTL.REV_SELF_RATING = CommonUtils.ConvertStringToDecimal(txt_RevSelfRating.Text);

                    TextBox txt_RevSelfComments = gvData.Rows[iLoop].FindControl("txtRevSelfComments") as TextBox;
                    hR_PER_APP_REVIEW_DTL.REV_SELF_COMMENTS = txt_RevSelfComments.Text;

                    TextBox txt_RevSelfDate = gvData.Rows[iLoop].FindControl("txtRevSelfDate") as TextBox;
                    if (txt_RevSelfDate.Text.Length > 0)
                    {
                        hR_PER_APP_REVIEW_DTL.REV_SELF_APP_DATE = DBMethod.ConvertStringToDate(txt_RevSelfDate.Text);
                    }

                    //if (ddlreviewType.SelectedValue != "Self")
                    //{
                    TextBox txt_RevReviewerRating = gvData.Rows[iLoop].FindControl("txtRevReviewerRating") as TextBox;
                    hR_PER_APP_REVIEW_DTL.REV_REVIEWER_RATING = CommonUtils.ConvertStringToDecimal(txt_RevReviewerRating.Text);

                    TextBox txt_RevReviewerComments = gvData.Rows[iLoop].FindControl("txtRevReviewerComments") as TextBox;
                    hR_PER_APP_REVIEW_DTL.REV_REVIEWER_COMMENTS = txt_RevReviewerComments.Text;

                    TextBox txt_RevReviewerDate = gvData.Rows[iLoop].FindControl("txtRevReviewerDate") as TextBox;
                    if (txt_RevSelfDate.Text.Length > 0)
                    {
                        hR_PER_APP_REVIEW_DTL.REV_DATE = DBMethod.ConvertStringToDate(txt_RevReviewerDate.Text);
                    }
                    // }
                    hR_PER_APP_REVIEW_DTL.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                    hR_PER_APP_REVIEW_DTL.WORKFLOW_COMPLETION_STATUS = FINAppConstants.Y;

                    if (gvData.DataKeys[iLoop].Values["REV_DTL_ID"].ToString() != "0")
                    {

                        hR_PER_APP_REVIEW_DTL.MODIFIED_BY = this.LoggedUserName;
                        hR_PER_APP_REVIEW_DTL.MODIFIED_DATE = DateTime.Today;

                        tmpChildEntity.Add(new Tuple<object, string>(hR_PER_APP_REVIEW_DTL, "U"));

                    }
                    else
                    {

                        hR_PER_APP_REVIEW_DTL.REV_DTL_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_039_D.ToString(), false, true);
                        hR_PER_APP_REVIEW_DTL.CREATED_BY = this.LoggedUserName;
                        hR_PER_APP_REVIEW_DTL.CREATED_DATE = DateTime.Today;

                        tmpChildEntity.Add(new Tuple<object, string>(hR_PER_APP_REVIEW_DTL, "A"));
                    }
                }

                if (Session["EXIST"] != null)
                {
                    CommonUtils.SavePCEntity<HR_PER_APP_REVIEW_HDR, HR_PER_APP_REVIEW_DTL>(hR_PER_APP_REVIEW_HDR, tmpChildEntity, hR_PER_APP_REVIEW_DTL, true);
                    savedBool = true;

                }
                else
                {
                    switch (Master.Mode)
                    {
                        case FINAppConstants.Add:
                            {
                                CommonUtils.SavePCEntity<HR_PER_APP_REVIEW_HDR, HR_PER_APP_REVIEW_DTL>(hR_PER_APP_REVIEW_HDR, tmpChildEntity, hR_PER_APP_REVIEW_DTL);
                                savedBool = true;
                                break;
                            }
                        case FINAppConstants.Update:
                            {
                                CommonUtils.SavePCEntity<HR_PER_APP_REVIEW_HDR, HR_PER_APP_REVIEW_DTL>(hR_PER_APP_REVIEW_HDR, tmpChildEntity, hR_PER_APP_REVIEW_DTL, true);
                                savedBool = true;
                                break;
                            }
                    }
                }
                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                if (hR_PER_APP_REVIEW_DTL.WORKFLOW_COMPLETION_STATUS == "1")
                {
                    FIN.BLL.SSM.Alert_BLL.GenerateEmail(FINMessageConstatns.Appraisal_Assesment_Appraiser_Alert);
                    FIN.BLL.SSM.Alert_BLL.GenerateEmail(FINMessageConstatns.Appraisal_Assesment_Appraisee_Alert);
                    if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0" && savedBool == true)
                    {
                        if (VMVServices.Web.Utils.IsAlert == "1")
                        {
                            FINSQL.UpdateAlertUserLevel();
                        }
                    }
                }
                if (ErrorCollection.Count > 0)
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AA_ATB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }


        private void FillComboBox()
        {

            FIN.BLL.HR.Department_BLL.GetDepartmentName(ref ddlDepartment);
            FIN.BLL.HR.Jobs_BLL.fn_getJobName(ref ddlJob);

            FIN.BLL.Lookup_BLL.GetLookUpValues(ref ddlRecommendation, "RECOM");
            // FIN.BLL.Lookup_BLL.GetLookUpValues(ref ddlreviewType, "RTYPE");

            //AppraisalDefineKRAEntry_BLL.fn_GetJobDetails(ref ddlJob);
            //AppraisalDefinition_BLL.fn_GetAppraisalPeriodDetails(ref ddlAppraisalCode);

        }



        /// <summary>
        /// Bind the records into grid voew
        /// </summary>
        /// <param name="dtData">Contains the database entities and correspoding records which is used in the grid view</param>

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();

                gvData.DataSource = dtData;
                gvData.DataBind();

                gvData.Columns[7].Visible = true;
                gvData.Columns[8].Visible = true;
                gvData.Columns[9].Visible = true;
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AA_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void BindGrid2(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();

                gvdata2.DataSource = dtData;
                gvdata2.DataBind();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AA_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>


        #endregion



        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                AssignToBE();

                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                }


            }


            catch (Exception ex)
            {
                ErrorCollection.Add("AA_btn_sve", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void ddlJob_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillAppraisalDefineKPI();
        }

        private void FillAppraisalDefineKPI()
        {
            //FIN.BLL.HR.AppraisalDefineKRAEntry_BLL.fn_GetAppraisalDefineKPIDetails(ref ddlAppraisal, ddlDepartment.SelectedValue, ddlJob.SelectedValue);
            FIN.BLL.HR.AppraisalDefineKRAEntry_BLL.fn_GetAppraisalDtlsBasedInitiateAppr(ref ddlAppraisal, ddlDepartment.SelectedValue, ddlJob.SelectedValue);
        }

        protected void ddlAppraisal_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillAppraiser();
            LoadKPI();
        }

        private void FillAppraiser()
        {
            FIN.BLL.HR.AppraisalAppraiser_BLL.fn_GetAppraiser4Appraisal(ref ddlAppraiser, ddlAppraisal.SelectedValue);
        }

        protected void ddlAppraiser_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillAppraisee();
        }
        private void FillAppraisee()
        {
            FIN.BLL.HR.AppraisalAppraiser_BLL.fn_GetAppraisee4Appraiser(ref ddlAppraisee, ddlAppraiser.SelectedValue);

            HR_PER_APP_REVIEW_ASGNMNT_HDR hR_PER_APP_REVIEW_ASGNMNT_HDR = new HR_PER_APP_REVIEW_ASGNMNT_HDR();
            using (IRepository<HR_PER_APP_REVIEW_ASGNMNT_HDR> userCtx = new DataRepository<HR_PER_APP_REVIEW_ASGNMNT_HDR>())
            {
                hR_PER_APP_REVIEW_ASGNMNT_HDR = userCtx.Find(r =>
                    (r.RA_HDR_ID == ddlAppraiser.SelectedValue)
                    ).SingleOrDefault();
            }
            hfAppraiserEmpId.Value = "0";
            if (hR_PER_APP_REVIEW_ASGNMNT_HDR != null)
            {
                hfAppraiserEmpId.Value = hR_PER_APP_REVIEW_ASGNMNT_HDR.RA_REVIEWER_ID;
            }

        }
        private void LoadKPI()
        {
            DataTable dt_data = FIN.BLL.HR.AppraisalAssesment_BLL.fn_GetKPI4Apprsial(Master.StrRecordId, ddlAppraisal.SelectedValue);
            BindGrid(dt_data);
            // ChangeReviewType();
        }

        protected void ddlreviewType_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChangeReviewType();
        }
        private void ChangeReviewType()
        {
            //if (ddlreviewType.SelectedValue.ToString().Length > 0)
            //{
            //    if (ddlreviewType.SelectedValue != "Self")
            //    {
            //        gvData.Columns[7].Visible = true;
            //        gvData.Columns[8].Visible = true;
            //        gvData.Columns[9].Visible = true;
            //    }
            //    else
            //    {
            //        gvData.Columns[7].Visible = false ;
            //        gvData.Columns[8].Visible = false;
            //        gvData.Columns[9].Visible = false;
            //    }
            //}
        }

        protected void ddlAppraisee_SelectedIndexChanged(object sender, EventArgs e)
        {

            dtGridData = DBMethod.ExecuteQuery(FIN.DAL.HR.AppraisalAssesment_DAL.GetAppraisalAssmntDtl2(ddlAppraisal.SelectedValue, hfAppraiserEmpId.Value, ddlAppraisee.SelectedValue)).Tables[0];
            if (dtGridData.Rows.Count > 0)
            {
                BindGrid(dtGridData);
                gvData.Visible = true;
                Session["EXIST"] = dtGridData;
                if (dtGridData.Rows[0]["REV_ID"].ToString() != string.Empty)
                {
                    using (IRepository<HR_PER_APP_REVIEW_HDR> userCtx = new DataRepository<HR_PER_APP_REVIEW_HDR>())
                    {
                        hR_PER_APP_REVIEW_HDR = userCtx.Find(r =>
                            (r.REV_ID == dtGridData.Rows[0]["REV_ID"].ToString())
                            ).SingleOrDefault();
                    }

                    if (hR_PER_APP_REVIEW_HDR != null)
                    {
                        EntityData = hR_PER_APP_REVIEW_HDR;
                    }
                }
            }
            reviewercmnts();
        }

        private void reviewercmnts()
        {
            DataTable dt_gvdata2 = DBMethod.ExecuteQuery(FIN.DAL.HR.AppraisalAssesment_DAL.GetReviewerCmts(hfAppraiserEmpId.Value, ddlAppraisee.SelectedValue, ddlAppraisal.SelectedValue, ddlDepartment.SelectedValue)).Tables[0];
            if (dt_gvdata2.Rows.Count > 0)
            {
                gvdata2.Visible = true;
                BindGrid2(dt_gvdata2);
            }


            // ChangeReviewType();
        }

        protected void ddlRecommendation_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlRecommendation.SelectedValue == "Training")
            {
                divNeeded.Visible = true;
            }
            else
            {
                divNeeded.Visible = false;
            }

        }


    }
}
