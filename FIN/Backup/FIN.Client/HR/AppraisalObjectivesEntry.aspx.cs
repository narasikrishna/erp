﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.BLL;
using FIN.BLL.HR;
using VMVServices.Web;

namespace FIN.Client.HR
{
    public partial class AppraisalObjectivesEntry : PageBase
    {

        HR_PER_OBJECTIVE_SET hR_PER_OBJECTIVE_SET = new HR_PER_OBJECTIVE_SET();
        DataTable dtGridData = new DataTable();
        AppraisalObjectives_BLL AppraisalObjectives_BLL = new AppraisalObjectives_BLL();
        string ProReturn = null;

        Boolean bol_rowVisiable;
        Boolean savedBool;
        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AOBJ_PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));


            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
        }

        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>
        /// 

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>



        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();



                EntityData = null;

                dtGridData = FIN.BLL.HR.AppraisalObjectives_BLL.getChildEntityDet(Master.StrRecordId);
                BindGrid(dtGridData);

                //if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                //{

                //    using (IRepository<HR_PER_OBJECTIVE_SET> userCtx = new DataRepository<HR_PER_OBJECTIVE_SET>())
                //    {
                //        hR_PER_OBJECTIVE_SET = userCtx.Find(r =>
                //            (r.CATEGORY_ID == Master.StrRecordId)
                //            ).SingleOrDefault();
                //    }

                //    EntityData = hR_PER_OBJECTIVE_SET;

                //}

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AOBJ_ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                var tmpChildEntity = new List<Tuple<object, string>>();

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    hR_PER_OBJECTIVE_SET = new HR_PER_OBJECTIVE_SET();
                    if (dtGridData.Rows[iLoop]["PER_OBJ_ID"].ToString() != "0")
                    {
                        using (IRepository<HR_PER_OBJECTIVE_SET> userCtx = new DataRepository<HR_PER_OBJECTIVE_SET>())
                        {
                            hR_PER_OBJECTIVE_SET = userCtx.Find(r =>
                                (r.PER_OBJ_ID == dtGridData.Rows[iLoop]["PER_OBJ_ID"].ToString())
                                ).SingleOrDefault();
                        }
                    }
                    hR_PER_OBJECTIVE_SET.PER_CODE = dtGridData.Rows[iLoop]["PER_CODE"].ToString();
                    hR_PER_OBJECTIVE_SET.PER_DESC = dtGridData.Rows[iLoop]["PER_DESC"].ToString();
                    hR_PER_OBJECTIVE_SET.PER_CODE_OL = dtGridData.Rows[iLoop]["PER_CODE_OL"].ToString();
                    hR_PER_OBJECTIVE_SET.PER_DESC_OL = dtGridData.Rows[iLoop]["PER_DESC_OL"].ToString();

                    hR_PER_OBJECTIVE_SET.PER_TYPE = dtGridData.Rows[iLoop]["PER_TYPE"].ToString();
                    hR_PER_OBJECTIVE_SET.PER_CATEGORY = dtGridData.Rows[iLoop]["PER_CATEGORY"].ToString();
                    hR_PER_OBJECTIVE_SET.PER_MIN_RATING = decimal.Parse(dtGridData.Rows[iLoop]["PER_MIN_RATING"].ToString());
                    hR_PER_OBJECTIVE_SET.PER_MAX_RATING = decimal.Parse(dtGridData.Rows[iLoop]["PER_MAX_RATING"].ToString());

                    //if (dtGridData.Rows[iLoop]["EFFECTIVE_FROM_DATE"] != DBNull.Value)
                    //{
                    //    hR_PER_OBJECTIVE_SET.EFFECTIVE_FROM_DATE = DateTime.Parse(dtGridData.Rows[iLoop]["EFFECTIVE_FROM_DATE"].ToString());
                    //}

                    //if (dtGridData.Rows[iLoop]["EFFECTIVE_TO_DATE"] != DBNull.Value)
                    //{
                    //    hR_PER_OBJECTIVE_SET.EFFECTIVE_TO_DATE = DateTime.Parse(dtGridData.Rows[iLoop]["EFFECTIVE_TO_DATE"].ToString());
                    //}


                    //if (dtGridData.Rows[iLoop][FINColumnConstants.ENABLED_FLAG].ToString().ToUpper() == "TRUE")
                    //{
                    hR_PER_OBJECTIVE_SET.ENABLED_FLAG = FINAppConstants.Y;
                    //}
                    //else
                    //{
                    //    hR_PER_OBJECTIVE_SET.ENABLED_FLAG = FINAppConstants.N;
                    //}

                    hR_PER_OBJECTIVE_SET.PER_ORG_ID = VMVServices.Web.Utils.OrganizationID;



                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {

                        tmpChildEntity.Add(new Tuple<object, string>(hR_PER_OBJECTIVE_SET, "D"));
                    }
                    else
                    {


                        // Duplicate Validation Through Backend Package PKG_VALIDATIONS

                        ProReturn = FIN.DAL.HR.AppraisalObjectives_DAL.GetSPFOR_DUPLICATE_CHECK(hR_PER_OBJECTIVE_SET.PER_CODE, hR_PER_OBJECTIVE_SET.PER_CATEGORY, hR_PER_OBJECTIVE_SET.PER_TYPE, hR_PER_OBJECTIVE_SET.PER_OBJ_ID);

                        if (ProReturn != string.Empty)
                        {
                            if (ProReturn != "0")
                            {
                                ErrorCollection.Add("APPOBJ", ProReturn);
                                if (ErrorCollection.Count > 0)
                                {
                                    return;
                                }
                            }
                        }


                        if (dtGridData.Rows[iLoop]["PER_OBJ_ID"].ToString() != "0")
                        {
                            hR_PER_OBJECTIVE_SET.PER_OBJ_ID = dtGridData.Rows[iLoop]["PER_OBJ_ID"].ToString();
                            hR_PER_OBJECTIVE_SET.MODIFIED_BY = this.LoggedUserName;
                            hR_PER_OBJECTIVE_SET.MODIFIED_DATE = DateTime.Today;
                            hR_PER_OBJECTIVE_SET.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_PER_OBJECTIVE_SET.PER_OBJ_ID);

                            DBMethod.SaveEntity<HR_PER_OBJECTIVE_SET>(hR_PER_OBJECTIVE_SET, true);
                            savedBool = true;
                            //tmpChildEntity.Add(new Tuple<object, string>(hR_PER_OBJECTIVE_SET, "U"));

                        }
                        else
                        {

                            hR_PER_OBJECTIVE_SET.PER_OBJ_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_036.ToString(), false, true);
                            hR_PER_OBJECTIVE_SET.CREATED_BY = this.LoggedUserName;
                            hR_PER_OBJECTIVE_SET.CREATED_DATE = DateTime.Today;
                            hR_PER_OBJECTIVE_SET.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_PER_OBJECTIVE_SET.PER_OBJ_ID);

                            DBMethod.SaveEntity<HR_PER_OBJECTIVE_SET>(hR_PER_OBJECTIVE_SET);
                            savedBool = true;
                            // tmpChildEntity.Add(new Tuple<object, string>(hR_PER_OBJECTIVE_SET, "A"));
                        }
                        if (ErrorCollection.Count > 0)
                        {
                            return;
                        }


                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AOBJ_ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddlCategory = tmpgvr.FindControl("ddlCategory") as DropDownList;
                Lookup_BLL.GetLookUpValues(ref ddlCategory, "CAT");

                if (gvData.EditIndex >= 0)
                {
                    ddlCategory.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.PER_CATEGORY].ToString();

                }


                DropDownList ddlType = tmpgvr.FindControl("ddlType") as DropDownList;
                Lookup_BLL.GetLookUpValues(ref ddlType, "AAT");

                if (gvData.EditIndex >= 0)
                {
                    ddlType.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.PER_TYPE].ToString();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AOBJ_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session[FINSessionConstants.GridData], "Appraisal Objective ");

                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                AssignToBE();
                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        #endregion

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";

                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AOBJ_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            try
            {
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = -1;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AOBJ_ROW_CNCL_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion

        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AOBJ_ROW_CMD", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {


            System.Collections.SortedList slControls = new System.Collections.SortedList();
            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));
            TextBox txtcode = gvr.FindControl("txtcode") as TextBox;
            TextBox txtdesc = gvr.FindControl("txtdesc") as TextBox;
            TextBox txtcodeAR = gvr.FindControl("txtcodeAR") as TextBox;
            TextBox txtdescAR = gvr.FindControl("txtdescAR") as TextBox;

            DropDownList ddlcategory = gvr.FindControl("ddlcategory") as DropDownList;
            DropDownList ddlType = gvr.FindControl("ddlType") as DropDownList;
            TextBox txtMin = gvr.FindControl("txtMin") as TextBox;
            TextBox txtmax = gvr.FindControl("txtmax") as TextBox;



            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList["PER_OBJ_ID"] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }

            slControls[0] = txtcode;
            slControls[1] = txtdesc;
            slControls[2] = ddlcategory;
            slControls[3] = ddlType;
            slControls[4] = txtMin;
            slControls[5] = txtmax;



            ErrorCollection.Clear();
            string strCtrlTypes = "TextBox~TextBox~DropDownList~DropDownList~TextBox~TextBox";
            string strMessage = Prop_File_Data["Code_P"] + " ~ " + Prop_File_Data["Description_P"] + " ~ " + Prop_File_Data["Category_P"] + " ~ " + Prop_File_Data["Type_P"] + " ~ " + Prop_File_Data["Minimum_Rating_P"] + " ~ " + Prop_File_Data["Maximum_Rating_P"] + "";
            //string strMessage = "Code ~ Description ~ Category ~ Type ~ Minimum Rating ~ Maximum Rating";
            ErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);
            if (ErrorCollection.Count > 0)
                return drList;


            string strCondition = "PER_CODE='" + txtcode.Text.Trim().ToUpper() + "'";
            strMessage = FINMessageConstatns.Code;
            ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            if (ErrorCollection.Count > 0)
            {

                return drList;
            }

            if ((decimal.Parse(txtmax.Text.ToString()) < decimal.Parse(txtMin.Text.ToString())) || (decimal.Parse(txtmax.Text.ToString()) == decimal.Parse(txtMin.Text.ToString())))
            {
                ErrorCollection.Add("raterange", "Maximum Rating must be greater than Minimum Rating");
            }


            drList["PER_CODE"] = txtcode.Text;
            drList["PER_DESC"] = txtdesc.Text;
            drList["PER_CODE_OL"] = txtcodeAR.Text;
            drList["PER_DESC_OL"] = txtdescAR.Text;

            drList["PER_CATEGORY"] = ddlcategory.SelectedValue.ToString();
            drList["PER_TYPE"] = ddlType.SelectedValue.ToString();
            drList["PER_MIN_RATING"] = txtMin.Text;
            drList["PER_MAX_RATING"] = txtmax.Text;


            //if (dtpStartDate.Text.ToString().Length > 0)
            //{
            //    drList["EFFECTIVE_FROM_DATE"] = DBMethod.ConvertStringToDate(dtpStartDate.Text.ToString());
            //}

            //if (dtpEndDate.Text.ToString().Length > 0)
            //{

            //    drList["EFFECTIVE_TO_DATE"] = DBMethod.ConvertStringToDate(dtpEndDate.Text.ToString());
            //}
            //else
            //{
            //    drList["EFFECTIVE_TO_DATE"] = DBNull.Value;
            //}
            //if (chkact.Checked)
            //{
            //    drList[FINColumnConstants.ENABLED_FLAG] = "TRUE";
            //}
            //else
            //{
            //    drList[FINColumnConstants.ENABLED_FLAG] = "FALSE";
            //}



            drList[FINColumnConstants.DELETED] = FINAppConstants.N;


            return drList;



        }



        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AOBJ_ROW_UP", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AOBJ_ROW_DEL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AOBJ_ROW_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AOBJ_RowCreated", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }

                    if (e.Row.RowType == DataControlRowType.DataRow)
                    {
                        e.Row.Cells[2].HorizontalAlign = HorizontalAlign.Right;
                        e.Row.Cells[3].HorizontalAlign = HorizontalAlign.Right;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AOBJ_RDB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {

                    hR_PER_OBJECTIVE_SET.PK_ID = Convert.ToInt32(dtGridData.Rows[iLoop]["PK_ID"].ToString());
                    DBMethod.DeleteEntity<HR_PER_OBJECTIVE_SET>(hR_PER_OBJECTIVE_SET);
                }


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AOBJ_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {

        }




    }
}