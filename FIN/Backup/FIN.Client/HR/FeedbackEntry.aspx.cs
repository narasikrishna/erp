﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.DAL.GL;
using FIN.BLL;
using FIN.BLL.GL;
using FIN.BLL.HR;
using VMVServices.Web;
using System.Collections;
using System.Data;

namespace FIN.Client.HR
{
    public partial class FeedbackEntry : PageBase
    {
        HR_FEEDBACK_HDR hR_FEEDBACK_HDR = new HR_FEEDBACK_HDR();
        HR_FEEDBACK_DTL hR_FEEDBACK_DTL = new HR_FEEDBACK_DTL();
        DataTable dtGridData = new DataTable();
        Boolean bol_rowVisiable;
        Boolean savedBool = false;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    ErrorCollection.Clear();

                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("FeedbackEntryPgLoad", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//  ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + iAcademeMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FIN.BLL.FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FIN.BLL.FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();

        }

        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }
        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>
        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();

                EntityData = null;


                dtGridData = FIN.BLL.HR.FeedbackTemplateBLL.getChildEntityDet(Master.StrRecordId);
                BindGrid(dtGridData);


                if (Master.Mode != FIN.BLL.FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    txtFBTemplateName.Enabled = false;
                    using (IRepository<HR_FEEDBACK_HDR> userCtx = new DataRepository<HR_FEEDBACK_HDR>())
                    {
                        hR_FEEDBACK_HDR = userCtx.Find(r =>
                            (r.FEEDBACK_ID == Master.StrRecordId.ToString())
                            ).SingleOrDefault();
                    }

                    EntityData = hR_FEEDBACK_HDR;

                    txtFBTemplateName.Text = hR_FEEDBACK_HDR.FEEDBACK_TEMP_NAME.ToString();
                    txtComments.Text = hR_FEEDBACK_HDR.FEEDBACK_TEMPLATE_COMMENTS.ToString();

                    txtFBTMPLName_ol.Text = hR_FEEDBACK_HDR.FEEDBACK_TEMP_NAME_OL;
                    txtComments_OL.Text = hR_FEEDBACK_HDR.FEEDBACK_TEMPLATE_COMMENTS_OL;
                    if (hR_FEEDBACK_HDR.EFFECTIVE_FROM_DT != null)
                    {
                        txtFromDate.Text = DBMethod.ConvertDateToString(hR_FEEDBACK_HDR.EFFECTIVE_FROM_DT.ToString());
                    }

                    if (hR_FEEDBACK_HDR.EFFECTIVE_TO_DT != null)
                    {
                        txtToDate.Text = DBMethod.ConvertDateToString(hR_FEEDBACK_HDR.EFFECTIVE_TO_DT.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("FeedbackEntryATC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//  ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + iAcademeMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the exam master table entities
        /// Fetch the CalendarID and OrganizationID from common properties which was stored while the user has been logined.
        /// </summary>

        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();

                if (EntityData != null)
                {
                    hR_FEEDBACK_HDR = (HR_FEEDBACK_HDR)EntityData;
                }

                hR_FEEDBACK_HDR.FEEDBACK_TEMP_NAME = txtFBTemplateName.Text.ToString();
                hR_FEEDBACK_HDR.FEEDBACK_TEMPLATE_COMMENTS = txtComments.Text.ToString();
                hR_FEEDBACK_HDR.FEEDBACK_TEMP_NAME_OL = txtFBTMPLName_ol.Text.ToString();
                hR_FEEDBACK_HDR.FEEDBACK_TEMPLATE_COMMENTS_OL = txtComments_OL.Text.ToString();
                if (txtFromDate.Text != string.Empty)
                {
                    hR_FEEDBACK_HDR.EFFECTIVE_FROM_DT = DBMethod.ConvertStringToDate(txtFromDate.Text.ToString());
                }
                if (txtToDate.Text != string.Empty)
                {
                    hR_FEEDBACK_HDR.EFFECTIVE_TO_DT = DBMethod.ConvertStringToDate(txtToDate.Text.ToString());
                }
                else
                {
                    hR_FEEDBACK_HDR.EFFECTIVE_TO_DT = null;
                }

                hR_FEEDBACK_HDR.ENABLED_FLAG = FINAppConstants.Y;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != string.Empty)
                {
                    hR_FEEDBACK_HDR.MODIFIED_BY = this.LoggedUserName;
                    hR_FEEDBACK_HDR.MODIFIED_DATE = DateTime.Today;
                }
                else
                {
                    hR_FEEDBACK_HDR.CREATED_BY = this.LoggedUserName;
                    hR_FEEDBACK_HDR.CREATED_DATE = DateTime.Today;
                    hR_FEEDBACK_HDR.FEEDBACK_ID = FINSP.GetSPFOR_SEQCode("HR_R_082_M".ToString(), false, true);
                }

                hR_FEEDBACK_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_FEEDBACK_HDR.FEEDBACK_ID);

                //Save Detail Table
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                var tmpChildEntity = new List<Tuple<object, string>>();

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    hR_FEEDBACK_DTL = new HR_FEEDBACK_DTL();
                    if (dtGridData.Rows[iLoop]["FEEDBACK_DTL_ID"].ToString() != "0" )
                    {
                        using (IRepository<HR_FEEDBACK_DTL> userCtx = new DataRepository<HR_FEEDBACK_DTL>())
                        {
                            hR_FEEDBACK_DTL = userCtx.Find(r =>
                                (r.FEEDBACK_DTL_ID == dtGridData.Rows[iLoop]["FEEDBACK_DTL_ID"].ToString())
                                ).SingleOrDefault();
                        }
                    }

                    if (dtGridData.Rows[iLoop]["FEEDBACK_DTL_ID"].ToString() != "0")
                    {
                        FIN.BLL.HR.FeedbackTemplateBLL.getClassEntity(dtGridData.Rows[iLoop]["FEEDBACK_DTL_ID"].ToString());
                    }

                    hR_FEEDBACK_DTL.FEEDBACK_DESC = dtGridData.Rows[iLoop]["FEEDBACK_DESC"].ToString();
                    hR_FEEDBACK_DTL.FEEDBACK_DESC_OL = dtGridData.Rows[iLoop]["FEEDBACK_DESC_OL"].ToString();
                    hR_FEEDBACK_DTL.FEEDBACK_TYPE = dtGridData.Rows[iLoop]["code_name"].ToString();
                    hR_FEEDBACK_DTL.FEEDBACK_RATING_TYPE = dtGridData.Rows[iLoop]["dtl_code_name"].ToString();
                    hR_FEEDBACK_DTL.FEEDBACK_ID = hR_FEEDBACK_HDR.FEEDBACK_ID;
                    hR_FEEDBACK_DTL.FEEDBACK_TYPE_ID = dtGridData.Rows[iLoop]["code_id"].ToString();
                    if (dtGridData.Rows[iLoop]["code_id"].ToString().Length <= 0)
                    {
                        hR_FEEDBACK_DTL.FEEDBACK_TYPE = "";
                    }
                    if (dtGridData.Rows[iLoop]["dtlFromDate"] != DBNull.Value)
                    {
                        hR_FEEDBACK_DTL.EFFECTIVE_FROM_DT = DateTime.Parse(dtGridData.Rows[iLoop]["dtlFromDate"].ToString());
                    }
                    if (dtGridData.Rows[iLoop]["dtlToDate"] != DBNull.Value)
                    {
                        hR_FEEDBACK_DTL.EFFECTIVE_TO_DT = DateTime.Parse(dtGridData.Rows[iLoop]["dtlToDate"].ToString());
                    }
                    else
                    {
                        hR_FEEDBACK_DTL.EFFECTIVE_TO_DT = null;
                    }

                    hR_FEEDBACK_DTL.ENABLED_FLAG = dtGridData.Rows[iLoop]["ENABLED_FLAG"].ToString() == FIN.BLL.FINAppConstants.TRUEFLAG ? FIN.BLL.FINAppConstants.EnabledFlag : FIN.BLL.FINAppConstants.DisabledFlag;
                    hR_FEEDBACK_DTL.WORKFLOW_COMPLETION_STATUS = FINAppConstants.Y;

                    if (dtGridData.Rows[iLoop]["DELETED"].ToString() == FIN.BLL.FINAppConstants.EnabledFlag)
                    {
                        hR_FEEDBACK_DTL.FEEDBACK_DTL_ID = dtGridData.Rows[iLoop]["FEEDBACK_DTL_ID"].ToString();
                        tmpChildEntity.Add(new Tuple<object, string>(hR_FEEDBACK_DTL, FINAppConstants.Delete));
                    }
                    else
                    {
                        if (dtGridData.Rows[iLoop]["FEEDBACK_DTL_ID"].ToString() != "0")
                        {
                            hR_FEEDBACK_DTL.FEEDBACK_DTL_ID = dtGridData.Rows[iLoop]["FEEDBACK_DTL_ID"].ToString();
                            hR_FEEDBACK_DTL.MODIFIED_BY = this.LoggedUserName;
                            hR_FEEDBACK_DTL.MODIFIED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(hR_FEEDBACK_DTL, FINAppConstants.Update));
                        }
                        else
                        {
                            hR_FEEDBACK_DTL.FEEDBACK_DTL_ID = FINSP.GetSPFOR_SEQCode("HR_R_082_D".ToString(), false, true);
                            hR_FEEDBACK_DTL.CREATED_BY = this.LoggedUserName;
                            hR_FEEDBACK_DTL.CREATED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(hR_FEEDBACK_DTL, FINAppConstants.Add));
                        }
                    }
                }
                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            CommonUtils.SavePCEntity<HR_FEEDBACK_HDR, HR_FEEDBACK_DTL>(hR_FEEDBACK_HDR, tmpChildEntity, hR_FEEDBACK_DTL);
                            savedBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {
                            CommonUtils.SavePCEntity<HR_FEEDBACK_HDR, HR_FEEDBACK_DTL>(hR_FEEDBACK_HDR, tmpChildEntity, hR_FEEDBACK_DTL, true);
                            savedBool = true;
                            break;
                        }
                }
                if (ErrorCollection.Count > 0)
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("FeedbackEntryATOBE", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }


        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session[FINSessionConstants.GridData], "FEEDBACK DETAILS ");

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                AssignToBE();

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                }

            }
            catch (Exception ex)
            {
                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//  ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + iAcademeMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion

        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            if (Session[FINSessionConstants.GridData] != null)
            {
                dtGridData = (DataTable)Session[FINSessionConstants.GridData];
            }
            gvData.EditIndex = -1;
            BindGrid(dtGridData);

        }
        #endregion

        /// <summary>
        /// Bind the records into grid voew
        /// </summary>
        /// <param name="dtData">Contains the database entities and correspoding records which is used in the grid view</param>
        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr["ENABLED_FLAG"] = "FALSE";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();

                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("FeedbackEntryBG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//  ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + iAcademeMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            DropDownList ddlFeedbacktyp = tmpgvr.FindControl("ddlFeedbacktyp") as DropDownList;
            DropDownList ddlFeedbackRatingType = tmpgvr.FindControl("ddlFeedbackRatingType") as DropDownList;

            FIN.BLL.HR.FeedbackTemplateBLL.fn_GetFeedbackTemplateTypes(ref ddlFeedbacktyp);
            Lookup_BLL.GetLookUpValues(ref ddlFeedbackRatingType, "FEEDBACK_RATING_TYP");

            if (gvData.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
            {
                if (gvData.DataKeys[gvData.EditIndex].Values["code_id"] != null)
                {
                    ddlFeedbacktyp.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["code_id"].ToString();
                }
                if (gvData.DataKeys[gvData.EditIndex].Values["dtl_code_id"] != null)
                {
                    ddlFeedbackRatingType.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["dtl_code_id"].ToString();
                }
            }
        }
        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FIN.BLL.FINAppConstants.Y;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("FeedbackEntryGVD", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//  ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + iAcademeMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("FeedbackEntryGVE", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//  ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + iAcademeMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("FeedbackEntryGVC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//  ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + iAcademeMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;
                    if (dtGridData.Rows.Count > 0)
                    {
                        if (dtGridData.Rows[e.Row.RowIndex][FINColumnConstants.DELETED].ToString() == FIN.BLL.FINAppConstants.Y)
                        {
                            e.Row.Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("FeedbackEntryGVB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//  ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + iAcademeMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }

                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//  ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + iAcademeMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("FeedbackEntryGVR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//  ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + iAcademeMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {
            System.Collections.SortedList slControls = new System.Collections.SortedList();
            DataRow drList;

            DropDownList ddlFeedbacktyp = gvr.FindControl("ddlFeedbacktyp") as DropDownList;
            DropDownList ddlFeedbackRatingType = gvr.FindControl("ddlFeedbackRatingType") as DropDownList;
            TextBox dtpStartDate = gvr.FindControl("dtpStartDate") as TextBox;
            TextBox dtpenddate = gvr.FindControl("dtpenddate") as TextBox;

            TextBox txtfeedbackdesc = gvr.FindControl("txtfeedbackdesc") as TextBox;
            TextBox txtfeedbackdesc_ol = gvr.FindControl("txtfeedbackdesc_ol") as TextBox;
            CheckBox chkact = gvr.FindControl("chkact") as CheckBox;


            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList["FEEDBACK_DTL_ID"] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);
            }

            //slControls[0] = ddlFeedbacktyp;
            slControls[0] = ddlFeedbackRatingType;
            slControls[1] = txtfeedbackdesc;
            slControls[2] = dtpStartDate;
            slControls[3] = dtpStartDate;
            slControls[4] = dtpenddate;


            ErrorCollection.Clear();
            string strCtrlTypes = "DropDownList~TextBox~TextBox~TextBox~DateRangeValidate";
            string strMessage = "Rating Type ~Desc ~Start Date~ Start Date~ End date";
            ErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);
            if (ErrorCollection.Count > 0)
                return drList;

            string strCondition = "code_id='" + ddlFeedbacktyp.SelectedValue + "' AND FEEDBACK_DESC='" + txtfeedbackdesc.Text + "'";
            strMessage = FINMessageConstatns.RecordAlreadyExists;
            ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            if (ErrorCollection.Count > 0)
            {
                return drList;
            }

            if (ddlFeedbacktyp.SelectedItem != null)
            {
                if (ddlFeedbacktyp.SelectedValue.ToString().Length > 0)
                {
                    drList["code_id"] = ddlFeedbacktyp.SelectedItem.Value;
                    drList["code_name"] = ddlFeedbacktyp.SelectedItem.Text;
                }
            }
            if (ddlFeedbackRatingType.SelectedItem != null)
            {
                drList["dtl_code_id"] = ddlFeedbackRatingType.SelectedItem.Value;
                drList["dtl_code_name"] = ddlFeedbackRatingType.SelectedItem.Text;
            }
            drList["FEEDBACK_DESC"] = txtfeedbackdesc.Text;
            drList["FEEDBACK_DESC_ol"] = txtfeedbackdesc_ol.Text;

            drList["dtlFromDate"] = dtpStartDate.Text;
            if (dtpenddate.Text.ToString().Trim().Length > 0)
            {
                drList["dtlToDate"] = dtpenddate.Text;
            }
            else
            {
                drList["dtlToDate"] = DBNull.Value;
            }

            if (chkact.Checked)
            {
                drList["enabled_flag"] = "TRUE";
            }
            else
            {
                drList["enabled_flag"] = "FALSE";
            }
            drList[FINColumnConstants.DELETED] = FIN.BLL.FINAppConstants.N;
            return drList;
        }


        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//  ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }

                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("FeedbackEntry_ROW_UP", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// Used to delete the exam master and detail table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {

            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                for (int jLoop = 0; jLoop < dtGridData.Rows.Count; jLoop++)
                {
                    DBMethod.DeleteEntity<HR_FEEDBACK_DTL>(hR_FEEDBACK_DTL);
                }

                DBMethod.DeleteEntity<HR_FEEDBACK_HDR>(hR_FEEDBACK_HDR);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("FeedbackEntryYES", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//  ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + iAcademeMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void ddlFeedbacktyp_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow gvr = gvData.FooterRow;
            DropDownList ddlFeedbacktyp = gvr.FindControl("ddlFeedbacktyp") as DropDownList;
            DropDownList ddlFeedbackRatingType = gvr.FindControl("ddlFeedbackRatingType") as DropDownList;
            TextBox dtpStartDate = gvr.FindControl("dtpStartDate") as TextBox;
            TextBox dtpenddate = gvr.FindControl("dtpenddate") as TextBox;
            TextBox txtfeedbackdesc = gvr.FindControl("txtfeedbackdesc") as TextBox;

            DataTable dtData = DBMethod.ExecuteQuery(FIN.DAL.HR.FeedbackTemplateDAL.getFeedbackTemplateRatingTypeName(ddlFeedbacktyp.SelectedValue.ToString())).Tables[0];

            if (ddlFeedbacktyp.SelectedValue.ToString().Length > 0)
            {
                ddlFeedbackRatingType.SelectedValue = dtData.Rows[0]["feedback_rating_type"].ToString();
                dtpStartDate.Text = txtFromDate.Text.ToString();
            }
            if (ddlFeedbackRatingType.SelectedValue.ToString() == "Paragraph")
            {
                txtfeedbackdesc.Text = ddlFeedbacktyp.SelectedItem.Text.ToString();
            }
            else
            {
                txtfeedbackdesc.Text = "";
            }
        }

    }
}