﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="EmployeeRequestEntry.aspx.cs" Inherits="FIN.Client.HR.EmployeeRequestEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 800px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 160px" id="lblDepartment">
                Department
            </div>
            <div class="divtxtBox  LNOrient" style="width: 152px">
                <asp:DropDownList ID="ddlDepartment" runat="server" TabIndex="1" AutoPostBack="True"
                    CssClass="validate[required] RequiredField ddlStype" OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
           <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 160px" id="lblDesignation">
                Designation
            </div>
            <div class="divtxtBox  LNOrient" style="width: 152px">
                <asp:DropDownList ID="ddlDesignation" runat="server" TabIndex="2" AutoPostBack="True"
                    CssClass="validate[required] RequiredField ddlStype" OnSelectedIndexChanged="ddlDesignation_SelectedIndexChanged">
                   <%-- <asp:ListItem Text="--Select-- " Value=""></asp:ListItem>--%>
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 160px" id="lblEmployeeName">
                Employee Name
            </div>
            <div class="divtxtBox  LNOrient" style="width: 485px">
                <asp:DropDownList ID="ddlEmployeeName" CssClass="validate[required] RequiredField ddlStype"
                    runat="server" TabIndex="3" AutoPostBack="True">
                   <%-- <asp:ListItem Text="---Select---" Value=""></asp:ListItem>--%>
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 160px" id="lblRequestType">
                Request Type
            </div>
            <div class="divtxtBox  LNOrient" style="width: 152px">
                <asp:DropDownList ID="ddlRequestType" CssClass="validate[required] RequiredField ddlStype"
                    runat="server" TabIndex="4">
                   <%-- <asp:ListItem Text="---Select---" Value=""></asp:ListItem>--%>
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 160px" id="lblRequestStatus">
                Request Status
            </div>
            <div class="divtxtBox  LNOrient" style="width: 152px">
                <asp:DropDownList ID="ddlStatus" runat="server" TabIndex="5" CssClass="validate[required] RequiredField ddlStype">
                   <%-- <asp:ListItem Text="---Select---" Value=""></asp:ListItem>--%>
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 160px" id="lblRequestDescription">
                Request Description
            </div>
            <div class="divtxtBox  LNOrient" style="width: 490px">
                <asp:TextBox ID="txtDescription" MaxLength="250" CssClass="validate[required] RequiredField txtBox"
                    runat="server" Height="50px" TabIndex="6" TextMode="MultiLine"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 160px" id="lblHRComments">
                HR Comments
            </div>
            <div class="divtxtBox  LNOrient" style="width: 490px">
                <asp:TextBox ID="txtHRComments" MaxLength="250" CssClass="txtBox" runat="server"
                    TabIndex="7" Height="50px" TextMode="MultiLine"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 160px" id="lblEnabledFlag">
                Active
            </div>
            <div class="divtxtBox  LNOrient" style="width: 152px">
                <asp:CheckBox ID="ChkEnabledFlag" runat="server" Checked="True" TabIndex="8" />
            </div>
            <div id="btnReqAssign" runat="server" class="lblBox" style="float: left; width: 312px">
                <asp:Button ID="btnReqAssignment" runat="server" Text="Assignment" OnClick="btnReqAssignment_Click" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" TabIndex="9" OnClick="btnSave_Click" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="10" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Reset" CssClass="btn" TabIndex="11"
                            OnClick="btnCancel_Click" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="12" />
                    </td>
                </tr>
            </table>
        </div>
        <%-- <div id="divDelete">
        <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
            PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
        </cc2:ModalPopupExtender>
        <asp:Panel ID="pnlConfirm" runat="server">
            <div class="ConfirmForm">
                <table>
                    <tr class="ConfirmHeading" style="width: 100%">
                        <td>
                            <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                Width="60px" />
                            &nbsp;
                            <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                        </td>
                    </tr>
                </table>
            </div>
        </asp:Panel>
    </div>--%>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/HR/HRChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
