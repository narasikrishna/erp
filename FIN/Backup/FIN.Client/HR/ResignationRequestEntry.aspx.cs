﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using FIN.DAL.HR;
using FIN.BLL.HR;
using FIN.DAL;
using FIN.BLL;
using VMVServices.Web;

namespace FIN.Client.HR
{
    public partial class ResignationRequestEntry : PageBase
    {
        HR_RESIG_REQUEST_HDR hR_RESIG_REQUEST_HDR = new HR_RESIG_REQUEST_HDR();
        string ProReturn = null;
        Boolean savedBool = false;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
            }

            if (VMVServices.Web.Utils.AddAllowed != FINAppConstants.Y && Master.Mode == FINAppConstants.Add)
            {
                btnSave.Visible = false;
            }

        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();

                EntityData = null;

                txtRequestDate.Text = DBMethod.ConvertDateToString(DateTime.Now.ToShortDateString());

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    using (IRepository<HR_RESIG_REQUEST_HDR> userCtx = new DataRepository<HR_RESIG_REQUEST_HDR>())
                    {
                        hR_RESIG_REQUEST_HDR = userCtx.Find(r =>
                            (r.RESIG_HDR_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = hR_RESIG_REQUEST_HDR;

                    txtRequestNumber.Text = hR_RESIG_REQUEST_HDR.RESIG_HDR_ID;
                    txtRequestDate.Text = DBMethod.ConvertDateToString(hR_RESIG_REQUEST_HDR.RESIG_REQ_DT.ToString());
                    ddlDepartmentName.SelectedValue = hR_RESIG_REQUEST_HDR.RESIG_DEPT_ID;
                    Department_BLL.GetDesignationName(ref ddldesignation, ddlDepartmentName.SelectedValue.ToString());
                    ddldesignation.SelectedValue = hR_RESIG_REQUEST_HDR.RESIG_DEPT_DESIG_ID;
                    Employee_BLL.GetEmplNameNDeptDesig(ref ddlEmployeeName, ddlDepartmentName.SelectedValue.ToString(), ddldesignation.SelectedValue.ToString());
                    ddlEmployeeName.SelectedValue = hR_RESIG_REQUEST_HDR.RESIG_EMP_ID;
                    ddlResignationType.SelectedValue = hR_RESIG_REQUEST_HDR.RESIG_TYPE;
                    txtNoticePeriod.Text = hR_RESIG_REQUEST_HDR.RESIG_NOTICE_PERIOD.ToString();
                    // txtFileName.Text = hR_RESIG_REQUEST_HDR.RESIG_FILE_NAME;
                    txtRemarks.Text = hR_RESIG_REQUEST_HDR.RESIG_REMARKS;
                    ddlRequestStatus.SelectedValue = hR_RESIG_REQUEST_HDR.RESIG_REQ_STATUS;
                    ddlReason.SelectedValue = hR_RESIG_REQUEST_HDR.RESIG_REASON;


                    if (hR_RESIG_REQUEST_HDR.NOTICE_FROM_DT != null)
                    {
                        txtFromDate.Text = DBMethod.ConvertDateToString(hR_RESIG_REQUEST_HDR.NOTICE_FROM_DT.ToString());
                    }
                    //if (hR_RESIG_REQUEST_HDR.NOTICE_TO_DT != null)
                    //{
                    //    txtToDate.Text = DBMethod.ConvertDateToString(hR_RESIG_REQUEST_HDR.NOTICE_TO_DT.ToString());
                    //}



                    if (hR_RESIG_REQUEST_HDR.ENABLED_FLAG == "1")
                    {
                        chkActive.Checked = true;
                    }
                    else
                    {
                        chkActive.Checked = false;
                    }

                    // hR_RESIG_REQUEST_HDR.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;
                    // hR_RESIG_REQUEST_HDR.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                    //hR_RESIG_REQUEST_HDR.RESIG_ORG_ID = FINAppConstants.EnabledFlag;

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    hR_RESIG_REQUEST_HDR = (HR_RESIG_REQUEST_HDR)EntityData;
                }


                //  hR_RESIG_REQUEST_HDR.RESIG_HDR_ID = txtRequestNumber.Text;
                hR_RESIG_REQUEST_HDR.RESIG_REQ_DT = DBMethod.ConvertStringToDate(txtRequestDate.Text.ToString());
                hR_RESIG_REQUEST_HDR.RESIG_DEPT_ID = ddlDepartmentName.SelectedValue;
                hR_RESIG_REQUEST_HDR.RESIG_EMP_ID = ddlEmployeeName.SelectedValue;
                hR_RESIG_REQUEST_HDR.RESIG_TYPE = ddlResignationType.SelectedValue;
                hR_RESIG_REQUEST_HDR.RESIG_NOTICE_PERIOD = int.Parse(txtNoticePeriod.Text.ToString());
                // hR_RESIG_REQUEST_HDR.RESIG_FILE_NAME = txtFileName.Text;
                hR_RESIG_REQUEST_HDR.RESIG_DEPT_DESIG_ID = ddldesignation.SelectedValue;
                hR_RESIG_REQUEST_HDR.RESIG_REMARKS = txtRemarks.Text;
                hR_RESIG_REQUEST_HDR.RESIG_REQ_STATUS = ddlRequestStatus.SelectedValue;
                hR_RESIG_REQUEST_HDR.RESIG_REASON = ddlReason.SelectedValue;
                hR_RESIG_REQUEST_HDR.RESIG_ORG_ID = VMVServices.Web.Utils.OrganizationID;
                if (txtFromDate.Text != string.Empty)
                {
                    hR_RESIG_REQUEST_HDR.NOTICE_FROM_DT = DBMethod.ConvertStringToDate(txtFromDate.Text.ToString());
                }
                //if (txtToDate.Text != string.Empty)
                //{
                //    hR_RESIG_REQUEST_HDR.NOTICE_TO_DT = DBMethod.ConvertStringToDate(txtToDate.Text.ToString());
                //}

                if (chkActive.Checked == true)
                {
                    hR_RESIG_REQUEST_HDR.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                }
                else
                {
                    hR_RESIG_REQUEST_HDR.ENABLED_FLAG = FINAppConstants.DisabledFlag;
                }
                //hR_RESIG_REQUEST_HDR.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    hR_RESIG_REQUEST_HDR.MODIFIED_BY = this.LoggedUserName;
                    hR_RESIG_REQUEST_HDR.MODIFIED_DATE = DateTime.Today;

                }
                else
                {
                    hR_RESIG_REQUEST_HDR.RESIG_HDR_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_041.ToString(), false, true);
                    hR_RESIG_REQUEST_HDR.CREATED_BY = this.LoggedUserName;
                    hR_RESIG_REQUEST_HDR.CREATED_DATE = DateTime.Today;
                }
                hR_RESIG_REQUEST_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_RESIG_REQUEST_HDR.RESIG_HDR_ID.ToString());

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void FillComboBox()
        {

            Department_BLL.GetDepartmentName(ref ddlDepartmentName);
            //Employee_BLL.GetEmployeeName(ref ddlEmployeeName);
            Lookup_BLL.GetLookUpValues(ref ddlResignationType, "RT");
            Lookup_BLL.GetLookUpValues(ref ddlRequestStatus, "RRS");
            Lookup_BLL.GetLookUpValues(ref ddlReason, "RRR");
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();





                AssignToBE();



                // Duplicate Validation Through Backend Package PKG_VALIDATIONS

                ProReturn = FIN.DAL.HR.ResignationRequestEntry_DAL.GetSPFOR_DUPLICATE_CHECK(hR_RESIG_REQUEST_HDR.RESIG_EMP_ID, hR_RESIG_REQUEST_HDR.RESIG_HDR_ID);

                if (ProReturn != string.Empty)
                {
                    if (ProReturn != "0")
                    {
                        ErrorCollection.Add("RESIGNREQ", ProReturn);
                        if (ErrorCollection.Count > 0)
                        {
                            return;
                        }
                    }
                }



                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            DBMethod.SaveEntity<HR_RESIG_REQUEST_HDR>(hR_RESIG_REQUEST_HDR);
                            //  DisplaySaveCompleteMessage(Master.ListPageToOpen);
                            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                            savedBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {

                            DBMethod.SaveEntity<HR_RESIG_REQUEST_HDR>(hR_RESIG_REQUEST_HDR, true);
                            //  DisplaySaveCompleteMessage(Master.ListPageToOpen);
                            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                            savedBool = true;
                            break;

                        }
                }

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                if (hR_RESIG_REQUEST_HDR.WORKFLOW_COMPLETION_STATUS == "1")
                {
                    FIN.BLL.SSM.Alert_BLL.GenerateEmail(FINMessageConstatns.RESIGNATION_REQ_ALERT);

                    if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0" && savedBool == true)
                    {
                        if (VMVServices.Web.Utils.IsAlert == "1")
                        {
                            FINSQL.UpdateAlertUserLevel();
                        }
                    }
                }
                if (ErrorCollection.Count > 0)
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion




        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                DBMethod.DeleteEntity<HR_RESIG_REQUEST_HDR>(hR_RESIG_REQUEST_HDR);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void ddlDepartmentName_SelectedIndexChanged(object sender, EventArgs e)
        {
            // ResignationRequestEntry_BLL.fn_getDepartment(ref ddlEmployeeName, ddlDepartmentName.SelectedValue.ToString());
            Department_BLL.GetDesignationName(ref ddldesignation, ddlDepartmentName.SelectedValue.ToString());
        }

        protected void ddldesignation_SelectedIndexChanged(object sender, EventArgs e)
        {
            Employee_BLL.GetEmplNameNDeptDesig(ref ddlEmployeeName, ddlDepartmentName.SelectedValue.ToString(), ddldesignation.SelectedValue.ToString());
        }

        protected void ddlEmployeeName_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadNoticePeriod();
        }
        private void LoadNoticePeriod()
        {
            HR_EMPLOYEES hR_EMPLOYEES = new HR_EMPLOYEES();
            using (IRepository<HR_EMPLOYEES> userCtx = new DataRepository<HR_EMPLOYEES>())
            {
                hR_EMPLOYEES = userCtx.Find(r =>
                    (r.EMP_ID == ddlEmployeeName.SelectedValue)
                    ).SingleOrDefault();
            }
            if (hR_EMPLOYEES != null)
            {
                txtNoticePeriod.Text = hR_EMPLOYEES.NOTICE_PERIOD.ToString();
            }
        }

    }
}