﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.BLL.AP;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.BLL;
using VMVServices.Web;

namespace FIN.Client.HR
{
    public partial class Vacancies : PageBase
    {

        HR_VACANCIES hR_VACANCIES = new HR_VACANCIES();
        System.Collections.SortedList slControls = new System.Collections.SortedList();
        //string ProReturn = null;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                //pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                //pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
                
            }

            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
            }

            if (VMVServices.Web.Utils.AddAllowed != FINAppConstants.Y && Master.Mode == FINAppConstants.Add)
            {
                btnSave.Visible = false;
            }

        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                //ddlWeightUOM.Enabled = false;
                //ddlLengthUOM.Enabled = false;
                //ddlAreaUOM.Enabled = false;
                //ddlVolumeUOM.Enabled = false;


                Startup();
                FillComboBox();
               

                EntityData = null;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    using (IRepository<HR_VACANCIES> userCtx = new DataRepository<HR_VACANCIES>())
                    {
                        hR_VACANCIES = userCtx.Find(r =>
                            (r.VAC_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = hR_VACANCIES;
                    txtVacancyName.Text = hR_VACANCIES.VAC_NAME;
                    ddlDepartment.SelectedValue = hR_VACANCIES.VAC_DEPT_ID;
                    FillDesignation();
                    ddlDesignation.SelectedValue = hR_VACANCIES.VAC_DESIG_ID;
                    txtNoOfPeoples.Text = hR_VACANCIES.VAC_NO_OF_PEOPLES.ToString();
                    txtExperienceYears.Text = hR_VACANCIES.VAC_EXP_YEARS;
                    ddlInternalExternal.SelectedValue = hR_VACANCIES.VAC_INTER_EXTERNAL;
                    showagency();
                    txtAgencyName.Text = hR_VACANCIES.VAC_AGENCY_NAME;
                    txtAgencyPerson.Text = hR_VACANCIES.VAC_AGENCY_PERSON;
                    txtAgencyContact.Text = hR_VACANCIES.VAC_AGENCY_CONTACT;
                    txtVacancyMaxExp.Text = hR_VACANCIES.VAC_MAX_EXP.ToString();
                    txtVacMinExp.Text   = hR_VACANCIES.VAC_MIN_EXP.ToString();
                    ddlType.SelectedValue = hR_VACANCIES.VAC_TYPE;
                    txtCommunicationlevel.Text = hR_VACANCIES.VAC_COMM_LEVEL;
                    
                    txtRequiredTechnology.Text = hR_VACANCIES.VAC_REQ_TECHNOLOGY;
                    txtBudget.Text = hR_VACANCIES.VAC_BUDGET_ID;
                    ddlPeriod.SelectedValue = hR_VACANCIES.PERIOD;
                    txtBudgetFrom.Text = DBMethod.GetAmtDecimalCommaSeparationValue(hR_VACANCIES.BUDGET_FROM.ToString());
                    txtBudgetTo.Text = DBMethod.GetAmtDecimalCommaSeparationValue(hR_VACANCIES.BUDGET_TO.ToString());
                    ddlStatus.SelectedValue = hR_VACANCIES.STATUS;



                    chkRelocate.Checked = hR_VACANCIES.VAC_RELOCATE_YN == FINAppConstants.EnabledFlag ? true : false;
                    ChkCertificateReq.Checked = hR_VACANCIES.VAC_CERT_REQ_YN == FINAppConstants.EnabledFlag ? true : false;
                    ChkEnabledFlag.Checked = hR_VACANCIES.ENABLED_FLAG == FINAppConstants.EnabledFlag ? true : false;
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    //Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>

        private void FillComboBox()
        {
            ComboFilling.fn_getDepartment(ref ddlDepartment);
            Lookup_BLL.GetLookUpValues(ref ddlInternalExternal, "INTERNAL_EXTERNAL");
            Lookup_BLL.GetLookUpValues(ref ddlType, "VAC_TYPE");
            Lookup_BLL.GetLookUpValues(ref ddlStatus, "VAC_STATUS");
            Lookup_BLL.GetLookUpValues(ref ddlPeriod, "PERIOD");
            
        }

        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    hR_VACANCIES = (HR_VACANCIES)EntityData;
                }
                hR_VACANCIES.VAC_NAME = txtVacancyName.Text;
                hR_VACANCIES.VAC_DEPT_ID = ddlDepartment.SelectedValue.ToString();
                hR_VACANCIES.VAC_DESIG_ID = ddlDesignation.SelectedValue.ToString();
                hR_VACANCIES.VAC_NO_OF_PEOPLES = CommonUtils.ConvertStringToInt(txtNoOfPeoples.Text.ToString());
                hR_VACANCIES.VAC_EXP_YEARS = txtExperienceYears.Text.ToString();
                hR_VACANCIES.VAC_AGENCY_NAME = txtAgencyName.Text;
                hR_VACANCIES.VAC_AGENCY_PERSON = txtAgencyPerson.Text;
                hR_VACANCIES.VAC_AGENCY_CONTACT = txtAgencyContact.Text;
                hR_VACANCIES.VAC_MAX_EXP = CommonUtils.ConvertStringToInt(txtVacancyMaxExp.Text.ToString());
                hR_VACANCIES.VAC_MIN_EXP = CommonUtils.ConvertStringToInt(txtVacMinExp.Text.ToString());
                hR_VACANCIES.VAC_TYPE = ddlType.SelectedValue.ToString();
                hR_VACANCIES.VAC_COMM_LEVEL = txtCommunicationlevel.Text;
                hR_VACANCIES.VAC_INTER_EXTERNAL = ddlInternalExternal.SelectedValue.ToString();
                hR_VACANCIES.VAC_REQ_TECHNOLOGY = txtRequiredTechnology.Text;
                hR_VACANCIES.VAC_BUDGET_ID = txtBudget.Text;
                hR_VACANCIES.PERIOD = ddlPeriod.SelectedValue;
                if (txtBudgetFrom.Text == "")
                {
                    txtBudgetFrom.Text = "0";
                }
                
                hR_VACANCIES.BUDGET_FROM = CommonUtils.ConvertStringToDecimal(txtBudgetFrom.Text);
                hR_VACANCIES.BUDGET_TO = CommonUtils.ConvertStringToDecimal(txtBudgetTo.Text);
                hR_VACANCIES.STATUS = ddlStatus.SelectedValue;
                
                hR_VACANCIES.ENABLED_FLAG = FINAppConstants.Y;


                hR_VACANCIES.ORG_ID = VMVServices.Web.Utils.OrganizationID;
                
                hR_VACANCIES.VAC_RELOCATE_YN = chkRelocate.Checked == true ? FINAppConstants.EnabledFlag : FINAppConstants.DisabledFlag;
                hR_VACANCIES.VAC_CERT_REQ_YN = ChkCertificateReq.Checked == true ? FINAppConstants.EnabledFlag : FINAppConstants.DisabledFlag;
                hR_VACANCIES.ENABLED_FLAG = ChkEnabledFlag.Checked == true ? FINAppConstants.EnabledFlag : FINAppConstants.DisabledFlag;




                //if (chkItemSerialControl.Checked == true)
                //{
                //    ddlServiceUOM.Enabled = true;
                //    iNV_ITEM_MASTER.SERVICE_DURATION_UOM = ddlServiceUOM.SelectedValue;
                //}
                //else
                //{
                //    ddlServiceUOM.Attributes.Add("disabled", "disabled");
                //}


                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    hR_VACANCIES.MODIFIED_BY = this.LoggedUserName;
                    hR_VACANCIES.MODIFIED_DATE = DateTime.Today;

                }
                else
                {
                    hR_VACANCIES.VAC_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_047.ToString(), false, true);
                    //iNV_ITEM_MASTER.WORKFLOW_COMPLETION_STATUS = "1";
                    hR_VACANCIES.CREATED_BY = this.LoggedUserName;
                    hR_VACANCIES.CREATED_DATE = DateTime.Today;

                }

                hR_VACANCIES.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_VACANCIES.VAC_ID);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                //ErrorCollection = CommonUtils.ValidateBetweenValues(txtBudget.Text,txtBudgetFrom.Text,txtBudgetTo.Text,"Difference of the Budget from and to should be equal or less than the Budget amount");
                //if (ErrorCollection.Count > 0)
                //{
                //    return;
                //}

                if (decimal.Parse(txtBudgetFrom.Text) > decimal.Parse(txtBudgetTo.Text))
                {
                    ErrorCollection.Add("Budget", "Budget From is Less than or equal to Budget To Amount");
                    return;
                }

                AssignToBE();

                //emptyvalid();
                if (ErrorCollection.Count > 0)
                {
                    return;
                }


                // Duplicate Validation Through Backend Package PKG_VALIDATIONS



                //ProReturn = FIN.DAL.AP.Item_DAL.GetSPFOR_DUPLICATE_CHECK(iNV_ITEM_MASTER.ITEM_CODE,txtItemName.Text, iNV_ITEM_MASTER.ORG_ID, iNV_ITEM_MASTER.ITEM_ID);

                //if (ProReturn != string.Empty)
                //{
                //    if (ProReturn != "0")
                //    {
                //        ErrorCollection.Add("ITEMNAME", ProReturn);
                //        if (ErrorCollection.Count > 0)
                //        {
                //            return;
                //        }
                //    }
                //}


                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            DBMethod.SaveEntity<HR_VACANCIES>(hR_VACANCIES);
                            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                            break;
                        }
                    case FINAppConstants.Update:
                        {

                            DBMethod.SaveEntity<HR_VACANCIES>(hR_VACANCIES, true);
                            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                            break;

                        }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion




        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                DBMethod.DeleteEntity<HR_VACANCIES>(hR_VACANCIES);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillDesignation();
            
            
        }
        private void FillDesignation()
        {
            ComboFilling.fn_getDesignation(ref ddlDesignation, ddlDepartment.SelectedValue.ToString());
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {

        }

        protected void ddlInternalExternal_SelectedIndexChanged(object sender, EventArgs e)
        {

            showagency();
           
        }
        private void showagency()
        {
            //Agency1.Visible = false;
            //Agency2.Visible = false;
            if (ddlInternalExternal.SelectedItem.Text.ToString().ToUpper() == "EXTERNAL")
            {
                Agency1.Visible = true;
                Agency2.Visible = true;
            }
            else
            {
                Agency1.Visible = false;
                Agency2.Visible = false;
            }
            
        }





        //private void emptyvalid()
        //{
        //    if (hR_VACANCIES.VAC_DEPT_ID == null)
        //    {

        //        if (ddlDepartment.SelectedValue == string.Empty)
        //        {
        //            ErrorCollection.Add("Department", "Department Cannot be empty");
        //        }

        //    }
        //    if (hR_VACANCIES.VAC_DESIG_ID == null)
        //    {
        //        if (ddlDesignation.SelectedValue == string.Empty)

        //            ErrorCollection.Add("Designation", "Designation Cannot be empty");
        //        }
        //    }
        //    //if (hR_VACANCIES.VAC_TYPE > 0)
        //    //{

        //    //    if (ddlType.SelectedValue == string.Empty)
        //    //    {
        //    //        ErrorCollection.Add("DepartmentType", "Department Type Cannot be empty");
        //    //    }
        //    //}

        //    //if  > 0)
        //    //{
        //    //    if (ddlVolumeUOM.SelectedValue == string.Empty)
        //    //    {

        //    //        ErrorCollection.Add("VOLEMT", "Volume UOM Cannot be empty");
        //    //    }
        //    //}

        //}






    }





}
