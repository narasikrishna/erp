﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="SuccessionPlanningEntry.aspx.cs" Inherits="FIN.Client.HR.SuccessionPlanningEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 900px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="lblDept">
                Department
            </div>
            <div class="divtxtBox  LNOrient" style="width: 250px">
                <asp:DropDownList ID="ddlDept" runat="server" TabIndex="1" Width="250px" CssClass="validate[required] RequiredField ddlStype"
                    AutoPostBack="True" OnSelectedIndexChanged="ddlDept_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div class="lblBox LNOrient" style="width: 150px" id="lblDesignation">
                Designation
            </div>
            <div class="divtxtBox  LNOrient" style="width: 255px">
                <asp:DropDownList ID="ddlDesignation" runat="server" TabIndex="2" Width="100%" CssClass="validate[required] RequiredField ddlStype"
                    OnSelectedIndexChanged="ddlDesignation_SelectedIndexChanged" 
                    AutoPostBack="True">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="lblEmployee">
                Career Path Name
            </div>
            <div class="divtxtBox  LNOrient" style="width: 250px">
                <asp:DropDownList ID="ddlCareerPathName" runat="server" AutoPostBack="true" TabIndex="3"
                    Width="250px" CssClass="validate[required] RequiredField ddlStype" OnSelectedIndexChanged="ddlCareerPathName_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
             <div class="lblBox LNOrient" style="width: 150px" id="lblPathName">
                Description
            </div>
            <div class="divtxtBox  LNOrient" style="width: 255px">
                <asp:TextBox ID="txtPathName" CssClass="validate[required] RequiredField txtBox"
                    Enabled="False" MaxLength="100" TabIndex="4" runat="server"></asp:TextBox>
            </div>
             
           
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
           <div class="lblBox LNOrient" style="width: 150px" id="Div1">
                Career Plan
            </div>
            <div class="divtxtBox  LNOrient" style="width: 250px">
                <asp:DropDownList OnSelectedIndexChanged="ddlCareerPlan_SelectedIndexChanged" AutoPostBack="true"
                    ID="ddlCareerPlan" runat="server" TabIndex="5" Width="250px" CssClass="validate[required] RequiredField ddlStype">
                </asp:DropDownList>
            </div>
            <div class="lblBox LNOrient" style="width: 150px" id="lblProfileID">
                Profile Date
            </div>
            <div class="divtxtBox  LNOrient" style="width: 255px">
                <asp:TextBox runat="server" ID="txtProfileDate" CssClass="validate[required,custom[ReqDateDDMMYYY],,]  RequiredField txtBox"
                    TabIndex="6"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender4" TargetControlID="txtProfileDate" />
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender54" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtProfileDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            
            <div class="lblBox LNOrient" style="width: 150px" id="lblRate">
                Profile Remarks
            </div>
            <div class="divtxtBox  LNOrient" style="width: 675px">
                <asp:TextBox ID="txtRemarks" MaxLength="200" CssClass="validate[required] RequiredField txtBox"
                    runat="server" TabIndex="7"></asp:TextBox>
            </div>
        </div>
    </div>
    <div class="divClear_10">
    </div>
    <div class="LNOrient">
        <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
            Width="300px" DataKeyNames="PROC_DTL_ID,PLAN_DTL_ID,PLAN_PROFILE_HDR_ID,PROF_ID" OnRowDataBound="gvData_RowDataBound"
            ShowFooter="false">
            <Columns>
                <asp:TemplateField HeaderText="Profile Id">
                    <EditItemTemplate>
                        <asp:DropDownList ID="ddlProfileId" Width="200px" AutoPostBack="true" TabIndex="6"
                            runat="server" CssClass="RequiredField EntryFont ddlStype" OnSelectedIndexChanged="ddlProfileId_SelectedIndexChanged">
                        </asp:DropDownList>
                    </EditItemTemplate>
                    <FooterTemplate>
                        <asp:DropDownList ID="ddlProfileId" Width="200px" AutoPostBack="true" TabIndex="6"
                            Visible="false" runat="server" CssClass="RequiredField EntryFont ddlStype" OnSelectedIndexChanged="ddlProfileId_SelectedIndexChanged">
                        </asp:DropDownList>
                    </FooterTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblProfileId" Width="200px" runat="server" Text='<%# Eval("prof_id") %>'></asp:Label>
                    </ItemTemplate>
                   
                    <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Profile Description">
                    <EditItemTemplate>
                        <asp:TextBox ID="txtdesc" Width="130px" MaxLength="200" runat="server" CssClass="EntryFont RequiredField  txtBox"
                            Text='<%# Eval("prof_name") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <FooterTemplate>
                        <asp:TextBox ID="txtdesc" TabIndex="5" Width="130px" MaxLength="200" runat="server"
                            Visible="false" CssClass="EntryFont RequiredField  txtBox"></asp:TextBox>
                    </FooterTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lbldesc" Width="130px" runat="server" Text='<%# Eval("prof_name") %>'></asp:Label>
                    </ItemTemplate>
                   
                    <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                </asp:TemplateField>

                 <asp:TemplateField HeaderText="Profile Detail Description">
                    <EditItemTemplate>
                        <asp:TextBox ID="txtDetdesc" Width="130px" MaxLength="200" runat="server" CssClass="EntryFont RequiredField  txtBox"
                            Text='<%# Eval("com_level_desc") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <FooterTemplate>
                        <asp:TextBox ID="txtDetdesc" TabIndex="5" Width="130px" MaxLength="200" runat="server"
                            Visible="false" CssClass="EntryFont RequiredField  txtBox"></asp:TextBox>
                    </FooterTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblDetdesc" Width="130px" runat="server" Text='<%# Eval("com_level_desc") %>'></asp:Label>
                    </ItemTemplate>
                   
                    <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                </asp:TemplateField> 
                <asp:TemplateField HeaderText="Active">
                    <EditItemTemplate>
                        <asp:CheckBox ID="chkActive" runat="server" Checked='<%# Convert.ToBoolean(Eval("enabled_flag")) %>' />
                    </EditItemTemplate>
                    <FooterTemplate>
                        <asp:CheckBox ID="chkActive" TabIndex="8" runat="server" Checked="true" Visible="false" />
                    </FooterTemplate>
                    <ItemTemplate>
                        <asp:CheckBox ID="chkActive" runat="server" Checked='<%# Convert.ToBoolean(Eval("enabled_flag")) %>' />
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="" Visible="false">
                    <ItemTemplate>
                        <asp:ImageButton ID="ibtnEdit" TabIndex="12" runat="server" AlternateText="Edit"
                            Visible="false" CausesValidation="false" ToolTip="Edit" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                        <asp:ImageButton ID="ibtnDelete" TabIndex="13" runat="server" AlternateText="Delete"
                            Visible="false" CausesValidation="false" ToolTip="Delete" CommandName="Delete"
                            ImageUrl="~/Images/Delete.JPG" />
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:ImageButton ID="ibtnUpdate" TabIndex="14" runat="server" AlternateText="Update"
                            Visible="false" CommandName="Update" ToolTip="Update" Height="20px" ImageUrl="~/Images/Update.png" />
                        <asp:ImageButton ID="ibtnCancel" TabIndex="15" runat="server" AlternateText="Cancel"
                            Visible="false" CausesValidation="false" ToolTip="Cancel" CommandName="Cancel"
                            ImageUrl="~/Images/Close.jpg" />
                    </EditItemTemplate>
                    <FooterTemplate>
                        <asp:ImageButton ID="ibtnInsert" TabIndex="10" runat="server" AlternateText="Add"
                            Visible="false" CommandName="FooterInsert" ToolTip="Add" ImageUrl="~/Images/Add.jpg" />
                    </FooterTemplate>
                    <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Center" />
                </asp:TemplateField>
            </Columns>
            <EmptyDataRowStyle CssClass="EmptyRowStyle" />
            <HeaderStyle CssClass="GridHeader" />
            <AlternatingRowStyle CssClass="GrdAltRow" />
        </asp:GridView>
    </div>
    <div class="divClear_10">
    </div>
    <div align="right" style="width: 300px;">
        <asp:Button ID="btnProcess" runat="server" Text="Process" CssClass="btn" TabIndex="8"
            OnClick="btnProcess_Click" />
        <asp:HiddenField ID="hf_PathHdrId" runat="server" Value="" />
    </div>
    <div class="divClear_10">
    </div>
    <div class="LNOrient">
        <asp:GridView ID="gvProfile" runat="server" AutoGenerateColumns="False" CssClass="Grid"
            OnRowDataBound="gvProfile_RowDataBound" Width="700px" DataKeyNames="PROC_DTL_DTL_ID,plan_profile_dtl_id,plan_dtl_id,prof_id,remarks,recommendations,employee_rating,Emp_name,PLAN_PROFILE_REQUIRED_VALUE,Min_Value,emp_id,Max_Value"
            >
            <Columns>
                <asp:TemplateField HeaderText="Profile Line">
                    <EditItemTemplate>
                        <asp:DropDownList ID="ddlProfileId" Width="200px" TabIndex="6" runat="server" CssClass="RequiredField EntryFont ddlStype">
                        </asp:DropDownList>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblProfileId" Width="200px" runat="server" Text='<%# Eval("prof_id") %>'></asp:Label>
                    </ItemTemplate>
                    
                    <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Employee Name">
                    <EditItemTemplate>
                        <asp:TextBox ID="txtEmpName" Width="130px" MaxLength="200" runat="server" CssClass="EntryFont RequiredField  txtBox"
                            Text='<%# Eval("Emp_name") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblEmpName" Width="130px" runat="server" Text='<%# Eval("Emp_name") %>'></asp:Label>
                    </ItemTemplate>
                   
                    <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Min Value">
                    <EditItemTemplate>
                        <asp:TextBox ID="txtMinValue" Width="130px" MaxLength="200" runat="server" CssClass="EntryFont RequiredField  txtBox"
                            Text='<%# Eval("Min_Value") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblMinValue" Width="130px" runat="server" Text='<%# Eval("Min_Value") %>'></asp:Label>
                    </ItemTemplate>
                    
                    <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Max Value">
                    <EditItemTemplate>
                        <asp:TextBox ID="txtMaxValue" Width="130px" MaxLength="200" runat="server" CssClass="EntryFont RequiredField  txtBox"
                            Text='<%# Eval("max_value") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblMaxValue" Width="130px" runat="server" Text='<%# Eval("max_value") %>'></asp:Label>
                    </ItemTemplate>
                   
                    <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Required Value">
                    <EditItemTemplate>
                        <asp:TextBox ID="txtRequiredValue" Width="130px" MaxLength="200" runat="server" CssClass="EntryFont RequiredField  txtBox"
                            Text='<%# Eval("PLAN_PROFILE_REQUIRED_VALUE") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblRequiredValue" Width="130px" runat="server" Text='<%# Eval("PLAN_PROFILE_REQUIRED_VALUE") %>'></asp:Label>
                    </ItemTemplate>
                   
                    <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Employee Value">
                    <EditItemTemplate>
                        <asp:TextBox ID="txtEmployeeValue" Width="130px" MaxLength="200" runat="server" CssClass="EntryFont RequiredField  txtBox"
                            Text='<%# Eval("employee_rating") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblEmployeeValue" Width="130px" runat="server" Text='<%# Eval("employee_rating") %>'></asp:Label>
                    </ItemTemplate>
                    
                    <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Recommendation">
                    <EditItemTemplate>
                        <asp:TextBox ID="txtRecommendation" Width="130px" MaxLength="50" runat="server" TabIndex="9"
                            CssClass="EntryFont RequiredField  txtBox" Text='<%# Eval("recommendations") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <FooterTemplate>
                        <asp:TextBox ID="txtRecommendation" TabIndex="5" Width="130px" MaxLength="200" runat="server"
                            Visible="false" CssClass="EntryFont RequiredField  txtBox"></asp:TextBox>
                    </FooterTemplate>
                    <ItemTemplate>
                        <asp:TextBox ID="txtRecommendation" Width="130px" MaxLength="50" runat="server" TabIndex="9"
                            CssClass="EntryFont RequiredField  txtBox" Text='<%# Eval("recommendations") %>'></asp:TextBox>
                    </ItemTemplate>
                   
                    <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Remarks">
                    <EditItemTemplate>
                        <asp:TextBox ID="txtProfRemarks" Width="130px" MaxLength="200" runat="server" CssClass="EntryFont RequiredField  txtBox"
                            Text='<%# Eval("remarks") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <FooterTemplate>
                        <asp:TextBox ID="txtProfRemarks" TabIndex="10" Width="130px" MaxLength="200" runat="server"
                            Visible="false" CssClass="EntryFont RequiredField  txtBox"></asp:TextBox>
                    </FooterTemplate>
                    <ItemTemplate>
                        <asp:TextBox ID="txtProfRemarks" Width="130px" MaxLength="200" runat="server" CssClass="EntryFont RequiredField  txtBox"
                            TabIndex="10" Text='<%# Eval("remarks") %>'></asp:TextBox>
                    </ItemTemplate>
                    
                    <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Profile Active">
                    <EditItemTemplate>
                        <asp:CheckBox ID="chkProfileActive" runat="server" Checked='<%# Convert.ToBoolean(Eval("enabled_flag")) %>' />
                    </EditItemTemplate>
                    <FooterTemplate>
                        <asp:CheckBox ID="chkProfileActive" TabIndex="8" runat="server" Checked="true" Visible="false" />
                    </FooterTemplate>
                    <ItemTemplate>
                        <asp:CheckBox ID="chkProfileActive" runat="server" Checked='<%# Convert.ToBoolean(Eval("enabled_flag")) %>' />
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" />
                </asp:TemplateField>
            </Columns>
            <EmptyDataRowStyle CssClass="EmptyRowStyle" />
            <HeaderStyle CssClass="GridHeader" />
            <AlternatingRowStyle CssClass="GrdAltRow" />
        </asp:GridView>
    </div>
    <div class="divClear_10">
    </div>
    <div class="divRowContainer divAction">
        <table class="SaveTable">
            <tr>
                <td>
                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" TabIndex="3" OnClick="btnSave_Click" />
                </td>
                <td>
                    <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="4" />
                </td>
                <td>
                    <asp:Button ID="btnCancel" runat="server" Text="Reset" CssClass="btn" TabIndex="5" />
                </td>
                <td>
                    <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="6" />
                </td>
            </tr>
        </table>
    </div>
    <div id="divDelete">
        <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
            PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
        </cc2:ModalPopupExtender>
        <asp:Panel ID="pnlConfirm" runat="server">
            <div class="ConfirmForm">
                <table>
                    <tr class="ConfirmHeading" style="width: 100%">
                        <td>
                            <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                Width="60px" />
                            &nbsp;
                            <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                        </td>
                    </tr>
                </table>
            </div>
        </asp:Panel>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
     <script src="../LanguageScript/HR/HRChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
