﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="InterviewNewEntry.aspx.cs" Inherits="FIN.Client.HR.InterviewNewEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1200px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblDesc">
                Vacancy
            </div>
            <div class="divtxtBox  LNOrient" style="width: 530px">
                <asp:DropDownList ID="ddlvacancy" runat="server" CssClass="validate[required] RequiredField ddlStype" TabIndex="1"
                    AutoPostBack="True" OnSelectedIndexChanged="ddlvacancy_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
        </div>
    </div>
    <div class="divClear_10">
    </div>
    <div class="divRowContainer">
        <div class="lblBox LNOrient" style="width: 200px" id="lblDate">
            Applicant
        </div>
        <div class="divtxtBox  LNOrient" style="width: 530px">
            <asp:DropDownList ID="ddlApplicant" runat="server" CssClass="validate[required] RequiredField ddlStype"
                TabIndex="2" AutoPostBack="True" OnSelectedIndexChanged="ddlApplicant_SelectedIndexChanged">
            </asp:DropDownList>
        </div>
    </div>
    <div class="divClear_10">
    </div>
    <div class="divClear_10">
    </div>
    <div class="divRowContainer">
        <div class="lblBox LNOrient" style="width: 200px" id="Div2">
            Employee
        </div>
        <div class="divtxtBox  LNOrient" style="width: 250px">
            <asp:DropDownList ID="ddlemployee" runat="server" CssClass="validate[required] RequiredField ddlStype"
                TabIndex="3" AutoPostBack="True" OnSelectedIndexChanged="ddlemployee_SelectedIndexChanged">
            </asp:DropDownList>
        </div>
         <div class="colspace  LNOrient" >
                &nbsp</div>
        <div class="lblBox LNOrient" style="width: 100px" >
            Date
        </div>
        <div class="divtxtBox  LNOrient" style="width: 155px">
            <asp:TextBox ID="txtdate" CssClass="validate[]  RequiredField  txtBox" Enabled="false" TabIndex="4"
                runat="server" ></asp:TextBox>
        </div>
    </div>
    <div class="divClear_10">
    </div>
    <div class="divRowContainer">
        <div class="lblBox LNOrient" style="width: 200px" id="Div1">
            Department
        </div>
        <div class="divtxtBox  LNOrient" style="width: 250px">
            <asp:TextBox ID="txtDept" CssClass="validate[]  RequiredField  txtBox" Enabled="false" TabIndex="5"
                runat="server"></asp:TextBox>
        </div>
        <div class="colspace  LNOrient" >
                &nbsp</div>
        <div class="lblBox LNOrient" style="width: 100px" >
            Designation
        </div>
        <div class="divtxtBox  LNOrient" style="width: 155px">
            <asp:TextBox ID="txtdesig" CssClass="validate[]  RequiredField  txtBox" Enabled="false"
                runat="server"></asp:TextBox>
        </div>
    </div>
    <div class="divClear_10">
    </div>
    <div class="divRowContainer">
        <div class="lblBox LNOrient" style="width: 200px" id="Div4">
            Location
        </div>
        <div class="divtxtBox  LNOrient" style="width: 250px">
            <asp:TextBox ID="txtLoc" CssClass="validate[]  RequiredField  txtBox" Enabled="false"
                runat="server"></asp:TextBox>
        </div>
         <div class="colspace  LNOrient" >
                &nbsp</div>
        <div class="lblBox LNOrient" style="width: 100px" >
            Mode
        </div>
        <div class="divtxtBox  LNOrient" style="width: 155px">
            <asp:TextBox ID="txtmode" CssClass="validate[]  RequiredField  txtBox" Enabled="false"
                runat="server"></asp:TextBox>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" style="display:none">
            <div class="lblBox LNOrient" style="width: 200px" id="Div6">
                Criteria
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:DropDownList ID="ddlcriteria" runat="server" CssClass="validate[required] RequiredField ddlStype">
                </asp:DropDownList>
            </div>
             <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 200px" id="Div7" runat="server">
                Level Scored
            </div>
            <div class="divtxtBox  LNOrient" style="width: 155px">
                <asp:TextBox ID="txtlevelscored" CssClass="validate[required]  RequiredField  txtBox"
                    runat="server"></asp:TextBox>
            </div>
        </div>
        
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="Div8">
                Comments
            </div>
            <div class="divtxtBox  LNOrient" style="width: 540px">
                <asp:TextBox ID="txtcomments"  CssClass=" RequiredField  txtBox" TabIndex="6"  MaxLength="500"
                    runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="Div9">
                Status
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:DropDownList ID="ddlstatus" runat="server" CssClass="validate[required] RequiredField ddlStype" TabIndex="7">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="LNOrient">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                DataKeyNames="INT_CRIT_ID,INT_DTL_ID,VAC_EVAL_ID,DELETED" OnRowCancelingEdit="gvData_RowCancelingEdit"
                OnRowCommand="gvData_RowCommand" OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound"
                OnRowDeleting="gvData_RowDeleting" OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating"
                ShowFooter="false">
                <Columns>
                    <asp:TemplateField HeaderText="Evaluation Criteria" FooterStyle-Width="350px">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlEvalCrit" runat="server" CssClass="RequiredField ddlStype" TabIndex="8"
                                Width="97%">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlEvalCrit" runat="server" CssClass="RequiredField ddlStype" TabIndex="8"
                                Width="97%">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblEvalCrit" CssClass="adminFormFieldHeading" Width="97%" runat="server"
                                Text='<%# Eval("VAC_EVAL_NAME") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" Width="200px" />
                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="Criteria Value" >
                     <ItemTemplate>
                            <asp:Label ID="lblEvalCritValue" CssClass="adminFormFieldHeading" Width="97%" runat="server"
                                Text='<%# Eval("VAC_CRIT_VALUE") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Right" Width="40px" />
                     </asp:TemplateField>
                    <asp:TemplateField HeaderText="Level Scored">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtLevelScored" runat="server" CssClass="EntryFont RequiredField txtBox" TabIndex="9" MaxLength="4"
                                Width="90%" Text='<%#  Eval("INT_LEVEL_SCORED") %>' ></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtLevelScored" runat="server" CssClass="EntryFont RequiredField txtBox" TabIndex="9" MaxLength="4"
                                Width="90%"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblLevelScored" runat="server" Width="90%" Text='<%# Eval("INT_LEVEL_SCORED") %>' Visible="false"></asp:Label>
                            <asp:TextBox ID="txtItemLevelScored" runat="server" CssClass="EntryFont RequiredField txtBox_N" TabIndex="9" MaxLength="4"
                                Width="90%" Text='<%#  Eval("INT_LEVEL_SCORED") %>' ></asp:TextBox>
                                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars=""
                                FilterType="Numbers,Custom" TargetControlID="txtItemLevelScored" />
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Right" Width="40px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="" Visible="false">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" runat="server" AlternateText="Edit" CausesValidation="false" TabIndex="10"
                                ToolTip="Edit" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                            <asp:ImageButton ID="ibtnDelete" runat="server" AlternateText="Delete" CausesValidation="false" TabIndex="11"
                                ToolTip="Delete" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibtnUpdate" runat="server" AlternateText="Update" CommandName="Update" TabIndex="12"
                                ToolTip="Update" Height="20px" ImageUrl="~/Images/Update.png" />
                            <asp:ImageButton ID="ibtnCancel" runat="server" AlternateText="Cancel" CausesValidation="false" TabIndex="13"
                                ToolTip="Cancel" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="ibtnInsert" runat="server" AlternateText="Add" CommandName="FooterInsert" TabIndex="14"
                                ToolTip="Add" ImageUrl="~/Images/Add.jpg" />
                        </FooterTemplate>
                        <ItemStyle  HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                            TabIndex="14" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="15" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="16" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="17" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table>
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/HR/HRChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
