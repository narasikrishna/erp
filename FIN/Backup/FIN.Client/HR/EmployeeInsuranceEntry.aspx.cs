﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.AP;
using FIN.BLL;
using FIN.BLL.GL;
using FIN.BLL.AP;
using FIN.BLL.HR;
using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;
namespace FIN.Client.HR
{
    public partial class EmployeeInsuranceEntry : PageBase
    {
        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();
        INSURANCE_PLAN_ENTRY_HDR iNSURANCE_PLAN_ENTRY_HDR = new INSURANCE_PLAN_ENTRY_HDR();
        INSURANCE_PLAN_ENTRY_DTL iNSURANCE_PLAN_ENTRY_DTL = new INSURANCE_PLAN_ENTRY_DTL();
        DataTable dtGridData = new DataTable();
        Terms_BLL Terms_BLL = new Terms_BLL();
        Boolean bol_rowVisiable;
        Boolean savedBool;
        string ProReturn = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    Session["InvTaxDet"] = null;
                    AssignToControl();

                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

                private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));

            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }
            UserRightsChecking();

        }
        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }
        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>
        private void FillComboBox()
        {
           // ComboFilling.getCalendar(ref ddlfinyear);
            AccountingCalendar_BLL.GetCalAcctYear(ref ddlfinyear);
            //ComboFilling.fn_getDepartment(ref ddldept);
            Department_BLL.GetDepartmentName(ref ddldept);
        }
        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>
        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                //hf_Temp_RefNo.Value = "";
                Startup();
                FillComboBox();

                EntityData = null;
                Session[FINSessionConstants.GridData] = null;
                 iNSURANCE_PLAN_ENTRY_HDR = EmployeeInsurance_BLL.getClassEntity(Master.StrRecordId);

                 if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                 {

                     EntityData = iNSURANCE_PLAN_ENTRY_HDR;

                     ddlfinyear.SelectedValue = iNSURANCE_PLAN_ENTRY_HDR.FISCAL_YEAR.ToString();
                     ddldept.SelectedValue = iNSURANCE_PLAN_ENTRY_HDR.DEPT_ID.ToString();
                 }

                 dtGridData = DBMethod.ExecuteQuery(FIN.DAL.HR.InsurancePlan_DAL.GetPlanDetails(Master.StrRecordId.ToString(),ddlfinyear.SelectedValue.ToString())).Tables[0];
                 BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                //GridViewRow gvr = gvData.FooterRow;
                //FillFooterGridCombo(gvr);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountCodesEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        
        //private void FillFooterGridCombo(GridViewRow tmpgvr)
        //{
        //    try
        //    {
        //        //ErrorCollection.Clear();

        //        //DropDownList ddlEplan = tmpgvr.FindControl("ddleligplan") as DropDownList;
        //        //DropDownList ddlSplan = tmpgvr.FindControl("ddlselplan") as DropDownList;
                
        //        //// PurchaseItemReceipt_BLL.fn_getGRNNo(ref ddl_ReceiptNo);
        //        ////Lookup_BLL.GetLookUpValues(ref ddl_LineType, "LINETYPE");
        //        //ComboFilling.GetEligiblePlan(ref ddlEplan);
        //        //ComboFilling.GetSelectedPlan(ref ddlSplan);

        //        //if (gvData.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
        //        //{
        //        //    ddlEplan.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.ELIGIBLE_PLAN_ID].ToString();
        //        //    ddlSplan.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.SELECTED_PLAN_ID].ToString();

        //        //    //ddlAccCode.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.CODE_ID].ToString();
        //        //    //FillEntityNumber(tmpgvr);
        //        //    //ddl_ReceiptNo.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.RECEIPT_ID].ToString();
        //        //    //FillItemNo(tmpgvr);

        //        //    //dd_lItemNo.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.ITEM_ID].ToString();


        //        //}
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorCollection.Add("Org_FillFootGrid", ex.Message);
        //    }
        //    finally
        //    {
        //        if (ErrorCollection.Count > 0)
        //        {
        //            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
        //            // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
        //        }
        //    }
        //}
        

        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            if (Session[FINSessionConstants.GridData] != null)
            {
                dtGridData = (DataTable)Session[FINSessionConstants.GridData];
            }
            gvData.EditIndex = -1;

            BindGrid(dtGridData);

        }
        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                 //   drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                  //  TempDataSave(drList, "A");
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>


        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                //drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                //TempDataSave(drList, "U");
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountCodesEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Insurance Plan", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                //FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Insurance Plan", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Insurance Plan", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                    {
                        e.Row.Visible = false;
                    }
                    else
                    {

                        DropDownList ddlEplan = (DropDownList)e.Row.FindControl("ddleligplan") as DropDownList;
                        DropDownList ddlSplan = (DropDownList)e.Row.FindControl("ddlselplan") as DropDownList;

                        // PurchaseItemReceipt_BLL.fn_getGRNNo(ref ddl_ReceiptNo);
                        //Lookup_BLL.GetLookUpValues(ref ddl_LineType, "LINETYPE");
                        ComboFilling.GetEligiblePlan(ref ddlEplan);
                        ComboFilling.GetSelectedPlan(ref ddlSplan);

                        //ddlCourse.SelectedValue = gvData.DataKeys[e.Row.RowIndex].Values[FINColumnConstants.COURSE_ID].ToString();
                        ddlEplan.SelectedValue = gvData.DataKeys[e.Row.RowIndex].Values[FINColumnConstants.ELIGIBLE_PLAN_ID].ToString();
                        ddlSplan.SelectedValue = gvData.DataKeys[e.Row.RowIndex].Values[FINColumnConstants.SELECTED_PLAN_ID].ToString();
                    }

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }
                   
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        #endregion



        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                DataTable dtAcctfrmSupplier = new DataTable();
                DataTable dtAcctfrmSysopt = new DataTable();
                System.Collections.SortedList slControls = new System.Collections.SortedList();

                slControls[0] = ddlfinyear;
                slControls[1] = ddldept;
                
                ErrorCollection.Clear();
                string strCtrlTypes = "DropDownList~DropDownList";

                string strMessage = "Financial Year ~ Department ";

                EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

                if (EmptyErrorCollection.Count > 0)
                {
                    ErrorCollection = EmptyErrorCollection;
                    return;
                }

                ErrorCollection.Clear();


                
                AssignToBE();



                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        private void AssignToBE()
        {
            try
            {
                
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    iNSURANCE_PLAN_ENTRY_HDR = (INSURANCE_PLAN_ENTRY_HDR)EntityData;
                }

                iNSURANCE_PLAN_ENTRY_HDR.FISCAL_YEAR = ddlfinyear.SelectedValue;
                iNSURANCE_PLAN_ENTRY_HDR.DEPT_ID = ddldept.SelectedValue;


                iNSURANCE_PLAN_ENTRY_HDR.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                // iNSURANCE_PLAN_ENTRY_HDR.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;
                iNSURANCE_PLAN_ENTRY_HDR.ORG_ID = VMVServices.Web.Utils.OrganizationID;


                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    iNSURANCE_PLAN_ENTRY_HDR.MODIFIED_BY = this.LoggedUserName;
                    iNSURANCE_PLAN_ENTRY_HDR.MODIFIED_DATE = DateTime.Today;
                }
                else
                {
                    iNSURANCE_PLAN_ENTRY_HDR.PLAN_ENTRY_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_126_M.ToString(), false, true);
                    iNSURANCE_PLAN_ENTRY_HDR.CREATED_BY = this.LoggedUserName;
                    iNSURANCE_PLAN_ENTRY_HDR.CREATED_DATE = DateTime.Today;
                }
                iNSURANCE_PLAN_ENTRY_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, iNSURANCE_PLAN_ENTRY_HDR.PLAN_ENTRY_ID);

                iNSURANCE_PLAN_ENTRY_HDR.ORG_ID = VMVServices.Web.Utils.OrganizationID;
                

                //Save Detail Table


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                

                ErrorCollection = CommonUtils.IsEmptyGrid(dtGridData, "Employee Insurance Plan Details ");
                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                var tmpChildEntity = new List<Tuple<object, string>>();
                
                INSURANCE_PLAN_ENTRY_DTL iNSURANCE_PLAN_ENTRY_DTL = new INSURANCE_PLAN_ENTRY_DTL();

                for (int iLoop = 0; iLoop < gvData.Rows.Count; iLoop++)
                {
                    iNSURANCE_PLAN_ENTRY_DTL = new INSURANCE_PLAN_ENTRY_DTL();

                    if (gvData.DataKeys[iLoop].Values[FINColumnConstants.PLAN_ENTRY_DTL_ID].ToString() != "0" && gvData.DataKeys[iLoop].Values[FINColumnConstants.PLAN_ENTRY_DTL_ID].ToString() != string.Empty)
                    {
                        iNSURANCE_PLAN_ENTRY_DTL = EmployeeInsurance_BLL.getDetailClassEntity(dtGridData.Rows[iLoop][FINColumnConstants.PLAN_ENTRY_DTL_ID].ToString());
                    }

                    TextBox txttotal = (TextBox)gvData.Rows[iLoop].FindControl("txttotalcost");
                    TextBox txtcomp = (TextBox)gvData.Rows[iLoop].FindControl("txtcomcost");
                    TextBox txtemp = (TextBox)gvData.Rows[iLoop].FindControl("txtempcost");
                    TextBox txtfamily = (TextBox)gvData.Rows[iLoop].FindControl("txtfamily");
                    TextBox txttotalamt = (TextBox)gvData.Rows[iLoop].FindControl("txttotamt");
                    DropDownList ddleplan = (DropDownList)gvData.Rows[iLoop].FindControl("ddleligplan");
                    DropDownList ddlsplan = (DropDownList)gvData.Rows[iLoop].FindControl("ddlselplan");


                    iNSURANCE_PLAN_ENTRY_DTL.EMP_ID = gvData.DataKeys[iLoop].Values["EMP_ID"].ToString();
                    iNSURANCE_PLAN_ENTRY_DTL.ELIGIBLE_PLAN_ID = ddleplan.SelectedValue.ToString();
                    iNSURANCE_PLAN_ENTRY_DTL.SELECTED_PLAN_ID = ddlsplan.SelectedValue.ToString();

                    if (txttotal.Text != "0" && txttotal.Text != string.Empty)
                    {
                        iNSURANCE_PLAN_ENTRY_DTL.TOTAL_COST = int.Parse(txttotal.Text);
                    }
                    if (txtcomp.Text != "0" && txtcomp.Text != string.Empty)
                    {
                        iNSURANCE_PLAN_ENTRY_DTL.COMPANY_COST = int.Parse(txtcomp.Text);
                    }


                    iNSURANCE_PLAN_ENTRY_DTL.EMP_COST = int.Parse (txtemp.Text);
                    iNSURANCE_PLAN_ENTRY_DTL.FAMILY_MEMBER = int.Parse( txtfamily.Text);
                    iNSURANCE_PLAN_ENTRY_DTL.PLAN_ENTRY_ID = iNSURANCE_PLAN_ENTRY_HDR.PLAN_ENTRY_ID;
                    iNSURANCE_PLAN_ENTRY_DTL.TOTAL_AMOUNT = int.Parse( txttotalamt.Text);
                    // iNSURANCE_PLAN_ENTRY_DTL.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;
                    iNSURANCE_PLAN_ENTRY_DTL.ENABLED_FLAG = FINAppConstants.EnabledFlag;


                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        tmpChildEntity.Add(new Tuple<object, string>(iNSURANCE_PLAN_ENTRY_DTL, FINAppConstants.Delete));
                    }
                    else
                    {
                        if (gvData.DataKeys[iLoop].Values[FINColumnConstants.PLAN_ENTRY_DTL_ID].ToString() != "0" && gvData.DataKeys[iLoop].Values[FINColumnConstants.PLAN_ENTRY_DTL_ID].ToString() != string.Empty)
                        {
                            iNSURANCE_PLAN_ENTRY_DTL.MODIFIED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(iNSURANCE_PLAN_ENTRY_DTL, FINAppConstants.Update));
                        }
                        else
                        {
                            iNSURANCE_PLAN_ENTRY_DTL.PLAN_ENTRY_DTL_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_126_D.ToString(), false, true);
                            iNSURANCE_PLAN_ENTRY_DTL.CREATED_BY = this.LoggedUserName;
                            iNSURANCE_PLAN_ENTRY_DTL.CREATED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(iNSURANCE_PLAN_ENTRY_DTL, FINAppConstants.Add));
                        }
                    }
                    iNSURANCE_PLAN_ENTRY_DTL.WORKFLOW_COMPLETION_STATUS = "1";

                }
                
                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            CommonUtils.SavePCEntity<INSURANCE_PLAN_ENTRY_HDR, INSURANCE_PLAN_ENTRY_DTL>(iNSURANCE_PLAN_ENTRY_HDR, tmpChildEntity, iNSURANCE_PLAN_ENTRY_DTL);
                            savedBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {
                            CommonUtils.SavePCEntity<INSURANCE_PLAN_ENTRY_HDR, INSURANCE_PLAN_ENTRY_DTL>(iNSURANCE_PLAN_ENTRY_HDR, tmpChildEntity, iNSURANCE_PLAN_ENTRY_DTL, true);
                            savedBool = true;
                            break;
                        }
                  }

               
                
                if (ErrorCollection.Count > 0)
                {
                    return;
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Insurance Plan ", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

       


       

        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {

            try
            {
                ErrorCollection.Clear();
                // DBMethod.DeleteEntity<iNSURANCE_PLAN_ENTRY_HDR>(iNSURANCE_PLAN_ENTRY_HDR);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            } 
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        #endregion

        protected void ddldept_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtgrddata = new DataTable();
            dtgrddata = DBMethod.ExecuteQuery (FIN.DAL.HR.InsurancePlan_DAL.GetStudentfmDept(ddldept.SelectedValue.ToString())).Tables[0];
            if (dtgrddata.Rows.Count > 0 )
            {
                BindGrid(dtgrddata);
               // GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
                //FillFooterGridCombo(gvr);
            }
        }

        protected void ddleligplan_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            DropDownList ddlEplan = gvr.FindControl("ddleligplan") as DropDownList;
            DropDownList ddlSplan = gvr.FindControl("ddlselplan") as DropDownList;
            TextBox txttotal = gvr.FindControl("txttotalcost") as TextBox;
            TextBox txtcomp = gvr.FindControl("txtcomcost") as TextBox;
            TextBox txtemp = gvr.FindControl("txtempcost") as TextBox;

            ddlSplan.SelectedValue = ddlEplan.SelectedValue;
            DataTable dtdtls = new DataTable();
            dtdtls = DBMethod.ExecuteQuery(FIN.DAL.HR.InsurancePlan_DAL.GetPlanAmountDtls(ddlEplan.SelectedValue)).Tables[0];
            if (dtdtls.Rows.Count > 0)
            {
                txttotal.Text = dtdtls.Rows[0]["TOTAL_COST"].ToString();
                txtcomp.Text = dtdtls.Rows[0]["COMPANY_COST"].ToString();
                txtemp.Text = dtdtls.Rows[0]["EMP_COST"].ToString();
            }
        }

        protected void ddlselplan_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            DropDownList ddlEplan = gvr.FindControl("ddleligplan") as DropDownList;
            DropDownList ddlSplan = gvr.FindControl("ddlselplan") as DropDownList;
            TextBox txttotal = gvr.FindControl("txttotalcost") as TextBox;
            TextBox txtcomp = gvr.FindControl("txtcomcost") as TextBox;
            TextBox txtemp = gvr.FindControl("txtempcost") as TextBox;

            //ddlSplan.SelectedValue = ddlEplan.SelectedValue;
            DataTable dtdtls = new DataTable();
            if (ddlSplan.SelectedValue == ddlEplan.SelectedValue)
            {
                dtdtls = DBMethod.ExecuteQuery(FIN.DAL.HR.InsurancePlan_DAL.GetPlanAmountDtls(ddlEplan.SelectedValue)).Tables[0];
                if (dtdtls.Rows.Count > 0)
                {
                    txttotal.Text = dtdtls.Rows[0]["TOTAL_COST"].ToString();
                    txtcomp.Text = dtdtls.Rows[0]["COMPANY_COST"].ToString();
                    txtemp.Text = dtdtls.Rows[0]["EMP_COST"].ToString();
                }
            }
            else
            {
                dtdtls = DBMethod.ExecuteQuery(FIN.DAL.HR.InsurancePlan_DAL.GetPlanAmountDtls(ddlSplan.SelectedValue)).Tables[0];
                if (dtdtls.Rows.Count > 0)
                {
                    txttotal.Text = dtdtls.Rows[0]["TOTAL_COST"].ToString();
                    txtcomp.Text = dtdtls.Rows[0]["COMPANY_COST"].ToString();
                    txtemp.Text = dtdtls.Rows[0]["EMP_COST"].ToString();
                }
            }
        }

    }
}