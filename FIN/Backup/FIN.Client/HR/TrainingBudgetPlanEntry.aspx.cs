﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.BLL.AP;
using FIN.BLL.GL;
using FIN.DAL;
using FIN.BLL.SSM;
using FIN.DAL.HR;
using FIN.DAL.SSM;
using FIN.DAL.GL;
using FIN.BLL;
using FIN.BLL.HR;
using VMVServices.Web;


namespace FIN.Client.HR
{
    public partial class TrainingBudgetPlanEntry : PageBase
    {
        HR_TRAINING_BUDGET_PLAN hR_TRAINING_BUDGET_PLAN = new HR_TRAINING_BUDGET_PLAN();
        System.Collections.SortedList slControls = new System.Collections.SortedList();
        Boolean bol_rowVisiable;
        HR_TRAINING_BUDGET_PLAN_HDR HR_TRAINING_BUDGET_PLAN_HDR = new HR_TRAINING_BUDGET_PLAN_HDR();
        DataTable dtGridData = new DataTable();
        bool savedBool;


        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();
                    //TrainingDate.Visible = false;

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                //pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                //pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";

            }

            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
            }

            if (VMVServices.Web.Utils.AddAllowed != FINAppConstants.Y && Master.Mode == FINAppConstants.Add)
            {
                btnSave.Visible = false;
            }

        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();

                FillComboBox();

                EntityData = null;
                Session[FINSessionConstants.GridData] = null;

                dtGridData = DBMethod.ExecuteQuery(TrainingBudgetPlan_DAL.GetTrainingBudgetPlan(Master.StrRecordId)).Tables[0];
                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    using (IRepository<HR_TRAINING_BUDGET_PLAN_HDR> userCtx = new DataRepository<HR_TRAINING_BUDGET_PLAN_HDR>())
                    {
                        HR_TRAINING_BUDGET_PLAN_HDR = userCtx.Find(r =>
                            (r.TRN_BUDGET_PLAN_HDR_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = HR_TRAINING_BUDGET_PLAN_HDR;
                    ddlAppraisal.SelectedValue = HR_TRAINING_BUDGET_PLAN_HDR.ASSIGN_HDR_ID;
                }

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AppTraining_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {
            System.Collections.SortedList slControls = new System.Collections.SortedList();

            DropDownList ddlCourseName = gvr.FindControl("ddlCourseName") as DropDownList;
            DropDownList ddlQuarter = gvr.FindControl("ddlQuarter") as DropDownList;
            DropDownList ddlTypeOfTraings = gvr.FindControl("ddlTypeOfTraings") as DropDownList;
            TextBox txtOrganiserName = gvr.FindControl("txtOrganiserName") as TextBox;
            TextBox txtExtensionCost = gvr.FindControl("txtExtensionCost") as TextBox;
            TextBox txtPeriods = gvr.FindControl("txtPeriods") as TextBox;
            TextBox txtNoOfPeople = gvr.FindControl("txtNoOfPeople") as TextBox;
            TextBox txtCoveredByKFAS = gvr.FindControl("txtCoveredByKFAS") as TextBox;

            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));

            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList[FINColumnConstants.APPRAISAL_TRAINING_ID] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);
            }

            ErrorCollection.Clear();

            string strCtrlTypes = string.Empty;
            string strMessage = string.Empty;

            slControls[0] = ddlCourseName;
            slControls[1] = ddlQuarter;
            slControls[2] = ddlTypeOfTraings;
            slControls[3] = txtOrganiserName;
            slControls[4] = txtExtensionCost;
            slControls[5] = txtPeriods;
            slControls[6] = txtNoOfPeople;
            slControls[7] = txtCoveredByKFAS;


            strCtrlTypes = FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX;
            strMessage = Prop_File_Data["Course_Details_P"] + " ~ " + Prop_File_Data["Organiser_Name_P"] + " ~ " + Prop_File_Data["Quarter_P"] + " ~ " + Prop_File_Data["Extension_Cost_P"] + " ~ " + Prop_File_Data["No_of_People_P"] + " ~ " + Prop_File_Data["Periods(Hrs/Days)_P"] + " ~ " + Prop_File_Data["Type_of_Trainings_P"] + " ~ " + Prop_File_Data["Covered_By_KFAS_P"] + "";

            ErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);
            if (ErrorCollection.Count > 0)
                return drList;


            string strCondition = "COURSE_ID='" + ddlCourseName.SelectedValue.ToString() + "'";
            strMessage = FINMessageConstatns.RecordAlreadyExists;
            ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);

            if (ErrorCollection.Count > 0)
            {
                return drList;
            }

            //drList[FINColumnConstants.EMP_ID] = ddlEmpl.SelectedValue;
            //drList[FINColumnConstants.EMP_NAME] = ddlEmpl.SelectedItem.Text;
            //drList[FINColumnConstants.COURSE_ID] = ddlCourse.SelectedValue;
            //drList[FINColumnConstants.COURSE_DESC] = ddlCourse.SelectedItem.Text;
            //drList[FINColumnConstants.COMMENTS] = txtComments.Text;

            //drList[FINColumnConstants.DELETED] = FINAppConstants.N;


            return drList;



        }
        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                    {
                        e.Row.Visible = false;
                    }
                    else
                    {
                        DropDownList ddlCourseName = (DropDownList)e.Row.FindControl("ddlCourseName");
                        DropDownList ddlQuarter = (DropDownList)e.Row.FindControl("ddlQuarter");
                        DropDownList ddlTypeOfTraings = (DropDownList)e.Row.FindControl("ddlTypeOfTraings");

                        FIN.BLL.HR.Course_BLL.GetTriningCourse(ref ddlCourseName, ddlAppraisal.SelectedValue.ToString());
                        FIN.BLL.Lookup_BLL.GetLookUpValues(ref ddlQuarter, "IAS");
                        FIN.BLL.Lookup_BLL.GetLookUpValues(ref ddlTypeOfTraings, "TRAIN_TYPE");

                        ddlQuarter.SelectedValue = gvData.DataKeys[e.Row.RowIndex].Values["quarter_id"].ToString();
                        ddlTypeOfTraings.SelectedValue = gvData.DataKeys[e.Row.RowIndex].Values["rating_id"].ToString();
                        ddlCourseName.SelectedValue = gvData.DataKeys[e.Row.RowIndex].Values[FINColumnConstants.COURSE_ID].ToString();

                    }
                    //if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    //{
                    //    e.Row.Visible = false;
                    //}
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AppTraining_RDB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>

        private void FillComboBox()
        {
           
            FIN.BLL.HR.AppraisalDefineKRAEntry_BLL.GetAppraisalDetails(ref ddlAppraisal);

        }
        protected void ddlAppraisal_SelectedIndexChanged(object sender, EventArgs e)
        {
            dtGridData = DBMethod.ExecuteQuery(TrainingBudgetPlan_DAL.GetTrainingEmployee(ddlAppraisal.SelectedValue.ToString())).Tables[0];
            BindGrid(dtGridData);
        }
        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();

                if (EntityData != null)
                {
                    HR_TRAINING_BUDGET_PLAN_HDR = (HR_TRAINING_BUDGET_PLAN_HDR)EntityData;
                }

                HR_TRAINING_BUDGET_PLAN_HDR.ENABLED_FLAG = "1";
                HR_TRAINING_BUDGET_PLAN_HDR.ORG_ID = VMVServices.Web.Utils.OrganizationID;
                HR_TRAINING_BUDGET_PLAN_HDR.ASSIGN_HDR_ID = ddlAppraisal.SelectedValue;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    HR_TRAINING_BUDGET_PLAN_HDR.MODIFIED_BY = this.LoggedUserName;
                    HR_TRAINING_BUDGET_PLAN_HDR.MODIFIED_DATE = DateTime.Today;
                }
                else
                {
                    HR_TRAINING_BUDGET_PLAN_HDR.TRN_BUDGET_PLAN_HDR_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_109_M.ToString(), false, true);
                    HR_TRAINING_BUDGET_PLAN_HDR.CREATED_BY = this.LoggedUserName;
                    HR_TRAINING_BUDGET_PLAN_HDR.CREATED_DATE = DateTime.Today;
                }

                HR_TRAINING_BUDGET_PLAN_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, HR_TRAINING_BUDGET_PLAN_HDR.TRN_BUDGET_PLAN_HDR_ID);

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                var tmpChildEntity = new List<Tuple<object, string>>();

                if (gvData.Rows.Count > 0)
                {
                    for (int iLoop = 0; iLoop < gvData.Rows.Count; iLoop++)
                    {
                        hR_TRAINING_BUDGET_PLAN = new HR_TRAINING_BUDGET_PLAN();

                        if (gvData.DataKeys[iLoop].Values["TRN_BUDGET_PLAN_ID"].ToString() != "0" && gvData.DataKeys[iLoop].Values["TRN_BUDGET_PLAN_ID"].ToString() != string.Empty)
                        {
                            using (IRepository<HR_TRAINING_BUDGET_PLAN> userCtx = new DataRepository<HR_TRAINING_BUDGET_PLAN>())
                            {
                                hR_TRAINING_BUDGET_PLAN = userCtx.Find(r =>
                                    (r.TRN_BUDGET_PLAN_ID == gvData.DataKeys[iLoop].Values["TRN_BUDGET_PLAN_ID"].ToString())
                                    ).SingleOrDefault();
                            }
                        }
                        hR_TRAINING_BUDGET_PLAN.TRN_BUDGET_PLAN_HDR_ID = HR_TRAINING_BUDGET_PLAN_HDR.TRN_BUDGET_PLAN_HDR_ID;

                        TextBox txtOrganiserName = (TextBox)gvData.Rows[iLoop].FindControl("txtOrganiserName");
                        TextBox txtExtensionCost = (TextBox)gvData.Rows[iLoop].FindControl("txtExtensionCost");
                        TextBox txtNoOfPeople = (TextBox)gvData.Rows[iLoop].FindControl("txtNoOfPeople");
                        TextBox txtPeriods = (TextBox)gvData.Rows[iLoop].FindControl("txtPeriods");
                        TextBox txtCoveredByKFAS = (TextBox)gvData.Rows[iLoop].FindControl("txtCoveredByKFAS");

                        DropDownList ddlCourseName = (DropDownList)gvData.Rows[iLoop].FindControl("ddlCourseName");
                        DropDownList ddlQuarter = (DropDownList)gvData.Rows[iLoop].FindControl("ddlQuarter");
                        DropDownList ddlTypeOfTraings = (DropDownList)gvData.Rows[iLoop].FindControl("ddlTypeOfTraings");


                        hR_TRAINING_BUDGET_PLAN.TRN_COURSE_ID = ddlCourseName.SelectedValue;
                        hR_TRAINING_BUDGET_PLAN.TRN_ORGANISER = txtOrganiserName.Text;
                        hR_TRAINING_BUDGET_PLAN.TRN_QUARTER = ddlQuarter.SelectedValue;
                        if (txtExtensionCost.Text != string.Empty)
                        {
                            hR_TRAINING_BUDGET_PLAN.TRN_ESTIMATION_CST =CommonUtils.ConvertStringToDecimal(txtExtensionCost.Text.ToString());
                        }
                        if (txtPeriods.Text != string.Empty)
                        {
                            hR_TRAINING_BUDGET_PLAN.TRN_PERIODS = int.Parse(txtPeriods.Text.ToString());
                        }
                        if (txtNoOfPeople.Text != string.Empty)
                        {
                            hR_TRAINING_BUDGET_PLAN.TRN_NO_OF_PEOPLES = int.Parse(txtNoOfPeople.Text.ToString());
                        }
                        hR_TRAINING_BUDGET_PLAN.TRN_COURSE_ID = ddlCourseName.SelectedValue;
                        hR_TRAINING_BUDGET_PLAN.TRN_EMPLOYEE_ID = gvData.DataKeys[iLoop].Values["EMP_ID"].ToString();
                        hR_TRAINING_BUDGET_PLAN.TRN_COVERED_BY_KFAS = CommonUtils.ConvertStringToDecimal(txtCoveredByKFAS.Text);
                        hR_TRAINING_BUDGET_PLAN.TRN_TRAINING_TYPE = ddlTypeOfTraings.SelectedValue;
                        hR_TRAINING_BUDGET_PLAN.TRN_ORG_ID = VMVServices.Web.Utils.OrganizationID;

                        hR_TRAINING_BUDGET_PLAN.TRN_EMPLOYEE_ID = gvData.DataKeys[iLoop].Values["EMP_ID"].ToString();

                        hR_TRAINING_BUDGET_PLAN.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                        hR_TRAINING_BUDGET_PLAN.TRN_ORG_ID = VMVServices.Web.Utils.OrganizationID;


                        if (gvData.DataKeys[iLoop].Values["TRN_BUDGET_PLAN_ID"].ToString() != "0" && gvData.DataKeys[iLoop].Values["TRN_BUDGET_PLAN_ID"].ToString() != string.Empty)
                        {
                            hR_TRAINING_BUDGET_PLAN.TRN_BUDGET_PLAN_ID = gvData.DataKeys[iLoop].Values["TRN_BUDGET_PLAN_ID"].ToString();
                            hR_TRAINING_BUDGET_PLAN.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_TRAINING_BUDGET_PLAN.TRN_BUDGET_PLAN_ID);
                            hR_TRAINING_BUDGET_PLAN.MODIFIED_BY = this.LoggedUserName;
                            hR_TRAINING_BUDGET_PLAN.MODIFIED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(hR_TRAINING_BUDGET_PLAN, FINAppConstants.Update));
                        }
                        else
                        {

                            hR_TRAINING_BUDGET_PLAN.TRN_BUDGET_PLAN_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_109.ToString(), false, true);
                            hR_TRAINING_BUDGET_PLAN.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_TRAINING_BUDGET_PLAN.TRN_BUDGET_PLAN_ID);
                            hR_TRAINING_BUDGET_PLAN.CREATED_BY = this.LoggedUserName;
                            hR_TRAINING_BUDGET_PLAN.CREATED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(hR_TRAINING_BUDGET_PLAN, FINAppConstants.Add));
                        }

                    }
                }




                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            CommonUtils.SavePCEntity<HR_TRAINING_BUDGET_PLAN_HDR, HR_TRAINING_BUDGET_PLAN>(HR_TRAINING_BUDGET_PLAN_HDR, tmpChildEntity, hR_TRAINING_BUDGET_PLAN);
                            savedBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {
                            CommonUtils.SavePCEntity<HR_TRAINING_BUDGET_PLAN_HDR, HR_TRAINING_BUDGET_PLAN>(HR_TRAINING_BUDGET_PLAN_HDR, tmpChildEntity, hR_TRAINING_BUDGET_PLAN, true);
                            savedBool = true;
                            break;
                        }
                }
                VMVServices.Web.Utils.SavedRecordId = HR_TRAINING_BUDGET_PLAN_HDR.TRN_BUDGET_PLAN_HDR_ID;

                if (ErrorCollection.Count > 0)
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                AssignToBE();


                //emptyvalid();
                if (ErrorCollection.Count > 0)
                {
                    return;
                }


                // Duplicate Validation Through Backend Package PKG_VALIDATIONS



                //ProReturn = FIN.DAL.AP.Item_DAL.GetSPFOR_DUPLICATE_CHECK(iNV_ITEM_MASTER.ITEM_CODE,txtItemName.Text, iNV_ITEM_MASTER.ORG_ID, iNV_ITEM_MASTER.ITEM_ID);

                //if (ProReturn != string.Empty)
                //{
                //    if (ProReturn != "0")
                //    {
                //        ErrorCollection.Add("ITEMNAME", ProReturn);
                //        if (ErrorCollection.Count > 0)
                //        {
                //            return;
                //        }
                //    }
                //}


                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED,true);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion




        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                DBMethod.DeleteEntity<HR_TRAINING_BUDGET_PLAN>(hR_TRAINING_BUDGET_PLAN);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }



        protected void btnCancel_Click(object sender, EventArgs e)
        {

        }


        //private void showDate()
        //{
        //    TrainingDate.Visible = false;

        //    if (ddlFiscalYear.Enabled == true)
        //    {
        //        TrainingDate.Visible = true;

        //    }


        //}







        //private void emptyvalid()
        //{
        //    if (hR_VACANCIES.VAC_DEPT_ID == null)
        //    {

        //        if (ddlDepartment.SelectedValue == string.Empty)
        //        {
        //            ErrorCollection.Add("Department", "Department Cannot be empty");
        //        }

        //    }
        //    if (hR_VACANCIES.VAC_DESIG_ID == null)
        //    {
        //        if (ddlDesignation.SelectedValue == string.Empty)

        //            ErrorCollection.Add("Designation", "Designation Cannot be empty");
        //        }
        //    }
        //    //if (hR_VACANCIES.VAC_TYPE > 0)
        //    //{

        //    //    if (ddlType.SelectedValue == string.Empty)
        //    //    {
        //    //        ErrorCollection.Add("DepartmentType", "Department Type Cannot be empty");
        //    //    }
        //    //}

        //    //if  > 0)
        //    //{
        //    //    if (ddlVolumeUOM.SelectedValue == string.Empty)
        //    //    {

        //    //        ErrorCollection.Add("VOLEMT", "Volume UOM Cannot be empty");
        //    //    }
        //    //}

        //}






    }





}



