﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="LeaveProvisionEntry.aspx.cs" Inherits="FIN.Client.HR.LeaveProvisionEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 800px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 150px" id="lblCalendarName">
                Payroll Period
            </div>
            <div class="divtxtBox LNOrient" style="  width: 400px">
                <asp:DropDownList ID="ddlPayrollPeriod" Width="400px" runat="server" CssClass="ddlStype validate[required] RequiredField "
                    TabIndex="1" AutoPostBack="True">
                </asp:DropDownList>
            </div>
            <div class="lblBox LNOrient" style="  width: 100px" id="lblEffectiveDate">
                <asp:ImageButton ID="btnView" runat="server" ImageUrl="~/Images/view.png" OnClick="btnView_Click"
                    Style="border: 0px;" />
                <%--<asp:Button ID="btnView" runat="server" Text="View" CssClass="btn" OnClick="btnView_Click" />--%>
            </div>
            <div class="lblBox LNOrient" style="  width: 100px" id="Div1">
                <asp:ImageButton ID="btnLeaveProvision" runat="server" ImageUrl="~/Images/print.png"
                    Visible="False" OnClick="btnLeaveProvision_Click" Style="border: 0px;" />
                <%--<asp:Button ID="btnLeaveProvision" runat="server" Text="Print" CssClass="btn" OnClick="btnLeaveProvision_Click"
                    Visible="false" />--%>
            </div>
        </div>
        <div class="divClear_10">
            <asp:HiddenField ID="hfDate" runat="server" />
        </div>
        <div class="LNOrient">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="DisplayFont  Grid"
                DataKeyNames="" ShowFooter="true" EmptyDataText="No Record to Found" Width="100%">
                <Columns>
                    <asp:BoundField DataField="EMP_NO" HeaderText="Employee Number" />
                    <asp:BoundField DataField="EMPLOYEE_NAME" HeaderText="Employee Name" />
                    <asp:BoundField DataField="leave_eligible_days" HeaderText="Accural Days" ItemStyle-HorizontalAlign="Right" />
                    <asp:BoundField DataField="leave_provision_amt" HeaderText="Leave Provision Amount"
                        ItemStyle-HorizontalAlign="Right" />
                    <asp:BoundField DataField="leave_bal" HeaderText="Leave Balance" ItemStyle-HorizontalAlign="Right" />
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divRowContainer divAction" style="display: none">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" OnClick="btnSave_Click1"
                            TabIndex="16" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="17" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="18" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="19" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/PER/PAChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
