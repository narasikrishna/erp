﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.BLL.AP;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.BLL;
using VMVServices.Web;
using VMVServices.Services.Data;
namespace FIN.Client.HR
{
    public partial class ExitinterviewQuestionsEntry : PageBase
    {

        HR_EXIT_INTERVIEW_QUESTIONS hR_EXIT_INTERVIEW_QUESTIONS = new HR_EXIT_INTERVIEW_QUESTIONS();


        DataTable dtGridData = new DataTable();
        Boolean savedBool;
        Boolean bol_rowVisiable;

        System.Collections.SortedList slControls = new System.Collections.SortedList();
        //string ProReturn = null;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));

            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";

            }

            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
            }

            if (VMVServices.Web.Utils.AddAllowed != FINAppConstants.Y && Master.Mode == FINAppConstants.Add)
            {
                btnSave.Visible = false;
            }

        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();


                Startup();
                FillComboBox();

                
                EntityData = null;


                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    using (IRepository<HR_EXIT_INTERVIEW_QUESTIONS> userCtx = new DataRepository<HR_EXIT_INTERVIEW_QUESTIONS>())
                    {
                        hR_EXIT_INTERVIEW_QUESTIONS = userCtx.Find(r =>
                            (r.EIQ_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = hR_EXIT_INTERVIEW_QUESTIONS;
                   


                    ddlQuestiongroup.SelectedValue = hR_EXIT_INTERVIEW_QUESTIONS.QUESTION_GROUP;

                    txtQusName.Text = hR_EXIT_INTERVIEW_QUESTIONS.QUESTION_NAME;

                    if (hR_EXIT_INTERVIEW_QUESTIONS.ENABLED_FLAG == "1")
                    {
                        chkActive.Checked = true;
                    }
                    else
                    {
                        chkActive.Checked = false;
                    }

                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>

        private void FillComboBox()
        {

            Lookup_BLL.GetLookUpValues(ref ddlQuestiongroup, "QUS_GRP");
        }



        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    hR_EXIT_INTERVIEW_QUESTIONS = (HR_EXIT_INTERVIEW_QUESTIONS)EntityData;
                }


                hR_EXIT_INTERVIEW_QUESTIONS.QUESTION_GROUP = ddlQuestiongroup.SelectedValue;
                hR_EXIT_INTERVIEW_QUESTIONS.QUESTION_NAME = txtQusName.Text;

                hR_EXIT_INTERVIEW_QUESTIONS.ORG_ID = VMVServices.Web.Utils.OrganizationID;

                //hR_EXIT_INTERVIEW_QUESTIONS.ENABLED_FLAG = FINAppConstants.EnabledFlag;

                if (chkActive.Checked == true)
                {
                    hR_EXIT_INTERVIEW_QUESTIONS.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                }
                else
                {
                    hR_EXIT_INTERVIEW_QUESTIONS.ENABLED_FLAG = FINAppConstants.DisabledFlag;
                }

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    hR_EXIT_INTERVIEW_QUESTIONS.MODIFIED_BY = this.LoggedUserName;
                    hR_EXIT_INTERVIEW_QUESTIONS.MODIFIED_DATE = DateTime.Today;

                }
                else
                {
                    hR_EXIT_INTERVIEW_QUESTIONS.EIQ_ID = FINSP.GetSPFOR_SEQCode("HR_117".ToString(), false, true);

                    hR_EXIT_INTERVIEW_QUESTIONS.CREATED_BY = this.LoggedUserName;
                    hR_EXIT_INTERVIEW_QUESTIONS.CREATED_DATE = DateTime.Today;

                }

                hR_EXIT_INTERVIEW_QUESTIONS.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_EXIT_INTERVIEW_QUESTIONS.EIQ_ID);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }




        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                AssignToBE();



                AssignToBE();



                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            DBMethod.SaveEntity<HR_EXIT_INTERVIEW_QUESTIONS>(hR_EXIT_INTERVIEW_QUESTIONS);

                            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                            break;
                        }
                    case FINAppConstants.Update:
                        {

                            DBMethod.SaveEntity<HR_EXIT_INTERVIEW_QUESTIONS>(hR_EXIT_INTERVIEW_QUESTIONS, true);

                            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                            break;

                        }
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion




        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                DBMethod.DeleteEntity<HR_EXIT_INTERVIEW_QUESTIONS>(hR_EXIT_INTERVIEW_QUESTIONS);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

      
        

    }
}