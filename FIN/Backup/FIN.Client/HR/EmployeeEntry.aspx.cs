﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.BLL;
using FIN.DAL.HR;
using FIN.BLL.HR;
using VMVServices.Web;
namespace FIN.Client.HR
{
    public partial class EmployeeEntry : PageBase
    {

        HR_EMPLOYEES hR_EMPLOYEES = new HR_EMPLOYEES();
       
        
        Boolean bol_WorkDet = false;
        Boolean bol_rowVisiable = false;
        Boolean save_data = false;
        string ProReturn = null;
        DataTable dt_SalaryDet;
        DataTable dt_SalaryEntry;
        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    Session["EmpAddress"] = null;
                    Session["EmpWorkDetails"] = null;
                    Session[FINSessionConstants.Attachments] = null;
                    AssignToControl();
                    EntryLoadHeader();
                    ChangeLanguage();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();



                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;

        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                //btnDelete.Visible = true;
                //pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                //btnDelete.Visible = false;
                //pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();

        }

        private void ChangeLanguage()
        {
            try
            {
                ErrorCollection.Clear();
                if (VMVServices.Web.Utils.LanguageCode.ToString().Length > 0)
                {
                    Dictionary<string, string> Prop_File_Data;
                    Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));
                    lblAddSaved.Text = Prop_File_Data["Address_P"];
                    lblWorDetHeader.Text = Prop_File_Data["WorkDetails_P"];
                    lblContSave.Text = Prop_File_Data["ContactDetails_P"];
                    lblQualDet.Text = Prop_File_Data["QualificationDetails_P"];
                    lblSkillDet.Text = Prop_File_Data["SkillDetails_P"];
                    lblEmpbankhe.Text = Prop_File_Data["BankDetails_P"];
                    lblPPdetHe.Text = Prop_File_Data["DocumentDetails_P"];
                    lblSelfServiceDtls.Text = Prop_File_Data["SelfServiceDetails_P"];
                    lblLeaveDtls.Text = Prop_File_Data["LeaveDetails_P"];
                    lblSalaryDtls.Text = Prop_File_Data["SalaryDetails_P"];
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("HREmpListChangeLang", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    // btnDelete.Visible = false;
                }
            }
        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>
        private void setImgbtnEnable(Boolean bol_value)
        {
            imgbtnAddress.Enabled = bol_value;
            imgBtnWorkDetails.Enabled = bol_value;
            imgBtnContDet.Enabled = bol_value;
            imgbtnQualDet.Enabled = bol_value;
            imgbtnSkillDet.Enabled = bol_value;
            imgbtnBankDetails.Enabled = bol_value;
            imgbtnPPDet.Enabled = bol_value;
            imgbtnPasswordDet.Enabled = bol_value;
            imbbtnLeaveDetails.Enabled = bol_value;
            imgbtnSalaryDetails.Enabled = bol_value;
        }
        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
                Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
                Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

                Startup();
                FillComboBox();

                EntityData = null;
                divImgbtnEmpDet.Visible = true;
                setImgbtnEnable(false);
                txtEmpNo.Text = Employee_BLL.getMaxEmpNo();
                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    setImgbtnEnable(true);
                    //pnlAddress.Visible = true;
                    div_MDData.Visible = true;
                    div_MHData.Visible = true;
                    using (IRepository<HR_EMPLOYEES> userCtx = new DataRepository<HR_EMPLOYEES>())
                    {
                        hR_EMPLOYEES = userCtx.Find(r =>
                            (r.EMP_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = hR_EMPLOYEES;

                    if (hR_EMPLOYEES.APP_ID != null)
                    {
                        ddlApplicantID.SelectedValue = hR_EMPLOYEES.APP_ID;
                    }

                    txtEmpNo.Text = hR_EMPLOYEES.EMP_NO;
                    txtFirstName.Text = hR_EMPLOYEES.EMP_FIRST_NAME;
                    txtMiddleName.Text = hR_EMPLOYEES.EMP_MIDDLE_NAME;
                    txtLastName.Text = hR_EMPLOYEES.EMP_LAST_NAME;
                    txtFirstNameol.Text = hR_EMPLOYEES.EMP_FIRST_NAME_OL;
                    txtMiddleNameol.Text = hR_EMPLOYEES.EMP_MIDDLE_NAME_OL;
                    txtLastNameol.Text = hR_EMPLOYEES.EMP_LAST_NAME_OL;
                    txtDateofBirth.Text = DBMethod.ConvertDateToString(hR_EMPLOYEES.EMP_DOB.ToString());
                   
                    txtDoj.Text = DBMethod.ConvertDateToString(hR_EMPLOYEES.EMP_DOJ.ToString());

                    if (hR_EMPLOYEES.EMP_ACCOUNT_CODE != null)
                    {
                        ddlglAccCode.SelectedValue = hR_EMPLOYEES.EMP_ACCOUNT_CODE;
                    }
                    
                    if (hR_EMPLOYEES.INDEM_ID != null)
                    {
                        ddlIndemnity.SelectedValue = hR_EMPLOYEES.INDEM_ID;
                    }

                    if (hR_EMPLOYEES.ATTRIBUTE3 != null)
                    {
                        txtCompanyname.Text = hR_EMPLOYEES.ATTRIBUTE3;
                    }
                    if (hR_EMPLOYEES.EMP_INTERNAL_EXTERNAL != null)
                    {
                        ddlInternalExternale.SelectedValue = hR_EMPLOYEES.EMP_INTERNAL_EXTERNAL.ToString();
                    }
                    if (hR_EMPLOYEES.EMP_TYPE != null)
                    {
                        ddlEmploymentType.SelectedValue = hR_EMPLOYEES.EMP_TYPE.ToString();
                    }
                    if (hR_EMPLOYEES.EMP_MARITAL_STATUS != null)
                    {
                        ddlMaritalStatus.SelectedValue = hR_EMPLOYEES.EMP_MARITAL_STATUS.ToString();
                    }
                    if (hR_EMPLOYEES.EMP_BLOOD_GROUP != null)
                    {
                        ddlBlodGroup.SelectedValue = hR_EMPLOYEES.EMP_BLOOD_GROUP.ToString();
                    }
                    if (hR_EMPLOYEES.EMP_RELIGION != null)
                    {
                        ddlReligion.SelectedValue = hR_EMPLOYEES.EMP_RELIGION.ToString();
                    }
                    if (hR_EMPLOYEES.EMP_EMAIL != null)
                    {
                        txtCompEmail.Text = hR_EMPLOYEES.EMP_EMAIL.ToString();
                    }

                    RBG.SelectedValue = hR_EMPLOYEES.EMP_GENDER;
                    txtPlaceofbirth.Text = hR_EMPLOYEES.PLACE_OF_BIRTH;
                    if (hR_EMPLOYEES.MARRIAGE_DATE != null)
                    {
                        txtMarriagedate.Text = DBMethod.ConvertDateToString(hR_EMPLOYEES.MARRIAGE_DATE.ToString());
                    }
                    chkProbation.Checked = false;
                    if (hR_EMPLOYEES.PROB_FLAG != null)
                    {
                        if (hR_EMPLOYEES.PROB_FLAG == FINAppConstants.Y)
                        {
                            chkProbation.Checked = true;
                        }
                    }
                    txtNoofDependents.Text = hR_EMPLOYEES.NUMBER_OF_DEPENDENTS.ToString();
                    ddlTitle.SelectedValue = hR_EMPLOYEES.TITLE;
                    ddlNationality.SelectedValue = hR_EMPLOYEES.NATIONALITY;
                    txtNoticePeriod.Text = hR_EMPLOYEES.NOTICE_PERIOD.ToString();
                    if (hR_EMPLOYEES.NOTICE_PERIOD_DATE != null)
                    {
                        txtNoticePeridDate.Text = DBMethod.ConvertDateToString(hR_EMPLOYEES.NOTICE_PERIOD_DATE.ToString());
                    }
                    if (hR_EMPLOYEES.CONFIRMATION_DATE != null)
                    {
                        txtConfDate.Text = DBMethod.ConvertDateToString(hR_EMPLOYEES.CONFIRMATION_DATE.ToString());
                    }
                    if (hR_EMPLOYEES.TERMINATION_DATE != null)
                    {
                        txtResignationDate.Text = DBMethod.ConvertDateToString(hR_EMPLOYEES.TERMINATION_DATE.ToString());
                    }
                    ssdFlagVisable();
                    if (hR_EMPLOYEES.SSD_FLAG != null)
                    {
                        if (hR_EMPLOYEES.SSD_FLAG == FINAppConstants.Y)
                        {
                            chkSSDFlag.Checked = true;
                        }
                        else
                        {
                            chkSSDFlag.Checked = false;
                        }
                    }
                    if (hR_EMPLOYEES.STOP_PAYTOLL != null)
                    {
                        if (hR_EMPLOYEES.STOP_PAYTOLL == FINAppConstants.Y)
                        {
                            chkStopPayroll.Checked = true;
                        }
                        else
                        {
                            chkStopPayroll.Checked = false;
                        }
                    }
                    divImgbtnEmpDet.Visible = true;
                    hf_EMPID.Value = hR_EMPLOYEES.EMP_ID;

                    EnableCompanyName();
                  
                    divStopPayroll.Visible = true;
                    //imgbtnAddClick();

                    string photoid = "";
                    DataTable dtphoto = new DataTable();
                    imgPatientFoto.ImageUrl = "~/Images/fotoshot.jpg";
                    if (hR_EMPLOYEES.PHOTO_ID != null)
                    {
                        photoid = hR_EMPLOYEES.PHOTO_ID.ToString();
                        dtphoto = DBMethod.ExecuteQuery(Applicant_DAL.GetPhotoFileName(photoid.ToString())).Tables[0];
                        if (dtphoto.Rows.Count > 0)
                        {
                            lblFileName.Text = dtphoto.Rows[0]["FILE_NAME"].ToString() + "." + dtphoto.Rows[0]["FILE_EXTENSTION"].ToString().Replace(".", "");
                            imgPatientFoto.ImageUrl = "~/UploadFile/PHOTO/" + hR_EMPLOYEES.EMP_ID + "." + dtphoto.Rows[0]["FILE_EXTENSTION"].ToString().Replace(".", "") + "?VER=" + hR_EMPLOYEES.PHOTO_ID.ToString(); ;
                        }
                    }
                    else
                    {
                        lblFileName.Text = "No Photo Upload for the Employee";
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>

        private void FillComboBox()
        {
            Lookup_BLL.GetLookUpValues(ref ddlInternalExternale, "IE");
            Lookup_BLL.GetLookUpValues(ref ddlEmploymentType, "EMPTYP");
            Lookup_BLL.GetLookUpValues(ref ddlMaritalStatus, "MS");
            Lookup_BLL.GetLookUpValues(ref ddlBlodGroup, "BG");
            Lookup_BLL.GetLookUpValues(ref ddlReligion, "RELIGION");
            Lookup_BLL.GetLookUpValues(ref ddlAddressType, "ADDTYP");
            Lookup_BLL.GetLookUpValues(ref ddlTitle, "TITLE");
            Lookup_BLL.GetLookUpValues(ref ddlNationality, "NATIONALITY");
            Applicant_BLL.fn_GetApplicantNMEmployeeEntry(ref ddlApplicantID, "'Offered'", Master.Mode);
            LeaveIndemCalc_BLL.fn_getIndemName(ref ddlIndemnity);
            FIN.BLL.GL.AccountCodes_BLL.fn_getAccount(ref ddlglAccCode);
            //FIN.BLL.SSM.Country_BLL.getNationalityName(ref ddlNationality);
            if (VMVServices.Web.Utils.LanguageCode.ToString().Length > 0)
            {
                Dictionary<string, string> Prop_File_Data;
                Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));
                RBG.Items[0].Text = Prop_File_Data["Male_P"];
                RBG.Items[1].Text = Prop_File_Data["Female_P"];
            }

        }

        private void AssignToBE()
        {
            try
            {

                save_data = false;
                ErrorCollection.Clear();

                // Duplicate Validation Through Backend Package PKG_VALIDATIONS

                //string ProReturn = FIN.DAL.HR.Employee_DAL.GetSPFOR_ERR_MGR_EMPNO(hR_EMPLOYEES.EMP_NO, hR_EMPLOYEES.EMP_ID);

                //if (ProReturn != string.Empty)
                //{
                //    if (ProReturn != "0")
                //    {
                //        ErrorCollection.Add("Employee", ProReturn);
                //        if (ErrorCollection.Count > 0)
                //        {
                //            return;
                //        }
                //    }
                //}

               
                if (EntityData != null)
                {
                    hR_EMPLOYEES = (HR_EMPLOYEES)EntityData;
                }

                if (ddlApplicantID.SelectedValue.ToString().Length > 0)
                {
                    hR_EMPLOYEES.APP_ID = ddlApplicantID.SelectedValue;
                }
                else
                {
                    hR_EMPLOYEES.APP_ID = null;
                }
                
                hR_EMPLOYEES.EMP_FIRST_NAME = txtFirstName.Text;
                hR_EMPLOYEES.EMP_MIDDLE_NAME = txtMiddleName.Text;
                hR_EMPLOYEES.EMP_LAST_NAME = txtLastName.Text;
                hR_EMPLOYEES.EMP_FIRST_NAME_OL = txtFirstNameol.Text;
                hR_EMPLOYEES.EMP_MIDDLE_NAME_OL = txtMiddleNameol.Text;
                hR_EMPLOYEES.EMP_LAST_NAME_OL = txtLastNameol.Text;
                hR_EMPLOYEES.EMP_DOB = DBMethod.ConvertStringToDate(txtDateofBirth.Text);
                hR_EMPLOYEES.EMP_DOJ = DBMethod.ConvertStringToDate(txtDoj.Text);
                hR_EMPLOYEES.INDEM_ID = ddlIndemnity.SelectedValue.ToString();

                if (ddlglAccCode.SelectedValue.ToString().Trim().Length > 0)
                {
                    hR_EMPLOYEES.EMP_ACCOUNT_CODE = ddlglAccCode.SelectedValue.ToString();
                }
                else
                {
                    hR_EMPLOYEES.EMP_ACCOUNT_CODE = null;
                }
                if (ddlIndemnity.SelectedValue.ToString().Trim().Length > 0)
                {
                    hR_EMPLOYEES.INDEM_ID = ddlIndemnity.SelectedValue.ToString();
                }
                else
                {
                    hR_EMPLOYEES.INDEM_ID = null;
                }
                if (ddlInternalExternale.SelectedValue.ToString().Length > 0)
                {
                    hR_EMPLOYEES.EMP_INTERNAL_EXTERNAL = ddlInternalExternale.SelectedValue.ToString();
                }

                if (ddlEmploymentType.SelectedValue.ToString().Length > 0)
                {
                    hR_EMPLOYEES.EMP_TYPE = ddlEmploymentType.SelectedValue.ToString();
                }

                if (ddlMaritalStatus.SelectedValue.ToString().Length > 0)
                {
                    hR_EMPLOYEES.EMP_MARITAL_STATUS = ddlMaritalStatus.SelectedValue.ToString();
                }

                if (ddlBlodGroup.SelectedValue.ToString().Length > 0)
                {
                    hR_EMPLOYEES.EMP_BLOOD_GROUP = ddlBlodGroup.SelectedValue.ToString();
                }
                if (ddlReligion.SelectedValue.ToString().Length > 0)
                {
                    hR_EMPLOYEES.EMP_RELIGION = ddlReligion.SelectedValue.ToString();
                }
                hR_EMPLOYEES.EMP_EMAIL = txtCompEmail.Text;
                hR_EMPLOYEES.ENABLED_FLAG = FINAppConstants.Y;
                hR_EMPLOYEES.ATTRIBUTE3 = txtCompanyname.Text;
                hR_EMPLOYEES.EMP_ORG_ID = VMVServices.Web.Utils.OrganizationID;
                hR_EMPLOYEES.EMP_NO = txtEmpNo.Text;
                hR_EMPLOYEES.EMP_GENDER = RBG.SelectedValue;
                hR_EMPLOYEES.PLACE_OF_BIRTH = txtPlaceofbirth.Text;
                if (txtMarriagedate.Text.ToString().Length > 0)
                {
                    hR_EMPLOYEES.MARRIAGE_DATE = DBMethod.ConvertStringToDate(txtMarriagedate.Text);
                }
                else
                {
                    hR_EMPLOYEES.MARRIAGE_DATE = null;
                }
                if (txtNoofDependents.Text.ToString().Length > 0)
                {
                    hR_EMPLOYEES.NUMBER_OF_DEPENDENTS = int.Parse(txtNoofDependents.Text);
                }
                else
                {
                    hR_EMPLOYEES.NUMBER_OF_DEPENDENTS = null;
                }
               


                hR_EMPLOYEES.TITLE = ddlTitle.SelectedValue;
                hR_EMPLOYEES.NATIONALITY = ddlNationality.SelectedValue;
                if (txtConfDate.Text.ToString().Length > 0)
                {
                    hR_EMPLOYEES.CONFIRMATION_DATE = DBMethod.ConvertStringToDate(txtConfDate.Text);
                }
                else
                {
                    hR_EMPLOYEES.CONFIRMATION_DATE = null;
                }
                if (txtResignationDate.Text.ToString().Length > 0)
                {
                    hR_EMPLOYEES.TERMINATION_DATE = DBMethod.ConvertStringToDate(txtResignationDate.Text);
                }
                else
                {
                    hR_EMPLOYEES.TERMINATION_DATE = null;
                }
                if (txtNoticePeriod.Text.ToString().Length > 0)
                {
                    hR_EMPLOYEES.NOTICE_PERIOD = decimal.Parse(txtNoticePeriod.Text);
                }
                else
                {
                    hR_EMPLOYEES.NOTICE_PERIOD = null;
                }

                if (txtNoticePeridDate.Text.ToString().Length > 0)
                {
                    hR_EMPLOYEES.NOTICE_PERIOD_DATE = DBMethod.ConvertStringToDate(txtNoticePeridDate.Text);
                }
                else
                {
                    hR_EMPLOYEES.NOTICE_PERIOD_DATE = null;
                }
                if (chkSSDFlag.Checked)
                {
                    hR_EMPLOYEES.SSD_FLAG = FINAppConstants.Y;
                }
                else
                {
                    hR_EMPLOYEES.SSD_FLAG = FINAppConstants.N;
                }
                if (chkStopPayroll.Checked)
                {
                    hR_EMPLOYEES.STOP_PAYTOLL = FINAppConstants.Y;
                }
                else
                {
                    hR_EMPLOYEES.STOP_PAYTOLL = FINAppConstants.N;
                }
                if (chkProbation.Checked)
                {
                    hR_EMPLOYEES.PROB_FLAG = FINAppConstants.Y;
                }
                else
                {
                    hR_EMPLOYEES.PROB_FLAG = FINAppConstants.N;
                }
                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    hR_EMPLOYEES.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_EMPLOYEES.EMP_ID);
                    hR_EMPLOYEES.MODIFIED_BY = this.LoggedUserName;
                    hR_EMPLOYEES.MODIFIED_DATE = DateTime.Today;
                    save_data = true;
                }
                else
                {
                    hR_EMPLOYEES.EMP_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_009.ToString(), false, true);
                    hR_EMPLOYEES.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_EMPLOYEES.EMP_ID);
                    //hR_EMPLOYEES.EMP_NO = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_009_NO.ToString(), false, true);
                    // txtEmpNo.Text = hR_EMPLOYEES.EMP_NO;
                    hR_EMPLOYEES.CREATED_BY = this.LoggedUserName;
                    hR_EMPLOYEES.CREATED_DATE = DateTime.Today;
                    hf_EMPID.Value = hR_EMPLOYEES.EMP_ID;
                    save_data = true;
                  
                }

                if (Session[FINSessionConstants.Attachments] != null)
                {
                    System.Collections.SortedList fu_tmp = (System.Collections.SortedList)Session[FINSessionConstants.Attachments];
                    if (System.IO.File.Exists(Server.MapPath("~/UploadFile/PHOTO/" + hR_EMPLOYEES.EMP_ID + "." + fu_tmp.GetByIndex(1))))
                        System.IO.File.Delete(Server.MapPath("~/UploadFile/PHOTO/" + hR_EMPLOYEES.EMP_ID + "." + fu_tmp.GetByIndex(1)));

                    System.IO.File.Copy(Server.MapPath("~/UploadFile/Captures/" + fu_tmp.GetByIndex(0) + "." + fu_tmp.GetByIndex(1)), Server.MapPath("~/UploadFile/PHOTO/" + hR_EMPLOYEES.EMP_ID + "." + fu_tmp.GetByIndex(1)));
                    hR_EMPLOYEES.PHOTO_ID = fu_tmp.GetByIndex(0).ToString();
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
           

        }
        #endregion




        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                DBMethod.DeleteEntity<HR_EMPLOYEES>(hR_EMPLOYEES);
                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATADELETED);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void SaveUserDetails()
        {
            SSM_USERS sSM_USERS = new SSM_USERS();
            sSM_USERS.USER_CODE = txtEmpNo.Text;
            sSM_USERS.FIRST_NAME = txtFirstName.Text;
            sSM_USERS.USER_CODE_OL = txtEmpNo.Text;
            sSM_USERS.FIRST_NAME_OL = txtFirstNameol.Text;
            sSM_USERS.LAST_NAME = txtLastName.Text;
            sSM_USERS.MIDDLE_NAME = txtMiddleName.Text;
            sSM_USERS.SURNAME = "";
            sSM_USERS.EFFECTIVE_DATE = DBMethod.ConvertStringToDate(txtDoj.Text.ToString());

            sSM_USERS.ENABLED_FLAG = FINAppConstants.EnabledFlag;
            sSM_USERS.ATTRIBUTE1 = "EMPLOYEE";

            sSM_USERS.USER_PASSWORD = EnCryptDecrypt.CryptorEngine.Encrypt(txtEmpNo.Text.ToString(), true);
            sSM_USERS.GENDER = RBG.SelectedValue;
            sSM_USERS.TITLE = ddlTitle.SelectedValue;

            if (txtDateofBirth.Text != string.Empty)
            {
                sSM_USERS.DATE_OF_BIRTH = DBMethod.ConvertStringToDate(txtDateofBirth.Text);
            }
            sSM_USERS.NATIONALITY = ddlNationality.SelectedValue;
            sSM_USERS.PASSPORT_NUMBER_OR_EMP_ID = hR_EMPLOYEES.EMP_ID;

            sSM_USERS.CHANGE_PASSWORD_ON_NEXT_LOGIN = FINAppConstants.EnabledFlag;
            sSM_USERS.CHANGE_PASSWORD_INTERVAL = FINAppConstants.DisabledFlag;
            sSM_USERS.PASSWORD_LOCK = FINAppConstants.DisabledFlag;

            //sSM_USERS.SUPERVISOR_CODE = ddlSupervisor.SelectedValue;
            //sSM_USERS.GROUP_ID = ddlgroup.SelectedValue;
            sSM_USERS.ADDRESS_LINE_1 = txtAddress1.Text;
            sSM_USERS.ADDRESS_LINE_2 = txtAddress2.Text;

            sSM_USERS.PHONE = txtPhone.Text;
            sSM_USERS.MOBILE_NO = txtMobile.Text;

            sSM_USERS.EMAIL_ID = txtCompEmail.Text;


            sSM_USERS.PK_ID = DBMethod.GetPrimaryKeyValue("SSM_USERS_SEQ");
            sSM_USERS.CREATED_BY = this.LoggedUserName;
            sSM_USERS.CREATED_DATE = DateTime.Today;

            sSM_USERS.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, sSM_USERS.USER_CODE);
            DBMethod.SaveEntity<SSM_USERS>(sSM_USERS);

        }
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                AssignToBE();
              

                if (!save_data)
                {
                    return;
                }

                // Duplicate Validation Through Backend Package PKG_VALIDATIONS
                ProReturn = FIN.DAL.HR.Employee_DAL.GetSPFOR_ERR_MGR_CATEGORY(Master.FormCode, hR_EMPLOYEES.EMP_ID, hR_EMPLOYEES.EMP_NO);

                if (ProReturn != string.Empty)
                {
                    if (ProReturn != "0")
                    {
                        ErrorCollection.Add("EMPLOYEE", ProReturn);
                        if (ErrorCollection.Count > 0)
                        {
                            return;
                        }
                    }
                }


                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {                           

                            DBMethod.SaveEntity<HR_EMPLOYEES>(hR_EMPLOYEES);
                            SaveUserDetails();                            

                            hfPassTxnID.Value = FINSP.GetSPFOR_SEQCode("EMP_PP_ID".ToString(), false, true);
                            FIN.DAL.HR.Employee_DAL.InsertPassPortDtl(hfPassTxnID.Value, hR_EMPLOYEES.EMP_ID, "Civil ID",ddlNationality.SelectedValue.ToString());

                            hfPassTxnID.Value = FINSP.GetSPFOR_SEQCode("EMP_PP_ID".ToString(), false, true);
                            FIN.DAL.HR.Employee_DAL.InsertPassPortDtl(hfPassTxnID.Value, hR_EMPLOYEES.EMP_ID, "Passport",ddlNationality.SelectedValue.ToString());
                            break;

                        }
                    case FINAppConstants.Update:
                        {

                            DBMethod.SaveEntity<HR_EMPLOYEES>(hR_EMPLOYEES, true);
                            break;

                        }
                }
                if (Master.Mode == FINAppConstants.Add)
                {
                    if (chkProbation.Checked == true)
                    {
                        HR_EMP_PROBATION hR_EMP_PROBATION = new HR_EMP_PROBATION();

                        hR_EMP_PROBATION.PROB_EMP_ID = hR_EMPLOYEES.EMP_ID;
                        hR_EMP_PROBATION.PROB_FROM_DT = DBMethod.ConvertStringToDate(txtDoj.Text);
                        hR_EMP_PROBATION.ENABLED_FLAG = FINAppConstants.Y;
                        hR_EMP_PROBATION.PROB_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_077.ToString(), false, true);
                        hR_EMP_PROBATION.PROB_ORG_ID = VMVServices.Web.Utils.OrganizationID;
                        hR_EMP_PROBATION.PROB_STATUS = "Initiated";

                        hR_EMP_PROBATION.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_EMP_PROBATION.PROB_ID);
                        hR_EMP_PROBATION.CREATED_BY = this.LoggedUserName;
                        hR_EMP_PROBATION.CREATED_DATE = DateTime.Today;
                        DBMethod.SaveEntity<HR_EMP_PROBATION>(hR_EMP_PROBATION);
                    }

                }
                btnSave.Enabled = false;
                Master.Mode = FINAppConstants.Update;
                Master.StrRecordId = hR_EMPLOYEES.EMP_ID;
                divImgbtnEmpDet.Visible = true;
                if (ddlApplicantID.SelectedValue.ToString().Length > 0)
                {
                    HR_APPLICANTS hR_APPLICANTS = new HR_APPLICANTS();

                    using (IRepository<HR_APPLICANTS> userCtx = new DataRepository<HR_APPLICANTS>())
                    {
                        hR_APPLICANTS = userCtx.Find(r =>
                            (r.APP_ID == ddlApplicantID.SelectedValue)
                            ).SingleOrDefault();
                    }
                    hR_APPLICANTS.STATUS = "Joined";
                    DBMethod.SaveEntity<HR_APPLICANTS>(hR_APPLICANTS, true);

                }
               

                if (pnlAddress.Visible)
                {
                    btnAddAdd_Click(sender, e);
                }
                if (pnlContDet.Visible)
                {
                    bntContDetAdd_Click(sender, e);
                }
                if (pnlSalaryDetails.Visible)
                {
                    SaveSalaryDetails();
                }
                VMVServices.Web.Utils.SavedRecordId = hf_EMPID.Value.ToString();
                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void btnAddress_Click(object sender, EventArgs e)
        {

            // //MPEAddress.Show();
            DataTable dt_EmpAddess = FIN.BLL.HR.Employee_BLL.getEmployeeAddress(Master.StrRecordId);
            Session["EmpAddress"] = dt_EmpAddess;
            if (dt_EmpAddess.Rows.Count > 0)
            {
                DisplayAddress(0);
                hfAddRecId.Value = "0";
            }
            else
            {
                btnAddAdd.Text = "Update";
            }
        }

        protected void imgbtnAddress_Click(object sender, ImageClickEventArgs e)
        {
            imgbtnAddClick();
        }
        private void imgbtnAddClick()
        {
            //MPEAddress.Show();
            SetVisiableFalsePanel();
            pnlAddress.Visible = true;

            DataTable dt_EmpAddess = FIN.BLL.HR.Employee_BLL.getEmployeeAddress(Master.StrRecordId);
            Session["EmpAddress"] = dt_EmpAddess;
            if (dt_EmpAddess.Rows.Count > 0)
            {
                DisplayAddress(0);
                hfAddRecId.Value = "0";
            }
            else
            {
                btnAddAdd.Text = "Update";
            }
        }
        private void DisplayAddress(int int_RowNumber)
        {
            //MPEAddress.Show();
            DataTable dt_EmpAddess = (DataTable)Session["EmpAddress"];

            ddlAddressType.SelectedValue = dt_EmpAddess.Rows[int_RowNumber]["ADDRESS_TYPE"].ToString();
            txtAddress1.Text = dt_EmpAddess.Rows[int_RowNumber]["EMP_ADD1"].ToString();
            txtAddress2.Text = dt_EmpAddess.Rows[int_RowNumber]["EMP_ADD2"].ToString();
            txtAddress3.Text = dt_EmpAddess.Rows[int_RowNumber]["EMP_ADD3"].ToString();
            txtCountry.Text = dt_EmpAddess.Rows[int_RowNumber]["EMP_COUNTRY"].ToString();
            txtState.Text = dt_EmpAddess.Rows[int_RowNumber]["EMP_STATE"].ToString();
            txtCity.Text = dt_EmpAddess.Rows[int_RowNumber]["EMP_CITY"].ToString();
            txtPostalCode.Text = dt_EmpAddess.Rows[int_RowNumber]["EMP_ZIP_CODE"].ToString();
            txtPhone.Text = dt_EmpAddess.Rows[int_RowNumber]["EMP_PHONE1"].ToString();
            txtMobile.Text = dt_EmpAddess.Rows[int_RowNumber]["EMP_PHONE2"].ToString();
            txtMobile2.Text = dt_EmpAddess.Rows[int_RowNumber]["EMP_PHONE3"].ToString();
            txtEffectiveDate.Text = DBMethod.ConvertDateToString(dt_EmpAddess.Rows[int_RowNumber]["EFFECTIVE_FROM_DT"].ToString());
            if (dt_EmpAddess.Rows[int_RowNumber]["EFFECTIVE_TO_DT"] != null)
            {
                if (dt_EmpAddess.Rows[int_RowNumber]["EFFECTIVE_TO_DT"].ToString().Length > 0)
                    txtEndDate.Text = DBMethod.ConvertDateToString(dt_EmpAddess.Rows[int_RowNumber]["EFFECTIVE_TO_DT"].ToString());
            }
            PermanentAddress.Checked = false;
            if (dt_EmpAddess.Rows[int_RowNumber]["EMP_PRIMARY_ADDRESS"].ToString() == FINAppConstants.Y)
                PermanentAddress.Checked = true;

            chkAddActive.Checked = false;
            if (dt_EmpAddess.Rows[int_RowNumber]["ENABLED_FLAG"].ToString() == FINAppConstants.Y)
                chkAddActive.Checked = true;

            hfAddressID.Value = dt_EmpAddess.Rows[int_RowNumber]["ADDRESS_ID"].ToString();
            btnAddAdd.Text = "Update";

        }
        protected void btnAddAdd_Click(object sender, EventArgs e)
        {
            try
            {
                Dictionary<string, string> Prop_File_Data;
                Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));
                DataTable dt_EmpAddess = (DataTable)Session["EmpAddress"];
                if (PermanentAddress.Checked)
                {
                    if (dt_EmpAddess != null)
                    {
                        DataRow[] dr = dt_EmpAddess.Select("EMP_PRIMARY_ADDRESS='" + FINAppConstants.Y + "' AND ADDRESS_ID <>'" + hfAddressID.Value + "'");
                        if (dr.Length > 0)
                        {
                            ErrorCollection.Add("DublicatePrimaryAddress", Prop_File_Data["DuplicatePrimaryAddress_P"]);

                            lblAddSaved.Text = "Address - Only One permanent Address can able to add";
                            lblAddSaved.Visible = true;
                            //MPEAddress.Show();
                            Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                            return;
                        }
                    }
                }



                HR_ADDRESS hR_ADDRESS = new HR_ADDRESS();
                if (hfAddressID.Value.ToString() != "0")
                {
                    using (IRepository<HR_ADDRESS> userCtx = new DataRepository<HR_ADDRESS>())
                    {
                        hR_ADDRESS = userCtx.Find(r =>
                            (r.EMP_ID == Master.StrRecordId && r.ADDRESS_ID == hfAddressID.Value.ToString())
                            ).SingleOrDefault();
                    }
                }
                hR_ADDRESS.EMP_ID = Master.StrRecordId;
                hR_ADDRESS.ADDRESS_TYPE = ddlAddressType.SelectedValue;
                hR_ADDRESS.EMP_ADD1 = txtAddress1.Text;
                hR_ADDRESS.EMP_ADD2 = txtAddress2.Text;
                hR_ADDRESS.EMP_ADD3 = txtAddress3.Text;
                hR_ADDRESS.EMP_COUNTRY = txtCountry.Text;
                hR_ADDRESS.EMP_STATE = txtState.Text;
                hR_ADDRESS.EMP_CITY = txtCity.Text;
                hR_ADDRESS.EMP_ZIP_CODE = txtPostalCode.Text;
                hR_ADDRESS.EMP_PHONE1 = txtPhone.Text;
                hR_ADDRESS.EMP_PHONE2 = txtMobile.Text;
                hR_ADDRESS.EMP_PHONE3 = txtMobile2.Text;
                hR_ADDRESS.EFFECTIVE_FROM_DT = DBMethod.ConvertStringToDate(txtEffectiveDate.Text);
                if (txtEndDate.Text.ToString().Length > 0)
                {
                    hR_ADDRESS.EFFECTIVE_TO_DT = DBMethod.ConvertStringToDate(txtEndDate.Text);
                }
                else
                {
                    hR_ADDRESS.EFFECTIVE_TO_DT = null;
                }
                if (PermanentAddress.Checked)
                {
                    hR_ADDRESS.EMP_PRIMARY_ADDRESS = FINAppConstants.Y;
                }
                else
                {
                    hR_ADDRESS.EMP_PRIMARY_ADDRESS = FINAppConstants.N;
                }
                hR_ADDRESS.ENABLED_FLAG = FINAppConstants.N;
                if (chkAddActive.Checked)
                {
                    hR_ADDRESS.ENABLED_FLAG = FINAppConstants.Y;
                }
                hR_ADDRESS.WORKFLOW_COMPLETION_STATUS = FINAppConstants.Y;

                string str_Error = Employee_BLL.ValidateEntity(hR_ADDRESS);
                if (str_Error.Length > 0)
                {
                    ErrorCollection.Add("EntityValidation_AG", str_Error);
                    return;
                }
                if (hfAddressID.Value.ToString() == "0")
                {
                    hR_ADDRESS.CREATED_BY = this.LoggedUserName;
                    hR_ADDRESS.CREATED_DATE = DateTime.Today;
                    hR_ADDRESS.ADDRESS_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_009_ADD.ToString(), false, true);
                    Employee_BLL.SaveEntity(hR_ADDRESS, FINAppConstants.Add, true);
                    hfAddressID.Value = hR_ADDRESS.ADDRESS_ID.ToString();
                }
                else
                {
                    hR_ADDRESS.MODIFIED_BY = this.LoggedUserName;
                    hR_ADDRESS.MODIFIED_DATE = DateTime.Today;
                    Employee_BLL.SaveEntity(hR_ADDRESS, FINAppConstants.Update, true);

                }
                //MPEAddress.Show();
                lblAddSaved.Text = "Address -  Saved Successfully ";
                lblAddSaved.Visible = true;
                ErrorCollection.Add("EmpAddressSaved", " Employee Address Details Saved ");
                Master.ShowMessage(ErrorCollection, FINAppConstants.SAVED);
                //ClearAddress();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save_AG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);

                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void ClearAddress()
        {
            ddlAddressType.SelectedIndex = -1;
            txtAddress1.Text = "";
            txtAddress2.Text = "";
            txtAddress3.Text = "";
            txtCity.Text = "";
            txtCountry.Text = "";
            txtState.Text = "";
            txtPhone.Text = "";
            txtPostalCode.Text = "";
            txtEffectiveDate.Text = "";
            txtEndDate.Text = "";
            txtMobile.Text = "";
            txtMobile2.Text = "";
            btnAddAdd.Text = "Update";
            hfAddressID.Value = "0";
            lblAddSaved.Text = "Address";
            lblAddSaved.Visible = true;
            //MPEAddress.Show();

        }

        protected void btnAddClear_Click(object sender, EventArgs e)
        {
            ClearAddress();

        }

        protected void btnContDet_Click(object sender, EventArgs e)
        {


            //MPEContDet.Show();
            FIN.BLL.Lookup_BLL.GetLookUpValues(ref ddlConRelType, "RELT");
            DataTable dt_EmpCont = FIN.BLL.HR.Employee_BLL.getEmployeeContact(Master.StrRecordId);
            Session["EMPContact"] = dt_EmpCont;
            if (dt_EmpCont.Rows.Count > 0)
            {
                DisplayContDet(0);
                hfContDetId.Value = "0";
            }
            else
            {
                btnAddAdd.Text = "Update";
            }
        }

        protected void imgBtnContDet_Click(object sender, ImageClickEventArgs e)
        {
            imgBtnContDetDisp();
        }

        private void imgBtnContDetDisp()
        {
            //MPEContDet.Show();
            SetVisiableFalsePanel();
            pnlContDet.Visible = true;
            FIN.BLL.Lookup_BLL.GetLookUpValues(ref ddlConRelType, "RELT");
            DataTable dt_EmpCont = FIN.BLL.HR.Employee_BLL.getEmployeeDependant(Master.StrRecordId);
            Session["EMPContact"] = dt_EmpCont;
            if (dt_EmpCont.Rows.Count > 0)
            {
                DisplayContDet(0);
                btnContDetAdd.Text = "Update";
                hfContRecid.Value = "0";
            }
            else
            {
                btnAddAdd.Text = "Update";
            }
        }

        private void DisplayContDet(int int_intRowNumber)
        {
            DataTable dt_EmpCont = (DataTable)Session["EMPContact"];
            txtConPerName.Text = dt_EmpCont.Rows[int_intRowNumber]["DEPENDANT_FIRST_NAME"].ToString();
            txtConLastName.Text = dt_EmpCont.Rows[int_intRowNumber]["DEPENDANT_LAST_NAME"].ToString();
            txtConMiddleName.Text = dt_EmpCont.Rows[int_intRowNumber]["DEPENDANT_MIDDLE_NAME"].ToString();
            if (dt_EmpCont.Rows[int_intRowNumber]["DEPENDANT_RELATION"] != null)
            {
                ddlConRelType.Text = dt_EmpCont.Rows[0]["DEPENDANT_RELATION"].ToString();
            }
            txtConPerAdd1.Text = dt_EmpCont.Rows[int_intRowNumber]["DEPENDANT_ADDRESS1"].ToString();
            txtConPerAdd2.Text = dt_EmpCont.Rows[int_intRowNumber]["DEPENDANT_ADDRESS2"].ToString();
            txtConPerAdd3.Text = dt_EmpCont.Rows[int_intRowNumber]["DEPENDANT_ADDRESS3"].ToString();



            txtContCountry.Text = dt_EmpCont.Rows[int_intRowNumber]["ATTRIBUTE1"].ToString();
            txtContAread.Text = dt_EmpCont.Rows[int_intRowNumber]["ATTRIBUTE2"].ToString();
            txtContCity.Text = dt_EmpCont.Rows[int_intRowNumber]["ATTRIBUTE3"].ToString();
            txtContPhone.Text = dt_EmpCont.Rows[int_intRowNumber]["ATTRIBUTE4"].ToString();
            txtContMob.Text = dt_EmpCont.Rows[int_intRowNumber]["ATTRIBUTE5"].ToString();

            chkContPerActive.Checked = false;
            if (dt_EmpCont.Rows[int_intRowNumber]["ENABLED_FLAG"].ToString() == FINAppConstants.Y)
                chkContPerActive.Checked = true;

            hfContDetId.Value = dt_EmpCont.Rows[int_intRowNumber]["DEPENDANT_ID"].ToString();
            //MPEContDet.Show();
        }
        protected void bntContDetAdd_Click(object sender, EventArgs e)
        {
            try
            {
                //HR_EMP_CONTACT hR_EMP_CONTACT = new HR_EMP_CONTACT();
                //if (hfContDetId.Value.ToString() != "0")
                //{
                //    using (IRepository<HR_EMP_CONTACT> userCtx = new DataRepository<HR_EMP_CONTACT>())
                //    {
                //        hR_EMP_CONTACT = userCtx.Find(r =>
                //            (r.EMP_ID == Master.StrRecordId && r.EMP_CONTACT_ID == hfContDetId.Value.ToString())
                //            ).SingleOrDefault();
                //    }
                //}
                //hR_EMP_CONTACT.EMP_ID = Master.StrRecordId;
                //hR_EMP_CONTACT.EMP_CONTACT_NAME = txtConPerName.Text;
                //hR_EMP_CONTACT.EMP_CON_RELATION = ddlConRelType.SelectedValue.ToString();
                //hR_EMP_CONTACT.EMP_CONTACT_ADD1 = txtConPerAdd1.Text;
                //hR_EMP_CONTACT.EMP_CONTACT_ADD2 = txtConPerAdd2.Text;
                //hR_EMP_CONTACT.EMP_CONTACT_ADD3 = txtConPerAdd3.Text;
                //hR_EMP_CONTACT.EMP_CONTACT_COUNTRY = txtConPerCountry.Text;
                //hR_EMP_CONTACT.EMP_CONTACT_STATE = txtConPerState.Text;
                //hR_EMP_CONTACT.EMP_CONTACT_CITY = txtConPerCity.Text;
                //hR_EMP_CONTACT.EMP_CONTACT_ZIP_CODE = txtConPerPostCode.Text;
                //hR_EMP_CONTACT.EMP_CONTACT_PHONE1 = txtConPerPhone.Text;
                //hR_EMP_CONTACT.EMP_CONTACT_PHONE1 = txtConPerMobile.Text;
                //hR_EMP_CONTACT.EFFECTIVE_FROM_DT = DBMethod.ConvertStringToDate(txtConPerEffDate.Text);
                //hR_EMP_CONTACT.EMP_PRIMARY_CONTACT = FINAppConstants.N;
                //if (txtConPerEndDate.Text.ToString().Length > 0)
                //{
                //    hR_EMP_CONTACT.EFFECTIVE_TO_DT = DBMethod.ConvertStringToDate(txtConPerEndDate.Text);
                //}
                //if (chkcontPerPrimCont.Checked)
                //{
                //    hR_EMP_CONTACT.EMP_PRIMARY_CONTACT = FINAppConstants.Y;
                //}

                //hR_EMP_CONTACT.ENABLED_FLAG = FINAppConstants.N;
                //if (chkContPerActive.Checked)
                //{
                //    hR_EMP_CONTACT.ENABLED_FLAG = FINAppConstants.Y;
                //}
                //hR_EMP_CONTACT.WORKFLOW_COMPLETION_STATUS = FINAppConstants.Y;

                //string str_Error = Employee_BLL.ValidateEntity(hR_EMP_CONTACT);
                //if (str_Error.Length > 0)
                //{
                //    ErrorCollection.Add("EntityValidation_AG", str_Error);
                //    return;
                //}
                //if (hfContDetId.Value.ToString() == "0")
                //{
                //    hR_EMP_CONTACT.CREATED_BY = this.LoggedUserName;
                //    hR_EMP_CONTACT.CREATED_DATE = DateTime.Today;
                //    hR_EMP_CONTACT.EMP_CONTACT_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_009_CT.ToString(), false, true);
                //    Employee_BLL.SaveEntity(hR_EMP_CONTACT, FINAppConstants.Add);
                //}
                //else
                //{
                //    hR_EMP_CONTACT.MODIFIED_BY = this.LoggedUserName;
                //    hR_EMP_CONTACT.MODIFIED_DATE = DateTime.Today;
                //    Employee_BLL.SaveEntity(hR_EMP_CONTACT, FINAppConstants.Update, true);
                //}

                ////MPEContDet.Show();
                //hfContDetId.Value = hR_EMP_CONTACT.EMP_CONTACT_ID.ToString();
                //btnContDetAdd.Text = "Update";
                //lblContSave.Text = "Data Saved Successfully";
                //lblContSave.Visible = true;


                HR_DEPENDANTS hR_DEPENDANTS = new HR_DEPENDANTS();
                if (hfContDetId.Value.ToString() != "0")
                {
                    using (IRepository<HR_DEPENDANTS> userCtx = new DataRepository<HR_DEPENDANTS>())
                    {
                        hR_DEPENDANTS = userCtx.Find(r =>
                            (r.DEPNDANT_EMP_ID == Master.StrRecordId && r.DEPENDANT_ID == hfContDetId.Value.ToString())
                            ).SingleOrDefault();
                    }
                }
                hR_DEPENDANTS.DEPNDANT_EMP_ID = Master.StrRecordId;
                hR_DEPENDANTS.DEPENDANT_RELATION = ddlConRelType.SelectedValue.ToString();
                hR_DEPENDANTS.DEPENDANT_FIRST_NAME = txtConPerName.Text;
                hR_DEPENDANTS.DEPENDANT_MIDDLE_NAME = txtConMiddleName.Text;
                hR_DEPENDANTS.DEPENDANT_LAST_NAME = txtConLastName.Text;
                hR_DEPENDANTS.DEPENDANT_ADDRESS1 = txtConPerAdd1.Text;
                hR_DEPENDANTS.DEPENDANT_ADDRESS2 = txtConPerAdd2.Text;
                hR_DEPENDANTS.DEPENDANT_ADDRESS3 = txtConPerAdd3.Text;

                hR_DEPENDANTS.ATTRIBUTE1 = txtContCountry.Text;
                hR_DEPENDANTS.ATTRIBUTE2 = txtContAread.Text;
                hR_DEPENDANTS.ATTRIBUTE3 = txtContCity.Text;
                hR_DEPENDANTS.ATTRIBUTE4 = txtContPhone.Text;
                hR_DEPENDANTS.ATTRIBUTE5 = txtContMob.Text;

                hR_DEPENDANTS.DEPENDANT_ORG_ID = VMVServices.Web.Utils.OrganizationID;

                hR_DEPENDANTS.ENABLED_FLAG = FINAppConstants.N;
                if (chkContPerActive.Checked)
                {
                    hR_DEPENDANTS.ENABLED_FLAG = FINAppConstants.Y;
                }
                hR_DEPENDANTS.WORKFLOW_COMPLETION_STATUS = FINAppConstants.Y;

                string str_Error = Employee_BLL.ValidateEntity(hR_DEPENDANTS);
                if (str_Error.Length > 0)
                {
                    ErrorCollection.Add("EntityValidation_AG", str_Error);
                    return;
                }
                if (hfContDetId.Value.ToString() == "0")
                {
                    hR_DEPENDANTS.CREATED_BY = this.LoggedUserName;
                    hR_DEPENDANTS.CREATED_DATE = DateTime.Today;
                    hR_DEPENDANTS.DEPENDANT_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_009_CT.ToString(), false, true);
                    Employee_BLL.SaveEntity(hR_DEPENDANTS, FINAppConstants.Add);
                }
                else
                {
                    hR_DEPENDANTS.MODIFIED_BY = this.LoggedUserName;
                    hR_DEPENDANTS.MODIFIED_DATE = DateTime.Today;
                    Employee_BLL.SaveEntity(hR_DEPENDANTS, FINAppConstants.Update, true);
                }

                //MPEContDet.Show();
                hfContDetId.Value = hR_DEPENDANTS.DEPENDANT_ID.ToString();
                btnContDetAdd.Text = "Update";
                lblContSave.Text = "Contact Details - Saved Successfully";
                lblContSave.Visible = true;
                ErrorCollection.Add("EmpContactSaved", "Contact Details Saved Successfully");
                Master.ShowMessage(ErrorCollection, FINAppConstants.SAVED);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save_AG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);

                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void ClearContAddress()
        {
            btnContDetAdd.Text = "Update";
            txtConPerName.Text = "";
            txtConPerAdd1.Text = "";
            txtConPerAdd2.Text = "";
            txtConPerAdd3.Text = "";
            txtConLastName.Text = "";
            txtConMiddleName.Text = "";
            txtContCountry.Text = "";
            txtContAread.Text = "";
            txtContCity.Text = "";
            txtContMob.Text = "";
            txtContPhone.Text = "";
            lblContSave.Text = "Contact Details";
            lblContSave.Visible = true;
            //MPEContDet.Show();
            hfContDetId.Value = "0";


        }

        protected void btnContDetClear_Click(object sender, EventArgs e)
        {
            ClearContAddress();
        }

        protected void btnWorkDetails_Click(object sender, EventArgs e)
        {

        }

        protected void imgBtnWorkDetails_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                BindWorkDetails();
                //MPEWorkDet.Show();
                SetVisiableFalsePanel();
                pnlWorkDet.Visible = true;
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EMPWorkDetails", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);

                }
            }
        }
        private void SetVisiableFalsePanel()
        {
            pnlWorkDet.Visible = false;
            pnlSkillsDet.Visible = false;
            pnlQualDet.Visible = false;
            pnlPPDet.Visible = false;
            pnlEmpBank.Visible = false;
            pnlContDet.Visible = false;
            pnlAddress.Visible = false;
            pnlPassworddet.Visible = false;
            pnlLeaveDetails.Visible = false;
            pnlSalaryDetails.Visible = false;
            //div_MasterData.Visible = false;
            div_MDData.Visible = false;
        }
        private void BindWorkDetails()
        {
            try
            {
                ErrorCollection.Clear();
                //MPEWorkDet.Show();
                DataTable dt_workDet = DBMethod.ExecuteQuery(FIN.DAL.HR.Employee_DAL.getWorkDetails(hf_EMPID.Value.ToString())).Tables[0];
                bol_WorkDet = false;
                DataTable dt_tmp = dt_workDet.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dt_tmp.Rows.Add(dr);
                    bol_WorkDet = true;
                }
                gvWorkDet.DataSource = dt_tmp;
                gvWorkDet.DataBind();
                GridViewRow gvr = gvWorkDet.FooterRow;
                FillFooterWorkDetDDL(gvr);
                Session["EmpWorkDetails"] = dt_tmp;
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EmpWOrkDetBindGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void gvWorkDet_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvWorkDet.EditIndex = e.NewEditIndex;
            BindWorkDetails();
            GridViewRow gvr = gvWorkDet.Rows[e.NewEditIndex];
            FillFooterWorkDetDDL(gvr);
            //MPEWorkDet.Show();
        }

        private void FillFooterWorkDetDDL(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddl_Department = tmpgvr.FindControl("ddlDepartment") as DropDownList;
                DropDownList ddl_Category = tmpgvr.FindControl("ddlCategory") as DropDownList;
                DropDownList ddl_Grade = tmpgvr.FindControl("ddlGrade") as DropDownList;
                DropDownList ddl_Job = tmpgvr.FindControl("ddlJob") as DropDownList;
                DropDownList ddl_ReportingTo = tmpgvr.FindControl("ddlReportingTo") as DropDownList;
                DropDownList ddl_ContPlan = tmpgvr.FindControl("ddlContPlan") as DropDownList;
                DropDownList ddlLocation = tmpgvr.FindControl("ddlLocation") as DropDownList;
                FIN.BLL.HR.Department_BLL.GetDepartmentName(ref ddl_Department);
                FIN.BLL.HR.Categories_BLL.fn_getCategory(ref ddl_Category, Master.Mode);
                FIN.BLL.HR.Employee_BLL.GetEmployeeName(ref ddl_ReportingTo);
                FIN.BLL.HR.Employee_BLL.GetEmployeeName(ref ddl_ContPlan);
                FIN.BLL.HR.Location_BLL.GetLocations(ref ddlLocation);
                ddl_ReportingTo.Items.Remove(new ListItem(Master.StrRecordId, txtFirstName.Text));
                //FIN.BLL.HR.Grades_BLL.fn_getGrade(ref ddl_Grade);
                // FIN.BLL.HR.Jobs_BLL.fn_getJobName(ref ddl_Job);

                if (gvWorkDet.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                {
                    ddl_Department.SelectedValue = gvWorkDet.DataKeys[gvWorkDet.EditIndex].Values["EMP_DEPT_ID"].ToString();
                    LoadDesgination(tmpgvr);
                    DropDownList ddl_Designation = tmpgvr.FindControl("ddlDesignation") as DropDownList;
                    DropDownList ddlSection = tmpgvr.FindControl("ddlSection") as DropDownList;
                   
                    ddl_Designation.SelectedValue = gvWorkDet.DataKeys[gvWorkDet.EditIndex].Values["EMP_DESIG_ID"].ToString();
                    ddlSection.SelectedValue = gvWorkDet.DataKeys[gvWorkDet.EditIndex].Values["SEC_ID"].ToString();
                    ddlLocation.SelectedValue = gvWorkDet.DataKeys[gvWorkDet.EditIndex].Values["LOC_ID"].ToString();
                    ddl_Category.SelectedValue = gvWorkDet.DataKeys[gvWorkDet.EditIndex].Values["EMP_CATEGORY"].ToString();
                    LoadCategoryJob(tmpgvr);
                    ddl_Job.SelectedValue = gvWorkDet.DataKeys[gvWorkDet.EditIndex].Values["EMP_JOB_ID"].ToString();
                    getPosition(tmpgvr);
                    DropDownList dd_lPosition = tmpgvr.FindControl("ddlPosition") as DropDownList;
                    dd_lPosition.SelectedValue = gvWorkDet.DataKeys[gvWorkDet.EditIndex].Values["EMP_POSITION_ID"].ToString();

                    LoadGrade4Position(tmpgvr);
                    ddl_Grade.SelectedValue = gvWorkDet.DataKeys[gvWorkDet.EditIndex].Values["EMP_GRADE_ID"].ToString();
                    ddl_ReportingTo.SelectedValue = gvWorkDet.DataKeys[gvWorkDet.EditIndex].Values["REPORTINGTO_ID"].ToString();
                    ddl_ContPlan.SelectedValue = gvWorkDet.DataKeys[gvWorkDet.EditIndex].Values["CONTINGENCY_PLAN"].ToString(); 


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Work_DDLFIll", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        protected void gvWorkDet_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_WorkDet)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EmpWorkDetRowDataBound", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }
        protected void gvWorkDet_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {

            gvWorkDet.EditIndex = -1;

            BindWorkDetails();

        }
        protected void gvWorkDet_RowCreated(object sender, GridViewRowEventArgs e)
        {
            //try
            //{
            //    ErrorCollection.Clear();
            //    if (e.Row.RowType == DataControlRowType.EmptyDataRow)
            //    {
            //        GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
            //        gvWorkDet.Controls[0].Controls.AddAt(0, gvr);
            //    }
            //}
            //catch (Exception ex)
            //{
            //    ErrorCollection.Add("AccountCodesEntry_RowCreated", ex.Message);
            //}
            //finally
            //{
            //    if (ErrorCollection.Count > 0)
            //    {
            //        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
            //        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
            //    }
            //}

        }

        protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //MPEWorkDet.Show();
                GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
                LoadDesgination(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ddlDeptChange", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void LoadDesgination(GridViewRow gvr)
        {
            DropDownList ddl_Designation = (DropDownList)gvr.FindControl("ddlDesignation");
            DropDownList ddl_Department = (DropDownList)gvr.FindControl("ddlDepartment");
            DropDownList ddlSection = (DropDownList)gvr.FindControl("ddlSection");
          
            FIN.BLL.HR.DepartmentDetails_BLL.fn_getDesigName4DeptId(ref ddl_Designation, ddl_Department.SelectedValue);
            FIN.BLL.HR.DepartmentDetails_BLL.fn_getSectionName4DeptId(ref ddlSection, ddl_Department.SelectedValue);
           
        }

        protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //MPEWorkDet.Show();
                GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
                LoadCategoryJob(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ddlDeptChange", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void LoadCategoryJob(GridViewRow gvr)
        {
            DropDownList ddl_Job = (DropDownList)gvr.FindControl("ddlJob");
            DropDownList ddl_Category = (DropDownList)gvr.FindControl("ddlCategory");
            FIN.BLL.HR.Jobs_BLL.fn_getJob4Category(ref ddl_Job, ddl_Category.SelectedValue);
        }

        protected void ddlPosition_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //MPEWorkDet.Show();
                GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
                LoadGrade4Position(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ddlDeptChange", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void LoadGrade4Position(GridViewRow gvr)
        {
            DropDownList ddl_Grade = (DropDownList)gvr.FindControl("ddlGrade");
            DropDownList ddl_Position = (DropDownList)gvr.FindControl("ddlPosition");
            FIN.BLL.HR.Grades_BLL.fn_getGradeName4Position(ref ddl_Grade, ddl_Position.SelectedValue);
        }

        protected void gvWorkDet_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;


                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvWorkDet.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    AssignTo_WD_GridControl(gvr, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }

                    BindWorkDetails();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CalendarEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void AssignTo_WD_GridControl(GridViewRow gvr, string GMode, int rowindex)
        {


            System.Collections.SortedList slControls = new System.Collections.SortedList();
            DropDownList ddl_Department = gvr.FindControl("ddlDepartment") as DropDownList;
            DropDownList ddl_Designation = gvr.FindControl("ddlDesignation") as DropDownList;
            DropDownList ddl_Category = gvr.FindControl("ddlCategory") as DropDownList;
            DropDownList ddl_Grade = gvr.FindControl("ddlGrade") as DropDownList;
            DropDownList ddl_Job = gvr.FindControl("ddlJob") as DropDownList;
            DropDownList ddl_Position = gvr.FindControl("ddlPosition") as DropDownList;
            DropDownList ddl_ReportingTo = gvr.FindControl("ddlReportingTo") as DropDownList;
            DropDownList ddl_ContPlan = gvr.FindControl("ddlContPlan") as DropDownList;
            TextBox dtp_FromDate = gvr.FindControl("dtpFromDate") as TextBox;
            TextBox dtp_ToDate = gvr.FindControl("dtpToDate") as TextBox;
            TextBox txtPosinworkpermit = gvr.FindControl("txtPosinworkpermit") as TextBox;
            DropDownList ddlSection = gvr.FindControl("ddlSection") as DropDownList;
            DropDownList ddlLocation = gvr.FindControl("ddlLocation") as DropDownList;
            


            slControls[0] = ddl_Department;
            slControls[1] = ddl_Designation;
            slControls[2] = ddl_Category;
            slControls[3] = ddl_Grade;
            slControls[4] = ddl_Job;
            slControls[5] = ddl_Position;
            slControls[6] = dtp_FromDate;
            slControls[7] = dtp_FromDate;
            slControls[8] = dtp_ToDate;
            slControls[9] = ddlSection;
            slControls[10] = ddlLocation;

            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));

            ErrorCollection.Clear();
            string strCtrlTypes = FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" +
                FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" +
                FINAppConstants.TEXT_BOX + "~" + FINAppConstants.DATE_TIME + "~" + FINAppConstants.DATE_RANGE_VALIDATE + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST;
            string strMessage = Prop_File_Data["Department_Name_P"] + " ~ " + Prop_File_Data["Designation_Name_P"] + " ~ " + Prop_File_Data["Category_P"] + " ~ " + Prop_File_Data["Grade_P"] + " ~ " + Prop_File_Data["Job_P"] + " ~ " + Prop_File_Data["Position_P"] + " ~ " + Prop_File_Data["Start_Date_P"] + " ~ " + Prop_File_Data["Start_Date_P"] + " ~ " + Prop_File_Data["Period_End_Date_P"] + " ~ " + Prop_File_Data["Section_P"] + " ~ " + Prop_File_Data["Location_P"] + "";
            //string strMessage = "DepartMentName ~ Desgination Name ~ Category ~ Grade ~ Job ~ Position  ~  Start Date ~  Start Date ~ Period End Date ";
            EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

            if (EmptyErrorCollection.Count > 0)
            {
                ErrorCollection = EmptyErrorCollection;
                return;

            }

            if (DBMethod.ConvertStringToDate(dtp_FromDate.Text) < DBMethod.ConvertStringToDate(txtDoj.Text))
            {
                ErrorCollection.Add("InvalidFromDate", " From Date Must Be Greater Then Date Of Join(DOJ) Date");
                return;
            }
            //if (Session["EmpWorkDetails"] != null)
            //{
            //    if (GMode == "A")
            //    {
            //        DataTable dt_tmp = (DataTable)Session["EmpWorkDetails"];
            //        string strCondition = "EFFECTIVE_TO_DT is null";
            //        strMessage = "Previous Record Found Not able to Add New Record ";
            //        ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            //        if (ErrorCollection.Count > 0)
            //        {

            //            return;
            //        }
            //    }
            //}

            HR_EMP_WORK_DTLS hR_EMP_WORK_DTLS = new HR_EMP_WORK_DTLS();
            if (GMode != "A")
            {
                using (IRepository<HR_EMP_WORK_DTLS> userCtx = new DataRepository<HR_EMP_WORK_DTLS>())
                {
                    hR_EMP_WORK_DTLS = userCtx.Find(r =>
                        (r.EMP_WH_ID == gvWorkDet.DataKeys[gvr.RowIndex].Values["EMP_WH_ID"].ToString())
                        ).SingleOrDefault();
                }
            }
            hR_EMP_WORK_DTLS.EMP_ID = hf_EMPID.Value;
            hR_EMP_WORK_DTLS.EMP_DEPT_ID = ddl_Department.SelectedValue.ToString();
            hR_EMP_WORK_DTLS.EMP_DESIG_ID = ddl_Designation.SelectedValue.ToString();
            hR_EMP_WORK_DTLS.EMP_SEC_ID = ddlSection.SelectedValue.ToString();
            hR_EMP_WORK_DTLS.LOC_ID = ddlLocation.SelectedValue.ToString();
            hR_EMP_WORK_DTLS.EMP_CATEGORY = ddl_Category.SelectedValue.ToString();
            hR_EMP_WORK_DTLS.EMP_GRADE_ID = ddl_Grade.SelectedValue.ToString();
            hR_EMP_WORK_DTLS.EMP_JOB_ID = ddl_Job.SelectedValue.ToString();
            hR_EMP_WORK_DTLS.EMP_POSITION_ID = ddl_Position.SelectedValue.ToString();
            hR_EMP_WORK_DTLS.POS_IN_WORK_PERMIT = txtPosinworkpermit.Text;
            hR_EMP_WORK_DTLS.EFFECTIVE_FROM_DT = DBMethod.ConvertStringToDate(dtp_FromDate.Text);

            hR_EMP_WORK_DTLS.EMP_ORG_ID = VMVServices.Web.Utils.OrganizationID;
            hR_EMP_WORK_DTLS.ENABLED_FLAG = "1";
            hR_EMP_WORK_DTLS.WORKFLOW_COMPLETION_STATUS = "1";

            if (ddl_ReportingTo.SelectedValue.ToString().Length == 0)
            {
                hR_EMP_WORK_DTLS.REPORTING_TO = null;
            }
            else
            {
                hR_EMP_WORK_DTLS.REPORTING_TO = ddl_ReportingTo.SelectedValue;
            }

            if (ddl_ContPlan.SelectedValue.ToString().Length == 0)
            {
                hR_EMP_WORK_DTLS.CONTINGENCY_PLAN = null;
            }
            else
            {
                hR_EMP_WORK_DTLS.CONTINGENCY_PLAN = ddl_ContPlan.SelectedValue;
            }

            if (dtp_ToDate.Text.ToString().Length > 0)
                hR_EMP_WORK_DTLS.EFFECTIVE_TO_DT = DBMethod.ConvertStringToDate(dtp_ToDate.Text);

            if (GMode == "A")
            {


                hR_EMP_WORK_DTLS.EMP_WH_ID = FINSP.GetSPFOR_SEQCode("EMP_WH_ID".ToString(), false, true);
                hR_EMP_WORK_DTLS.CREATED_BY = this.LoggedUserName;
                hR_EMP_WORK_DTLS.CREATED_DATE = DateTime.Today;
                Employee_BLL.SaveEntity(hR_EMP_WORK_DTLS, FINAppConstants.Add);
            }
            else
            {

                hR_EMP_WORK_DTLS.MODIFIED_BY = this.LoggedUserName;
                hR_EMP_WORK_DTLS.MODIFIED_DATE = DateTime.Today;
                Employee_BLL.SaveEntity(hR_EMP_WORK_DTLS, FINAppConstants.Update);
            }

        }

        protected void gvWorkDet_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvWorkDet.Rows[e.RowIndex] as GridViewRow;

                if (gvr == null)
                {
                    return;
                }

                AssignTo_WD_GridControl(gvr, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvWorkDet.EditIndex = -1;
                BindWorkDetails();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EmpWDRowUpdate", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }








        protected void btnQualDet_Click(object sender, EventArgs e)
        {

        }

        protected void imgbtnQualDet_Click(object sender, ImageClickEventArgs e)
        {
            try
            {

                BindQualificationDetails();
                //MPEQualDet.Show();
                SetVisiableFalsePanel();
                pnlQualDet.Visible = true;

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EMPQualDetails", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);

                }
            }
        }
        private void BindQualificationDetails()
        {
            try
            {
                ErrorCollection.Clear();
                //MPEQualDet.Show();
                DataTable dt_workDet = DBMethod.ExecuteQuery(FIN.DAL.HR.Employee_DAL.getQualificationDetails(hf_EMPID.Value.ToString())).Tables[0];
                bol_WorkDet = false;
                DataTable dt_tmp = dt_workDet.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dt_tmp.Rows.Add(dr);
                    bol_WorkDet = true;
                }
                gvQualDet.DataSource = dt_tmp;
                gvQualDet.DataBind();
                GridViewRow gvr = gvQualDet.FooterRow;
                FillFooterQualDetDDL(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EmpQualDetBindGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void FillFooterQualDetDDL(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddl_QualType = tmpgvr.FindControl("ddlQualType") as DropDownList;
                FIN.BLL.Lookup_BLL.GetLookUpValues(ref ddl_QualType, "QT");


                if (gvQualDet.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                {
                    ddl_QualType.SelectedValue = gvQualDet.DataKeys[gvQualDet.EditIndex].Values[FINColumnConstants.QUALI_TYPE].ToString();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Qual_DDLFIll", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        protected void gvQualDet_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_WorkDet)
                        e.Row.Visible = false;
                    else
                    {
                        CheckBox chk_box = (CheckBox)e.Row.FindControl("chkQualDetPrimay");

                        if (gvQualDet.DataKeys[e.Row.RowIndex].Values["PRIMARY_FLAG"].ToString() == "1")
                        {
                            chk_box.Checked = true;
                        }
                        else
                        {
                            chk_box.Checked = false;
                        }

                    }

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EmpQualDetRowDataBound", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }
        protected void gvQualDet_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {

            gvQualDet.EditIndex = -1;

            BindQualificationDetails();

        }
        protected void gvQualDet_RowCreated(object sender, GridViewRowEventArgs e)
        {
            //try
            //{
            //    ErrorCollection.Clear();
            //    if (e.Row.RowType == DataControlRowType.EmptyDataRow)
            //    {
            //        GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
            //        gvWorkDet.Controls[0].Controls.AddAt(0, gvr);
            //    }
            //}
            //catch (Exception ex)
            //{
            //    ErrorCollection.Add("AccountCodesEntry_RowCreated", ex.Message);
            //}
            //finally
            //{
            //    if (ErrorCollection.Count > 0)
            //    {
            //        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
            //        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
            //    }
            //}

        }


        protected void gvQualDet_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;


                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvQualDet.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    AssignTo_QD_GridControl(gvr, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }

                    BindQualificationDetails();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CalendarEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void AssignTo_QD_GridControl(GridViewRow gvr, string GMode, int rowindex)
        {


            System.Collections.SortedList slControls = new System.Collections.SortedList();
            DropDownList ddl_QualType = gvr.FindControl("ddlQualType") as DropDownList;
            TextBox txt_QualName = gvr.FindControl("txtQualName") as TextBox;
            TextBox txt_Duration = gvr.FindControl("txtDuration") as TextBox;
            CheckBox chk_QualDetPrimay = gvr.FindControl("chkQualDetPrimay") as CheckBox;
            TextBox dtp_FromDate = gvr.FindControl("dtpFromDate") as TextBox;
            TextBox dtp_ToDate = gvr.FindControl("dtpToDate") as TextBox;



            slControls[0] = ddl_QualType;
            slControls[1] = txt_QualName;
            slControls[2] = txt_Duration;
            slControls[3] = dtp_FromDate;
            slControls[4] = dtp_FromDate;
            slControls[5] = dtp_ToDate;

            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));

            ErrorCollection.Clear();
            string strCtrlTypes = FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" +
                FINAppConstants.TEXT_BOX + "~" + FINAppConstants.DATE_TIME + "~" + FINAppConstants.DATE_RANGE_VALIDATE;
            string strMessage = Prop_File_Data["Qualification_Type_P"] + " ~ " + Prop_File_Data["Qualification_Name_P"] + " ~ " + Prop_File_Data["Duration_P"] + " ~ " + Prop_File_Data["Effective_Date_P"] + " ~ " + Prop_File_Data["Effective_Date_P"] + " ~ " + Prop_File_Data["End_Date_P"] + "";
            //string strMessage = "Qualification Type ~ Qualification Name ~ Duration ~  Start Date ~  Start Date ~ Period End Date ";
            EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

            if (EmptyErrorCollection.Count > 0)
            {
                ErrorCollection = EmptyErrorCollection;
                return;
            }



            //string strCondition = "ACCT_CODE_SEGMENT_ID='" + ddl_Segment.SelectedItem.Text.Trim().ToUpper() + "'";
            //strMessage = FINMessageConstatns.RecordAlreadyExists;
            //ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            //if (ErrorCollection.Count > 0)
            //{

            //    return ;
            //}

            HR_QUALIFICATIONS hR_QUALIFICATIONS = new HR_QUALIFICATIONS();
            if (GMode != "A")
            {
                using (IRepository<HR_QUALIFICATIONS> userCtx = new DataRepository<HR_QUALIFICATIONS>())
                {
                    hR_QUALIFICATIONS = userCtx.Find(r =>
                        (r.QUALI_ID == gvQualDet.DataKeys[gvr.RowIndex].Values["QUALI_ID"].ToString())
                        ).SingleOrDefault();
                }
            }
            hR_QUALIFICATIONS.EMP_ID = hf_EMPID.Value;
            hR_QUALIFICATIONS.QUALI_TYPE = ddl_QualType.SelectedValue.ToString();
            hR_QUALIFICATIONS.QUALI_NAME = txt_QualName.Text.ToString();
            hR_QUALIFICATIONS.DURATION = decimal.Parse(txt_Duration.Text.ToString());

            hR_QUALIFICATIONS.PRIMARY_FLAG = FINAppConstants.N;
            if (chk_QualDetPrimay.Checked)
                hR_QUALIFICATIONS.PRIMARY_FLAG = FINAppConstants.Y;
            hR_QUALIFICATIONS.ENABLED_FLAG = FINAppConstants.Y;
            hR_QUALIFICATIONS.EFFECTIVE_FROM_DT = DBMethod.ConvertStringToDate(dtp_FromDate.Text);
            hR_QUALIFICATIONS.ENABLED_FLAG = FINAppConstants.Y;
            hR_QUALIFICATIONS.WORKFLOW_COMPLETION_STATUS = FINAppConstants.Y;
            if (dtp_ToDate.Text.ToString().Length > 0)
                hR_QUALIFICATIONS.EFFECTIVE_TO_DT = DBMethod.ConvertStringToDate(dtp_ToDate.Text);

            if (GMode == "A")
            {
                hR_QUALIFICATIONS.QUALI_ID = FINSP.GetSPFOR_SEQCode("EMP_Q_ID".ToString(), false, true);
                hR_QUALIFICATIONS.CREATED_BY = this.LoggedUserName;
                hR_QUALIFICATIONS.CREATED_DATE = DateTime.Today;
                Employee_BLL.SaveEntity(hR_QUALIFICATIONS, FINAppConstants.Add, true);
            }
            else
            {
                hR_QUALIFICATIONS.MODIFIED_BY = this.LoggedUserName;
                hR_QUALIFICATIONS.MODIFIED_DATE = DateTime.Today;
                Employee_BLL.SaveEntity(hR_QUALIFICATIONS, FINAppConstants.Update, false);
            }


        }

        protected void gvQualDet_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvQualDet.EditIndex = e.NewEditIndex;
            BindQualificationDetails();
            GridViewRow gvr = gvQualDet.Rows[e.NewEditIndex];
            FillFooterQualDetDDL(gvr);
            //MPEQualDet.Show();
        }

        protected void gvQualDet_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvQualDet.Rows[e.RowIndex] as GridViewRow;

                if (gvr == null)
                {
                    return;
                }

                AssignTo_QD_GridControl(gvr, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvQualDet.EditIndex = -1;
                BindQualificationDetails();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EmpQDRowUpdate", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }





        protected void imgbtnPasswrodDet_Click(object sender, ImageClickEventArgs e)
        {
            try
            {

                BindPasswordDetails();
                //MPEQualDet.Show();
                SetVisiableFalsePanel();
                pnlPassworddet.Visible = true;

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EMPQualDetails", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);

                }
            }
        }
        private void BindPasswordDetails()
        {
            try
            {
                ErrorCollection.Clear();
                //MPEQualDet.Show();
                DataTable dt_workDet = DBMethod.ExecuteQuery(FIN.DAL.HR.Employee_DAL.getPasswordDetails(hf_EMPID.Value.ToString())).Tables[0];
                bol_WorkDet = false;
                DataTable dt_tmp = dt_workDet.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr["ENABLED_FLAG"] = "FALSE";
                    dt_tmp.Rows.Add(dr);
                    bol_WorkDet = true;
                }
                gvPasswordDet.DataSource = dt_tmp;
                gvPasswordDet.DataBind();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EmpQualDetBindGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void gvPasswordDet_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_WorkDet)
                        e.Row.Visible = false;
                    else
                    {
                        CheckBox chk_box = (CheckBox)e.Row.FindControl("chkPasswordActive");

                        if (gvPasswordDet.DataKeys[e.Row.RowIndex].Values["ENABLED_FLAG"].ToString() == "1")
                        {
                            chk_box.Checked = true;
                        }
                        else
                        {
                            chk_box.Checked = false;
                        }

                    }

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EmpQualDetRowDataBound", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }
        protected void gvPasswordDet_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {

            gvPasswordDet.EditIndex = -1;

            BindPasswordDetails();

        }
        protected void gvPasswordDet_RowCreated(object sender, GridViewRowEventArgs e)
        {
            //try
            //{
            //    ErrorCollection.Clear();
            //    if (e.Row.RowType == DataControlRowType.EmptyDataRow)
            //    {
            //        GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
            //        gvWorkDet.Controls[0].Controls.AddAt(0, gvr);
            //    }
            //}
            //catch (Exception ex)
            //{
            //    ErrorCollection.Add("AccountCodesEntry_RowCreated", ex.Message);
            //}
            //finally
            //{
            //    if (ErrorCollection.Count > 0)
            //    {
            //        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
            //        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
            //    }
            //}

        }


        protected void gvPasswordDet_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;


                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvPasswordDet.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    AssignTo_PD_GridControl(gvr, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }

                    BindPasswordDetails();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CalendarEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void AssignTo_PD_GridControl(GridViewRow gvr, string GMode, int rowindex)
        {


            System.Collections.SortedList slControls = new System.Collections.SortedList();

            TextBox txt_UserName = gvr.FindControl("txtUserName") as TextBox;
            TextBox txt_Password = gvr.FindControl("txtPassword") as TextBox;
            CheckBox chk_PasswordActive = gvr.FindControl("chkPasswordActive") as CheckBox;
            TextBox dtp_PDFromDate = gvr.FindControl("dtpPDFromDate") as TextBox;
            TextBox dtp_PDToDate = gvr.FindControl("dtpPDToDate") as TextBox;


            txt_UserName.Text = txtEmpNo.Text;

            slControls[0] = txt_UserName;
            slControls[1] = txt_Password;
            slControls[2] = dtp_PDFromDate;
            slControls[3] = dtp_PDFromDate;
            slControls[4] = dtp_PDToDate;


            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));

            ErrorCollection.Clear();
            string strCtrlTypes = FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" +
                FINAppConstants.TEXT_BOX + "~" + FINAppConstants.DATE_TIME + "~" + FINAppConstants.DATE_RANGE_VALIDATE;
            string strMessage = Prop_File_Data["User_Name_P"] + " ~ " + Prop_File_Data["Password_P"] + " ~ " + Prop_File_Data["From_Date_P"] + " ~ " + Prop_File_Data["From_Date_P"] + " ~ " + Prop_File_Data["End_Date_P"];
            //string strMessage = "Qualification Type ~ Qualification Name ~ Duration ~  Start Date ~  Start Date ~ Period End Date ";
            EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

            if (EmptyErrorCollection.Count > 0)
            {
                ErrorCollection = EmptyErrorCollection;
                return;
            }



            //string strCondition = "ACCT_CODE_SEGMENT_ID='" + ddl_Segment.SelectedItem.Text.Trim().ToUpper() + "'";
            //strMessage = FINMessageConstatns.RecordAlreadyExists;
            //ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            //if (ErrorCollection.Count > 0)
            //{

            //    return ;
            //}

            HR_EMP_PASSWORD_DTLS hR_EMP_PASSWORD_DTLS = new HR_EMP_PASSWORD_DTLS();
            if (GMode != "A")
            {
                using (IRepository<HR_EMP_PASSWORD_DTLS> userCtx = new DataRepository<HR_EMP_PASSWORD_DTLS>())
                {
                    hR_EMP_PASSWORD_DTLS = userCtx.Find(r =>
                        (r.EPD_ID == gvPasswordDet.DataKeys[gvr.RowIndex].Values["EPD_ID"].ToString())
                        ).SingleOrDefault();
                }
            }
            hR_EMP_PASSWORD_DTLS.EMP_ID = hf_EMPID.Value;
            hR_EMP_PASSWORD_DTLS.USER_ID = txt_UserName.Text.ToString();
            hR_EMP_PASSWORD_DTLS.USER_PASSWORD = txt_Password.Text.ToString();
            hR_EMP_PASSWORD_DTLS.ENABLED_FLAG = FINAppConstants.N;
            if (chk_PasswordActive.Checked)
                hR_EMP_PASSWORD_DTLS.ENABLED_FLAG = FINAppConstants.Y;

            hR_EMP_PASSWORD_DTLS.WORKFLOW_COMPLETION_STATUS = FINAppConstants.Y;

            hR_EMP_PASSWORD_DTLS.EFFECTIVE_FROM = DBMethod.ConvertStringToDate(dtp_PDFromDate.Text);

            if (dtp_PDToDate.Text.ToString().Length > 0)
                hR_EMP_PASSWORD_DTLS.EFFECTIVE_TO = DBMethod.ConvertStringToDate(dtp_PDToDate.Text);

            if (GMode == "A")
            {
                hR_EMP_PASSWORD_DTLS.EPD_ID = FINSP.GetSPFOR_SEQCode("EMP_PW_ID".ToString(), false, true);
                hR_EMP_PASSWORD_DTLS.CREATED_BY = this.LoggedUserName;
                hR_EMP_PASSWORD_DTLS.CREATED_DATE = DateTime.Today;
                Employee_BLL.SaveEntity(hR_EMP_PASSWORD_DTLS, FINAppConstants.Add, true);
            }
            else
            {
                hR_EMP_PASSWORD_DTLS.MODIFIED_BY = this.LoggedUserName;
                hR_EMP_PASSWORD_DTLS.MODIFIED_DATE = DateTime.Today;
                Employee_BLL.SaveEntity(hR_EMP_PASSWORD_DTLS, FINAppConstants.Update, false);
            }


        }

        protected void gvPasswordDet_RowEditing(object sender, GridViewEditEventArgs e)
        {

            gvPasswordDet.EditIndex = e.NewEditIndex;
            BindPasswordDetails();
            GridViewRow gvr = gvPasswordDet.Rows[e.NewEditIndex];
            // FillFooterQualDetDDL(gvr);
            //MPEQualDet.Show();
        }

        protected void gvPasswordDet_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvPasswordDet.Rows[e.RowIndex] as GridViewRow;

                if (gvr == null)
                {
                    return;
                }

                AssignTo_PD_GridControl(gvr, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvPasswordDet.EditIndex = -1;
                BindPasswordDetails();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EmpQDRowUpdate", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }





        protected void btnSkillsDet_Click(object sender, EventArgs e)
        {

            try
            {

                BindSkillsDetails();
                //MPESkillsDet.Show();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EMPSkillsDetails", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);

                }
            }
        }

        protected void imgbtnSkillDet_Click(object sender, ImageClickEventArgs e)
        {
            try
            {

                BindSkillsDetails();
                //MPESkillsDet.Show();
                SetVisiableFalsePanel();
                pnlSkillsDet.Visible = true;

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EMPSkillsDetails", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);

                }

            }
        }
        private void BindSkillsDetails()
        {
            try
            {
                ErrorCollection.Clear();
                //MPESkillsDet.Show();
                DataTable dt_workDet = DBMethod.ExecuteQuery(FIN.DAL.HR.Employee_DAL.getSkillDetails(hf_EMPID.Value.ToString())).Tables[0];
                bol_WorkDet = false;
                DataTable dt_tmp = dt_workDet.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dt_tmp.Rows.Add(dr);
                    bol_WorkDet = true;
                }
                gvSkillsDet.DataSource = dt_tmp;
                gvSkillsDet.DataBind();
                GridViewRow gvr = gvSkillsDet.FooterRow;
                FillFooterSkillsDetDDL(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EmpSkillsDetBindGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void FillFooterSkillsDetDDL(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddl_SkillCat = tmpgvr.FindControl("ddlSkillCat") as DropDownList;
                DropDownList ddl_SkillType = tmpgvr.FindControl("ddlSkillType") as DropDownList;
                FIN.BLL.Lookup_BLL.GetLookUpValues(ref ddl_SkillCat, "SC");
                FIN.BLL.Lookup_BLL.GetLookUpValues(ref ddl_SkillType, "SKT");

                if (gvSkillsDet.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                {
                    ddl_SkillCat.SelectedValue = gvSkillsDet.DataKeys[gvSkillsDet.EditIndex].Values[FINColumnConstants.SKILL_CATEGORY].ToString();
                    ddl_SkillType.SelectedValue = gvSkillsDet.DataKeys[gvSkillsDet.EditIndex].Values[FINColumnConstants.EMP_SKILL_TYPE].ToString();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Skills_DDLFIll", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        protected void gvSkillsDet_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_WorkDet)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EmpSkillsDetRowDataBound", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }
        protected void gvSkillsDet_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {

            gvSkillsDet.EditIndex = -1;

            BindSkillsDetails();

        }
        protected void gvSkillsDet_RowCreated(object sender, GridViewRowEventArgs e)
        {
            //try
            //{
            //    ErrorCollection.Clear();
            //    if (e.Row.RowType == DataControlRowType.EmptyDataRow)
            //    {
            //        GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
            //        gvWorkDet.Controls[0].Controls.AddAt(0, gvr);
            //    }
            //}
            //catch (Exception ex)
            //{
            //    ErrorCollection.Add("AccountCodesEntry_RowCreated", ex.Message);
            //}
            //finally
            //{
            //    if (ErrorCollection.Count > 0)
            //    {
            //        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
            //        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
            //    }
            //}

        }


        protected void gvSkillsDet_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;


                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvSkillsDet.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    AssignTo_SD_GridControl(gvr, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }

                    BindSkillsDetails();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("SkillDet", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void AssignTo_SD_GridControl(GridViewRow gvr, string GMode, int rowindex)
        {


            System.Collections.SortedList slControls = new System.Collections.SortedList();
            DropDownList ddl_SkillCat = gvr.FindControl("ddlSkillCat") as DropDownList;
            DropDownList ddl_SkillType = gvr.FindControl("ddlSkillType") as DropDownList;
            TextBox txt_SkillName = gvr.FindControl("txtSkillName") as TextBox;
            TextBox txt_ValDet = gvr.FindControl("txtValDet") as TextBox;
            TextBox txt_Remarks = gvr.FindControl("txtRemarks") as TextBox;
            TextBox dtp_FromDate = gvr.FindControl("dtpFromDate") as TextBox;




            slControls[0] = ddl_SkillCat;
            slControls[1] = ddl_SkillType;
            slControls[2] = txt_SkillName;
            slControls[3] = txt_ValDet;
            slControls[4] = dtp_FromDate;
            slControls[5] = dtp_FromDate;

            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));
            ErrorCollection.Clear();
            string strCtrlTypes = FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.TEXT_BOX + "~" +
                FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.DATE_TIME;
            string strMessage = Prop_File_Data["Skill_Category_P"] + " ~ " + Prop_File_Data["Skill_Type_P"] + " ~ " + Prop_File_Data["Skill_Name_P"] + " ~ " + Prop_File_Data["Validity_Details_P"] + " ~ " + Prop_File_Data["Effective_Date_P"] + " ~ " + Prop_File_Data["Effective_Date_P"] + "";
            // string strMessage = "Skills Category ~ Skills Type ~ Skill Name ~Skill Name ~  Effectiv From ~  Effective From ";
            EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

            if (EmptyErrorCollection.Count > 0)
            {
                ErrorCollection = EmptyErrorCollection;

            }

            if (ErrorCollection.Count > 0)
            {
                return;
            }


            //string strCondition = "ACCT_CODE_SEGMENT_ID='" + ddl_Segment.SelectedItem.Text.Trim().ToUpper() + "'";
            //strMessage = FINMessageConstatns.RecordAlreadyExists;
            //ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            //if (ErrorCollection.Count > 0)
            //{

            //    return ;
            //}

            HR_EMP_SKILL_DTLS hR_EMP_SKILL_DTLS = new HR_EMP_SKILL_DTLS();
            if (GMode != "A")
            {
                using (IRepository<HR_EMP_SKILL_DTLS> userCtx = new DataRepository<HR_EMP_SKILL_DTLS>())
                {
                    hR_EMP_SKILL_DTLS = userCtx.Find(r =>
                        (r.EMP_SKILL_ID == gvSkillsDet.DataKeys[gvr.RowIndex].Values["EMP_SKILL_ID"].ToString())
                        ).SingleOrDefault();
                }
            }
            hR_EMP_SKILL_DTLS.EMP_ID = hf_EMPID.Value;
            hR_EMP_SKILL_DTLS.SKILL_CATEGORY = ddl_SkillCat.SelectedValue.ToString();
            hR_EMP_SKILL_DTLS.EMP_SKILL_TYPE = ddl_SkillType.SelectedValue.ToString();
            hR_EMP_SKILL_DTLS.EMP_SKILL_NAME = txt_SkillName.Text;
            hR_EMP_SKILL_DTLS.EMP_VALIDITY_DTLS = txt_ValDet.Text;
            hR_EMP_SKILL_DTLS.EMP_REMARKS = txt_Remarks.Text;
            hR_EMP_SKILL_DTLS.ENABLED_FLAG = FINAppConstants.Y;
            hR_EMP_SKILL_DTLS.EFFECTIVE_FROM = DBMethod.ConvertStringToDate(dtp_FromDate.Text);
            hR_EMP_SKILL_DTLS.WORKFLOW_COMPLETION_STATUS = FINAppConstants.Y;
            if (GMode == "A")
            {
                hR_EMP_SKILL_DTLS.EMP_SKILL_ID = FINSP.GetSPFOR_SEQCode("EMP_SK_ID".ToString(), false, true);
                hR_EMP_SKILL_DTLS.CREATED_BY = this.LoggedUserName;
                hR_EMP_SKILL_DTLS.CREATED_DATE = DateTime.Today;
                Employee_BLL.SaveEntity(hR_EMP_SKILL_DTLS, FINAppConstants.Add);
            }
            else
            {
                hR_EMP_SKILL_DTLS.MODIFIED_BY = this.LoggedUserName;
                hR_EMP_SKILL_DTLS.MODIFIED_DATE = DateTime.Today;
                Employee_BLL.SaveEntity(hR_EMP_SKILL_DTLS, FINAppConstants.Update);
            }

        }

        protected void gvSkillsDet_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvSkillsDet.Rows[e.RowIndex] as GridViewRow;

                if (gvr == null)
                {
                    return;
                }

                AssignTo_SD_GridControl(gvr, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvSkillsDet.EditIndex = -1;
                BindSkillsDetails();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EmpSDRowUpdate", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }







        protected void btnBankDetails_Click(object sender, EventArgs e)
        {
            try
            {

                BindEmpBankDetails();
                //MPEEmpBank.Show();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EMPEmpBankDetails", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);

                }
            }
        }
        protected void imgbtnBankDetails_Click(object sender, ImageClickEventArgs e)
        {
            try
            {

                BindEmpBankDetails();
                //MPEEmpBank.Show();
                SetVisiableFalsePanel();
                pnlEmpBank.Visible = true;

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EMPEmpBankDetails", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);

                }
            }
        }

        private void BindEmpBankDetails()
        {
            try
            {
                ErrorCollection.Clear();
                //MPEEmpBank.Show();
                DataTable dt_workDet = DBMethod.ExecuteQuery(FIN.DAL.HR.Employee_DAL.getBankDetails(hf_EMPID.Value.ToString())).Tables[0];
                bol_WorkDet = false;
                DataTable dt_tmp = dt_workDet.Copy();
                Session["EMPBankDetails"] = dt_workDet;
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dt_tmp.Rows.Add(dr);
                    bol_WorkDet = true;
                }
                gvEmpBank.DataSource = dt_tmp;
                gvEmpBank.DataBind();
                GridViewRow gvr = gvEmpBank.FooterRow;
                FillFooterEmpBankDetDDL(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EmpEmpBankDetBindGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void FillFooterEmpBankDetDDL(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddl_BankName = tmpgvr.FindControl("ddlBankName") as DropDownList;
                FIN.BLL.CA.Bank_BLL.fn_getBankName(ref ddl_BankName);

                if (gvEmpBank.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                {
                    ddl_BankName.SelectedValue = gvEmpBank.DataKeys[gvEmpBank.EditIndex].Values[FINColumnConstants.EMP_BANK_CODE].ToString();
                    getBankBranch(tmpgvr);
                    DropDownList ddl_BranchName = tmpgvr.FindControl("ddlBranchName") as DropDownList;
                    ddl_BranchName.SelectedValue = gvEmpBank.DataKeys[gvEmpBank.EditIndex].Values[FINColumnConstants.EMP_BANK_BRANCH_CODE].ToString();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EmpBank_DDLFIll", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        protected void gvEmpBank_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_WorkDet)
                        e.Row.Visible = false;
                    else
                    {
                        CheckBox chk_box = (CheckBox)e.Row.FindControl("chkEmpBankActive");

                        if (gvEmpBank.DataKeys[e.Row.RowIndex].Values["ENABLED_FLAG"].ToString() == "1")
                        {
                            chk_box.Checked = true;
                        }
                        else
                        {
                            chk_box.Checked = false;
                        }

                    }

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EmpEmpBankDetRowDataBound", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }
        protected void gvEmpBank_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {

            gvEmpBank.EditIndex = -1;

            BindEmpBankDetails();

        }
        protected void gvEmpBank_RowCreated(object sender, GridViewRowEventArgs e)
        {
            //try
            //{
            //    ErrorCollection.Clear();
            //    if (e.Row.RowType == DataControlRowType.EmptyDataRow)
            //    {
            //        GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
            //        gvWorkDet.Controls[0].Controls.AddAt(0, gvr);
            //    }
            //}
            //catch (Exception ex)
            //{
            //    ErrorCollection.Add("AccountCodesEntry_RowCreated", ex.Message);
            //}
            //finally
            //{
            //    if (ErrorCollection.Count > 0)
            //    {
            //        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
            //        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
            //    }
            //}

        }


        protected void gvEmpBank_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;


                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvEmpBank.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    AssignTo_EB_GridControl(gvr, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }

                    BindEmpBankDetails();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EmpBankDet", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void AssignTo_EB_GridControl(GridViewRow gvr, string GMode, int rowindex)
        {


            System.Collections.SortedList slControls = new System.Collections.SortedList();
            TextBox txt_AccNumber = gvr.FindControl("txtAccNumber") as TextBox;
            TextBox txtIBANNumber = gvr.FindControl("txtIBANNumber") as TextBox;
            DropDownList ddl_BankName = gvr.FindControl("ddlBankName") as DropDownList;
            DropDownList ddl_BranchName = gvr.FindControl("ddlBranchName") as DropDownList;
            TextBox dtp_FromDate = gvr.FindControl("dtpFromDate") as TextBox;
            TextBox dtp_ToDate = gvr.FindControl("dtpToDate") as TextBox;
            CheckBox chk_EmpBankActive = gvr.FindControl("chkEmpBankActive") as CheckBox;



            slControls[0] = txt_AccNumber;
            slControls[1] = ddl_BankName;
            slControls[2] = ddl_BranchName;
            slControls[3] = dtp_FromDate;
            slControls[4] = dtp_FromDate;
            slControls[5] = dtp_ToDate;
            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));
            ErrorCollection.Clear();
            string strCtrlTypes = FINAppConstants.TEXT_BOX + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" +
                FINAppConstants.TEXT_BOX + "~" + FINAppConstants.DATE_TIME + "~" + FINAppConstants.DATE_RANGE_VALIDATE;
            string strMessage = Prop_File_Data["Account_Number_P"] + " ~ " + Prop_File_Data["Bank_Name_P"] + " ~ " + Prop_File_Data["Branch_Name_P"] + " ~ " + Prop_File_Data["Effective_Date_P"] + " ~ " + Prop_File_Data["Effective_Date_P"] + " ~ " + Prop_File_Data["End_Date_P"] + "";
            //string strMessage = "Account Number ~ Bank Name ~ BranchName ~ EffectiveFrom ~  Effectiv From ~  Effective To ";
            EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

            if (EmptyErrorCollection.Count > 0)
            {
                ErrorCollection = EmptyErrorCollection;
                return;

            }


            DataTable dt_tmp = (DataTable)Session["EMPBankDetails"];

            if (GMode != "A")
            {
                dt_tmp.Rows.RemoveAt(rowindex);

            }
            string strCondition = "EMP_BANK_ACCT_CODE='" + txt_AccNumber.Text + "'";
            strMessage = FINMessageConstatns.RecordAlreadyExists;
            ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            if (ErrorCollection.Count > 0)
            {

                return;
            }

            HR_EMP_BANK_DTLS hR_EMP_BANK_DTLS = new HR_EMP_BANK_DTLS();

            if (GMode != "A")
            {
                using (IRepository<HR_EMP_BANK_DTLS> userCtx = new DataRepository<HR_EMP_BANK_DTLS>())
                {
                    hR_EMP_BANK_DTLS = userCtx.Find(r =>
                         (r.PK_ID == int.Parse(gvEmpBank.DataKeys[gvr.RowIndex].Values["PK_ID"].ToString()))
                        ).SingleOrDefault();
                }
            }


            hR_EMP_BANK_DTLS.EMP_ID = hf_EMPID.Value;

            hR_EMP_BANK_DTLS.EMP_BANK_CODE = ddl_BankName.SelectedValue.ToString();
            hR_EMP_BANK_DTLS.EMP_BANK_BRANCH_CODE = ddl_BranchName.SelectedValue.ToString();
            hR_EMP_BANK_DTLS.ENABLED_FLAG = FINAppConstants.N;
            if (chk_EmpBankActive.Checked)
                hR_EMP_BANK_DTLS.ENABLED_FLAG = FINAppConstants.Y;

            hR_EMP_BANK_DTLS.EFFECTIVE_FROM_DT = DBMethod.ConvertStringToDate(dtp_FromDate.Text);
            if (dtp_ToDate.Text.ToString().Length > 0)
                hR_EMP_BANK_DTLS.EFFECTIVE_TO_DT = DBMethod.ConvertStringToDate(dtp_ToDate.Text);

            hR_EMP_BANK_DTLS.EMP_BANK_ACCT_CODE = txt_AccNumber.Text;
            hR_EMP_BANK_DTLS.EMP_IBAN_NUM = txtIBANNumber.Text;
            hR_EMP_BANK_DTLS.WORKFLOW_COMPLETION_STATUS = FINAppConstants.Y;
            if (GMode == "A")
            {
                hR_EMP_BANK_DTLS.CREATED_BY = this.LoggedUserName;
                hR_EMP_BANK_DTLS.CREATED_DATE = DateTime.Now;
                hR_EMP_BANK_DTLS.PK_ID = DBMethod.GetPrimaryKeyValue("HR_EMP_BANK_DTLS_SEQ");
                Employee_BLL.SaveEntity(hR_EMP_BANK_DTLS, FINAppConstants.Add);
            }
            else
            {
                hR_EMP_BANK_DTLS.MODIFIED_BY = this.LoggedUserName;
                hR_EMP_BANK_DTLS.MODIFIED_DATE = DateTime.Now;
                Employee_BLL.SaveEntity(hR_EMP_BANK_DTLS, FINAppConstants.Update);
            }

        }

        protected void gvEmpBank_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvEmpBank.Rows[e.RowIndex] as GridViewRow;

                if (gvr == null)
                {
                    return;
                }

                AssignTo_EB_GridControl(gvr, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvEmpBank.EditIndex = -1;
                BindEmpBankDetails();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EmpEmpBankRowUpdate", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void ddlJob_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //MPEWorkDet.Show();
                GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
                getPosition(gvr);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ddlDeptChange", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        private void getPosition(GridViewRow gvr)
        {
            DropDownList ddlPosition = (DropDownList)gvr.FindControl("ddlPosition");
            DropDownList ddlJob = (DropDownList)gvr.FindControl("ddlJob");
            FIN.BLL.HR.Position_BLL.fn_GetPositionNameBasedonJob(ref ddlPosition, ddlJob.SelectedValue);

        }

        protected void txtAddress1_TextChanged(object sender, EventArgs e)
        {

        }

        protected void ddlBankName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //MPEEmpBank.Show();
                GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
                getBankBranch(gvr);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ddlDeptChange", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void getBankBranch(GridViewRow gvr)
        {
            DropDownList ddl_BankName = (DropDownList)gvr.FindControl("ddlBankName");
            DropDownList ddl_BranchName = (DropDownList)gvr.FindControl("ddlBranchName");
            FIN.BLL.CA.BankBranch_BLL.fn_getBranchName(ref ddl_BranchName, ddl_BankName.SelectedValue.ToString());
        }

        protected void imgAddressNext_Click(object sender, ImageClickEventArgs e)
        {
            lblAddSaved.Text = "Address";
            lblAddSaved.Visible = true;
            hfAddRecId.Value = (int.Parse(hfAddRecId.Value) + 1).ToString();
            DataTable dt_EmpAddess = (DataTable)Session["EmpAddress"];
            if (int.Parse(hfAddRecId.Value) >= dt_EmpAddess.Rows.Count - 1)
            {
                hfAddRecId.Value = (dt_EmpAddess.Rows.Count - 1).ToString();
                lblAddSaved.Visible = true;
                lblAddSaved.Text = "Address - Last Record ";
            }
            DisplayAddress(int.Parse(hfAddRecId.Value));

        }

        protected void imgAddressPrevious_Click(object sender, ImageClickEventArgs e)
        {
            lblAddSaved.Text = "Address";
            lblAddSaved.Visible = true;
            hfAddRecId.Value = (int.Parse(hfAddRecId.Value) - 1).ToString();
            if (int.Parse(hfAddRecId.Value) <= 0)
            {
                hfAddRecId.Value = "0";
                lblAddSaved.Visible = true;
                lblAddSaved.Text = "Address - First Record ";
            }
            DisplayAddress(int.Parse(hfAddRecId.Value));
        }

        protected void imgAddressLast_Click(object sender, ImageClickEventArgs e)
        {
            DataTable dt_EmpAddess = (DataTable)Session["EmpAddress"];
            hfAddRecId.Value = (dt_EmpAddess.Rows.Count - 1).ToString();
            DisplayAddress(int.Parse(hfAddRecId.Value));
        }

        protected void imgAddressFirst_Click(object sender, ImageClickEventArgs e)
        {
            hfAddRecId.Value = "0";
            DisplayAddress(int.Parse(hfAddRecId.Value));
        }

        protected void imgbtncontnext_Click(object sender, ImageClickEventArgs e)
        {
            lblContSave.Text = "Contact Details";
            lblContSave.Visible = true;
            hfContRecid.Value = (int.Parse(hfContRecid.Value) + 1).ToString();
            DataTable dt_EmpCont = (DataTable)Session["EMPContact"];
            if (int.Parse(hfContRecid.Value) >= dt_EmpCont.Rows.Count - 1)
            {
                hfContRecid.Value = (dt_EmpCont.Rows.Count - 1).ToString();
                lblContSave.Text = "Contact Details - Last Record ";
                lblContSave.Visible = true;
            }
            DisplayContDet(int.Parse(hfContRecid.Value));
        }

        protected void imtbtncontprevious_Click(object sender, ImageClickEventArgs e)
        {
            lblContSave.Text = "Contact Details";
            lblContSave.Visible = true;
            hfContRecid.Value = (int.Parse(hfContRecid.Value) - 1).ToString();
            if (int.Parse(hfContRecid.Value) <= 0)
            {
                hfContRecid.Value = "0";
                lblContSave.Text = "Contact Details - First Record ";
                lblContSave.Visible = true;
            }
            DisplayContDet(int.Parse(hfContRecid.Value));
        }

        protected void imgbtncontlast_Click(object sender, ImageClickEventArgs e)
        {
            DataTable dt_ContDet = (DataTable)Session["EMPContact"];
            hfContRecid.Value = (dt_ContDet.Rows.Count - 1).ToString();
            DisplayContDet(int.Parse(hfContRecid.Value));
        }

        protected void imgbtncontFirst_Click(object sender, ImageClickEventArgs e)
        {
            hfContRecid.Value = "0";
            DisplayContDet(int.Parse(hfContRecid.Value));
        }

        protected void gvSkillsDet_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvSkillsDet.EditIndex = e.NewEditIndex;
            BindSkillsDetails();
            GridViewRow gvr = gvSkillsDet.Rows[e.NewEditIndex];
            FillFooterSkillsDetDDL(gvr);
            //MPESkillsDet.Show();
        }

        protected void gvEmpBank_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvEmpBank.EditIndex = e.NewEditIndex;
            BindEmpBankDetails();
            GridViewRow gvr = gvEmpBank.Rows[e.NewEditIndex];
            FillFooterEmpBankDetDDL(gvr);
            //MPEEmpBank.Show();
        }

        protected void imgbtnPPDet_Click(object sender, ImageClickEventArgs e)
        {
            try
            {

                BindPassPortDetails();
                //MPEPassPortDet.Show();
                SetVisiableFalsePanel();
                pnlPPDet.Visible = true;

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EMPPPDetails", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);

                }
            }

        }

        private void BindPassPortDetails()
        {
            try
            {
                ErrorCollection.Clear();
                //MPEPassPortDet.Show();
                DataTable dt_workDet = DBMethod.ExecuteQuery(FIN.DAL.HR.Employee_DAL.getPassPortDetails(hf_EMPID.Value.ToString())).Tables[0];
                bol_WorkDet = false;
                DataTable dt_tmp = dt_workDet.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dt_tmp.Rows.Add(dr);
                    bol_WorkDet = true;
                }
                gvPPDet.DataSource = dt_tmp;
                gvPPDet.DataBind();
                GridViewRow gvr = gvPPDet.FooterRow;
                FillFooterPPDetDDL(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EmpQualDetBindGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void FillFooterPPDetDDL(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();
                DropDownList ddlIdType = tmpgvr.FindControl("ddlIdType") as DropDownList;
                DropDownList ddl_ppRelShip = tmpgvr.FindControl("ddlppRelShip") as DropDownList;
                DropDownList ddl_ppNationality = tmpgvr.FindControl("ddlppNationality") as DropDownList;

                FIN.BLL.Lookup_BLL.GetLookUpValues(ref ddl_ppRelShip, "RELT");
                FIN.BLL.Lookup_BLL.GetLookUpValues(ref ddl_ppNationality, "NATIONALITY");
                FIN.BLL.Lookup_BLL.GetLookUpValues(ref ddlIdType, "ID TYPE");

                if (gvPPDet.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                {
                    ddlIdType.SelectedValue = gvPPDet.DataKeys[gvPPDet.EditIndex].Values["LOOKUP_ID"].ToString();
                    ddl_ppRelShip.SelectedValue = gvPPDet.DataKeys[gvPPDet.EditIndex].Values["PASSPORT_RELATIONSHIP"].ToString();
                    ddl_ppNationality.SelectedValue = gvPPDet.DataKeys[gvPPDet.EditIndex].Values["PASSPORT_NATIONALITY"].ToString();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Qual_DDLFIll", ex.Message);
            }
            finally
            {

                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        protected void gvPPDet_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_WorkDet)
                        e.Row.Visible = false;
                    else
                    {
                        CheckBox chk_PPActive = (CheckBox)e.Row.FindControl("chkPPActive");

                        if (gvPPDet.DataKeys[e.Row.RowIndex].Values[FINColumnConstants.ENABLED_FLAG].ToString() == FINAppConstants.Y)
                        {
                            chk_PPActive.Checked = true;
                        }
                        else
                        {
                            chk_PPActive.Checked = false;
                        }

                    }

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EmpQualDetRowDataBound", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }
        protected void gvPPDet_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {

            gvPPDet.EditIndex = -1;

            BindPassPortDetails();

        }
        protected void gvPPDet_RowCreated(object sender, GridViewRowEventArgs e)
        {
            //try
            //{
            //    ErrorCollection.Clear();
            //    if (e.Row.RowType == DataControlRowType.EmptyDataRow)
            //    {
            //        GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
            //        gvWorkDet.Controls[0].Controls.AddAt(0, gvr);
            //    }
            //}
            //catch (Exception ex)
            //{
            //    ErrorCollection.Add("AccountCodesEntry_RowCreated", ex.Message);
            //}
            //finally
            //{
            //    if (ErrorCollection.Count > 0)
            //    {
            //        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
            //        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
            //    }
            //}

        }


        protected void gvPPDet_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;


                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvPPDet.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    AssignTo_PP_GridControl(gvr, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }

                    BindPassPortDetails();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PasspoareAPG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void AssignTo_PP_GridControl(GridViewRow gvr, string GMode, int rowindex)
        {


            System.Collections.SortedList slControls = new System.Collections.SortedList();

            DropDownList ddlIdType = gvr.FindControl("ddlIdType") as DropDownList;
            TextBox txt_PPName = gvr.FindControl("txtPPName") as TextBox;
            DropDownList ddl_ppRelShip = gvr.FindControl("ddlppRelShip") as DropDownList;
            TextBox txt_PPNumber = gvr.FindControl("txtPPNumber") as TextBox;
            DropDownList ddl_ppNationality = gvr.FindControl("ddlppNationality") as DropDownList;
            TextBox dtp_PPIssueDate = gvr.FindControl("dtpPPIssueDate") as TextBox;
            TextBox dtp_PPExpiryDate = gvr.FindControl("dtpPPExpiryDate") as TextBox;
            TextBox txt_PPIssuePlace = gvr.FindControl("txtPPIssuePlace") as TextBox;
            TextBox txt_PPIssuedBy = gvr.FindControl("txtPPIssuedBy") as TextBox;
            CheckBox chk_PPActive = gvr.FindControl("chkPPActive") as CheckBox;

            if (ddl_ppRelShip.SelectedValue.ToString().Length > 0)
            {
                if (ddl_ppRelShip.SelectedValue.ToString() == "Self")
                {
                    if (txt_PPName.Text.ToString().Trim().Length == 0)
                    {
                        txt_PPName.Text = txtFirstName.Text;
                    }
                }

            }


            slControls[0] = ddlIdType;
            //slControls[1] = txt_PPName;
            slControls[1] = ddl_ppRelShip;
            slControls[2] = txt_PPNumber;
            slControls[3] = ddl_ppNationality;
            // slControls[5] = dtp_PPIssueDate;
            // slControls[5] = dtp_PPExpiryDate;
            slControls[4] = txt_PPIssuePlace;
            slControls[5] = txt_PPIssuedBy;


            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));
            ErrorCollection.Clear();
            string strCtrlTypes = FINAppConstants.DROP_DOWN_LIST +  "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.DROP_DOWN_LIST + "~" +
                 FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX;
            //FINAppConstants.DATE_TIME + "~"  +FINAppConstants.DATE_RANGE_VALIDATE + "~" +

            // string strMessage = Prop_File_Data["Name_In_Passport_P"] + " ~ " + Prop_File_Data["RelationShip_P"] + " ~ " + Prop_File_Data["Passport_Number_P"] + " ~ " + Prop_File_Data["Passport_Nationality_P"] + " ~ " + Prop_File_Data["Issue_Date_P"] + " ~ " + Prop_File_Data["Expiry_Date_P"] + " ~ " + Prop_File_Data["Issue_Place_P"] + " ~ " + Prop_File_Data["Issued_By_P"] + "";
            //+ " ~ " + Prop_File_Data["Issue_Date_P"]+ " ~ " + Prop_File_Data["Expiry_Date_P"]
            string strMessage = Prop_File_Data["ID_Type_P"] + " ~ " + Prop_File_Data["Relationship_P"] + " ~ " + Prop_File_Data["ID_Number_P"] + " ~ " + Prop_File_Data["Nationality_P"] + " ~ " + Prop_File_Data["Issue_Place_P"] + " ~ " + Prop_File_Data["Issued_By_P"] + "";
            EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

            if (EmptyErrorCollection.Count > 0)
            {
                ErrorCollection = EmptyErrorCollection;
                return;
            }



            //string strCondition = "ACCT_CODE_SEGMENT_ID='" + ddl_Segment.SelectedItem.Text.Trim().ToUpper() + "'";
            //strMessage = FINMessageConstatns.RecordAlreadyExists;
            //ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            //if (ErrorCollection.Count > 0)
            //{

            //    return ;
            //}

            HR_EMP_PASSPORT_DETAILS hR_EMP_PASSPORT_DETAILS = new HR_EMP_PASSPORT_DETAILS();
            if (GMode != "A")
            {
                using (IRepository<HR_EMP_PASSPORT_DETAILS> userCtx = new DataRepository<HR_EMP_PASSPORT_DETAILS>())
                {
                    hR_EMP_PASSPORT_DETAILS = userCtx.Find(r =>
                        (r.PASS_TXN_ID == gvPPDet.DataKeys[gvr.RowIndex].Values["PASS_TXN_ID"].ToString())
                        ).SingleOrDefault();
                }
            }
            hR_EMP_PASSPORT_DETAILS.ID_TYPE = ddlIdType.SelectedValue;
            hR_EMP_PASSPORT_DETAILS.PASSPORT_EMP_ID = hf_EMPID.Value;
            hR_EMP_PASSPORT_DETAILS.NAME_IN_PASSPORT = txt_PPName.Text;
            hR_EMP_PASSPORT_DETAILS.PASSPORT_RELATIONSHIP = ddl_ppRelShip.SelectedValue;
            hR_EMP_PASSPORT_DETAILS.PASSPORT_NUMBER = txt_PPNumber.Text;
            hR_EMP_PASSPORT_DETAILS.PASSPORT_NATIONALITY = ddl_ppNationality.SelectedValue;
            if (dtp_PPIssueDate.Text.ToString().Length > 0)
            {

                hR_EMP_PASSPORT_DETAILS.PASSPORT_ISSUE_DATE = DBMethod.ConvertStringToDate(dtp_PPIssueDate.Text);
            }
            else
            {
                // hR_EMP_PASSPORT_DETAILS.PASSPORT_ISSUE_DATE = null;
            }

            hR_EMP_PASSPORT_DETAILS.PASSPORT_EXPIRY_DATE = DBMethod.ConvertStringToDate(dtp_PPExpiryDate.Text);
            hR_EMP_PASSPORT_DETAILS.PASSPORT_ISSUE_PLACE = txt_PPIssuePlace.Text;
            hR_EMP_PASSPORT_DETAILS.PASSPORT_ISSUED_BY = txt_PPIssuedBy.Text;
            hR_EMP_PASSPORT_DETAILS.ENABLED_FLAG = FINAppConstants.N;
            if (chk_PPActive.Checked)
                hR_EMP_PASSPORT_DETAILS.ENABLED_FLAG = FINAppConstants.Y;


            hR_EMP_PASSPORT_DETAILS.WORKFLOW_COMPLETION_STATUS = FINAppConstants.Y;

            hR_EMP_PASSPORT_DETAILS.PASS_ORG_ID = VMVServices.Web.Utils.OrganizationID;

            if (GMode == "A")
            {
                hR_EMP_PASSPORT_DETAILS.PASS_TXN_ID = FINSP.GetSPFOR_SEQCode("EMP_PP_ID".ToString(), false, true);
                hR_EMP_PASSPORT_DETAILS.CREATED_BY = this.LoggedUserName;
                hR_EMP_PASSPORT_DETAILS.CREATED_DATE = DateTime.Today;
                Employee_BLL.SaveEntity(hR_EMP_PASSPORT_DETAILS, FINAppConstants.Add, true);
            }
            else
            {
                hR_EMP_PASSPORT_DETAILS.MODIFIED_BY = this.LoggedUserName;
                hR_EMP_PASSPORT_DETAILS.MODIFIED_DATE = DateTime.Today;
                Employee_BLL.SaveEntity(hR_EMP_PASSPORT_DETAILS, FINAppConstants.Update, false);
            }


        }

        protected void gvPPDet_RowEditing(object sender, GridViewEditEventArgs e)
        {

            gvPPDet.EditIndex = e.NewEditIndex;
            BindPassPortDetails();
            GridViewRow gvr = gvPPDet.Rows[e.NewEditIndex];
            FillFooterPPDetDDL(gvr);
            //MPEPassPortDet.Show();
        }

        protected void gvPPDet_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvPPDet.Rows[e.RowIndex] as GridViewRow;

                if (gvr == null)
                {
                    return;
                }

                AssignTo_PP_GridControl(gvr, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);

                    return;
                }
                gvPPDet.EditIndex = -1;
                BindPassPortDetails();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EmpPPRowUpdate", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);

                }
            }
        }

        protected void ddlApplicantID_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtApplicantdtl = new DataTable();

            dtApplicantdtl = DBMethod.ExecuteQuery(Employee_DAL.GetApplicntdtl(ddlApplicantID.SelectedValue)).Tables[0];
            if (dtApplicantdtl.Rows.Count > 0)
            {
                txtFirstName.Text = dtApplicantdtl.Rows[0]["APP_FIRST_NAME"].ToString();
                txtMiddleName.Text = dtApplicantdtl.Rows[0]["APP_MIDDLE_NAME"].ToString();
                txtLastName.Text = dtApplicantdtl.Rows[0]["APP_LAST_NAME"].ToString();
                txtDateofBirth.Text = DBMethod.ConvertDateToString(dtApplicantdtl.Rows[0]["APP_DOB"].ToString());
                ddlNationality.SelectedValue = dtApplicantdtl.Rows[0]["nationality"].ToString();
                if (dtApplicantdtl.Rows[0]["APP_GENDER"].ToString() == "MALE")
                {
                    RBG.SelectedValue = "M";
                }
                else
                {
                    RBG.SelectedValue = "F";
                }

            }

        }

        protected void imbbtnLeaveDetails_Click(object sender, ImageClickEventArgs e)
        {
            imgbtnLeaveDetails();
        }
        private void imgbtnLeaveDetails()
        {
            //MPEAddress.Show();
            SetVisiableFalsePanel();
            pnlLeaveDetails.Visible = true;

            DataTable dt_EmpLeave = DBMethod.ExecuteQuery(FIN.DAL.HR.StaffLeaveDetails_DAL.getLeaveDetails4Empid(Master.StrRecordId)).Tables[0];

            if (dt_EmpLeave.Rows.Count > 0)
            {
                imgbtnLeavePost.Visible = false;
            }
            gvLeaveDetails.DataSource = dt_EmpLeave;
            gvLeaveDetails.DataBind();

        }

        private void btnSalaryClick()
        {
            SetVisiableFalsePanel();
            pnlSalaryDetails.Visible = true;


            FIN.BLL.PER.PayrollEmployeeElement_BLL.fn_getGroup(ref ddlGroupCode);
            dt_SalaryEntry = FIN.BLL.PER.PayrollEmployeeElement_BLL.getChildEntityDet4EmpId(Master.StrRecordId);
            BindGrid(dt_SalaryEntry);
            if (dt_SalaryEntry.Rows.Count > 0)
            {
                ddlGroupCode.SelectedValue = dt_SalaryEntry.Rows[0]["PAY_GROUP_ID"].ToString();
                fn_fillGroupName();
                ddlGroupCode.Enabled = false;
            }


            //dt_SalaryDet = FIN.BLL.PER.PayrollEmployeeElement_BLL.getChildEntityDet4EmpId(Master.StrRecordId);

            //if (dt_SalaryDet.Rows.Count > 0)
            //{
            //    if (dt_SalaryDet.Rows[0]["PAY_AMOUNT"].ToString().Length > 0)
            //    {
            //        dt_SalaryDet.AsEnumerable().ToList().ForEach(p => p.SetField<String>("PAY_AMOUNT", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("PAY_AMOUNT"))));
            //        dt_SalaryDet.AcceptChanges();
            //    }
            //}

            //hf_SalaryType.Value = "Earning";
            //gvSalaryDetails.DataSource = dt_SalaryDet;
            //gvSalaryDetails.DataBind();
            //hf_SalaryType.Value = "Deduction";
            //gvSalaryDetails_Det.DataSource = dt_SalaryDet;
            //gvSalaryDetails_Det.DataBind();

        }
        protected void imgbtnSalaryDetails_Click(object sender, ImageClickEventArgs e)
        {

            btnSalaryClick();
            
        }

        protected void gvSalaryDetails_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (dt_SalaryDet.Rows[e.Row.RowIndex]["PAY_ELE_CLASS"].ToString() != hf_SalaryType.Value.ToString())
                {
                    e.Row.Visible = false;
                }

            }
        }

        protected void ddlInternalExternale_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }

        protected void imgbtnPost_Click(object sender, ImageClickEventArgs e)
        {
            DataTable dt_Dept = Employee_BLL.getEmployeeCurrentDepartment(Master.StrRecordId);
            if (dt_Dept.Rows.Count == 0)
            {
                ErrorCollection.Add("DepartDetailsNotFound", "Department Details Not Found ");
                Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                return;
            }
            DataTable dt_finYear = FIN.BLL.GL.AccountingCalendar_BLL.getFinanceYear4Date(txtDoj.Text);
            if (dt_finYear.Rows.Count == 0)
            {
                ErrorCollection.Add("FinanceYearNotFOund", "Finance Year Not Found");
                Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                return;
            }

            DateTime dt_start = DateTime.Parse(dt_finYear.Rows[0]["CAL_EFF_START_DT"].ToString());
            int int_month = dt_start.Month;

            DataTable dt_LeaveDet = Employee_BLL.getLeaveDetails(dt_finYear.Rows[0]["CAL_DTL_ID"].ToString(), dt_Dept.Rows[0]["EMP_DEPT_ID"].ToString());

            if (dt_LeaveDet.Rows.Count == 0)
            {
                ErrorCollection.Add("NoLeaveDetails", "Leave Details Not Found");
                Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                return;
            }
            else
            {
                FIN.DAL.HR.LeaveDefinition_DAL.InserSP_EMP_LEAVE_PRORATE(dt_finYear.Rows[0]["CAL_DTL_ID"].ToString(), dt_Dept.Rows[0]["EMP_DEPT_ID"].ToString(), Master.StrRecordId, DBMethod.ConvertStringToDate(txtDoj.Text));
            }

            //for (int iLoop = 0; iLoop < dt_LeaveDet.Rows.Count; iLoop++)
            //{
            //    HR_LEAVE_STAFF_DEFINTIONS hR_LEAVE_STAFF_DEFINTIONS = new HR_LEAVE_STAFF_DEFINTIONS();
            //    hR_LEAVE_STAFF_DEFINTIONS.LEAVE_ID = dt_LeaveDet.Rows[iLoop][FINColumnConstants.LEAVE_ID].ToString();
            //    hR_LEAVE_STAFF_DEFINTIONS.DEPT_ID = dt_Dept.Rows[0]["EMP_DEPT_ID"].ToString();
            //    hR_LEAVE_STAFF_DEFINTIONS.STAFF_ID = Master.StrRecordId;
            //    hR_LEAVE_STAFF_DEFINTIONS.FISCAL_YEAR = (dt_finYear.Rows[0]["CAL_DTL_ID"].ToString());
            //    hR_LEAVE_STAFF_DEFINTIONS.LEAVE_AVAILED = 0;
            //    hR_LEAVE_STAFF_DEFINTIONS.LEAVE_BALANCE = CommonUtils.ConvertStringToInt(dt_LeaveDet.Rows[iLoop][FINColumnConstants.NO_OF_DAYS].ToString());
            //    hR_LEAVE_STAFF_DEFINTIONS.NO_OF_DAYS = CommonUtils.ConvertStringToInt(dt_LeaveDet.Rows[iLoop][FINColumnConstants.NO_OF_DAYS].ToString());
            //    hR_LEAVE_STAFF_DEFINTIONS.ENABLED_FLAG = FINAppConstants.EnabledFlag;
            //    hR_LEAVE_STAFF_DEFINTIONS.ORG_ID = VMVServices.Web.Utils.OrganizationID;
            //    hR_LEAVE_STAFF_DEFINTIONS.LSD_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_021.ToString(), false, true);
            //    hR_LEAVE_STAFF_DEFINTIONS.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_LEAVE_STAFF_DEFINTIONS.LSD_ID);
            //    hR_LEAVE_STAFF_DEFINTIONS.CREATED_BY = this.LoggedUserName;
            //    hR_LEAVE_STAFF_DEFINTIONS.CREATED_DATE = DateTime.Today;
            //    DBMethod.SaveEntity<HR_LEAVE_STAFF_DEFINTIONS>(hR_LEAVE_STAFF_DEFINTIONS);
            //    DateTime FrmDate = DBMethod.ConvertStringToDate(txtDoj.Text.ToString());
            //   StaffLeaveDetails_DAL.GetSP_EMP_LEAVE_TRN_LEDGER("OPENBAL", hR_LEAVE_STAFF_DEFINTIONS.LSD_ID.ToString(), "LSD", FrmDate, int_month, dt_finYear.Rows[0]["CAL_DTL_ID"].ToString(), Master.StrRecordId, hR_LEAVE_STAFF_DEFINTIONS.ORG_ID.ToString(), hR_LEAVE_STAFF_DEFINTIONS.LEAVE_ID.ToString(), dt_Dept.Rows[0]["EMP_DEPT_ID"].ToString(), int.Parse(hR_LEAVE_STAFF_DEFINTIONS.LEAVE_BALANCE.ToString()));
            //    imgbtnLeavePost.Visible = false;
            //}
            imbbtnLeaveDetails_Click(sender, e);
        }

        protected void imgbtnPersonal_Click(object sender, ImageClickEventArgs e)
        {
            SetVisiableFalsePanel();
            div_MDData.Visible = true;
        }

        protected void ddlNationality_SelectedIndexChanged(object sender, EventArgs e)
        {
            ssdFlagVisable();
        }

        private void ssdFlagVisable()
        {
            if (ddlNationality.SelectedValue == "Kuwait")
            {
                divSSDFlag.Visible = true;
                chkSSDFlag.Checked = true;
            }
            else
            {
                divSSDFlag.Visible = false;
                chkSSDFlag.Checked = false;
            }
        }

        protected void btnFoto_Click(object sender, EventArgs e)
        {
            //RegisterStartupScript("Startup", "<script language='javascript'>window.open('PhotoCapturing.aspx?ID=" + Master.RecordID.ToString() + ",'','toolbar=no,menubar=no,location=no,scrollbars=yes,resizable=no,status=no,width=860px,height=660px,left=0px,Top=0px');</script>");
            //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('PhotoCapturing.aspx?ID=" + Master.RecordID.ToString() + ",'','" + MessageConstants.ReportProperties + "');", true);

            //string alertScript = "javascript:alert('Alter From asp.net page')";
            //ClientScript.RegisterStartupScript(typeof(Page), "alertscript", alertScript);
            ModalPopupExtender2.Show();
        }

        protected void btnCloseImage_Click(object sender, EventArgs e)
        {
            if (Session[FINSessionConstants.Attachments] != null)
            {
                System.Collections.SortedList fu_tmp = (System.Collections.SortedList)Session[FINSessionConstants.Attachments];
                imgCaptureImagediv.Visible = true;
                imgPatientFoto.Visible = true;
                imgPatientFoto.ImageUrl = "~/UploadFile/Captures/" + fu_tmp.GetByIndex(0).ToString() + "." + fu_tmp.GetByIndex(1).ToString();
                Session["fotoLoad"] = "yes";
                ModalPopupExtender2.Hide();

            }
        }

        # region Salary Entry
        protected void ddlGroupCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            fn_fillGroupName();
            bindElement();
        }


        private void fn_fillGroupName()
        {
            dt_SalaryEntry = DBMethod.ExecuteQuery(FIN.DAL.PER.PayrollEmployeeElement_DAL.getPayGroup_Name(ddlGroupCode.SelectedValue.ToString())).Tables[0];
            if (dt_SalaryEntry != null)
            {
                if (dt_SalaryEntry.Rows.Count > 0)
                {
                    txtDescription.Text = dt_SalaryEntry.Rows[0][0].ToString();
                    txtDescription.Enabled = false;
                }
            }




        }

        private void bindElement()
        {
            dt_SalaryEntry = DBMethod.ExecuteQuery(FIN.DAL.PER.PayrollEmployeeElement_DAL.GetPayPerioddtls_Bsdon_Grpcode(ddlGroupCode.SelectedValue)).Tables[0];
            BindGrid(dt_SalaryEntry);
        }

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session["SalaryEntry"] = dtData;

                if (dtData.Rows.Count > 0)
                {
                    if (dtData.Rows[0]["PAY_AMOUNT"].ToString().Length > 0)
                    {
                        dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("PAY_AMOUNT", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("PAY_AMOUNT"))));
                        dtData.AcceptChanges();
                    }
                }
                              

                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr["ENABLED_FLAG"] = "FALSE";
                    dr["PAY_ELEMENT_PRORATE"] = "FALSE";
                    dr["PAID_FOR_ANNUAL_LEAVE"] = "FALSE";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);
               


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PP_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            try
            {
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dt_SalaryEntry = (DataTable)Session["SalaryEntry"];
                }
                gvData.EditIndex = -1;

                BindGrid(dt_SalaryEntry);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PGEE_ROW_CNCL_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>
        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session["SalaryEntry"] != null)
                {
                    dt_SalaryEntry = (DataTable)Session["SalaryEntry"];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dt_SalaryEntry, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dt_SalaryEntry.Rows.Add(drList);
                    BindGrid(dt_SalaryEntry);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PEEE_ROW_CMD", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>
        /// 
        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();


                if (Session["SalaryEntry"] != null)
                {
                    dt_SalaryEntry = (DataTable)Session["SalaryEntry"];
                }
                DataRow drList = null;
                drList = dt_SalaryEntry.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;

                BindGrid(dt_SalaryEntry);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PGEE_ROW_DEL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>
        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session["SalaryEntry"] != null)
                {
                    dt_SalaryEntry = (DataTable)Session["SalaryEntry"];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dt_SalaryEntry);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PGEE_ROW_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>
        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PGEE_RowCreated", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    TextBox dtp_fromDate = e.Row.FindControl("dtpfromDate") as TextBox;
                    TextBox txt_Amount = e.Row.FindControl("txtAmount") as TextBox;
                    if (dtp_fromDate.Text.ToString().Trim().Length == 0)
                    {
                        dtp_fromDate.Text = txtDoj.Text;
                    }
                    if (txt_Amount.Text.ToString().Length == 0)
                    {
                        txt_Amount.Text = "0";
                    }

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PGEE_RDB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session["SalaryEntry"] != null)
                {
                    dt_SalaryEntry = (DataTable)Session["SalaryEntry"];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dt_SalaryEntry, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dt_SalaryEntry);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PGEE_ROW_UP", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {


            System.Collections.SortedList slControls = new System.Collections.SortedList();


            DropDownList ddl_ElementCode = gvr.FindControl("ddlElementCode") as DropDownList;
            TextBox txt_ElementDescription = gvr.FindControl("txtElementDescription") as TextBox;
            TextBox txt_Amount = gvr.FindControl("txtAmount") as TextBox;
            TextBox dtp_fromDate = gvr.FindControl("dtpfromDate") as TextBox;
            TextBox dtp_ToDate = gvr.FindControl("dtpToDate") as TextBox;
            CheckBox chkprorate = gvr.FindControl("chkprorate") as CheckBox;
            CheckBox chkannualleave = gvr.FindControl("chkannualleave") as CheckBox;

            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dt_SalaryEntry.NewRow();
                drList["PAY_EMP_ELEMENT_ID"] = "0";
            }
            else
            {
                drList = dt_SalaryEntry.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }

            slControls[0] = ddl_ElementCode;
            slControls[1] = txt_Amount;
            slControls[2] = dtp_fromDate;
            slControls[3] = dtp_fromDate;
            slControls[4] = dtp_ToDate;



            ErrorCollection.Clear();
            string strCtrlTypes = "DropDownList~TextBox~TextBox~DateTime~DateRangeValidate";
            string strMessage = "Element Code ~ Amount ~ From Date ~ From Date ~ To Date";
            ErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);
            if (ErrorCollection.Count > 0)
                return drList;

            ErrorCollection.Clear();

            //CheckAmtLimit();

            ErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);
            if (ErrorCollection.Count > 0)
                return drList;
            string strCondition = "PAY_ELEMENT_ID='" + ddl_ElementCode.SelectedValue + "'";
            strMessage = FINMessageConstatns.RecordAlreadyExists;
            ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            if (ErrorCollection.Count > 0)
            {
                return drList;
            }




            drList["PAY_ELEMENT_ID"] = ddl_ElementCode.SelectedValue;
            drList["PAY_ELEMENT_CODE"] = ddl_ElementCode.SelectedItem.Text;
            drList["PAY_ELEMENT_DESC"] = ddl_ElementCode.SelectedItem.Text;

            drList["PAY_AMOUNT"] = txt_Amount.Text;
            //fn_fill_element_name();

            if (dtp_fromDate.Text.ToString().Length > 0)
            {
                drList["EFFECTIVE_FROM_DT"] = DBMethod.ConvertStringToDate(dtp_fromDate.Text.ToString());
            }

            if (dtp_ToDate.Text.ToString().Length > 0)
            {

                drList["EFFECTIVE_TO_DT"] = DBMethod.ConvertStringToDate(dtp_ToDate.Text.ToString());
            }
            else
            {
                drList["EFFECTIVE_TO_DT"] = DBNull.Value;
            }

            if (chkprorate.Checked)
            {
                drList["PAY_ELEMENT_PRORATE"] = "TRUE";
            }
            else
            {
                drList["PAY_ELEMENT_PRORATE"] = "FALSE";
            }

            if (chkannualleave.Checked)
            {
                drList["PAID_FOR_ANNUAL_LEAVE"] = "TRUE";
            }
            else
            {
                drList["PAID_FOR_ANNUAL_LEAVE"] = "FALSE";
            }

            drList[FINColumnConstants.DELETED] = FINAppConstants.N;


            return drList;



        }

        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddl_ElementCode = tmpgvr.FindControl("ddlElementCode") as DropDownList;
                FIN.BLL.PER.PayrollEmployeeElement_BLL.fn_getElement(ref ddl_ElementCode);


                if (gvData.EditIndex >= 0)
                {
                    ddl_ElementCode.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.PAY_ELEMENT_ID].ToString();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PEEE_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void ddlElementCode_SelectedIndexChanged(object sender, EventArgs e)
        {

            fn_fill_ElemetDescription(sender, e);
        }

        private void fn_fill_ElemetDescription(object sender, EventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;

            CheckBox chkprorate = gvr.FindControl("chkprorate") as CheckBox;
            CheckBox chkannualleave = gvr.FindControl("chkannualleave") as CheckBox;

            DropDownList ddl_ElementCode = gvr.FindControl("ddlElementCode") as DropDownList;
            TextBox txt_ElementDescription = gvr.FindControl("txtElementDescription") as TextBox;
            DataTable dt_elementName = DBMethod.ExecuteQuery(FIN.DAL.PER.PayrollEmployeeElement_DAL.getPayElements_Name(ddl_ElementCode.SelectedValue.ToString())).Tables[0];

            if (dt_elementName != null)
            {
                if (dt_elementName.Rows.Count > 0)
                {
                    txt_ElementDescription.Text = dt_elementName.Rows[0][0].ToString();
                    txt_ElementDescription.Enabled = false;
                }
            }

            if (dt_elementName.Rows[0]["PAY_ELEMENT_PRORATE"].ToString().ToUpper() == "TRUE")
            {
                chkprorate.Checked = true;
            }
            else
            {
                chkprorate.Checked = false;
            }

            if (dt_elementName.Rows[0]["PAID_FOR_ANNUAL_LEAVE"].ToString().ToUpper() == "TRUE")
            {
                chkannualleave.Checked = true;
            }
            else
            {
                chkannualleave.Checked = false;
            }

        }


        

        protected void imgbtnSalarySave_Click(object sender, ImageClickEventArgs e)
        {
            SaveSalaryDetails();
            btnSalaryClick();
        }
        private void SaveSalaryDetails()
        {
            PAY_EMP_ELEMENT_VALUE pAY_EMP_ELEMENT_VALUE;

            DataTable dt_Dept = Employee_BLL.getEmployeeCurrentDepartment(Master.StrRecordId);
            if (dt_Dept.Rows.Count == 0)
            {
                ErrorCollection.Add("DepartDetailsNotFound", "Department Details Not Found ");
                Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                return;
            }

            for (int iLoop = 0; iLoop < gvData.Rows.Count; iLoop++)
            {
                pAY_EMP_ELEMENT_VALUE = new PAY_EMP_ELEMENT_VALUE();
                if (gvData.DataKeys[iLoop].Values["PAY_EMP_ELEMENT_ID"].ToString() != "0")
                {
                    using (IRepository<PAY_EMP_ELEMENT_VALUE> userCtx = new DataRepository<PAY_EMP_ELEMENT_VALUE>())
                    {
                        pAY_EMP_ELEMENT_VALUE = userCtx.Find(r =>
                            (r.PAY_EMP_ELEMENT_ID == gvData.DataKeys[iLoop].Values["PAY_EMP_ELEMENT_ID"].ToString())
                            ).SingleOrDefault();
                    }

                    
                }
                pAY_EMP_ELEMENT_VALUE.PAY_EMP_DEPT_ID = dt_Dept.Rows[0]["EMP_DEPT_ID"].ToString();
                pAY_EMP_ELEMENT_VALUE.PAY_EMP_ID = Master.StrRecordId;
                pAY_EMP_ELEMENT_VALUE.PAY_GROUP_ID = ddlGroupCode.SelectedValue.ToString();
                pAY_EMP_ELEMENT_VALUE.PAY_ELEMENT_ID = gvData.DataKeys[iLoop].Values["PAY_ELEMENT_ID"].ToString();// dtGridData.Rows[iLoop]["PAY_ELEMENT_ID"].ToString();
                TextBox txt_payamount = (TextBox)gvData.Rows[iLoop].FindControl("txtAmount");
                if (txt_payamount.Text.ToString().Length > 0)
                {
                    pAY_EMP_ELEMENT_VALUE.PAY_AMOUNT = CommonUtils.ConvertStringToDecimal(txt_payamount.Text);
                    TextBox txt_FromDate = (TextBox)gvData.Rows[iLoop].FindControl("dtpfromDate");
                    if (txt_FromDate.Text.ToString().Length > 0)
                    {
                        pAY_EMP_ELEMENT_VALUE.EFFECTIVE_FROM_DT = DBMethod.ConvertStringToDate(txt_FromDate.Text.ToString());
                    }
                    TextBox txt_ToDate = (TextBox)gvData.Rows[iLoop].FindControl("dtpToDate");
                    if (txt_ToDate.Text.ToString().Length > 0)
                    {
                        pAY_EMP_ELEMENT_VALUE.EFFECTIVE_TO_DT = DBMethod.ConvertStringToDate(txt_ToDate.Text);
                    }
                    else
                    {
                        pAY_EMP_ELEMENT_VALUE.EFFECTIVE_TO_DT = null;
                    }

                    CheckBox chk_ELEPRO = (CheckBox)gvData.Rows[iLoop].FindControl("chkprorate");
                    if (chk_ELEPRO.Checked)
                    {
                        pAY_EMP_ELEMENT_VALUE.PAY_ELEMENT_PRORATE = FINAppConstants.Y;
                    }
                    else
                    {
                        pAY_EMP_ELEMENT_VALUE.PAY_ELEMENT_PRORATE = FINAppConstants.N;
                    }
                    CheckBox chk_AnnLeave = (CheckBox)gvData.Rows[iLoop].FindControl("chkannualleave");
                    if (chk_AnnLeave.Checked)
                    {
                        pAY_EMP_ELEMENT_VALUE.PAID_FOR_ANNUAL_LEAVE = FINAppConstants.Y;
                    }
                    else
                    {
                        pAY_EMP_ELEMENT_VALUE.PAID_FOR_ANNUAL_LEAVE = FINAppConstants.N;
                    }

                    pAY_EMP_ELEMENT_VALUE.ENABLED_FLAG = "1";

                    pAY_EMP_ELEMENT_VALUE.PAY_ORG_ID = VMVServices.Web.Utils.OrganizationID;
                    
                    if (gvData.DataKeys[iLoop].Values[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {                        
                        DBMethod.DeleteEntity<PAY_EMP_ELEMENT_VALUE>(pAY_EMP_ELEMENT_VALUE);
                    }
                    else
                    {
                        if (gvData.DataKeys[iLoop].Values["PAY_EMP_ELEMENT_ID"].ToString() != "0")
                        {
                            pAY_EMP_ELEMENT_VALUE.PAY_EMP_ELEMENT_ID = gvData.DataKeys[iLoop].Values["PAY_EMP_ELEMENT_ID"].ToString();// dtGridData.Rows[iLoop]["PAY_EMP_ELEMENT_ID"].ToString();
                            pAY_EMP_ELEMENT_VALUE.MODIFIED_BY = this.LoggedUserName;
                            pAY_EMP_ELEMENT_VALUE.MODIFIED_DATE = DateTime.Today;
                            pAY_EMP_ELEMENT_VALUE.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, pAY_EMP_ELEMENT_VALUE.PAY_EMP_ELEMENT_ID);
                            DBMethod.SaveEntity<PAY_EMP_ELEMENT_VALUE>(pAY_EMP_ELEMENT_VALUE, true);                            

                        }
                        else
                        {
                            pAY_EMP_ELEMENT_VALUE.PAY_EMP_ELEMENT_ID = FINSP.GetSPFOR_SEQCode("PER_006".ToString(), false, true);
                            pAY_EMP_ELEMENT_VALUE.CREATED_BY = this.LoggedUserName;
                            pAY_EMP_ELEMENT_VALUE.CREATED_DATE = DateTime.Today;
                            pAY_EMP_ELEMENT_VALUE.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, pAY_EMP_ELEMENT_VALUE.PAY_EMP_ELEMENT_ID);
                            DBMethod.SaveEntity<PAY_EMP_ELEMENT_VALUE>(pAY_EMP_ELEMENT_VALUE);
                            
                        }

                    }
                }
                else
                {
                    if (gvData.DataKeys[iLoop].Values["PAY_EMP_ELEMENT_ID"].ToString() != "0")
                    {
                        pAY_EMP_ELEMENT_VALUE.PAY_EMP_ELEMENT_ID = gvData.DataKeys[iLoop].Values["PAY_EMP_ELEMENT_ID"].ToString();// dtGridData.Rows[iLoop]["PAY_EMP_ELEMENT_ID"].ToString();
                        DBMethod.DeleteEntity<PAY_EMP_ELEMENT_VALUE>(pAY_EMP_ELEMENT_VALUE);
                    }
                }

            }
        }

        #endregion

        protected void ddlEmploymentType_SelectedIndexChanged(object sender, EventArgs e)
        {
            EnableCompanyName();
        }
        private void EnableCompanyName()
        {
            if (ddlEmploymentType.SelectedValue.ToString() == "OutSourced")
                txtCompanyname.Enabled = true;
            else
                txtCompanyname.Enabled = false;
        }



    }
}