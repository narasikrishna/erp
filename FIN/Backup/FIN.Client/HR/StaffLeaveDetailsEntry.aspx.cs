﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.DAL.GL;
using FIN.BLL;
using FIN.BLL.GL;
using FIN.BLL.HR;
using VMVServices.Web;


namespace FIN.Client.HR
{
    public partial class StaffLeaveDetailsEntry : PageBase
    {

        HR_LEAVE_STAFF_DEFINTIONS HR_LEAVE_STAFF_DEFINTIONS = new HR_LEAVE_STAFF_DEFINTIONS();
        HR_EMPLOYEES hR_EMPLOYEES = new HR_EMPLOYEES();
        DataTable dtData = new DataTable();
        Boolean bol_rowVisiable;
        string ProReturn = null;
        DataTable Month_data = new DataTable();
        static int month;
        Boolean savedBool;
        DataTable dtGridData = new DataTable();
        decimal leaveBalance;
        decimal NoOfDays;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));


            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
        }

        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        private void FillComboBox()
        {
            AccountingCalendar_BLL.GetCalAcctYear(ref ddlFinancialYear);
            Department_BLL.GetDepartmentName(ref ddlDepartment);
            Employee_BLL.GetEmployeeName(ref ddlStaffName);
        }
        private void FillFromToDate()
        {
            if (ddlFinancialYear.SelectedValue != null)
            {
                dtData = DBMethod.ExecuteQuery(AccountingCalendar_DAL.GetStartdtEnddtfrcalyear(ddlFinancialYear.SelectedValue.ToString())).Tables[0];
                if (dtData != null)
                {
                    if (dtData.Rows.Count > 0)
                    {
                        if (dtData.Rows[0][FINColumnConstants.CAL_EFF_START_DT].ToString() != string.Empty)
                        {
                            txtFromDate.Text = DateTime.Parse(dtData.Rows[0][FINColumnConstants.CAL_EFF_START_DT].ToString()).ToString("dd/MM/yyyy");
                            String Start_Date;
                            Start_Date = DateTime.Parse(dtData.Rows[0][FINColumnConstants.CAL_EFF_START_DT].ToString()).ToString("dd/MM/yyyy");

                            Month_data = DBMethod.ExecuteQuery(StaffLeaveDetails_DAL.GetMonth(Start_Date)).Tables[0];
                            month = int.Parse(Month_data.Rows[0][0].ToString());
                        }
                        if (dtData.Rows[0][FINColumnConstants.CAL_EFF_END_DT] != null)
                        {
                            if (dtData.Rows[0][FINColumnConstants.CAL_EFF_END_DT].ToString().Count() > 0)
                            {
                                txtToDate.Text = DateTime.Parse(dtData.Rows[0][FINColumnConstants.CAL_EFF_END_DT].ToString()).ToString("dd/MM/yyyy");
                            }
                        }
                    }
                }
            }
        }
        protected void ddlFinancialYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                FillFromToDate();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// .RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();

                EntityData = null;
                Session[FINSessionConstants.GridData] = null;

                FillComboBox();
                dtGridData = DBMethod.ExecuteQuery(StaffLeaveDetails_DAL.GetStaffLeaveDtls(Master.StrRecordId)).Tables[0];

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    if (hR_EMPLOYEES.EMP_INTERNAL_EXTERNAL != "E")
                    {
                        gvData.Enabled = false;
                    }

                   // HR_LEAVE_STAFF_DEFINTIONS = StaffLeaveDetails_BLL.getClassEntity(Master.StrRecordId);

                    DataTable dtsld = new DataTable();

                    dtsld = DBMethod.ExecuteQuery(StaffLeaveDetails_DAL.GetStaffLeaveDts(Master.StrRecordId)).Tables[0];
                   
                   // EntityData = HR_LEAVE_STAFF_DEFINTIONS;
                 //   ddlFinancialYear.SelectedValue = HR_LEAVE_STAFF_DEFINTIONS.FISCAL_YEAR.ToString();
                    ddlFinancialYear.SelectedValue = dtsld.Rows[0]["FISCAL_YEAR"].ToString();
                    FillFromToDate();
                   // ddlDepartment.SelectedValue = HR_LEAVE_STAFF_DEFINTIONS.DEPT_ID.ToString();
                    ddlDepartment.SelectedValue = dtsld.Rows[0]["DEPT_ID"].ToString();
                    fillempnm();
                   // ddlStaffName.SelectedValue = HR_LEAVE_STAFF_DEFINTIONS.STAFF_ID.ToString();
                    ddlStaffName.SelectedValue = dtsld.Rows[0]["STAFF_ID"].ToString();
                    dtGridData = DBMethod.ExecuteQuery(StaffLeaveDetails_DAL.GetStaffLeaveDtls_asperemp(ddlFinancialYear.SelectedValue, ddlDepartment.SelectedValue, ddlStaffName.SelectedValue)).Tables[0];
                }
                BindGrid(dtGridData);
               
                

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Vaca_Entry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);

                }
            }

        }

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;


                Session[FINSessionConstants.GridData] = dtData;

                if (dtData.Rows.Count > 0)
                {
                    dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("LEAVE_BALANCE", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("LEAVE_BALANCE"))));
                    //dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("LEAVE_AVAILED", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("LEAVE_AVAILED"))));
                    dtData.AcceptChanges();
                }
               
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddlLeave = tmpgvr.FindControl("ddlLeave") as DropDownList;
                LeaveDefinition_BLL.GetLeaveBasedDept_frSLD(ref ddlLeave, ddlDepartment.SelectedValue);


                if (gvData.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                {
                    ddlLeave.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.LEAVE_ID].ToString();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RLD_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion
        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session[FINSessionConstants.GridData], "Staff Leave ");

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                AssignToBE();

                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                var tmpChildEntity = new List<Tuple<object, string>>();

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    HR_LEAVE_STAFF_DEFINTIONS = new HR_LEAVE_STAFF_DEFINTIONS();

                    if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                    {
                        if ((dtGridData.Rows[iLoop][FINColumnConstants.LSD_ID].ToString()) != "0" && dtGridData.Rows[iLoop][FINColumnConstants.LSD_ID].ToString() != string.Empty)
                        {
                            HR_LEAVE_STAFF_DEFINTIONS = StaffLeaveDetails_BLL.getClassEntity(dtGridData.Rows[iLoop][FINColumnConstants.LSD_ID].ToString());
                        }
                    }
                    HR_LEAVE_STAFF_DEFINTIONS.LEAVE_ID = dtGridData.Rows[iLoop][FINColumnConstants.LEAVE_ID].ToString();
                    HR_LEAVE_STAFF_DEFINTIONS.DEPT_ID = ddlDepartment.SelectedValue.ToString();
                    HR_LEAVE_STAFF_DEFINTIONS.STAFF_ID = ddlStaffName.SelectedValue.ToString();
                    HR_LEAVE_STAFF_DEFINTIONS.FISCAL_YEAR = (ddlFinancialYear.SelectedValue.ToString());
                    HR_LEAVE_STAFF_DEFINTIONS.LEAVE_AVAILED = CommonUtils.ConvertStringToInt(dtGridData.Rows[iLoop][FINColumnConstants.LEAVE_AVAILED].ToString());
                    HR_LEAVE_STAFF_DEFINTIONS.LEAVE_BALANCE = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop][FINColumnConstants.LEAVE_BALANCE].ToString());
                    HR_LEAVE_STAFF_DEFINTIONS.NO_OF_DAYS = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop][FINColumnConstants.NO_OF_DAYS].ToString());
                    HR_LEAVE_STAFF_DEFINTIONS.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                   // HR_LEAVE_STAFF_DEFINTIONS.ACCURED_LEAVE_BALANCE = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop][FINColumnConstants.LEAVE_BALANCE].ToString());
                    HR_LEAVE_STAFF_DEFINTIONS.ACCURED_LEAVE_BALANCE = dtGridData.Rows[iLoop][FINColumnConstants.LEAVE_BALANCE].ToString();




                    //  HR_LEAVE_STAFF_DEFINTIONS.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;
                    HR_LEAVE_STAFF_DEFINTIONS.ORG_ID = VMVServices.Web.Utils.OrganizationID;

                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        tmpChildEntity.Add(new Tuple<object, string>(HR_LEAVE_STAFF_DEFINTIONS, "D"));
                    }
                    else
                    {
                        if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                        {
                            if ((dtGridData.Rows[iLoop][FINColumnConstants.LSD_ID].ToString()) != "0" && dtGridData.Rows[iLoop][FINColumnConstants.LSD_ID].ToString() != string.Empty)
                            {
                                HR_LEAVE_STAFF_DEFINTIONS.LSD_ID = dtGridData.Rows[iLoop]["LSD_ID"].ToString();

                                HR_LEAVE_STAFF_DEFINTIONS.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, HR_LEAVE_STAFF_DEFINTIONS.LSD_ID);

                                HR_LEAVE_STAFF_DEFINTIONS.MODIFIED_BY = this.LoggedUserName;
                                HR_LEAVE_STAFF_DEFINTIONS.MODIFIED_DATE = DateTime.Today;
                                DBMethod.SaveEntity<HR_LEAVE_STAFF_DEFINTIONS>(HR_LEAVE_STAFF_DEFINTIONS, true);
                                savedBool = true;
                            }
                        }
                        else
                        {
                            if ((dtGridData.Rows[iLoop][FINColumnConstants.LSD_ID].ToString()) != "0" && dtGridData.Rows[iLoop][FINColumnConstants.LSD_ID].ToString() != string.Empty)
                            {
                                HR_LEAVE_STAFF_DEFINTIONS.LSD_ID = dtGridData.Rows[iLoop]["LSD_ID"].ToString();
                                HR_LEAVE_STAFF_DEFINTIONS.MODIFIED_BY = this.LoggedUserName;
                                HR_LEAVE_STAFF_DEFINTIONS.MODIFIED_DATE = DateTime.Today;

                                HR_LEAVE_STAFF_DEFINTIONS.CREATED_BY = this.LoggedUserName;
                                HR_LEAVE_STAFF_DEFINTIONS.CREATED_DATE = DateTime.Today;

                                HR_LEAVE_STAFF_DEFINTIONS.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, HR_LEAVE_STAFF_DEFINTIONS.LSD_ID);

                                DBMethod.SaveEntity<HR_LEAVE_STAFF_DEFINTIONS>(HR_LEAVE_STAFF_DEFINTIONS, true);
                                savedBool = true;
                            }
                            else
                            {
                                HR_LEAVE_STAFF_DEFINTIONS.LSD_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_021.ToString(), false, true);
                                HR_LEAVE_STAFF_DEFINTIONS.CREATED_BY = this.LoggedUserName;
                                HR_LEAVE_STAFF_DEFINTIONS.CREATED_DATE = DateTime.Today;
                                HR_LEAVE_STAFF_DEFINTIONS.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, HR_LEAVE_STAFF_DEFINTIONS.LSD_ID);

                                DBMethod.SaveEntity<HR_LEAVE_STAFF_DEFINTIONS>(HR_LEAVE_STAFF_DEFINTIONS);
                                savedBool = true;

                                DateTime FrmDate = DBMethod.ConvertStringToDate(txtFromDate.Text.ToString());

                                StaffLeaveDetails_DAL.GetSP_EMP_LEAVE_TRN_LEDGER("OPENBAL", HR_LEAVE_STAFF_DEFINTIONS.LSD_ID.ToString(), "LSD", txtFromDate.Text.ToString(), CommonUtils.ConvertStringToDecimal(month.ToString()), ddlFinancialYear.SelectedValue.ToString(), ddlStaffName.SelectedValue.ToString(), HR_LEAVE_STAFF_DEFINTIONS.ORG_ID.ToString(), HR_LEAVE_STAFF_DEFINTIONS.LEAVE_ID.ToString(), ddlDepartment.SelectedValue.ToString(), CommonUtils.ConvertStringToDecimal(HR_LEAVE_STAFF_DEFINTIONS.LEAVE_BALANCE.ToString()));
                            }




                          
                            //if (ProReturn != string.Empty)
                            //{
                            //    if (ProReturn != "0")
                            //    {
                            //        ErrorCollection.Add("STAFFLEAVEDTLS", ProReturn);
                            //        if (ErrorCollection.Count > 0)
                            //        {
                            //            return;
                            //        }
                            //    }
                            //}
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {

                    HR_LEAVE_STAFF_DEFINTIONS.LSD_ID = (dtGridData.Rows[iLoop]["LSD_ID"].ToString());
                    DBMethod.DeleteEntity<HR_LEAVE_STAFF_DEFINTIONS>(HR_LEAVE_STAFF_DEFINTIONS);
                }


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion


        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            try
            {
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = -1;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_ROW_CNCL_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_ROW_CMD", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {


            System.Collections.SortedList slControls = new System.Collections.SortedList();

            DropDownList ddlLeave = gvr.FindControl("ddlLeave") as DropDownList;
            TextBox txtNoOfDays = gvr.FindControl("txtNoOfDays") as TextBox;
            TextBox txtLeaveAvailed = gvr.FindControl("txtLeaveAvailed") as TextBox;
            TextBox txtLeaveBalance = gvr.FindControl("txtLeaveBalance") as TextBox;
            TextBox txtAccuredLeaveBalance = gvr.FindControl("txtAccuredLeaveBalance") as TextBox;
            
            if (txtLeaveAvailed.Text.ToString().Length == 0)
            {
                txtLeaveAvailed.Text = "0";
            }

            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));
            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();

            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList[FINColumnConstants.LSD_ID] = "0";

            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }

            slControls[0] = ddlLeave;
            slControls[1] = txtNoOfDays;
            slControls[2] = txtLeaveAvailed;
            slControls[3] = txtAccuredLeaveBalance;

            ErrorCollection.Clear();
            string strCtrlTypes = FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX;

            string strMessage = Prop_File_Data["Leave_Name_P"] + " ~ " + Prop_File_Data["No_of_Days_P"] + " ~ " + Prop_File_Data["Leave_Availed_P"] + " ~ " + Prop_File_Data["Accured_Leave_Balance_P"] + "";
            //string strMessage = "Leave Name ~ Leave Availed ~Leave Balance";
            ErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);
            if (ErrorCollection.Count > 0)
                return drList;


            string strCondition = "LEAVE_ID='" + ddlLeave.SelectedValue.ToString() + "'";
            strMessage = FINMessageConstatns.RecordAlreadyExists;
            ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            if (ErrorCollection.Count > 0)
            {
                return drList;
            }

            drList[FINColumnConstants.LEAVE_ID] = ddlLeave.SelectedValue;
            drList[FINColumnConstants.LEAVE_NAME] = ddlLeave.SelectedItem.Text;
            drList[FINColumnConstants.NO_OF_DAYS] = txtNoOfDays.Text;
            drList[FINColumnConstants.LEAVE_BALANCE] = txtLeaveBalance.Text;
            drList[FINColumnConstants.LEAVE_AVAILED] = txtLeaveAvailed.Text;
            drList[FINColumnConstants.ACCURED_LEAVE_BALANCE] = txtAccuredLeaveBalance.Text;
            

            drList[FINColumnConstants.DELETED] = FINAppConstants.N;



            if (GMode == "A")
            {

                // ProReturn = StaffLeaveDetails_DAL.GetSP_EMP_LEAVE_TRN_LEDGER("OPENBAL",HR_LEAVE_STAFF_DEFINTIONS.LSD_ID.ToString(),"LSD",txtFromDate.Text.ToString(),);


                //if (ProReturn != string.Empty)
                //{

                //    if (ProReturn != "0")
                //    {

                //        ErrorCollection.Add("BANKBRNCH", ProReturn);
                //        if (ErrorCollection.Count > 0)
                //        {
                //            return;
                //        }
                //    }
                //}
            }
            return drList;



        }



        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_ROW_UP", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_ROW_DEL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_ROW_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_RowCreated", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                    {
                        e.Row.Visible = false;

                    }

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;

                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_RDB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }



        #endregion



        private void fillempnm()
        {
            Employee_BLL.GetEmplNameBothIntExt(ref ddlStaffName, ddlDepartment.SelectedValue);
        }

        protected void ddlDepartment_SelectedIndexChanged1(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                fillempnm();

                dtGridData = DBMethod.ExecuteQuery(StaffLeaveDetails_DAL.GetStaffLeaveDtls(Master.StrRecordId)).Tables[0];
                BindGrid(dtGridData);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RLD_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void ddlLeave_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtleaveBal = new DataTable();
            //DataTable dtNoOfDays = new DataTable();
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;

            DropDownList ddlLeave = gvr.FindControl("ddlLeave") as DropDownList;
            TextBox txtLeaveBalance = gvr.FindControl("txtLeaveBalance") as TextBox;
            TextBox txtAccuredLeaveBalance = gvr.FindControl("txtAccuredLeaveBalance") as TextBox;
            TextBox txtNoOfDays = gvr.FindControl("txtNoOfDays") as TextBox;
            dtleaveBal = DBMethod.ExecuteQuery(StaffLeaveDetails_DAL.GetNoOfDays(ddlLeave.SelectedValue)).Tables[0];
            //dtNoOfDays = DBMethod.ExecuteQuery(StaffLeaveDetails_DAL.GetNoOfDays(ddlLeave.SelectedValue)).Tables[0];
            if (dtleaveBal.Rows.Count != null && dtleaveBal.Rows.Count > 0)
            {
                txtLeaveBalance.Text = dtleaveBal.Rows[0]["NO_OF_DAYS"].ToString();
                txtNoOfDays.Text = dtleaveBal.Rows[0]["NO_OF_DAYS"].ToString();
                txtAccuredLeaveBalance.Text = dtleaveBal.Rows[0]["NO_OF_DAYS"].ToString();


                if (txtLeaveBalance.Text != string.Empty)
                {
                    txtLeaveBalance.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtLeaveBalance.Text);
                }
                //if (txtNoOfDays.Text != string.Empty)
                //{
                //    txtNoOfDays.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtNoOfDays.Text);
                //}

                if (txtAccuredLeaveBalance.Text != string.Empty)
                {
                    txtAccuredLeaveBalance.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtLeaveBalance.Text);
                }
            }
            else
            {
                txtLeaveBalance.Text = "0";
                txtNoOfDays.Text = "0";
                txtAccuredLeaveBalance.Text = "0";
            }
            NoOfDays = CommonUtils.ConvertStringToDecimal(txtNoOfDays.Text.ToString());
            leaveBalance = CommonUtils.ConvertStringToDecimal(txtLeaveBalance.Text.ToString());

            //if (ddlLeave.SelectedValue == "AL")
            //{
            //    leaveBalance = CommonUtils.ConvertStringToDecimal(txtAccuredLeaveBalance.Text.ToString());
            //}
            //else
            //{
               
            //}
        }

        protected void txtFromDate_TextChanged(object sender, EventArgs e)
        {
        }

        protected void ddlStaffName_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dt_empdept = Employee_BLL.getEmployeeCurrentDepartment(ddlStaffName.SelectedValue);
            if (dt_empdept.Rows.Count > 0)
            {
                ddlDepartment.SelectedValue = dt_empdept.Rows[0]["EMP_DEPT_ID"].ToString();
            }
            dtGridData = DBMethod.ExecuteQuery(StaffLeaveDetails_DAL.GetStaffLeaveDtls_asperemp(ddlFinancialYear.SelectedValue, ddlDepartment.SelectedValue, ddlStaffName.SelectedValue)).Tables[0];
            BindGrid(dtGridData);
        }

        protected void txtToDate_TextChanged(object sender, EventArgs e)
        {

        }

    }
}