﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.BLL.AP;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.BLL;
using VMVServices.Web;
using VMVServices.Services.Data;
namespace FIN.Client.HR
{
    public partial class LeaveEnhancementEntry : PageBase
    {

        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();
        DataTable dtGridData = new DataTable();
        Boolean savedBool;
        Boolean bol_rowVisiable;
        HR_LEAVE_ENCASHMENT hR_LEAVE_ENCASHMENT = new HR_LEAVE_ENCASHMENT();
        HR_EMP_LEAVE_SAL_ELEMENT hr_emp_leave_sal_element = new HR_EMP_LEAVE_SAL_ELEMENT();
        System.Collections.SortedList slControls = new System.Collections.SortedList();
        //string ProReturn = null;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));

            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";

            }

            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
            }

            if (VMVServices.Web.Utils.AddAllowed != FINAppConstants.Y && Master.Mode == FINAppConstants.Add)
            {
                btnSave.Visible = false;
            }

        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();


                Startup();
                FillComboBox();

                txtTransDate.Text = DBMethod.ConvertDateToString(DateTime.Now.ToShortDateString());

                ddlLeave.Enabled = false;
                DataTable dt_data = FIN.BLL.SSM.SystemOptions_BLL.fn_getAnnualLeave();
                dtGridData = DBMethod.ExecuteQuery(FIN.DAL.HR.AppraisalAppraiser_DAL.GetElementSalaryDetails(Master.StrRecordId)).Tables[0];

                if (dt_data.Rows.Count > 0)
                {
                    if (dt_data.Rows[0]["hr_earn_leave"] != null)
                    {
                        try
                        {
                            ddlLeave.SelectedValue = dt_data.Rows[0]["hr_earn_leave"].ToString();


                        }
                        catch (Exception ex)
                        {

                        }
                    }
                }


                EntityData = null;
                DivBank.Visible = false;
                btnPrint.Visible = false;
                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    if (Request.QueryString.ToString().Contains("NAV_MODE"))
                    {

                        HR_LEAVE_APPLICATIONS hR_LEAVE_APPLICATIONS = new HR_LEAVE_APPLICATIONS();
                        using (IRepository<HR_LEAVE_APPLICATIONS> userCtx = new DataRepository<HR_LEAVE_APPLICATIONS>())
                        {
                            hR_LEAVE_APPLICATIONS = userCtx.Find(r =>
                                (r.LEAVE_REQ_ID == Master.StrRecordId)
                                ).SingleOrDefault();
                        }
                        if (hR_LEAVE_APPLICATIONS != null)
                        {
                            ddlDepartment.SelectedValue = hR_LEAVE_APPLICATIONS.DEPT_ID;
                            FILLEmployee();
                            ddlEmployee.SelectedValue = hR_LEAVE_APPLICATIONS.STAFF_ID;
                            fillLeaveTransaction();
                            ddlTransTyp.SelectedValue = "Application";
                            fillTransID();
                            ddlTransID.SelectedValue = Master.StrRecordId;
                            LeaveIdChange();
                        }
                        BindGrid(dtGridData);
                        Master.StrRecordId = "0";
                        Master.Mode = FINAppConstants.Add;
                        return;
                    }

                    DivBank.Visible = true;
                    btnPrint.Visible = true;
                    btnSave.Visible = false;
                    using (IRepository<HR_LEAVE_ENCASHMENT> userCtx = new DataRepository<HR_LEAVE_ENCASHMENT>())
                    {
                        hR_LEAVE_ENCASHMENT = userCtx.Find(r =>
                            (r.LC_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = hR_LEAVE_ENCASHMENT;
                    if (hR_LEAVE_ENCASHMENT.LC_DATE != null)
                    {
                        txtTransDate.Text = DBMethod.ConvertDateToString(hR_LEAVE_ENCASHMENT.LC_DATE.ToString());
                    }


                    ddlDepartment.SelectedValue = hR_LEAVE_ENCASHMENT.LC_DEPT_ID;
                    FILLEmployee();
                    ddlEmployee.SelectedValue = hR_LEAVE_ENCASHMENT.LC_EMP_ID;
                    //FILLLeaveType();
                    fillLeaveTransaction();
                    ddlLeave.SelectedValue = hR_LEAVE_ENCASHMENT.LEAVE_ID;
                    txtbalpay.Text = hR_LEAVE_ENCASHMENT.ATTRIBUTE1;
                    // FILLLeaveFrom();
                    if (hR_LEAVE_ENCASHMENT.LC_FROM_DT != null)
                    {
                        txtLeaveFrom.Text = DBMethod.ConvertDateToString(hR_LEAVE_ENCASHMENT.LC_FROM_DT.ToString());
                    }
                    if (hR_LEAVE_ENCASHMENT.LC_TO_DT != null)
                    {
                        txtLeaveTo.Text = DBMethod.ConvertDateToString(hR_LEAVE_ENCASHMENT.LC_TO_DT.ToString());
                    }

                    txtNoofdays.Text = hR_LEAVE_ENCASHMENT.LC_NO_OF_DAYS.ToString();
                    txtLevSal.Text = DBMethod.GetAmtDecimalCommaSeparationValue(hR_LEAVE_ENCASHMENT.LC_LEAVE_SALARY.ToString());
                    ddlPaymentOption.SelectedValue = hR_LEAVE_ENCASHMENT.LC_PAYMENT_OPTION;
                    if (hR_LEAVE_ENCASHMENT.LC_PAYMENT_OPTION == "IN_PAYROLL")
                    {
                        IdPayPrd.Visible = true;
                        ddlPayperiod.SelectedValue = hR_LEAVE_ENCASHMENT.LC_PAY_PERIOD_ID;

                    }
                    else
                    {
                        IdPayPrd.Visible = false;
                    }
                    ddlTransTyp.SelectedValue = hR_LEAVE_ENCASHMENT.TRANS_TYPE;
                    fillTransID();
                    ddlTransID.SelectedValue = hR_LEAVE_ENCASHMENT.TRANS_ID;
                    ddlBank.SelectedValue = hR_LEAVE_ENCASHMENT.BANK_ID;

                    txtRemarks.Text = hR_LEAVE_ENCASHMENT.LC_REMARKS;
                    chkAirticket.Checked = hR_LEAVE_ENCASHMENT.LC_AIR_TICKET == FINAppConstants.EnabledFlag ? true : false;
                    if (hR_LEAVE_ENCASHMENT.LC_AIR_TICKET_AMT == 0)
                    {
                        txtTicketAmount.Text = "";
                    }
                    else
                    {
                        txtTicketAmount.Text = DBMethod.GetAmtDecimalCommaSeparationValue(hR_LEAVE_ENCASHMENT.LC_AIR_TICKET_AMT.ToString());
                    }


                    hR_LEAVE_ENCASHMENT.ENABLED_FLAG = FINAppConstants.EnabledFlag;

                    if (ddlPaymentOption.SelectedValue == "IN_PAYROLL")
                    {
                        IdPayPrd.Visible = true;
                        btnPrint.Visible = false;
                        ddlBank.Visible = false;
                    }
                    else
                    {
                        IdPayPrd.Visible = false;
                        ddlBank.Visible = true;
                    }
                }
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>

        private void FillComboBox()
        {
            FIN.BLL.HR.LeaveApplication_BLL.GetDepartment(ref ddlDepartment);
            //FIN.BLL.HR.LeaveDefinition_BLL.GetLeaveType(ref ddlLeave, ddlDepartment.SelectedValue, ddlEmployee.SelectedValue);

            Lookup_BLL.GetLookUpValues(ref ddlPaymentOption, "PAYMENT_OPTION");
            FIN.BLL.PER.PayrollPeriods_BLL.GetPayrollWithoutApprovPeriods(ref ddlPayperiod);
            //FIN.BLL.HR.Employee_BLL.GetEmployeeName(ref ddlEmployee);
            Lookup_BLL.GetLookUpValues(ref ddlTransTyp, "TRANS_TYP");

            FIN.BLL.CA.Bank_BLL.fn_getBankName(ref ddlBank);
            FIN.BLL.HR.Employee_BLL.GetEmpName(ref ddlEmployee);
        }

        private void FILLEmployee()
        {
            FIN.BLL.HR.Employee_BLL.GetEmployeeNMasperdept_whohavALonly(ref ddlEmployee, ddlDepartment.SelectedValue);
            FIN.BLL.HR.LeaveDefinition_BLL.GetLeaveBasedDept(ref ddlLeave, ddlDepartment.SelectedValue);
            //FIN.BLL.HR.Employee_BLL.GetEmplName(ref ddlEmployee, ddlDepartment.SelectedValue);

        }

        //private void FILLLeaveType()
        //{
        //    FIN.BLL.HR.LeaveDefinition_BLL.GetLeaveType(ref ddlLeave, ddlDepartment.SelectedValue, ddlEmployee.SelectedValue);

        //}
        private void fillleavebal()
        {
            DataTable dtleavebal = new DataTable();
            dtleavebal = DBMethod.ExecuteQuery(FIN.DAL.HR.LeaveApplication_DAL.GetLeaveAvailabilit(ddlEmployee.SelectedValue, ddlLeave.SelectedValue)).Tables[0];
            if (dtleavebal.Rows.Count > 0)
            {
                txtNoofdays.Text = dtleavebal.Rows[0]["LEAVE_AVAIL"].ToString();
            }

        }

        //private void FILLLeaveFrom()
        //{
        //     DataTable dtdata = new DataTable();
        //     dtdata = DBMethod.ExecuteQuery(FIN.DAL.HR.LeaveApplication_DAL.GetLeaveAvailability(ddlEmployee.SelectedValue,ddlLeave.SelectedValue)).Tables[0];
        //    if (dtdata.Rows.Count > 0)
        //    {
        //        txtLeaveFrom.Text = dtdata.Rows[0]["EFFECTIVE_FROM_DT"].ToString();
        //        txtLeaveTo.Text = dtdata.Rows[0]["EFFECTIVE_TO_DT"].ToString();
        //        txtNoofdays.Text = dtdata.Rows[0]["LEAVE_AVAIL"].ToString();
        //    }
        //}




        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    hR_LEAVE_ENCASHMENT = (HR_LEAVE_ENCASHMENT)EntityData;
                }
                if (txtTransDate.Text != string.Empty)
                {
                    hR_LEAVE_ENCASHMENT.LC_DATE = DBMethod.ConvertStringToDate(txtTransDate.Text.ToString());
                }



                hR_LEAVE_ENCASHMENT.LC_DEPT_ID = ddlDepartment.SelectedValue;
                hR_LEAVE_ENCASHMENT.LC_EMP_ID = ddlEmployee.SelectedValue;
                hR_LEAVE_ENCASHMENT.LEAVE_ID = ddlLeave.SelectedValue;
                hR_LEAVE_ENCASHMENT.TRANS_TYPE = ddlTransTyp.SelectedValue;
                hR_LEAVE_ENCASHMENT.TRANS_ID = ddlTransID.SelectedValue;
                hR_LEAVE_ENCASHMENT.BANK_ID = ddlBank.SelectedValue;
                hR_LEAVE_ENCASHMENT.ATTRIBUTE1 = txtbalpay.Text;
                if (txtLeaveFrom.Text != string.Empty)
                {
                    hR_LEAVE_ENCASHMENT.LC_FROM_DT = DBMethod.ConvertStringToDate(txtLeaveFrom.Text.ToString());
                }
                if (txtLeaveTo.Text != string.Empty)
                {
                    hR_LEAVE_ENCASHMENT.LC_TO_DT = DBMethod.ConvertStringToDate(txtLeaveTo.Text.ToString());
                }


                hR_LEAVE_ENCASHMENT.LC_NO_OF_DAYS = CommonUtils.ConvertStringToDecimal(txtNoofdays.Text.ToString());
                hR_LEAVE_ENCASHMENT.LC_LEAVE_SALARY = CommonUtils.ConvertStringToDecimal(txtLevSal.Text.ToString());
                hR_LEAVE_ENCASHMENT.LC_PAYMENT_OPTION = ddlPaymentOption.SelectedValue;

                if (ddlPaymentOption.SelectedValue == "IN_PAYROLL")
                {
                    hR_LEAVE_ENCASHMENT.LC_PAY_PERIOD_ID = ddlPayperiod.SelectedValue;
                }


                hR_LEAVE_ENCASHMENT.LC_REMARKS = txtRemarks.Text;
                hR_LEAVE_ENCASHMENT.LC_AIR_TICKET = FINAppConstants.EnabledFlag;
                hR_LEAVE_ENCASHMENT.LC_AIR_TICKET_AMT = CommonUtils.ConvertStringToDecimal(txtTicketAmount.Text.ToString());
                hR_LEAVE_ENCASHMENT.LC_ORG_ID = VMVServices.Web.Utils.OrganizationID;

                hR_LEAVE_ENCASHMENT.ENABLED_FLAG = FINAppConstants.EnabledFlag;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    hR_LEAVE_ENCASHMENT.MODIFIED_BY = this.LoggedUserName;
                    hR_LEAVE_ENCASHMENT.MODIFIED_DATE = DateTime.Today;

                }
                else
                {
                    hR_LEAVE_ENCASHMENT.LC_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_097.ToString(), false, true);
                    //iNV_ITEM_MASTER.WORKFLOW_COMPLETION_STATUS = "1";
                    hR_LEAVE_ENCASHMENT.CREATED_BY = this.LoggedUserName;
                    hR_LEAVE_ENCASHMENT.CREATED_DATE = DateTime.Today;

                }

                hR_LEAVE_ENCASHMENT.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_LEAVE_ENCASHMENT.LC_ID);


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                //ErrorCollection = CommonUtils.IsEmptyGrid(dtGridData, "ElementSalaryAmount");
                //if (ErrorCollection.Count > 0)
                //{
                //    return;
                //}

                var tmpChildEntity = new List<Tuple<object, string>>();

                if (dtGridData.Rows.Count > 0)
                {
                    for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                    {
                        hr_emp_leave_sal_element = new HR_EMP_LEAVE_SAL_ELEMENT();
                        //if (dtGridData.Rows[iLoop][FINColumnConstants.LOOKUP_ID].ToString() != "0")
                        //{
                        //    using (IRepository<HR_EMP_LEAVE_SAL_ELEMENT> userCtx = new DataRepository<HR_EMP_LEAVE_SAL_ELEMENT>())
                        //    {
                        //        hr_emp_leave_sal_element = userCtx.Find(r =>
                        //            (r.LEAVE_SAL_ELEMENT_ID == FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_040_D.ToString(), false, true))
                        //            ).SingleOrDefault();
                        //    }
                        //}

                        if (dtGridData.Rows[iLoop][FINColumnConstants.LEAVE_SAL_ELEMENT_ID].ToString() != "0" && dtGridData.Rows[iLoop][FINColumnConstants.LEAVE_SAL_ELEMENT_ID].ToString() != string.Empty)
                        {
                            hr_emp_leave_sal_element = PurchaseOrder_BLL.getDetailSalClassEntity(dtGridData.Rows[iLoop][FINColumnConstants.LEAVE_SAL_ELEMENT_ID].ToString());
                        }

                        //if (dtGridData.Rows[iLoop][FINColumnConstants.RA_DTL_ID].ToString() != "0")
                        //{
                        //    FIN.BLL.HR.AppraisalAppraiser_BLL.getClassEntity(dtGridData.Rows[iLoop][FINColumnConstants.RA_DTL_ID].ToString());
                        //}


                        hr_emp_leave_sal_element.PAY_EMP_ID = ddlEmployee.SelectedValue.ToString();
                        hr_emp_leave_sal_element.PAY_AMOUNT = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop][FINColumnConstants.PAY_AMOUNT].ToString());
                        hr_emp_leave_sal_element.PAY_ELEMENT_ID = (dtGridData.Rows[iLoop][FINColumnConstants.LOOKUP_ID].ToString());
                        hr_emp_leave_sal_element.ENABLED_FLAG = FINAppConstants.Y;

                        hr_emp_leave_sal_element.WORKFLOW_COMPLETION_STATUS = "1";
                        hr_emp_leave_sal_element.LC_ID = hR_LEAVE_ENCASHMENT.LC_ID;

                        if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                        {
                            //hr_emp_leave_sal_element.LEAVE_SAL_ELEMENT_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_040_D.ToString(), false, true);
                            //tmpChildEntity.Add(new Tuple<object, string>(hr_emp_leave_sal_element, FINAppConstants.Delete));
                            tmpChildEntity.Add(new Tuple<object, string>(hr_emp_leave_sal_element, FINAppConstants.Delete));
                        }
                        else
                        {

                            if (dtGridData.Rows[iLoop][FINColumnConstants.LEAVE_SAL_ELEMENT_ID].ToString() != "0" && dtGridData.Rows[iLoop][FINColumnConstants.LEAVE_SAL_ELEMENT_ID].ToString() != string.Empty)
                            {
                                //hr_emp_leave_sal_element.LEAVE_SAL_ELEMENT_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_040_D.ToString(), false, true);
                                hr_emp_leave_sal_element.MODIFIED_BY = this.LoggedUserName;
                                hr_emp_leave_sal_element.MODIFIED_DATE = DateTime.Today;
                                tmpChildEntity.Add(new Tuple<object, string>(hr_emp_leave_sal_element, FINAppConstants.Update));

                            }
                            else
                            {
                                hr_emp_leave_sal_element.LEAVE_SAL_ELEMENT_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_097_D.ToString(), false, true);
                                hr_emp_leave_sal_element.CREATED_BY = this.LoggedUserName;
                                hr_emp_leave_sal_element.CREATED_DATE = DateTime.Today;
                                tmpChildEntity.Add(new Tuple<object, string>(hr_emp_leave_sal_element, FINAppConstants.Add));
                            }
                        }

                    }
                }
                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            CommonUtils.SavePCEntity<HR_LEAVE_ENCASHMENT, HR_EMP_LEAVE_SAL_ELEMENT>(hR_LEAVE_ENCASHMENT, tmpChildEntity, hr_emp_leave_sal_element);
                            savedBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {
                            CommonUtils.SavePCEntity<HR_LEAVE_ENCASHMENT, HR_EMP_LEAVE_SAL_ELEMENT>(hR_LEAVE_ENCASHMENT, tmpChildEntity, hr_emp_leave_sal_element, true);
                            savedBool = true;
                            break;
                        }
                }
                //if (hR_LEAVE_ENCASHMENT.WORKFLOW_COMPLETION_STATUS == "1")
                //{
                //    FINSP.GetSP_GL_Posting(hR_LEAVE_ENCASHMENT.LC_ID, "HR_097");
                //}

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        public string CalculateTotalAmount(DataTable dtGridData, string ColumnName, string extraCondition = "")
        {
            //dtGridData.Compute("sum(ColumnName)", "DELETED='N'").ToString()
            string deletedValue = "N";
            string str_Condition = "0";
            double totInvItemAmt = 0;

            if (dtGridData.Rows.Count > 0)
            {
                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    totInvItemAmt += CommonUtils.ConvertStringToDouble(dtGridData.Rows[iLoop]["PAY_AMOUNT"].ToString());

                    if (dtGridData.Rows[iLoop]["DELETED"].ToString() == "1")
                    {
                        totInvItemAmt -= CommonUtils.ConvertStringToDouble(dtGridData.Rows[iLoop]["PAY_AMOUNT"].ToString());
                    }

                    //if (dtGridData.Rows[iLoop]["INV_LINE_ID"].ToString() == "" || dtGridData.Rows[iLoop]["INV_LINE_ID"].ToString() == "0")
                    //{
                    //    deletedValue = dtGridData.Rows[iLoop]["DELETED"].ToString();
                    //}
                }
                str_Condition = "DELETED='" + deletedValue + "'";
            }
            if (extraCondition.Length > 0)
            {
                str_Condition += " and " + extraCondition;
            }

            return totInvItemAmt.ToString();

            //return dtGridData.Compute("sum(" + ColumnName + ")", str_Condition).ToString();
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                AssignToBE();



                //emptyvalid();
                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                string str_finyear = FINSP.GetSPFOR_FiscalYear((txtTransDate.Text));

                hR_LEAVE_ENCASHMENT.LC_FISCAL_YEAR = str_finyear;
                // Duplicate Validation Through Backend Package PKG_VALIDATIONS



                //ProReturn = FIN.DAL.AP.Item_DAL.GetSPFOR_DUPLICATE_CHECK(iNV_ITEM_MASTER.ITEM_CODE,txtItemName.Text, iNV_ITEM_MASTER.ORG_ID, iNV_ITEM_MASTER.ITEM_ID);

                //if (ProReturn != string.Empty)
                //{
                //    if (ProReturn != "0")
                //    {
                //        ErrorCollection.Add("ITEMNAME", ProReturn);
                //        if (ErrorCollection.Count > 0)
                //        {
                //            return;
                //        }
                //    }
                //}


                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                if (VMVServices.Web.Utils.IsAlert == "1")
                {
                    FINSQL.UpdateAlertUserLevel();
                }
                if (hR_LEAVE_ENCASHMENT.WORKFLOW_COMPLETION_STATUS == "1")
                {
                    FIN.BLL.SSM.Alert_BLL.GenerateEmail(FINMessageConstatns.Annual_Leave_payment_Alert);
                    if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0" && savedBool == true)
                    {
                        //if (VMVServices.Web.Utils.IsAlert == "1")
                        //{
                        //    FINSQL.UpdateAlertUserLevel();
                        //}
                    }
                    LeaveApplication_DAL.GetSPFOR_LEAVE_ENCASH_POSTING(hR_LEAVE_ENCASHMENT.LC_ID, hR_LEAVE_ENCASHMENT.LC_PAYMENT_OPTION, hR_LEAVE_ENCASHMENT.LC_PAY_PERIOD_ID);
                }
                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                if (ddlTransTyp.SelectedValue == "Settlement")
                {
                    StaffLeaveDetails_DAL.GetSP_EMP_LEAVE_TRN_LEDGER("LENCASH", hR_LEAVE_ENCASHMENT.LC_ID.ToString(), "LNC", (txtTransDate.Text), CommonUtils.ConvertStringToDecimal((DBMethod.ConvertStringToDate(txtTransDate.Text).Month).ToString()), str_finyear, ddlEmployee.SelectedValue, VMVServices.Web.Utils.OrganizationID, ddlLeave.SelectedValue, ddlDepartment.SelectedValue.ToString(), CommonUtils.ConvertStringToDecimal(txtNoofdays.Text));
                    
                    StaffLeaveDetails_DAL.UPDATE_EMP_LEAVE_BALANCE(str_finyear, ddlEmployee.SelectedValue, hR_LEAVE_ENCASHMENT.LC_ORG_ID.ToString(), hR_LEAVE_ENCASHMENT.LEAVE_ID.ToString(), ddlDepartment.SelectedValue.ToString(), decimal.Parse(hR_LEAVE_ENCASHMENT.LC_NO_OF_DAYS.ToString()), "APPLICATION");
                }
                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                else
                {
                    if (savedBool)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion




        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                DBMethod.DeleteEntity<HR_LEAVE_ENCASHMENT>(hR_LEAVE_ENCASHMENT);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            FILLEmployee();
        }

        protected void ddlEmployee_SelectedIndexChanged(object sender, EventArgs e)
        {
            //FILLLeaveType();
            fillLeaveTransaction();
        }

        private void fillLeaveTransaction()
        {
            DataTable dt_empdept =FIN.BLL.HR.Employee_BLL.getEmployeeCurrentDepartment(ddlEmployee.SelectedValue);
            if (dt_empdept.Rows.Count > 0)
            {
                ddlDepartment.SelectedValue = dt_empdept.Rows[0]["EMP_DEPT_ID"].ToString();
            }
            FIN.BLL.HR.LeaveDefinition_BLL.GetLeaveBasedDept(ref ddlLeave, ddlDepartment.SelectedValue);
            fillTransID();
            FIN.BLL.HR.LeaveApplication_BLL.GetLeaveReqID(ref ddlTransID, ddlEmployee.SelectedValue, Master.Mode);



        }

        protected void ddlLeave_SelectedIndexChanged(object sender, EventArgs e)
        {
            // FILLLeaveFrom();
            getAvailableLeave();

        }
        private void getAvailableLeave()
        {
            if (txtTransDate.Text.ToString().Length > 0)
            {
                //string str_days = FIN.DAL.HR.StaffLeaveDetails_DAL.GetSP_EMP_LEAVE_PROVISION(ddlDepartment.SelectedValue, ddlEmployee.SelectedValue, ddlLeave.SelectedValue, DBMethod.ConvertStringToDate(txtTransDate.Text.ToString()), ddlTransTyp.SelectedValue);

                //string str_days = FIN.DAL.HR.StaffLeaveDetails_DAL.GetSP_EMP_LEAVE_PROVISION(ddlDepartment.SelectedValue, ddlEmployee.SelectedValue, ddlLeave.SelectedValue,txtTransDate.Text);



                string str_days = DBMethod.GetDecimalValue(StaffLeaveDetails_DAL.getAccuredleavebalnce(ddlDepartment.SelectedValue, ddlEmployee.SelectedValue, ddlLeave.SelectedValue, txtTransDate.Text)).ToString();

                lblAvailableLeave.Text = "Available Leave :  " + str_days;
                hf_AvailableLeave.Value = str_days;
            }
        }

        protected void txtLeaveTo_TextChanged(object sender, EventArgs e)
        {
            if (txtLeaveFrom.Text.Trim() != string.Empty && txtLeaveTo.Text.Trim() != string.Empty)
            {
                DateTime FrmDate = DBMethod.ConvertStringToDate(txtLeaveFrom.Text.ToString());
                DateTime ToDate = DBMethod.ConvertStringToDate(txtLeaveTo.Text.ToString());

                if (FrmDate == ToDate)
                {
                    txtNoofdays.Text = "1";
                }
                else
                {
                    txtNoofdays.Text = ((ToDate - FrmDate).Days + 1).ToString();
                }
            }
        }
        private void getBalPayable()
        {
            decimal balpay = 0;
            if (txtLevSal.Text == string.Empty && txtInvoiceAmount.Text == "0")
            {
                balpay = 0;
            }
            else
            {
                balpay = CommonUtils.ConvertStringToDecimal(txtLevSal.Text) - CommonUtils.ConvertStringToDecimal(txtInvoiceAmount.Text);
            }

            txtbalpay.Text = balpay.ToString();

            if (txtbalpay.Text != string.Empty && txtbalpay.Text != "0")
            {
                txtbalpay.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtbalpay.Text);
            }
        }
        protected void txtLevSal_TextChanged(object sender, EventArgs e)
        {
            getBalPayable();

        }

        protected void txtNoofdays_TextChanged(object sender, EventArgs e)
        {
            ChkNoofDays();

        }
        private void ChkNoofDays()
        {
            if (txtNoofdays.Text.Length > 0)
            {


                if (ddlTransTyp.SelectedValue == "Settlement")
                {
                    //if (((CommonUtils.ConvertStringToDouble(hf_AvailableLeave.Value) * 75) / 100) < CommonUtils.ConvertStringToDouble(txtNoofdays.Text))
                    //{
                    //    txtNoofdays.Text = "";
                    //    ErrorCollection.Add("InvalidNoofdays", "Only Able to apply less then or equal to 75 % of days from avaiable leave ");
                    //    Master.ShowMessage(ErrorCollection, FINAppConstants.ERROR);
                    //    return;
                    //}
                    //txtLevSal.Text = FIN.DAL.HR.LeaveApplication_DAL.GetSPFOR_Leave_Sal(ddlPayperiod.SelectedValue, ddlDepartment.SelectedValue, ddlEmployee.SelectedValue, txtNoofdays.Text);
                    string str_BasicAmt = FIN.DAL.HR.LeaveApplication_DAL.getBasicAmount(ddlEmployee.SelectedValue);
                    double dbl_oneDaySal = Convert.ToDouble(str_BasicAmt) / 26;
                    txtLevSal.Text = DBMethod.GetAmtDecimalCommaSeparationValue((dbl_oneDaySal * Convert.ToDouble(txtNoofdays.Text)).ToString());

                }
                else if (ddlTransTyp.SelectedValue == "Application")
                {
                    DataTable dtlevdt = new DataTable();
                    DateTime dtfrmdt = new DateTime();
                    DateTime dtTodt = new DateTime();
                    dtlevdt = DBMethod.ExecuteQuery(FIN.DAL.HR.LeaveApplication_DAL.GetLeaveReqDat(ddlEmployee.SelectedValue, ddlTransID.SelectedValue)).Tables[0];

                    dtfrmdt = Convert.ToDateTime(dtlevdt.Rows[0]["LEAVE_DATE_FROM"].ToString());
                    dtTodt = Convert.ToDateTime(dtlevdt.Rows[0]["LEAVE_DATE_TO"].ToString());
                    txtLevSal.Text = FIN.DAL.HR.LeaveApplication_DAL.GetSPFOR_Leave_Sal_app(ddlPayperiod.SelectedValue, ddlDepartment.SelectedValue, ddlEmployee.SelectedValue, dtfrmdt, dtTodt);
                }

            }
        }

        protected void ddlTransTyp_SelectedIndexChanged(object sender, EventArgs e)
        {
            fillTransID();
        }

        private void fillTransID()
        {

            DataTable dt_data = FIN.BLL.SSM.SystemOptions_BLL.fn_getAnnualLeave();
            if (dt_data.Rows.Count > 0)
            {
                if (dt_data.Rows[0]["hr_earn_leave"] != null)
                {
                    try
                    {
                        ddlLeave.SelectedValue = dt_data.Rows[0]["hr_earn_leave"].ToString();
                        ddlLeave.Enabled = false;
                        getAvailableLeave();
                    }
                    catch (Exception ex)
                    {

                    }
                }
            }


            if (ddlTransTyp.SelectedValue == "Application")
            {

                ddlTransID.Enabled = true;
                txtNoofdays.Enabled = false;
            }
            else
            {
                ddlTransID.Enabled = false;
                ddlTransID.SelectedValue = "";
                txtNoofdays.Enabled = true;
            }
        }

        protected void ddlTransID_SelectedIndexChanged(object sender, EventArgs e)
        {
            LeaveIdChange();
        }
        private void LeaveIdChange()
        {
            DataTable dtNoofdys = new DataTable();
            dtNoofdys = DBMethod.ExecuteQuery(FIN.DAL.HR.LeaveApplication_DAL.GetNoofDys(ddlTransID.SelectedValue)).Tables[0];
            if (dtNoofdys.Rows.Count > 0)
            {
                txtNoofdays.Text = dtNoofdys.Rows[0]["NO_OF_DAYS"].ToString();
                ChkNoofDays();
            }
        }

        protected void btnPrint_Click(object sender, ImageClickEventArgs e)
        {

            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;


                //if (ddlSupplierName.SelectedValue != string.Empty)
                //{
                //    htFilterParameter.Add("SUPPLIER_NAME", ddlSupplierName.SelectedValue);
                //}
                if (txtTransDate.Text != string.Empty)
                {
                    htFilterParameter.Add("From_Date", txtTransDate.Text);
                }
                //if (txtToDate.Text != string.Empty)
                //{
                //    htFilterParameter.Add("To_Date", txtToDate.Text);
                //}


                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;

                ReportData = FIN.BLL.AR.ARAgingAnalysis_BLL.GetCheckListReportData();

                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());

                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerPath + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("APAgingAnalysisReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void ddlPaymentOption_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlPaymentOption.SelectedValue == "IN_PAYROLL")
            {
                IdPayPrd.Visible = true;
                btnPrint.Visible = false;
                ddlBank.Visible = false;
            }
            else
            {
                //DivBank.Visible = true;
                IdPayPrd.Visible = false;
                ddlBank.Visible = true;
            }
        }

        private void AssignToGridBE()
        {
            try
            {
                ErrorCollection.Clear();


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                var tmpChildEntity = new List<Tuple<object, string>>();

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {

                    if (dtGridData.Rows[iLoop]["PAY_ELEMENT_ID"].ToString() != "0")
                    {
                        using (IRepository<HR_EMP_LEAVE_SAL_ELEMENT> userCtx = new DataRepository<HR_EMP_LEAVE_SAL_ELEMENT>())
                        {
                            hr_emp_leave_sal_element = userCtx.Find(r =>
                                (r.PAY_ELEMENT_ID == dtGridData.Rows[iLoop]["LOOKUP_ID"].ToString())
                                ).SingleOrDefault();
                        }
                    }

                    hr_emp_leave_sal_element.PAY_ELEMENT_ID = dtGridData.Rows[iLoop]["LOOKUP_ID"].ToString();
                    hr_emp_leave_sal_element.PAY_EMP_ID = ddlEmployee.SelectedValue.ToString();
                    hr_emp_leave_sal_element.PAY_AMOUNT = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop]["PAY_AMOUNT"].ToString());
                    hr_emp_leave_sal_element.LC_ID = hR_LEAVE_ENCASHMENT.LC_ID;


                    if (dtGridData.Rows[iLoop]["EFFECTIVE_FROM_DT"] != DBNull.Value)
                    {
                        hr_emp_leave_sal_element.EFFECTIVE_FROM_DT = DateTime.Parse(dtGridData.Rows[iLoop]["EFFECTIVE_FROM_DT"].ToString());
                    }

                    if (dtGridData.Rows[iLoop]["EFFECTIVE_TO_DT"] != DBNull.Value)
                    {
                        hr_emp_leave_sal_element.EFFECTIVE_TO_DT = DateTime.Parse(dtGridData.Rows[iLoop]["EFFECTIVE_TO_DT"].ToString());
                    }

                    if (dtGridData.Rows[iLoop]["ENABLED_FLAG"].ToString().ToUpper() == "TRUE")
                    {
                        hr_emp_leave_sal_element.ENABLED_FLAG = FINAppConstants.Y;
                    }
                    else
                    {
                        hr_emp_leave_sal_element.ENABLED_FLAG = FINAppConstants.N;
                    }

                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {

                        tmpChildEntity.Add(new Tuple<object, string>(hr_emp_leave_sal_element, "D"));
                    }
                    else
                    {
                        if (dtGridData.Rows[iLoop]["LOOKUP_ID"].ToString() != "0")
                        {
                            hr_emp_leave_sal_element.PAY_ELEMENT_ID = dtGridData.Rows[iLoop]["LOOKUP_ID"].ToString();
                            hr_emp_leave_sal_element.MODIFIED_BY = this.LoggedUserName;
                            hr_emp_leave_sal_element.MODIFIED_DATE = DateTime.Today;
                            hr_emp_leave_sal_element.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hr_emp_leave_sal_element.PAY_ELEMENT_ID);
                            DBMethod.SaveEntity<HR_EMP_LEAVE_SAL_ELEMENT>(hr_emp_leave_sal_element, true);
                            savedBool = true;
                        }
                        else
                        {

                            hr_emp_leave_sal_element.PAY_ELEMENT_ID = dtGridData.Rows[iLoop]["LOOKUP_ID"].ToString();
                            hr_emp_leave_sal_element.CREATED_BY = this.LoggedUserName;
                            hr_emp_leave_sal_element.CREATED_DATE = DateTime.Today;
                            hr_emp_leave_sal_element.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hr_emp_leave_sal_element.PAY_ELEMENT_ID);
                            DBMethod.SaveEntity<HR_EMP_LEAVE_SAL_ELEMENT>(hr_emp_leave_sal_element);
                            savedBool = true;
                        }


                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WRI_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private DataRow AssignToGridControl(GridViewRow gvData, DataTable tmpdtGridData, string GMode, int rowindex)
        {
            //Dictionary<string, string> Prop_File_Data;
            //Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));
            System.Collections.SortedList slControls = new System.Collections.SortedList();
            System.Collections.SortedList s2Controls = new System.Collections.SortedList();
            DropDownList ddlElement = gvData.FindControl("ddlelement") as DropDownList;
            TextBox txtamount = gvData.FindControl("txtamount") as TextBox;

            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList[FINColumnConstants.LEAVE_SAL_ELEMENT_ID] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }

            slControls[0] = ddlElement;
            s2Controls[1] = txtamount;


            ErrorCollection.Clear();
            string strCtrlTypes = FINAppConstants.DROP_DOWN_LIST;
            string strMessage = "Element";
            ErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);
            if (ErrorCollection.Count > 0)
                return drList;

            string strCondition = "LOOKUP_NAME='" + ddlElement.Text.Trim().ToUpper() + "'";
            strMessage = FINMessageConstatns.RecordAlreadyExists;
            ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            if (ErrorCollection.Count > 0)
            {

                return drList;
            }

            if (ddlElement.SelectedItem != null)
            {
                drList["LOOKUP_ID"] = ddlElement.SelectedItem.Value.ToString();
                drList["LOOKUP_NAME"] = ddlElement.SelectedItem.Text.ToString();
                drList["PAY_AMOUNT"] = txtamount.Text;
            }

            drList[FINColumnConstants.DELETED] = FINAppConstants.N;

            return drList;
        }

        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                //else if (FacilityMasterBLL.ErrorCollection.Count > 0)
                //{
                //    ErrorCollection = FacilityMasterBLL.ErrorCollection;
                //    return;
                //}
                //else
                //{
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
                //}
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Row_upd", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                dtGridData.Rows[e.RowIndex][FINColumnConstants.DELETED] = FINAppConstants.Y;
                //BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Row_del", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Row_Edt", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            try
            {
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = -1;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_ROW_CNCL_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }


        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }

                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_ROW_CMD", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AppraisalDefineKRA", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (bol_rowVisiable)
                    e.Row.Visible = false;

                if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                {
                    e.Row.Visible = false;
                }

            }
        }

        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddlElement = tmpgvr.FindControl("ddlelement") as DropDownList;
                TextBox txtamount = tmpgvr.FindControl("txtamount") as TextBox;
                //Lookup_BLL.GetLookUpValues(ref ddlElement, "EARNING CLASS");
                FIN.BLL.PER.PayrollElements_BLL.getPayElements_NonRecurring_Deductiononly(ref ddlElement);


                if (gvData.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                {
                    ddlElement.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.LOOKUP_ID].ToString();
                    //txtamount.Text = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.PAY_AMOUNT].ToString();
                    Session["SaleditValue"] = txtamount.Text;
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PE_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;

                if (dtData.Rows.Count > 0)
                {
                    //if (dtData.Rows[0]["PO_ITEM_UNIT_PRICE"].ToString().Length > 0)
                    //{
                    //    dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("PO_ITEM_UNIT_PRICE", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("PO_ITEM_UNIT_PRICE"))));
                    //}
                    if (dtData.Rows[0]["PAY_AMOUNT"].ToString().Length > 0)
                    {
                        dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("PAY_AMOUNT", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("PAY_AMOUNT"))));
                    }
                    dtData.AcceptChanges();
                }
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    //dr["ENABLED_FLAG"] = "FALSE";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);
                txtInvoiceAmount.Text = CalculateTotalAmount(dtData, "PAY_AMOUNT");

                getBalPayable();


                if (txtInvoiceAmount.Text != string.Empty && txtInvoiceAmount.Text != "0")
                {
                    txtInvoiceAmount.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtInvoiceAmount.Text);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WRI_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

    }





}
