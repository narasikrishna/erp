﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="EmployeeWorkProfileEntry.aspx.cs" Inherits="FIN.Client.HR.EmployeeWorkProfileEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 900px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 120px" id="lblDepartment">
                Department
            </div>
            <div class="divtxtBox  LNOrient" style="width: 250px">
                <asp:DropDownList ID="ddlDepartment" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    AutoPostBack="True" TabIndex="1" OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged">
                    <%--<asp:ListItem>---Select---</asp:ListItem>--%>
                </asp:DropDownList>
            </div>
           <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 120px" id="lblDesignation">
                Designation
            </div>
            <div class="divtxtBox  LNOrient" style="width: 250px">
                <asp:DropDownList ID="ddlDesignation" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    AutoPostBack="True" TabIndex="2" OnSelectedIndexChanged="ddlDesignation_SelectedIndexChanged">
                  <%--  <asp:ListItem>---Select---</asp:ListItem>--%>
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 120px" id="lblCategory">
                Category
            </div>
            <div class="divtxtBox  LNOrient" style="width: 250px">
                <asp:DropDownList ID="ddlcategory" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    AutoPostBack="True" TabIndex="3" OnSelectedIndexChanged="ddlcategory_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 120px" id="lblJob">
                Job
            </div>
            <div class="divtxtBox  LNOrient" style="width: 250px">
                <asp:DropDownList ID="ddljob" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    AutoPostBack="True" TabIndex="4" OnSelectedIndexChanged="ddljob_SelectedIndexChanged">
                   <%-- <asp:ListItem>---Select---</asp:ListItem>--%>
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 120px" id="lblposition">
                Position
            </div>
            <div class="divtxtBox  LNOrient" style="width: 250px">
                <asp:DropDownList ID="ddlposition" runat="server" AutoPostBack="True" CssClass="validate[required] RequiredField ddlStype"
                    TabIndex="5" OnSelectedIndexChanged="ddlposition_SelectedIndexChanged">
                   <%-- <asp:ListItem>---Select---</asp:ListItem>--%>
                </asp:DropDownList>
            </div>
           <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 120px" id="lblGrade">
                Grade
            </div>
            <div class="divtxtBox  LNOrient" style="width: 250px">
                <asp:DropDownList ID="ddlgrade" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    AutoPostBack="True" TabIndex="6" OnSelectedIndexChanged="ddlgrade_SelectedIndexChanged">
                    <%--<asp:ListItem>---Select---</asp:ListItem>--%>
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 120px" id="lblProfile">
                Profile
            </div>
            <div class="divtxtBox  LNOrient" style="width: 640px">
                <asp:DropDownList ID="ddlProfile" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    AutoPostBack="True" TabIndex="7">
                    <%--<asp:ListItem>---Select---</asp:ListItem>--%>
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 120px" id="lblFromDate">
                From Date
            </div>
            <div class="divtxtBox  LNOrient" style="width: 155px">
                <asp:TextBox ID="txtfromdate" CssClass="validate[required,custom[ReqDateDDMMYYY],dateRange[dg1]] RequiredField txtBox"
                    TabIndex="8" runat="server"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtfromdate"
                    OnClientDateSelectionChanged="checkDate" />
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtfromdate" />
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 162px" id="lblToDate">
                To Date
            </div>
            <div class="divtxtBox  LNOrient" style="width: 155px">
                <asp:TextBox ID="txttodate" CssClass="validate[custom[ReqDateDDMMYYY],dateRange[dg1]] txtBox"
                    TabIndex="9" runat="server"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender2" TargetControlID="txttodate"
                    OnClientDateSelectionChanged="checkDate" />
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txttodate" />
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 100px" id="lblActiveFlag">
                Active
            </div>
            <div class="divtxtBox  LNOrient" style="width: 50px">
                <asp:CheckBox ID="chKEnabledFlag" runat="server" Checked="True" TabIndex="10" Text =" " />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="LNOrient">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                Width="100%" DataKeyNames="EMP_WORK_PROF_DTL_ID,EMP_PROF_DTL_ID,EMPLOYEE_RATING,EMP_ID,EMPLOYEE_NAME,PROF_DTL_ID,DELETED"
                OnRowCancelingEdit="gvData_RowCancelingEdit" OnRowCommand="gvData_RowCommand"
                OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound" OnRowDeleting="gvData_RowDeleting"
                OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating" ShowFooter="True">
                <Columns>
                    <asp:TemplateField HeaderText="Employee">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlEmployee" Width="100%" runat="server" CssClass="EntryFont RequiredField ddlStype"
                                TabIndex="11">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlEmployee" Width="100%" runat="server" CssClass="EntryFont RequiredField ddlStype"
                                TabIndex="11">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblEmpName" runat="server" TabIndex="10" Text='<%# Eval("EMPLOYEE_NAME") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Employee Details">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlEmpProfiledtl" Width="100%" runat="server" CssClass="EntryFont RequiredField ddlStype"
                                TabIndex="12">
                                <asp:ListItem>---Select---</asp:ListItem>
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlEmpProfiledtl" Width="100%" runat="server" CssClass="EntryFont RequiredField ddlStype"
                                TabIndex="12">
                                <asp:ListItem>---Select---</asp:ListItem>
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblEmpProfdtl" runat="server" TabIndex="12" Text='<%# Eval("COM_LEVEL_DESC") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Employee Rating">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtEmprate" Width="100px" MaxLength="4" runat="server" CssClass="RequiredField txtBox_N"
                                TabIndex="13" Text='<%# Eval("EMPLOYEE_RATING") %>'></asp:TextBox>
                                 <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender22" runat="server" FilterType="Numbers,Custom"
                                    ValidChars=".," TargetControlID="txtEmprate" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtEmprate" Width="100px" MaxLength="4" runat="server" TabIndex="13"
                                CssClass=" RequiredField txtBox_N"></asp:TextBox>
                                 <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender22" runat="server" FilterType="Numbers,Custom"
                                    ValidChars=".," TargetControlID="txtEmprate" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblEmprate" runat="server" TabIndex="13" Text='<%# Eval("EMPLOYEE_RATING") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Right" Width="100px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" runat="server" AlternateText="Edit" CausesValidation="false"
                                TabIndex="14" ToolTip="Edit" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                            <asp:ImageButton ID="ibtnDelete" runat="server" AlternateText="Delete" CausesValidation="false"
                                ToolTip="Delete" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibtnUpdate" runat="server" AlternateText="Update" CommandName="Update"
                                TabIndex="14" ToolTip="Update" Height="20px" ImageUrl="~/Images/Update.png" />
                            <asp:ImageButton ID="ibtnCancel" runat="server" AlternateText="Cancel" CausesValidation="false"
                                ToolTip="Cancel" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="ibtnInsert" runat="server" AlternateText="Add" CommandName="FooterInsert"
                                TabIndex="14" ToolTip="Add" ImageUrl="~/Images/Add.jpg" />
                        </FooterTemplate>
                        <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
            <asp:HiddenField ID="hF_COM_LINE_ID" runat="server" />
            <asp:HiddenField ID="HF_COM_LINK_DTL_ID" runat="server" />
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                            TabIndex="15" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="16" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="17" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="18" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table>
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/HR/HRChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
