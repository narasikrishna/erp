﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.BLL.AP;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.BLL;
using FIN.BLL.HR;
using VMVServices.Web;
namespace FIN.Client.HR
{
    public partial class MobileAllowanceEntry : PageBase
    {
        HR_EMP_MOB_LIMIT_HDR hR_EMP_MOB_LIMIT_HDR = new HR_EMP_MOB_LIMIT_HDR();
        HR_EMP_MOB_LIMIT_DTL hR_EMP_MOB_LIMIT_DTL = new HR_EMP_MOB_LIMIT_DTL();
        DataTable dtData = new DataTable();
        Boolean savedBool;
        System.Collections.SortedList slControls = new System.Collections.SortedList();
        //string ProReturn = null;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";

            }

            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
            }

            if (VMVServices.Web.Utils.AddAllowed != FINAppConstants.Y && Master.Mode == FINAppConstants.Add)
            {
                btnSave.Visible = false;
            }

        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                //ddlWeightUOM.Enabled = false;
                //ddlLengthUOM.Enabled = false;
                //ddlAreaUOM.Enabled = false;
                //ddlVolumeUOM.Enabled = false;


                Startup();
                FillComboBox();


                EntityData = null;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    using (IRepository<HR_EMP_MOB_LIMIT_HDR> userCtx = new DataRepository<HR_EMP_MOB_LIMIT_HDR>())
                    {
                        hR_EMP_MOB_LIMIT_HDR = userCtx.Find(r =>
                            (r.MOB_LIMIT_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }
                    //btnReqAssignment.Visible = true;
                    EntityData = hR_EMP_MOB_LIMIT_HDR;
                    ddlEmployeeName.SelectedValue = hR_EMP_MOB_LIMIT_HDR.EMP_ID;
                    ddlEmployeeName.Enabled = false;
                    FillDept();
                    txtMobile.Text = hR_EMP_MOB_LIMIT_HDR.MOB_NUMBER;
                    txtLimit.Text = DBMethod.GetAmtDecimalCommaSeparationValue(hR_EMP_MOB_LIMIT_HDR.MOB_LIMIT.ToString());
                    dtData = DBMethod.ExecuteQuery(Employee_DAL.GetFacilityData(Master.StrRecordId)).Tables[0];
                    if (dtData.Rows.Count > 0)
                    {
                        for (int j = 0; j < dtData.Rows.Count; j++)
                        {
                            if (chkFacilities.Items.Count > 0)
                            {
                                for (int k = 0; k < chkFacilities.Items.Count; k++)
                                {
                                    if (dtData.Rows[j]["VALUE_KEY_ID"].ToString() != string.Empty && chkFacilities.Items[k].Value.ToString() != string.Empty)
                                    {
                                        if (dtData.Rows[j]["VALUE_KEY_ID"].ToString() == chkFacilities.Items[k].Value.ToString())
                                        {
                                            chkFacilities.Items[k].Selected = true;
                                        }
                                    }
                                }
                            }
                        }
                    }



                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>

        private void FillComboBox()
        {
            Employee_BLL.GetEmployeeName(ref ddlEmployeeName);
            DataTable dtDropDownData = new DataTable();
            string idVal = string.Empty;
            dtDropDownData = DBMethod.ExecuteQuery(Lookup_DAL.GetLookUpValues("FACILTY")).Tables[0];


            chkFacilities.DataSource = dtDropDownData;
            chkFacilities.DataTextField = "LOOKUP_NAME";
            chkFacilities.DataValueField = "LOOKUP_ID";
            chkFacilities.DataBind();




        }

        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    hR_EMP_MOB_LIMIT_HDR = (HR_EMP_MOB_LIMIT_HDR)EntityData;
                }

                hR_EMP_MOB_LIMIT_HDR.EMP_ID = ddlEmployeeName.SelectedValue.ToString();
                hR_EMP_MOB_LIMIT_HDR.DEPT_ID = hd_dept_id.Value;
                hR_EMP_MOB_LIMIT_HDR.MOB_NUMBER = txtMobile.Text;
                hR_EMP_MOB_LIMIT_HDR.MOB_LIMIT = CommonUtils.ConvertStringToDecimal(txtLimit.Text.ToString());
                hR_EMP_MOB_LIMIT_HDR.ENABLED_FLAG = FINAppConstants.EnabledFlag;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    hR_EMP_MOB_LIMIT_HDR.MODIFIED_BY = this.LoggedUserName;
                    hR_EMP_MOB_LIMIT_HDR.MODIFIED_DATE = DateTime.Today;

                }
                else
                {
                    hR_EMP_MOB_LIMIT_HDR.MOB_LIMIT_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_124_M.ToString(), false, true);
                    //iNV_ITEM_MASTER.WORKFLOW_COMPLETION_STATUS = "1";
                    hR_EMP_MOB_LIMIT_HDR.CREATED_BY = this.LoggedUserName;
                    hR_EMP_MOB_LIMIT_HDR.CREATED_DATE = DateTime.Today;

                }

                hR_EMP_MOB_LIMIT_HDR.ORG_ID = VMVServices.Web.Utils.OrganizationID;
                hR_EMP_MOB_LIMIT_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_EMP_MOB_LIMIT_HDR.MOB_LIMIT_ID);

                var tmpChildEntity = new List<Tuple<object, string>>();

                string mode = string.Empty;
                bool bol_found = false;
                if (chkFacilities.Items.Count > 0)
                {
                    for (int iLoop = 0; iLoop < chkFacilities.Items.Count; iLoop++)
                    {
                        bol_found = false;

                        hR_EMP_MOB_LIMIT_DTL = new HR_EMP_MOB_LIMIT_DTL();
                        mode = string.Empty;
                        if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                        {

                            HR_EMP_MOB_LIMIT_DTL obj_HR_EMP_MOB_LIMIT_DTL = new HR_EMP_MOB_LIMIT_DTL();
                            using (IRepository<HR_EMP_MOB_LIMIT_DTL> userCtx = new DataRepository<HR_EMP_MOB_LIMIT_DTL>())
                            {
                                obj_HR_EMP_MOB_LIMIT_DTL = userCtx.Find(r =>
                                    (r.VALUE_KEY_ID == chkFacilities.Items[iLoop].Value.ToString() && r.MOB_LIMIT_ID == hR_EMP_MOB_LIMIT_HDR.MOB_LIMIT_ID.ToString())
                                    ).SingleOrDefault();
                            }
                            if (obj_HR_EMP_MOB_LIMIT_DTL != null)
                            {
                                bol_found = true;
                                hR_EMP_MOB_LIMIT_DTL = obj_HR_EMP_MOB_LIMIT_DTL;
                            }
                        }

                        hR_EMP_MOB_LIMIT_DTL.VALUE_KEY_ID = chkFacilities.Items[iLoop].Value;
                        hR_EMP_MOB_LIMIT_DTL.MOB_LIMIT_ID = hR_EMP_MOB_LIMIT_HDR.MOB_LIMIT_ID;

                        hR_EMP_MOB_LIMIT_DTL.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                        hR_EMP_MOB_LIMIT_DTL.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;

                        if (chkFacilities.Items[iLoop].Selected == true)
                        {
                            if (bol_found)
                            {
                                hR_EMP_MOB_LIMIT_DTL.MODIFIED_BY = this.LoggedUserName;
                                hR_EMP_MOB_LIMIT_DTL.MODIFIED_DATE = DateTime.Today;
                                tmpChildEntity.Add(new Tuple<object, string>(hR_EMP_MOB_LIMIT_DTL, FINAppConstants.Update));

                            }
                            else
                            {
                                hR_EMP_MOB_LIMIT_DTL.MOB_LIMIT_DTL_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_124_D.ToString(), false, true);
                                hR_EMP_MOB_LIMIT_DTL.CREATED_BY = this.LoggedUserName;
                                hR_EMP_MOB_LIMIT_DTL.CREATED_DATE = DateTime.Today;
                                tmpChildEntity.Add(new Tuple<object, string>(hR_EMP_MOB_LIMIT_DTL, FINAppConstants.Add));
                            }
                        }
                        else
                        {
                            if (bol_found)
                            {
                                DBMethod.DeleteEntity<HR_EMP_MOB_LIMIT_DTL>(hR_EMP_MOB_LIMIT_DTL);
                            }
                        }
                    }

                }
                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            CommonUtils.SavePCEntity<HR_EMP_MOB_LIMIT_HDR, HR_EMP_MOB_LIMIT_DTL>(hR_EMP_MOB_LIMIT_HDR, tmpChildEntity, hR_EMP_MOB_LIMIT_DTL);
                            savedBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {

                            CommonUtils.SavePCEntity<HR_EMP_MOB_LIMIT_HDR, HR_EMP_MOB_LIMIT_DTL>(hR_EMP_MOB_LIMIT_HDR, tmpChildEntity, hR_EMP_MOB_LIMIT_DTL, true);

                            savedBool = true;
                            break;
                        }
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                AssignToBE();


                //emptyvalid();
                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                }


                // Duplicate Validation Through Backend Package PKG_VALIDATIONS



                //ProReturn = FIN.DAL.AP.Item_DAL.GetSPFOR_DUPLICATE_CHECK(iNV_ITEM_MASTER.ITEM_CODE,txtItemName.Text, iNV_ITEM_MASTER.ORG_ID, iNV_ITEM_MASTER.ITEM_ID);

                //if (ProReturn != string.Empty)
                //{
                //    if (ProReturn != "0")
                //    {
                //        ErrorCollection.Add("ITEMNAME", ProReturn);
                //        if (ErrorCollection.Count > 0)
                //        {
                //            return;
                //        }
                //    }
                //}


                //switch (Master.Mode)
                //{
                //    case FINAppConstants.Add:
                //        {
                //            DBMethod.SaveEntity<HR_EMP_MOB_LIMIT_HDR>(hR_EMP_MOB_LIMIT_HDR);
                //            DBMethod.SaveEntity<HR_EMP_MOB_LIMIT_DTL>(hR_EMP_MOB_LIMIT_DTL);
                //            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                //            break;
                //        }
                //    case FINAppConstants.Update:
                //        {

                //            DBMethod.SaveEntity<HR_EMP_MOB_LIMIT_HDR>(hR_EMP_MOB_LIMIT_HDR, true);
                //            DBMethod.SaveEntity<HR_EMP_MOB_LIMIT_DTL>(hR_EMP_MOB_LIMIT_DTL, true);
                //            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                //            break;

                //        }
                //}
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion




        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                DBMethod.DeleteEntity<HR_EMP_MOB_LIMIT_HDR>(hR_EMP_MOB_LIMIT_HDR);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }



        protected void btnCancel_Click(object sender, EventArgs e)
        {

        }



        protected void ddlEmployeeName_SelectedIndexChanged(object sender, EventArgs e)
        {

            try
            {
                using (IRepository<HR_EMP_MOB_LIMIT_HDR> userCtx = new DataRepository<HR_EMP_MOB_LIMIT_HDR>())
                {
                    hR_EMP_MOB_LIMIT_HDR = userCtx.Find(r =>
                        (r.EMP_ID == ddlEmployeeName.SelectedValue)
                        ).SingleOrDefault();
                }
                if (hR_EMP_MOB_LIMIT_HDR != null)
                {
                    ddlEmployeeName.SelectedIndex = 0;
                    ErrorCollection.Add("AlreadyDataFound", " Selected Employee Data Found, Please Come with Edit Mode ");
                    return;
                }
                FillDept();
                // WarehouseReceiptItem_BLL.fn_getReceipt4Wh(ref ddlReceiptNumber, ddlWarehousefrom.SelectedItem.Text.ToString());
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void FillDept()
        {
            try
            {
                DataTable dtdept = new DataTable();


                dtdept = DBMethod.ExecuteQuery(Employee_DAL.GetDeptDesigName(ddlEmployeeName.SelectedValue)).Tables[0];
                if (dtdept.Rows.Count > 0)
                {
                    txtDepartment.Text = dtdept.Rows[0]["DEPT_NAME"].ToString();
                    hd_dept_id.Value = dtdept.Rows[0]["DEPT_ID"].ToString();
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


    }





}
