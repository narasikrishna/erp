﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="FeedbackEntry.aspx.cs" Inherits="FIN.Client.HR.FeedbackEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="Server">
    <div class="divFormcontainer" style="width: 800px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 120px" id="lblFeedBackTempName">
                Template Name
            </div>
            <div class="divtxtBox  LNOrient" style="width: 350px">
                <asp:TextBox ID="txtFBTemplateName" MaxLength="100" runat="server" CssClass="validate[required] RequiredField txtBox"
                    TabIndex="1"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 120px" id="Div1">
                Comments
            </div>
            <div class="divtxtBox  LNOrient" style="width: 350px">
                <asp:TextBox ID="txtComments" MaxLength="3000" runat="server" CssClass="validate[required] RequiredField txtBox"
                    Height="80px" TabIndex="2" TextMode="MultiLine"></asp:TextBox>
            </div>
        </div>
           <div class="divClear_10">
        </div>
         <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 120px" id="Div3">
                Template Name Arabic
            </div>
            <div class="divtxtBox  LNOrient" style="width: 350px">
                <asp:TextBox ID="txtFBTMPLName_ol" MaxLength="100" runat="server" CssClass=" txtBox_ol"
                    TabIndex="1"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 120px" id="Div4">
                Comments Arabic
            </div>
            <div class="divtxtBox  LNOrient" style="width: 350px">
                <asp:TextBox ID="txtComments_OL" MaxLength="3000" runat="server" CssClass="txtBox_ol"
                    Height="80px" TabIndex="2" TextMode="MultiLine"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 120px" id="lblDate">
                From Date
            </div>
            <div class="divtxtBox  LNOrient" style="width: 105px">
                <asp:TextBox ID="txtFromDate" runat="server" TabIndex="2" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]]  RequiredField txtBox"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calExtender1" TargetControlID="txtFromDate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
            <div class="lblBox LNOrient" style="width: 120px" id="Div2">
                To Date
            </div>
            <div class="divtxtBox  LNOrient" style="width: 105px">
                <asp:TextBox ID="txtToDate" runat="server" TabIndex="3" CssClass="validate[required]  validate[custom[ReqDateDDMMYYY]] RequiredField  txtBox"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtToDate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="LNOrient">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="DisplayFont Grid"
                DataKeyNames="FEEDBACK_ID,FEEDBACK_DTL_ID,DELETED,dtl_code_id,code_id" OnRowCancelingEdit="gvData_RowCancelingEdit"
                OnRowCommand="gvData_RowCommand" OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound"
                OnRowDeleting="gvData_RowDeleting" OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating"
                ShowFooter="True" Width="450px">
                <Columns>
                    <asp:TemplateField HeaderText="Feedback Type" ItemStyle-Width="250px">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlFeedbacktyp" runat="server" CssClass="DisplayFont RequiredField ddlStype"
                                OnSelectedIndexChanged="ddlFeedbacktyp_SelectedIndexChanged" AutoPostBack="true"
                                Width="250px">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlFeedbacktyp" runat="server" CssClass="DisplayFont RequiredField ddlStype"
                                OnSelectedIndexChanged="ddlFeedbacktyp_SelectedIndexChanged" AutoPostBack="true"
                                Width="250px">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblFeedbacktyp" Style="word-wrap: break-word;white-space: pre-wrap;" Width="250px" runat="server" Text='<%# Eval("code_name") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Feedback Description" ItemStyle-Width="250px" ItemStyle-Height="70px">
                        <ItemTemplate>
                            <asp:Label ID="lblFeedbackdesc" Text='<%# Eval("feedback_desc") %>' CssClass="DisplayFont lblBox" Width="250px"  Style="word-wrap: break-word;white-space: pre-wrap;"
                                runat="server"></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox runat="server" ID="txtfeedbackdesc" CssClass="EntryFont RequiredField txtBox"
                                Width="250px" Text='<%#  Eval("feedback_desc") %>' />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox runat="server" ID="txtfeedbackdesc" CssClass="EntryFont RequiredField txtBox"
                                Width="250px" />
                        </FooterTemplate>
                        <ControlStyle Height="25px"></ControlStyle>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="Feedback Description Arabic" ItemStyle-Width="250px" ItemStyle-Height="70px">
                        <ItemTemplate>
                            <asp:Label ID="lblFeedbackdesc_OL" Text='<%# Eval("feedback_desc_OL") %>' CssClass="DisplayFont lblBox" Width="250px"  Style="word-wrap: break-word;white-space: pre-wrap;"
                                runat="server"></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox runat="server" ID="txtfeedbackdesc_OL" CssClass="EntryFont  txtBox_ol"
                                Width="250px" Text='<%#  Eval("feedback_desc_OL") %>' />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox runat="server" ID="txtfeedbackdesc_OL" CssClass="EntryFont  txtBox_ol"
                                Width="250px" />
                        </FooterTemplate>
                        <ControlStyle Height="25px"></ControlStyle>
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Feedback Rating Type">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlFeedbackRatingType" runat="server" CssClass="DisplayFont RequiredField ddlStype"
                                Width="100px">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlFeedbackRatingType" runat="server" CssClass="DisplayFont RequiredField ddlStype"
                                Width="100px">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblFeedbackRatingType" runat="server" Text='<%# Eval("dtl_code_name") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Effective from Date">
                        <EditItemTemplate>
                            <asp:TextBox ID="dtpStartDate" runat="server" CssClass="EntryFont RequiredField txtBox"
                                Width="100px" Text='<%#  Eval("dtlFromDate","{0:dd/MM/yyyy}") %>'></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender8" runat="server" Format="dd/MM/yyyy" TargetControlID="dtpStartDate">
                            </cc2:CalendarExtender>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="dtpStartDate" runat="server" Width="100px" CssClass="EntryFont RequiredField txtBox"></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender9" runat="server" Format="dd/MM/yyyy" TargetControlID="dtpStartDate">
                            </cc2:CalendarExtender>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblStartDate" runat="server" Text='<%# Eval("dtlFromDate","{0:dd/MM/yyyy}") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Effective To Date">
                        <EditItemTemplate>
                            <asp:TextBox ID="dtpenddate" runat="server" Width="100px" CssClass="EntryFont  txtBox"
                                Text='<%#  Eval("dtlToDate","{0:dd/MM/yyyy}") %>'></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender88" runat="server" Format="dd/MM/yyyy"
                                TargetControlID="dtpenddate">
                            </cc2:CalendarExtender>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="dtpenddate" runat="server" Width="100px" CssClass="EntryFont  txtBox"></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender99" runat="server" Format="dd/MM/yyyy"
                                TargetControlID="dtpenddate">
                            </cc2:CalendarExtender>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblEndDate" runat="server" Text='<%# Eval("dtlToDate","{0:dd/MM/yyyy}") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Active">
                        <EditItemTemplate>
                            <asp:CheckBox ID="chkact" runat="server" Enabled="true" Checked='<%# Convert.ToBoolean(Eval("enabled_flag")) %>' />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:CheckBox ID="chkact" Checked="true" Enabled="true" runat="server" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkact" runat="server" Enabled="false" Checked='<%# Convert.ToBoolean(Eval("enabled_flag")) %>' />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" AlternateText="Edit" runat="server" CausesValidation="false"
                                CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                            <asp:ImageButton ID="ibtnDelete" runat="server" AlternateText="Delete" CausesValidation="false"
                                CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibtnUpdate" Height="20px" runat="server" AlternateText="Update"
                                ImageUrl="~/Images/Update.png" CommandName="Update" />
                            <asp:ImageButton ID="ibtnCancel" ImageUrl="~/Images/Close.jpg" AlternateText="Cancel"
                                runat="server" CausesValidation="false" CommandName="Cancel" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="ibtnInsert" runat="server" AlternateText="Add" CommandName="FooterInsert"
                                ImageUrl="~/Images/Add.jpg" />
                        </FooterTemplate>
                        <%--  <ItemStyle HorizontalAlign="Center" />--%>
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                            TabIndex="12" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="13" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="14" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="15" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table>
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/HR/HRChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

    </script>
</asp:Content>