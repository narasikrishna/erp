﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.BLL.AP;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.BLL;
using VMVServices.Web;
using VMVServices.Services.Data;

namespace FIN.Client.HR
{
    public partial class LeaveReturnEntry : PageBase
    {

        HR_LEAVE_RETURN hR_LEAVE_RETURN = new HR_LEAVE_RETURN();


        DataTable dtGridData = new DataTable();
        Boolean savedBool;
        Boolean bol_rowVisiable;

        System.Collections.SortedList slControls = new System.Collections.SortedList();
        //string ProReturn = null;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));

            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";

            }

            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
            }

            if (VMVServices.Web.Utils.AddAllowed != FINAppConstants.Y && Master.Mode == FINAppConstants.Add)
            {
                btnSave.Visible = false;
            }

        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();


                Startup();
                FillComboBox();

                txtDate.Text = DBMethod.ConvertDateToString(DateTime.Now.ToShortDateString());


                EntityData = null;

                hf_editMode_leave.Value = "0";
                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    using (IRepository<HR_LEAVE_RETURN> userCtx = new DataRepository<HR_LEAVE_RETURN>())
                    {
                        hR_LEAVE_RETURN = userCtx.Find(r =>
                            (r.LEV_RET_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = hR_LEAVE_RETURN;
                    if (hR_LEAVE_RETURN.RETURN_DATE != null)
                    {
                        txtDate.Text = DBMethod.ConvertDateToString(hR_LEAVE_RETURN.RETURN_DATE.ToString());
                    }


                    ddlEmployee.SelectedValue = hR_LEAVE_RETURN.EMP_ID;
                    FillLeaveReq();
                    ddlLevEncash.SelectedValue = hR_LEAVE_RETURN.LC_ID;
                    if (hR_LEAVE_RETURN.FROM_DT != null)
                    {
                        txtFromdat.Text = DBMethod.ConvertDateToString(hR_LEAVE_RETURN.FROM_DT.ToString());
                    }
                    if (hR_LEAVE_RETURN.TO_DT != null)
                    {
                        txttodate.Text = DBMethod.ConvertDateToString(hR_LEAVE_RETURN.TO_DT.ToString());
                    }

                    txtNoofdys.Text = hR_LEAVE_RETURN.NO_OF_DAYS.ToString();
                    txtlevbaldys.Text = hR_LEAVE_RETURN.LEV_BAL_DAYS.ToString();

                    ddlEmployee.Enabled = false;
                    ddlLevEncash.Enabled = false;
                    LoadLeaveDetails();
                    hf_editMode_leave.Value = hR_LEAVE_RETURN.LEV_BAL_DAYS.ToString();



                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>

        private void FillComboBox()
        {


            FIN.BLL.HR.Employee_BLL.GetEmployeeName(ref ddlEmployee);
            //  FIN.BLL.HR.LeaveApplication_BLL.GetLeaveEncash(ref ddlLevEncash);


        }



        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    hR_LEAVE_RETURN = (HR_LEAVE_RETURN)EntityData;
                }



                if (txtDate.Text != string.Empty)
                {
                    hR_LEAVE_RETURN.RETURN_DATE = DBMethod.ConvertStringToDate(txtDate.Text.ToString());
                }
                if (txtFromdat.Text != string.Empty)
                {
                    hR_LEAVE_RETURN.FROM_DT = DBMethod.ConvertStringToDate(txtFromdat.Text.ToString());
                }
                if (txttodate.Text != string.Empty)
                {
                    hR_LEAVE_RETURN.TO_DT = DBMethod.ConvertStringToDate(txttodate.Text.ToString());
                }
                hR_LEAVE_RETURN.EMP_ID = ddlEmployee.SelectedValue;
                hR_LEAVE_RETURN.LC_ID = ddlLevEncash.SelectedValue;

                hR_LEAVE_RETURN.NO_OF_DAYS = decimal.Parse(txtNoofdys.Text);
                if (txtlevbaldys.Text.ToString().Trim().Length > 0)
                {
                    hR_LEAVE_RETURN.LEV_BAL_DAYS = decimal.Parse(txtlevbaldys.Text);
                }
                else
                {
                    txtlevbaldys.Text = "0";
                    hf_editMode_leave.Value = "0";
                    hR_LEAVE_RETURN.LEV_BAL_DAYS = null;
                }

                hR_LEAVE_RETURN.ORG_ID = VMVServices.Web.Utils.OrganizationID;

                hR_LEAVE_RETURN.ENABLED_FLAG = FINAppConstants.EnabledFlag;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    hR_LEAVE_RETURN.MODIFIED_BY = this.LoggedUserName;
                    hR_LEAVE_RETURN.MODIFIED_DATE = DateTime.Today;

                }
                else
                {
                    hR_LEAVE_RETURN.LEV_RET_ID = FINSP.GetSPFOR_SEQCode("HR_115".ToString(), false, true);

                    hR_LEAVE_RETURN.CREATED_BY = this.LoggedUserName;
                    hR_LEAVE_RETURN.CREATED_DATE = DateTime.Today;

                }

                hR_LEAVE_RETURN.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_LEAVE_RETURN.LEV_RET_ID);




            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }




        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                AssignToBE();



                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            DBMethod.SaveEntity<HR_LEAVE_RETURN>(hR_LEAVE_RETURN);
                            DBMethod.ExecuteNonQuery("update hr_leave_staff_defintions set LEAVE_AVAILED= LEAVE_AVAILED + " + (decimal.Parse(hf_editMode_leave.Value)).ToString() + " - " + (decimal.Parse(txtlevbaldys.Text)).ToString() + " where staff_id='" + ddlEmployee.SelectedValue + "' and fiscal_year='" + hf_finYear.Value + "' and leave_id='" + hf_leave_id.Value + "'");
                            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                            break;
                        }
                    case FINAppConstants.Update:
                        {

                            DBMethod.SaveEntity<HR_LEAVE_RETURN>(hR_LEAVE_RETURN, true);
                            DBMethod.ExecuteNonQuery("update hr_leave_staff_defintions set LEAVE_AVAILED= LEAVE_AVAILED + " + (decimal.Parse(hf_editMode_leave.Value)).ToString() + " - " + (decimal.Parse(txtlevbaldys.Text)).ToString() + " where staff_id='" + ddlEmployee.SelectedValue + "' and fiscal_year='" + hf_finYear.Value + "' and leave_id='" + hf_leave_id.Value + "'");
                            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                            break;

                        }
                }


            }

            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion




        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                DBMethod.DeleteEntity<HR_LEAVE_RETURN>(hR_LEAVE_RETURN);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void ddlLevEncash_SelectedIndexChanged(object sender, EventArgs e)
        {


            LoadLeaveDetails();

        }

        private void LoadLeaveDetails()
        {
            DataTable dtlevencash = new DataTable();
            dtlevencash = DBMethod.ExecuteQuery(LeaveReturn_DAL.GetLeaveEncashDtls(ddlLevEncash.SelectedValue)).Tables[0];
            if (dtlevencash.Rows.Count > 0)
            {
                txtFromdat.Text = DBMethod.ConvertDateToString(dtlevencash.Rows[0]["LEAVE_DATE_FROM"].ToString());
                txttodate.Text = DBMethod.ConvertDateToString(dtlevencash.Rows[0]["LEAVE_DATE_TO"].ToString());
                txtNoofdys.Text = dtlevencash.Rows[0]["NO_OF_DAYS"].ToString();
                hf_finYear.Value = dtlevencash.Rows[0]["FISCAL_YEAR"].ToString();
                hf_leave_id.Value = dtlevencash.Rows[0]["LEAVE_ID"].ToString();
            }
            else
            {
                txtFromdat.Text = "";
                txttodate.Text = "";
                txtNoofdys.Text = "";
                hf_finYear.Value = "";
            }
        }
        protected void ddlEmployee_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillLeaveReq();
        }
        private void FillLeaveReq()
        {
            FIN.BLL.HR.LeaveApplication_BLL.GetLeaveEncashLevReq(ref ddlLevEncash, ddlEmployee.SelectedValue, Master.Mode);
        }



    }
}