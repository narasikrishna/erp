﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.DAL.GL;
using FIN.BLL;
using FIN.BLL.GL;
using FIN.BLL.HR;
using VMVServices.Web;

namespace FIN.Client.HR
{
    public partial class EmployeeWorkProfileEntry : PageBase
    {
        HR_EMP_WORK_PROFILE_HDR hR_EMP_WORK_PROFILE_HDR = new HR_EMP_WORK_PROFILE_HDR();
        HR_EMP_WORK_PROFILE_DTL hR_EMP_WORK_PROFILE_DTL = new HR_EMP_WORK_PROFILE_DTL();
        string ProReturn = null;

        Boolean saveBool = false;

        DataTable dtGridData = new DataTable();
        Boolean bol_rowVisiable;
        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EMPPF_PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));


            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
        }

        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>
        /// 

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void FillComboBox()
        {
            Department_BLL.GetDepartmentName(ref ddlDepartment);
            Categories_BLL.fn_getCategory(ref ddlcategory, Master.Mode);
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();
                EntityData = null;

                //  txtEndDate.Text = DBMethod.ConvertDateToString(DateTime.Now.ToShortDateString());

                dtGridData = FIN.BLL.HR.EmployeeWorkProfiles_BLL.getChildEntityDet(Master.StrRecordId);
                

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    using (IRepository<HR_EMP_WORK_PROFILE_HDR> userCtx = new DataRepository<HR_EMP_WORK_PROFILE_HDR>())
                    {
                        hR_EMP_WORK_PROFILE_HDR = userCtx.Find(r =>
                            (r.EMP_WORK_PROF_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = hR_EMP_WORK_PROFILE_HDR;
                    ddlDepartment.SelectedValue = hR_EMP_WORK_PROFILE_HDR.EMP_DEPT_ID;
                    FillDesignation();
                    ddlDesignation.SelectedValue = hR_EMP_WORK_PROFILE_HDR.EMP_DESIG_ID;
                    
                    ddlcategory.SelectedValue = hR_EMP_WORK_PROFILE_HDR.EMP_CATEGORY;
                   
                    filljob();
                    ddljob.SelectedValue = hR_EMP_WORK_PROFILE_HDR.EMP_JOB_ID;
                    fillposition();
                    ddlposition.SelectedValue = hR_EMP_WORK_PROFILE_HDR.EMP_POSITION_ID;
                    fillgrade();
                    ddlgrade.SelectedValue = hR_EMP_WORK_PROFILE_HDR.EMP_GRADE_ID;
                    FillProfileName();
                    ddlProfile.SelectedValue = hR_EMP_WORK_PROFILE_HDR.EMP_PROFILE_ID;
                    if (hR_EMP_WORK_PROFILE_HDR.EFFECTIVE_FROM_DT != null)
                    {
                        txtfromdate.Text = DBMethod.ConvertDateToString(hR_EMP_WORK_PROFILE_HDR.EFFECTIVE_FROM_DT.ToString());
                    }

                    if (hR_EMP_WORK_PROFILE_HDR.EFFECTIVE_TO_DT != null)
                    {
                        txttodate.Text = DBMethod.ConvertDateToString(hR_EMP_WORK_PROFILE_HDR.EFFECTIVE_TO_DT.ToString());
                    }

                    chKEnabledFlag.Checked = hR_EMP_WORK_PROFILE_HDR.ENABLED_FLAG == FINAppConstants.EnabledFlag ? true : false;

                }

                BindGrid(dtGridData);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EMPPf_ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    hR_EMP_WORK_PROFILE_HDR = (HR_EMP_WORK_PROFILE_HDR)EntityData;
                }


                hR_EMP_WORK_PROFILE_HDR.EMP_DEPT_ID = ddlDepartment.SelectedValue;
                hR_EMP_WORK_PROFILE_HDR.EMP_DESIG_ID = ddlDesignation.SelectedValue;
                hR_EMP_WORK_PROFILE_HDR.EMP_CATEGORY = ddlcategory.SelectedValue;
                hR_EMP_WORK_PROFILE_HDR.EMP_GRADE_ID = ddlgrade.SelectedValue;
                hR_EMP_WORK_PROFILE_HDR.EMP_JOB_ID = ddljob.SelectedValue;
                hR_EMP_WORK_PROFILE_HDR.EMP_POSITION_ID = ddlposition.SelectedValue;
                hR_EMP_WORK_PROFILE_HDR.EMP_PROFILE_ID = ddlProfile.SelectedValue;
                if (txtfromdate.Text.Trim() != string.Empty)
                {
                    hR_EMP_WORK_PROFILE_HDR.EFFECTIVE_FROM_DT = DBMethod.ConvertStringToDate(txtfromdate.Text.ToString());
                }
                if (txttodate.Text.Trim() != string.Empty)
                {
                    hR_EMP_WORK_PROFILE_HDR.EFFECTIVE_TO_DT = DBMethod.ConvertStringToDate(txttodate.Text.ToString());
                }
                hR_EMP_WORK_PROFILE_HDR.EMP_ORG_ID = VMVServices.Web.Utils.OrganizationID;

                hR_EMP_WORK_PROFILE_HDR.ENABLED_FLAG = FINAppConstants.EnabledFlag;

                // hR_EMP_PROFILE_HDR.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;


                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    hR_EMP_WORK_PROFILE_HDR.MODIFIED_BY = this.LoggedUserName;
                    hR_EMP_WORK_PROFILE_HDR.MODIFIED_DATE = DateTime.Today;

                }
                else
                {
                    hR_EMP_WORK_PROFILE_HDR.EMP_WORK_PROF_ID = FINSP.GetSPFOR_SEQCode("HR_065_M".ToString(), false, true);
                    //hR_EMP_PROFILE_HDR.PK_ID = DBMethod.GetPrimaryKeyValue(FINSequenceConstant.hR_EMP_PROFILE_HDR_SEQ);
                    hR_EMP_WORK_PROFILE_HDR.CREATED_BY = this.LoggedUserName;
                    hR_EMP_WORK_PROFILE_HDR.CREATED_DATE = DateTime.Today;

                }
                hR_EMP_WORK_PROFILE_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_EMP_WORK_PROFILE_HDR.EMP_WORK_PROF_ID);

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }


                var tmpChildEntity = new List<Tuple<object, string>>();

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    hR_EMP_WORK_PROFILE_DTL = new HR_EMP_WORK_PROFILE_DTL();
                    if (dtGridData.Rows[iLoop]["EMP_WORK_PROF_DTL_ID"].ToString() != "0" && dtGridData.Rows[iLoop]["EMP_WORK_PROF_DTL_ID"].ToString() != string.Empty)
                    {
                        using (IRepository<HR_EMP_WORK_PROFILE_DTL> userCtx = new DataRepository<HR_EMP_WORK_PROFILE_DTL>())
                        {
                            hR_EMP_WORK_PROFILE_DTL = userCtx.Find(r =>
                                (r.EMP_WORK_PROF_DTL_ID == dtGridData.Rows[iLoop]["EMP_WORK_PROF_DTL_ID"].ToString())
                                ).SingleOrDefault();
                        }
                    }

                    //hR_EMP_PROFILE_DTLS.COM_LINE_NUM = (iLoop + 1);
                    hR_EMP_WORK_PROFILE_DTL.EMP_ID = dtGridData.Rows[iLoop]["EMP_ID"].ToString();
                    hR_EMP_WORK_PROFILE_DTL.EMP_PROF_DTL_ID = dtGridData.Rows[iLoop]["PROF_DTL_ID"].ToString();
                    hR_EMP_WORK_PROFILE_DTL.EMPLOYEE_RATING =decimal.Parse(dtGridData.Rows[iLoop]["EMPLOYEE_RATING"].ToString());
                    //hR_EMP_PROFILE_DTLS.COM_LINK_DTL_ID = dtGridData.Rows[iLoop]["COM_LINK_DTL_ID"].ToString();

                    hR_EMP_WORK_PROFILE_DTL.EMP_WORK_PROF_ID = hR_EMP_WORK_PROFILE_HDR.EMP_WORK_PROF_ID;

                    hR_EMP_WORK_PROFILE_DTL.WORKFLOW_COMPLETION_STATUS = "1";
                    hR_EMP_WORK_PROFILE_DTL.ENABLED_FLAG = FINAppConstants.Y;



                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {

                        tmpChildEntity.Add(new Tuple<object, string>(hR_EMP_WORK_PROFILE_DTL, "D"));
                    }
                    else
                    {

                        // Duplicate Validation Through Backend Package PKG_VALIDATIONS

                        //ProReturn = FIN.DAL.HR.Competency_DAL.GetSPFOR_DUPLICATE_CHECK(hR_EMP_PROFILE_DTLS.COM_LEVEL_DESC, hR_EMP_PROFILE_DTLS.COM_LINE_ID);

                        //if (ProReturn != string.Empty)
                        //{
                        //    if (ProReturn != "0")
                        //    {
                        //        ErrorCollection.Add("COMPETENCY", ProReturn);
                        //        if (ErrorCollection.Count > 0)
                        //        {
                        //            return;
                        //        }
                        //    }
                        //}

                        if (dtGridData.Rows[iLoop]["EMP_WORK_PROF_DTL_ID"].ToString() != "0" && dtGridData.Rows[iLoop]["EMP_WORK_PROF_DTL_ID"].ToString() != string.Empty)
                        {
                            hR_EMP_WORK_PROFILE_DTL.EMP_WORK_PROF_DTL_ID = dtGridData.Rows[iLoop]["EMP_WORK_PROF_DTL_ID"].ToString();
                            hR_EMP_WORK_PROFILE_DTL.MODIFIED_BY = this.LoggedUserName;
                            hR_EMP_WORK_PROFILE_DTL.MODIFIED_DATE = DateTime.Today;

                            tmpChildEntity.Add(new Tuple<object, string>(hR_EMP_WORK_PROFILE_DTL, "U"));

                        }
                        else
                        {

                            hR_EMP_WORK_PROFILE_DTL.EMP_WORK_PROF_DTL_ID = FINSP.GetSPFOR_SEQCode("HR_065_D".ToString(), false, true);
                            hR_EMP_WORK_PROFILE_DTL.CREATED_BY = this.LoggedUserName;
                            hR_EMP_WORK_PROFILE_DTL.CREATED_DATE = DateTime.Today;
                            //DBMethod.SaveEntity<GL_ACCT_CODE_SEGMENTS>(gL_ACCT_CODE_SEGMENTS);
                            tmpChildEntity.Add(new Tuple<object, string>(hR_EMP_WORK_PROFILE_DTL, "A"));
                        }
                    }

                }

                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            FIN.BLL.HR.EmployeeProfile_BLL.SavePCEntity<HR_EMP_WORK_PROFILE_HDR, HR_EMP_WORK_PROFILE_DTL>(hR_EMP_WORK_PROFILE_HDR, tmpChildEntity, hR_EMP_WORK_PROFILE_DTL);
                            saveBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {
                            FIN.BLL.HR.EmployeeProfile_BLL.SavePCEntity<HR_EMP_WORK_PROFILE_HDR, HR_EMP_WORK_PROFILE_DTL>(hR_EMP_WORK_PROFILE_HDR, tmpChildEntity, hR_EMP_WORK_PROFILE_DTL, true);
                            saveBool = true;
                            break;
                        }
                }



            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Empprof_ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddlEmployee = tmpgvr.FindControl("ddlEmployee") as DropDownList;
                DropDownList ddlEmpProfiledtl = tmpgvr.FindControl("ddlEmpProfiledtl") as DropDownList;
               
                if(ddlProfile.SelectedValue.ToString().Length > 0)
                    FIN.BLL.HR.EmployeeWorkProfiles_BLL.fn_GetEmployeeworkdtl(ref ddlEmpProfiledtl, ddlProfile.SelectedValue.ToString());
                //FIN.BLL.HR.EmployeeProfile_BLL.fn_GetCompetency(ref ddlcompetency);
                FIN.BLL.HR.EmployeeWorkProfiles_BLL.fn_GetEmployeeName(ref ddlEmployee, ddlDepartment.SelectedValue.ToString(), ddlDesignation.SelectedValue.ToString());

                if (gvData.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                {
                    
                    ddlEmployee.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["EMP_ID"].ToString();

                    ddlEmpProfiledtl.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["EMP_PROF_DTL_ID"].ToString();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Emppf_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session[FINSessionConstants.GridData], "Employee Profile Details ");

                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                AssignToBE();

                if (saveBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        #endregion

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";

                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Emppf_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            if (Session[FINSessionConstants.GridData] != null)
            {
                dtGridData = (DataTable)Session[FINSessionConstants.GridData];
            }
            gvData.EditIndex = -1;

            BindGrid(dtGridData);

        }
        #endregion

        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Emppf_ROW_CMD", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {


            System.Collections.SortedList slControls = new System.Collections.SortedList();


            DropDownList ddlEmployee = gvr.FindControl("ddlEmployee") as DropDownList;
            DropDownList ddlEmpProfiledtl = gvr.FindControl("ddlEmpProfiledtl") as DropDownList;
            TextBox txtEmprate = gvr.FindControl("txtEmprate") as TextBox;

            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList["EMP_WORK_PROF_DTL_ID"] = "0";

            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }

            // txtlineno.Text = (txtlineno.Text + rowindex + 1).ToString();

            slControls[0] = ddlEmployee;
            slControls[1] = ddlEmpProfiledtl;
            slControls[2] = txtEmprate;
            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));
            ErrorCollection.Clear();
            string strCtrlTypes = FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.TEXT_BOX;
            string strMessage = Prop_File_Data["Employee_Id_P"] + " ~ " + Prop_File_Data["Employee_Details_P"] + "~" + Prop_File_Data["Rating_P"];
        
            EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

            if (EmptyErrorCollection.Count > 0)
            {
                ErrorCollection = EmptyErrorCollection;
                return drList;
            }


            //string strCondition = "LOT_ID='" + ddlLotNo.SelectedValue + "'";
            //strMessage = FINMessageConstatns.RecordAlreadyExists;
            //ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            //if (ErrorCollection.Count > 0)
            //{

            //    return drList;
            //}
            //DataTable dtCompLinkDTLID = new DataTable();
            //dtCompLinkDTLID = DBMethod.ExecuteQuery(EmployeeProfile_DAL.GetCompetencyLinkDtlid(ddlcompetency.SelectedValue, ddlcompetencyLevel.SelectedValue)).Tables[0];
            //if (dtCompLinkDTLID.Rows.Count > 0)
            //{
            //    drList["COM_LINK_DTL_ID"] = dtCompLinkDTLID.Rows[0]["COM_LINK_DTL_ID"].ToString();
            //}



            //if (ddlcompetency.SelectedItem != null)
            //{
            //    drList["COM_HDR_ID"] = ddlcompetency.SelectedItem.Value;
            //    drList["COM_DESC"] = ddlcompetency.SelectedItem.Text;
            //}
            //if (ddlcompetencyLevel.SelectedItem != null)
            //{
            //    drList["COM_LINE_ID"] = ddlcompetencyLevel.SelectedItem.Value;
            //    drList["COM_LEVEL_DESC"] = ddlcompetencyLevel.SelectedItem.Text;
            //}

            //drList["EMP_PROF_RATING"] = txtEmprate.Text;
            //drList["COM_LINK_DTL_ID"] = HF_COM_LINK_DTL_ID.Value;
            drList["EMP_ID"] = ddlEmployee.SelectedValue;
            drList["EMPLOYEE_NAME"] = ddlEmployee.SelectedItem;
            drList["PROF_DTL_ID"] = ddlEmpProfiledtl.SelectedValue;
            drList["COM_LEVEL_DESC"] = ddlEmpProfiledtl.SelectedItem.Text;


            drList["EMPLOYEE_RATING"] = txtEmprate.Text;
            drList[FINColumnConstants.DELETED] = FINAppConstants.N;


            return drList;

        }



        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Emppf_ROW_UP", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Emppf_ROW_DEL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Emppf_ROW_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Emppf_RowCreated", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Emppf_RDB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                // COURSE_GROUP.COURSE_GROUP_ID = short.Parse(Master.RecordID.ToString());
                DBMethod.DeleteEntity<HR_EMP_WORK_PROFILE_HDR>(hR_EMP_WORK_PROFILE_HDR);
                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATADELETED);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Emppf_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        //protected void ddlcompetency_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;

        //    fillcomplevel(gvr);

        //}

        //private void fillcomplevel(GridViewRow gvr)
        //{
        //    DropDownList ddlcompetency = gvr.FindControl("ddlcompetency") as DropDownList;
        //    DropDownList ddlcompetencyLevel = gvr.FindControl("ddlcompetencyLevel") as DropDownList;

        //    FIN.BLL.HR.EmployeeProfile_BLL.fn_GetCompetencyLevedesc(ref ddlcompetencyLevel, ddlcompetency.SelectedValue);

        //}

        private void fillgrade()
        {
           // Grades_BLL.fn_getGrade4Category(ref ddlgrade, ddlcategory.SelectedValue);
            Grades_BLL.fn_getGradeName4Position(ref ddlgrade, ddlposition.SelectedValue);
        }

        protected void ddlcategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            filljob();
        }

        protected void ddlgrade_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillProfileName();
        }

        private void filljob()
        {
            Jobs_BLL.fn_getJob4Category(ref ddljob, ddlcategory.SelectedValue);
            // Jobs_BLL.fn_getJobName4Grade(ref ddljob, ddlgrade.SelectedValue);
        }

        protected void ddljob_SelectedIndexChanged(object sender, EventArgs e)
        {
            fillposition();
        }

        private void fillposition()
        {
            Position_BLL.fn_GetPositionNameBasedonJob(ref ddlposition, ddljob.SelectedValue);
        }

       
        protected void ddlcompetencyLevel_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            DropDownList ddlcompetency = gvr.FindControl("ddlcompetency") as DropDownList;
            DropDownList ddlcompetencyLevel = gvr.FindControl("ddlcompetencyLevel") as DropDownList;


        }
        private void FillEmployeedls()
        {
            try
            {
                ErrorCollection.Clear();

                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// .RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillDesignation();
        }
        private void FillDesignation()
        {
            ComboFilling.fn_getDesignation(ref ddlDesignation, ddlDepartment.SelectedValue.ToString());
        }

        
        private void FillProfileName()
        {
            FIN.BLL.HR.EmployeeProfile_BLL.fn_GetEmployeeProfile(ref ddlProfile, ddlposition.SelectedValue.ToString());
        }

        protected void ddlposition_SelectedIndexChanged(object sender, EventArgs e)
        {
            fillgrade();
            
        }

        protected void ddlDesignation_SelectedIndexChanged(object sender, EventArgs e)
        {

            FillEmployeeName();
        }
        private void FillEmployeeName()
        {
            //DropDownList ddlEmployee = e.Row.FindControl("ddlEmployee") as DropDownList;
            GridViewRow gvr = gvData.FooterRow;
            DropDownList ddlEmployee = gvr.FindControl("ddlEmployee") as DropDownList;

            FIN.BLL.HR.EmployeeWorkProfiles_BLL.fn_GetEmployeeName(ref ddlEmployee, ddlDepartment.SelectedValue.ToString(), ddlDesignation.SelectedValue.ToString());
        }
    }
}