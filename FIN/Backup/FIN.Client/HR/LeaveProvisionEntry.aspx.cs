﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.BLL;
using FIN.BLL.HR;
using VMVServices.Web;
using System.Collections;
namespace FIN.Client.HR
{
    public partial class LeaveProvisionEntry : PageBase
    {
        DataTable dtGridData = new DataTable();
        DataTable dtGridData1 = new DataTable();

        string ProReturn = null;
        Boolean bol_rowVisiable;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BPL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                //  pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                //pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();

        }


        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        /// <summary>
        /// To assign the values to Controls and fetch the StrRecordId,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();


                Startup();

                FillComboBox();

                EntityData = null;




            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        private void FillComboBox()
        {
            FIN.BLL.PER.PayrollPeriods_BLL.fn_GetPayrollPeriods(ref ddlPayrollPeriod);


        }


        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>


        #endregion

        private void BindGrid(DataTable dtData)
        {


            bol_rowVisiable = false;
            Session[FINSessionConstants.GridData] = dtData;
            DataTable dt_tmp = dtData.Copy();
            if (dt_tmp.Rows.Count == 0)
            {
                DataRow dr = dt_tmp.NewRow();
                dr[0] = "0";

                dt_tmp.Rows.Add(dr);
                bol_rowVisiable = true;
            }
            gvData.DataSource = dt_tmp;
            gvData.DataBind();


        }


        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();


                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("GradeEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void btnSave_Click1(object sender, EventArgs e)
        {

            try
            {

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ACT_CAL_btn_sve", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }





        protected void btnView_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dtDate = new DataTable();
                DataTable dtDept = new DataTable();
                DataTable dtEmp = new DataTable();
                DataTable dtProvi = new DataTable();
                dtDate = DBMethod.ExecuteQuery(FIN.DAL.HR.IndemnityLedger_DAL.IndemnityProvisionToDate(ddlPayrollPeriod.SelectedValue)).Tables[0];
                string leaveID = string.Empty;
                decimal leaveBal = 0;
                string ToDate = string.Empty;
                if (dtDate.Rows.Count > 0)
                {
                    if (dtDate.Rows[0]["pay_to_dt"].ToString() != string.Empty)
                    {
                        ToDate = dtDate.Rows[0]["pay_to_dt"].ToString();
                        hfDate.Value = DBMethod.ConvertDateToString(ToDate);
                    }
                }

                dtProvi = DBMethod.ExecuteQuery(FIN.DAL.HR.LeaveDefinition_DAL.GetLeaveProvision(DBMethod.ConvertDateToString(ToDate))).Tables[0];
                if (dtProvi.Rows.Count == 0)
                {
                    FIN.DAL.HR.LeaveDefinition_DAL.GetSP_LeaveProvision(ToDate);
                    dtProvi = DBMethod.ExecuteQuery(FIN.DAL.HR.LeaveDefinition_DAL.GetLeaveProvision(DBMethod.ConvertDateToString(ToDate))).Tables[0];
                }

                //leaveID = DBMethod.GetStringValue(LeaveApplication_DAL.GetLeaveID_for_Provision());

                //if (ToDate != string.Empty && leaveID != string.Empty)
                //{
                //    dtDept = DBMethod.ExecuteQuery(FIN.DAL.HR.IndemnityLedger_DAL.IndemnityProvisionDepartment()).Tables[0];
                //    if (dtDept.Rows.Count > 0)
                //    {

                //        for (int i = 0; i < dtDept.Rows.Count; i++)
                //        {
                //            dtEmp = DBMethod.ExecuteQuery(FIN.DAL.HR.IndemnityLedger_DAL.IndemnityProvisionEmployee(dtDept.Rows[i]["dept_id"].ToString())).Tables[0];
                //            if (dtEmp.Rows.Count > 0)
                //            {
                //                for (int j = 0; j < dtEmp.Rows.Count; j++)
                //                {

                //                    leaveBal = DBMethod.GetDecimalValue(LeaveDefinition_DAL.GetLeaveEligibleDays(dtEmp.Rows[j]["Emp_id"].ToString(), leaveID));
                //                    LeaveDefinition_DAL.UpdateAccuredLeave(dtEmp.Rows[j]["Emp_id"].ToString(), leaveID, CommonUtils.ConvertStringToDecimal(leaveBal.ToString()).ToString());
                //                }
                //            }

                //        }
                //    }
                //}


                BindGrid(dtProvi);
                btnLeaveProvision.Visible = true;
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AccountGroupSummaryReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void btnLeaveProvision_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ReportFile = "HR_REPORTS/RPTLeaveProvisionReport.rpt";


                Hashtable htFilterParameter = new Hashtable();
                Hashtable htHeadingParameters = new Hashtable();
                if (hfDate.Value.ToString().Length > 0)
                {
                    htFilterParameter.Add("LeaveDate", hfDate.Value);
                }

                htFilterParameter.Add("PAY_PERIOD_DESC", ddlPayrollPeriod.SelectedItem.Text);
                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;
                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;

                ReportData = FIN.BLL.HR.LeaveDefinition_BLL.get_LeaveProvisionReportData();

                htHeadingParameters.Add("ReportName", "Leave Provision");

                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerPath + "?Id=" + Master.Mode + "','','" +  VMVServices.Services.Data.DbConsts.ReportProperties + "');", true);


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("APAgingAnalysisReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        //protected void ddlDept_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    FIN.BLL.HR.Employee_BLL.GetEmplName(ref ddlEmp, ddlDept.SelectedValue);
        //}

    }
}