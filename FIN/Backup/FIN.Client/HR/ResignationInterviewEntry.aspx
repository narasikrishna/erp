﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="ResignationInterviewEntry.aspx.cs" Inherits="FIN.Client.HR.ResignationInterviewEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 800px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblRequestNumber">
                Request Number
            </div>
            <div class="divtxtBox  LNOrient" style="width: 175px">
                <asp:DropDownList ID="ddlRequestNumber" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    AutoPostBack="True" TabIndex="1" OnSelectedIndexChanged="ddlRequestNumber_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 200px" id="lblRequestDate">
                Request Date
            </div>
            <div class="divtxtBox  LNOrient" style="width: 175px">
                <asp:TextBox ID="txtRequestDate" CssClass="txtBox" runat="server" TabIndex="2"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblReason">
                Remarks
            </div>
            <div class="divtxtBox  LNOrient" style="width: 580px">
                <%-- <asp:TextBox ID="txtReason" CssClass="txtBox" TextMode="MultiLine" Height="50px" runat="server" TabIndex="3"></asp:TextBox>--%>
                <asp:TextBox ID="txtRemarks" CssClass="txtBox" runat="server" TextMode="MultiLine"
                    Height="50px" TabIndex="4"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="Div2">
                Reason
            </div>
            <div class="divtxtBox  LNOrient" style="overflow: auto; height: 110px; width: 575px;">
                <asp:CheckBoxList runat="server" ID="chkReasons" Width="575px" TabIndex="5" BorderWidth="3">
                </asp:CheckBoxList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px; height: 14px;" id="lblDepartmentName">
                Department Name
            </div>
            <div class="divtxtBox  LNOrient" style="width: 175px">
                <asp:DropDownList ID="ddldeptname" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    AutoPostBack="True" OnSelectedIndexChanged="ddldeptname_SelectedIndexChanged"
                    TabIndex="5">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 200px" id="lblDesigName">
                Designation Name
            </div>
            <div class="divtxtBox  LNOrient" style="width: 175px">
                <asp:DropDownList ID="ddldesigName" runat="server" AutoPostBack = "true" CssClass="validate[required] RequiredField ddlStype"
                    Height="22px" TabIndex="7" OnSelectedIndexChanged="ddldesigName_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblConductedBy">
                Conducted By
            </div>
            <div class="divtxtBox  LNOrient" style="width: 175px">
                <asp:DropDownList ID="ddlConductedBy" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    TabIndex="6">
                </asp:DropDownList>
            </div>
           <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 200px" id="Div1">
                Last Working Date
            </div>
            <div class="divtxtBox  LNOrient" style="width: 175px">
                <asp:TextBox TabIndex="2" runat="server" ID="txtLastWorking" CssClass="validate[required,custom[ReqDateDDMMYYY]]  RequiredField txtBox"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendsarExtender2"
                    TargetControlID="txtLastWorking" OnClientDateSelectionChanged="checkDate" />
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExstender2" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtLastWorking" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblActive">
                Active
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:CheckBox ID="chkActive" runat="server" Checked="True" TabIndex="6" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="LNOrient">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                DataKeyNames="EX_INT_DTL_ID,EX_INT_QUESTION,QUESTION_GROUP,LOOKUP_ID,DELETED"
                Width="900px" OnRowDataBound="gvData_RowDataBound" ShowFooter="false">
                <Columns>
                    <asp:TemplateField HeaderText="Sl No.">
                        <ItemTemplate>
                            <asp:TextBox ID="txtlineno" Enabled="false" TabIndex="8" MaxLength="10" runat="server"
                                CssClass="txtBox_N" Width="44px" Text='<%# Eval("EX_INT_LINE_NUM") %>'></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars="0"
                                FilterType="Numbers,Custom" TargetControlID="txtlineno" />
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:BoundField DataField="QUESTION_GROUP" HeaderText="Question Group" ItemStyle-Width="100px"
                        ItemStyle-Wrap="True">
                        <ItemStyle Wrap="True" />
                    </asp:BoundField>
                    <asp:BoundField DataField="EX_INT_QUESTION" HeaderText="Question" ItemStyle-Width="100px"
                        ItemStyle-Wrap="True">
                        <ItemStyle Wrap="True" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="Interview Answer">
                        <ItemTemplate>
                            <asp:DropDownList ID="ddlInterviewAnswer" Width="100%" runat="server" CssClass="RequiredField ddlStype">
                            </asp:DropDownList>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Comments">
                        <ItemTemplate>
                            <asp:TextBox ID="txtResponse" TabIndex="10" MaxLength="500" runat="server" CssClass="txtBox"
                                Width="520px" TextMode="MultiLine" Wrap="true" Text='<%# Eval("EX_INT_RESPONSE") %>'></asp:TextBox>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                            TabIndex="12" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="13" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="14" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="15" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table>
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/HR/HRChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
