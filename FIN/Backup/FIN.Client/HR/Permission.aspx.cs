﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.BLL;
using FIN.BLL.HR;
using VMVServices.Web;

namespace FIN.Client.HR
{
    public partial class Permission : PageBase
    {
        HR_PERMISSIONS hR_PERMISSIONS = new HR_PERMISSIONS();
        Permission_BLL Permission_BLL = new Permission_BLL();

        Boolean saveBool;
        string ProReturn = null;
        DataTable dt = new DataTable();
        string orgstart;
        string orgend;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("HRPermissions", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
        }

        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        /// <summary>
        /// AssignToControl Function is used to assign the data from table to the master
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        ///
        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                Startup();
                FillComboBox();
                EntityData = null;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    using (IRepository<HR_PERMISSIONS> userCtx = new DataRepository<HR_PERMISSIONS>())
                    {
                        hR_PERMISSIONS = userCtx.Find(r =>
                            (r.PER_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }
                    HR_EMP_WORK_DTLS hR_EMP_WORK_DTLS = new HR_EMP_WORK_DTLS();
                    using (IRepository<HR_EMP_WORK_DTLS> userCtx = new DataRepository<HR_EMP_WORK_DTLS>())
                    {
                        hR_EMP_WORK_DTLS = userCtx.Find(r =>
                            (r.EMP_ID == hR_PERMISSIONS.PER_EMP_ID.ToString())
                            ).SingleOrDefault();
                    }

                    EntityData = hR_PERMISSIONS;
                    if (hR_EMP_WORK_DTLS.EMP_DEPT_ID != null)
                    {
                        ddlDepartName.SelectedValue = hR_EMP_WORK_DTLS.EMP_DEPT_ID.ToString();
                    }
                    FillEmpName();
                    if (hR_PERMISSIONS.PER_EMP_ID != null)
                    {
                        ddlEmpName.SelectedValue = hR_PERMISSIONS.PER_EMP_ID.ToString();
                    }
                    if (hR_PERMISSIONS.PER_CATEGORY != null)
                    {
                        ddlCategory.SelectedValue = hR_PERMISSIONS.PER_CATEGORY.ToString();
                        Permission_BLL.GetReasonBasedonCategory(ref ddlreason, ddlCategory.SelectedValue);
                    }

                    ddlStatus.SelectedValue = hR_PERMISSIONS.PER_STATUS;
                    ddlreason.SelectedValue = hR_PERMISSIONS.PER_REASON;
                    txtComment.Text = hR_PERMISSIONS.PER_COMMENTS;

                    if (hR_PERMISSIONS.FROM_TIME != null)
                    {
                        ddlStartTimeHr.SelectedValue = hR_PERMISSIONS.FROM_TIME.Substring(hR_PERMISSIONS.FROM_TIME.Length - 11, 2).Replace(" ", "0");
                    }
                    if (hR_PERMISSIONS.TO_TIME != null)
                    {
                        ddlEndTimeHr.SelectedValue = hR_PERMISSIONS.TO_TIME.Substring(hR_PERMISSIONS.TO_TIME.Length - 11, 2).Replace(" ", "0");
                    }


                    if (hR_PERMISSIONS.FROM_TIME != null)
                    {
                        ddlStartTimeMin.SelectedValue = hR_PERMISSIONS.FROM_TIME.Substring(hR_PERMISSIONS.FROM_TIME.Length - 8, 2);
                    }
                    if (hR_PERMISSIONS.TO_TIME != null)
                    {
                        ddlEndTimeMin.SelectedValue = hR_PERMISSIONS.TO_TIME.Substring(hR_PERMISSIONS.TO_TIME.Length - 8, 2);
                    }


                    if (hR_PERMISSIONS.FROM_TIME != null)
                    {
                        ddlStartTimeAMPM.SelectedValue = hR_PERMISSIONS.FROM_TIME.Substring(hR_PERMISSIONS.FROM_TIME.Length - 2, 2);
                    }
                    if (hR_PERMISSIONS.TO_TIME != null)
                    {
                        ddlEndTimeAMPM.SelectedValue = hR_PERMISSIONS.TO_TIME.Substring(hR_PERMISSIONS.TO_TIME.Length - 2, 2);
                    }





                    ////txtToDate.Text = DBMethod.ConvertDateToString(hR_PERMISSIONS.PER_TO_DT.ToString());
                    //if (hR_PERMISSIONS.PER_TO_DT != null)
                    //{
                    //    txtToDate.Text = DBMethod.ConvertDateToString(hR_PERMISSIONS.PER_TO_DT.ToString());
                    //}

                    if (hR_PERMISSIONS.PERMISSION_DATE != null)
                    {
                        txtDate.Text = DBMethod.ConvertDateToString(hR_PERMISSIONS.PERMISSION_DATE.ToString());
                    }
                    if (hR_PERMISSIONS.ENABLED_FLAG == "1")
                    {
                        chkActive.Checked = true;
                    }
                    else
                    {
                        chkActive.Checked = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("HRPermissions", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }



        private void FillComboBox()
        {
            Department_BLL.GetDepartmentName(ref ddlDepartName);

            //Permission_BLL.GetgetCategory(ref ddlCategory);
            // Permission_BLL.getReason(ref ddlreason);
            Lookup_BLL.GetLookUpValues(ref ddlStatus, "STS");
            Lookup_BLL.GetLookUpValues(ref ddlCategory, "PER_CAT");
            //Categories_BLL.fn_getCategory(ref ddlCategory, Master.Mode);

        }


        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>

        private void AssignToBE(DateTime dtStartTime1, DateTime dtEndTime1)
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    hR_PERMISSIONS = (HR_PERMISSIONS)EntityData;
                }

                hR_PERMISSIONS.PER_EMP_ID = ddlEmpName.SelectedValue.ToString();
                hR_PERMISSIONS.PER_CATEGORY = ddlCategory.SelectedValue.ToString();
                hR_PERMISSIONS.PER_STATUS = ddlStatus.SelectedValue.ToString();
                hR_PERMISSIONS.PER_REASON = ddlreason.SelectedValue;
                hR_PERMISSIONS.PER_COMMENTS = txtComment.Text;
                //  hR_PERMISSIONS.PER_FROM_DT = DBMethod.ConvertStringToDate(txtFromDate.Text.ToString());
                //hR_PERMISSIONS.ENABLED_FLAG = FINAppConstants.Y;

                //var dateNow = DateTime.Now;
                //var fromdate = new DateTime(dateNow.Year, dateNow.Month, dateNow.Day, int.Parse(txtFromDate.Text.ToString().Substring(0, 2)), int.Parse(txtFromDate.Text.ToString().Substring(2, 3)), int.Parse(txtFromDate.Text.ToString().Substring(3, 4)));
                //var todate = new DateTime(dateNow.Year, dateNow.Month, dateNow.Day, int.Parse(txtToDate.Text.ToString().Substring(0, 1)), int.Parse(txtToDate.Text.ToString().Substring(2, 3)), int.Parse(txtToDate.Text.ToString().Substring(3, 4)));

                // hR_PERMISSIONS.PER_FROM_DT = fromdate;

                if (dtStartTime1.ToString() != string.Empty && dtStartTime1 != DateTime.MinValue)
                {
                    hR_PERMISSIONS.FROM_TIME = dtStartTime1.ToString();
                }
                if (dtEndTime1.ToString() != string.Empty && dtEndTime1 != DateTime.MinValue)
                {
                    hR_PERMISSIONS.TO_TIME = dtEndTime1.ToString();
                }


                //hR_PERMISSIONS.FROM_TIME = txtFromDate.Text;
                //hR_PERMISSIONS.TO_TIME = txtToDate.Text;
                hR_PERMISSIONS.PER_ORG_ID = VMVServices.Web.Utils.OrganizationID;
                //if (txtToDate.Text != null && txtToDate.Text != string.Empty && txtToDate.Text.ToString().Trim().Length > 0)
                //{

                //    hR_PERMISSIONS.PER_TO_DT = todate;// DBMethod.ConvertStringToDate(txtToDate.Text.ToString());
                //}

                if (txtDate.Text != null && txtDate.Text != string.Empty && txtDate.Text.ToString().Trim().Length > 0)
                {
                    hR_PERMISSIONS.PERMISSION_DATE = DBMethod.ConvertStringToDate(txtDate.Text.ToString());
                }

                if (chkActive.Checked == true)
                {
                    hR_PERMISSIONS.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                }
                else
                {
                    hR_PERMISSIONS.ENABLED_FLAG = FINAppConstants.DisabledFlag;
                }

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    hR_PERMISSIONS.MODIFIED_BY = this.LoggedUserName;
                    hR_PERMISSIONS.MODIFIED_DATE = DateTime.Today;
                }
                else
                {
                    hR_PERMISSIONS.PER_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_PERM_M.ToString(), false, true);
                    hR_PERMISSIONS.CREATED_BY = this.LoggedUserName;
                    hR_PERMISSIONS.CREATED_DATE = DateTime.Today;
                }

                hR_PERMISSIONS.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_PERMISSIONS.PER_ID.ToString());





                // Duplicate Validation Through Backend Package PKG_VALIDATIONS

                //ProReturn = FIN.DAL.HR.Permission_DAL.GetSPFOR_DUPLICATE_CHECK(hR_PERMISSIONS.PER_EMP_ID, txtFromDate.Text, txtToDate.Text, hR_PERMISSIONS.PER_ID);

                //if (ProReturn != string.Empty)
                //{
                //    if (ProReturn != "0")
                //    {
                //        ErrorCollection.Add("PERMISSION", ProReturn);
                //        if (ErrorCollection.Count > 0)
                //        {
                //            return;
                //        }
                //    }
                //}

                switch (Master.Mode)
                {


                    case FINAppConstants.Add:
                        {
                            DBMethod.SaveEntity<HR_PERMISSIONS>(hR_PERMISSIONS);
                            saveBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {
                            DBMethod.SaveEntity<HR_PERMISSIONS>(hR_PERMISSIONS, true);
                            saveBool = true;
                            break;
                        }
                }
                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                if (hR_PERMISSIONS.WORKFLOW_COMPLETION_STATUS == "1")
                {
                    FIN.BLL.SSM.Alert_BLL.GenerateEmail(FINMessageConstatns.PERMISSION_ALERT);
                    FIN.BLL.SSM.Alert_BLL.GenerateEmail(FINMessageConstatns.PERMISSION_EMP_ALERT);
                    if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0" && saveBool == true)
                    {
                        if (VMVServices.Web.Utils.IsAlert == "1")
                        {
                            FINSQL.UpdateAlertUserLevel();
                        }
                    }
                }
                if (ErrorCollection.Count > 0)
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("HRPermissions", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                System.Collections.SortedList slControls = new System.Collections.SortedList();


                slControls[0] = txtDate;
                slControls[1] = ddlEmpName;

                slControls[2] = ddlStartTimeHr;
                slControls[3] = ddlEndTimeHr;

                slControls[4] = ddlStartTimeMin;
                slControls[5] = ddlEndTimeMin;

                slControls[6] = ddlStartTimeAMPM;
                slControls[7] = ddlEndTimeAMPM;

                ErrorCollection.Clear();
                Dictionary<string, string> Prop_File_Data;
                Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));

                string strCtrlTypes = FINAppConstants.TEXT_BOX + '~' + FINAppConstants.DROP_DOWN_LIST + '~' + FINAppConstants.DROP_DOWN_LIST + '~' + FINAppConstants.DROP_DOWN_LIST + '~' + FINAppConstants.DROP_DOWN_LIST + '~' + FINAppConstants.DROP_DOWN_LIST + '~' + FINAppConstants.DROP_DOWN_LIST + '~' + FINAppConstants.DROP_DOWN_LIST;
                string strMessage = Prop_File_Data["Permission_Date_P"] + " ~ " + Prop_File_Data["Employee_Name_P"] + " ~ " + Prop_File_Data["From_Time_P"] + " ~ " + Prop_File_Data["To_Time_P"] + " ~ " + Prop_File_Data["From_Time_P"] + " ~ " + Prop_File_Data["To_Time_P"] + " ~ " + Prop_File_Data["From_Time_P"] + " ~ " + Prop_File_Data["To_Time_P"] + "";

                ErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

                if (ErrorCollection.Count > 0)
                {
                    return;
                }



                int startHr1 = 0;
                int EndHr1 = Convert.ToInt16(ddlEndTimeHr.SelectedValue.ToString());
                if (ddlStartTimeHr.SelectedValue != string.Empty)
                {
                    startHr1 = Convert.ToInt16(ddlStartTimeHr.SelectedValue.ToString());
                    if (ddlStartTimeAMPM.SelectedValue == "PM" && startHr1 != 12)
                        startHr1 = startHr1 + 12;
                }

                DateTime dtStartTime1 = DateTime.MinValue;
                DateTime dtEndTime1 = DateTime.MinValue;
                if (ddlEndTimeHr.SelectedValue != string.Empty && ddlStartTimeHr.SelectedValue != string.Empty)
                {


                    if (ddlEndTimeAMPM.SelectedValue == "PM" && EndHr1 != 12)
                        EndHr1 = EndHr1 + 12;

                    if (ddlStartTimeMin.SelectedValue != string.Empty && ddlEndTimeMin.SelectedValue != string.Empty)
                    {
                        dtStartTime1 = new DateTime(FINAppConstants.TMP_YEAR, FINAppConstants.TMP_MONTH, FINAppConstants.TMP_DAY, startHr1, Convert.ToInt16(ddlStartTimeMin.SelectedValue), 0);
                        dtEndTime1 = new DateTime(FINAppConstants.TMP_YEAR, FINAppConstants.TMP_MONTH, FINAppConstants.TMP_DAY, EndHr1, Convert.ToInt16(ddlEndTimeMin.SelectedValue), 0);
                        if ((dtEndTime1 - dtStartTime1).TotalMinutes <= 0)
                        {
                            ErrorCollection.Remove("starttime1");
                            ErrorCollection.Add("starttime1", "From Time must be less than the End Time");
                            return;
                        }

                        if (ddlStartTimeAMPM.SelectedValue != string.Empty && ddlEndTimeAMPM.SelectedValue != string.Empty)
                        {
                            if (ddlStartTimeMin.SelectedValue == ddlEndTimeMin.SelectedValue && ddlStartTimeHr.SelectedValue == ddlEndTimeHr.SelectedValue && ddlStartTimeAMPM.SelectedValue == ddlEndTimeAMPM.SelectedValue)
                            {
                                ErrorCollection.Remove("starttime3");
                                ErrorCollection.Add("starttime3", "From Time should not be equal to End Time");
                                return;
                            }
                        }
                    }
                }



                dt = Permission_BLL.GetTimeScheduleforOrg();

                if (dt.Rows.Count > 0)
                {
                    orgstart = dt.Rows[0].ItemArray[0].ToString();
                    orgend = dt.Rows[0].ItemArray[1].ToString();
                }
                int orgstartHr = 0;
                int orgstartMin = 0;
                int orgendHr = 0;
                int orgendMin = 0;
                string orgendampm = string.Empty;

                orgstartHr = int.Parse(orgstart.Substring(orgstart.Length -11, 2).Replace(" ", ""));
                orgstartMin = int.Parse(orgstart.Substring(orgstart.Length - 8, 2).Replace(" ", ""));

                orgendHr = int.Parse(orgend.Substring(orgend.Length - 11, 2).Replace(" ", ""));

                orgendMin = int.Parse(orgend.Substring(orgend.Length - 8, 2).Replace(" ", ""));

                if (orgend.Length >= 10)
                {
                    orgendampm = orgend.Substring(orgend.Length - 2, 2);
                }
                if (startHr1 < orgstartHr)
                {
                    ErrorCollection.Add("starttime3", "From Time Must be Greater than Organization Time Schedule");
                    return;
                }
                else
                {
                    if (startHr1 == orgstartHr)
                    {
                        if (int.Parse(ddlStartTimeMin.SelectedValue.ToString()) <= orgstartMin)
                        {
                            ErrorCollection.Add("Start Min", "From Time Must be Greater than Organization Time Schedule");
                            return;
                        }
                    }
                }


                if (orgendampm.Trim().ToUpper() == "PM")
                {
                    orgendHr = orgendHr + 12;
                }

                if (EndHr1 > orgendHr)
                {
                    ErrorCollection.Add("endtime3", "End Time Must be smaller than Organization time Schedule");
                }
                else
                {
                    if (EndHr1 == orgendHr)
                    {
                        if (int.Parse(ddlEndTimeMin.SelectedValue.ToString()) >= orgendMin)
                        {
                            ErrorCollection.Add("EndMin", "End Time Must be smaller than Organization Time Schedule");
                            return;
                        }
                    }
                }



                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                AssignToBE(dtStartTime1, dtEndTime1);

                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                if (saveBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("HRPermissionsSave", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        #endregion

        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                DBMethod.DeleteEntity<HR_PERMISSIONS>(hR_PERMISSIONS);
                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATADELETED);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("HRPermissionsDelete", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void ddlDepartName_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillEmpName();

        }
        private void FillEmpName()
        {
            Permission_BLL.fn_GetEmployeeDetails(ref ddlEmpName, ddlDepartName.SelectedValue);

        }

        protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            Permission_BLL.GetReasonBasedonCategory(ref ddlreason, ddlCategory.SelectedValue);
        }

    }
}