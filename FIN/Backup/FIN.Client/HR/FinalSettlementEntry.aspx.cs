﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.BLL.AP;
using FIN.DAL;
using FIN.DAL.PER;
using FIN.DAL.HR;
using FIN.BLL;
using VMVServices.Web;
using VMVServices.Services.Data;


namespace FIN.Client.HR
{
    public partial class FinalSettlementEntry : PageBase
    {
        Boolean bol_rowVisiable;

        HR_LEAVE_ENCASHMENT hR_LEAVE_ENCASHMENT = new HR_LEAVE_ENCASHMENT();
        HR_EMP_LEAVE_SAL_ELEMENT hr_emp_leave_sal_element = new HR_EMP_LEAVE_SAL_ELEMENT();
        HR_SETTLEMENTS HR_SETTLEMENTS = new HR_SETTLEMENTS();
        HR_FINAL_SETTLEMENTS HR_FINAL_SETTLEMENTS = new HR_FINAL_SETTLEMENTS();
        HR_FINAL_SETTLEMENT_HDR hR_FINAL_SETTLEMENT_HDR = new HR_FINAL_SETTLEMENT_HDR();
        HR_FINAL_SETTLEMENT_DTL hR_FINAL_SETTLEMENT_DTL = new HR_FINAL_SETTLEMENT_DTL();
        DataTable dtGridData = new DataTable();

        Boolean savedBool;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                if (!IsPostBack)
                {
                    Startup();
                    AssignToControl();

                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();

                    FillComboBox();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("hfads_Payroll_Id", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));

            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();

            //if (Master.Mode == FINAppConstants.Delete)
            //{
            //    btnSave.Visible = false;
            //    btnDelete.Visible = true;
            //    pnlConfirm.Attributes["display"] = "none";
            //}
            //else
            //{
            //    btnSave.Visible = true;
            //    btnDelete.Visible = false;
            //    pnlConfirm.Visible = false;
            //}
            //if (Master.Mode == FINAppConstants.Update)
            //{
            //    btnSave.Text = "Update";

            //}

            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
            }

            if (VMVServices.Web.Utils.AddAllowed != FINAppConstants.Y && Master.Mode == FINAppConstants.Add)
            {
                //  btnSave.Visible = false;
            }

        }


        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();

                EntityData = null;
                Session[FINSessionConstants.GridData] = null;




                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    using (IRepository<HR_FINAL_SETTLEMENT_HDR> userCtx = new DataRepository<HR_FINAL_SETTLEMENT_HDR>())
                    {
                        hR_FINAL_SETTLEMENT_HDR = userCtx.Find(r =>
                            (r.FNL_STL_HDR_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    ddlEmpName.Text = hR_FINAL_SETTLEMENT_HDR.EMP_ID;
                    CalculateSettlementAmount();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }



        private void AssignToBE()
        {
            try
            {

                CalAmtToPay();
                ErrorCollection.Clear();
                string mode = "ADD";
                using (IRepository<HR_FINAL_SETTLEMENT_HDR> userCtx = new DataRepository<HR_FINAL_SETTLEMENT_HDR>())
                {
                    hR_FINAL_SETTLEMENT_HDR = userCtx.Find(r =>
                        (r.EMP_ID == ddlEmpName.SelectedValue)
                        ).SingleOrDefault();

                }
                if (hR_FINAL_SETTLEMENT_HDR == null)
                {
                    hR_FINAL_SETTLEMENT_HDR = new HR_FINAL_SETTLEMENT_HDR();
                }
                else
                {
                    mode = "update";
                }
                //PO_HEADERS.PO_HEADER_ID = txtPONumber.Text;


                //  oM_PICK_RELEASE_HDR.QUOTE_DATE = DBMethod.ConvertStringToDate(txtQuoteDate.Text.ToString());
                hR_FINAL_SETTLEMENT_HDR.EMP_ID = ddlEmpName.SelectedValue;
                if (txtDate.Text != string.Empty)
                {
                    hR_FINAL_SETTLEMENT_HDR.LAST_WORKING_DATE = DBMethod.ConvertStringToDate(txtDate.Text.ToString());
                }

                hR_FINAL_SETTLEMENT_HDR.INDEMN_AMOUNT = decimal.Parse(txtPayableAmt.Text.ToString());
                hR_FINAL_SETTLEMENT_HDR.LOAN_AMOUNT = decimal.Parse(txtLoanBalAmt.Text.ToString());

                hR_FINAL_SETTLEMENT_HDR.NO_OF_DAYS = decimal.Parse(txtNoOfDays.Text.ToString());
                hR_FINAL_SETTLEMENT_HDR.LEAVE_SETMT_AMT = decimal.Parse(txtLeaveAmount.Text.ToString());
                hR_FINAL_SETTLEMENT_HDR.BANK = ddlBankName.SelectedValue;
                hR_FINAL_SETTLEMENT_HDR.BRANCH = ddlBankBranch.SelectedValue;
                hR_FINAL_SETTLEMENT_HDR.ACCT_NO = ddlAccountnumber.SelectedValue;
                hR_FINAL_SETTLEMENT_HDR.CHEQUE_NO = ddlChequeNumber.SelectedValue;
                hR_FINAL_SETTLEMENT_HDR.ATTRIBUTE1 = lblAmtTOPAy.Text;


                hR_FINAL_SETTLEMENT_HDR.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                //PO_HEADERS.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;


                if (mode == "update")
                {
                    hR_FINAL_SETTLEMENT_HDR.MODIFIED_BY = this.LoggedUserName;
                    hR_FINAL_SETTLEMENT_HDR.MODIFIED_DATE = DateTime.Today;
                }
                else
                {
                    hR_FINAL_SETTLEMENT_HDR.FNL_STL_HDR_ID = FINSP.GetSPFOR_SEQCode("HR_127_M".ToString(), false, true);

                    hR_FINAL_SETTLEMENT_HDR.CREATED_BY = this.LoggedUserName;
                    hR_FINAL_SETTLEMENT_HDR.CREATED_DATE = DateTime.Today;
                }
                hR_FINAL_SETTLEMENT_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_FINAL_SETTLEMENT_HDR.FNL_STL_HDR_ID);


                //Save Detail Table


                var tmpChildEntity = new List<Tuple<object, string>>();

                HR_FINAL_SETTLEMENT_DTL hR_FINAL_SETTLEMENT_DTL = new HR_FINAL_SETTLEMENT_DTL();

                for (int iLoop = 0; iLoop < gvEmployeePay.Rows.Count; iLoop++)
                {
                    if (gvEmployeePay.Rows[iLoop].Visible)
                    {
                        hR_FINAL_SETTLEMENT_DTL = new HR_FINAL_SETTLEMENT_DTL();

                        if (gvEmployeePay.DataKeys[iLoop].Values["FNL_STL_DTL_ID"].ToString() != "0" && gvEmployeePay.DataKeys[iLoop].Values["FNL_STL_DTL_ID"].ToString() != string.Empty)
                        {
                            using (IRepository<HR_FINAL_SETTLEMENT_DTL> userCtx = new DataRepository<HR_FINAL_SETTLEMENT_DTL>())
                            {
                                hR_FINAL_SETTLEMENT_DTL = userCtx.Find(r =>
                                    (r.FNL_STL_DTL_ID == gvEmployeePay.DataKeys[iLoop].Values["FNL_STL_DTL_ID"].ToString())
                                    ).SingleOrDefault();
                            }
                        }

                        TextBox txtAmount = (TextBox)gvEmployeePay.Rows[iLoop].FindControl("txtAmount");

                        hR_FINAL_SETTLEMENT_DTL.ELEMENT_ID = gvEmployeePay.DataKeys[iLoop].Values["PAY_EMP_ELEMENT_ID"].ToString();

                        // hR_FINAL_SETTLEMENT_DTL.AMT_VALUE = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop]["PAY_AMOUNT"].ToString());
                        hR_FINAL_SETTLEMENT_DTL.AMT_VALUE = decimal.Parse(txtAmount.Text.ToString());

                        hR_FINAL_SETTLEMENT_DTL.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;
                        hR_FINAL_SETTLEMENT_DTL.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                        hR_FINAL_SETTLEMENT_DTL.FNL_STL_HDR_ID = hR_FINAL_SETTLEMENT_HDR.FNL_STL_HDR_ID;


                        if (gvEmployeePay.DataKeys[iLoop].Values["DELETED"].ToString() == FINAppConstants.Y)
                        {
                            tmpChildEntity.Add(new Tuple<object, string>(hR_FINAL_SETTLEMENT_DTL, FINAppConstants.Delete));
                        }
                        else
                        {
                            if (gvEmployeePay.DataKeys[iLoop].Values["FNL_STL_DTL_ID"].ToString() != "0" && gvEmployeePay.DataKeys[iLoop].Values["FNL_STL_DTL_ID"].ToString() != string.Empty)
                            {
                                hR_FINAL_SETTLEMENT_DTL.MODIFIED_DATE = DateTime.Today;
                                tmpChildEntity.Add(new Tuple<object, string>(hR_FINAL_SETTLEMENT_DTL, FINAppConstants.Update));
                            }
                            else
                            {
                                hR_FINAL_SETTLEMENT_DTL.FNL_STL_DTL_ID = FINSP.GetSPFOR_SEQCode("HR_127_D".ToString(), false, true);
                                hR_FINAL_SETTLEMENT_DTL.CREATED_BY = this.LoggedUserName;
                                hR_FINAL_SETTLEMENT_DTL.CREATED_DATE = DateTime.Today;
                                tmpChildEntity.Add(new Tuple<object, string>(hR_FINAL_SETTLEMENT_DTL, FINAppConstants.Add));
                            }
                        }
                    }
                }
                for (int iLoop = 0; iLoop < gvEmployeePay2.Rows.Count; iLoop++)
                {
                    if (gvEmployeePay2.Rows[iLoop].Visible)
                    {
                        hR_FINAL_SETTLEMENT_DTL = new HR_FINAL_SETTLEMENT_DTL();

                        if (gvEmployeePay2.DataKeys[iLoop].Values["FNL_STL_DTL_ID"].ToString() != "0" && gvEmployeePay.DataKeys[iLoop].Values["FNL_STL_DTL_ID"].ToString() != string.Empty)
                        {
                            using (IRepository<HR_FINAL_SETTLEMENT_DTL> userCtx = new DataRepository<HR_FINAL_SETTLEMENT_DTL>())
                            {
                                hR_FINAL_SETTLEMENT_DTL = userCtx.Find(r =>
                                    (r.FNL_STL_DTL_ID == gvEmployeePay2.DataKeys[iLoop].Values["FNL_STL_DTL_ID"].ToString())
                                    ).SingleOrDefault();
                            }
                        }

                        TextBox txtAmount2 = (TextBox)gvEmployeePay2.Rows[iLoop].FindControl("txtAmount2");

                        hR_FINAL_SETTLEMENT_DTL.ELEMENT_ID = gvEmployeePay2.DataKeys[iLoop].Values["PAY_EMP_ELEMENT_ID"].ToString();

                        // hR_FINAL_SETTLEMENT_DTL.AMT_VALUE = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop]["PAY_AMOUNT"].ToString());
                        hR_FINAL_SETTLEMENT_DTL.AMT_VALUE = decimal.Parse(txtAmount2.Text.ToString());

                        hR_FINAL_SETTLEMENT_DTL.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;
                        hR_FINAL_SETTLEMENT_DTL.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                        hR_FINAL_SETTLEMENT_DTL.FNL_STL_HDR_ID = hR_FINAL_SETTLEMENT_HDR.FNL_STL_HDR_ID;


                        if (gvEmployeePay2.DataKeys[iLoop].Values["DELETED"].ToString() == FINAppConstants.Y)
                        {
                            tmpChildEntity.Add(new Tuple<object, string>(hR_FINAL_SETTLEMENT_DTL, FINAppConstants.Delete));
                        }
                        else
                        {
                            if (gvEmployeePay2.DataKeys[iLoop].Values["FNL_STL_DTL_ID"].ToString() != "0" && gvEmployeePay2.DataKeys[iLoop].Values["FNL_STL_DTL_ID"].ToString() != string.Empty)
                            {
                                hR_FINAL_SETTLEMENT_DTL.MODIFIED_DATE = DateTime.Today;
                                tmpChildEntity.Add(new Tuple<object, string>(hR_FINAL_SETTLEMENT_DTL, FINAppConstants.Update));
                            }
                            else
                            {
                                hR_FINAL_SETTLEMENT_DTL.FNL_STL_DTL_ID = FINSP.GetSPFOR_SEQCode("HR_127_D".ToString(), false, true);
                                hR_FINAL_SETTLEMENT_DTL.CREATED_BY = this.LoggedUserName;
                                hR_FINAL_SETTLEMENT_DTL.CREATED_DATE = DateTime.Today;
                                tmpChildEntity.Add(new Tuple<object, string>(hR_FINAL_SETTLEMENT_DTL, FINAppConstants.Add));
                            }
                        }
                    }
                }


                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            CommonUtils.SavePCEntity<HR_FINAL_SETTLEMENT_HDR, HR_FINAL_SETTLEMENT_DTL>(hR_FINAL_SETTLEMENT_HDR, tmpChildEntity, hR_FINAL_SETTLEMENT_DTL);
                            savedBool = true;


                            break;
                        }
                    case FINAppConstants.Update:
                        {


                            CommonUtils.SavePCEntity<HR_FINAL_SETTLEMENT_HDR, HR_FINAL_SETTLEMENT_DTL>(hR_FINAL_SETTLEMENT_HDR, tmpChildEntity, hR_FINAL_SETTLEMENT_DTL, true);
                            savedBool = true;

                            break;
                        }
                }

                FIN.DAL.HR.FinalSettlement_DAL.UpdateLeaveLedger_FinalSettlment(ddlEmpName.SelectedValue);
                FIN.DAL.HR.FinalSettlement_DAL.UpdateIndemnityAmount_FinalSettlment(ddlEmpName.SelectedValue);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();


                AssignToBE();

                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        private void FillComboBox()
        {
            txtDate.Text = DBMethod.ConvertDateToString(DateTime.Now.ToShortDateString());
            FIN.BLL.HR.Employee_BLL.GetResignedEmpDetails(ref ddlEmpName);
            FIN.BLL.CA.Bank_BLL.fn_getBankName(ref ddlBankName);
        }



        protected void imgBtnPost_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                System.Collections.SortedList slControls = new System.Collections.SortedList();
                slControls[0] = ddlEmpName;
                slControls[1] = ddlBankName;
                slControls[2] = ddlBankBranch;
                slControls[3] = ddlAccountnumber;
                slControls[4] = ddlChequeNumber;

                Dictionary<string, string> Prop_File_Data;
                Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));

                ErrorCollection.Clear();
                string strCtrlTypes = FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST;
                string strMessage = "Employee Name" + "~" + "Bank Name" + " ~ " + "BranchName" + " ~ " + "Account Number" + " ~ " + "Cheque Number" + "";
                //string strMessage = " Short Name ~ Description ~ Issuing Authority ~ Duration ";
                ErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);
                if (ErrorCollection.Count > 0)
                    return;

                AssignToBE();


                //using (IRepository<HR_FINAL_SETTLEMENT_HDR> userCtx = new DataRepository<HR_FINAL_SETTLEMENT_HDR>())
                //{
                //    hR_FINAL_SETTLEMENT_HDR = userCtx.Find(r =>
                //        (r.EMP_ID == ddlEmpName.SelectedValue)
                //        ).SingleOrDefault();

                //}
                //if (hR_FINAL_SETTLEMENT_HDR != null)
                //{
                //    hR_FINAL_SETTLEMENT_HDR.ATTRIBUTE2 = "POSTED";
                //    DBMethod.SaveEntity<HR_FINAL_SETTLEMENT_HDR>(hR_FINAL_SETTLEMENT_HDR, true);

                //}


                //CA_CHECK_DTL cA_CHECK_DTL = new CA_CHECK_DTL();
                //using (IRepository<CA_CHECK_DTL> userCtx = new DataRepository<CA_CHECK_DTL>())
                //{
                //    cA_CHECK_DTL = userCtx.Find(r =>
                //        (r.CHECK_DTL_ID == ddlChequeNumber.SelectedValue.ToString())
                //        ).SingleOrDefault();
                //}
                //if (cA_CHECK_DTL != null)
                //{

                //    cA_CHECK_DTL.CHECK_PAY_TO = ddlEmpName.Text;

                //    cA_CHECK_DTL.CHECK_DT = DBMethod.ConvertStringToDate(txtDate.Text);
                //    cA_CHECK_DTL.CHECK_AMT = decimal.Parse(lblAmtTOPAy.Text);
                //    cA_CHECK_DTL.CHECK_STATUS = "USED";
                //    cA_CHECK_DTL.CHECK_CURRENCY_CODE = Session[FINSessionConstants.ORGCurrency].ToString();
                //    cA_CHECK_DTL.MODIFIED_BY = this.LoggedUserName;
                //    cA_CHECK_DTL.MODIFIED_DATE = DateTime.Today;
                //    DBMethod.SaveEntity<CA_CHECK_DTL>(cA_CHECK_DTL, true);
                //}


                //PayrollPosting();
                //if (ErrorCollection.Count > 0)
                //{
                //    return;
                //}
                //IndemnityPosting();
                //if (ErrorCollection.Count > 0)
                //{
                //    return;
                //}
                //LoanPosting();
                //if (ErrorCollection.Count > 0)
                //{
                //    return;
                //}
                //AnnulLeavePosting();


                //if (ErrorCollection.Count > 0)
                //{
                //    return;
                //}
                //ErrorCollection.Add("finalsetpayrollPost", "Final settlement data posted successfully");
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("hf_Pasdayroll_Id", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void PayrollPosting()
        {
            string str_Departments = hf_dept_id.Value.ToString();

            // FIN.DAL.HR.PayrollTrailrunDetailsEntry.RunSPFOR_GL_POSTING_PR(hf_Payroll_Id.Value);

            if (ErrorCollection.Count > 0)
            {
                return;
            }
            // AssignToFinalSettlement("SALARY", CommonUtils.ConvertStringToDecimal(lblNet.Text));
            // loadEmpSalaryDetails();
            if (ErrorCollection.Count > 0)
            {
                return;
            }
            //ErrorCollection.Remove("payrollPost");
            //ErrorCollection.Add("payrollPost", "Payroll data posted sucessfully");

        }
        private void IndemnityPosting()
        {
            if (txtDate.Text.Length > 0)
            {
                FIN.DAL.HR.IndemnityLedger_DAL.GetSP_IndemnityPaymentPosting(txtDate.Text);
            }
            if (ErrorCollection.Count > 0)
            {
                return;
            }
            // AssignToFinalSettlement("INDEM", CommonUtils.ConvertStringToDecimal(txtPayableAmt.Text));
            if (ErrorCollection.Count > 0)
            {
                return;
            }
            //ErrorCollection.Remove("IndemPost");
            //ErrorCollection.Add("IndemPost", "Indemnity data posted sucessfully");
        }
        private void LoanPosting()
        {
            ErrorCollection.Clear();
            if (hd_loan_req_id.Value != string.Empty)
            {
                FINSP.GetSP_GL_Posting(hd_loan_req_id.Value, "HR_098");
                //  AssignToFinalSettlement("LOAN", CommonUtils.ConvertStringToDecimal(txtLoanBalAmt.Text));
            }
            if (ErrorCollection.Count > 0)
            {
                return;
            }
            //ErrorCollection.Remove("LoanPost");
            //ErrorCollection.Add("LoanPost", "Loan data posted sucessfully");
        }
        protected void imgBtnIndemnity_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                //ErrorCollection.Clear();

                //if (txtDate.Text.Length > 0)
                //{
                //    FIN.DAL.HR.IndemnityLedger_DAL.GetSP_IndemnityPaymentPosting(txtDate.Text);
                //}
                //if (ErrorCollection.Count > 0)
                //{
                //    return;
                //}
                //ErrorCollection.Remove("IndemPost");
                //ErrorCollection.Add("IndemPost", "Indemnity data posted sucessfully");
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("hf_Payroll_Id", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void AnnulLeavePosting()
        {
            try
            {
                ErrorCollection.Clear();

                hR_LEAVE_ENCASHMENT.LC_DATE = DateTime.Today;

                hR_LEAVE_ENCASHMENT.LC_DEPT_ID = hf_dept_id.Value;
                hR_LEAVE_ENCASHMENT.LC_EMP_ID = ddlEmpName.SelectedValue;
                hR_LEAVE_ENCASHMENT.LEAVE_ID = hd_AnnualLeave_Id.Value;
                hR_LEAVE_ENCASHMENT.TRANS_TYPE = "Settlement";
                hR_LEAVE_ENCASHMENT.TRANS_ID = "";

                hR_LEAVE_ENCASHMENT.ATTRIBUTE1 = "0";
                hR_LEAVE_ENCASHMENT.LC_FROM_DT = DateTime.Today;
                hR_LEAVE_ENCASHMENT.LC_TO_DT = DateTime.Today;

                hR_LEAVE_ENCASHMENT.LC_NO_OF_DAYS = CommonUtils.ConvertStringToDecimal(hf_No_of_days.Value);
                hR_LEAVE_ENCASHMENT.LC_LEAVE_SALARY = CommonUtils.ConvertStringToDecimal(txtLeaveAmount.Text.ToString());
                hR_LEAVE_ENCASHMENT.LC_PAYMENT_OPTION = "PAY_NOW";
                hR_LEAVE_ENCASHMENT.LC_PAY_PERIOD_ID = null;

                hR_LEAVE_ENCASHMENT.BANK_ID = ddlBankName.SelectedValue;
                hR_LEAVE_ENCASHMENT.BRANCH_ID = ddlBankBranch.SelectedValue;
                hR_LEAVE_ENCASHMENT.ACCOUNT_ID = ddlAccountnumber.SelectedValue;
                hR_LEAVE_ENCASHMENT.CHEQUE_DTL_ID = ddlChequeNumber.SelectedValue;

                hR_LEAVE_ENCASHMENT.LC_REMARKS = "";
                hR_LEAVE_ENCASHMENT.LC_AIR_TICKET = FINAppConstants.EnabledFlag;
                hR_LEAVE_ENCASHMENT.LC_AIR_TICKET_AMT = 0;
                hR_LEAVE_ENCASHMENT.LC_ORG_ID = VMVServices.Web.Utils.OrganizationID;

                hR_LEAVE_ENCASHMENT.ENABLED_FLAG = FINAppConstants.EnabledFlag;

                hR_LEAVE_ENCASHMENT.LC_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_097.ToString(), false, true);
                hR_LEAVE_ENCASHMENT.CREATED_BY = this.LoggedUserName;
                hR_LEAVE_ENCASHMENT.CREATED_DATE = DateTime.Today;

                hR_LEAVE_ENCASHMENT.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_LEAVE_ENCASHMENT.LC_ID);

                DBMethod.SaveEntity<HR_LEAVE_ENCASHMENT>(hR_LEAVE_ENCASHMENT);

                CA_CHECK_DTL cA_CHECK_DTL = new CA_CHECK_DTL();
                using (IRepository<CA_CHECK_DTL> userCtx = new DataRepository<CA_CHECK_DTL>())
                {
                    cA_CHECK_DTL = userCtx.Find(r =>
                        (r.CHECK_DTL_ID == ddlChequeNumber.SelectedValue.ToString())
                        ).SingleOrDefault();
                }
                if (cA_CHECK_DTL != null)
                {
                    cA_CHECK_DTL.CHECK_PAY_TO = ddlEmpName.SelectedItem.Text;
                    cA_CHECK_DTL.CHECK_DT = DateTime.Today;
                    cA_CHECK_DTL.CHECK_AMT = CommonUtils.ConvertStringToDecimal(txtLeaveAmount.Text);
                    cA_CHECK_DTL.CHECK_STATUS = "USED";
                    cA_CHECK_DTL.CHECK_CURRENCY_CODE = Session[FINSessionConstants.ORGCurrency].ToString();
                    cA_CHECK_DTL.MODIFIED_BY = this.LoggedUserName;
                    cA_CHECK_DTL.MODIFIED_DATE = DateTime.Today;
                    DBMethod.SaveEntity<CA_CHECK_DTL>(cA_CHECK_DTL, true);
                }

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                LeaveApplication_DAL.GetSPFOR_LEAVE_ENCASH_POSTING(hR_LEAVE_ENCASHMENT.LC_ID, "PAY_NOW");
                //  AssignToFinalSettlement("ANNUAL LEAVE", CommonUtils.ConvertStringToDecimal(txtLeaveAmount.Text));
                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                //ErrorCollection.Remove("LeavePost");
                //ErrorCollection.Add("LeavePost", "Annual Leave data posted sucessfully");
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void AssignToFinalSettlement(string entityType, decimal settleAmt)
        {
            try
            {
                ErrorCollection.Clear();

                if (EntityData != null)
                {
                    HR_FINAL_SETTLEMENTS = (HR_FINAL_SETTLEMENTS)EntityData;
                }
                if (txtDate.Text != string.Empty)
                {
                    HR_FINAL_SETTLEMENTS.FINAL_SETTLEMENT_DATE = DBMethod.ConvertStringToDate(txtDate.Text);
                }
                HR_FINAL_SETTLEMENTS.FINAL_SETTLEMENT_ORG_ID = VMVServices.Web.Utils.OrganizationID;
                HR_FINAL_SETTLEMENTS.FINAL_SETTLEMENT_DEPT_ID = hf_dept_id.Value;
                HR_FINAL_SETTLEMENTS.FINAL_SETTLEMENT_EMP_ID = ddlEmpName.SelectedValue;
                HR_FINAL_SETTLEMENTS.FINAL_SETTLEMENT_ENTITY_TYPE = entityType;
                HR_FINAL_SETTLEMENTS.FINAL_SETTLEMENT_ENTITY_ID = "";
                HR_FINAL_SETTLEMENTS.FINAL_SETTLEMENT_AMOUNT = settleAmt;
                HR_FINAL_SETTLEMENTS.FINAL_SETTLEMENT_REMARKS = "";
                HR_FINAL_SETTLEMENTS.FINAL_SETTLE_IDENTIFIER1 = "";
                HR_FINAL_SETTLEMENTS.FINAL_SETTLE_IDENTIFIER2 = "";
                HR_FINAL_SETTLEMENTS.FINAL_SETTLE_IDENTIFIER3 = "";
                HR_FINAL_SETTLEMENTS.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                HR_FINAL_SETTLEMENTS.FINAL_SETTLEMENT_TYPE = "FULL";

                HR_FINAL_SETTLEMENTS.FINAL_SETTLEMENT_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_127.ToString(), false, true);
                HR_FINAL_SETTLEMENTS.CREATED_BY = this.LoggedUserName;
                HR_FINAL_SETTLEMENTS.CREATED_DATE = DateTime.Today;

                HR_FINAL_SETTLEMENTS.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, HR_FINAL_SETTLEMENTS.FINAL_SETTLEMENT_ID);

                DBMethod.SaveEntity<HR_FINAL_SETTLEMENTS>(HR_FINAL_SETTLEMENTS);

                if (ErrorCollection.Count > 0)
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        protected void ddlBankName_SelectedIndexChanged(object sender, EventArgs e)
        {
            getBranchName();
        }
        private void getBranchName()
        {
            FIN.BLL.CA.BankBranch_BLL.fn_getBranchName(ref ddlBankBranch, ddlBankName.SelectedValue.ToString());
        }
        protected void ddlBankBranch_SelectedIndexChanged(object sender, EventArgs e)
        {
            getAccountName();
        }
        private void getAccountName()
        {
            FIN.BLL.CA.BankAccounts_BLL.fn_getBankAccount(ref ddlAccountnumber, ddlBankName.SelectedValue, ddlBankBranch.SelectedValue);
        }
        protected void ddlAccountnumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            getChequeNumber();
        }
        private void getChequeNumber()
        {
            FIN.BLL.CA.Cheque_BLL.GetUnusedCheckNumberDetails(ref ddlChequeNumber, ddlBankName.SelectedValue, ddlBankBranch.SelectedValue, ddlAccountnumber.SelectedValue, Master.Mode);
            if (ddlChequeNumber.Items.Count > 1)
            {
                ddlChequeNumber.SelectedIndex = 1;
            }
        }
        protected void imgBtnLoan_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                //LoanPosting();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("hf_Payroll_Id", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void BindGrid_EmployeePayData(DataTable dtEmployeePayData)
        {
            bol_rowVisiable = false;
            Session["dtEmployeePayData"] = dtEmployeePayData;

            if (dtEmployeePayData.Rows.Count > 0)
            {
                dtEmployeePayData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("pay_amount", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("pay_amount"))));
                dtEmployeePayData.AcceptChanges();
            }

            DataTable dt_tmp1 = dtEmployeePayData.Copy();
            if (dt_tmp1.Rows.Count == 0)
            {
                DataRow dr = dt_tmp1.NewRow();
                dr[0] = "0";

                dt_tmp1.Rows.Add(dr);
                bol_rowVisiable = true;
            }
            gvEmployeePay.DataSource = dtEmployeePayData;
            gvEmployeePay.DataBind();
            lblTotalEar.Text = CommonUtils.CalculateTotalAmounts(dtEmployeePayData, "pay_amount", " pay_element_Type='Earning'");
            lblNet.Text = (CommonUtils.ConvertStringToDecimal(lblTotalEar.Text) - CommonUtils.ConvertStringToDecimal(lblTotDed.Text)).ToString();
        }

        private void BindGrid_EmployeePayData2(DataTable dtEmployeePayData2)
        {
            bol_rowVisiable = false;
            Session["dtEmployeePayData2"] = dtEmployeePayData2;

            if (dtEmployeePayData2.Rows.Count > 0)
            {
                dtEmployeePayData2.AsEnumerable().ToList().ForEach(p => p.SetField<String>("pay_amount", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("pay_amount"))));
                dtEmployeePayData2.AcceptChanges();
            }

            DataTable dt_tmp1 = dtEmployeePayData2.Copy();
            if (dt_tmp1.Rows.Count == 0)
            {
                DataRow dr = dt_tmp1.NewRow();
                dr[0] = "0";

                dt_tmp1.Rows.Add(dr);
                bol_rowVisiable = true;
            }
            gvEmployeePay2.DataSource = dtEmployeePayData2;
            gvEmployeePay2.DataBind();
            lblTotDed.Text = CommonUtils.CalculateTotalAmounts(dtEmployeePayData2, "pay_amount", " pay_element_Type='Deduction'");
            lblNet.Text = (CommonUtils.ConvertStringToDecimal(lblTotalEar.Text) - CommonUtils.ConvertStringToDecimal(lblTotDed.Text)).ToString();

        }
        protected void gvEmployeePay_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row["pay_element_Type"].ToString() == "Deduction")
                    {
                        e.Row.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("SBD_ROW_DBOUND", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }

        protected void gvEmployeePay2_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row["pay_element_Type"].ToString() == "Earning")
                    {
                        e.Row.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("SBD_ROW_DBOUND", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }

        private void LoadStoredSettlemetDetails()
        {
            EntityData = hR_FINAL_SETTLEMENT_HDR;




            ddlEmpName.SelectedValue = hR_FINAL_SETTLEMENT_HDR.EMP_ID;
            if (hR_FINAL_SETTLEMENT_HDR.LAST_WORKING_DATE != null)
            {
                txtDate.Text = DBMethod.ConvertDateToString(hR_FINAL_SETTLEMENT_HDR.LAST_WORKING_DATE.ToString());
            }
            txtPayableAmt.Text = DBMethod.GetAmtDecimalCommaSeparationValue(hR_FINAL_SETTLEMENT_HDR.INDEMN_AMOUNT.ToString());
            txtLoanBalAmt.Text = DBMethod.GetAmtDecimalCommaSeparationValue(hR_FINAL_SETTLEMENT_HDR.LOAN_AMOUNT.ToString());
            txtNoOfDays.Text = hR_FINAL_SETTLEMENT_HDR.NO_OF_DAYS.ToString();
            txtLeaveAmount.Text = DBMethod.GetAmtDecimalCommaSeparationValue(hR_FINAL_SETTLEMENT_HDR.LEAVE_SETMT_AMT.ToString());
            ddlBankName.SelectedValue = hR_FINAL_SETTLEMENT_HDR.BANK;
            ddlBankBranch.SelectedValue = hR_FINAL_SETTLEMENT_HDR.BRANCH;
            ddlAccountnumber.SelectedValue = hR_FINAL_SETTLEMENT_HDR.ACCT_NO;
            ddlChequeNumber.SelectedValue = hR_FINAL_SETTLEMENT_HDR.CHEQUE_NO;
            lblAmtTOPAy.Text = DBMethod.GetAmtDecimalCommaSeparationValue(hR_FINAL_SETTLEMENT_HDR.ATTRIBUTE1);
            dtGridData = DBMethod.ExecuteQuery(FinalSettlement_DAL.GetFinalSettlementdtls(ddlEmpName.SelectedValue)).Tables[0];
            BindGrid_EmployeePayData(dtGridData);
            BindGrid_EmployeePayData2(dtGridData);

            if (hR_FINAL_SETTLEMENT_HDR.ATTRIBUTE2 == "POSTED")
            {
                imgBtnPost.Visible = false;
                btnSave.Visible = false;
            }
        }
        private void CalculateSettlementAmount()
        {
            Master.Mode = FINAppConstants.Add;
            Master.StrRecordId = "0";
            txtDate.Enabled = true;
            using (IRepository<HR_FINAL_SETTLEMENT_HDR> userCtx = new DataRepository<HR_FINAL_SETTLEMENT_HDR>())
            {
                hR_FINAL_SETTLEMENT_HDR = userCtx.Find(r =>
                    (r.EMP_ID == ddlEmpName.SelectedValue)
                    ).SingleOrDefault();
            }
            if (hR_FINAL_SETTLEMENT_HDR != null)
            {
                getEmployeeDepartment();
                LoadStoredSettlemetDetails();
                CalAmtToPay();
                txtDate.Enabled = false;
                Master.Mode = FINAppConstants.Update;
                Master.StrRecordId = hR_FINAL_SETTLEMENT_HDR.FNL_STL_HDR_ID;
            }
            else
            {
                getEmployeeDepartment();
                getTerminationDate();
                if (txtDate.Text.ToString().Length > 0)
                {
                    IndemCalc();
                    getLoanBalanceAmt();
                    getEmployeeSettSalary();
                    getAnnualLeaveSettlement();
                    CalAmtToPay();
                }
            }
        }
        private void CalAmtToPay()
        {
            lblAmtTOPAy.Text = ((CommonUtils.ConvertStringToDecimal(txtPayableAmt.Text) + CommonUtils.ConvertStringToDecimal(lblNet.Text) + CommonUtils.ConvertStringToDecimal(txtLeaveAmount.Text)) - CommonUtils.ConvertStringToDecimal(txtLoanBalAmt.Text)).ToString();

        }
        private void getTerminationDate()
        {
            DataTable dtEmpData = DBMethod.ExecuteQuery(Employee_DAL.GetResignedEmpDetails(ddlEmpName.SelectedValue)).Tables[0];
            if (dtEmpData != null)
            {
                if (dtEmpData.Rows.Count > 0)
                {
                    txtDate.Text = DBMethod.ConvertDateToString(dtEmpData.Rows[0]["termination_date"].ToString());

                }
            }
        }

        private void IndemCalc()
        {
            try
            {
                ErrorCollection.Clear();

                string counts = string.Empty;

                counts = DBMethod.GetStringValue(IndemnityDAL.ValidateExistingEmp(hf_dept_id.Value, ddlEmpName.SelectedValue, DBMethod.ConvertStringToDate(txtDate.Text.ToString())));

                if (counts == string.Empty || counts == "0")
                {
                    txtPayableAmt.Text = CommonUtils.ConvertStringToDecimal(IndemnityDAL.GetSP_Indemnity_Amount(ddlEmpName.SelectedValue, DBMethod.ConvertStringToDate(txtDate.Text.ToString())).ToString()).ToString();
                    if (CommonUtils.ConvertStringToDecimal(txtPayableAmt.Text) > 0)
                    {
                        txtPayableAmt.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtPayableAmt.Text);
                    }
                }
                else
                {
                    ErrorCollection.Remove("AlrdyExisting");
                    ErrorCollection.Add("AlrdyExisting", "Already payment completed for this Employee");
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("hf_Payroll_Id", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void getLoanBalanceAmt()
        {
            DataTable dtLoanBal = new DataTable();
            dtLoanBal = DBMethod.ExecuteQuery(LoanRequest_DAL.GetLoanBalAmount(ddlEmpName.SelectedValue)).Tables[0];
            if (dtLoanBal != null)
            {
                if (dtLoanBal.Rows.Count > 0)
                {
                    txtLoanBalAmt.Text = CommonUtils.ConvertStringToDecimal(dtLoanBal.Rows[0]["balance_amount"].ToString()).ToString();
                    if (CommonUtils.ConvertStringToDecimal(txtLoanBalAmt.Text) > 0)
                    {
                        txtLoanBalAmt.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtLoanBalAmt.Text);
                    }

                    hd_loan_req_id.Value = dtLoanBal.Rows[0]["req_id"].ToString();
                }
            }
        }

        private void getEmployeeDepartment()
        {
            DataTable dtgetdtlEmp = DBMethod.ExecuteQuery(PayrollTrailrunDetails_DAL.GetDtlEmp(ddlEmpName.SelectedValue)).Tables[0];
            if (dtgetdtlEmp.Rows.Count > 0)
            {

                hf_dept_id.Value = dtgetdtlEmp.Rows[0]["DEPT_ID"].ToString();
            }
        }
        private void getEmployeeSettSalary()
        {


            //get the employee payroll details
            FINSP.GetPAY_EMP_LEAVE_OLD_SALARY(hf_dept_id.Value, ddlEmpName.SelectedValue, VMVServices.Web.Utils.OrganizationID, DBMethod.ConvertStringToDate(txtDate.Text).ToShortDateString());

            loadEmpSalaryDetails();


        }

        private void loadEmpSalaryDetails()
        {



            DataTable dtEmployeePay = DBMethod.ExecuteQuery(FinalSettlement_DAL.getPayDtlFromTmp(ddlEmpName.SelectedValue)).Tables[0];

            BindGrid_EmployeePayData(dtEmployeePay);
            BindGrid_EmployeePayData2(dtEmployeePay);



        }

        private void getAnnualLeaveSettlement()
        {
            DataTable dt_data = FIN.BLL.SSM.SystemOptions_BLL.fn_getAnnualLeave();
            if (dt_data.Rows.Count > 0)
            {
                if (dt_data.Rows[0]["hr_earn_leave"] != null)
                {
                    //find annual leave id
                    hd_AnnualLeave_Id.Value = dt_data.Rows[0]["hr_earn_leave"].ToString();


                    //get no of annual days 
                    string str_days = DBMethod.GetDecimalValue(StaffLeaveDetails_DAL.getAccuredleavebalnce(hf_dept_id.Value, ddlEmpName.SelectedValue, dt_data.Rows[0]["hr_earn_leave"].ToString(),txtDate.Text)).ToString();
                    txtNoOfDays.Text = CommonUtils.ConvertStringToDecimal(str_days).ToString();
                    if (CommonUtils.ConvertStringToDecimal(txtNoOfDays.Text) > 0)
                    {
                        txtNoOfDays.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtNoOfDays.Text);
                    }
                    hf_No_of_days.Value = CommonUtils.ConvertStringToDecimal(str_days).ToString();


                    //get annual leave salary amount for the leave days
                    txtLeaveAmount.Text = CommonUtils.ConvertStringToDecimal(FIN.DAL.HR.LeaveApplication_DAL.GetSPFOR_Leave_Sal(hf_Payroll_Id.Value, hf_dept_id.Value, ddlEmpName.SelectedValue, hf_No_of_days.Value)).ToString();
                    if (CommonUtils.ConvertStringToDecimal(txtLeaveAmount.Text) > 0)
                    {
                        txtLeaveAmount.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtLeaveAmount.Text);
                    }


                }

            }
        }
        protected void ddlEmpName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                CalculateSettlementAmount();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("SBD_ROW_DBOUND", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void txtDate_TextChanged(object sender, EventArgs e)
        {
            CalSet4DateChange();
        }

        private void CalSet4DateChange()
        {
            IndemCalc();
            getLoanBalanceAmt();
            getEmployeeSettSalary();
            getAnnualLeaveSettlement();
            CalAmtToPay();
        }
    }
}