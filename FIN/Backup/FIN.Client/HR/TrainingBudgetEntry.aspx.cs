﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.BLL.AP;
using FIN.BLL.GL;
using FIN.DAL;
using FIN.BLL.SSM;
using FIN.DAL.HR;
using FIN.DAL.SSM;
using FIN.DAL.GL;
using FIN.BLL;
using FIN.BLL.HR;
using VMVServices.Web;

namespace FIN.Client.HR
{
    public partial class TrainingBudgetEntry : PageBase
    {

        HR_TRAINING_BUDGET hR_TRAINING_BUDGET = new HR_TRAINING_BUDGET();
        System.Collections.SortedList slControls = new System.Collections.SortedList();
        //string ProReturn = null;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();
                   // TrainingDate.Visible = false;

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                //pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                //pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";

            }

            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
            }

            if (VMVServices.Web.Utils.AddAllowed != FINAppConstants.Y && Master.Mode == FINAppConstants.Add)
            {
                btnSave.Visible = false;
            }

        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                //ddlWeightUOM.Enabled = false;
                //ddlLengthUOM.Enabled = false;
                //ddlAreaUOM.Enabled = false;
                //ddlVolumeUOM.Enabled = false;


                Startup();
                FillComboBox();


                EntityData = null;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    using (IRepository<HR_TRAINING_BUDGET> userCtx = new DataRepository<HR_TRAINING_BUDGET>())
                    {
                        hR_TRAINING_BUDGET = userCtx.Find(r =>
                            (r.TRN_BUDGET_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = hR_TRAINING_BUDGET;

                    ddlDepartment.SelectedValue = hR_TRAINING_BUDGET.TRN_BUDGET_DEPT_ID;

                    ddlProgramName.SelectedValue = hR_TRAINING_BUDGET.TRN_BUDGET_PROG_ID;
                    txtShortName.Text = hR_TRAINING_BUDGET.TRN_BUDGET_SHORT_NAME;
                    txtPreparedBy.Text = hR_TRAINING_BUDGET.TRN_BUDGET_PREPARED_BY;
                    ddlFiscalYear.SelectedValue = hR_TRAINING_BUDGET.TRN_FISCAL_YEAR;
                    if (hR_TRAINING_BUDGET.TRN_EFFECTIVE_FROM_DT != null)
                    {
                        txtFromDate.Text = DBMethod.ConvertDateToString(hR_TRAINING_BUDGET.TRN_EFFECTIVE_FROM_DT.ToString());
                    }
                    if (hR_TRAINING_BUDGET.TRN_EFFECTIVE_TO_DT != null)
                    {
                        txtToDate.Text = DBMethod.ConvertDateToString(hR_TRAINING_BUDGET.TRN_EFFECTIVE_TO_DT.ToString());
                    }
                    showDate();
                    ddlCurrency.SelectedValue = hR_TRAINING_BUDGET.TRN_BUDGET_CURRENCY;
                    //txtAmount.Text = hR_TRAINING_BUDGET.TRN_BUDGET_AMOUNT.ToString();
                    //txtAmount.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtAmount.Text);
                    txtAmount.Text = DBMethod.GetAmtDecimalCommaSeparationValue(hR_TRAINING_BUDGET.TRN_BUDGET_AMOUNT.ToString());

                    txtRemarks.Text = hR_TRAINING_BUDGET.TRN_BUDGET_REMARKS;









                    ChkEnabledFlag.Checked = hR_TRAINING_BUDGET.ENABLED_FLAG == FINAppConstants.EnabledFlag ? true : false;
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>

        private void FillComboBox()
        {
            ComboFilling.fn_getDepartment(ref ddlDepartment);
            AccountingCalendar_BLL.GetCalAcctYear(ref ddlFiscalYear);
            Program_BLL.fn_getProgrameName(ref ddlProgramName);
            Country_BLL.GetCurrencyDetails(ref ddlCurrency);



        }

        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    hR_TRAINING_BUDGET = (HR_TRAINING_BUDGET)EntityData;
                }

                hR_TRAINING_BUDGET.TRN_BUDGET_DEPT_ID = ddlDepartment.SelectedValue;

                hR_TRAINING_BUDGET.TRN_BUDGET_PROG_ID = ddlProgramName.SelectedValue;
                hR_TRAINING_BUDGET.TRN_BUDGET_SHORT_NAME = txtShortName.Text;
                hR_TRAINING_BUDGET.TRN_BUDGET_PREPARED_BY = txtPreparedBy.Text;
                hR_TRAINING_BUDGET.TRN_FISCAL_YEAR = ddlFiscalYear.SelectedValue;
                if (txtFromDate.Text != string.Empty)
                {
                    hR_TRAINING_BUDGET.TRN_EFFECTIVE_FROM_DT = DBMethod.ConvertStringToDate(txtFromDate.Text.ToString());
                }
                if (txtToDate.Text != string.Empty)
                {
                    hR_TRAINING_BUDGET.TRN_EFFECTIVE_TO_DT = DBMethod.ConvertStringToDate(txtToDate.Text.ToString());
                }
                hR_TRAINING_BUDGET.TRN_BUDGET_CURRENCY = ddlCurrency.SelectedValue;
                //hR_TRAINING_BUDGET.TRN_BUDGET_AMOUNT = decimal.Parse(txtAmount.Text);
                hR_TRAINING_BUDGET.TRN_BUDGET_AMOUNT = CommonUtils.ConvertStringToDecimal(txtAmount.Text);

                hR_TRAINING_BUDGET.TRN_BUDGET_REMARKS = txtRemarks.Text;
                hR_TRAINING_BUDGET.TRN_BUDGET_ORG_ID = VMVServices.Web.Utils.OrganizationID;

                // hR_TRAINING_BUDGET.ENABLED_FLAG = FINAppConstants.Y;

                hR_TRAINING_BUDGET.ENABLED_FLAG = ChkEnabledFlag.Checked == true ? FINAppConstants.EnabledFlag : FINAppConstants.DisabledFlag;

                //if (chkItemSerialControl.Checked == true)
                //{
                //    ddlServiceUOM.Enabled = true;
                //    iNV_ITEM_MASTER.SERVICE_DURATION_UOM = ddlServiceUOM.SelectedValue;
                //}
                //else
                //{
                //    ddlServiceUOM.Attributes.Add("disabled", "disabled");
                //}


                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    hR_TRAINING_BUDGET.MODIFIED_BY = this.LoggedUserName;
                    hR_TRAINING_BUDGET.MODIFIED_DATE = DateTime.Today;

                }
                else
                {
                    hR_TRAINING_BUDGET.TRN_BUDGET_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_089.ToString(), false, true);
                    //iNV_ITEM_MASTER.WORKFLOW_COMPLETION_STATUS = "1";
                    hR_TRAINING_BUDGET.CREATED_BY = this.LoggedUserName;
                    hR_TRAINING_BUDGET.CREATED_DATE = DateTime.Today;

                }

                hR_TRAINING_BUDGET.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_TRAINING_BUDGET.TRN_BUDGET_ID);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                AssignToBE();


                //emptyvalid();
                if (ErrorCollection.Count > 0)
                {
                    return;
                }


                // Duplicate Validation Through Backend Package PKG_VALIDATIONS



                //ProReturn = FIN.DAL.AP.Item_DAL.GetSPFOR_DUPLICATE_CHECK(iNV_ITEM_MASTER.ITEM_CODE,txtItemName.Text, iNV_ITEM_MASTER.ORG_ID, iNV_ITEM_MASTER.ITEM_ID);

                //if (ProReturn != string.Empty)
                //{
                //    if (ProReturn != "0")
                //    {
                //        ErrorCollection.Add("ITEMNAME", ProReturn);
                //        if (ErrorCollection.Count > 0)
                //        {
                //            return;
                //        }
                //    }
                //}


                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            DBMethod.SaveEntity<HR_TRAINING_BUDGET>(hR_TRAINING_BUDGET);
                            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                            break;
                        }
                    case FINAppConstants.Update:
                        {

                            DBMethod.SaveEntity<HR_TRAINING_BUDGET>(hR_TRAINING_BUDGET, true);
                            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                            break;

                        }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion




        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                DBMethod.DeleteEntity<HR_TRAINING_BUDGET>(hR_TRAINING_BUDGET);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }



        protected void btnCancel_Click(object sender, EventArgs e)
        {

        }


        private void showDate()
        {
            TrainingDate.Visible = false;

            if (ddlFiscalYear.Enabled == true)
            {
                TrainingDate.Visible = true;

            }


        }

        protected void ddlFiscalYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            showDate();
            FillFromToDate();
        }


        private void FillFromToDate()
        {
            DataTable dtData = new DataTable();
            if (ddlFiscalYear.SelectedValue != null)
            {
                dtData = DBMethod.ExecuteQuery(AccountingCalendar_DAL.GetStartdtEnddtfrcalyear((ddlFiscalYear.SelectedValue.ToString()))).Tables[0];
                if (dtData != null)
                {
                    if (dtData.Rows.Count > 0)
                    {
                        txtFromDate.Text = DateTime.Parse(dtData.Rows[0][FINColumnConstants.CAL_EFF_START_DT].ToString()).ToString("dd/MM/yyyy");

                        if (dtData.Rows[0][FINColumnConstants.CAL_EFF_END_DT].ToString().Count() > 0)
                        {
                            txtToDate.Text = DateTime.Parse(dtData.Rows[0][FINColumnConstants.CAL_EFF_END_DT].ToString()).ToString("dd/MM/yyyy");
                        }
                    }
                }
            }
        }


        //private void emptyvalid()
        //{
        //    if (hR_VACANCIES.VAC_DEPT_ID == null)
        //    {

        //        if (ddlDepartment.SelectedValue == string.Empty)
        //        {
        //            ErrorCollection.Add("Department", "Department Cannot be empty");
        //        }

        //    }
        //    if (hR_VACANCIES.VAC_DESIG_ID == null)
        //    {
        //        if (ddlDesignation.SelectedValue == string.Empty)

        //            ErrorCollection.Add("Designation", "Designation Cannot be empty");
        //        }
        //    }
        //    //if (hR_VACANCIES.VAC_TYPE > 0)
        //    //{

        //    //    if (ddlType.SelectedValue == string.Empty)
        //    //    {
        //    //        ErrorCollection.Add("DepartmentType", "Department Type Cannot be empty");
        //    //    }
        //    //}

        //    //if  > 0)
        //    //{
        //    //    if (ddlVolumeUOM.SelectedValue == string.Empty)
        //    //    {

        //    //        ErrorCollection.Add("VOLEMT", "Volume UOM Cannot be empty");
        //    //    }
        //    //}

        //}






    }





}
