﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.DAL.GL;
using FIN.BLL;
using FIN.BLL.GL;
using FIN.BLL.HR;
using VMVServices.Web;


namespace FIN.Client.HR
{
    public partial class AppraisalTrainingEntry : PageBase
    {
        HR_APPRAISAL_TRAINING hR_APPRAISAL_TRAINING = new HR_APPRAISAL_TRAINING();

        DataTable dtData = new DataTable();
        Boolean bol_rowVisiable;
        Boolean savedBool;
        DataTable dtGridData = new DataTable();

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AppT_PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
        }

        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        private void FillLeaveRequest(GridViewRow gvr)
        {
            // GridViewRow gvr = gvData.FooterRow;

            DropDownList ddlStaff = gvr.FindControl("ddlStaff") as DropDownList;
            DropDownList ddlAttendanceType = gvr.FindControl("ddlAttendanceType") as DropDownList;
            DropDownList ddlRequest = gvr.FindControl("ddlRequest") as DropDownList;

            if (ddlStaff.SelectedValue != "")
            {
                LeaveApplication_BLL.GetLeaveApplication(ref ddlRequest, ddlStaff.SelectedValue.ToString());

                if (gvData.EditIndex >= 0)
                {
                    
                }
            }

            if (ddlAttendanceType.SelectedItem.Text == "Leave")
            {
                ddlRequest.Enabled = true;
            }
            else
            {
                ddlRequest.Enabled = false;
            }
        }
        protected void ddlAttendanceType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
                FillLeaveRequest(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// .RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void FillComboBox()
        {
            FIN.BLL.HR.AppraisalDefineKRAEntry_BLL.fn_GetAppraisalDtlsBasedInitiateAppr(ref ddlAppraisal);
        }
        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();

                EntityData = null;
                Session[FINSessionConstants.GridData] = null;

                dtGridData = DBMethod.ExecuteQuery(AppraisalAssesment_DAL.GetAppraisalTrainingDetails(Master.StrRecordId)).Tables[0];

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    using (IRepository<HR_APPRAISAL_TRAINING> userCtx = new DataRepository<HR_APPRAISAL_TRAINING>())
                    {
                        hR_APPRAISAL_TRAINING = userCtx.Find(r =>
                            (r.APPRAISAL_TRAINING_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }
                    EntityData = hR_APPRAISAL_TRAINING;
                    ddlAppraisal.SelectedValue = hR_APPRAISAL_TRAINING.ASSIGN_HDR_ID;
                }
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AppTraining_Entry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                //GridViewRow gvr = gvData.FooterRow;
                //FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AppTraining_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddlEmpl = tmpgvr.FindControl("ddlEmpl") as DropDownList;
                DropDownList ddlCourse = tmpgvr.FindControl("ddlCourseDetails") as DropDownList;

                AppraisalAssesment_BLL.GetTrainingEmployeeDetails(ref ddlEmpl);
                AppraisalAssesment_BLL.GetCourseDetails(ref ddlCourse);

                if (gvData.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                {
                    ddlEmpl.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.EMP_ID].ToString();
                    ddlCourse.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.COURSE_ID].ToString();

                    if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                    {
                        //FillLeaveRequest(tmpgvr);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RLD_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        #endregion
        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session[FINSessionConstants.GridData], "Appraisal Training");

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                AssignToBE();

                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }
        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                var tmpChildEntity = new List<Tuple<object, string>>();

                if (gvData.Rows.Count > 0)
                {
                    for (int iLoop = 0; iLoop < gvData.Rows.Count; iLoop++)
                    {
                        hR_APPRAISAL_TRAINING = new HR_APPRAISAL_TRAINING();

                        if (gvData.DataKeys[iLoop].Values["APPRAISAL_TRAINING_ID"].ToString() != "0" && gvData.DataKeys[iLoop].Values["APPRAISAL_TRAINING_ID"].ToString() != string.Empty)
                        {
                            using (IRepository<HR_APPRAISAL_TRAINING> userCtx = new DataRepository<HR_APPRAISAL_TRAINING>())
                            {
                                hR_APPRAISAL_TRAINING  = userCtx.Find(r =>
                                    (r.APPRAISAL_TRAINING_ID == gvData.DataKeys[iLoop].Values["APPRAISAL_TRAINING_ID"].ToString())
                                    ).SingleOrDefault();
                            }
                        }

                        TextBox txtComments = (TextBox)gvData.Rows[iLoop].FindControl("txtComments");
                        DropDownList ddlCourseDetails = (DropDownList)gvData.Rows[iLoop].FindControl("ddlCourseDetails");
                        
                        hR_APPRAISAL_TRAINING.EMP_ID = gvData.DataKeys[iLoop].Values["EMP_ID"].ToString();
                        hR_APPRAISAL_TRAINING.COURSE_ID = ddlCourseDetails.SelectedValue;
                        hR_APPRAISAL_TRAINING.REV_TRNING_NEEDED = gvData.DataKeys[iLoop].Values["REV_TRNING_NEEDED"].ToString();
                        hR_APPRAISAL_TRAINING.COMMENTS = txtComments.Text.ToString();

                        hR_APPRAISAL_TRAINING.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                        hR_APPRAISAL_TRAINING.ORG_ID = VMVServices.Web.Utils.OrganizationID;
                        hR_APPRAISAL_TRAINING.ASSIGN_HDR_ID = ddlAppraisal.SelectedValue;

                        if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                        {
                            tmpChildEntity.Add(new Tuple<object, string>(hR_APPRAISAL_TRAINING, "D"));
                        }
                        else
                        {
                            if (gvData.DataKeys[iLoop].Values["APPRAISAL_TRAINING_ID"].ToString() != "0" && gvData.DataKeys[iLoop].Values["APPRAISAL_TRAINING_ID"].ToString() != string.Empty)
                            {
                                hR_APPRAISAL_TRAINING.APPRAISAL_TRAINING_ID = gvData.DataKeys[iLoop].Values["APPRAISAL_TRAINING_ID"].ToString();
                                hR_APPRAISAL_TRAINING.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_APPRAISAL_TRAINING.APPRAISAL_TRAINING_ID);
                                hR_APPRAISAL_TRAINING.MODIFIED_BY = this.LoggedUserName;
                                hR_APPRAISAL_TRAINING.MODIFIED_DATE = DateTime.Today;
                                tmpChildEntity.Add(new Tuple<object, string>(hR_APPRAISAL_TRAINING, FINAppConstants.Update));
                            }
                            else
                            {
                                //HR_STAFF_ATTENDANCE.STATT_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_022);
                                hR_APPRAISAL_TRAINING.APPRAISAL_TRAINING_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_111.ToString(), false, true);
                                hR_APPRAISAL_TRAINING.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_APPRAISAL_TRAINING.APPRAISAL_TRAINING_ID);
                                hR_APPRAISAL_TRAINING.CREATED_BY = this.LoggedUserName;
                                hR_APPRAISAL_TRAINING.CREATED_DATE = DateTime.Today;
                                tmpChildEntity.Add(new Tuple<object, string>(hR_APPRAISAL_TRAINING, FINAppConstants.Add));
                            }
                        }
                    }
                }


                //for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                //{
                //    hR_APPRAISAL_TRAINING = new HR_APPRAISAL_TRAINING();

                //    if ((dtGridData.Rows[iLoop][FINColumnConstants.APPRAISAL_TRAINING_ID].ToString()) != "0" && dtGridData.Rows[iLoop][FINColumnConstants.APPRAISAL_TRAINING_ID].ToString() != string.Empty)
                //    {
                //        hR_APPRAISAL_TRAINING = AppraisalTraining_BLL.getClassEntity(dtGridData.Rows[iLoop][FINColumnConstants.APPRAISAL_TRAINING_ID].ToString());

                //    }
                //    hR_APPRAISAL_TRAINING.EMP_ID = dtGridData.Rows[iLoop][FINColumnConstants.EMP_ID].ToString();
                //    hR_APPRAISAL_TRAINING.REV_TRNING_NEEDED = gvData.DataKeys[iLoop].Values[FINColumnConstants.REV_TRNING_NEEDED].ToString();
                    
                //    hR_APPRAISAL_TRAINING.COURSE_ID = dtGridData.Rows[iLoop][FINColumnConstants.COURSE_ID].ToString();

                //    hR_APPRAISAL_TRAINING.COMMENTS = dtGridData.Rows[iLoop][FINColumnConstants.COMMENTS].ToString();
                   
                //    hR_APPRAISAL_TRAINING.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                //    hR_APPRAISAL_TRAINING.ORG_ID = VMVServices.Web.Utils.OrganizationID;


                //    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                //    {
                //        tmpChildEntity.Add(new Tuple<object, string>(hR_APPRAISAL_TRAINING, "D"));
                //    }
                //    else
                //    {
                //        if ((dtGridData.Rows[iLoop][FINColumnConstants.APPRAISAL_TRAINING_ID].ToString()) != "0" && dtGridData.Rows[iLoop][FINColumnConstants.APPRAISAL_TRAINING_ID].ToString() != string.Empty)
                //        {
                //            hR_APPRAISAL_TRAINING.APPRAISAL_TRAINING_ID = dtGridData.Rows[iLoop]["APPRAISAL_TRAINING_ID"].ToString();
                //            hR_APPRAISAL_TRAINING.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_APPRAISAL_TRAINING.APPRAISAL_TRAINING_ID);
                //            hR_APPRAISAL_TRAINING.MODIFIED_BY = this.LoggedUserName;
                //            hR_APPRAISAL_TRAINING.MODIFIED_DATE = DateTime.Today;
                //            tmpChildEntity.Add(new Tuple<object, string>(hR_APPRAISAL_TRAINING, FINAppConstants.Update));
                //        }
                //        else
                //        {
                //            //HR_STAFF_ATTENDANCE.STATT_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_022);
                //            hR_APPRAISAL_TRAINING.APPRAISAL_TRAINING_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_111.ToString(), false, true);
                //            hR_APPRAISAL_TRAINING.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_APPRAISAL_TRAINING.APPRAISAL_TRAINING_ID);
                //            hR_APPRAISAL_TRAINING.CREATED_BY = this.LoggedUserName;
                //            hR_APPRAISAL_TRAINING.CREATED_DATE = DateTime.Today;
                //            tmpChildEntity.Add(new Tuple<object, string>(hR_APPRAISAL_TRAINING, FINAppConstants.Add));
                //        }
                //    }
                //}
                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            CommonUtils.SaveSingleEntity<HR_APPRAISAL_TRAINING>(tmpChildEntity);
                            savedBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {
                            CommonUtils.SaveSingleEntity<HR_APPRAISAL_TRAINING>(tmpChildEntity, true);
                            savedBool = true;
                            break;
                        }
                }
                if (ErrorCollection.Count > 0)
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {

                    // HR_STAFF_ATTENDANCE.LSD_ID = (dtGridData.Rows[iLoop]["LSD_ID"].ToString());
                    DBMethod.DeleteEntity<HR_APPRAISAL_TRAINING>(hR_APPRAISAL_TRAINING);
                }


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion


        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            try
            {
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = -1;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_ROW_CNCL_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_ROW_CMD", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {
            System.Collections.SortedList slControls = new System.Collections.SortedList();

            DropDownList ddlEmpl = gvr.FindControl("ddlEmpl") as DropDownList;
            DropDownList ddlCourse = gvr.FindControl("ddlCourseDetails") as DropDownList;
            TextBox txtComments = gvr.FindControl("txtComments") as TextBox;

            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));

            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList[FINColumnConstants.APPRAISAL_TRAINING_ID] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);
            }

            ErrorCollection.Clear();

            string strCtrlTypes = string.Empty;
            string strMessage = string.Empty;

            //slControls[0] = ddlEmpl;
            slControls[0] = ddlCourse;
            slControls[1] = txtComments;

            strCtrlTypes = FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.TEXT_BOX;
            strMessage = Prop_File_Data["Course_Details_P"] + " ~ " + Prop_File_Data["Comments_P"] + "";

            ErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);
            if (ErrorCollection.Count > 0)
                return drList;

            //string strCondition = "EMP_ID='" + ddlEmpl.SelectedValue.ToString() + "'";
            string strCondition = "COURSE_ID='" + ddlCourse.SelectedValue.ToString() + "'";
            strMessage = FINMessageConstatns.RecordAlreadyExists;
            ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);

            if (ErrorCollection.Count > 0)
            {
                return drList;
            }

            //drList[FINColumnConstants.EMP_ID] = ddlEmpl.SelectedValue;
            drList[FINColumnConstants.EMP_NAME] = ddlEmpl.SelectedItem.Text;
            drList[FINColumnConstants.COURSE_ID] = ddlCourse.SelectedValue;
            drList[FINColumnConstants.COURSE_DESC] = ddlCourse.SelectedItem.Text;
            drList[FINColumnConstants.COMMENTS] = txtComments.Text;

            drList[FINColumnConstants.DELETED] = FINAppConstants.N;


            return drList;



        }



        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AppTraining_ROW_UP", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AppTraining_ROW_DEL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                //GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                //FillFooterGridCombo(gvr);


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AppTraining_ROW_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AppTraining_RowCreated", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                    {
                        e.Row.Visible = false;
                    }
                    else
                    {
                        DropDownList ddlEmpl = (DropDownList)e.Row.FindControl("ddlEmpl");
                        DropDownList ddlCourse = (DropDownList)e.Row.FindControl("ddlCourseDetails");

                        AppraisalAssesment_BLL.GetTrainingEmployeeDetails(ref ddlEmpl);
                        AppraisalAssesment_BLL.GetCourseDetails(ref ddlCourse);

                        ddlCourse.SelectedValue = gvData.DataKeys[e.Row.RowIndex].Values[FINColumnConstants.COURSE_ID].ToString();
                        
                    }

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AppTraining_RDB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        #endregion

        protected void ddlAppraisal_SelectedIndexChanged(object sender, EventArgs e)
        {
            dtGridData = DBMethod.ExecuteQuery(AppraisalAssesment_DAL.GetAppraisalTrainingDtls(ddlAppraisal.SelectedValue.ToString())).Tables[0];
            BindGrid(dtGridData);
        }
    }
}