﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.DAL.GL;
using FIN.BLL;
using FIN.BLL.GL;
using FIN.BLL.HR;
using VMVServices.Web;
using System.Collections;
using System.Data;

namespace FIN.Client.HR
{
    public partial class EmployeeFeedbackEntry : PageBase
    {
        HR_EMP_FEEDBACK_HDR hR_EMP_FEEDBACK_HDR = new HR_EMP_FEEDBACK_HDR();
        HR_EMP_FEEDBACK_DTL hR_EMP_FEEDBACK_DTL = new HR_EMP_FEEDBACK_DTL();
        DataTable dtGridData = new DataTable();
        Boolean bol_rowVisiable;
        Boolean savedBool = false;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    ErrorCollection.Clear();

                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("FeedbackEntryPgLoad", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//  ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + iAcademeMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FIN.BLL.FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FIN.BLL.FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();

        }

        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        private void FillComboBox()
        {
            txtFromDate.Text = DateTime.Now.ToShortDateString();
            FIN.BLL.HR.Employee_BLL.GetEmployeeId(ref ddlEmpName);
            FIN.BLL.HR.FeedbackTemplateBLL.fn_GetFeedbackTemplateName(ref ddlFeedbackTempName);
        }
        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>
        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();

                EntityData = null;

                dtGridData = FIN.BLL.HR.FeedbackTemplateBLL.getEmpChildEntityDetails(Master.StrRecordId);
                BindGrid(dtGridData);


                if (Master.Mode != FIN.BLL.FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    using (IRepository<HR_EMP_FEEDBACK_HDR> userCtx = new DataRepository<HR_EMP_FEEDBACK_HDR>())
                    {
                        hR_EMP_FEEDBACK_HDR = userCtx.Find(r =>
                            (r.EMP_FEEDBACK_ID == Master.StrRecordId.ToString())
                            ).SingleOrDefault();
                    }

                    EntityData = hR_EMP_FEEDBACK_HDR;
                    ddlEmpName.SelectedValue = hR_EMP_FEEDBACK_HDR.EMP_ID.ToString();
                    ddlFeedbackTempName.SelectedValue = hR_EMP_FEEDBACK_HDR.EMP_FEEDBACK_TEMP_NAME.ToString();
                    if (hR_EMP_FEEDBACK_HDR.EFFECTIVE_FROM_DT != null)
                    {
                        txtFromDate.Text = DBMethod.ConvertDateToString(hR_EMP_FEEDBACK_HDR.EFFECTIVE_FROM_DT.ToString());
                    }

                    if (hR_EMP_FEEDBACK_HDR.EFFECTIVE_TO_DT != null)
                    {
                        txtToDate.Text = DBMethod.ConvertDateToString(hR_EMP_FEEDBACK_HDR.EFFECTIVE_TO_DT.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("FeedbackEntryATC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//  ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + iAcademeMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the exam master table entities
        /// Fetch the CalendarID and OrganizationID from common properties which was stored while the user has been logined.
        /// </summary>

        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();

                if (EntityData != null)
                {
                    hR_EMP_FEEDBACK_HDR = (HR_EMP_FEEDBACK_HDR)EntityData;
                }

                hR_EMP_FEEDBACK_HDR.EMP_FEEDBACK_TEMP_NAME = ddlFeedbackTempName.SelectedValue.ToString();
                hR_EMP_FEEDBACK_HDR.EMP_ID = ddlEmpName.SelectedValue.ToString();
                if (txtFromDate.Text != string.Empty)
                {
                    hR_EMP_FEEDBACK_HDR.EFFECTIVE_FROM_DT = DBMethod.ConvertStringToDate(txtFromDate.Text.ToString());
                }

                hR_EMP_FEEDBACK_HDR.ENABLED_FLAG = FINAppConstants.Y;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != string.Empty)
                {
                    hR_EMP_FEEDBACK_HDR.MODIFIED_BY = this.LoggedUserName;
                    hR_EMP_FEEDBACK_HDR.MODIFIED_DATE = DateTime.Today;
                }
                else
                {
                    hR_EMP_FEEDBACK_HDR.CREATED_BY = this.LoggedUserName;
                    hR_EMP_FEEDBACK_HDR.CREATED_DATE = DateTime.Today;
                    hR_EMP_FEEDBACK_HDR.EMP_FEEDBACK_ID = FINSP.GetSPFOR_SEQCode("HR_R_083_M".ToString(), false, true);
                }

                hR_EMP_FEEDBACK_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_EMP_FEEDBACK_HDR.EMP_FEEDBACK_ID);

                //Save Detail Table
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                var tmpChildEntity = new List<Tuple<object, string>>();

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    hR_EMP_FEEDBACK_DTL = new HR_EMP_FEEDBACK_DTL();
                    if (dtGridData.Rows[iLoop]["EMP_FEEDBACK_DTL_ID"].ToString() != "0")
                    {
                        using (IRepository<HR_EMP_FEEDBACK_DTL> userCtx = new DataRepository<HR_EMP_FEEDBACK_DTL>())
                        {
                            hR_EMP_FEEDBACK_DTL = userCtx.Find(r =>
                                (r.EMP_FEEDBACK_DTL_ID == dtGridData.Rows[iLoop]["EMP_FEEDBACK_DTL_ID"].ToString())
                                ).SingleOrDefault();
                        }
                    }

                    if (dtGridData.Rows[iLoop]["EMP_FEEDBACK_DTL_ID"].ToString() != "0")
                    {
                        FIN.BLL.HR.FeedbackTemplateBLL.getEmpClassEntity(dtGridData.Rows[iLoop]["EMP_FEEDBACK_DTL_ID"].ToString());
                    }
                    hR_EMP_FEEDBACK_DTL.EMP_FEEDBACK_DESC = dtGridData.Rows[iLoop]["feedback_id_desc"].ToString();
                    hR_EMP_FEEDBACK_DTL.EMP_FEEDBACK_TYPE = dtGridData.Rows[iLoop]["feedback_type"].ToString();
                    hR_EMP_FEEDBACK_DTL.EMP_FEEDBACK_RATING = dtGridData.Rows[iLoop]["feedback_rating_type"].ToString();
                    hR_EMP_FEEDBACK_DTL.EMP_COMMENTS = dtGridData.Rows[iLoop]["emp_comments"].ToString();
                    hR_EMP_FEEDBACK_DTL.EMP_FEEDBACK_RATE = dtGridData.Rows[iLoop]["rating_code_name"].ToString();
                    hR_EMP_FEEDBACK_DTL.EMP_FEEDBACK_ID = hR_EMP_FEEDBACK_HDR.EMP_FEEDBACK_ID;

                    hR_EMP_FEEDBACK_DTL.ENABLED_FLAG = dtGridData.Rows[iLoop]["ENABLED_FLAG"].ToString() == FIN.BLL.FINAppConstants.TRUEFLAG ? FIN.BLL.FINAppConstants.EnabledFlag : FIN.BLL.FINAppConstants.DisabledFlag;
                    hR_EMP_FEEDBACK_DTL.WORKFLOW_COMPLETION_STATUS = FINAppConstants.Y;

                    if (dtGridData.Rows[iLoop]["DELETED"].ToString() == FIN.BLL.FINAppConstants.EnabledFlag)
                    {
                        hR_EMP_FEEDBACK_DTL.EMP_FEEDBACK_DTL_ID = dtGridData.Rows[iLoop]["EMP_FEEDBACK_DTL_ID"].ToString();
                        tmpChildEntity.Add(new Tuple<object, string>(hR_EMP_FEEDBACK_DTL, FINAppConstants.Delete));
                    }
                    else
                    {
                        if (dtGridData.Rows[iLoop]["EMP_FEEDBACK_DTL_ID"].ToString() != "0")
                        {
                            hR_EMP_FEEDBACK_DTL.EMP_FEEDBACK_DTL_ID = dtGridData.Rows[iLoop]["EMP_FEEDBACK_DTL_ID"].ToString();
                            hR_EMP_FEEDBACK_DTL.MODIFIED_BY = this.LoggedUserName;
                            hR_EMP_FEEDBACK_DTL.MODIFIED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(hR_EMP_FEEDBACK_DTL, FINAppConstants.Update));
                        }
                        else
                        {
                            hR_EMP_FEEDBACK_DTL.EMP_FEEDBACK_DTL_ID = FINSP.GetSPFOR_SEQCode("HR_R_083_D".ToString(), false, true);
                            hR_EMP_FEEDBACK_DTL.CREATED_BY = this.LoggedUserName;
                            hR_EMP_FEEDBACK_DTL.CREATED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(hR_EMP_FEEDBACK_DTL, FINAppConstants.Add));
                        }
                    }
                }
                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            CommonUtils.SavePCEntity<HR_EMP_FEEDBACK_HDR, HR_EMP_FEEDBACK_DTL>(hR_EMP_FEEDBACK_HDR, tmpChildEntity, hR_EMP_FEEDBACK_DTL);
                            savedBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {
                            CommonUtils.SavePCEntity<HR_EMP_FEEDBACK_HDR, HR_EMP_FEEDBACK_DTL>(hR_EMP_FEEDBACK_HDR, tmpChildEntity, hR_EMP_FEEDBACK_DTL, true);
                            savedBool = true;
                            break;
                        }
                }
                if (ErrorCollection.Count > 0)
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("FeedbackEntryATOBE", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }


        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session[FINSessionConstants.GridData], "FEEDBACK DETAILS ");

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                AssignToBE();

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                }

            }
            catch (Exception ex)
            {
                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//  ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + iAcademeMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion

        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            if (Session[FINSessionConstants.GridData] != null)
            {
                dtGridData = (DataTable)Session[FINSessionConstants.GridData];
            }
            gvData.EditIndex = -1;
            BindGrid(dtGridData);

        }
        #endregion

        /// <summary>
        /// Bind the records into grid voew
        /// </summary>
        /// <param name="dtData">Contains the database entities and correspoding records which is used in the grid view</param>
        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr["ENABLED_FLAG"] = "FALSE";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();

                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("FeedbackEntryBG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//  ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + iAcademeMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            DropDownList ddlFeedbacktype = tmpgvr.FindControl("ddlFeedbacktyp") as DropDownList;
            DropDownList ddlFeedbackDesc = tmpgvr.FindControl("ddlFeedbackDesc") as DropDownList;
            DropDownList ddlFeedbackRatingType = tmpgvr.FindControl("ddlFeedbackRatingType") as DropDownList;
            DropDownList ddlFeedbackRating = tmpgvr.FindControl("ddlFeedbackRating") as DropDownList;

            if (ddlFeedbackTempName.SelectedValue.ToString().Length > 0)
            {
                FIN.BLL.HR.FeedbackTemplateBLL.fn_GetFeedbackTypes(ref ddlFeedbacktype, ddlFeedbackTempName.SelectedValue.ToString());
                FIN.BLL.HR.FeedbackTemplateBLL.fn_GetFeedbackDesc(ref ddlFeedbackDesc, ddlFeedbackTempName.SelectedValue.ToString());
                FIN.BLL.HR.FeedbackTemplateBLL.fn_GetFeedbackRating(ref ddlFeedbackRatingType, ddlFeedbackTempName.SelectedValue.ToString());
            }

            if (gvData.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
            {
                if (gvData.DataKeys[gvData.EditIndex].Values["feedback_id"] != null)
                {
                    ddlFeedbacktype.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["feedback_id_type"].ToString();
                    ddlFeedbackRatingType.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["feedback_id_rating_type"].ToString();
                    ddlFeedbackDesc.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["feedback_id_desc"].ToString();
                }

                if (ddlFeedbackRatingType.SelectedValue.ToString().Length > 0)
                {
                    if (gvData.DataKeys[gvData.EditIndex].Values["rating_code_id"] != null)
                    {
                        if (ddlFeedbackRatingType.SelectedItem.Text.ToString().ToUpper() == "Y/N")
                        {
                            Lookup_BLL.GetLookUpValues(ref ddlFeedbackRating, "FEEDBACK_RATING");
                            ddlFeedbackRating.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["rating_code_id"].ToString();
                        }
                        if (ddlFeedbackRatingType.SelectedItem.Text.ToString().ToUpper() == "NUMBERS")
                        {
                            Lookup_BLL.GetLookUpValues(ref ddlFeedbackRating, "FEEDBACK_RATING_N");
                            ddlFeedbackRating.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["rating_code_id"].ToString();
                        }
                    }
                }
            }
        }
        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FIN.BLL.FINAppConstants.Y;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("FeedbackEntryGVD", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//  ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + iAcademeMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("FeedbackEntryGVE", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//  ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + iAcademeMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("FeedbackEntryGVC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//  ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + iAcademeMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;
                    if (dtGridData.Rows.Count > 0)
                    {
                        if (dtGridData.Rows[e.Row.RowIndex][FINColumnConstants.DELETED].ToString() == FIN.BLL.FINAppConstants.Y)
                        {
                            e.Row.Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("FeedbackEntryGVB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//  ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + iAcademeMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }

                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//  ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + iAcademeMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("FeedbackEntryGVR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//  ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + iAcademeMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {
            System.Collections.SortedList slControls = new System.Collections.SortedList();
            DataRow drList;

            DropDownList ddlFeedbacktype = gvr.FindControl("ddlFeedbacktyp") as DropDownList;
            DropDownList ddlFeedbackRatingType = gvr.FindControl("ddlFeedbackRatingType") as DropDownList;
            DropDownList ddlFeedbackRating = gvr.FindControl("ddlFeedbackRating") as DropDownList;
            DropDownList ddlFeedbackDesc = gvr.FindControl("ddlFeedbackDesc") as DropDownList;
            TextBox txtComments = gvr.FindControl("txtComments") as TextBox;
            CheckBox chkact = gvr.FindControl("chkact") as CheckBox;

            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList["EMP_FEEDBACK_DTL_ID"] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);
            }

            slControls[0] = ddlFeedbacktype;
            slControls[1] = ddlFeedbackRatingType;
            slControls[2] = ddlFeedbackRating;
            slControls[3] = ddlFeedbackDesc;
            slControls[4] = txtComments;


            ErrorCollection.Clear();
            string strCtrlTypes = "DropDownList~DropDownList~DropDownList~DropDownList~TextBox";
            string strMessage = "Type ~ RatingType ~Rating~ Desc~ Comments";
            ErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);
            if (ErrorCollection.Count > 0)
                return drList;

            if (ddlFeedbacktype.SelectedItem != null)
            {
                drList["feedback_id_type"] = ddlFeedbacktype.SelectedItem.Value;
                drList["feedback_type"] = ddlFeedbacktype.SelectedItem.Text;
            }

            if (ddlFeedbackRatingType.SelectedItem != null)
            {
                drList["feedback_id_rating"] = ddlFeedbackRatingType.SelectedItem.Value;
                drList["feedback_rating_type"] = ddlFeedbackRatingType.SelectedItem.Text;
            }
            if (ddlFeedbackRating.SelectedItem != null)
            {
                drList["rating_code_id"] = ddlFeedbackRating.SelectedItem.Value;
                drList["rating_code_name"] = ddlFeedbackRating.SelectedItem.Text;
            }
            if (ddlFeedbackDesc.SelectedItem != null)
            {
                drList["feedback_id_desc"] = ddlFeedbackDesc.SelectedItem.Value;
                drList["feedback_desc"] = ddlFeedbackDesc.SelectedItem.Text;
            }

            drList["emp_comments"] = txtComments.Text.ToString();
            if (chkact.Checked)
            {
                drList["enabled_flag"] = "TRUE";
            }
            else
            {
                drList["enabled_flag"] = "FALSE";
            }
            drList[FINColumnConstants.DELETED] = FIN.BLL.FINAppConstants.N;
            return drList;
        }


        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//  ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }

                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("FeedbackEntry_ROW_UP", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// Used to delete the exam master and detail table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {

            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                for (int jLoop = 0; jLoop < dtGridData.Rows.Count; jLoop++)
                {
                    DBMethod.DeleteEntity<HR_EMP_FEEDBACK_DTL>(hR_EMP_FEEDBACK_DTL);
                }

                DBMethod.DeleteEntity<HR_EMP_FEEDBACK_HDR>(hR_EMP_FEEDBACK_HDR);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("FeedbackEntryYES", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//  ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + iAcademeMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void ddlFeedbackTempName_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow gvr = gvData.FooterRow;
            DropDownList ddlFeedbacktype = gvr.FindControl("ddlFeedbacktyp") as DropDownList;
            DropDownList ddlFeedbackDesc = gvr.FindControl("ddlFeedbackDesc") as DropDownList;
            DropDownList ddlFeedbackRatingType = gvr.FindControl("ddlFeedbackRatingType") as DropDownList;

            if (ddlFeedbackTempName.SelectedValue.ToString().Length > 0)
            {
                FIN.BLL.HR.FeedbackTemplateBLL.fn_GetFeedbackTypes(ref ddlFeedbacktype, ddlFeedbackTempName.SelectedValue.ToString());
                FIN.BLL.HR.FeedbackTemplateBLL.fn_GetFeedbackDesc(ref ddlFeedbackDesc, ddlFeedbackTempName.SelectedValue.ToString());
                FIN.BLL.HR.FeedbackTemplateBLL.fn_GetFeedbackRating(ref ddlFeedbackRatingType, ddlFeedbackTempName.SelectedValue.ToString());
            }

        }

        protected void ddlFeedbackRatingType_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            DropDownList ddlFeedbackRatingType = gvr.FindControl("ddlFeedbackRatingType") as DropDownList;
            DropDownList ddlFeedbackRating = gvr.FindControl("ddlFeedbackRating") as DropDownList;

            if (ddlFeedbackRatingType.SelectedItem.Text.ToString().ToUpper() == "Y/N")
            {
                Lookup_BLL.GetLookUpValues(ref ddlFeedbackRating, "FEEDBACK_RATING");
            }
            else if (ddlFeedbackRatingType.SelectedItem.Text.ToString().ToUpper() == "NUMBERS")
            {
                Lookup_BLL.GetLookUpValues(ref ddlFeedbackRating, "FEEDBACK_RATING_N");
            }

        }



    }
}