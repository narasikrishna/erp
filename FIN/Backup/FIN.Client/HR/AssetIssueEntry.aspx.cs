﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.DAL.GL;
using FIN.BLL;
using FIN.BLL.GL;
using FIN.BLL.HR;
using VMVServices.Web;

namespace FIN.Client.HR
{
    public partial class AssetIssueEntry : PageBase
    {
        HR_ASSET_ISSUE_HDR hR_ASSET_ISSUE_HDR = new HR_ASSET_ISSUE_HDR();
        HR_ASSET_ISSUE_DTL hR_ASSET_ISSUE_DTL = new HR_ASSET_ISSUE_DTL();
        DataTable dtData = new DataTable();
        DataTable Chk_duplicate = new DataTable();
        Boolean bol_rowVisiable;
        Boolean savedBool;
        DataTable dtGridData = new DataTable();

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));


            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
        }

        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        private void FillComboBox()
        {
            //AccountingCalendar_BLL.GetFinancialYear(ref ddlFinancialYear);
            AssetIssue_BLL.GetDepartmentName(ref ddlDept);
            //  Employee_BLL.GetEmployeeName(ref ddlStaffName);
        }


        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();

                EntityData = null;
                Session[FINSessionConstants.GridData] = null;

                FillComboBox();

                dtGridData = DBMethod.ExecuteQuery(AssetIssue_DAL.GetAssetIssueDtls(Master.StrRecordId)).Tables[0];
                BindGrid(dtGridData);

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    hR_ASSET_ISSUE_HDR = AssetIssue_BLL.getClassEntity(Master.StrRecordId);
                    EntityData = hR_ASSET_ISSUE_HDR;

                    ddlDept.SelectedValue = hR_ASSET_ISSUE_HDR.EMP_DEPT_ID.ToString();
                    fn_fill_Designation();
                    ddlDesignation.SelectedValue = hR_ASSET_ISSUE_HDR.EMP_DESIG_ID.ToString();
                    fn_fill_Employee();
                    ddlEmployee.SelectedValue = hR_ASSET_ISSUE_HDR.EMP_ID.ToString();

                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Vaca_Entry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);

                }
            }

        }

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr["ENABLED_FLAG"] = "FALSE";
                    dr["ASSET_RETURNABLE"] = "FALSE";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddlAssetName = tmpgvr.FindControl("ddlAssetName") as DropDownList;
                DropDownList ddlAssetType = tmpgvr.FindControl("ddlAssetType") as DropDownList;

                //AssetIssue_BLL.GetAssetName(ref ddlAssetName);
                Lookup_BLL.GetLookUpValues(ref ddlAssetType, "AST_TYP");

                Lookup_BLL.GetLookUpValues(ref ddlAssetName, "ASSET_DETAIL");

                if (gvData.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                {
                    ddlAssetName.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["ASSET_ID"].ToString();
                    ddlAssetType.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["ASSET_TYPE_ID"].ToString();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RLD_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion
        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                System.Collections.SortedList slControls = new System.Collections.SortedList();

                slControls[0] = ddlDept;
                slControls[1] = ddlDesignation;
                slControls[2] = ddlEmployee;
                //slControls[3] = txtAttendanceDate;
                Dictionary<string, string> Prop_File_Data;
                Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));
                string strCtrlTypes = FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST;
                string strMessage = Prop_File_Data["Department_P"] + " ~ " + Prop_File_Data["Designation_P"] + " ~ " + Prop_File_Data["Employee_P"] + "";
                //string strMessage = "Department ~ Designation~Employee";

                EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

                if (EmptyErrorCollection.Count > 0)
                {
                    ErrorCollection = EmptyErrorCollection;
                    return;
                }

                ErrorCollection.Clear();
                AssignToBE();
                ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session[FINSessionConstants.GridData], "Asset Issue");

                if (ErrorCollection.Count > 0)
                {
                    return;
                }





                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    hR_ASSET_ISSUE_HDR = (HR_ASSET_ISSUE_HDR)EntityData;
                }

                hR_ASSET_ISSUE_HDR.EMP_DEPT_ID = ddlDept.SelectedValue.ToString();
                hR_ASSET_ISSUE_HDR.EMP_DESIG_ID = ddlDesignation.SelectedValue.ToString();
                hR_ASSET_ISSUE_HDR.EMP_ID = ddlEmployee.SelectedValue.ToString();


                hR_ASSET_ISSUE_HDR.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                hR_ASSET_ISSUE_HDR.EMP_ORG_ID = VMVServices.Web.Utils.OrganizationID;
                // hR_TRM_COURSE.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;


                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    hR_ASSET_ISSUE_HDR.MODIFIED_BY = this.LoggedUserName;
                    hR_ASSET_ISSUE_HDR.MODIFIED_DATE = DateTime.Today;

                }
                else
                {
                    hR_ASSET_ISSUE_HDR.ASSET_HDR_ID = FINSP.GetSPFOR_SEQCode("HR_070_M".ToString(), false, true);
                    //hR_TRM_COURSE.PK_ID = DBMethod.GetPrimaryKeyValue(FINSequenceConstant.hR_TRM_COURSE_SEQ);
                    hR_ASSET_ISSUE_HDR.CREATED_BY = this.LoggedUserName;
                    hR_ASSET_ISSUE_HDR.CREATED_DATE = DateTime.Today;

                }
                hR_ASSET_ISSUE_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_ASSET_ISSUE_HDR.ASSET_HDR_ID);

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }


                var tmpChildEntity = new List<Tuple<object, string>>();

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    hR_ASSET_ISSUE_DTL = new HR_ASSET_ISSUE_DTL();
                    if (dtGridData.Rows[iLoop]["ASSET_DTL_ID"].ToString() != "0" && dtGridData.Rows[iLoop]["ASSET_DTL_ID"].ToString() != string.Empty)
                    {
                        using (IRepository<HR_ASSET_ISSUE_DTL> userCtx = new DataRepository<HR_ASSET_ISSUE_DTL>())
                        {
                            hR_ASSET_ISSUE_DTL = userCtx.Find(r =>
                                (r.ASSET_DTL_ID == dtGridData.Rows[iLoop]["ASSET_DTL_ID"].ToString())
                                ).SingleOrDefault();
                        }
                    }

                    //hR_TRM_SUBJECT.COM_LINE_NUM = (iLoop + 1);
                    hR_ASSET_ISSUE_DTL.ASET_LINE_NO = (iLoop + 1);
                    hR_ASSET_ISSUE_DTL.ASET_NUM = dtGridData.Rows[iLoop]["ASET_NUM"].ToString();
                    hR_ASSET_ISSUE_DTL.ASSET_ID = dtGridData.Rows[iLoop]["ASSET_ID"].ToString();
                    hR_ASSET_ISSUE_DTL.ASSET_TYPE = dtGridData.Rows[iLoop]["ASSET_TYPE_ID"].ToString();
                    hR_ASSET_ISSUE_DTL.ASSET_HDR_ID = hR_ASSET_ISSUE_HDR.ASSET_HDR_ID;

                    hR_ASSET_ISSUE_DTL.WORKFLOW_COMPLETION_STATUS = "1";
                    if (dtGridData.Rows[iLoop]["ASSET_ISSUE_DATE"].ToString() != string.Empty)
                    {
                        hR_ASSET_ISSUE_DTL.ASSET_ISSUE_DATE = DateTime.Parse(dtGridData.Rows[iLoop]["ASSET_ISSUE_DATE"].ToString());
                    }
                    if (dtGridData.Rows[iLoop]["ASSET_RETURN_DATE"].ToString() != string.Empty)
                    {
                        hR_ASSET_ISSUE_DTL.ASSET_RETURN_DATE = DateTime.Parse(dtGridData.Rows[iLoop]["ASSET_RETURN_DATE"].ToString());
                    }
                    if (dtGridData.Rows[iLoop]["ASSET_RETURNABLE"].ToString().ToUpper() == "TRUE")
                    {
                        hR_ASSET_ISSUE_DTL.ASSET_RETURNABLE = FINAppConstants.Y;
                    }
                    else
                    {
                        hR_ASSET_ISSUE_DTL.ASSET_RETURNABLE = FINAppConstants.N;
                    }
                    if (dtGridData.Rows[iLoop]["ENABLED_FLAG"].ToString().ToUpper() == "TRUE")
                    {
                        hR_ASSET_ISSUE_DTL.ENABLED_FLAG = "1";
                    }
                    else
                    {
                        hR_ASSET_ISSUE_DTL.ENABLED_FLAG = "0";
                    }

                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {

                        tmpChildEntity.Add(new Tuple<object, string>(hR_ASSET_ISSUE_DTL, "D"));
                    }
                    else
                    {

                        // Duplicate Validation Through Backend Package PKG_VALIDATIONS

                        //ProReturn = FIN.DAL.HR.Competency_DAL.GetSPFOR_DUPLICATE_CHECK(hR_TRM_SUBJECT.COM_LEVEL_DESC, hR_TRM_SUBJECT.COM_LINE_ID);

                        //if (ProReturn != string.Empty)
                        //{
                        //    if (ProReturn != "0")
                        //    {
                        //        ErrorCollection.Add("COMPETENCY", ProReturn);
                        //        if (ErrorCollection.Count > 0)
                        //        {
                        //            return;
                        //        }
                        //    }
                        //}

                        if (dtGridData.Rows[iLoop]["ASSET_DTL_ID"].ToString() != "0" && dtGridData.Rows[iLoop]["ASSET_DTL_ID"].ToString() != string.Empty)
                        {
                            hR_ASSET_ISSUE_DTL.ASSET_DTL_ID = dtGridData.Rows[iLoop]["ASSET_DTL_ID"].ToString();
                            hR_ASSET_ISSUE_DTL.MODIFIED_BY = this.LoggedUserName;
                            hR_ASSET_ISSUE_DTL.MODIFIED_DATE = DateTime.Today;

                            tmpChildEntity.Add(new Tuple<object, string>(hR_ASSET_ISSUE_DTL, "U"));

                        }
                        else
                        {

                            hR_ASSET_ISSUE_DTL.ASSET_DTL_ID = FINSP.GetSPFOR_SEQCode("HR_070_D".ToString(), false, true);
                            hR_ASSET_ISSUE_DTL.CREATED_BY = this.LoggedUserName;
                            hR_ASSET_ISSUE_DTL.CREATED_DATE = DateTime.Today;
                            //DBMethod.SaveEntity<GL_ACCT_CODE_SEGMENTS>(gL_ACCT_CODE_SEGMENTS);
                            tmpChildEntity.Add(new Tuple<object, string>(hR_ASSET_ISSUE_DTL, "A"));
                        }
                    }

                }

                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            FIN.BLL.HR.Competency_BLL.SavePCEntity<HR_ASSET_ISSUE_HDR, HR_ASSET_ISSUE_DTL>(hR_ASSET_ISSUE_HDR, tmpChildEntity, hR_ASSET_ISSUE_DTL);
                            savedBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {

                            FIN.BLL.HR.Competency_BLL.SavePCEntity<HR_ASSET_ISSUE_HDR, HR_ASSET_ISSUE_DTL>(hR_ASSET_ISSUE_HDR, tmpChildEntity, hR_ASSET_ISSUE_DTL, true);
                            savedBool = true;
                            break;

                        }
                }



            }
            catch (Exception ex)
            {
                ErrorCollection.Add("COurs_ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {

                    // HR_STAFF_ATTENDANCE.LSD_ID = (dtGridData.Rows[iLoop]["LSD_ID"].ToString());
                    DBMethod.DeleteEntity<HR_ASSET_ISSUE_DTL>(hR_ASSET_ISSUE_DTL);
                }

                DBMethod.DeleteEntity<HR_ASSET_ISSUE_HDR>(hR_ASSET_ISSUE_HDR);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion

        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            if (Session[FINSessionConstants.GridData] != null)
            {
                dtGridData = (DataTable)Session[FINSessionConstants.GridData];
            }
            gvData.EditIndex = -1;

            BindGrid(dtGridData);

        }

        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("COM_ROW_CMD", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {


            System.Collections.SortedList slControls = new System.Collections.SortedList();


            DropDownList ddlAssetName = gvr.FindControl("ddlAssetName") as DropDownList;
            DropDownList ddlAssetType = gvr.FindControl("ddlAssetType") as DropDownList;
            TextBox dtpIssueDate = gvr.FindControl("dtpIssueDate") as TextBox;
            TextBox txtSNo = gvr.FindControl("txtSNo") as TextBox;
            TextBox txtAssetNumber = gvr.FindControl("txtAssetNumber") as TextBox;
            TextBox dtpReturnDate = gvr.FindControl("dtpReturnDate") as TextBox;
            CheckBox chkRet = gvr.FindControl("chkRet") as CheckBox;
            CheckBox chkActive = gvr.FindControl("chkActive") as CheckBox;

            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList["ASSET_DTL_ID"] = "0";
                //txtlineno.Text = (tmpdtGridData.Rows.Count + 1).ToString();
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }

            txtSNo.Text = (rowindex + 1).ToString();


            slControls[0] = txtAssetNumber;
            slControls[1] = ddlAssetName;
            slControls[2] = ddlAssetType;
            slControls[3] = dtpIssueDate;
            slControls[4] = dtpIssueDate;
            slControls[5] = dtpReturnDate;

            ErrorCollection.Clear();
            //string strCtrlTypes = "Dropdownlist~Dropdownlist";

            //string strMessage = " Asset Name ~ Asset Type";


            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));
            ErrorCollection.Clear();

            string strCtrlTypes = FINAppConstants.TEXT_BOX + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.DATE_TIME + "~" + FINAppConstants.DATE_TIME;
            string strMessage = Prop_File_Data["Asset_Number_P"] + " ~ " + Prop_File_Data["Asset_Name_P"] + " ~ " + Prop_File_Data["Asset_Type_P"] + " ~ " + Prop_File_Data["Issue_Date_P"] + " ~ " + Prop_File_Data["Issue_Date_P"] + " ~ " + Prop_File_Data["Return_Date_P"] + "";


            EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

            if (EmptyErrorCollection.Count > 0)
            {
                ErrorCollection = EmptyErrorCollection;
                return drList;
            }


            //string strCondition = "LOT_ID='" + ddlLotNo.SelectedValue + "'";
            //strMessage = FINMessageConstatns.RecordAlreadyExists;
            //ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            //if (ErrorCollection.Count > 0)
            //{

            //    return drList;
            //}

            drList["ASET_LINE_NO"] = txtSNo.Text;
            drList["ASSET_ID"] = ddlAssetName.SelectedValue;
            drList["ASSET_NAME"] = ddlAssetName.SelectedItem.Text;
            drList["ASSET_TYPE_ID"] = ddlAssetType.SelectedValue;
            drList["ASSET_TYPE"] = ddlAssetType.SelectedItem.Text;



            drList["ASET_NUM"] = txtAssetNumber.Text;
            if (dtpIssueDate.Text.Count() > 0)
            {
                drList["ASSET_ISSUE_DATE"] = DBMethod.ConvertStringToDate(dtpIssueDate.Text);
            }
            if (dtpReturnDate.Text.Count() > 0)
            {
                drList["ASSET_RETURN_DATE"] = DBMethod.ConvertStringToDate(dtpReturnDate.Text);
            }
            if (chkActive.Checked)
            {
                drList[FINColumnConstants.ENABLED_FLAG] = "TRUE";
            }
            else
            {
                drList[FINColumnConstants.ENABLED_FLAG] = "FALSE";
            }

            if (chkRet.Checked)
            {
                drList[FINColumnConstants.ASSET_RETURNABLE] = "TRUE";
            }
            else
            {
                drList[FINColumnConstants.ASSET_RETURNABLE] = "FALSE";
            }



            drList[FINColumnConstants.DELETED] = FINAppConstants.N;


            return drList;

        }



        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("COM_ROW_UP", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("COM_ROW_DEL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("COM_ROW_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("COM_RowCreated", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("COM_RDB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        #endregion



        protected void ddlDept_SelectedIndexChanged(object sender, EventArgs e)
        {
            fn_fill_Designation();
        }

        private void fn_fill_Designation()
        {
            AssetIssue_BLL.GetDesignationName(ref ddlDesignation, ddlDept.SelectedValue.ToString());
        }

        protected void ddlDesignation_SelectedIndexChanged(object sender, EventArgs e)
        {
            fn_fill_Employee();
        }

        private void fn_fill_Employee()
        {
            AssetIssue_BLL.GetEmployeeName(ref ddlEmployee, ddlDept.SelectedValue.ToString(), ddlDesignation.SelectedValue.ToString());
        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {

        }

        protected void ddlEmployee_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                Chk_duplicate = DBMethod.ExecuteQuery(AssetIssue_DAL.CheckDuplicateData(ddlDept.SelectedValue.ToString(), ddlDesignation.SelectedValue.ToString(), ddlEmployee.SelectedValue.ToString())).Tables[0];

                if (Chk_duplicate != null)
                {
                    if (int.Parse(Chk_duplicate.Rows[0][0].ToString()) > 0)
                    {
                        ErrorCollection.Add("Invalid Add", "Asset has already been issued for Employee, Please edit the data ");
                        ddlEmployee.SelectedIndex = 0;
                    }
                }
                if (ErrorCollection.Count > 0)
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


    }
}