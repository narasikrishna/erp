﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.BLL.AP;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.BLL;
using VMVServices.Web;

namespace FIN.Client.HR
{
    public partial class EmployeeRequestEntry : PageBase
    {
        HR_EMP_REQUESTS hR_EMP_REQUESTS = new HR_EMP_REQUESTS();
        System.Collections.SortedList slControls = new System.Collections.SortedList();
        //string ProReturn = null;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();
                   

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                //pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                //pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";

            }

            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
            }

            if (VMVServices.Web.Utils.AddAllowed != FINAppConstants.Y && Master.Mode == FINAppConstants.Add)
            {
                btnSave.Visible = false;
            }

        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                //ddlWeightUOM.Enabled = false;
                //ddlLengthUOM.Enabled = false;
                //ddlAreaUOM.Enabled = false;
                //ddlVolumeUOM.Enabled = false;


                Startup();
                FillComboBox();

                if (Request.QueryString.ToString().Contains("ReqId"))
                {
                    Master.StrRecordId = Request.QueryString["ReqId"].ToString();
                    Master.Mode = FINAppConstants.Update;
                    FillDetails();
                    return;
                }

                EntityData = null;
                btnReqAssignment.Visible = false;
                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    btnReqAssignment.Visible = true;
                    using (IRepository<HR_EMP_REQUESTS> userCtx = new DataRepository<HR_EMP_REQUESTS>())
                    {
                        hR_EMP_REQUESTS = userCtx.Find(r =>
                            (r.REQ_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }
                    //btnReqAssignment.Visible = true;
                    EntityData = hR_EMP_REQUESTS;
                    ddlDepartment.SelectedValue = hR_EMP_REQUESTS.REQ_DEPT;
                    FillDesignation();
                    ddlDesignation.SelectedValue =  hR_EMP_REQUESTS.REQ_DESIG  ;
                    FillEmployeeName();
                    ddlEmployeeName.SelectedValue = hR_EMP_REQUESTS.REQ_EMP_ID ;
                    ddlRequestType.SelectedValue  =  hR_EMP_REQUESTS.REQ_TYPE  ;
                    ddlStatus.SelectedValue = hR_EMP_REQUESTS.REQ_STATUS  ;
                    txtDescription.Text = hR_EMP_REQUESTS.REQ_DESC  ;
                    txtHRComments.Text = hR_EMP_REQUESTS.REQ_HR_COMMENTS  ;
                    ChkEnabledFlag.Checked = hR_EMP_REQUESTS.ENABLED_FLAG == FINAppConstants.EnabledFlag ? true : false;
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        private void FillDetails()
        {
            DataTable dtbind = new DataTable();
            dtbind = DBMethod.ExecuteQuery(FIN.DAL.HR.TrainingRequest_DAL.GetRequestPage(Master.StrRecordId)).Tables[0];
            if (dtbind.Rows.Count > 0)
            {
                ddlDepartment.SelectedValue = dtbind.Rows[0]["REQ_DEPT"].ToString();
                FillDesignation();
                ddlDesignation.SelectedValue = dtbind.Rows[0]["REQ_DESIG"].ToString();
                FillEmployeeName();
                ddlEmployeeName.SelectedValue = dtbind.Rows[0]["REQ_EMP_ID"].ToString();
                ddlRequestType.SelectedValue = dtbind.Rows[0]["REQ_TYPE"].ToString();
                ddlStatus.SelectedValue = dtbind.Rows[0]["REQ_STATUS"].ToString();
                txtDescription.Text = dtbind.Rows[0]["REQ_DESC"].ToString();
                if (dtbind.Rows[0]["REQ_HR_COMMENTS"].ToString() != null)
                {
                    txtHRComments.Text = dtbind.Rows[0]["REQ_HR_COMMENTS"].ToString();
                }
                if (dtbind.Rows[0]["ENABLED_FLAG"].ToString() == "Y")
                {
                    ChkEnabledFlag.Checked = true;
                }
                else
                {
                    ChkEnabledFlag.Checked = false;
                }
            }
        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>

        private void FillComboBox()
        {
            ComboFilling.fn_getDepartment(ref ddlDepartment);
            Lookup_BLL.GetLookUpValues(ref ddlRequestType, "REQUEST_TYPE");
            Lookup_BLL.GetLookUpValues(ref ddlStatus, "REQUEST_STATUS");
            ddlStatus.SelectedValue = "INITIATED";

        }

        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null) 
                {
                    hR_EMP_REQUESTS = (HR_EMP_REQUESTS)EntityData;
                }
                
                hR_EMP_REQUESTS.REQ_DEPT = ddlDepartment.SelectedValue.ToString();
                hR_EMP_REQUESTS.REQ_DESIG = ddlDesignation.SelectedValue.ToString();
                hR_EMP_REQUESTS.REQ_EMP_ID = ddlEmployeeName.SelectedValue.ToString();
                hR_EMP_REQUESTS.REQ_TYPE = ddlRequestType.SelectedValue.ToString();
                hR_EMP_REQUESTS.REQ_STATUS = ddlStatus.SelectedValue.ToString();
                hR_EMP_REQUESTS.REQ_DESC = txtDescription.Text;
                hR_EMP_REQUESTS.REQ_HR_COMMENTS = txtHRComments.Text;
                hR_EMP_REQUESTS.ENABLED_FLAG = FINAppConstants.Y;
                hR_EMP_REQUESTS.ENABLED_FLAG = ChkEnabledFlag.Checked == true ? FINAppConstants.EnabledFlag : FINAppConstants.DisabledFlag;
                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    hR_EMP_REQUESTS.MODIFIED_BY = this.LoggedUserName;
                    hR_EMP_REQUESTS.MODIFIED_DATE = DateTime.Today;

                }
                else
                {
                    hR_EMP_REQUESTS.REQ_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_078.ToString(), false, true);
                    //iNV_ITEM_MASTER.WORKFLOW_COMPLETION_STATUS = "1";
                    hR_EMP_REQUESTS.CREATED_BY = this.LoggedUserName;
                    hR_EMP_REQUESTS.CREATED_DATE = DateTime.Today;

                }

                hR_EMP_REQUESTS.REQ_ORG_ID = VMVServices.Web.Utils.OrganizationID;
                hR_EMP_REQUESTS.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_EMP_REQUESTS.REQ_ID);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                AssignToBE();


                //emptyvalid();
                if (ErrorCollection.Count > 0)
                {
                    return;
                }


                // Duplicate Validation Through Backend Package PKG_VALIDATIONS



                //ProReturn = FIN.DAL.AP.Item_DAL.GetSPFOR_DUPLICATE_CHECK(iNV_ITEM_MASTER.ITEM_CODE,txtItemName.Text, iNV_ITEM_MASTER.ORG_ID, iNV_ITEM_MASTER.ITEM_ID);

                //if (ProReturn != string.Empty)
                //{
                //    if (ProReturn != "0")
                //    {
                //        ErrorCollection.Add("ITEMNAME", ProReturn);
                //        if (ErrorCollection.Count > 0)
                //        {
                //            return;
                //        }
                //    }
                //}


                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            DBMethod.SaveEntity<HR_EMP_REQUESTS>(hR_EMP_REQUESTS);
                            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                            break;
                        }
                    case FINAppConstants.Update:
                        {

                            DBMethod.SaveEntity<HR_EMP_REQUESTS>(hR_EMP_REQUESTS, true);
                            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                            break;

                        }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion




        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                DBMethod.DeleteEntity<HR_EMP_REQUESTS>(hR_EMP_REQUESTS);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillDesignation();
            FillEmployeeName();


        }
        private void FillDesignation()
        {
            ComboFilling.fn_getDesignation(ref ddlDesignation, ddlDepartment.SelectedValue.ToString());
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {

        }

        protected void ddlDesignation_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillEmployeeName();
        }
        private void FillEmployeeName()
        {
            FIN.BLL.HR.Employee_BLL.GetEmplDet(ref ddlEmployeeName, ddlDepartment.SelectedValue.ToString(), ddlDesignation.SelectedValue.ToString());
        }

        protected void btnReqAssignment_Click(object sender, EventArgs e)
        {
            Response.Redirect("RequestAssignmentEntry.aspx?ProgramID=259&Mode=A&ID=0&AddFlag=1&UpdateFlag=1&DeleteFlag=1&QueryFlag=1&ReportName=Reports&ReportName_OL=&ReqId="+ Master.StrRecordId);
        }
        //protected void ddlInternalExternal_SelectedIndexChanged(object sender, EventArgs e)
        //{

        //    showagency();

        //}
        //private void showagency()
        //{
        //    Agency1.Visible = false;
        //    Agency2.Visible = false;
        //    if (ddlInternalExternal.SelectedItem.Text.ToString().ToUpper() != "INTERNAL")
        //    {
        //        Agency1.Visible = true;
        //        Agency2.Visible = true;
        //    }


        //}





        //private void emptyvalid()
        //{
        //    if (hR_VACANCIES.VAC_DEPT_ID == null)
        //    {

        //        if (ddlDepartment.SelectedValue == string.Empty)
        //        {
        //            ErrorCollection.Add("Department", "Department Cannot be empty");
        //        }

        //    }
        //    if (hR_VACANCIES.VAC_DESIG_ID == null)
        //    {
        //        if (ddlDesignation.SelectedValue == string.Empty)

        //            ErrorCollection.Add("Designation", "Designation Cannot be empty");
        //        }
        //    }
        //    //if (hR_VACANCIES.VAC_TYPE > 0)
        //    //{

        //    //    if (ddlType.SelectedValue == string.Empty)
        //    //    {
        //    //        ErrorCollection.Add("DepartmentType", "Department Type Cannot be empty");
        //    //    }
        //    //}

        //    //if  > 0)
        //    //{
        //    //    if (ddlVolumeUOM.SelectedValue == string.Empty)
        //    //    {

        //    //        ErrorCollection.Add("VOLEMT", "Volume UOM Cannot be empty");
        //    //    }
        //    //}

        //}






    }





}
