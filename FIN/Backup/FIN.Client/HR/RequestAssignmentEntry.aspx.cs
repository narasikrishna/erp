﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.BLL.AP;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.BLL;
using VMVServices.Web;


namespace FIN.Client.HR
{
    public partial class RequestAssignmentEntry : PageBase
    {
        HR_EMP_REQUESTS_ASSIGNMENT hR_EMP_REQUESTS_ASSIGNMENT = new HR_EMP_REQUESTS_ASSIGNMENT();
        System.Collections.SortedList slControls = new System.Collections.SortedList();
        //string ProReturn = null;
        public String record_id;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                //pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                //pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";

            }

            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
            }

            if (VMVServices.Web.Utils.AddAllowed != FINAppConstants.Y && Master.Mode == FINAppConstants.Add)
            {
                btnSave.Visible = false;
            }

        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                //ddlWeightUOM.Enabled = false;
                //ddlLengthUOM.Enabled = false;
                //ddlAreaUOM.Enabled = false;
                //ddlVolumeUOM.Enabled = false;


                Startup();
                FillComboBox();

                if (Request.QueryString.ToString().Contains("ReqId"))
                {
                    
                    record_id = Request.QueryString["ReqId"].ToString();
                    ddlRequest.SelectedValue = record_id;
                    FillRequestDescription();
                    btnReqV.Visible = true;
                }

                
                EntityData = null;
                txtAssignedDate.Text = DBMethod.ConvertDateToString(DateTime.Now.ToShortDateString());
                txtAssignedDate.Enabled = false;
                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    using (IRepository<HR_EMP_REQUESTS_ASSIGNMENT> userCtx = new DataRepository<HR_EMP_REQUESTS_ASSIGNMENT>())
                    {
                        hR_EMP_REQUESTS_ASSIGNMENT = userCtx.Find(r =>
                            (r.REQ_ASSIGN_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = hR_EMP_REQUESTS_ASSIGNMENT;
                    ddlDepartment.SelectedValue = hR_EMP_REQUESTS_ASSIGNMENT.REQ_ASSIGN_DEPT_ID;
                    FillDesignation();
                    if (hR_EMP_REQUESTS_ASSIGNMENT.REQ_ASSIGN_DATE != null)
                    {
                        txtAssignedDate.Text = DBMethod.ConvertDateToString(hR_EMP_REQUESTS_ASSIGNMENT.REQ_ASSIGN_DATE.ToString());
                    }
                    ddlDesignation.SelectedValue = hR_EMP_REQUESTS_ASSIGNMENT.REQ_ASSIGN_DESIG_ID;
                    FillEmployeeName();
                    if (hR_EMP_REQUESTS_ASSIGNMENT.REQ_ASSIGN_COMPLETE_DATE != null)
                    {
                        txtCompletionDate.Text = DBMethod.ConvertDateToString(hR_EMP_REQUESTS_ASSIGNMENT.REQ_ASSIGN_COMPLETE_DATE.ToString());
                    }
                    ddlEmployeeName.SelectedValue = hR_EMP_REQUESTS_ASSIGNMENT.REQ_ASSIGN_EMP_ID;
                    txtHRRemarks.Text = hR_EMP_REQUESTS_ASSIGNMENT.REQ_ASSIGN_REMARKS;
                    ddlRequest.SelectedValue = hR_EMP_REQUESTS_ASSIGNMENT.REQ_ID;
                    FillRequestDescription();
                    ChkActive.Checked = hR_EMP_REQUESTS_ASSIGNMENT.ENABLED_FLAG == FINAppConstants.EnabledFlag ? true : false;
                    
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>

        private void FillComboBox()
        {
            ComboFilling.fn_getDepartment(ref ddlDepartment);
            ComboFilling.fn_getRequestDeptNReqType(ref ddlRequest);


        }
        private void checkRequestAssignmentFound()
        {
            DataTable dt_reqAss = DBMethod.ExecuteQuery(FIN.DAL.HR.RequestAssignment_DAL.getRequest4ReqId(ddlRequest.SelectedValue.ToString())).Tables[0];
            if (dt_reqAss.Rows.Count > 0)
            {
                Master.Mode = FINAppConstants.Update;
                Master.StrRecordId = dt_reqAss.Rows[0]["req_assign_id"].ToString();

                using (IRepository<HR_EMP_REQUESTS_ASSIGNMENT> userCtx = new DataRepository<HR_EMP_REQUESTS_ASSIGNMENT>())
                {
                    hR_EMP_REQUESTS_ASSIGNMENT = userCtx.Find(r =>
                        (r.REQ_ASSIGN_ID == Master.StrRecordId)
                        ).SingleOrDefault();
                }

                EntityData = hR_EMP_REQUESTS_ASSIGNMENT;
                ddlDepartment.SelectedValue = hR_EMP_REQUESTS_ASSIGNMENT.REQ_ASSIGN_DEPT_ID;
                FillDesignation();
                if (hR_EMP_REQUESTS_ASSIGNMENT.REQ_ASSIGN_DATE != null)
                {
                    txtAssignedDate.Text = DBMethod.ConvertDateToString(hR_EMP_REQUESTS_ASSIGNMENT.REQ_ASSIGN_DATE.ToString());
                }
                ddlDesignation.SelectedValue = hR_EMP_REQUESTS_ASSIGNMENT.REQ_ASSIGN_DESIG_ID;
                FillEmployeeName();
                if (hR_EMP_REQUESTS_ASSIGNMENT.REQ_ASSIGN_COMPLETE_DATE != null)
                {
                    txtCompletionDate.Text = DBMethod.ConvertDateToString(hR_EMP_REQUESTS_ASSIGNMENT.REQ_ASSIGN_COMPLETE_DATE.ToString());
                }
                ddlEmployeeName.SelectedValue = hR_EMP_REQUESTS_ASSIGNMENT.REQ_ASSIGN_EMP_ID;
                txtHRRemarks.Text = hR_EMP_REQUESTS_ASSIGNMENT.REQ_ASSIGN_REMARKS;
            }

                
        }
        
        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    hR_EMP_REQUESTS_ASSIGNMENT = (HR_EMP_REQUESTS_ASSIGNMENT)EntityData;
                }

                hR_EMP_REQUESTS_ASSIGNMENT.REQ_ASSIGN_DEPT_ID = ddlDepartment.SelectedValue.ToString();
                if (txtAssignedDate.Text != string.Empty)
                {
                    hR_EMP_REQUESTS_ASSIGNMENT.REQ_ASSIGN_DATE = DBMethod.ConvertStringToDate(txtAssignedDate.Text.ToString());
                }

                hR_EMP_REQUESTS_ASSIGNMENT.REQ_ASSIGN_DESIG_ID = ddlDesignation.SelectedValue.ToString();
                if (txtCompletionDate.Text != string.Empty)
                {
                    hR_EMP_REQUESTS_ASSIGNMENT.REQ_ASSIGN_COMPLETE_DATE = DBMethod.ConvertStringToDate(txtCompletionDate.Text.ToString());
                }
                hR_EMP_REQUESTS_ASSIGNMENT.REQ_ASSIGN_EMP_ID = ddlEmployeeName.SelectedValue.ToString();
                hR_EMP_REQUESTS_ASSIGNMENT.REQ_ASSIGN_REMARKS = txtHRRemarks.Text;
                hR_EMP_REQUESTS_ASSIGNMENT.REQ_ID = ddlRequest.SelectedValue.ToString();
                hR_EMP_REQUESTS_ASSIGNMENT.ENABLED_FLAG = FINAppConstants.Y;
                hR_EMP_REQUESTS_ASSIGNMENT.ENABLED_FLAG = ChkActive.Checked == true ? FINAppConstants.EnabledFlag : FINAppConstants.DisabledFlag;
                hR_EMP_REQUESTS_ASSIGNMENT.REQ_ASSIGN_ORG_ID = VMVServices.Web.Utils.OrganizationID;
               
                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    hR_EMP_REQUESTS_ASSIGNMENT.MODIFIED_BY = this.LoggedUserName;
                    hR_EMP_REQUESTS_ASSIGNMENT.MODIFIED_DATE = DateTime.Today;

                }
                else
                {
                    hR_EMP_REQUESTS_ASSIGNMENT.REQ_ASSIGN_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_079.ToString(), false, true);
                    //iNV_ITEM_MASTER.WORKFLOW_COMPLETION_STATUS = "1";
                    hR_EMP_REQUESTS_ASSIGNMENT.CREATED_BY = this.LoggedUserName;
                    hR_EMP_REQUESTS_ASSIGNMENT.CREATED_DATE = DateTime.Today;

                }

                hR_EMP_REQUESTS_ASSIGNMENT.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_EMP_REQUESTS_ASSIGNMENT.REQ_ASSIGN_ID);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                AssignToBE();

                if (DBMethod.ConvertStringToDate(txtCompletionDate.Text.ToString()) < DBMethod.ConvertStringToDate(txtAssignedDate.Text.ToString()))
                {
                    ErrorCollection.Add("Daterange", "Completion Date Must be grater than Assigned Date");

                }

                //emptyvalid();
                if (ErrorCollection.Count > 0)
                {
                    return;
                }


                // Duplicate Validation Through Backend Package PKG_VALIDATIONS



                //ProReturn = FIN.DAL.AP.Item_DAL.GetSPFOR_DUPLICATE_CHECK(iNV_ITEM_MASTER.ITEM_CODE,txtItemName.Text, iNV_ITEM_MASTER.ORG_ID, iNV_ITEM_MASTER.ITEM_ID);

                //if (ProReturn != string.Empty)
                //{
                //    if (ProReturn != "0")
                //    {
                //        ErrorCollection.Add("ITEMNAME", ProReturn);
                //        if (ErrorCollection.Count > 0)
                //        {
                //            return;
                //        }
                //    }
                //}


                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            DBMethod.SaveEntity<HR_EMP_REQUESTS_ASSIGNMENT>(hR_EMP_REQUESTS_ASSIGNMENT);
                            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                            break;
                        }
                    case FINAppConstants.Update:
                        {

                            DBMethod.SaveEntity<HR_EMP_REQUESTS_ASSIGNMENT>(hR_EMP_REQUESTS_ASSIGNMENT, true);
                            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                            break;

                        }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion




        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                DBMethod.DeleteEntity<HR_EMP_REQUESTS_ASSIGNMENT>(hR_EMP_REQUESTS_ASSIGNMENT);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillDesignation();
            FillEmployeeName();


        }
        private void FillDesignation()
        {
            ComboFilling.fn_getDesignation(ref ddlDesignation, ddlDepartment.SelectedValue.ToString());
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {

        }

        protected void ddlDesignation_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillEmployeeName();
        }
        private void FillEmployeeName()
        {
            FIN.BLL.HR.Employee_BLL.GetEmplDet(ref ddlEmployeeName, ddlDepartment.SelectedValue.ToString(), ddlDesignation.SelectedValue.ToString());
            if (hf_ReqEmpId.Value.ToString().Length > 0)
            {
                ddlEmployeeName.Items.Remove(new ListItem(txtRequestedEmployee.Text, hf_ReqEmpId.Value.ToString()));
            }

        }

        protected void ddlRequest_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillRequestDescription();
        }
        private void FillRequestDescription()
        {
            DataTable dtReqDesc = new DataTable();
            dtReqDesc = DBMethod.ExecuteQuery(FIN.DAL.HR.TrainingRequest_DAL.GetRequestDescription(ddlRequest.SelectedValue)).Tables[0];
            txtRequestDescription.Text = dtReqDesc.Rows[0]["REQ_DESC"].ToString();
            txtRequestedEmployee.Text = dtReqDesc.Rows[0]["EMPLOYEE_NAME"].ToString();
            hf_ReqEmpId.Value = dtReqDesc.Rows[0]["EMP_ID"].ToString();
            checkRequestAssignmentFound();
        }

        protected void btnReqV_Click(object sender, EventArgs e)
        {
            Response.Redirect("EmployeeRequestEntry.aspx?ProgramId=258&Mode=A&ID=0&AddFlag=1&UpdateFlag=1&DeleteFlag=1&QueryFlag=1&ReportName=Reports&ReportName_OL=&ReqId="+ ddlRequest.SelectedValue);
        }
        //protected void ddlInternalExternal_SelectedIndexChanged(object sender, EventArgs e)
        //{

        //    showagency();

        //}
        //private void showagency()
        //{
        //    Agency1.Visible = false;
        //    Agency2.Visible = false;
        //    if (ddlInternalExternal.SelectedItem.Text.ToString().ToUpper() != "INTERNAL")
        //    {
        //        Agency1.Visible = true;
        //        Agency2.Visible = true;
        //    }


        //}





        //private void emptyvalid()
        //{
        //    if (hR_VACANCIES.VAC_DEPT_ID == null)
        //    {

        //        if (ddlDepartment.SelectedValue == string.Empty)
        //        {
        //            ErrorCollection.Add("Department", "Department Cannot be empty");
        //        }

        //    }
        //    if (hR_VACANCIES.VAC_DESIG_ID == null)
        //    {
        //        if (ddlDesignation.SelectedValue == string.Empty)

        //            ErrorCollection.Add("Designation", "Designation Cannot be empty");
        //        }
        //    }
        //    //if (hR_VACANCIES.VAC_TYPE > 0)
        //    //{

        //    //    if (ddlType.SelectedValue == string.Empty)
        //    //    {
        //    //        ErrorCollection.Add("DepartmentType", "Department Type Cannot be empty");
        //    //    }
        //    //}

        //    //if  > 0)
        //    //{
        //    //    if (ddlVolumeUOM.SelectedValue == string.Empty)
        //    //    {

        //    //        ErrorCollection.Add("VOLEMT", "Volume UOM Cannot be empty");
        //    //    }
        //    //}

        //}






    }





}
