﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.BLL;
using FIN.BLL.HR;
using VMVServices.Web;

namespace FIN.Client.HR
{
    public partial class TimeAttendanceGroupEntry : PageBase
    {

        TM_EMP_GROUP_LINKS tM_EMP_GROUP_LINKS = new TM_EMP_GROUP_LINKS();


        TimeAttendanceGroup_BLL TimeAttendanceGroup_BLL = new TimeAttendanceGroup_BLL();

        DataTable dtGridData = new DataTable();
        Boolean bol_rowVisiable;

        Boolean savedBool;
        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TAG_PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));


            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
        }

        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>
        /// 

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void FillComboBox()
        {
            TimeAttendanceGroup_BLL.fn_GetDepartmentDetails(ref ddlDepartment);


        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();


                Startup();
                FillComboBox();

                EntityData = null;

                dtGridData = FIN.BLL.HR.TimeAttendanceGroup_BLL.getChildEntityDet(Master.RecordID);
                BindGrid(dtGridData);

                if (Master.Mode != FINAppConstants.Add && Master.RecordID > 0)
                {

                    using (IRepository<TM_EMP_GROUP_LINKS> userCtx = new DataRepository<TM_EMP_GROUP_LINKS>())
                    {
                        tM_EMP_GROUP_LINKS = userCtx.Find(r =>
                            (r.PK_ID == Master.RecordID)
                            ).SingleOrDefault();
                    }

                    EntityData = tM_EMP_GROUP_LINKS;

                    ddlDepartment.SelectedValue = tM_EMP_GROUP_LINKS.TM_DEPT_ID;
                    txtgrpcode.Text = tM_EMP_GROUP_LINKS.TM_GRP_CODE;
                    if (tM_EMP_GROUP_LINKS.TM_START_DATE != null)
                    {
                        txtfromdate.Text = DBMethod.ConvertDateToString(tM_EMP_GROUP_LINKS.TM_START_DATE.ToString());
                    }
                    if (tM_EMP_GROUP_LINKS.TM_END_DATE != null)
                    {
                        txtEndDate.Text = DBMethod.ConvertDateToString(tM_EMP_GROUP_LINKS.TM_END_DATE.ToString());
                    }


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TAG_ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                var tmpChildEntity = new List<Tuple<object, string>>();

                if (gvData.Rows.Count > 0)
                {
                    for (int iLoop = 0; iLoop < gvData.Rows.Count; iLoop++)
                    {


                        if (int.Parse(gvData.DataKeys[iLoop].Values["PK_ID"].ToString()) > 0)
                        {
                            using (IRepository<TM_EMP_GROUP_LINKS> userCtx = new DataRepository<TM_EMP_GROUP_LINKS>())
                            {
                                tM_EMP_GROUP_LINKS = userCtx.Find(r =>
                                    (r.PK_ID == int.Parse(gvData.DataKeys[iLoop].Values["PK_ID"].ToString()))
                                    ).SingleOrDefault();
                            }
                        }


                        CheckBox chkact = (CheckBox)gvData.Rows[iLoop].FindControl("chkact");


                        tM_EMP_GROUP_LINKS.TM_EMP_ID = gvData.DataKeys[iLoop].Values["EMP_ID"].ToString();
                        tM_EMP_GROUP_LINKS.TM_GRP_CODE = txtgrpcode.Text;
                        tM_EMP_GROUP_LINKS.TM_DEPT_ID = ddlDepartment.SelectedValue;
                        tM_EMP_GROUP_LINKS.TM_ORG_ID = VMVServices.Web.Utils.OrganizationID;


                        if (txtfromdate.Text != string.Empty)
                        {
                            tM_EMP_GROUP_LINKS.TM_START_DATE = DBMethod.ConvertStringToDate(txtfromdate.Text.ToString());
                        }
                        if (txtEndDate.Text.ToString().Length > 0)
                        {
                            tM_EMP_GROUP_LINKS.TM_END_DATE = DBMethod.ConvertStringToDate(txtEndDate.Text.ToString());
                        }
                        tM_EMP_GROUP_LINKS.WORKFLOW_COMPLETION_STATUS = FINAppConstants.Y;

                        if (chkact.Checked == true)
                        {
                            tM_EMP_GROUP_LINKS.ENABLED_FLAG = FINAppConstants.Y;
                        }
                        else
                        {
                            tM_EMP_GROUP_LINKS.ENABLED_FLAG = FINAppConstants.N;
                        }

                        if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                        {

                            // tmpChildEntity.Add(new Tuple<object, string>(tM_EMP_GROUP_LINKS, "D"));
                            if (dtGridData.Rows[iLoop]["PK_ID"].ToString() != "0")
                            {
                                DBMethod.DeleteEntity<TM_EMP_GROUP_LINKS>(tM_EMP_GROUP_LINKS);
                            }
                        }
                        else
                        {
                            if (dtGridData.Rows[iLoop]["PK_ID"].ToString() != "0")
                            {
                                if (chkact.Checked)
                                {
                                    tM_EMP_GROUP_LINKS.PK_ID = int.Parse(dtGridData.Rows[iLoop]["PK_ID"].ToString());
                                    tM_EMP_GROUP_LINKS.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, tM_EMP_GROUP_LINKS.PK_ID.ToString());

                                    tM_EMP_GROUP_LINKS.MODIFIED_BY = this.LoggedUserName;
                                    tM_EMP_GROUP_LINKS.MODIFIED_DATE = DateTime.Today;
                                    DBMethod.SaveEntity<TM_EMP_GROUP_LINKS>(tM_EMP_GROUP_LINKS, true);
                                    //tmpChildEntity.Add(new Tuple<object, string>(tM_EMP_GROUP_LINKS, "U"));
                                    savedBool = true;
                                }
                                else
                                {
                                    DBMethod.DeleteEntity<TM_EMP_GROUP_LINKS>(tM_EMP_GROUP_LINKS);
                                }

                            }
                            else
                            {
                                if (chkact.Checked)
                                {
                                    tM_EMP_GROUP_LINKS.PK_ID = DBMethod.GetPrimaryKeyValue(FINSequenceConstant.TM_EMP_GROUP_LINKS_SEQ);
                                    tM_EMP_GROUP_LINKS.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, tM_EMP_GROUP_LINKS.PK_ID.ToString());

                                    tM_EMP_GROUP_LINKS.CREATED_BY = this.LoggedUserName;
                                    tM_EMP_GROUP_LINKS.CREATED_DATE = DateTime.Today;
                                    DBMethod.SaveEntity<TM_EMP_GROUP_LINKS>(tM_EMP_GROUP_LINKS);
                                }
                                //tmpChildEntity.Add(new Tuple<object, string>(tM_EMP_GROUP_LINKS, "A"));
                                savedBool = true;
                            }
                        }

                    }


                }

                //switch (Master.Mode)
                //{
                //    case FINAppConstants.Add:
                //        {
                //            FIN.BLL.HR.CompetencyLinks_BLL.SavePCEntity<TM_EMP_GROUP_LINKS, HR_COMPETANCY_LINKS_DTL>(tM_EMP_GROUP_LINKS, tmpChildEntity, tM_EMP_GROUP_LINKS);
                //            break;
                //        }
                //    case FINAppConstants.Update:
                //        {

                //            FIN.BLL.HR.CompetencyLinks_BLL.SavePCEntity<TM_EMP_GROUP_LINKS, HR_COMPETANCY_LINKS_DTL>(tM_EMP_GROUP_LINKS, tmpChildEntity, tM_EMP_GROUP_LINKS, true);
                //            break;

                //        }
                //}



            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TAG_ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                //DropDownList ddlLotNo = tmpgvr.FindControl("ddlLotNo") as DropDownList;
                //WarehouseTransfer_BLL.fn_getLotNo(ref ddlLotNo);


                //if (gvData.EditIndex >= 0)
                //{
                //    ddlLotNo.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.LOT_ID].ToString();


                //}
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TAG_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                System.Collections.SortedList slControls = new System.Collections.SortedList();

                slControls[0] = txtgrpcode;
                slControls[1] = txtfromdate;
                slControls[2] = ddlDepartment;
                Dictionary<string, string> Prop_File_Data;
                Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));
                ErrorCollection.Clear();

                string strCtrlTypes = "TextBox~TextBox~" + FINAppConstants.DROP_DOWN_LIST;
                string strMessage = Prop_File_Data["Group_Code_P"] + " ~ " + Prop_File_Data["From_Date_P"] + " ~ " + Prop_File_Data["Department_P"] + "";
                //string strMessage = "Group Code ~From Date~Department";

                ErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session[FINSessionConstants.GridData], "Attendance Group ");
                if (ErrorCollection.Count > 0)
                {
                    return;
                }


                AssignToBE();


                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        #endregion

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr["ENABLED_FLAG"] = "FALSE";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TAG_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TAG_ROW_CMD", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {


            System.Collections.SortedList slControls = new System.Collections.SortedList();



            CheckBox chkact = gvr.FindControl("chkact") as CheckBox;

            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList["PK_ID"] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }




            if (chkact.Checked)
            {
                drList[FINColumnConstants.ENABLED_FLAG] = "TRUE";
            }
            else
            {
                drList[FINColumnConstants.ENABLED_FLAG] = "FALSE";
            }



            drList[FINColumnConstants.DELETED] = FINAppConstants.N;


            return drList;

        }






        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TAG_RDB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                // COURSE_GROUP.COURSE_GROUP_ID = short.Parse(Master.RecordID.ToString());
                DBMethod.DeleteEntity<TM_EMP_GROUP_LINKS>(tM_EMP_GROUP_LINKS);
                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATADELETED);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TAG_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }




        protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillEmployee();
        }

        private void FillEmployee()
        {
            dtGridData = DBMethod.ExecuteQuery(FIN.DAL.HR.TimeAttendanceGroup_DAL.GetEmployees(ddlDepartment.SelectedValue)).Tables[0];
            dtGridData.Columns.Add("DELETED");
            BindGrid(dtGridData);

        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {

        }



    }
}