﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="RepayPlanEntry.aspx.cs" Inherits="FIN.Client.HR.RepayPlanEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 800px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <%-- <div class="lblBox" style="float: left; width: 200px" id="lblEmployeeName">
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
            </div>--%>
            <div class="lblBox LNOrient" style=" width: 155px" id="Div1">
                Employee Name
            </div>
            <div class="divtxtBox  LNOrient" style="width: 448px">
                <asp:DropDownList ID="ddlEmployeeName" CssClass="validate[required] RequiredField ddlStype"
                    runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlEmployeeName_SelectedIndexChanged"
                    TabIndex="1">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 155px" id="Div2">
                Request Number
            </div>
            <div class="divtxtBox  LNOrient" style="width: 155px">
                <asp:DropDownList ID="ddlReqNo" CssClass="validate[required] RequiredField ddlStype"
                    runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlReqNo_SelectedIndexChanged"
                    TabIndex="2">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 100px" id="lblRequestDate">
                Request Date
            </div>
            <div class="divtxtBox  LNOrient" style="width: 155px">
                <asp:TextBox ID="txtRequestDate" CssClass="validate[required,custom[ReqDateDDMMYYY]] RequiredField  txtBox" runat="server" TabIndex="3"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtRequestDate"
                    OnClientDateSelectionChanged="checkDate" />
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtRequestDate" />
            </div>
        </div>
        <%--<div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="lblRequestNumber">
                Request Number
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:DropDownList ID="ddlRequestNumber" CssClass="validate[required] RequiredField txtBox"
                    runat="server" TabIndex="1" AutoPostBack="True" OnSelectedIndexChanged="ddlRequestNumber_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
           
        </div>--%>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 155px" id="lblDepartment">
                Department
            </div>
            <div class="divtxtBox  LNOrient" style="width: 450px">
                <asp:TextBox ID="txtDept" Enabled="false" CssClass="validate[required] RequiredField  txtBox" runat="server"
                 TabIndex="4"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 155px" id="lblDesignation">
                Designation
            </div>
            <div class="divtxtBox  LNOrient" style="width: 450px">
                <asp:TextBox ID="txtDesign" Enabled="false" CssClass="validate[] txtBox" runat="server"
                    TabIndex="5"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 155px" id="lblType">
                Type
            </div>
            <div class="divtxtBox  LNOrient" style="width: 155px">
                <asp:TextBox ID="txtype" CssClass="validate[required] RequiredField  txtBox" runat="server"
                    Enabled="true" TabIndex="6"></asp:TextBox>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 100px" id="lblAmount">
                Amount
            </div>
            <div class="divtxtBox  LNOrient" style="width: 155px">
                <asp:TextBox ID="txtAmount" CssClass="validate[] txtBox_N" runat="server" Enabled="true"
                    TabIndex="7" MaxLength="13"></asp:TextBox>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" ValidChars=".,"
                    FilterType="Numbers,Custom" TargetControlID="txtAmount" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="LNOrient">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
            OnRowDataBound="gvAdditional_RowDataBound"
                DataKeyNames="REPAY_ID,INSTALLMENT_NO" ShowFooter="False">
                <Columns>
                    <asp:TemplateField HeaderText="No">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtNo" TabIndex="8" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                Width="100px"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" ValidChars=""
                                FilterType="Numbers,Custom" TargetControlID="txtNo" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtNo" TabIndex="9" runat="server" CssClass="EntyFont RequiredField txtBox_N"
                                Width="100px"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" ValidChars=""
                                FilterType="Numbers,Custom" TargetControlID="txtNo" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblNo" runat="server" TabIndex="10" Text='<%# Eval("INSTALLMENT_NO") %>' Width="130px"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Repay Amount">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtRepayAmount" TabIndex="11" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                Width="100px"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" ValidChars=".,"
                                FilterType="Numbers,Custom" TargetControlID="txtRepayAmount" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtRepayAmount" TabIndex="12" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                Width="100px"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" ValidChars=".,"
                                FilterType="Numbers,Custom" TargetControlID="txtRepayAmount" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:TextBox ID="txtRepayAmount" TabIndex="13" runat="server" CssClass="EntryFont RequiredField txtBox_N"
                                Text='<%# Eval("REPAY_AMT") %>' Width="100px"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" ValidChars=".,"
                                FilterType="Numbers,Custom" TargetControlID="txtRepayAmount" />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Repay Date">
                        <EditItemTemplate>
                            <asp:TextBox ID="dtpRepayDate" TabIndex="14" runat="server" CssClass="EntryFont  txtBox"
                                Text='<%#  Eval("REPAY_DT","{0:dd/MM/yyyy}") %>' Onkeypress="return DateKeyCheck(event,this);"
                                Width="95%"></asp:TextBox>
                            <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender2" TargetControlID="dtpRepayDate" />
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars="/"
                                FilterType="Numbers,Custom" TargetControlID="dtpRepayDate" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="dtpRepayDate" TabIndex="15" runat="server" CssClass="EntryFont  txtBox"
                                Onkeypress="return DateKeyCheck(event,this);" Width="95%"></asp:TextBox>
                            <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender3" TargetControlID="dtpRepayDate" />
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" ValidChars="/"
                                FilterType="Numbers,Custom" TargetControlID="dtpRepayDate" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:TextBox ID="dtpRepayDate" TabIndex="16" runat="server" CssClass="EntryFont  txtBox"
                                Text='<%#  Eval("REPAY_DT","{0:dd/MM/yyyy}") %>' Onkeypress="return DateKeyCheck(event,this);"
                                Width="95%"></asp:TextBox>
                            <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender2" TargetControlID="dtpRepayDate" />
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars="/"
                                FilterType="Numbers,Custom" TargetControlID="dtpRepayDate" />
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Actual Date">
                        <EditItemTemplate>
                            <asp:TextBox ID="dtpActualDate" TabIndex="17" runat="server" CssClass="EntryFont  txtBox"
                                Text='<%#  Eval("REPAY_ACTUAL_DT","{0:dd/MM/yyyy}") %>' Width="95%"></asp:TextBox>
                            <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender4" TargetControlID="dtpActualDate" />
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" ValidChars="/"
                                FilterType="Numbers,Custom" TargetControlID="dtpActualDate" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="dtpActualDate" TabIndex="18" runat="server" CssClass="EntryFont  txtBox"
                                Width="95%"></asp:TextBox>
                            <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender5" TargetControlID="dtpActualDate" />
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" ValidChars="/"
                                FilterType="Numbers,Custom" TargetControlID="dtpActualDate" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:TextBox ID="dtpActualDate" TabIndex="19" runat="server" CssClass="EntryFont  txtBox"
                                Text='<%#  Eval("REPAY_ACTUAL_DT","{0:dd/MM/yyyy}") %>' Width="95%"></asp:TextBox>
                            <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender4" TargetControlID="dtpActualDate" />
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" ValidChars="/"
                                FilterType="Numbers,Custom" TargetControlID="dtpActualDate" />
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Paid">
                        <EditItemTemplate>
                            <asp:CheckBox ID="chkPaid" TabIndex="20" runat="server" Checked='<%# Convert.ToBoolean(Eval("REPAY_PAIDYN")) %>' />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:CheckBox ID="chkPaid" TabIndex="21" runat="server" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkPaid" TabIndex="22" runat="server" Enabled='<%# Convert.ToBoolean(Eval("REPAY_PAIDYN")) %>' Checked='<%# Convert.ToBoolean(Eval("REPAY_PAIDYN")) %>' />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Comments">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtComments" TabIndex="23" MaxLength="500" runat="server" CssClass="EntryFont txtBox"
                                Width="100px"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtComments" TabIndex="24" MaxLength="500" runat="server" CssClass="EntryFont txtBox"
                                Width="100px"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:TextBox ID="txtComments" TabIndex="25" MaxLength="500" runat="server" CssClass="EntryFont txtBox"
                                Width="100px"></asp:TextBox>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <%--<asp:TemplateField HeaderText="">
                            <ItemTemplate>
                                <asp:ImageButton ID="ibtnEdit" TabIndex="13" runat="server" AlternateText="Edit"
                                    CausesValidation="false" ToolTip="Edit" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                                <asp:ImageButton ID="ibtnDelete" TabIndex="14" runat="server" AlternateText="Delete"
                                    CausesValidation="false" ToolTip="Delete" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:ImageButton ID="ibtnUpdate" TabIndex="13" runat="server" AlternateText="Update"
                                    CommandName="Update" ToolTip="Update" Height="20px" ImageUrl="~/Images/Update.png" />
                                <asp:ImageButton ID="ibtnCancel" TabIndex="14" runat="server" AlternateText="Cancel"
                                    CausesValidation="false" ToolTip="Cancel" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:ImageButton ID="ibtnInsert" TabIndex="12" runat="server" AlternateText="Add"
                                    CommandName="FooterInsert" ToolTip="Add" ImageUrl="~/Images/Add.jpg" />
                            </FooterTemplate>
                            <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Center" />
                        </asp:TemplateField>--%>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                            TabIndex="26" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="27" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="28" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="29" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table>
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/HR/HRChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
