﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.BLL;
using VMVServices.Web;

namespace FIN.Client.HR
{
    public partial class InsurancePlanEntry : PageBase
    {
        INSURANCE_PLAN iNSURANCE_PLAN = new INSURANCE_PLAN();


        DataTable dtGridData = new DataTable();
        Boolean bol_rowVisiable;
        Boolean saveBool;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("APTerms", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
        }

        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                Startup();
                EntityData = null;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    using (IRepository<INSURANCE_PLAN> userCtx = new DataRepository<INSURANCE_PLAN>())
                    {
                        iNSURANCE_PLAN = userCtx.Find(r =>
                            (r.PLAN_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }


                    EntityData = iNSURANCE_PLAN;

                    txtlevel.Text = iNSURANCE_PLAN.PLAN_LEVAL;
                    if (iNSURANCE_PLAN.TOTAL_COST != null)
                    {
                        txttotalcost.Text = DBMethod.GetAmtDecimalCommaSeparationValue(iNSURANCE_PLAN.TOTAL_COST.ToString());
                    }
                    if (iNSURANCE_PLAN.COMPANY_COST != null)
                    {
                        txtcompcost.Text = DBMethod.GetAmtDecimalCommaSeparationValue(iNSURANCE_PLAN.COMPANY_COST.ToString());
                    }
                    if (iNSURANCE_PLAN.EMP_COST != null)
                    {
                        if (iNSURANCE_PLAN.EMP_COST > 0)
                        {
                            txtempcost.Text = DBMethod.GetAmtDecimalCommaSeparationValue(iNSURANCE_PLAN.EMP_COST.ToString());
                        }
                    }

                    if (iNSURANCE_PLAN.PLAN_FROM != null)
                    {
                        txtEffectiveFromDate.Text = DBMethod.ConvertDateToString(iNSURANCE_PLAN.PLAN_FROM.ToString());
                    }

                    if (iNSURANCE_PLAN.PLAN_TO != null)
                    {
                        txtEffectiveToDate.Text = DBMethod.ConvertDateToString(iNSURANCE_PLAN.PLAN_TO.ToString());
                    }
                    chkActive.Checked = false;
                    if (iNSURANCE_PLAN.ENABLED_FLAG == FINAppConstants.Y)
                        chkActive.Checked = true;
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("APTerm", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }


        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();

                if (EntityData != null)
                {
                    iNSURANCE_PLAN = (INSURANCE_PLAN)EntityData;
                }

                iNSURANCE_PLAN.PLAN_LEVAL = txtlevel.Text;

                //iNSURANCE_PLAN.TERM_NAME_OL = txtTermNameOL.Text;
                iNSURANCE_PLAN.TOTAL_COST = FIN.BLL.CommonUtils.ConvertStringToDecimal(txttotalcost.Text.ToString());
                iNSURANCE_PLAN.COMPANY_COST = CommonUtils.ConvertStringToDecimal(txtcompcost.Text.ToString());
                iNSURANCE_PLAN.EMP_COST = CommonUtils.ConvertStringToDecimal(txtempcost.Text.ToString());

                if (txtEffectiveFromDate.Text != null && txtEffectiveFromDate.Text != string.Empty && txtEffectiveFromDate.Text.ToString().Trim().Length > 0)
                {
                    iNSURANCE_PLAN.PLAN_FROM = DBMethod.ConvertStringToDate(txtEffectiveFromDate.Text.ToString());
                }

                if (txtEffectiveToDate.Text != null && txtEffectiveToDate.Text != string.Empty && txtEffectiveToDate.Text.ToString().Trim().Length > 0)
                {
                    iNSURANCE_PLAN.PLAN_TO = DBMethod.ConvertStringToDate(txtEffectiveToDate.Text.ToString());
                }

                if (chkActive.Checked)
                {
                    iNSURANCE_PLAN.ENABLED_FLAG = FINAppConstants.Y;
                }
                else
                {
                    iNSURANCE_PLAN.ENABLED_FLAG = FINAppConstants.N;
                }
                // iNSURANCE_PLAN.WORKFLOW_COMPLETION_STATUS = FINAppConstants.Y;
                iNSURANCE_PLAN.ORG_ID = VMVServices.Web.Utils.OrganizationID;
                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    iNSURANCE_PLAN.MODIFIED_BY = this.LoggedUserName;
                    iNSURANCE_PLAN.MODIFIED_DATE = DateTime.Today;
                }
                else
                {
                    iNSURANCE_PLAN.PLAN_ID = FINSP.GetSPFOR_SEQCode("HR_123".ToString(), false, true);

                    iNSURANCE_PLAN.CREATED_BY = this.LoggedUserName;
                    iNSURANCE_PLAN.CREATED_DATE = DateTime.Today;
                }
                iNSURANCE_PLAN.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, iNSURANCE_PLAN.PLAN_ID);
                iNSURANCE_PLAN.ORG_ID = VMVServices.Web.Utils.OrganizationID;



                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            DBMethod.SaveEntity<INSURANCE_PLAN>(iNSURANCE_PLAN);
                            saveBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {
                            DBMethod.SaveEntity<INSURANCE_PLAN>(iNSURANCE_PLAN, true);
                            saveBool = true;
                            break;
                        }
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Insurance Plan", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }


        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                //ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session[FINSessionConstants.GridData], "Insurance Plan ");

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                if (txttotalcost.Text != txtcompcost.Text)
                {
                    if (txtempcost.Text == string.Empty)
                    {
                        ErrorCollection.Add("Company Cost", "Company Cost must be equal to Total Cost");
                        return;
                    }
                }

                if (txtempcost.Text != string.Empty)
                {
                    if (decimal.Parse(DBMethod.GetAmtDecimalCommaSeparationValue(txttotalcost.Text)) != (decimal.Parse(DBMethod.GetAmtDecimalCommaSeparationValue(txtcompcost.Text)) + decimal.Parse(DBMethod.GetAmtDecimalCommaSeparationValue(txtempcost.Text))))
                    {
                        ErrorCollection.Add("Total Cost", "Company Cost + Employee Cost must be equal to Total Cost");
                        return;
                    }
                }

                if (txtEffectiveToDate.Text == string.Empty)
                {
                    DataTable dtcom = new DataTable();
                    dtcom = DBMethod.ExecuteQuery(FIN.DAL.FINSQL.GetInsurePlanDtlswtCond(txtlevel.Text, Master.StrRecordId)).Tables[0];
                    if (dtcom.Rows.Count > 0)
                    {
                        ErrorCollection.Add("Insurance Plan Level", "Level Type already Opened ");
                        return;
                    }
                }


                AssignToBE();

                if (ErrorCollection.Count > 0)
                {
                    return;
                }


                if (saveBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Insurance Plan", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        #endregion


        protected void btnYes_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                DBMethod.DeleteEntity<INSURANCE_PLAN>(iNSURANCE_PLAN);
                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATADELETED);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("APTermEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
    }
}