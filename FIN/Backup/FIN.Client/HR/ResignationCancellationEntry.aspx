﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="ResignationCancellationEntry.aspx.cs" Inherits="FIN.Client.HR.ResignationCancellationEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 800px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblCancellationNumber">
                Cancellation Number
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:TextBox ID="txtCancellationNumber" CssClass="txtBox" runat="server" TabIndex="1"
                    MaxLength="50" Enabled="true"></asp:TextBox>
            </div>
             <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 200px" id="lblCancellationDate">
                Cancellation Date
            </div>
            <div class="divtxtBox  LNOrient" style="width: 152px">
                <asp:TextBox runat="server" ID="txtCancellationDate" CssClass="validate[required ,custom[ReqDateDDMMYYY]] RequiredField   txtBox"
                  MaxLength="10"  TabIndex="2" OnTextChanged="txtCancellationDate_TextChanged"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtCancellationDate"
                    OnClientDateSelectionChanged="checkDate" />
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtCancellationDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblRequestNumber">
                Request Number
            </div>
            <div class="divtxtBox  LNOrient" style="width: 525px">
                <asp:DropDownList ID="ddlreqno" runat="server" CssClass="validate[required] RequiredField EntryFont ddlStype"
                    TabIndex="3" AutoPostBack="True" OnSelectedIndexChanged="ddlreqno_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <%--<div class="colspace" style="float: left;">
                &nbsp</div>
            <div class="lblBox" style="float: left; width: 195px" id="lblRequestDate">
                Employee
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:TextBox ID="txtEmployeeName" CssClass="validate[] txtBox" runat="server" TabIndex="4"
                    Enabled="true" MaxLength="500"></asp:TextBox>
            </div>--%>
        </div>
         <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="Div2">
               Employee
            </div>
            <div class="divtxtBox  LNOrient" style="width: 530px">
                <asp:TextBox ID="txtEmployeeName" CssClass="validate[] txtBox" runat="server" TabIndex="4"
                    Enabled="true" MaxLength="500"></asp:TextBox>
            </div>
           
        </div>
        <div class="divClear_10">
        </div>
        <%--  <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="lblDepartmentName">
                Department Name
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:DropDownList ID="ddlDepartment" TabIndex="5" CssClass="validate[required] RequiredField ddlStype" runat="server" Width="100%"
                    AutoPostBack="true">
                </asp:DropDownList>
            </div>
            <div class="lblBox" style="float: left; width: 200px" id="lblEmployeeName">
                Employee Name
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:DropDownList ID="ddlEmployeeName" TabIndex="6" CssClass="validate[] txtBox"
                    runat="server">
                </asp:DropDownList>
            </div>
        </div>--%>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblReason">
                Request Date
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:TextBox runat="server" ID="txtRequestDate" Enabled="true" CssClass="validate[,custom[ReqDateDDMMYYY],,]  txtBox"
                  MaxLength="10"  TabIndex="5"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender2" TargetControlID="txtRequestDate"
                    OnClientDateSelectionChanged="checkDate" />
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtRequestDate" />
            </div>
             <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 200px" id="lblRemarks">
                Reason
            </div>
            <div class="divtxtBox  LNOrient" style="width: 155px">
                <asp:DropDownList ID="ddlReason" runat="server" CssClass="validate[required] RequiredField ddlStype" TabIndex="6">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblActive">
                Remarks
            </div>
            <div class="divtxtBox  LNOrient" style="width: 530px">
                <asp:TextBox ID="txtRemarks" CssClass="validate[] txtBox" TextMode="MultiLine" Height="50px"
                    runat="server" TabIndex="7" MaxLength="500"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="Div1">
                Active
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:CheckBox ID="chkActive" runat="server" Checked="True" TabIndex="8" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                            TabIndex="9" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="10" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="11" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="12" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table>
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/HR/HRChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
