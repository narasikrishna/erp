﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.BLL;
using FIN.BLL.AP;
using FIN.DAL.AP;
using VMVServices.Web;

namespace FIN.Client.HR
{
    public partial class TimeAttendanceScheduleEntry : PageBase
    {
        TM_SCHEDULES tM_SCHEDULES = new TM_SCHEDULES();
        Boolean savedBool;
        string ProReturn = null;
        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();

        }


        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
               

                Startup();
                FillComboBox();

                EntityData = null;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    using (IRepository<TM_SCHEDULES> userCtx = new DataRepository<TM_SCHEDULES>())
                    {
                        tM_SCHEDULES = userCtx.Find(r =>
                            (r.TM_SCH_CODE == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = tM_SCHEDULES;

                    txtScheduleCode.Text = tM_SCHEDULES.TM_SCH_CODE;
                    txtDescription.Text = tM_SCHEDULES.TM_SCH_DESC;

                    if (tM_SCHEDULES.TM_FROM1 != null)
                    {
                        ddlStartTimeHr.SelectedValue = tM_SCHEDULES.TM_FROM1.Substring(tM_SCHEDULES.TM_FROM1.Length - 11, 2).Replace(" ", "0");
                    }
                    if (tM_SCHEDULES.TM_TO1 != null)
                    {
                        ddlEndTimeHr.SelectedValue = tM_SCHEDULES.TM_TO1.Substring(tM_SCHEDULES.TM_TO1.Length - 11, 2).Replace(" ", "0");
                    }


                    if (tM_SCHEDULES.TM_FROM1 != null)
                    {
                        ddlStartTimeMin.SelectedValue = tM_SCHEDULES.TM_FROM1.Substring(tM_SCHEDULES.TM_FROM1.Length - 8, 2);
                    }
                    if (tM_SCHEDULES.TM_TO1 != null)
                    {
                        ddlEndTimeMin.SelectedValue = tM_SCHEDULES.TM_TO1.Substring(tM_SCHEDULES.TM_TO1.Length - 8, 2);
                    }


                    if (tM_SCHEDULES.TM_FROM1 != null)
                    {
                        ddlStartTimeAMPM.SelectedValue = tM_SCHEDULES.TM_FROM1.Substring(tM_SCHEDULES.TM_FROM1.Length - 2, 2);
                    }
                    if (tM_SCHEDULES.TM_TO1 != null)
                    {
                        ddlEndTimeAMPM.SelectedValue = tM_SCHEDULES.TM_TO1.Substring(tM_SCHEDULES.TM_TO1.Length - 2, 2);
                    }
                    

                    if (tM_SCHEDULES.TM_FLEX_TIME != null)
                    {
                        txtFlexTime.Text = tM_SCHEDULES.TM_FLEX_TIME.ToString();
                    }
                    if (tM_SCHEDULES.TM_GRACE_TIME != null)
                    {
                        txtGraceTime.Text = tM_SCHEDULES.TM_GRACE_TIME.ToString();
                    }

                    if (tM_SCHEDULES.ENABLED_FLAG == "1")
                    {
                        chkActive.Checked = true;
                    }
                    else
                    {
                        chkActive.Checked = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>

        private void FillComboBox()
        {
            //  FIN.BLL.Lookup_BLL.GetLookUpValues(ref ddlWarehouseType, "20");

        }

        private void AssignToBE(DateTime dtStartTime1, DateTime dtEndTime1)
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    tM_SCHEDULES = (TM_SCHEDULES)EntityData;                    
                }

                
                tM_SCHEDULES.TM_SCH_CODE = txtScheduleCode.Text;
                tM_SCHEDULES.TM_SCH_DESC = txtDescription.Text;

                if (dtStartTime1.ToString() != string.Empty && dtStartTime1 != DateTime.MinValue)
                {
                    tM_SCHEDULES.TM_FROM1 = dtStartTime1.ToString();
                }
                if (dtEndTime1.ToString() != string.Empty && dtEndTime1 != DateTime.MinValue)
                {
                    tM_SCHEDULES.TM_TO1 = dtEndTime1.ToString();
                }

                tM_SCHEDULES.TM_FLEX_TIME =   CommonUtils.ConvertStringToDecimal(txtFlexTime.Text.ToString());
                tM_SCHEDULES.TM_GRACE_TIME = CommonUtils.ConvertStringToDecimal(txtGraceTime.Text.ToString());
                tM_SCHEDULES.TM_ORG_ID = (VMVServices.Web.Utils.OrganizationID.ToString());

                //tM_SCHEDULES.ENABLED_FLAG = FINAppConstants.EnabledFlag;

                if (chkActive.Checked == true)
                {
                    tM_SCHEDULES.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                }
                else
                {
                    tM_SCHEDULES.ENABLED_FLAG = FINAppConstants.DisabledFlag;
                }

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    tM_SCHEDULES.MODIFIED_BY = this.LoggedUserName;
                    tM_SCHEDULES.MODIFIED_DATE = DateTime.Today;                 
                    

                }
                else
                {
                    tM_SCHEDULES.TM_SCH_CODE = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_027.ToString(), false, true);
                    
                 
                    tM_SCHEDULES.CREATED_BY = this.LoggedUserName;
                    tM_SCHEDULES.CREATED_DATE = DateTime.Today;

                }
                tM_SCHEDULES.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, tM_SCHEDULES.TM_SCH_CODE);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                System.Collections.SortedList slControls = new System.Collections.SortedList();

                
                slControls[0] = txtDescription;
                slControls[1] = ddlStartTimeHr;
                slControls[2] = txtFlexTime;
                slControls[3] = txtGraceTime;

                ErrorCollection.Clear();
                Dictionary<string, string> Prop_File_Data;
                Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));
         
                string strCtrlTypes = FINAppConstants.TEXT_BOX + '~' + FINAppConstants.DROP_DOWN_LIST + '~' + FINAppConstants.TEXT_BOX + '~' + FINAppConstants.TEXT_BOX;
                string strMessage = Prop_File_Data["Description_P"] + " ~ " + Prop_File_Data["From_Time_P"] + " ~ " + Prop_File_Data["Flex_Time_P"] + " ~ " + Prop_File_Data["Grace_Time_P"] + "";
                //string strMessage = "Description ~From Time~Flex Time~Grace Time";

                ErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

                if (ErrorCollection.Count > 0)
                {
                    return;
                }



                int startHr1 = 0;
                if (ddlStartTimeHr.SelectedValue != string.Empty)
                {
                    startHr1 = Convert.ToInt16(ddlStartTimeHr.SelectedValue.ToString());
                    if (ddlStartTimeAMPM.SelectedValue == "PM" && startHr1 != 12)
                        startHr1 = startHr1 + 12;
                }

                DateTime dtStartTime1 = DateTime.MinValue;
                DateTime dtEndTime1 = DateTime.MinValue;
                if (ddlEndTimeHr.SelectedValue != string.Empty && ddlStartTimeHr.SelectedValue != string.Empty)
                {
                    int EndHr1 = Convert.ToInt16(ddlEndTimeHr.SelectedValue.ToString());

                    if (ddlEndTimeAMPM.SelectedValue == "PM" && EndHr1 != 12)
                        EndHr1 = EndHr1 + 12;

                    dtStartTime1 = new DateTime(FINAppConstants.TMP_YEAR, FINAppConstants.TMP_MONTH, FINAppConstants.TMP_DAY, startHr1, Convert.ToInt16(ddlStartTimeMin.SelectedValue), 0);
                    dtEndTime1 = new DateTime(FINAppConstants.TMP_YEAR, FINAppConstants.TMP_MONTH, FINAppConstants.TMP_DAY, EndHr1, Convert.ToInt16(ddlEndTimeMin.SelectedValue), 0);
                    if ((dtEndTime1 - dtStartTime1).TotalMinutes <= 0)
                    {
                        if (ddlStartTimeAMPM.SelectedValue == "AM" && ddlEndTimeAMPM.SelectedValue == "PM")
                        {
                            ErrorCollection.Remove("starttime1");
                            ErrorCollection.Add("starttime1", "From Time must be less than the End Time");
                            return;
                        }
                    }
                    if (ddlStartTimeMin.SelectedValue == ddlEndTimeMin.SelectedValue && ddlStartTimeHr.SelectedValue == ddlEndTimeHr.SelectedValue && ddlStartTimeAMPM.SelectedValue == ddlEndTimeAMPM.SelectedValue)
                    {
                        ErrorCollection.Remove("starttime3");
                        ErrorCollection.Add("starttime3", "From Time should not be equal to End Time");
                        return;
                    }
                }

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                AssignToBE(dtStartTime1, dtEndTime1);

                if (ErrorCollection.Count > 0)
                {
                    return;
                }


                // Duplicate Validation Through Backend Package PKG_VALIDATIONS

                //ProReturn = FIN.DAL.HR.TimeAttendanceSchedule_DAL.GetSPFOR_DUPLICATE_CHECK(tM_SCHEDULES.TM_SCH_CODE, tM_SCHEDULES.TM_SCH_CODE);

                //if (ProReturn != string.Empty)
                //{
                //    if (ProReturn != "0")
                //    {
                //        ErrorCollection.Add("DEPTSCHD", ProReturn);
                //        if (ErrorCollection.Count > 0)
                //        {
                //            return;
                //        }
                //    }
                //}
						


                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            DBMethod.SaveEntity<TM_SCHEDULES>(tM_SCHEDULES);

                            savedBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {

                            DBMethod.SaveEntity<TM_SCHEDULES>(tM_SCHEDULES, true);
                            savedBool = true;
                            break;

                        }
                }

                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                }

            }



            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion




        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                DBMethod.DeleteEntity<TM_SCHEDULES>(tM_SCHEDULES);

                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void txtFromTime_TextChanged(object sender, EventArgs e)
        {

        }



    }
}