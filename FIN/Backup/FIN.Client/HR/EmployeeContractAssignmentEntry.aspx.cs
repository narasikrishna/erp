﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.BLL.AP;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.BLL;
using VMVServices.Web;
namespace FIN.Client.HR
{
    public partial class EmployeeContractAssignmentEntry : PageBase
    {
        HR_EMP_CONTRACT_ASSIGNMENT hR_EMP_CONTRACT_ASSIGNMENT = new HR_EMP_CONTRACT_ASSIGNMENT();
        HR_EMPLOYEES hR_EMPLOYEES = new HR_EMPLOYEES();
        System.Collections.SortedList slControls = new System.Collections.SortedList();
        string ProReturn = null;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                //pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                //pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";

            }

            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
            }

            if (VMVServices.Web.Utils.AddAllowed != FINAppConstants.Y && Master.Mode == FINAppConstants.Add)
            {
                btnSave.Visible = false;
            }

        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                //ddlWeightUOM.Enabled = false;
                //ddlLengthUOM.Enabled = false;
                //ddlAreaUOM.Enabled = false;
                //ddlVolumeUOM.Enabled = false;


                Startup();
                FillComboBox();


                EntityData = null;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    using (IRepository<HR_EMP_CONTRACT_ASSIGNMENT> userCtx = new DataRepository<HR_EMP_CONTRACT_ASSIGNMENT>())
                    {
                        hR_EMP_CONTRACT_ASSIGNMENT = userCtx.Find(r =>
                            (r.CA_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = hR_EMP_CONTRACT_ASSIGNMENT;

                    ddlEmployee.SelectedValue = hR_EMP_CONTRACT_ASSIGNMENT.CA_EMP_ID;
                    //FillEmployeeName();
                    FillContractDetails();
                    ddlContractName.SelectedValue = hR_EMP_CONTRACT_ASSIGNMENT.CA_CONTRACT_HDR_ID;
                    if (hR_EMP_CONTRACT_ASSIGNMENT.CA_EFFECTIVE_FROM_DT != null)
                    {
                        txtFromDate.Text = DBMethod.ConvertDateToString(hR_EMP_CONTRACT_ASSIGNMENT.CA_EFFECTIVE_FROM_DT.ToString());
                    }
                    if (hR_EMP_CONTRACT_ASSIGNMENT.CA_EFFECTIVE_TO_DT != null)
                    {
                        txtToDate.Text = DBMethod.ConvertDateToString(hR_EMP_CONTRACT_ASSIGNMENT.CA_EFFECTIVE_TO_DT.ToString());
                    }
                    txtRemarks.Text = hR_EMP_CONTRACT_ASSIGNMENT.CA_REMARKS;
                    chKActive.Checked = hR_EMP_CONTRACT_ASSIGNMENT.ENABLED_FLAG == FINAppConstants.EnabledFlag ? true : false;
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>

        private void FillComboBox()
        {
            FIN.BLL.HR.Employee_BLL.GetEmployeeNameWithCode(ref ddlEmployee);
            
            //ComboFilling.fn_getDepartment(ref ddlDepartment);
            //Lookup_BLL.GetLookUpValues(ref ddlInternalExternal, "INTERNAL_EXTERNAL");
            //Lookup_BLL.GetLookUpValues(ref ddlType, "VAC_TYPE");


        }

        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    hR_EMP_CONTRACT_ASSIGNMENT = (HR_EMP_CONTRACT_ASSIGNMENT)EntityData;
                }
                hR_EMP_CONTRACT_ASSIGNMENT.CA_EMP_ID = ddlEmployee.Text;

                hR_EMP_CONTRACT_ASSIGNMENT.CA_CONTRACT_HDR_ID = ddlContractName.SelectedValue.ToString();
                if (txtFromDate.Text != string.Empty)
                {
                    hR_EMP_CONTRACT_ASSIGNMENT.CA_EFFECTIVE_FROM_DT = DBMethod.ConvertStringToDate(txtFromDate.Text.ToString());
                }
                if (txtToDate.Text != string.Empty)
                {
                    hR_EMP_CONTRACT_ASSIGNMENT.CA_EFFECTIVE_TO_DT = DBMethod.ConvertStringToDate(txtToDate.Text.ToString());
                }
                hR_EMP_CONTRACT_ASSIGNMENT.CA_REMARKS = txtRemarks.Text;


                 hR_EMP_CONTRACT_ASSIGNMENT.ENABLED_FLAG = FINAppConstants.Y;
                 hR_EMP_CONTRACT_ASSIGNMENT.ENABLED_FLAG = chKActive.Checked == true ? FINAppConstants.EnabledFlag : FINAppConstants.DisabledFlag;
                 hR_EMP_CONTRACT_ASSIGNMENT.CA_ORG_ID = VMVServices.Web.Utils.OrganizationID;





                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    hR_EMP_CONTRACT_ASSIGNMENT.MODIFIED_BY = this.LoggedUserName;
                    hR_EMP_CONTRACT_ASSIGNMENT.MODIFIED_DATE = DateTime.Today;

                }
                else
                {
                    hR_EMP_CONTRACT_ASSIGNMENT.CA_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_075.ToString(), false, true);
                    //iNV_ITEM_MASTER.WORKFLOW_COMPLETION_STATUS = "1";
                    hR_EMP_CONTRACT_ASSIGNMENT.CREATED_BY = this.LoggedUserName;
                    hR_EMP_CONTRACT_ASSIGNMENT.CREATED_DATE = DateTime.Today;

                }

                hR_EMP_CONTRACT_ASSIGNMENT.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_EMP_CONTRACT_ASSIGNMENT.CA_ID);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                AssignToBE();
                ProReturn = FINSP.GetSPFOR_ERR_MGR_CATEGORY_TWO(Master.FormCode, hR_EMP_CONTRACT_ASSIGNMENT.CA_ID, hR_EMP_CONTRACT_ASSIGNMENT.CA_EMP_ID, hR_EMP_CONTRACT_ASSIGNMENT.CA_CONTRACT_HDR_ID);
                if (ProReturn != string.Empty)
                {
                    if (ProReturn != "0")
                    {
                        ErrorCollection.Add("EMPLOYEECONTRACTASSIGNMENT", ProReturn);
                        if (ErrorCollection.Count > 0)
                        {
                            return;
                        }
                    }
                }

                //emptyvalid();
                if (ErrorCollection.Count > 0)
                {
                    return;
                }


                // Duplicate Validation Through Backend Package PKG_VALIDATIONS



                //ProReturn = FIN.DAL.AP.Item_DAL.GetSPFOR_DUPLICATE_CHECK(iNV_ITEM_MASTER.ITEM_CODE,txtItemName.Text, iNV_ITEM_MASTER.ORG_ID, iNV_ITEM_MASTER.ITEM_ID);

                //if (ProReturn != string.Empty)
                //{
                //    if (ProReturn != "0")
                //    {
                //        ErrorCollection.Add("ITEMNAME", ProReturn);
                //        if (ErrorCollection.Count > 0)
                //        {
                //            return;
                //        }
                //    }
                //}


                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            DBMethod.SaveEntity<HR_EMP_CONTRACT_ASSIGNMENT>(hR_EMP_CONTRACT_ASSIGNMENT);
                            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                            break;
                        }
                    case FINAppConstants.Update:
                        {

                            DBMethod.SaveEntity<HR_EMP_CONTRACT_ASSIGNMENT>(hR_EMP_CONTRACT_ASSIGNMENT, true);
                            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                            break;

                        }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion




        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                DBMethod.DeleteEntity<HR_EMP_CONTRACT_ASSIGNMENT>(hR_EMP_CONTRACT_ASSIGNMENT);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void ddlEmployee_SelectedIndexChanged(object sender, EventArgs e)
        {
            //FillEmployeeName();
            FillContractDetails();
        }
        private void FillContractDetails()
        {
            FIN.BLL.HR.EmployeeContractDetails_BLL.fn_getContractName4EmpId(ref ddlContractName, ddlEmployee.SelectedValue);
        }

        

        //private void FillEmployeeName()
        //{
        //    DataTable dtemployeename = new DataTable();
        //    dtemployeename = DBMethod.ExecuteQuery(FIN.DAL.HR.EmployeeContractAssignment_DAL.GetEmployeeName(ddlEmployee.SelectedValue)).Tables[0];
        //    txtName.Text = dtemployeename.Rows[0]["EMP_FIRST_NAME"].ToString();
        //}
        





        





    }





}
