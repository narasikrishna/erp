﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="TimeAttendanceCompanyScheduleEntry.aspx.cs" Inherits="FIN.Client.HR.TimeAttendanceCompanyScheduleEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 950px" id="divMainContainer">
        <asp:TextBox ID="txtStartTime1" MaxLength="10" CssClass="validate[required] RequiredField txtBox_N"
            Visible="false" runat="server" TabIndex="45"></asp:TextBox>
        <asp:TextBox ID="txtOutTime" MaxLength="10" CssClass="validate[required] RequiredField txtBox_N"
            Visible="false" runat="server" TabIndex="44"></asp:TextBox>
        <asp:TextBox ID="txtInTime" MaxLength="10" CssClass="validate[required] RequiredField txtBox_N"
            Visible="false" runat="server" OnTextChanged="txtInTime_TextChanged" TabIndex="3"></asp:TextBox>
        <asp:TextBox ID="txtEndTime1" MaxLength="10" CssClass="validate[required] RequiredField txtBox_N"
            Visible="false" runat="server" TabIndex="46"></asp:TextBox>
        <asp:TextBox ID="txtStartTime2" MaxLength="10" CssClass="validate[required] RequiredField txtBox_N"
            Visible="false" runat="server" TabIndex="47"></asp:TextBox>
        <asp:TextBox ID="txtEndTime2" MaxLength="10" CssClass="validate[required] RequiredField  txtBox_N"
            Visible="false" runat="server" TabIndex="48"></asp:TextBox>
        <asp:TextBox ID="txtStartTime3" MaxLength="10" CssClass="validate[required] RequiredField  txtBox_N"
            Visible="false" runat="server" TabIndex="49"></asp:TextBox>
        <asp:TextBox ID="txtEndTime3" MaxLength="10" CssClass="txtBox_N" runat="server" TabIndex="10"
            Visible="false"></asp:TextBox><div class="divClear_10">
            </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblStartDate">
                Start Date
            </div>
            <div class="divtxtBox  LNOrient" style="width: 190px">
                <asp:TextBox ID="txtStartDate" CssClass="validate[required,custom[ReqDateDDMMYYY],,dateRange[dg1]]  RequiredField  txtBox"
                    runat="server" TabIndex="1"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender2" TargetControlID="txtStartDate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>

            <div class="lblBox LNOrient" style="width: 200px" id="lblEndDate">
                End Date
            </div>
            <div class="divtxtBox  LNOrient" style="width: 190px">
                <asp:TextBox ID="txtEndDate" CssClass="validate[,custom[ReqDateDDMMYYY],,dateRange[dg1]]  txtBox"
                    runat="server" TabIndex="2"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender3" TargetControlID="txtEndDate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
        </div>
<%--        <div class="divClear_10">
        </div>--%>
     <%--   <div class="divRowContainer">
            
        </div>--%>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblInTime">
                In Time
            </div>
            <div class="divtxtBox  LNOrient" style="width: 60px">
                <asp:DropDownList ID="ddl_FHour" runat="server" CssClass="validate[required] RequiredField  ddlStype" Width="45px"  TabIndex="3">
                    <asp:ListItem Value="" Text="-"></asp:ListItem>
                    <asp:ListItem Value="01">01</asp:ListItem>
                    <asp:ListItem Value="02">02</asp:ListItem>
                    <asp:ListItem Value="03">03</asp:ListItem>
                    <asp:ListItem Value="04">04</asp:ListItem>
                    <asp:ListItem Value="05">05</asp:ListItem>
                    <asp:ListItem Value="06">06</asp:ListItem>
                    <asp:ListItem Value="07">07</asp:ListItem>
                    <asp:ListItem Value="08">08</asp:ListItem>
                    <asp:ListItem Value="09">09</asp:ListItem>
                    <asp:ListItem Value="10">10</asp:ListItem>
                    <asp:ListItem Value="11">11</asp:ListItem>
                    <asp:ListItem Value="12">12</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="divtxtBox  LNOrient" style="width: 60px">
                <asp:DropDownList ID="ddl_FMin" runat="server" CssClass="validate[required] RequiredField  ddlStype" Width="45px"  TabIndex="4">
                    <asp:ListItem Value="" Text="-"></asp:ListItem>
                    <asp:ListItem Value="00">00</asp:ListItem>
                    <asp:ListItem Value="05">05</asp:ListItem>
                    <asp:ListItem Value="10">10</asp:ListItem>
                    <asp:ListItem Value="15">15</asp:ListItem>
                    <asp:ListItem Value="20">20</asp:ListItem>
                    <asp:ListItem Value="25">25</asp:ListItem>
                    <asp:ListItem Value="30">30</asp:ListItem>
                    <asp:ListItem Value="35">35</asp:ListItem>
                    <asp:ListItem Value="40">40</asp:ListItem>
                    <asp:ListItem Value="45">45</asp:ListItem>
                    <asp:ListItem Value="50">50</asp:ListItem>
                    <asp:ListItem Value="55">55</asp:ListItem>
                    <asp:ListItem Value="60">60</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="divtxtBox  LNOrient" style="width: 60px">
                <asp:DropDownList ID="ddl_FNoon" runat="server" CssClass="validate[required] RequiredField  ddlStype" Width="45px"  TabIndex="5">
                    <asp:ListItem Value="" Text="-"></asp:ListItem>
                    <asp:ListItem Value="AM">AM</asp:ListItem>
                    <asp:ListItem Value="PM">PM</asp:ListItem>
                </asp:DropDownList>
            </div>

            <div class="lblBox LNOrient" style="width: 200px" id="lblOutTime">
                Out Time
            </div>
            <div class="divtxtBox  LNOrient" style="width: 60px">
                <asp:DropDownList ID="ddl_THour" runat="server" CssClass="validate[required] RequiredField  ddlStype" Width="45px"  TabIndex="6">
                    <asp:ListItem Value="" Text="-"></asp:ListItem>
                    <asp:ListItem Value="01">01</asp:ListItem>
                    <asp:ListItem Value="02">02</asp:ListItem>
                    <asp:ListItem Value="03">03</asp:ListItem>
                    <asp:ListItem Value="04">04</asp:ListItem>
                    <asp:ListItem Value="05">05</asp:ListItem>
                    <asp:ListItem Value="06">06</asp:ListItem>
                    <asp:ListItem Value="07">07</asp:ListItem>
                    <asp:ListItem Value="08">08</asp:ListItem>
                    <asp:ListItem Value="09">09</asp:ListItem>
                    <asp:ListItem Value="10">10</asp:ListItem>
                    <asp:ListItem Value="11">11</asp:ListItem>
                    <asp:ListItem Value="12">12</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="divtxtBox  LNOrient" style="width: 60px">
                <asp:DropDownList ID="ddl_TMin" runat="server" CssClass="validate[required] RequiredField  ddlStype" Width="45px"  TabIndex="7">
                    <asp:ListItem Value="" Text="-"></asp:ListItem>
                    <asp:ListItem Value="00">00</asp:ListItem>
                    <asp:ListItem Value="05">05</asp:ListItem>
                    <asp:ListItem Value="10">10</asp:ListItem>
                    <asp:ListItem Value="15">15</asp:ListItem>
                    <asp:ListItem Value="20">20</asp:ListItem>
                    <asp:ListItem Value="25">25</asp:ListItem>
                    <asp:ListItem Value="30">30</asp:ListItem>
                    <asp:ListItem Value="35">35</asp:ListItem>
                    <asp:ListItem Value="40">40</asp:ListItem>
                    <asp:ListItem Value="45">45</asp:ListItem>
                    <asp:ListItem Value="50">50</asp:ListItem>
                    <asp:ListItem Value="55">55</asp:ListItem>
                    <asp:ListItem Value="60">60</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="divtxtBox  LNOrient" style="width: 60px">
                <asp:DropDownList ID="ddl_TNoon" runat="server" CssClass="validate[required] RequiredField  ddlStype" Width="45px"  TabIndex="8">
                    <asp:ListItem Value="" Text="-"></asp:ListItem>
                    <asp:ListItem Value="AM">AM</asp:ListItem>
                    <asp:ListItem Value="PM">PM</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
<%--        <div class="divRowContainer">
            
        </div>--%>
<%--        <div class="divClear_10">
        </div>--%>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblStartTime1">
                Break1 Start
            </div>
            <div class="divtxtBox  LNOrient" style="width: 60px">
                <asp:DropDownList ID="ddlStartTime1Hr" runat="server" CssClass="  ddlStype"  TabIndex="9"
                    Width="45px">
                    <asp:ListItem Value="" Text="-"></asp:ListItem>
                    <asp:ListItem Value="01">01</asp:ListItem>
                    <asp:ListItem Value="02">02</asp:ListItem>
                    <asp:ListItem Value="03">03</asp:ListItem>
                    <asp:ListItem Value="04">04</asp:ListItem>
                    <asp:ListItem Value="05">05</asp:ListItem>
                    <asp:ListItem Value="06">06</asp:ListItem>
                    <asp:ListItem Value="07">07</asp:ListItem>
                    <asp:ListItem Value="08">08</asp:ListItem>
                    <asp:ListItem Value="09">09</asp:ListItem>
                    <asp:ListItem Value="10">10</asp:ListItem>
                    <asp:ListItem Value="11">11</asp:ListItem>
                    <asp:ListItem Value="12">12</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="divtxtBox  LNOrient" style="width: 60px">
                <asp:DropDownList ID="ddlStartTime1Min" runat="server" CssClass="  ddlStype"  TabIndex="10"
                    Width="45px">
                    <asp:ListItem Value="" Text="-"></asp:ListItem>
                    <asp:ListItem Value="00">00</asp:ListItem>
                    <asp:ListItem Value="05">05</asp:ListItem>
                    <asp:ListItem Value="10">10</asp:ListItem>
                    <asp:ListItem Value="15">15</asp:ListItem>
                    <asp:ListItem Value="20">20</asp:ListItem>
                    <asp:ListItem Value="25">25</asp:ListItem>
                    <asp:ListItem Value="30">30</asp:ListItem>
                    <asp:ListItem Value="35">35</asp:ListItem>
                    <asp:ListItem Value="40">40</asp:ListItem>
                    <asp:ListItem Value="45">45</asp:ListItem>
                    <asp:ListItem Value="50">50</asp:ListItem>
                    <asp:ListItem Value="55">55</asp:ListItem>
                    <asp:ListItem Value="60">60</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="divtxtBox  LNOrient" style="width:60px">
                <asp:DropDownList ID="ddlStartTime1AMPM" runat="server" CssClass="  ddlStype"  TabIndex="11"
                    Width="45px">
                    <asp:ListItem Value="" Text="-"></asp:ListItem>
                    <asp:ListItem Value="AM">AM</asp:ListItem>
                    <asp:ListItem Value="PM">PM</asp:ListItem>
                </asp:DropDownList>
            </div>

              <div class="lblBox LNOrient" style="width: 200px" id="lblEndTime1">
                Break1 End
            </div>
            <div class="divtxtBox  LNOrient" style="width: 60px">
                <asp:DropDownList ID="ddlEndTime1Hr" runat="server" CssClass="   ddlStype"  TabIndex="12"
                    Width="45px">
                    <asp:ListItem Value="" Text="-"></asp:ListItem>
                    <asp:ListItem Value="01">01</asp:ListItem>
                    <asp:ListItem Value="02">02</asp:ListItem>
                    <asp:ListItem Value="03">03</asp:ListItem>
                    <asp:ListItem Value="04">04</asp:ListItem>
                    <asp:ListItem Value="05">05</asp:ListItem>
                    <asp:ListItem Value="06">06</asp:ListItem>
                    <asp:ListItem Value="07">07</asp:ListItem>
                    <asp:ListItem Value="08">08</asp:ListItem>
                    <asp:ListItem Value="09">09</asp:ListItem>
                    <asp:ListItem Value="10">10</asp:ListItem>
                    <asp:ListItem Value="11">11</asp:ListItem>
                    <asp:ListItem Value="12">12</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="divtxtBox  LNOrient" style="width: 60px">
                <asp:DropDownList ID="ddlEndTime1Min" runat="server" CssClass="  ddlStype"  TabIndex="13"
                    Width="45px">
                    <asp:ListItem Value="" Text="-"></asp:ListItem>
                    <asp:ListItem Value="00">00</asp:ListItem>
                    <asp:ListItem Value="05">05</asp:ListItem>
                    <asp:ListItem Value="10">10</asp:ListItem>
                    <asp:ListItem Value="15">15</asp:ListItem>
                    <asp:ListItem Value="20">20</asp:ListItem>
                    <asp:ListItem Value="25">25</asp:ListItem>
                    <asp:ListItem Value="30">30</asp:ListItem>
                    <asp:ListItem Value="35">35</asp:ListItem>
                    <asp:ListItem Value="40">40</asp:ListItem>
                    <asp:ListItem Value="45">45</asp:ListItem>
                    <asp:ListItem Value="50">50</asp:ListItem>
                    <asp:ListItem Value="55">55</asp:ListItem>
                    <asp:ListItem Value="60">60</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="divtxtBox  LNOrient" style="width: 60px">
                <asp:DropDownList ID="ddlEndTime1AMPM" runat="server" CssClass="  ddlStype"  TabIndex="14"
                    Width="45px">
                    <asp:ListItem Value="" Text="-"></asp:ListItem>
                    <asp:ListItem Value="AM">AM</asp:ListItem>
                    <asp:ListItem Value="PM">PM</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
<%--        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
          
        </div>--%>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblStartTime2">
                Break2 Start
            </div>
            <div class="divtxtBox  LNOrient" style="width: 60px">
                <asp:DropDownList ID="ddlStartTime2Hr" runat="server" CssClass=" ddlStype"  TabIndex="15"
                    Width="45px">
                    <asp:ListItem Value="" Text="-"></asp:ListItem>
                    <asp:ListItem Value="01">01</asp:ListItem>
                    <asp:ListItem Value="02">02</asp:ListItem>
                    <asp:ListItem Value="03">03</asp:ListItem>
                    <asp:ListItem Value="04">04</asp:ListItem>
                    <asp:ListItem Value="05">05</asp:ListItem>
                    <asp:ListItem Value="06">06</asp:ListItem>
                    <asp:ListItem Value="07">07</asp:ListItem>
                    <asp:ListItem Value="08">08</asp:ListItem>
                    <asp:ListItem Value="09">09</asp:ListItem>
                    <asp:ListItem Value="10">10</asp:ListItem>
                    <asp:ListItem Value="11">11</asp:ListItem>
                    <asp:ListItem Value="12">12</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="divtxtBox  LNOrient" style="width: 60px">
                <asp:DropDownList ID="ddlStartTime2Min" runat="server" CssClass=" ddlStype"  TabIndex="16"
                    Width="45px">
                    <asp:ListItem Value="" Text="-"></asp:ListItem>
                    <asp:ListItem Value="00">00</asp:ListItem>
                    <asp:ListItem Value="05">05</asp:ListItem>
                    <asp:ListItem Value="10">10</asp:ListItem>
                    <asp:ListItem Value="15">15</asp:ListItem>
                    <asp:ListItem Value="20">20</asp:ListItem>
                    <asp:ListItem Value="25">25</asp:ListItem>
                    <asp:ListItem Value="30">30</asp:ListItem>
                    <asp:ListItem Value="35">35</asp:ListItem>
                    <asp:ListItem Value="40">40</asp:ListItem>
                    <asp:ListItem Value="45">45</asp:ListItem>
                    <asp:ListItem Value="50">50</asp:ListItem>
                    <asp:ListItem Value="55">55</asp:ListItem>
                    <asp:ListItem Value="60">60</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="divtxtBox  LNOrient" style="width:60px">
                <asp:DropDownList ID="ddlStartTime2AMPM" runat="server" CssClass=" ddlStype"  TabIndex="17"
                    Width="45px">
                    <asp:ListItem Value="" Text="-"></asp:ListItem>
                    <asp:ListItem Value="AM">AM</asp:ListItem>
                    <asp:ListItem Value="PM">PM</asp:ListItem>
                </asp:DropDownList>
            </div>

            <div class="lblBox LNOrient" style="width: 200px" id="lblEndTime2">
            Break2 End
        </div>
        <div class="divtxtBox  LNOrient" style="width: 60px">
            <asp:DropDownList ID="ddlEndTime2Hr" runat="server" CssClass=" ddlStype"  TabIndex="18"
                Width="45px">
                <asp:ListItem Value="" Text="-"></asp:ListItem>
                <asp:ListItem Value="01">01</asp:ListItem>
                <asp:ListItem Value="02">02</asp:ListItem>
                <asp:ListItem Value="03">03</asp:ListItem>
                <asp:ListItem Value="04">04</asp:ListItem>
                <asp:ListItem Value="05">05</asp:ListItem>
                <asp:ListItem Value="06">06</asp:ListItem>
                <asp:ListItem Value="07">07</asp:ListItem>
                <asp:ListItem Value="08">08</asp:ListItem>
                <asp:ListItem Value="09">09</asp:ListItem>
                <asp:ListItem Value="10">10</asp:ListItem>
                <asp:ListItem Value="11">11</asp:ListItem>
                <asp:ListItem Value="12">12</asp:ListItem>
            </asp:DropDownList>
        </div>
        <div class="divtxtBox  LNOrient" style="width: 60px">
            <asp:DropDownList ID="ddlEndTime2Min" runat="server" CssClass=" ddlStype"  TabIndex="19"
                Width="45px">
                <asp:ListItem Value="" Text="-"></asp:ListItem>
                <asp:ListItem Value="00">00</asp:ListItem>
                <asp:ListItem Value="05">05</asp:ListItem>
                <asp:ListItem Value="10">10</asp:ListItem>
                <asp:ListItem Value="15">15</asp:ListItem>
                <asp:ListItem Value="20">20</asp:ListItem>
                <asp:ListItem Value="25">25</asp:ListItem>
                <asp:ListItem Value="30">30</asp:ListItem>
                <asp:ListItem Value="35">35</asp:ListItem>
                <asp:ListItem Value="40">40</asp:ListItem>
                <asp:ListItem Value="45">45</asp:ListItem>
                <asp:ListItem Value="50">50</asp:ListItem>
                <asp:ListItem Value="55">55</asp:ListItem>
                <asp:ListItem Value="60">60</asp:ListItem>
            </asp:DropDownList>
        </div>
        <div class="divtxtBox  LNOrient" style="width: 60px">
            <asp:DropDownList ID="ddlEndTime2AMPM" runat="server" CssClass=" ddlStype"  TabIndex="20"
                Width="45px">
                <asp:ListItem Value="" Text="-"></asp:ListItem>
                <asp:ListItem Value="AM">AM</asp:ListItem>
                <asp:ListItem Value="PM">PM</asp:ListItem>
            </asp:DropDownList>
        </div>
        </div>
        <div class="divClear_10">
        </div>
<%--        
        <div class="divClear_10">
        </div>--%>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblStartTime3">
                Break3 Start
            </div>
            <div class="divtxtBox  LNOrient" style="width: 60px">
                <asp:DropDownList ID="ddlStartTime3Hr" runat="server" CssClass=" ddlStype"  TabIndex="21"
                    Width="45px">
                    <asp:ListItem Value="" Text="-"></asp:ListItem>
                    <asp:ListItem Value="01">01</asp:ListItem>
                    <asp:ListItem Value="02">02</asp:ListItem>
                    <asp:ListItem Value="03">03</asp:ListItem>
                    <asp:ListItem Value="04">04</asp:ListItem>
                    <asp:ListItem Value="05">05</asp:ListItem>
                    <asp:ListItem Value="06">06</asp:ListItem>
                    <asp:ListItem Value="07">07</asp:ListItem>
                    <asp:ListItem Value="08">08</asp:ListItem>
                    <asp:ListItem Value="09">09</asp:ListItem>
                    <asp:ListItem Value="10">10</asp:ListItem>
                    <asp:ListItem Value="11">11</asp:ListItem>
                    <asp:ListItem Value="12">12</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="divtxtBox  LNOrient" style="width: 60px">
                <asp:DropDownList ID="ddlStartTime3Min" runat="server" CssClass=" ddlStype"  TabIndex="22"
                    Width="45px">
                    <asp:ListItem Value="" Text="-"></asp:ListItem>
                    <asp:ListItem Value="00">00</asp:ListItem>
                    <asp:ListItem Value="05">05</asp:ListItem>
                    <asp:ListItem Value="10">10</asp:ListItem>
                    <asp:ListItem Value="15">15</asp:ListItem>
                    <asp:ListItem Value="20">20</asp:ListItem>
                    <asp:ListItem Value="25">25</asp:ListItem>
                    <asp:ListItem Value="30">30</asp:ListItem>
                    <asp:ListItem Value="35">35</asp:ListItem>
                    <asp:ListItem Value="40">40</asp:ListItem>
                    <asp:ListItem Value="45">45</asp:ListItem>
                    <asp:ListItem Value="50">50</asp:ListItem>
                    <asp:ListItem Value="55">55</asp:ListItem>
                    <asp:ListItem Value="60">60</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="divtxtBox  LNOrient" style="width: 60px">
                <asp:DropDownList ID="ddlStartTime3AMPM" runat="server" CssClass=" ddlStype"  TabIndex="23"
                    Width="45px">
                    <asp:ListItem Value="" Text="-"></asp:ListItem>
                    <asp:ListItem Value="AM">AM</asp:ListItem>
                    <asp:ListItem Value="PM">PM</asp:ListItem>
                </asp:DropDownList>
            </div>

            <div class="lblBox LNOrient" style="width: 200px" id="lblEndTime3">
            Break3 End
        </div>
        <div class="divtxtBox  LNOrient" style="width: 60px">
            <asp:DropDownList ID="ddlEndTime3Hr" runat="server" CssClass=" ddlStype"  TabIndex="24"
                Width="45px">
                <asp:ListItem Value="" Text="-"></asp:ListItem>
                <asp:ListItem Value="01">01</asp:ListItem>
                <asp:ListItem Value="02">02</asp:ListItem>
                <asp:ListItem Value="03">03</asp:ListItem>
                <asp:ListItem Value="04">04</asp:ListItem>
                <asp:ListItem Value="05">05</asp:ListItem>
                <asp:ListItem Value="06">06</asp:ListItem>
                <asp:ListItem Value="07">07</asp:ListItem>
                <asp:ListItem Value="08">08</asp:ListItem>
                <asp:ListItem Value="09">09</asp:ListItem>
                <asp:ListItem Value="10">10</asp:ListItem>
                <asp:ListItem Value="11">11</asp:ListItem>
                <asp:ListItem Value="12">12</asp:ListItem>
            </asp:DropDownList>
        </div>
        <div class="divtxtBox  LNOrient" style="width: 60px">
            <asp:DropDownList ID="ddlEndTime3Min" runat="server" CssClass=" ddlStype"  TabIndex="25"
                Width="45px">
                <asp:ListItem Value="" Text="-"></asp:ListItem>
                <asp:ListItem Value="00">00</asp:ListItem>
                <asp:ListItem Value="05">05</asp:ListItem>
                <asp:ListItem Value="10">10</asp:ListItem>
                <asp:ListItem Value="15">15</asp:ListItem>
                <asp:ListItem Value="20">20</asp:ListItem>
                <asp:ListItem Value="25">25</asp:ListItem>
                <asp:ListItem Value="30">30</asp:ListItem>
                <asp:ListItem Value="35">35</asp:ListItem>
                <asp:ListItem Value="40">40</asp:ListItem>
                <asp:ListItem Value="45">45</asp:ListItem>
                <asp:ListItem Value="50">50</asp:ListItem>
                <asp:ListItem Value="55">55</asp:ListItem>
                <asp:ListItem Value="60">60</asp:ListItem>
            </asp:DropDownList>
        </div>
        <div class="divtxtBox  LNOrient" style="width: 60px">
            <asp:DropDownList ID="ddlEndTime3AMPM" runat="server" CssClass=" ddlStype"  TabIndex="26"
                Width="45px">
                <asp:ListItem Value="" Text="-"></asp:ListItem>
                <asp:ListItem Value="AM">AM</asp:ListItem>
                <asp:ListItem Value="PM">PM</asp:ListItem>
            </asp:DropDownList>
        </div>
        </div>
        <div class="divClear_10">
        </div>
<%--        
        <div class="divClear_10">
        </div>--%>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                            ValidationGroup="DataSave" TabIndex="11" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="12" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="13" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="14" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table width="100%">
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/HR/HRChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
