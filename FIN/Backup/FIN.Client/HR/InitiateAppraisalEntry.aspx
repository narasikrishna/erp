﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="InitiateAppraisalEntry.aspx.cs" Inherits="FIN.Client.HR.Initiate_AppraisalEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 800px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
           
            <div class="lblBox LNOrient" style="width: 200px" id="lblDepartment">
                Department
            </div>
            <div class="divtxtBox  LNOrient" style="width: 155px">
                <asp:DropDownList ID="ddlDepartment" TabIndex="1" CssClass="validate[] RequiredField ddlStype"
                    runat="server" AutoPostBack="True">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
             <div class="lblBox LNOrient" style="width: 195px" id="lblJob">
                Job
            </div>
            <div class="divtxtBox  LNOrient" style="width: 155px">
                <asp:DropDownList ID="ddlJob" TabIndex="2" CssClass="validate[required] RequiredField ddlStype"
                     runat="server" AutoPostBack="True" 
                    onselectedindexchanged="ddlJob_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
       
        <div class="divClear_10">
        </div>
         <div class="divRowContainer">
         <div class="lblBox LNOrient" style="width: 200px" id="lblAppraisal">
                Appraisal
            </div>
            <div class="divtxtBox  LNOrient" style="width: 155px">
                <asp:DropDownList ID="ddlAppraisal" TabIndex="3" CssClass="validate[] RequiredField ddlStype"
                     runat="server" AutoPostBack="True" 
                    onselectedindexchanged="ddlAppraisal_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
             <div class="colspace  LNOrient" >
                &nbsp</div>
             <div class="lblBox LNOrient" style="width: 195px" id="lblPeriodStartDate">
                Period Date
            </div>
            <div class="divtxtBox  LNOrient" style="width: 155px">
            <asp:TextBox runat="server" ID="txtPeriodStartDate" TabIndex="4" CssClass="validate[,custom[ReqDateDDMMYYY],,dateRange[g1]]  txtBox"></asp:TextBox>
            <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender2" TargetControlID="txtPeriodStartDate" OnClientDateSelectionChanged="checkDate"/>
            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars="/"
                FilterType="Numbers,Custom" TargetControlID="txtPeriodStartDate" />
        </div>
          </div> 
        <div class="divClear_10">
        </div>
    <div class="LNOrient">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"  DataKeyNames="EMP_NO,Employee_Name" OnRowCancelingEdit="gvData_RowCancelingEdit" OnRowCommand="gvData_RowCommand" 
                Width="600px"  ShowFooter="True">
                <Columns>
                    <asp:TemplateField HeaderText="Employee Number">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlEmployeeNumber" TabIndex="5" runat="server" CssClass="EntryFont RequiredField ddlStype"
                                Width="100%">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlEmployeeNumber" TabIndex="5" runat="server" CssClass="EntryFont RequiredField ddlStype"
                                Width="100%">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblType" CssClass="adminFormFieldHeading" runat="server" Text='<%# Eval("EMP_NO") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Employee Name">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlEmployeeName" TabIndex="6" runat="server" CssClass="EntryFont RequiredField ddlStype"
                                Width="100%">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlEmployeeName" TabIndex="6" runat="server" CssClass="EntryFont RequiredField ddlStype"
                                Width="100%">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblGroupName" CssClass="adminFormFieldHeading" runat="server" Text='<%# Eval("Employee_Name") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Select">
                        <EditItemTemplate>
                            <asp:CheckBox ID="CheckBoxSelect" TabIndex="7" runat="server" Width="100%" Checked='<%# Convert.ToBoolean(Eval("ENABLED_FLAG")) %>' ></asp:CheckBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:CheckBox ID="CheckBoxSelect" TabIndex="7" runat="server" Width="100%"></asp:CheckBox>
                        </FooterTemplate>
                        <ItemTemplate>
                             <asp:CheckBox ID="CheckBoxSelect" TabIndex="7" runat="server" Width="100%" Checked='<%# Convert.ToBoolean(Eval("ENABLED_FLAG")) %>' ></asp:CheckBox>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" TabIndex="8"
                            onclick="btnSave_Click" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" TabIndex="9" Text="Delete" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" TabIndex="10" Text="Cancel" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" TabIndex="11" Text="Back" CssClass="btn" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table>
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" CssClass="button" ID="btnYes" Text="Yes" Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
    
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/HR/HRChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
