﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.BLL.AP;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.BLL;
using VMVServices.Web;

namespace FIN.Client.HR
{
    public partial class ResumeEntry : PageBase
    {
        HR_RESUME_DB hR_RESUME_DB = new HR_RESUME_DB();
        System.Collections.SortedList slControls = new System.Collections.SortedList();
        //string ProReturn = null;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                //pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                //pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
            }

            if (VMVServices.Web.Utils.AddAllowed != FINAppConstants.Y && Master.Mode == FINAppConstants.Add)
            {
                btnSave.Visible = false;
            }

        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                //ddlWeightUOM.Enabled = false;
                //ddlLengthUOM.Enabled = false;
                //ddlAreaUOM.Enabled = false;
                //ddlVolumeUOM.Enabled = false;


                Startup();
                FillComboBox();

                EntityData = null;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    using (IRepository<HR_RESUME_DB> userCtx = new DataRepository<HR_RESUME_DB>())
                    {
                        hR_RESUME_DB = userCtx.Find(r =>
                            (r.RES_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = hR_RESUME_DB;
                    txtName.Text = hR_RESUME_DB.RES_NAME;
                    txtTitle.Text = hR_RESUME_DB.RES_TITLE;
                    ddlFileFormat.SelectedValue = hR_RESUME_DB.RES_FILE_FORMAT;
                    txtPrimarySkills.Text = hR_RESUME_DB.RES_PRIMARY_SKILLS;
                    txtSecSkills.Text = hR_RESUME_DB.RES_SEC_SKILLS;
                    if (hR_RESUME_DB.RES_LAST_CONTACT_DT != null)
                    {
                        txtLastContactDate.Text = DBMethod.ConvertDateToString(hR_RESUME_DB.RES_LAST_CONTACT_DT.ToString());
                    }
                    txtComment.Text = hR_RESUME_DB.RES_COMMENTS;
                   







                    chkActive.Checked = hR_RESUME_DB.RES_IS_WORKING == FINAppConstants.EnabledFlag ? true : false;
                   
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>

        private void FillComboBox()
        {

            Lookup_BLL.GetLookUpValues(ref ddlFileFormat, "RES_FILE_FORMAT");



        }

        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    hR_RESUME_DB = (HR_RESUME_DB)EntityData;
                }

                hR_RESUME_DB.RES_NAME = txtName.Text;
                hR_RESUME_DB.RES_TITLE = txtTitle.Text;
                hR_RESUME_DB.RES_FILE_FORMAT = ddlFileFormat.SelectedValue.ToString();
                hR_RESUME_DB.RES_PRIMARY_SKILLS = txtPrimarySkills.Text;
                hR_RESUME_DB.RES_SEC_SKILLS = txtSecSkills.Text;
                string str_FileType = "";
                str_FileType = fpUploadFile.PostedFile.FileName.ToString();
                str_FileType = str_FileType.Substring(str_FileType.LastIndexOf('.') + 1, str_FileType.Length - str_FileType.LastIndexOf('.') - 1);
                string ResumeURL = ConfigurationManager.AppSettings["ResumeFileUrl"].ToString();
                fpUploadFile.SaveAs(Server.MapPath("ResumeURL" + hR_RESUME_DB.RES_ID + "." + str_FileType));
                if (txtLastContactDate.Text != string.Empty)
                {
                    hR_RESUME_DB.RES_LAST_CONTACT_DT = DBMethod.ConvertStringToDate(txtLastContactDate.Text.ToString());
                }
                hR_RESUME_DB.RES_COMMENTS = txtComment.Text;
               


                hR_RESUME_DB.ENABLED_FLAG = FINAppConstants.Y;











                hR_RESUME_DB.RES_IS_WORKING = chkActive.Checked == true ? FINAppConstants.EnabledFlag : FINAppConstants.DisabledFlag;
                




                //if (chkItemSerialControl.Checked == true)
                //{
                //    ddlServiceUOM.Enabled = true;
                //    iNV_ITEM_MASTER.SERVICE_DURATION_UOM = ddlServiceUOM.SelectedValue;
                //}
                //else
                //{
                //    ddlServiceUOM.Attributes.Add("disabled", "disabled");
                //}


                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    hR_RESUME_DB.MODIFIED_BY = this.LoggedUserName;
                    hR_RESUME_DB.MODIFIED_DATE = DateTime.Today;

                }
                else
                {
                    hR_RESUME_DB.RES_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.AP_0005.ToString(), false, true);
                    //iNV_ITEM_MASTER.WORKFLOW_COMPLETION_STATUS = "1";
                    hR_RESUME_DB.CREATED_BY = this.LoggedUserName;
                    hR_RESUME_DB.CREATED_DATE = DateTime.Today;

                }

                hR_RESUME_DB.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_RESUME_DB.RES_ID);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                AssignToBE();


                //emptyvalid();
                if (ErrorCollection.Count > 0)
                {
                    return;
                }


                // Duplicate Validation Through Backend Package PKG_VALIDATIONS



                //ProReturn = FIN.DAL.AP.Item_DAL.GetSPFOR_DUPLICATE_CHECK(iNV_ITEM_MASTER.ITEM_CODE,txtItemName.Text, iNV_ITEM_MASTER.ORG_ID, iNV_ITEM_MASTER.ITEM_ID);

                //if (ProReturn != string.Empty)
                //{
                //    if (ProReturn != "0")
                //    {
                //        ErrorCollection.Add("ITEMNAME", ProReturn);
                //        if (ErrorCollection.Count > 0)
                //        {
                //            return;
                //        }
                //    }
                //}


                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            DBMethod.SaveEntity<HR_RESUME_DB>(hR_RESUME_DB);
                            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                            break;
                        }
                    case FINAppConstants.Update:
                        {

                            DBMethod.SaveEntity<HR_RESUME_DB>(hR_RESUME_DB, true);
                            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                            break;

                        }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion




        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                DBMethod.DeleteEntity<HR_RESUME_DB>(hR_RESUME_DB);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        
        protected void btnCancel_Click(object sender, EventArgs e)
        {

        }

      





        //private void emptyvalid()
        //{
        //    if (hR_VACANCIES.VAC_DEPT_ID == null)
        //    {

        //        if (ddlDepartment.SelectedValue == string.Empty)
        //        {
        //            ErrorCollection.Add("Department", "Department Cannot be empty");
        //        }

        //    }
        //    if (hR_VACANCIES.VAC_DESIG_ID == null)
        //    {
        //        if (ddlDesignation.SelectedValue == string.Empty)

        //            ErrorCollection.Add("Designation", "Designation Cannot be empty");
        //        }
        //    }
        //    //if (hR_VACANCIES.VAC_TYPE > 0)
        //    //{

        //    //    if (ddlType.SelectedValue == string.Empty)
        //    //    {
        //    //        ErrorCollection.Add("DepartmentType", "Department Type Cannot be empty");
        //    //    }
        //    //}

        //    //if  > 0)
        //    //{
        //    //    if (ddlVolumeUOM.SelectedValue == string.Empty)
        //    //    {

        //    //        ErrorCollection.Add("VOLEMT", "Volume UOM Cannot be empty");
        //    //    }
        //    //}

        //}






    }





}
