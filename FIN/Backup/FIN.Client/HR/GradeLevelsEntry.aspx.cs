﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.BLL;
using FIN.BLL.HR;
using VMVServices.Web;

namespace FIN.Client.HR
{
    public partial class GradeLevelsEntry : PageBase
    {

        HR_GRADES_LEVEL_DTLS hR_GRADES_LEVEL_DTLS = new HR_GRADES_LEVEL_DTLS();
        DataTable dtGridData = new DataTable();
        DataTable dtgridvalid = new DataTable();
        GradeLevel_BLL GradeLevel_BLL = new GradeLevel_BLL();

        Boolean bol_rowVisiable;

        Boolean savedBool;
        string ProReturn = null;
        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("GL_PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));


            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
        }

        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>
        /// 

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>



        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();



                EntityData = null;

                dtGridData = FIN.BLL.HR.GradeLevel_BLL.getChildEntityDet(Master.StrRecordId);
                BindGrid(dtGridData);

                //if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                //{

                //    using (IRepository<HR_CATEGORIES> userCtx = new DataRepository<HR_CATEGORIES>())
                //    {
                //        hR_CATEGORIES = userCtx.Find(r =>
                //            (r.CATEGORY_ID == Master.StrRecordId)
                //            ).SingleOrDefault();
                //    }

                //    EntityData = hR_CATEGORIES;

                //}

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("GRADELEVEL_ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                var tmpChildEntity = new List<Tuple<object, string>>();

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    hR_GRADES_LEVEL_DTLS = new HR_GRADES_LEVEL_DTLS();
                    if (dtGridData.Rows[iLoop]["GRADE_LEVEL_ID"].ToString() != "0")
                    {
                        using (IRepository<HR_GRADES_LEVEL_DTLS> userCtx = new DataRepository<HR_GRADES_LEVEL_DTLS>())
                        {
                            hR_GRADES_LEVEL_DTLS = userCtx.Find(r =>
                                (r.GRADE_LEVEL_ID == dtGridData.Rows[iLoop]["GRADE_LEVEL_ID"].ToString())
                                ).SingleOrDefault();
                        }
                    }

                    hR_GRADES_LEVEL_DTLS.GRADE_ID = dtGridData.Rows[iLoop]["GRADE_ID"].ToString();
                    hR_GRADES_LEVEL_DTLS.PAY_ELEMENT_ID = dtGridData.Rows[iLoop]["PAY_ELEMENT_ID"].ToString();
                    hR_GRADES_LEVEL_DTLS.LOW_LIMIT = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop]["LOW_LIMIT"].ToString());
                    hR_GRADES_LEVEL_DTLS.HIGH_LIMIT = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop]["HIGH_LIMIT"].ToString());
                    hR_GRADES_LEVEL_DTLS.INCREMENT_AMT = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop]["INCREMENT_AMT"].ToString());
                    hR_GRADES_LEVEL_DTLS.AVERAGE_AMT = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop]["AVERAGE_AMT"].ToString());

                    if (dtGridData.Rows[iLoop]["EFFECTIVE_FROM_DT"] != DBNull.Value)
                    {
                        hR_GRADES_LEVEL_DTLS.EFFECTIVE_FROM_DT = DateTime.Parse(dtGridData.Rows[iLoop]["EFFECTIVE_FROM_DT"].ToString());
                    }

                    if (dtGridData.Rows[iLoop]["EFFECTIVE_TO_DT"] != DBNull.Value)
                    {
                        hR_GRADES_LEVEL_DTLS.EFFECTIVE_TO_DT = DateTime.Parse(dtGridData.Rows[iLoop]["EFFECTIVE_TO_DT"].ToString());
                    }
                    else
                    {
                        hR_GRADES_LEVEL_DTLS.EFFECTIVE_TO_DT = null;
                    }
                    hR_GRADES_LEVEL_DTLS.ORG_ID = VMVServices.Web.Utils.OrganizationID;

                    if (dtGridData.Rows[iLoop][FINColumnConstants.ENABLED_FLAG].ToString().ToUpper() == "TRUE")
                    {
                        hR_GRADES_LEVEL_DTLS.ENABLED_FLAG = FINAppConstants.Y;
                    }
                    else
                    {
                        hR_GRADES_LEVEL_DTLS.ENABLED_FLAG = FINAppConstants.N;
                    }

                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        tmpChildEntity.Add(new Tuple<object, string>(hR_GRADES_LEVEL_DTLS, "D"));
                    }
                    else
                    {

                        // Duplicate Validation Through Backend Package PKG_VALIDATIONS

                        //ProReturn = FIN.DAL.HR.Category_DAL.GetSPFOR_ERR_MGR_CATEGORY(hR_CATEGORIES.CATEGORY_CODE, hR_CATEGORIES.CATEGORY_ID);

                        //if (ProReturn != string.Empty)
                        //{
                        //    if (ProReturn != "0")
                        //    {
                        //        ErrorCollection.Add("CATEGORY", ProReturn);
                        //        if (ErrorCollection.Count > 0)
                        //        {
                        //            return;
                        //        }
                        //    }
                        //}



                        if (dtGridData.Rows[iLoop]["GRADE_LEVEL_ID"].ToString() != "0")
                        {
                            hR_GRADES_LEVEL_DTLS.GRADE_LEVEL_ID = dtGridData.Rows[iLoop]["GRADE_LEVEL_ID"].ToString();
                            hR_GRADES_LEVEL_DTLS.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_GRADES_LEVEL_DTLS.GRADE_LEVEL_ID);
                            hR_GRADES_LEVEL_DTLS.MODIFIED_BY = this.LoggedUserName;
                            hR_GRADES_LEVEL_DTLS.MODIFIED_DATE = DateTime.Today;
                            DBMethod.SaveEntity<HR_GRADES_LEVEL_DTLS>(hR_GRADES_LEVEL_DTLS, true);
                            //tmpChildEntity.Add(new Tuple<object, string>(hR_CATEGORIES, "U"));
                            savedBool = true;
                        }
                        else
                        {

                            hR_GRADES_LEVEL_DTLS.GRADE_LEVEL_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_083.ToString(), false, true);
                            hR_GRADES_LEVEL_DTLS.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_GRADES_LEVEL_DTLS.GRADE_LEVEL_ID);
                            hR_GRADES_LEVEL_DTLS.CREATED_BY = this.LoggedUserName;
                            hR_GRADES_LEVEL_DTLS.CREATED_DATE = DateTime.Today;
                            DBMethod.SaveEntity<HR_GRADES_LEVEL_DTLS>(hR_GRADES_LEVEL_DTLS);
                            savedBool = true;
                        }


                    }

                }

                VMVServices.Web.Utils.SavedRecordId = "";
                //switch (Master.Mode)
                //{
                //    case FINAppConstants.Add:
                //        {
                //            FIN.BLL.GL.AccountCodes_BLL.SavePCEntity<INV_TRANSFER_HDR, INV_TRANSFER_DTL>(iNV_TRANSFER_HDR, tmpChildEntity, iNV_TRANSFER_DTL);
                //            break;
                //        }
                //    case FINAppConstants.Update:
                //        {

                //            FIN.BLL.GL.AccountCodes_BLL.SavePCEntity<INV_TRANSFER_HDR, INV_TRANSFER_DTL>(iNV_TRANSFER_HDR, tmpChildEntity, iNV_TRANSFER_DTL, true);
                //            break;

                //        }
                //}



            }
            catch (Exception ex)
            {
                ErrorCollection.Add("GRDLVL_ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();
                DropDownList ddlCategory = tmpgvr.FindControl("ddlCategory") as DropDownList;
                DropDownList ddlJob = tmpgvr.FindControl("ddlJob") as DropDownList;
                DropDownList ddlPosition = tmpgvr.FindControl("ddlPosition") as DropDownList;
                DropDownList ddlGrade = tmpgvr.FindControl("ddlGrade") as DropDownList;
                DropDownList ddlElement = tmpgvr.FindControl("ddlElement") as DropDownList;
                //  GradeLevel_BLL.fn_GetGradeNM(ref ddlGrade);
                GradeLevel_BLL.fn_getElement(ref ddlElement);
                Categories_BLL.fn_getCategory(ref ddlCategory, Master.Mode);

                if (gvData.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                {
                    ddlCategory.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.CATEGORY_ID].ToString();
                    filljob(tmpgvr);
                    ddlJob.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.JOB_ID].ToString();
                    fillPosition(tmpgvr);
                    ddlPosition.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.POSITION_ID].ToString();
                    fillgrade(tmpgvr);
                    ddlGrade.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.GRADE_ID].ToString();
                    ddlElement.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.PAY_ELEMENT_ID].ToString();


                }


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("GRDLVL_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //protected void btnSave_Click(object sender, EventArgs e)
        //{
        //    try
        //    {

        //        ErrorCollection.Clear();
        //        AssignToBE();
        //        ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session[FINSessionConstants.GridData], "Grade Level");

        //        if (ErrorCollection.Count > 0)
        //        {
        //            return;
        //        }

        //        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);

        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorCollection.Add("Save", ex.Message);
        //    }
        //    finally
        //    {
        //        if (ErrorCollection.Count > 0)
        //        {
        //            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
        //            // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
        //        }
        //    }

        //}
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session[FINSessionConstants.GridData], "Grade Levels ");

                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                AssignToBE();

                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        #endregion

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();

                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;

                if (dtData.Rows.Count > 0)
                {
                    if (dtData.Rows[0]["INCREMENT_AMT"].ToString().Length > 0)
                    {
                        dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("INCREMENT_AMT", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("INCREMENT_AMT"))));
                    }
                    if (dtData.Rows[0]["AVERAGE_AMT"].ToString().Length > 0)
                    {
                        dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("AVERAGE_AMT", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("AVERAGE_AMT"))));
                    }
                    dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("LOW_LIMIT", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("LOW_LIMIT"))));
                    dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("HIGH_LIMIT", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("HIGH_LIMIT"))));
                    dtData.AcceptChanges();
                }

                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr["ENABLED_FLAG"] = "FALSE";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            try
            {
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = -1;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("GL_ROW_CNCL_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion

        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("GL_ROW_CMD", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {


            System.Collections.SortedList slControls = new System.Collections.SortedList();
            DropDownList ddlCategory = gvr.FindControl("ddlCategory") as DropDownList;
            DropDownList ddlJob = gvr.FindControl("ddlJob") as DropDownList;
            DropDownList ddlPosition = gvr.FindControl("ddlPosition") as DropDownList;
            DropDownList ddlGrade = gvr.FindControl("ddlGrade") as DropDownList;
            DropDownList ddlElement = gvr.FindControl("ddlElement") as DropDownList;
            TextBox txtlowlimit = gvr.FindControl("txtlowlimit") as TextBox;
            TextBox txtHighlimit = gvr.FindControl("txtHighlimit") as TextBox;
            TextBox txtincrementamt = gvr.FindControl("txtincrementamt") as TextBox;
            TextBox txtAvgamt = gvr.FindControl("txtAvgamt") as TextBox;
            TextBox dtpStartDate = gvr.FindControl("dtpStartDate") as TextBox;
            TextBox dtpEndDate = gvr.FindControl("dtpEndDate") as TextBox;
            CheckBox chkact = gvr.FindControl("chkact") as CheckBox;


            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList["GRADE_LEVEL_ID"] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }

            //Duplicate Validation through Backend
            ErrorCollection.Clear();
            slControls[0] = ddlCategory;
            //slControls[1] = ddlJob;
            //slControls[2] = ddlPosition;
            //slControls[3] = ddlGrade;
            slControls[1] = ddlElement;
            slControls[2] = txtlowlimit;
            slControls[3] = txtHighlimit;
            slControls[4] = txtincrementamt;
            slControls[5] = txtAvgamt;
            slControls[6] = dtpStartDate;
            slControls[7] = dtpStartDate;

            

            Dictionary<string, string> Prop_File_Data;

            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));

            string strCtrlTypes = FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.DATE_TIME; 

            //string strMessage = Prop_File_Data["Category_P"] + " ~ " + Prop_File_Data["Job_P"] + " ~ " + Prop_File_Data["Job_P"] + " ~ " + Prop_File_Data["Grade"] + " ~ " + Prop_File_Data["Element_P"] + " ~ " + Prop_File_Data["Low_Limit_P"] + " ~ " + Prop_File_Data["High_Limit_P"] + " ~ " + Prop_File_Data["Increment_Amount_P"] + " ~ " + Prop_File_Data["Average_Amount_P"] + " ~ " + Prop_File_Data["From_Date_P"] + "";



            string strMessage = "Category ~ Element ~ Low Limit ~ High Limit ~ Increment Amount ~ Average Amount ~ From Date ~ From Date";


            if (dtpEndDate.Text.Trim().Length > 0)
            {
                slControls[8] = dtpEndDate;
                strCtrlTypes += "~" + FINAppConstants.DATE_RANGE_VALIDATE;
                strMessage += "~" + " To Date ";
            }

            ErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);
            if (ErrorCollection.Count > 0)
            {
                return drList;
            }
            ErrorCollection.Remove("job");
            ErrorCollection.Remove("ddlPosition");
            ErrorCollection.Remove("ddlGrade");

            if (ddlJob.SelectedValue.ToString() == FINAppConstants.intialRowValueField && ddlJob.SelectedItem.Text.ToString() == FINAppConstants.intialRowTextField)
            {
                ErrorCollection.Add("job", "Job cannot be empty");
                return drList;
            }
            if (ddlPosition.SelectedValue.ToString() == FINAppConstants.intialRowValueField && ddlPosition.SelectedItem.Text.ToString() == FINAppConstants.intialRowTextField)
            {
                ErrorCollection.Add("ddlPosition", "Position cannot be empty");
                return drList;
            }
            if (ddlGrade.SelectedValue.ToString() == FINAppConstants.intialRowValueField && ddlGrade.SelectedItem.Text.ToString() == FINAppConstants.intialRowTextField)
            {
                ErrorCollection.Add("ddlGrade", "Grade cannot be empty");
                return drList;
            }

            ErrorCollection.Remove("txtlowlimit");
            if (CommonUtils.ConvertStringToDecimal(txtlowlimit.Text) > CommonUtils.ConvertStringToDecimal(txtHighlimit.Text))
            {
                ErrorCollection.Add("txtlowlimit", "Low limit cannot be greater than the High Limit");
                return drList;
            }

            ErrorCollection.Remove("GradeLevels");

            dtgridvalid = FIN.BLL.HR.GradeLevel_BLL.fn_GetGradeDtlsForDuplicate(ddlGrade.SelectedValue.ToString(), ddlPosition.SelectedValue.ToString(), ddlElement.SelectedValue.ToString(), ddlJob.SelectedValue.ToString(), ddlCategory.SelectedValue.ToString(), drList["GRADE_LEVEL_ID"].ToString());
            if (dtgridvalid.Rows.Count > 0)
            {
                if (int.Parse(dtgridvalid.Rows[0]["COUNTS"].ToString()) == 1)
                {
                    ErrorCollection.Add("GradeLevels", "Details Already Exists");
                    if (ErrorCollection.Count > 0)
                    {
                        return drList;
                    }
                }
            }

           
            //slControls[0] = ddlGrade;
            //slControls[1] = ddlElement;
            //slControls[2] = txtlowlimit;
            //slControls[3] = txtHighlimit;
            //slControls[4] = txtincrementamt;
            //slControls[5] = txtAvgamt;
            //slControls[6] = dtpStartDate;
            //slControls[7] = dtpStartDate;
            //slControls[8] = dtpEndDate;

            //Dictionary<string, string> Prop_File_Data;
            //Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));

            //ErrorCollection.Clear();
            //string strCtrlTypes = "DropDownList~DropDownList~TextBox~TextBox~TextBox~TextBox~TextBox~DateTime~DateRangeValidate";
            //strMessage = Prop_File_Data["Grade_P"] + " ~ " + Prop_File_Data["Element_P"] + " ~ " + Prop_File_Data["Low_Limit_P"] + " ~ " + Prop_File_Data["High_Limit_P"] + " ~ " + Prop_File_Data["Increment_Amount_P"] + " ~ " + Prop_File_Data["Average_Amount_P"] + " ~ " + Prop_File_Data["Start_Date_P"] + " ~ " + Prop_File_Data["Start_Date_P"] + " ~ " + Prop_File_Data["End_Date_P"] + "";
            ////string strMessage = "Gade ~ Element ~ Low Limit ~ High Limit ~ Increment Amount ~ Average Amount ~ Start Date ~ Start Date ~ End Date";
            //ErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);
            //if (ErrorCollection.Count > 0)
            //    return drList;


            //string strCondition = "CATEGORY_CODE='" + txtcode.Text + "'";
            //strMessage = FINMessageConstatns.RecordAlreadyExists;
            //ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            //if (ErrorCollection.Count > 0)
            //{
            //    return drList;
            //}



            //  DataTable dtchilcat = new DataTable();

            //if (gvData.EditIndex >= 0)
            //{
            //    hR_CATEGORIES.CATEGORY_ID = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.CATEGORY_ID].ToString();

            //    dtchilcat = null;
            //    dtchilcat = DBMethod.ExecuteQuery(FIN.DAL.HR.Category_DAL.Get_Childdataof_Category(hR_CATEGORIES.CATEGORY_ID)).Tables[0];
            //    if (dtchilcat.Rows.Count > 0)
            //    {
            //        if (chkact.Checked)
            //        {


            //        }
            //        else
            //        {
            //            ErrorCollection.Add("chkflg", "Cannot change the Active flag. Child record found");
            //            return drList;
            //        }
            //    }
            //}

            drList["CATEGORY_ID"] = ddlCategory.SelectedValue;
            drList["CATEGORY_DESC"] = ddlCategory.SelectedItem.Text;
            drList["JOB_ID"] = ddlJob.SelectedValue;
            drList["JOB_DESC"] = ddlJob.SelectedItem.Text;
            drList["POSITION_ID"] = ddlPosition.SelectedValue;
            drList["POSITION_DESC"] = ddlPosition.SelectedItem.Text;


            drList["GRADE_ID"] = ddlGrade.SelectedValue;
            drList["GRADE_DESC"] = ddlGrade.SelectedItem.Text;
            drList["PAY_ELEMENT_ID"] = ddlElement.SelectedValue;
            drList["PAY_ELEMENT"] = ddlElement.SelectedItem.Text;

            drList["LOW_LIMIT"] = txtlowlimit.Text;
            drList["HIGH_LIMIT"] = txtHighlimit.Text;
            drList["INCREMENT_AMT"] = txtincrementamt.Text;
            drList["AVERAGE_AMT"] = txtAvgamt.Text;


            if (dtpStartDate.Text.ToString().Length > 0)
            {
                drList["EFFECTIVE_FROM_DT"] = DBMethod.ConvertStringToDate(dtpStartDate.Text.ToString());
            }

            if (dtpEndDate.Text.ToString().Length > 0)
            {

                drList["EFFECTIVE_TO_DT"] = DBMethod.ConvertStringToDate(dtpEndDate.Text.ToString());
            }
            else
            {
                drList["EFFECTIVE_TO_DT"] = DBNull.Value;
            }
            if (chkact.Checked)
            {
                drList[FINColumnConstants.ENABLED_FLAG] = "TRUE";
            }
            else
            {
                drList[FINColumnConstants.ENABLED_FLAG] = "FALSE";
            }



            drList[FINColumnConstants.DELETED] = FINAppConstants.N;


            return drList;



        }



        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("GL_ROW_UP", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("GL_ROW_DEL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("GL_ROW_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("GL_RowCreated", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }
                    if (e.Row.RowType == DataControlRowType.DataRow)
                    {
                        e.Row.Cells[5].HorizontalAlign = HorizontalAlign.Right;
                        e.Row.Cells[6].HorizontalAlign = HorizontalAlign.Right;
                        e.Row.Cells[7].HorizontalAlign = HorizontalAlign.Right;
                        e.Row.Cells[8].HorizontalAlign = HorizontalAlign.Right;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("GL_RDB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {

                    hR_GRADES_LEVEL_DTLS.GRADE_LEVEL_ID = dtGridData.Rows[iLoop]["GRADE_LEVEL_ID"].ToString();
                    DBMethod.DeleteEntity<HR_GRADES_LEVEL_DTLS>(hR_GRADES_LEVEL_DTLS);
                }


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("GL_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {

        }

        protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            filljob(gvr);
        }

        private void filljob(GridViewRow gvr)
        {
            DropDownList ddlJob = gvr.FindControl("ddlJob") as DropDownList;
            DropDownList ddlCategory = gvr.FindControl("ddlCategory") as DropDownList;

            Jobs_BLL.fn_getJob4Category(ref ddlJob, ddlCategory.SelectedValue);

        }

        protected void ddlJob_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            fillPosition(gvr);
        }

        private void fillPosition(GridViewRow gvr)
        {
            DropDownList ddlJob = gvr.FindControl("ddlJob") as DropDownList;
            DropDownList ddlPosition = gvr.FindControl("ddlPosition") as DropDownList;

            Position_BLL.fn_GetPositionNameBasedonJob(ref ddlPosition, ddlJob.SelectedValue);

        }

        protected void ddlPosition_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            fillgrade(gvr);
        }

        private void fillgrade(GridViewRow gvr)
        {
            DropDownList ddlPosition = gvr.FindControl("ddlPosition") as DropDownList;
            DropDownList ddlGrade = gvr.FindControl("ddlGrade") as DropDownList;
            Grades_BLL.fn_getGradeName4Position(ref ddlGrade, ddlPosition.SelectedValue);


        }

        protected void txtHighlimit_TextChanged(object sender, EventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            CheckAmtLimtAndAssing(gvr);


        }
        private void CheckAmtLimtAndAssing(GridViewRow gvr)
        {
            TextBox txt_lowlimit = (TextBox)gvr.FindControl("txtlowlimit");
            TextBox txt_Highlimit = (TextBox)gvr.FindControl("txtHighlimit");
            TextBox txt_incrementamt = (TextBox)gvr.FindControl("txtincrementamt");
            if (txt_Highlimit.Text.ToString().Length > 0)
            {
                if (txt_lowlimit.Text.ToString().Length > 0)
                {
                    if (double.Parse(txt_Highlimit.Text) < double.Parse(txt_lowlimit.Text))
                    {
                        ErrorCollection.Add("InvalidAmtLimit", "Higher Limit Always Greater then Lower Limit ");
                        txt_Highlimit.Text = "";
                        txt_Highlimit.Focus();
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    }
                    else
                    {
                        //txt_incrementamt.Text = (double.Parse(txt_Highlimit.Text) - double.Parse(txt_lowlimit.Text)).ToString();
                    }
                }
            }
        }


    }
}