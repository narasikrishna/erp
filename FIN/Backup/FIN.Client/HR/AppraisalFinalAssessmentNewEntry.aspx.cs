﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.BLL;
using FIN.BLL.HR;
using VMVServices.Web;

namespace FIN.Client.HR
{
    public partial class AppraisalFinalAssessmentNewEntry : PageBase
    {

        HR_APPR_FNL_ASESMNT_HDR hR_APPR_FNL_ASESMNT_HDR = new HR_APPR_FNL_ASESMNT_HDR();
        HR_APPR_FNL_ASESMNT_DTL hR_APPR_FNL_ASESMNT_DTL = new HR_APPR_FNL_ASESMNT_DTL();



        DataTable dtGridData = new DataTable();
        Boolean bol_rowVisiable;
        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CML_PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));


            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
        }

        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>
        /// 

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void FillComboBox()
        {
            FIN.BLL.HR.AppraisalDefineKRAEntry_BLL.GetAraisal(ref ddlAppraisal);



        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();

                EntityData = null;

                dtGridData = DBMethod.ExecuteQuery(FIN.DAL.HR.AppraisalFinalAssessment_DAL.GetAraisalFinalAssessmentDtl(Master.StrRecordId)).Tables[0];

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    using (IRepository<HR_APPR_FNL_ASESMNT_HDR> userCtx = new DataRepository<HR_APPR_FNL_ASESMNT_HDR>())
                    {
                        hR_APPR_FNL_ASESMNT_HDR = userCtx.Find(r =>
                            (r.APFA_HDR_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = hR_APPR_FNL_ASESMNT_HDR;

                    ddlAppraisal.SelectedValue = hR_APPR_FNL_ASESMNT_HDR.ASSIGN_HDR_ID;

                }

                BindGrid(dtGridData);


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CML_ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    hR_APPR_FNL_ASESMNT_HDR = (HR_APPR_FNL_ASESMNT_HDR)EntityData;
                }


                hR_APPR_FNL_ASESMNT_HDR.ASSIGN_HDR_ID = ddlAppraisal.SelectedValue;

                hR_APPR_FNL_ASESMNT_HDR.ORG_ID = VMVServices.Web.Utils.OrganizationID;

                hR_APPR_FNL_ASESMNT_HDR.ENABLED_FLAG = FINAppConstants.Y;


                // hR_APPR_FNL_ASESMNT_HDR.WORKFLOW_COMPLETION_STATUS = FINAppConstants.Y;



                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    hR_APPR_FNL_ASESMNT_HDR.MODIFIED_BY = this.LoggedUserName;
                    hR_APPR_FNL_ASESMNT_HDR.MODIFIED_DATE = DateTime.Today;

                }
                else
                {
                    hR_APPR_FNL_ASESMNT_HDR.APFA_HDR_ID = FINSP.GetSPFOR_SEQCode("HR_110".ToString(), false, true);

                    hR_APPR_FNL_ASESMNT_HDR.CREATED_BY = this.LoggedUserName;
                    hR_APPR_FNL_ASESMNT_HDR.CREATED_DATE = DateTime.Today;


                }
                hR_APPR_FNL_ASESMNT_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_APPR_FNL_ASESMNT_HDR.APFA_HDR_ID);

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                var tmpChildEntity = new List<Tuple<object, string>>();

                if (gvData.Rows.Count > 0)
                {
                    for (int iLoop = 0; iLoop < gvData.Rows.Count; iLoop++)
                    {
                        HR_APPR_FNL_ASESMNT_DTL hR_APPR_FNL_ASESMNT_DTL = new HR_APPR_FNL_ASESMNT_DTL();

                        if (gvData.DataKeys[iLoop].Values["APFA_DTL_ID"].ToString() != "0")
                        {
                            using (IRepository<HR_APPR_FNL_ASESMNT_DTL> userCtx = new DataRepository<HR_APPR_FNL_ASESMNT_DTL>())
                            {
                                hR_APPR_FNL_ASESMNT_DTL = userCtx.Find(r =>
                                    (r.APFA_DTL_ID == gvData.DataKeys[iLoop].Values["APFA_DTL_ID"].ToString())
                                    ).SingleOrDefault();
                            }
                        }

                        TextBox txtHrRat = (TextBox)gvData.Rows[iLoop].FindControl("txtHrRat");
                        TextBox txtchairmanrat = (TextBox)gvData.Rows[iLoop].FindControl("txtchairmanrat");
                        TextBox txtTotRat = (TextBox)gvData.Rows[iLoop].FindControl("txtTotRat");


                        hR_APPR_FNL_ASESMNT_DTL.EMP_ID = gvData.DataKeys[iLoop].Values["EMP_ID"].ToString();
                        hR_APPR_FNL_ASESMNT_DTL.FINAL_RATING = decimal.Parse(gvData.DataKeys[iLoop].Values["FINAL_RATING"].ToString());
                        hR_APPR_FNL_ASESMNT_DTL.FINAL_HR_CEO_RATING = decimal.Parse(txtHrRat.Text);
                        hR_APPR_FNL_ASESMNT_DTL.FINAL_CHAIRMAN_RATING = decimal.Parse(txtchairmanrat.Text);
                        hR_APPR_FNL_ASESMNT_DTL.FINAL_TOTAL_RATING = decimal.Parse(txtTotRat.Text);
                        hR_APPR_FNL_ASESMNT_DTL.WORKFLOW_COMPLETION_STATUS = FINAppConstants.Y;
                        hR_APPR_FNL_ASESMNT_DTL.APFA_HDR_ID = hR_APPR_FNL_ASESMNT_HDR.APFA_HDR_ID;

                        hR_APPR_FNL_ASESMNT_DTL.ENABLED_FLAG = FINAppConstants.Y;
                        hR_APPR_FNL_ASESMNT_DTL.ORG_ID = VMVServices.Web.Utils.OrganizationID;

                        if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                        {

                            tmpChildEntity.Add(new Tuple<object, string>(hR_APPR_FNL_ASESMNT_DTL, "D"));
                        }
                        else
                        {
                            if (dtGridData.Rows[iLoop]["APFA_DTL_ID"].ToString() != "0")
                            {
                                hR_APPR_FNL_ASESMNT_DTL.APFA_DTL_ID = dtGridData.Rows[iLoop]["APFA_DTL_ID"].ToString();
                                hR_APPR_FNL_ASESMNT_DTL.MODIFIED_BY = this.LoggedUserName;
                                hR_APPR_FNL_ASESMNT_DTL.MODIFIED_DATE = DateTime.Today;

                                tmpChildEntity.Add(new Tuple<object, string>(hR_APPR_FNL_ASESMNT_DTL, "U"));

                            }
                            else
                            {

                                hR_APPR_FNL_ASESMNT_DTL.APFA_DTL_ID = FINSP.GetSPFOR_SEQCode("HR_110_D".ToString(), false, true);
                                hR_APPR_FNL_ASESMNT_DTL.CREATED_BY = this.LoggedUserName;
                                hR_APPR_FNL_ASESMNT_DTL.CREATED_DATE = DateTime.Today;

                                tmpChildEntity.Add(new Tuple<object, string>(hR_APPR_FNL_ASESMNT_DTL, "A"));
                            }
                        }

                    }


                }

                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            FIN.BLL.HR.CompetencyLinks_BLL.SavePCEntity<HR_APPR_FNL_ASESMNT_HDR, HR_APPR_FNL_ASESMNT_DTL>(hR_APPR_FNL_ASESMNT_HDR, tmpChildEntity, hR_APPR_FNL_ASESMNT_DTL);
                            break;
                        }
                    case FINAppConstants.Update:
                        {

                            FIN.BLL.HR.CompetencyLinks_BLL.SavePCEntity<HR_APPR_FNL_ASESMNT_HDR, HR_APPR_FNL_ASESMNT_DTL>(hR_APPR_FNL_ASESMNT_HDR, tmpChildEntity, hR_APPR_FNL_ASESMNT_DTL, true);
                            break;

                        }
                }



            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CML_ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                //DropDownList ddlLotNo = tmpgvr.FindControl("ddlLotNo") as DropDownList;
                //WarehouseTransfer_BLL.fn_getLotNo(ref ddlLotNo);


                //if (gvData.EditIndex >= 0)
                //{
                //    ddlLotNo.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.LOT_ID].ToString();


                //}
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CML_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                AssignToBE();

                //ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session[FINSessionConstants.GridData], "Competency Link");

                //if (ErrorCollection.Count > 0)
                //{
                //    return;
                //}

                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        #endregion

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    // dr["ENABLED_FLAG"] = "FALSE";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CMl_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }




        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CML_RDB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                // COURSE_GROUP.COURSE_GROUP_ID = short.Parse(Master.RecordID.ToString());
                DBMethod.DeleteEntity<HR_APPR_FNL_ASESMNT_HDR>(hR_APPR_FNL_ASESMNT_HDR);
                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATADELETED);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("cML_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }




        private void filltotrat(GridViewRow gvr)
        {
            try
            {
                DataTable dtTotrat = new DataTable();

               // GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;

                TextBox txtHrRat = gvr.FindControl("txtHrRat") as TextBox;
                TextBox txtchairmanrat = gvr.FindControl("txtchairmanrat") as TextBox;
                TextBox txtTotRat = gvr.FindControl("txtTotRat") as TextBox;

                dtTotrat = DBMethod.ExecuteQuery(AppraisalFinalAssessment_DAL.GetTotalRating(decimal.Parse(gvData.DataKeys[gvr.RowIndex].Values["FINAL_RATING"].ToString()), decimal.Parse(txtHrRat.Text), decimal.Parse(txtchairmanrat.Text))).Tables[0];

                txtTotRat.Text = dtTotrat.Rows[0]["TOT_RAT"].ToString();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("val_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void txtchairmanrat_TextChanged1(object sender, EventArgs e)
        {
            try
            {
           // filltotrat(sender);
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
             filltotrat(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("TS_ATO45B", ex.Message);
            }
        }

        protected void ddlAppraisal_SelectedIndexChanged(object sender, EventArgs e)
        {
            dtGridData = DBMethod.ExecuteQuery(FIN.DAL.HR.AppraisalFinalAssessment_DAL.GetAraisalFinalAssessmentDtlforAraisal(ddlAppraisal.SelectedValue)).Tables[0];
            BindGrid(dtGridData);
        }




    }
}