﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.DAL.GL;
using FIN.BLL;
using FIN.BLL.GL;
using FIN.BLL.HR;
using VMVServices.Web;


namespace FIN.Client.HR
{
    public partial class CourseEntry : PageBase
    {

        HR_TRM_COURSE hR_TRM_COURSE = new HR_TRM_COURSE();
        HR_TRM_SUBJECT hR_TRM_SUBJECT = new HR_TRM_SUBJECT();
        string ProReturn = null;

        Boolean saveBool = false;

        DataTable dtGridData = new DataTable();
        Boolean bol_rowVisiable;
        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("COM_PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));


            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
        }

        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>
        /// 

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void FillComboBox()
        {
            // FIN.BLL.Lookup_BLL.GetLookUpValues(ref ddlCompetencyType, "COMT");
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();
                EntityData = null;

                //  txtEndDate.Text = DBMethod.ConvertDateToString(DateTime.Now.ToShortDateString());

                dtGridData = FIN.BLL.HR.Course_BLL.getChildEntityDet(Master.StrRecordId);
                BindGrid(dtGridData);

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    using (IRepository<HR_TRM_COURSE> userCtx = new DataRepository<HR_TRM_COURSE>())
                    {
                        hR_TRM_COURSE = userCtx.Find(r =>
                            (r.COURSE_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = hR_TRM_COURSE;

                    txtCourse.Text = hR_TRM_COURSE.COURSE_DESC;
                    //  txtGroup.Text = hR_TRM_COURSE.COURSE_GROUP;
                    txtDuration.Text = hR_TRM_COURSE.COURSE_DURATION.ToString();



                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("COM_ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    hR_TRM_COURSE = (HR_TRM_COURSE)EntityData;
                }


                hR_TRM_COURSE.COURSE_DESC = txtCourse.Text;
                //hR_TRM_COURSE.COURSE_GROUP = txtGroup.Text;
                hR_TRM_COURSE.COURSE_DURATION = decimal.Parse(txtDuration.Text);


                hR_TRM_COURSE.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                hR_TRM_COURSE.COURSE_ORG_ID = VMVServices.Web.Utils.OrganizationID;

                // hR_TRM_COURSE.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;


                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    hR_TRM_COURSE.MODIFIED_BY = this.LoggedUserName;
                    hR_TRM_COURSE.MODIFIED_DATE = DateTime.Today;

                }
                else
                {
                    hR_TRM_COURSE.COURSE_ID = FINSP.GetSPFOR_SEQCode("HR_052_M".ToString(), false, true);
                    //hR_TRM_COURSE.PK_ID = DBMethod.GetPrimaryKeyValue(FINSequenceConstant.hR_TRM_COURSE_SEQ);
                    hR_TRM_COURSE.CREATED_BY = this.LoggedUserName;
                    hR_TRM_COURSE.CREATED_DATE = DateTime.Today;

                }
                hR_TRM_COURSE.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_TRM_COURSE.COURSE_ID);

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }


                var tmpChildEntity = new List<Tuple<object, string>>();

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    hR_TRM_SUBJECT = new HR_TRM_SUBJECT();
                    if (dtGridData.Rows[iLoop]["SUBJECT_ID"].ToString() != "0" && dtGridData.Rows[iLoop]["SUBJECT_ID"].ToString() != string.Empty)
                    {
                        using (IRepository<HR_TRM_SUBJECT> userCtx = new DataRepository<HR_TRM_SUBJECT>())
                        {
                            hR_TRM_SUBJECT = userCtx.Find(r =>
                                (r.SUBJECT_ID == dtGridData.Rows[iLoop]["SUBJECT_ID"].ToString())
                                ).SingleOrDefault();
                        }
                    }

                    //hR_TRM_SUBJECT.COM_LINE_NUM = (iLoop + 1);
                    hR_TRM_SUBJECT.SUB_CODE = dtGridData.Rows[iLoop]["SUB_CODE"].ToString();
                    hR_TRM_SUBJECT.SUBJECT_DESC = dtGridData.Rows[iLoop]["SUBJECT_DESC"].ToString();
                    //  hR_TRM_SUBJECT.FACILITY_ID = dtGridData.Rows[iLoop]["FACILITY_ID"].ToString();

                    hR_TRM_SUBJECT.COURSE_ID = hR_TRM_COURSE.COURSE_ID;

                    hR_TRM_SUBJECT.WORKFLOW_COMPLETION_STATUS = "1";
                    hR_TRM_SUBJECT.ENABLED_FLAG = FINAppConstants.Y;

                    //if (dtGridData.Rows[iLoop]["INSTRUCTOR_FLAG"].ToString().ToUpper() == "TRUE")
                    //{
                    //    hR_TRM_SUBJECT.INSTRUCTOR_FLAG = FINAppConstants.Y;
                    //}
                    //else
                    //{
                    //    hR_TRM_SUBJECT.INSTRUCTOR_FLAG = FINAppConstants.N;
                    //}
                    //if (dtGridData.Rows[iLoop]["CBT_FLAG"].ToString().ToUpper() == "TRUE")
                    //{
                    //    hR_TRM_SUBJECT.CBT_FLAG = FINAppConstants.Y;
                    //}
                    //else
                    //{
                    //    hR_TRM_SUBJECT.CBT_FLAG = FINAppConstants.N;
                    //}

                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {

                        tmpChildEntity.Add(new Tuple<object, string>(hR_TRM_SUBJECT, "D"));
                    }
                    else
                    {

                        // Duplicate Validation Through Backend Package PKG_VALIDATIONS

                        //ProReturn = FIN.DAL.HR.Competency_DAL.GetSPFOR_DUPLICATE_CHECK(hR_TRM_SUBJECT.COM_LEVEL_DESC, hR_TRM_SUBJECT.COM_LINE_ID);

                        //if (ProReturn != string.Empty)
                        //{
                        //    if (ProReturn != "0")
                        //    {
                        //        ErrorCollection.Add("COMPETENCY", ProReturn);
                        //        if (ErrorCollection.Count > 0)
                        //        {
                        //            return;
                        //        }
                        //    }
                        //}

                        if (dtGridData.Rows[iLoop]["SUBJECT_ID"].ToString() != "0" && dtGridData.Rows[iLoop]["SUBJECT_ID"].ToString() != string.Empty)
                        {
                            hR_TRM_SUBJECT.SUBJECT_ID = dtGridData.Rows[iLoop]["SUBJECT_ID"].ToString();
                            hR_TRM_SUBJECT.MODIFIED_BY = this.LoggedUserName;
                            hR_TRM_SUBJECT.MODIFIED_DATE = DateTime.Today;

                            tmpChildEntity.Add(new Tuple<object, string>(hR_TRM_SUBJECT, "U"));

                        }
                        else
                        {

                            hR_TRM_SUBJECT.SUBJECT_ID = FINSP.GetSPFOR_SEQCode("HR_052_D".ToString(), false, true);
                            hR_TRM_SUBJECT.CREATED_BY = this.LoggedUserName;
                            hR_TRM_SUBJECT.CREATED_DATE = DateTime.Today;
                            //DBMethod.SaveEntity<GL_ACCT_CODE_SEGMENTS>(gL_ACCT_CODE_SEGMENTS);
                            tmpChildEntity.Add(new Tuple<object, string>(hR_TRM_SUBJECT, "A"));
                        }
                    }

                }

                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            FIN.BLL.HR.Competency_BLL.SavePCEntity<HR_TRM_COURSE, HR_TRM_SUBJECT>(hR_TRM_COURSE, tmpChildEntity, hR_TRM_SUBJECT);
                            saveBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {
                            FIN.BLL.HR.Competency_BLL.SavePCEntity<HR_TRM_COURSE, HR_TRM_SUBJECT>(hR_TRM_COURSE, tmpChildEntity, hR_TRM_SUBJECT, true);
                            saveBool = true;
                            break;
                        }
                }



            }
            catch (Exception ex)
            {
                ErrorCollection.Add("COurs_ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                // DropDownList ddlFaility = tmpgvr.FindControl("ddlFaility") as DropDownList;
                //FIN.BLL.HR.Course_BLL.fn_GetFacilitydtls(ref ddlFaility);


                // if (gvData.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                // {
                //     ddlFaility.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["FACILITY_ID"].ToString();


                // }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("COURSE_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session[FINSessionConstants.GridData], "Subject Details ");

                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                AssignToBE();

                if (saveBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        #endregion

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    //dr["INSTRUCTOR_FLAG"] = "FALSE";
                    //dr["CBT_FLAG"] = "FALSE";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                //GridViewRow gvr = gvData.FooterRow;
                //FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("COurse_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            if (Session[FINSessionConstants.GridData] != null)
            {
                dtGridData = (DataTable)Session[FINSessionConstants.GridData];
            }
            gvData.EditIndex = -1;

            BindGrid(dtGridData);

        }
        #endregion

        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("COURSE_ROW_CMD", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {


            System.Collections.SortedList slControls = new System.Collections.SortedList();


            TextBox txtSubCode = gvr.FindControl("txtSubCode") as TextBox;
            TextBox txtDesc = gvr.FindControl("txtDesc") as TextBox;

            // DropDownList ddlFaility = gvr.FindControl("ddlFaility") as DropDownList;

            //CheckBox chkInstructor = gvr.FindControl("chkInstructor") as CheckBox;
            //CheckBox chkCBT = gvr.FindControl("chkCBT") as CheckBox;

            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList["SUBJECT_ID"] = "0";

            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }

            // txtlineno.Text = (txtlineno.Text + rowindex + 1).ToString();

            slControls[0] = txtSubCode;
            slControls[1] = txtDesc;
            // slControls[2] = ddlFaility;
            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));
            ErrorCollection.Clear();
            string strCtrlTypes = FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX;// +"~" + FINAppConstants.DROP_DOWN_LIST;
            string strMessage = Prop_File_Data["Subject_Code_P"] + " ~ " + Prop_File_Data["Description_P"];// +" ~ " + Prop_File_Data["Facility_P"] + "";
            //string strMessage = " Subject Code ~ Description ~ Facility";

            EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

            if (EmptyErrorCollection.Count > 0)
            {
                ErrorCollection = EmptyErrorCollection;
                return drList;
            }



            string strCondition = "SUB_CODE='" + txtSubCode.Text + "'";
            strMessage = FINMessageConstatns.RecordAlreadyExists;
            ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            if (ErrorCollection.Count > 0)
            {
                return drList;
            }
            ProReturn = FINSP.GetSPFOR_ERR_MGR_CATEGORY_TWO(Master.FormCode, drList["SUBJECT_ID"].ToString(), txtCourse.Text, "");
            if (ProReturn != string.Empty)
            {
                if (ProReturn != "0")
                {
                    ErrorCollection.Add("COURSE", ProReturn);
                    if (ErrorCollection.Count > 0)
                    {
                        return drList;
                    }
                }
            }


            drList["SUB_CODE"] = txtSubCode.Text;
            drList["SUBJECT_DESC"] = txtDesc.Text;
            //drList["FACILITY_ID"] = ddlFaility.SelectedValue;

            //if (ddlFaility.SelectedItem != null)
            //{
            //    drList["FACILITY_ID"] = ddlFaility.SelectedItem.Value;
            //    drList["FACILITY_DTL"] = ddlFaility.SelectedItem.Text;
            //}

            //if (chkInstructor.Checked)
            //{
            //    drList["INSTRUCTOR_FLAG"] = "TRUE";
            //}
            //else
            //{
            //    drList["INSTRUCTOR_FLAG"] = "FALSE";
            //}

            //if (chkCBT.Checked)
            //{
            //    drList["CBT_FLAG"] = "TRUE";
            //}
            //else
            //{
            //    drList["CBT_FLAG"] = "FALSE";
            //}

            drList[FINColumnConstants.DELETED] = FINAppConstants.N;


            return drList;

        }



        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("COURSE_ROW_UP", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("COURSE_ROW_DEL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                //GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                //FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("COURSE_ROW_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("COURSE_RowCreated", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("COURSE_RDB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                // COURSE_GROUP.COURSE_GROUP_ID = short.Parse(Master.RecordID.ToString());
                DBMethod.DeleteEntity<HR_TRM_COURSE>(hR_TRM_COURSE);
                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATADELETED);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("course_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }



    }
}