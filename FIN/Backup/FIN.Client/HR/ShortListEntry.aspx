﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="ShortListEntry.aspx.cs" Inherits="FIN.Client.HR.ShortListEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1000px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 190px" id="lblFinancialYear">
                Vacancy
            </div>
            <div class="divtxtBox  LNOrient" style="width: 515px">
                <asp:DropDownList ID="ddlVacancy" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    TabIndex="1">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 190px" id="lblDepartment">
                Required Technology/Skill
            </div>
            <div class="divtxtBox  LNOrient" style="width: 515px">
                <asp:TextBox ID="txtReqTech" TabIndex="2" MaxLength="250" runat="server" CssClass=" txtBox"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 190px" id="lblFromDate">
                Minimum Experience
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:TextBox ID="txtExpMin" TabIndex="3" MaxLength="4" runat="server" CssClass=" txtBox_N"></asp:TextBox>
            </div>
            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers,Custom"
                ValidChars="." TargetControlID="txtExpMin" />
            <div class="lblBox LNOrient" style="float: left; width: 190px" id="lblToDate">
                Maximum Experience
            </div>
            <div class="divtxtBox  LNOrient" style="width: 155px">
                <asp:TextBox ID="txtExpMax" TabIndex="4" MaxLength="4" runat="server" CssClass=" txtBox_N"></asp:TextBox>
            </div>
            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers,Custom"
                ValidChars="." TargetControlID="txtExpMax" />
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 190px" id="Div1">
                Minimum Age
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:TextBox ID="txtAgeMin" TabIndex="5" MaxLength="2" runat="server" CssClass=" txtBox_N"></asp:TextBox>
            </div>
            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Numbers,Custom"
                ValidChars="." TargetControlID="txtAgeMin" />
            <div class="lblBox LNOrient" style="float: left; width: 190px" id="Div2">
                Maximum Age
            </div>
            <div class="divtxtBox  LNOrient" style="width: 155px">
                <asp:TextBox ID="txtAgeMax" TabIndex="6" MaxLength="2" runat="server" CssClass=" txtBox_N"></asp:TextBox>
            </div>
            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Numbers,Custom"
                ValidChars="." TargetControlID="txtAgeMax" />
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 190px" id="Div4">
                Qualification
            </div>
            <div class="divtxtBox  LNOrient" style="width: 515px">
                <asp:TextBox ID="txtQualification" TabIndex="7" MaxLength="150" runat="server" CssClass=" txtBox"></asp:TextBox>
                <%--  <asp:DropDownList ID="ddlQualification" runat="server" CssClass="validate[required] RequiredField ddlStype"
                   TabIndex="1">
                </asp:DropDownList>--%>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 190px" id="Div3">
                Nationality
            </div>
            <div class="divtxtBox  LNOrient" style="width: 345px; height: 150px; overflow: auto;">
                <asp:CheckBoxList runat="server" ID="chkDropNation" Width="250px" CssClass="" TabIndex="8"
                    BorderWidth="3">
                </asp:CheckBoxList>
            </div>
            <div style="float: right; width: 400px" id="Div5">
                <asp:ImageButton ID="btnProcess" runat="server" ImageUrl="~/Images/Process.png" TabIndex="9"
                    OnClick="btnProcess_Click" Style="border: 0px;" />
                <%-- <asp:Button ID="btnProcess" runat="server" Width="110px" Text="Process" CssClass="btn"
                    OnClick="btnProcess_Click" TabIndex="9" /></div>--%>
            </div>
            <div class="divClear_10">
            </div>
            <div class="LNOrient">
                <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                    Width="100%" DataKeyNames="SHORT_LIST_DTL_ID,file_path" OnRowCancelingEdit="gvData_RowCancelingEdit"
                    OnRowCommand="gvData_RowCommand" OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound"
                    OnRowDeleting="gvData_RowDeleting" OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating"
                    ShowFooter="false">
                    <Columns>
                        <asp:TemplateField HeaderText="Applicant Id">
                            <ItemTemplate>
                                <asp:Label ID="lblAppId" Width="130px" runat="server" Text='<%# Eval("applicant_id") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Name">
                            <ItemTemplate>
                                <asp:Label ID="lblApplicantName" Width="130px" runat="server" Text='<%# Eval("applicant_name") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Contact Number">
                            <ItemTemplate>
                                <asp:Label ID="lblRequest" Width="130px" runat="server" Text='<%# Eval("contact_number") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Mail">
                            <ItemTemplate>
                                <asp:Label ID="lblReason" runat="server" Text='<%# Eval("email") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Total Experience">
                            <ItemTemplate>
                                <asp:Label ID="lblReasson" runat="server" Text='<%# Eval("tot_exp") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Resume">
                            <ItemTemplate>
                                <asp:HyperLink ID="hl_Resume" runat="server" Target="_download">View
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="">
                            <ItemTemplate>
                                <asp:CheckBox runat="server" ID="chkSelection" Checked='<%# Convert.ToBoolean(Eval("is_select")) %>' />
                            </ItemTemplate>
                            <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <%-- <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" TabIndex="17" runat="server" AlternateText="Edit"
                                CausesValidation="false" ToolTip="Edit" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                            <asp:ImageButton ID="ibtnDelete" TabIndex="17" runat="server" AlternateText="Delete"
                                CausesValidation="false" ToolTip="Delete" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibtnUpdate" TabIndex="17" runat="server" AlternateText="Update"
                                CommandName="Update" ToolTip="Update" Height="20px" ImageUrl="~/Images/Update.png" />
                            <asp:ImageButton ID="ibtnCancel" TabIndex="17" runat="server" AlternateText="Cancel"
                                CausesValidation="false" ToolTip="Cancel" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="ibtnInsert" TabIndex="18" runat="server" AlternateText="Add"
                                CommandName="FooterInsert" ToolTip="Add" ImageUrl="~/Images/Add.jpg" />
                        </FooterTemplate>
                        <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Center" />
                    </asp:TemplateField>--%>
                    </Columns>
                    <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                    <HeaderStyle CssClass="GridHeader" />
                    <AlternatingRowStyle CssClass="GrdAltRow" />
                </asp:GridView>
            </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table>
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/HR/HRChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
