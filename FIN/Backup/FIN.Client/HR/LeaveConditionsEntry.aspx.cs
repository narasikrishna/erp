﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.DAL.GL;
using FIN.BLL;
using FIN.BLL.GL;
using FIN.BLL.HR;
using VMVServices.Web;


namespace FIN.Client.HR
{
    public partial class LeaveConditionsEntry : PageBase
    {
        HR_LEAVE_CONDITIONS_HDR hR_LEAVE_CONDITIONS_HDR = new HR_LEAVE_CONDITIONS_HDR();
        HR_LEAVE_CONDITIONS_DTL hR_LEAVE_CONDITIONS_DTL = new HR_LEAVE_CONDITIONS_DTL();
        DataTable dtGridData = new DataTable();

        string ProReturn = null;
        Boolean bol_rowVisiable;
        Boolean savedBool;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }


        }

        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //  div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }


            UserRightsChecking();

        }
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        /// <summary>
        /// To assign the values to Controls and fetch the StrRecordId,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();
                EntityData = null;

                dtGridData = DBMethod.ExecuteQuery(FIN.DAL.HR.LeaveCondition_DAL.GetLeaveconditiondtls(Master.StrRecordId)).Tables[0];

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    using (IRepository<HR_LEAVE_CONDITIONS_HDR> userCtx = new DataRepository<HR_LEAVE_CONDITIONS_HDR>())
                    {
                        hR_LEAVE_CONDITIONS_HDR = userCtx.Find(r =>
                            (r.LC_HDR_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = hR_LEAVE_CONDITIONS_HDR;

                    ddlLeave.SelectedValue = hR_LEAVE_CONDITIONS_HDR.LC_LEAVE_ID;

                    txtFromDate.Text = DBMethod.ConvertDateToString(hR_LEAVE_CONDITIONS_HDR.EFFECTIVE_FROM_DT.ToString());
                    if (hR_LEAVE_CONDITIONS_HDR.EFFECTIVE_TO_DT != null)
                    {
                        txtToDate.Text = DBMethod.ConvertDateToString(hR_LEAVE_CONDITIONS_HDR.EFFECTIVE_TO_DT.ToString());
                    }

                }

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("LC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Course master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    hR_LEAVE_CONDITIONS_HDR = (HR_LEAVE_CONDITIONS_HDR)EntityData;
                }


                hR_LEAVE_CONDITIONS_HDR.LC_LEAVE_ID = ddlLeave.SelectedValue;
                hR_LEAVE_CONDITIONS_HDR.EFFECTIVE_FROM_DT = DBMethod.ConvertStringToDate(txtFromDate.Text.ToString());
                if (txtToDate.Text.ToString().Trim().Length > 0)
                {
                    hR_LEAVE_CONDITIONS_HDR.EFFECTIVE_TO_DT = DBMethod.ConvertStringToDate(txtToDate.Text.ToString());
                }
                else
                {
                    hR_LEAVE_CONDITIONS_HDR.EFFECTIVE_TO_DT = null;
                }


                hR_LEAVE_CONDITIONS_HDR.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                hR_LEAVE_CONDITIONS_HDR.LC_ORG_ID = VMVServices.Web.Utils.OrganizationID;



                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != string.Empty)
                {
                    hR_LEAVE_CONDITIONS_HDR.MODIFIED_BY = this.LoggedUserName;
                    hR_LEAVE_CONDITIONS_HDR.MODIFIED_DATE = DateTime.Today;

                }
                else
                {

                    hR_LEAVE_CONDITIONS_HDR.CREATED_BY = this.LoggedUserName;
                    hR_LEAVE_CONDITIONS_HDR.CREATED_DATE = DateTime.Today;
                    hR_LEAVE_CONDITIONS_HDR.LC_HDR_ID = FINSP.GetSPFOR_SEQCode("HR_094_M".ToString(), false, true);

                }
                hR_LEAVE_CONDITIONS_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_LEAVE_CONDITIONS_HDR.LC_HDR_ID);

                //Save Detail Table


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                ErrorCollection = CommonUtils.IsEmptyGrid(dtGridData, "Leave Condition");
                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                var tmpChildEntity = new List<Tuple<object, string>>();


                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    hR_LEAVE_CONDITIONS_DTL = new HR_LEAVE_CONDITIONS_DTL();
                    if (dtGridData.Rows[iLoop]["LC_DTL_ID"].ToString() != "0")
                    {
                        using (IRepository<HR_LEAVE_CONDITIONS_DTL> userCtx = new DataRepository<HR_LEAVE_CONDITIONS_DTL>())
                        {
                            hR_LEAVE_CONDITIONS_DTL = userCtx.Find(r =>
                                (r.LC_DTL_ID == dtGridData.Rows[iLoop]["LC_DTL_ID"].ToString())
                                ).SingleOrDefault();
                        }
                    }



                    if (dtGridData.Rows[iLoop][FINColumnConstants.LOOKUP_ID] != DBNull.Value)
                    {
                        hR_LEAVE_CONDITIONS_DTL.LC_TYPE = dtGridData.Rows[iLoop][FINColumnConstants.LOOKUP_ID].ToString();
                    }
                    if (dtGridData.Rows[iLoop]["LC_HIGH_VALUE"] != DBNull.Value)
                    {
                        hR_LEAVE_CONDITIONS_DTL.LC_HIGH_VALUE = decimal.Parse(dtGridData.Rows[iLoop]["LC_HIGH_VALUE"].ToString());
                    }
                    else
                    {
                        hR_LEAVE_CONDITIONS_DTL.LC_HIGH_VALUE = null;
                    }
                    if (dtGridData.Rows[iLoop]["LC_LOW_VALUE"] != DBNull.Value)
                    {
                        hR_LEAVE_CONDITIONS_DTL.LC_LOW_VALUE = decimal.Parse(dtGridData.Rows[iLoop]["LC_LOW_VALUE"].ToString());
                    }
                    else
                    {
                        hR_LEAVE_CONDITIONS_DTL.LC_LOW_VALUE = null;
                    }
                    if (dtGridData.Rows[iLoop]["LC_VALUE"] != DBNull.Value)
                    {
                        hR_LEAVE_CONDITIONS_DTL.LC_VALUE = decimal.Parse(dtGridData.Rows[iLoop]["LC_VALUE"].ToString());
                    }
                    else
                    {
                        hR_LEAVE_CONDITIONS_DTL.LC_VALUE = null;
                    }
                    // hR_LEAVE_CONDITIONS_DTL.LC_FORMULA = dtGridData.Rows[iLoop]["LC_FORMULA"].ToString();

                    if (dtGridData.Rows[iLoop]["CONDITION_ID"] != DBNull.Value)
                    {
                        hR_LEAVE_CONDITIONS_DTL.LC_CONDITION = dtGridData.Rows[iLoop]["CONDITION_ID"].ToString();
                    }
                    if (dtGridData.Rows[iLoop]["EFFECTIVE_FROM_DT"] != DBNull.Value)
                    {
                        hR_LEAVE_CONDITIONS_DTL.EFFECTIVE_FROM_DT = DateTime.Parse(dtGridData.Rows[iLoop]["EFFECTIVE_FROM_DT"].ToString());
                    }

                    if (dtGridData.Rows[iLoop]["EFFECTIVE_TO_DT"] != DBNull.Value)
                    {
                        hR_LEAVE_CONDITIONS_DTL.EFFECTIVE_TO_DT = DateTime.Parse(dtGridData.Rows[iLoop]["EFFECTIVE_TO_DT"].ToString());

                    }
                    else
                    {
                        hR_LEAVE_CONDITIONS_DTL.EFFECTIVE_TO_DT = null;
                    }

                    if (dtGridData.Rows[iLoop][FINColumnConstants.ENABLED_FLAG].ToString().ToUpper() == "TRUE")
                    {
                        hR_LEAVE_CONDITIONS_DTL.ENABLED_FLAG = FINAppConstants.Y;
                    }
                    else
                    {
                        hR_LEAVE_CONDITIONS_DTL.ENABLED_FLAG = FINAppConstants.N;
                    }

                    hR_LEAVE_CONDITIONS_DTL.WORKFLOW_COMPLETION_STATUS = "1";
                    hR_LEAVE_CONDITIONS_DTL.LC_HDR_ID = hR_LEAVE_CONDITIONS_HDR.LC_HDR_ID;

                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        hR_LEAVE_CONDITIONS_DTL.LC_DTL_ID = dtGridData.Rows[iLoop]["LC_DTL_ID"].ToString();
                        tmpChildEntity.Add(new Tuple<object, string>(hR_LEAVE_CONDITIONS_DTL, FINAppConstants.Delete));
                    }
                    else
                    {

                        if (dtGridData.Rows[iLoop]["LC_DTL_ID"].ToString() != "0" && dtGridData.Rows[iLoop]["LC_DTL_ID"].ToString() != string.Empty)
                        {
                            hR_LEAVE_CONDITIONS_DTL.LC_DTL_ID = dtGridData.Rows[iLoop]["LC_DTL_ID"].ToString();
                            hR_LEAVE_CONDITIONS_DTL.MODIFIED_BY = this.LoggedUserName;
                            hR_LEAVE_CONDITIONS_DTL.MODIFIED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(hR_LEAVE_CONDITIONS_DTL, FINAppConstants.Update));

                        }
                        else
                        {
                            hR_LEAVE_CONDITIONS_DTL.LC_DTL_ID = FINSP.GetSPFOR_SEQCode("HR_094_D".ToString(), false, true);
                            hR_LEAVE_CONDITIONS_DTL.CREATED_BY = this.LoggedUserName;
                            hR_LEAVE_CONDITIONS_DTL.CREATED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(hR_LEAVE_CONDITIONS_DTL, FINAppConstants.Add));
                        }
                    }

                }



                // Duplicate Validation Through Backend Package PKG_VALIDATIONS

                //ProReturn = FIN.DAL.HR.AppraisalDefineKRA_DAL.GetSPFOR_DUPLICATE_CHECK(hR_LEAVE_CONDITIONS_HDR.ASSIGN_APP_ID, hR_LEAVE_CONDITIONS_HDR.ASSIGN_DEPT_ID, hR_LEAVE_CONDITIONS_HDR.ASSIGN_JOB_ID, hR_LEAVE_CONDITIONS_HDR.ASSIGN_HDR_ID);

                //if (ProReturn != string.Empty)
                //{
                //    if (ProReturn != "0")
                //    {
                //        ErrorCollection.Add("APPDEFKRA", ProReturn);
                //        if (ErrorCollection.Count > 0)
                //        {
                //            return;
                //        }
                //    }
                //}


                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            CommonUtils.SavePCEntity<HR_LEAVE_CONDITIONS_HDR, HR_LEAVE_CONDITIONS_DTL>(hR_LEAVE_CONDITIONS_HDR, tmpChildEntity, hR_LEAVE_CONDITIONS_DTL);
                            savedBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {
                            CommonUtils.SavePCEntity<HR_LEAVE_CONDITIONS_HDR, HR_LEAVE_CONDITIONS_DTL>(hR_LEAVE_CONDITIONS_HDR, tmpChildEntity, hR_LEAVE_CONDITIONS_DTL, true);
                            savedBool = true;
                            break;
                        }
                }
                if (ErrorCollection.Count > 0)
                {
                    return;
                }


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_ATB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }


        private void FillComboBox()
        {

            LeaveDefinition_BLL.GetLeave(ref ddlLeave);
        }



        /// <summary>
        /// Bind the records into grid voew
        /// </summary>
        /// <param name="dtData">Contains the database entities and correspoding records which is used in the grid view</param>

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr["ENABLED_FLAG"] = "FALSE";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("WRI_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>


        #endregion

        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>

        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = -1;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Row_cnl", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }

                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    else
                    {
                        dtGridData.Rows.Add(drList);
                        BindGrid(dtGridData);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Row_Cmd", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }



        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {

            System.Collections.SortedList slControls = new System.Collections.SortedList();
            DropDownList ddlLeavecondtyp = gvr.FindControl("ddlLeavecondtyp") as DropDownList;
            DropDownList ddlCondititon = gvr.FindControl("ddlCondititon") as DropDownList;

            TextBox txtHighval = gvr.FindControl("txtHighval") as TextBox;
            TextBox txtlowval = gvr.FindControl("txtlowval") as TextBox;
            TextBox txtVal = gvr.FindControl("txtVal") as TextBox;
            TextBox txtFormula = gvr.FindControl("txtFormula") as TextBox;
            TextBox dtpStartDate = gvr.FindControl("dtpStartDate") as TextBox;
            TextBox dtpEndDate = gvr.FindControl("dtpEndDate") as TextBox;
            CheckBox chkact = gvr.FindControl("chkact") as CheckBox;

            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList["LC_DTL_ID"] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }
            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));
            ErrorCollection.Clear();

            string strCtrlTypes = string.Empty;
            string strMessage = string.Empty;


            slControls[0] = ddlLeavecondtyp;
            strCtrlTypes = FINAppConstants.DROP_DOWN_LIST;
            strMessage = Prop_File_Data["Leave_Condition_Type_P"];
            if (ddlLeavecondtyp.SelectedItem.Text == "SLAB")
            {

                slControls[1] = txtHighval;
                slControls[2] = txtlowval;
                slControls[3] = txtVal;
                slControls[4] = dtpStartDate;
                slControls[5] = dtpStartDate;
                slControls[6] = dtpEndDate;

                strCtrlTypes += "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.DATE_TIME + "~" + FINAppConstants.DATE_RANGE_VALIDATE;
                strMessage += " ~ " + Prop_File_Data["High_Value_P"] + " ~ " + Prop_File_Data["Low_Value_P"] + " ~ " + Prop_File_Data["Value_P"] + " ~ " + Prop_File_Data["Effective_Date_P"] + " ~ " + Prop_File_Data["Effective_Date_P"] + " ~ " + Prop_File_Data["End_Date_P"] + "";

                if (CommonUtils.ConvertStringToDecimal(txtlowval.Text) > CommonUtils.ConvertStringToDecimal(txtHighval.Text))
                {
                    ErrorCollection.Add("checkval", "High value must be grater than low value");
                    return drList;
                }

            }

            else if (ddlLeavecondtyp.SelectedItem.Text == "CONDITION")
            {


                slControls[1] = txtVal;
                slControls[2] = ddlCondititon;
                slControls[3] = dtpStartDate;
                slControls[4] = dtpStartDate;
                slControls[5] = dtpEndDate;

                strCtrlTypes += "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.DROP_DOWN_LIST + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.DATE_TIME + "~" + FINAppConstants.DATE_RANGE_VALIDATE;
                strMessage += " ~ " + Prop_File_Data["Value_P"] + " ~ " + Prop_File_Data["Condition_P"] + "~" + Prop_File_Data["Effective_Date_P"] + " ~ " + Prop_File_Data["Effective_Date_P"] + " ~ " + Prop_File_Data["End_Date_P"] + "";
            }
            else if (ddlLeavecondtyp.SelectedItem.Text == "N-A")
            {


                slControls[1] = dtpStartDate;
                slControls[2] = dtpStartDate;
                slControls[3] = dtpEndDate;
                strCtrlTypes += "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.DATE_TIME + "~" + FINAppConstants.DATE_RANGE_VALIDATE;
                strMessage += " ~ " + Prop_File_Data["Effective_Date_P"] + " ~ " + Prop_File_Data["Effective_Date_P"] + " ~ " + Prop_File_Data["End_Date_P"] + "";
            }


            ErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);
            if (ErrorCollection.Count > 0)
                return drList;



            bool bol_ValuLimit = true;
            decimal dbl_StartValue = 0;
            decimal dbl_EndValue = 0;
            for (int gLoop = 0; gLoop < dt_tmp.Rows.Count; gLoop++)
            {
                dbl_StartValue = CommonUtils.ConvertStringToDecimal(dt_tmp.Rows[gLoop]["LC_LOW_VALUE"].ToString());
                dbl_EndValue = CommonUtils.ConvertStringToDecimal(dt_tmp.Rows[gLoop]["LC_HIGH_VALUE"].ToString());

                if (((CommonUtils.ConvertStringToDecimal(txtlowval.Text) >= dbl_StartValue) && (CommonUtils.ConvertStringToDecimal(txtlowval.Text) <= dbl_EndValue)))
                {
                    bol_ValuLimit = false;
                    break;
                }
                if (((CommonUtils.ConvertStringToDecimal(txtHighval.Text) >= dbl_StartValue) && (CommonUtils.ConvertStringToDecimal(txtHighval.Text) <= dbl_EndValue)))
                {
                    bol_ValuLimit = false;
                    break;
                }

                if (((CommonUtils.ConvertStringToDecimal(txtlowval.Text) <= dbl_StartValue) && (CommonUtils.ConvertStringToDecimal(txtHighval.Text) >= dbl_EndValue)))
                {
                    bol_ValuLimit = false;
                    break;
                }

            }

            if (!bol_ValuLimit)
            {
                ErrorCollection.Add("InvalidValueLimit", "Invalid Value Limit ");
                return drList;
            }

            //string strCondition = "(" + txtlowval.Text + ">=LC_LOW_VALUE AND " + txtlowval.Text + " <=LC_HIGH_VALUE) or (" + txtlowval.Text + "<LC_LOW_VALUE OR " + txtlowval.Text + ">LC_HIGH_VALUE)";
            //strMessage = FINMessageConstatns.RecordAlreadyExists;
            //ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            //if (ErrorCollection.Count > 0)
            //{

            //    return drList;
            //}
            //strCondition = "(" + txtHighval.Text + ">=LC_LOW_VALUE AND " + txtHighval.Text + " <=LC_HIGH_VALUE) or (" + txtHighval.Text + "<LC_LOW_VALUE OR " + txtHighval.Text + ">LC_HIGH_VALUE)";
            //strMessage = FINMessageConstatns.RecordAlreadyExists;
            //ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            //if (ErrorCollection.Count > 0)
            //{

            //    return drList;
            //}


            if (ddlLeavecondtyp.SelectedItem != null)
            {
                drList[FINColumnConstants.LOOKUP_ID] = ddlLeavecondtyp.SelectedItem.Value;
                drList[FINColumnConstants.LOOKUP_NAME] = ddlLeavecondtyp.SelectedItem.Text;
            }

            if (ddlCondititon.SelectedItem != null)
            {
                drList["CONDITION_ID"] = ddlCondititon.SelectedItem.Value;
                drList["CONDITION_NAME"] = ddlCondititon.SelectedItem.Text;
            }
            if (txtHighval.Text != "")
            {
                drList["LC_HIGH_VALUE"] = txtHighval.Text;
            }
            else
            {
                drList["LC_HIGH_VALUE"] = DBNull.Value;
            }
            if (txtlowval.Text != "")
            {
                drList["LC_LOW_VALUE"] = txtlowval.Text;
            }
            else
            {
                drList["LC_LOW_VALUE"] = DBNull.Value;
            }
            if (txtVal.Text != "")
            {

                drList["LC_VALUE"] = txtVal.Text;
            }
            else
            {
                drList["LC_VALUE"] = DBNull.Value;
            }
            drList[FINColumnConstants.EFFECTIVE_FROM_DT] = DBMethod.ConvertStringToDate(dtpStartDate.Text.ToString());

            if (dtpEndDate.Text.ToString().Length > 0)
            {

                drList[FINColumnConstants.EFFECTIVE_TO_DT] = DBMethod.ConvertStringToDate(dtpEndDate.Text.ToString());

            }
            else
            {
                drList[FINColumnConstants.EFFECTIVE_TO_DT] = DBNull.Value;
            }

            if (chkact.Checked)
            {
                drList[FINColumnConstants.ENABLED_FLAG] = "TRUE";
            }
            else
            {
                drList[FINColumnConstants.ENABLED_FLAG] = "FALSE";
            }

            drList[FINColumnConstants.DELETED] = FINAppConstants.N;

            return drList;
        }

        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //  ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                //else if (FacilityMasterBLL.ErrorCollection.Count > 0)
                //{
                //    ErrorCollection = FacilityMasterBLL.ErrorCollection;
                //    return;
                //}
                else
                {
                    gvData.EditIndex = -1;
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Row_upd", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row delete mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewDeleteEventArgs indicates which row's delete button was clicked. </param>


        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                dtGridData.Rows[e.RowIndex][FINColumnConstants.DELETED] = FINAppConstants.Y;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Row_del", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Row_Edt", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AppraisalDefineKRA", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }


        /// <summary>
        /// Used to delete the exam master and detail table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }



            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_Btn_ys_clik", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }

        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddlLeavecondtyp = tmpgvr.FindControl("ddlLeavecondtyp") as DropDownList;
                DropDownList ddlCondititon = tmpgvr.FindControl("ddlCondititon") as DropDownList;


                Lookup_BLL.GetLookUpValues(ref ddlLeavecondtyp, "LC_TYPE");
                Lookup_BLL.GetLookUpValues(ref ddlCondititon, "LC_CONDITION");


                if (gvData.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                {
                    ddlLeavecondtyp.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.LOOKUP_ID].ToString();
                    ddlCondititon.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["CONDITION_ID"].ToString();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        protected void cmbCalendarTypeId_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (bol_rowVisiable)
                    e.Row.Visible = false;

                if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                {
                    e.Row.Visible = false;
                }

            }
        }

        #endregion

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                AssignToBE();

                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                }

                //switch (Master.Mode)
                //{
                //    case FINAppConstants.Add:
                //        {
                //            DBMethod.SaveEntity<GL_COMPANIES_HDR>(gL_COMPANIES_HDR);
                //            break;
                //        }
                //    case FINAppConstants.Update:
                //        {

                //            DBMethod.SaveEntity<GL_COMPANIES_HDR>(gL_COMPANIES_HDR, true);
                //            break;
                //        }
            }


            catch (Exception ex)
            {
                ErrorCollection.Add("Org_btn_sve", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void ddlLeave_SelectedIndexChanged(object sender, EventArgs e)
        {
            //dtGridData = DBMethod.ExecuteQuery(LeaveCondition_DAL.GetLeaveconditiondtls_basedon_leave(ddlLeave.SelectedValue)).Tables[0];
            //BindGrid(dtGridData);
        }

        protected void ddlLeavecondtyp_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;

            DropDownList ddlLeavecondtyp = gvr.FindControl("ddlLeavecondtyp") as DropDownList;
            DropDownList ddlCondititon = gvr.FindControl("ddlCondititon") as DropDownList;

            TextBox txtHighval = gvr.FindControl("txtHighval") as TextBox;
            TextBox txtlowval = gvr.FindControl("txtlowval") as TextBox;
            TextBox txtVal = gvr.FindControl("txtVal") as TextBox;

            if (ddlLeavecondtyp.SelectedItem.Text == "SLAB")
            {
                ddlCondititon.Enabled = false;
                txtlowval.Enabled = true;
                txtHighval.Enabled = true;
                txtVal.Enabled = true;
            }
            else if (ddlLeavecondtyp.SelectedItem.Text == "N-A")
            {
                txtlowval.Enabled = false;
                txtHighval.Enabled = false;
                txtVal.Enabled = false;
                ddlCondititon.Enabled = false;
            }
            else if (ddlLeavecondtyp.SelectedItem.Text == "CONDITION")
            {
                txtlowval.Enabled = true;
                txtHighval.Enabled = true;
                txtVal.Enabled = true;
                ddlCondititon.Enabled = true;
            }


        }






    }
}