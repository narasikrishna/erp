﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.DAL.GL;
using FIN.BLL;
using FIN.BLL.GL;
using FIN.BLL.HR;
using VMVServices.Web;


namespace FIN.Client.HR
{
    public partial class LeaveYearEndProcessEntry : PageBase
    {

        HR_LEAVE_YR_END_PROCESS HR_LEAVE_YR_END_PROCESS = new HR_LEAVE_YR_END_PROCESS();

        DataTable dtData = new DataTable();
        Boolean bol_rowVisiable;
        string ProReturn = null;
        DataTable Month_data = new DataTable();
        static int month;
        Boolean savedBool;
        DataTable dtGridData = new DataTable();
        int leaveBalance;
        int NoOfDays;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
    
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));


            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
        }

        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        private void FillComboBox()
        {
            AccountingCalendar_BLL.GetCalAcctYear(ref ddlFinancialYear);
            Lookup_BLL.GetLookUpValues(ref ddlProcCode, "Process Code");
            ddlProcCode.Items.RemoveAt(0);
          
        }

        protected void ddlFinancialYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                AccountingCalendar_BLL.GetPreviousCalAcctYear(ref ddlPreviousFinancialYear, ddlFinancialYear.SelectedValue);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// .RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();

                EntityData = null;
                Session[FINSessionConstants.GridData] = null;

                FillComboBox();

                btnSave.Visible = false;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    //if (hR_EMPLOYEES.EMP_INTERNAL_EXTERNAL != "E")
                    //   {
                    //       gvData.Enabled = false;
                    //   }

                    //  // HR_LEAVE_STAFF_DEFINTIONS = StaffLeaveDetails_BLL.getClassEntity(Master.StrRecordId);

                    //   DataTable dtsld = new DataTable();

                    //   dtsld = DBMethod.ExecuteQuery(StaffLeaveDetails_DAL.GetStaffLeaveDts(Master.StrRecordId)).Tables[0];

                    //  // EntityData = HR_LEAVE_STAFF_DEFINTIONS;
                    ////   ddlFinancialYear.SelectedValue = HR_LEAVE_STAFF_DEFINTIONS.FISCAL_YEAR.ToString();
                    //   ddlFinancialYear.SelectedValue = dtsld.Rows[0]["FISCAL_YEAR"].ToString();
                    //   FillFromToDate();
                    //  // ddlDepartment.SelectedValue = HR_LEAVE_STAFF_DEFINTIONS.DEPT_ID.ToString();
                    //   ddlDepartment.SelectedValue = dtsld.Rows[0]["DEPT_ID"].ToString();
                    //   fillempnm();
                    //  // ddlStaffName.SelectedValue = HR_LEAVE_STAFF_DEFINTIONS.STAFF_ID.ToString();
                    //   ddlStaffName.SelectedValue = dtsld.Rows[0]["STAFF_ID"].ToString();
                }
                //dtGridData = DBMethod.ExecuteQuery(StaffLeaveDetails_DAL.GetStaffLeaveDtls(Master.StrRecordId)).Tables[0];
                //BindGrid(dtGridData);


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Vaca_Entry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);

                }
            }

        }


        #endregion
        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                //ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session[FINSessionConstants.GridData], "Staff Leave ");

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                AssignToBE();

                if (ddlFinancialYear.SelectedValue != string.Empty && ddlPreviousFinancialYear.SelectedValue != string.Empty)
                {
                    AccountingCalendar_DAL.GetSPFOR_LeaveYrEnd_Process(ddlFinancialYear.SelectedValue, ddlPreviousFinancialYear.SelectedValue);
                }
                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();


                HR_LEAVE_YR_END_PROCESS.PROCESS_CODE = ddlProcCode.SelectedValue;
                HR_LEAVE_YR_END_PROCESS.PROCESS_DATE = DateTime.Now;

                HR_LEAVE_YR_END_PROCESS.ENABLED_FLAG = FINAppConstants.EnabledFlag;

                HR_LEAVE_YR_END_PROCESS.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;
                HR_LEAVE_YR_END_PROCESS.ORG_ID = VMVServices.Web.Utils.OrganizationID;
                HR_LEAVE_YR_END_PROCESS.CREATED_BY = this.LoggedUserName;
                HR_LEAVE_YR_END_PROCESS.CREATED_DATE = DateTime.Today;
                HR_LEAVE_YR_END_PROCESS.LEAVE_YR_END_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_107.ToString(), false, true);
               
                DBMethod.SaveEntity<HR_LEAVE_YR_END_PROCESS>(HR_LEAVE_YR_END_PROCESS);

                savedBool = true;

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        protected void btnYes_Click(object sender, EventArgs e)
        {



        }
        #endregion


    }
}