﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="LeaveDefinitionEntry.aspx.cs" Inherits="FIN.Client.HR.LeaveDefinitionEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        function reload() {
            alert('hi');
            document.location.reload(true);
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1000px" id="divMainContainer">
        <div class="divClear_10">
        </div>
       <%-- <div class="divRowContainer">
            <div class="lblBox"  id="lblFinancialYear">
                Financial Year
            </div>
            <div class="divtxtBox" >
                <asp:DropDownList ID="ddlFinancialYear" runat="server" CssClass="validate[required]  RequiredField ddlStype"
                    Width="150px" AutoPostBack="true" OnSelectedIndexChanged="ddlFinancialYear_SelectedIndexChanged"
                    TabIndex="1">
                </asp:DropDownList>
            </div>
            <div class="lblBox" style="float: left; width: 150px" id="lblFromDate">
                From Date
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:TextBox ID="txtFromDate" CssClass="EntryFont validate[required] txtBox" runat="server"
                    Enabled="true" TabIndex="2"></asp:TextBox>
            </div>
            <div class="lblBox" style="float: left; width: 150px" id="lblToDate">
                To Date
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:TextBox ID="txtToDate" CssClass="EntryFont validate[required] txtBox" runat="server" Enabled="true"
                    TabIndex="3"></asp:TextBox>
            </div>
        </div>--%>
        <div class="divClear_10">
        </div>
        <div class="LNOrient">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid" Width = "1000px"
                DataKeyNames="PK_ID,DELETED" OnRowCancelingEdit="gvData_RowCancelingEdit" OnRowCommand="gvData_RowCommand"
                OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound" OnRowDeleting="gvData_RowDeleting"
                OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating" ShowFooter="True">
                <Columns>
                    <asp:TemplateField HeaderText="Leave Name" ControlStyle-Width = "100px" >
                        <EditItemTemplate>
                            <asp:TextBox ID="txtLeaveName" Width="100px" MaxLength="50" runat="server" CssClass=" RequiredField  txtBox_en"
                                Text='<%# Eval("LEAVE_ID") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtLeaveName" TabIndex="4" Width="100px" MaxLength="50" runat="server" Wrap = "true"
                                CssClass="RequiredField   txtBox_en"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblLeaveName" Width="100px" Style="word-wrap: break-word;white-space: pre-wrap;" runat="server" Text='<%# Eval("LEAVE_ID") %>'></asp:Label>
                        </ItemTemplate>
                       
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Description" ControlStyle-Width = "300px">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtdesc"  MaxLength="200" runat="server" CssClass="EntryFont RequiredField  txtBox"
                                Text='<%# Eval("LEAVE_DESC") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtdesc" TabIndex="5"  MaxLength="200" runat="server" Wrap = "true"
                                CssClass="EntryFont RequiredField  txtBox"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lbldesc" runat="server" Style="word-wrap: break-word;white-space: pre-wrap;" Text='<%# Eval("LEAVE_DESC") %>'></asp:Label>
                        </ItemTemplate>
                        
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Leave Name(Arabic)" ControlStyle-Width = "100px">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtLeaveNameAR" Width="100px" MaxLength="50" runat="server" CssClass="txtBox_ol"
                                Text='<%# Eval("LEAVE_ID_OL") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtLeaveNameAR" TabIndex="5" Width="100px" MaxLength="50" runat="server" Wrap = "true"
                                CssClass="txtBox_ol"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblLeaveNameAR" Width="100px" Style="word-wrap: break-word;white-space: pre-wrap;" runat="server" Text='<%# Eval("LEAVE_ID_OL") %>'></asp:Label>
                        </ItemTemplate>
                       
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="No of Days" ControlStyle-Width = "50px">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtNoOfDays" Width="50px" MaxLength="3" runat="server" CssClass="RequiredField txtBox_N"
                                Text='<%# Eval("NO_OF_DAYS") %>'></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" FilterType="Numbers,Custom"
                                ValidChars="." TargetControlID="txtNoOfDays" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtNoOfDays" TabIndex="6" Width="50px" MaxLength="3" runat="server"
                                CssClass=" RequiredField txtBox_N"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Numbers,Custom"
                                ValidChars="." TargetControlID="txtNoOfDays" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblNoOfDays" Width="50px" runat="server" Text='<%# Eval("NO_OF_DAYS") %>'></asp:Label>
                        </ItemTemplate>
                        
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="With Pay" ControlStyle-Width = "75px">
                        <EditItemTemplate>
                            <asp:CheckBox ID="chkWithPay" runat="server" Checked='<%# Convert.ToBoolean(Eval("WITH_PAY_YN")) %>' />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:CheckBox ID="chkWithPay" TabIndex="7" runat="server" Checked="true" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkWithPay" runat="server" Enabled="false" Checked='<%# Convert.ToBoolean(Eval("WITH_PAY_YN")) %>' />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Carry Over" ControlStyle-Width = "75px">
                        <EditItemTemplate>
                            <asp:CheckBox ID="chkCarryOver" runat="server" Checked='<%# Convert.ToBoolean(Eval("CARRY_OVER_YN")) %>' />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:CheckBox ID="chkCarryOver" TabIndex="8" runat="server" Checked="true" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkCarryOver" runat="server" Enabled="false" Checked='<%# Convert.ToBoolean(Eval("CARRY_OVER_YN")) %>' />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Start Date">
                        <EditItemTemplate>
                            <asp:TextBox ID="dtpStartDate" MaxLength="10" runat="server" CssClass="validate[required] validate[custom[ReqDateDDMMYYY],dateRange[dg1]] RequiredField txtBox"
                                Width="95%" Text='<%#  Eval("EFFECTIVE_FROM_DT","{0:dd/MM/yyyy}") %>' Onkeypress="return DateKeyCheck(event,this);"></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender8" runat="server" Format="dd/MM/yyyy" TargetControlID="dtpStartDate">
                            </cc2:CalendarExtender>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" ValidChars="/"
                                FilterType="Numbers,Custom" TargetControlID="dtpStartDate" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="dtpStartDate" TabIndex="10" MaxLength="10" runat="server" CssClass="RequiredField EntryFont  txtBox"
                                Width="95%" Onkeypress="return DateKeyCheck(event,this);"></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender9" runat="server" Format="dd/MM/yyyy" TargetControlID="dtpStartDate">
                            </cc2:CalendarExtender>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars="/"
                                FilterType="Numbers,Custom" TargetControlID="dtpStartDate" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblStartDate" runat="server" Text='<%# Eval("EFFECTIVE_FROM_DT","{0:dd/MM/yyyy}") %>'></asp:Label>
                        </ItemTemplate>
                        
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="End Date">
                        <EditItemTemplate>
                            <asp:TextBox ID="dtpEndDate" MaxLength="10" runat="server" CssClass="validate[custom[ReqDateDDMMYYY],dateRange[dg1]] txtBox"
                                Width="95%" Text='<%#  Eval("EFFECTIVE_TO_DT","{0:dd/MM/yyyy}") %>'></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender10" runat="server" Format="dd/MM/yyyy"
                                TargetControlID="dtpEndDate">
                            </cc2:CalendarExtender>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" ValidChars="/"
                                FilterType="Numbers,Custom" TargetControlID="dtpEndDate" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="dtpEndDate" TabIndex="11" MaxLength="10" runat="server" CssClass="EntryFont  txtBox"
                                Width="95%"></asp:TextBox>
                            <cc2:CalendarExtender ID="CalendarExtender10" runat="server" Format="dd/MM/yyyy"
                                TargetControlID="dtpEndDate">
                            </cc2:CalendarExtender>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExten" runat="server" ValidChars="/"
                                FilterType="Numbers,Custom" TargetControlID="dtpEndDate" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblEndDate" runat="server" Text='<%# Eval("EFFECTIVE_TO_DT","{0:dd/MM/yyyy}") %>'></asp:Label>
                        </ItemTemplate>
                       
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Active">
                        <EditItemTemplate>
                            <asp:CheckBox ID="chkact" runat="server" Enabled = "true" Checked='<%# Convert.ToBoolean(Eval("ENABLED_FLAG")) %>' />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:CheckBox ID="chkact" TabIndex="12"  runat="server" Checked="true" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkact" runat="server" Enabled = "false" Checked='<%# Convert.ToBoolean(Eval("ENABLED_FLAG")) %>' />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" runat="server" AlternateText="Edit" CausesValidation="false"
                                ToolTip="Edit" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                            <asp:ImageButton ID="ibtnDelete" runat="server" AlternateText="Delete" CausesValidation="false"
                                ToolTip="Delete" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibtnUpdate" TabIndex="14" runat="server" AlternateText="Update"
                                CommandName="Update" ToolTip="Update" Height="20px" ImageUrl="~/Images/Update.png" />
                            <asp:ImageButton ID="ibtnCancel" TabIndex="15" runat="server" AlternateText="Cancel"
                                CausesValidation="false" ToolTip="Cancel" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="ibtnInsert" TabIndex="13" runat="server" AlternateText="Add"
                                CommandName="FooterInsert" ToolTip="Add" ImageUrl="~/Images/Add.jpg" />
                        </FooterTemplate>
                        <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                            TabIndex="3" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" OnClick="btnDelete_Click"
                            TabIndex="4" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="5" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="6" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table>
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/HR/HRChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
