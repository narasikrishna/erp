﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="ProbationEmpEvaluationEntry.aspx.cs" Inherits="FIN.Client.HR.ProbationEmpEvaluationEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 800px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style="width: 200px" id="Div1">
                Department
            </div>
            <div class="divtxtBox  LNOrient" style="width: 500px">
                <asp:DropDownList ID="ddlDepartment" TabIndex="1" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    AutoPostBack="True" OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style="width: 200px" id="Div6">
                Employee Name
            </div>
            <div class="divtxtBox  LNOrient" style="width: 500px">
                <asp:DropDownList ID="ddlEmployee" TabIndex="1" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    AutoPostBack="True" OnSelectedIndexChanged="ddlEmployee_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style="width: 200px" id="lblCompName">
                Probation Competency
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:DropDownList ID="ddlProbCompt" TabIndex="1" runat="server" CssClass="validate[required] RequiredField ddlStype">
                </asp:DropDownList>
            </div>
            <div class="lblBox  LNOrient" style="width: 180px" id="Div3">
                Level Status
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:DropDownList ID="ddlLineStatus" TabIndex="1" runat="server" CssClass="validate[required] RequiredField ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div  class="divRowContainer" runat="server">
           <div class="lblBox  LNOrient" style="width: 200px" id="Div2">
                Appraiser
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:DropDownList ID="ddlAppraiser" TabIndex="1" runat="server" 
                    CssClass="validate[required] RequiredField ddlStype" AutoPostBack="True" 
                    onselectedindexchanged="ddlAppraiser_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div class="lblBox  LNOrient" style="width: 180px" id="Div5">
                Recommendation
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px">
                <asp:DropDownList ID="ddlRecomendation" TabIndex="1" runat="server" 
                    CssClass="validate[required] RequiredField ddlStype" AutoPostBack="True" 
                    onselectedindexchanged="ddlRecomendation_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
         <div class="divClear_10">
        </div>
        <div class="divRowContainer" >
            <div class="lblBox  LNOrient" style="width: 200px" id="divConfDate" runat="server" visible="false">
                Confirmation Date
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px" id="divtxtConfDate" runat="server" visible="false">
                <asp:TextBox runat="server" ID="txtConfDate" CssClass="validate[required,custom[ReqDateDDMMYYY]] RequiredField  txtBox"
                        TabIndex="16"></asp:TextBox>
                    <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtConfDate" />
                    <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars="/"
                        FilterType="Numbers,Custom" TargetControlID="txtConfDate" />
            </div>
             <div class="lblBox  LNOrient" style="width: 200px;display:none" id="Div4" >
                Last Level
            </div>
            <div class="divtxtBox  LNOrient" style="width: 150px;display:none" >
                <asp:CheckBox ID="chkLastLevel" runat="server" Checked="false" TabIndex="1" AutoPostBack="True"
                    OnCheckedChanged="chkLastLevel_CheckedChanged" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="LNOrient">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                DataKeyNames="PROB_LINK_DTL_ID,PROB_DTL_ID,LOOKUP_ID,DELETED" OnRowCancelingEdit="gvData_RowCancelingEdit"
                OnRowCommand="gvData_RowCommand" OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound"
                OnRowDeleting="gvData_RowDeleting" OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating"
                ShowFooter="True" Width="100%">
                <Columns>
                    <asp:TemplateField HeaderText="Line Number" Visible="false">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtLineNum" Width="80px" MaxLength="10" Enabled="false" runat="server"
                                CssClass=" RequiredField  txtBox" Text='<%# Eval("LINE_NUM_1") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtLineNum" Width="80px" Enabled="false" MaxLength="10" runat="server"
                                CssClass="RequiredField   txtBox"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblLineNum" Width="80px" runat="server" Text='<%# Eval("LINE_NUM_1") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" Width="50px" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Competency Name">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlCompTencyName" runat="server" CssClass="RequiredField EntryFont ddlStype"
                                Width="100%" TabIndex="2" AutoPostBack="True" OnSelectedIndexChanged="ddlCompTencyName_SelectedIndexChanged">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlCompTencyName" runat="server" CssClass="RequiredField EntryFont ddlStype"
                                Width="100%" TabIndex="2" AutoPostBack="True" OnSelectedIndexChanged="ddlCompTencyName_SelectedIndexChanged">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblComptencyName"  runat="server" Text='<%# Eval("COM_DESC") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Level Description">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtLevelDesc" Enabled="false" runat="server" CssClass=" RequiredField  txtBox"
                                Text='<%# Eval("COM_LEVEL_DESC") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtLevelDesc" Enabled="false" runat="server" CssClass="RequiredField   txtBox"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblLevelDesc" CssClass="adminFormFieldHeading" runat="server" Text='<%# Eval("COM_LEVEL_DESC") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Rating">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlRating" TabIndex="5" runat="server" CssClass="ddlStype"
                                Width="100%">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlRating" TabIndex="5" runat="server" CssClass="ddlStype"
                                Width="100%">
                            </asp:DropDownList>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lbldeptTyp" CssClass="adminFormFieldHeading" runat="server" Text='<%# Eval("LOOKUP_NAME") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <%--  <asp:TemplateField HeaderText="Rating">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtRating" Width="96%" MaxLength="7" runat="server" CssClass="RequiredField txtBox_N"
                                Text='<%# Eval("PROB_EVAL_RATING") %>'></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars=""
                                FilterType="Numbers,Custom" TargetControlID="txtRating" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtRating" TabIndex="3" Width="96%" MaxLength="7" runat="server"
                                CssClass=" RequiredField txtBox_N"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars=""
                                FilterType="Numbers,Custom" TargetControlID="txtRating" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblRating" CssClass="adminFormFieldHeading" runat="server" Text='<%# Eval("PROB_EVAL_RATING") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>--%>
                    <asp:TemplateField HeaderText="Active">
                        <EditItemTemplate>
                            <asp:CheckBox ID="chkact" runat="server" Enabled = "true" Checked='<%# Convert.ToBoolean(Eval("ENABLED_FLAG")) %>' />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:CheckBox ID="chkact" runat="server" Enabled = "true" TabIndex="4" Checked="true" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkact" runat="server" Enabled = "false"  Checked='<%# Convert.ToBoolean(Eval("ENABLED_FLAG")) %>' />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" TabIndex="9" runat="server" AlternateText="Edit" CausesValidation="false"
                                ToolTip="Edit" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                            <asp:ImageButton ID="ibtnDelete" TabIndex="10" runat="server" AlternateText="Delete"
                                CausesValidation="false" ToolTip="Delete" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibtnUpdate" TabIndex="9" runat="server" AlternateText="Update"
                                CommandName="Update" ToolTip="Update" Height="20px" ImageUrl="~/Images/Update.png" />
                            <asp:ImageButton ID="ibtnCancel" TabIndex="10" runat="server" AlternateText="Cancel"
                                CausesValidation="false" ToolTip="Cancel" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="ibtnInsert" TabIndex="11" runat="server" AlternateText="Add"
                                CommandName="FooterInsert" ToolTip="Add" ImageUrl="~/Images/Add.jpg" />
                        </FooterTemplate>
                        <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divClear_10">
        </div>
        <div class="LNOrient">
            <asp:GridView ID="gvEmpDeptCmnts" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                DataKeyNames="PROB_COMMENT_ID,DELETED" OnRowCancelingEdit="gvEmpDeptCmnts_RowCancelingEdit"
                OnRowCommand="gvEmpDeptCmnts_RowCommand" OnRowCreated="gvEmpDeptCmnts_RowCreated"
                OnRowDataBound="gvEmpDeptCmnts_RowDataBound" OnRowDeleting="gvEmpDeptCmnts_RowDeleting"
                OnRowEditing="gvEmpDeptCmnts_RowEditing" OnRowUpdating="gvEmpDeptCmnts_RowUpdating"
                ShowFooter="True" Width="100%">
                <Columns>
                    <asp:TemplateField HeaderText="Line Number" Visible="false">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtLineNum2" Width="80px" MaxLength="10" Enabled="false" runat="server"
                                CssClass=" RequiredField  txtBox" Text='<%# Eval("LINE_NUM_2") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtLineNum2" Enabled="false" Width="80px" MaxLength="10" runat="server"
                                CssClass="RequiredField   txtBox"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblLineNum2" Width="80px" runat="server" Text='<%# Eval("LINE_NUM_2") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Area to be improve">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtComments"  runat="server" CssClass=" RequiredField  txtBox" MaxLength="500"
                                Text='<%# Eval("PROB_COMMENT") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtComments"  runat="server" CssClass="RequiredField   txtBox" MaxLength="500"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblComments" Style="word-wrap: break-word; white-space: pre-wrap;
                                height: 100%"  runat="server" Text='<%# Eval("PROB_COMMENT") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" TabIndex="9" runat="server" AlternateText="Edit" CausesValidation="false"
                                ToolTip="Edit" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                            <asp:ImageButton ID="ibtnDelete" TabIndex="10" runat="server" AlternateText="Delete"
                                CausesValidation="false" ToolTip="Delete" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibtnUpdate" TabIndex="9" runat="server" AlternateText="Update"
                                CommandName="Update" ToolTip="Update" Height="20px" ImageUrl="~/Images/Update.png" />
                            <asp:ImageButton ID="ibtnCancel" TabIndex="10" runat="server" AlternateText="Cancel"
                                CausesValidation="false" ToolTip="Cancel" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="ibtnInsert" TabIndex="11" runat="server" AlternateText="Add"
                                CommandName="FooterInsert" ToolTip="Add" ImageUrl="~/Images/Add.jpg" />
                        </FooterTemplate>
                        <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divClear_10">
        </div>
        <div class="LNOrient">
            <asp:GridView ID="gvEmpImp" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                DataKeyNames="PROB_IMP_ID,DELETED" OnRowCancelingEdit="gvEmpImp_RowCancelingEdit"
                OnRowCommand="gvEmpImp_RowCommand" OnRowCreated="gvEmpImp_RowCreated" OnRowDataBound="gvEmpImp_RowDataBound"
                OnRowDeleting="gvEmpImp_RowDeleting" OnRowEditing="gvEmpImp_RowEditing" OnRowUpdating="gvEmpImp_RowUpdating"
                ShowFooter="True" Width="100%">
                <Columns>
                    <asp:TemplateField HeaderText="Comments">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtComments3"  runat="server" CssClass=" RequiredField  txtBox" MaxLength="500"
                                Text='<%# Eval("PROB_IMP_COMMENTS") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtComments3"  runat="server" CssClass="RequiredField   txtBox" MaxLength="500"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblComments3" Style="word-wrap: break-word; white-space: pre-wrap;
                                " runat="server" Text='<%# Eval("PROB_IMP_COMMENTS") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" TabIndex="9" runat="server" AlternateText="Edit" CausesValidation="false"
                                ToolTip="Edit" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                            <asp:ImageButton ID="ibtnDelete" TabIndex="10" runat="server" AlternateText="Delete"
                                CausesValidation="false" ToolTip="Delete" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibtnUpdate" TabIndex="9" runat="server" AlternateText="Update"
                                CommandName="Update" ToolTip="Update" Height="20px" ImageUrl="~/Images/Update.png" />
                            <asp:ImageButton ID="ibtnCancel" TabIndex="10" runat="server" AlternateText="Cancel"
                                CausesValidation="false" ToolTip="Cancel" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="ibtnInsert" TabIndex="11" runat="server" AlternateText="Add"
                                CommandName="FooterInsert" ToolTip="Add" ImageUrl="~/Images/Add.jpg" />
                        </FooterTemplate>
                        <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                            TabIndex="12" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="13" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="14" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="15" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table>
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/HR/HRChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
