﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.BLL.AP;
using FIN.BLL.HR;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.BLL;
using VMVServices.Web;


namespace FIN.Client.HR
{
    public partial class TrainerEntry : PageBase
    {
        HR_TRM_TRAINER hR_TRM_TRAINER = new HR_TRM_TRAINER();
        System.Collections.SortedList slControls = new System.Collections.SortedList();
        string ProReturn = null;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                //pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                //pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
            }

            if (VMVServices.Web.Utils.AddAllowed != FINAppConstants.Y && Master.Mode == FINAppConstants.Add)
            {
                btnSave.Visible = false;
            }

        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                //ddlWeightUOM.Enabled = false;
                //ddlLengthUOM.Enabled = false;
                //ddlAreaUOM.Enabled = false;
                //ddlVolumeUOM.Enabled = false;


                Startup();
                FillComboBox();

                EntityData = null;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    using (IRepository<HR_TRM_TRAINER> userCtx = new DataRepository<HR_TRM_TRAINER>())
                    {
                        hR_TRM_TRAINER = userCtx.Find(r =>
                            (r.TRNR_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = hR_TRM_TRAINER;
                    txtName.Text = hR_TRM_TRAINER.TRNR_NAME;
                    ddlType.SelectedValue = hR_TRM_TRAINER.TRNR_TYPE;
                    ChangeType();
                    ddlEmployeeName.SelectedValue = hR_TRM_TRAINER.TRNR_EMP_ID;
                    txtProfile.Text = hR_TRM_TRAINER.TRNR_PROFILE;
                    txtExperience.Text = hR_TRM_TRAINER.TRNR_EXPERIENCE.ToString();
                    txtConsultancyName.Text = hR_TRM_TRAINER.TRNR_CONS_NAME;
                    txtConsultancyContactPerson.Text = hR_TRM_TRAINER.TRNR_CONS_PERSON;
                    txtContactNo.Text = hR_TRM_TRAINER.TRNR_CONS_CONTACT_NO;
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>

        private void FillComboBox()
        {

            Lookup_BLL.GetLookUpValues(ref ddlType, "TRNR_TYPE");
            Employee_BLL.GetEmpName(ref ddlEmployeeName);


        }

        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    hR_TRM_TRAINER = (HR_TRM_TRAINER)EntityData;
                }

                hR_TRM_TRAINER.TRNR_NAME = txtName.Text;
                hR_TRM_TRAINER.TRNR_TYPE = ddlType.SelectedValue.ToString();
                hR_TRM_TRAINER.TRNR_EMP_ID = ddlEmployeeName.SelectedValue.ToString();
                hR_TRM_TRAINER.TRNR_PROFILE = txtProfile.Text;
                if (txtExperience.Text.Length > 0)
                {
                    hR_TRM_TRAINER.TRNR_EXPERIENCE = int.Parse(txtExperience.Text.ToString());
                }
                hR_TRM_TRAINER.TRNR_CONS_NAME = txtConsultancyName.Text;
                hR_TRM_TRAINER.TRNR_CONS_PERSON = txtConsultancyContactPerson.Text;
                hR_TRM_TRAINER.TRNR_CONS_CONTACT_NO = txtContactNo.Text;
                hR_TRM_TRAINER.ENABLED_FLAG = FINAppConstants.Y;
                hR_TRM_TRAINER.TRNR_ORG_ID = VMVServices.Web.Utils.OrganizationID;
                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    hR_TRM_TRAINER.MODIFIED_BY = this.LoggedUserName;
                    hR_TRM_TRAINER.MODIFIED_DATE = DateTime.Today;

                }
                else
                {
                    hR_TRM_TRAINER.TRNR_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.HR_062.ToString(), false, true);
                    //iNV_ITEM_MASTER.WORKFLOW_COMPLETION_STATUS = "1";
                    hR_TRM_TRAINER.CREATED_BY = this.LoggedUserName;
                    hR_TRM_TRAINER.CREATED_DATE = DateTime.Today;

                }

                hR_TRM_TRAINER.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, hR_TRM_TRAINER.TRNR_ID);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                //ErrorCollection.Clear();
                //slControls[0] = txtName;
                //slControls[1] = ddlType;


                //DataRow drList;

                ErrorCollection.Clear();
                //string strCtrlTypes = "TextBox~DropDownList";
                //string strMessage = " Name ~  Type";
                //ErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);
                //if (ErrorCollection.Count > 0)
                //    //return drList;



                //emptyvalid();
                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                AssignToBE();

                // Duplicate Validation Through Backend Package PKG_VALIDATIONS

                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                ProReturn = FINSP.GetSPFOR_ERR_MGR_CATEGORY(Master.FormCode, hR_TRM_TRAINER.TRNR_ID, hR_TRM_TRAINER.TRNR_NAME);
                if (ProReturn != string.Empty)
                {
                    if (ProReturn != "0")
                    {
                        ErrorCollection.Add("TRAINER", ProReturn);
                        if (ErrorCollection.Count > 0)
                        {
                            return;
                        }
                    }
                }

                //ProReturn = FIN.DAL.AP.Item_DAL.GetSPFOR_DUPLICATE_CHECK(iNV_ITEM_MASTER.ITEM_CODE,txtItemName.Text, iNV_ITEM_MASTER.ORG_ID, iNV_ITEM_MASTER.ITEM_ID);

                //if (ProReturn != string.Empty)
                //{
                //    if (ProReturn != "0")
                //    {
                //        ErrorCollection.Add("ITEMNAME", ProReturn);
                //        if (ErrorCollection.Count > 0)
                //        {
                //            return;
                //        }
                //    }
                //}


                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            DBMethod.SaveEntity<HR_TRM_TRAINER>(hR_TRM_TRAINER);
                            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                            break;
                        }
                    case FINAppConstants.Update:
                        {

                            DBMethod.SaveEntity<HR_TRM_TRAINER>(hR_TRM_TRAINER, true);
                            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                            break;

                        }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion




        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                DBMethod.DeleteEntity<HR_TRM_TRAINER>(hR_TRM_TRAINER);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }



        protected void btnCancel_Click(object sender, EventArgs e)
        {

        }

        protected void ddlEmployeeName_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (ddlEmployeeName.SelectedValue.ToString().Length > 0)
            //{
            //    txtName.Enabled = false;
                txtName.Text = ddlEmployeeName.SelectedItem.Text;
            //}
            //else
            //{
            //    txtName.Enabled = true;
            //    txtName.Text = "";
            //}
        }

        protected void ddlType_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChangeType();
        }
        private void ChangeType()
        {
            if (ddlType.SelectedItem.Text.ToUpper().Trim() == "INTERNAL")
            {
                divName.Visible = false;
                txtName.Enabled = false;
                ddlEmployeeName.Enabled = true;
                lblConsultancyName.Visible = false;
                txtConsultancyName.Visible = false;
                ConsultancyContact.Visible = false;

            }
            else if (ddlType.SelectedItem.Text.ToUpper().Trim() == "EXTERNAL")
            {
                divName.Visible = true;
                txtName.Enabled = true;
                ddlEmployeeName.Enabled = false;
                lblConsultancyName.Visible = true;
                txtConsultancyName.Visible = true;
                ConsultancyContact.Visible = true;
                // txtName.Text = "";
            }
        }







        //private void emptyvalid()
        //{
        //    if (hR_VACANCIES.VAC_DEPT_ID == null)
        //    {

        //        if (ddlDepartment.SelectedValue == string.Empty)
        //        {
        //            ErrorCollection.Add("Department", "Department Cannot be empty");
        //        }

        //    }
        //    if (hR_VACANCIES.VAC_DESIG_ID == null)
        //    {
        //        if (ddlDesignation.SelectedValue == string.Empty)

        //            ErrorCollection.Add("Designation", "Designation Cannot be empty");
        //        }
        //    }
        //    //if (hR_VACANCIES.VAC_TYPE > 0)
        //    //{

        //    //    if (ddlType.SelectedValue == string.Empty)
        //    //    {
        //    //        ErrorCollection.Add("DepartmentType", "Department Type Cannot be empty");
        //    //    }
        //    //}

        //    //if  > 0)
        //    //{
        //    //    if (ddlVolumeUOM.SelectedValue == string.Empty)
        //    //    {

        //    //        ErrorCollection.Add("VOLEMT", "Volume UOM Cannot be empty");
        //    //    }
        //    //}

        //}






    }





}
