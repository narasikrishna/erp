﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="AppraisalFinalAssessmentNewEntry.aspx.cs" Inherits="FIN.Client.HR.AppraisalFinalAssessmentNewEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 800px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="lblEntityType">
                Appraisal
            </div>
            <div class="divtxtBox  LNOrient" style="width: 300px">
                <asp:DropDownList ID="ddlAppraisal" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    AutoPostBack="True" TabIndex="1" OnSelectedIndexChanged="ddlAppraisal_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divClear_10">
        </div>
        <div class="LNOrient">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                DataKeyNames="APFA_DTL_ID,EMP_ID,FINAL_RATING,DELETED" Width="700px" OnRowDataBound="gvData_RowDataBound"
                ShowFooter="false">
                <Columns>
                    <asp:BoundField DataField="EMP_NAME" HeaderText="Employee" ItemStyle-Width="200px"
                        ItemStyle-Wrap="True" Visible="true">
                        <ItemStyle Wrap="True" />
                    </asp:BoundField>
                    <asp:BoundField DataField="DEPT_NAME" HeaderText="Department" ItemStyle-Width="150px"
                        ItemStyle-Wrap="True">
                        <ItemStyle Wrap="True" />
                    </asp:BoundField>
                    <asp:BoundField DataField="FINAL_RATING" HeaderText="Rating" ItemStyle-Width="80px"
                        ItemStyle-Wrap="True" ItemStyle-HorizontalAlign="Right">
                        <ItemStyle Wrap="True" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="HR/CEO Rating">
                        <ItemTemplate>
                            <asp:TextBox ID="txtHrRat" MaxLength="15" TabIndex="10" runat="server" CssClass="txtBox_N"
                                Width="96%" Text='<%# Eval("FINAL_HR_CEO_RATING") %>'></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars=".,0"
                                FilterType="Numbers,Custom" TargetControlID="txtHrRat" />
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Chairman/Vice Chairman Rating">
                        <ItemTemplate>
                            <asp:TextBox ID="txtchairmanrat" MaxLength="15" TabIndex="10" runat="server" CssClass="txtBox_N"
                                Width="96%" Text='<%# Eval("FINAL_CHAIRMAN_RATING") %>' AutoPostBack="True" OnTextChanged="txtchairmanrat_TextChanged1"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars=".,"
                                FilterType="Numbers,Custom" TargetControlID="txtchairmanrat" />
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Total Rating">
                        <ItemTemplate>
                            <asp:TextBox ID="txtTotRat" MaxLength="15" TabIndex="10" runat="server" CssClass="txtBox_N" 
                                Width="96%" Text='<%# Eval("FINAL_TOTAL_RATING") %>'></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" ValidChars=".,"
                                FilterType="Numbers,Custom" TargetControlID="txtTotRat" />
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Right" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                            TabIndex="11" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="12" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="13" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="14" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table>
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
      <script src="../LanguageScript/HR/HRChangeLang.js" type="text/javascript"></script>
     <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
