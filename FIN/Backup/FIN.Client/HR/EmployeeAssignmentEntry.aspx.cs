﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.DAL.GL;
using FIN.BLL;
using FIN.BLL.GL;
using FIN.BLL.HR;
using VMVServices.Web;

namespace FIN.Client.HR
{
    public partial class EmployeeAssignmentEntry : PageBase
    {

        HR_EMP_ASSIGNMENT_DTLS HR_EMP_ASSIGNMENT_DTLS = new HR_EMP_ASSIGNMENT_DTLS();
        string ProReturn = null;

        Boolean saveBool = false;

        DataTable dtGridData = new DataTable();
        Boolean bol_rowVisiable;
        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EMPPF_PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));


            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
        }

        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void FillComboBox()
        {
            Employee_BLL.GetEmployeeId(ref ddlEmployee);
        }
        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>
        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                EntityData = null;

                FillComboBox();


                dtGridData = DBMethod.ExecuteQuery(EmployeeAssignment_DAL.GetEmpAssignmentdtls(Master.StrRecordId)).Tables[0];
                BindGrid(dtGridData);

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    using (IRepository<HR_EMP_ASSIGNMENT_DTLS> userCtx = new DataRepository<HR_EMP_ASSIGNMENT_DTLS>())
                    {
                        HR_EMP_ASSIGNMENT_DTLS = userCtx.Find(r =>
                            (r.ASSIGNMENT_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }
                    EntityData = HR_EMP_ASSIGNMENT_DTLS;
                    ddlEmployee.SelectedValue = HR_EMP_ASSIGNMENT_DTLS.ASSIGNMENT_EMP_ID;

                    BindEmName();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EMPPf_ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }
        protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            filljob(gvr);
        }
       

        private void filljob( GridViewRow tmpgvr)
        {

            DropDownList ddl_Category = tmpgvr.FindControl("ddlCategory") as DropDownList;
            DropDownList ddl_Job = tmpgvr.FindControl("ddlJob") as DropDownList;
            Jobs_BLL.fn_getJob4Category(ref ddl_Job, ddl_Category.SelectedValue);
        }

        protected void ddljob_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            fillposition(gvr);
        }

        private void fillposition(GridViewRow tmpgvr)
        {
            
            DropDownList ddlJob = tmpgvr.FindControl("ddlJob") as DropDownList;
            DropDownList ddlPosition = tmpgvr.FindControl("ddlPosition") as DropDownList;
            Position_BLL.fn_GetPositionNameBasedonJob(ref ddlPosition, ddlJob.SelectedValue);
        }
        protected void ddlPosition_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            fillGrade(gvr);
        }
        private void fillGrade(GridViewRow tmpgvr)
        {
            DropDownList ddl_Grade = tmpgvr.FindControl("ddlGrade") as DropDownList;
            DropDownList ddl_Position = tmpgvr.FindControl("ddlPosition") as DropDownList;
            Grades_BLL.fn_getGradeName4Position(ref ddl_Grade, ddl_Position.SelectedValue);

        }
        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();

                if (EntityData != null)
                {
                    HR_EMP_ASSIGNMENT_DTLS = (HR_EMP_ASSIGNMENT_DTLS)EntityData;
                }

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                var tmpChildEntity = new List<Tuple<object, string>>();

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    HR_EMP_ASSIGNMENT_DTLS = new HR_EMP_ASSIGNMENT_DTLS();
                    if (dtGridData.Rows[iLoop]["ASSIGNMENT_ID"].ToString() != "0" && dtGridData.Rows[iLoop]["ASSIGNMENT_ID"].ToString() != string.Empty)
                    {
                        using (IRepository<HR_EMP_ASSIGNMENT_DTLS> userCtx = new DataRepository<HR_EMP_ASSIGNMENT_DTLS>())
                        {
                            HR_EMP_ASSIGNMENT_DTLS = userCtx.Find(r =>
                                (r.ASSIGNMENT_ID == dtGridData.Rows[iLoop]["ASSIGNMENT_ID"].ToString())
                                ).SingleOrDefault();
                        }
                    }
                    HR_EMP_ASSIGNMENT_DTLS.ASSIGNMENT_EMP_ID = ddlEmployee.SelectedValue;

                    if (dtGridData.Rows[iLoop]["ASSIGNMENT_FROM_DT"] != DBNull.Value)
                    {
                        HR_EMP_ASSIGNMENT_DTLS.ASSIGNMENT_FROM_DT = DateTime.Parse(dtGridData.Rows[iLoop]["ASSIGNMENT_FROM_DT"].ToString());
                    }
                    if (dtGridData.Rows[iLoop]["ASSIGNMENT_TO_DT"] != DBNull.Value)
                    {
                        HR_EMP_ASSIGNMENT_DTLS.ASSIGNMENT_TO_DT = DateTime.Parse(dtGridData.Rows[iLoop]["ASSIGNMENT_TO_DT"].ToString());
                    }
                    //HR_EMP_ASSIGNMENT_DTLS.ASSIGNMENT_FROM_DT = DateTime.Parse(dtGridData.Rows[iLoop]["ASSIGNMENT_FROM_DT"].ToString());
                    //HR_EMP_ASSIGNMENT_DTLS.ASSIGNMENT_TO_DT = DateTime.Parse(dtGridData.Rows[iLoop]["ASSIGNMENT_TO_DT"].ToString());\
                    HR_EMP_ASSIGNMENT_DTLS.LOCATION_ID = dtGridData.Rows[iLoop]["LOCATION_ID"].ToString();
                    HR_EMP_ASSIGNMENT_DTLS.CATEGORY_ID = dtGridData.Rows[iLoop]["CATEGORY_ID"].ToString();
                    HR_EMP_ASSIGNMENT_DTLS.ASSIGNMENT_JOB = dtGridData.Rows[iLoop]["ASSIGNMENT_JOB"].ToString();
                    HR_EMP_ASSIGNMENT_DTLS.ASSIGNMENT_GRADE = dtGridData.Rows[iLoop]["ASSIGNMENT_GRADE"].ToString();
                    HR_EMP_ASSIGNMENT_DTLS.ASSIGNMENT_POSITION = dtGridData.Rows[iLoop]["ASSIGNMENT_POSITION"].ToString();
                    HR_EMP_ASSIGNMENT_DTLS.ASSIGNMENT_REPORTING_TO = dtGridData.Rows[iLoop]["ASSIGNMENT_REPORTING_TO"].ToString();
                    HR_EMP_ASSIGNMENT_DTLS.ASSIGNMENT_ORG_ID = VMVServices.Web.Utils.OrganizationID;
                    
                   // HR_EMP_ASSIGNMENT_DTLS.WORKFLOW_COMPLETION_STATUS = "1";
                    HR_EMP_ASSIGNMENT_DTLS.ENABLED_FLAG = dtGridData.Rows[iLoop]["ENABLED_FLAG"].ToString() == FINAppConstants.TRUEFLAG ? "1" : "0";

                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        tmpChildEntity.Add(new Tuple<object, string>(HR_EMP_ASSIGNMENT_DTLS, "D"));
                    }
                    else
                    {
                        if (dtGridData.Rows[iLoop]["ASSIGNMENT_ID"].ToString() != "0" && dtGridData.Rows[iLoop]["ASSIGNMENT_ID"].ToString() != string.Empty)
                        {
                            HR_EMP_ASSIGNMENT_DTLS.ASSIGNMENT_ID = dtGridData.Rows[iLoop]["ASSIGNMENT_ID"].ToString();
                            HR_EMP_ASSIGNMENT_DTLS.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, HR_EMP_ASSIGNMENT_DTLS.ASSIGNMENT_ID);
                            HR_EMP_ASSIGNMENT_DTLS.MODIFIED_BY = this.LoggedUserName;
                            HR_EMP_ASSIGNMENT_DTLS.MODIFIED_DATE = DateTime.Today;

                            tmpChildEntity.Add(new Tuple<object, string>(HR_EMP_ASSIGNMENT_DTLS, "U"));
                        }
                        else
                        {
                            HR_EMP_ASSIGNMENT_DTLS.ASSIGNMENT_ID = FINSP.GetSPFOR_SEQCode("HR_081".ToString(), false, true);
                            HR_EMP_ASSIGNMENT_DTLS.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, HR_EMP_ASSIGNMENT_DTLS.ASSIGNMENT_ID);
                            HR_EMP_ASSIGNMENT_DTLS.CREATED_BY = this.LoggedUserName;
                            HR_EMP_ASSIGNMENT_DTLS.CREATED_DATE = DateTime.Today;

                            tmpChildEntity.Add(new Tuple<object, string>(HR_EMP_ASSIGNMENT_DTLS, "A"));
                        }
                    }
                }

                //ProReturn = FINSP.GetSPFOR_ERR_MGR_CATEGORY_TWO(Master.FormCode, HR_EMP_ASSIGNMENT_DTLS.ASSIGNMENT_ID, HR_EMP_ASSIGNMENT_DTLS.ASSIGNMENT_EMP_ID, HR_EMP_ASSIGNMENT_DTLS.ASSIGNMENT_JOB);
                //if (ProReturn != string.Empty)
                //{
                //    if (ProReturn != "0")
                //    {
                //        ErrorCollection.Add("GRADE", ProReturn);
                //        if (ErrorCollection.Count > 0)
                //        {
                //            return;
                //        }
                //    }
                //}
                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            DBMethod.SaveEntity<HR_EMP_ASSIGNMENT_DTLS>(HR_EMP_ASSIGNMENT_DTLS);
                            saveBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {
                            DBMethod.SaveEntity<HR_EMP_ASSIGNMENT_DTLS>(HR_EMP_ASSIGNMENT_DTLS, true);
                            saveBool = true;
                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Emppradsof_ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();


                DropDownList ddl_Location = tmpgvr.FindControl("ddlLocation") as DropDownList;
                DropDownList ddl_Category = tmpgvr.FindControl("ddlCategory") as DropDownList;
                DropDownList ddlReporting = tmpgvr.FindControl("ddlReporting") as DropDownList;
              
                
               

               
                Employee_BLL.GetEmployeeName(ref ddlReporting);
                Location_BLL.GetLocations(ref ddl_Location);
                //Lookup_BLL.GetLookUpValues(ref ddl_Location, "LOCATION");
                Categories_BLL.fn_getCategory(ref ddl_Category, Master.Mode);

                if (gvData.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                {
                    DropDownList ddlPosition = tmpgvr.FindControl("ddlPosition") as DropDownList;
                    DropDownList ddlGrade = tmpgvr.FindControl("ddlGrade") as DropDownList;
                    DropDownList ddlJob = tmpgvr.FindControl("ddlJob") as DropDownList;
                    ddl_Location.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["LOCATION_ID"].ToString();
                    ddl_Category.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["CATEGORY_ID"].ToString();
                    filljob(tmpgvr);
                    ddlJob.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["assignment_job"].ToString();
                    fillposition(tmpgvr);
                    ddlPosition.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["assignment_position"].ToString();
                    fillGrade(tmpgvr);
                    ddlGrade.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["assignment_grade"].ToString();
                    ddlReporting.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["assignment_reporting_to"].ToString();
                  
                   
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Emppf_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session[FINSessionConstants.GridData], "Employee Assignment Details ");

                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                AssignToBE();

                if (saveBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        #endregion

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr["enabled_flag"] = "false";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Emppf_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            if (Session[FINSessionConstants.GridData] != null)
            {
                dtGridData = (DataTable)Session[FINSessionConstants.GridData];
            }
            gvData.EditIndex = -1;

            BindGrid(dtGridData);

        }
        #endregion

        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Emppf_ROW_CMD", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {
            System.Collections.SortedList slControls = new System.Collections.SortedList();
            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/HR_" + Session["Sel_Lng"].ToString() + ".properties"));
            DropDownList ddl_Location = gvr.FindControl("ddlLocation") as DropDownList;
            DropDownList ddl_Category = gvr.FindControl("ddlCategory") as DropDownList;
            DropDownList ddlGrade = gvr.FindControl("ddlGrade") as DropDownList;
            DropDownList ddlJob = gvr.FindControl("ddlJob") as DropDownList;

            TextBox txtFromDate = gvr.FindControl("txtFromDate") as TextBox;
            TextBox txtToDate = gvr.FindControl("txtToDate") as TextBox;

            DropDownList ddlPosition = gvr.FindControl("ddlPosition") as DropDownList;
            DropDownList ddlReporting = gvr.FindControl("ddlReporting") as DropDownList;

            CheckBox chkActive = gvr.FindControl("chkActive") as CheckBox;

            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList["ASSIGNMENT_ID"] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);
            }

            slControls[0] = ddl_Category;
            slControls[1] = ddlGrade;
            slControls[2] = ddlJob;
            slControls[3] = ddlPosition;
            slControls[4] = ddlReporting;
            slControls[5] = txtFromDate;
            slControls[6] = txtFromDate;
            slControls[7] = txtToDate;

            ErrorCollection.Clear();

            string strCtrlTypes = "DropDownList~DropDownList~DropDownList~DropDownList~DropDownList~TextBox~DateTime~DateRangeValidate";
            string strMessage = Prop_File_Data["Category_P"] + " ~ " + Prop_File_Data["Grade_P"] + " ~ " + Prop_File_Data["Job_P"] + " ~ " + Prop_File_Data["Position_P"] + " ~ " + Prop_File_Data["Reporting_P"] + " ~ " + Prop_File_Data["From_Date_P"] + " ~ " + Prop_File_Data["From_Date_P"] + " ~ " + Prop_File_Data["To_Date_P"] + "";
            //string strMessage = "Category ~ Grade ~ Job ~ Position ~ Reporting ~ From Date ~ From Date ~ To Date";

            EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

            if (EmptyErrorCollection.Count > 0)
            {
                ErrorCollection = EmptyErrorCollection;
                return drList;
            }


            drList["LOCATION_ID"] = ddl_Location.SelectedValue;
            drList["LOCATION_NAME"] = ddl_Location.SelectedItem.Text;

            drList["CATEGORY_ID"] = ddl_Category.SelectedValue;
            drList["CATEGORY_NAME"] = ddl_Category.SelectedItem.Text;

            if (ddlJob.SelectedItem != null)
            {
                drList["ASSIGNMENT_JOB"] = ddlJob.SelectedItem.Value;
                drList["job_name"] = ddlJob.SelectedItem.Text;
            }
            if (ddlGrade.SelectedItem != null)
            {
                drList["ASSIGNMENT_GRADE"] = ddlGrade.SelectedItem.Value;
                drList["grade_name"] = ddlGrade.SelectedItem.Text;
            }
            if (ddlPosition.SelectedItem != null)
            {
                drList["ASSIGNMENT_POSITION"] = ddlPosition.SelectedItem.Value;
                drList["position_Name"] = ddlPosition.SelectedItem.Text;
            }
            if (ddlPosition.SelectedItem != null)
            {
                drList["ASSIGNMENT_REPORTING_TO"] = ddlReporting.SelectedItem.Value;
                drList["reporting_name"] = ddlReporting.SelectedItem.Text;
            }



            if (txtFromDate.Text.ToString().Length > 0)
            {
                drList["ASSIGNMENT_FROM_DT"] = DBMethod.ConvertStringToDate(txtFromDate.Text.ToString());
            }

            if (txtToDate.Text.ToString().Length > 0)
            {

                drList["ASSIGNMENT_TO_DT"] = DBMethod.ConvertStringToDate(txtToDate.Text.ToString());
            }
            else
            {
                drList["ASSIGNMENT_TO_DT"] = DBNull.Value;
            }

            //drList["ASSIGNMENT_FROM_DT"] = DBMethod.ConvertStringToDate(txtFromDate.Text.ToString());
            //drList["ASSIGNMENT_TO_DT"] = DBMethod.ConvertStringToDate(txtToDate.Text.ToString());

            drList[FINColumnConstants.ENABLED_FLAG] = chkActive.Checked == true ? FINAppConstants.TRUEFLAG : FINAppConstants.FALSEFLAG;
            drList[FINColumnConstants.DELETED] = FINAppConstants.N;

            return drList;
        }



        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Emppf_ROW_UP", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Emppf_ROW_DEL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Emppf_ROW_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Emppf_RowCreated", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Emppf_RDB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                // COURSE_GROUP.COURSE_GROUP_ID = short.Parse(Master.RecordID.ToString());
                DBMethod.DeleteEntity<HR_EMP_ASSIGNMENT_DTLS>(HR_EMP_ASSIGNMENT_DTLS);
                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATADELETED);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Emppf_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void ddlcompetency_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;

            fillcomplevel(gvr);

        }

        private void fillcomplevel(GridViewRow gvr)
        {
            DropDownList ddlcompetency = gvr.FindControl("ddlcompetency") as DropDownList;
            DropDownList ddlcompetencyLevel = gvr.FindControl("ddlcompetencyLevel") as DropDownList;

            FIN.BLL.HR.EmployeeProfile_BLL.fn_GetCompetencyLevedesc(ref ddlcompetencyLevel, ddlcompetency.SelectedValue);

        }

        protected void ddlcategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            // fillgrade();
        }

        //protected void ddlgrade_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    filljob();
        //}

        //protected void ddljob_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    fillposition();
        //}

        protected void ddlcompetencyLevel_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            DropDownList ddlcompetency = gvr.FindControl("ddlcompetency") as DropDownList;
            DropDownList ddlcompetencyLevel = gvr.FindControl("ddlcompetencyLevel") as DropDownList;


        }

        protected void ddlEmployee_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindEmName();
        }


        private void BindEmName()
        {
            DataTable dtData = new DataTable();
            dtData = DBMethod.ExecuteQuery(Employee_DAL.GetEmployeeName(ddlEmployee.SelectedValue)).Tables[0];
            if (dtData != null)
            {
                if (dtData.Rows.Count > 0)
                {
                    txtEmpName.Text = dtData.Rows[0]["emp_name"].ToString();
                }
            }
        }
    }
}