﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="AppraisalAssesmentEntry.aspx.cs" Inherits="FIN.Client.HR.AppraisalAssesmentEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 800px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblDepartment">
                Department
            </div>
            <div class="divtxtBox  LNOrient" style="width: 155px">
                <asp:DropDownList ID="ddlDepartment" runat="server" TabIndex="1" CssClass="validate[required] RequiredField ddlStype">
                </asp:DropDownList>
            </div>
           <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 195px" id="lblJob">
                Job
            </div>
            <div class="divtxtBox  LNOrient" style="width: 155px">
                <asp:DropDownList ID="ddlJob" runat="server" TabIndex="2" CssClass="validate[required] RequiredField ddlStype"
                    AutoPostBack="True" OnSelectedIndexChanged="ddlJob_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblappraisal">
                Appraisal
            </div>
            <div class="divtxtBox  LNOrient" style="width: 155px">
                <asp:DropDownList ID="ddlAppraisal" runat="server" TabIndex="3" CssClass="validate[required] RequiredField ddlStype"
                    AutoPostBack="True" OnSelectedIndexChanged="ddlAppraisal_SelectedIndexChanged">
                    <%--<asp:ListItem Value="">---Select---</asp:ListItem>--%>
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 195px" id="lblAppraisalCode">
                Recommendation
            </div>
            <div class="divtxtBox  LNOrient" style="width: 155px">
                <asp:DropDownList ID="ddlRecommendation" runat="server" TabIndex="4" AutoPostBack="True"
                    CssClass="validate[required] RequiredField ddlStype" 
                    onselectedindexchanged="ddlRecommendation_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblReviewtype">
                Assessment Date
            </div>
            <div class="divtxtBox  LNOrient" style="width: 155px">
                <asp:TextBox ID="txtAssessmentDate" CssClass="validate[required] RequiredField txtBox"
                    runat="server" TabIndex="6"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtAssessmentDate" />
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtAssessmentDate" />
            </div>
            <%-- <div class="lblBox" style="float: left; width: 200px" id="lblReviewtype">
                Review Type
            </div>
            <div class="divtxtBox" style="float: left; width: 155px">
                <asp:DropDownList ID="ddlreviewType" runat="server" TabIndex="5" 
                    CssClass="validate[required] RequiredField ddlStype" AutoPostBack="True" 
                    onselectedindexchanged="ddlreviewType_SelectedIndexChanged">
                </asp:DropDownList>
            </div>--%>
           <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 195px" id="lblAssessmentDate">
                Appraiser
            </div>
            <div class="divtxtBox  LNOrient" style="width: 155px">
                <asp:DropDownList ID="ddlAppraiser" runat="server" TabIndex="7" CssClass="validate[required] RequiredField ddlStype"
                    AutoPostBack="True" OnSelectedIndexChanged="ddlAppraiser_SelectedIndexChanged">
                    <%--<asp:ListItem Value="">---Select---</asp:ListItem>--%>
                </asp:DropDownList>
                <asp:HiddenField ID="hfAppraiserEmpId" runat="server" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblAppraiser">
                Appraisee
            </div>
            <div class="divtxtBox  LNOrient" style="width: 525px">
                <asp:DropDownList ID="ddlAppraisee" runat="server" TabIndex="8" CssClass="validate[required] RequiredField ddlStype"
                    AutoPostBack="True" OnSelectedIndexChanged="ddlAppraisee_SelectedIndexChanged">
                    <%--<asp:ListItem Value="">---Select---</asp:ListItem>--%>
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblOverallAssessment">
                Overall Assessment
            </div>
            <div class="divtxtBox  LNOrient" style="width: 535px">
                <asp:TextBox ID="txtOverallAssessment" TextMode="MultiLine" CssClass="validate[required] RequiredField txtBox" MaxLength="500"
                    runat="server" TabIndex="9"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblComments">
                Comments
            </div>
            <div class="divtxtBox  LNOrient" style="width: 535px">
                <asp:TextBox ID="txtComments" TextMode="MultiLine" CssClass="txtBox" runat="server" MaxLength="500"
                    TabIndex="11"></asp:TextBox>
            </div>
        </div>
         <div class="divClear_10">
        </div>
        <div class="divRowContainer" id="divNeeded" visible="false" runat="server">
            <div class="lblBox LNOrient" style="width: 200px" id="lblTrainingNeeded">
                Training Needed
            </div>
            <div class="divtxtBox  LNOrient" style="width: 535px">
                <asp:TextBox ID="txtTrainingNeeded" CssClass="validate[required] RequiredField txtBox" MaxLength="50"
                    runat="server" TabIndex="12"></asp:TextBox>
            </div>
        </div>

        <div class="divClear_10">
        </div>
        <div class="LNOrient">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                DataKeyNames="REV_DTL_ID,ASSIGN_DTL_ID">
                <Columns>
                    <asp:BoundField DataField="PER_DESC" HeaderText="Description" />
                    <asp:BoundField DataField="PER_TYPE" HeaderText="TYPE" />
                    <asp:BoundField DataField="PER_MIN_RATING" HeaderText="Min Rate" >
                    <ItemStyle HorizontalAlign="Right" />
                    </asp:BoundField>
                    <asp:BoundField DataField="PER_MAX_RATING" HeaderText="Max Rate" >
                    <ItemStyle HorizontalAlign="Right" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="Self Rate">
                        <ItemTemplate>
                            <asp:TextBox ID="txtRevSelfRating" MaxLength="4" runat="server" CssClass="EntryFont RequiredField  txtBox_N"
                                Width="30px" Text='<%# Eval("REV_SELF_RATING") %>'></asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Self Comments">
                        <ItemTemplate>
                            <asp:TextBox ID="txtRevSelfComments" MaxLength="500" runat="server" CssClass="EntryFont RequiredField  txtBox"
                                Width="130px" Text='<%# Eval("REV_SELF_COMMENTS") %>'></asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="App Date">
                        <ItemTemplate>
                            <asp:TextBox ID="txtRevSelfDate" MaxLength="10" runat="server" CssClass="EntryFont RequiredField  txtBox"
                                Width="80px" Text='<%#  Eval("REV_SELF_APP_DATE","{0:dd/MM/yyyy}") %>'></asp:TextBox>
                            <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtenderS1"
                                TargetControlID="txtRevSelfDate" />
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtenderS1" runat="server" ValidChars="/"
                                FilterType="Numbers,Custom" TargetControlID="txtRevSelfDate" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Reviewer Rate">
                        <ItemTemplate>
                            <asp:TextBox ID="txtRevReviewerRating" MaxLength="4" runat="server" CssClass="EntryFont RequiredField  txtBox_N"
                                Width="80px" Text='<%# Eval("REV_REVIEWER_RATING") %>'></asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Reviewer Comments">
                        <ItemTemplate>
                            <asp:TextBox ID="txtRevReviewerComments" MaxLength="500" runat="server" CssClass="EntryFont RequiredField  txtBox"
                                Width="130px" Text='<%# Eval("REV_REVIEWER_COMMENTS") %>'></asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Reviewer Date">
                        <ItemTemplate>
                            <asp:TextBox ID="txtRevReviewerDate" MaxLength="10" runat="server" CssClass="EntryFont RequiredField  txtBox"
                                Width="80px" Text='<%#  Eval("REV_DATE","{0:dd/MM/yyyy}") %>'></asp:TextBox>
                            <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtenderS3"
                                TargetControlID="txtRevReviewerDate" />
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtenderS3" runat="server" ValidChars="/"
                                FilterType="Numbers,Custom" TargetControlID="txtRevReviewerDate" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer " align="left">
            <asp:GridView ID="gvdata2" runat="server" AutoGenerateColumns="False" CssClass="Grid">
                <Columns>
                    <asp:BoundField DataField="APPR_NAME" HeaderText="Appraiser" />
                    <asp:BoundField DataField="PER_DESC" HeaderText="Description" />
                    <asp:BoundField DataField="PER_TYPE" HeaderText="TYPE" />
                    <asp:BoundField DataField="PER_MIN_RATING" HeaderText="Min Rate" ItemStyle-HorizontalAlign = "Right"  />
                    <asp:BoundField DataField="PER_MAX_RATING" HeaderText="Max Rate" ItemStyle-HorizontalAlign = "Right"  />
                    <asp:BoundField DataField="REV_SELF_RATING" HeaderText="Self Rate" ItemStyle-HorizontalAlign = "Right"  />
                    <asp:BoundField DataField="REV_SELF_COMMENTS" HeaderText="Self Comments" />
                    <asp:BoundField DataField="REV_SELF_APP_DATE" HeaderText="App Date" DataFormatString="{0:d}" />
                    <asp:BoundField DataField="REV_REVIEWER_RATING" HeaderText="Reviewer Rate" ItemStyle-HorizontalAlign = "Right"  />
                    <asp:BoundField DataField="REV_REVIEWER_COMMENTS" HeaderText="Reviewer Comments" />
                    <asp:BoundField DataField="REV_DATE" HeaderText="Reviewer Date" DataFormatString="{0:d}" />
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                            TabIndex="11" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="12" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="13" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="14" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table>
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" CssClass="button" ID="btnYes" Text="Yes" Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/HR/HRChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
