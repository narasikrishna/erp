﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="LeaveCancellationEntry.aspx.cs" Inherits="FIN.Client.HR.LeaveCancellationEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1200px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="lblFinancialYear">
                Financial Year
            </div>
            <div class="divtxtBox  LNOrient" style="width: 155px">
                <asp:DropDownList ID="ddlFinancialYear" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    TabIndex="1">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 150px" id="lblCancelDate">
                Cancel Date
            </div>
            <div class="divtxtBox  LNOrient" style="width: 155px">
                <asp:TextBox ID="txtCancelDate" CssClass="EntryFont RequiredField txtBox" runat="server"
                    TabIndex="2"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender3" TargetControlID="txtCancelDate"
                    OnClientDateSelectionChanged="checkDate" />
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtCancelDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="Div1">
                Department
            </div>
            <div class="divtxtBox  LNOrient" style="width: 488px">
                <asp:DropDownList ID="ddlDept" runat="server" AutoPostBack="True" CssClass="validate[required] EntryFont RequiredField ddlStype"
                    TabIndex="3" OnSelectedIndexChanged="ddlDept_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="lblStaffName">
                Staff Name
            </div>
            <div class="divtxtBox  LNOrient" style="width: 488px">
                <asp:DropDownList ID="ddlStaffName" runat="server" AutoPostBack="True" CssClass="validate[required] EntryFont RequiredField ddlStype"
                    OnSelectedIndexChanged="ddlStaffName_SelectedIndexChanged" TabIndex="3">
                </asp:DropDownList>
            </div>
        </div>
       
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="lblLeaveApplication">
                Leave Application
            </div>
            <div class="divtxtBox  LNOrient" style="width: 488px">
                <asp:DropDownList ID="ddlLeaveApplication" runat="server" CssClass="validate[required] EntryFont RequiredField ddlStype"
                    TabIndex="4" AutoPostBack="True" OnSelectedIndexChanged="ddlLeaveApplication_SelectedIndexChanged">
                    <%--<asp:ListItem>--- Select ---</asp:ListItem>--%>
                </asp:DropDownList>
            </div>
        </div>
         <div class="divClear_10">
        </div>
        <div runat="server" id="IdPayPrd">
            <div class="lblBox LNOrient" style="width: 150px" id="Div9">
                Pay Period
            </div>
            <div class="divtxtBox  LNOrient" style="width: 488px">
                <asp:DropDownList ID="ddlPayperiod" runat="server" TabIndex="10" 
                    CssClass="ddlStype" AutoPostBack="True" 
                    onselectedindexchanged="ddlPayperiod_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="lblLeaveFrom">
                Leave From
            </div>
            <div class="divtxtBox  LNOrient" style="width: 155px">
                <asp:TextBox ID="txtLeaveFrom" CssClass="validate[required, custom[ReqDateDDMMYYY]]  RequiredField  txtBox"
                    AutoPostBack="True" OnTextChanged="txtLeaveTo_TextChanged" runat="server" TabIndex="5"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtLeaveFrom"
                    OnClientDateSelectionChanged="checkDate" />
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtLeaveFrom" />
                
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 150px" id="lblLeaveTo">
                Leave To
            </div>
            <div class="divtxtBox  LNOrient" style="width: 155px">
                <asp:TextBox ID="txtLeaveTo" CssClass="validate[required, custom[ReqDateDDMMYYY]] RequiredField  txtBox"
                    runat="server" AutoPostBack="True" OnTextChanged="txtLeaveTo_TextChanged" TabIndex="6"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender2" TargetControlID="txtLeaveTo" />
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtLeaveTo" />
                   
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="lblNoOfDays">
                No. Of Days
            </div>
            <div class="divtxtBox  LNOrient" style="width: 155px">
                <asp:TextBox ID="txtNoOfDays" MaxLength="10" CssClass="EntryFont txtBox_N" runat="server"
                    Enabled="false" TabIndex="7"></asp:TextBox>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 150px" id="lbllevsal" runat="server">
                Leave Salary
            </div>
            <div class="divtxtBox  LNOrient" style="width: 155px" id="IDLevsal" runat="server">
                <asp:TextBox ID="txtLevSal" CssClass="  txtBox_N" runat="server" TabIndex="8"
                    Width="150px" MaxLength="13"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="lblCancelReason">
                Cancel Reason
            </div>
            <div class="divtxtBox  LNOrient" style="width: 485px">
                <asp:DropDownList ID="ddlCancelReason" CssClass="validate[required] EntryFont RequiredField ddlStype"
                    runat="server" TabIndex="8">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="lblHalfDayFrom">
                Half Day From
            </div>
            <div class="divtxtBox  LNOrient" style="width: 155px">
                <asp:CheckBox ID="chkHalfDayFrom" runat="server" Checked="True" TabIndex="9" />
            </div>
             <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 150px" id="lblHalfDayTo">
                Half Day To
            </div>
            <div class="divtxtBox  LNOrient" style="width: 155px">
                <asp:CheckBox ID="chkHalfDayTo" runat="server" Checked="True" TabIndex="10" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 150px" id="Div3">
                Remarks
            </div>
            <div class="divtxtBox  LNOrient" style="width: 488px; height: 50px;">
                <asp:TextBox ID="txtRemarks" MaxLength="240" Height="50px" CssClass="validate[] txtBox"
                    TextMode="MultiLine" runat="server" TabIndex="11"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <asp:HiddenField runat="server" ID="hfLC_HDR_ID" />
         <asp:HiddenField runat="server" ID="hfLevFrm" />
         <asp:HiddenField runat="server" ID="hfLevto" />
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                            TabIndex="12" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="13" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="14" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" OnClick="btnBack_Click"
                            TabIndex="15" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table width="100%">
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/HR/HRChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
