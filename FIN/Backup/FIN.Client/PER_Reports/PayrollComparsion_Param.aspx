﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="PayrollComparsion_Param.aspx.cs" Inherits="FIN.Client.PER_Reports.PayrollComparsion_Param" %>

    <%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 700px" id="div1">
        <div class="divRowContainer">
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="  width: 120px" id="lblDate">
                    From Period
                </div>
                <div class="divtxtBox LNOrient" style="  width: 200px">
                    <asp:DropDownList ID="ddlFromPeriod" runat="server" TabIndex="2" CssClass="ddlStype">
                    </asp:DropDownList>
                </div>
                 <div class="lblBox LNOrient" style="  width: 120px" id="Div2">
                    To Period
                </div>
                <div class="divtxtBox LNOrient" style="  width: 200px">
                   <asp:DropDownList ID="ddlToPeriod" runat="server" TabIndex="2" CssClass="ddlStype">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="divClear_10">
            </div>
           <div class="divRowContainer">
                <div class="lblBox LNOrient" style="  width: 120px" id="lblEmployeeName">
                    Employee Name
                </div>
                <div class="divtxtBox LNOrient" style="  width: 532px">
                    <asp:DropDownList ID="ddlEmployeeName" runat="server" TabIndex="1" CssClass="ddlStype">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divFormcontainer" style="width: 600px" id="divMainContainer">
                <div class="divRowContainer divReportAction">
                    <table class="ReportTable">
                        <tr>
                            <td>
                                <asp:ImageButton ID="btnSave" runat="server" ImageUrl="../Images/show-report-icon.png"
                                    OnClick="btnSave_Click" Width="35px" Height="25px" Style="border: 0px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/PER/PAChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
