﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


using FIN.DAL;
using System.Data;

namespace FIN.Client.HR_DASHBOARD
{
    public partial class VacancyChart : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                FillComboBox();



            }
        }
        private void FillComboBox()
        {
            FIN.BLL.HR.DepartmentDetails_BLL DepartmentDetails_BLL = new FIN.BLL.HR.DepartmentDetails_BLL();
            DepartmentDetails_BLL.fn_GetDepartmentDetails(ref ddlDeptname);

        }

        private void FillOrgEmpVacancy()
        {

            string str_ORG_STR = "";
            string str_tmp = "";
            string str_Replace_str = "";
            string str_new_replace_str = "";
            DataTable dt_OrgData = DBMethod.ExecuteQuery(FIN.DAL.HR.DashBoard_HR_DAL.getEmployeeVacancyDet(ddlDeptname.SelectedValue)).Tables[0];

            var var_HeadEmpList = dt_OrgData.AsEnumerable().Where(r => r["PARENT_DESIG"].ToString().Contains("HEAD"));
            DataTable dt_HeadEmplist = System.Data.DataTableExtensions.CopyToDataTable(var_HeadEmpList);

            DataTable dt_EmpProcessList = new DataTable();
            dt_EmpProcessList.Columns.Add("EMP_ID");
            dt_EmpProcessList.Columns.Add("EMP_NO");
            dt_EmpProcessList.Columns.Add("EMP_NAME");
            dt_EmpProcessList.Columns.Add("DESIG_NAME");
            dt_EmpProcessList.Columns.Add("DESIG_ID");
            dt_EmpProcessList.Columns.Add("STATUS");
            dt_EmpProcessList.Columns.Add("NO_OF_EMP");

            str_ORG_STR += "  <ul id='organisation'> ";
            str_ORG_STR += "  <li>  " + ddlDeptname.SelectedItem.Text;
            str_ORG_STR += " <ul>";
            for (int iLoop = 0; iLoop < dt_HeadEmplist.Rows.Count; iLoop++)
            {
                DataRow dr = dt_EmpProcessList.NewRow();
                dr["EMP_ID"] = dt_HeadEmplist.Rows[iLoop]["EMP_ID"].ToString();
                dr["EMP_NO"] = dt_HeadEmplist.Rows[iLoop]["EMP_NO"].ToString();
                dr["EMP_NAME"] = dt_HeadEmplist.Rows[iLoop]["EMP_NAME"].ToString();
                dr["DESIG_NAME"] = dt_HeadEmplist.Rows[iLoop]["DESIG_NAME"].ToString();
                dr["DESIG_ID"] = dt_HeadEmplist.Rows[iLoop]["DESIG_ID"].ToString();
                dr["NO_OF_EMP"] = dt_HeadEmplist.Rows[iLoop]["NO_OF_EMP"].ToString();
                dr["STATUS"] = "N";
                dt_EmpProcessList.Rows.Add(dr);

                str_ORG_STR += "<li>";
                str_ORG_STR += dt_HeadEmplist.Rows[iLoop]["EMP_ID"].ToString();
                str_ORG_STR += "</li>";
                DataRow[] dr_col;
                do
                {
                    dr_col = dt_EmpProcessList.Select("STATUS='N'");
                    if (dr_col.Length > 0)
                    {
                        str_Replace_str = "<li>";
                        str_Replace_str += dr_col[0]["EMP_ID"].ToString();
                        str_Replace_str += "</li>";

                        str_new_replace_str = "<li>";


                        str_new_replace_str += "<TABLE border ='1' align='left'>";
                        str_new_replace_str += " <TR><TD> <img class='imgSize' src='../UploadFile/PHOTO/" + dr_col[0]["EMP_ID"].ToString() + ".jpg' alt='  '/></TD></TR>";
                        str_new_replace_str += " <TR><TD>" + dr_col[0]["EMP_NO"].ToString() + " - " + dr_col[0]["DESIG_NAME"].ToString() + "</TD></TR>";
                        str_new_replace_str += " <TR><TD>" + dr_col[0]["EMP_NAME"].ToString() + "</TD></TR>";
                        str_new_replace_str += " <TR><TD> Required Employee : " + dr_col[0]["NO_OF_EMP"].ToString() + "</TD></TR>";
                        str_new_replace_str += "</TABLE>";
                        str_new_replace_str += "<UL>";



                        var var_empList = dt_OrgData.AsEnumerable().Where(r => r["PARENT_DESIG"].ToString().Contains(dr_col[0]["DESIG_ID"].ToString()));
                        str_tmp = "";
                        if (var_empList.Any())
                        {
                            DataTable dt_EMP_List = System.Data.DataTableExtensions.CopyToDataTable(var_empList);

                            for (int jLoop = 0; jLoop < dt_EMP_List.Rows.Count; jLoop++)
                            {
                                str_tmp += "<li>";
                                str_tmp += dt_EMP_List.Rows[jLoop]["EMP_ID"].ToString();
                                str_tmp += "</li>";

                                dr = dt_EmpProcessList.NewRow();
                                dr["EMP_ID"] = dt_EMP_List.Rows[jLoop]["EMP_ID"].ToString();
                                dr["EMP_NO"] = dt_EMP_List.Rows[jLoop]["EMP_NO"].ToString();
                                dr["EMP_NAME"] = dt_EMP_List.Rows[jLoop]["EMP_NAME"].ToString();
                                dr["DESIG_NAME"] = dt_EMP_List.Rows[jLoop]["DESIG_NAME"].ToString();
                                dr["DESIG_ID"] = dt_EMP_List.Rows[jLoop]["DESIG_ID"].ToString();
                                dr["NO_OF_EMP"] = dt_EMP_List.Rows[jLoop]["NO_OF_EMP"].ToString();
                                dr["STATUS"] = "N";
                                dt_EmpProcessList.Rows.Add(dr);

                            }

                            if (dr_col[0]["NO_OF_EMP"].ToString().Length > 0)
                            {
                                for (int kLoop = dt_EMP_List.Rows.Count; kLoop < int.Parse(dr_col[0]["NO_OF_EMP"].ToString()); kLoop++)
                                {
                                    str_tmp += "<li>";
                                    str_tmp += "<TABLE border ='1' align='left'>";
                                    str_tmp += " <TR><TD> Vacancy Found </TD></TR>";
                                    str_tmp += " <TR><TD> <img class='imgSize' src='../Images/Jobvacancy.jpg' alt=' New Vacancy '/></TD></TR>";
                                    str_tmp += "</TABLE>";
                                    str_tmp += "</li>";
                                }
                            }
                            str_ORG_STR = str_ORG_STR.Replace(str_Replace_str, str_new_replace_str + str_tmp + "</UL></li>");
                        }
                        else
                        {
                            if (dr_col[0]["NO_OF_EMP"].ToString().Length > 0)
                            {

                                for (int kLoop = 0; kLoop < int.Parse(dr_col[0]["NO_OF_EMP"].ToString()); kLoop++)
                                {
                                    str_tmp += "<li>";
                                    str_tmp += "<TABLE border ='1' align='left'>";
                                    str_tmp += " <TR><TD> Vacancy Found </TD></TR>";
                                    str_tmp += " <TR><TD> <img class='imgSize' src='../Images/Jobvacancy.jpg' alt=' New Vacancy '/></TD></TR>";
                                    str_tmp += "</TABLE>";
                                    str_tmp += "</li>";
                                }

                                str_ORG_STR = str_ORG_STR.Replace(str_Replace_str, str_new_replace_str + str_tmp + "</UL></li>");
                            }
                            else
                            {
                                str_ORG_STR = str_ORG_STR.Replace(str_Replace_str, str_new_replace_str.Replace("<UL>", "</li>"));
                            }
                        }

                        dr_col[0]["STATUS"] = "Y";
                        dt_EmpProcessList.AcceptChanges();
                    }
                    dr_col = dt_EmpProcessList.Select("STATUS='N'");
                } while (dr_col.Length > 0);

            }
            str_ORG_STR += " </ul>";
            str_ORG_STR += "  </li> ";
            str_ORG_STR += "  </ul> ";

            left.InnerHtml = str_ORG_STR;

            ScriptManager.RegisterStartupScript(ddlDeptname, typeof(DropDownList), "LoadGraph", "fn_showGraph()", true);

        }



        protected void ddlDeptname_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillOrgEmpVacancy();
        }
    }
}