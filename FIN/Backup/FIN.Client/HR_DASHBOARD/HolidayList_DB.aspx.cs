﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using FIN.DAL;
namespace FIN.Client.HR_DASHBOARD
{
    public partial class HolidayList_DB : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {

                FillComboBox();

                getHolidayDetails();

            }
        }
        private void FillComboBox()
        {

            string str_finyear = FINSP.GetSPFOR_FiscalYear(DateTime.Now.Date.ToString("dd/MM/yyyy"));

            FIN.BLL.GL.AccountingCalendar_BLL.GetCalAcctYear(ref ddlHolFinYear);
            if (ddlHolFinYear.Items.Count > 0)
            {
                //ddlLAFinYear.SelectedIndex = ddlLAFinYear.Items.Count - 1;
                ddlHolFinYear.SelectedValue = str_finyear;
            }


        }
        private void getHolidayDetails()
        {
            DataTable dt_Holiday = DBMethod.ExecuteQuery(FIN.DAL.HR.HolidayMaster_DAL.GetHolodayDetails_finyr(ddlHolFinYear.SelectedValue)).Tables[0];
            gvHolidayDet.DataSource = dt_Holiday;
            gvHolidayDet.DataBind();
        }
        protected void ddlHolFinYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            getHolidayDetails();

        }
    }
}