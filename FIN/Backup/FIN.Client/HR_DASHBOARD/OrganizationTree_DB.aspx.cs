﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using FIN.DAL;
using System.Data;
using System.IO;

using iTextSharp.text;

using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
namespace FIN.Client.HR_DASHBOARD
{
    public partial class OrganizationTree_DB : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //FillOrgData();
                FillComboBox();
                FillOrgEmpStructure();

            }
        }
        private void FillComboBox()
        {
            FIN.BLL.Lookup_BLL.GetLookUpValues(ref ddlDeptGroup, "DEPT_TYP", true);
            FIN.BLL.HR.Department_BLL.GetDepartmentName(ref ddlDeptname, false);

        }
        private void FillOrgEmpStructure()
        {

            string str_ORG_STR = "";
            string str_tmp = "";
            string str_Replace_str = "";
            string str_new_replace_str = "";
            DataTable dt_OrgData = DBMethod.ExecuteQuery(FIN.DAL.HR.DashBoard_HR_DAL.getEmployOrgStructure(ddlDeptGroup.SelectedValue.ToString(), ddlDeptname.SelectedValue.ToString())).Tables[0];

            if (dt_OrgData.Rows.Count == 0)
            {
                return;
            }
            var var_HeadEmpList = dt_OrgData.AsEnumerable().Where(r => r["REPORTING_TO"].ToString().Contains("HEAD"));
            DataTable dt_HeadEmplist = new DataTable();
            if (var_HeadEmpList.Any())
            {

                dt_HeadEmplist = System.Data.DataTableExtensions.CopyToDataTable(var_HeadEmpList);
            }
            if (dt_HeadEmplist.Rows.Count == 0)
            {
                return;
            }

            DataTable dt_EmpProcessList = new DataTable();
            dt_EmpProcessList.Columns.Add("EMP_ID");
            dt_EmpProcessList.Columns.Add("EMP_NO");
            dt_EmpProcessList.Columns.Add("EMP_NAME");
            dt_EmpProcessList.Columns.Add("DEPT_NAME");
            dt_EmpProcessList.Columns.Add("DESIG_NAME");
            dt_EmpProcessList.Columns.Add("STATUS");

            str_ORG_STR += "  <ul id='organisation'> ";
            str_ORG_STR += "  <li>  " + VMVServices.Web.Utils.OrganizationName;
            str_ORG_STR += " <ul>";
            for (int iLoop = 0; iLoop < dt_HeadEmplist.Rows.Count; iLoop++)
            {
                DataRow dr = dt_EmpProcessList.NewRow();
                dr["EMP_ID"] = dt_HeadEmplist.Rows[iLoop]["EMP_ID"].ToString();
                dr["EMP_NO"] = dt_HeadEmplist.Rows[iLoop]["EMP_NO"].ToString();
                dr["EMP_NAME"] = dt_HeadEmplist.Rows[iLoop]["EMP_NAME"].ToString();
                dr["DEPT_NAME"] = dt_HeadEmplist.Rows[iLoop]["DEPT_NAME"].ToString();
                dr["DESIG_NAME"] = dt_HeadEmplist.Rows[iLoop]["DESIG_NAME"].ToString();
                dr["STATUS"] = "N";
                dt_EmpProcessList.Rows.Add(dr);
                str_ORG_STR += "<li>";
                str_ORG_STR += dt_HeadEmplist.Rows[iLoop]["EMP_ID"].ToString();
                str_ORG_STR += "</li>";
                DataRow[] dr_col;
                do
                {
                    dr_col = dt_EmpProcessList.Select("STATUS='N'");
                    if (dr_col.Length > 0)
                    {
                        str_Replace_str = "<li>";
                        str_Replace_str += dr_col[0]["EMP_ID"].ToString();
                        str_Replace_str += "</li>";

                        str_new_replace_str = "<li>";
                        //str_new_replace_str += "<TABLE border ='1' align='left'>";
                        //str_new_replace_str += " <TR><TD ROWSPAN='2'> <img class='imgSize' src='../UploadFile/PHOTO/" + dr_col[0]["EMP_ID"].ToString() + ".jpg' alt='  '/></TD><TD>" + dr_col[0]["EMP_NO"].ToString() + "</TD></TR>";
                        //str_new_replace_str += " <TR><TD>" + dr_col[0]["EMP_NAME"].ToString() + "</TD></TR>";
                        //str_new_replace_str += " <TR><TD COLSPAN='3'> <TABLE  > ";
                        //str_new_replace_str += " <TR><TD>" + dr_col[0]["DEPT_NAME"].ToString() + "</TD></TR>";
                        //str_new_replace_str += "  </TABLE > ";
                        //str_new_replace_str += " <TR><TD COLSPAN='3'> <TABLE > ";
                        //str_new_replace_str += " <TR><TD>" + dr_col[0]["DESIG_NAME"].ToString() + "</TD></TR>";
                        //str_new_replace_str += "</TABLE> </TD></TR> ";
                        //str_new_replace_str += "</TABLE>";

                        str_new_replace_str += "<a href='#'><TABLE border ='1' align='left'>";
                        str_new_replace_str += " <TR><TD> <img class='imgSize' src='../UploadFile/PHOTO/" + dr_col[0]["EMP_ID"].ToString() + ".jpg' alt='  '/></TD></TR>";
                        str_new_replace_str += " <TR><TD>" + dr_col[0]["EMP_NO"].ToString() + "</TD></TR>";
                        // str_new_replace_str += " <TR><TD>" + dr_col[0]["DEPT_NAME"].ToString() + "</TD></TR>";                        
                        str_new_replace_str += " <TR><TD>" + dr_col[0]["DESIG_NAME"].ToString() + "</TD></TR>";
                        str_new_replace_str += " <TR><TD>" + dr_col[0]["EMP_NAME"].ToString() + "</TD></TR>";
                        str_new_replace_str += "</TABLE></a>";
                        str_new_replace_str += "<UL>";



                        var var_empList = dt_OrgData.AsEnumerable().Where(r => r["REPORTING_TO"].ToString().Contains(dr_col[0]["EMP_ID"].ToString()));
                        str_tmp = "";
                        if (var_empList.Any())
                        {
                            DataTable dt_EMP_List = System.Data.DataTableExtensions.CopyToDataTable(var_empList);

                            for (int jLoop = 0; jLoop < dt_EMP_List.Rows.Count; jLoop++)
                            {
                                str_tmp += "<li>";
                                str_tmp += dt_EMP_List.Rows[jLoop]["EMP_ID"].ToString();
                                str_tmp += "</li>";

                                dr = dt_EmpProcessList.NewRow();
                                dr["EMP_ID"] = dt_EMP_List.Rows[jLoop]["EMP_ID"].ToString();
                                dr["EMP_NO"] = dt_EMP_List.Rows[jLoop]["EMP_NO"].ToString();
                                dr["EMP_NAME"] = dt_EMP_List.Rows[jLoop]["EMP_NAME"].ToString();
                                dr["DEPT_NAME"] = dt_EMP_List.Rows[jLoop]["DEPT_NAME"].ToString();
                                dr["DESIG_NAME"] = dt_EMP_List.Rows[jLoop]["DESIG_NAME"].ToString();
                                dr["STATUS"] = "N";
                                dt_EmpProcessList.Rows.Add(dr);

                            }
                            str_ORG_STR = str_ORG_STR.Replace(str_Replace_str, str_new_replace_str + str_tmp + "</UL></li>");
                        }
                        else
                        {
                            str_ORG_STR = str_ORG_STR.Replace(str_Replace_str, str_new_replace_str.Replace("<UL>", "</li>"));
                        }

                        dr_col[0]["STATUS"] = "Y";
                        dt_EmpProcessList.AcceptChanges();
                    }
                    dr_col = dt_EmpProcessList.Select("STATUS='N'");
                } while (dr_col.Length > 0);

            }
            str_ORG_STR += " </ul>";
            str_ORG_STR += "  </li> ";
            str_ORG_STR += "  </ul> ";

            left.InnerHtml = str_ORG_STR;
            btnShow_Click(btnShow, new EventArgs());

        }
        private void FillOrgData()
        {
            string sqlQuery = "";
            DataTable dt_OrgData = DBMethod.ExecuteQuery(FIN.DAL.HR.DashBoard_HR_DAL.getOrganizationStruDet()).Tables[0];


            DataTable dt_Category = dt_OrgData.DefaultView.ToTable(true, "CATEGORY_ID", "CATEGORY_DESC");

            sqlQuery += "  <ul id='organisation'> ";
            sqlQuery += "  <li>  " + VMVServices.Web.Utils.OrganizationName;
            sqlQuery += " <ul>";
            for (int iLoop = 0; iLoop < dt_Category.Rows.Count; iLoop++)
            {
                sqlQuery += "  <li> ";
                sqlQuery += dt_Category.Rows[iLoop]["CATEGORY_DESC"].ToString();

                List<DataTable> tbl_Job = dt_OrgData.AsEnumerable()
               .Where(r => r["CATEGORY_ID"].ToString().Contains(dt_Category.Rows[iLoop]["CATEGORY_ID"].ToString()))
                      .GroupBy(row => new
                      {
                          Cate_Id = row.Field<string>("CATEGORY_ID")
                      }).Select(g => System.Data.DataTableExtensions.CopyToDataTable(g)).ToList();

                DataTable dt_Jobs = tbl_Job[0].DefaultView.ToTable(true, "JOB_ID", "JOB_DESC");


                sqlQuery += " <ul>";
                for (int jLoop = 0; jLoop < dt_Jobs.Rows.Count; jLoop++)
                {
                    sqlQuery += "  <li> ";
                    sqlQuery += dt_Jobs.Rows[jLoop]["JOB_DESC"].ToString();



                    List<DataTable> tbl_Pos = dt_OrgData.AsEnumerable()
                                     .Where(r => r["CATEGORY_ID"].ToString().Contains(dt_Category.Rows[iLoop]["CATEGORY_ID"].ToString()) && r["JOB_ID"].ToString().Contains(dt_Jobs.Rows[jLoop]["JOB_ID"].ToString()))
                                      .GroupBy(row => new
                                      {
                                          Cate_Id = row.Field<string>("CATEGORY_ID"),
                                          jo_Id = row.Field<string>("JOB_ID")
                                      }).Select(g => System.Data.DataTableExtensions.CopyToDataTable(g)).ToList();

                    DataTable dt_Pos = tbl_Pos[0].DefaultView.ToTable(true, "POSITION_ID", "POSITION_DESC");


                    sqlQuery += " <ul>";
                    for (int pLoop = 0; pLoop < dt_Pos.Rows.Count; pLoop++)
                    {
                        sqlQuery += "  <li> ";
                        sqlQuery += dt_Pos.Rows[pLoop]["POSITION_DESC"].ToString();


                        List<DataTable> tbl_Grade = dt_OrgData.AsEnumerable()
                                     .Where(r => r["CATEGORY_ID"].ToString().Contains(dt_Category.Rows[iLoop]["CATEGORY_ID"].ToString()) && r["JOB_ID"].ToString().Contains(dt_Jobs.Rows[jLoop]["JOB_ID"].ToString()) && r["POSITION_ID"].ToString().Contains(dt_Pos.Rows[pLoop]["POSITION_ID"].ToString()))
                                      .GroupBy(row => new
                                      {
                                          Cate_Id = row.Field<string>("CATEGORY_ID"),
                                          jo_Id = row.Field<string>("JOB_ID"),
                                          pos_Id = row.Field<string>("POSITION_ID"),
                                      }).Select(g => System.Data.DataTableExtensions.CopyToDataTable(g)).ToList();

                        DataTable dt_Grade = tbl_Grade[0].DefaultView.ToTable(true, "GRADE_ID", "GRADE_DESC");


                        sqlQuery += " <ul>";
                        for (int gLoop = 0; gLoop < dt_Grade.Rows.Count; gLoop++)
                        {
                            sqlQuery += "  <li> ";
                            sqlQuery += dt_Grade.Rows[gLoop]["GRADE_DESC"].ToString();
                            sqlQuery += "</li>";

                        }
                        sqlQuery += " </ul>";


                        sqlQuery += "</li>";

                    }
                    sqlQuery += " </ul>";


                    sqlQuery += "</li>";

                }
                sqlQuery += " </ul>";



                sqlQuery += "</li>";
            }
            sqlQuery += " </ul>";
            sqlQuery += "  </li> ";
            sqlQuery += "  </ul> ";




            //sqlQuery += "  <ul id='organisation'> ";
            //sqlQuery += " <li> ";
            //sqlQuery += " <adjunct>Alfred</adjunct> ";
            //sqlQuery += " <em>Batman</em> ";
            //sqlQuery += " <ul> ";
            //sqlQuery += "  <li>Batman Begins<br /> ";
            ////sqlQuery+=" <img class="star" src="star-one.png"> " ;
            ////sqlQuery+="  <img class="star" src="star-one.png"> " ;
            //// sqlQuery+=" <img class="star" src="star-one.png"> " ;
            ////sqlQuery+="  <img class="star" src="star-one.png"> " ;
            //sqlQuery += "  <ul> ";
            //sqlQuery += "   <li>Ra's Al Ghul</li> ";
            //sqlQuery += "  <li>Carmine Falconi</li> ";
            //sqlQuery += "  </ul> ";
            //sqlQuery += "  </li> ";
            //sqlQuery += "  <li>The Dark Knight<br /> ";
            ////sqlQuery+="  <img class="star" src="star-one.png"> " ;
            //// sqlQuery+=" <img class="star" src="star-one.png"> " ;
            //// sqlQuery+="  <img class="star" src="star-one.png"> " ;
            ////sqlQuery+="  <img class="star" src="star-one.png"> " ;
            //// sqlQuery+="   <img class="star" src="star-one.png"> " ;
            //sqlQuery += "  <ul> ";
            //sqlQuery += " <li>Joker</li> ";
            //sqlQuery += "  <li>Harvey Dent</li> ";
            //sqlQuery += " </ul> ";
            //sqlQuery += " </li> ";
            //sqlQuery += " <li>The Dark Knight Rises<br /> ";
            ////sqlQuery+="  <img class="star" src="star-one.png"> " ;
            ////sqlQuery+="  <img class="star" src="star-one.png"> " ;
            //// sqlQuery+=" <img class="star" src="star-one.png"> " ;
            ////sqlQuery+=" <img class="star" src="star-one.png"> " ;
            ////sqlQuery+="  <img class="star" src="star-half.png"> " ;
            //sqlQuery += "  <ul> ";
            //sqlQuery += "  <li>Bane</li> ";
            //sqlQuery += "  <li>Talia Al Ghul</li> ";
            //sqlQuery += " </ul> ";
            //sqlQuery += "  </li> ";
            //sqlQuery += " </ul> ";
            //sqlQuery += "  </li> ";
            //sqlQuery += "  </ul> ";
            left.InnerHtml = sqlQuery;
            btnShow_Click(btnShow, new EventArgs());
        }
        protected void btnShow_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(btnShow, typeof(Button), "LoadGraph", "fn_showGraph()", true);
        }

        protected void btnProcess_Click(object sender, EventArgs e)
        {
            FillOrgEmpStructure();
        }

        protected void ddlDeptGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlDeptGroup.SelectedValue.ToString().Length > 0)
            {
                FIN.BLL.HR.Department_BLL.GetDepartmentName4Group(ref ddlDeptname, ddlDeptGroup.SelectedValue.ToString(), false);
            }
            else
            {
                FIN.BLL.HR.Department_BLL.GetDepartmentName(ref ddlDeptname, false);
            }
        }


    }
}