﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using VMVServices.Web;
using System.Collections;
namespace FIN.Client.HR_DASHBOARD
{
    public partial class EmployeeDetails_DB : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session["EMP_DEPT_GRAP"] = null;
                Session["EMP_DESIG_GRAP"] = null;
                AssignToControl();
                DisplayEmployeeDetails();
            }

        }
        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                //FillComboBox();
                divGraphData.Visible = false;
                divPrintIcon.Visible = false;
                if (Request.QueryString.ToString().ToUpper().Contains("TVYN"))
                {
                    Startup();
                    divGraphData.Visible = true;
                    divPrintIcon.Visible = true;
                    divHeader.Style.Add(" background-color", "white");
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DetailedRevenueATC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }


        private void Startup()
        {
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[FIN.BLL.QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[FIN.BLL.QueryStringTags.ReportName.ToString()]));
        }
        private void DisplayEmployeeDetails()
        {
            DataTable dt_EmpDet = DBMethod.ExecuteQuery(FIN.DAL.HR.Employee_DAL.GetEmployeeDetails4Dashbord()).Tables[0];
            Session["EMPDET"] = dt_EmpDet;
            if (dt_EmpDet.Rows.Count > 0)
            {
                DataTable dt_Dist_DEPT = dt_EmpDet.DefaultView.ToTable(true, "DEPT_ID", "DEPT_NAME");

                DataTable dt_Dept_List = new DataTable();
                dt_Dept_List.Columns.Add("ID");
                dt_Dept_List.Columns.Add("NAME");
                dt_Dept_List.Columns.Add("COUNT");

                for (int iLoop = 0; iLoop < dt_Dist_DEPT.Rows.Count; iLoop++)
                {
                    DataRow dr = dt_Dept_List.NewRow();
                    dr["ID"] = dt_Dist_DEPT.Rows[iLoop]["DEPT_ID"].ToString();
                    dr["NAME"] = dt_Dist_DEPT.Rows[iLoop]["DEPT_NAME"].ToString();
                    DataRow[] dr_count = dt_EmpDet.Select("DEPT_ID='" + dt_Dist_DEPT.Rows[iLoop]["DEPT_ID"].ToString() + "'");
                    dr["COUNT"] = dr_count.Length;
                    dt_Dept_List.Rows.Add(dr);
                }
                gvDeptDet.DataSource = dt_Dept_List;
                gvDeptDet.DataBind();
                Session["EMP_DEPT_GRAP"] = dt_Dept_List;



                chrt_Dept.DataSource = (DataTable)Session["EMP_DEPT_GRAP"];
                chrt_Dept.Series["S_Dept_Count"].XValueMember = "NAME";
                chrt_Dept.Series["S_Dept_Count"].YValueMembers = "COUNT";
                chrt_Dept.DataBind();

                Session["RPTDATA"] = dt_Dept_List;
                gvGraphdata.DataSource = (DataTable)Session["EMP_DEPT_GRAP"];
                gvGraphdata.DataBind();
            }

        }

        protected void lnk_Dept_Name_Click(object sender, EventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            LoadDesigDet(gvr);
        }

        private void LoadDesigDet(GridViewRow gvr)
        {
            DataTable dt_EmpDet = (DataTable)Session["EMPDET"];
            List<DataTable> tables = dt_EmpDet.AsEnumerable()
                .Where(r => r["DEPT_ID"].ToString().Contains(gvDeptDet.DataKeys[gvr.RowIndex].Values["ID"].ToString()))
                       .GroupBy(row => new
                       {
                           Email = row.Field<string>("DEPT_ID"),
                           Name = row.Field<string>("DEPT_NAME")
                       }).Select(g => System.Data.DataTableExtensions.CopyToDataTable(g)).ToList();

            DataTable dt_Dist_Desig = tables[0].DefaultView.ToTable(true, "DEPT_DESIG_ID", "DESIG_NAME");

            DataTable dt_Desig_List = new DataTable();
            dt_Desig_List.Columns.Add("ID");

            dt_Desig_List.Columns.Add("NAME");
            dt_Desig_List.Columns.Add("COUNT");

            for (int iLoop = 0; iLoop < dt_Dist_Desig.Rows.Count; iLoop++)
            {
                DataRow dr = dt_Desig_List.NewRow();

                dr["ID"] = dt_Dist_Desig.Rows[iLoop]["DEPT_DESIG_ID"].ToString();
                dr["NAME"] = dt_Dist_Desig.Rows[iLoop]["DESIG_NAME"].ToString();
                DataRow[] dr_count = dt_EmpDet.Select("DEPT_DESIG_ID='" + dt_Dist_Desig.Rows[iLoop]["DEPT_DESIG_ID"].ToString() + "'");
                dr["COUNT"] = dr_count.Length;
                dt_Desig_List.Rows.Add(dr);
            }

            gvDesigDet.DataSource = dt_Desig_List;
            gvDesigDet.DataBind();

            Session["EMP_DESIG_GRAP"] = dt_Desig_List;
            Session["RPTDATA"] = dt_Desig_List;

            chrt_Dept.DataSource = (DataTable)Session["EMP_DESIG_GRAP"];
            chrt_Dept.Series["S_Dept_Count"].XValueMember = "NAME";
            chrt_Dept.Series["S_Dept_Count"].YValueMembers = "COUNT";
            chrt_Dept.DataBind();

            gvGraphdata.DataSource = (DataTable)Session["EMP_DESIG_GRAP"];
            gvGraphdata.DataBind();

        }


        protected void imgBack_Click(object sender, ImageClickEventArgs e)
        {
            divEmpDept.Visible = true;
            divEmpDesig.Visible = false;
            Session["EMP_DESIG_GRAP"] = null;

        }

        protected void imgEmp_Click_Click(object sender, ImageClickEventArgs e)
        {
            divEmpDept.Visible = false;
            divEmpDesig.Visible = true;
            divEmpDet.Visible = false;
        }


        protected void lnk_Desig_Name_Click(object sender, EventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            LoadEMPDet(gvr);
        }

        private void LoadEMPDet(GridViewRow gvr)
        {
            DataTable dt_EmpDet = (DataTable)Session["EMPDET"];
            var tmp_menulist = dt_EmpDet.AsEnumerable()
                       .Where(r => r["DEPT_ID"].ToString().Contains(gvDesigDet.DataKeys[gvr.RowIndex].Values["DEPT_ID"].ToString()) && r["DEPT_DESIG_ID"].ToString() == gvDesigDet.DataKeys[gvr.RowIndex].Values["DEPT_DESIG_ID"].ToString())
                       ;
            DataTable dt_Employee_Det = new DataTable();
            if (tmp_menulist.Any())
            {
                dt_Employee_Det = System.Data.DataTableExtensions.CopyToDataTable(tmp_menulist);
            }

            gvEmpDet.DataSource = dt_Employee_Det;
            gvEmpDet.DataBind();
            divEmpDesig.Visible = false;
            divEmpDept.Visible = false;
            divEmpDet.Visible = true;




        }
        protected void btnGraphRep_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;

                Hashtable htParameters = new Hashtable();
                Hashtable htHeadingParameters = new Hashtable();
                Hashtable htFilterParameter = new Hashtable();

                chrt_Dept.SaveImage(System.Web.Configuration.WebConfigurationManager.AppSettings["ChartFileFolder"].ToString() + Master.ProgramID + ".png");
                htFilterParameter.Add("ChartPath", System.Web.Configuration.WebConfigurationManager.AppSettings["ChartFileFolder"].ToString() + Master.ProgramID + ".png");


                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;




                DataTable dt_data = (DataTable)Session["RPTDATA"];
                ReportData = new DataSet();
                DataTable dt = dt_data.Copy();
                dt.TableName = "VW_BI_EMPLOYEE_COUNT";
                ReportData.Tables.Add(dt.Copy());



                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FIN.BLL.FINMessageConstatns.CrystalReportViewerReportPath + "?Id=" + Master.ProgramID + "','','" + VMVServices.Services.Data.DbConsts.ReportProperties + "');", true);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ddd", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
    }
}