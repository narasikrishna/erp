﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
namespace FIN.Client.HR_DASHBOARD
{
    public partial class EmpDistinYear_BI : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                getEmployeeDistrubutionList();
            }

        }
        private void getEmployeeDistrubutionList()
        {
            DataTable dt_Emp_Dist_list = DBMethod.ExecuteQuery(FIN.DAL.HR.Employee_DAL.getEmployDistributionList()).Tables[0];
            Session["EMP_DIST_LIST"] = dt_Emp_Dist_list;
            chrt_emp_expire.DataSource = (DataTable)Session["EMP_DIST_LIST"];
            chrt_emp_expire.DataBind();
        }
    }
}