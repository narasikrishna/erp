﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="EmployeeDetails_DB.aspx.cs" Inherits="FIN.Client.HR_DASHBOARD.EmployeeDetails_DB" %>
    <%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function fn_ShowDept() {
            $("#divempGrid").fadeToggle(1000);
        }
    </script>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div id="divempGraph" style="float: left; width: 100%;">
                <div class="DBGridHeader" style="float: left; width: 100%; height: 25px" id="divHeader"
            runat="server">
            <div style="float: left; padding-left: 10px;padding-top: 2px">
                Employee Details
            </div>
            <div id="divPrintIcon" runat="server" style="float: right; padding-right: 10px; padding-top: 2px">
                <asp:ImageButton ID="btnGraphRep" runat="server" ImageUrl="../Images/show-report-icon.png"
                    Width="35px" Height="25px" OnClick="btnGraphRep_Click" />
            </div>
            <div style="float: right; padding-right: 10px; padding-top: 2px">
                <img alt="==>" src="../Images/DashBoardImage/ViewList.png" width="20px" height="20px"
                    onclick="fn_ShowDept()" />
            </div>
        </div>
        <div class="divClear_10" style="width: 70%">
        </div>
        <div style="float: left; width: 100%;">
            <asp:Chart ID="chrt_Dept" runat="server" Height="300px" Width="500px" EnableViewState="true">
                <Series>
                    <asp:Series Name="S_Dept_Count" ChartType="Pie" XValueMember="DEPT_NAME" YValueMembers="DEPT_COUNT"
                        IsValueShownAsLabel="true" LabelAngle="60" CustomProperties="PieLabelStyle=Outside"
                        Legend="Legend1" LegendText="#VALX ( #VAL )">
                    </asp:Series>
                </Series>
                <ChartAreas>
                    <asp:ChartArea Name="ChartArea1" Area3DStyle-Enable3D="true" Area3DStyle-Inclination="20">
                        <Area3DStyle Enable3D="True" Inclination="20"></Area3DStyle>
                    </asp:ChartArea>
                </ChartAreas>
                <Legends>
                    <asp:Legend Docking="Right" Name="Legend1" Alignment="Near" IsDockedInsideChartArea="False">
                    </asp:Legend>
                </Legends>
            </asp:Chart>
        </div>
        <div style="float: left; width: 48%; display: none">
            <asp:Chart ID="chrt_Desig" runat="server" Height="300px" Width="500px">
                <Series>
                    <asp:Series Name="S_Desig_Count" ChartType="Pie" XValueMember="DESIG_COUNT" YValueMembers="DESIG_COUNT"
                        IsValueShownAsLabel="true" LabelAngle="60" CustomProperties="PieLabelStyle=Outside">
                    </asp:Series>
                </Series>
                <ChartAreas>
                    <asp:ChartArea Name="ChartArea1" Area3DStyle-Enable3D="true" Area3DStyle-Inclination="20">
                        <Area3DStyle Enable3D="True" Inclination="20"></Area3DStyle>
                    </asp:ChartArea>
                </ChartAreas>
            </asp:Chart>
        </div>
        <div class="divClear_10" style="width: 70%">
        </div>
        <div id="divempGrid" style="width: 95%; background-color: ThreeDFace; position: absolute;
            z-index: 5000; top: 30px; left: 10px; display: none ; height: 300px ; overflow: auto">
            <div id="divEmpDept" runat="server" visible="true">
                <asp:GridView ID="gvDeptDet" runat="server" CssClass="Grid" Width="100%" DataKeyNames="ID"
                    AutoGenerateColumns="False">
                    <Columns>
                        <asp:TemplateField HeaderText="Department">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnk_Dept_Name" runat="server" Text='<%# Eval("NAME") %>'
                                    OnClick="lnk_Dept_Name_Click"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="COUNT" HeaderText="Count">
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:BoundField>
                    </Columns>
                    <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                    <HeaderStyle CssClass="GridHeader" />
                    <AlternatingRowStyle CssClass="GrdAltRow" />
                </asp:GridView>
            </div>
            <div id="divEmpDesig" runat="server" visible="false" style="display: none">
                <table width="100%">
                    <tr>
                        <td align="right">
                            <asp:ImageButton ID="imgDesgBack" runat="server" ImageUrl="~/Images/DashBoardImage/back.png"
                                Width="20px" Height="20px" OnClick="imgBack_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:GridView ID="gvDesigDet" runat="server" CssClass="Grid" Width="100%" DataKeyNames="ID"
                                AutoGenerateColumns="False">
                                <Columns>
                                    <asp:TemplateField HeaderText="Designation">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnk_Desig_Name" runat="server" Text='<%# Eval("NAME") %>'
                                                OnClick="lnk_Desig_Name_Click"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="COUNT" HeaderText="Count">
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                </Columns>
                                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                <HeaderStyle CssClass="GridHeader" />
                                <AlternatingRowStyle CssClass="GrdAltRow" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </div>
            <div id="divEmpDet" runat="server" visible="false" style="display: none">
                <table width="100%">
                    <tr>
                        <td align="right">
                            <asp:ImageButton ID="imgEmp" runat="server" ImageUrl="~/Images/DashBoardImage/back.png"
                                Width="20px" Height="20px" OnClick="imgEmp_Click_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:GridView ID="gvEmpDet" runat="server" CssClass="Grid" Width="100%" DataKeyNames="EMP_ID"
                                AutoGenerateColumns="False">
                                <Columns>
                                    <asp:BoundField DataField="EMP_NO" HeaderText="Number"></asp:BoundField>
                                    <asp:BoundField DataField="EMP_NAME" HeaderText="Name"></asp:BoundField>
                                </Columns>
                                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                <HeaderStyle CssClass="GridHeader" />
                                <AlternatingRowStyle CssClass="GrdAltRow" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </div>

        </div>
          <div class="divClear_10" style="width: 70%">
        </div>
        <div id="divGraphData" runat="server">
            <asp:GridView ID="gvGraphdata" runat="server" CssClass="Grid" Width="100%" AutoGenerateColumns="False"
                ShowHeaderWhenEmpty="true">
                <Columns>
                    <asp:BoundField DataField="NAME" HeaderText="Department Name"></asp:BoundField>
                    <asp:BoundField DataField="COUNT" HeaderText="Count">
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:BoundField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
</asp:Content>
