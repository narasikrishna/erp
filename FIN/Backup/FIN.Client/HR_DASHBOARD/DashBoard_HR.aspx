﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DashBoard_HR.aspx.cs" Inherits="FIN.Client.HR_DASHBOARD.DashBoard_HR" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .frmStyle
        {
            width: 100%;
            height: 100%;
            background-color: transparent;
            border: 0px solid red;
            top: 0;
            left: 0;
        }
        .frmdiv
        {
            border: 1px solid aqua;
            float:left;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div id="divDashboard">
        <div id="divEmpDetails" class="frmdiv" style="width: 500px; height: 350px;"  runat="server">
            <iframe id="frm_EmpDetails" class="frmStyle" src="EmployeeDetails_DB.aspx" scrolling="no">
            </iframe>
        </div>
        <div id="divEmpReqTer" class="frmdiv" style="width: 500px; height: 350px;"   runat="server">
            <iframe id="frm_EmpReqTer" class="frmStyle" src="EmployeeReqTer_DB.aspx" scrolling="no">
            </iframe>
        </div>
        <div id="divPerRenDet" class="frmdiv" style="width: 500px; height: 350px;"   runat="server">
            <iframe id="frm_PerRenDet" class="frmStyle" src="PermitRenewalDetails_DB.aspx" scrolling="no">
            </iframe>
        </div>
        <div id="divAgeBar" class="frmdiv" style="width: 500px; height: 350px;"   runat="server">
            <iframe id="frmAgeBar" class="frmStyle" src="EmployeeAgeBar_DB.aspx" scrolling="no">
            </iframe>
        </div>
        <div id="divLD" class="frmdiv" style="width: 500px; height: 350px;"   runat="server">
            <iframe id="frm_LD" class="frmStyle" src="LeaveDetails_DB.aspx" scrolling="no">
            </iframe>
        </div>
         <div id="divHL" class="frmdiv" style="width: 500px; height: 350px;"   runat="server"> 
            <iframe id="frm_HL" class="frmStyle" src="HolidayList_DB.aspx" scrolling="no">
            </iframe>
        </div>
        <div id="div_EmpDistList" class="frmdiv" style="width: 500px; height: 350px;"   runat="server">
            <iframe id="frm_EDL" class="frmStyle" src="EmpDistinYear_DB.aspx" scrolling="no">
            </iframe>
        </div>
        <div id="div_OrgChart" class="frmdiv" style="width: 100%; height: 600px; display:none "   runat="server">
            <iframe id="frm_orgChart" class="frmStyle" src="OrganizationTree_DB.aspx" scrolling="no" >
            </iframe>
        </div>
    </div>
    </form>
</body>
</html>
