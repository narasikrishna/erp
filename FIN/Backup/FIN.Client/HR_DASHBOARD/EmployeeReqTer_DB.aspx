﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="EmployeeReqTer_DB.aspx.cs" Inherits="FIN.Client.HR_DASHBOARD.EmployeeReqTer_DB" %>
<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="DBGridHeader" style="float: left; width: 100%; height: 25px" id="divHeader"
            runat="server">
        <div style="float: left;padding-left: 10px;padding-top: 2px">
            Employee Recuritment / Resigned
        </div>
        <div id="divPrintIcon" runat="server" style="float: right; padding-right: 10px; padding-top: 2px">
                <asp:ImageButton ID="btnGraphRep" runat="server" ImageUrl="../Images/show-report-icon.png"
                    Width="35px" Height="25px" OnClick="btnGraphRep_Click" />
            </div>
        <div style="width: 150px; float: right; padding-right: 10px;padding-top: 2px">
            <asp:DropDownList ID="ddlFinYear" runat="server" CssClass="ddlStype" AutoPostBack="True"
                OnSelectedIndexChanged="ddlFinYear_SelectedIndexChanged">
            </asp:DropDownList>
        </div>
    </div>
    <div class="divClear_10" style="width: 70%">
    </div>
    <div id="divcompEmpGrap" style="width: 100%">
        <asp:Chart ID="chrt_empCount" runat="server" Width="500px" Height="300px" EnableViewState="True">
            <Series>
                <asp:Series ChartArea="ChartArea1" Name="s_Total_Emp" IsValueShownAsLabel="True"
                    XValueMember="row_number" YValueMembers="TOTAL_EMP" Legend="Legend1" LegendText="Total Employee ( #TOTAL{N0} )">
                </asp:Series>
                <asp:Series ChartArea="ChartArea1" IsValueShownAsLabel="True" Name="s_NEW_JOIN_EMP"
                    XValueMember="row_number" YValueMembers="NEW_JOIN_EMP" Legend="Legend1" LegendText="New Joined  ( #TOTAL{N0} )">
                </asp:Series>
                <asp:Series ChartArea="ChartArea1" IsValueShownAsLabel="True" Name="s_CONFIRMED_EMP"
                    XValueMember="row_number" YValueMembers="CONFIRMED_EMP" Legend="Legend1" LegendText="Conifrmed Employee  ( #TOTAL{N0} )">
                </asp:Series>
                <asp:Series ChartArea="ChartArea1" IsValueShownAsLabel="True" Name="s_TERMINATION_EMP"
                    XValueMember="row_number" YValueMembers="TERMINATION_EMP" Legend="Legend1" LegendText="Termination Employee ( #TOTAL{N0})">
                </asp:Series>
            </Series>
            <ChartAreas>
                <asp:ChartArea Name="ChartArea1">
                    <Area3DStyle Enable3D="True" Inclination="25" Rotation="70"></Area3DStyle>
                </asp:ChartArea>
                <%-- <asp:ChartArea Name="ChartArea2" >
                                </asp:ChartArea>--%>
            </ChartAreas>
            <Legends>
                <asp:Legend Alignment="Center" Name="Legend1" Docking="Bottom" 
                    LegendStyle="Table" >
                </asp:Legend>
            </Legends>
        </asp:Chart>
    </div>
    <div class="divClear_10" style="width: 70%">
    </div>
    <div id="divGraphData" runat="server">
        <asp:GridView ID="gvGraphdata" runat="server" CssClass="Grid" Width="100%" AutoGenerateColumns="False"
            ShowHeaderWhenEmpty="true">
            
            <Columns>
                <asp:BoundField DataField="row_number" HeaderText="Row Number"></asp:BoundField>
                <asp:BoundField DataField="TOTAL_EMP" HeaderText="Total Employee">
                    <ItemStyle HorizontalAlign="Right" />
                </asp:BoundField>
                <asp:BoundField DataField="NEW_JOIN_EMP" HeaderText="New Joined Employee"></asp:BoundField>
                <asp:BoundField DataField="CONFIRMED_EMP" HeaderText="Confirmed Employee"></asp:BoundField>
                <asp:BoundField DataField="TERMINATION_EMP" HeaderText="Termination Employee"></asp:BoundField>
            </Columns>
            <EmptyDataRowStyle CssClass="EmptyRowStyle" />
            <HeaderStyle CssClass="GridHeader" />
            <AlternatingRowStyle CssClass="GrdAltRow" />
        </asp:GridView>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
</asp:Content>
