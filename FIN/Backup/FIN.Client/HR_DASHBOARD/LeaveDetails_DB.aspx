﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="LeaveDetails_DB.aspx.cs" Inherits="FIN.Client.HR_DASHBOARD.LeaveDetails_DB" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="DBGridHeader" style="float: left; width: 100%; height: 25px" id="divHeader"
        runat="server">
        <div style="float: left; padding-left: 10px; padding-top: 2px">
            Leave Details
        </div>
        <div id="divPrintIcon" runat="server" style="float: right; padding-right: 10px; padding-top: 2px">
            <asp:ImageButton ID="btnGraphRep" runat="server" ImageUrl="../Images/show-report-icon.png"
                Width="35px" Height="25px" OnClick="btnGraphRep_Click" />
        </div>
        <div style="width: 120px; float: right; padding-right: 10px">
            <asp:DropDownList ID="ddlLAPeriod" runat="server" CssClass="ddlStype" AutoPostBack="True"
                OnSelectedIndexChanged="ddlLAPeriod_SelectedIndexChanged">
            </asp:DropDownList>
        </div>
        <div style="width: 120px; float: right; padding-right: 10px; padding-top: 2px">
            <asp:DropDownList ID="ddlLAFinYear" runat="server" CssClass="ddlStype" AutoPostBack="True"
                OnSelectedIndexChanged="ddlLAFinYear_SelectedIndexChanged">
            </asp:DropDownList>
        </div>
    </div>
    <div class="divClear_10" style="width: 60%">
    </div>
    <div>
        <asp:Chart ID="chrt_LD" runat="server" Width="500px" Height="300px" EnableViewState="True">
            <Series>
                <asp:Series Name="Series1" XValueMember="DEPT_NAME" YValueMembers="No_Of_Emp" IsValueShownAsLabel="true"
                    ChartType="Bar" YValuesPerPoint="5">
                </asp:Series>
            </Series>
            <ChartAreas>
                <asp:ChartArea Name="ChartArea1">
                    <Area3DStyle Enable3D="false"></Area3DStyle>
                    <AxisX>
                        <MajorGrid Enabled="false" />
                    </AxisX>
                    <AxisY>
                        <MajorGrid Enabled="false" />
                    </AxisY>
                </asp:ChartArea>
            </ChartAreas>
        </asp:Chart>
    </div>
    <div class="divClear_10" style="width: 70%">
    </div>
    <div id="divGraphData" runat="server">
        <asp:GridView ID="gvGraphdata" runat="server" CssClass="Grid" Width="100%" AutoGenerateColumns="False"
            ShowHeaderWhenEmpty="true">
            <Columns>
                <asp:BoundField DataField="DEPT_NAME" HeaderText="Department Name"></asp:BoundField>
                <asp:BoundField DataField="No_Of_Emp" HeaderText="Number of Employees">
                    <ItemStyle HorizontalAlign="Right" />
                </asp:BoundField>
            </Columns>
            <EmptyDataRowStyle CssClass="EmptyRowStyle" />
            <HeaderStyle CssClass="GridHeader" />
            <AlternatingRowStyle CssClass="GrdAltRow" />
        </asp:GridView>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
</asp:Content>
