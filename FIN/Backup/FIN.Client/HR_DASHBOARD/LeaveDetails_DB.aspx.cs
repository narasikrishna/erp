﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using VMVServices.Web;
using System.Collections;
namespace FIN.Client.HR_DASHBOARD
{
    public partial class LeaveDetails_DB : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session["EMP_LD"] = null;
                AssignToControl();
                FillComboBox();

                getEmployeeleaveDetails();

            }
        }
        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                divGraphData.Visible = false;
                divPrintIcon.Visible = false;
                if (Request.QueryString.ToString().ToUpper().Contains("TVYN"))
                {
                    Startup();
                    divGraphData.Visible = true;
                    divPrintIcon.Visible = true;
                    divHeader.Style.Add(" background-color", "white");
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DetailedRevenueATC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }


        private void Startup()
        {
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[FIN.BLL.QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[FIN.BLL.QueryStringTags.ReportName.ToString()]));
        }
        private void FillComboBox()
        {

            string str_finyear = FINSP.GetSPFOR_FiscalYear(DateTime.Now.Date.ToString("dd/MM/yyyy"));

            DataTable dt_cur_period = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountingCalendar_DAL.GetPeriodId4CurrentDate()).Tables[0];

            FIN.BLL.GL.AccountingCalendar_BLL.GetCalAcctYear(ref ddlLAFinYear);
            if (ddlLAFinYear.Items.Count > 0)
            {
                //ddlLAFinYear.SelectedIndex = ddlLAFinYear.Items.Count - 1;
                ddlLAFinYear.SelectedValue = str_finyear;
            }
            FIN.BLL.GL.AccountingCalendar_BLL.GetFinancialPeriod4Year(ref ddlLAPeriod, ddlLAFinYear.SelectedValue);
            if (dt_cur_period.Rows.Count > 0)
            {
                ddlLAPeriod.SelectedValue = dt_cur_period.Rows[0]["PERIOD_ID"].ToString();
            }

        }
        private void getEmployeeleaveDetails()
        {
            DataTable dt_Emp_LD = DBMethod.ExecuteQuery(FIN.DAL.HR.Employee_DAL.getEmployeeLeaveDetails(ddlLAPeriod.SelectedValue)).Tables[0];
            Session["EMP_LD"] = dt_Emp_LD;

            chrt_LD.DataSource = (DataTable)Session["EMP_LD"];
            chrt_LD.DataBind();

            gvGraphdata.DataSource = (DataTable)Session["EMP_LD"];
            gvGraphdata.DataBind();
        }

        protected void ddlLAFinYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            FIN.BLL.GL.AccountingCalendar_BLL.GetFinancialPeriod4Year(ref ddlLAPeriod, ddlLAFinYear.SelectedValue);

        }

        protected void ddlLAPeriod_SelectedIndexChanged(object sender, EventArgs e)
        {
            getEmployeeleaveDetails();
        }
        protected void btnGraphRep_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;

                Hashtable htParameters = new Hashtable();
                Hashtable htHeadingParameters = new Hashtable();
                Hashtable htFilterParameter = new Hashtable();

                chrt_LD.SaveImage(System.Web.Configuration.WebConfigurationManager.AppSettings["ChartFileFolder"].ToString() + Master.ProgramID + ".png");
                htFilterParameter.Add("ChartPath", System.Web.Configuration.WebConfigurationManager.AppSettings["ChartFileFolder"].ToString() + Master.ProgramID + ".png");


                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;

                ReportData = DBMethod.ExecuteQuery(FIN.DAL.HR.Employee_DAL.getEmployeeLeaveDetails(ddlLAPeriod.SelectedValue));


                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FIN.BLL.FINMessageConstatns.CrystalReportViewerReportPath + "?Id=" + Master.ProgramID + "','','" + VMVServices.Services.Data.DbConsts.ReportProperties + "');", true);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ddd", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
    }
}