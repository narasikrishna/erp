﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using VMVServices.Web;

namespace FIN.Client
{
    public partial class Message : PageBase
    {

        Exception customException = new Exception();
        SortedList ErrorCollection = new SortedList();

        protected void Page_Load(object sender, EventArgs e)
        {

            //ASPxRoundPanel1.HeaderText = "Message";
            lblHeading.Text = "Message";
            IDictionaryEnumerator myEnumerator = ErrorCollection.GetEnumerator();

            //while (myEnumerator.MoveNext())
            //{
            //    lblErrorMessage.Items.Add(this.AppMessageBody);//myEnumerator.Value.ToString());

            //}
            if (this.AppMessageBody != null && this.AppMessageBody != string.Empty)
            {
                lblbody.Text = this.AppMessageBody;
            }
            else
            {
                lblbody.Text = this.customException.Message;
            }
            //if (Request.QueryString["msg"] != null)
            //{
            //    lblbody.Text = Request.QueryString["msg"].ToString();
            //}
        }


        protected void Page_Error(object sender, EventArgs e)
        {

        }



        protected void btnOk_Click(object sender, EventArgs e)
        {
            goTo(AppMessageContinueUrl, false);
        }
    }
}