﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    Inherits="FIN.Client.Message" Title="" CodeBehind="Message.aspx.cs" %>

<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="Server">
    <asp:Panel ID="pnlConfirm" runat="server">
        <div class="ConfirmForm" style ="width :500px">
            <table width="100%">
                <tr class="ConfirmHeading" style="width: 100%">
                    <td>
                        <asp:Label ID="lblHeading" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Image ID="imgMessageImage" runat="server" ImageUrl="~/Images/icon_information.gif" />
                        &nbsp;<asp:Label ID="lblbody" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Button ID="btnOk" runat="server" CssClass="button" OnClick="btnOk_Click" 
                            Text="OK" />
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
</asp:Content>
