﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.GL;
using FIN.DAL.AP;
using FIN.DAL.AR;
using FIN.BLL;
using FIN.BLL.AP;
using FIN.BLL.AR;
using FIN.BLL.HR;
using FIN.BLL.GL;
using FIN.BLL.PER;
using FIN.BLL.SSM;
using FIN.BLL.LOAN;
using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;

namespace FIN.Client.LOAN_Reports
{
    public partial class RPTFinancialDetailsParam : PageBase
    {
        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();
        DataTable dtEmployeeDtls = new DataTable();
        DataTable dtEarningDeduc = new DataTable();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("EmpDetailsReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                FillComboBox();
                FillStartDate();
                Startup();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AbsentReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void Startup()
        {

            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));

            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();

        }
        private void FillStartDate()
        {

            //string str_finyear = FINSP.GetSPFOR_FiscalYear(DateTime.Now.Date.ToString("dd/MM/yyyy"));
            //DataTable dtDate = new DataTable();
            //if (str_finyear != string.Empty)
            //{
            //    dtDate = DBMethod.ExecuteQuery(FIN.DAL.GL.InventoryReports_DAL.getCalDate(str_finyear)).Tables[0];
            //    txtFromDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["cal_eff_start_dt"].ToString());
            //    txtToDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["call_eff_end_dt"].ToString());
            //}


        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dtEmployeeDtls = new DataTable();
                DataTable dtEarningDed = new DataTable();

                ErrorCollection.Clear();
                ReportFile = Master.ReportName;


                if (ddlFacility.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("FACILTTY_ID", ddlFacility.SelectedItem.Value);
                }

                if (ddlContractNo.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("CONTRACT_ID", ddlContractNo.SelectedItem.Text);
                }
                if (ddlProperty.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("PROPERTY_ID", ddlProperty.SelectedItem.Value);
                }
                if (ddlBank.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("BANK_ID", ddlBank.SelectedItem.Value);
                }
              

                
                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;

                //ReportData = FIN.BLL.HR.Payslip_BLL.GetReportData();
                //dsData = DBMethod.ExecuteQuery(FIN.DAL.HR.LeaveDefinition_DAL.GetAbsentLeaveDetails(txtFromDate.Text.ToString(), txtToDate.Text.ToString()));
                ReportData = FIN.BLL.LOAN.Interest_BLL.GetFinancialDtlsReport();




                SubReportData = new DataSet();

                DataTable dt_2 = new DataTable("vw_ln_cost_centre");
                dt_2 = DBMethod.ExecuteQuery(FIN.DAL.LOAN.Facility_DAL.getCostCentre()).Tables[0];
                SubReportData.Tables.Add(dt_2.Copy());
               
                
               
                DataTable dt_1 = new DataTable("vw_ln_installment");
                dt_1 = DBMethod.ExecuteQuery(FIN.DAL.LOAN.Facility_DAL.getInstallment()).Tables[0];
               // SubReportData.Tables.Add(dt_1.Copy());

                DataTable dt_3 = dt_1.Copy();
                dt_3.TableName = "SS";
                SubReportData.Tables.Add(dt_3);




                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());

                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerReportPath + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AbsentReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }
        private void FillComboBox()
        {
            Facility_BLL.fn_GetFacilityNames(ref ddlFacility);
            Contract_BLL.fn_getContractNumbers(ref ddlContractNo);
            FIN.BLL.CA.Bank_BLL.fn_getBankNam(ref ddlBank);
            BankChargesBLL.GetPropertyName(ref ddlProperty); 
           
            

        }

        protected void ddlBank_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Contract_BLL.fn_getContractNumbersByBank(ref ddlContractNo, ddlBank.SelectedValue.ToString());
            Contract_BLL.fn_getContractNumbersByAll(ref ddlContractNo,ddlFacility.SelectedValue.ToString(),ddlProperty.SelectedValue.ToString(),ddlBank.SelectedValue.ToString());

        }

        protected void ddlFacility_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Contract_BLL.fn_getContractNumbersByFacility(ref ddlContractNo,ddlFacility.SelectedValue.ToString());
            Contract_BLL.fn_getContractNumbersByAll(ref ddlContractNo, ddlFacility.SelectedValue.ToString(), ddlProperty.SelectedValue.ToString(), ddlBank.SelectedValue.ToString());
        }

        protected void ddlProperty_SelectedIndexChanged(object sender, EventArgs e)
        {
           // Contract_BLL.fn_getContractNumbersByProperty(ref ddlContractNo, ddlProperty.SelectedValue.ToString());
            Contract_BLL.fn_getContractNumbersByAll(ref ddlContractNo, ddlFacility.SelectedValue.ToString(), ddlProperty.SelectedValue.ToString(), ddlBank.SelectedValue.ToString());
        }
        //private void FillContractNumber()
        //{
        //    if(ddlFacility.SelectedValue!=string.Empty && ddlProperty.SelectedValue!=string.Empty)
        //    {

        //    }
        //}
            



    }
}