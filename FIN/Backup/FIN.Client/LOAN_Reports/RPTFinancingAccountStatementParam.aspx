﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="RPTFinancingAccountStatementParam.aspx.cs" Inherits="FIN.Client.LOAN_Reports.RPTFinancingAccountStatementParam" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 900px" id="div1">
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblSupplierName">
                Finance Type
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 200px">
                <asp:DropDownList ID="ddlContractType" runat="server" TabIndex="1" CssClass=" ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="Div2">
                Finance Number
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 200px">
                <asp:DropDownList ID="ddlContractNo" runat="server" TabIndex="2" CssClass=" ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 150px" id="Div3">
                Bank
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 200px">
                <asp:DropDownList ID="ddlBank" runat="server" TabIndex="3" CssClass="ddlStype">
                </asp:DropDownList>
            </div>
            <div class="divClear_10">
            </div>
        </div>
        <div class="divRowContainer" style="display: none">
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblFromDate">
                From Date
            </div>
            <div class="divtxtBox  LNOrient" >
                <asp:TextBox ID="txtFromDate" runat="server" Width="200px" TabIndex="8" CssClass="RequiredField EntryFont  txtBox"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calExtender1" TargetControlID="txtFromDate"
                    OnClientDateSelectionChanged="checkDate" />
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtFromDate" />
            </div>
            <div class="colspace  LNOrient" style="width: 15px; float: left">
            </div>
            <div class="lblBox  LNOrient" style=" width: 150px" id="lblToDate">
                To Date
            </div>
            <div class="divtxtBox  LNOrient" >
                <asp:TextBox ID="txtToDate" runat="server" Width="200px" TabIndex="9" CssClass="RequiredField EntryFont  txtBox"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtToDate"
                    OnClientDateSelectionChanged="checkDate" />
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtToDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divFormcontainer" id="divMainContainer">
            <div class="divRowContainer divReportAction">
                <table class="ReportTable">
                    <tr>
                        <td style=" width: 500px">
                            <asp:ImageButton ID="btnSave" runat="server" ImageUrl="../Images/show-report-icon.png"
                                Width="35px" Height="25px" OnClick="btnSave_Click" />
                            <%--<asp:Button ID="btnSave" runat="server" Text="Show Report" CssClass="btn" OnClick="btnSave_Click" />--%>
                        </td>
                        <td>
                            <%-- <asp:Button ID="btnPrint" runat="server" Text="Print" CssClass="btn" />--%>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
     <script src="../LanguageScript/LN/LNChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
