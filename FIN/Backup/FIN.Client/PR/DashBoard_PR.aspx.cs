﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;

namespace FIN.Client.PR
{
    public partial class DashBoard_PR : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session["EMP_DEPT_GRAP"] = null;
                Session["EMP_DESIG_GRAP"] = null;

                FillComboBox();
                getSal4FinYear();
                getAnnualLeavePaid();

                DisplayEmployeeDetails();
                getEmpMobileExp();
                LoadGraph();
            }
            else
            {
                LoadGraph();
            }
        }

        private void getSal4FinYear()
        {
            DataTable dtNetSal = DBMethod.ExecuteQuery(FIN.DAL.PER.PayrollDashboard_DAL.getSal4FinYear(ddlDept.SelectedValue, ddlFinYear.SelectedValue)).Tables[0];
            Session["NET_SAL"] = dtNetSal;
            LoadGraph();
        }

        private void getEmpMobileExp()
        {
            DataTable dtEmpMobExp = DBMethod.ExecuteQuery(FIN.DAL.PER.PayrollDashboard_DAL.getEmpMobileExp(ddlDeptName.SelectedValue, ddlEmpl.SelectedValue, ddlFinanceYr.SelectedValue,ddlPayElement.SelectedValue)).Tables[0];
            Session["EMP_MOB_EXP"] = dtEmpMobExp;
            LoadGraph();
        }

        private void FillComboBox()
        {
            FIN.BLL.GL.AccountingCalendar_BLL.GetCalAcctYear(ref ddlFinYear);
            string str_finyear = FINSP.GetSPFOR_FiscalYear(DateTime.Now.Date.ToString("dd/MM/yyyy"));
            if (ddlFinYear.Items.Count > 0)
            {
                //ddlFinYear.SelectedIndex = ddlFinYear.Items.Count - 1;
                ddlFinYear.SelectedValue = str_finyear;
            }
            FIN.BLL.GL.AccountingCalendar_BLL.GetCalAcctYear(ref ddlFinanceYr);
            if (ddlFinanceYr.Items.Count > 0)
            {
                ddlFinanceYr.SelectedValue = str_finyear;
            }
            FIN.BLL.GL.AccountingCalendar_BLL.GetCalAcctYear(ref ddlFinYr);
            if (ddlFinYr.Items.Count > 0)
            {
                ddlFinYr.SelectedValue = str_finyear;
            }

            FIN.BLL.HR.Department_BLL.GetDepartmentNam(ref ddlDept);
            FIN.BLL.HR.Department_BLL.GetDepartmentNam(ref ddlDeptName);
            if (ddlDept.Items.Count > 0)
            {
                ddlDept.SelectedIndex = ddlDept.Items.Count - 1;
            }
           

            FIN.BLL.HR.Employee_BLL.GetEmployeeNam(ref ddlEmplName);
            if (ddlEmplName.Items.Count > 0)
            {
                ddlEmplName.SelectedIndex = 1;
            }

            FIN.BLL.PER.PayrollElements_BLL.getPayElementsDeductions(ref ddlPayElement);
            //if (ddlPayElement.Items.Count > 0)
            //{
            //    ddlPayElement.SelectedIndex = 1;
            //}

            FIN.BLL.GL.AccountingCalendar_BLL.GetCalAcctYear(ref ddl_ALP_Year);
            if (ddl_ALP_Year.Items.Count > 0)
            {
                //ddlLAFinYear.SelectedIndex = ddlLAFinYear.Items.Count - 1;
                ddl_ALP_Year.SelectedValue = str_finyear;
            }
            DataTable dt_cur_period = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountingCalendar_DAL.GetPeriodId4CurrentDate()).Tables[0];
            FIN.BLL.GL.AccountingCalendar_BLL.GetFinancialPeriod4Year(ref ddl_ALP_Period, ddl_ALP_Year.SelectedValue);
            if (dt_cur_period.Rows.Count > 0)
            {
                ddl_ALP_Period.SelectedValue = dt_cur_period.Rows[0]["PERIOD_ID"].ToString();
            }
        }

        private void LoadGraph()
        {
            if (Session["NET_SAL"] != null)
            {
                chrt_SalCount.DataSource = (DataTable)Session["NET_SAL"];
                chrt_SalCount.DataBind();
            }
            if (Session["EMP_MOB_EXP"] != null)
            {
                chrt_Dept.DataSource = (DataTable)Session["EMP_MOB_EXP"];
                chrt_Dept.Series["S_Dept_Count"].XValueMember = "pay_period_desc";
                chrt_Dept.Series["S_Dept_Count"].YValueMembers = "USED_AMT";
                chrt_Dept.DataBind();
            }

            if (Session["EMP_ANNUAL_LEAVE_PAID"] != null)
            {
                chrtAnnualLeave.DataSource = (DataTable)Session["EMP_ANNUAL_LEAVE_PAID"];
                chrtAnnualLeave.DataBind();
            }
            if (Session["EMP_INDEMNITY_LEAVE_PAID"] != null)
            {
                chrtIndemnityLeave.DataSource = (DataTable)Session["EMP_INDEMNITY_LEAVE_PAID"];
                chrtIndemnityLeave.DataBind();
            }

            if (Session["NET_SAL_EMP"] != null)
            {
                chrtEmpDetails.DataSource = (DataTable)Session["NET_SAL_EMP"];
                chrtEmpDetails.DataBind();
            }


        }

        private void getAnnualLeavePaid()
        {
            DataTable dtEmpAnnualLeavePaid = DBMethod.ExecuteQuery(FIN.DAL.PER.PayrollDashboard_DAL.getAnnualLeavePaid(ddl_ALP_Year.SelectedValue, ddl_ALP_Period.SelectedValue)).Tables[0];
            Session["EMP_ANNUAL_LEAVE_PAID"] = dtEmpAnnualLeavePaid;
        }

        private void getIndemnityPaidLeave()
        {
            DataTable dtEmpIndemnityLeavePaid = DBMethod.ExecuteQuery(FIN.DAL.PER.PayrollDashboard_DAL.getIndemLeavePaid()).Tables[0];
            Session["EMP_INDEMNITY_LEAVE_PAID"] = dtEmpIndemnityLeavePaid;
        }

        private void DisplayEmployeeDetails()
        {
            DataTable dt_EmpDet = DBMethod.ExecuteQuery(FIN.DAL.PER.PayrollDashboard_DAL.GetEmployeeDetails4Dashbord(ddlEmplName.SelectedValue)).Tables[0];
            Session["EMPDET"] = dt_EmpDet;
            if (dt_EmpDet.Rows.Count > 0)
            {
                lblEmpNo.Text = dt_EmpDet.Rows[0]["EMP_NO"].ToString();
                lblNmae.Text = dt_EmpDet.Rows[0]["EMP_NAME"].ToString();
                lblDepartmet.Text = dt_EmpDet.Rows[0]["DEPT_NAME"].ToString();
                lblDesignation.Text = dt_EmpDet.Rows[0]["DESIG_NAME"].ToString();
                lblDOJ.Text = DBMethod.ConvertDateToString(dt_EmpDet.Rows[0]["DOJ"].ToString());
                if (dt_EmpDet.Rows[0]["DOC"] != null)
                {
                    lblDOC.Text = DBMethod.ConvertDateToString(dt_EmpDet.Rows[0]["DOC"].ToString());
                }
                else
                {
                    lblDOC.Text = "";
                }

                DataTable dtNetSal = DBMethod.ExecuteQuery(FIN.DAL.PER.PayrollDashboard_DAL.getSal4FinYear(dt_EmpDet.Rows[0]["DEPT_ID"].ToString(), ddlFinYr.SelectedValue,ddlEmplName.SelectedValue)).Tables[0];
                Session["NET_SAL_EMP"] = dtNetSal;
            }
            LoadGraph();
        }

        protected void ddlFinYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            getSal4FinYear();
        }

        protected void ddlDept_SelectedIndexChanged(object sender, EventArgs e)
        {
            getSal4FinYear();
        }


        protected void ddl_ALP_Year_SelectedIndexChanged(object sender, EventArgs e)
        {
            FIN.BLL.GL.AccountingCalendar_BLL.GetFinancialPeriod4Year(ref ddl_ALP_Period, ddl_ALP_Year.SelectedValue);
            LoadGraph();
        }

        protected void ddl_ALP_Period_SelectedIndexChanged(object sender, EventArgs e)
        {
            getAnnualLeavePaid();
            LoadGraph();
        }


        protected void ddlDeptName_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlDeptName.SelectedValue.ToString() != "")
            {
                FIN.BLL.HR.Employee_BLL.GetEmplName(ref ddlEmpl, ddlDeptName.SelectedValue.ToString());
            }
            //getEmpMobileExp();
        }
        protected void ddlEmpl_SelectedIndexChanged(object sender, EventArgs e)
        {
            //getEmpMobileExp();
        }

        protected void ddlFinanceYr_SelectedIndexChanged(object sender, EventArgs e)
        {
            //getEmpMobileExp();
        }


        protected void ddlEmplName_SelectedIndexChanged(object sender, EventArgs e)
        {
            DisplayEmployeeDetails();
        }

        protected void ddlFinYr_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

      

        //private void FillComboBox()
        //{
        //    FIN.BLL.GL.Segments_BLL.GetGlobalSegmentvaluesBasedOrg(ref ddlGlobalSegment);
        //    //FIN.BLL.GL.AccountingGroups_BLL.fn_getAccountGroup(ref ddlGroupName);
        //    FIN.BLL.GL.AccountingGroups_BLL.getGroupDetails(ref ddlGroupName);
        //}

        //protected void btnGenerateChart_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        DataSet dsData = new DataSet();
        //        dsData = FIN.DAL.GL.AccountingGroups_DAL.GetSP_AccountGroupSummary(ddlGlobalSegment.SelectedValue.ToString(), DBMethod.ConvertStringToDate(txtDate.Text), FIN.BLL.GL.AccountingGroups_BLL.GetAccountingGroupsSummaryReportData());

        //        ChartGL.Series["GLCreditSeries"].ChartType = SeriesChartType.Column;
        //        ChartGL.Series["GLDebitSeries"].ChartType = SeriesChartType.Column;
        //        ChartGL.Series["GLCreditSeries"]["DrawingStyle"] = "Default";
        //        ChartGL.Series["GLDebitSeries"]["DrawingStyle"] = "Default";
        //        ChartGL.ChartAreas["ChartAreaGL"].Area3DStyle.Enable3D = false;
        //        ChartGL.Series["GLCreditSeries"].IsValueShownAsLabel = false;
        //        ChartGL.Series["GLDebitSeries"].IsValueShownAsLabel = false;

        //        ChartGL.Series["GLCreditSeries"].BorderWidth = 2;
        //        ChartGL.Series["GLDebitSeries"].BorderWidth = 2;
        //        //ChartGL.Series["Series1"].IsXValueIndexed = true;
        //        //ChartGL.Series["GLDebitSeries"].IsXValueIndexed = true;

        //        ChartGL.Series["GLCreditSeries"]["StackedGroupName"] = "Group1";
        //        ChartGL.Series["GLDebitSeries"]["StackedGroupName"] = "Group1";

        //        ChartGL.DataSource = dsData.Tables[0];
        //        ChartGL.DataBind();

        //        ChartGL.Series["GLCreditSeries"].XValueMember = "ACCOUNT_GROUP_DESCRIPTION";
        //        ChartGL.Series["GLCreditSeries"].YValueMembers = "TRANSACTION_CREDIT";

        //        ChartGL.Series["GLDebitSeries"].XValueMember = "ACCOUNT_GROUP_DESCRIPTION";
        //        ChartGL.Series["GLDebitSeries"].YValueMembers = "TRANSACTION_DEBIT";


        //        ChartGL.Series["GLCreditSeries"].Color = System.Drawing.Color.IndianRed;
        //        ChartGL.Series["GLCreditSeries"].Legend = "Default";
        //        ChartGL.Series["GLCreditSeries"].LegendText = "GL Credit";

        //        ChartGL.Series["GLDebitSeries"].Color = System.Drawing.Color.GreenYellow;
        //        ChartGL.Series["GLDebitSeries"].Legend = "Default";
        //        ChartGL.Series["GLDebitSeries"].LegendText = "GL Debit";


        //        ChartGL.Legends["Default"].BackColor = Color.Transparent;
        //        ChartGL.Legends["Default"].LegendStyle = LegendStyle.Column;
        //        ChartGL.Legends["Default"].ForeColor = Color.Black;
        //        ChartGL.Legends["Default"].Docking = Docking.Bottom;


        //        ChartGL.Legends["Default"].BorderColor = Color.Transparent;
        //        ChartGL.Legends["Default"].BackSecondaryColor = Color.Transparent;
        //        ChartGL.Legends["Default"].BackGradientStyle = GradientStyle.None;
        //        ChartGL.Legends["Default"].BorderColor = Color.Black;
        //        ChartGL.Legends["Default"].BorderWidth = 1;
        //        ChartGL.Legends["Default"].BorderDashStyle = ChartDashStyle.Solid;
        //        ChartGL.Legends["Default"].ShadowOffset = 1;

        //        //ChartGL.Series["Series1"]["PixelPointWidth"] = "50";
        //        //ChartGL.Series["Series2"]["PixelPointWidth"] = "50";


        //        ChartGL.BackColor = Color.Transparent;
        //        ChartGL.ChartAreas["ChartAreaGL"].AxisY = new System.Web.UI.DataVisualization.Charting.Axis { LabelStyle = new LabelStyle() { Font = new Font("Verdana", 8.0f, FontStyle.Regular) } };
        //        //ChartGL.ChartAreas["ChartAreaGL"].AxisY = new System.Web.UI.DataVisualization.Charting.Axis { LabelStyle = new LabelStyle() { ForeColor = Color.Blue } };

        //        ChartGL.ChartAreas["ChartAreaGL"].AxisX = new System.Web.UI.DataVisualization.Charting.Axis { LabelStyle = new LabelStyle() { Font = new Font("Verdana", 8.0f, FontStyle.Regular) } };
        //        //ChartGL.ChartAreas["ChartAreaGL"].AxisX = new System.Web.UI.DataVisualization.Charting.Axis { LabelStyle = new LabelStyle() { ForeColor = Color.Blue } };



        //        ChartGL.ChartAreas["ChartAreaGL"].Area3DStyle.Enable3D = false;
        //        ChartGL.ChartAreas["ChartAreaGL"].BackColor = Color.Transparent;
        //        ChartGL.ChartAreas["ChartAreaGL"].AxisX.MajorGrid.Enabled = false;
        //        ChartGL.ChartAreas["ChartAreaGL"].AxisY.MajorGrid.Enabled = true;
        //        ChartGL.ChartAreas["ChartAreaGL"].Position.Width = 100;
        //        ChartGL.ChartAreas["ChartAreaGL"].Position.Height = 100;


        //        ChartGL.ChartAreas["ChartAreaGL"].AxisX.IsMarginVisible = true;
        //        ChartGL.ChartAreas["ChartAreaGL"].Position.X = 6;
        //        ChartGL.ChartAreas["ChartAreaGL"].AxisX.Title = ".\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n.";


        //        ChartGL.ChartAreas["ChartAreaGL"].AxisY.IsStartedFromZero = true;
        //        ChartGL.ChartAreas["ChartAreaGL"].AxisY.Title = "Total Amount";
        //        ChartGL.ChartAreas["ChartAreaGL"].AxisX.Title = "GL Credit/Debit";
        //        ChartGL.ChartAreas["ChartAreaGL"].AxisY.LabelStyle.Format = "#,##,##0.#0";// "{900:C}";"#,##0;-#,##0;0"

        //        ChartGL.ChartAreas["ChartAreaGL"].AxisY.TitleFont = new Font("Verdana", 12, FontStyle.Regular);
        //        //ChartGL.ChartAreas["ChartAreaGL"].AxisY.TitleForeColor = Color.Blue;

        //        ChartGL.ChartAreas["ChartAreaGL"].AxisX.TitleFont = new Font("Verdana", 12, FontStyle.Regular);
        //        //ChartGL.ChartAreas["ChartAreaGL"].AxisX.TitleForeColor = Color.Blue;


        //        ChartGL.ChartAreas["ChartAreaGL"].AxisX.LabelStyle.Angle = -90;
        //        //ChartGL.DataBind();


        //        //CustomLabel fccl = new CustomLabel();
        //        //fccl.FromPosition = 0.50;
        //        //fccl.ToPosition = 1;
        //        //fccl.Text = "1 \n\n";
        //        //ChartGL.ChartAreas[0].AxisX.CustomLabels.Add(fccl);

        //        //CustomLabel fcc6 = new CustomLabel();
        //        //fcc6.FromPosition = 1.50;
        //        //fcc6.ToPosition = 2;
        //        //fcc6.Text = "2 \n\n";
        //        //ChartGL.ChartAreas[0].AxisX.CustomLabels.Add(fcc6);

        //        //CustomLabel fcc7 = new CustomLabel();
        //        //fcc7.FromPosition = 2.50;
        //        //fcc7.ToPosition = 3.00;
        //        //fcc7.Text = "3 \n\n";
        //        //ChartGL.ChartAreas[0].AxisX.CustomLabels.Add(fcc7);

        //        //CustomLabel fcc8 = new CustomLabel();
        //        //fcc8.FromPosition = 3.50;
        //        //fcc8.ToPosition = 4.00;
        //        //fcc8.Text = "4 \n\n";
        //        //ChartGL.ChartAreas[0].AxisX.CustomLabels.Add(fcc8);

        //        //CustomLabel fcc9 = new CustomLabel();
        //        //fcc9.FromPosition = 4.50;
        //        //fcc9.ToPosition = 5;
        //        //fcc9.Text = "5 \n\n";
        //        //ChartGL.ChartAreas[0].AxisX.CustomLabels.Add(fcc9);
        //        GenerateGroupChart();

        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //    finally
        //    {
        //        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerPath + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);
        //    }
        //}

        //protected void GenerateGroupChart()
        //{
        //    try
        //    {
        //        DataSet dsData = new DataSet();
        //        dsData = FIN.DAL.GL.AccountingGroups_DAL.GetSP_AccountGroupSummary(ddlGlobalSegment.SelectedValue.ToString(), DBMethod.ConvertStringToDate(txtDate.Text), FIN.BLL.GL.AccountingGroups_BLL.GetAccountingGroupsSummaryReportData());

        //        DataTable dtGroupData = new DataTable();
        //        dtGroupData = dsData.Tables[0];
        //        var query = from r in dsData.Tables[0].AsEnumerable()
        //                    where r.Field<string>("ACCOUNT_GROUP_DESCRIPTION") == (ddlGroupName.SelectedItem.Text)
        //                    select new
        //                    {
        //                        AccountDesc = r.Field<string>("ACCOUNT_GROUP_DESCRIPTION"),
        //                        TransCredit = r.Field<Decimal>("TRANSACTION_CREDIT"),
        //                        TransDebit = r.Field<Decimal>("TRANSACTION_DEBIT"),
        //                        OPCredit = r.Field<Decimal>("OPENING_BALANCE"),
        //                        OPDebit = r.Field<Decimal>("CLOSING_BALANCE"),
        //                        CBCredit = r.Field<Decimal>("CLOSING_BALANCE_CREDIT"),
        //                        CBDebit = r.Field<Decimal>("CLOSING_BALANCE_DEBIT")
        //                    };

        //        DataTable dtGroupData1 = new DataTable();
        //        dtGroupData1.Columns.Add("Account_Group");
        //        dtGroupData1.Columns.Add("TransCredit");
        //        dtGroupData1.Columns.Add("TransDebit");
        //        dtGroupData1.Columns.Add("OPCredit");
        //        dtGroupData1.Columns.Add("OPDebit");
        //        dtGroupData1.Columns.Add("CBCredit");
        //        dtGroupData1.Columns.Add("CBDebit");

        //        foreach (var item in query)
        //        {
        //            dtGroupData1.Rows.Add(item.AccountDesc, item.TransCredit, item.TransDebit, item.OPCredit, item.OPDebit, item.CBCredit, item.CBDebit);
        //        }

        //        ChartGroupGL.Series["Series1"].ChartType = SeriesChartType.Column;
        //        ChartGroupGL.Series["Series2"].ChartType = SeriesChartType.Column;
        //        ChartGroupGL.Series["Series3"].ChartType = SeriesChartType.Column;
        //        ChartGroupGL.Series["Series4"].ChartType = SeriesChartType.Column;
        //        ChartGroupGL.Series["Series5"].ChartType = SeriesChartType.Column;
        //        ChartGroupGL.Series["Series6"].ChartType = SeriesChartType.Column;

        //        ChartGroupGL.Series["Series1"]["DrawingStyle"] = "Default";
        //        ChartGroupGL.Series["Series2"]["DrawingStyle"] = "Default";
        //        ChartGroupGL.Series["Series3"]["DrawingStyle"] = "Default";
        //        ChartGroupGL.Series["Series4"]["DrawingStyle"] = "Default";
        //        ChartGroupGL.Series["Series5"]["DrawingStyle"] = "Default";
        //        ChartGroupGL.Series["Series6"]["DrawingStyle"] = "Default";

        //        ChartGroupGL.ChartAreas["ChartAreaGroupGL"].Area3DStyle.Enable3D = false;
        //        ChartGroupGL.Series["Series1"].IsValueShownAsLabel = false;
        //        ChartGroupGL.Series["Series2"].IsValueShownAsLabel = false;
        //        ChartGroupGL.Series["Series3"].IsValueShownAsLabel = false;
        //        ChartGroupGL.Series["Series4"].IsValueShownAsLabel = false;
        //        ChartGroupGL.Series["Series5"].IsValueShownAsLabel = false;
        //        ChartGroupGL.Series["Series6"].IsValueShownAsLabel = false;

        //        ChartGroupGL.Series["Series1"].BorderWidth = 2;
        //        ChartGroupGL.Series["Series2"].BorderWidth = 2;
        //        ChartGroupGL.Series["Series3"].BorderWidth = 2;
        //        ChartGroupGL.Series["Series4"].BorderWidth = 2;
        //        ChartGroupGL.Series["Series5"].BorderWidth = 2;
        //        ChartGroupGL.Series["Series6"].BorderWidth = 2;
        //        //ChartGroupGL.Series["Series1"].IsXValueIndexed = true;
        //        //ChartGroupGL.Series["Series2"].IsXValueIndexed = true;

        //        ChartGroupGL.Series["Series1"]["StackedGroupName"] = "Group1";
        //        ChartGroupGL.Series["Series2"]["StackedGroupName"] = "Group1";
        //        ChartGroupGL.Series["Series3"]["StackedGroupName"] = "Group1";
        //        ChartGroupGL.Series["Series4"]["StackedGroupName"] = "Group1";
        //        ChartGroupGL.Series["Series5"]["StackedGroupName"] = "Group1";
        //        ChartGroupGL.Series["Series6"]["StackedGroupName"] = "Group1";


        //        ChartGroupGL.DataSource = dtGroupData1;
        //        ChartGroupGL.DataBind();

        //        ChartGroupGL.Series["Series1"].XValueMember = "Account_Group";
        //        ChartGroupGL.Series["Series1"].YValueMembers = "TransCredit";

        //        ChartGroupGL.Series["Series2"].XValueMember = "Account_Group";
        //        ChartGroupGL.Series["Series2"].YValueMembers = "TransDebit";

        //        ChartGroupGL.Series["Series3"].XValueMember = "Account_Group";
        //        ChartGroupGL.Series["Series3"].YValueMembers = "OPCredit";

        //        ChartGroupGL.Series["Series4"].XValueMember = "Account_Group";
        //        ChartGroupGL.Series["Series4"].YValueMembers = "OPDebit";

        //        ChartGroupGL.Series["Series5"].XValueMember = "Account_Group";
        //        ChartGroupGL.Series["Series5"].YValueMembers = "CBCredit";

        //        ChartGroupGL.Series["Series6"].XValueMember = "Account_Group";
        //        ChartGroupGL.Series["Series6"].YValueMembers = "CBDebit";


        //        ChartGroupGL.Series["Series1"].Color = System.Drawing.Color.Pink;
        //        ChartGroupGL.Series["Series1"].Legend = "DefaultGroup";
        //        ChartGroupGL.Series["Series1"].LegendText = "Transaction Credit";

        //        ChartGroupGL.Series["Series2"].Color = System.Drawing.Color.Goldenrod;
        //        ChartGroupGL.Series["Series2"].Legend = "DefaultGroup";
        //        ChartGroupGL.Series["Series2"].LegendText = "Transaction Debit";

        //        ChartGroupGL.Series["Series3"].Color = System.Drawing.Color.ForestGreen;
        //        ChartGroupGL.Series["Series3"].Legend = "DefaultGroup";
        //        ChartGroupGL.Series["Series3"].LegendText = "Opening Balance";

        //        ChartGroupGL.Series["Series4"].Color = System.Drawing.Color.Green;
        //        ChartGroupGL.Series["Series4"].Legend = "DefaultGroup";
        //        ChartGroupGL.Series["Series4"].LegendText = "Closing Balance";

        //        ChartGroupGL.Series["Series5"].Color = System.Drawing.Color.Gray;
        //        ChartGroupGL.Series["Series5"].Legend = "DefaultGroup";
        //        ChartGroupGL.Series["Series5"].LegendText = "Closing Balance Credit";

        //        ChartGroupGL.Series["Series6"].Color = System.Drawing.Color.Red;
        //        ChartGroupGL.Series["Series6"].Legend = "DefaultGroup";
        //        ChartGroupGL.Series["Series6"].LegendText = "Closing Balance Debit";



        //        ChartGroupGL.Legends["DefaultGroup"].BackColor = Color.Transparent;
        //        ChartGroupGL.Legends["DefaultGroup"].LegendStyle = LegendStyle.Column;
        //        ChartGroupGL.Legends["DefaultGroup"].ForeColor = Color.Black;
        //        ChartGroupGL.Legends["DefaultGroup"].Docking = Docking.Bottom;


        //        ChartGroupGL.Legends["DefaultGroup"].BorderColor = Color.Transparent;
        //        ChartGroupGL.Legends["DefaultGroup"].BackSecondaryColor = Color.Transparent;
        //        ChartGroupGL.Legends["DefaultGroup"].BackGradientStyle = GradientStyle.None;
        //        ChartGroupGL.Legends["DefaultGroup"].BorderColor = Color.Black;
        //        ChartGroupGL.Legends["DefaultGroup"].BorderWidth = 2;
        //        ChartGroupGL.Legends["DefaultGroup"].BorderDashStyle = ChartDashStyle.Solid;
        //        ChartGroupGL.Legends["DefaultGroup"].ShadowOffset = 2;

        //        //ChartGroupGL.Series["Series1"]["PixelPointWidth"] = "50";
        //        //ChartGroupGL.Series["Series2"]["PixelPointWidth"] = "50";


        //        ChartGroupGL.BackColor = Color.Transparent;
        //        ChartGroupGL.ChartAreas["ChartAreaGroupGL"].AxisY = new System.Web.UI.DataVisualization.Charting.Axis { LabelStyle = new LabelStyle() { Font = new Font("Verdana", 8.0f, FontStyle.Regular) } };
        //        //ChartGroupGL.ChartAreas["ChartAreaGL"].AxisY = new System.Web.UI.DataVisualization.Charting.Axis { LabelStyle = new LabelStyle() { ForeColor = Color.Blue } };

        //        ChartGroupGL.ChartAreas["ChartAreaGroupGL"].AxisX = new System.Web.UI.DataVisualization.Charting.Axis { LabelStyle = new LabelStyle() { Font = new Font("Verdana", 8.0f, FontStyle.Regular) } };
        //        //ChartGroupGL.ChartAreas["ChartAreaGL"].AxisX = new System.Web.UI.DataVisualization.Charting.Axis { LabelStyle = new LabelStyle() { ForeColor = Color.Blue } };



        //        ChartGroupGL.ChartAreas["ChartAreaGroupGL"].Area3DStyle.Enable3D = false;
        //        ChartGroupGL.ChartAreas["ChartAreaGroupGL"].BackColor = Color.Transparent;
        //        ChartGroupGL.ChartAreas["ChartAreaGroupGL"].AxisX.MajorGrid.Enabled = false;
        //        ChartGroupGL.ChartAreas["ChartAreaGroupGL"].AxisY.MajorGrid.Enabled = true;
        //        ChartGroupGL.ChartAreas["ChartAreaGroupGL"].Position.Width = 100;
        //        ChartGroupGL.ChartAreas["ChartAreaGroupGL"].Position.Height = 100;


        //        ChartGroupGL.ChartAreas["ChartAreaGroupGL"].AxisX.IsMarginVisible = true;
        //        ChartGroupGL.ChartAreas["ChartAreaGroupGL"].Position.X = 6;
        //        ChartGroupGL.ChartAreas["ChartAreaGroupGL"].AxisX.Title = ".\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n.";


        //        ChartGroupGL.ChartAreas["ChartAreaGroupGL"].AxisY.IsStartedFromZero = true;
        //        ChartGroupGL.ChartAreas["ChartAreaGroupGL"].AxisY.Title = "Total Amount";
        //        ChartGroupGL.ChartAreas["ChartAreaGroupGL"].AxisX.Title = "Total GL Credit/Debit";
        //        ChartGroupGL.ChartAreas["ChartAreaGroupGL"].AxisY.LabelStyle.Format = "#,##,##0.#0";// "{900:C}";"#,##0;-#,##0;0"

        //        ChartGroupGL.ChartAreas["ChartAreaGroupGL"].AxisY.TitleFont = new Font("Verdana", 12, FontStyle.Regular);
        //        //ChartGroupGL.ChartAreas["ChartAreaGroupGL"].AxisY.TitleForeColor = Color.Blue;

        //        ChartGroupGL.ChartAreas["ChartAreaGroupGL"].AxisX.TitleFont = new Font("Verdana", 12, FontStyle.Regular);
        //        //ChartGroupGL.ChartAreas["ChartAreaGroupGL"].AxisX.TitleForeColor = Color.Blue;

        //        //ChartGroupGL.ChartAreas["ChartAreaGroupGL"].AxisX.IsLabelAutoFit = false;
        //        ChartGroupGL.ChartAreas["ChartAreaGroupGL"].AxisX.LabelStyle.Angle = -90;


        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //    finally
        //    {
        //        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerPath + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);
        //    }
        //}

        protected void btnSave_Click(object sender, EventArgs e)
        {

        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {

        }
        protected void btnView_Click(object sender, EventArgs e)
        {
            getEmpMobileExp();
        }

    }
}