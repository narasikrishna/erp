﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using FIN.BLL;
using FIN.DAL;
using FIN.DAL.FA;
using FIN.DAL.GL;
using FIN.DAL.AP;
using FIN.DAL.HR;
using System.Web.UI.WebControls;
namespace FIN.Client
{
    public class ComboFilling
    {
        static string intialRowValueField = "";
        static string intialRowTextField = "---Select---";

        public static void fn_GetLookUpData(ref DropDownList ddl_lookup, string str_KeyName)
        {
            ddl_lookup.AppendDataBoundItems = true;
            ddl_lookup.Items.Clear();
            if (VMVServices.Web.Utils.LanguageCode.ToString().Trim().Length == 0)
            {

                ddl_lookup.Items.Add(new ListItem(FINAppConstants.intialRowTextField, ""));
            }
            else
            {
                ddl_lookup.Items.Add(new ListItem(FINAppConstants.intialRowTextField_OL, ""));
            }
            //ddl_lookup.Items.Add(new ListItem("---Select---", ""));
            ddl_lookup.DataTextField = FINColumnConstants.VALUE_NAME;
            ddl_lookup.DataValueField = FINColumnConstants.VALUE_KEY_ID;
            ddl_lookup.DataSource = DBMethod.ExecuteQuery(AccountingCalendar_DAL.getfromLookup(str_KeyName)).Tables[0];
            ddl_lookup.DataBind();
        }


     
        public static void fn_getGroupName(ref DropDownList ddlGroupName,string mode)
        {
            ddlGroupName.AppendDataBoundItems = true;
            ddlGroupName.Items.Clear();
            if (VMVServices.Web.Utils.LanguageCode.ToString().Trim().Length == 0)
            {

                ddlGroupName.Items.Add(new ListItem(FINAppConstants.intialRowTextField, ""));
            }
            else
            {
                ddlGroupName.Items.Add(new ListItem(FINAppConstants.intialRowTextField_OL, ""));
            }
           // ddlGroupName.Items.Add(new ListItem("---Select---", ""));
            ddlGroupName.DataTextField = "ACCT_GRP_DESC";
            ddlGroupName.DataValueField = "ACCT_GRP_ID";
            ddlGroupName.DataSource = DBMethod.ExecuteQuery(AccountingGroupDefaultSegment_DAL.getGroupName(mode)).Tables[0];
            ddlGroupName.DataBind();
        }

        public static void fn_getSegmentName(ref DropDownList ddlSegmentName)
        {
            ddlSegmentName.AppendDataBoundItems = true;
            ddlSegmentName.Items.Clear();
            if (VMVServices.Web.Utils.LanguageCode.ToString().Trim().Length == 0)
            {

                ddlSegmentName.Items.Add(new ListItem(FINAppConstants.intialRowTextField, ""));
            }
            else
            {
                ddlSegmentName.Items.Add(new ListItem(FINAppConstants.intialRowTextField_OL, ""));
            }
           // ddlSegmentName.Items.Add(new ListItem("---Select---", ""));
            ddlSegmentName.DataTextField = "SEGMENT_NAME";
            ddlSegmentName.DataValueField = "SEGMENT_ID";
            ddlSegmentName.DataSource = DBMethod.ExecuteQuery(AccountingGroupDefaultSegment_DAL.getSegmentName()).Tables[0];
            ddlSegmentName.DataBind();
        }


        //GL > Account Group Links                
        public static void fn_getGroupNameDetails(ref DropDownList ddlGroupName)
        {
            ddlGroupName.AppendDataBoundItems = true;
            ddlGroupName.Items.Clear();
            if (VMVServices.Web.Utils.LanguageCode.ToString().Trim().Length == 0)
            {

                ddlGroupName.Items.Add(new ListItem(FINAppConstants.intialRowTextField, ""));
            }
            else
            {
                ddlGroupName.Items.Add(new ListItem(FINAppConstants.intialRowTextField_OL, ""));
            }
            //ddlGroupName.Items.Add(new ListItem("---Select---", ""));
            ddlGroupName.DataTextField = "GROUP_NAME";
            ddlGroupName.DataValueField = "GROUP_ID";
            ddlGroupName.DataSource = DBMethod.ExecuteQuery(AccountingGroupLinks_DAL.getAccountGroupName()).Tables[0];
            ddlGroupName.DataBind();
        }


        //GL > Journal Entry
        public static void fn_getOrganisationDetails(ref DropDownList ddlOrganisation)
        {
            ddlOrganisation.AppendDataBoundItems = true;
            ddlOrganisation.Items.Clear();
            if (VMVServices.Web.Utils.LanguageCode.ToString().Trim().Length == 0)
            {

                ddlOrganisation.Items.Add(new ListItem(FINAppConstants.intialRowTextField, ""));
            }
            else
            {
                ddlOrganisation.Items.Add(new ListItem(FINAppConstants.intialRowTextField_OL, ""));
            }
           // ddlOrganisation.Items.Add(new ListItem("---Select---", ""));
            ddlOrganisation.DataTextField = "ORG_NAME";
            ddlOrganisation.DataValueField = "ORG_ID";
            ddlOrganisation.DataSource = DBMethod.ExecuteQuery(JournalEntry_DAL.getOrganisationDetails()).Tables[0];
            ddlOrganisation.DataBind();
        }


        //GL > Journal Entry
        public static void fn_getGlobalSegment(ref DropDownList ddlGlobalSegment)
        {
            ddlGlobalSegment.AppendDataBoundItems = true;
            ddlGlobalSegment.Items.Clear();
            if (VMVServices.Web.Utils.LanguageCode.ToString().Trim().Length == 0)
            {

                ddlGlobalSegment.Items.Add(new ListItem(FINAppConstants.intialRowTextField, ""));
            }
            else
            {
                ddlGlobalSegment.Items.Add(new ListItem(FINAppConstants.intialRowTextField_OL, ""));
            }
            //ddlGlobalSegment.Items.Add(new ListItem("---Select---", ""));
            ddlGlobalSegment.DataTextField = "SEGMENT_NAME";
            ddlGlobalSegment.DataValueField = "SEGMENT_ID";
            ddlGlobalSegment.DataSource = DBMethod.ExecuteQuery(JournalEntry_DAL.getGlobalSegment()).Tables[0];
            ddlGlobalSegment.DataBind();
        }


        //GL > Journal Entry
        public static void fn_getJournalType(ref DropDownList ddlJournalType)
        {
            ddlJournalType.AppendDataBoundItems = true;
            ddlJournalType.Items.Clear();
            if (VMVServices.Web.Utils.LanguageCode.ToString().Trim().Length == 0)
            {

                ddlJournalType.Items.Add(new ListItem(FINAppConstants.intialRowTextField, ""));
            }
            else
            {
                ddlJournalType.Items.Add(new ListItem(FINAppConstants.intialRowTextField_OL, ""));
            }
            //ddlJournalType.Items.Add(new ListItem("---Select---", ""));
            ddlJournalType.DataTextField = "JOURNAL_NAME";
            ddlJournalType.DataValueField = "JOURNAL_ID";
            ddlJournalType.DataSource = DBMethod.ExecuteQuery(JournalEntry_DAL.getJournalType()).Tables[0];
            ddlJournalType.DataBind();
        }

        //GL > Journal Entry
        public static void fn_getCurrencyDetails(ref DropDownList ddlCurrency)
        {
            ddlCurrency.AppendDataBoundItems = true;
            ddlCurrency.Items.Clear();
            if (VMVServices.Web.Utils.LanguageCode.ToString().Trim().Length == 0)
            {

                ddlCurrency.Items.Add(new ListItem(FINAppConstants.intialRowTextField, ""));
            }
            else
            {
                ddlCurrency.Items.Add(new ListItem(FINAppConstants.intialRowTextField_OL, ""));
            }
            //ddlCurrency.Items.Add(new ListItem("---Select---", ""));
            ddlCurrency.DataTextField = "CURRENCY_NAME";
            ddlCurrency.DataValueField = "CURRENCY_ID";
            ddlCurrency.DataSource = DBMethod.ExecuteQuery(JournalEntry_DAL.getCurrencyDetails()).Tables[0];
            ddlCurrency.DataBind();
        }


        public static void getStructName(ref DropDownList ddlAcctStruct)
        {
            ddlAcctStruct.AppendDataBoundItems = true;
            ddlAcctStruct.Items.Clear();
            if (VMVServices.Web.Utils.LanguageCode.ToString().Trim().Length == 0)
            {

                ddlAcctStruct.Items.Add(new ListItem(FINAppConstants.intialRowTextField, ""));
            }
            else
            {
                ddlAcctStruct.Items.Add(new ListItem(FINAppConstants.intialRowTextField_OL, ""));
            }
            //ddlAcctStruct.Items.Add(new ListItem("---Select---", ""));
            ddlAcctStruct.DataTextField = "ACCT_STRUCT_NAME";
            ddlAcctStruct.DataValueField = "ACCT_STRUCT_ID";
            ddlAcctStruct.DataSource = DBMethod.ExecuteQuery(Organisation_DAL.getStructName()).Tables[0];
            ddlAcctStruct.DataBind();
        }

        public static void getSegments(ref DropDownList ddl_Segment)
        {
            ddl_Segment.AppendDataBoundItems = true;
            ddl_Segment.Items.Clear();
            if (VMVServices.Web.Utils.LanguageCode.ToString().Trim().Length == 0)
            {

                ddl_Segment.Items.Add(new ListItem(FINAppConstants.intialRowTextField, ""));
            }
            else
            {
                ddl_Segment.Items.Add(new ListItem(FINAppConstants.intialRowTextField_OL, ""));
            }
           // ddl_Segment.Items.Add(new ListItem("---Select---", ""));
            ddl_Segment.DataTextField = "SEGMENT_NAME";
            ddl_Segment.DataValueField = "SEGMENT_ID";
            ddl_Segment.DataSource = DBMethod.ExecuteQuery(AccountCodes_DAL.getSegment()).Tables[0];
            ddl_Segment.DataBind();
        }
        public static void getCalendar(ref DropDownList ddlAcctcal)
        {
            ddlAcctcal.AppendDataBoundItems = true;
            ddlAcctcal.Items.Clear();
            if (VMVServices.Web.Utils.LanguageCode.ToString().Trim().Length == 0)
            {

                ddlAcctcal.Items.Add(new ListItem(FINAppConstants.intialRowTextField, ""));
            }
            else
            {
                ddlAcctcal.Items.Add(new ListItem(FINAppConstants.intialRowTextField_OL, ""));
            }
           // ddlAcctcal.Items.Add(new ListItem("---Select---", ""));
            ddlAcctcal.DataTextField = "CAL_DESC";
            ddlAcctcal.DataValueField = "CAL_ID";
            ddlAcctcal.DataSource = DBMethod.ExecuteQuery(Organisation_DAL.getCalendar()).Tables[0];
            ddlAcctcal.DataBind();
        }

        //GL > Account Codes
        public static void getStructure(ref DropDownList ddl_StructureName)
        {
            ddl_StructureName.AppendDataBoundItems = true;
            ddl_StructureName.Items.Clear();
            if (VMVServices.Web.Utils.LanguageCode.ToString().Trim().Length == 0)
            {

                ddl_StructureName.Items.Add(new ListItem(FINAppConstants.intialRowTextField, ""));
            }
            else
            {
                ddl_StructureName.Items.Add(new ListItem(FINAppConstants.intialRowTextField_OL, ""));
            }
           // ddl_StructureName.Items.Add(new ListItem("---Select---", ""));
            ddl_StructureName.DataTextField = "ACCT_STRUCT_NAME";
            ddl_StructureName.DataValueField = "ACCT_STRUCT_ID";
            ddl_StructureName.DataSource = DBMethod.ExecuteQuery(AccountCodes_DAL.getStructName()).Tables[0];
            ddl_StructureName.DataBind();
        }

        //GL > Account Codes
        public static void getGroup(ref DropDownList ddl_GroupName)
        {
            ddl_GroupName.AppendDataBoundItems = true;
            ddl_GroupName.Items.Clear();
            if (VMVServices.Web.Utils.LanguageCode.ToString().Trim().Length == 0)
            {

                ddl_GroupName.Items.Add(new ListItem(FINAppConstants.intialRowTextField, ""));
            }
            else
            {
                ddl_GroupName.Items.Add(new ListItem(FINAppConstants.intialRowTextField_OL, ""));
            }
            //ddl_GroupName.Items.Add(new ListItem("---Select---", ""));
            ddl_GroupName.DataTextField = "ACCT_GRP_DESC";
            ddl_GroupName.DataValueField = "ACCT_GRP_ID";
            ddl_GroupName.DataSource = DBMethod.ExecuteQuery(AccountCodes_DAL.getGroupName()).Tables[0];
            ddl_GroupName.DataBind();
        }

        public static void getCountry(ref DropDownList ddlCountry)
        {
            ddlCountry.AppendDataBoundItems = true;
            ddlCountry.Items.Clear();
            if (VMVServices.Web.Utils.LanguageCode.ToString().Trim().Length == 0)
            {

                ddlCountry.Items.Add(new ListItem(FINAppConstants.intialRowTextField, ""));
            }
            else
            {
                ddlCountry.Items.Add(new ListItem(FINAppConstants.intialRowTextField_OL, ""));
            }
            //ddlCountry.Items.Add(new ListItem(intialRowTextField, intialRowValueField));
            ddlCountry.DataTextField = "FULL_NAME";
            ddlCountry.DataValueField = "COUNTRY_CODE";
            ddlCountry.DataSource = DBMethod.ExecuteQuery(Organisation_DAL.getCountry()).Tables[0];
            ddlCountry.DataBind();
        }


        //GL > Budget
        public static void fn_getBudgetType(ref DropDownList ddlBudgetType)
        {
            ddlBudgetType.AppendDataBoundItems = true;
            ddlBudgetType.Items.Clear();
            if (VMVServices.Web.Utils.LanguageCode.ToString().Trim().Length == 0)
            {

                ddlBudgetType.Items.Add(new ListItem(FINAppConstants.intialRowTextField, ""));
            }
            else
            {
                ddlBudgetType.Items.Add(new ListItem(FINAppConstants.intialRowTextField_OL, ""));
            }
           // ddlBudgetType.Items.Add(new ListItem("---Select---", ""));
            ddlBudgetType.DataTextField = "JOURNAL_NAME";
            ddlBudgetType.DataValueField = "JOURNAL_ID";
            ddlBudgetType.DataSource = DBMethod.ExecuteQuery(Budget_DAL.getBudgetType()).Tables[0];
            ddlBudgetType.DataBind();
        }

        // GL > Journal Entry Details - Segment 1
        public static void fn_getSegment1Details(ref DropDownList ddlSegment1)
        {
            ddlSegment1.AppendDataBoundItems = true;
            ddlSegment1.Items.Clear();
            if (VMVServices.Web.Utils.LanguageCode.ToString().Trim().Length == 0)
            {

                ddlSegment1.Items.Add(new ListItem(FINAppConstants.intialRowTextField, ""));
            }
            else
            {
                ddlSegment1.Items.Add(new ListItem(FINAppConstants.intialRowTextField_OL, ""));
            }
           // ddlSegment1.Items.Add(new ListItem("---Select---", ""));
            ddlSegment1.DataTextField = "SEGMENT_VALUE";
            ddlSegment1.DataValueField = "SEGMENT_VALUE_ID";
            ddlSegment1.DataSource = DBMethod.ExecuteQuery(JournalEntry_DAL.getSegmentValues()).Tables[0];
            ddlSegment1.DataBind();
        }
        // GL > Journal Entry Details - Segment 2
        public static void fn_getSegment2Details(ref DropDownList ddlSegment2)
        {
            ddlSegment2.AppendDataBoundItems = true;
            ddlSegment2.Items.Clear();
            if (VMVServices.Web.Utils.LanguageCode.ToString().Trim().Length == 0)
            {

                ddlSegment2.Items.Add(new ListItem(FINAppConstants.intialRowTextField, ""));
            }
            else
            {
                ddlSegment2.Items.Add(new ListItem(FINAppConstants.intialRowTextField_OL, ""));
            }
            //ddlSegment2.Items.Add(new ListItem("---Select---", ""));
            ddlSegment2.DataTextField = "SEGMENT_VALUE";
            ddlSegment2.DataValueField = "SEGMENT_VALUE_ID";
            ddlSegment2.DataSource = DBMethod.ExecuteQuery(JournalEntry_DAL.getSegmentValues()).Tables[0];
            ddlSegment2.DataBind();
        }
        // GL > Journal Entry Details - Segment 3
        public static void fn_getSegment3Details(ref DropDownList ddlSegment3)
        {
            ddlSegment3.AppendDataBoundItems = true;
            ddlSegment3.Items.Clear();
            if (VMVServices.Web.Utils.LanguageCode.ToString().Trim().Length == 0)
            {

                ddlSegment3.Items.Add(new ListItem(FINAppConstants.intialRowTextField, ""));
            }
            else
            {
                ddlSegment3.Items.Add(new ListItem(FINAppConstants.intialRowTextField_OL, ""));
            }
           // ddlSegment3.Items.Add(new ListItem("---Select---", ""));
            ddlSegment3.DataTextField = "SEGMENT_VALUE";
            ddlSegment3.DataValueField = "SEGMENT_VALUE_ID";
            ddlSegment3.DataSource = DBMethod.ExecuteQuery(JournalEntry_DAL.getSegmentValues()).Tables[0];
            ddlSegment3.DataBind();
        }
        // GL > Journal Entry Details - Segment 4
        public static void fn_getSegment4Details(ref DropDownList ddlSegment4)
        {
            ddlSegment4.AppendDataBoundItems = true;
            ddlSegment4.Items.Clear();
            if (VMVServices.Web.Utils.LanguageCode.ToString().Trim().Length == 0)
            {

                ddlSegment4.Items.Add(new ListItem(FINAppConstants.intialRowTextField, ""));
            }
            else
            {
                ddlSegment4.Items.Add(new ListItem(FINAppConstants.intialRowTextField_OL, ""));
            }
           // ddlSegment4.Items.Add(new ListItem("---Select---", ""));
            ddlSegment4.DataTextField = "SEGMENT_VALUE";
            ddlSegment4.DataValueField = "SEGMENT_VALUE_ID";
            ddlSegment4.DataSource = DBMethod.ExecuteQuery(JournalEntry_DAL.getSegmentValues()).Tables[0];
            ddlSegment4.DataBind();
        }
        // GL > Journal Entry Details - Segment 5
        public static void fn_getSegment5Details(ref DropDownList ddlSegment5)
        {
            ddlSegment5.AppendDataBoundItems = true;
            ddlSegment5.Items.Clear();
            if (VMVServices.Web.Utils.LanguageCode.ToString().Trim().Length == 0)
            {

                ddlSegment5.Items.Add(new ListItem(FINAppConstants.intialRowTextField, ""));
            }
            else
            {
                ddlSegment5.Items.Add(new ListItem(FINAppConstants.intialRowTextField_OL, ""));
            }
           // ddlSegment5.Items.Add(new ListItem("---Select---", ""));
            ddlSegment5.DataTextField = "SEGMENT_VALUE";
            ddlSegment5.DataValueField = "SEGMENT_VALUE_ID";
            ddlSegment5.DataSource = DBMethod.ExecuteQuery(JournalEntry_DAL.getSegmentValues()).Tables[0];
            ddlSegment5.DataBind();
        }
        // GL > Journal Entry Details - Segment 6
        public static void fn_getSegment6Details(ref DropDownList ddlSegment6)
        {
            ddlSegment6.AppendDataBoundItems = true;
            ddlSegment6.Items.Clear();
            if (VMVServices.Web.Utils.LanguageCode.ToString().Trim().Length == 0)
            {

                ddlSegment6.Items.Add(new ListItem(FINAppConstants.intialRowTextField, ""));
            }
            else
            {
                ddlSegment6.Items.Add(new ListItem(FINAppConstants.intialRowTextField_OL, ""));
            }
           // ddlSegment6.Items.Add(new ListItem("---Select---", ""));
            ddlSegment6.DataTextField = "SEGMENT_VALUE";
            ddlSegment6.DataValueField = "SEGMENT_VALUE_ID";
            ddlSegment6.DataSource = DBMethod.ExecuteQuery(JournalEntry_DAL.getSegmentValues()).Tables[0];
            ddlSegment6.DataBind();
        }

        //AP UOM Master
        public static void fn_getUOMMaster(ref DropDownList ddlCountry)
        {
            ddlCountry.AppendDataBoundItems = true;
            ddlCountry.Items.Clear();
            if (VMVServices.Web.Utils.LanguageCode.ToString().Trim().Length == 0)
            {

                ddlCountry.Items.Add(new ListItem(FINAppConstants.intialRowTextField, ""));
            }
            else
            {
                ddlCountry.Items.Add(new ListItem(FINAppConstants.intialRowTextField_OL, ""));
            }
            //ddlCountry.Items.Add(new ListItem(intialRowTextField,intialRowValueField));
            ddlCountry.DataTextField = "LOOKUP_NAME";
            ddlCountry.DataValueField = "LOOKUP_ID";
            ddlCountry.DataSource = DBMethod.ExecuteQuery(UOMMasters_DAL.getUOMMaster()).Tables[0];
            ddlCountry.DataBind();
        }

        //AP UOM Conversation
        public static void fn_getUOMConversationFrom(ref DropDownList ddlUOMFrom)
        {
            ddlUOMFrom.AppendDataBoundItems = true;
            ddlUOMFrom.Items.Clear();
            if (VMVServices.Web.Utils.LanguageCode.ToString().Trim().Length == 0)
            {

                ddlUOMFrom.Items.Add(new ListItem(FINAppConstants.intialRowTextField, ""));
            }
            else
            {
                ddlUOMFrom.Items.Add(new ListItem(FINAppConstants.intialRowTextField_OL, ""));
            }
           // ddlUOMFrom.Items.Add(new ListItem(intialRowTextField, intialRowValueField));
            ddlUOMFrom.DataTextField = "UOM_NAME";
            ddlUOMFrom.DataValueField = "UOM_CODE";
            ddlUOMFrom.DataSource = DBMethod.ExecuteQuery(UOMConversion_DAL.getUOMConversationFrom()).Tables[0];
            ddlUOMFrom.DataBind();
        }

        //AP UOM Conversation
        public static void fn_getUOMConversationTo(ref DropDownList ddlUOMTo)
        {
            ddlUOMTo.AppendDataBoundItems = true;
            ddlUOMTo.Items.Clear();
            if (VMVServices.Web.Utils.LanguageCode.ToString().Trim().Length == 0)
            {

                ddlUOMTo.Items.Add(new ListItem(FINAppConstants.intialRowTextField, ""));
            }
            else
            {
                ddlUOMTo.Items.Add(new ListItem(FINAppConstants.intialRowTextField_OL, ""));
            }
           // ddlUOMTo.Items.Add(new ListItem(intialRowTextField, intialRowValueField));
            ddlUOMTo.DataTextField = "UOM_NAME";
            ddlUOMTo.DataValueField = "UOM_CODE";
            ddlUOMTo.DataSource = DBMethod.ExecuteQuery(UOMConversion_DAL.getUOMConversationTo()).Tables[0];
            ddlUOMTo.DataBind();
        }


        //AP Item Category
        public static void fn_getCategoryParent(ref DropDownList ddlCategoryParent)
        {
            ddlCategoryParent.AppendDataBoundItems = true;
            ddlCategoryParent.Items.Clear();
            if (VMVServices.Web.Utils.LanguageCode.ToString().Trim().Length == 0)
            {

                ddlCategoryParent.Items.Add(new ListItem(FINAppConstants.intialRowTextField, ""));
            }
            else
            {
                ddlCategoryParent.Items.Add(new ListItem(FINAppConstants.intialRowTextField_OL, ""));
            }
            //ddlCategoryParent.Items.Add(new ListItem("---Select---", ""));
            ddlCategoryParent.DataTextField = "CAT_NAME";
            ddlCategoryParent.DataValueField = "CAT_ID";
            ddlCategoryParent.DataSource = DBMethod.ExecuteQuery(ItemCategory_DAL.getCategoryParent()).Tables[0];
            ddlCategoryParent.DataBind();
        }


        public static void fn_getCategoryChild(ref DropDownList ddlCategoryParent)
        {
            ddlCategoryParent.AppendDataBoundItems = true;
            ddlCategoryParent.Items.Clear();
            if (VMVServices.Web.Utils.LanguageCode.ToString().Trim().Length == 0)
            {

                ddlCategoryParent.Items.Add(new ListItem(FINAppConstants.intialRowTextField, ""));
            }
            else
            {
                ddlCategoryParent.Items.Add(new ListItem(FINAppConstants.intialRowTextField_OL, ""));
            }
            //ddlCategoryParent.Items.Add(new ListItem("---Select---", ""));
            ddlCategoryParent.DataTextField = "ITEM_CAT_NAME";
            ddlCategoryParent.DataValueField = "ITEM_CATEGORY_PARENT_ID";
            ddlCategoryParent.DataSource = DBMethod.ExecuteQuery(ItemCategory_DAL.getCategoryChild()).Tables[0];
            ddlCategoryParent.DataBind();
        }
        public static void getCategory(ref DropDownList ddlCategoryParent)
        {
            ddlCategoryParent.AppendDataBoundItems = true;
            ddlCategoryParent.Items.Clear();
            if (VMVServices.Web.Utils.LanguageCode.ToString().Trim().Length == 0)
            {

                ddlCategoryParent.Items.Add(new ListItem(FINAppConstants.intialRowTextField, ""));
            }
            else
            {
                ddlCategoryParent.Items.Add(new ListItem(FINAppConstants.intialRowTextField_OL, ""));
            }
            //ddlCategoryParent.Items.Add(new ListItem("---Select---", ""));
            ddlCategoryParent.DataTextField = "ITEM_CAT_NAME";
            ddlCategoryParent.DataValueField = "ITEM_CATEGORY_PARENT_ID";
            ddlCategoryParent.DataSource = DBMethod.ExecuteQuery(ItemCategory_DAL.getCategory()).Tables[0];
            ddlCategoryParent.DataBind();

         
        }


        //AP Item
        public static void fn_getItemCategory(ref DropDownList ddlItemCategory)
        {
            ddlItemCategory.AppendDataBoundItems = true;
            ddlItemCategory.Items.Clear();
            if (VMVServices.Web.Utils.LanguageCode.ToString().Trim().Length == 0)
            {

                ddlItemCategory.Items.Add(new ListItem(FINAppConstants.intialRowTextField, ""));
            }
            else
            {
                ddlItemCategory.Items.Add(new ListItem(FINAppConstants.intialRowTextField_OL, ""));
            }
            //ddlItemCategory.Items.Add(new ListItem(intialRowTextField, intialRowValueField));
            ddlItemCategory.DataTextField = "CATEGORY_NAME";
            ddlItemCategory.DataValueField = "CATEGORY_ID";
            ddlItemCategory.DataSource = DBMethod.ExecuteQuery(Item_DAL.getItemCategory()).Tables[0];
            ddlItemCategory.DataBind();
        }


        //AP Item
        public static void fn_getUOMDetails(ref DropDownList ddlItemUOM)
        {
            ddlItemUOM.AppendDataBoundItems = true;
            ddlItemUOM.Items.Clear();
            if (VMVServices.Web.Utils.LanguageCode.ToString().Trim().Length == 0)
            {

                ddlItemUOM.Items.Add(new ListItem(FINAppConstants.intialRowTextField, ""));
            }
            else
            {
                ddlItemUOM.Items.Add(new ListItem(FINAppConstants.intialRowTextField_OL, ""));
            }
            //ddlItemUOM.Items.Add(new ListItem("---Select---", ""));
            ddlItemUOM.DataTextField = "UOM_NAME";
            ddlItemUOM.DataValueField = "UOM_ID";
            ddlItemUOM.DataSource = DBMethod.ExecuteQuery(UOMMasters_DAL.getUOMDetails()).Tables[0];
            ddlItemUOM.DataBind();
        }


        //AP Item
        public static void fn_getItemGroup(ref DropDownList ddlItemGroup)
        {
            ddlItemGroup.AppendDataBoundItems = true;
            ddlItemGroup.Items.Clear();
            if (VMVServices.Web.Utils.LanguageCode.ToString().Trim().Length == 0)
            {

                ddlItemGroup.Items.Add(new ListItem(FINAppConstants.intialRowTextField, ""));
            }
            else
            {
                ddlItemGroup.Items.Add(new ListItem(FINAppConstants.intialRowTextField_OL, ""));
            }
            //ddlItemGroup.Items.Add(new ListItem(intialRowTextField,intialRowValueField));
            ddlItemGroup.DataTextField = "GROUP_NAME";
            ddlItemGroup.DataValueField = "GROUP_ID";
            ddlItemGroup.DataSource = DBMethod.ExecuteQuery(Item_DAL.getItemGroup()).Tables[0];
            ddlItemGroup.DataBind();
        }

        //AP Item
        public static void fn_getWeightUOM(ref DropDownList ddlWeightUOM)
        {
            ddlWeightUOM.AppendDataBoundItems = true;
            ddlWeightUOM.Items.Clear();
            if (VMVServices.Web.Utils.LanguageCode.ToString().Trim().Length == 0)
            {

                ddlWeightUOM.Items.Add(new ListItem(FINAppConstants.intialRowTextField, ""));
            }
            else
            {
                ddlWeightUOM.Items.Add(new ListItem(FINAppConstants.intialRowTextField_OL, ""));
            }
            //ddlWeightUOM.Items.Add(new ListItem("---Select---", ""));
            ddlWeightUOM.DataTextField = "UOM_NAME";
            ddlWeightUOM.DataValueField = "UOM_CODE";
            ddlWeightUOM.DataSource = DBMethod.ExecuteQuery(Item_DAL.getWeightUOM()).Tables[0];
            ddlWeightUOM.DataBind();
        }


        //AP Item
        public static void fn_getLengthUOM(ref DropDownList ddlLengthUOM)
        {
            ddlLengthUOM.AppendDataBoundItems = true;
            ddlLengthUOM.Items.Clear();
            if (VMVServices.Web.Utils.LanguageCode.ToString().Trim().Length == 0)
            {

                ddlLengthUOM.Items.Add(new ListItem(FINAppConstants.intialRowTextField, ""));
            }
            else
            {
                ddlLengthUOM.Items.Add(new ListItem(FINAppConstants.intialRowTextField_OL, ""));
            }
           // ddlLengthUOM.Items.Add(new ListItem("---Select---", ""));
            ddlLengthUOM.DataTextField = "UOM_NAME";
            ddlLengthUOM.DataValueField = "UOM_CODE";
            ddlLengthUOM.DataSource = DBMethod.ExecuteQuery(Item_DAL.getLengthUOM()).Tables[0];
            ddlLengthUOM.DataBind();
        }


        //AP Item
        public static void fn_getAreaUOM(ref DropDownList ddlAreaUOM)
        {
            ddlAreaUOM.AppendDataBoundItems = true;
            ddlAreaUOM.Items.Clear();
            if (VMVServices.Web.Utils.LanguageCode.ToString().Trim().Length == 0)
            {

                ddlAreaUOM.Items.Add(new ListItem(FINAppConstants.intialRowTextField, ""));
            }
            else
            {
                ddlAreaUOM.Items.Add(new ListItem(FINAppConstants.intialRowTextField_OL, ""));
            }
            //ddlAreaUOM.Items.Add(new ListItem("---Select---", ""));
            ddlAreaUOM.DataTextField = "UOM_NAME";
            ddlAreaUOM.DataValueField = "UOM_CODE";
            ddlAreaUOM.DataSource = DBMethod.ExecuteQuery(Item_DAL.getAreaUOM()).Tables[0];
            ddlAreaUOM.DataBind();
        }


        //AP Item
        public static void fn_getVolumeUOM(ref DropDownList ddlVolumeUOM)
        {
            ddlVolumeUOM.AppendDataBoundItems = true;
            ddlVolumeUOM.Items.Clear();
            if (VMVServices.Web.Utils.LanguageCode.ToString().Trim().Length == 0)
            {

                ddlVolumeUOM.Items.Add(new ListItem(FINAppConstants.intialRowTextField, ""));
            }
            else
            {
                ddlVolumeUOM.Items.Add(new ListItem(FINAppConstants.intialRowTextField_OL, ""));
            }
            //ddlVolumeUOM.Items.Add(new ListItem("---Select---", ""));
            ddlVolumeUOM.DataTextField = "UOM_NAME";
            ddlVolumeUOM.DataValueField = "UOM_CODE";
            ddlVolumeUOM.DataSource = DBMethod.ExecuteQuery(Item_DAL.getVolumeUOM()).Tables[0];
            ddlVolumeUOM.DataBind();
        }

        public static void fn_getWarehouseName(ref DropDownList ddlWarehouseNumber)
        {
            ddlWarehouseNumber.AppendDataBoundItems = true;
            ddlWarehouseNumber.Items.Clear();
            if (VMVServices.Web.Utils.LanguageCode.ToString().Trim().Length == 0)
            {

                ddlWarehouseNumber.Items.Add(new ListItem(FINAppConstants.intialRowTextField, ""));
            }
            else
            {
                ddlWarehouseNumber.Items.Add(new ListItem(FINAppConstants.intialRowTextField_OL, ""));
            }
            //ddlWarehouseNumber.Items.Add(new ListItem("---Select---", ""));
            ddlWarehouseNumber.DataTextField = "UOM_NAME";
            ddlWarehouseNumber.DataValueField = "UOM_CODE";
            ddlWarehouseNumber.DataSource = DBMethod.ExecuteQuery(WarehouseFacility_DAL.getWarehouseName()).Tables[0];
            ddlWarehouseNumber.DataBind();
        }


        public static void fn_getDepartment(ref DropDownList ddlDepartment)
        {
            ddlDepartment.AppendDataBoundItems = true;
            ddlDepartment.Items.Clear();
            if (VMVServices.Web.Utils.LanguageCode.ToString().Trim().Length == 0)
            {

                ddlDepartment.Items.Add(new ListItem(FINAppConstants.intialRowTextField, ""));
            }
            else
            {
                ddlDepartment.Items.Add(new ListItem(FINAppConstants.intialRowTextField_OL, ""));
            }
            //ddlDepartment.Items.Add(new ListItem("---Select---", ""));
            ddlDepartment.DataTextField = "DEPT_NAME";
            ddlDepartment.DataValueField = "DEPT_ID";
            ddlDepartment.DataSource = DBMethod.ExecuteQuery(Department_DAL.GetDepartmentDetails()).Tables[0];
            ddlDepartment.DataBind();
        }

        public static void fn_getDesignation(ref DropDownList ddlDesignation,string design_Id)
        {
            ddlDesignation.AppendDataBoundItems = true;
            ddlDesignation.Items.Clear();
            if (VMVServices.Web.Utils.LanguageCode.ToString().Trim().Length == 0)
            {

                ddlDesignation.Items.Add(new ListItem(FINAppConstants.intialRowTextField, ""));
            }
            else
            {
                ddlDesignation.Items.Add(new ListItem(FINAppConstants.intialRowTextField_OL, ""));
            }
            //ddlDesignation.Items.Add(new ListItem("---Select---", ""));
            ddlDesignation.DataTextField = "DESIG_NAME";
            ddlDesignation.DataValueField = "DESIGN_ID";
            ddlDesignation.DataSource = DBMethod.ExecuteQuery(Department_DAL.GetDesignationName(design_Id)).Tables[0];
            ddlDesignation.DataBind();
        }
        public static void fn_getDepartmentType(ref DropDownList ddlType, string depart_Id)
        {
            ddlType.AppendDataBoundItems = true;
            ddlType.Items.Clear();
            if (VMVServices.Web.Utils.LanguageCode.ToString().Trim().Length == 0)
            {

                ddlType.Items.Add(new ListItem(FINAppConstants.intialRowTextField, ""));
            }
            else
            {
                ddlType.Items.Add(new ListItem(FINAppConstants.intialRowTextField_OL, ""));
            }
            //ddlType.Items.Add(new ListItem("---Select---", ""));
            ddlType.DataTextField = "DEPT_TYPE";
            ddlType.DataValueField = "DEPT_ID";
            ddlType.DataSource = DBMethod.ExecuteQuery(Department_DAL.GetDepartmentType(depart_Id)).Tables[0];
            ddlType.DataBind();
        }

        public static void fn_Type(ref DropDownList ddlType)
        {
            ddlType.AppendDataBoundItems = true;
            ddlType.Items.Clear();
            if (VMVServices.Web.Utils.LanguageCode.ToString().Trim().Length == 0)
            {

                ddlType.Items.Add(new ListItem(FINAppConstants.intialRowTextField, ""));
            }
            else
            {
                ddlType.Items.Add(new ListItem(FINAppConstants.intialRowTextField_OL, ""));
            }
            //ddlType.Items.Add(new ListItem("---Select---", ""));
            ddlType.DataTextField = "DEPT_NAME";
            ddlType.DataValueField = "DEPT_ID";
            ddlType.DataSource = DBMethod.ExecuteQuery(Department_DAL.GetDepartmentDetails()).Tables[0];
            ddlType.DataBind();
        }

        public static void fn_getRequestDeptNReqType(ref DropDownList ddlType)
        {
            ddlType.AppendDataBoundItems = true;
            ddlType.Items.Clear();
            if (VMVServices.Web.Utils.LanguageCode.ToString().Trim().Length == 0)
            {

                ddlType.Items.Add(new ListItem(FINAppConstants.intialRowTextField, ""));
            }
            else
            {
                ddlType.Items.Add(new ListItem(FINAppConstants.intialRowTextField_OL, ""));
            }
            //ddlType.Items.Add(new ListItem("---Select---", ""));
            ddlType.DataTextField = "REQUEST";
            ddlType.DataValueField = "REQ_ID";
            ddlType.DataSource = DBMethod.ExecuteQuery(TrainingRequest_DAL.GetRequestType()).Tables[0];
            ddlType.DataBind();
        }

        public static void GetEligiblePlan(ref DropDownList ddlAcctcal)
        {
            ddlAcctcal.AppendDataBoundItems = true;
            ddlAcctcal.Items.Clear();
            if (VMVServices.Web.Utils.LanguageCode.ToString().Trim().Length == 0)
            {

                ddlAcctcal.Items.Add(new ListItem(FINAppConstants.intialRowTextField, ""));
            }
            else
            {
                ddlAcctcal.Items.Add(new ListItem(FINAppConstants.intialRowTextField_OL, ""));
            }
            //ddlAcctcal.Items.Add(new ListItem("---Select---", ""));
            ddlAcctcal.DataTextField = "ELIGIBLE_PLAN_LEVEL";
            ddlAcctcal.DataValueField = "ELIGIBLE_PLAN_ID";
            ddlAcctcal.DataSource = DBMethod.ExecuteQuery(Organisation_DAL.GetEligiblePlan()).Tables[0];
            ddlAcctcal.DataBind();
        }

        public static void GetSelectedPlan(ref DropDownList ddlAcctcal)
        {
            ddlAcctcal.AppendDataBoundItems = true;
            ddlAcctcal.Items.Clear();
            if (VMVServices.Web.Utils.LanguageCode.ToString().Trim().Length == 0)
            {

                ddlAcctcal.Items.Add(new ListItem(FINAppConstants.intialRowTextField, ""));
            }
            else
            {
                ddlAcctcal.Items.Add(new ListItem(FINAppConstants.intialRowTextField_OL, ""));
            }
           // ddlAcctcal.Items.Add(new ListItem("---Select---", ""));
            ddlAcctcal.DataTextField = "SELECTED_PLAN_LEVEL";
            ddlAcctcal.DataValueField = "SELECTED_PLAN_ID";
            ddlAcctcal.DataSource = DBMethod.ExecuteQuery(Organisation_DAL.GetSelectedPlan()).Tables[0];
            ddlAcctcal.DataBind();
        }

        #region Fixed Assets
        //Major Category
        public static void fn_MajorCategory(ref DropDownList ddl_MajorCategory)
        {
            ddl_MajorCategory.AppendDataBoundItems = true;
            ddl_MajorCategory.Items.Clear();
            if (VMVServices.Web.Utils.LanguageCode.ToString().Trim().Length == 0)
            {

                ddl_MajorCategory.Items.Add(new ListItem(FINAppConstants.intialRowTextField, ""));
            }
            else
            {
                ddl_MajorCategory.Items.Add(new ListItem(FINAppConstants.intialRowTextField_OL, ""));
            }
            //ddl_MajorCategory.Items.Add(new ListItem("---Select---", ""));
            ddl_MajorCategory.DataTextField = FINColumnConstants.CATEGORY_NAME;
            ddl_MajorCategory.DataValueField = FINColumnConstants.MAJOR_CATEGORY_ID;

            using (IRepository<AST_MAJOR_CATEGORY_MST> userCtx = new DataRepository<AST_MAJOR_CATEGORY_MST>())
            {
                ddl_MajorCategory.DataSource = userCtx.GetAll();
                ddl_MajorCategory.DataBind();
            }
        }
        //Minor Category
        public static void fn_MinorCategory(ref DropDownList ddl_MinorCategory)
        {
            ddl_MinorCategory.AppendDataBoundItems = true;
            ddl_MinorCategory.Items.Clear();
            if (VMVServices.Web.Utils.LanguageCode.ToString().Trim().Length == 0)
            {

                ddl_MinorCategory.Items.Add(new ListItem(FINAppConstants.intialRowTextField, ""));
            }
            else
            {
                ddl_MinorCategory.Items.Add(new ListItem(FINAppConstants.intialRowTextField_OL, ""));
            }
            //ddl_MinorCategory.Items.Add(new ListItem("---Select---", ""));
            ddl_MinorCategory.DataTextField = FINColumnConstants.MINOR_CATEGORY_NAME;
            ddl_MinorCategory.DataValueField = FINColumnConstants.MINOR_CATEGORY_ID;

            using (IRepository<AST_MINOR_CATEGORY_DTL> userCtx = new DataRepository<AST_MINOR_CATEGORY_DTL>())
            {
                ddl_MinorCategory.DataSource = userCtx.GetAll();
                ddl_MinorCategory.DataBind();
            }
        }

        public static void GetInitialamtseperator(ref DropDownList ddl_InitialAmtSep)
        {
            ddl_InitialAmtSep.AppendDataBoundItems = true;
            ddl_InitialAmtSep.Items.Clear();
            if (VMVServices.Web.Utils.LanguageCode.ToString().Trim().Length == 0)
            {

                ddl_InitialAmtSep.Items.Add(new ListItem(FINAppConstants.intialRowTextField, ""));
            }
            else
            {
                ddl_InitialAmtSep.Items.Add(new ListItem(FINAppConstants.intialRowTextField_OL, ""));
            }
           // ddl_InitialAmtSep.Items.Add(new ListItem("---Select---", ""));
            ddl_InitialAmtSep.DataTextField = FINColumnConstants.INITIAL_AMT_SEP_NAME;
            ddl_InitialAmtSep.DataValueField = FINColumnConstants.INITIAL_AMT_SEP_ID;


            ddl_InitialAmtSep.DataSource = DBMethod.ExecuteQuery(FA_SQL.GetCurrencyInitial()).Tables[0];
            ddl_InitialAmtSep.DataBind();

        }

        public static void GetSubsequentamtseperator(ref DropDownList ddl_SubAmtSep)
        {
            ddl_SubAmtSep.AppendDataBoundItems = true;
            ddl_SubAmtSep.Items.Clear();
            if (VMVServices.Web.Utils.LanguageCode.ToString().Trim().Length == 0)
            {

                ddl_SubAmtSep.Items.Add(new ListItem(FINAppConstants.intialRowTextField, ""));
            }
            else
            {
                ddl_SubAmtSep.Items.Add(new ListItem(FINAppConstants.intialRowTextField_OL, ""));
            }
            //ddl_SubAmtSep.Items.Add(new ListItem("---Select---", ""));
            ddl_SubAmtSep.DataTextField = FINColumnConstants.SUBSEQUENT_AMT_SEP_NAME;
            ddl_SubAmtSep.DataValueField = FINColumnConstants.SUBSEQUENT_AMT_SEP_ID;


            ddl_SubAmtSep.DataSource = DBMethod.ExecuteQuery(FA_SQL.GetCurrencySubsequent()).Tables[0];
            ddl_SubAmtSep.DataBind();

        }

        public static void GetDepreciationName(ref DropDownList ddlName)
        {
            ddlName.AppendDataBoundItems = true;

            ddlName.Items.Clear();
            if (VMVServices.Web.Utils.LanguageCode.ToString().Trim().Length == 0)
            {

                ddlName.Items.Add(new ListItem(FINAppConstants.intialRowTextField, ""));
            }
            else
            {
                ddlName.Items.Add(new ListItem(FINAppConstants.intialRowTextField_OL, ""));
            }
           // ddlName.Items.Add(new ListItem("---Select---", ""));
            ddlName.DataTextField = FINColumnConstants.DPRN_NAME;
            //ddlName.DataValueField = FINColumnConstants.DPRN_METHOD_MST_ID;
            ddlName.DataValueField = FINColumnConstants.DPRN_NAME;
            ddlName.DataSource = DBMethod.ExecuteQuery(FA_SQL.GetDprnName()).Tables[0];
            ddlName.DataBind();
            //using (IRepository<AST_DEPRECIATION_METHOD_MST> userCtx = new DataRepository<AST_DEPRECIATION_METHOD_MST>())
            //{
            //    ddlName.DataSource = userCtx.GetAll();
            //    ddlName.DataBind();
            //}
        }




        #endregion
    }
}