﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.BLL;
using FIN.BLL.CA;
using FIN.DAL.CA;
using VMVServices.Web;
using Excel = Microsoft.Office.Interop.Excel;


namespace FIN.Client.CA
{
    public partial class BankStatementUploadEntry : PageBase
    {

        CA_BANK_STATEMENT_UPLOAD_HDR cA_BANK_STATEMENT_UPLOAD_HDR = new CA_BANK_STATEMENT_UPLOAD_HDR();
        CA_BANK_STATEMENT_UPLOAD_DTL cA_BANK_STATEMENT_UPLOAD_DTL = new CA_BANK_STATEMENT_UPLOAD_DTL();
        DataTable dtGridData = new DataTable();
        Boolean bol_rowVisiable;
        Boolean savedBool;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    Session[FINSessionConstants.Attachments] = null;
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("GL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();

            //if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            //{
            //    VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
            //}

            //if (VMVServices.Web.Utils.AddAllowed != FINAppConstants.Y && Master.Mode == FINAppConstants.Add)
            //{
            //    btnSave.Visible = false;
            //}

        }

        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }


        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();
                EntityData = null;
                Session[FINSessionConstants.GridData] = null;


                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    using (IRepository<CA_BANK_STATEMENT_UPLOAD_HDR> userCtx = new DataRepository<CA_BANK_STATEMENT_UPLOAD_HDR>())
                    {
                        cA_BANK_STATEMENT_UPLOAD_HDR = userCtx.Find(r =>
                            (r.STATEMENT_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }
                    //cA_BANK_STATEMENT_UPLOAD_HDR = FIN.BLL.CA.Cheque_BLL.getClassEntity(Master.StrRecordId);

                    EntityData = cA_BANK_STATEMENT_UPLOAD_HDR;

                    ddlBankName.SelectedValue = cA_BANK_STATEMENT_UPLOAD_HDR.BANK_ID;
                    FillBankShortNameDetails();
                  
                    ddlBranchName.SelectedValue = cA_BANK_STATEMENT_UPLOAD_HDR.BRANCH_ID;
                    FillBranchShortNameDetails();
                    ddlAccountNumber.SelectedValue = cA_BANK_STATEMENT_UPLOAD_HDR.BANK_ACCOUNT_NUMBER;


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {



                ErrorCollection.Clear();
                if (Session[FINSessionConstants.Attachments] == null)
                {
                    ErrorCollection.Add("Please Attach File", "Please Attach Excel File ");
                    return;
                }
                if (EntityData != null)
                {
                    cA_BANK_STATEMENT_UPLOAD_HDR = (CA_BANK_STATEMENT_UPLOAD_HDR)EntityData;
                }

                System.Collections.SortedList fu_tmp = (System.Collections.SortedList)Session[FINSessionConstants.Attachments];


                cA_BANK_STATEMENT_UPLOAD_HDR.BANK_ID = ddlBankName.SelectedValue.ToString();
                cA_BANK_STATEMENT_UPLOAD_HDR.BANK_NAME = ddlBankName.SelectedItem.Text;
                cA_BANK_STATEMENT_UPLOAD_HDR.BRANCH_ID = ddlBranchName.SelectedValue.ToString();
                cA_BANK_STATEMENT_UPLOAD_HDR.BRANCH_NAME = ddlBranchName.SelectedItem.Text;
                cA_BANK_STATEMENT_UPLOAD_HDR.BANK_ACCOUNT_NUMBER = ddlAccountNumber.SelectedValue.ToString();

                cA_BANK_STATEMENT_UPLOAD_HDR.STATEMENT_ORG_ID = VMVServices.Web.Utils.OrganizationID;
                cA_BANK_STATEMENT_UPLOAD_HDR.STATEMENT_FILE_NAME = fu_tmp.GetByIndex(2).ToString() + "." + fu_tmp.GetByIndex(1).ToString();

                cA_BANK_STATEMENT_UPLOAD_HDR.STATEMENT_FILE_PATH = fu_tmp.GetByIndex(0).ToString();

                cA_BANK_STATEMENT_UPLOAD_HDR.STATEMENT_ID = FINSP.GetSPFOR_SEQCode("CA_005_M".ToString(), false, true);
                cA_BANK_STATEMENT_UPLOAD_HDR.CREATED_BY = this.LoggedUserName;
                cA_BANK_STATEMENT_UPLOAD_HDR.CREATED_DATE = DateTime.Today;


                cA_BANK_STATEMENT_UPLOAD_HDR.ENABLED_FLAG = FINAppConstants.Y;
                cA_BANK_STATEMENT_UPLOAD_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, cA_BANK_STATEMENT_UPLOAD_HDR.STATEMENT_ID);



                DataTable dt_data = DBMethod.ExecuteQuery(FIN.DAL.CA.BankStatementTemplate_DAL.getBankTemplateDetails4Name(ddlTemplateName.SelectedValue.ToString())).Tables[0];





                Excel.Application xlApp;
                Excel.Workbook xlWorkBook;
                Excel.Worksheet xlWorkSheet;
                Excel.Range range;


                int rCnt = 0;

                xlApp = new Excel.Application();


                string existingFile = Server.MapPath("~/UploadFile/DocFIle/") + fu_tmp.GetByIndex(0).ToString() + "." + fu_tmp.GetByIndex(1).ToString();


                xlWorkBook = xlApp.Workbooks.Open(existingFile, 0, true, 5, "", "", true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
                xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);

                range = xlWorkSheet.UsedRange;




                DataRow[] dr = dt_data.Select("BR_COL_NAME='STATEMENTNUMBER'");
                if (dr.Length > 0)
                {
                    cA_BANK_STATEMENT_UPLOAD_HDR.STATEMENT_NUMBER = Convert.ToString((range.Cells[dr[0]["BR_ROW_NUMBER"].ToString(), dr[0]["BR_COL_NUMBER"].ToString()] as Excel.Range).Value2);
                }

                dr = dt_data.Select("BR_COL_NAME='STATEMENTDATE'");
                if (dr.Length > 0)
                {
                    try
                    {
                        cA_BANK_STATEMENT_UPLOAD_HDR.STATEMENT_DATE = DateTime.Parse(Convert.ToString((range.Cells[dr[0]["BR_ROW_NUMBER"].ToString(), dr[0]["BR_COL_NUMBER"].ToString()] as Excel.Range).Value2));
                    }
                    catch (Exception ex1) { }
                }

                dr = dt_data.Select("BR_COL_NAME='TOTALAMOUNT'");
                if (dr.Length > 0)
                {
                    try
                    {
                        cA_BANK_STATEMENT_UPLOAD_HDR.STATEMENT_AMT = decimal.Parse(Convert.ToString((range.Cells[dr[0]["BR_ROW_NUMBER"].ToString(), dr[0]["BR_COL_NUMBER"].ToString()] as Excel.Range).Value2));
                    }
                    catch (Exception ex2) { }
                }


                int count = 0;


                int int_startRow = int.Parse(dr[0]["BR_ROW_NUMBER"].ToString()) + 1;
                string str_slno = "";
                string str_trans_date = "";
                string str_Remarks = "";
                string str_cheno = "";
                string str_crdr = "";
                string str_amt = "";

                dr = dt_data.Select("BR_COL_NAME='SLNO'");
                if (dr.Length > 0)
                {
                    str_slno = dr[0]["BR_COL_NUMBER"].ToString();
                }

                dr = dt_data.Select("BR_COL_NAME='TRANSACTIONDATE'");
                if (dr.Length > 0)
                {
                    str_trans_date = dr[0]["BR_COL_NUMBER"].ToString();
                }
                dr = dt_data.Select("BR_COL_NAME='REMARKS'");
                if (dr.Length > 0)
                {
                    str_Remarks = dr[0]["BR_COL_NUMBER"].ToString();
                }
                dr = dt_data.Select("BR_COL_NAME='CHEQUENUMBER'");
                if (dr.Length > 0)
                {
                    str_cheno = dr[0]["BR_COL_NUMBER"].ToString();
                }

                dr = dt_data.Select("BR_COL_NAME='CRDR'");
                if (dr.Length > 0)
                {
                    str_crdr = dr[0]["BR_COL_NUMBER"].ToString();
                }

                dr = dt_data.Select("BR_COL_NAME='AMOUNT'");
                if (dr.Length > 0)
                {
                    str_amt = dr[0]["BR_COL_NUMBER"].ToString();
                }

                var tmpChildEntity = new List<Tuple<object, string>>();
                int_startRow = 9;
                for (rCnt = int_startRow; rCnt <= range.Rows.Count; rCnt++)
                {


                    string str_slno_data = "";
                    string str_trans_date_data = "";
                    string str_Remarks_data = "";
                    string str_cheno_data = "";
                    string str_crdr_data = "";
                    string str_amt_data = "";

                    str_slno_data = Convert.ToString((range.Cells[rCnt, str_slno] as Excel.Range).Value2);


                    if (str_slno_data != null)
                    {

                        str_trans_date_data = Convert.ToString((range.Cells[rCnt, str_trans_date] as Excel.Range).Value2);
                        str_Remarks_data = Convert.ToString((range.Cells[rCnt, str_Remarks] as Excel.Range).Value2);
                        str_cheno_data = Convert.ToString((range.Cells[rCnt, str_cheno] as Excel.Range).Value2);
                        str_crdr_data = Convert.ToString((range.Cells[rCnt, str_crdr] as Excel.Range).Value2);
                        str_amt_data = Convert.ToString((range.Cells[rCnt, str_amt] as Excel.Range).Value2);

                        cA_BANK_STATEMENT_UPLOAD_DTL = new CA_BANK_STATEMENT_UPLOAD_DTL();

                        cA_BANK_STATEMENT_UPLOAD_DTL.STATEMENT_ID = cA_BANK_STATEMENT_UPLOAD_HDR.STATEMENT_ID;
                        cA_BANK_STATEMENT_UPLOAD_DTL.STATEMENT_LINE_NUM = decimal.Parse(str_slno_data);
                        try
                        {
                            cA_BANK_STATEMENT_UPLOAD_DTL.TXN_DATE = DateTime.Parse(str_trans_date_data);
                        }
                        catch (Exception ex3)
                        { }

                        cA_BANK_STATEMENT_UPLOAD_DTL.TXN_CHEQUE_NO = str_cheno_data;

                        cA_BANK_STATEMENT_UPLOAD_DTL.AMOUNT = decimal.Parse(str_amt_data);

                        cA_BANK_STATEMENT_UPLOAD_DTL.ENABLED_FLAG = FINAppConstants.Y;
                        cA_BANK_STATEMENT_UPLOAD_DTL.WORKFLOW_COMPLETION_STATUS = FINAppConstants.Y;
                        cA_BANK_STATEMENT_UPLOAD_DTL.CREATED_BY = this.LoggedUserName;
                        cA_BANK_STATEMENT_UPLOAD_DTL.CREATED_DATE = DateTime.Today;

                        cA_BANK_STATEMENT_UPLOAD_DTL.STATEMENT_DTL_ID = FINSP.GetSPFOR_SEQCode("CA_005_D".ToString(), false, true);

                        tmpChildEntity.Add(new Tuple<object, string>(cA_BANK_STATEMENT_UPLOAD_DTL, FINAppConstants.Add));

                    }
                    else
                    {
                        count = count + 1;
                        if (count > 10)
                        {
                            break;
                        }
                    }



                }

                xlWorkBook.Close(true, null, null);
                xlApp.Quit();

                //Save Detail Table              

                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            CommonUtils.SavePCEntity<CA_BANK_STATEMENT_UPLOAD_HDR, CA_BANK_STATEMENT_UPLOAD_DTL>(cA_BANK_STATEMENT_UPLOAD_HDR, tmpChildEntity, cA_BANK_STATEMENT_UPLOAD_DTL);
                            savedBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {
                            CommonUtils.SavePCEntity<CA_BANK_STATEMENT_UPLOAD_HDR, CA_BANK_STATEMENT_UPLOAD_DTL>(cA_BANK_STATEMENT_UPLOAD_HDR, tmpChildEntity, cA_BANK_STATEMENT_UPLOAD_DTL, true);
                            savedBool = true;
                            break;
                        }
                }
                if (ErrorCollection.Count > 0)
                {
                    return;
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        private void FillComboBox()
        {
            Bank_BLL.fn_getBankName(ref ddlBankName);
            BankStatementTemplate_BLL.fn_getTemplateName(ref ddlTemplateName);
        }


        protected void ddlBankName_SelectedIndexChanged(object sender, EventArgs e)
        {
            {
                try
                {
                    ErrorCollection.Clear();

                    FillBankShortNameDetails();


                }
                catch (Exception ex)
                {
                    ErrorCollection.Add("POR", ex.Message);
                }
                finally
                {
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                    }
                }
            }
        }



        private void FillBankShortNameDetails()
        {
            DataTable dtgrndata = new DataTable();

            dtgrndata = Bank_BLL.fn_getBankShortName(ddlBankName.SelectedValue.ToString());

            if (dtgrndata != null)
            {
                if (dtgrndata.Rows.Count > 0)
                {
                    txtBankShortName.Text = dtgrndata.Rows[0]["BANK_SHORT_NAME"].ToString();
                }
            }
            BankBranch_BLL.fn_getBranchName(ref ddlBranchName, ddlBankName.SelectedValue);



        }



        protected void ddlBranchName_SelectedIndexChanged(object sender, EventArgs e)
        {
            {
                try
                {
                    ErrorCollection.Clear();

                    FillBranchShortNameDetails();



                }
                catch (Exception ex)
                {
                    ErrorCollection.Add("POR", ex.Message);
                }
                finally
                {
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                    }
                }
            }
        }



        private void FillBranchShortNameDetails()
        {
            DataTable dtgrndata = new DataTable();

            dtgrndata = BankBranch_BLL.fn_getBankBranchShortName(ddlBranchName.SelectedValue.ToString());

            if (dtgrndata != null)
            {
                if (dtgrndata.Rows.Count > 0)
                {
                    txtBranchShortName.Text = dtgrndata.Rows[0]["ATTRIBUTE2"].ToString();
                }
            }
            Cheque_BLL.fn_getAccountNumber(ref ddlAccountNumber, ddlBankName.SelectedValue, ddlBranchName.SelectedValue);

        }





        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();

                DBMethod.DeleteEntity<CA_BANK_STATEMENT_UPLOAD_HDR>(cA_BANK_STATEMENT_UPLOAD_HDR);
                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATADELETED);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ChequeEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                AssignToBE();

                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);
                }
                //switch (Master.Mode)
                //{
                //    case FINAppConstants.Add:
                //        {
                //            DBMethod.SaveEntity<GL_ACCT_GROUP_LINK_HDR>(gL_ACCT_GROUP_LINK_HDR);
                //            DisplaySaveCompleteMessage(Master.ListPageToOpen);
                //            break;
                //        }
                //    case FINAppConstants.Update:
                //        {

                //            DBMethod.SaveEntity<GL_ACCT_GROUP_LINK_HDR>(gL_ACCT_GROUP_LINK_HDR, true);
                //            DisplaySaveCompleteMessage(Master.ListPageToOpen);
                //            break;

                //        }
                //}
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void btnSelFile_Click(object sender, EventArgs e)
        {
            mpeExcelFileUpload.Show();
        }

        protected void btnCloseImage_Click(object sender, EventArgs e)
        {
            if (Session[FINSessionConstants.Attachments] != null)
            {
                System.Collections.SortedList fu_tmp = (System.Collections.SortedList)Session[FINSessionConstants.Attachments];

                //imgPatientFoto.ImageUrl = "~/UploadFile/Captures/" + fu_tmp.GetByIndex(0).ToString() + "." + fu_tmp.GetByIndex(1).ToString();
                lblSelFile.Text = fu_tmp.GetByIndex(2).ToString() + "." + fu_tmp.GetByIndex(1).ToString();

            }
        }

    }
        #endregion
}