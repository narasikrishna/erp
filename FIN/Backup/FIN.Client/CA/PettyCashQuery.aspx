﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="PettyCashQuery.aspx.cs" Inherits="FIN.Client.CA.PettyCashQuery" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 700px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 100px" id="lblUserCodes">
                User ID
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 152px">
                <asp:DropDownList ID="ddlUserCodes" runat="server" TabIndex="1" Width="150px" CssClass="validate[required] RequiredField ddlStype"
                    OnSelectedIndexChanged="ddlUserCodes_SelectedIndexChanged" AutoPostBack="true">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox  LNOrient" style=" width: 98px" id="Div1">
                User Name
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtUserName" CssClass="txtBox" MaxLength="100" Enabled="false" runat="server"
                    TabIndex="2"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 100px" id="lblFromDate">
                From Date
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtFromDate" CssClass="validate[required, custom[ReqDateDDMMYYY],dateRange[dg1]]  RequiredField txtBox"
                    runat="server" TabIndex="3"></asp:TextBox>
                <cc2:CalendarExtender ID="CalendarExtender8" runat="server" Format="dd/MM/yyyy" TargetControlID="txtFromDate"
                    OnClientDateSelectionChanged="checkDate">
                </cc2:CalendarExtender>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtFromDate" />
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox  LNOrient" style=" width: 100px" id="lblEndDate">
                To Date
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtEndDate" CssClass="validate[required, custom[ReqDateDDMMYYY],dateRange[dg1]] RequiredField txtBox"
                    runat="server" TabIndex="4"></asp:TextBox>
                <cc2:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MM/yyyy" TargetControlID="txtEndDate"
                    OnClientDateSelectionChanged="checkDate">
                </cc2:CalendarExtender>
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtEndDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="btn" style="float: right; width: 210px" id="divBtn">
                <asp:ImageButton ID="btnQuery" runat="server" ImageUrl="~/Images/btnQuery.png"
                    OnClick="btnQuery_Click" CssClass="btn" Style="border: 0px" TabIndex="5" />
                <%--<asp:Button runat="server" CssClass="btn" ID="btnQuery" OnClick="btnQuery_Click" TabIndex="5"
                    Text="Query" />--%>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer " align="left">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                Width="300px" ShowFooter="True">
                <Columns>
                    <asp:TemplateField HeaderText="Account Detail">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtAccDtls" Width="180px" MaxLength="250" runat="server" CssClass="txtBox"
                                Enabled="false" Text='<%# Eval("ACCT_CODE") %>' TabIndex="6"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtAccDtls" Width="180px" MaxLength="250" runat="server" CssClass="txtBox"
                                Enabled="false" TabIndex="6"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblType" CssClass="adminFormFieldHeading" runat="server" Text='<%# Eval("ACCT_CODE") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Remarks">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtRemarks" Width="180px" MaxLength="250" runat="server" CssClass="txtBox"
                                Enabled="false" Text='<%# Eval("GL_PCE_REMARKS") %>' TabIndex="7"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtRemarks" Width="180px" MaxLength="250" runat="server" CssClass="txtBox"
                                Enabled="false" TabIndex="7"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblGroupName1" CssClass="adminFormFieldHeading" runat="server" Text='<%# Eval("GL_PCE_REMARKS") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Amount">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtExpenditureAmount" Width="180px" MaxLength="250" runat="server"
                                Enabled="false" CssClass="txtBox" Text='<%# Eval("GL_EXP_AMOUNT") %>' TabIndex="8"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtExpenditureAmount" Width="180px" MaxLength="250" runat="server"
                                Enabled="false" CssClass="txtBox" TabIndex="8"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblGroupName2" CssClass="adminFormFieldHeading" runat="server" Text='<%# Eval("GL_EXP_AMOUNT") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Posted">
                        <EditItemTemplate>
                            <asp:CheckBox ID="chkPosted" Enabled="false" runat="server" TabIndex="9" Width="30px"
                                Checked='<%# Convert.ToBoolean(Eval("GL_PCE_POSTED")) %>' />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:CheckBox ID="chkPosted" Enabled="false" runat="server" TabIndex="9" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkPosted" Enabled="false" runat="server" Width="30px" TabIndex="9"
                                Checked='<%# Convert.ToBoolean(Eval("GL_PCE_POSTED")) %>' />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" TabIndex="10" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="11" />
                    </td>
                    <td>
                        <asp:Button ID="Button3" runat="server" Text="Cancel" CssClass="btn" TabIndex="12" />
                    </td>
                    <td>
                        <asp:Button ID="Button4" runat="server" Text="Back" CssClass="btn" TabIndex="13" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table width="100%">
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" CssClass="button" ID="btnYes" Text="Yes" Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/CA/CAChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () { $("#form1").validationEngine(); });
        $("#FINContent_btnSave").click(function () {
            if ($("#form1").validationEngine('validate') == false)
                return false;
        });
    </script>
</asp:Content>
