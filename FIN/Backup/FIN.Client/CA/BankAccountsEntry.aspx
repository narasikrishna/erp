﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="BankAccountsEntry.aspx.cs" Inherits="FIN.Client.CA.BankAccountsEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 800px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblBankName">
                Bank Name
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 155px">
                <asp:DropDownList ID="ddlBankName" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlBankName_SelectedIndexChanged"
                    CssClass="validate[required] RequiredField ddlStype" TabIndex="1">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox  LNOrient" style=" width: 195px" id="lblBankShortName">
                Bank Short Name
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtBankShortName1" Enabled="false" CssClass="validate[] RequiredField txtBox"
                    MaxLength="10" runat="server" TabIndex="2"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblBranchName">
                Branch Name
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 155px">
                <asp:DropDownList ID="ddlBranchName" TabIndex="3" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    AutoPostBack="True" OnSelectedIndexChanged="ddlBranchName_SelectedIndexChanged1">
                    <asp:ListItem>--- Select ---</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox  LNOrient" style=" width: 195px" id="lblShortName">
                Branch Short Name
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtShortName" CssClass="validate[required] RequiredField txtBox"
                    MaxLength="10" runat="server" TabIndex="4"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblSWIFTCode">
                SWIFT Code
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 152px">
                <asp:TextBox ID="txtSWIFTCode" MaxLength="50" CssClass="validate[required] RequiredField txtBox"
                    TabIndex="5" runat="server"></asp:TextBox>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox  LNOrient" style=" width: 197px" id="lblIFSCCode">
                IFSC Code
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtIFSCCode" CssClass="validate[required] RequiredField txtBox"
                    MaxLength="50" runat="server" TabIndex="6"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblAccountType">
                Account Type
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 155px">
                <asp:DropDownList ID="ddlAccountType" runat="server" TabIndex="7" CssClass="validate[required] RequiredField ddlStype">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox  LNOrient" style=" width: 195px" id="lblAccountNumber">
                Account Number
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtAccountNumber" TabIndex="8" MaxLength="50" CssClass="validate[required] RequiredField txtBox"
                    runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 200px" id="Div5">
                GL Account
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 155px">
                <asp:DropDownList ID="ddlGLAccount" runat="server" TabIndex="9" CssClass="ddlStype">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox  LNOrient" style=" width: 195px" id="Div1">
                Currency
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 152px">
                <asp:DropDownList ID="ddlcurrency" runat="server" TabIndex="9" CssClass="ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 200px" id="Div2">
                Multi Currency Allowed
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:CheckBox ID="chkMultiCurrency" runat="server" TabIndex="10" Checked="true" />
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblActive">
                Active
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:CheckBox ID="chkActive" runat="server" TabIndex="11" Checked="true" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                            TabIndex="12" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="13" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="14" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="15" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table>
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/CA/CAChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
