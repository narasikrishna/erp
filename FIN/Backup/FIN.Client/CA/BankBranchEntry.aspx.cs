﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.BLL;
using FIN.DAL.CA;
using FIN.BLL.CA;
using FIN.BLL.AP;
using FIN.DAL.AP;
using VMVServices.Web;

namespace FIN.Client.CA
{
    public partial class BankBranch_Entry : PageBase
    {
        CA_BANK_BRANCH cA_BANK_BRANCH = new CA_BANK_BRANCH();
        string ProReturn = null;
        Boolean savedBool;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
            }

            if (VMVServices.Web.Utils.AddAllowed != FINAppConstants.Y && Master.Mode == FINAppConstants.Add)
            {
                btnSave.Visible = false;
            }

        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();

                EntityData = null;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    using (IRepository<CA_BANK_BRANCH> userCtx = new DataRepository<CA_BANK_BRANCH>())
                    {
                        cA_BANK_BRANCH = userCtx.Find(r =>
                            (r.BANK_BRANCH_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = cA_BANK_BRANCH;

                    ddlBankName.SelectedValue = cA_BANK_BRANCH.BANK_ID;
                    FillLineNumberDetails();
                    //ddlBankShortName.SelectedValue = cA_BANK_BRANCH.ATTRIBUTE1;
                    txtBranchNameEn.Text = cA_BANK_BRANCH.BANK_BRANCH_NAME;
                    txtBranchNameAr.Text = cA_BANK_BRANCH.BANK_BRANCH_NAME_OL;
                    txtShortName.Text = cA_BANK_BRANCH.ATTRIBUTE2;
                    txtSWIFTCode.Text = cA_BANK_BRANCH.BANK_SWIFT_CODE;
                    txtIFSCCode.Text = cA_BANK_BRANCH.BANK_IFSC_CODE;
                    txtAddress1.Text = cA_BANK_BRANCH.BANK_BRANCH_ADD1;
                    txtAddress2.Text = cA_BANK_BRANCH.BANK_BRANCH_ADD2;
                    txtAddress3.Text = cA_BANK_BRANCH.BANK_BRANCH_ADD3;
                   txtPostalCode.Text = cA_BANK_BRANCH.BANK_BRANCH_ZIP_CODE;
                    txtPhone.Text = cA_BANK_BRANCH.BANK_BRANCH_PHONE;
                    ddlCountry.SelectedValue = cA_BANK_BRANCH.BANK_BRANCH_COUNTRY;
                    FILLstate();
                    ddlState.SelectedValue = cA_BANK_BRANCH.BANK_BRANCH_STATE;
                    fillcity();
                    ddlCity.SelectedValue = cA_BANK_BRANCH.BANK_BRANCH_CITY;

                    if (cA_BANK_BRANCH.ENABLED_FLAG == FINAppConstants.EnabledFlag)
                    {
                        chkActive.Checked = true;
                    }
                    else
                    {
                        chkActive.Checked = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>

        private void FillComboBox()
        {
            Bank_BLL.fn_getBankName(ref ddlBankName);
            Supplier_BLL.fn_getCountry(ref ddlCountry);


        }

        protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            FILLstate();
        }

        private void FILLstate()
        {
            Supplier_BLL.fn_getState(ref ddlState, ddlCountry.SelectedValue);
        }

        protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
        {
            fillcity();
        }
        private void fillcity()
        {
            Supplier_BLL.fn_getCity(ref ddlCity, ddlState.SelectedValue);
        }


        protected void ddlBankName_SelectedIndexChanged(object sender, EventArgs e)
        {
            {
                try
                {
                    ErrorCollection.Clear();

                    FillLineNumberDetails();

                }
                catch (Exception ex)
                {
                    ErrorCollection.Add("POR", ex.Message);
                }
                finally
                {
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                    }
                }
            }
        }



        private void FillLineNumberDetails()
        {
            DataTable dtgrndata = new DataTable();

            dtgrndata = Bank_BLL.fn_getBankShortName(ddlBankName.SelectedValue.ToString());

            if (dtgrndata != null)
                if (dtgrndata.Rows.Count > 0)

                    txtBankShortName.Text = dtgrndata.Rows[0]["BANK_SHORT_NAME"].ToString();

        }





        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    cA_BANK_BRANCH = (CA_BANK_BRANCH)EntityData;
                }
                cA_BANK_BRANCH.BANK_ID = ddlBankName.SelectedValue;
                //cA_BANK_BRANCH.ATTRIBUTE1 = txtBankShortName.SelectedValue;
                cA_BANK_BRANCH.BANK_BRANCH_NAME = txtBranchNameEn.Text;
                cA_BANK_BRANCH.BANK_BRANCH_NAME_OL = txtBranchNameAr.Text;
                cA_BANK_BRANCH.ATTRIBUTE2 = txtShortName.Text;
                cA_BANK_BRANCH.BANK_IFSC_CODE = txtIFSCCode.Text;
                cA_BANK_BRANCH.BANK_SWIFT_CODE = txtSWIFTCode.Text;
                cA_BANK_BRANCH.BANK_BRANCH_ADD1 = txtAddress1.Text;
                cA_BANK_BRANCH.BANK_BRANCH_ADD2 = txtAddress2.Text;
                cA_BANK_BRANCH.BANK_BRANCH_ADD3 = txtAddress3.Text;
                cA_BANK_BRANCH.BANK_BRANCH_COUNTRY = ddlCountry.SelectedValue;
                cA_BANK_BRANCH.BANK_BRANCH_STATE = ddlState.SelectedValue;
                cA_BANK_BRANCH.BANK_BRANCH_CITY = ddlCity.SelectedValue;
                cA_BANK_BRANCH.BANK_BRANCH_ZIP_CODE = txtPostalCode.Text;
                cA_BANK_BRANCH.BANK_BRANCH_PHONE = txtPhone.Text;
                //cA_BANK_BRANCH.ORG_ID = VMVServices.Web.Utils.OrganizationID;
                cA_BANK_BRANCH.ENABLED_FLAG = chkActive.Checked == true ? FINAppConstants.EnabledFlag : FINAppConstants.DisabledFlag;
                

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    cA_BANK_BRANCH.MODIFIED_BY = this.LoggedUserName;
                    cA_BANK_BRANCH.MODIFIED_DATE = DateTime.Today;

                }
                else
                {
                    cA_BANK_BRANCH.BANK_BRANCH_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.CA_002.ToString(), false, true);
                    cA_BANK_BRANCH.CREATED_BY = this.LoggedUserName;
                    cA_BANK_BRANCH.CREATED_DATE = DateTime.Today;

                }

                cA_BANK_BRANCH.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, cA_BANK_BRANCH.BANK_BRANCH_ID);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }



        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                System.Collections.SortedList slControls = new System.Collections.SortedList();

                slControls[0] = ddlBankName;
                slControls[1] = txtBranchNameEn;
                slControls[2] = txtShortName;
                slControls[3] = txtSWIFTCode;
                slControls[4] = txtIFSCCode;

                Dictionary<string, string> Prop_File_Data;
                Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/CA_" + Session["Sel_Lng"].ToString() + ".properties"));
                ErrorCollection.Clear();
                string strCtrlTypes = "DropDownList~TextBox~TextBox~TextBox~TextBox";
                string strMessage = Prop_File_Data["Bank_Name_P"] + " ~ " + Prop_File_Data["Branch_Name_P"] + " ~ " + Prop_File_Data["Short_Name_P"] +  " ~ " + Prop_File_Data["SWIFT_Code_P"] + " ~ " + Prop_File_Data["IFSC_Code_P"] +  "";
                //string strMessage = "Bank Name ~ Branch Name ~ Short Name ~ SWIFT Code ~ IFSC Code";

                EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

                if (EmptyErrorCollection.Count > 0)
                {
                    ErrorCollection = EmptyErrorCollection;
                    return;
                }
                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                AssignToBE();



                ProReturn = BankBranch_DAL.GetSPFOR_ERR_MGR_BANK_BRANCH(ddlBankName.SelectedValue, txtBranchNameEn.Text, cA_BANK_BRANCH.BANK_BRANCH_ID);
                

                if (ProReturn != string.Empty)
                {

                    if (ProReturn != "0")
                    {

                        ErrorCollection.Add("BANKBRNCH", ProReturn);
                        if (ErrorCollection.Count > 0)
                        {
                            return;
                        }
                    }
                }




                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            DBMethod.SaveEntity<CA_BANK_BRANCH>(cA_BANK_BRANCH);
                            savedBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {

                            DBMethod.SaveEntity<CA_BANK_BRANCH>(cA_BANK_BRANCH, true);
                            savedBool = true;
                            break;

                        }
                }

                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                }

            }

            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion




        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                DBMethod.DeleteEntity<CA_BANK_BRANCH>(cA_BANK_BRANCH);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

    }
}