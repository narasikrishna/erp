﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.BLL;
using FIN.DAL.CA;
using FIN.BLL.CA;
using FIN.BLL.AP;
using FIN.DAL.AP;
using VMVServices.Web;

namespace FIN.Client.CA
{
    public partial class EmployeePettycashAcctEntry : PageBase
    {
        EMP_PETTYCASH_ACCT eMP_PETTYCASH_ACCT = new EMP_PETTYCASH_ACCT();
        string ProReturn = null;
        Boolean savedBool;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
            }

            if (VMVServices.Web.Utils.AddAllowed != FINAppConstants.Y && Master.Mode == FINAppConstants.Add)
            {
                btnSave.Visible = false;
            }

        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();

                EntityData = null;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    using (IRepository<EMP_PETTYCASH_ACCT> userCtx = new DataRepository<EMP_PETTYCASH_ACCT>())
                    {
                        eMP_PETTYCASH_ACCT = userCtx.Find(r =>
                            (r.PTY_CSH_ACCT_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = eMP_PETTYCASH_ACCT;

                    ddlEmp.SelectedValue = eMP_PETTYCASH_ACCT.EMP_ID;
                    filldept();
                    ddlAcctcode.SelectedValue = eMP_PETTYCASH_ACCT.ACCT_CODE;
                    ddlStaffReceivable.SelectedValue = eMP_PETTYCASH_ACCT.STAFF_RECEIVABLE;
                    

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>

        private void FillComboBox()
        {
            FIN.BLL.HR.Employee_BLL.GetEmpName(ref ddlEmp);
            FIN.BLL.GL.AccountCodes_BLL.fn_getAccount(ref ddlAcctcode);
            FIN.BLL.GL.AccountCodes_BLL.fn_getAccount(ref ddlStaffReceivable);


        }
        
        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    eMP_PETTYCASH_ACCT = (EMP_PETTYCASH_ACCT)EntityData;
                }
                eMP_PETTYCASH_ACCT.EMP_ID = ddlEmp.SelectedValue;
                eMP_PETTYCASH_ACCT.ACCT_CODE = ddlAcctcode.SelectedValue;
                eMP_PETTYCASH_ACCT.STAFF_RECEIVABLE = ddlStaffReceivable.SelectedValue;
                eMP_PETTYCASH_ACCT.ENABLED_FLAG = FINAppConstants.EnabledFlag;


                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    eMP_PETTYCASH_ACCT.MODIFIED_BY = this.LoggedUserName;
                    eMP_PETTYCASH_ACCT.MODIFIED_DATE = DateTime.Today;

                }
                else
                {
                    eMP_PETTYCASH_ACCT.PTY_CSH_ACCT_ID = FINSP.GetSPFOR_SEQCode("CA_011".ToString(), false, true);
                    eMP_PETTYCASH_ACCT.CREATED_BY = this.LoggedUserName;
                    eMP_PETTYCASH_ACCT.CREATED_DATE = DateTime.Today;

                }

                eMP_PETTYCASH_ACCT.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, eMP_PETTYCASH_ACCT.PTY_CSH_ACCT_ID);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }



        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                //System.Collections.SortedList slControls = new System.Collections.SortedList();

                //slControls[0] = ddlBankName;
                //slControls[1] = txtBranchNameEn;
                //slControls[2] = txtShortName;
                //slControls[3] = txtSWIFTCode;
                //slControls[4] = txtIFSCCode;

                //Dictionary<string, string> Prop_File_Data;
                //Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/CA_" + Session["Sel_Lng"].ToString() + ".properties"));
                //ErrorCollection.Clear();
                //string strCtrlTypes = "DropDownList~TextBox~TextBox~TextBox~TextBox";
                //string strMessage = Prop_File_Data["Bank_Name_P"] + " ~ " + Prop_File_Data["Branch_Name_P"] + " ~ " + Prop_File_Data["Short_Name_P"] + " ~ " + Prop_File_Data["SWIFT_Code_P"] + " ~ " + Prop_File_Data["IFSC_Code_P"] + "";
                ////string strMessage = "Bank Name ~ Branch Name ~ Short Name ~ SWIFT Code ~ IFSC Code";

                //EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

                //if (EmptyErrorCollection.Count > 0)
                //{
                //    ErrorCollection = EmptyErrorCollection;
                //    return;
                //}
                //if (ErrorCollection.Count > 0)
                //{
                //    return;
                //}
                AssignToBE();



              

                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            DBMethod.SaveEntity<EMP_PETTYCASH_ACCT>(eMP_PETTYCASH_ACCT);
                            savedBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {

                            DBMethod.SaveEntity<EMP_PETTYCASH_ACCT>(eMP_PETTYCASH_ACCT, true);
                            savedBool = true;
                            break;

                        }
                }

                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                }

            }

            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion




        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                DBMethod.DeleteEntity<EMP_PETTYCASH_ACCT>(eMP_PETTYCASH_ACCT);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void ddlEmp_SelectedIndexChanged(object sender, EventArgs e)
        {
            filldept();
        }

        private void filldept()
        {
            DataTable dtdeptdesig = new DataTable();

            dtdeptdesig = DBMethod.ExecuteQuery(FIN.DAL.HR.Employee_DAL.GetDeptDesigName(ddlEmp.SelectedValue)).Tables[0];
            if (dtdeptdesig.Rows.Count > 0)
            {
                txtDept.Text = dtdeptdesig.Rows[0]["DEPT_NAME"].ToString();
                txtDesig.Text = dtdeptdesig.Rows[0]["DESIG_NAME"].ToString();
            }

        }


    }
}