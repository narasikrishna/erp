﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.BLL;
using FIN.BLL.GL;
using FIN.DAL.CA;
using FIN.BLL.CA;
using FIN.BLL.AP;
using FIN.DAL.AP;
using VMVServices.Web;

namespace FIN.Client.CA
{
    public partial class BankAccountsEntry : PageBase
    {
        CA_BANK_ACCOUNTS cA_BANK_ACCOUNTS = new CA_BANK_ACCOUNTS();
        string ProReturn = null;
        Boolean savedBool;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
            }

            if (VMVServices.Web.Utils.AddAllowed != FINAppConstants.Y && Master.Mode == FINAppConstants.Add)
            {
                btnSave.Visible = false;
            }

        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                txtBankShortName1.Enabled = false;
                txtShortName.Enabled = false;
                txtSWIFTCode.Enabled = false;
                txtIFSCCode.Enabled = false;
                FillComboBox();

                EntityData = null;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    using (IRepository<CA_BANK_ACCOUNTS> userCtx = new DataRepository<CA_BANK_ACCOUNTS>())
                    {
                        cA_BANK_ACCOUNTS = userCtx.Find(r =>
                            (r.ACCOUNT_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = cA_BANK_ACCOUNTS;


                    ddlBankName.SelectedValue = cA_BANK_ACCOUNTS.BANK_ID;
                    FillBankShortNameDetails();
                    ddlBranchName.SelectedValue = cA_BANK_ACCOUNTS.BANK_BRANCH_ID;
                    FillBranchShortNameDetails();
                    ddlAccountType.Text = cA_BANK_ACCOUNTS.VENDOR_BANK_ACCTOUNT_TYPE;
                    txtAccountNumber.Text = cA_BANK_ACCOUNTS.VENDOR_BANK_ACCOUNT_CODE;

                    if (cA_BANK_ACCOUNTS.ENABLED_FLAG == FINAppConstants.EnabledFlag)
                    {
                        chkActive.Checked = true;
                    }
                    else
                    {
                        chkActive.Checked = false;
                    }


                    if (cA_BANK_ACCOUNTS.IS_MULTI_CURRENCY_ALLOWED == FINAppConstants.EnabledFlag)
                    {
                        chkMultiCurrency.Checked = true;
                    }
                    else
                    {
                        chkMultiCurrency.Checked = false;
                    }
                    ddlGLAccount.SelectedValue = cA_BANK_ACCOUNTS.GL_ACCOUNT;
                    ddlcurrency.SelectedValue = cA_BANK_ACCOUNTS.ACCT_CURRENCY;
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>

        private void FillComboBox()
        {
            Bank_BLL.fn_getBankName(ref ddlBankName);
            FIN.BLL.Lookup_BLL.GetLookUpValues(ref ddlAccountType, "ACT_TYP");
            //AccountCodes_BLL.getAccountWithControlAC(ref ddlGLAccount);
            FIN.BLL.GL.AccountCodes_BLL.fn_getAccount(ref ddlGLAccount);
            Currency_BLL.getCurrencyDesc(ref ddlcurrency);
        }


        protected void ddlBankName_SelectedIndexChanged(object sender, EventArgs e)
        {
            {
                try
                {
                    ErrorCollection.Clear();

                    FillBankShortNameDetails();

                   
                }
                catch (Exception ex)
                {
                    ErrorCollection.Add("POR", ex.Message);
                }
                finally
                {
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                    }
                }
            }
        }



        private void FillBankShortNameDetails()
        {
            DataTable dtgrndata = new DataTable();
            BankBranch_BLL.fn_getBranchName(ref ddlBranchName, ddlBankName.SelectedValue);
            dtgrndata = Bank_BLL.fn_getBankShortName(ddlBankName.SelectedValue.ToString());

            if (dtgrndata != null)
                if (dtgrndata.Rows.Count > 0)

                    txtBankShortName1.Text = dtgrndata.Rows[0]["BANK_SHORT_NAME"].ToString();

        }



      


        private void FillBranchShortNameDetails()
        {
            DataTable dtgrndata = new DataTable();

            dtgrndata = BankBranch_BLL.fn_getBankBranchShortName(ddlBranchName.SelectedValue.ToString());

            if (dtgrndata != null)
            {
                if (dtgrndata.Rows.Count > 0)
                {
                    txtShortName.Text = dtgrndata.Rows[0]["ATTRIBUTE2"].ToString();
                    txtIFSCCode.Text = dtgrndata.Rows[0]["BANK_IFSC_CODE"].ToString();
                    txtSWIFTCode.Text = dtgrndata.Rows[0]["BANK_SWIFT_CODE"].ToString();
                }
            }
        }



        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    cA_BANK_ACCOUNTS = (CA_BANK_ACCOUNTS)EntityData;
                }

                cA_BANK_ACCOUNTS.BANK_ID = ddlBankName.SelectedValue;
                cA_BANK_ACCOUNTS.BANK_BRANCH_ID = ddlBranchName.SelectedValue;
                cA_BANK_ACCOUNTS.VENDOR_BANK_ACCTOUNT_TYPE = ddlAccountType.Text;
                cA_BANK_ACCOUNTS.VENDOR_BANK_ACCOUNT_CODE = txtAccountNumber.Text;
                cA_BANK_ACCOUNTS.ACCT_CURRENCY = ddlcurrency.SelectedValue;
                cA_BANK_ACCOUNTS.ORG_ID = VMVServices.Web.Utils.OrganizationID;
                cA_BANK_ACCOUNTS.ENABLED_FLAG = chkActive.Checked == true ? FINAppConstants.EnabledFlag : FINAppConstants.DisabledFlag;
                cA_BANK_ACCOUNTS.IS_MULTI_CURRENCY_ALLOWED = chkMultiCurrency.Checked == true ? FINAppConstants.EnabledFlag : FINAppConstants.DisabledFlag;

                cA_BANK_ACCOUNTS.GL_ACCOUNT = ddlGLAccount.SelectedValue;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    cA_BANK_ACCOUNTS.MODIFIED_BY = this.LoggedUserName;
                    cA_BANK_ACCOUNTS.MODIFIED_DATE = DateTime.Today;

                }
                else
                {
                    cA_BANK_ACCOUNTS.ACCOUNT_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.CA_003.ToString(), false, true);
                    cA_BANK_ACCOUNTS.CREATED_BY = this.LoggedUserName;
                    cA_BANK_ACCOUNTS.CREATED_DATE = DateTime.Today;

                }
                cA_BANK_ACCOUNTS.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, cA_BANK_ACCOUNTS.ACCOUNT_ID);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }


        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                System.Collections.SortedList slControls = new System.Collections.SortedList();

                slControls[0] = ddlBankName;
                slControls[1] = ddlBranchName;
                slControls[2] = txtBankShortName1;
                slControls[3] = txtShortName;
                slControls[4] = txtSWIFTCode;
                slControls[5] = txtIFSCCode;
                slControls[6] = txtAccountNumber;
                Dictionary<string, string> Prop_File_Data;
                Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/CA_" + Session["Sel_Lng"].ToString() + ".properties"));
                ErrorCollection.Clear();
                string strCtrlTypes = "DropDownList~DropDownList~TextBox~TextBox~TextBox~TextBox~TextBox";
                string strMessage = Prop_File_Data["Bank_Name_P"] + " ~ " + Prop_File_Data["Branch_Name_P"] + " ~ " + Prop_File_Data["Bank_Short_Name_P"] + " ~ " + Prop_File_Data["Branch_Short_Name_P"] + " ~ " + Prop_File_Data["SWIFT_Code_P"] + " ~ " + Prop_File_Data["IFSC_Code_P"] + " ~ " + Prop_File_Data["Account_Number_P"] + "";
                //string strMessage = "Bank Name ~ Branch Name ~ Bank Short Name ~ Short Name ~ SWIFT Code ~ IFSC Code ~ Account Number";

                EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

                if (EmptyErrorCollection.Count > 0)
                {
                    ErrorCollection = EmptyErrorCollection;
                    return;
                }


                AssignToBE();


                ProReturn = BankAccounts_DAL.GetSPFOR_ERR_MGR_BANK_ACCOUNTS(ddlBankName.SelectedValue, ddlBranchName.SelectedValue, txtAccountNumber.Text, cA_BANK_ACCOUNTS.ACCOUNT_ID);


                if (ProReturn != string.Empty)
                {

                    if (ProReturn != "0")
                    {

                        ErrorCollection.Add("BANKACCOUNTS", ProReturn);
                        if (ErrorCollection.Count > 0)
                        {
                            return;
                        }
                    }
                }




                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            DBMethod.SaveEntity<CA_BANK_ACCOUNTS>(cA_BANK_ACCOUNTS);
                            savedBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {

                            DBMethod.SaveEntity<CA_BANK_ACCOUNTS>(cA_BANK_ACCOUNTS,true);
                            savedBool = true;
                            break;

                        }
                }

                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                }

            }

            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion




        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                DBMethod.DeleteEntity<CA_BANK_ACCOUNTS>(cA_BANK_ACCOUNTS);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void ddlBranchName_SelectedIndexChanged1(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                FillBranchShortNameDetails();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

    }
}