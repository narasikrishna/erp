﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="BankStatementUploadEntry.aspx.cs" Inherits="FIN.Client.CA.BankStatementUploadEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 800px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblBankName">
                Bank Name
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 155px">
                <asp:DropDownList ID="ddlBankName" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlBankName_SelectedIndexChanged"
                    CssClass="validate[required] RequiredField ddlStype" TabIndex="1">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox  LNOrient" style=" width: 195px" id="lblBankShortName">
                Bank Short Name
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtBankShortName" Enabled="false" CssClass="validate[] txtBox" MaxLength="10"
                    runat="server" TabIndex="2"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblBranchName">
                Branch Name
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 155px">
                <asp:DropDownList ID="ddlBranchName" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlBranchName_SelectedIndexChanged"
                    CssClass="validate[required] RequiredField ddlStype" TabIndex="3">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox  LNOrient" style=" width: 195px" id="lblBranchShortName">
                Branch Short Name
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtBranchShortName" Enabled="false" CssClass="validate[] txtBox"
                    MaxLength="10" runat="server" TabIndex="4"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblAccountNumber">
                Account Number
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 155px">
                <asp:DropDownList ID="ddlAccountNumber" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    TabIndex="5">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblNumberofCheques">
                Statement Number From
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtStatementNoFrom" CssClass="validate[required] RequiredField txtBox_N"
                    MaxLength="10" runat="server" TabIndex="6"></asp:TextBox>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox  LNOrient" style=" width: 200px" id="Div2">
                Statement Number To
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtStatementNoTo" CssClass="validate[required] RequiredField txtBox_N"
                    MaxLength="10" runat="server" TabIndex="7"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 200px" id="Div3">
                Statement Date From
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtStatementDateFrom" CssClass="validate[required] RequiredField txtBox_N"
                    MaxLength="10" runat="server" TabIndex="8"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calExtender1" TargetControlID="txtStatementDateFrom"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox  LNOrient" style=" width: 200px" id="Div5">
                Statement Date To
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtStatementDateTo" CssClass="validate[required] RequiredField txtBox_N"
                    MaxLength="10" runat="server" TabIndex="9"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtStatementDateTo"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 200px" id="Div4">
                Gl Date
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtGLDate" CssClass="validate[required] RequiredField txtBox_N"
                    MaxLength="10" runat="server" TabIndex="10"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender2" TargetControlID="txtGLDate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox  LNOrient" style=" width: 200px" id="Div6">
                Statement Path
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtStatementPath" CssClass="validate[required] RequiredField txtBox_N"
                    MaxLength="10" runat="server" TabIndex="11"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 200px" id="Div7">
                File Name
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 150px">
                <asp:TextBox ID="txtFileName" CssClass="validate[required] RequiredField txtBox_N"
                    MaxLength="10" runat="server" TabIndex="12"></asp:TextBox>
            </div>
            <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox  LNOrient" style=" width: 200px" id="Div1">
                Template Name
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 155px">
                <asp:DropDownList ID="ddlTemplateName" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    TabIndex="13">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 200px" id="Div9">
                Choose File
            </div>
            <div class="lblBox  LNOrient" style=" width: 200px" id="Div8">
                <asp:ImageButton ID="btnSelFile" runat="server" ImageUrl="~/Images/btnSelect.png"
                    OnClick="btnSelFile_Click" Style="border: 0px" />
                <%--<asp:Button ID="btnSelFile" runat="server" Text="Select" 
                    onclick="btnSelFile_Click" />--%>
                &nbsp;&nbsp;
                <asp:Label ID="lblSelFile" runat="server" Text=""></asp:Label>
            </div>
        </div>
        <div class="divRowContainer">
            <div class="divClear_10">
            </div>
            <div class="divRowContainer divAction">
                <table class="SaveTable">
                    <tr>
                        <td>
                            <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn" />
                        </td>
                        <td>
                            <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" />
                        </td>
                        <td>
                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" />
                        </td>
                        <td>
                            <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" />
                        </td>
                    </tr>
                </table>
            </div>
            <div id="divDelete">
                <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                    PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
                </cc2:ModalPopupExtender>
                <asp:Panel ID="pnlConfirm" runat="server">
                    <div class="ConfirmForm">
                        <table>
                            <tr class="ConfirmHeading" style="width: 100%">
                                <td>
                                    <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                        Width="60px" />
                                    &nbsp;
                                    <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </asp:Panel>
            </div>
        </div>
    </div>
    <asp:Panel ID="pnlpopupupload" runat="server" Visible="true" Height="300px">
        <div id="div12">
            <asp:HiddenField ID="hf_excelFile" runat="server" />
            <cc2:ModalPopupExtender ID="mpeExcelFileUpload" runat="server" TargetControlID="hf_excelFile"
                PopupControlID="pnlFoto" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlFoto" runat="server" Width="800px">
                <div class="ConfirmForm">
                    <table>
                        <tr class="ConfirmFotoHeading">
                            <td>
                                <iframe id="frmFoto" runat="server" src="../UpLoadFile/BankStatementUpload.aspx"
                                    name="frmFoto" frameborder="0" style="width: 750px; height: 280px;"></iframe>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:ImageButton ID="btnNo" runat="server" ImageUrl="~/Images/btnClose.png"
                                    OnClick="btnCloseImage_Click" CssClass="button" Style="border: 0px"  />
                                <%--<asp:Button runat="server" CssClass="button" ID="btnNo" Text="Close" OnClick="btnCloseImage_Click"
                                    Width="60px" />--%>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </asp:Panel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/CA/CAChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
            $("#FINContent_btnCreateCheques").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
