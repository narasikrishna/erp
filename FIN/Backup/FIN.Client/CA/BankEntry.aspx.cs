﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.BLL;
using FIN.BLL.AP;
using FIN.DAL.AP;
using VMVServices.Web;

namespace FIN.Client.CA
{
    public partial class BankEntry : PageBase
    {
        CA_BANK cA_BANK = new CA_BANK();
        
        Boolean savedBool;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
            //if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            //{
            //    VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
            //}

            //if (VMVServices.Web.Utils.AddAllowed != FINAppConstants.Y && Master.Mode == FINAppConstants.Add)
            //{
            //    btnSave.Visible = false;
            //}

        }

        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();

                EntityData = null;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    using (IRepository<CA_BANK> userCtx = new DataRepository<CA_BANK>())
                    {
                        cA_BANK = userCtx.Find(r =>
                            (r.BANK_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = cA_BANK;

                    txtBankNameEn.Text = cA_BANK.BANK_NAME;
                    txtBankNameAr.Text = cA_BANK.BANK_NAME_OL;
                    txtShortName.Text = cA_BANK.BANK_SHORT_NAME;
                    txtAddress1.Text = cA_BANK.BANK_ADD1;
                    txtAddress2.Text = cA_BANK.BANK_ADD2;
                    txtAddress3.Text = cA_BANK.BANK_ADD3;
                    txtPostalCode.Text = cA_BANK.BANK_ZIP_CODE;
                    txtPhone.Text = cA_BANK.BANK_PHONE;
                    ddlCountry.SelectedValue = cA_BANK.BANK_COUNTRY;
                    FILLSTATE();
                    ddlState.SelectedValue = cA_BANK.BANK_STATE;
                    FILLCITY();
                    ddlCity.SelectedValue = cA_BANK.BANK_CITY;

                    if (cA_BANK.ENABLED_FLAG == FINAppConstants.EnabledFlag)
                    {
                        chkActive.Checked = true;
                    }
                    else
                    {
                        chkActive.Checked = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>

        private void FillComboBox()
        {
            Supplier_BLL.fn_getCountry(ref ddlCountry);

        }

        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    cA_BANK = (CA_BANK)EntityData;
                }

                cA_BANK.BANK_NAME =  txtBankNameEn.Text;
                cA_BANK.BANK_NAME_OL = txtBankNameAr.Text;
                cA_BANK.BANK_SHORT_NAME = txtShortName.Text;
                cA_BANK.BANK_ADD1 = txtAddress1.Text;
                cA_BANK.BANK_ADD2 = txtAddress2.Text;
                cA_BANK.BANK_ADD3 = txtAddress3.Text;
                cA_BANK.BANK_COUNTRY = ddlCountry.SelectedValue;
                cA_BANK.BANK_STATE = ddlState.SelectedValue;
                cA_BANK.BANK_CITY = ddlCity.SelectedValue;
                cA_BANK.BANK_ZIP_CODE = txtPostalCode.Text;
                cA_BANK.BANK_PHONE = txtPhone.Text;
                cA_BANK.ORG_ID = VMVServices.Web.Utils.OrganizationID;
                cA_BANK.ENABLED_FLAG = chkActive.Checked == true ? FINAppConstants.EnabledFlag : FINAppConstants.DisabledFlag;
                
                             
                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    cA_BANK.MODIFIED_BY = this.LoggedUserName;
                    cA_BANK.MODIFIED_DATE = DateTime.Today;
                    
                }
                else
                {
                    cA_BANK.BANK_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.CA_001.ToString(), false, true);
                    cA_BANK.CREATED_BY = this.LoggedUserName;
                    cA_BANK.CREATED_DATE = DateTime.Today;

                }

                cA_BANK.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, cA_BANK.BANK_ID);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }


        protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            FILLSTATE();
        }
        private void FILLSTATE()
        {
            Supplier_BLL.fn_getState(ref ddlState, ddlCountry.SelectedValue);            
        }

        protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
        {
            FILLCITY();
        }
        private void FILLCITY()
        {
            Supplier_BLL.fn_getCity(ref ddlCity, ddlState.SelectedValue);
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                System.Collections.SortedList slControls = new System.Collections.SortedList();

                slControls[0] = txtBankNameEn;
                slControls[1] = txtShortName;          
                

                ErrorCollection.Clear();
                string strCtrlTypes = "TextBox~TextBox";

                string strMessage = "Bank Name ~ Short Name";

                EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

                if (EmptyErrorCollection.Count > 0)
                {
                    ErrorCollection = EmptyErrorCollection;
                    return;
                }


                AssignToBE();


                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            DBMethod.SaveEntity<CA_BANK>(cA_BANK);                            
                            savedBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {

                            DBMethod.SaveEntity<CA_BANK>(cA_BANK, true);                            
                            savedBool = true;
                            break;

                        }
                }

                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                }

            }

            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion




        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                DBMethod.DeleteEntity<CA_BANK>(cA_BANK);                
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void txtShortName_TextChanged(object sender, EventArgs e)
        {

        }

        protected void txtBankNameAr_TextChanged(object sender, EventArgs e)
        {

        }

    }
}