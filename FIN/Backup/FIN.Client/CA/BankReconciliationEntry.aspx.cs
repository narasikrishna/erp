﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.BLL;
using FIN.BLL.CA;
using FIN.DAL.CA;
using VMVServices.Web;

namespace FIN.Client.CA
{
    public partial class BankReconciliationEntry : PageBase
    {
        CA_CHECK_HDR cA_CHECK_HDR = new CA_CHECK_HDR();
        CA_CHECK_DTL cA_CHECK_DTL = new CA_CHECK_DTL();
        DataTable dtGridData = new DataTable();
        Boolean bol_rowVisiable;
        Boolean savedBool;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("GL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();

            //if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            //{
            //    VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
            //}

            //if (VMVServices.Web.Utils.AddAllowed != FINAppConstants.Y && Master.Mode == FINAppConstants.Add)
            //{
            //    btnSave.Visible = false;
            //}

        }
      
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }


        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();
                EntityData = null;
                Session[FINSessionConstants.GridData] = null;

                dtGridData = DBMethod.ExecuteQuery(FIN.DAL.CA.Cheque_DAL.GetChequeDetails(Master.StrRecordId)).Tables[0];
                //dtGridData = DBMethod.ExecuteQuery(FIN.DAL.CA.Cheque_DAL.GetChequeCreationDetails()).Tables[0];
                BindGrid(dtGridData);

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    cA_CHECK_HDR = FIN.BLL.CA.Cheque_BLL.getClassEntity(Master.StrRecordId);

                    EntityData = cA_CHECK_HDR;

                    ddlBankName.SelectedValue = cA_CHECK_HDR.CHECK_BANK_ID.ToString();
                    FillBankShortNameDetails();
                    ddlBranchName.SelectedValue = cA_CHECK_HDR.CHECK_BRANCH_ID.ToString();
                    FillBranchShortNameDetails();
                    ddlAccountNumber.SelectedValue = cA_CHECK_HDR.ACCOUNT_ID.ToString();
                  //  txtNumberofCheques.Text = cA_CHECK_HDR.CHECK_NO_LEAVES.ToString();
                    //txtNumberofCheques.Enabled = false;
                  //  btnCreateCheques.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                Dictionary<string, string> Prop_File_Data;
                Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/CA_" + Session["Sel_Lng"].ToString() + ".properties"));
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    cA_CHECK_HDR = (CA_CHECK_HDR)EntityData;
                }

                cA_CHECK_HDR.CHECK_BANK_ID = ddlBankName.SelectedValue.ToString();
                cA_CHECK_HDR.CHECK_BRANCH_ID = ddlBranchName.SelectedValue.ToString();
                cA_CHECK_HDR.ACCOUNT_ID = ddlAccountNumber.SelectedValue;
             //   cA_CHECK_HDR.CHECK_NO_LEAVES = int.Parse(txtNumberofCheques.Text);
                cA_CHECK_HDR.ORG_ID = VMVServices.Web.Utils.OrganizationID;


                cA_CHECK_HDR.ENABLED_FLAG = FINAppConstants.EnabledFlag;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    cA_CHECK_HDR.MODIFIED_BY = this.LoggedUserName;
                    cA_CHECK_HDR.MODIFIED_DATE = DateTime.Today;

                }
                else
                {
                    cA_CHECK_HDR.CHECK_HDR_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.CA_004_M.ToString(), false, true);
                    cA_CHECK_HDR.CREATED_BY = this.LoggedUserName;
                    cA_CHECK_HDR.CREATED_DATE = DateTime.Today;

                }
                cA_CHECK_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, cA_CHECK_HDR.CHECK_HDR_ID);

                //Save Detail Table


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                ErrorCollection = CommonUtils.IsEmptyGrid(dtGridData, Prop_File_Data["Cheque_Entry_P"]);

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                var tmpChildEntity = new List<Tuple<object, string>>();

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    cA_CHECK_DTL = new CA_CHECK_DTL();
                    if (dtGridData.Rows[iLoop][FINColumnConstants.CHECK_DTL_ID].ToString() != "0" && dtGridData.Rows[iLoop]["CHECK_DTL_ID"].ToString() != string.Empty)
                    {
                        using (IRepository<CA_CHECK_DTL> userCtx = new DataRepository<CA_CHECK_DTL>())
                        {
                            cA_CHECK_DTL = userCtx.Find(r =>
                                (r.CHECK_DTL_ID == dtGridData.Rows[iLoop][FINColumnConstants.CHECK_DTL_ID].ToString())
                                ).SingleOrDefault();
                        }
                    }

                    if (dtGridData.Rows[iLoop][FINColumnConstants.CHECK_DTL_ID].ToString() != "0")
                    {
                        Cheque_BLL.getClassEntity(dtGridData.Rows[iLoop][FINColumnConstants.CHECK_DTL_ID].ToString());
                    }
                    cA_CHECK_DTL.CHECK_DTL_ID = (dtGridData.Rows[iLoop][FINColumnConstants.CHECK_DTL_ID].ToString());


                    if (dtGridData.Rows[iLoop][FINColumnConstants.CHECK_LINE_NUM] != DBNull.Value)
                    {
                        cA_CHECK_DTL.CHECK_LINE_NUM = int.Parse(dtGridData.Rows[iLoop][FINColumnConstants.CHECK_LINE_NUM].ToString());
                    }
                    if (dtGridData.Rows[iLoop][FINColumnConstants.CHECK_NUMBER] != DBNull.Value)
                    {
                        cA_CHECK_DTL.CHECK_NUMBER = dtGridData.Rows[iLoop][FINColumnConstants.CHECK_NUMBER].ToString();
                    }
                    if (dtGridData.Rows[iLoop][FINColumnConstants.CHECK_DT] != DBNull.Value)
                    {
                        cA_CHECK_DTL.CHECK_DT = DateTime.Parse(dtGridData.Rows[iLoop][FINColumnConstants.CHECK_DT].ToString());
                    }
                    if (dtGridData.Rows[iLoop][FINColumnConstants.CHECK_PAY_TO] != DBNull.Value)
                    {
                        cA_CHECK_DTL.CHECK_PAY_TO = dtGridData.Rows[iLoop][FINColumnConstants.CHECK_PAY_TO].ToString();
                    }
                    if (dtGridData.Rows[iLoop][FINColumnConstants.CHECK_AMT] != DBNull.Value)
                    {
                        cA_CHECK_DTL.CHECK_AMT = CommonUtils.ConvertStringToDecimal(dtGridData.Rows[iLoop][FINColumnConstants.CHECK_AMT].ToString());
                    }
                    if (dtGridData.Rows[iLoop][FINColumnConstants.CHECK_STATUS] != DBNull.Value)
                    {
                        cA_CHECK_DTL.CHECK_STATUS = dtGridData.Rows[iLoop][FINColumnConstants.CHECK_STATUS].ToString();
                    }

                    if (dtGridData.Rows[iLoop][FINColumnConstants.CHECK_CANCEL_REMARKS] != DBNull.Value)
                    {
                        cA_CHECK_DTL.CHECK_CANCEL_REMARKS = dtGridData.Rows[iLoop][FINColumnConstants.CHECK_CANCEL_REMARKS].ToString();
                    }

                    if (dtGridData.Rows[iLoop][FINColumnConstants.CHECK_CROSSED_YN].ToString().ToUpper() == "TRUE")
                    {
                        cA_CHECK_DTL.CHECK_CROSSED_YN = FINAppConstants.EnabledFlag;
                    }
                    else
                    {
                        cA_CHECK_DTL.CHECK_CROSSED_YN = FINAppConstants.DisabledFlag;
                    }



                    cA_CHECK_DTL.ENABLED_FLAG = FINAppConstants.EnabledFlag;

                    cA_CHECK_DTL.WORKFLOW_COMPLETION_STATUS = FINAppConstants.EnabledFlag;

                    cA_CHECK_DTL.CHECK_HDR_ID = cA_CHECK_HDR.CHECK_HDR_ID;

                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        cA_CHECK_DTL.CHECK_DTL_ID = dtGridData.Rows[iLoop][FINColumnConstants.CHECK_DTL_ID].ToString();
                        tmpChildEntity.Add(new Tuple<object, string>(cA_CHECK_DTL, FINAppConstants.Delete));
                    }
                    else
                    {
                        if (dtGridData.Rows[iLoop][FINColumnConstants.CHECK_DTL_ID].ToString() != "0" && dtGridData.Rows[iLoop][FINColumnConstants.CHECK_DTL_ID].ToString() != string.Empty)
                        {
                            cA_CHECK_DTL.CHECK_DTL_ID = dtGridData.Rows[iLoop][FINColumnConstants.CHECK_DTL_ID].ToString();
                            cA_CHECK_DTL.MODIFIED_DATE = DateTime.Today;
                            cA_CHECK_DTL.MODIFIED_BY = this.LoggedUserName;
                            tmpChildEntity.Add(new Tuple<object, string>(cA_CHECK_DTL, FINAppConstants.Update));
                        }
                        else
                        {
                            cA_CHECK_DTL.CHECK_DTL_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.CA_004_D.ToString(), false, true);
                            cA_CHECK_DTL.CREATED_BY = this.LoggedUserName;
                            cA_CHECK_DTL.CREATED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(cA_CHECK_DTL, FINAppConstants.Add));
                        }
                    }
                }

                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            CommonUtils.SavePCEntity<CA_CHECK_HDR, CA_CHECK_DTL>(cA_CHECK_HDR, tmpChildEntity, cA_CHECK_DTL);
                            savedBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {
                            CommonUtils.SavePCEntity<CA_CHECK_HDR, CA_CHECK_DTL>(cA_CHECK_HDR, tmpChildEntity, cA_CHECK_DTL, true);
                            savedBool = true;
                            break;
                        }
                }
                if (ErrorCollection.Count > 0)
                {
                    return;
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        private void FillComboBox()
        {
            Bank_BLL.fn_getBankName(ref ddlBankName);
        }


        protected void ddlBankName_SelectedIndexChanged(object sender, EventArgs e)
        {
            {
                try
                {
                    ErrorCollection.Clear();

                    FillBankShortNameDetails();


                }
                catch (Exception ex)
                {
                    ErrorCollection.Add("POR", ex.Message);
                }
                finally
                {
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                    }
                }
            }
        }



        private void FillBankShortNameDetails()
        {
            DataTable dtgrndata = new DataTable();

            dtgrndata = Bank_BLL.fn_getBankShortName(ddlBankName.SelectedValue.ToString());

            if (dtgrndata != null)
            {
                if (dtgrndata.Rows.Count > 0)
                {
                    txtBankShortName.Text = dtgrndata.Rows[0]["BANK_SHORT_NAME"].ToString();
                }
            }
            BankBranch_BLL.fn_getBranchName(ref ddlBranchName, ddlBankName.SelectedValue);



        }



        protected void ddlBranchName_SelectedIndexChanged(object sender, EventArgs e)
        {
            {
                try
                {
                    ErrorCollection.Clear();

                    FillBranchShortNameDetails();



                }
                catch (Exception ex)
                {
                    ErrorCollection.Add("POR", ex.Message);
                }
                finally
                {
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                    }
                }
            }
        }



        private void FillBranchShortNameDetails()
        {
            DataTable dtgrndata = new DataTable();

            dtgrndata = BankBranch_BLL.fn_getBankBranchShortName(ddlBranchName.SelectedValue.ToString());

            if (dtgrndata != null)
            {
                if (dtgrndata.Rows.Count > 0)
                {
                    txtBranchShortName.Text = dtgrndata.Rows[0]["ATTRIBUTE2"].ToString();
                }
            }
            Cheque_BLL.fn_getAccountNumber(ref ddlAccountNumber, ddlBankName.SelectedValue, ddlBranchName.SelectedValue);

        }




        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                //DropDownList ddlGroupName = tmpgvr.FindControl("ddlGroupName") as DropDownList;
                //AccountingGroupLinks_BLL.fn_getAccountGroupName(ref ddlGroupName);


                //if (gvData.EditIndex >= 0)
                //{
                //    ddlGroupName.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.ACCT_GRP_ID].ToString();


                //}
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Org_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                if (dtData.Rows.Count > 0)
                {
                    dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("CHECK_AMT", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("CHECK_AMT"))));
                    dtData.AcceptChanges();
                }
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr["CHECK_CROSSED_YN"] = "FALSE";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ChequeEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            if (Session[FINSessionConstants.GridData] != null)
            {
                dtGridData = (DataTable)Session[FINSessionConstants.GridData];
            }
            gvData.EditIndex = -1;

            BindGrid(dtGridData);

        }
        #endregion

        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ChequeEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {


            System.Collections.SortedList slControls = new System.Collections.SortedList();

            TextBox txtLineNo = gvr.FindControl("txtLineNo") as TextBox;
            TextBox txtChequeNumber = gvr.FindControl("txtChequeNumber") as TextBox;
            TextBox ddtpChequeDate = gvr.FindControl("ddtpChequeDate") as TextBox;
            TextBox txtPayTo = gvr.FindControl("txtPayTo") as TextBox;
            TextBox txtAmount = gvr.FindControl("txtAmount") as TextBox;
            TextBox txtChequeStatus = gvr.FindControl("txtChequeStatus") as TextBox;
            TextBox txtRemarks = gvr.FindControl("txtRemarks") as TextBox;
            CheckBox chkCrossedCheque = gvr.FindControl("chkCrossedCheque") as CheckBox;

            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList[FINColumnConstants.CHECK_DTL_ID] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }

            slControls[0] = txtLineNo;
            slControls[1] = txtChequeNumber;
            slControls[2] = ddtpChequeDate;
            slControls[3] = txtPayTo;            
            slControls[4] = txtAmount;
            slControls[5] = txtChequeStatus;
            slControls[6] = txtRemarks;

            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/CA_" + Session["Sel_Lng"].ToString() + ".properties"));

            ErrorCollection.Clear();
            string strCtrlTypes = FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.DATE_TIME + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX + "~" + FINAppConstants.TEXT_BOX;
            string strMessage = Prop_File_Data["Line_No_P"] + " ~ " + Prop_File_Data["Cheque_Number_P"] + " ~ " + Prop_File_Data["Cheque_Date_P"] + " ~ " + Prop_File_Data["Pay_To_P"] + " ~ " + Prop_File_Data["Amount_P"] + " ~ " + Prop_File_Data["Cheque_Status_P"] + " ~ " + Prop_File_Data["Remarks_P"] + "";
            //string strMessage = "Line No ~ Cheque Number ~ Cheque Date ~ Pay To ~ Amount ~ Cheque Status ~ Remarks";

            EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

            if (EmptyErrorCollection.Count > 0)
            {
                ErrorCollection = EmptyErrorCollection;
                return drList;
            }


            //ErrorCollection = UserUtility_BLL.DateRangeValidate(DBMethod.ConvertStringToDate(dtp_StartDate.Text), DBMethod.ConvertStringToDate(dtp_EndDate.Text), Master.Mode);
            //if (ErrorCollection.Count > 0)
            //    return drList;


            string strCondition = "CHECK_NUMBER='" + txtChequeNumber.Text.ToString() + "'";
            strMessage = FINMessageConstatns.RecordAlreadyExists;
            ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            if (ErrorCollection.Count > 0)
            {
                return drList;
            }

                        
            if (txtLineNo.Text.ToString().Length > 0)
            {
                drList[FINColumnConstants.CHECK_LINE_NUM] = txtLineNo.Text.ToString();
            }
            else
            {
                drList[FINColumnConstants.CHECK_LINE_NUM] = DBNull.Value;
            }


            if (txtChequeNumber.Text.ToString().Length > 0)
            {
                drList[FINColumnConstants.CHECK_NUMBER] = txtChequeNumber.Text.ToString();
            }
            else
            {
                drList[FINColumnConstants.CHECK_NUMBER] = DBNull.Value;
            }
            if (ddtpChequeDate.Text.ToString().Length > 0)
            {
                drList[FINColumnConstants.CHECK_DT] = DBMethod.ConvertStringToDate(ddtpChequeDate.Text.ToString());
            }
            else
            {
                drList[FINColumnConstants.CHECK_DT] = DBNull.Value;
            }

            if (txtPayTo.Text.ToString().Length > 0)
            {
                drList[FINColumnConstants.CHECK_PAY_TO] = txtPayTo.Text.ToString();
            }
            else
            {
                drList[FINColumnConstants.CHECK_PAY_TO] = DBNull.Value;
            }

            if (txtAmount.Text.ToString().Length > 0)
            {
                drList[FINColumnConstants.CHECK_AMT] = txtAmount.Text.ToString();
            }
            else
            {
                drList[FINColumnConstants.CHECK_AMT] = DBNull.Value;
            }

            if (txtChequeStatus.Text.ToString().Length > 0)
            {
                drList[FINColumnConstants.CHECK_STATUS] = txtChequeStatus.Text.ToString();
            }
            else
            {
                drList[FINColumnConstants.CHECK_STATUS] = DBNull.Value;
            }

            if (txtRemarks.Text.ToString().Length > 0)
            {
                drList[FINColumnConstants.CHECK_CANCEL_REMARKS] = txtRemarks.Text.ToString();
            }
            else
            {
                drList[FINColumnConstants.CHECK_CANCEL_REMARKS] = DBNull.Value;
            }



            if (chkCrossedCheque.Checked)
            {
                drList[FINColumnConstants.CHECK_CROSSED_YN] = "TRUE";
            }
            else
            {
                drList[FINColumnConstants.CHECK_CROSSED_YN] = "FALSE";
            }

            return drList;

        }

        //public void DataDuplication(System.Data.DataTable dtGridData, string strCondition, string strMessage)
        //{
        //    try
        //    {
        //        // SortedList ErrorCollection = new SortedList();
        //        if (dtGridData.Select(strCondition).Length > 0)
        //        {
        //            ErrorCollection.Add(strMessage, strMessage);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorCollection.Add("AccountingGroupLinksEntry", ex.Message);
        //    }
        //    finally
        //    {
        //        if (ErrorCollection.Count > 0)
        //        {
        //            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
        //            // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
        //        }
        //    }
        //    //return ErrorCollection;
        //}

        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ChequeEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ChequeEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ChequeEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ChequeEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ChequeEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }



        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>


        #endregion




        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();

                DBMethod.DeleteEntity<CA_CHECK_HDR>(cA_CHECK_HDR);
                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATADELETED);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ChequeEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                AssignToBE();

                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);
                }
                //switch (Master.Mode)
                //{
                //    case FINAppConstants.Add:
                //        {
                //            DBMethod.SaveEntity<GL_ACCT_GROUP_LINK_HDR>(gL_ACCT_GROUP_LINK_HDR);
                //            DisplaySaveCompleteMessage(Master.ListPageToOpen);
                //            break;
                //        }
                //    case FINAppConstants.Update:
                //        {

                //            DBMethod.SaveEntity<GL_ACCT_GROUP_LINK_HDR>(gL_ACCT_GROUP_LINK_HDR, true);
                //            DisplaySaveCompleteMessage(Master.ListPageToOpen);
                //            break;

                //        }
                //}
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

       

        
    }
        #endregion
}