﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="PayrollDeductionPartyEntry.aspx.cs" Inherits="FIN.Client.PER.PayrollDeductionPartyEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 800px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 200px" id="lblDeductionPartyNumber">
                Deduction Party Number
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:TextBox ID="txtDeductionPartyNumber" MaxLength="50" CssClass="EntryFont validate[] txtBox"
                    Enabled="true" TabIndex="1" runat="server"></asp:TextBox>
            </div>
            </div>
             <div class="divClear_10">
        </div>
        <div class="divRowContainer">
             
            <div class="lblBox LNOrient" style="  width: 200px" id="lblDeductionPartyNameEn">
                Deduction Party Name
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:TextBox ID="txtDeductionPartyNameEn" CssClass="EntryFont validate[required] RequiredField txtBox_en"
                    TabIndex="2" MaxLength="200" runat="server" 
                    ></asp:TextBox>
            </div>
               <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="  width: 200px" id="lblDeductionPartyNameAr">
                Deduction Party Name(Arabic)
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:TextBox ID="txtDeductionPartyNameAr" CssClass="EntryFont txtBox_ol"
                    TabIndex="3" MaxLength="200" runat="server" 
                    ></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 200px" id="lblPaymentMode">
                Payment Mode
            </div>
            <div class="divtxtBox LNOrient" style="  width: 155px">
                <asp:DropDownList ID="ddlPaymentMode" runat="server" TabIndex="4" CssClass="EntryFont validate[required] RequiredField ddlStype"
                    >
                </asp:DropDownList>
            </div>
             <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="  width: 195px" id="lblElement">
                Element
            </div>
            <div class="divtxtBox LNOrient" style="  width: 155px">
                <asp:DropDownList ID="ddlElement" runat="server" TabIndex="5" CssClass="EntryFont validate[required] RequiredField ddlStype"
                    >
                </asp:DropDownList>
            </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="  width: 200px" id="lblAddress1">
                    Address 1
                </div>
                <div class="divtxtBox LNOrient" style="  width: 150px">
                    <asp:TextBox ID="txtAddress1" MaxLength="200" TabIndex="6" CssClass="EntryFont validate[required] RequiredField txtBox"
                        runat="server"></asp:TextBox>
                </div>
                 <div class="colspace  LNOrient" >
                &nbsp</div>
                <div class="lblBox LNOrient" style="  width: 200px" id="lblCountry">
                    Country
                </div>
                <div class="divtxtBox LNOrient" style="  width: 155px">
                    <asp:DropDownList ID="ddlCountry" AutoPostBack="True" TabIndex="7" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged"
                        runat="server" CssClass="EntryFont validate[]   ddlStype" >
                    </asp:DropDownList>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <div class="lblBox LNOrient" style="  width: 200px" id="Div1">
                    Address 2
                </div>
                <div class="divtxtBox LNOrient" style="  width: 150px">
                    <asp:TextBox ID="txtAddress2" MaxLength="200" TabIndex="8" CssClass="EntryFont validate[required] RequiredField txtBox"
                        runat="server"></asp:TextBox>
                </div>
                  <div class="colspace  LNOrient" >
                &nbsp</div>
                <div class="lblBox LNOrient" style="  width: 200px" id="Div2">
                    State
                </div>
                <div class="divtxtBox LNOrient" style="  width: 155px">
                    <asp:DropDownList ID="ddlState" AutoPostBack="True" TabIndex="9" OnSelectedIndexChanged="ddlState_SelectedIndexChanged"
                        runat="server" CssClass="EntryFont validate[]   ddlStype">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
                <%-- <div class="lblBox LNOrient" style="  width: 200px" id="lblAddress3">
                Address 3
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:TextBox ID="txtAddress3" MaxLength="100" TabIndex="8"  CssClass="EntryFont validate txtBox" runat="server"
                    ></asp:TextBox>
            </div>--%>
                <div class="lblBox LNOrient" style="  width: 200px" id="lblCity">
                    City
                </div>
                <div class="divtxtBox LNOrient" style="  width: 155px">
                    <asp:DropDownList ID="ddlCity" runat="server" TabIndex="10" CssClass="EntryFont validate[] ddlStype"
                        >
                    </asp:DropDownList>
                </div>
                 <div class="colspace  LNOrient" >
                &nbsp</div>
                 <div class="lblBox LNOrient" style="  width: 195px" id="lblPostalCode">
                    Postal Code
                </div>
                <div class="divtxtBox LNOrient" style="  width: 150px">
                    <asp:TextBox ID="txtPostalCode" MaxLength="50" TabIndex="11" CssClass="EntryFont validate[] txtBox"
                        runat="server"></asp:TextBox>
                    <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars=""
                        FilterType="Numbers,Custom" TargetControlID="txtPostalCode" />
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer">
               
                <div class="lblBox LNOrient" style="  width: 200px" id="lblPhone">
                    Phone
                </div>
                <div class="divtxtBox LNOrient" style="  width: 150px">
                    <asp:TextBox ID="txtPhone" CssClass="EntryFont validate[] txtBox_N" TabIndex="12" runat="server"
                        MaxLength="20"></asp:TextBox>
                    <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars="+,-"
                        FilterType="Numbers,Custom" TargetControlID="txtPhone" />
                </div>
            </div>
            <div class="divClear_10">
            </div>
            <div class="divRowContainer divAction">
                <table class="SaveTable">
                    <tr>
                        <td>
                            <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn" />
                        </td>
                        <td>
                            <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" />
                        </td>
                        <td>
                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" />
                        </td>
                        <td>
                            <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" />
                        </td>
                    </tr>
                </table>
            </div>
            <div id="divDelete">
                <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                    PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
                </cc2:ModalPopupExtender>
                <asp:Panel ID="pnlConfirm" runat="server">
                    <div class="ConfirmForm">
                        <table>
                            <tr class="ConfirmHeading" style="width: 100%">
                                <td>
                                    <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                        Width="60px" />
                                    &nbsp;
                                    <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </asp:Panel>
            </div>
        </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/PER/PAChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () { $("#form1").validationEngine(); });
        $("#FINContent_btnSave").click(function () {
            if ($("#form1").validationEngine('validate') == false)
                return false;
        });
    </script>
</asp:Content>
