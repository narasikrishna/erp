﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.DAL.PER;
using FIN.DAL.GL;
using FIN.BLL;
using FIN.BLL.GL;
using FIN.BLL.PER;
using VMVServices.Web;

namespace FIN.Client.PER
{
    public partial class PayrolladjustmentEntry : PageBase
    {
        PAY_NR_ELEMENTS_HDR pAY_NR_ELEMENTS_HDR = new PAY_NR_ELEMENTS_HDR();

        DataTable dtData = new DataTable();
        Boolean bol_rowVisiable;
        Boolean savedBool;
        DataTable dtGridData = new DataTable();
        string ProReturn = null;


        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));


            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
        }

        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        private void FillComboBox()
        {
            FIN.BLL.PER.PayrollPeriods_BLL.fn_GetPayrollPeriods(ref ddlPayrollperiod);
            //FIN.BLL.PER.PayrollElements_BLL.fn_getElement(ref ddlElement);
            FIN.BLL.PER.PayrollElements_BLL.getPayElements_Recuringonly(ref ddlElement);

        }


        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();

                EntityData = null;
                // Session[FINSessionConstants.GridData] = null;

                FillComboBox();

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    pAY_NR_ELEMENTS_HDR = FIN.BLL.PER.NONrecurringElement_BLL.getClassEntity(Master.StrRecordId);
                    EntityData = pAY_NR_ELEMENTS_HDR;
                    ddlPayrollperiod.SelectedValue = pAY_NR_ELEMENTS_HDR.PAY_PERIOD_ID.ToString();

                    ddlElement.SelectedValue = pAY_NR_ELEMENTS_HDR.PAY_ELEMENT_ID.ToString();


                }

                fn_getNonRecElementEmployee();


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Vaca_Entry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);

                }
            }

        }

        private void BindGrid(DataTable dtData)
        {
            try
            {
                if (dtData.Rows.Count > 0)
                {
                    //if (dtData.Rows[0]["PAY_DED_AMOUNT"].ToString().Length > 0)
                    //{
                    dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("PAY_DED_AMOUNT", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("PAY_DED_AMOUNT"))));
                    dtData.AcceptChanges();
                    // }
                }
                gvData.DataSource = dtData;
                gvData.DataBind();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        #endregion
        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                AssignToBE();

                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);
                    btnSave.Attributes.Add("onclick", "reload();");
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    pAY_NR_ELEMENTS_HDR = (PAY_NR_ELEMENTS_HDR)EntityData;
                }

                pAY_NR_ELEMENTS_HDR.PAY_PERIOD_ID = (ddlPayrollperiod.SelectedValue.ToString());
                pAY_NR_ELEMENTS_HDR.PAY_ELEMENT_ID = ddlElement.SelectedValue.ToString();
                pAY_NR_ELEMENTS_HDR.ENABLED_FLAG = FINAppConstants.Y;
                pAY_NR_ELEMENTS_HDR.ATTRIBUTE1 = "PAYROLLADJUSTMENT";
                pAY_NR_ELEMENTS_HDR.PAY_ORG_ID = VMVServices.Web.Utils.OrganizationID;
                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != string.Empty)
                {
                    pAY_NR_ELEMENTS_HDR.MODIFIED_BY = this.LoggedUserName;
                    pAY_NR_ELEMENTS_HDR.MODIFIED_DATE = DateTime.Today;
                }
                else
                {
                    pAY_NR_ELEMENTS_HDR.PAY_NR_ID = FINSP.GetSPFOR_SEQCode("PER_015_M", false, true);
                    pAY_NR_ELEMENTS_HDR.CREATED_BY = this.LoggedUserName;
                    pAY_NR_ELEMENTS_HDR.CREATED_DATE = DateTime.Today;
                }

                //Save Detail Table

                pAY_NR_ELEMENTS_HDR.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, pAY_NR_ELEMENTS_HDR.PAY_NR_ID.ToString());


                var tmpChildEntity = new List<Tuple<object, string>>();

                PAY_NR_ELEMENTS_DTL pAY_NR_ELEMENTS_DTL = new PAY_NR_ELEMENTS_DTL();
                for (int iLoop = 0; iLoop < gvData.Rows.Count; iLoop++)
                {
                    pAY_NR_ELEMENTS_DTL = new PAY_NR_ELEMENTS_DTL();
                    if (gvData.DataKeys[iLoop].Values["PAY_NR_DTL_ID"].ToString() != "0")
                    {
                        pAY_NR_ELEMENTS_DTL = NONrecurringElement_BLL.getChildClassEntity(gvData.DataKeys[iLoop].Values["PAY_NR_DTL_ID"].ToString());
                    }
                    TextBox txt_Amount = (TextBox)gvData.Rows[iLoop].FindControl("txtAmount");
                    TextBox txt_Remarks = (TextBox)gvData.Rows[iLoop].FindControl("txtRemarks");
                    if (txt_Amount.Text.ToString().Length > 0)
                    {
                        pAY_NR_ELEMENTS_DTL.PAY_EMP_DEPT_ID = gvData.DataKeys[iLoop].Values["DEPT_ID"].ToString();
                        pAY_NR_ELEMENTS_DTL.PAY_EMP_ID = gvData.DataKeys[iLoop].Values["EMP_ID"].ToString();
                        pAY_NR_ELEMENTS_DTL.PAY_DED_AMOUNT = decimal.Parse(txt_Amount.Text);
                        pAY_NR_ELEMENTS_DTL.PAY_DED_REMARKS = txt_Remarks.Text;
                        pAY_NR_ELEMENTS_DTL.ENABLED_FLAG = FINAppConstants.Y;
                        pAY_NR_ELEMENTS_DTL.WORKFLOW_COMPLETION_STATUS = FINAppConstants.Y;
                        pAY_NR_ELEMENTS_DTL.PAY_NR_ID = pAY_NR_ELEMENTS_HDR.PAY_NR_ID;
                        if (gvData.DataKeys[iLoop].Values["PAY_NR_DTL_ID"].ToString() != "0")
                        {
                            pAY_NR_ELEMENTS_DTL.PAY_NR_DTL_ID = gvData.DataKeys[iLoop].Values["PAY_NR_DTL_ID"].ToString();
                            pAY_NR_ELEMENTS_DTL.MODIFIED_BY = this.LoggedUserName;
                            pAY_NR_ELEMENTS_DTL.MODIFIED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(pAY_NR_ELEMENTS_DTL, FINAppConstants.Update));
                        }
                        else
                        {
                            pAY_NR_ELEMENTS_DTL.PAY_NR_DTL_ID = FINSP.GetSPFOR_SEQCode("PER_015_D", false, true);
                            pAY_NR_ELEMENTS_DTL.CREATED_BY = this.LoggedUserName;
                            pAY_NR_ELEMENTS_DTL.CREATED_DATE = DateTime.Today;
                            tmpChildEntity.Add(new Tuple<object, string>(pAY_NR_ELEMENTS_DTL, FINAppConstants.Add));
                        }
                    }


                }

                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {

                            CommonUtils.SavePCEntity<PAY_NR_ELEMENTS_HDR, PAY_NR_ELEMENTS_DTL>(pAY_NR_ELEMENTS_HDR, tmpChildEntity, pAY_NR_ELEMENTS_DTL);
                            savedBool = true;
                            break;
                        }
                    case FINAppConstants.Update:
                        {
                            CommonUtils.SavePCEntity<PAY_NR_ELEMENTS_HDR, PAY_NR_ELEMENTS_DTL>(pAY_NR_ELEMENTS_HDR, tmpChildEntity, pAY_NR_ELEMENTS_DTL, true);
                            savedBool = true;
                            break;
                        }
                }


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// .RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {



            }
            catch (Exception ex)
            {
                ErrorCollection.Add("CATG_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion

        protected void ddlElement_SelectedIndexChanged(object sender, EventArgs e)
        {
            fn_ChkDatafound();
            fn_getNonRecElementEmployee();
        }
        private void fn_ChkDatafound()
        {
            PAY_NR_ELEMENTS_HDR obj_PAY_NR_ELEMENTS_HDR = new PAY_NR_ELEMENTS_HDR();
            using (IRepository<PAY_NR_ELEMENTS_HDR> userCtx = new DataRepository<PAY_NR_ELEMENTS_HDR>())
            {
                obj_PAY_NR_ELEMENTS_HDR = userCtx.Find(r =>
                    (r.PAY_PERIOD_ID == ddlPayrollperiod.SelectedValue.ToString() && r.PAY_ELEMENT_ID == ddlElement.SelectedValue.ToString())
                    ).SingleOrDefault();
            }
            if (obj_PAY_NR_ELEMENTS_HDR != null)
            {
                Master.Mode = FINAppConstants.Update;
                Master.StrRecordId = obj_PAY_NR_ELEMENTS_HDR.PAY_NR_ID;
                EntityData = obj_PAY_NR_ELEMENTS_HDR;
            }
        }
        private void fn_getNonRecElementEmployee()
        {



            if (ddlPayrollperiod.SelectedValue.ToString().Length > 0)
            {
                if (ddlElement.SelectedValue.ToString().Length > 0)
                {
                    DataTable dt_empList = FIN.BLL.PER.NONrecurringElement_BLL.fn_getNonRecEmplist(ddlElement.SelectedValue, ddlPayrollperiod.SelectedValue);
                    BindGrid(dt_empList);
                }
            }
        }

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }


    }
}