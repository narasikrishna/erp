﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.BLL;
using VMVServices.Web;
using FIN.BLL.PER;
using FIN.DAL.PER;

namespace FIN.Client.PER
{
    public partial class PayrollEmployeeStopResumeEntry : PageBase
    {
        DataTable dt_employee_data = new DataTable();

        DataTable dt_elements_data = new DataTable();
        PAY_EMP_STOP_RESUME_PAY pAY_EMP_STOP_RESUME_PAY = new PAY_EMP_STOP_RESUME_PAY();
        HR_EMPLOYEES hr_employees = new HR_EMPLOYEES();
        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
            }

            if (VMVServices.Web.Utils.AddAllowed != FINAppConstants.Y && Master.Mode == FINAppConstants.Add)
            {
                btnSave.Visible = false;
            }

        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();

                EntityData = null;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    lblAllElements.Visible = true;
                    divch.Visible = true;
                    div_Resume.Visible = true;
                    using (IRepository<PAY_EMP_STOP_RESUME_PAY> userCtx = new DataRepository<PAY_EMP_STOP_RESUME_PAY>())
                    {
                        pAY_EMP_STOP_RESUME_PAY = userCtx.Find(r =>
                            (r.PAY_STOP_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = pAY_EMP_STOP_RESUME_PAY;

                    ddlEmployeeNo.SelectedValue = pAY_EMP_STOP_RESUME_PAY.EMP_ID.ToString();
                    //txtEmployeeName.Text 
                    FillElement();

                    DataTable dtnew = new DataTable();
                    dtnew = DBMethod.ExecuteQuery(PayrollEmployeeStopResume_DAL.getstopresume(ddlEmployeeNo.SelectedValue.ToString())).Tables[0];
                    if (dtnew.Rows.Count > 0)
                    {
                        if (dtnew.Rows[0]["STOP_PAYTOLL"].ToString() == "1")
                        {
                            chkall.Checked = true;
                            ddlElement.Enabled = false;
                        }
                        else
                        {
                            chkall.Checked = false;
                            ddlElement.Enabled = true;
                        }
                    }

                    
                    if (pAY_EMP_STOP_RESUME_PAY.EMP_STOP_FROM_DT != null)
                    {
                        txtFromDate.Text = DBMethod.ConvertDateToString(pAY_EMP_STOP_RESUME_PAY.EMP_STOP_FROM_DT.ToString());
                    }
                    if (pAY_EMP_STOP_RESUME_PAY.EMP_STOP_TO_DT != null)
                    {
                        txtToDate.Text = DBMethod.ConvertDateToString(pAY_EMP_STOP_RESUME_PAY.EMP_STOP_TO_DT.ToString());
                    }

                    if (pAY_EMP_STOP_RESUME_PAY.STOP_RESUME == FINAppConstants.EnabledFlag)
                    {
                        chkstopresume.Checked = true;
                    }
                    else
                    {
                        chkstopresume.Checked = false;
                    }
                    if (pAY_EMP_STOP_RESUME_PAY.ELEMENT_ID != null)
                    {
                        ddlElement.SelectedValue = pAY_EMP_STOP_RESUME_PAY.ELEMENT_ID.ToString();
                    }
                    ddlStopReason.SelectedValue = pAY_EMP_STOP_RESUME_PAY.PAY_STOP_REASON.ToString();

                    fn_fill_Element();
                    //txtRemarks.Text = pAY_EMP_STOP_RESUME_PAY.
                    if (pAY_EMP_STOP_RESUME_PAY.RESUME_FLAG == FINAppConstants.EnabledFlag)
                    {
                        chkResume.Checked = true;
                    }
                    else
                    {
                        chkResume.Checked = false;
                    }
                    if (pAY_EMP_STOP_RESUME_PAY.RESUME_DATE != null)
                    {
                        txtResumeDate.Text = DBMethod.ConvertDateToString(pAY_EMP_STOP_RESUME_PAY.RESUME_DATE.ToString());
                    }
                    if (pAY_EMP_STOP_RESUME_PAY.ATTRIBUTE1 != null)
                    {
                        txtRemarks.Text = pAY_EMP_STOP_RESUME_PAY.ATTRIBUTE1.ToString();
                    }
                    if (pAY_EMP_STOP_RESUME_PAY.ATTRIBUTE2 != null)
                    {
                        txtResumeRemarks.Text = pAY_EMP_STOP_RESUME_PAY.ATTRIBUTE2.ToString();
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PESR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    pAY_EMP_STOP_RESUME_PAY = (PAY_EMP_STOP_RESUME_PAY)EntityData;
                }
                
                pAY_EMP_STOP_RESUME_PAY.EMP_ID = ddlEmployeeNo.SelectedValue.ToString();
                if (txtFromDate.Text != string.Empty)
                {
                    pAY_EMP_STOP_RESUME_PAY.EMP_STOP_FROM_DT = DBMethod.ConvertStringToDate(txtFromDate.Text.ToString());
                }

                if (txtResumeDate.Text != string.Empty)
                {
                    txtToDate.Text = txtResumeDate.Text;
                    pAY_EMP_STOP_RESUME_PAY.RESUME_DATE = DBMethod.ConvertStringToDate(txtResumeDate.Text.ToString());
                }

                if (txtToDate.Text != string.Empty)
                {
                    pAY_EMP_STOP_RESUME_PAY.EMP_STOP_TO_DT = DBMethod.ConvertStringToDate(txtToDate.Text.ToString());
                }

                pAY_EMP_STOP_RESUME_PAY.EMP_ALL_ELEMENTS = FINAppConstants.DisabledFlag;

                pAY_EMP_STOP_RESUME_PAY.STOP_RESUME = chkstopresume.Checked == true ? FINAppConstants.EnabledFlag : FINAppConstants.DisabledFlag;

                pAY_EMP_STOP_RESUME_PAY.ELEMENT_ID = ddlElement.SelectedValue.ToString();

                pAY_EMP_STOP_RESUME_PAY.PAY_STOP_REASON = ddlStopReason.SelectedValue.ToString();

                pAY_EMP_STOP_RESUME_PAY.ATTRIBUTE1 = txtRemarks.Text.ToString();
               
                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    pAY_EMP_STOP_RESUME_PAY.RESUME_FLAG = FINAppConstants.EnabledFlag;
                }
                else
                {
                    pAY_EMP_STOP_RESUME_PAY.RESUME_FLAG = FINAppConstants.DisabledFlag;
                }

                pAY_EMP_STOP_RESUME_PAY.ENABLED_FLAG = "1";

                if (txtResumeDate.Text != string.Empty)
                {
                    pAY_EMP_STOP_RESUME_PAY.RESUME_DATE = DBMethod.ConvertStringToDate(txtResumeDate.Text.ToString());
                }

                using (IRepository<HR_EMPLOYEES> userCtx = new DataRepository<HR_EMPLOYEES>())
                {
                    hr_employees = userCtx.Find(r =>
                        (r.EMP_ID == ddlEmployeeNo.SelectedValue)
                        ).SingleOrDefault();
                }

                pAY_EMP_STOP_RESUME_PAY.ATTRIBUTE2 = txtResumeRemarks.Text.ToString();

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    pAY_EMP_STOP_RESUME_PAY.MODIFIED_BY = this.LoggedUserName;
                    pAY_EMP_STOP_RESUME_PAY.MODIFIED_DATE = DateTime.Today;
                    if (pAY_EMP_STOP_RESUME_PAY.RESUME_DATE != null)
                    {
                        if (hr_employees != null)
                        {
                            hr_employees.STOP_PAYTOLL = "0";
                        }
                    }
                }
                else
                {
                    pAY_EMP_STOP_RESUME_PAY.PAY_STOP_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.PER_007.ToString(), false, true);
                    //tAX_TERMS.WORKFLOW_COMPLETION_STATUS = "1";
                    pAY_EMP_STOP_RESUME_PAY.CREATED_BY = this.LoggedUserName;
                    pAY_EMP_STOP_RESUME_PAY.CREATED_DATE = DateTime.Today;
                }
                pAY_EMP_STOP_RESUME_PAY.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, pAY_EMP_STOP_RESUME_PAY.PAY_STOP_ID);
                pAY_EMP_STOP_RESUME_PAY.PAY_ORG_ID = VMVServices.Web.Utils.OrganizationID;

                
                if (hr_employees != null)
                {
                    if (chkall.Checked == true)
                    {
                        hr_employees.STOP_PAYTOLL = "1";
                    }
                    DBMethod.SaveEntity<HR_EMPLOYEES>(hr_employees, true);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PRSRE", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void FillComboBox()
        {
            //PayrollEmployeeStopResume.GetEmployeeName(ref ddlEmployeeNo);
            FIN.BLL.HR.Employee_BLL.GetEmployeeName(ref ddlEmployeeNo);
            
            FIN.BLL.Lookup_BLL.GetLookUpValues(ref ddlStopReason, "SR");

        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                System.Collections.SortedList slControls = new System.Collections.SortedList();
                slControls[0] = ddlEmployeeNo;
                slControls[1] = txtFromDate;
                //slControls[2] = ddlElement;
                slControls[2] = ddlStopReason;     

                ErrorCollection.Clear();
                string strCtrlTypes = "DropDownList~TextBox~DropDownList";
                string strMessage = "Employee No ~ From Date ~ Stop Reason";
                ErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);
                if (ErrorCollection.Count > 0)
                    return;



                AssignToBE();


                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            DBMethod.SaveEntity<PAY_EMP_STOP_RESUME_PAY>(pAY_EMP_STOP_RESUME_PAY);
                            //  DisplaySaveCompleteMessage(Master.ListPageToOpen);

                            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                            break;
                        }
                    case FINAppConstants.Update:
                        {

                            DBMethod.SaveEntity<PAY_EMP_STOP_RESUME_PAY>(pAY_EMP_STOP_RESUME_PAY, true);
                            //  DisplaySaveCompleteMessage(Master.ListPageToOpen);
                            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                            break;

                        }
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion

        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                DBMethod.DeleteEntity<PAY_EMP_STOP_RESUME_PAY>(pAY_EMP_STOP_RESUME_PAY);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PESRE", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void ddlEmployeeNo_SelectedIndexChanged(object sender, EventArgs e)
        {

            FillElement();
        }

        private void FillElement()
        {
            PayrollEmployeeStopResume.fn_getPayElements(ref ddlElement,ddlEmployeeNo.SelectedValue);
        }
        protected void ddlElement_SelectedIndexChanged(object sender, EventArgs e)
        {
            fn_fill_Element();   
        }

       

        private void fn_fill_Element()
        {
            dt_elements_data = DBMethod.ExecuteQuery(FIN.DAL.PER.PayrollEmployeeStopResume_DAL.getElements(ddlElement.SelectedValue.ToString())).Tables[0];

            if (dt_elements_data != null)
            {
                if (dt_elements_data.Rows.Count > 0)
                {
                    txtDescription.Text = dt_elements_data.Rows[0][0].ToString();
                    txtDescription.Enabled = false;
                }
            }

        }

        protected void chkall_CheckedChanged(object sender, EventArgs e)
        {
            if (chkall.Checked == true)
            {
                ddlElement.Enabled = false;
            }
            else
            {
                ddlElement.Enabled = true;
            }

        }


    }
}