﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="PayrollTrailrunDetailsEntry.aspx.cs" Inherits="FIN.Client.PER.PayrollTrailrunDetailsEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(function () {
            $(document).ready(function () {
                $("#divListDept").slideUp();
            });
            $("#ListDeptDown").click(function (event) {
                event.preventDefault();
                $("#divListDept").slideDown();
                $("#ListDeptUp").css("display", "inline");
                $("#ListDeptDown").css("display", "none");
            });

            $("#ListDeptUp").click(function (event) {
                event.preventDefault();
                $("#divListDept").slideUp();
                $("#ListDeptUp").css("display", "none");
                $("#ListDeptDown").css("display", "inline");
            });
        });
    </script>
    <script type="text/javascript">
        window.onscroll = function () {

            var scrollY = document.body.scrollTop;

            if (scrollY == 0) {
                if (window.pageYOffset) {
                    scrollY = window.pageYOffset;
                }
                else {
                    scrollY = (document.body.parentElement) ? document.body.parentElement.scrollTop : 0;
                }
            }
            if (scrollY > 0) {
                $("#FINContent_hf_ScrollPos").val(scrollY);
            }
        };
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1000px" id="divMainContainer">
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 100px" id="lblPayrollPeriod">
                Payroll Period
            </div>
            <div class="divtxtBox LNOrient" style="  width: 200px">
                <asp:DropDownList ID="ddlPayrollPeriod" runat="server" TabIndex="1" CssClass="validate[required] EntryFont RequiredField ddlStype"
                    AutoPostBack="True" OnSelectedIndexChanged="ddlPayrollPeriod_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div class="colspace" style="float: left">
            </div>
            <div class="lblBox LNOrient" style="  width: 100px" id="lblGroup">
                Group
            </div>
            <div class="divtxtBox LNOrient" style="  width: 200px">
                <asp:DropDownList ID="ddlGroup" runat="server" CssClass="ddlStype" TabIndex="2" AutoPostBack="True"
                    OnSelectedIndexChanged="ddlGroup_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div class="colspace" style="float: left">
            </div>
            <div class="lblBox LNOrient" style="  width: 100px" id="lblEffectiveStartDate">
                Payroll Date
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:TextBox ID="txtPayrollDate" CssClass="validate[required]  RequiredField  txtBox"
                    runat="server" TabIndex="3"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calExtenderd1" TargetControlID="txtPayrollDate"
                    OnClientDateSelectionChanged="checkDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <br />
        <div class="GridHeader" style="height: 30px; vertical-align: top">
            <div class="lblBox LNOrient" style="  color: White; font-weight: bold" id="lblListDepartment">
                Department
            </div>
            <div style="float: right; padding-right: 10px">
                <asp:CheckBox runat="server" Checked="true" Text="ALL" ID="chkAlldept" CssClass="lblBox LNOrient"
                    TabIndex="4" Style="color: White" OnCheckedChanged="chkAlldept_CheckedChanged"
                    AutoPostBack="True" />
                &nbsp;
                <img alt="Down" src="../Images/next.png" width="25px" height="25px" id="ListDeptDown" />
                <img alt="Up" src="../Images/previous.png" width="25px" height="25px" id="ListDeptUp"
                    style="display: none" />
            </div>
        </div>
        <div class="divRowContainer" id="divListDept" style="border: 1px solid black">
            <asp:CheckBoxList ID="cblDeptName" runat="server" CellSpacing="5" CssClass="lblBox LNOrient"
                RepeatColumns="3" TabIndex="5" RepeatDirection="Horizontal" Width="98%">
            </asp:CheckBoxList>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" align="right">
            <asp:ImageButton ID="btnSave" runat="server" ImageUrl="~/Images/process.png" OnClick="btnSave_Click"
                ToolTip="View" Style="border: 0px;" />
            <%-- <asp:Button ID="btnSave" runat="server" Text="Process" OnClick="btnSave_Click" CssClass="btn" />--%>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" style="display: none">
            <div class="lblBox LNOrient" style="  width: 100px; display: none" id="lblPayrollStatus">
                Payroll Status
            </div>
            <div class="divtxtBox LNOrient" style="  width: 200px; display: none">
                <asp:DropDownList ID="ddlPayrollStatus" TabIndex="6" runat="server" CssClass=" ddlStype">
                </asp:DropDownList>
            </div>
            <div class="lblBox LNOrient" style="  width: 100px" id="lblPayrollTotal">
                Total Payment
            </div>
            <div class="divtxtBox LNOrient" style="  width: 200px">
                <asp:TextBox ID="txtPayrollTotal" CssClass="validate[required] RequiredField txtBox"
                    Enabled="true" MaxLength="13" runat="server" TabIndex="7"></asp:TextBox>
            </div>
             <div class="colspace  LNOrient" >
                &nbsp</div>
            <div class="lblBox LNOrient" style="  width: 100px" id="lblDeductionTotal">
                Total Deduction
            </div>
            <div class="divtxtBox LNOrient" style="  width: 200px">
                <asp:TextBox ID="txtDeductionTotal" CssClass="validate[required] RequiredField txtBox"
                    Enabled="true" MaxLength="13" runat="server" TabIndex="8"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="  width: 100px" id="Div1">
                Remarks
            </div>
            <div class="divtxtBox LNOrient" style="  width: 600px; height: 50px;">
                <asp:TextBox ID="txtRemarks" CssClass="txtBox" Style="height: 50px;" runat="server"
                    TabIndex="9"></asp:TextBox>
            </div>
            <div align="right">
                <asp:ImageButton ID="btnApprove" runat="server" ImageUrl="~/Images/btnApprove.png"
                    OnClick="btnApprove_Click" Visible="false" Style="border: 0px;" />
                <%--<asp:Button ID="btnApprove" runat="server" Text="Approve" CssClass="btn" OnClick="btnApprove_Click"
                    Visible="false" />--%>
            </div>
            <asp:HiddenField ID="HfPayrollid" runat="server" />
        </div>
        <div class="divClear_10">
        </div>
        <div class="divClear_10">
            <asp:HiddenField ID="hf_WF" runat="server" Value="0" />
        </div>
        <div class="LNOrient">
            <asp:GridView ID="gvGropWise" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                Width="80%" DataKeyNames="PAYROLL_ID" ShowFooter="false">
                <Columns>
                    <asp:TemplateField HeaderText="Group">
                        <ItemTemplate>
                            <asp:Label ID="Group" Width="130px" runat="server" Text='<%# Eval("PAY_GROUP_DESC") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Final Status">
                        <ItemTemplate>
                            <asp:Label ID="lblStatus" Width="130px" runat="server" Text='<%# Eval("PAYROLL_STATUS") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Send Approve" ItemStyle-Width="50px" ItemStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                             <asp:ImageButton ID="btnSendApprove" runat="server" ImageUrl="~/Images/btnSendForApprove.png" Visible='<%# Convert.ToBoolean(Eval("APPROVE_SEND")) %>'
                                OnClick="btnSendApprove_Click" Style="border: 0px;" />
                            <%--<asp:Button ID="btnSendApprove" runat="server" CssClass="btn" Visible='<%# Convert.ToBoolean(Eval("APPROVE_SEND")) %>'
                                Text="Send For Approve" OnClick="btnSendApprove_Click" />--%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Details" ItemStyle-Width="50px" ItemStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <asp:ImageButton ID="btnGroupDetails" runat="server" ImageUrl="~/Images/details.png"
                                OnClick="btnGroupDetails_Click" Style="border: 0px;" />
                            <%--<asp:Button ID="btnGroupDetails" runat="server" CssClass="btn" Text="Details" OnClick="btnGroupDetails_Click" />--%>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divClear_10">
        </div>
        <div class="LNOrient">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                Width="80%" DataKeyNames="PAYROLL_DTL_ID,PAYROLL_ID,PAY_DEPT_ID,WF" ShowFooter="false">
                <Columns>
                    <asp:TemplateField HeaderText="Department">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtDepartmentName" Width="130px" MaxLength="100" runat="server"
                                Enabled="false" CssClass=" RequiredField  txtBox" Text='<%# Eval("dept_name") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtDepartmentName" Width="130px" MaxLength="100" runat="server"
                                Enabled="false" Visible="false" CssClass="RequiredField   txtBox"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblDepartmentName" Width="130px" runat="server" Text='<%# Eval("dept_name") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Payment Amount">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtAmount" Width="130px" MaxLength="100" runat="server" CssClass=" RequiredField txtBox_N"
                                Enabled="false" Text='<%# Eval("pay_amount") %>'></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars=".,"
                                FilterType="Numbers,Custom" TargetControlID="txtAmount" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtAmount" Width="130px" MaxLength="100" runat="server" Visible="false"
                                CssClass="RequiredField   txtBox_N" Enabled="false"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars=".,"
                                FilterType="Numbers,Custom" TargetControlID="txtAmount" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblAmount" Width="130px" runat="server" Text='<%# Eval("pay_amount") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Deduction Amount">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtDeductionAmount" Width="130px" MaxLength="100" runat="server"
                                CssClass=" RequiredField  txtBox" Text='<%# Eval("pay_ded_amount") %>'></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars=".,"
                                FilterType="Numbers,Custom" TargetControlID="txtDeductionAmount" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtDeductionAmount" Width="130px" MaxLength="100" runat="server"
                                Visible="false" CssClass="RequiredField   txtBox"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars=".,"
                                FilterType="Numbers,Custom" TargetControlID="txtDeductionAmount" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblDeductionAmount" Width="130px" runat="server" Text='<%# Eval("pay_ded_amount") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Net Amount">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtNetAmount" Width="130px" MaxLength="100" runat="server" CssClass=" RequiredField  txtBox"
                                Text='<%# Eval("pay_net_amount") %>'></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars=".,"
                                FilterType="Numbers,Custom" TargetControlID="txtNetAmount" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtNetAmount" Width="130px" MaxLength="100" runat="server" Visible="false"
                                CssClass="RequiredField   txtBox"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars=".,"
                                FilterType="Numbers,Custom" TargetControlID="txtNetAmount" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblNetAmount" Width="130px" runat="server" Text='<%# Eval("pay_net_amount") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Element" ItemStyle-Width="50px" ItemStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <asp:ImageButton ID="btnDetails" runat="server" ImageUrl="~/Images/details.png" OnClick="btnDetails_Click"
                                Style="border: 0px;" />
                            <%--<asp:Button ID="btnDetails" runat="server" CssClass="btn" Text="Details" OnClick="btnDetails_Click" />--%>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="btnDetails" runat="server" ImageUrl="~/Images/details.png" OnClick="btnDetails_Click"
                                Style="border: 0px;" />
                            <%--<asp:Button ID="btnDetails" runat="server" CssClass="btn" Text="Details" OnClick="btnDetails_Click" />--%>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="btnDetails" runat="server" ImageUrl="~/Images/details.png" Visible="false"
                                OnClick="btnDetails_Click" Style="border: 0px;" />
                            <%--<asp:Button ID="btnDetails" runat="server" CssClass="btn" Text="Details" OnClick="btnDetails_Click" 
                                Visible="false" />--%>
                        </FooterTemplate>
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divClear_10">
        </div>
        <div class="LNOrient">
            <asp:GridView ID="gvEmpDetails" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                Visible="false" Width="80%" DataKeyNames="PAYROLL_DTL_ID,EMP_ID" ShowFooter="false">
                <Columns>
                    <asp:TemplateField HeaderText="Employee Name">
                        <ItemTemplate>
                            <asp:Label ID="lblEmpName" Width="130px" runat="server" Text='<%# Eval("EMP_NAME") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Employee Number">
                        <ItemTemplate>
                            <asp:Label ID="lblEmpNumber" Width="130px" runat="server" Text='<%# Eval("EMP_NO") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Payment Amount">
                        <ItemTemplate>
                            <asp:Label ID="lblAmount" Width="130px" runat="server" Text='<%# Eval("pay_amount") %>'></asp:Label>
                            <%--<asp:TextBox ID="txtPayAmount" Width="130px" MaxLength="100" runat="server" CssClass="txtBox_N"
                                Enabled="false" Text='<%# Eval("pay_amount") %>'></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars=".,"
                                FilterType="Numbers,Custom" TargetControlID="txtPayAmount" />--%>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Deduction Amount">
                        <ItemTemplate>
                            <asp:Label ID="lblDeductionAmount" Width="130px" runat="server" Text='<%# Eval("pay_ded_amount") %>'></asp:Label>
                            <%--<asp:TextBox ID="txtDedAmount" Width="130px" MaxLength="100" runat="server" CssClass="txtBox_N"
                                Enabled="false" Text='<%# Eval("pay_ded_amount") %>'></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars=".,"
                                FilterType="Numbers,Custom" TargetControlID="txtDedAmount" />--%>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Net Amount">
                        <ItemTemplate>
                            <asp:Label ID="lblNetAmount" Width="130px" runat="server" Text='<%# Eval("pay_net_amount") %>'></asp:Label>
                            <%--<asp:TextBox ID="txtDedAmount" Width="130px" MaxLength="100" runat="server" CssClass="txtBox_N"
                                Enabled="false" Text='<%# Eval("pay_ded_amount") %>'></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars=".,"
                                FilterType="Numbers,Custom" TargetControlID="txtDedAmount" />--%>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Element" ItemStyle-Width="50px" ItemStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <asp:ImageButton ID="btnEmpDetails" runat="server" ImageUrl="~/Images/details.png"
                                OnClick="btnEmpDetails_Click" Style="border: 0px;" />
                            <%-- <asp:Button ID="btnEmpDetails" runat="server" CssClass="btn" Text="Details" OnClick="btnEmpDetails_Click" />--%>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divClear_10">
        </div>
        <div id="divPopUp">
            <asp:HiddenField ID="btnDetails1" runat="server" />
            <cc2:ModalPopupExtender ID="ModalPopupExtender2" runat="server" TargetControlID="btnDetails1"
                PopupControlID="panelDetailsPopup" CancelControlID="btnCancel1" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="panelDetailsPopup" runat="server" BackColor="White" Width="90%" Height="90%"
                ScrollBars="Auto">
                <div class="divClear_10">
                </div>
                <div class="ConfirmForm">
                    <div class="divRowContainer">
                        <div class="lblBox LNOrient" style="  font-weight: bold; width: 100px" id="Div2">
                            Employee
                        </div>
                        <div class="divtxtBox LNOrient" style="  width: 300px">
                            <asp:Label ID="lblEmp" runat="server" Text=""></asp:Label>
                        </div>
                        <div class="lblBox LNOrient" style="  font-weight: bold; width: 100px" id="Div12">
                            Department
                        </div>
                        <div class="divtxtBox LNOrient" style="  width: 200px">
                            <asp:Label ID="lbldept" runat="server" Text=""></asp:Label>
                        </div>
                    </div>
                    <div class="divClear_10">
                    </div>
                    <div class="divRowContainer">
                        <div class="lblBox LNOrient" style="  font-weight: bold; width: 100px" id="Div4">
                            Designation
                        </div>
                        <div class="divtxtBox LNOrient" style="  width: 300px">
                            <asp:Label ID="lblDesig" runat="server" Text=""></asp:Label>
                        </div>
                        <div class="lblBox LNOrient" style="  font-weight: bold; width: 100px" id="Div3">
                            Date of Join
                        </div>
                        <div class="divtxtBox LNOrient" style="  width: 200px">
                            <asp:Label ID="lblDoj" runat="server" Text=""></asp:Label>
                        </div>
                    </div>
                    <div class="divClear_10">
                    </div>
                    <div class="divRowContainer">
                        <table width="98%">
                            <tr>
                                <td>
                                    <div class="lblBox LNOrient" style="  font-weight: bold; width: 100px" id="Div8">
                                        Earnings
                                    </div>
                                </td>
                                <td>
                                    <div class="lblBox LNOrient" style="  font-weight: bold; width: 100px" id="Div9">
                                        Deduction
                                    </div>
                                </td>
                            </tr>
                            <tr valign="top">
                                <td style="width: 50%">
                                    <asp:GridView ID="gvEmployeePay" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                                        Width="98%" DataKeyNames="PAYROLL_DTL_ID,PAYROLL_DTL_DTL_ID,PAY_EMP_ELEMENT_ID,PAY_EMP_ID,pay_element_Type"
                                        OnRowDataBound="gvEmployeePay_RowDataBound" ShowFooter="false">
                                        <Columns>
                                            <asp:TemplateField Visible="false" HeaderText="Employee">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtEmployeeNo" MaxLength="100" runat="server" CssClass=" RequiredField  txtBox"
                                                        Text='<%# Eval("emp_name") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <FooterTemplate>
                                                    <asp:TextBox ID="txtEmployeeNo" Visible="false" MaxLength="100" runat="server" CssClass="RequiredField   txtBox"></asp:TextBox>
                                                </FooterTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblEmployeeNo" runat="server" Text='<%# Eval("emp_name") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                            <asp:TemplateField Visible="false" HeaderText="Type">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtElementType" Width="130px" MaxLength="100" runat="server" CssClass=" RequiredField  txtBox"
                                                        Text='<%# Eval("pay_element_Type") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <FooterTemplate>
                                                    <asp:TextBox ID="txtElementType" Width="130px" Visible="false" MaxLength="100" runat="server"
                                                        CssClass="RequiredField   txtBox"></asp:TextBox>
                                                </FooterTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblElementType" Width="130px" runat="server" Text='<%# Eval("pay_element_Type") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" Width="150px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Element">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtElement" Width="280px" MaxLength="100" runat="server" CssClass=" RequiredField  txtBox"
                                                        Text='<%# Eval("pay_element_desc") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <FooterTemplate>
                                                    <asp:TextBox ID="txtElement" Width="280px" Visible="false" MaxLength="100" runat="server"
                                                        CssClass="RequiredField   txtBox"></asp:TextBox>
                                                </FooterTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblElement" Width="280px" runat="server" Text='<%# Eval("pay_element_desc") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Amount">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtAmount" Width="97%" MaxLength="100" onkeyup="CalculateEar();"
                                                        runat="server" CssClass=" RequiredField  txtBox_N" Text='<%# Eval("pay_amount") %>'></asp:TextBox>
                                                    <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" ValidChars=".,"
                                                        FilterType="Numbers,Custom" TargetControlID="txtAmount" />
                                                </EditItemTemplate>
                                                <FooterTemplate>
                                                    <asp:TextBox ID="txtAmount" Width="97%" onkeyup="CalculateEar();" Visible="false"
                                                        MaxLength="100" runat="server" CssClass="RequiredField   txtBox_N"></asp:TextBox>
                                                    <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars=".,"
                                                        FilterType="Numbers,Custom" TargetControlID="txtAmount" />
                                                </FooterTemplate>
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtAmount" Width="97%" onkeyup="CalculateEar();" MaxLength="100"
                                                        runat="server" CssClass=" RequiredField  txtBox_N" Text='<%# Eval("pay_amount") %>'></asp:TextBox>
                                                </ItemTemplate>
                                                <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                        </Columns>
                                        <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                        <HeaderStyle CssClass="GridHeader" />
                                        <AlternatingRowStyle CssClass="GrdAltRow" />
                                    </asp:GridView>
                                </td>
                                <td style="width: 50%">
                                    <asp:GridView ID="gvEmployeePay2" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                                        OnRowDataBound="gvEmployeePay2_RowDataBound" Width="98%" DataKeyNames="PAYROLL_DTL_ID,PAYROLL_DTL_DTL_ID,PAY_EMP_ELEMENT_ID,PAY_EMP_ID,pay_element_Type"
                                        ShowFooter="false">
                                        <Columns>
                                            <asp:TemplateField Visible="false" HeaderText="Employee">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtEmployeeNo" Width="130px" MaxLength="100" runat="server" CssClass=" RequiredField  txtBox"
                                                        Text='<%# Eval("emp_name") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <FooterTemplate>
                                                    <asp:TextBox ID="txtEmployeeNo" Width="130px" Visible="false" MaxLength="100" runat="server"
                                                        CssClass="RequiredField   txtBox"></asp:TextBox>
                                                </FooterTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblEmployeeNo" Width="130px" runat="server" Text='<%# Eval("emp_name") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                            <asp:TemplateField Visible="false" HeaderText="Type">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtElementType" Width="130px" MaxLength="100" runat="server" CssClass=" RequiredField  txtBox"
                                                        Text='<%# Eval("pay_element_Type") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <FooterTemplate>
                                                    <asp:TextBox ID="txtElementType" Width="130px" Visible="false" MaxLength="100" runat="server"
                                                        CssClass="RequiredField   txtBox"></asp:TextBox>
                                                </FooterTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblElementType" Width="130px" runat="server" Text='<%# Eval("pay_element_Type") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" Width="150px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Element">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtElement" Width="280px" MaxLength="100" runat="server" CssClass=" RequiredField  txtBox"
                                                        Text='<%# Eval("pay_element_desc") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <FooterTemplate>
                                                    <asp:TextBox ID="txtElement" Width="280px" Visible="false" MaxLength="100" runat="server"
                                                        CssClass="RequiredField   txtBox"></asp:TextBox>
                                                </FooterTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblElement" Width="280px" runat="server" Text='<%# Eval("pay_element_desc") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Amount">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtAmount2" Width="97%" MaxLength="100" onkeyup="CalculateDed();"
                                                        runat="server" CssClass=" RequiredField  txtBox_N" Text='<%# Eval("pay_amount") %>'></asp:TextBox>
                                                    <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" ValidChars=".,"
                                                        FilterType="Numbers,Custom" TargetControlID="txtAmount" />
                                                </EditItemTemplate>
                                                <FooterTemplate>
                                                    <asp:TextBox ID="txtAmount2" Width="97%" onkeyup="CalculateDed();" Visible="false"
                                                        MaxLength="100" runat="server" CssClass="RequiredField   txtBox_N"></asp:TextBox>
                                                    <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" ValidChars=".,"
                                                        FilterType="Numbers,Custom" TargetControlID="txtAmount" />
                                                </FooterTemplate>
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtAmount2" Width="97%" onkeyup="CalculateDed();" MaxLength="100"
                                                        runat="server" CssClass=" RequiredField  txtBox_N" Text='<%# Eval("pay_amount") %>'></asp:TextBox>
                                                </ItemTemplate>
                                                <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                        </Columns>
                                        <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                        <HeaderStyle CssClass="GridHeader" />
                                        <AlternatingRowStyle CssClass="GrdAltRow" />
                                    </asp:GridView>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <div class="lblBox LNOrient" style="  font-weight: bold; width: 150px" id="Div16">
                                        Total Earnings
                                    </div>
                                    <div class="lblBox LNOrient" style="float: right; width: 100px; height: 50px" id="Div17">
                                        <asp:Label ID="lblTotalEar" runat="server"></asp:Label>
                                    </div>
                                </td>
                                <td align="right">
                                    <div class="lblBox LNOrient" style="  font-weight: bold; width: 150px; height: 50px"
                                        id="Div14">
                                        Total Deduction
                                    </div>
                                    <div class="lblBox LNOrient" style="float: right; width: 100px" id="Div15">
                                        <asp:Label ID="lblTotDed" runat="server"></asp:Label>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <div class="lblBox LNOrient" style="  font-weight: bold; width: 100px" id="Div11">
                                        Net Amount</div>
                                    <div class="lblBox LNOrient" style="float: right; width: 100px" id="Div13">
                                        <asp:Label ID="lblNet" runat="server" Text=""></asp:Label>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" colspan="2">
                                    <asp:Button runat="server" CssClass="button" ID="btnOkay" Text="Ok" Width="60px"
                                        OnClick="btnOkay_Click" />
                                    &nbsp;
                                    <asp:Button runat="server" CssClass="button" ID="btnCancel1" Text="Cancel" Width="60px" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </asp:Panel>
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="1" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="2" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="3" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table width="100%">
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
        <asp:HiddenField ID="hf_ScrollPos" runat="server" Value="0" />
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/PER/PAChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
            var scrollY = $("#FINContent_hf_ScrollPos").val()

            if (scrollY > 0) {
                window.scrollTo(0, scrollY);
            }

            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }
        function CalculateEar() {
            var grid = document.getElementById('<%=gvEmployeePay.ClientID %>');
            var total = 0;

            if (grid.rows.length > 0) {
                var inputs = grid.getElementsByTagName("input");


                for (var i = 0; i < inputs.length; i++) {

                    if (inputs[i].name.indexOf("txtAmount") > 1) {
                        if (inputs[i].value != "" && inputs[i].value != ".000") {
                            total = parseFloat(total) + parseFloat(inputs[i].value);
                        }
                    }
                }
            }

            document.getElementById('<%= lblTotalEar.ClientID %>').innerHTML = total.toFixed(3);
            calculate_time();

        }

        function CalculateDed() {
            var grid = document.getElementById('<%=gvEmployeePay2.ClientID %>');
            var total = 0;

            if (grid.rows.length > 0) {
                var inputs = grid.getElementsByTagName("input");


                for (var i = 0; i < inputs.length; i++) {

                    if (inputs[i].name.indexOf("txtAmount2") > 1) {

                        if (inputs[i].value != "" && inputs[i].value != ".000") {
                            total = parseFloat(total) + parseFloat(inputs[i].value);
                            //alert(total);
                        }
                    }
                }
            }

            document.getElementById('<%= lblTotDed.ClientID %>').innerHTML = total.toFixed(3);
            calculate_time();


        }

        function calculate_time() {
            var hour_start = document.getElementById('<%= lblTotalEar.ClientID %>').innerHTML
            // var hour_start = document.getElementById(parseInt('lblTotalEar'));                    
            var hour_end = document.getElementById('<%= lblTotDed.ClientID %>').innerHTML
            var hour_total = hour_start - hour_end;

            document.getElementById('<%=lblNet.ClientID %>').innerHTML = hour_total.toFixed(3);

        }
       


    </script>
</asp:Content>
