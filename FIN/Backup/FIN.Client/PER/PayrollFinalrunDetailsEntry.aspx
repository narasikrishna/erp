﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="PayrollFinalrunDetailsEntry.aspx.cs" Inherits="FIN.Client.PER.PayrollFinalrunDetailsEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 750px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="lblPayrollPeriod">
                Payroll Period
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:TextBox ID="txtPayrollPeriod" CssClass="validate[required] RequiredField txtBox"
                    MaxLength="3" runat="server" TabIndex="4"></asp:TextBox>
            </div>
            <div class="lblBox" style="float: left; width: 200px" id="lblPayrollHoursDays">
                Payroll Hours/Days
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:TextBox ID="txtPayrollHoursDays" CssClass="validate[required] RequiredField txtBox"
                    Enabled="false" MaxLength="3" runat="server" TabIndex="4"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="lblPayrollStatus">
                Payroll Status
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:TextBox ID="txtPayrollStatus" CssClass="validate[required] RequiredField txtBox"
                    Enabled="false" MaxLength="3" runat="server" TabIndex="4"></asp:TextBox>
            </div>
            <div class="lblBox" style="float: left; width: 200px" id="lblPayrollDate">
                Payroll Date
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:TextBox runat="server" ID="txtPayrollDate" CssClass="validate[,custom[ReqDateDDMMYYY],,]  txtBox"
                    TabIndex="1" OnTextChanged="txtPayrollDate_TextChanged"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtPayrollDate" />
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtPayrollDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="lblAllGroup">
                All Group
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:CheckBox ID="chkAllGroup" runat="server" Checked="True" TabIndex="5" />
            </div>
            <div class="lblBox" style="float: left; width: 200px" id="lblGroup">
                Group
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:DropDownList ID="ddlGroup" runat="server">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="lblPayrollTotal">
                Payroll Total
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:TextBox ID="txtPayrollTotal" CssClass="validate[required] RequiredField txtBox"
                    Enabled="false" MaxLength="3" runat="server" TabIndex="4"></asp:TextBox>
            </div>
            <div class="lblBox" style="float: left; width: 200px" id="lblDeductionTotal">
                Deduction Total
            </div>
            <div class="divtxtBox" style="float: left; width: 150px">
                <asp:TextBox ID="txtDeductionTotal" CssClass="validate[required] RequiredField txtBox"
                    Enabled="false" MaxLength="3" runat="server" TabIndex="4"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 120px" id="Div1">
            </div>
            <div class="lblBox" style="float: left; width: 300px" id="lblDetails">
                <asp:Button ID="btnDetails" runat="server" Text="Details" CssClass="btnProcess" OnClick="btnDetails_Click" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div align="center">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                DataKeyNames="DELETED" OnRowCancelingEdit="gvData_RowCancelingEdit" OnRowCommand="gvData_RowCommand"
                OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound" OnRowDeleting="gvData_RowDeleting"
                OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating" ShowFooter="True">
                <Columns>
                    <asp:TemplateField HeaderText="Department Name">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtDepartmentName" Width="130px" MaxLength="100" runat="server"
                                CssClass=" RequiredField  txtBox" Text='<%# Eval("PER_DESC") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtDepartmentName" Width="130px" MaxLength="100" runat="server"
                                CssClass="RequiredField   txtBox"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblDepartmentName" Width="130px" runat="server" Text='<%# Eval("PER_DESC") %>'></asp:Label>
                        </ItemTemplate>
                       
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Amount">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtAmount" Width="130px" MaxLength="100" runat="server" CssClass=" RequiredField  txtBox"
                                Text='<%# Eval("PER_DESC") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtAmount" Width="130px" MaxLength="100" runat="server" CssClass="RequiredField   txtBox"></asp:TextBox>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblAmount" Width="130px" runat="server" Text='<%# Eval("PER_DESC") %>'></asp:Label>
                        </ItemTemplate>
                       
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Element" ItemStyle-Width="50px" 
                        ItemStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <asp:Button ID="btnDetails" runat="server" CssClass="btn" Text="Details" OnClick="btnClick_Click" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:Button ID="btnDetails" runat="server" CssClass="btn" Text="Details" OnClick="btnClick_Click" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:Button ID="btnDetails" runat="server" CssClass="btn" Text="Details" OnClick="btnClick_Click" />
                        </FooterTemplate>
                       
                        <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Right"></ItemStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Add / Edit">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" runat="server" AlternateText="Edit" CausesValidation="false"
                                CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                            <asp:ImageButton ID="ibtnDelete" runat="server" AlternateText="Delete" CausesValidation="false"
                                CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibtnUpdate" runat="server" AlternateText="Update" CommandName="Update"
                                Height="20px" ImageUrl="~/Images/Update.png" />
                            <asp:ImageButton ID="ibtnCancel" runat="server" AlternateText="Cancel" CausesValidation="false"
                                CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="ibtnInsert" runat="server" AlternateText="Add" CommandName="FooterInsert"
                                ImageUrl="~/Images/Add.jpg" />
                        </FooterTemplate>
                        <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div id="divPopUp">
            <asp:HiddenField ID="btnDetails1" runat="server" />
            <cc2:ModalPopupExtender ID="ModalPopupExtender2" runat="server" TargetControlID="btnDetails1"
                PopupControlID="panelDetailsPopup" CancelControlID="btnCancel1" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="panelDetailsPopup" runat="server" BackColor="Violet" Width="50%" Height="60%"
                ScrollBars="Auto">
                <div class="ConfirmForm">
                    <table width="60%">
                        <div align="center">
                            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                                DataKeyNames="DELETED" OnRowCancelingEdit="gvData_RowCancelingEdit" OnRowCommand="gvData_RowCommand"
                                OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound" OnRowDeleting="gvData_RowDeleting"
                                OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating" ShowFooter="True">
                                <Columns>
                                    <asp:TemplateField HeaderText="Employee No">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtEmployeeNo" Width="130px" MaxLength="100" runat="server" CssClass=" RequiredField  txtBox"
                                                Text='<%# Eval("PER_DESC") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtEmployeeNo" Width="130px" MaxLength="100" runat="server" CssClass="RequiredField   txtBox"></asp:TextBox>
                                        </FooterTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblEmployeeNo" Width="130px" runat="server" Text='<%# Eval("PER_DESC") %>'></asp:Label>
                                        </ItemTemplate>
                                       
                                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Employee Name">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtEmployeeName" Width="130px" MaxLength="100" runat="server" CssClass=" RequiredField  txtBox"
                                                Text='<%# Eval("PER_DESC") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtEmployeeName" Width="130px" MaxLength="100" runat="server" CssClass="RequiredField   txtBox"></asp:TextBox>
                                        </FooterTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblEmployeeName" Width="130px" runat="server" Text='<%# Eval("PER_DESC") %>'></asp:Label>
                                        </ItemTemplate>
                                       
                                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Element">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtElement" Width="130px" MaxLength="100" runat="server" CssClass=" RequiredField  txtBox"
                                                Text='<%# Eval("PER_DESC") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtElement" Width="130px" MaxLength="100" runat="server" CssClass="RequiredField   txtBox"></asp:TextBox>
                                        </FooterTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblElement" Width="130px" runat="server" Text='<%# Eval("PER_DESC") %>'></asp:Label>
                                        </ItemTemplate>
                                        
                                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Element Description">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtElementDescription" Width="130px" MaxLength="100" runat="server"
                                                CssClass=" RequiredField  txtBox" Text='<%# Eval("PER_DESC") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtElementDescription" Width="130px" MaxLength="100" runat="server"
                                                CssClass="RequiredField   txtBox"></asp:TextBox>
                                        </FooterTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblElementDescription" Width="130px" runat="server" Text='<%# Eval("PER_DESC") %>'></asp:Label>
                                        </ItemTemplate>
                                       
                                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Amount">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtAmount" Width="130px" MaxLength="100" runat="server" CssClass=" RequiredField  txtBox"
                                                Text='<%# Eval("PER_DESC") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtAmount" Width="130px" MaxLength="100" runat="server" CssClass="RequiredField   txtBox"></asp:TextBox>
                                        </FooterTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblAmount" Width="130px" runat="server" Text='<%# Eval("PER_DESC") %>'></asp:Label>
                                        </ItemTemplate>
                                       
                                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                <HeaderStyle CssClass="GridHeader" />
                                <AlternatingRowStyle CssClass="GrdAltRow" />
                            </asp:GridView>
                        </div>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" CssClass="button" ID="btnOkay" Text="Ok" Width="60px"
                                    OnClick="btnOkay_Click" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btnCancel1" Text="Cancel" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="1" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" OnClick="btnCancel_Click"
                            TabIndex="2" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="3" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table width="100%">
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/PER/PayrollFinalrunDetails.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () { $("#form1").validationEngine(); });
        $("#FINContent_btnSave").click(function () {
            if ($("#form1").validationEngine('validate') == false)
                return false;
        });
    </script>
</asp:Content>
