﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.DAL.GL;
using FIN.BLL;
using FIN.BLL.PER;
using VMVServices.Web;
using VMVServices.Services.Data;

namespace FIN.Client.PER
{
    public partial class PayrollAdviceEntry : PageBase
    {
        PAY_ADVICE pAY_ADVICE = new PAY_ADVICE();
        DataTable dtGridData = new DataTable();
        Boolean bol_Saved = false;
        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PAE", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
            }

            if (VMVServices.Web.Utils.AddAllowed != FINAppConstants.Y && Master.Mode == FINAppConstants.Add)
            {
                btnSave.Visible = false;
            }
        }

        private void FillComboBox()
        {
            //FIN.BLL.CA.Bank_BLL.fn_getBankName(ref ddlBankName);
            FIN.BLL.CA.BankAccounts_BLL.fn_getFullDetails(ref ddlBankName);
            FIN.BLL.PER.PayrollPeriods_BLL.getPayrollPeriods4PayAdvice(ref ddlPayroll, Master.Mode);

        }
        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();
                EntityData = null;
                Session[FINSessionConstants.GridData] = null;

                txtPayrollDate.Text = DBMethod.ConvertDateToString(DateTime.Now.ToShortDateString());
                hfPayAdviceNumber.Value = "";

                btnPrint.Visible = false;
                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    btnPrint.Visible = true;
                    //using (IRepository<PAY_ADVICE> userCtx = new DataRepository<PAY_ADVICE>())
                    //{
                    //    pAY_ADVICE = userCtx.Find(r =>
                    //        (r.PAY_ADV_ID == Master.StrRecordId)
                    //        ).SingleOrDefault();
                    //}

                    EntityData = pAY_ADVICE;
                    DataTable dt_PayAdvice = DBMethod.ExecuteQuery(FIN.DAL.PER.PayrollAdvice_DAL.getPayAdviceDet4PayAdviceNo(Master.StrRecordId)).Tables[0];

                    divPayAdvNo.Visible = true;
                    if (dt_PayAdvice.Rows.Count > 0)
                    {
                        txtPayAdvNo.Text = dt_PayAdvice.Rows[0]["ATTRIBUTE5"].ToString();
                        hfPayAdviceNumber.Value = dt_PayAdvice.Rows[0]["ATTRIBUTE5"].ToString();
                        ddlBankName.SelectedValue = dt_PayAdvice.Rows[0]["BANK_ID"].ToString();
                        ddlPayroll.SelectedValue = dt_PayAdvice.Rows[0]["PAY_PERIOD_ID"].ToString();
                        FillPayrollDetails();
                        ddlPayrollPeriod.SelectedValue = dt_PayAdvice.Rows[0]["PAYROLL_ID"].ToString();

                        if (dt_PayAdvice.Rows[0]["PAY_DATE"] != null)
                        {
                            txtPayrollDate.Text = DBMethod.ConvertDateToString(dt_PayAdvice.Rows[0]["PAY_DATE"].ToString());
                        }
                        //txtPayAdvNo.Text = pAY_ADVICE.ATTRIBUTE5.ToString();
                        //hfPayAdviceNumber.Value = pAY_ADVICE.ATTRIBUTE5.ToString();
                        //ddlBankName.SelectedValue = pAY_ADVICE.BANK_ID.ToString();
                        //ddlPayroll.SelectedValue = pAY_ADVICE.PAY_PERIOD_ID.ToString();
                        //FillPayrollDetails();
                        //ddlPayrollPeriod.SelectedValue = pAY_ADVICE.PAYROLL_ID.ToString();

                        //if (pAY_ADVICE.PAY_DATE != null)
                        //{
                        //    txtPayrollDate.Text = DBMethod.ConvertDateToString(pAY_ADVICE.PAY_DATE.ToString());
                        //}
                        btnProcess_Click(btnProcess, new EventArgs());
                        ddlPayroll.Enabled = false;
                        ddlPayrollPeriod.Enabled = false;
                        ddlBankName.Enabled = false;
                        txtPayAdvNo.Enabled = false;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PA_ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void btnProcess_Click(object sender, EventArgs e)
        {
            try
            {

                DataTable dtGridData = DBMethod.ExecuteQuery(FIN.DAL.PER.PayrollRun_DAL.GetPayFinalRunDtlPayAdvice(ddlBankName.SelectedValue, ddlPayroll.SelectedValue, ddlPayrollPeriod.SelectedValue)).Tables[0];
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PA_ProcessClick", ex.ToString());
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void BindGrid(DataTable dtData)
        {
            Session[FINSessionConstants.GridData] = dtData;
            if (dtData.Rows.Count > 0)
            {
                dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("pay_net_amt", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("pay_net_amt"))));
                dtData.AcceptChanges();
            }

            gvData.DataSource = dtData;
            gvData.DataBind();

            if (dtData.Rows.Count > 0)
            {
                txtTotalAmount.Text = (CommonUtils.CalculateTotalAmounts(dtData, "pay_net_amt"));
                if (txtTotalAmount.Text != string.Empty)
                {
                    txtTotalAmount.Text = DBMethod.GetAmtDecimalCommaSeparationValue(txtTotalAmount.Text);
                }
            }
            for (int iLoop = 0; iLoop < dtData.Rows.Count; iLoop++)
            {
                if (dtData.Rows[iLoop]["PAY_ADVICE_NUMBER"].ToString().Length > 0)
                {
                    hfPayAdviceNumber.Value = dtData.Rows[iLoop]["PAY_ADVICE_NUMBER"].ToString();
                    break;
                }
            }
        }
        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (hfPayAdviceNumber.Value.ToString().Length == 0)
                {
                    hfPayAdviceNumber.Value = FINSP.GetSPFOR_SEQCode(FINAppConstants.PR_020_PA.ToString(), false, true);
                }


                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    PAY_ADVICE pAY_ADVICE = new PAY_ADVICE();
                    CheckBox chk_selected = (CheckBox)gvData.Rows[iLoop].FindControl("chkApprove");
                    if (chk_selected.Checked)
                    {
                        if (dtGridData.Rows[iLoop]["PAY_ADV_ID"].ToString() != "0")
                        {
                            using (IRepository<PAY_ADVICE> userCtx = new DataRepository<PAY_ADVICE>())
                            {
                                pAY_ADVICE = userCtx.Find(r =>
                                    (r.PAY_ADV_ID.ToUpper() == dtGridData.Rows[iLoop]["PAY_ADV_ID"].ToString())
                                    ).SingleOrDefault();
                            }
                        }

                        pAY_ADVICE.BANK_ID = ddlBankName.SelectedValue.ToString();
                        pAY_ADVICE.PAYROLL_ID = dtGridData.Rows[iLoop]["PAYROLL_ID"].ToString();
                        pAY_ADVICE.PAYROLL_DTL_ID = dtGridData.Rows[iLoop]["PAYROLL_DTL_ID"].ToString();
                        pAY_ADVICE.PAY_PERIOD_ID = dtGridData.Rows[iLoop]["PAYROLL_PERIOD"].ToString();
                        pAY_ADVICE.ENABLED_FLAG = "1";

                        pAY_ADVICE.ATTRIBUTE1 = dtGridData.Rows[iLoop]["emp_id"].ToString(); //emp id
                        pAY_ADVICE.ATTRIBUTE2 = dtGridData.Rows[iLoop]["emp_bank_code"].ToString(); //emp's bank id
                        pAY_ADVICE.ATTRIBUTE3 = dtGridData.Rows[iLoop]["pay_net_amt"].ToString(); // net amt
                        pAY_ADVICE.ATTRIBUTE4 = dtGridData.Rows[iLoop]["emp_bank_acct_code"].ToString(); //emp's account no
                        pAY_ADVICE.PAY_ORG_ID = VMVServices.Web.Utils.OrganizationID;

                        if (txtPayrollDate.Text != string.Empty)
                        {
                            pAY_ADVICE.PAY_DATE = DBMethod.ConvertStringToDate(txtPayrollDate.Text.ToString());
                        }

                        if (dtGridData.Rows[iLoop]["PAY_ADV_ID"].ToString() != "0")
                        {
                            pAY_ADVICE.MODIFIED_BY = this.LoggedUserName;
                            pAY_ADVICE.MODIFIED_DATE = DateTime.Today;
                            DBMethod.SaveEntity<PAY_ADVICE>(pAY_ADVICE, true);
                        }
                        else
                        {
                            pAY_ADVICE.PAY_ADV_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.PR_020_PA.ToString(), false, true);
                            pAY_ADVICE.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, pAY_ADVICE.PAY_ADV_ID);
                            pAY_ADVICE.ATTRIBUTE5 = hfPayAdviceNumber.Value.ToString();
                            pAY_ADVICE.CREATED_BY = this.LoggedUserName;
                            pAY_ADVICE.CREATED_DATE = DateTime.Today;
                            DBMethod.SaveEntity<PAY_ADVICE>(pAY_ADVICE);
                        }
                        bol_Saved = true;
                    }
                    else
                    {
                        if (dtGridData.Rows[iLoop]["PAY_ADV_ID"].ToString() != "0")
                        {
                            using (IRepository<PAY_ADVICE> userCtx = new DataRepository<PAY_ADVICE>())
                            {
                                pAY_ADVICE = userCtx.Find(r =>
                                    (r.PAY_ADV_ID.ToUpper() == dtGridData.Rows[iLoop]["PAY_ADV_ID"].ToString())
                                    ).SingleOrDefault();
                            }
                            DBMethod.DeleteEntity<PAY_ADVICE>(pAY_ADVICE);
                        }

                    }

                }


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PA_ATOBE", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                System.Collections.SortedList slControls = new System.Collections.SortedList();

                slControls[0] = ddlBankName;
                slControls[1] = ddlPayrollPeriod;

                Dictionary<string, string> Prop_File_Data;
                Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/PER_" + Session["Sel_Lng"].ToString() + ".properties"));

                ErrorCollection.Clear();
                string strCtrlTypes = "DropDownList~DropDownList";
                string strMessage = Prop_File_Data["Bank_Name_P"] + " ~ " + Prop_File_Data["Payroll_Period_P"];
                //string strMessage = "Deduction Party Name ~ Payment Mode ~ Element ~ Address1 ~ Address2";

                EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

                if (EmptyErrorCollection.Count > 0)
                {
                    ErrorCollection = EmptyErrorCollection;
                    return;
                }

                AssignToBE();
                if (bol_Saved)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                }

                //switch (Master.Mode)
                //{
                //    case FINAppConstants.Add:
                //        {
                //            DBMethod.SaveEntity<PAY_ADVICE>(pAY_ADVICE);
                //            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                //            break;
                //        }
                //    case FINAppConstants.Update:
                //        {
                //            DBMethod.SaveEntity<PAY_ADVICE>(pAY_ADVICE, true);
                //            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                //            break;
                //        }
                //}
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void btnPrint_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;


                htFilterParameter.Add("Pay_Adv_Number", txtPayAdvNo.Text);
                htFilterParameter.Add("Bank_Name", ddlBankName.SelectedItem.Text);
                htFilterParameter.Add("Payroll", ddlPayroll.SelectedItem.Text);
                htFilterParameter.Add("Payroll_Period", ddlPayrollPeriod.SelectedItem.Text);

                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;
                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;

                ReportData = FIN.BLL.PER.PayrollTrailrunDetails_BLL.GetPayAdviceReportData();

                htHeadingParameters.Add("ReportName", "Payment Advice ");

                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerPath + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PayAdviceReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void ddlPayroll_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillPayrollDetails();
        }
        private void FillPayrollDetails()
        {
            FIN.BLL.PER.PayrollTrailrunDetails_BLL.fn_getFinalPayrollDtlId4Payadvice(ref ddlPayrollPeriod, ddlPayroll.SelectedValue, Master.Mode);
            dtGridData = new DataTable();
            BindGrid(dtGridData);
        }

        protected void ddlBankName_SelectedIndexChanged(object sender, EventArgs e)
        {
            dtGridData = new DataTable();
            BindGrid(dtGridData);
        }

        protected void ddlPayrollPeriod_SelectedIndexChanged(object sender, EventArgs e)
        {
            dtGridData = new DataTable();
            BindGrid(dtGridData);
        }

        protected void chkAllApprove_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chkAll = (CheckBox)sender;
            Boolean bol_sel = false;
            if (chkAll.Checked)
            {
                bol_sel = true;
            }
            else
            {
                bol_sel = false;
            }

            for (int iLoop = 0; iLoop < gvData.Rows.Count; iLoop++)
            {
                CheckBox chk_box = (CheckBox)gvData.Rows[iLoop].FindControl("chkApprove");
                chk_box.Checked = bol_sel;
            }

        }
    }
}