﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="PayrollPayDeductionPartyEntry.aspx.cs" Inherits="FIN.Client.PER.PayrollPayDeductionPartyEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 750px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblPaymentNumber">
                Payroll Number
            </div>
            <div class="divtxtBox LNOrient" style="width: 155px">
                <asp:TextBox ID="txtPaymentNumber" CssClass="validate[]  txtBox" MaxLength="3" runat="server"
                    Enabled="false" TabIndex="1"></asp:TextBox>
            </div>
            <div class="colspace  LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 200px" id="lblDeductionPartyNumber">
                Deduction Party Number
            </div>
            <div class="divtxtBox LNOrient" style="width: 155px">
                <asp:DropDownList ID="ddlDeductionPartyNumber" runat="server" AutoPostBack="True"
                    CssClass="validate[required] RequiredField ddlStype" OnSelectedIndexChanged="ddlDeductionPartyNumber_SelectedIndexChanged"
                    TabIndex="2">
                </asp:DropDownList>
            </div>
            <%-- <div class="lblBox LNOrient" style="  width: 200px" id="lblPaymentDate">
                Payment Date
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:TextBox runat="server" ID="txtPaymentDate" CssClass="validate[,custom[ReqDateDDMMYYY],,]  txtBox"
                    TabIndex="2" ontextchanged="txtPaymentDate_TextChanged" ></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtPaymentDate" />
                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars="/"
                    FilterType="Numbers,Custom" TargetControlID="txtPaymentDate" />
            </div>--%>
        </div>
        <%--<div class="divClear_10">
        </div>--%>
        <%--<div class="divRowContainer">
           
            <div class="lblBox LNOrient" style="  width: 200px" id="lblDeductionPartyName">
                Deduction Party Name
            </div>
            <div class="divtxtBox LNOrient" style="  width: 150px">
                <asp:TextBox ID="txtDeductionPartyName" CssClass="validate[required] RequiredField txtBox"
                    Enabled="false" MaxLength="3" runat="server" TabIndex="4"></asp:TextBox>
            </div>
        </div>--%>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblPaymentMode">
                Payment Mode
            </div>
            <div class="divtxtBox LNOrient" style="width: 155px">
                <asp:DropDownList ID="ddlPaymentMode" AutoPostBack="true" CssClass="validate[required] RequiredField ddlStype"
                    runat="server" Width="102%" TabIndex="3" OnSelectedIndexChanged="ddlPaymentMode_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient">
                &nbsp</div>
            <div class="lblBox LNOrient" style="width: 200px" id="lblPaymentAmount">
                Payment Amount
            </div>
            <div class="divtxtBox LNOrient" style="width: 155px">
                <asp:TextBox ID="txtPaymentAmount" CssClass="validate[required] RequiredField txtBox_N"
                    MaxLength="15" runat="server" TabIndex="10"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div id="Bankdetails" runat="server">
                <div class="lblBox LNOrient" style="width: 200px" id="lblBankName">
                    Bank Name
                </div>
                <div class="divtxtBox LNOrient" style="width: 155px">
                    <asp:DropDownList ID="ddlBankName" runat="server" AutoPostBack="True" CssClass="validate[required] RequiredField ddlStype"
                        OnSelectedIndexChanged="ddlBankName_SelectedIndexChanged" TabIndex="4">
                    </asp:DropDownList>
                </div>
                <div class="colspace  LNOrient">
                    &nbsp</div>
                <div class="lblBox LNOrient" style="width: 200px" id="lblBankBranch">
                    Bank Branch
                </div>
                <div class="divtxtBox LNOrient" style="width: 155px">
                    <asp:DropDownList ID="ddlBankBranch" runat="server" AutoPostBack="True" CssClass="validate[required] RequiredField ddlStype"
                        OnSelectedIndexChanged="ddlBankBranch_SelectedIndexChanged" TabIndex="5">
                    </asp:DropDownList>
                </div>
                <div class="divClear_10">
                </div>
                <div class="divRowContainer">
                    <div class="lblBox LNOrient" style="width: 200px" id="lblAccountNumber">
                        Account Number
                    </div>
                    <div class="divtxtBox LNOrient" style="width: 155px">
                        <asp:DropDownList ID="ddlAccountNumber" CssClass="validate[required] RequiredField ddlStype"
                            runat="server" TabIndex="6">
                        </asp:DropDownList>
                    </div>
                    <div class="colspace  LNOrient">
                        &nbsp</div>
                    <div class="lblBox LNOrient" style="width: 200px" id="lblChequeNumber">
                        Cheque Number
                    </div>
                    <div class="divtxtBox LNOrient" style="width: 155px">
                        <asp:DropDownList ID="ddlChequeNumber" CssClass="validate[required] RequiredField ddlStype"
                            runat="server" TabIndex="7">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="divClear_10">
                </div>
                <div class="divRowContainer">
                    <div class="lblBox LNOrient" style="width: 200px" id="lblChequeDate">
                        Cheque Date
                    </div>
                    <div class="divtxtBox LNOrient" style="width: 155px">
                        <asp:TextBox runat="server" ID="txtChequeDate" CssClass="validate[,custom[ReqDateDDMMYYY],,] RequiredField  txtBox"
                            TabIndex="8"></asp:TextBox>
                        <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender2" TargetControlID="txtChequeDate" />
                        <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars="/"
                            FilterType="Numbers,Custom" TargetControlID="txtChequeDate" />
                    </div>
                    <div class="colspace  LNOrient">
                        &nbsp</div>
                    <div class="lblBox LNOrient" style="width: 200px" id="lblDeductionNumber">
                        Cheque Amount
                    </div>
                    <div class="divtxtBox LNOrient" style="width: 155px">
                        <asp:TextBox ID="txtchqamount" CssClass="validate[required] RequiredField txtBox_N"
                            MaxLength="15" runat="server" TabIndex="9"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="11" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="12" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="13" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table width="100%">
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/PER/PAChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () { $("#form1").validationEngine(); });
        $("#FINContent_btnSave").click(function () {
            if ($("#form1").validationEngine('validate') == false)
                return false;
        });
    </script>
</asp:Content>
