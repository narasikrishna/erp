﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.DAL.PER;
using FIN.BLL;
using FIN.BLL.PER;
using VMVServices.Web;

namespace FIN.Client.PER
{
    public partial class PayrollElementsEntry : PageBase
    {

        PAY_ELEMENTS pAY_ELEMENTS = new PAY_ELEMENTS();
        DataTable dtGridData = new DataTable();
        DataTable dtelementcode = new DataTable();
        PayrollElements_BLL PayrollElements_BLL = new PayrollElements_BLL();

        Boolean bol_rowVisiable;
        Boolean savedBool;
        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Pe_PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));


            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
        }

        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>
        /// 

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>



        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();



                EntityData = null;

                dtGridData = FIN.BLL.PER.PayrollElements_BLL.getChildEntityDet(Master.StrRecordId);
                BindGrid(dtGridData);

                //if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                //{

                //    using (IRepository<HR_CATEGORIES> userCtx = new DataRepository<HR_CATEGORIES>())
                //    {
                //        hR_CATEGORIES = userCtx.Find(r =>
                //            (r.CATEGORY_ID == Master.StrRecordId)
                //            ).SingleOrDefault();
                //    }

                //    EntityData = hR_CATEGORIES;

                //}

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PE_ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                var tmpChildEntity = new List<Tuple<object, string>>();

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    pAY_ELEMENTS = new PAY_ELEMENTS();
                    if (dtGridData.Rows[iLoop]["PAY_ELEMENT_ID"].ToString() != "0")
                    {
                        using (IRepository<PAY_ELEMENTS> userCtx = new DataRepository<PAY_ELEMENTS>())
                        {
                            pAY_ELEMENTS = userCtx.Find(r =>
                                (r.PAY_ELEMENT_ID == dtGridData.Rows[iLoop]["PAY_ELEMENT_ID"].ToString())
                                ).SingleOrDefault();
                        }
                    }

                    pAY_ELEMENTS.PAY_ELEMENT_CODE = dtGridData.Rows[iLoop]["PAY_ELEMENT_CODE"].ToString();
                    pAY_ELEMENTS.PAY_ELEMENT_CODE_OL = dtGridData.Rows[iLoop]["PAY_ELEMENT_CODE_OL"].ToString();
                    pAY_ELEMENTS.PAY_ELEMENT_DESC = dtGridData.Rows[iLoop]["PAY_ELEMENT_DESC"].ToString();
                    pAY_ELEMENTS.PAY_ELEMENT_DESC_OL = dtGridData.Rows[iLoop]["PAY_ELEMENT_DESC_OL"].ToString();
                    pAY_ELEMENTS.PAY_ELE_ALIAS = dtGridData.Rows[iLoop]["PAY_ELE_ALIAS"].ToString();
                    pAY_ELEMENTS.PAY_ELE_CLASS = dtGridData.Rows[iLoop]["VALUE_KEY_ID"].ToString();
                    pAY_ELEMENTS.GL_ID = dtGridData.Rows[iLoop]["CODE_ID"].ToString();
                    pAY_ELEMENTS.RECEIVABLE_ID = dtGridData.Rows[iLoop]["REC_CODE_ID"].ToString();

                    pAY_ELEMENTS.PAY_ORG_ID = VMVServices.Web.Utils.OrganizationID;

                    if (dtGridData.Rows[iLoop]["EFFECTIVE_FROM_DT"] != DBNull.Value)
                    {
                        pAY_ELEMENTS.EFFECTIVE_FROM_DT = DateTime.Parse(dtGridData.Rows[iLoop]["EFFECTIVE_FROM_DT"].ToString());
                    }

                    if (dtGridData.Rows[iLoop]["EFFECTIVE_TO_DT"] != DBNull.Value)
                    {
                        pAY_ELEMENTS.EFFECTIVE_TO_DT = DateTime.Parse(dtGridData.Rows[iLoop]["EFFECTIVE_TO_DT"].ToString());
                    }


                    if (dtGridData.Rows[iLoop]["PAY_ELE_RECURRING"].ToString().ToUpper() == "TRUE")
                    {
                        pAY_ELEMENTS.PAY_ELE_RECURRING = FINAppConstants.Y;
                    }
                    else
                    {
                        pAY_ELEMENTS.PAY_ELE_RECURRING = FINAppConstants.N;
                    }

                    if (dtGridData.Rows[iLoop]["PAY_ELE_TAXABLE"].ToString().ToUpper() == "TRUE")
                    {
                        pAY_ELEMENTS.PAY_ELE_TAXABLE = FINAppConstants.Y;
                    }
                    else
                    {
                        pAY_ELEMENTS.PAY_ELE_TAXABLE = FINAppConstants.N;
                    }

                    if (dtGridData.Rows[iLoop]["APPERARS_IN_PAYSLIP"].ToString().ToUpper() == "TRUE")
                    {
                        pAY_ELEMENTS.APPERARS_IN_PAYSLIP = FINAppConstants.Y;
                    }
                    else
                    {
                        pAY_ELEMENTS.APPERARS_IN_PAYSLIP = FINAppConstants.N;
                    }
                    if (dtGridData.Rows[iLoop]["UNPAID_LEAVE"].ToString().ToUpper() == "TRUE")
                    {
                        pAY_ELEMENTS.UNPAID_LEAVE = FINAppConstants.Y;
                    }
                    else
                    {
                        pAY_ELEMENTS.UNPAID_LEAVE = FINAppConstants.N;
                    }

                    if (dtGridData.Rows[iLoop]["ENABLED_FLAG"].ToString().ToUpper() == "TRUE")
                    {
                        pAY_ELEMENTS.ENABLED_FLAG = FINAppConstants.Y;
                    }
                    else
                    {
                        pAY_ELEMENTS.ENABLED_FLAG = FINAppConstants.N;
                    }

                    if (dtGridData.Rows[iLoop]["PAY_ELEMENT_PRORATE"].ToString().ToUpper() == "TRUE")
                    {
                        pAY_ELEMENTS.PAY_ELEMENT_PRORATE = FINAppConstants.Y;
                    }
                    else
                    {
                        pAY_ELEMENTS.PAY_ELEMENT_PRORATE = FINAppConstants.N;
                    }
                    if (dtGridData.Rows[iLoop]["PAID_FOR_ANNUAL_LEAVE"].ToString().ToUpper() == "TRUE")
                    {
                        pAY_ELEMENTS.PAID_FOR_ANNUAL_LEAVE = FINAppConstants.Y;
                    }
                    else
                    {
                        pAY_ELEMENTS.PAID_FOR_ANNUAL_LEAVE = FINAppConstants.N;
                    }



                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {

                        tmpChildEntity.Add(new Tuple<object, string>(pAY_ELEMENTS, "D"));
                    }
                    else
                    {
                        if (dtGridData.Rows[iLoop]["PAY_ELEMENT_ID"].ToString() != "0")
                        {
                            pAY_ELEMENTS.PAY_ELEMENT_ID = dtGridData.Rows[iLoop]["PAY_ELEMENT_ID"].ToString();
                            pAY_ELEMENTS.MODIFIED_BY = this.LoggedUserName;
                            pAY_ELEMENTS.MODIFIED_DATE = DateTime.Today;
                            pAY_ELEMENTS.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, pAY_ELEMENTS.PAY_ELEMENT_ID);
                            DBMethod.SaveEntity<PAY_ELEMENTS>(pAY_ELEMENTS, true);
                            savedBool = true;
                            //tmpChildEntity.Add(new Tuple<object, string>(hR_CATEGORIES, "U"));
                        }
                        else
                        {

                            pAY_ELEMENTS.PAY_ELEMENT_ID = FINSP.GetSPFOR_SEQCode("PER_002".ToString(), false, true);
                            pAY_ELEMENTS.CREATED_BY = this.LoggedUserName;
                            pAY_ELEMENTS.CREATED_DATE = DateTime.Today;
                            pAY_ELEMENTS.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, pAY_ELEMENTS.PAY_ELEMENT_ID);
                            DBMethod.SaveEntity<PAY_ELEMENTS>(pAY_ELEMENTS);
                            // tmpChildEntity.Add(new Tuple<object, string>(hR_CATEGORIES, "A"));
                            savedBool = true;
                        }



                    }

                }

                //switch (Master.Mode)
                //{
                //    case FINAppConstants.Add:
                //        {
                //            FIN.BLL.GL.AccountCodes_BLL.SavePCEntity<INV_TRANSFER_HDR, INV_TRANSFER_DTL>(iNV_TRANSFER_HDR, tmpChildEntity, iNV_TRANSFER_DTL);
                //            break;
                //        }
                //    case FINAppConstants.Update:
                //        {

                //            FIN.BLL.GL.AccountCodes_BLL.SavePCEntity<INV_TRANSFER_HDR, INV_TRANSFER_DTL>(iNV_TRANSFER_HDR, tmpChildEntity, iNV_TRANSFER_DTL, true);
                //            break;

                //        }
                //}



            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PE_ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddlElementClass = tmpgvr.FindControl("ddlElementClass") as DropDownList;
                DropDownList ddlAcctcode = tmpgvr.FindControl("ddlAcctcode") as DropDownList;
                DropDownList ddlRecAcctcode = tmpgvr.FindControl("ddlRecAcctcode") as DropDownList;

                Lookup_BLL.GetLookUpValues(ref ddlElementClass, "EARNING CLASS");
                FIN.BLL.GL.AccountCodes_BLL.fn_getAccount(ref ddlAcctcode);
                FIN.BLL.GL.AccountCodes_BLL.fn_getAccount(ref ddlRecAcctcode);


                if (gvData.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                {
                    ddlElementClass.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.VALUE_KEY_ID].ToString();
                    ddlAcctcode.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["CODE_ID"].ToString();
                    ddlRecAcctcode.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values["REC_CODE_ID"].ToString();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PE_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session[FINSessionConstants.GridData], "Payroll Element");

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                AssignToBE();

                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        #endregion

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr["PAY_ELE_RECURRING"] = "FALSE";
                    dr["PAY_ELE_TAXABLE"] = "FALSE";
                    dr["APPERARS_IN_PAYSLIP"] = "FALSE";
                    dr["UNPAID_LEAVE"] = "FALSE";
                    dr["ENABLED_FLAG"] = "FALSE";
                    dr["PAY_ELEMENT_PRORATE"] = "FALSE";
                    dr["PAID_FOR_ANNUAL_LEAVE"] = "FALSE";

                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PE_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            try
            {
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = -1;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PE_ROW_CNCL_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion

        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PE_ROW_CMD", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {


            System.Collections.SortedList slControls = new System.Collections.SortedList();


            TextBox txtElementCodeEn = gvr.FindControl("txtElementCodeEn") as TextBox;
            TextBox txtElementCodeAr = gvr.FindControl("txtElementCodeAr") as TextBox;
            TextBox txtElementdescEn = gvr.FindControl("txtElementdescEn") as TextBox;
            TextBox txtElementdescAr = gvr.FindControl("txtElementdescAr") as TextBox;
            TextBox txtAlias = gvr.FindControl("txtAlias") as TextBox;
            DropDownList ddlAcctcode = gvr.FindControl("ddlAcctcode") as DropDownList;
            DropDownList ddlRecAcctcode = gvr.FindControl("ddlRecAcctcode") as DropDownList;
            DropDownList ddlElementClass = gvr.FindControl("ddlElementClass") as DropDownList;
            TextBox dtpfromDate = gvr.FindControl("dtpfromDate") as TextBox;
            TextBox dtpToDate = gvr.FindControl("dtpToDate") as TextBox;
            CheckBox chkRecurring = gvr.FindControl("chkRecurring") as CheckBox;
            CheckBox chkTaxable = gvr.FindControl("chkTaxable") as CheckBox;
            CheckBox chkAip = gvr.FindControl("chkAip") as CheckBox;
            CheckBox chkunpaidlv = gvr.FindControl("chkunpaidlv") as CheckBox;
            CheckBox chkact = gvr.FindControl("chkact") as CheckBox;

            CheckBox chkprorate = gvr.FindControl("chkprorate") as CheckBox;
            CheckBox chkannualleave = gvr.FindControl("chkannualleave") as CheckBox;


            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList["PAY_ELEMENT_ID"] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }

            slControls[0] = txtElementCodeEn;

            slControls[1] = txtElementdescEn;
            slControls[2] = ddlAcctcode;

            slControls[3] = txtAlias;
            slControls[4] = ddlElementClass;
            slControls[5] = dtpfromDate;
            slControls[6] = dtpfromDate;
            slControls[7] = dtpToDate;

            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/PER_" + Session["Sel_Lng"].ToString() + ".properties"));

            ErrorCollection.Clear();
            string strCtrlTypes = "TextBox~TextBox~DropDownList~TextBox~DropDownList~TextBox~DateTime~DateRangeValidate";
            string strMessage = Prop_File_Data["Element_Code_P"] + " ~ " + Prop_File_Data["Element_Description_P"] + " ~ " + Prop_File_Data["Account_Code_P"] + " ~ " + Prop_File_Data["Alias_P"] + " ~ " + Prop_File_Data["Element_Class_P"] + " ~ " + Prop_File_Data["From_Date_P"] + " ~ " + Prop_File_Data["From_Date_P"] + " ~ " + Prop_File_Data["End_Date_P"] + "";
            //string strMessage = "Element Code ~ Element Description ~ Alias ~ Element Class ~ From Date ~ From Date ~ End Date";
            ErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);
            if (ErrorCollection.Count > 0)
                return drList;

            dtelementcode = DBMethod.ExecuteQuery(PayrollElements_DAL.GetElementCode(txtElementCodeEn.Text, drList["PAY_ELEMENT_ID"].ToString(), dtpfromDate.Text, dtpToDate.Text)).Tables[0];

            if (dtelementcode.Rows.Count > 0)
            {
                ErrorCollection.Add("chkelementcode", Prop_File_Data["Element_Code_Aready_P"]);
                return drList;
            }

            string strCondition = "PAY_ELEMENT_CODE='" + txtElementCodeEn.Text.Trim().ToUpper() + "'";
            strMessage = FINMessageConstatns.ElementCodeAlreadyExists;
            ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            if (ErrorCollection.Count > 0)
            {
                return drList;
            }

            //if (Session[FINSessionConstants.GridData] != null)
            //{
            //    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
            //    for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
            //    {
            //        if (dtGridData.Rows[iLoop]["PAY_ELEMENT_ID"].ToString() != "0")
            //        {
            //            pAY_ELEMENTS.PAY_ELEMENT_ID = dtGridData.Rows[iLoop]["PAY_ELEMENT_ID"].ToString();
            //        }
            //    }
            //}

            //Duplicate Validation
            ErrorCollection.Clear();
            string ProReturn = null;
            ProReturn = FINSP.GetSPFOR_ERR_MGR_CATEGORY(Master.FormCode, drList["PAY_ELEMENT_ID"].ToString(), txtElementCodeEn.Text.ToString());
            if (ProReturn != string.Empty)
            {
                if (ProReturn != "0")
                {
                    ErrorCollection.Add("PAYROLLELEMENTS", ProReturn);
                    if (ErrorCollection.Count > 0)
                    {
                        return drList;
                    }
                }
            }

            drList["PAY_ELEMENT_CODE"] = txtElementCodeEn.Text;
            drList["PAY_ELEMENT_CODE_OL"] = txtElementCodeAr.Text;
            drList["PAY_ELEMENT_DESC"] = txtElementdescEn.Text;
            drList["PAY_ELEMENT_DESC_OL"] = txtElementdescAr.Text;
            drList["PAY_ELE_ALIAS"] = txtAlias.Text;
            //  drList["PAY_ELE_CLASS"] = txtElementClass.Text;
            if (ddlElementClass.SelectedItem != null)
            {
                drList[FINColumnConstants.VALUE_KEY_ID] = ddlElementClass.SelectedItem.Value;
                drList[FINColumnConstants.VALUE_NAME] = ddlElementClass.SelectedItem.Text;
            }

            if (ddlAcctcode.SelectedItem != null)
            {
                drList[FINColumnConstants.CODE_ID] = ddlAcctcode.SelectedItem.Value;
                drList[FINColumnConstants.CODE_NAME] = ddlAcctcode.SelectedItem.Text;
            }

            if (ddlRecAcctcode.SelectedItem != null)
            {
                drList["REC_CODE_ID"] = ddlRecAcctcode.SelectedItem.Value;
                drList["REC_CODE_NAME"] = ddlRecAcctcode.SelectedItem.Text;
            }


            if (dtpfromDate.Text.ToString().Length > 0)
            {
                drList["EFFECTIVE_FROM_DT"] = DBMethod.ConvertStringToDate(dtpfromDate.Text.ToString());
            }

            if (dtpToDate.Text.ToString().Length > 0)
            {

                drList["EFFECTIVE_TO_DT"] = DBMethod.ConvertStringToDate(dtpToDate.Text.ToString());
            }
            else
            {
                drList["EFFECTIVE_TO_DT"] = DBNull.Value;
            }
            if (chkRecurring.Checked)
            {
                drList["PAY_ELE_RECURRING"] = "TRUE";
            }
            else
            {
                drList["PAY_ELE_RECURRING"] = "FALSE";
            }

            if (chkTaxable.Checked)
            {
                drList["PAY_ELE_TAXABLE"] = "TRUE";
            }
            else
            {
                drList["PAY_ELE_TAXABLE"] = "FALSE";
            }

            if (chkAip.Checked)
            {
                drList["APPERARS_IN_PAYSLIP"] = "TRUE";
            }
            else
            {
                drList["APPERARS_IN_PAYSLIP"] = "FALSE";
            }
            if (chkunpaidlv.Checked)
            {
                drList["UNPAID_LEAVE"] = "TRUE";
            }
            else
            {
                drList["UNPAID_LEAVE"] = "FALSE";
            }
            if (chkact.Checked)
            {
                drList["ENABLED_FLAG"] = "TRUE";
            }
            else
            {
                drList["ENABLED_FLAG"] = "FALSE";
            }


            if (chkprorate.Checked)
            {
                drList["PAY_ELEMENT_PRORATE"] = "TRUE";
            }
            else
            {
                drList["PAY_ELEMENT_PRORATE"] = "FALSE";
            }

            if (chkannualleave.Checked)
            {
                drList["PAID_FOR_ANNUAL_LEAVE"] = "TRUE";
            }
            else
            {
                drList["PAID_FOR_ANNUAL_LEAVE"] = "FALSE";
            }

            drList[FINColumnConstants.DELETED] = FINAppConstants.N;


            return drList;



        }



        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PE_ROW_UP", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PE_ROW_DEL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }


        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PE_ROW_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PE_RowCreated", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PE_RDB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {

                    pAY_ELEMENTS.PAY_ELEMENT_ID = dtGridData.Rows[iLoop]["PAY_ELEMENT_ID"].ToString();
                    DBMethod.DeleteEntity<PAY_ELEMENTS>(pAY_ELEMENTS);
                }


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Pe_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {

        }




    }
}