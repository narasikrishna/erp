﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.DAL.GL;
using FIN.BLL;
using FIN.BLL.GL;
using FIN.BLL.PER;
using FIN.DAL.PER;
using VMVServices.Web;
using VMVServices.Services.Data;


namespace FIN.Client.PER
{
    public partial class PayrollTrailrunDetailsEntry : PageBase
    {
        DataTable dtGridData = new DataTable();
        DataTable dtEmployeePay = new DataTable();
        DataTable dtAccBal_currwise = new DataTable();
        DataTable dtAccBal_Daywise = new DataTable();
        string ProReturn = null;
        Boolean bol_rowVisiable;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    ChangeLanguage();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BPL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }
        private void ChangeLanguage()
        {
            try
            {
                ErrorCollection.Clear();
                if (VMVServices.Web.Utils.LanguageCode.ToString().Length > 0)
                {
                    Dictionary<string, string> Prop_File_Data;
                    Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/PER_" + Session["Sel_Lng"].ToString() + ".properties"));
                    chkAlldept.Text = Prop_File_Data["ALL_P"];

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("MisReceiptChangeLang", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
               // btnSave.Text = "Update";
            }

            UserRightsChecking();

        }


        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }

        /// <summary>
        /// To assign the values to Controls and fetch the StrRecordId,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();


                Startup();
                chkAlldept.Checked = true;
                FillComboBox();

                EntityData = null;
                txtPayrollDate.Text = DBMethod.ConvertDateToString(DateTime.Now.ToShortDateString());
                dtGridData = DBMethod.ExecuteQuery(PayrollTrailrunDetails_DAL.GetPayTrialRunHdr(Master.StrRecordId)).Tables[0];
                BindGrid(dtGridData);


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        private void FillComboBox()
        {

            PayrollGroup_BLL.fn_GetPayGroup(ref ddlGroup);
            Lookup_BLL.GetLookUpValues(ref ddlPayrollStatus, "SS");
            PayrollPeriods_BLL.fn_GetPayrollPeriods(ref ddlPayrollPeriod);
            LoadDepartment();

        }


        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>


        #endregion

        private void BindGrid(DataTable dtData)
        {
            bol_rowVisiable = false;
            Session[FINSessionConstants.GridData] = dtData;

            if (dtData.Rows.Count > 0)
            {
                dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("pay_amount", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("pay_amount"))));
                dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("pay_ded_amount", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("pay_ded_amount"))));
                dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("pay_net_amount", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("pay_net_amount"))));
                dtData.AcceptChanges();
            }

            DataTable dt_tmp = dtData.Copy();
            if (dt_tmp.Rows.Count == 0)
            {
                DataRow dr = dt_tmp.NewRow();
                dr[0] = "0";

                dt_tmp.Rows.Add(dr);
                bol_rowVisiable = true;
            }
            gvData.DataSource = dt_tmp;
            gvData.DataBind();
            gvEmpDetails.Visible = false;
        }

        private void BindGrid_EmployeePayData(DataTable dtEmployeePayData)
        {
            bol_rowVisiable = false;
            Session["dtEmployeePayData"] = dtEmployeePayData;

            if (dtEmployeePayData.Rows.Count > 0)
            {
                dtEmployeePayData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("pay_amount", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("pay_amount"))));
                //dtEmployeePayData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("pay_ded_amount", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("pay_ded_amount"))));

                dtEmployeePayData.AcceptChanges();
            }

            DataTable dt_tmp1 = dtEmployeePayData.Copy();
            if (dt_tmp1.Rows.Count == 0)
            {
                DataRow dr = dt_tmp1.NewRow();
                dr[0] = "0";

                dt_tmp1.Rows.Add(dr);
                bol_rowVisiable = true;
            }
            gvEmployeePay.DataSource = dtEmployeePayData;
            gvEmployeePay.DataBind();
            lblTotalEar.Text = CommonUtils.CalculateTotalAmounts(dtEmployeePayData, "pay_amount", " pay_element_Type='Earning'");
            lblNet.Text = (CommonUtils.ConvertStringToDecimal(lblTotalEar.Text) - CommonUtils.ConvertStringToDecimal(lblTotDed.Text)).ToString();

            
        }

        protected void gvEmployeePay_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    //if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    //{
                    //    e.Row.Visible = false;
                    //}
                    if (((DataRowView)e.Row.DataItem).Row["pay_element_Type"].ToString() == "Deduction")
                    {
                        e.Row.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("SBD_ROW_DBOUND", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        private void BindGrid_EmployeePayData2(DataTable dtEmployeePayData2)
        {
            bol_rowVisiable = false;
            Session["dtEmployeePayData2"] = dtEmployeePayData2;

            if (dtEmployeePayData2.Rows.Count > 0)
            {
                dtEmployeePayData2.AsEnumerable().ToList().ForEach(p => p.SetField<String>("pay_amount", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("pay_amount"))));
                //dtEmployeePayData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("pay_ded_amount", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("pay_ded_amount"))));

                dtEmployeePayData2.AcceptChanges();
            }

            DataTable dt_tmp1 = dtEmployeePayData2.Copy();
            if (dt_tmp1.Rows.Count == 0)
            {
                DataRow dr = dt_tmp1.NewRow();
                dr[0] = "0";

                dt_tmp1.Rows.Add(dr);
                bol_rowVisiable = true;
            }
            gvEmployeePay2.DataSource = dtEmployeePayData2;
            gvEmployeePay2.DataBind();
            lblTotDed.Text = CommonUtils.CalculateTotalAmounts(dtEmployeePayData2, "pay_amount", " pay_element_Type='Deduction'");
            lblNet.Text = (CommonUtils.ConvertStringToDecimal(lblTotalEar.Text) - CommonUtils.ConvertStringToDecimal(lblTotDed.Text)).ToString();

            
        }
        protected void gvEmployeePay2_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    //if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    //{
                    //    e.Row.Visible = false;
                    //}
                    if (((DataRowView)e.Row.DataItem).Row["pay_element_Type"].ToString() == "Earning")
                    {
                        e.Row.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("SBD_ROW_DBOUND", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnOkay_Click(object sender, EventArgs e)
        {
            for (int iLoop = 0; iLoop < gvEmployeePay.Rows.Count; iLoop++)
            {
                TextBox txt_Amount = (TextBox)gvEmployeePay.Rows[iLoop].FindControl("txtAmount");
                //PAY_TRIAL_RUN_DTL_DTL pAY_TRIAL_RUN_DTL_DTL = new PAY_TRIAL_RUN_DTL_DTL();
                //using (IRepository<PAY_TRIAL_RUN_DTL_DTL> userCtx = new DataRepository<PAY_TRIAL_RUN_DTL_DTL>())
                //{
                //    pAY_TRIAL_RUN_DTL_DTL = userCtx.Find(r =>
                //        (r.PAYROLL_DTL_DTL_ID == gvEmployeePay.DataKeys[iLoop].Values["PAYROLL_DTL_DTL_ID"].ToString())
                //        ).SingleOrDefault();
                //}
                //if (pAY_TRIAL_RUN_DTL_DTL != null)
                //{
                //    pAY_TRIAL_RUN_DTL_DTL.PAY_AMOUNT = decimal.Parse(DBMethod.GetAmtDecimalCommaSeparationValue(DBMethod.GetAmountDecimalValue(txt_Amount.Text).ToString()));
                //    DBMethod.SaveEntity<PAY_TRIAL_RUN_DTL_DTL> ( pAY_TRIAL_RUN_DTL_DTL,true);
                //}

                //string amtCommaSeparatedValue = DBMethod.GetAmtDecimalCommaSeparationValue(txt_Amount.Text.ToString());
                //pAY_TRIAL_RUN_DTL_DTL.PAY_AMOUNT = CommonUtils.ConvertStringToDecimal(txt_Amount.Text.ToString());

                if (gvEmployeePay.DataKeys[iLoop].Values["pay_element_Type"].ToString() == "Earning")
                {
                    DBMethod.ExecuteNonQuery(" update pay_trial_run_dtl_dtl set pay_amount=" + CommonUtils.ConvertStringToDecimal(txt_Amount.Text.ToString()) + "where PAYROLL_DTL_DTL_ID='" + gvEmployeePay.DataKeys[iLoop].Values["PAYROLL_DTL_DTL_ID"].ToString() + "'");
                }
            }

            for (int jLoop = 0; jLoop < gvEmployeePay2.Rows.Count; jLoop++)
            {
                TextBox txtAmount2 = (TextBox)gvEmployeePay2.Rows[jLoop].FindControl("txtAmount2");
                //PAY_TRIAL_RUN_DTL_DTL pAY_TRIAL_RUN_DTL_DTL = new PAY_TRIAL_RUN_DTL_DTL();
                //using (IRepository<PAY_TRIAL_RUN_DTL_DTL> userCtx = new DataRepository<PAY_TRIAL_RUN_DTL_DTL>())
                //{
                //    pAY_TRIAL_RUN_DTL_DTL = userCtx.Find(r =>
                //        (r.PAYROLL_DTL_DTL_ID == gvEmployeePay.DataKeys[iLoop].Values["PAYROLL_DTL_DTL_ID"].ToString())
                //        ).SingleOrDefault();
                //}
                //if (pAY_TRIAL_RUN_DTL_DTL != null)
                //{
                //    pAY_TRIAL_RUN_DTL_DTL.PAY_AMOUNT = decimal.Parse(DBMethod.GetAmtDecimalCommaSeparationValue(DBMethod.GetAmountDecimalValue(txt_Amount.Text).ToString()));
                //    DBMethod.SaveEntity<PAY_TRIAL_RUN_DTL_DTL> ( pAY_TRIAL_RUN_DTL_DTL,true);
                //}

                //string amtCommaSeparatedValue = DBMethod.GetAmtDecimalCommaSeparationValue(txt_Amount.Text.ToString());
                //pAY_TRIAL_RUN_DTL_DTL.PAY_AMOUNT = CommonUtils.ConvertStringToDecimal(txt_Amount.Text.ToString());
                if (gvEmployeePay.DataKeys[jLoop].Values["pay_element_Type"].ToString() == "Deduction")
                {
                    DBMethod.ExecuteNonQuery(" update pay_trial_run_dtl_dtl set pay_amount=" + CommonUtils.ConvertStringToDecimal(txtAmount2.Text.ToString()) + "where PAYROLL_DTL_DTL_ID='" + gvEmployeePay2.DataKeys[jLoop].Values["PAYROLL_DTL_DTL_ID"].ToString() + "'");
                }
            }

            ErrorCollection.Add("ElementAmountSave", "Element Amount Saved Successfully");
            Master.ShowMessage(ErrorCollection, FINAppConstants.SAVED);
        }

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();


                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("GradeEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {

            try
            {

                string str_Departments = "";
                str_Departments = "";
                for (int iLoop = 0; iLoop < cblDeptName.Items.Count; iLoop++)
                {
                    if (cblDeptName.Items[iLoop].Selected)
                    {
                        str_Departments = str_Departments + cblDeptName.Items[iLoop].Value.ToString() + ",";
                    }
                }
                str_Departments = str_Departments + "0";
                if (str_Departments.Length > 5)
                {
                    FIN.DAL.HR.PayrollTrailrunDetailsEntry.RunSPFOR_Trailpayroll(ddlGroup.SelectedValue, ddlPayrollPeriod.SelectedValue, str_Departments);

                    ErrorCollection.Clear();
                    LoadPayrollGroupWise();
                    //dtGridData = DBMethod.ExecuteQuery(PayrollTrailrunDetails_DAL.GetPayTrialRunDtl(ddlPayrollPeriod.SelectedValue, ddlGroup.SelectedValue)).Tables[0];
                    // BindGrid(dtGridData);

                }
                else
                {
                    ErrorCollection.Add("PleaseselectDepartment", "Please Select Minimum 1 Department TO Process ");
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ACT_CAL_btn_sve", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        protected void btnEmpDetails_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                DataTable dtgetdtlEmp = new DataTable();

                ModalPopupExtender2.Show();

                GridViewRow gvrP = (GridViewRow)((Control)sender).Parent.Parent;

                dtgetdtlEmp = DBMethod.ExecuteQuery(PayrollTrailrunDetails_DAL.GetDtlEmp(gvEmpDetails.DataKeys[gvrP.RowIndex].Values["EMP_ID"].ToString())).Tables[0];
                if (dtgetdtlEmp.Rows.Count > 0)
                {
                    lblEmp.Text = dtgetdtlEmp.Rows[0]["EMP_NAME"].ToString();
                    lbldept.Text = dtgetdtlEmp.Rows[0]["DEPT_NAME"].ToString();
                    lblDesig.Text = dtgetdtlEmp.Rows[0]["DESIG_NAME"].ToString();
                    lblDoj.Text = DBMethod.ConvertDateToString(dtgetdtlEmp.Rows[0]["EMP_DOJ"].ToString());
                }
                dtEmployeePay = DBMethod.ExecuteQuery(PayrollTrailrunDetails_DAL.GetPayTrialRunDtlDtl(gvEmpDetails.DataKeys[gvrP.RowIndex].Values["PAYROLL_DTL_ID"].ToString(), gvEmpDetails.DataKeys[gvrP.RowIndex].Values["EMP_ID"].ToString())).Tables[0];

                BindGrid_EmployeePayData(dtEmployeePay);
                BindGrid_EmployeePayData2(dtEmployeePay);
                
                
                if (hf_WF.Value == FINAppConstants.Y)
                {
                    btnOkay.Visible = false;

                }
                else
                {
                    btnOkay.Visible = true;
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ACT_CAL_btn_sve", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        protected void btnDetails_Click(object sender, EventArgs e)
        {
            try
            {
                GridViewRow gvrP = (GridViewRow)((Control)sender).Parent.Parent;
                dtEmployeePay = DBMethod.ExecuteQuery(PayrollTrailrunDetails_DAL.getPayTrailrunEMP4Dept(gvData.DataKeys[gvrP.RowIndex].Values["PAYROLL_DTL_ID"].ToString())).Tables[0];
                if (dtEmployeePay.Rows.Count > 0)
                {
                    dtEmployeePay.AsEnumerable().ToList().ForEach(p => p.SetField<String>("pay_amount", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("pay_amount"))));
                    dtEmployeePay.AsEnumerable().ToList().ForEach(p => p.SetField<String>("pay_ded_amount", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("pay_ded_amount"))));
                    dtEmployeePay.AsEnumerable().ToList().ForEach(p => p.SetField<String>("pay_net_amount", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("pay_net_amount"))));
                    dtEmployeePay.AcceptChanges();
                }
                gvEmpDetails.DataSource = dtEmployeePay;
                gvEmpDetails.DataBind();
                gvEmpDetails.Visible = true;
                hf_WF.Value = FINAppConstants.N;
                if (gvData.DataKeys[gvrP.RowIndex].Values["WF"].ToString() == FINAppConstants.Y)
                {
                    hf_WF.Value = FINAppConstants.Y;
                }
                gvEmpDetails.Focus();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ACT_CAL_btn_sve", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }



        protected void chkAlldept_CheckedChanged(object sender, EventArgs e)
        {
            fn_SelectAllDepartment();
        }
        private void fn_SelectAllDepartment()
        {
            bool bol_chktrue = true;
            if (chkAlldept.Checked)
            {
                bol_chktrue = true;
            }
            else
            {
                bol_chktrue = false;
            }
            for (int iLoop = 0; iLoop < cblDeptName.Items.Count; iLoop++)
            {
                cblDeptName.Items[iLoop].Selected = bol_chktrue;
            }
        }

        protected void btnApprove_Click(object sender, EventArgs e)
        {
            DataTable dtPayrollID = new DataTable();

            dtPayrollID = DBMethod.ExecuteQuery(FIN.DAL.HR.PayrollTrailrunDetailsEntry.GetPayrollID(ddlPayrollPeriod.SelectedValue, ddlGroup.SelectedValue)).Tables[0];

            HfPayrollid.Value = dtPayrollID.Rows[0]["PAYROLL_ID"].ToString();

            if (ddlPayrollStatus.SelectedItem.Text == "Approved")
            {
                FIN.DAL.HR.PayrollTrailrunDetailsEntry.RunSPFOR_GL_POSTING_PR(HfPayrollid.Value);
            }
        }

        protected void ddlGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            dtGridData = DBMethod.ExecuteQuery(PayrollTrailrunDetails_DAL.GetPayTrialRunDtl(ddlPayrollPeriod.SelectedValue, ddlGroup.SelectedValue)).Tables[0];
            BindGrid(dtGridData);
            // LoadDepartment();
        }

        protected void ddlPayrollPeriod_SelectedIndexChanged(object sender, EventArgs e)
        {
            // dtGridData = DBMethod.ExecuteQuery(PayrollTrailrunDetails_DAL.GetPayTrialRunDtl(ddlPayrollPeriod.SelectedValue, ddlGroup.SelectedValue)).Tables[0];
            // BindGrid(dtGridData);
            LoadPayrollGroupWise();
        }
        private void LoadPayrollGroupWise()
        {
            dtGridData = DBMethod.ExecuteQuery(PayrollTrailrunDetails_DAL.getPayroll4GroupWise(ddlPayrollPeriod.SelectedValue)).Tables[0];
            BingGroupGrid(dtGridData);
            gvData.Visible = false;
            gvEmpDetails.Visible = false;

        }
        private void BingGroupGrid(DataTable dtdata)
        {
            gvGropWise.DataSource = dtdata;
            gvGropWise.DataBind();
        }
        protected void btnSendApprove_Click(object sender, EventArgs e)
        {
            GridViewRow gvrP = (GridViewRow)((Control)sender).Parent.Parent;
            string str_workflowstatus = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, gvGropWise.DataKeys[gvrP.RowIndex].Values["PAYROLL_ID"].ToString());
            DBMethod.ExecuteNonQuery("Update PAY_TRIAL_RUN_HDR set Workflow_Completion_Status=" + str_workflowstatus + ",ATTRIBUTE1='FALSE' where PAYROLL_ID='" + gvGropWise.DataKeys[gvrP.RowIndex].Values["PAYROLL_ID"].ToString() + "'");
            LoadPayrollGroupWise();
        }
        protected void btnGroupDetails_Click(object sender, EventArgs e)
        {
            try
            {

                GridViewRow gvrP = (GridViewRow)((Control)sender).Parent.Parent;
                dtGridData = DBMethod.ExecuteQuery(PayrollTrailrunDetails_DAL.GetPayTrialRunDtl4PayrollId(gvGropWise.DataKeys[gvrP.RowIndex].Values["PAYROLL_ID"].ToString())).Tables[0];
                BindGrid(dtGridData);
                gvData.Visible = true;
                gvEmpDetails.Visible = false;
                gvData.Focus();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ACT_CAL_btn_sve", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void LoadDepartment()
        {
            DataTable dtdata = DBMethod.ExecuteQuery(FIN.DAL.PER.PayrollTrailrunDetails_DAL.GetDepartmentDetails4PayPeriodGroup(ddlPayrollPeriod.SelectedValue, ddlGroup.SelectedValue)).Tables[0];
            cblDeptName.DataSource = dtdata;
            cblDeptName.DataTextField = "DEPT_NAME";
            cblDeptName.DataValueField = "DEPT_ID";
            cblDeptName.DataBind();
            fn_SelectAllDepartment();
        }

        
          
       
    }
}