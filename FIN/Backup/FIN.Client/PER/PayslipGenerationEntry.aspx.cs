﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.DAL.GL;
using FIN.BLL;
using FIN.BLL.PER;
using VMVServices.Web;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Collections;

namespace FIN.Client.PER
{
    public partial class PayslipGenerationEntry : PageBase
    {
        DataTable dtEmpData = new DataTable();
     
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                AssignToControl();
            }
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
            }
            else
            {
                btnSave.Visible = true;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
        }

        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void FillComboBox()
        {
            PayrollPeriods_BLL.fn_GetPayrollPeriods(ref ddlPayPeriod);
        }

        protected void btnProcess_Click(object sender, EventArgs e)
        {
            try
            {
                ReportDocument cryRpt = new ReportDocument();
                string rptFile = null;

                if (ddlPayPeriod.SelectedValue.ToString().Length > 0)
                {
                    dtEmpData = DBMethod.ExecuteQuery(FIN.DAL.PER.PayrollPeriods_DAL.GetEmpBasedOnPayrollPeriod(ddlPayPeriod.SelectedValue.ToString())).Tables[0];
                    if (dtEmpData.Rows.Count > 0)
                    {
                        for (int iLoop = 0; iLoop < dtEmpData.Rows.Count; iLoop++)
                        {
                            //Load the report
                            getEmpReportDetails(cryRpt, dtEmpData.Rows[iLoop]["pay_emp_id"].ToString());

                            cryRpt.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, true, "EmpDetails");

                            ////Export the report to PDF to a location
                            //ExportOptions CrExportOptions;
                            //DiskFileDestinationOptions CrDiskFileDestinationOptions = new DiskFileDestinationOptions();
                            //PdfRtfWordFormatOptions CrFormatTypeOptions = new PdfRtfWordFormatOptions();
                            //CrDiskFileDestinationOptions.DiskFileName = "C:\\";
                            //CrExportOptions = cryRpt.ExportOptions;
                            //{
                            //    CrExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                            //    CrExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                            //    CrExportOptions.DestinationOptions = CrDiskFileDestinationOptions;
                            //    CrExportOptions.FormatOptions = CrFormatTypeOptions;
                            //}
                            //cryRpt.Export();
                        }
                    }
                }

                btnSave.Enabled = false;

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PayslipGenerationProcess", ex.ToString());
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }

        protected void ddlPayPeriod_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (ddlPayPeriod.SelectedValue.ToString().Length > 0)
            //{
            //    dtEmpData = DBMethod.ExecuteQuery(FIN.DAL.PER.PayrollPeriods_DAL.GetEmpBasedOnPayrollPeriod(ddlPayPeriod.SelectedValue.ToString())).Tables[0];
            //}
        }


        protected void getEmpReportDetails(ReportDocument crystRep, string empID)
        {
            Hashtable htParameters = new Hashtable();
            Hashtable htHeadingParameters = new Hashtable();
            Hashtable htFilterParameter = new Hashtable();

            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));
            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();

            ReportFile = Master.ReportName;
            Session["photoUrl"] = null;

            if (empID != string.Empty)
            {
                htFilterParameter.Add(FINColumnConstants.EMP_ID, empID);

                string photoid = "";
                string photoUrl = string.Empty;

                DataTable dtphoto = new DataTable();

                photoid = empID.ToString();
                dtphoto = DBMethod.ExecuteQuery(FIN.DAL.HR.Applicant_DAL.GetPhotoFileNameBasedEmpId(photoid.ToString())).Tables[0];

                if (dtphoto.Rows.Count > 0)
                {
                    photoUrl = "~/UploadFile/PHOTO/" + empID + "." + dtphoto.Rows[0]["FILE_EXTENSTION"].ToString().Replace(".", "");
                }
                Session["photoUrl"] = photoUrl;
            }

            if (ddlPayPeriod.SelectedValue != string.Empty)
            {
                htFilterParameter.Add(FINColumnConstants.PAY_PERIOD_ID, ddlPayPeriod.SelectedItem.Value);
            }

            VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
            ReportData = FIN.BLL.HR.Payslip_BLL.GetEmployeeDtlsReportData();
            SubReportData = FIN.BLL.HR.Payslip_BLL.GetEarningReportData();

            ReportFormulaParameter = htHeadingParameters;

            generateCrystalReport(ref crystRep, empID);
        }

        protected void generateCrystalReport(ref ReportDocument rdReport, string empID)
        {
            ErrorCollection.Clear();
            try
            {
                string reportUrl = string.Empty, strURL = string.Empty;
                DataSet dsReport = new DataSet();
                Hashtable htParameters = new Hashtable();
                Hashtable htHeadingParameters = new Hashtable();
                bool isCrossTabReport = false;
                bool isChartObject = false;

                htHeadingParameters.Remove("UserName");
                htHeadingParameters.Remove("OrgName");
                htHeadingParameters.Remove("OrgAddress1");
                htHeadingParameters.Remove("OrgAddress2");
                htHeadingParameters.Remove("OrgAddress3");
                htHeadingParameters.Remove("OrgPhone");
                htHeadingParameters.Remove("OrgMobile");
                htHeadingParameters.Remove("OrgFax");
                htHeadingParameters.Remove("ReportName");
                htHeadingParameters.Remove("RowBackGround");
                htHeadingParameters.Remove("AlternateRowBackGround");

                htHeadingParameters.Add("UserName", VMVServices.Web.Utils.UserName.ToString());
                htHeadingParameters.Add("OrgName", VMVServices.Web.Utils.OrganizationName.ToString());
                htHeadingParameters.Add("RowBackGround", "255");
                htHeadingParameters.Add("AlternateRowBackGround", "192");


                if (Session["ProgramID"].ToString().Length > 0)
                {
                    System.Data.DataTable dt_menulist = (System.Data.DataTable)Session[FIN.BLL.FINSessionConstants.MenuData];
                    System.Data.DataRow[] dr_list = dt_menulist.Select("MENU_KEY_ID=" + Session["ProgramID"].ToString());

                    if (dr_list.Length > 0)
                    {
                        if (VMVServices.Web.Utils.LanguageCode.ToString().Length > 0)
                        {
                            htHeadingParameters.Add("ReportName", dr_list[0]["ATTRIBUTE3"].ToString());
                        }
                        else
                        {
                            htHeadingParameters.Add("ReportName", dr_list[0]["ATTRIBUTE2"].ToString());
                        }
                    }
                }

                if (System.IO.File.Exists(System.Web.Configuration.WebConfigurationManager.AppSettings["OrgLogoFolder"].ToString() + VMVServices.Web.Utils.OrganizationID + ".jpg"))
                {
                    VMVServices.Web.Utils.OrgLogo = System.Web.Configuration.WebConfigurationManager.AppSettings["OrgLogoFolder"].ToString() + VMVServices.Web.Utils.OrganizationID + ".jpg";
                }

                htHeadingParameters.Add("OrgLogo", VMVServices.Web.Utils.OrgLogo.ToString());

                if (System.IO.File.Exists(System.Web.Configuration.WebConfigurationManager.AppSettings["OrgLogoFolder"].ToString() + VMVServices.Web.Utils.OrganizationID + "_GOVT.jpg"))
                {
                    htHeadingParameters.Add("OrgGOVTLogo", System.Web.Configuration.WebConfigurationManager.AppSettings["OrgLogoFolder"].ToString() + VMVServices.Web.Utils.OrganizationID + "_GOVT.jpg");
                }

                if (Session["photoUrl"] != null)
                {
                    htHeadingParameters.Add("EmpFoto", Session["photoUrl"]);
                }

                ReportFormulaParameter = htHeadingParameters;

                if (VMVServices.Web.Utils.LanguageCode.ToString().Length > 0)
                {
                    if (ReportFile.Contains("_OL"))
                    {
                        ReportFile = ReportFile.ToString().ToUpper().Replace("_OL", "");
                    }

                    ReportFile = ReportFile.ToString().ToUpper().Replace(".RPT", "_OL.rpt");

                }

                rdReport.Load(Server.MapPath(ReportFile));
                this.ApplyLogonToTables(rdReport);

                rdReport.SetDatabaseLogon(System.Configuration.ConfigurationManager.AppSettings["UNAME"].ToString(), System.Configuration.ConfigurationManager.AppSettings["PWD"].ToString(), System.Configuration.ConfigurationManager.AppSettings["DBNAME"].ToString(), System.Configuration.ConfigurationManager.AppSettings["DBNAME"].ToString());

                if (ReportParameter == null || ReportParameter.Count == 0)
                {
                    dsReport = ReportData;
                    if (dsReport != null)
                    {
                        if (dsReport.Tables.Count > 0)
                        {
                            for (int i = 0; i < dsReport.Tables.Count; i++)
                            {
                                rdReport.SetDataSource(dsReport.Tables[i]);
                            }
                        }
                    }
                    int k = 0;

                    if (SubReportData != null && SubReportData.Tables.Count > 0)
                    {
                        dsReport = SubReportData;

                        CrystalDecisions.CrystalReports.Engine.Subreports subReports = rdReport.Subreports;

                        foreach (CrystalDecisions.CrystalReports.Engine.ReportDocument subReport in subReports)
                        {
                            subReport.SetDataSource(dsReport.Tables[0]);
                            k = k + 1;

                        }
                    }
                }
                else if (ReportParameter.Count > 0)
                {
                    IDictionaryEnumerator hsEnum = ReportParameter.GetEnumerator();
                    while (hsEnum.MoveNext())
                    {
                        rdReport.SetParameterValue(hsEnum.Key.ToString(), hsEnum.Value);
                    }
                }

                if (Session["HeadingParameters"] != null)
                {
                    ReportFormulaParameter = (Hashtable)Session["HeadingParameters"];
                }

                if (ReportFormulaParameter.Count > 0)
                {
                    IDictionaryEnumerator hsEnum = ReportFormulaParameter.GetEnumerator();
                    while (hsEnum.MoveNext())
                    {
                        try
                        {
                            rdReport.DataDefinition.FormulaFields[hsEnum.Key.ToString()].Text = "'" + hsEnum.Value.ToString() + "'";
                        }
                        catch (Exception ex1)
                        {
                        }
                    }
                }

                if (VMVServices.Web.Utils.ReportFilterParameter.Count > 0)
                {
                    IDictionaryEnumerator hsEnum = VMVServices.Web.Utils.ReportFilterParameter.GetEnumerator();
                    while (hsEnum.MoveNext())
                    {
                        try
                        {
                            rdReport.DataDefinition.FormulaFields[hsEnum.Key.ToString()].Text = "'" + hsEnum.Value.ToString() + "'";
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                }

                isCrossTabReport = false;
                isChartObject = false;
                for (int iLoop = 0; iLoop < rdReport.ReportDefinition.ReportObjects.Count; iLoop++)
                {
                    if (rdReport.ReportDefinition.ReportObjects[iLoop].Kind == ReportObjectKind.CrossTabObject)
                    {
                        isCrossTabReport = true;
                    }
                    if (rdReport.ReportDefinition.ReportObjects[iLoop].Kind == ReportObjectKind.ChartObject)
                    {
                        isChartObject = true;
                    }
                }
                isCrossTabReport = true;
                isChartObject = true;
                rdReport.Refresh();
                rdReport.SetCssClass(ObjectScope.AllReportObjectsInPageFooterSections, "reportFooterTitle");
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("RPTError", ex.Message);

            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('Validation.aspx','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void ApplyLogonToTables(ReportDocument rdReport)
        {
            TableLogOnInfo ConInfo = new TableLogOnInfo();
            Tables crTables = rdReport.Database.Tables;
            foreach (CrystalDecisions.CrystalReports.Engine.Table crTable in crTables)
            {
                {
                    ConInfo.ConnectionInfo.ServerName = System.Configuration.ConfigurationManager.AppSettings["DBNAME"].ToString();
                    ConInfo.ConnectionInfo.UserID = System.Configuration.ConfigurationManager.AppSettings["UNAME"].ToString();
                    ConInfo.ConnectionInfo.Password = System.Configuration.ConfigurationManager.AppSettings["PWD"].ToString();
                }
                crTable.ApplyLogOnInfo(ConInfo);
            }

            CrystalDecisions.CrystalReports.Engine.Subreports subReports = rdReport.Subreports;

            foreach (CrystalDecisions.CrystalReports.Engine.ReportDocument subReport in subReports)
            {
                TableLogOnInfo ConInfoSub = new TableLogOnInfo();
                Tables crTablesSub = subReport.Database.Tables;
                foreach (CrystalDecisions.CrystalReports.Engine.Table crTable in crTablesSub)
                {
                    {
                        ConInfoSub.ConnectionInfo.ServerName = System.Configuration.ConfigurationManager.AppSettings["DBNAME"].ToString();
                        ConInfoSub.ConnectionInfo.UserID = System.Configuration.ConfigurationManager.AppSettings["UNAME"].ToString();
                        ConInfoSub.ConnectionInfo.Password = System.Configuration.ConfigurationManager.AppSettings["PWD"].ToString();
                    }
                    crTable.ApplyLogOnInfo(ConInfoSub);
                }
            }

            rdReport.Refresh();
        }


    }
}