﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.BLL;
using FIN.BLL.PER;
using VMVServices.Web;

namespace FIN.Client.PER
{
    public partial class PayrollEmployeeElementEntry : PageBase
    {
        PAY_EMP_ELEMENT_VALUE pAY_EMP_ELEMENT_VALUE = new PAY_EMP_ELEMENT_VALUE();
        HR_GRADES_LEVEL_DTLS hR_GRADES_LEVEL_DTLS = new HR_GRADES_LEVEL_DTLS();
        DataTable dtGridData = new DataTable();
        DataTable dt_groupName = new DataTable();
        DataTable dt_employeeName = new DataTable();
        DataTable dt_elementName = new DataTable();
        DataTable dt_grade = new DataTable();
        DataTable dt_element = new DataTable();
        Boolean bol_rowVisiable;
        Boolean savedBool;
        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();




                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PEEE_PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));


            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
        }
        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }
        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>
        /// 

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>
        /// 
        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();


                EntityData = null;
                dtGridData = FIN.BLL.PER.PayrollEmployeeElement_BLL.getChildEntityDet(Master.StrRecordId);


                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    //using (IRepository<HR_GRADES_LEVEL_DTLS> userCtx = new DataRepository<HR_GRADES_LEVEL_DTLS>())
                    //{
                    //    hR_GRADES_LEVEL_DTLS = userCtx.Find(r =>
                    //        (r.PAY_ELEMENT_ID == pAY_EMP_ELEMENT_VALUE.PAY_ELEMENT_ID.ToString())
                    //        ).SingleOrDefault();
                    //}
                    //EntityData = pAY_EMP_ELEMENT_VALUE;




                    ddlGroupCode.Enabled = false;
                    pAY_EMP_ELEMENT_VALUE = PayrollEmployeeElement_BLL.getClassEntity(Master.StrRecordId);

                    EntityData = pAY_EMP_ELEMENT_VALUE;
                    ddldept.SelectedValue = pAY_EMP_ELEMENT_VALUE.PAY_EMP_DEPT_ID;
                    fillemp();
                    ddlEmployeeNo.SelectedValue = pAY_EMP_ELEMENT_VALUE.PAY_EMP_ID.ToString();
                    fn_fill_employeeName();
                    ddlGroupCode.SelectedValue = pAY_EMP_ELEMENT_VALUE.PAY_GROUP_ID.ToString();
                    fn_fillGroupName();
                    dtGridData = FIN.BLL.PER.PayrollEmployeeElement_BLL.getChildEntityDet4EmpId(ddlEmployeeNo.SelectedValue);
                    //txtElement.Text = hR_GRADES_LEVEL_DTLS.PAY_ELEMENT_ID;
                    //txtMinLimit.Text = hR_GRADES_LEVEL_DTLS.LOW_LIMIT.ToString();
                    //txtMaxLimit.Text = hR_GRADES_LEVEL_DTLS.HIGH_LIMIT.ToString();

                }
                BindGrid(dtGridData);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PP_PEEE", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }


        }


        private void FillComboBox()
        {
            FIN.BLL.HR.Department_BLL.GetDepartmentName(ref ddldept);
            //PayrollEmployeeElement_BLL.fn_getEmployee(ref ddlEmployeeNo);
            PayrollEmployeeElement_BLL.fn_getGroup(ref ddlGroupCode);
        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                var tmpChildEntity = new List<Tuple<object, string>>();

                for (int iLoop = 0; iLoop < gvData.Rows.Count; iLoop++)
                {
                    pAY_EMP_ELEMENT_VALUE = new PAY_EMP_ELEMENT_VALUE();
                    if (gvData.DataKeys[iLoop].Values["PAY_EMP_ELEMENT_ID"].ToString() != "0")
                    {
                        using (IRepository<PAY_EMP_ELEMENT_VALUE> userCtx = new DataRepository<PAY_EMP_ELEMENT_VALUE>())
                        {
                            pAY_EMP_ELEMENT_VALUE = userCtx.Find(r =>
                                (r.PAY_EMP_ELEMENT_ID == gvData.DataKeys[iLoop].Values["PAY_EMP_ELEMENT_ID"].ToString())
                                ).SingleOrDefault();
                        }

                        //using (IRepository<HR_GRADES_LEVEL_DTLS> userCtx = new DataRepository<HR_GRADES_LEVEL_DTLS>())
                        //{
                        //    hR_GRADES_LEVEL_DTLS = userCtx.Find(r =>
                        //        (r.PAY_ELEMENT_ID ==pAY_EMP_ELEMENT_VALUE.PAY_ELEMENT_ID)
                        //        ).SingleOrDefault();
                        //}
                    }
                    pAY_EMP_ELEMENT_VALUE.PAY_EMP_DEPT_ID = ddldept.SelectedValue;
                    pAY_EMP_ELEMENT_VALUE.PAY_EMP_ID = ddlEmployeeNo.SelectedValue.ToString();
                    pAY_EMP_ELEMENT_VALUE.PAY_GROUP_ID = ddlGroupCode.SelectedValue.ToString();
                    pAY_EMP_ELEMENT_VALUE.PAY_ELEMENT_ID = gvData.DataKeys[iLoop].Values["PAY_ELEMENT_ID"].ToString();// dtGridData.Rows[iLoop]["PAY_ELEMENT_ID"].ToString();
                    TextBox txt_payamount = (TextBox)gvData.Rows[iLoop].FindControl("txtAmount");
                    if (txt_payamount.Text.ToString().Length > 0)
                    {



                        pAY_EMP_ELEMENT_VALUE.PAY_AMOUNT = CommonUtils.ConvertStringToDecimal(txt_payamount.Text);
                        TextBox txt_FromDate = (TextBox)gvData.Rows[iLoop].FindControl("dtpfromDate");
                        if (txt_FromDate.Text.ToString().Length > 0)
                        {
                            pAY_EMP_ELEMENT_VALUE.EFFECTIVE_FROM_DT = DBMethod.ConvertStringToDate(txt_FromDate.Text.ToString());
                        }
                        TextBox txt_ToDate = (TextBox)gvData.Rows[iLoop].FindControl("dtpToDate");
                        if (txt_ToDate.Text.ToString().Length > 0)
                        {
                            pAY_EMP_ELEMENT_VALUE.EFFECTIVE_TO_DT = DBMethod.ConvertStringToDate(txt_ToDate.Text);
                        }
                        else
                        {
                            pAY_EMP_ELEMENT_VALUE.EFFECTIVE_TO_DT = null;
                        }

                        CheckBox chk_ELEPRO = (CheckBox)gvData.Rows[iLoop].FindControl("chkprorate");
                        if (chk_ELEPRO.Checked)
                        {
                            pAY_EMP_ELEMENT_VALUE.PAY_ELEMENT_PRORATE = FINAppConstants.Y;
                        }
                        else
                        {
                            pAY_EMP_ELEMENT_VALUE.PAY_ELEMENT_PRORATE = FINAppConstants.N;
                        }
                        CheckBox chk_AnnLeave = (CheckBox)gvData.Rows[iLoop].FindControl("chkannualleave");
                        if (chk_AnnLeave.Checked)
                        {
                            pAY_EMP_ELEMENT_VALUE.PAID_FOR_ANNUAL_LEAVE = FINAppConstants.Y;
                        }
                        else
                        {
                            pAY_EMP_ELEMENT_VALUE.PAID_FOR_ANNUAL_LEAVE = FINAppConstants.N;
                        }

                        pAY_EMP_ELEMENT_VALUE.ENABLED_FLAG = "1";

                        pAY_EMP_ELEMENT_VALUE.PAY_ORG_ID = VMVServices.Web.Utils.OrganizationID;
                        //  hR_CATEGORIES.WORKFLOW_COMPLETION_STATUS = "1";



                        if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                        {

                            tmpChildEntity.Add(new Tuple<object, string>(pAY_EMP_ELEMENT_VALUE, "D"));
                            DBMethod.DeleteEntity<PAY_EMP_ELEMENT_VALUE>(pAY_EMP_ELEMENT_VALUE);
                        }
                        else
                        {
                            if (gvData.DataKeys[iLoop].Values["PAY_EMP_ELEMENT_ID"].ToString() != "0")
                            {
                                pAY_EMP_ELEMENT_VALUE.PAY_EMP_ELEMENT_ID = gvData.DataKeys[iLoop].Values["PAY_EMP_ELEMENT_ID"].ToString();// dtGridData.Rows[iLoop]["PAY_EMP_ELEMENT_ID"].ToString();
                                pAY_EMP_ELEMENT_VALUE.MODIFIED_BY = this.LoggedUserName;
                                pAY_EMP_ELEMENT_VALUE.MODIFIED_DATE = DateTime.Today;
                                pAY_EMP_ELEMENT_VALUE.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, pAY_EMP_ELEMENT_VALUE.PAY_EMP_ELEMENT_ID);
                                DBMethod.SaveEntity<PAY_EMP_ELEMENT_VALUE>(pAY_EMP_ELEMENT_VALUE, true);
                                //tmpChildEntity.Add(new Tuple<object, string>(hR_CATEGORIES, "U"));

                            }
                            else
                            {

                                pAY_EMP_ELEMENT_VALUE.PAY_EMP_ELEMENT_ID = FINSP.GetSPFOR_SEQCode("PER_006".ToString(), false, true);
                                pAY_EMP_ELEMENT_VALUE.CREATED_BY = this.LoggedUserName;
                                pAY_EMP_ELEMENT_VALUE.CREATED_DATE = DateTime.Today;
                                pAY_EMP_ELEMENT_VALUE.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, pAY_EMP_ELEMENT_VALUE.PAY_EMP_ELEMENT_ID);
                                DBMethod.SaveEntity<PAY_EMP_ELEMENT_VALUE>(pAY_EMP_ELEMENT_VALUE);
                                // tmpChildEntity.Add(new Tuple<object, string>(hR_CATEGORIES, "A"));
                            }



                        }
                    }
                    else
                    {
                        if (gvData.DataKeys[iLoop].Values["PAY_EMP_ELEMENT_ID"].ToString() != "0")
                        {
                            pAY_EMP_ELEMENT_VALUE.PAY_EMP_ELEMENT_ID = gvData.DataKeys[iLoop].Values["PAY_EMP_ELEMENT_ID"].ToString();// dtGridData.Rows[iLoop]["PAY_EMP_ELEMENT_ID"].ToString();
                            DBMethod.DeleteEntity<PAY_EMP_ELEMENT_VALUE>(pAY_EMP_ELEMENT_VALUE);
                        }
                    }

                }

                //switch (Master.Mode)
                //{
                //    case FINAppConstants.Add:
                //        {
                //            FIN.BLL.GL.AccountCodes_BLL.SavePCEntity<INV_TRANSFER_HDR, INV_TRANSFER_DTL>(iNV_TRANSFER_HDR, tmpChildEntity, iNV_TRANSFER_DTL);
                //            break;
                //        }
                //    case FINAppConstants.Update:
                //        {

                //            FIN.BLL.GL.AccountCodes_BLL.SavePCEntity<INV_TRANSFER_HDR, INV_TRANSFER_DTL>(iNV_TRANSFER_HDR, tmpChildEntity, iNV_TRANSFER_DTL, true);
                //            break;

                //        }
                //}



            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PP_PGEE", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void ddlEmployeeNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            fn_fill_employeeName();
        }

        protected void ddlElementCode_SelectedIndexChanged(object sender, EventArgs e)
        {

            fn_fill_ElemetDescription(sender, e);
        }

        private void fn_fill_ElemetDescription(object sender, EventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;

            CheckBox chkprorate = gvr.FindControl("chkprorate") as CheckBox;
            CheckBox chkannualleave = gvr.FindControl("chkannualleave") as CheckBox;

            DropDownList ddl_ElementCode = gvr.FindControl("ddlElementCode") as DropDownList;
            TextBox txt_ElementDescription = gvr.FindControl("txtElementDescription") as TextBox;
            dt_elementName = DBMethod.ExecuteQuery(FIN.DAL.PER.PayrollEmployeeElement_DAL.getPayElements_Name(ddl_ElementCode.SelectedValue.ToString())).Tables[0];

            if (dt_elementName != null)
            {
                if (dt_elementName.Rows.Count > 0)
                {
                    txt_ElementDescription.Text = dt_elementName.Rows[0][0].ToString();
                    txt_ElementDescription.Enabled = false;
                }
            }

            if (dt_elementName.Rows[0]["PAY_ELEMENT_PRORATE"].ToString().ToUpper() == "TRUE")
            {
                chkprorate.Checked = true;
            }
            else
            {
                chkprorate.Checked = false;
            }

            if (dt_elementName.Rows[0]["PAID_FOR_ANNUAL_LEAVE"].ToString().ToUpper() == "TRUE")
            {
                chkannualleave.Checked = true;
            }
            else
            {
                chkannualleave.Checked = false;
            }

        }

        private void fn_fill_employeeName()
        {
            dt_employeeName = DBMethod.ExecuteQuery(FIN.DAL.PER.PayrollEmployeeElement_DAL.getEmployeeName(ddlEmployeeNo.SelectedValue.ToString())).Tables[0];
            dt_grade = DBMethod.ExecuteQuery(FIN.DAL.PER.PayrollEmployeeElement_DAL.getGrade(ddlEmployeeNo.SelectedValue.ToString())).Tables[0];
            dt_element = DBMethod.ExecuteQuery(FIN.DAL.PER.PayrollEmployeeElement_DAL.getElement(ddlEmployeeNo.SelectedValue.ToString())).Tables[0];
            if (dt_employeeName != null)
            {
                if (dt_employeeName.Rows.Count > 0)
                {
                    txtEmployeeName.Text = dt_employeeName.Rows[0][0].ToString();
                    txtEmployeeName.Enabled = false;
                }
            }
            if (dt_grade != null)
            {
                if (dt_grade.Rows.Count > 0)
                {
                    txtGrade.Text = dt_grade.Rows[0][0].ToString();
                    txtGrade.Enabled = false;
                }
            }
            if (dt_element != null)
            {
                if (dt_element.Rows.Count > 0)
                {
                    txtElement.Text = dt_element.Rows[0][0].ToString();
                    txtElement.Enabled = false;

                    txtMinLimit.Text = dt_element.Rows[0][1].ToString();
                    txtMinLimit.Enabled = false;
                    txtMaxLimit.Text = dt_element.Rows[0][2].ToString();
                    txtMaxLimit.Enabled = false;
                }
            }

            dtGridData = FIN.BLL.PER.PayrollEmployeeElement_BLL.getChildEntityDet4EmpId(ddlEmployeeNo.SelectedValue);
            if (dtGridData.Rows.Count > 0)
            {
                BindGrid(dtGridData);
            }
            else
            {
                bindElement();
            }
        }


        protected void ddlGroupCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            fn_fillGroupName();
            bindElement();
        }


        private void fn_fillGroupName()
        {
            dt_groupName = DBMethod.ExecuteQuery(FIN.DAL.PER.PayrollEmployeeElement_DAL.getPayGroup_Name(ddlGroupCode.SelectedValue.ToString())).Tables[0];
            if (dt_groupName != null)
            {
                if (dt_groupName.Rows.Count > 0)
                {
                    txtDescription.Text = dt_groupName.Rows[0][0].ToString();
                    txtDescription.Enabled = false;
                }
            }




        }

        private void bindElement()
        {
            dtGridData = DBMethod.ExecuteQuery(FIN.DAL.PER.PayrollEmployeeElement_DAL.GetPayPerioddtls_Bsdon_Grpcode(ddlGroupCode.SelectedValue)).Tables[0];
            BindGrid(dtGridData);
        }

        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddl_ElementCode = tmpgvr.FindControl("ddlElementCode") as DropDownList;
                PayrollEmployeeElement_BLL.fn_getElement(ref ddl_ElementCode);


                if (gvData.EditIndex >= 0)
                {
                    ddl_ElementCode.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.PAY_ELEMENT_ID].ToString();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PEEE_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session[FINSessionConstants.GridData], "Payroll Group");

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                //if (dtGridData.Select("PAY_AMOUNT is null").Length > 0)
                //{
                //    ErrorCollection.Add("amtnotnull", "Amount cannot be null");
                //    return;
                //}

                AssignToBE();

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        #endregion

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;

                if (dtData.Rows.Count > 0)
                {
                    if (dtData.Rows[0]["PAY_AMOUNT"].ToString().Length > 0)
                    {
                        dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("PAY_AMOUNT", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("PAY_AMOUNT"))));
                        dtData.AcceptChanges();
                    }
                }

                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr["ENABLED_FLAG"] = "FALSE";
                    dr["PAY_ELEMENT_PRORATE"] = "FALSE";
                    dr["PAID_FOR_ANNUAL_LEAVE"] = "FALSE";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PP_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            try
            {
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = -1;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PGEE_ROW_CNCL_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>
        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PEEE_ROW_CMD", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>
        /// 
        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PGEE_ROW_DEL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>
        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PGEE_ROW_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>
        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PGEE_RowCreated", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PGEE_RDB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PGEE_ROW_UP", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {


            System.Collections.SortedList slControls = new System.Collections.SortedList();


            DropDownList ddl_ElementCode = gvr.FindControl("ddlElementCode") as DropDownList;
            TextBox txt_ElementDescription = gvr.FindControl("txtElementDescription") as TextBox;
            TextBox txt_Amount = gvr.FindControl("txtAmount") as TextBox;
            TextBox dtp_fromDate = gvr.FindControl("dtpfromDate") as TextBox;
            TextBox dtp_ToDate = gvr.FindControl("dtpToDate") as TextBox;
            CheckBox chkprorate = gvr.FindControl("chkprorate") as CheckBox;
            CheckBox chkannualleave = gvr.FindControl("chkannualleave") as CheckBox;

            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList["PAY_EMP_ELEMENT_ID"] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }

            slControls[0] = ddl_ElementCode;
            slControls[1] = txt_Amount;
            slControls[2] = dtp_fromDate;
            slControls[3] = dtp_fromDate;
            slControls[4] = dtp_ToDate;



            ErrorCollection.Clear();
            string strCtrlTypes = "DropDownList~TextBox~TextBox~DateTime~DateRangeValidate";
            string strMessage = "Element Code ~ Amount ~ From Date ~ From Date ~ To Date";
            ErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);
            if (ErrorCollection.Count > 0)
                return drList;

            ErrorCollection.Clear();

            //CheckAmtLimit();

            ErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);
            if (ErrorCollection.Count > 0)
                return drList;



            bool bol_ValuLimit = true;
            DateTime dbl_StartDate = new DateTime();
            DateTime dbl_EndDate = new DateTime();
            string dbl_Ele_Code = "0";

            for (int gLoop = 0; gLoop < gvData.Rows.Count; gLoop++)
            {

                TextBox txt_startDate = gvData.Rows[gLoop].FindControl("dtpfromDate") as TextBox;
                TextBox txt_EndDate = gvData.Rows[gLoop].FindControl("dtpToDate") as TextBox;
                dbl_StartDate = DBMethod.ConvertStringToDate(txt_startDate.Text);

                dbl_Ele_Code = gvData.DataKeys[gLoop].Values["PAY_ELEMENT_ID"].ToString();

                if (ddl_ElementCode.SelectedValue == dt_tmp.Rows[gLoop]["PAY_ELEMENT_ID"].ToString())
                {
                    if (txt_EndDate.Text.ToString().Length == 0)
                    {
                        string strCondition = "PAY_ELEMENT_ID='" + ddl_ElementCode.SelectedValue + "'";
                        strMessage = FINMessageConstatns.RecordAlreadyExists;
                        ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
                        if (ErrorCollection.Count > 0)
                        {
                            return drList;
                        }
                    }
                    else
                    {

                        dbl_EndDate = DBMethod.ConvertStringToDate(txt_EndDate.Text);
                        if (DBMethod.ConvertStringToDate(dtp_fromDate.Text) <= dbl_EndDate)
                        {
                            bol_ValuLimit = false;
                            break;
                        }

                        //if (((DBMethod.ConvertStringToDate(dtp_fromDate.Text) >= dbl_StartDate) && (DBMethod.ConvertStringToDate(dtp_fromDate.Text) <= dbl_EndDate)))
                        //{
                        //    bol_ValuLimit = false;
                        //    break;
                        //}
                        //if (((DBMethod.ConvertStringToDate(dtp_ToDate.Text) >= dbl_StartDate) && ((DBMethod.ConvertStringToDate(dtp_ToDate.Text)) <= dbl_EndDate)))
                        //{
                        //    bol_ValuLimit = false;
                        //    break;
                        //}

                        //if (((DBMethod.ConvertStringToDate(dtp_fromDate.Text) <= dbl_StartDate) && (DBMethod.ConvertStringToDate(dtp_ToDate.Text) >= dbl_EndDate)))
                        //{
                        //    bol_ValuLimit = false;
                        //    break;
                        //}

                    }
                }

            }

            if (!bol_ValuLimit)
            {
                ErrorCollection.Add("InvalidValueLimit", "Invalid date range for the particular element code.");
                return drList;
            }



            drList["PAY_ELEMENT_ID"] = ddl_ElementCode.SelectedValue;
            drList["PAY_ELEMENT_CODE"] = ddl_ElementCode.SelectedItem.Text;
            drList["PAY_ELEMENT_DESC"] = ddl_ElementCode.SelectedItem.Text;

            drList["PAY_AMOUNT"] = txt_Amount.Text;
            //fn_fill_element_name();

            if (dtp_fromDate.Text.ToString().Length > 0)
            {
                drList["EFFECTIVE_FROM_DT"] = DBMethod.ConvertStringToDate(dtp_fromDate.Text.ToString());
            }

            if (dtp_ToDate.Text.ToString().Length > 0)
            {

                drList["EFFECTIVE_TO_DT"] = DBMethod.ConvertStringToDate(dtp_ToDate.Text.ToString());
            }
            else
            {
                drList["EFFECTIVE_TO_DT"] = DBNull.Value;
            }

            if (chkprorate.Checked)
            {
                drList["PAY_ELEMENT_PRORATE"] = "TRUE";
            }
            else
            {
                drList["PAY_ELEMENT_PRORATE"] = "FALSE";
            }

            if (chkannualleave.Checked)
            {
                drList["PAID_FOR_ANNUAL_LEAVE"] = "TRUE";
            }
            else
            {
                drList["PAID_FOR_ANNUAL_LEAVE"] = "FALSE";
            }

            drList[FINColumnConstants.DELETED] = FINAppConstants.N;


            return drList;



        }

        #endregion

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {

                    pAY_EMP_ELEMENT_VALUE.PAY_EMP_ELEMENT_ID = dtGridData.Rows[iLoop]["PAY_EMP_ELEMENT_ID"].ToString();
                    DBMethod.DeleteEntity<PAY_EMP_ELEMENT_VALUE>(pAY_EMP_ELEMENT_VALUE);
                }


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PEEE_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {

        }

        protected void ddldept_SelectedIndexChanged(object sender, EventArgs e)
        {
            fillemp();
        }

        private void fillemp()
        {
            FIN.BLL.HR.Employee_BLL.GetEmplName(ref ddlEmployeeNo, ddldept.SelectedValue);
        }

        private void CheckAmtLimit()
        {
            try
            {
                int count = 0;
                for (int iLoop = 0; iLoop < gvData.Rows.Count; iLoop++)
                {
                    TextBox txt_Amount = (TextBox)gvData.Rows[iLoop].FindControl("txtAmount");
                    if (txt_Amount != null)
                    {
                        count = int.Parse(txt_Amount.Text);
                    }
                }
                if (count > int.Parse(txtMaxLimit.Text))
                {
                    ErrorCollection.Add("PEEE_BTNS", "Amount should not exceed maximum limit");
                    return;
                }
                if (count < int.Parse(txtMinLimit.Text))
                {
                    ErrorCollection.Add("PEEE_BTNS", "Amount should not be less than minimum limit");
                    return;
                }
            }

            catch (Exception ex)
            {
                ErrorCollection.Add("PEEE_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void txtAmount_TextChanged(object sender, EventArgs e)
        {

            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            TextBox txt_Amount = gvr.FindControl("txtAmount") as TextBox;
            dtGridData = (DataTable)Session[FINSessionConstants.GridData];
            if (dtGridData.Rows.Count > 0)
            {
                dtGridData.Rows[gvr.RowIndex]["PAY_AMOUNT"] = txt_Amount.Text;
            }
            Session[FINSessionConstants.GridData] = dtGridData;
        }

        protected void dtpfromDate_TextChanged(object sender, EventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            TextBox dtp_fromDate = gvr.FindControl("dtpfromDate") as TextBox;
            dtGridData = (DataTable)Session[FINSessionConstants.GridData];
            if (dtGridData.Rows.Count > 0)
            {
                dtGridData.Rows[gvr.RowIndex]["EFFECTIVE_FROM_DT"] = dtp_fromDate.Text;
            }
            Session[FINSessionConstants.GridData] = dtGridData;
        }

        protected void dtpToDate_TextChanged(object sender, EventArgs e)
        {
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            TextBox dtp_ToDate = gvr.FindControl("dtpToDate") as TextBox;
            dtGridData = (DataTable)Session[FINSessionConstants.GridData];
            if (dtGridData.Rows.Count > 0)
            {
                dtGridData.Rows[gvr.RowIndex]["EFFECTIVE_TO_DT"] = dtp_ToDate.Text;
            }
            Session[FINSessionConstants.GridData] = dtGridData;
        }

    }
}