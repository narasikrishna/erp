﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.BLL;
using FIN.BLL.PER;
using VMVServices.Web;

namespace FIN.Client.PER
{
    public partial class PayrollGroupElementEntry : PageBase
    {
        PAY_GROUP_ELEMENT_MAPPING pAY_GROUP_ELEMENT_MAPPING = new PAY_GROUP_ELEMENT_MAPPING();
        DataTable dtGridData = new DataTable();
        DataTable dt_groupName = new DataTable();

        PAY_ELEMENTS pAY_ELEMENTS = new PAY_ELEMENTS();
        PayrollElements_BLL payrollElements_BLL = new PayrollElements_BLL();

        DataTable dt_elementName = new DataTable();
        PayrollGroupElement_BLL payrollGroupElement_BLL = new PayrollGroupElement_BLL();
        Boolean bol_rowVisiable;
        Boolean savedBool;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PP_PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));


            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
        }
        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }
        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>
        /// 

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();


                EntityData = null;

                dtGridData = FIN.BLL.PER.PayrollGroupElement_BLL.getChildEntityDet(Master.StrRecordId);


                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    pAY_GROUP_ELEMENT_MAPPING = PayrollGroupElement_BLL.getClassEntity(Master.StrRecordId);

                    EntityData = pAY_GROUP_ELEMENT_MAPPING;

                    ddlGroupCode.SelectedValue = pAY_GROUP_ELEMENT_MAPPING.PAY_GROUP_ID.ToString();
                    fn_fill_group_name();
                    dtGridData = FIN.BLL.PER.PayrollGroupElement_BLL.getChildEntityDet(pAY_GROUP_ELEMENT_MAPPING.PAY_GROUP_ID);

                }
                BindGrid(dtGridData);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PP_ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }


        }


        private void FillComboBox()
        {
            payrollGroupElement_BLL.fn_getGroup(ref ddlGroupCode);
        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                var tmpChildEntity = new List<Tuple<object, string>>();

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    pAY_GROUP_ELEMENT_MAPPING = new PAY_GROUP_ELEMENT_MAPPING();
                    if (dtGridData.Rows[iLoop]["PG_ELE_ID"].ToString() != "0")
                    {
                        using (IRepository<PAY_GROUP_ELEMENT_MAPPING> userCtx = new DataRepository<PAY_GROUP_ELEMENT_MAPPING>())
                        {
                            pAY_GROUP_ELEMENT_MAPPING = userCtx.Find(r =>
                                (r.PG_ELE_ID == dtGridData.Rows[iLoop]["PG_ELE_ID"].ToString())
                                ).SingleOrDefault();
                        }
                    }

                    pAY_GROUP_ELEMENT_MAPPING.PAY_GROUP_ID = ddlGroupCode.SelectedValue.ToString();
                    pAY_GROUP_ELEMENT_MAPPING.PAY_ELEMENT_ID = dtGridData.Rows[iLoop]["PAY_ELEMENT_ID"].ToString();
                    if (dtGridData.Rows[iLoop]["EFFECTIVE_FROM_DT"] != DBNull.Value)
                    {
                        pAY_GROUP_ELEMENT_MAPPING.EFFECTIVE_FROM_DT = DateTime.Parse(dtGridData.Rows[iLoop]["EFFECTIVE_FROM_DT"].ToString());
                    }

                    if (dtGridData.Rows[iLoop]["EFFECTIVE_TO_DT"] != DBNull.Value)
                    {
                        pAY_GROUP_ELEMENT_MAPPING.EFFECTIVE_TO_DT = DateTime.Parse(dtGridData.Rows[iLoop]["EFFECTIVE_TO_DT"].ToString());
                    }



                    pAY_GROUP_ELEMENT_MAPPING.ENABLED_FLAG = "1";

                    pAY_GROUP_ELEMENT_MAPPING.PAY_ORG_ID = VMVServices.Web.Utils.OrganizationID;
                    //  hR_CATEGORIES.WORKFLOW_COMPLETION_STATUS = "1";



                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {

                        tmpChildEntity.Add(new Tuple<object, string>(pAY_GROUP_ELEMENT_MAPPING, "D"));
                    }
                    else
                    {
                        if (dtGridData.Rows[iLoop]["PG_ELE_ID"].ToString() != "0")
                        {
                            pAY_GROUP_ELEMENT_MAPPING.PG_ELE_ID = dtGridData.Rows[iLoop]["PG_ELE_ID"].ToString();
                            pAY_GROUP_ELEMENT_MAPPING.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, pAY_GROUP_ELEMENT_MAPPING.PG_ELE_ID);
                            pAY_GROUP_ELEMENT_MAPPING.MODIFIED_BY = this.LoggedUserName;
                            pAY_GROUP_ELEMENT_MAPPING.MODIFIED_DATE = DateTime.Today;
                            DBMethod.SaveEntity<PAY_GROUP_ELEMENT_MAPPING>(pAY_GROUP_ELEMENT_MAPPING, true);
                            //tmpChildEntity.Add(new Tuple<object, string>(hR_CATEGORIES, "U"));

                        }
                        else
                        {

                            pAY_GROUP_ELEMENT_MAPPING.PG_ELE_ID = FINSP.GetSPFOR_SEQCode("PER_004".ToString(), false, true);
                            pAY_GROUP_ELEMENT_MAPPING.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, pAY_GROUP_ELEMENT_MAPPING.PG_ELE_ID);
                            pAY_GROUP_ELEMENT_MAPPING.CREATED_BY = this.LoggedUserName;
                            pAY_GROUP_ELEMENT_MAPPING.CREATED_DATE = DateTime.Today;
                            DBMethod.SaveEntity<PAY_GROUP_ELEMENT_MAPPING>(pAY_GROUP_ELEMENT_MAPPING);
                            // tmpChildEntity.Add(new Tuple<object, string>(hR_CATEGORIES, "A"));
                        }



                    }

                }

                //switch (Master.Mode)
                //{
                //    case FINAppConstants.Add:
                //        {
                //            FIN.BLL.GL.AccountCodes_BLL.SavePCEntity<INV_TRANSFER_HDR, INV_TRANSFER_DTL>(iNV_TRANSFER_HDR, tmpChildEntity, iNV_TRANSFER_DTL);
                //            break;
                //        }
                //    case FINAppConstants.Update:
                //        {

                //            FIN.BLL.GL.AccountCodes_BLL.SavePCEntity<INV_TRANSFER_HDR, INV_TRANSFER_DTL>(iNV_TRANSFER_HDR, tmpChildEntity, iNV_TRANSFER_DTL, true);
                //            break;

                //        }
                //}



            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PP_PGEE", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddl_ElementCode = tmpgvr.FindControl("ddlElementCode") as DropDownList;
                payrollGroupElement_BLL.fn_getElement(ref ddl_ElementCode);


                if (gvData.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                {
                    ddl_ElementCode.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.PAY_ELEMENT_ID].ToString();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PP_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session[FINSessionConstants.GridData], "Payroll Group Element");

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                AssignToBE();

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        #endregion

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr["ENABLED_FLAG"] = "FALSE";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PP_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            try
            {
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = -1;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PGEE_ROW_CNCL_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>
        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PGEE_ROW_CMD", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>
        /// 
        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PGEE_ROW_DEL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>
        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PGEE_ROW_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>
        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PGEE_RowCreated", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PGEE_RDB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PGEE_ROW_UP", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {


            System.Collections.SortedList slControls = new System.Collections.SortedList();


            DropDownList ddl_ElementCode = gvr.FindControl("ddlElementCode") as DropDownList;
            TextBox txt_ElementDescription = gvr.FindControl("txtElementDescription") as TextBox;
            TextBox dtp_fromDate = gvr.FindControl("dtpfromDate") as TextBox;
            TextBox dtp_ToDate = gvr.FindControl("dtpToDate") as TextBox;


            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList["PG_ELE_ID"] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }

            slControls[0] = ddl_ElementCode;
            slControls[1] = dtp_fromDate;
            slControls[2] = dtp_fromDate;
            slControls[3] = dtp_ToDate;
            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/PER_" + Session["Sel_Lng"].ToString() + ".properties"));


            ErrorCollection.Clear();
            string strCtrlTypes = "DropDownList~TextBox~DateTime~DateRangeValidate";
            string strMessage = Prop_File_Data["Element_Code_P"] + " ~ " + Prop_File_Data["From_Date_P"] + " ~ " + Prop_File_Data["From_Date_P"] + " ~ " + Prop_File_Data["To_Date_P"] + "";
            // string strMessage = "Element Code ~ From Date ~ From Date ~ To Date";
            ErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);
            if (ErrorCollection.Count > 0)
                return drList;

            //Data Duplication - Grid
            DataTable dtelementcode = new DataTable();
            dtelementcode = DBMethod.ExecuteQuery(GetElementCode(ddl_ElementCode.SelectedValue, drList["PAY_ELEMENT_ID"].ToString(), dtp_fromDate.Text, dtp_ToDate.Text)).Tables[0];

            if (dtelementcode.Rows.Count > 0)
            {
                ErrorCollection.Add("chkelementcode", Prop_File_Data["Element_Code_Aready_P"]);
                return drList;
            }

            string strCondition = "PAY_ELEMENT_CODE='" + ddl_ElementCode.SelectedValue.Trim().ToUpper() + "'";
            strMessage = FINMessageConstatns.ElementCodeAlreadyExists;
            ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            if (ErrorCollection.Count > 0)
            {
                return drList;
            }



            //string strCondition = "CATEGORY_ID='" + txtcode.Text + "'";
            //strMessage = FINMessageConstatns.RecordAlreadyExists;
            //ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            //if (ErrorCollection.Count > 0)
            //{
            //    return drList;
            //}




            drList["PAY_ELEMENT_ID"] = ddl_ElementCode.SelectedValue;
            drList["PAY_ELEMENT_CODE"] = ddl_ElementCode.SelectedItem.Text;
            drList["PAY_ELEMENT_DESC"] = txt_ElementDescription.Text;
            //fn_fill_element_name();

            if (dtp_fromDate.Text.ToString().Length > 0)
            {
                drList["EFFECTIVE_FROM_DT"] = DBMethod.ConvertStringToDate(dtp_fromDate.Text.ToString());
            }

            if (dtp_ToDate.Text.ToString().Length > 0)
            {

                drList["EFFECTIVE_TO_DT"] = DBMethod.ConvertStringToDate(dtp_ToDate.Text.ToString());
            }
            else
            {
                drList["EFFECTIVE_TO_DT"] = DBNull.Value;
            }


            drList[FINColumnConstants.DELETED] = FINAppConstants.N;


            return drList;



        }

        #endregion
        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {

                    pAY_GROUP_ELEMENT_MAPPING.PG_ELE_ID = dtGridData.Rows[iLoop]["PG_ELE_ID"].ToString();
                    DBMethod.DeleteEntity<PAY_GROUP_ELEMENT_MAPPING>(pAY_GROUP_ELEMENT_MAPPING);
                }


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PGEE_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {

        }

        protected void ddlGroupCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                fn_fill_group_name();
                getElementDetails();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PayrollGroupElement", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void ddlElementCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            fn_fill_element_name(sender, e);
        }

        private void getElementDetails()
        {
            DataTable dtElementDtl = new DataTable();
            dtElementDtl = DBMethod.ExecuteQuery(FIN.DAL.PER.PayrollGroupElement_DAL.geTEmpelementdetails(ddlGroupCode.SelectedValue.ToString())).Tables[0];
            BindGrid(dtElementDtl);
        }

        private void fn_fill_group_name()
        {

            dt_groupName = DBMethod.ExecuteQuery(FIN.DAL.PER.PayrollGroupElement_DAL.getPayGroup_Name(ddlGroupCode.SelectedValue.ToString())).Tables[0];
            if (dt_groupName != null)
            {
                if (dt_groupName.Rows.Count > 0)
                {
                    txtDescription.Text = dt_groupName.Rows[0][0].ToString();
                    txtDescription.Enabled = false;
                }
            }

        }

        private void fn_fill_element_name(object sender, EventArgs e)
        {
            //GridViewRow gvr = gvData.FooterRow;
            GridViewRow gvr = (GridViewRow)((Control)sender).Parent.Parent;
            DropDownList ddl_ElementCode = gvr.FindControl("ddlElementCode") as DropDownList;
            TextBox txt_ElementDescription = gvr.FindControl("txtElementDescription") as TextBox;
            dt_elementName = DBMethod.ExecuteQuery(FIN.DAL.PER.PayrollGroupElement_DAL.getElement_Name(ddl_ElementCode.SelectedValue.ToString())).Tables[0];
            if (dt_elementName != null)
            {
                if (dt_elementName.Rows.Count > 0)
                {

                    txt_ElementDescription.Text = dt_elementName.Rows[0][0].ToString();
                    txt_ElementDescription.Enabled = false;
                }
            }
        }

        public string GetElementCode(string pay_element_code, String PAY_ELEMENT_ID, string EFFECTIVE_FROM_DT, string EFFECTIVE_TO_DT)
        {
            string sqlQuery = string.Empty;

            sqlQuery = " SELECT TRIM(PE.PAY_ELEMENT_CODE)";
            sqlQuery += " FROM PAY_ELEMENTS PE";
            sqlQuery += " WHERE TRIM(PE.EFFECTIVE_FROM_DT) = to_date('" + EFFECTIVE_FROM_DT + "','dd/MM/yyyy')";
            if (EFFECTIVE_TO_DT != string.Empty)
            {
                sqlQuery += " AND TRIM(NVL(PE.EFFECTIVE_TO_DT,SYSDATE)) =  to_date('" + EFFECTIVE_TO_DT + "','dd/MM/yyyy')";
            }
            sqlQuery += " AND TRIM(pe.pay_element_code) = '" + pay_element_code + "'";
            if (PAY_ELEMENT_ID.ToString() != null)
            {

                sqlQuery += " AND PE.PAY_ELEMENT_ID <> '" + PAY_ELEMENT_ID + "'";
            }

            return sqlQuery;
        }


    }
}