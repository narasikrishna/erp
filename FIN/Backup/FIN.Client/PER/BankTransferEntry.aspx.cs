﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.BLL;
using FIN.BLL.GL;
using FIN.DAL.CA;
using FIN.BLL.CA;
using FIN.BLL.AP;
using FIN.DAL.AP;
using VMVServices.Web;
using System.Text;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.VisualBasic;
using System.Collections;
using System.Diagnostics;
using System.Web.SessionState;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using System.Configuration;
using System.Drawing;
using VMVServices.Services.Data;


namespace FIN.Client.CA
{
    public partial class BankTransfer : PageBase
    {
        CA_BANK_ACCOUNTS cA_BANK_ACCOUNTS = new CA_BANK_ACCOUNTS();
        string ProReturn = null;
        Boolean savedBool;
        DataTable dtData = new DataTable();
        DataTable dtDtlData = new DataTable();
        GridView grid = new GridView();
        DataTable dtExportData = new System.Data.DataTable();

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                //btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                //  btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                //  btnSave.Text = "Update";
            }

            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
            }

            if (VMVServices.Web.Utils.AddAllowed != FINAppConstants.Y && Master.Mode == FINAppConstants.Add)
            {
                // btnSave.Visible = false;
            }

        }

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();

                FillComboBox();

                Session["GridData"] = null;

                EntityData = null;

                Session[FINSessionConstants.GridData] = null;

                txtDate.Text = DBMethod.ConvertDateToString(DateTime.Now.ToShortDateString());

                BindGrid();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>

        private void FillComboBox()
        {
            FIN.BLL.PER.PayrollAdvice_BLL.fn_PayrollAdviceNumber(ref ddlpayAdvice);
        }
        private void BindGrid()
        {
            try
            {
                ErrorCollection.Clear();

                dtDtlData = DBMethod.ExecuteQuery(Bank_DAL.getEmpAccData(ddlpayAdvice.SelectedItem.Text, txtDate.Text)).Tables[0];

                Session[FINSessionConstants.GridData] = dtData;
                Session["DtlGridData"] = dtDtlData;

                if (dtDtlData.Rows.Count > 0)
                {
                    txtBankName.Text = dtDtlData.Rows[0]["BANK_BRANCH_NAME"].ToString();
                    txtIfscCode.Text = dtDtlData.Rows[0]["BANK_IFSC_CODE"].ToString();
                    txtAccountNumber.Text = dtDtlData.Rows[0]["VENDOR_BANK_ACCOUNT_CODE"].ToString();
                    if (dtDtlData.Rows[0]["TRANSFER_STATUS"].ToString() == FINAppConstants.Y)
                        btnProcess.Enabled = false;
                }


                if (dtDtlData.Rows.Count > 0)
                {
                    dtDtlData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("Salary", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("salary"))));
                    dtDtlData.AcceptChanges();
                }
                gvDtlData.DataSource = dtDtlData;
                gvDtlData.DataBind();

                Session["GridData"] = dtDtlData;
                Session[FINSessionConstants.GridData] = dtDtlData;


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                btnProcess.Enabled = false;
                DataTable dtExportDatas = new DataTable();

                if (Session["GridData"] != null)
                {
                    dtExportDatas = (DataTable)Session["GridData"];
                }
                if (dtExportDatas.Rows.Count > 0)
                {
                    string filename = "AccountDetails.txt";// +"_" + DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss");

                    string filePath = Server.MapPath("../uploadfile/DocFile/" + filename);

                    FileInfo fi = new FileInfo(filePath);
                    if (fi.Exists)
                    {
                        fi.Delete();
                    }

                    StreamWriter sws;
                    using (sws = fi.CreateText())
                    {
                        sws.Write("HDRRSH");
                        sws.Write("\t");
                        sws.Write("002");
                        sws.Write("\t");
                        sws.Write(DateTime.Now.ToString("ddmmyyyy"));
                        sws.Write("\t");
                        sws.Write(DateTime.Now.ToString("ddmmyyyy"));
                        sws.Write("\t");
                        sws.Write(txtIfscCode.Text);

                        double dbl_sal = 0;
                        for (int iLoop = 0; iLoop < dtExportDatas.Rows.Count; iLoop++)
                        {
                            sws.WriteLine("");
                            sws.Write("DTLRSH");
                            sws.Write("\t");
                            sws.Write("002");
                            sws.Write("\t");
                            sws.Write("9999");
                            sws.Write("\t");
                            sws.Write(dtExportDatas.Rows[iLoop]["emp_iban_num"].ToString());
                            sws.Write("\t");
                            dbl_sal = double.Parse(dtExportDatas.Rows[iLoop]["salary"].ToString());
                            sws.Write(dbl_sal.ToString("00000000.000"));
                            sws.Write("\t");
                            sws.Write(dtExportDatas.Rows[iLoop]["emp_name"].ToString());
                        }

                        sws.WriteLine("");
                        sws.Write("TRLRSH");
                        sws.Write("\t");
                        sws.Write("002");
                        sws.Write("\t");

                        double dbl_total_sal = double.Parse(CommonUtils.CalculateTotalAmount(dtExportDatas, "salary"));
                        sws.Write(dbl_total_sal.ToString("00000000.000"));
                        sws.Write("\t");
                        sws.Write(dtExportDatas.Rows[0]["salary"].ToString());


                    }

                    sws.Close();

                    DBMethod.ExecuteNonQuery("update PAY_ADVICE set attribute6='" + FINAppConstants.Y + "' where attribute5='" + ddlpayAdvice.SelectedItem.Text.ToString() + "'");

                    div_download.Visible = true;
                  
                    
                    HttpResponse res = HttpContext.Current.Response;
                    res.Clear();
                    res.AppendHeader("content-disposition", "attachment; filename=" + filePath);
                    res.ContentType = "application/octet-stream";
                    res.WriteFile(filePath);
                    res.Flush();
                    res.End();

                   
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        public void ExportDataTabletoFile(DataTable datatable, string delimited, bool exportcolumnsheader, string file)
        {
            try
            {
                ErrorCollection.Clear();

                StreamWriter str = new StreamWriter(file, false, System.Text.Encoding.Default);
                if (exportcolumnsheader)
                {
                    string Columns = string.Empty;
                    foreach (DataColumn column in datatable.Columns)
                    {
                        Columns += column.ColumnName + delimited;
                    }
                    str.WriteLine(Columns.Remove(Columns.Length - 1, 1));
                }
                foreach (DataRow datarow in datatable.Rows)
                {
                    string row = string.Empty;

                    foreach (object items in datarow.ItemArray)
                    {
                        row += items.ToString() + delimited;
                    }
                    str.WriteLine(row.Remove(row.Length - 1, 1));
                }
                str.Flush();
                str.Close();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        #endregion
        private void ExportToText()
        {
            DataTable dtExportDatas = new DataTable();

            if (Session["GridData"] != null)
            {
                dtExportDatas = (DataTable)Session["GridData"];
            }

            grid.DataSource = dtExportData;
            grid.DataBind();


            //using (StringWriter sw = new StringWriter())
            //{
            //    using (HtmlTextWriter hw = new HtmlTextWriter(sw))
            //    {
            //        grid.AllowPaging = false;
            //        grid.RenderControl(hw);

            //        StringReader sr = new StringReader(sw.ToString());
            //        Document pdfDoc = new Document(PageSize.A2, 10f, 10f, 10f, 0f);
            //        HTMLWorker htmlparser = new HTMLWorker(pdfDoc);

            //        PdfWriter.GetInstance(pdfDoc, (HttpContext.Current.Response.OutputStream));
            //        pdfDoc.Open();
            //        htmlparser.Parse(sr);
            //        pdfDoc.Close();


            //        HttpContext.Current.Response.ContentType = "application/pdf";
            //        HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=GridViewExport.pdf");
            //        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            //        HttpContext.Current.Response.Write(pdfDoc);
            //        HttpContext.Current.Response.Flush();
            //       // HttpContext.Current.Response.End();
            //    }
            //}


            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition",
             "attachment;filename=GridViewExport.txt");
            Response.Charset = "";
            Response.ContentType = "application/text";

            // File.WriteAllText(Server.MapPath("~/file.txt"), "this is a text");

            grid.AllowPaging = false;
            grid.DataBind();

            StringBuilder sb = new StringBuilder();
            for (int k = 0; k < grid.Columns.Count; k++)
            {
                //add separator
                sb.Append(grid.Columns[k].HeaderText + ',');
            }
            //append new line
            sb.Append("\r\n");
            for (int i = 0; i < grid.Rows.Count; i++)
            {
                for (int k = 0; k < grid.Columns.Count; k++)
                {
                    //add separator
                    sb.Append(grid.Rows[i].Cells[k].Text + ',');
                }
                //append new line
                sb.Append("\r\n");
            }

            Response.Output.Write(sb.ToString());
            Response.WriteFile("d:\\GridViewExport.txt");
            Response.Flush();
            string linkToDownloadAsPdf = "d:\\GridViewExport.txt";
            ScriptManager.RegisterStartupScript(Page, this.GetType(), "DownloadAsPdf", "window.open('" + linkToDownloadAsPdf + "','','" + DbConsts.ReportProperties + "');", true);

            //Response.End();
        }
        protected void ddlBankName_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadBankAndPayment();
        }
        private void LoadBankAndPayment()
        {
            try
            {
                ErrorCollection.Clear();
                BindGrid();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void txtDate_TextChanged(object sender, EventArgs e)
        {
            BindGrid();
        }

        protected void ddlpayAdvice_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadBankAndPayment();
        }

    }
}