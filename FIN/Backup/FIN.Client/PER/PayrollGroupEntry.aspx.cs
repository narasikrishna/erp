﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.BLL;
using FIN.BLL.PER;
using VMVServices.Web;

namespace FIN.Client.PER
{
    public partial class PayrollGroupEntry : PageBase
    {
        PAY_GROUP_DTLS pAY_GROUP_DTLS = new PAY_GROUP_DTLS();
        DataTable dtGridData = new DataTable();
        PayrollGroup_BLL payrollGroup_BLL = new PayrollGroup_BLL();

        Boolean bol_rowVisiable;
        Boolean savedBool;
        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PP_PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));


            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
        }
        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }
        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>
        /// 

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();



                EntityData = null;

                dtGridData = FIN.BLL.PER.PayrollGroup_BLL.getChildEntityDet(Master.StrRecordId);
                BindGrid(dtGridData);

                //if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                //{

                //    using (IRepository<HR_CATEGORIES> userCtx = new DataRepository<HR_CATEGORIES>())
                //    {
                //        hR_CATEGORIES = userCtx.Find(r =>
                //            (r.CATEGORY_ID == Master.StrRecordId)
                //            ).SingleOrDefault();
                //    }

                //    EntityData = hR_CATEGORIES;

                //}

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PP_ATOC", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        
        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                var tmpChildEntity = new List<Tuple<object, string>>();

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    pAY_GROUP_DTLS = new PAY_GROUP_DTLS();
                    if (dtGridData.Rows[iLoop]["PAY_GROUP_ID"].ToString() != "0")
                    {
                        using (IRepository<PAY_GROUP_DTLS> userCtx = new DataRepository<PAY_GROUP_DTLS>())
                        {
                            pAY_GROUP_DTLS = userCtx.Find(r =>
                                (r.PAY_GROUP_ID == dtGridData.Rows[iLoop]["PAY_GROUP_ID"].ToString())
                                ).SingleOrDefault();
                        }
                    }

                    pAY_GROUP_DTLS.PAY_GROUP_CODE = dtGridData.Rows[iLoop]["PAY_GROUP_CODE"].ToString();
                    pAY_GROUP_DTLS.PAY_GROUP_CODE_OL = dtGridData.Rows[iLoop]["PAY_GROUP_CODE_OL"].ToString();
                    pAY_GROUP_DTLS.PAY_GROUP_DESC = dtGridData.Rows[iLoop]["PAY_GROUP_DESC"].ToString();
                    pAY_GROUP_DTLS.PAY_GROUP_DESC_OL = dtGridData.Rows[iLoop]["PAY_GROUP_DESC_OL"].ToString();
                    pAY_GROUP_DTLS.PAY_PAYROLL_TYPE = dtGridData.Rows[iLoop]["PAY_PAYROLL_TYPE"].ToString();
                    pAY_GROUP_DTLS.PAY_GROUP_CURR = dtGridData.Rows[iLoop]["CURRENCY_CODE"].ToString();
                    pAY_GROUP_DTLS.ROUND_OFF_DECIMALS = int.Parse(dtGridData.Rows[iLoop]["ROUND_OFF_DECIMALS"].ToString());
                    if (dtGridData.Rows[iLoop]["EFFECTIVE_FROM_DT"] != DBNull.Value)
                    {
                        pAY_GROUP_DTLS.EFFECTIVE_FROM_DT = DateTime.Parse(dtGridData.Rows[iLoop]["EFFECTIVE_FROM_DT"].ToString());
                    }

                    if (dtGridData.Rows[iLoop]["EFFECTIVE_TO_DT"] != DBNull.Value)
                    {
                        pAY_GROUP_DTLS.EFFECTIVE_TO_DT = DateTime.Parse(dtGridData.Rows[iLoop]["EFFECTIVE_TO_DT"].ToString());
                    }

                    if (dtGridData.Rows[iLoop]["ENABLED_FLAG"].ToString().ToUpper() == "TRUE")
                    {
                        pAY_GROUP_DTLS.ENABLED_FLAG = FINAppConstants.Y;
                    }
                    else
                    {
                        pAY_GROUP_DTLS.ENABLED_FLAG = FINAppConstants.N;
                    }

                                       
                    

                    //  hR_CATEGORIES.WORKFLOW_COMPLETION_STATUS = "1";


                        
                        pAY_GROUP_DTLS.PAY_ORG_ID = VMVServices.Web.Utils.OrganizationID;
                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {

                        tmpChildEntity.Add(new Tuple<object, string>(pAY_GROUP_DTLS, "D"));
                    }
                    else
                    {
                        if (dtGridData.Rows[iLoop]["PAY_GROUP_ID"].ToString() != "0")
                        {
                            pAY_GROUP_DTLS.PAY_GROUP_ID = dtGridData.Rows[iLoop]["PAY_GROUP_ID"].ToString();
                            pAY_GROUP_DTLS.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, pAY_GROUP_DTLS.PAY_GROUP_ID);
                            pAY_GROUP_DTLS.MODIFIED_BY = this.LoggedUserName;
                            pAY_GROUP_DTLS.MODIFIED_DATE = DateTime.Today;
                            DBMethod.SaveEntity<PAY_GROUP_DTLS>(pAY_GROUP_DTLS, true);
                            savedBool = true; 

                        }
                        else
                        {

                            pAY_GROUP_DTLS.PAY_GROUP_ID = FINSP.GetSPFOR_SEQCode("PER_003".ToString(), false, true);
                            pAY_GROUP_DTLS.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, pAY_GROUP_DTLS.PAY_GROUP_ID);
                            pAY_GROUP_DTLS.CREATED_BY = this.LoggedUserName;
                            pAY_GROUP_DTLS.CREATED_DATE = DateTime.Today;
                            DBMethod.SaveEntity<PAY_GROUP_DTLS>(pAY_GROUP_DTLS);
                            savedBool = true;
                        }



                    }

                }

                //switch (Master.Mode)
                //{
                //    case FINAppConstants.Add:
                //        {
                //            FIN.BLL.GL.AccountCodes_BLL.SavePCEntity<INV_TRANSFER_HDR, INV_TRANSFER_DTL>(iNV_TRANSFER_HDR, tmpChildEntity, iNV_TRANSFER_DTL);
                //            break;
                //        }
                //    case FINAppConstants.Update:
                //        {

                //            FIN.BLL.GL.AccountCodes_BLL.SavePCEntity<INV_TRANSFER_HDR, INV_TRANSFER_DTL>(iNV_TRANSFER_HDR, tmpChildEntity, iNV_TRANSFER_DTL, true);
                //            break;

                //        }
                //}



            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PP_PGE", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        private void FillFooterGridCombo(GridViewRow tmpgvr)
        {
            try
            {
                ErrorCollection.Clear();

                DropDownList ddl_Currency = tmpgvr.FindControl("ddlCurrency") as DropDownList;
                payrollGroup_BLL.fn_getCurrency(ref ddl_Currency);


                if (gvData.EditIndex >= 0 && tmpgvr.RowType.ToString() != "Footer")
                {
                    ddl_Currency.SelectedValue = gvData.DataKeys[gvData.EditIndex].Values[FINColumnConstants.CURRENCY_CODE].ToString();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PP_FillFootGrid", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session[FINSessionConstants.GridData], "Payroll Group");

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                AssignToBE();

                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                if (savedBool)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        #endregion

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;
                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr["ENABLED_FLAG"] = "FALSE";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                GridViewRow gvr = gvData.FooterRow;
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PP_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        # region Grid Events
        /// <summary>
        /// The GridView control is entering Canceling mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCancelEditEventArgs indicates which row's cancel button was clicked. </param>
        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            try
            {
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = -1;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PG_ROW_CNCL_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        /// <summary>
        ///   The GridView control is entering row Command mode      
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewCommandEventArgs class does not contain a property that indicates which row's command button was clicked. </param>
        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }


                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {
                    drList = AssignToGridControl(gvr, dtGridData, "A", 0);
                    if (ErrorCollection.Count > 0)
                    {
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    dtGridData.Rows.Add(drList);
                    BindGrid(dtGridData);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PP_ROW_CMD", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        /// <summary>
        /// To assign the controls to grid view
        /// </summary>
        /// <param name="tmpdrlist">Datarow details</param>
        /// <param name="tmpgvr">Grid view objects</param>
        /// <returns></returns>
        /// 
        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList[FINColumnConstants.DELETED] = FINAppConstants.Y;

                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PR_ROW_DEL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        /// <summary>
        ///  The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewEditEventArgs indicates which row's edit button was clicked. </param>
        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                GridViewRow gvr = gvData.Rows[e.NewEditIndex];
                FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PR_ROW_EDT", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        /// <summary>
        /// The GridView control is entering row created mode
        /// To identify rowtype and created a row in the grid view control       
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">The GridViewRowEventArgs indicates which row's created button was clicked. </param>
        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PR_RowCreated", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        /// <summary>
        /// The GridView control is entering edit mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (bol_rowVisiable)
                        e.Row.Visible = false;

                    if (((DataRowView)e.Row.DataItem).Row[FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {
                        e.Row.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PR_RDB", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        ///  The GridView control is entering row updating mode  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }

                drList = AssignToGridControl(gvr, dtGridData, "U", e.RowIndex);
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PP_ROW_UP", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private DataRow AssignToGridControl(GridViewRow gvr, DataTable tmpdtGridData, string GMode, int rowindex)
        {


            System.Collections.SortedList slControls = new System.Collections.SortedList();


            TextBox txt_GroupCodeEn = gvr.FindControl("txtGroupCodeEn") as TextBox;
            TextBox txt_GroupCodeAr = gvr.FindControl("txtGroupCodeAr") as TextBox;
            TextBox txt_descEn = gvr.FindControl("txtdescEn") as TextBox;
            TextBox txt_descAr = gvr.FindControl("txtdescAr") as TextBox;
            TextBox txt_Type = gvr.FindControl("txtType") as TextBox;
            DropDownList ddl_Currency = gvr.FindControl("ddlCurrency") as DropDownList;
            TextBox dtp_fromDate = gvr.FindControl("dtpfromDate") as TextBox;
            TextBox dtp_ToDate = gvr.FindControl("dtpToDate") as TextBox;
            TextBox txt_RoundoffDecimal = gvr.FindControl("txtRoundoffDecimal") as TextBox;
            CheckBox chkact = gvr.FindControl("chkact") as CheckBox;

            DataRow drList;
            DataTable dt_tmp = tmpdtGridData.Copy();
            if (GMode == "A")
            {
                drList = dtGridData.NewRow();
                drList["PAY_GROUP_ID"] = "0";
            }
            else
            {
                drList = dtGridData.Rows[rowindex];
                dt_tmp.Rows.RemoveAt(rowindex);

            }

            slControls[0] = txt_GroupCodeEn;
            
            slControls[1] = txt_Type;
            //slControls[2] = ddl_Currency;
            slControls[2] = dtp_fromDate;
            slControls[3] = dtp_fromDate;
            slControls[4] = dtp_ToDate;
            slControls[5] = txt_RoundoffDecimal;


            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/PER_" + Session["Sel_Lng"].ToString() + ".properties"));

            ErrorCollection.Clear();
            string strCtrlTypes = "TextBox~TextBox~TextBox~DateTime~DateRangeValidate~TextBox";
            string strMessage = Prop_File_Data["Group_Code_P"] +  " ~ " + Prop_File_Data["Type_P"] + " ~ " + Prop_File_Data["From_Date_P"] + " ~ " + Prop_File_Data["From_Date_P"] + " ~ " + Prop_File_Data["To_Date_P"] + " ~ " + Prop_File_Data["Round_off_Decimal_P"] + "";
            //string strMessage = "Group Code ~ Type ~ Currency ~ Start Date ~ Start Date ~ End Date ~ Round off Decimal";
            ErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);
            if (ErrorCollection.Count > 0)
                return drList;

            
            string strCondition = "PAY_GROUP_CODE='" + txt_GroupCodeEn.Text.Trim().ToUpper() + "'";
            strMessage = FINMessageConstatns.ElementCodeAlreadyExists;
            ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            if (ErrorCollection.Count > 0)
            {
                return drList;
            }

            //if (Session[FINSessionConstants.GridData] != null)
            //{
            //    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
            //    for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
            //    {
            //        if (dtGridData.Rows[iLoop]["PAY_GROUP_ID"].ToString() != "0")
            //        {
            //            pAY_GROUP_DTLS.PAY_GROUP_ID = dtGridData.Rows[iLoop]["PAY_GROUP_ID"].ToString();
            //        }
            //    }
            //}

            //Duplicate Validation through Backend
            ErrorCollection.Clear();
            string ProReturn = null;
            ProReturn = FINSP.GetSPFOR_ERR_MGR_CATEGORY(Master.FormCode, drList["PAY_GROUP_ID"].ToString(), txt_GroupCodeEn.Text.ToString());
            if (ProReturn != string.Empty)
            {
                if (ProReturn != "0")
                {
                    ErrorCollection.Add("PAYROLLGROUP", ProReturn);
                    if (ErrorCollection.Count > 0)
                    {
                        return drList;
                    }
                }
            }
            
            //string strCondition = "CATEGORY_ID='" + txtcode.Text + "'";
            //strMessage = FINMessageConstatns.RecordAlreadyExists;
            //ErrorCollection = UserUtility_BLL.DataDuplication(dt_tmp, strCondition, strMessage);
            //if (ErrorCollection.Count > 0)
            //{
            //    return drList;
            //}




            drList["PAY_GROUP_CODE"] = txt_GroupCodeEn.Text;
            drList["PAY_GROUP_CODE_OL"] = txt_GroupCodeAr.Text;
            drList["PAY_GROUP_DESC"] = txt_descEn.Text;
           drList["PAY_GROUP_DESC_OL"] = txt_descAr.Text;
            drList["PAY_PAYROLL_TYPE"] = txt_Type.Text; 
            //drList["PAY_GROUP_CURR"] = ddl_Currency.SelectedValue;

            if (ddl_Currency.SelectedItem != null)
            {
                drList[FINColumnConstants.CURRENCY_CODE] = ddl_Currency.SelectedItem.Value;
                drList[FINColumnConstants.CURRENCY] = ddl_Currency.SelectedItem.Text;
            }
            if (dtp_fromDate.Text.ToString().Length > 0)
            {
                drList["EFFECTIVE_FROM_DT"] = DBMethod.ConvertStringToDate(dtp_fromDate.Text.ToString());
            }

            if (dtp_ToDate.Text.ToString().Length > 0)
            {

                drList["EFFECTIVE_TO_DT"] = DBMethod.ConvertStringToDate(dtp_ToDate.Text.ToString());
            }
            else
            {
                drList["EFFECTIVE_TO_DT"] = DBNull.Value;
            }

            if (chkact.Checked)
            {
                drList["ENABLED_FLAG"] = "TRUE";
            }
            else
            {
                drList["ENABLED_FLAG"] = "FALSE";
            }


            drList["ROUND_OFF_DECIMALS"] = txt_RoundoffDecimal.Text;


            drList[FINColumnConstants.DELETED] = FINAppConstants.N;


            return drList;



        }

        #endregion

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {

                    pAY_GROUP_DTLS.PAY_GROUP_ID = dtGridData.Rows[iLoop]["PAY_GROUP_ID"].ToString();
                    DBMethod.DeleteEntity<PAY_GROUP_DTLS>(pAY_GROUP_DTLS);
                }


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PP_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {

        }

    }
}