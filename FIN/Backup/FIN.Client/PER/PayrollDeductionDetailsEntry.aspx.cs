﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.BLL;
using FIN.BLL.PER;
using VMVServices.Web;

namespace FIN.Client.PER
{
    public partial class PayrollDeductionDetailsEntry : PageBase
    {
        PAY_DEDUCTION_DTLS pAY_DEDUCTION_DTLS = new PAY_DEDUCTION_DTLS();
        DataTable dtGridData = new DataTable();
        DataTable dtDateGrid = new DataTable();
        DataTable dt_DedPartyName = new DataTable();
        DataTable dt_PayrollName = new DataTable();
        DataTable dt_employeeName = new DataTable();
        DataTable dt_elementName = new DataTable();

        Boolean bol_rowVisiable;
        Boolean savedBool;

        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PEEE_PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]);
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));


            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            UserRightsChecking();
        }
        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    btnDelete.Visible = false;
                }
            }
        }
        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>
        /// 

        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>
        /// 
        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();


                EntityData = null;
                

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    pAY_DEDUCTION_DTLS = PayrollDeductionDetails_BLL.getClassEntity(Master.StrRecordId);

                    EntityData = pAY_DEDUCTION_DTLS;
                    //ddlPayrollNumber.SelectedValue = pAY_DEDUCTION_DTLS.
                    ddlDeductionPartyNumber.SelectedValue = pAY_DEDUCTION_DTLS.DED_PARTY_CODE.ToString();
                    //ddlEmployeeNo.SelectedValue = pAY_EMP_ELEMENT_VALUE.PAY_EMP_ID.ToString();
                    //fn_fill_employeeName();
                    //ddlGroupCode.SelectedValue = pAY_EMP_ELEMENT_VALUE.PAY_GROUP_ID.ToString();
                    //fn_fillGroupName();

                  
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PP_PEEE", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }


        }

        private void FillComboBox()
        {
            //PayrollDeductionDetails_BLL.fn_getPayRollDtls(ref ddlPayrollNumber);
            FIN.BLL.PER.PayrollTrailrunDetails_BLL.fn_getFinalPayrollId(ref ddlPayrollNumber);
            PayrollDeductionDetails_BLL.fn_getDedPartyDtls(ref ddlDeductionPartyNumber);
        }

        // <summary>
        // To assign the controls to the Grade Master table entities
        // </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();


                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                var tmpChildEntity = new List<Tuple<object, string>>();

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    pAY_DEDUCTION_DTLS = new PAY_DEDUCTION_DTLS();
                    if (dtGridData.Rows[iLoop]["DED_ID"].ToString() != "0")
                    {
                        using (IRepository<PAY_DEDUCTION_DTLS> userCtx = new DataRepository<PAY_DEDUCTION_DTLS>())
                        {
                            pAY_DEDUCTION_DTLS = userCtx.Find(r =>
                                (r.DED_ID == dtGridData.Rows[iLoop]["DED_ID"].ToString())
                                ).SingleOrDefault();
                        }
                    }

                    pAY_DEDUCTION_DTLS.DED_PARTY_CODE = ddlDeductionPartyNumber.SelectedValue.ToString();
                    pAY_DEDUCTION_DTLS.EMP_ID = dtGridData.Rows[iLoop]["EMP_ID"].ToString();
                    pAY_DEDUCTION_DTLS.PAY_ELEMENT_ID = dtGridData.Rows[iLoop]["PAY_ELEMENT_ID"].ToString();
                    pAY_DEDUCTION_DTLS.DED_AMOUNT = Decimal.Parse(dtGridData.Rows[iLoop]["PAY_AMOUNT"].ToString());



                    if (dtGridData.Rows[iLoop]["PAY_FROM_DT"] != DBNull.Value)
                    {
                        pAY_DEDUCTION_DTLS.PAID_FROM_DATE = DateTime.Parse(dtGridData.Rows[iLoop]["PAY_FROM_DT"].ToString());
                    }

                    if (dtGridData.Rows[iLoop]["PAY_TO_DT"] != DBNull.Value)
                    {
                        pAY_DEDUCTION_DTLS.PAID_TO_DATE = DateTime.Parse(dtGridData.Rows[iLoop]["PAY_TO_DT"].ToString());
                    }



                    pAY_DEDUCTION_DTLS.ENABLED_FLAG = "1";


                    pAY_DEDUCTION_DTLS.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, pAY_DEDUCTION_DTLS.DED_ID);
                    pAY_DEDUCTION_DTLS.ORG_ID = VMVServices.Web.Utils.OrganizationID;

                    if (dtGridData.Rows[iLoop][FINColumnConstants.DELETED].ToString() == FINAppConstants.Y)
                    {

                        tmpChildEntity.Add(new Tuple<object, string>(pAY_DEDUCTION_DTLS, "D"));
                    }
                    else
                    {
                        if (dtGridData.Rows[iLoop]["DED_ID"].ToString() != "0")
                        {
                            pAY_DEDUCTION_DTLS.DED_ID = dtGridData.Rows[iLoop]["DED_ID"].ToString();
                            pAY_DEDUCTION_DTLS.MODIFIED_BY = this.LoggedUserName;
                            pAY_DEDUCTION_DTLS.MODIFIED_DATE = DateTime.Today;
                            DBMethod.SaveEntity<PAY_DEDUCTION_DTLS>(pAY_DEDUCTION_DTLS, true);
                            //tmpChildEntity.Add(new Tuple<object, string>(hR_CATEGORIES, "U"));

                        }
                        else
                        {

                            pAY_DEDUCTION_DTLS.DED_ID = FINSP.GetSPFOR_SEQCode("PER_010".ToString(), false, true);
                            pAY_DEDUCTION_DTLS.CREATED_BY = this.LoggedUserName;
                            pAY_DEDUCTION_DTLS.CREATED_DATE = DateTime.Today;
                            DBMethod.SaveEntity<PAY_DEDUCTION_DTLS>(pAY_DEDUCTION_DTLS);
                            // tmpChildEntity.Add(new Tuple<object, string>(hR_CATEGORIES, "A"));
                        }
                    }
                }

                //switch (Master.Mode)
                //{
                //    case FINAppConstants.Add:
                //        {
                //            FIN.BLL.GL.AccountCodes_BLL.SavePCEntity<INV_TRANSFER_HDR, INV_TRANSFER_DTL>(iNV_TRANSFER_HDR, tmpChildEntity, iNV_TRANSFER_DTL);
                //            break;
                //        }
                //    case FINAppConstants.Update:
                //        {

                //            FIN.BLL.GL.AccountCodes_BLL.SavePCEntity<INV_TRANSFER_HDR, INV_TRANSFER_DTL>(iNV_TRANSFER_HDR, tmpChildEntity, iNV_TRANSFER_DTL, true);
                //            break;

                //        }
                //}



            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PP_PGEE", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                ErrorCollection = CommonUtils.IsEmptyGrid((DataTable)Session[FINSessionConstants.GridData], "Payroll Deduction Details");

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                AssignToBE();

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED, true);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        #endregion

        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                bol_rowVisiable = false;
                Session[FINSessionConstants.GridData] = dtData;

                if (dtData.Rows.Count > 0)
                {
                    dtData.AsEnumerable().ToList().ForEach(p => p.SetField<String>("PAY_AMOUNT", DBMethod.GetAmtDecimalCommaSeparationValue(p.Field<String>("PAY_AMOUNT"))));
                    dtData.AcceptChanges();
                }

                DataTable dt_tmp = dtData.Copy();
                if (dt_tmp.Rows.Count == 0)
                {
                    DataRow dr = dt_tmp.NewRow();
                    dr[0] = "0";
                    dr["ENABLED_FLAG"] = "FALSE";
                    dt_tmp.Rows.Add(dr);
                    bol_rowVisiable = true;
                }
                gvData.DataSource = dt_tmp;
                gvData.DataBind();
                //GridViewRow gvr = gvData.FooterRow;
                //FillFooterGridCombo(gvr);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PP_BG", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void btnGetDetails_Click(object sender, EventArgs e)
        {
            dtGridData = FIN.BLL.PER.PayrollDeductionDetails_BLL.getChildEntityDet(ddlDeductionPartyNumber.SelectedValue, ddlPayrollNumber.SelectedValue);
            BindGrid(dtGridData);
        }

        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {

                    pAY_DEDUCTION_DTLS.DED_ID = dtGridData.Rows[iLoop]["DED_ID"].ToString();
                    DBMethod.DeleteEntity<PAY_DEDUCTION_DTLS>(pAY_DEDUCTION_DTLS);
                }


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PEEE_BTNS", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {

        }

        protected void ddlPayrollNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            dt_PayrollName = DBMethod.ExecuteQuery(FIN.DAL.PER.PayrollDeductionDetails_DAL.GetPayRollDetails_Name(ddlPayrollNumber.SelectedValue)).Tables[0];
            if (dt_PayrollName != null)
            {
                if (dt_PayrollName.Rows.Count > 0)
                {
                    txtDescription.Text = dt_PayrollName.Rows[0][0].ToString();
                    txtDescription.Enabled = false;
                }
            }
        }

        protected void ddlDeductionPartyNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            dt_DedPartyName = DBMethod.ExecuteQuery(FIN.DAL.PER.PayrollDeductionDetails_DAL.GetDedPartyDetails_Name(ddlDeductionPartyNumber.SelectedValue)).Tables[0];
            if (dt_DedPartyName != null)
            {
                if (dt_DedPartyName.Rows.Count > 0)
                {
                    txtDeductionPartyName.Text = dt_DedPartyName.Rows[0][0].ToString();
                    txtDeductionPartyName.Enabled = false;
                }
            }

        }

    }
}