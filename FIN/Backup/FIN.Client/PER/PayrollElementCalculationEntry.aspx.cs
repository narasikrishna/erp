﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;
using FIN.BLL;
using VMVServices.Web;
using FIN.BLL.PER;
using FIN.DAL.PER;

namespace FIN.Client.PER
{
    public partial class PayrollElementCalculationEntry : PageBase
    {
        DataTable dt_group_data = new DataTable();
        DataTable Dt_dept_Name = new DataTable();
        DataTable dt_Desig_Name = new DataTable();
        DataTable dt_Element_Name = new DataTable();

        PAY_ELEMENT_CALC pAY_ELEMENT_CALC = new PAY_ELEMENT_CALC();
        # region Page Load
        /// <summary>
        /// when the pages is rendered and loaded for the first time execution goes here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = Server.HtmlEncode(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
            }

            if (VMVServices.Web.Utils.AddAllowed != FINAppConstants.Y && Master.Mode == FINAppConstants.Add)
            {
                btnSave.Visible = false;
            }

        }
        /// <summary>
        /// To assign the values to Controls and fetch the RecordID,Mode,ProgramID from the Querystring,BInd the dropdownlist
        /// </summary>

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                FillComboBox();

                EntityData = null;

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {

                    using (IRepository<PAY_ELEMENT_CALC> userCtx = new DataRepository<PAY_ELEMENT_CALC>())
                    {
                        pAY_ELEMENT_CALC = userCtx.Find(r =>
                            (r.PEC_ID == Master.StrRecordId)
                            ).SingleOrDefault();
                    }

                    EntityData = pAY_ELEMENT_CALC;

                    if (pAY_ELEMENT_CALC.PEC_CALC_TYPE.ToString() == "INDEPENDANT")
                    {
                        IDformula.Visible = false;
                    }
                    else
                    {
                        IDformula.Visible = true;
                    }

                    ddlGroupCode.SelectedValue = pAY_ELEMENT_CALC.PAY_GROUP_ID.ToString();
                    fillgroupdesc();
                    ddlDepartment.SelectedValue = pAY_ELEMENT_CALC.PEC_DEPT_ID.ToString();
                    fn_fill_designation();
                    ddlDesignation.SelectedValue = pAY_ELEMENT_CALC.PEC_DESIG_ID.ToString();
                    fillDesigdesc();
                    ddlElement.SelectedValue = pAY_ELEMENT_CALC.PEC_ELEMENT_ID.ToString();
                    fillelementdesc();
                    ddlCalculationType.SelectedValue = pAY_ELEMENT_CALC.PEC_CALC_TYPE.ToString();
                    txtFormula.Text = pAY_ELEMENT_CALC.PEC_FORMULA;
                    txtFromDate.Text = DBMethod.ConvertDateToString(pAY_ELEMENT_CALC.EFFECTIVE_FROM_DT.ToString());

                    if (pAY_ELEMENT_CALC.EFFECTIVE_TO_DT != null)
                    {
                        txttodate.Text = DBMethod.ConvertDateToString(pAY_ELEMENT_CALC.EFFECTIVE_TO_DT.ToString());
                    }


                    if (pAY_ELEMENT_CALC.PEC_AMOUNT != null)
                    {
                        txtAmount.Text = pAY_ELEMENT_CALC.PEC_AMOUNT.ToString();
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PESR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        /// <summary>
        /// To assign the controls to the Grade Master table entities
        /// </summary>


        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    pAY_ELEMENT_CALC = (PAY_ELEMENT_CALC)EntityData;
                }

                pAY_ELEMENT_CALC.PAY_GROUP_ID = ddlGroupCode.SelectedValue.ToString();
                pAY_ELEMENT_CALC.PEC_DEPT_ID = ddlDepartment.SelectedValue.ToString();
                pAY_ELEMENT_CALC.PEC_DESIG_ID = ddlDesignation.SelectedValue.ToString();
                pAY_ELEMENT_CALC.PEC_ELEMENT_ID = ddlElement.SelectedValue.ToString();
                pAY_ELEMENT_CALC.PEC_CALC_TYPE = ddlCalculationType.SelectedValue.ToString();
                pAY_ELEMENT_CALC.PEC_FORMULA = txtFormula.Text.ToString();


                pAY_ELEMENT_CALC.EFFECTIVE_FROM_DT = DBMethod.ConvertStringToDate(txtFromDate.Text.ToString());


                if (txttodate.Text != null && txttodate.Text != string.Empty && txttodate.Text.ToString().Trim().Length > 0)
                {
                    pAY_ELEMENT_CALC.EFFECTIVE_TO_DT = DBMethod.ConvertStringToDate(txttodate.Text.ToString());
                }

                if (txtAmount.Text != "")
                {
                    pAY_ELEMENT_CALC.PEC_AMOUNT = Decimal.Parse(txtAmount.Text.ToString());
                }
                pAY_ELEMENT_CALC.PEC_ORG_ID = VMVServices.Web.Utils.OrganizationID;
                pAY_ELEMENT_CALC.ENABLED_FLAG = "1";

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    pAY_ELEMENT_CALC.MODIFIED_BY = this.LoggedUserName;
                    pAY_ELEMENT_CALC.MODIFIED_DATE = DateTime.Today;

                }
                else
                {
                    pAY_ELEMENT_CALC.PEC_ID = FINSP.GetSPFOR_SEQCode(FINAppConstants.PER_005.ToString(), false, true);
                    //tAX_TERMS.WORKFLOW_COMPLETION_STATUS = "1";
                    pAY_ELEMENT_CALC.CREATED_BY = this.LoggedUserName;
                    pAY_ELEMENT_CALC.CREATED_DATE = DateTime.Today;
                }
                pAY_ELEMENT_CALC.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, pAY_ELEMENT_CALC.PEC_ID);

                // Duplicate Validation Through Backend.
                string ProReturn = FINSP.GetSPFOR_ERR_MGR_CATEGORY_THREE(Master.FormCode, pAY_ELEMENT_CALC.PEC_ID, pAY_ELEMENT_CALC.PEC_DESIG_ID, pAY_ELEMENT_CALC.PEC_ELEMENT_ID, pAY_ELEMENT_CALC.PAY_GROUP_ID);

                if (ProReturn != string.Empty)
                {
                    if (ProReturn != "0")
                    {
                        ErrorCollection.Add("PAYROLL_ELEMENTS_CALCULATION", ProReturn);
                        if (ErrorCollection.Count > 0)
                        {
                            return;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PRSRE", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void FillComboBox()
        {
            PayrollElementCalculation_BLL.fn_getGroup(ref ddlGroupCode);
           
            FIN.BLL.HR.Department_BLL.GetDepartmentName(ref ddlDepartment);
            // PayrollEmployeeStopResume.GetEmployeeName(ref ddlEmployeeNo);
            //FIN.BLL.PER.PayrollElements_BLL.fn_getElement(ref ddlelment);
            //PayrollEmployeeStopResume.fn_getPayElements(ref ddlelment);
            FIN.BLL.Lookup_BLL.GetLookUpValues(ref ddlCalculationType, "CAL_TYP");

        }

        # region Save,Update and Delete
        /// <summary>
        /// Validate the controls ,Save the records and update the records into the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                AssignToBE();


                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            DBMethod.SaveEntity<PAY_ELEMENT_CALC>(pAY_ELEMENT_CALC);
                            //  DisplaySaveCompleteMessage(Master.ListPageToOpen);
                            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                            break;
                        }
                    case FINAppConstants.Update:
                        {

                            DBMethod.SaveEntity<PAY_ELEMENT_CALC>(pAY_ELEMENT_CALC, true);
                            //  DisplaySaveCompleteMessage(Master.ListPageToOpen);
                            Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                            break;

                        }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Save", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        #endregion

        /// <summary>
        /// Used to delete the Grade Master table records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnYes_Click(object sender, EventArgs e)
        {


            try
            {
                ErrorCollection.Clear();
                DBMethod.DeleteEntity<PAY_ELEMENT_CALC>(pAY_ELEMENT_CALC);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PECE", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        protected void ddlDepartment_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            fn_fill_designation();



        }

        private void fn_fill_designation()
        {
            PayrollElementCalculation_BLL.GetDesignationName(ref ddlDesignation, ddlDepartment.SelectedValue);
            Dt_dept_Name = DBMethod.ExecuteQuery(FIN.DAL.PER.PayrollElementCalculation_DAL.GetDeptName(ddlDepartment.SelectedValue.ToString())).Tables[0];
            if (Dt_dept_Name != null)
            {
                if (Dt_dept_Name.Rows.Count > 0)
                {
                    //txtDescription2.Text = Dt_dept_Name.Rows[0][0].ToString();
                    //txtDescription2.Enabled = false;
                }
            }

        }

        protected void ddlGroupCode_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            fillgroupdesc();



        }
        private void fillgroupdesc()
        {
            PayrollElementCalculation_BLL.GetElement(ref ddlElement, ddlGroupCode.SelectedValue);
            
            dt_group_data = DBMethod.ExecuteQuery(FIN.DAL.PER.PayrollElementCalculation_DAL.GetGroupName(ddlGroupCode.SelectedValue.ToString())).Tables[0];
            if (dt_group_data != null)
            {
                if (dt_group_data.Rows.Count > 0)
                {
                    //txtDescription.Text = dt_group_data.Rows[0][0].ToString();
                    //txtDescription.Enabled = false;
                }
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {

        }

        protected void ddlElement_SelectedIndexChanged(object sender, EventArgs e)
        {
            fillelementdesc();
              //PayrollElementCalculation_BLL.GetElment_notinelment(ref ddlelment, ddlElement.SelectedValue);
            PayrollElementCalculation_BLL.GetElementName_notinelement(ref ddlelment, ddlGroupCode.SelectedValue,ddlElement.SelectedValue);

        }

        private void fillelementdesc()
        {
            dt_Element_Name = DBMethod.ExecuteQuery(FIN.DAL.PER.PayrollElementCalculation_DAL.GetElemName(ddlElement.SelectedValue.ToString())).Tables[0];
            if (dt_Element_Name != null)
            {
                if (dt_Element_Name.Rows.Count > 0)
                {
                    //txtDescription4.Text = dt_Element_Name.Rows[0][0].ToString();
                    //txtDescription4.Enabled = false;
                }
            }
        }

        protected void ddlDesignation_SelectedIndexChanged(object sender, EventArgs e)
        {
            fillDesigdesc();
        }

        private void fillDesigdesc()
        {
            dt_Desig_Name = DBMethod.ExecuteQuery(FIN.DAL.PER.PayrollElementCalculation_DAL.GetDesigName(ddlDesignation.SelectedValue.ToString())).Tables[0];
            if (dt_Desig_Name != null)
            {
                if (dt_Desig_Name.Rows.Count > 0)
                {
                    //txtDescription3.Text = dt_Desig_Name.Rows[0][0].ToString();
                    //txtDescription3.Enabled = false;
                }
            }
        }

        protected void ddlCalculationType_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void txtDescription3_TextChanged(object sender, EventArgs e)
        {

        }

        protected void txttodate_TextChanged(object sender, EventArgs e)
        {

        }

        protected void btn_Click(object sender, EventArgs e)
        {
            Button tempbtn = (Button)sender;
            txtFormula.Text = txtFormula.Text + tempbtn.CommandArgument;
        }

        protected void ddlelment_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtFormula.Text = txtFormula.Text + "#" + ddlelment.SelectedValue + "#";
        }

        protected void btncheck_Click(object sender, EventArgs e)
        {
            txtAmount.Text = FINSP.GetSPFOR_PAY_ELE_CAL(ddlDepartment.SelectedValue, ddlDesignation.SelectedValue, txtFormula.Text);
        }

        protected void ddlCalculationType_SelectedIndexChanged1(object sender, EventArgs e)
        {
            if (ddlCalculationType.SelectedValue == "INDEPENDANT")
            {
                IDformula.Visible = false;
            }
            else
            {
                IDformula.Visible = true;
            }
        }

    }
}