﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="PayslipGenerationEntry.aspx.cs" Inherits="FIN.Client.PER.PayslipGenerationEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1000px" id="div1">
        <div class="divRowContainer">
            <div class="lblBox" style="float: left; width: 200px" id="Div2">
                Payroll Period</div>
            <div class="divtxtBox" style="float: left; width: 550px">
                <asp:DropDownList ID="ddlPayPeriod" runat="server" TabIndex="3" CssClass="validate[required] EntryFont RequiredField ddlStype"
                    AutoPostBack="True" OnSelectedIndexChanged="ddlPayPeriod_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="divtxtBox" style="float: left; width: 780px" align="right">
                <asp:Button ID="btnSave" runat="server" Text="Process Payslip and Send e-mail" OnClick="btnProcess_Click" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/PER/PAChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () { $("#form1").validationEngine(); });
        $("#FINContent_btnSave").click(function () {
            if ($("#form1").validationEngine('validate') == false)
                return false;
        });

    </script>
</asp:Content>
