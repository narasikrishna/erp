﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.BLL;
using FIN.DAL;
using VMVServices.Web;
using System.Net;
using System.Net.Mail;

namespace FIN.Client
{
    public partial class LoginNew1 : PageBase
    {

        SSM_USERS sSM_USERS = new SSM_USERS();

        Dictionary<string, string> Prop_File_Data;

        protected void Page_Load(object sender, EventArgs e)
        {
            lblError.Visible = false;
            if (!IsPostBack)
            {
                if (Session[FINSessionConstants.Sel_Lng] == null || Session[FINSessionConstants.Sel_Lng] == "EN")
                {
                    Session[FINSessionConstants.Sel_Lng] = "EN";
                    VMVServices.Web.Utils.LanguageCode = "";
                    VMVServices.Web.Utils.DecimalPrecision = "0";
                    
                }
                else
                {                
                    VMVServices.Web.Utils.LanguageCode = "_OL";
                }
            }
        }


        protected void btnYes_Click(object sender, EventArgs e)
        {


            //using (IRepository<SSM_USERS> userCtx = new DataRepository<SSM_USERS>())
            //{
            //    sSM_USERS = userCtx.Find(r =>
            //        (r.USER_CODE == txtUserID.Text)
            //        ).SingleOrDefault();
            //}

            //if (sSM_USERS.CHANGE_PASSWORD_ON_NEXT_LOGIN == FINAppConstants.Y)
            //{

            //    //if (FINColumnConstants.USER_CODE.ToString() != null)
            //    //{
            //    //    using (IRepository<SSM_USERS> userCtx = new DataRepository<SSM_USERS>())
            //    //    {
            //    //        sSM_USERS = userCtx.Find(r =>
            //    //            (r.USER_CODE == txtUserID.Text)
            //    //            ).SingleOrDefault();
            //    //    }


            //    //    sSM_USERS.CHANGE_PASSWORD_ON_NEXT_LOGIN = FINAppConstants.N;
            //    //  //  sSM_USERS.USER_PASSWORD = txtconfirmpw.Text;
            //    //    DBMethod.SaveEntity<SSM_USERS>(sSM_USERS, true);
            //    //    Response.Redirect("~/MainPage/MainPage.aspx");
            //    //}
            //}

        }
        public void GenerateEmail()
        {
            try
            {
                

                MailMessage oMsg = new MailMessage();
                oMsg.From = new MailAddress("jeyaranivr@gmail.com", "jj");
                oMsg.To.Add(new MailAddress("jeyaranivr@gmail.com", "mahe"));
                oMsg.Subject = "VMV Test Email";
                oMsg.Body = " VMV Test Email";
                oMsg.SubjectEncoding = System.Text.Encoding.UTF8;
                oMsg.BodyEncoding = System.Text.Encoding.UTF8;
                oMsg.IsBodyHtml = false;
                oMsg.Priority = MailPriority.High;

                SmtpClient oSmtp = new SmtpClient(".com", 587);
                oSmtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                oSmtp.EnableSsl = true;

                NetworkCredential oCredential = new NetworkCredential("vmvsyserp@gmail.com", "India12*");
                oSmtp.UseDefaultCredentials = true;
                oSmtp.Credentials = oCredential;
                oSmtp.Send(oMsg);
            }
            catch (Exception ex)
            {
                
            }
        }
        public void GenerateSMS()
        {
            string api = "http://bulksms.mysmsmantra.com:8080/WebSMS/SMSAPI.jsp";
            string uid = "vmvsys";
            string password = "741833959";
            string sendername = "vmvsys";
            string msg = "Welcome to VMV Systems";
            string mobinumber = "919962880010";
            HttpWebRequest myReq =
                //  (HttpWebRequest)WebRequest.Create(api + "?username=" + uid + "&password=" + password + "&message=" + msg + "&mobileno=" + mobinumber + "&provider=mysmsmantra");

            (HttpWebRequest)WebRequest.Create(api + "?username=" + uid + "&password=" + password + "&sendername=" + sendername + "&message=" + msg + "&mobileno=" + mobinumber);
            HttpWebResponse myResp = (HttpWebResponse)myReq.GetResponse();
            System.IO.StreamReader respStreamReader = new System.IO.StreamReader(myResp.GetResponseStream());
            string responseString = respStreamReader.ReadToEnd();
            respStreamReader.Close();
            myResp.Close();
        }
        protected void btnYes_Click1(object sender, EventArgs e)
        {
           //GenerateEmail();
            //GenerateSMS();
            //Dictionary<string, string> Prop_File_Data;
            if (Session[FINSessionConstants.Sel_Lng] == null || Session[FINSessionConstants.Sel_Lng] == "EN")
            {
                Session[FINSessionConstants.Sel_Lng] = "EN";
                VMVServices.Web.Utils.LanguageCode = "";
            }
            else
            {
                VMVServices.Web.Utils.LanguageCode = "_OL";
            }

            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/Generic_" + Session["Sel_Lng"].ToString() + ".properties"));

            try
            {
                ErrorCollection.Clear();

                lblError.Text = "";
                lblError.Visible = true;
                sSM_USERS = null;
                using (IRepository<SSM_USERS> userCtx = new DataRepository<SSM_USERS>())
                {
                    sSM_USERS = userCtx.Find(r =>
                        (r.USER_CODE.ToUpper() == txtUserID.Text.ToUpper()   && r.ATTRIBUTE1 != "EMPLOYEE") 
                        ).SingleOrDefault();
                }

                //&&
                //        (r.USER_PASSWORD.ToUpper() == txtPassword.Text.ToUpper()) &&
                //        (r.PASSWORD_LOCK == FINAppConstants.N)


                //lblError.Visible = false;
                if (sSM_USERS != null)
                {
                    if (sSM_USERS.PASSWORD_LOCK == FINAppConstants.Y)
                    {
                        //ErrorCollection.Add("Invalid User", "Password is Locked");
                        //lblError.Text = "Password is Locked";
                        ErrorCollection.Add(Prop_File_Data["InvalidUser_P"], Prop_File_Data["PasswordLocked_P"]);
                        lblError.Text = Prop_File_Data["PasswordLocked_P"];
                        return;
                    }

                    if (sSM_USERS.USER_PASSWORD.ToString() == EnCryptDecrypt.CryptorEngine.Encrypt(txtPassword.Text,true))
                    {
                        this.LoggedOnUserID = int.Parse(sSM_USERS.PK_ID.ToString());
                        this.LoggedUserName = sSM_USERS.USER_CODE;
                        Session[FINSessionConstants.PassWord] = sSM_USERS.USER_PASSWORD;
                        //Session[FINSessionConstants.UserGroupID] = sSM_USERS.GROUP_ID;
                        Session[FINSessionConstants.UserName] = sSM_USERS.USER_CODE;
                        Session["FullUserName"] = sSM_USERS.FIRST_NAME + " " + sSM_USERS.MIDDLE_NAME + " " + sSM_USERS.LAST_NAME;

                        VMVServices.Web.Utils.UserName = sSM_USERS.USER_CODE;
                        VMVServices.Web.Utils.UserId = sSM_USERS.PASSPORT_NUMBER_OR_EMP_ID;
                      
                        VMVServices.Web.Utils.Multilanguage = false;
                        if (sSM_USERS.CHANGE_PASSWORD_ON_NEXT_LOGIN == FINAppConstants.N)
                        {
                            lblpwdNotMatch.Visible = false;
                            Session[FINSessionConstants.ModuleName] = FINListConstants_BLL.GL;
                            //ScriptManager.RegisterStartupScript(btnYes, typeof(Button), "openfullscreen", "window.open('MainPage/" + System.Configuration.ConfigurationManager.AppSettings["ModulePage"].ToString() + "','fs','fullscreen=yes');window.close();", true);
                            ScriptManager.RegisterStartupScript(btnYes, typeof(Button), "openfullscreen", "window.open('MainPage/" + System.Configuration.ConfigurationManager.AppSettings["ModulePage"].ToString() + "','fs','fullscreen=yes');window.close();", true);
                        }
                        else
                        {
                            txtPassword.Attributes.Add("value", "a");
                            lblpwdNotMatch.Visible = false;
                            btnChangePwd.Text = Prop_File_Data["ChangePassword_P"];
                            btnchangepwdCancel.Text = Prop_File_Data["Cancel_P"];
                            lblNewPassword.Text = Prop_File_Data["NewPassword_P"];
                            lblConfirmPassword.Text = Prop_File_Data["ConfirmPassword_P"];
                            //lblpwdNotMatch.Text = Prop_File_Data["PasswordNotMatch_P"];
                            pnlChangePwd.Visible = true;
                            MPEChangePwd.Show();
                        }
                    }
                    else
                    {
                        hf_pwdtryCount.Value = (int.Parse(hf_pwdtryCount.Value) + 1).ToString();
                        //ErrorCollection.Add("Invalid User", "Password is InValid.");
                        //lblError.Text = "Password is InValid";
                        ErrorCollection.Add(Prop_File_Data["InvalidUser_P"], Prop_File_Data["InvalidPassword_P"]);
                        lblError.Text = Prop_File_Data["InvalidPassword_P"];
                        if (int.Parse(hf_pwdtryCount.Value) > 3)
                        {
                            sSM_USERS.PASSWORD_LOCK = FINAppConstants.Y;
                            DBMethod.SaveEntity<SSM_USERS>(sSM_USERS,true);
                        }
                    }
                }
                else
                {                    
                    //ErrorCollection.Add("Invalid User", "User Name  is InValid");
                    //lblError.Text = "User Name  is InValid";
                    ErrorCollection.Add(Prop_File_Data["InvalidUser_P"], Prop_File_Data["InvalidUserName_P"]);
                    lblError.Text = Prop_File_Data["InvalidUserName_P"];
                }
            }
            catch (Exception ex)
            {
                //ErrorCollection.Remove("LoginError");
                //ErrorCollection.Add("LoginError", ex.Message);
                ErrorCollection.Remove(Prop_File_Data["LoginError_P"]);
                ErrorCollection.Add(Prop_File_Data["LoginError_P"], ex.Message);
                lblError.Text = ex.Message;
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    
                   // ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('validation.aspx','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void ChangeLanguage(object sender, EventArgs e)
        {
            LinkButton lb_lang = (LinkButton)sender;           
            Session[FINSessionConstants.Sel_Lng] = lb_lang.CommandArgument.ToString();
            divLang.InnerHtml = lb_lang.CommandName.ToString();

            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/Generic_" + Session["Sel_Lng"].ToString() + ".properties"));
            btnYes.Text = Prop_File_Data["Login_P"];
            btNo.Text = Prop_File_Data["Cancel_P"];
        }

        protected void btnChangePwd_Click(object sender, EventArgs e)
        {
            if (Session[FINSessionConstants.Sel_Lng] == null)
            {
                Session[FINSessionConstants.Sel_Lng] = "EN";
            }
            lblpwdNotMatch.Visible = false;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/Generic_" + Session["Sel_Lng"].ToString() + ".properties"));
            if (txtNewPassword.Text != txtConfirmPassword.Text)
            {
                MPEChangePwd.Show();
                lblpwdNotMatch.Text = Prop_File_Data["PasswordNotMatch_P"];
                lblpwdNotMatch.Visible = true;

            }
            else
            {
                using (IRepository<SSM_USERS> userCtx = new DataRepository<SSM_USERS>())
                {
                    sSM_USERS = userCtx.Find(r =>
                        (r.USER_CODE.ToUpper() == this.LoggedUserName.ToString().ToUpper())
                        ).SingleOrDefault();
                }
                sSM_USERS.USER_PASSWORD = EnCryptDecrypt.CryptorEngine.Encrypt(txtNewPassword.Text, true);
                sSM_USERS.CHANGE_PASSWORD_ON_NEXT_LOGIN = FINAppConstants.N;
                sSM_USERS.PASSWORD_CHANGE_DATE = DateTime.Now;
                DBMethod.SaveEntity<SSM_USERS>(sSM_USERS,true);
            }

        }

    }
}