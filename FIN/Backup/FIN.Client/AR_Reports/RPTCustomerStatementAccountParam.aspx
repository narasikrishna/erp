﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="RPTCustomerStatementAccountParam.aspx.cs" Inherits="FIN.Client.AR_Reports.RPTCustomerStatementAmountParam" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 900px" id="div1">
        <div class="divRowContainer">
            <div class="lblBox LNOrient" style="width: 200px" id="lblGlobalSegment">
                Global Segment
            </div>
            <div class="divtxtBox LNOrient" style="width: 200px">
                <asp:DropDownList ID="ddlGlobalSegment" runat="server" TabIndex="1" CssClass=" ddlStype">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
    </div>
    <div class="divRowContainer">
        <div class="lblBox LNOrient" style="width: 200px" id="lblRequisitionType">
            Customer From
        </div>
        <div class="divtxtBox LNOrient" style="width: 200px">
            <asp:DropDownList ID="ddlCustomerFrom" runat="server" TabIndex="2" CssClass="ddlStype">
            </asp:DropDownList>
        </div>
        <div class="colspace LNOrient">
            &nbsp</div>
        <div class="lblBox LNOrient" style="width: 200px" id="Div5">
            Customer To
        </div>
        <div class="divtxtBox LNOrient" style="width: 200px">
            <asp:DropDownList ID="ddlCustomerTo" runat="server" TabIndex="3" CssClass="ddlStype">
            </asp:DropDownList>
        </div>
    </div>
    <div class="divClear_10">
    </div>
    <div class="divRowContainer" id="CusNo" runat="server" visible="false">
        <div class="lblBox LNOrient" style="width: 200px" id="Div3">
            Customer Number
        </div>
        <div class="divtxtBox LNOrient" style="width: 200px">
            <asp:DropDownList ID="ddlCustomerNumber" runat="server" TabIndex="4" CssClass=" ddlStype">
            </asp:DropDownList>
        </div>
    </div>
    <div class="divClear_10">
    </div>
    <div class="divRowContainer">
        <div class="lblBox LNOrient" style="width: 200px" id="Div7">
            From Amount
        </div>
        <div class="divtxtBox LNOrient" style="width: 200px">
            <asp:TextBox ID="txtFromAmt" runat="server" TabIndex="8" CssClass="txtBox_N" MaxLength="13"></asp:TextBox>
            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers,Custom"
                ValidChars=".,-" TargetControlID="txtFromAmt" />
        </div>
        <div class="colspace LNOrient">
            &nbsp</div>
        <div class="lblBox LNOrient" style="width: 200px" id="Div11">
            To Amount
        </div>
        <div class="divtxtBox LNOrient" style="width: 200px">
            <asp:TextBox ID="txtToAmount" runat="server" TabIndex="9" CssClass="txtBox_N" MaxLength="13"></asp:TextBox>
            <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterType="Numbers,Custom"
                ValidChars=".,-" TargetControlID="txtToAmount" />
        </div>
    </div>
    <div class="divClear_10">
    </div>
    <div class="divRowContainer">
        <div class="lblBox LNOrient" style="width: 200px" id="lblDate">
            From Date
        </div>
        <div class="divtxtBox LNOrient" style="width: 200px">
            <asp:TextBox ID="txtFromDate" runat="server" TabIndex="2" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]]  RequiredField txtBox"></asp:TextBox>
            <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calExtender1" TargetControlID="txtFromDate"
                OnClientDateSelectionChanged="checkDate" />
        </div>
        <div class="colspace LNOrient">
            &nbsp</div>
        <div class="lblBox LNOrient" style="width: 200px" id="Div2">
            To Date
        </div>
        <div class="divtxtBox LNOrient" style="width: 200px">
            <asp:TextBox ID="txtToDate" runat="server" TabIndex="3" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]]  RequiredField txtBox"></asp:TextBox>
            <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtToDate"
                OnClientDateSelectionChanged="checkDate" />
        </div>
    </div>
    <div class="divClear_10">
    </div>
    <div class="divClear_10">
    </div>
    <div class="divRowContainer">
        <div class="lblBox LNOrient" style="width: 200px" id="Div6">
            Include Zeros
        </div>
        <div class="divtxtBox LNOrient" style="width: 150px">
            <asp:CheckBox ID="chkWithZero" runat="server" Text="" Checked="false" TabIndex="8" />
        </div>
    </div>
    <div class="divRowContainer">
    </div>
    <div class="divClear_10">
        <div class="divFormcontainer" style="width: 370px" id="divMainContainer">
            <div class="divRowContainer divReportAction">
                <table class="ReportTable">
                    <tr>
                        <td>
                            <asp:ImageButton ID="btnSave" runat="server" ImageUrl="../Images/show-report-icon.png"
                                OnClick="btnSave_Click" Width="35px" Height="25px" Style="border: 0px" />
                            <%--<asp:Button ID="btnSave" runat="server" Text="Show Report" CssClass="btn" OnClick="btnSave_Click" />--%>
                        </td>
                        <td>
                            <%--<asp:Button ID="btnPrint" runat="server" Text="Print" CssClass="btn" />--%>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/AR/ARChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });

        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

    </script>
</asp:Content>
