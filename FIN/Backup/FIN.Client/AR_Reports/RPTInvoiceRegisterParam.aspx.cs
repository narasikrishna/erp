﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.GL;
using FIN.DAL.AP;
using FIN.DAL.AR;
using FIN.BLL;
using FIN.BLL.AP;
using FIN.BLL.AR;
using FIN.BLL.HR;
using FIN.BLL.GL;
using FIN.BLL.SSM;
using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;

namespace FIN.Client.AR_Reports
{
    public partial class RPTInvoiceRegisterParam : PageBase
    {
        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    ChangeLanguage();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ARInvoiceRegisterReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void ChangeLanguage()
        {
            try
            {
                ErrorCollection.Clear();
                if (VMVServices.Web.Utils.LanguageCode.ToString().Length > 0)
                {
                    Dictionary<string, string> Prop_File_Data;
                    Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/AR_" + Session["Sel_Lng"].ToString() + ".properties"));
                    rbUNPosted.Items[0].Text = (Prop_File_Data["Posted_P"]);
                    rbUNPosted.Items[0].Selected = true;
                    rbUNPosted.Items[1].Text = (Prop_File_Data["Unposted_P"]);
                    rbUNPosted.Items[2].Text = (Prop_File_Data["Cancelled_P"]);
                    rbUNPosted.Items[3].Text = (Prop_File_Data["All_P"]);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("MisReceiptChangeLang", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                Startup();
                FillStartDate();
                FillComboBox();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ARInvoiceRegisterReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void FillStartDate()
        {
            string str_finyear = FINSP.GetSPFOR_FiscalYear(DateTime.Now.Date.ToString("dd/MM/yyyy"));
            DataTable dtDate = new DataTable();
            if (str_finyear != string.Empty)
            {
                dtDate = DBMethod.ExecuteQuery(FIN.DAL.GL.InventoryReports_DAL.getCalDate(str_finyear)).Tables[0];
                txtFromDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["cal_eff_start_dt"].ToString());
                txtToDate.Text = DBMethod.ConvertDateToString(dtDate.Rows[0]["call_eff_end_dt"].ToString());
            }
        }
        private void Startup()
        {
            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));

            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();
        }

        private void ParamValidation()
        {
            ErrorCollection.Clear();

            ErrorCollection = CommonUtils.Validate2AmountsExp(txtInvoiceFromAmount.Text, txtInvoiceToAmount.Text);
            if (ErrorCollection.Count > 0)
            {
                return;
            }

            ErrorCollection = CommonUtils.ValidateDateRange(txtFromDate.Text, txtToDate.Text);

            if (ErrorCollection.Count > 0)
            {
                return;
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;


                ParamValidation();
                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                if (txtFromDate.Text != string.Empty)
                {
                    htFilterParameter.Add("From_Date", txtFromDate.Text);
                }
                if (txtToDate.Text != string.Empty)
                {
                    htFilterParameter.Add("To_Date", txtToDate.Text);
                }

                if (ddlFromCustomer.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("FROM_CUST_NAME", ddlFromCustomer.SelectedItem.Text.ToString());
                    htFilterParameter.Add("FROM_CUST_ID", ddlFromCustomer.SelectedValue.ToString());
                }
                else
                {
                    htFilterParameter.Add("FROM_CUST_NAME", "All");
                }

                if (ddlToCustomer.SelectedValue != string.Empty)
                {
                    htFilterParameter.Add("TO_CUST_NAME", ddlToCustomer.SelectedItem.Text.ToString());
                    htFilterParameter.Add("TO_CUST_ID", ddlToCustomer.SelectedValue.ToString());
                }
                else
                {
                    htFilterParameter.Add("TO_CUST_NAME", "All");
                }


                if (ddlFromInvoiceNumber.SelectedValue.ToString().Length > 0)
                {
                  
                    htFilterParameter.Add("From_InvNum", ddlFromInvoiceNumber.SelectedItem.Text.ToString());
                }

                else
                {
                    htFilterParameter.Add("FromInvNumber", "All");
                }
                if (ddlToInvoiceNumber.SelectedValue.ToString().Length > 0)
                {
                  
                    htFilterParameter.Add("To_InvNum", ddlToInvoiceNumber.SelectedItem.Text.ToString());
                }
                else
                {
                    htFilterParameter.Add("ToInvNumber", "All");
                }
                if (CommonUtils.ConvertStringToDecimal(txtInvoiceFromAmount.Text) > 0)
                {
                    htFilterParameter.Add("FromAmount", txtInvoiceFromAmount.Text);
                    htFilterParameter.Add("From_Amt", DBMethod.GetAmtDecimalCommaSeparationValue(txtInvoiceFromAmount.Text));

                    if (CommonUtils.ConvertStringToDecimal(txtInvoiceToAmount.Text) > 0)
                    {
                        htFilterParameter.Add("ToAmount", txtInvoiceToAmount.Text);
                        htFilterParameter.Add("To_Amt", DBMethod.GetAmtDecimalCommaSeparationValue(txtInvoiceToAmount.Text));
                    }
                }

                htFilterParameter.Add("POSTEDTYPE", rbUNPosted.SelectedValue.ToString());
                htFilterParameter.Add("POSTEDSTATUS", rbUNPosted.SelectedItem.Text);

                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;

                ReportData = FIN.BLL.AR.Invoice_BLL.GetReportData();

                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());

                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerPath + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("InvoiceRegisterReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }
        private void FillComboBox()
        {
            // Supplier_BLL.GetSupplier(ref ddlSupplierName);
            FIN.BLL.AP.Supplier_BLL.GetCustomerNumber_Name(ref ddlFromCustomer, true);
            FIN.BLL.AP.Supplier_BLL.GetCustomerNumber_Name(ref ddlToCustomer, true);
            FIN.BLL.AR.Invoice_BLL.fn_getInvoiceNumber_forcustomer(ref ddlFromInvoiceNumber, "", true);
            FIN.BLL.AR.Invoice_BLL.fn_getInvoiceNumber_forcustomer(ref ddlToInvoiceNumber, "", true);
            //Lookup_BLL.GetLookUpValues(ref ddlPaymentType, "PAYMENT_MODE");
            //FIN.BLL.AR.DayBookPayment_BLL.GetGroupName(ref ddlGlobalSegment);

        }
    }
}