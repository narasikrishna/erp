﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.GL;
using FIN.DAL.AP;
using FIN.DAL.AR;
using FIN.BLL;
using FIN.BLL.AP;
using FIN.BLL.AR;
using FIN.BLL.HR;
using FIN.BLL.GL;
using FIN.BLL.SSM;
using VMVServices.Web;
using System.Data;
using VMVServices.Services.Data;

namespace FIN.Client.Reports.AR
{
    public partial class RPTAgingAnalysisReportParam : PageBase
    {
        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ARAgingAnalysisReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();
                FillComboBox();

                Startup();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ARAgingAnalysisReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        private void Startup()
        {

            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));

            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();

        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;


                ErrorCollection.Remove("datemsg1");
                ErrorCollection.Remove("datemsg2");
                ErrorCollection.Remove("datemsg");
                ErrorCollection.Remove("datemsg1st");
                ErrorCollection.Remove("datemsg2nd");
                ErrorCollection.Remove("datemsgcase");


                if (txt30.Text != string.Empty)
                {
                    htFilterParameter.Add("firstCase", txt30.Text);

                    if (txt60.Text != string.Empty)
                    {
                        if (CommonUtils.ConvertStringToInt(txt30.Text) < CommonUtils.ConvertStringToInt(txt60.Text))
                        {
                            htFilterParameter.Add("secondCase", txt60.Text);
                        }
                        else
                        {
                            ErrorCollection.Add("datemsg1st", "Aging Bucket2 should be greater than Aging Bucket1");
                        }
                    }
                    else
                    {
                        ErrorCollection.Add("datemsg1", "Aging Bucket2 cannot be empty");
                    }
                }
                else
                {
                    ErrorCollection.Add("datemsgcase", "Aging Bucket1 cannot be empty");
                }
                if (ErrorCollection.Count > 0)
                {
                    return;
                }


                if (txt90.Text != string.Empty && txt60.Text != string.Empty)
                {
                    if (CommonUtils.ConvertStringToInt(txt60.Text) < CommonUtils.ConvertStringToInt(txt90.Text))
                    {
                        htFilterParameter.Add("thirdCase", txt90.Text);
                    }
                    else
                    {
                        ErrorCollection.Add("datemsg2", "Aging Bucket3 should be greater than Aging Bucket2");
                    }
                }
                else
                {
                    ErrorCollection.Add("datemsg2nd", "Aging Bucket3 cannot be empty");
                }

                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                if (txtFromDate.Text != string.Empty)
                {
                    htFilterParameter.Add("From_Date", txtFromDate.Text);




                    VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;
                    VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;

                    ReportData = FIN.BLL.AR.ARAgingAnalysis_BLL.GetReportData();

                    htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());

                    ReportFormulaParameter = htHeadingParameters;

                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerPath + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);
                }
                else
                {
                    ErrorCollection.Add("datemsg", "Date cannot be empty");
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ARAgingAnalysisReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }

        }
        private void FillComboBox()
        {

            //FIN.BLL.AR.ARAgingAnalysis_BLL.GetGroupName(ref ddlGlobalSegment);

        }
    }
}