﻿function fn_changeLng(Sel_lng) {
    //alert('inside : ' + Sel_lng);
    var Sel_lng = '../LanguageCollection/HR_' + Sel_lng + '.properties';
    //alert(Sel_lng);
    $.getJSON(Sel_lng, function (result) {

        var x = document.getElementById("lblName");
        x.innerHTML = result.Name_p

        x = document.getElementById("lblCompetencyType");
        x.innerHTML = result.CompetencyType_P

        x = document.getElementById("lblEffectiveDate");
        x.innerHTML = result.EffectiveDate_P

        x = document.getElementById("lblEndDate");
        x.innerHTML = result.EndDate_P

    });
}