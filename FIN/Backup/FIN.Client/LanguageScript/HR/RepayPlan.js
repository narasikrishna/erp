﻿function fn_changeLng(Sel_lng) {
    //alert('inside : ' + Sel_lng);
    var Sel_lng = '../LanguageCollection/HR_' + Sel_lng + '.properties';
    //alert(Sel_lng);
    $.getJSON(Sel_lng, function (result) {

        var x = document.getElementById("lblEmployeeName");
        x.innerHTML = result.EmployeeName_p

        x = document.getElementById("lblRequestNumber");
        x.innerHTML = result.RequestNumber_p

        x = document.getElementById("lblRequestDate");
        x.innerHTML = result.RequestDate_p

        x = document.getElementById("lblDepartment");
        x.innerHTML = result.Department_p

        x = document.getElementById("lblType");
        x.innerHTML = result.Type_p

        x = document.getElementById("lblDesignation");
        x.innerHTML = result.Designation_p

        x = document.getElementById("lblAmount");
        x.innerHTML = result.Amount_p

    });
}