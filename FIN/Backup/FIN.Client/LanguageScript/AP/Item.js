﻿function fn_changeLng(Sel_lng) {
    //alert('inside : ' + Sel_lng);
    var Sel_lng = '../LanguageCollection/AP_' + Sel_lng + '.properties';
    //alert(Sel_lng);
    $.getJSON(Sel_lng, function (result) {
        //alert(result.CalendarName_P);

        var x = document.getElementById("lblItemNumber");
        x.innerHTML = result.ItemNumber_P

        x = document.getElementById("lblItemShortName");
        x.innerHTML = result.ItemShortName_P

        x = document.getElementById("lblItemName");
        x.innerHTML = result.ItemName_P

        x = document.getElementById("lblItemDescription");
        x.innerHTML = result.ItemDescription_P

        x = document.getElementById("lblItemCategory");
        x.innerHTML = result.ItemCategory_P

        x = document.getElementById("lblItemUOM");
        x.innerHTML = result.ItemUOM_P

        x = document.getElementById("lblEffectiveStartDate");
        x.innerHTML = result.EffectiveStartDate_P

        x = document.getElementById("lblEffectiveEnddate");
        x.innerHTML = result.EffectiveEnddate_P

        x = document.getElementById("lblUnitPrice");
        x.innerHTML = result.UnitPrice_P

        x = document.getElementById("lblReOrderQuantity");
        x.innerHTML = result.SegmentName_P

        x = document.getElementById("lblItemGroup");
        x.innerHTML = result.ItemGroup_P

        x = document.getElementById("lblActive");
        x.innerHTML = result.Active_P

        x = document.getElementById("lblSalesFlag");
        x.innerHTML = result.SalesFlag_P

        x = document.getElementById("lblPurchaseFlag");
        x.innerHTML = result.PurchaseFlag_P

        x = document.getElementById("lblServiceDuration");
        x.innerHTML = result.ServiceDuration_P

        x = document.getElementById("lblServiceUOM");
        x.innerHTML = result.ServiceUOM_P

        x = document.getElementById("lblItemLotControl");
        x.innerHTML = result.ItemLotControl_P

        x = document.getElementById("lblItemSerialControl");
        x.innerHTML = result.ItemSerialControl_P

        x = document.getElementById("lblWeight");
        x.innerHTML = result.Weight_P

        x = document.getElementById("lblWeightUOM");
        x.innerHTML = result.WeightUOM_P

        x = document.getElementById("lblArea");
        x.innerHTML = result.Area_P

        x = document.getElementById("lblAreaUOM");
        x.innerHTML = result.AreaUOM_P

        x = document.getElementById("lblVolume");
        x.innerHTML = result.Volume_P

        x = document.getElementById("lblVolumeUOM");
        x.innerHTML = result.VolumeUOM_P

        // Button 
        //        x = document.getElementById("ContentPlaceHolder1_btnNewUser");
        //        x.value = btnNewUser;

        //        x = document.getElementById("ContentPlaceHolder1_btnBPLogin");
        //        x.value = btnBPLogin;

        //        x = document.getElementById("ContentPlaceHolder1_btnInternalUser");
        //        x.value = btnInternalUser;
    });
}