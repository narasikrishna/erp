﻿function fn_changeLng(Sel_lng) {
    //alert('inside : ' + Sel_lng);
    var Sel_lng = '../LanguageCollection/GL_' + Sel_lng + '.properties';
    //alert(Sel_lng);
    $.getJSON(Sel_lng, function (result) {
        //alert(result.CalendarName_P);
        
        var x = document.getElementById("lblGroupName");
        x.innerHTML = result.GroupName_P

        x = document.getElementById("lblAlternateName");
        x.innerHTML = result.AlternateName_P

        x = document.getElementById("lblFinancialGroup");
        x.innerHTML = result.FinancialGroup_P

        x = document.getElementById("lblFundTransfer");
        x.innerHTML = result.Fund_Transfer_P

        x = document.getElementById("lblEffectiveDate");
        x.innerHTML = result.EffectiveDate_P

        x = document.getElementById("lblEndDate");
        x.innerHTML = result.EndDate_P

        x = document.getElementById("lblActive");
        x.innerHTML = result.Active_P
     
    });
}
