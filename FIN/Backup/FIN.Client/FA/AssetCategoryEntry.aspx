﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="AssetCategoryEntry.aspx.cs" Inherits="FIN.Client.FA.AssetCategoryEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 1000px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblTaxName">
                Category Name
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 255px">
                <asp:TextBox ID="txtCategoryName" MaxLength="100" CssClass="validate[required] RequiredField   txtBox_en"
                    TabIndex="1" runat="server" Width="255px"></asp:TextBox>
            </div>
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblTaxNameAr">
                Category Name(Arabic)
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 255px">
                <asp:TextBox ID="txtCategoryNameAr" MaxLength="100" CssClass="RequiredField   txtBox_ol"
                    TabIndex="1" runat="server" Width="255px"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblTaxRate">
                Category Description
            </div>
            <div class="divtxtBox  LNOrient" style=" width: 255px">
                <asp:TextBox ID="txtCategoryDesc" MaxLength="100" CssClass="validate[required] RequiredField   txtBox"
                    TabIndex="2" runat="server" Width="255px"></asp:TextBox>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style=" width: 200px" id="lblActive">
                Active
            </div>
            <div class="divChkbox LNOrient" style=" width: 150px">
                <asp:CheckBox ID="chkEnable" runat="server" TabIndex="3" Checked="true" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="LNOrient">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                Width="500px" OnRowCancelingEdit="gvData_RowCancelingEdit" OnRowCommand="gvData_RowCommand"
                OnRowCreated="gvData_RowCreated" OnRowDataBound="gvData_RowDataBound" OnRowDeleting="gvData_RowDeleting"
                OnRowEditing="gvData_RowEditing" OnRowUpdating="gvData_RowUpdating" ShowFooter="True">
                <Columns>
                    <asp:TemplateField HeaderText="MINOR_CATEGORY_ID" Visible="false">
                        <ItemTemplate>
                            <asp:Label ID="lblMINORCATEGORID" Text='<%# Eval("MINOR_CATEGORY_ID") %>' runat="server"></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:Label ID="lblMINORCATEGORID" Text='<%# Eval("MINOR_CATEGORY_ID") %>' runat="server"></asp:Label>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="lblMINORCATEGORID" Text='' runat="server"></asp:Label>
                        </FooterTemplate>
                        <ItemStyle CssClass="ColumnHide" />
                        <HeaderStyle CssClass="ColumnHide" />
                        <FooterStyle CssClass="ColumnHide" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="MAJOR_CATEGORY_ID" Visible="false">
                        <ItemTemplate>
                            <asp:Label ID="lblMAJORCATEGORID" Text='<%# Eval("MAJOR_CATEGORY_ID") %>' runat="server"></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:Label ID="lblMAJORCATEGORID" Text='<%# Eval("MAJOR_CATEGORY_ID") %>' runat="server"></asp:Label>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="lblMAJORCATEGORID" Text='' runat="server"></asp:Label>
                        </FooterTemplate>
                        <ItemStyle CssClass="ColumnHide" />
                        <HeaderStyle CssClass="ColumnHide" />
                        <FooterStyle CssClass="ColumnHide" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Minor Category">
                        <ItemTemplate>
                            <asp:Label ID="lblMinorCategory" Text='<%# Eval("MINOR_CATEGORY_NAME") %>' CssClass="Label_Style"
                                runat="server"></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtMinorCategory" MaxLength="100" CssClass=" RequiredField   txtBox_en"
                                TabIndex="4" Text='<%# Eval("MINOR_CATEGORY_NAME") %>' Width="210px" runat="server"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtMinorCategory" MaxLength="100" CssClass=" RequiredField   txtBox_en"
                                TabIndex="4" runat="server" Width="210px"></asp:TextBox>
                        </FooterTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Minor Category(Arabic)">
                        <ItemTemplate>
                            <asp:Label ID="lblMinorCategoryAr" Text='<%# Eval("MINOR_CATEGORY_NAME_OL") %>' CssClass="Label_Style"
                                runat="server"></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtMinorCategoryAr" MaxLength="100" CssClass="   txtBox_ol"
                                TabIndex="4" Text='<%# Eval("MINOR_CATEGORY_NAME_OL") %>' Width="210px" runat="server"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtMinorCategoryAr" MaxLength="100" CssClass="   txtBox_ol"
                                TabIndex="4" runat="server" Width="210px"></asp:TextBox>
                        </FooterTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Minor Category Description">
                        <ItemTemplate>
                            <asp:Label ID="lblMinorCategoryDesc" CssClass="Label_Style" Text='<%# Eval("MINOR_CATEGORY_DESC") %>'
                                runat="server"></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtMinorCategoryDesc" MaxLength="150" CssClass=" RequiredField   txtBox"
                                TabIndex="5" Text='<%# Eval("MINOR_CATEGORY_DESC") %>' runat="server" Width="210px"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtMinorCategoryDesc" MaxLength="150" CssClass=" RequiredField   txtBox"
                                TabIndex="5" Width="210px" Text='' runat="server"></asp:TextBox>
                        </FooterTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Active">
                        <EditItemTemplate>
                            <asp:CheckBox ID="chkGridEnable" runat="server" TabIndex="6" Checked='<%# Convert.ToBoolean(Eval("MINOR_ENABLED_FLAG")) %>' />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:CheckBox ID="chkGridEnable" runat="server" TabIndex="6" Checked="true" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkGridEnable" runat="server" Checked='<%# Convert.ToBoolean(Eval("MINOR_ENABLED_FLAG")) %>' />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <asp:ImageButton ID="ibtnEdit" runat="server" AlternateText="Edit" CausesValidation="false"
                                TabIndex="8" ToolTip="Edit" CommandName="Edit" ImageUrl="~/Images/Edit.GIF" />
                            <asp:ImageButton ID="ibtnDelete" runat="server" AlternateText="Delete" CausesValidation="false"
                                TabIndex="9" ToolTip="Delete" CommandName="Delete" ImageUrl="~/Images/Delete.JPG" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="ibtnUpdate" runat="server" AlternateText="Update" CommandName="Update"
                                TabIndex="10" ToolTip="Update" Height="20px" ImageUrl="~/Images/Update.png" />
                            <asp:ImageButton ID="ibtnCancel" runat="server" AlternateText="Cancel" CausesValidation="false"
                                TabIndex="11" ToolTip="Cancel" CommandName="Cancel" ImageUrl="~/Images/Close.jpg" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="ibtnInsert" runat="server" AlternateText="Add" TabIndex="7"
                                CommandName="FooterInsert" ToolTip="Add" ImageUrl="~/Images/Add.jpg" />
                        </FooterTemplate>
                        <ItemStyle BorderColor="#7EACB1" HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn"
                            TabIndex="12" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="13" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="14" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="15" />
                    </td>
                </tr>
            </table>
        </div>
        <%-- <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table>
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>--%>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/FA/FAChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">


        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });
    </script>
</asp:Content>
