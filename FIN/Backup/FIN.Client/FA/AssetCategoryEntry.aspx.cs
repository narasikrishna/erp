﻿using System;

using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Collections;
using System.Configuration;
using System.Data;

using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Linq;
using FIN.DAL;
using FIN.BLL;
using FIN.DAL.FA;
using FIN.BLL.FA;

using VMVServices.Web;

namespace FIN.Client.FA
{
    public partial class AssetCategoryEntry : PageBase
    {
        /// <summary>
        /// Creted by Jeyarani
        /// Master details for asset's major and minor categories...
        /// </summary>

        AST_MAJOR_CATEGORY_MST AST_MAJOR_CATEGORY_MST = new AST_MAJOR_CATEGORY_MST();
        AST_MINOR_CATEGORY_DTL AST_MINOR_CATEGORY_DTL = new AST_MINOR_CATEGORY_DTL();
        AssetCategoryBLL AssetCategoryBLL = new AssetCategoryBLL();
        string screenMode = string.Empty;
        DataSet dsData = new DataSet();
        DataTable dtGridData = new DataTable();
        Boolean headerRowVisible;
        SortedList slControls = new SortedList();
        string strCtrlTypes = string.Empty;
        string strMessage = string.Empty;


        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();

                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Error1", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        #region "Start Up "
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                //btnSave.Visible = false;
                //btnDelete.Visible = true;
              //  pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                //btnSave.Visible = true;
                //btnDelete.Visible = false;
              //  pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                // btnSave.Text = "Update";
            }
            UserRightsChecking();

        }
        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    //  btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    // btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    // btnDelete.Visible = false;
                }
            }
        }
        private void AssignToControl()
        {

            try
            {
                ErrorCollection.Clear();

                Startup();

                EntityData = null;
                Session[FINSessionConstants.GridData] = null;



                using (IRepository<AST_MINOR_CATEGORY_DTL> userCtx = new DataRepository<AST_MINOR_CATEGORY_DTL>())
                {
                    dtGridData = DataSetLinqOperators.CopyToDataTable<AST_MINOR_CATEGORY_DTL>(userCtx.Find(r =>
                        (r.MINOR_CATEGORY_ID == Master.RecordID)
                        ));
                }

                if (Master.Mode != FINAppConstants.Add && Master.RecordID > 0)
                {
                    dtGridData = DBMethod.ExecuteQuery(FA_SQL.GetAssetCategoriesWithData(Master.RecordID)).Tables[0];
                    using (IRepository<AST_MAJOR_CATEGORY_MST> userCtx = new DataRepository<AST_MAJOR_CATEGORY_MST>())
                    {
                        AST_MAJOR_CATEGORY_MST = userCtx.Find(r =>
                            (r.MAJOR_CATEGORY_ID == Master.RecordID)
                            ).SingleOrDefault();
                    }

                    EntityData = AST_MAJOR_CATEGORY_MST;

                    txtCategoryName.Text = AST_MAJOR_CATEGORY_MST.CATEGORY_NAME;
                    txtCategoryNameAr.Text = AST_MAJOR_CATEGORY_MST.CATEGORY_NAME_OL;
                    txtCategoryDesc.Text = AST_MAJOR_CATEGORY_MST.CATEGORY_DESC;
                    chkEnable.Checked = AST_MAJOR_CATEGORY_MST.ENABLED_FLAG.ToString() == "1" ? true : false;
                }
                else
                {
                    dtGridData = DBMethod.ExecuteQuery(FA_SQL.GetAssetCategoriesWithData(0)).Tables[0];
                }


                dtGridData.Columns.Add("DELETED");
                BindGrid(dtGridData);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Error1", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void BindGrid(DataTable dtData)
        {
            try
            {
                ErrorCollection.Clear();
                headerRowVisible = false;
                Session[FINSessionConstants.GridData] = dtData;

                DataTable dtTempTable = dtData.Copy();
                if (dtTempTable.Rows.Count == 0)
                {
                    DataRow dr = dtTempTable.NewRow();
                    dr[0] = "0";
                    dr["MINOR_ENABLED_FLAG"] = "FALSE";
                    dtTempTable.Rows.Add(dr);
                    headerRowVisible = true;
                }

                gvData.DataSource = dtTempTable;
                gvData.DataBind();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("JR_ROW_UP", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }
        #endregion

        #region Insert ,Update the records

        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();

                if (EntityData != null)
                {
                    AST_MAJOR_CATEGORY_MST = (AST_MAJOR_CATEGORY_MST)EntityData;
                }

                //Organization and calendar id
                AST_MAJOR_CATEGORY_MST.ORGANIZATION_MASTER_ID = VMVServices.Web.Utils.OrganizationID.ToString();
                //    AST_MAJOR_CATEGORY_MST.CALENDAR_ID = VMVServices.Web.Utils.CalendarID.ToString() == null ? 0 : VMVServices.Web.Utils.CalendarID;

                AST_MAJOR_CATEGORY_MST.CATEGORY_NAME = txtCategoryName.Text;
                AST_MAJOR_CATEGORY_MST.CATEGORY_NAME_OL = txtCategoryNameAr.Text;
                AST_MAJOR_CATEGORY_MST.CATEGORY_DESC = txtCategoryDesc.Text;
                AST_MAJOR_CATEGORY_MST.ENABLED_FLAG = chkEnable.Checked == true ? FINAppConstants.EnabledFlag : FINAppConstants.DisabledFlag;

                if (Master.Mode != null)
                {
                    if (Master.Mode != FINAppConstants.Add && Master.RecordID > 0)
                    {
                        AST_MAJOR_CATEGORY_MST.MODIFIED_BY = this.LoggedUserName;
                        AST_MAJOR_CATEGORY_MST.MODIFIED_DATE = DateTime.Today;
                    }
                    else
                    {
                        AST_MAJOR_CATEGORY_MST.CREATE_BY = this.LoggedUserName;
                        AST_MAJOR_CATEGORY_MST.CREATED_DATE = DateTime.Today;
                        AST_MAJOR_CATEGORY_MST.MAJOR_CATEGORY_ID = DBMethod.GetPrimaryKeyValue(FINSequenceConstant.AST_MAJOR_CATEGORY_SEQ);
                        
                    }
                    AST_MAJOR_CATEGORY_MST.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, AST_MAJOR_CATEGORY_MST.MAJOR_CATEGORY_ID.ToString());
                }

                AssetCategoryBLL.AST_MAJOR_CATEGORY_MST = AST_MAJOR_CATEGORY_MST;
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("JR_ROW_UP", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void AssignToGridBE()
        {
            try
            {
                ErrorCollection.Clear();

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                };

                for (int iLoop = 0; iLoop < dtGridData.Rows.Count; iLoop++)
                {
                    AST_MINOR_CATEGORY_DTL AST_MINOR_CATEGORY_DTL = new AST_MINOR_CATEGORY_DTL();

                    AST_MINOR_CATEGORY_DTL.MAJOR_CATEGORY_ID = AST_MAJOR_CATEGORY_MST.MAJOR_CATEGORY_ID;
                    AST_MINOR_CATEGORY_DTL.MINOR_CATEGORY_ID = int.Parse(dtGridData.Rows[iLoop][FINColumnConstants.MINOR_CATEGORY_ID].ToString());
                    AST_MINOR_CATEGORY_DTL.MINOR_CATEGORY_DESC = dtGridData.Rows[iLoop][FINColumnConstants.MINOR_CATEGORY_DESC].ToString();
                    AST_MINOR_CATEGORY_DTL.MINOR_CATEGORY_NAME = dtGridData.Rows[iLoop][FINColumnConstants.MINOR_CATEGORY_NAME].ToString();
                    AST_MINOR_CATEGORY_DTL.MINOR_CATEGORY_NAME_OL = dtGridData.Rows[iLoop]["MINOR_CATEGORY_NAME_OL"].ToString();                 
                    if (dtGridData.Rows[iLoop][FINColumnConstants.ENABLED_FLAG].ToString().ToUpper() == "TRUE")
                    {
                        AST_MINOR_CATEGORY_DTL.ENABLED_FLAG = FINAppConstants.EnabledFlag;
                    }
                    else
                    {
                        AST_MINOR_CATEGORY_DTL.ENABLED_FLAG = FINAppConstants.DisabledFlag;
                    }

                    //Organization and calendar id
                    AST_MINOR_CATEGORY_DTL.ORGANIZATION_MASTER_ID = VMVServices.Web.Utils.OrganizationID.ToString();
                    // AST_MINOR_CATEGORY_DTL.CALENDAR_ID = VMVServices.Web.Utils.CalendarID.ToString() == null ? 0 : VMVServices.Web.Utils.CalendarID;

                    if (dtGridData.Rows[iLoop]["DELETED"].ToString() == "Y")
                    {
                        AST_MINOR_CATEGORY_DTL.MINOR_CATEGORY_ID = Convert.ToInt32(dtGridData.Rows[iLoop][FINColumnConstants.MINOR_CATEGORY_ID].ToString());
                        DBMethod.DeleteEntity<AST_MINOR_CATEGORY_DTL>(AST_MINOR_CATEGORY_DTL);
                    }
                    else
                    {
                        if (Convert.ToInt32(dtGridData.Rows[iLoop][FINColumnConstants.MINOR_CATEGORY_ID].ToString()) > 0)
                        {
                            AST_MINOR_CATEGORY_DTL.MINOR_CATEGORY_ID = Convert.ToInt32(dtGridData.Rows[iLoop][FINColumnConstants.MINOR_CATEGORY_ID].ToString());
                            AST_MINOR_CATEGORY_DTL.MODIFIED_BY = this.LoggedUserName;
                            AST_MINOR_CATEGORY_DTL.MODIFIED_DATE = DateTime.Today;
                            AST_MINOR_CATEGORY_DTL.CREATE_BY = this.LoggedUserName;
                            DBMethod.SaveEntity<AST_MINOR_CATEGORY_DTL>(AST_MINOR_CATEGORY_DTL, true);
                        }
                        else
                        {
                            AST_MINOR_CATEGORY_DTL.CREATE_BY = this.LoggedUserName;
                            AST_MINOR_CATEGORY_DTL.CREATED_DATE = DateTime.Today;
                            AST_MINOR_CATEGORY_DTL.MINOR_CATEGORY_ID = DBMethod.GetPrimaryKeyValue(FINSequenceConstant.AST_MINOR_CATEGORY_DTL_SEQ);
                            DBMethod.SaveEntity<AST_MINOR_CATEGORY_DTL>(AST_MINOR_CATEGORY_DTL);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("JR_ROW_UP", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                AssignToBE();
               

                ErrorCollection.Remove("gridvalidation");
                Dictionary<string, string> Prop_File_Data;
                Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/FA_" + Session["Sel_Lng"].ToString() + ".properties"));
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (dtGridData.Rows.Count == 0)
                {
                    ErrorCollection.Add("gridvalidation", Prop_File_Data["gridvalidation_P"]);
                     
                }


                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                AssetCategoryBLL.Validate();
                if (AssetCategoryBLL.ErrorCollection.Count > 0)
                {
                    ErrorCollection = AssetCategoryBLL.ErrorCollection;
                }

                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            DBMethod.SaveEntity<AST_MAJOR_CATEGORY_MST>(AST_MAJOR_CATEGORY_MST);
                            break;
                        }
                    case FINAppConstants.Update:
                        {
                            AST_MAJOR_CATEGORY_MST.MAJOR_CATEGORY_ID = int.Parse(Master.RecordID.ToString());
                            DBMethod.SaveEntity<AST_MAJOR_CATEGORY_MST>(AST_MAJOR_CATEGORY_MST, true);
                            break;
                        }
                }

                AssignToGridBE();

                ClearGrid();

                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                else
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AssetCategoryEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void ClearGrid()
        {
            if (Session["dtGridData"] != null)
            {
                dtGridData = (DataTable)Session["dtGridData"];
            }
            dtGridData.Rows.Clear();
            gvData.DataSource = dtGridData;
            gvData.DataBind();
        }
        #endregion


        /// <summary>
        /// Grid Processes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        #region Grid Process for Minor Category
        private void SetVisibleDeleteAction()
        {
            try
            {
                ErrorCollection.Clear();

                for (int iLoop = 0; iLoop < gvData.Rows.Count; iLoop++)
                {
                    ImageButton ibtnDelete = gvData.Rows[iLoop].FindControl("ibtnDelete") as ImageButton;
                    if (ibtnDelete != null)
                    {
                        if (((Label)gvData.Rows[iLoop].FindControl("lblMAJORCATEGORID")).Text != null)
                        {
                            if (((Label)gvData.Rows[iLoop].FindControl("lblMAJORCATEGORID")).Text != string.Empty)
                            {

                                if (int.Parse(((Label)gvData.Rows[iLoop].FindControl("lblMAJORCATEGORID")).Text) > 0)
                                {
                                    gvData.Rows[iLoop].FindControl("ibtnDelete").Visible = false;
                                }
                                else
                                {
                                    gvData.Rows[iLoop].FindControl("ibtnDelete").Visible = true;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("JR_ROW_UP", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = -1;
                BindGrid(dtGridData);
                SetVisibleDeleteAction();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("JR_ROW_UP", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        public DataRow AssignGridControlToBE(DataRow drList, GridViewRow gvr)
        {
            if (Session[FINSessionConstants.GridData] != null)
            {
                dtGridData = (DataTable)Session[FINSessionConstants.GridData];
            }

            TextBox txtMinorCategory = gvr.FindControl("txtMinorCategory") as TextBox;
            TextBox txtMinorCategoryAr = gvr.FindControl("txtMinorCategoryAr") as TextBox;
            TextBox txtMinorCategoryDesc = gvr.FindControl("txtMinorCategoryDesc") as TextBox;
            CheckBox chkGridEnable = gvr.FindControl("chkGridEnable") as CheckBox;

            Label lblMAJORCATEGORID = gvr.FindControl("lblMAJORCATEGORID") as Label;
            Label lblMINORCATEGORID = gvr.FindControl("lblMINORCATEGORID") as Label;

            drList[FINColumnConstants.MINOR_CATEGORY_NAME] = txtMinorCategory.Text == string.Empty ? "0" : txtMinorCategory.Text;
            drList["MINOR_CATEGORY_NAME_OL"] = txtMinorCategory.Text == string.Empty ? "0" : txtMinorCategoryAr.Text;
            drList[FINColumnConstants.MINOR_CATEGORY_DESC] = txtMinorCategoryDesc.Text == string.Empty ? "0" : txtMinorCategoryDesc.Text;

            drList[FINColumnConstants.ENABLED_FLAG] = chkGridEnable.Checked == true ? "TRUE" : "FALSE";
            drList["MINOR_enabled_flag"] = chkGridEnable.Checked == true ? "TRUE" : "FALSE";

            if (int.Parse(Master.RecordID.ToString()) > 0)
            {
                AST_MAJOR_CATEGORY_MST.MAJOR_CATEGORY_ID = int.Parse(Master.RecordID.ToString());
            }
            else
            {
                AST_MAJOR_CATEGORY_MST.MAJOR_CATEGORY_ID = 0;
            }

            if (lblMINORCATEGORID.Text != string.Empty)
            {
                if (int.Parse(lblMINORCATEGORID.Text.ToString()) > 0)
                {
                    AST_MINOR_CATEGORY_DTL.MINOR_CATEGORY_ID = int.Parse(lblMINORCATEGORID.Text.ToString());
                }
                else
                {
                    AST_MINOR_CATEGORY_DTL.MINOR_CATEGORY_ID = 0;
                }
            }
            else
            {
                AST_MINOR_CATEGORY_DTL.MINOR_CATEGORY_ID = 0;
            }

            AST_MAJOR_CATEGORY_MST.CATEGORY_NAME = txtCategoryName.Text;
            AST_MINOR_CATEGORY_DTL.MINOR_CATEGORY_NAME = txtMinorCategory.Text;

            ErrorCollection.Clear();

            //Validation
            AssetCategoryBLL.AST_MAJOR_CATEGORY_MST = AST_MAJOR_CATEGORY_MST;
            AssetCategoryBLL.AST_MINOR_CATEGORY_DTL = AST_MINOR_CATEGORY_DTL;
            AssetCategoryBLL.Validate();
            AssetCategoryBLL.ValidateGrid(dtGridData, Master.Mode, gvr.RowIndex);

            slControls[0] = txtCategoryName;
            slControls[1] = txtMinorCategory;
            Dictionary<string, string> Prop_File_Data;
            Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/FA_" + Session["Sel_Lng"].ToString() + ".properties"));
            strCtrlTypes = "TextBox~TextBox";
            string strMessage = Prop_File_Data["Minor_Category_P"] + " ~ " + Prop_File_Data["Minor_Category_Description_P"] +  "";
            //strMessage = "Major Category Name ~ Minor Category Name";
            EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

            return drList;
        }
        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                DataRow drGridList = null;
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                if (e.CommandName.Equals("EmptyDataTemplateInsert"))
                {
                    gvr = gvData.Controls[0].Controls[1] as GridViewRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }
                else if (e.CommandName.Equals("FooterInsert"))
                {
                    gvr = gvData.FooterRow;
                    if (gvr == null)
                    {
                        return;
                    }
                }

                if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
                {

                    drGridList = dtGridData.NewRow();

                    drGridList[FINColumnConstants.MINOR_CATEGORY_ID] = 0;
                    drGridList[FINColumnConstants.MAJOR_CATEGORY_ID] = 0;

                    AssignGridControlToBE(drGridList, gvr);

                    if (EmptyErrorCollection.Count > 0)
                    {
                        ErrorCollection = EmptyErrorCollection;
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    else if (AssetCategoryBLL.ErrorCollection.Count > 0)
                    {
                        ErrorCollection = AssetCategoryBLL.ErrorCollection;
                        Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                        return;
                    }
                    else
                    {
                        dtGridData.Rows.Add(drGridList);
                        BindGrid(dtGridData);
                        SetVisibleDeleteAction();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("JR_ROW_UP", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void gvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GridViewRow gvr = gvData.Rows[e.RowIndex] as GridViewRow;
                DataRow drGridList = null;

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                if (gvr == null)
                {
                    return;
                }


                Label lblMAJORCATEGORID = gvr.FindControl("lblMAJORCATEGORID") as Label;
                Label lblMINORCATEGORID = gvr.FindControl("lblMINORCATEGORID") as Label;

                drGridList = dtGridData.Rows[e.RowIndex];

                drGridList[FINColumnConstants.MAJOR_CATEGORY_ID] = lblMAJORCATEGORID.Text == string.Empty ? "0" : lblMAJORCATEGORID.Text;
                drGridList[FINColumnConstants.MINOR_CATEGORY_ID] = lblMINORCATEGORID.Text == string.Empty ? "0" : lblMINORCATEGORID.Text;


                AssignGridControlToBE(drGridList, gvr);

                if (EmptyErrorCollection.Count > 0)
                {
                    ErrorCollection = EmptyErrorCollection;
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                else if (AssetCategoryBLL.ErrorCollection.Count > 0)
                {
                    ErrorCollection = AssetCategoryBLL.ErrorCollection;
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                    return;
                }
                else
                {
                    gvData.EditIndex = -1;
                    BindGrid(dtGridData);
                    SetVisibleDeleteAction();
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("JR_ROW_UP", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void gvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                if (e.Row.RowType == DataControlRowType.EmptyDataRow)
                {
                    GridViewRow gvr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                    gvData.Controls[0].Controls.AddAt(0, gvr);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("JR_ROW_UP", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void gvData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }

                DataRow drList = null;
                drList = dtGridData.Rows[e.RowIndex];
                drList["DELETED"] = "Y";

                BindGrid(dtGridData);
                SetVisibleDeleteAction();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("JR_ROW_UP", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void gvData_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                gvData.EditIndex = e.NewEditIndex;
                BindGrid(dtGridData);
                SetVisibleDeleteAction();

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("JR_ROW_UP", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (((DataRowView)e.Row.DataItem).Row["DELETED"].ToString() == "Y")
                    {
                        e.Row.Visible = false;
                    }
                }
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (headerRowVisible)
                        e.Row.Visible = false;
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("JR_ROW_UP", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        #endregion

        /// <summary>
        /// Confirm button for delete the particular records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        #region Delete the records
        protected void btnYes_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    AST_MAJOR_CATEGORY_MST = (AST_MAJOR_CATEGORY_MST)EntityData;
                }

                //delete the detail records
                AST_MINOR_CATEGORY_DTL.MAJOR_CATEGORY_ID = short.Parse(Master.RecordID.ToString());
                DBMethod.DeleteEntity<AST_MINOR_CATEGORY_DTL>(AST_MINOR_CATEGORY_DTL);

                //delete the master records
                AST_MAJOR_CATEGORY_MST.MAJOR_CATEGORY_ID = short.Parse(Master.RecordID.ToString());
                DBMethod.DeleteEntity<AST_MAJOR_CATEGORY_MST>(AST_MAJOR_CATEGORY_MST);

                //DisplayDeleteCompleteMessage(Master.ListPageToOpen);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AssetCateEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        /// <summary>
        /// Search button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void ibtnGet_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("AssetCategoryList.aspx", false);
        }


    }
}