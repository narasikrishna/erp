﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/FINMaster.Master" AutoEventWireup="true"
    CodeBehind="AssetDepreciationDetailEntry.aspx.cs" Inherits="FIN.Client.FA.AssetDepreciationDetailEntry" %>

<%@ MasterType VirtualPath="~/MasterPage/FINMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="FINContent" runat="server">
    <div class="divFormcontainer" style="width: 900px" id="divMainContainer">
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" id="div2" runat="server">
            <div class="lblBox  LNOrient" style="width: 150px" id="Div3">
                Category
            </div>
            <div class="divtxtBox  LNOrient" style="width: 155px">
                <asp:DropDownList ID="ddlCategory" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    TabIndex="1">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" id="divFinancialYear" runat="server">
            <div class="lblBox  LNOrient" style="width: 150px" id="lblFinancialYear">
                Financial Year
            </div>
            <div class="divtxtBox  LNOrient" style="width: 155px">
                <asp:DropDownList ID="ddlFinancialYear" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    TabIndex="2">
                </asp:DropDownList>
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <div class="lblBox  LNOrient" style="width: 150px" id="lblDate">
                Date
            </div>
            <div class="divtxtBox  LNOrient" style="width: 155px">
                <asp:TextBox OnTextChanged="txtDate_TextChanged1" AutoPostBack="true" ID="txtDate"
                    runat="server" CssClass="validate[required] validate[custom[ReqDateDDMMYYY]] RequiredField txtBox"
                    TabIndex="3"></asp:TextBox>
                <cc2:CalendarExtender runat="server" Format="dd/MM/yyyy" ID="calExtenader1" TargetControlID="txtDate" />
            </div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" id="divPeriods" runat="server">
            <div class="lblBox  LNOrient" style="width: 150px" id="lblPeriods">
                Periods
            </div>
            <div class="divtxtBox  LNOrient" style="width: 155px">
                <asp:DropDownList ID="ddlPeriods" runat="server" CssClass="validate[required] RequiredField ddlStype"
                    TabIndex="4">
                </asp:DropDownList>
            </div>
            <div class="colspace  LNOrient">
                &nbsp</div>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer" align="center">
            <asp:ImageButton ID="btnGet" runat="server" ImageUrl="~/Images/btnGet.png" OnClick="btnGet_Click"
                TabIndex="8" Style="border: 0px;" />
            <%-- <asp:Button ID="btnGet" runat="server" TabIndex="8" Text="Get" CssClass="btn" Width="100px"
                OnClick="btnGet_Click" />--%>
            &nbsp; &nbsp; &nbsp;
            <asp:ImageButton ID="btnPrint" runat="server" ImageUrl="~/Images/Print.png" OnClick="btnPrint_Click"
                Visible="false" Style="border: 0px;" />
            <asp:ImageButton ID="imgBtnPost" runat="server" ImageUrl="~/Images/Post.png" OnClick="imgBtnPost_Click"
                Style="border: 0px" />
            <asp:ImageButton ID="imgBtnJVPrint" runat="server" ImageUrl="~/Images/jvPrint.png"
                Style="border: 0px" OnClick="imgBtnJVPrint_Click" />
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer " align="left">
            <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                Width="900px" DataKeyNames="ASSET_DEPRECIATION_ID,asset_mst_id" OnRowCancelingEdit="gvData_RowCancelingEdit"
                OnRowCommand="gvData_RowCommand" ShowFooter="false" ShowHeaderWhenEmpty="true">
                <Columns>
                    <asp:TemplateField HeaderText="Category Name">
                        <ItemTemplate>
                            <asp:Label ID="lblCategoryName" Text='<%# Eval("category_name") %>' CssClass="Label_Style"
                                runat="server"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Asset Number">
                        <ItemTemplate>
                            <asp:Label ID="lblAssetNumber" Text='<%# Eval("asset_number") %>' CssClass="Label_Style"
                                runat="server"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Asset Name">
                        <ItemTemplate>
                            <asp:Label ID="lblAssetName" Text='<%# Eval("asset_name") %>' CssClass="Label_Style"
                                runat="server"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Asset Cost">
                        <ItemTemplate>
                            <asp:Label ID="lblAssetCost" Text='<%# Eval("asset_cost") %>' CssClass="Label_Style"
                                runat="server"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" HorizontalAlign="right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Depreciation Amount">
                        <ItemTemplate>
                            <asp:TextBox ID="txtGLAmount" Text='<%#  Eval("depre_amt") %>' CssClass="validate[required] RequiredField txtBox_N"
                                MaxLength="13" TabIndex="6" runat="server" AutoPostBack="True" OnTextChanged="txtGLAmount_TextChanged"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="FilteredTextBasdoxExtendaer2" runat="server" FilterType="Numbers,Custom"
                                TargetControlID="txtGLAmount" ValidChars="." />
                        </ItemTemplate>
                        <ItemStyle CssClass="adminFormFieldData" Width="250px" HorizontalAlign="Left" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GrdAltRow" />
            </asp:GridView>
        </div>
        <div class="divClear_10">
        </div>
        <div class="divRowContainer">
            <table>
                <tr>
                    <td>
                       <div align="right" class="lblBox OppLNOrient" style="width: 620px" id="Div1">
                            Total
                        </div>
                    </td>
                    <td>
                        <div class="divtxtBox LNOrient" style="width: 250px">
                            <asp:TextBox ID="txtTotalAmt" runat="server" CssClass="txtBox_N" Enabled="false"
                                TabIndex="2"></asp:TextBox>
                        </div>
                    </td>
                </tr>
                <%--<div class="divtxtBox  OppLNOrient" style="width: 155px">
                <asp:TextBox ID="txtTotalAmt" runat="server" CssClass="txtBox_N" Enabled="false"
                    TabIndex="2"></asp:TextBox>
            </div>
            <div class="lblBox  OppLNOrient" style="width: 150px" id="Div1">
                Total
            </div>--%>
            </table>
        </div>
        <div class="divRowContainer divAction">
            <table class="SaveTable">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" TabIndex="8" Text="Save" OnClick="btnSave_Click"
                            CssClass="btn" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn" TabIndex="9" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" TabIndex="10" />
                    </td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" TabIndex="11" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divDelete">
            <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnDelete"
                PopupControlID="pnlConfirm" CancelControlID="btNo" BackgroundCssClass="ConfirmBackground">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server">
                <div class="ConfirmForm">
                    <table width="100%">
                        <tr class="ConfirmHeading" style="width: 100%">
                            <td>
                                <asp:Label ID="lblConfirm" runat="server" Text="Are you sure to delete this record"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button runat="server" OnClick="btnYes_Click" CssClass="button" ID="btnYes" Text="Yes"
                                    Width="60px" />
                                &nbsp;
                                <asp:Button runat="server" CssClass="button" ID="btNo" Text="No" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FINBottom" runat="server">
    <script src="../LanguageScript/FA/FAChangeLang.js" type="text/javascript"></script>
    <script type="text/javascript">


        $(document).ready(function () {
            $("#form1").validationEngine();
            return fn_SaveValidation();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            return fn_SaveValidation();
        });

        function fn_SaveValidation() {
            $("#FINContent_btnSave").click(function (e) {
                //e.preventDefault();
                return $("#form1").validationEngine('validate')
            })
        }

        $(document).ready(function () {
            fn_changeLng('<%= Session["Sel_Lng"] %>');
        });
    </script>
</asp:Content>
