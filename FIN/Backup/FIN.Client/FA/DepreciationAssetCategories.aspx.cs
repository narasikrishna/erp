﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Linq;
using System.Collections.Generic;
using FIN.DAL;
using FIN.BLL;
using FIN.DAL.FA;
using FIN.BLL.FA;

using VMVServices.Web;

namespace FIN.Client.FA
{
    public partial class DepreciationAssetCategories : PageBase
    {
        /// <summary>
        /// Creted by Jeyarani
        /// Master details for asset's major and minor categories...
        /// </summary>

        AST_DPRN_CATEGORY_CONFIG aST_DPRN_CATEGORY_CONFIG = new AST_DPRN_CATEGORY_CONFIG();


        string screenMode = string.Empty;
        DataSet dsData = new DataSet();


        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                //btnSave.Visible = false;
                //btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                //btnSave.Visible = true;
                //btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                //  btnSave.Text = "Update";
            }
            UserRightsChecking();

        }
        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    //btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    // btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    // btnDelete.Visible = false;
                }
            }
        }
        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();


                Startup();

                EntityData = null;
                Session[FINSessionConstants.GridData] = null;

                FillComboBox();
                if (Master.Mode != null)
                {

                    if (Master.Mode != FINAppConstants.Add && Master.RecordID > 0)
                    {

                        using (IRepository<AST_DPRN_CATEGORY_CONFIG> userCtx = new DataRepository<AST_DPRN_CATEGORY_CONFIG>())
                        {
                            aST_DPRN_CATEGORY_CONFIG = userCtx.Find(r =>
                                (r.DPRN_CATEGORY_ID == Master.RecordID)
                                ).SingleOrDefault();
                        }

                        EntityData = aST_DPRN_CATEGORY_CONFIG;

                        ddlMajorcategoryid.SelectedValue = aST_DPRN_CATEGORY_CONFIG.MAJOR_CATEGORY_ID.ToString();
                        ddlDepreciationMethodid.SelectedValue = aST_DPRN_CATEGORY_CONFIG.DPRN_METHOD_MST_ID.ToString();

                        if (aST_DPRN_CATEGORY_CONFIG.EFFECT_FROM != null)
                        {
                            dtpEffectFromdate.Text = DBMethod.ConvertDateToString(aST_DPRN_CATEGORY_CONFIG.EFFECT_FROM.ToString());
                        }
                    }


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }



        #region Insert ,Update and Delete the records

        private void AssignToBE()
        {
            try
            {
                ErrorCollection.Clear();
                if (EntityData != null)
                {
                    aST_DPRN_CATEGORY_CONFIG = (AST_DPRN_CATEGORY_CONFIG)EntityData;
                }

                aST_DPRN_CATEGORY_CONFIG.ORGANIZATION_MASTER_ID = VMVServices.Web.Utils.OrganizationID.ToString();
                // aST_DPRN_CATEGORY_CONFIG.CALENDAR_ID = VMVServices.Web.Utils.CalendarID.ToString() == null ? 0 : VMVServices.Web.Utils.CalendarID;

                aST_DPRN_CATEGORY_CONFIG.MAJOR_CATEGORY_ID = int.Parse(ddlMajorcategoryid.Text.ToString());
                aST_DPRN_CATEGORY_CONFIG.DPRN_METHOD_MST_ID = int.Parse(ddlDepreciationMethodid.Text.ToString());

                if (dtpEffectFromdate.Text != null && dtpEffectFromdate.Text != string.Empty)
                {
                    aST_DPRN_CATEGORY_CONFIG.EFFECT_FROM = DBMethod.ConvertStringToDate(dtpEffectFromdate.Text);
                }
                aST_DPRN_CATEGORY_CONFIG.ENABLED_FLAG = "1";

                if (Master.Mode != null)
                {
                    if (Master.Mode != FINAppConstants.Add && Master.RecordID > 0)
                    {

                        aST_DPRN_CATEGORY_CONFIG.MODIFIED_BY = this.LoggedUserName;
                        aST_DPRN_CATEGORY_CONFIG.MODIFIED_DATE = DateTime.Today;
                    }
                    else
                    {
                        aST_DPRN_CATEGORY_CONFIG.CREATE_BY = this.LoggedUserName;
                        aST_DPRN_CATEGORY_CONFIG.CREATED_DATE = DateTime.Today;
                        aST_DPRN_CATEGORY_CONFIG.DPRN_CATEGORY_ID = DBMethod.GetPrimaryKeyValue(FINSequenceConstant.AST_DPRN_CATEGORY_CONFIG_SEQ);

                    }
                }
                aST_DPRN_CATEGORY_CONFIG.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, aST_DPRN_CATEGORY_CONFIG.DPRN_CATEGORY_ID.ToString());
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();

                SortedList slControls = new SortedList();
                string strCtrlTypes = string.Empty;
                string strMessage = string.Empty;

                slControls[0] = ddlMajorcategoryid;
                slControls[1] = ddlDepreciationMethodid;
                slControls[2] = dtpEffectFromdate;
                Dictionary<string, string> Prop_File_Data;
                Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/FA_" + Session["Sel_Lng"].ToString() + ".properties"));

                strCtrlTypes = "DropDownList~DropDownList~TextBox";
                strMessage = Prop_File_Data["Major_Category_P"] + " ~ " + Prop_File_Data["Depreciation_Method_P"] + " ~ " + Prop_File_Data["Effective_From_P"] + "";
               // strMessage = "Asset Category ~ Depreciation Method ~ Effective Date";
                ErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                ErrorCollection.Clear();


                ErrorCollection = DepreciationAssetCategoriesBLL.Validate(long.Parse(ddlMajorcategoryid.SelectedValue.ToString()), long.Parse(ddlDepreciationMethodid.SelectedValue.ToString()), DBMethod.ConvertStringToDate(dtpEffectFromdate.Text));


                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                AssignToBE();

                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            DBMethod.SaveEntity<AST_DPRN_CATEGORY_CONFIG>(aST_DPRN_CATEGORY_CONFIG);
                            //DisplaySaveCompleteMessage(Master.ListPageToOpen);
                            break;
                        }
                    case FINAppConstants.Update:
                        {
                            aST_DPRN_CATEGORY_CONFIG.DPRN_CATEGORY_ID = int.Parse(Master.RecordID.ToString());
                            DBMethod.SaveEntity<AST_DPRN_CATEGORY_CONFIG>(aST_DPRN_CATEGORY_CONFIG, true);
                            //  DisplaySaveCompleteMessage(Master.ListPageToOpen);
                            break;

                        }
                    case FINAppConstants.Delete:
                        {
                            aST_DPRN_CATEGORY_CONFIG.DPRN_CATEGORY_ID = int.Parse(Master.RecordID.ToString());
                            DBMethod.DeleteEntity<AST_DPRN_CATEGORY_CONFIG>(aST_DPRN_CATEGORY_CONFIG);
                            // DisplayDeleteCompleteMessage(Master.ListPageToOpen);
                            break;
                        }
                }

                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                else
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DepreciationAssetCategories", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        #endregion
        private void FillComboBox()
        {
            try
            {
                ErrorCollection.Clear();

                using (IRepository<AST_MAJOR_CATEGORY_MST> userCtx = new DataRepository<AST_MAJOR_CATEGORY_MST>())
                {
                    ddlMajorcategoryid.AppendDataBoundItems = true;
                    ddlMajorcategoryid.Items.Clear();
                    ddlMajorcategoryid.Items.Add(new ListItem(FINAppConstants.intialRowTextField, FINAppConstants.intialRowValueField));

                    ddlMajorcategoryid.DataTextField = FINColumnConstants.CATEGORY_NAME;
                    ddlMajorcategoryid.DataValueField = FINColumnConstants.MAJOR_CATEGORY_ID;
                    ddlMajorcategoryid.DataSource = userCtx.Find(x => x.ENABLED_FLAG == "1" && x.WORKFLOW_COMPLETION_STATUS == "1");
                    ddlMajorcategoryid.DataBind();


                }

                using (IRepository<AST_DEPRECIATION_METHOD_MST> userCtx = new DataRepository<AST_DEPRECIATION_METHOD_MST>())
                {
                    ddlDepreciationMethodid.AppendDataBoundItems = true;
                    ddlDepreciationMethodid.Items.Clear();
                    ddlDepreciationMethodid.Items.Add(new ListItem(FINAppConstants.intialRowTextField, FINAppConstants.intialRowValueField));

                    ddlDepreciationMethodid.DataTextField = FINColumnConstants.DPRN_NAME;
                    ddlDepreciationMethodid.DataValueField = FINColumnConstants.DPRN_METHOD_MST_ID;
                    ddlDepreciationMethodid.DataSource = userCtx.Find(x => x.ENABLED_FLAG == "1" && x.WORKFLOW_COMPLETION_STATUS == "1");
                    ddlDepreciationMethodid.DataBind();
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void btnYes_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                aST_DPRN_CATEGORY_CONFIG.DPRN_CATEGORY_ID = short.Parse(Master.RecordID.ToString());
                DBMethod.DeleteEntity<AST_DPRN_CATEGORY_CONFIG>(aST_DPRN_CATEGORY_CONFIG);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("DepreciationAssetCategories", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void ibtnGet_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("DepreciationAssetCategoriesList.aspx", false);
        }


    }
}