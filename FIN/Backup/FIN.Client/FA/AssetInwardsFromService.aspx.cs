﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Linq;
using FIN.BLL.GL;
using FIN.DAL;
using FIN.BLL;
using FIN.DAL.FA;
using FIN.BLL.FA;
using FIN.BLL.AP;
using VMVServices.Web;

namespace FIN.Client.FA
{
    public partial class AssetInwardsFromService : PageBase
    {
        AST_ASSET_INWARD_SERVICE aST_ASSET_INWARD_SERVICE = new AST_ASSET_INWARD_SERVICE();
        string screenMode = string.Empty;
        DataTable dtData = new DataTable();
        SortedList slControls = new SortedList();
        string strCtrlTypes = string.Empty;
        string strMessage = string.Empty;
        AssetInwardFromServiceBLL assetInwardFromServiceBLL = new AssetInwardFromServiceBLL();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                //btnSave.Visible = false;
                //btnDelete.Visible = true;
                //  pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                //btnSave.Visible = true;
                //btnDelete.Visible = false;
                // pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                // btnSave.Text = "Update";
            }
            UserRightsChecking();

        }
        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    // btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    // btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    // btnDelete.Visible = false;
                }
            }
        }

        private void AssignToControl()
        {

            try
            {
                ErrorCollection.Clear();

                Startup();
                BindBaseCurrency();
                BindThirdPartyDetails();
                BindAsset();
                BindCurrency();
                BindExchangeRateTypeData();

                if (Master.Mode != null)
                {

                    EntityData = null;
                    if (Master.Mode != FINAppConstants.Add && Master.RecordID > 0)
                    {
                        using (IRepository<AST_ASSET_INWARD_SERVICE> userCtx = new DataRepository<AST_ASSET_INWARD_SERVICE>())
                        {
                            aST_ASSET_INWARD_SERVICE = userCtx.Find(r =>
                                (r.INWARD_SERVICE_ID == Master.RecordID)
                                ).SingleOrDefault();
                        }

                        EntityData = aST_ASSET_INWARD_SERVICE;

                        if (aST_ASSET_INWARD_SERVICE.ASSET_MST_ID != null)
                        {
                            ddlAsset.SelectedValue = aST_ASSET_INWARD_SERVICE.ASSET_MST_ID.ToString();
                        }


                        BindThirdPartyDetails();
                        if (aST_ASSET_INWARD_SERVICE.OUTWARD_SERVICE_ID != null)
                        {
                            ddlThirdParty.SelectedValue = aST_ASSET_INWARD_SERVICE.OUTWARD_SERVICE_ID.ToString();
                        }
                      
                        if (aST_ASSET_INWARD_SERVICE.INWARD_DATE != null)
                        {
                            dtpInwardDate.Text = DBMethod.ConvertDateToString(aST_ASSET_INWARD_SERVICE.INWARD_DATE.ToString());
                        }
                        ntxtServiceCost.Text = aST_ASSET_INWARD_SERVICE.SERVICE_COST.ToString();
                        if (aST_ASSET_INWARD_SERVICE.CURRENCY_ID != null)
                        {
                            ddlCurrency.SelectedValue = aST_ASSET_INWARD_SERVICE.CURRENCY_ID.ToString();
                        }
                        if (aST_ASSET_INWARD_SERVICE.EXCHANGE_TYPE_ID != null)
                        {
                            ddlExchangeRateType.SelectedValue = aST_ASSET_INWARD_SERVICE.EXCHANGE_TYPE_ID.ToString();
                        }
                        if (aST_ASSET_INWARD_SERVICE.EXCHANGE_RATE_DATE != null)
                        {
                            dtpExchangeRateDate.Text = DBMethod.ConvertDateToString(aST_ASSET_INWARD_SERVICE.EXCHANGE_RATE_DATE.ToString());
                        }
                        if (aST_ASSET_INWARD_SERVICE.BASE_CURRENCY_ID != null)
                        {
                            ddlBaseCurrency.SelectedValue = aST_ASSET_INWARD_SERVICE.BASE_CURRENCY_ID.ToString();
                        }
                        txtPaymentReference.Text = aST_ASSET_INWARD_SERVICE.PAYMENT_REFERENCE;
                        if (aST_ASSET_INWARD_SERVICE.BASE_CURRENCY_ID != null)
                        {
                            ntxtExchangeRate.Text = aST_ASSET_INWARD_SERVICE.EXCHANGE_RATE.ToString();
                        }
                    }

                }


            }
            catch (Exception ex)
            {
                ErrorCollection.Remove("AssetInwardsValidation");
                ErrorCollection.Add("AssetInwardsValidation", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        #region Insert ,Update and Delete the records

        private void AssignToBE()
        {
            ErrorCollection.Clear();


            if (EntityData != null)
            {
                aST_ASSET_INWARD_SERVICE = (AST_ASSET_INWARD_SERVICE)EntityData;
            }



            aST_ASSET_INWARD_SERVICE.ORGANIZATION_MASTER_ID = VMVServices.Web.Utils.OrganizationID.ToString();
            //  aST_ASSET_INWARD_SERVICE.CALENDAR_ID = VMVServices.Web.Utils.CalendarID.ToString() == null ? 0 : VMVServices.Web.Utils.CalendarID;

            //aST_ASSET_INWARD_SERVICE.THIRD_PARTY_ID = ddlThirdParty.SelectedValue.ToString();
            aST_ASSET_INWARD_SERVICE.OUTWARD_SERVICE_ID = CommonUtils.ConvertStringToInt(ddlThirdParty.SelectedValue.ToString());
            aST_ASSET_INWARD_SERVICE.ASSET_MST_ID = int.Parse(ddlAsset.SelectedValue.ToString());
            aST_ASSET_INWARD_SERVICE.INWARD_DATE = DBMethod.ConvertStringToDate(dtpInwardDate.Text);
            aST_ASSET_INWARD_SERVICE.SERVICE_COST = CommonUtils.ConvertStringToDecimal(ntxtServiceCost.Text.ToString());
            aST_ASSET_INWARD_SERVICE.CURRENCY_ID = (ddlCurrency.SelectedValue.ToString());
            aST_ASSET_INWARD_SERVICE.EXCHANGE_RATE = CommonUtils.ConvertStringToDecimal(ntxtExchangeRate.Text == string.Empty ? "0" : ntxtExchangeRate.Text.ToString());
            aST_ASSET_INWARD_SERVICE.EXCHANGE_TYPE_ID = (ddlExchangeRateType.SelectedValue == string.Empty ? "0" : ddlExchangeRateType.SelectedValue.ToString());
            aST_ASSET_INWARD_SERVICE.BASE_CURRENCY_ID = (ddlBaseCurrency.SelectedValue == string.Empty ? "0" : ddlBaseCurrency.SelectedValue.ToString());
            if (dtpExchangeRateDate.Text != null && dtpExchangeRateDate.Text != string.Empty)
            {
                aST_ASSET_INWARD_SERVICE.EXCHANGE_RATE_DATE = DBMethod.ConvertStringToDate(dtpExchangeRateDate.Text);
            }
            aST_ASSET_INWARD_SERVICE.PAYMENT_REFERENCE = txtPaymentReference.Text;
            aST_ASSET_INWARD_SERVICE.ENABLED_FLAG = "1";

            if (Master.Mode != null)
            {
                if (Master.Mode != FINAppConstants.Add && Master.RecordID > 0)
                {

                    aST_ASSET_INWARD_SERVICE.MODIFIED_BY = this.LoggedUserName;
                    aST_ASSET_INWARD_SERVICE.MODIFIED_DATE = DateTime.Today;
                }
                else
                {
                    aST_ASSET_INWARD_SERVICE.CREATE_BY = this.LoggedUserName;
                    aST_ASSET_INWARD_SERVICE.CREATED_DATE = DateTime.Today;
                    aST_ASSET_INWARD_SERVICE.INWARD_SERVICE_ID = DBMethod.GetPrimaryKeyValue(FINSequenceConstant.AST_DPRN_CATEGORY_CONFIG_SEQ);


                }
                aST_ASSET_INWARD_SERVICE.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, aST_ASSET_INWARD_SERVICE.INWARD_SERVICE_ID.ToString());
            }
        }

        private void EmptyValidation()
        {
            try
            {
                slControls[0] = ddlThirdParty;
                slControls[1] = ddlAsset;
                slControls[2] = dtpInwardDate;
                slControls[3] = ntxtServiceCost;
                slControls[4] = ddlCurrency;
                slControls[5] = txtPaymentReference;
                Dictionary<string, string> Prop_File_Data;
                Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/FA_" + Session["Sel_Lng"].ToString() + ".properties"));
                strCtrlTypes = "DropDownList~DropDownList~TextBox~TextBox~DropDownList~TextBox";
                string strMessage = Prop_File_Data["Third_Party_P"] + " ~ " + Prop_File_Data["Asset_P"] + " ~ " + Prop_File_Data["Inward_Date_P"] + " ~ " + Prop_File_Data["Service_Cost_P"] + " ~ " + Prop_File_Data["Currency_P"] + " ~ " + Prop_File_Data["Payment_Reference_P"] + "";
                //strMessage = "Third Party ~ Asset ~Inward Date ~Service Cost ~Currency ~Payment Reference";

                EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

                if (EmptyErrorCollection.Count > 0)
                {
                    ErrorCollection = EmptyErrorCollection;
                    return;
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Remove("AssetInwardsValidation");
                ErrorCollection.Add("AssetInwardsValidation", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();
                EmptyErrorCollection.Clear();
                EmptyValidation();

                if (EmptyErrorCollection.Count > 0)
                {
                    return;
                }

                AssetInwardFromServiceBLL.Validate(long.Parse(ddlAsset.SelectedValue.ToString()), DBMethod.ConvertStringToDate(dtpInwardDate.Text));

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                AssignToBE();

                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            DBMethod.SaveEntity<AST_ASSET_INWARD_SERVICE>(aST_ASSET_INWARD_SERVICE);
                            //DisplaySaveCompleteMessage(Master.ListPageToOpen);
                            break;
                        }
                    case FINAppConstants.Update:
                        {
                            aST_ASSET_INWARD_SERVICE.INWARD_SERVICE_ID = int.Parse(Master.RecordID.ToString());
                            DBMethod.SaveEntity<AST_ASSET_INWARD_SERVICE>(aST_ASSET_INWARD_SERVICE, true);
                            //  DisplaySaveCompleteMessage(Master.ListPageToOpen);
                            break;

                        }
                    case FINAppConstants.Delete:
                        {
                            aST_ASSET_INWARD_SERVICE.INWARD_SERVICE_ID = int.Parse(Master.RecordID.ToString());
                            DBMethod.DeleteEntity<AST_ASSET_INWARD_SERVICE>(aST_ASSET_INWARD_SERVICE);
                            // DisplayDeleteCompleteMessage(Master.ListPageToOpen);
                            break;
                        }
                }


                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                else
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AssetInwardsFromService", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        #endregion

        # region FillComboBox

        private void BindBaseCurrency()
        {
            Currency_BLL.getCurrencyDesc(ref ddlBaseCurrency);
        }

        private void BindThirdPartyDetails()
        {
            dtData = DBMethod.ExecuteQuery(FA_SQL.BindThirdParty(ddlAsset.SelectedValue.ToString())).Tables[0];
            if (dtData != null)
            {
                if (dtData.Rows.Count > 0)
                {
                    CommonUtils.LoadDropDownList(ddlThirdParty, FINColumnConstants.VENDOR_NAME, FINColumnConstants.VENDOR_ID, dtData, true, false);
                }
            }

            //      Supplier_BLL.GetSupplierName(ref ddlThirdParty);


        }

        private void BindAsset()
        {
            dtData = DBMethod.ExecuteQuery(FA_SQL.BindOutWardAssets()).Tables[0];
            if (dtData != null)
            {
                if (dtData.Rows.Count > 0)
                {
                    CommonUtils.LoadDropDownList(ddlAsset, FINColumnConstants.ASSET_NAME, FINColumnConstants.ASSET_MST_ID, dtData, true, false);
                }
            }
        }

        private void BindCurrency()
        {
            Currency_BLL.getCurrencyDesc(ref ddlCurrency);
        }

        private void BindExchangeRateTypeData()
        {
            Lookup_BLL.GetLookUpValues(ref ddlExchangeRateType, "ET");
        }

        # endregion FillComboBox

        protected void ibtnGet_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("AssetInwardsFromServiceList.aspx", false);
        }

        protected void btnYes_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                aST_ASSET_INWARD_SERVICE.INWARD_SERVICE_ID = short.Parse(Master.RecordID.ToString());
                DBMethod.DeleteEntity<AST_ASSET_INWARD_SERVICE>(aST_ASSET_INWARD_SERVICE);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AssetInwardsFromService", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                }
            }

        }


        protected void ddlCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                if (Currency_BLL.IsBaseCurrency(ddlCurrency.SelectedValue.ToString()))
                {
                    ddlExchangeRateType.Enabled = false;
                    ntxtExchangeRate.Enabled = false;
                    dtpExchangeRateDate.Enabled = false;
                    //   ddlBaseCurrecny.SelectedValue = ddlCurrency.SelectedValue;
                    //ddlBaseCurrecny.Enabled = false;
                    ntxtExchangeRate.Text = "1";
                }
                else
                {
                    ddlExchangeRateType.Enabled = true;
                    ntxtExchangeRate.Enabled = true;
                    dtpExchangeRateDate.Enabled = true;
                    // ddlBaseCurrecny.Enabled = true;
                    ntxtExchangeRate.Text = string.Empty;

                    GetExchangeRate();
                }


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AssetEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        private void GetExchangeRate()
        {
            DataTable dtData = new DataTable();

            if (ddlCurrency.SelectedValue != string.Empty & dtpExchangeRateDate.Text != string.Empty)
            {
                dtData = ExchangeRate_BLL.getStandardRate(ddlCurrency.SelectedValue, (dtpExchangeRateDate.Text));
                if (dtData != null)
                {
                    if (dtData.Rows.Count > 0)
                    {
                        ntxtExchangeRate.Text = dtData.Rows[0]["currency_std_rate"].ToString();
                    }
                }
            }
        }
        protected void dtpExchangeDate_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                GetExchangeRate();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AssetEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void ddlAsset_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                BindThirdPartyDetails();
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("AssetEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
    }
}