﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Linq;
using System.Collections.Generic;
using FIN.DAL;
using FIN.BLL;
using FIN.DAL.FA;
using FIN.BLL.FA;

using VMVServices.Web;

namespace FIN.Client.FA
{
    public partial class AssetTransferEntry : PageBase
    {
        string screenMode = string.Empty;
        DataSet dsData = new DataSet();
        DataTable dtData = new DataTable();
        AST_ASSET_LOCATION_DTL AST_ASSET_LOCATION_DTL = new AST_ASSET_LOCATION_DTL();
        SortedList slControls = new SortedList();
        string strCtrlTypes = string.Empty;
        string strMessage = string.Empty;


        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                   
                    AssignToControl();
                    ChangeLanguage();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                //btnSave.Visible = false;
                //btnDelete.Visible = true;
                //pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                //btnSave.Visible = true;
                //btnDelete.Visible = false;
                //pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                // btnSave.Text = "Update";
            }
            UserRightsChecking();

        }
        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    // btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    // btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    // btnDelete.Visible = false;
                }
            }
        }
        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();

                EntityData = null;


                BindAsset();
                BindCurrentLocation();
                divEnd.Visible = false;
                divEnd1.Visible = false;
                if (Master.Mode != FINAppConstants.Add && Master.RecordID > 0)
                {

                    using (IRepository<AST_ASSET_LOCATION_DTL> userCtx = new DataRepository<AST_ASSET_LOCATION_DTL>())
                    {
                        AST_ASSET_LOCATION_DTL = userCtx.Find(r =>
                            (r.ASSET_LOCATION_DTL_ID == Master.RecordID)
                            ).SingleOrDefault();
                    }

                    EntityData = AST_ASSET_LOCATION_DTL;
                    divEnd.Visible = true;
                    divEnd1.Visible = true;
                    ddlAsset.Enabled = false;
                    ddlCurrentLocation.Enabled = false;
                    txtDepartmentDtl.Enabled = false;
                    txtLocationIncharge.Enabled = false;
                    currentLocdiv.Visible = false;
                    PrevLocdiv.Visible = true;

                    ddlAsset.SelectedValue = AST_ASSET_LOCATION_DTL.ASSET_MST_ID.ToString();
                    ddlCurrentLocation.SelectedValue = AST_ASSET_LOCATION_DTL.CURRENT_LOCATION_ID.ToString();

                    dtData = DBMethod.ExecuteQuery(FA_SQL.GetAssetLocation(long.Parse(AST_ASSET_LOCATION_DTL.PREVIOUS_LOCATION_ID.ToString()))).Tables[0];
                    if (dtData != null)
                    {
                        if (dtData.Rows.Count > 0)
                        {
                            lblPreviousLocation.Text = dtData.Rows[0]["LOCATION_NAME"].ToString();
                        }
                    }

                    txtLocationIncharge.Text = AST_ASSET_LOCATION_DTL.LOCATION_INCHARGE;
                    txtDepartmentDtl.Text = AST_ASSET_LOCATION_DTL.DEPARTMENT_DTL;
                    if (AST_ASSET_LOCATION_DTL.EFFECT_FROM != null)
                    {
                        dtpEffectiveDate.Text = DBMethod.ConvertDateToString(AST_ASSET_LOCATION_DTL.EFFECT_FROM.ToString());
                    }
                    if (AST_ASSET_LOCATION_DTL.END_DATE != null)
                    {
                        dtpEndDate.Text = DBMethod.ConvertDateToString(AST_ASSET_LOCATION_DTL.END_DATE.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Remove("AssetInwardsValidation");
                ErrorCollection.Add("AssetInwardsValidation", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void ChangeLanguage()
        {
            try
            {
                ErrorCollection.Clear();
                if (VMVServices.Web.Utils.LanguageCode.ToString().Length > 0)
                {
                    Dictionary<string, string> Prop_File_Data;
                    Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/FA_" + Session["Sel_Lng"].ToString() + ".properties"));
                    currentLocdiv.InnerText = Prop_File_Data["Current_Location_P"];
                    PrevLocdiv.InnerText = Prop_File_Data["Previous_Location_P"];
                    divEnd.InnerText = Prop_File_Data["End_Date_P"];
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("MisReceiptChangeLang", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        #region Fill Dropdownlist

        private void BindAsset()
        {

            dtData = DBMethod.ExecuteQuery(FA_SQL.GetAssetDetails()).Tables[0];
            if (dtData != null)
            {
                if (dtData.Rows.Count > 0)
                {
                    CommonUtils.LoadDropDownList(ddlAsset, FINColumnConstants.ASSET_NAME, FINColumnConstants.ASSET_MST_ID, dtData, true, false);
                }
            }
        }
        private void BindCurrentLocation()
        {
            using (IRepository<AST_LOCATION_MST> userCtx = new DataRepository<AST_LOCATION_MST>())
            {
                dtData = DataSetLinqOperators.CopyToDataTable<AST_LOCATION_MST>(userCtx.Find(r => (r.ENABLED_FLAG == "1") && (r.ORGANIZATION_MASTER_ID == VMVServices.Web.Utils.OrganizationID)));
            }
            if (dtData != null)
            {
                if (dtData.Rows.Count > 0)
                {
                    CommonUtils.LoadDropDownList(ddlCurrentLocation, FINColumnConstants.LOCATION_NAME, FINColumnConstants.ASSET_LOCATION_ID, dtData, true, false);
                }
            }
        }
        private void BindPreviousLocation()
        {
            using (IRepository<AST_LOCATION_MST> userCtx = new DataRepository<AST_LOCATION_MST>())
            {
                dtData = DataSetLinqOperators.CopyToDataTable<AST_LOCATION_MST>(userCtx.GetAll());
            }
            if (dtData != null)
            {
                if (dtData.Rows.Count > 0)
                {
                    lblPreviousLocation.Text = dtData.Rows[0][FINColumnConstants.LOCATION_NAME].ToString();
                    hdPreviousLocation.Value = dtData.Rows[0][FINColumnConstants.ASSET_LOCATION_ID].ToString();
                    //VMVServices.Web.Utils.BindDropDownList(ddPreviousLocation, FINColumnConstants.LOCATION_NAME, FINColumnConstants.ASSET_LOCATION_ID, dtData);
                }
            }
        }
        #endregion
        #region Insert ,Update and Delete the records

        protected void ddlAsset_SelectedIndexChanged(object sender, EventArgs e)
        {
            hdPreviousLocation.Value = string.Empty;

            if (ddlAsset != null)
            {

                if (ddlAsset.SelectedValue != null)
                {
                    if (Master.Mode != FINAppConstants.Add)
                    {
                        divEnd.Visible = true;
                        divEnd1.Visible = true;
                    }
                    else
                    {
                        divEnd.Visible = false;
                        divEnd1.Visible = false;
                    }
                    //ddPreviousLocation.AppendDataBoundItems = true;
                    //ddPreviousLocation.Items.Clear();
                    //ddPreviousLocation.DataTextField = FINColumnConstants.LOCATION_NAME;
                    //ddPreviousLocation.DataValueField = FINColumnConstants.ASSET_LOCATION_DTL_ID;


                    using (IRepository<AST_MINOR_CATEGORY_DTL> userCtx = new DataRepository<AST_MINOR_CATEGORY_DTL>())
                    {
                        dtData = DBMethod.ExecuteQuery(FA_SQL.GetAssetLocationDetails(long.Parse(ddlAsset.SelectedValue.ToString()))).Tables[0];
                        //ddPreviousLocation.DataSource = dtData;
                        //ddPreviousLocation.DataBind();
                        //ddPreviousLocation.SelectedIndex = -1;
                    }
                    if (dtData != null)
                    {
                        if (dtData.Rows.Count > 0)
                        {
                            lblPreviousLocation.Text = dtData.Rows[0][FINColumnConstants.LOCATION_NAME].ToString();
                            if (lblPreviousLocation.Text.Trim() != string.Empty)
                            {
                                hdPreviousLocation.Value = dtData.Rows[0]["PREVIOUS_LOCATION_ID"].ToString();
                            }
                        }
                    }
                    //using (IRepository<AST_ASSET_LOCATION_DTL> userCtx = new DataRepository<AST_ASSET_LOCATION_DTL>())
                    //{
                    //    dtData = DBMethod.ExecuteQuery(FA_SQL.GetAssetLocationDetails(long.Parse(ddlAsset.SelectedValue.ToString()))).Tables[0];
                    //}
                    //if (dtData != null)
                    //{
                    //    if (dtData.Rows.Count > 0)
                    //    {
                    //        VMVServices.Web.Utils.BindDropDownList(ddPreviousLocation, FINColumnConstants.LOCATION_NAME, FINColumnConstants.ASSET_LOCATION_DTL_ID, dtData);
                    //    }
                    //}
                }

            }
        }
        private void EmptyValidation()
        {
            try
            {


                slControls[0] = ddlAsset;
                slControls[1] = ddlCurrentLocation;
                slControls[2] = dtpEffectiveDate;

                Dictionary<string, string> Prop_File_Data;
                Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/FA_" + Session["Sel_Lng"].ToString() + ".properties"));
                strCtrlTypes = "DropDownList~DropDownList~TextBox";
                string strMessage = Prop_File_Data["Asset_Name_P"] + " ~ " + Prop_File_Data["Current_Location_P"] + " ~ " + Prop_File_Data["Effective_From_P"] + "";
                // strMessage = "Asset Name ~ Current Location ~Effective Date";

                EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

                if (EmptyErrorCollection.Count > 0)
                {
                    ErrorCollection = EmptyErrorCollection;
                    return;
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Remove("AssetTransferValidation");
                ErrorCollection.Add("AssetTransferValidation", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void AssignToBE()
        {
            try
            {
                if (EntityData != null)
                {
                    AST_ASSET_LOCATION_DTL = (AST_ASSET_LOCATION_DTL)EntityData;
                }
                //Organization and calendar id
                AST_ASSET_LOCATION_DTL.ORGANIZATION_MASTER_ID = VMVServices.Web.Utils.OrganizationID.ToString();
                // AST_ASSET_LOCATION_DTL.CALENDAR_ID = VMVServices.Web.Utils.CalendarID.ToString() == null ? 0 : VMVServices.Web.Utils.CalendarID;


                AST_ASSET_LOCATION_DTL.ASSET_MST_ID = int.Parse(ddlAsset.SelectedValue == string.Empty ? "0" : ddlAsset.SelectedValue.ToString());
                AST_ASSET_LOCATION_DTL.CURRENT_LOCATION_ID = int.Parse(ddlCurrentLocation.SelectedValue.ToString() == string.Empty ? "0" : ddlCurrentLocation.SelectedValue.ToString());
                //AST_ASSET_LOCATION_DTL.PREVIOUS_LOCATION_ID = int.Parse(ddlCurrentLocation.SelectedValue.ToString() == string.Empty ? "0" : ddlCurrentLocation.SelectedValue.ToString());
                AST_ASSET_LOCATION_DTL.PREVIOUS_LOCATION_ID = int.Parse(hdPreviousLocation.Value.ToString());
                AST_ASSET_LOCATION_DTL.LOCATION_INCHARGE = txtLocationIncharge.Text;
                AST_ASSET_LOCATION_DTL.DEPARTMENT_DTL = txtDepartmentDtl.Text;

                if (dtpEffectiveDate.Text != null && dtpEffectiveDate.Text != string.Empty)
                {
                    AST_ASSET_LOCATION_DTL.EFFECT_FROM = DBMethod.ConvertStringToDate(dtpEffectiveDate.Text);
                }

                if (Master.RecordID > 0)
                {
                    if (dtpEndDate.Text != null && dtpEndDate.Text != string.Empty)
                    {
                        AST_ASSET_LOCATION_DTL.END_DATE = DBMethod.ConvertStringToDate(dtpEndDate.Text);
                    }
                    else
                    {
                        AST_ASSET_LOCATION_DTL.END_DATE = DateTime.Today;
                    }

                    AST_ASSET_LOCATION_DTL.MODIFIED_BY = this.LoggedUserName;
                    AST_ASSET_LOCATION_DTL.MODIFIED_DATE = DateTime.Today;
                    AST_ASSET_LOCATION_DTL.ASSET_LOCATION_DTL_ID = Master.RecordID;
                    DBMethod.SaveEntity<AST_ASSET_LOCATION_DTL>(AST_ASSET_LOCATION_DTL, true);
                }
                else
                {
                    AST_ASSET_LOCATION_DTL.CREATE_BY = this.LoggedUserName;
                    AST_ASSET_LOCATION_DTL.CREATED_DATE = DateTime.Today;
                    AST_ASSET_LOCATION_DTL.ASSET_LOCATION_DTL_ID = DBMethod.GetPrimaryKeyValue(FINSequenceConstant.AST_ASSET_LOCATION_DTL_SEQ);
                    AST_ASSET_LOCATION_DTL.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, AST_ASSET_LOCATION_DTL.ASSET_LOCATION_DTL_ID.ToString());
                    AST_ASSET_LOCATION_DTL.ENABLED_FLAG = "1";
                    DBMethod.SaveEntity<AST_ASSET_LOCATION_DTL>(AST_ASSET_LOCATION_DTL);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Remove("AssetTransferBEValidation");
                ErrorCollection.Add("AssetTransferBEValidation", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                }
            }

        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();

                EmptyValidation();

                AssignToBE();

                //if (ErrorCollection.Count > 0)
                //{
                //    return;
                //}
                ////DBMethod.SaveEntity<AST_ASSET_LOCATION_DTL>(AST_ASSET_LOCATION_DTL);

                ////Update the end date of previous asset location
                //if (Master.Mode != FINAppConstants.Add)
                //{
                //    if (hdPreviousLocation.Value != null && hdPreviousLocation.Value != string.Empty)
                //    {
                //        //if (ddPreviousLocation.SelectedValue != null && ddPreviousLocation.SelectedValue != string.Empty)
                //        //{
                //        if (int.Parse(hdPreviousLocation.Value.ToString()) > 0)
                //        {
                //            if (dtpEndDate.Text != null && dtpEndDate.Text != string.Empty)
                //            {
                //                AST_ASSET_LOCATION_DTL.END_DATE = DBMethod.ConvertStringToDate(dtpEndDate.Text);
                //            }
                //            else
                //            {
                //                AST_ASSET_LOCATION_DTL.END_DATE = DateTime.Today;
                //            }

                //            AST_ASSET_LOCATION_DTL.MODIFIED_BY = this.LoggedUserName;
                //            AST_ASSET_LOCATION_DTL.MODIFIED_DATE = DateTime.Today;
                //            //AST_ASSET_LOCATION_DTL.PREVIOUS_LOCATION_ID = int.Parse(hdPreviousLocation.Value.ToString());
                //            AST_ASSET_LOCATION_DTL.ASSET_LOCATION_DTL_ID = int.Parse(hdPreviousLocation.Value.ToString());

                //            DBMethod.SaveEntity<AST_ASSET_LOCATION_DTL>(AST_ASSET_LOCATION_DTL, true);
                //        }
                //    }
                //}
                //else
                //{
                //    DBMethod.SaveEntity<AST_ASSET_LOCATION_DTL>(AST_ASSET_LOCATION_DTL);
                //}

                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                else
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                }

            }
            catch (Exception ex)
            {
                ErrorCollection.Add(FINMessageConstatns.AssetTransfer, ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion
    }
}