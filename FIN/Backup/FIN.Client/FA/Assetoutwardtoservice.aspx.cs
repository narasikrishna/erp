﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Linq;
using System.Collections.Generic;
using FIN.DAL;
using FIN.BLL;
using FIN.DAL.FA;
using FIN.BLL.FA;
using FIN.BLL.AP;
using VMVServices.Web;

namespace FIN.Client.FA
{
    public partial class Assetoutwardtoservice : PageBase
    {
        /// <summary>
        /// Creted by Jeyarani
        /// Master details for asset's major and minor categories...
        /// </summary>

        AST_ASSET_OUTWARD_SERVICE_DTL aST_ASSET_OUTWARD_SERVICE_DTL = new AST_ASSET_OUTWARD_SERVICE_DTL();
        AssetoutwardtoserviceBLL assetoutwardtoserviceBLL = new AssetoutwardtoserviceBLL();

        string screenMode = string.Empty;
        DataSet dsData = new DataSet();

        Boolean headerRowVisible;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                //btnSave.Visible = false;
                //btnDelete.Visible = true;
               // pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                //btnSave.Visible = true;
                //btnDelete.Visible = false;
                //pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                // btnSave.Text = "Update";
            }
            UserRightsChecking();

        }
        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    //btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    //btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    //btnDelete.Visible = false;
                }
            }
        }
        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                EntityData = null;

                FillComboBox();

                if (Master.Mode != null)
                {


                    if (Master.Mode != FINAppConstants.Add && Master.RecordID > 0)
                    {
                        dtpoutwarddate.Enabled = false;
                        using (IRepository<AST_ASSET_OUTWARD_SERVICE_DTL> userCtx = new DataRepository<AST_ASSET_OUTWARD_SERVICE_DTL>())
                        {
                            aST_ASSET_OUTWARD_SERVICE_DTL = userCtx.Find(r =>
                                (r.OUTWARD_SERVICE_ID == Master.RecordID)
                                ).SingleOrDefault();
                        }

                        EntityData = aST_ASSET_OUTWARD_SERVICE_DTL;

                        ddlAssetMasterid.SelectedValue = aST_ASSET_OUTWARD_SERVICE_DTL.ASSET_MST_ID.ToString();
                        //  dtpoutwarddate.Text = DateTime.Parse(aST_ASSET_OUTWARD_SERVICE_DTL.OUTWARD_DATE.ToString()).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.GetCultureInfo("en-US"));
                        if (aST_ASSET_OUTWARD_SERVICE_DTL.OUTWARD_DATE != null)
                        {
                            dtpoutwarddate.Text = DBMethod.ConvertDateToString(aST_ASSET_OUTWARD_SERVICE_DTL.OUTWARD_DATE.ToString());
                        }
                        ddlThirdpartyid.SelectedValue = aST_ASSET_OUTWARD_SERVICE_DTL.THIRD_PARTY_ID.ToString();
                        txtServiceproviderref.Text = aST_ASSET_OUTWARD_SERVICE_DTL.SERVICE_PROVIDER_REF;
                        txtServiceref.Text = aST_ASSET_OUTWARD_SERVICE_DTL.SERVICE_REFERENCE;
                        // dtpExpectedDeliveryDate.Text = DateTime.Parse(aST_ASSET_OUTWARD_SERVICE_DTL.EXP_DELIVERY_DATE.ToString()).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.GetCultureInfo("en-US"));
                        if (aST_ASSET_OUTWARD_SERVICE_DTL.EXP_DELIVERY_DATE != null)
                        {
                            dtpExpectedDeliveryDate.Text = DBMethod.ConvertDateToString(aST_ASSET_OUTWARD_SERVICE_DTL.EXP_DELIVERY_DATE.ToString());
                        }
                        ntxtApproximatecost.Text = aST_ASSET_OUTWARD_SERVICE_DTL.APPROX_COST.ToString();
                    }


                }


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }



        #region Insert ,Update and Delete the records

        private void AssignToBE()
        {
            ErrorCollection.Clear();
            if (EntityData != null)
            {
                aST_ASSET_OUTWARD_SERVICE_DTL = (AST_ASSET_OUTWARD_SERVICE_DTL)EntityData;
            }

            aST_ASSET_OUTWARD_SERVICE_DTL.ORGANIZATION_MASTER_ID = VMVServices.Web.Utils.OrganizationID.ToString();
            //  aST_ASSET_OUTWARD_SERVICE_DTL.CALENDAR_ID = VMVServices.Web.Utils.CalendarID.ToString() == null ? 0 : VMVServices.Web.Utils.CalendarID;

            aST_ASSET_OUTWARD_SERVICE_DTL.ASSET_MST_ID = int.Parse(ddlAssetMasterid.Text.ToString());

            if (dtpoutwarddate.Text != null && dtpoutwarddate.Text != string.Empty)
            {
                aST_ASSET_OUTWARD_SERVICE_DTL.OUTWARD_DATE = DBMethod.ConvertStringToDate(dtpoutwarddate.Text);
            }
            //aST_ASSET_OUTWARD_SERVICE_DTL.OUTWARD_DATE = DateTime.Parse(dtpoutwarddate.Text.ToString());
            aST_ASSET_OUTWARD_SERVICE_DTL.THIRD_PARTY_ID = ddlThirdpartyid.SelectedValue.ToString();
            aST_ASSET_OUTWARD_SERVICE_DTL.SERVICE_PROVIDER_REF = txtServiceproviderref.Text;
            aST_ASSET_OUTWARD_SERVICE_DTL.SERVICE_REFERENCE = txtServiceref.Text;

            if (dtpExpectedDeliveryDate.Text != null && dtpExpectedDeliveryDate.Text != string.Empty)
            {
                aST_ASSET_OUTWARD_SERVICE_DTL.EXP_DELIVERY_DATE = DBMethod.ConvertStringToDate(dtpExpectedDeliveryDate.Text);
            }

            // aST_ASSET_OUTWARD_SERVICE_DTL.EXP_DELIVERY_DATE = DateTime.Parse(dtpExpectedDeliveryDate.Text.ToString());


            aST_ASSET_OUTWARD_SERVICE_DTL.APPROX_COST = ntxtApproximatecost.Text == null ? 0 : (ntxtApproximatecost.Text.ToString()) == string.Empty ? 0 : CommonUtils.ConvertStringToDecimal(ntxtApproximatecost.Text.ToString());
            aST_ASSET_OUTWARD_SERVICE_DTL.ENABLED_FLAG = "1";

            if (Master.Mode != null)
            {
                if (Master.Mode != FINAppConstants.Add && Master.RecordID > 0)
                {

                    aST_ASSET_OUTWARD_SERVICE_DTL.MODIFIED_BY = this.LoggedUserName;
                    aST_ASSET_OUTWARD_SERVICE_DTL.MODIFIED_DATE = DateTime.Today;
                }
                else
                {
                    aST_ASSET_OUTWARD_SERVICE_DTL.CREATE_BY = this.LoggedUserName;
                    aST_ASSET_OUTWARD_SERVICE_DTL.CREATED_DATE = DateTime.Today;
                    aST_ASSET_OUTWARD_SERVICE_DTL.OUTWARD_SERVICE_ID = DBMethod.GetPrimaryKeyValue(FINSequenceConstant.ASSET_OUTWARD_SERVICE_DTL_SEQ);


                }
            }
            aST_ASSET_OUTWARD_SERVICE_DTL.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, aST_ASSET_OUTWARD_SERVICE_DTL.OUTWARD_SERVICE_ID.ToString());
            assetoutwardtoserviceBLL.aST_ASSET_OUTWARD_SERVICE_DTL = aST_ASSET_OUTWARD_SERVICE_DTL;


        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ErrorCollection.Clear();


                SortedList slControls = new SortedList();
                string strCtrlTypes = string.Empty;
                string strMessage = string.Empty;

                slControls[0] = ddlAssetMasterid;
                slControls[1] = dtpoutwarddate;
                slControls[2] = txtServiceref;
                Dictionary<string, string> Prop_File_Data;
                Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/FA_" + Session["Sel_Lng"].ToString() + ".properties"));
                strCtrlTypes = "DropDownList~TextBox~TextBox";
                strMessage = Prop_File_Data["Asset_P"] + " ~ " + Prop_File_Data["Outward_Date_P"] + " ~ " + Prop_File_Data["Service_Reference_P"] + "";
                //strMessage = "Asset Master ~ Outward Date ~ Service Reference";
                ErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                ErrorCollection.Clear();

                AssignToBE();


                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                ErrorCollection = assetoutwardtoserviceBLL.Validate(long.Parse(ddlAssetMasterid.SelectedValue.ToString()), int.Parse(Master.RecordID.ToString()), Master.Mode);

                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            DBMethod.SaveEntity<AST_ASSET_OUTWARD_SERVICE_DTL>(aST_ASSET_OUTWARD_SERVICE_DTL);
                            //DisplaySaveCompleteMessage(Master.ListPageToOpen);
                            break;
                        }
                    case FINAppConstants.Update:
                        {
                            aST_ASSET_OUTWARD_SERVICE_DTL.OUTWARD_SERVICE_ID = int.Parse(Master.RecordID.ToString());
                            DBMethod.SaveEntity<AST_ASSET_OUTWARD_SERVICE_DTL>(aST_ASSET_OUTWARD_SERVICE_DTL, true);
                            //  DisplaySaveCompleteMessage(Master.ListPageToOpen);
                            break;

                        }
                    //case FINAppConstants.Delete:
                    //    {
                    //        aST_ASSET_OUTWARD_SERVICE_DTL.OUTWARD_SERVICE_ID = int.Parse(Master.RecordID.ToString());
                    //        DBMethod.DeleteEntity<AST_ASSET_OUTWARD_SERVICE_DTL>(aST_ASSET_OUTWARD_SERVICE_DTL);
                    //        // DisplayDeleteCompleteMessage(Master.ListPageToOpen);
                    //        break;
                    //    }
                }



                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                else
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Assetoutwardtoservice", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        #endregion
        private void FillComboBox()
        {
            //using (IRepository<AST_ASSET_MST> userCtx = new DataRepository<AST_ASSET_MST>())
            //{
            //    ddlAssetMasterid.DataTextField = FINColumnConstants.ASSET_NAME;
            //    ddlAssetMasterid.DataValueField = FINColumnConstants.ASSET_MST_ID;
            //    ddlAssetMasterid.DataSource = userCtx.GetAll();
            //    ddlAssetMasterid.DataBind();
            //}

            //using (IRepository<ACD_THIRD_PARTY_MASTER> userCtx = new iAcademeDataRepository<ACD_THIRD_PARTY_MASTER>())
            //{
            //    ddlThirdpartyid.DataTextField = FINColumnConstants.THIRD_PARTY_NAME;
            //    ddlThirdpartyid.DataValueField = FINColumnConstants.THIRD_PARTY_ID;
            //    ddlThirdpartyid.DataSource = userCtx.GetAll();
            //    ddlThirdpartyid.DataBind();
            //}

            DataTable dtData = new DataTable();
            dtData = DBMethod.ExecuteQuery(FA_SQL.GetAssetDetails()).Tables[0];
            if (dtData != null)
            {
                if (dtData.Rows.Count > 0)
                {
                    CommonUtils.LoadDropDownList(ddlAssetMasterid, FINColumnConstants.ASSET_NAME, FINColumnConstants.ASSET_MST_ID, dtData, true, false);
                }
            }
            Supplier_BLL.GetSupplierName(ref ddlThirdpartyid);


        }

        protected void btnYes_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                aST_ASSET_OUTWARD_SERVICE_DTL.OUTWARD_SERVICE_ID = short.Parse(Master.RecordID.ToString());
                DBMethod.DeleteEntity<AST_ASSET_OUTWARD_SERVICE_DTL>(aST_ASSET_OUTWARD_SERVICE_DTL);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("Assetoutwardtoservice", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void ibtnGet_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("AssetoutwardtoserviceList.aspx", false);
        }




    }
}