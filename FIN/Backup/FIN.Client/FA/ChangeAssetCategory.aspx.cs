﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Linq;
using System.Collections.Generic;
using FIN.DAL;
using FIN.BLL;
using FIN.DAL.FA;
using FIN.BLL.FA;

using VMVServices.Web;

namespace FIN.Client.FA
{
    public partial class ChangeAssetCategory : PageBase
    {
        AST_ASSET_CATEGORY_DTL aST_ASSET_CATEGORY_DTL = new AST_ASSET_CATEGORY_DTL();
        string screenMode = string.Empty;
        DataSet dsData = new DataSet();
        DataTable dtMajor = new DataTable();
        DataTable dtMinor = new DataTable();
        DataTable dtAsset = new DataTable();
        SortedList slControls = new SortedList();
        string strCtrlTypes = string.Empty;
        string strMessage = string.Empty;
        ChangeAssetCategoryBLL changeAssetCategoryBLL = new ChangeAssetCategoryBLL();

        Boolean headerRowVisible;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.RecordID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));

            if (Master.Mode == FINAppConstants.Delete)
            {
                //btnSave.Visible = false;
                //btnDelete.Visible = true;
                //  pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                //btnSave.Visible = true;
                //btnDelete.Visible = false;
                //  pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                // btnSave.Text = "Update";
            }
            UserRightsChecking();

        }
        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    // btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    //btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    //  btnDelete.Visible = false;
                }
            }
        }
        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();
                EntityData = null;


                BindAssetDetail();
                show_Asset_Label();
                BindMajorCategory();
                BindMinorCategory();

                if (Master.Mode != null)
                {


                    if (Master.Mode != FINAppConstants.Add && Master.RecordID > 0)
                    {
                        DataTable dtGridData = new DataTable();

                        dtGridData = DBMethod.ExecuteQuery(FA_SQL.GetAssetCategory(Master.RecordID)).Tables[0];

                        ddlAssetName.SelectedValue = dtGridData.Rows[0]["ASSET_MST_ID"].ToString();
                        lblCurrentCategory.Text = dtGridData.Rows[0]["CURRENT_CATEGORY"].ToString();
                        show_Asset_Label();
                        ddlAssetName.Enabled = false;
                        ddlMajorCategory.SelectedValue = dtGridData.Rows[0]["MAJOR_CATEGORY_ID"].ToString();
                        BindMinor();
                        ddlMinorCategory.SelectedValue = dtGridData.Rows[0]["minor_category_id"].ToString();
                        if (dtGridData.Rows[0]["EFFECT_FROM"].ToString() != string.Empty)
                        {
                            dtpEffectivedate.Text = DBMethod.ConvertDateToString(dtGridData.Rows[0]["EFFECT_FROM"].ToString());
                        }

                    }

                }


            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        # region FillComboBox
        private void BindAssetDetail()
        {
            dtAsset = DBMethod.ExecuteQuery(FA_SQL.GetAssetDetails()).Tables[0];
            if (dtAsset != null)
            {
                if (dtAsset.Rows.Count > 0)
                {
                    CommonUtils.LoadDropDownList(ddlAssetName, FINColumnConstants.ASSET_NAME, FINColumnConstants.ASSET_MST_ID, dtAsset, true, false);
                }
            }
        }

        private void BindMajorCategory()
        {
            dtMajor = DBMethod.ExecuteQuery(FA_SQL.SelectSql(FINTableConstant.AST_MAJOR_CATEGORY_MST, "MAJOR_CATEGORY_ID,CATEGORY_NAME", "ENABLED_FLAG='1'", null, "CATEGORY_NAME asc", false)).Tables[0];
            if (dtMajor != null)
            {
                if (dtMajor.Rows.Count > 0)
                {
                    CommonUtils.LoadDropDownList(ddlMajorCategory, FINColumnConstants.CATEGORY_NAME, FINColumnConstants.MAJOR_CATEGORY_ID, dtMajor, true, false);
                }
            }
        }

        private void BindMinorCategory()
        {
            dtMinor = DBMethod.ExecuteQuery(FA_SQL.BindMinorCategory()).Tables[0];
            if (dtMinor != null)
            {
                if (dtMinor.Rows.Count > 0)
                {
                    CommonUtils.LoadDropDownList(ddlMinorCategory, FINColumnConstants.MINOR_CATEGORY_NAME, FINColumnConstants.MINOR_CATEGORY_ID, dtMinor, true, false);
                }
            }
        }

        #endregion FillComboBox

        #region Insert ,Update and Delete the records

        private void AssignToBE()
        {
            ErrorCollection.Clear();
            if (Master.Mode == FINAppConstants.Update)
            {
                using (IRepository<AST_ASSET_CATEGORY_DTL> userCtx = new DataRepository<AST_ASSET_CATEGORY_DTL>())
                {
                    aST_ASSET_CATEGORY_DTL = userCtx.Find(r =>
                        (r.ASSET_CATEGORY_ID == Master.RecordID)
                        ).SingleOrDefault();
                }
            }


            //Organization and calendar id
            aST_ASSET_CATEGORY_DTL.ORGANIZATION_MASTER_ID = VMVServices.Web.Utils.OrganizationID.ToString();
            //  aST_ASSET_CATEGORY_DTL.CALENDAR_ID = VMVServices.Web.Utils.CalendarID.ToString() == null ? 0 : VMVServices.Web.Utils.CalendarID;


            aST_ASSET_CATEGORY_DTL.ASSET_MST_ID = int.Parse(ddlAssetName.SelectedValue);
            aST_ASSET_CATEGORY_DTL.MAJORE_CATEGORY_ID = int.Parse(ddlMajorCategory.SelectedValue);
            aST_ASSET_CATEGORY_DTL.MINOR_CATEGORY_ID = int.Parse(ddlMinorCategory.SelectedValue);
            if (dtpEffectivedate.Text != string.Empty)
            {
                aST_ASSET_CATEGORY_DTL.EFFECT_FROM = DBMethod.ConvertStringToDate(dtpEffectivedate.Text.ToString());
            }
            aST_ASSET_CATEGORY_DTL.ENABLED_FLAG = "1";

            if (Master.Mode != null)
            {
                if (Master.Mode != FINAppConstants.Add && Master.RecordID > 0)
                {

                    aST_ASSET_CATEGORY_DTL.MODIFIED_BY = this.LoggedUserName;
                    aST_ASSET_CATEGORY_DTL.MODIFIED_DATE = DateTime.Today;
                }
                else
                {
                    aST_ASSET_CATEGORY_DTL.CREATE_BY = this.LoggedUserName;
                    aST_ASSET_CATEGORY_DTL.CREATED_DATE = DateTime.Today;

                }
            }
        }

        private void EmptyValidation()
        {
            try
            {
                slControls[0] = ddlAssetName;
                slControls[1] = ddlMajorCategory;
                slControls[2] = ddlMinorCategory;
                Dictionary<string, string> Prop_File_Data;
                Prop_File_Data = FIN.Client.PropertiesFileHeader.GetProperties(Server.MapPath("~/LanguageCollection/FA_" + Session["Sel_Lng"].ToString() + ".properties"));
                strCtrlTypes = "DropDownList~DropDownList~DropDownList";
                strMessage = Prop_File_Data["Asset_Name_P"] + " ~ " + Prop_File_Data["Major_Category_P"] + " ~ " + Prop_File_Data["Minor_Category_P"] + "";
                strMessage = "Asset Name Major Category ~Minor Category";

                EmptyErrorCollection = CommonUtils.IsValid(slControls, strCtrlTypes, strMessage);

                if (EmptyErrorCollection.Count > 0)
                {
                    ErrorCollection = EmptyErrorCollection;
                    return;
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Remove("ChangeAssetCategoryValidation");
                ErrorCollection.Add("ChangeAssetCategoryValidation", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            DataTable dtGridData = new DataTable();
            try
            {

                ErrorCollection.Clear();
                EmptyValidation();
                AssignToBE();
                if (ErrorCollection.Count > 0)
                {
                    return;
                }

                ErrorCollection = ChangeAssetCategoryBLL.Validate(long.Parse(ddlAssetName.SelectedValue.ToString()), DBMethod.ConvertStringToDate(dtpEffectivedate.Text));
                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                switch (Master.Mode)
                {
                    case FINAppConstants.Add:
                        {
                            DBMethod.ExecuteQuery(FA_SQL.UpdFlagAssetChange(int.Parse(ddlAssetName.SelectedValue), DBMethod.ConvertStringToDate(dtpEffectivedate.Text)));//.Tables[0];

                            aST_ASSET_CATEGORY_DTL.ASSET_CATEGORY_ID = DBMethod.GetPrimaryKeyValue(FINSequenceConstant.AST_DPRN_CATEGORY_CONFIG_SEQ);
                            aST_ASSET_CATEGORY_DTL.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, aST_ASSET_CATEGORY_DTL.ASSET_CATEGORY_ID.ToString());

                            DBMethod.SaveEntity<AST_ASSET_CATEGORY_DTL>(aST_ASSET_CATEGORY_DTL);
                            break;
                        }
                    case FINAppConstants.Update:
                        {
                            aST_ASSET_CATEGORY_DTL.ASSET_CATEGORY_ID = int.Parse(Master.RecordID.ToString());
                            aST_ASSET_CATEGORY_DTL.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, aST_ASSET_CATEGORY_DTL.ASSET_CATEGORY_ID.ToString());
                            DBMethod.SaveEntity<AST_ASSET_CATEGORY_DTL>(aST_ASSET_CATEGORY_DTL, true);
                            break;

                        }
                }

                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                else
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ChangeAssetCategoryEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        #endregion

        protected void ibtnGet_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("ChangeAssetCategoryList.aspx", false);
        }

        protected void btnYes_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                aST_ASSET_CATEGORY_DTL.ASSET_CATEGORY_ID = short.Parse(Master.RecordID.ToString());
                DBMethod.DeleteEntity<AST_ASSET_CATEGORY_DTL>(aST_ASSET_CATEGORY_DTL);
                DisplayDeleteCompleteMessage(Master.ListPageToOpen);
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("ChangeAssetCategoryEntry", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                }
            }

        }

        protected void ddlAssetName_SelectedIndexChanged(object sender, EventArgs e)
        {
            show_Asset_Label();

        }

        protected void show_Asset_Label()
        {
            if (ddlAssetName.SelectedValue != string.Empty)
            {

                if (ddlAssetName.SelectedValue != null)
                {
                    DataTable dt = DBMethod.ExecuteQuery(FA_SQL.GetAssetNameLabel(int.Parse(ddlAssetName.SelectedValue))).Tables[0];

                    if (dt.Rows.Count > 0)
                    {
                        lblCurrentCategory.Text = dt.Rows[0][0].ToString();
                    }
                }
            }
        }
        protected void BindMinor()
        {
            if (ddlMajorCategory != null)
            {
                if (ddlMajorCategory.SelectedValue != null)
                {
                    using (IRepository<AST_MINOR_CATEGORY_DTL> userCtx = new DataRepository<AST_MINOR_CATEGORY_DTL>())
                    {
                        dtMinor = DataSetLinqOperators.CopyToDataTable<AST_MINOR_CATEGORY_DTL>(userCtx.Find(r => (r.MAJOR_CATEGORY_ID == int.Parse(ddlMajorCategory.SelectedValue.ToString()))));
                    }
                    if (dtMinor != null)
                    {
                        if (dtMinor.Rows.Count > 0)
                        {
                            CommonUtils.LoadDropDownList(ddlMinorCategory, FINColumnConstants.MINOR_CATEGORY_NAME, FINColumnConstants.MINOR_CATEGORY_ID, dtMinor, true, false);
                        }
                    }
                }
                else
                {
                    BindMinorCategory();
                }
            }
        }
        protected void ddlMajorCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlMinorCategory.Items.Clear();

            BindMinor();
        }

    }
}
