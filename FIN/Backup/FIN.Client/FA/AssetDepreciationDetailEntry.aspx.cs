﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Linq;

using FIN.DAL;
using FIN.BLL;
using FIN.DAL.FA;
using FIN.BLL.FA;
using FIN.BLL.GL;
using VMVServices.Services.Data;
using VMVServices.Web;

namespace FIN.Client.FA
{
    public partial class AssetDepreciationDetailEntry : PageBase
    {
        string screenMode = string.Empty;
        DataTable dtGridData = new DataTable();
        Boolean bol_rowVisiable;
        DataTable dtData = new DataTable();
        DataTable dtData1 = new DataTable();
        AST_ASSET_DEPRECIATION_DTL AST_ASSET_DEPRECIATION_DTL = new AST_ASSET_DEPRECIATION_DTL();
        AST_ASSET_MST AST_ASSET_MST = new AST_ASSET_MST();

        Hashtable htParameters = new Hashtable();
        Hashtable htHeadingParameters = new Hashtable();
        Hashtable htFilterParameter = new Hashtable();


        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                if (!IsPostBack)
                {
                    AssignToControl();
                    EntryLoadHeader();
                    if (VMVServices.Web.Utils.Multilanguage)
                        AssignLanguage();
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("PL", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        private void EntryLoadHeader()
        {
            string str_Header = "";
            str_Header = ClsGridBase.EntryFormMenuHeader(Master, Master.ProgramID);
            //div_FormHeader.InnerHtml = str_Header;
        }
        private void AssignLanguage()
        {
            ClsGridBase.ChangeLanguage();
        }

        private void Startup()
        {
            Master.StrRecordId = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ID.ToString()]));
            Master.Mode = Server.HtmlEncode(Request.QueryString[QueryStringTags.Mode.ToString()]);
            Master.ProgramID = int.Parse(Server.HtmlEncode(Request.QueryString[QueryStringTags.ProgramID.ToString()]));
            Master.ReportName = (Server.HtmlEncode(Request.QueryString[QueryStringTags.ReportName.ToString()]));

            Hashtable htProgram = Menu_BLL.GetMenuDetail(Master.ProgramID);
            Session["ProgramName"] = htProgram[ProgramParameters.ProgramName.ToString()].ToString();

            if (Master.Mode == FINAppConstants.Delete)
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                pnlConfirm.Attributes["display"] = "none";
            }
            else
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                pnlConfirm.Visible = false;
            }
            if (Master.Mode == FINAppConstants.Update)
            {
                btnSave.Text = "Update";
            }

            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
            }

            if (VMVServices.Web.Utils.AddAllowed != FINAppConstants.Y && Master.Mode == FINAppConstants.Add)
            {
                btnSave.Visible = false;
            }
        }
        /// <summary>
        /// UserRightsChecking Function is used to check wheather that user Have Right's to do the selected Action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void UserRightsChecking()
        {
            if (Request.QueryString[QueryStringTags.AddFlag.ToString()] != null)
            {
                VMVServices.Web.Utils.AddAllowed = Server.HtmlEncode(Request.QueryString[QueryStringTags.AddFlag.ToString()]);
                if (Request.QueryString[QueryStringTags.AddFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    // btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.UpdateFlag.ToString()].ToString() == FINAppConstants.N && Master.Mode == FINAppConstants.Update)
                {
                    //  btnSave.Visible = false;
                }
            }
            if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()] != null)
            {
                if (Request.QueryString[QueryStringTags.DeleteFlag.ToString()].ToString() == FINAppConstants.N)
                {
                    // btnDelete.Visible = false;
                }
            }
        }
        //private void BindAsset()
        //{
        //    dtData = DBMethod.ExecuteQuery(FA_SQL.BindAssetMaster()).Tables[0];
        //    if (dtData != null)
        //    {
        //        if (dtData.Rows.Count > 0)
        //        {
        //            CommonUtils.LoadDropDownList(ddlAsset, FINColumnConstants.ASSET_NAME, FINColumnConstants.ASSET_MST_ID, dtData, true, false);
        //        }
        //    }
        //}
        private void FillComboBox()
        {

            dtData1.Clear();
            using (IRepository<AST_MAJOR_CATEGORY_MST> userCtx = new DataRepository<AST_MAJOR_CATEGORY_MST>())
            {
                dtData1 = DataSetLinqOperators.CopyToDataTable<AST_MAJOR_CATEGORY_MST>(userCtx.Find(r => (r.ENABLED_FLAG == "1" && r.WORKFLOW_COMPLETION_STATUS == "1")));
            }
            if (dtData1 != null)
            {
                if (dtData1.Rows.Count > 0)
                {
                    CommonUtils.LoadDropDownList(ddlCategory, FINColumnConstants.CATEGORY_NAME, FINColumnConstants.MAJOR_CATEGORY_ID, dtData1, true, false);
                }
            }

            FIN.BLL.GL.AccountingCalendar_BLL.GetCalAcctYear(ref ddlFinancialYear);
            AccountingCalendar_BLL.GetCompPeriodBasedOrgJDate(ref ddlPeriods, (txtDate.Text));
        }
        private void AssignToControl()
        {
            try
            {
                ErrorCollection.Clear();

                Startup();

                EntityData = null;
                FillComboBox();

                imgBtnJVPrint.Visible = false;
                imgBtnPost.Visible = false;
                btnPrint.Visible = false;



                DataTable dtData = new DataTable();
                dtData = DBMethod.ExecuteQuery(FIN.DAL.SSM.SystemOptions_DAL.getSysoptIDData("FA")).Tables[0];

                gvData.DataSource = new DataTable();
                gvData.DataBind();
                if (dtData != null)
                {
                    if (dtData.Rows.Count > 0)
                    {
                        if (dtData.Rows[0]["FA_DEPRICIATION_RUN"].ToString() != string.Empty)
                        {
                            if (dtData.Rows[0]["FA_DEPRICIATION_RUN"].ToString().ToUpper() == "MONTHLY")
                            {
                                divPeriods.Visible = true;
                                divFinancialYear.Visible = false;
                            }
                            else if (dtData.Rows[0]["FA_DEPRICIATION_RUN"].ToString().ToUpper() == "YEARLY")
                            {
                                divPeriods.Visible = false;
                                divFinancialYear.Visible = true;
                            }
                        }
                    }
                }

                if (Master.Mode != FINAppConstants.Add && Master.StrRecordId != "0")
                {
                    btnPrint.Visible = true;
                    dtGridData = DBMethod.ExecuteQuery(FA_SQL.GetAssetDepDet(int.Parse(Master.StrRecordId.ToString()))).Tables[0];
                    if (dtGridData.Rows.Count > 0)
                    {
                        ddlFinancialYear.SelectedValue = dtGridData.Rows[0]["CAL_DTL_ID"].ToString();
                        txtDate.Text = DBMethod.ConvertDateToString(dtGridData.Rows[0]["GL_TRANSFER_DATE"].ToString());
                        GetPeriods();
                        ddlPeriods.SelectedValue = dtGridData.Rows[0]["PERIOD_ID"].ToString();
                        ddlCategory.SelectedValue = dtGridData.Rows[0]["MAJOR_CATEGORY_ID"].ToString();
                        getDetpDetails();
                        ddlFinancialYear.Enabled = false;
                        ddlPeriods.Enabled = false;
                        ddlCategory.Enabled = false;
                        txtDate.Enabled = false;
                        //if (dtGridData.Rows[0]["POSTED_FLAG"].ToString() == "1")
                        //{
                        //    btnSave.Visible = false;
                        //    btnGet.Visible = false;
                        //}
                    }


                }



            }
            catch (Exception ex)
            {
                ErrorCollection.Add("POR", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void gvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            if (Session[FINSessionConstants.GridData] != null)
            {
                dtGridData = (DataTable)Session[FINSessionConstants.GridData];
            }
            gvData.EditIndex = -1;
            BindGrid(dtGridData);
            SetVisibleDeleteAction();

        }

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {


            GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
            DataRow drList = null;
            if (Session[FINSessionConstants.GridData] != null)
            {
                dtGridData = (DataTable)Session[FINSessionConstants.GridData];
            }

            if (e.CommandName.Equals("EmptyDataTemplateInsert"))
            {
                gvr = gvData.Controls[0].Controls[1] as GridViewRow;
                if (gvr == null)
                {
                    return;
                }
            }
            else if (e.CommandName.Equals("FooterInsert"))
            {
                gvr = gvData.FooterRow;
                if (gvr == null)
                {
                    return;
                }
            }


            if (e.CommandName.Equals("EmptyDataTemplateInsert") || e.CommandName.Equals("FooterInsert"))
            {
                TextBox txt_Location_Name = new TextBox();
                txt_Location_Name = gvr.FindControl("txtLocationName") as TextBox;

                TextBox txt_Location_Description = new TextBox();
                txt_Location_Description = gvr.FindControl("txtLocationDescription") as TextBox;

                drList = dtGridData.NewRow();

                drList[FINColumnConstants.ASSET_LOCATION_ID] = 0;

                drList[FINColumnConstants.LOCATION_NAME] = txt_Location_Name.Text == string.Empty ? "0" : txt_Location_Name.Text;

                drList[FINColumnConstants.LOCATION_DESC] = txt_Location_Description.Text == string.Empty ? "0" : txt_Location_Description.Text;

                dtGridData.Rows.Add(drList);

                BindGrid(dtGridData);
                SetVisibleDeleteAction();
            }

        }

        private void BindGrid(DataTable dtData)
        {
            bol_rowVisiable = false;
            Session[FINSessionConstants.GridData] = dtData;
            DataTable dt_tmp = dtData.Copy();
            if (dt_tmp.Rows.Count == 0)
            {
                DataRow dr = dt_tmp.NewRow();
                dr[0] = "0";
                dt_tmp.Rows.Add(dr);
                bol_rowVisiable = true;
            }
            gvData.DataSource = dt_tmp;
            gvData.DataBind();

            gvData.FooterRow.Visible = false;

            Cal_TotAmt();
        }
        private void Cal_TotAmt()
        {
            decimal dbl_Tot_amt = 0;
            for (int iLoop = 0; iLoop < gvData.Rows.Count; iLoop++)
            {
                TextBox txt_Amt = (TextBox)gvData.Rows[iLoop].FindControl("txtGLAmount");
                if (txt_Amt.Text.ToString().Trim().Length > 0)
                {
                    dbl_Tot_amt = dbl_Tot_amt + FIN.BLL.CommonUtils.ConvertStringToDecimal(txt_Amt.Text);
                }
            }
            txtTotalAmt.Text = DBMethod.GetAmtDecimalCommaSeparationValue(dbl_Tot_amt.ToString());
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session[FINSessionConstants.GridData] != null)
                {
                    dtGridData = (DataTable)Session[FINSessionConstants.GridData];
                }
                string transferDate = string.Empty;

                for (int iLoop = 0; iLoop < gvData.Rows.Count; iLoop++)
                {
                    AST_ASSET_DEPRECIATION_DTL = new AST_ASSET_DEPRECIATION_DTL();

                    if (gvData.DataKeys[iLoop].Values["ASSET_DEPRECIATION_ID"].ToString() != "0")
                    {
                        using (IRepository<AST_ASSET_DEPRECIATION_DTL> userCtx = new DataRepository<AST_ASSET_DEPRECIATION_DTL>())
                        {
                            AST_ASSET_DEPRECIATION_DTL = userCtx.Find(r =>
                                (r.ASSET_DEPRECIATION_ID == int.Parse(gvData.DataKeys[iLoop].Values["ASSET_DEPRECIATION_ID"].ToString()))
                                ).SingleOrDefault();
                        }

                    }

                    if (AST_ASSET_DEPRECIATION_DTL == null)
                    {
                        AST_ASSET_DEPRECIATION_DTL = new AST_ASSET_DEPRECIATION_DTL();
                    }
                    AST_ASSET_DEPRECIATION_DTL.PERIOD_ID = ddlPeriods.SelectedValue.ToString();
                    AST_ASSET_DEPRECIATION_DTL.CAL_DTL_ID = ddlFinancialYear.SelectedValue.ToString();
                    if (txtDate.Text.ToString().Length > 0)
                    {
                        AST_ASSET_DEPRECIATION_DTL.GL_TRANSFER_DATE = DBMethod.ConvertStringToDate(txtDate.Text.ToString());
                    }

                    AST_ASSET_DEPRECIATION_DTL.GL_AMOUNT = ((TextBox)gvData.Rows[iLoop].FindControl("txtGLAmount")) != null ? decimal.Parse(((TextBox)gvData.Rows[iLoop].FindControl("txtGLAmount")).Text == string.Empty ? "0" : ((TextBox)gvData.Rows[iLoop].FindControl("txtGLAmount")).Text) : decimal.Parse(((TextBox)gvData.Rows[iLoop].FindControl("txtGLAmount")).Text);
                    // AST_ASSET_DEPRECIATION_DTL.GL_ACCOUNT = ((TextBox)gvData.Rows[iLoop].FindControl("txtGLAccount")) != null ? (((TextBox)gvData.Rows[iLoop].FindControl("txtGLAccount")).Text == string.Empty ? "0" : ((TextBox)gvData.Rows[iLoop].FindControl("txtGLAccount")).Text) : (((TextBox)gvData.Rows[iLoop].FindControl("txtGLAccount")).Text);
                    //  transferDate = ((TextBox)gvData.Rows[iLoop].FindControl("dtpDateGrid")) != null ? (((TextBox)gvData.Rows[iLoop].FindControl("dtpDateGrid")).Text == string.Empty ? "0" : ((TextBox)gvData.Rows[iLoop].FindControl("dtpDateGrid")).Text) : (((TextBox)gvData.Rows[iLoop].FindControl("dtpDateGrid")).Text);
                    // AST_ASSET_DEPRECIATION_DTL.GL_TRANSFER_DATE = DBMethod.ConvertStringToDate(transferDate.ToString());
                    AST_ASSET_DEPRECIATION_DTL.GL_TRANSFER_STATUS = "N";

                    AST_ASSET_DEPRECIATION_DTL.ENABLED_FLAG = "1";
                    AST_ASSET_DEPRECIATION_DTL.ASSET_COST = ((Label)gvData.Rows[iLoop].FindControl("lblAssetCost")) != null ? decimal.Parse(((Label)gvData.Rows[iLoop].FindControl("lblAssetCost")).Text == string.Empty ? "0" : ((Label)gvData.Rows[iLoop].FindControl("lblAssetCost")).Text) : decimal.Parse(((Label)gvData.Rows[iLoop].FindControl("lblAssetCost")).Text);
                    AST_ASSET_DEPRECIATION_DTL.ASSET_MST_ID = CommonUtils.ConvertStringToInt(gvData.DataKeys[iLoop].Values["asset_mst_id"].ToString());
                    //  AST_ASSET_DEPRECIATION_DTL.ASSET_MST_ID = CommonUtils.ConvertStringToInt(gvData.DataKeys[iLoop].Values["asset_mst_id"].ToString());
                    //   AST_ASSET_DEPRECIATION_DTL.ASSET_DEPRECIATION_ID = AST_ASSET_DEPRECIATION_DTL.ASSET_DEPRECIATION_ID;
                    AST_ASSET_DEPRECIATION_DTL.ORGANIZATION_MASTER_ID = VMVServices.Web.Utils.OrganizationID;

                    if (ddlCategory.SelectedValue != null)
                    {
                        AST_ASSET_DEPRECIATION_DTL.MAJOR_CATEGORY_ID = int.Parse(ddlCategory.SelectedValue.ToString());
                    }
                    //if (AST_ASSET_DEPRECIATION_DTL.MAJOR_CATEGORY_ID != 0)
                    //{
                    //    if (AST_ASSET_DEPRECIATION_DTL.MAJOR_CATEGORY_ID.ToString().Length > 0)
                    //    {
                    //        ErrorCollection.Add("ASTDEP", "Record Already exists for the Selected Category.");
                    //        return;
                    //    }
                    //}
                    if (gvData.DataKeys[iLoop].Values["ASSET_DEPRECIATION_ID"].ToString() != "0")
                    {
                        AST_ASSET_DEPRECIATION_DTL.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, AST_ASSET_DEPRECIATION_DTL.ASSET_DEPRECIATION_ID.ToString());

                        AST_ASSET_DEPRECIATION_DTL.MODIFIED_BY = this.LoggedUserName;
                        AST_ASSET_DEPRECIATION_DTL.MODIFIED_DATE = DateTime.Today;
                        DBMethod.SaveEntity<AST_ASSET_DEPRECIATION_DTL>(AST_ASSET_DEPRECIATION_DTL, true);
                    }
                    else
                    {
                        AST_ASSET_DEPRECIATION_DTL.ASSET_DEPRECIATION_ID = (DBMethod.GetPrimaryKeyValue("AST_DEPRECIATION_DTL_SEQ"));
                        AST_ASSET_DEPRECIATION_DTL.WORKFLOW_COMPLETION_STATUS = FINSP.GetWorhflowstatus(Master.FormCode, this.LoggedUserName, AST_ASSET_DEPRECIATION_DTL.ASSET_DEPRECIATION_ID.ToString());

                        AST_ASSET_DEPRECIATION_DTL.CREATE_BY = this.LoggedUserName;
                        AST_ASSET_DEPRECIATION_DTL.CREATED_DATE = DateTime.Today;
                        DBMethod.SaveEntity<AST_ASSET_DEPRECIATION_DTL>(AST_ASSET_DEPRECIATION_DTL);
                    }

                }
                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                if (AST_ASSET_DEPRECIATION_DTL.WORKFLOW_COMPLETION_STATUS == "1")
                {
                    //FINSP.GetSP_GL_Posting(AST_ASSET_DEPRECIATION_DTL.ASSET_DEPRECIATION_ID.ToString(), "FA_006");
                }
                if (ErrorCollection.Count > 0)
                {
                    return;
                }
                else
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.DATASAVED);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add(FINMessageConstatns.AssetDepreciationToGL, ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);// ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void ibtnGet_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("AssetLocationList.aspx", false);
        }

        protected void ibtnDelete_Click(object sender, ImageClickEventArgs e)
        {
            ModalPopupExtender1.Show();
        }

        protected void btnYes_Click(object sender, EventArgs e)
        {
            //AST_LOCATION_MST aST_LOCATION_MST = new AST_LOCATION_MST();
            //try
            //{
            //    ErrorCollection.Clear();
            //    aST_LOCATION_MST.ASSET_LOCATION_ID = short.Parse(Master.RecordID.ToString());
            //    DBMethod.DeleteEntity<AST_LOCATION_MST>(aST_LOCATION_MST);
            //    DisplayDeleteCompleteMessage(Master.ListPageToOpen);
            //}
            //catch (Exception ex)
            //{
            //    ErrorCollection.Add(FINMessageConstatns.AssetDepreciationToGL, ex.Message);
            //}
            //finally
            //{
            //    if (ErrorCollection.Count > 0)
            //    {
            //        ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('../Validation.aspx','','" + ValidationWindowProperties + "');", true);
            //    }
            //}

        }

        private void SetVisibleDeleteAction()
        {
            try
            {
                //for (int iLoop = 0; iLoop < gvData.Rows.Count; iLoop++)
                //{
                //    ImageButton ibtnGridDelete = gvData.Rows[iLoop].FindControl("ibtnGridDelete") as ImageButton;
                //    if (ibtnGridDelete != null)
                //    {
                //        if (((Label)gvData.Rows[iLoop].FindControl("lblASSETDEPRECIATIONID")).Text != null)
                //        {
                //            if (((Label)gvData.Rows[iLoop].FindControl("lblASSETDEPRECIATIONID")).Text != string.Empty)
                //            {

                //                if (int.Parse(((Label)gvData.Rows[iLoop].FindControl("lblASSETDEPRECIATIONID")).Text) > 0)
                //                {
                //                    gvData.Rows[iLoop].FindControl("ibtnGridDelete").Visible = false;
                //                }
                //                else
                //                {
                //                    gvData.Rows[iLoop].FindControl("ibtnGridDelete").Visible = true;
                //                }
                //            }
                //        }
                //    }
                //}
            }
            catch (Exception ex)
            {

            }
        }


        protected void txtDate_TextChanged1(object sender, EventArgs e)
        {
            GetPeriods();
        }
        private void GetPeriods()
        {
            try
            {
                ErrorCollection.Clear();
                AccountingCalendar_BLL.GetCompPeriodBasedOrgJDate(ref ddlPeriods, (txtDate.Text));
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYCs", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }
        protected void btnGet_Click(object sender, EventArgs e)
        {
            getDetpDetails();
        }
        private void getDetpDetails()
        {
            try
            {
                ErrorCollection.Clear();
                imgBtnJVPrint.Visible = false;
                imgBtnPost.Visible = false;
                btnPrint.Visible = false;
                gvData.Enabled = true;
                btnSave.Visible = true;
                dtGridData = DBMethod.ExecuteQuery(FA_SQL.GetAssetDepreciationList(ddlFinancialYear.SelectedValue, ddlPeriods.SelectedValue, ddlCategory.SelectedValue)).Tables[0];
                if (dtGridData.Rows.Count == 0)
                {
                    if (ddlFinancialYear.SelectedValue != null && ddlFinancialYear.SelectedValue != string.Empty)
                    {
                        dtGridData = DBMethod.ExecuteQuery(FA_SQL.GetAssetDepreciatedDeatils("YEARLY", ddlFinancialYear.SelectedValue, ddlPeriods.SelectedValue, ddlCategory.SelectedValue)).Tables[0];
                        BindGrid(dtGridData);
                        //FA_SQL.GetAssetDepreciatedDeatils("YEARLY", "");
                    }
                    else if (ddlPeriods.SelectedValue != null && ddlPeriods.SelectedValue != string.Empty)
                    {
                        dtGridData = DBMethod.ExecuteQuery(FA_SQL.GetAssetDepreciatedDeatils("MONTHLY", ddlFinancialYear.SelectedValue, ddlPeriods.SelectedValue, ddlCategory.SelectedValue)).Tables[0];
                        BindGrid(dtGridData);
                        //FA_SQL.GetAssetDepreciatedDeatils("MONTHLY", ddlPeriods.SelectedValue);
                    }
                    else
                    {
                        dtGridData = DBMethod.ExecuteQuery(FA_SQL.GetAssetDepreciatedDeatils("0", ddlFinancialYear.SelectedValue, ddlPeriods.SelectedValue, ddlCategory.SelectedValue)).Tables[0];
                        BindGrid(dtGridData);
                    }
                }
                else
                {

                    imgBtnPost.Visible = true;
                    btnPrint.Visible = true;


                    BindGrid(dtGridData);

                    DataTable dt_date = DBMethod.ExecuteQuery(FA_SQL.GetAssetDepDet(int.Parse(dtGridData.Rows[0]["ASSET_DEPRECIATION_ID"].ToString()))).Tables[0];
                    if (dt_date.Rows.Count > 0)
                    {
                        if (dt_date.Rows[0]["POSTED_FLAG"].ToString().Length > 0)
                        {
                            gvData.Enabled = false;
                            btnSave.Visible = false;
                            imgBtnPost.Visible = false;
                            imgBtnJVPrint.Visible = true;
                        }
                    }


                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("BYCs", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);//ScriptManager.RegisterStartupScript(Page, this.GetType(), "Popup", "window.open('" + FINMessageConstatns.ValidationFormPath + "','','" + ValidationWindowProperties + "');", true);
                }
            }
        }

        protected void imgBtnPost_Click(object sender, ImageClickEventArgs e)
        {


            try
            {
                imgBtnPost.Visible = false;
                dtGridData = DBMethod.ExecuteQuery(FA_SQL.GetAssetDepreciationList(ddlFinancialYear.SelectedValue, ddlPeriods.SelectedValue)).Tables[0];
                if (dtGridData.Rows.Count > 0)
                {
                    if (dtGridData.Rows[0]["WORKFLOW_COMPLETION_STATUS"].ToString() == "1")
                    {
                        FINSP.GetSP_GL_Posting(dtGridData.Rows[0]["ASSET_DEPRECIATION_ID"].ToString(), "FA_006");
                    }

                    imgBtnPost.Visible = false;

                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.POSTED);

                    imgBtnJVPrint.Visible = true;
                    Hashtable htParameters = new Hashtable();
                    Hashtable htHeadingParameters = new Hashtable();
                    Hashtable htFilterParameter = new Hashtable();


                    ReportFile = "GL_REPORTS//RPTJournalVoucher.rpt";// "../Reports/GL/RPTJournalVoucher.rpt";

                    dtGridData = DBMethod.ExecuteQuery(FA_SQL.GetAssetDepreciationList(ddlFinancialYear.SelectedValue, ddlPeriods.SelectedValue)).Tables[0];
                    ReportData = DBMethod.ExecuteQuery(FIN.DAL.GL.JournalEntry_DAL.GetJournalVoucherDetailsRPT("", dtGridData.Rows[0]["JE_HDR_ID"].ToString()));


                    Session["ProgramName"] = "Journal Voucher";
                    htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());

                    VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;

                    ReportFormulaParameter = htHeadingParameters;

                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('../RPTCrystalReportViewer.aspx?Id?Id=" + Master.Mode + "','','" + VMVServices.Services.Data.DbConsts.ReportProperties + "');", true);
                }
            }
            catch (Exception ex)
            {
                ErrorCollection.Add("posting", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    imgBtnPost.Visible = true;
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void imgBtnJVPrint_Click(object sender, ImageClickEventArgs e)
        {
            dtGridData = DBMethod.ExecuteQuery(FA_SQL.GetAssetDepreciationList(ddlFinancialYear.SelectedValue, ddlPeriods.SelectedValue)).Tables[0];
            if (dtGridData.Rows.Count > 0)
            {
                Hashtable htParameters = new Hashtable();
                Hashtable htHeadingParameters = new Hashtable();
                Hashtable htFilterParameter = new Hashtable();

                ReportFile = "GL_REPORTS//RPTJournalVoucher.rpt";// "../Reports/GL/RPTJournalVoucher.rpt";

                ReportData = DBMethod.ExecuteQuery(FIN.DAL.GL.JournalEntry_DAL.GetJournalVoucherDetailsRPT("", dtGridData.Rows[0]["JE_HDR_ID"].ToString()));

                Session["ProgramName"] = "Journal Voucher";
                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());

                VMVServices.Web.Utils.ReportFilterParameter = htFilterParameter;

                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('../RPTCrystalReportViewer.aspx?Id?Id=" + Master.Mode + "','','" + VMVServices.Services.Data.DbConsts.ReportProperties + "');", true);


            }
        }

        protected void btnPrint_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ErrorCollection.Clear();
                ReportFile = Master.ReportName;

                AST_ASSET_DEPRECIATION_DTL = FIN.BLL.FA.AssetDepreciationBLL.getClassEntity(Master.StrRecordId);

                if (AST_ASSET_DEPRECIATION_DTL.PERIOD_ID.ToString() != string.Empty)
                {
                    htFilterParameter.Add("PERIOD_ID", AST_ASSET_DEPRECIATION_DTL.PERIOD_ID.ToString());
                }

                VMVServices.Web.Utils.ReportViewFilterParameter = htFilterParameter;


                ReportData = FIN.BLL.FA.AssetDepreciationBLL.GetAssetDepreciationReportData();
                htHeadingParameters.Add("ReportName", Session["ProgramName"].ToString());

                ReportFormulaParameter = htHeadingParameters;

                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Report", "window.open('" + FINMessageConstatns.CrystalReportViewerPath + "?Id=" + Master.Mode + "','','" + DbConsts.ReportProperties + "');", true);

            }
            catch (Exception ex)
            {
                ErrorCollection.Add("FAAssetReport", ex.Message);
            }
            finally
            {
                if (ErrorCollection.Count > 0)
                {
                    Master.ShowMessage(ErrorCollection, FIN.BLL.FINAppConstants.ERROR);
                }
            }
        }

        protected void txtGLAmount_TextChanged(object sender, EventArgs e)
        {
            Cal_TotAmt();
        }
    }
}