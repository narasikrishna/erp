﻿// JScript File
var ctrlList = new Array();
var msgList = new Array();
var dtypeList = new Array();
var flag = 0;

var validInt = '-0123456789';
var validDbl = '-0123456789.';

function IsValid(ctrlName, displayMsg, dataType) {
    flag = 0;
    ctrlList = ctrlName.split('~');
    msgList = displayMsg.split('~');
    dtypeList = dataType.split('~');

    for (var i = 0; i < ctrlList.length; i++) {

        var cntrlPosition;
        var tempCtrl;
        if (dtypeList[i] == "FTDate") {
            var ftctrllist = new Array();
            ftctrllist = ctrlList[i].split('#');
            var FtempCtrl = ControlValid(ftctrllist[0]);
            var TTempCtrl = ControlValid(ftctrllist[1]);
            
            if (!FTDateChk(document.forms[0].elements[FtempCtrl], document.forms[0].elements[TTempCtrl])) {                
                alert('From Date Must Be less then To Date');                
                flag = 1;
                break;
            }
        }
        else {

            cntrlPosition = ControlValid(ctrlList[i]);
            tempCtrl = document.forms[0].elements[cntrlPosition];


            if (dtypeList[i] == "Date") {
                if (!IsDate(tempCtrl)) {
                    tempCtrl.focus();
                    flag = 1;
                    break;
                }

            }
            if (dtypeList[i] == "DropDown") {

                if (IsDropdown(tempCtrl) == 0) {
                    alert('Please Enter ' + msgList[i]);
                    flag = 1;
                    break;
                }

            }


            if (IsEmpty(tempCtrl) == 1) {
                flag = 1;
                alert("Please Enter " + msgList[i]);
                tempCtrl.focus();
                break;
            }
            else {
                if (dtypeList[i] == "Integer") {
                    if (IsIntegerCheck(tempCtrl) == 0) {
                        alert('Please Check ' + msgList[i]);
                        tempCtrl.focus();
                        flag = 1;
                        break;
                    }
                }
                else if (dtypeList[i] == "Double") {
                    if (IsDouble(tempCtrl) == 0) {
                        alert('Please Check ' + msgList[i]);
                        tempCtrl.focus();
                        flag = 1;
                        break;
                    }
                }

            }
        }
    }

    if (flag == 1) {

        return false;
    }
    else {

        return true;
    }
}

function FTDateChk(fctrl, tctrl) {

    if (IsDate(tctrl)) {
        if (IsDate(fctrl)) {

            flag = 0;
            var FArray = new Array();
            var TArray = new Array();

            FArray = fctrl.value.split('/');
            TArray = tctrl.value.split('/');

            var FYear = parseFloat(FArray[2]);
            var TYear = parseFloat(TArray[2]);

            var FMonth = parseFloat(FArray[1]);
            var TMonth = parseFloat(TArray[1]);

            var FDay = parseFloat(FArray[0]);
            var TDay = parseFloat(TArray[0]);


            if (FYear < TYear) {
                return true;

            }
            else if (FYear > TYear) {
                return false;

            }
            else {

                if (FMonth < TMonth) {
                    return true;

                }
                else if (FMonth > TMonth) {
                    return false;
                }
                else {

                    if (FDay < TDay) {
                        return true;
                    }
                    else if (FDay > TDay) {
                        return false;
                    }
                    else {

                        return false; //Both Date is same
                    }
                }
            }

        }
        else {
            return false;
        }
    }
    else {

        return false;
    }
}

function IsEmpty(CtrlN) {
    if (CtrlN.value == '') {
        return 1;
    }
    else {
        return 0;
    }
}

function ControlValid(ctrlName) {
    var intResult = 1;
    for (var i = 0; i < document.forms[0].elements.length; i++) {
        var ctrlNameWotDoll;
        var temp = new Array();
        var tempCtrlName = document.forms[0].elements[i].name;

        temp = tempCtrlName.split('$');
        if (temp.length == 4)
        { ctrlNameWotDoll = temp[temp.length - 2]; }
        else if (temp.length == 3)
        { ctrlNameWotDoll = temp[2]; }
        else
        { ctrlNameWotDoll = temp[temp.length - 1]; }

        if (ctrlNameWotDoll == ctrlName) {
            intResult = i;
            break;
        }
    }
    return intResult;
}


function IsIntegerCheck(CtrlN) {
    var tempArray = new Array();
    tempArray = CtrlN.value.split('');
    flag = 0;

    for (var i = 0; i < tempArray.length; i++) {
        if (validInt.indexOf(tempArray[i]) == -1) {
            flag = 1;
        }
    }
    if (flag == 0) {
        return 1;


    }
    else {
        return 0;

    }
}

function IsDouble(CtrlN) {
    var tempArray = new Array();
    tempArray = CtrlN.value.split('');
    flag = 0;

    if (CtrlN.value.indexOf('.') != CtrlN.value.lastIndexOf('.')) {
        flag = 1;
    }
    else {
        for (var i = 0; i < tempArray.length; i++) {
            if (validDbl.indexOf(tempArray[i]) == -1) {
                flag = 1;
            }
        }
    }

    if (flag == 0) {
        return 1;
    }
    else {
        return 0;
    }
}
function IsDropdown(CtrlN) {

    var temp;
    temp = CtrlN.value;

    flag = 0;

    if (temp == 0) {
        flag = 1;
    }

    if (flag == 0) {
        return 1;
    }
    else {
        return 0;
    }
}

function CheckSamePort(ddFrm, ddto) {
    var ddlFrom = document.forms[0].elements[ControlValid(ddFrm)];
    var ddlTo = document.forms[0].elements[ControlValid(ddto)];

    if (ddlFrom.value == "--Select--" && ddlTo.value == "--Select--") {
        alert("Please Select From & To Port");
        return false;
    }
    else if (ddlFrom.value == ddlTo.value) {
        alert("From & To Port Should Not Be Same");
        return false;
    }
    else if (ddlFrom.value == "--Select--") {
        alert("Please Select From Port");
        return false;
    }
    else if (ddlTo.value == "--Select--") {
        alert("Please Select To Port");
        return false;
    }
    else {
        return true;
    }
}

function DOIssue() {
    var header = document.forms[0].elements[ControlValid("hid_Head")];
    if (confirm("Do you want Letter Head?"))
    { header.value = "Y"; }
    else
    { header.value = "N"; }
    return true;
}


function IntegerCheck(e) {

    var keycode;


    if (window.event)
        keycode = e.keyCode;
    else if (e.which)

        keycode = e.which;

    if (keycode != undefined) {
        if ((keycode >= 48 && keycode <= 57) || (keycode == 8) || (keycode == 127) || (keycode == 13)) {

            return true;
        }

        else {
            alert(" Enter Numeric Values");
            return false;


        }
    }
}

function DateKeyCheck(e) {

    var keycode;


    if (window.event)
        keycode = e.keyCode;
    else if (e.which)

        keycode = e.which;


    if (keycode != undefined) {
        if ((keycode >= 48 && keycode <= 57) || (keycode == 8) || (keycode == 127) || (keycode == 13) || (keycode == 47)) {

            return true;
        }

        else {
            //alert(" Numeric and / only allowed");
            return false;


        }
    }
}

function NoSpaceAllowed(eventRef) {
    var keyStroke = (eventRef.which) ? eventRef.which : (window.event) ? window.event.keyCode : -1;
   // alert(keyStroke);
    var returnValue = true;
    if (keyStroke == 32)
        returnValue = false;
    if (navigator.appName.indexOf('Microsoft') != -1)
        window.event.returnValue = returnValue;

    return returnValue;
}

function StringOnly(eventRef) {
    var keyStroke = (eventRef.which) ? eventRef.which : (window.event) ? window.event.keyCode : -1;
    //alert(keyStroke);
    var returnValue = false;
    if (((keyStroke >= 65) && (keyStroke <= 90)) || ((keyStroke >= 97) && (keyStroke <= 122)) || ((keyStroke >= 48) && (keyStroke < 58)) || (keyStroke == 32) || (keyStroke == 8) || (keyStroke == 127) || (keyStroke == 13) || (keyStroke==45) || (keyStroke=95))
        returnValue = true;
    if (navigator.appName.indexOf('Microsoft') != -1)
        window.event.returnValue = returnValue;

    return returnValue;
}

function DateCheck(e) {

    return false;
}
function Check(objRef) {


    var row = objRef.parentNode.parentNode;

    var GridView = row.parentNode;



    var inputList = GridView.getElementsByTagName("input");

    for (var i = 0; i < inputList.length; i++) {


        var headerCheckBox = inputList[0];

        var checked = true;

        if (inputList[i].type == "checkbox" && inputList[i] != headerCheckBox) {

            if (!inputList[i].checked) {

                checked = false;

                break;

            }

        }

    }

    headerCheckBox.checked = checked;


}


function checkAll(objRef) {

    var GridView = objRef.parentNode.parentNode.parentNode;

    var inputList = GridView.getElementsByTagName("input");

    for (var i = 0; i < inputList.length; i++) {

        var row = inputList[i].parentNode.parentNode;

        if (inputList[i].type == "checkbox" && objRef != inputList[i]) {

            if (objRef.checked) {



                inputList[i].checked = true;

            }

            else {

                inputList[i].checked = false;

            }

        }

    }

}

function IsMonthCheck(From, To, Msg) {
    var ddlFrom = document.forms[0].elements[ControlValid(From)];
    var ddlTo = document.forms[0].elements[ControlValid(To)];

    if (ddlFrom.value > ddlTo.value) {
        alert(Msg + " - To Month must be greater than From Month");
        return false;
    }
    else {
        return true;
    }

}
function IsDate(ctrlName) {
    var cntrlPosition;
    cntrlPosition = ControlValid(ctrlName);
    var strDate = ctrlName;
    var validformat = /^\d{1,2}\/\d{1,2}\/\d{4}$/;
    if (!validformat.test(strDate.value)) {
        alert('Enter Date Format DD/MM/YYYY')
        strDate.value = "";
        return false;
    }
    else {
        var DateText = new Array();
        DateText = strDate.value.split('/');
        var day = DateText[0];
        var month = DateText[1];
        var year = DateText[2];
        if (DateText[0] <= 0 || DateText[0] > 31) {
            alert('Enter Day between 1 to 31')
            strDate.value = "";
            strDate.focus();
            return false;
        }
        else if (DateText[1] <= 0 || DateText[1] > 12) {
            alert('Enter Month between 1 to 12')
            strDate.value = "";
            strDate.focus();
            return false;
        }
        else if (DateText[2] < 1900 || DateText[2] > 2079) {
            alert('Enter Year between 1900 to 2079')
            strDate.value = "";
            strDate.focus();
            return false;
        }
        else
            flag = 1;

    }

    if (flag = 1) {
        return true;
    }
}