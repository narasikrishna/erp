﻿using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.PER;
using VMVServices.Web;
using System.Data;

namespace FIN.BLL.PER
{
    public class PayrollEmployeeStopResume
    {
        static DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();

        # region FillCombo
        public static void GetEmployeeName(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(PayrollEmployeeStopResume_DAL.GetEmployeeDetails()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.EMP_ID, FINColumnConstants.EMP_ID, dtDropDownData, true, false);
        }

        public static void fn_getPayElements(ref DropDownList ddlList,string  str_emp_id)
        {

            dtDropDownData = DBMethod.ExecuteQuery(PayrollEmployeeStopResume_DAL.getPayElements(str_emp_id)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.PAY_ELEMENT, FINColumnConstants.PAY_ELEMENT_ID, dtDropDownData, true, false);
        }

        #endregion

    }
}
