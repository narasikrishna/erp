﻿using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.PER;
using VMVServices.Web;
using System.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FIN.BLL.PER
{
    public class PayrollElementCalculation_BLL
    {
        static DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();

        # region FillCombo
        public static void fn_getGroup(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(PayrollElementCalculation_DAL.getPayGroup()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "PAY_GROUP", "PAY_GROUP_ID", dtDropDownData, true, false);
        }

        

        public static void GetDesignationName(ref DropDownList ddlList, string DEPT_ID)
        {
            dtDropDownData = DBMethod.ExecuteQuery(PayrollElementCalculation_DAL.GetDesignationName(DEPT_ID)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.DESIG_NAME, FINColumnConstants.DESIGN_ID, dtDropDownData, true, false);
        }

        public static void GetElement(ref DropDownList ddlList, string GROUP_ID)
        {
            dtDropDownData = DBMethod.ExecuteQuery(PayrollElementCalculation_DAL.GetElementName(GROUP_ID)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.PAY_ELEMENT, FINColumnConstants.PAY_ELEMENT_ID, dtDropDownData, true, false);
        }

        public static void GetElementName_notinelement(ref DropDownList ddlList, string GROUP_ID, string PAY_ELEMENT_ID)
        {
            dtDropDownData = DBMethod.ExecuteQuery(PayrollElementCalculation_DAL.GetElementName_notinelement(GROUP_ID, PAY_ELEMENT_ID)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.PAY_ELEMENT, FINColumnConstants.PAY_ELEMENT_ID, dtDropDownData, true, false);
        }

        public static void GetElment_notinelment(ref DropDownList ddlList, string PAY_ELEMENT_ID)
        {
            dtDropDownData = DBMethod.ExecuteQuery(PayrollElementCalculation_DAL.GetElment_notinelment(PAY_ELEMENT_ID)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.PAY_ELEMENT, FINColumnConstants.PAY_ELEMENT_ID, dtDropDownData, true, false);
        }

        #endregion
    }
}
