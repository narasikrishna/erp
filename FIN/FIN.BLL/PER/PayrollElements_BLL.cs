﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.PER;
using VMVServices.Web;
using System.Data;
namespace FIN.BLL.PER
{
   public class PayrollElements_BLL
   {
       
       static DataTable dtDropDownData = new DataTable();


       public static DataSet GetPayElementDetailsReport()
       {
           return DBMethod.ExecuteQuery(FIN.DAL.PER.PayrollElements_DAL.getPayElementDetailsReportData());
       }

       public static DataTable getChildEntityDet(string Str_ID)
       {
           DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.PER.PayrollElements_DAL.GetPayElementdtls(Str_ID)).Tables[0];
           return dt_Det;
       }
       public static void fn_getElement(ref DropDownList ddlList)
       {
           dtDropDownData = DBMethod.ExecuteQuery(FIN.DAL.PER.PayrollElements_DAL.getPayElements()).Tables[0];
           CommonUtils.LoadDropDownList(ddlList, "PAY_ELEMENT", "PAY_ELEMENT_ID", dtDropDownData, true, false);

       }
       public static void getPayElements_Recuringonly(ref DropDownList ddlList)
       {
           dtDropDownData = DBMethod.ExecuteQuery(FIN.DAL.PER.PayrollElements_DAL.getPayElements_Recuringonly()).Tables[0];
           CommonUtils.LoadDropDownList(ddlList, "PAY_ELEMENT", "PAY_ELEMENT_ID", dtDropDownData, true, false);

       }

       public static void getPayElements_deductiononly(ref DropDownList ddlList)
       {
           dtDropDownData = DBMethod.ExecuteQuery(FIN.DAL.PER.PayrollElements_DAL.getPayElements_deductiononly()).Tables[0];
           CommonUtils.LoadDropDownList(ddlList, "PAY_ELEMENT", "PAY_ELEMENT_ID", dtDropDownData, true, false);
       }

       public static void getPayElementsDeductions(ref DropDownList ddlList)
       {
           dtDropDownData = DBMethod.ExecuteQuery(FIN.DAL.PER.PayrollElements_DAL.getPayElements_deductiononly()).Tables[0];
           CommonUtils.LoadDropDownList(ddlList, "PAY_ELEMENT", "PAY_ELEMENT_ID", dtDropDownData, false, true);
       }

       public static void getPayElements_Earningonly(ref DropDownList ddlList)
       {
           dtDropDownData = DBMethod.ExecuteQuery(FIN.DAL.PER.PayrollElements_DAL.getPayElements_Earningonly()).Tables[0];
           CommonUtils.LoadDropDownList(ddlList, "PAY_ELEMENT", "PAY_ELEMENT_ID", dtDropDownData, true, false);

       }

       public static void getPayElements_NONRecuringonly(ref DropDownList ddlList)
       {
           dtDropDownData = DBMethod.ExecuteQuery(FIN.DAL.PER.PayrollElements_DAL.getPayElements_NONRecuringonly()).Tables[0];
           CommonUtils.LoadDropDownList(ddlList, "PAY_ELEMENT", "PAY_ELEMENT_ID", dtDropDownData, true, false);

       }
        public static void GetELement4LeaveSalEnhance(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(FIN.DAL.PER.PayrollElements_DAL.getPayElements4SalLeave()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.LOOKUP_NAME, FINColumnConstants.LOOKUP_ID, dtDropDownData, true, false);
        }

        public static void getPayElements_NonRecurring_Deductiononly(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(FIN.DAL.PER.PayrollElements_DAL.getPayElements_NonRecurring_Deductiononly()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "PAY_ELEMENT", "PAY_ELEMENT_ID", dtDropDownData, true, false);
        }

   }
}
