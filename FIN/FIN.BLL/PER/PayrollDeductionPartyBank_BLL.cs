﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.PER;
using VMVServices.Web;
using System.Data;
namespace FIN.BLL.PER
{
    public class PayrollDeductionPartyBank_BLL
    {

        DropDownList ddlList = new DropDownList();
        DataTable dtDropDownData = new DataTable();

        # region FillCombo
        public void fn_getDeductionPartyName(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(PayrollDeductionPartyBank_DAL.getDeductionPartyName()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "DED_PARTY_NAME", "DED_PARTY_CODE", dtDropDownData, true, false);
        }
        # endregion

        public static DataTable getChildEntityDet(string Str_ID)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.PER.PayrollDeductionPartyBank_DAL.GetPayrolldeductionpartybankdtls(Str_ID)).Tables[0];
            return dt_Det;
        }
        

    }
}
