﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.PER;
using VMVServices.Web;
using System.Data;
namespace FIN.BLL.PER
{
    public class PayrollPeriods_BLL
    {



        static DataTable dtDropDownData = new DataTable();

        public static DataTable getChildEntityDet(string Str_ID)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.PER.PayrollPeriods_DAL.GetPayPerioddtls(Str_ID)).Tables[0];
            return dt_Det;
        }

        public static void fn_GetPayrollPeriods(ref DropDownList ddlList,bool bol_Sel=true)
        {
            dtDropDownData = DBMethod.ExecuteQuery(PayrollPeriods_DAL.GetPayrollPeriods()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "PAY_PERIOD_DESC", "PAY_PERIOD_ID", dtDropDownData, bol_Sel, !bol_Sel);

        }
        public static void GetPayrollWithoutApprovPeriods(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(PayrollPeriods_DAL.GetPayrollWithoutApprovPeriods()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "PAY_PERIOD_DESC", "PAY_PERIOD_ID", dtDropDownData, true, false);

        }
        public static void getPayrollPeriods4PayAdvice(ref DropDownList ddlList, string str_Mode)
        {
            dtDropDownData = DBMethod.ExecuteQuery(PayrollPeriods_DAL.getPayrollPeriods4PayAdvice(str_Mode)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "PAY_PERIOD_DESC", "PAY_PERIOD_ID", dtDropDownData, true, false);

        }

        public static void fn_GetPayrollPeriodswithDate(ref DropDownList ddlList, bool bol_Sel = true)
        {
            dtDropDownData = DBMethod.ExecuteQuery(PayrollPeriods_DAL.getPayrollPeriodWithDate()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "PAY_PERIOD_DESC", "PAY_FROM_DT", dtDropDownData, bol_Sel, !bol_Sel);

        }
    }
}
