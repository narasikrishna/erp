﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.PER;
using VMVServices.Web;
using System.Data;

namespace FIN.BLL.PER
{
    public class PayrollDeductionDetails_BLL
    {
        static DataTable dtDropDownData = new DataTable();

        public static PAY_DEDUCTION_DTLS getClassEntity(string str_Id)
        {
            PAY_DEDUCTION_DTLS pAY_DEDUCTION_DTLS = new PAY_DEDUCTION_DTLS();
            using (IRepository<PAY_DEDUCTION_DTLS> userCtx = new DataRepository<PAY_DEDUCTION_DTLS>())
            {
                pAY_DEDUCTION_DTLS = userCtx.Find(r =>
                    (r.DED_ID == str_Id)
                    ).SingleOrDefault();
            }
            return pAY_DEDUCTION_DTLS;
        }

        public static DataTable getChildEntityDet(string DedParty_code, String Payroll_id)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.PER.PayrollDeductionDetails_DAL.GetPayPerioddtls(DedParty_code,Payroll_id)).Tables[0];
            return dt_Det;
        }

        # region FillCombo

        //public void fn_getLineNo(ref DropDownList ddlList)
        //{
        //    dtDropDownData = DBMethod.ExecuteQuery(WarehouseReceiptItem_DAL.getLineNo()).Tables[0];
        //    CommonUtils.LoadDropDownList(ddlList, "LINE_NUM", "ITEM_ID", dtDropDownData, true, false);
        //}
        
        public static void fn_getPayRollDtls(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(PayrollDeductionDetails_DAL.GetPayRollDetails()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "PAY_PERIOD_ID", "PAYROLL_ID", dtDropDownData, true, false);

        }

        public static void fn_getDedPartyDtls(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(PayrollDeductionDetails_DAL.GetDedPartyDetails()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "DED_PARTY_CODE", "DED_PARTY_CODE", dtDropDownData, true, false);

        }

        //public static void fn_getElement(ref DropDownList ddlList)
        //{
        //    dtDropDownData = DBMethod.ExecuteQuery(PayrollEmployeeElement_DAL.getPayElements()).Tables[0];
        //    CommonUtils.LoadDropDownList(ddlList, "PAY_ELEMENT", "PAY_ELEMENT_ID", dtDropDownData, true, false);

        //}
        # endregion
    }
}
