﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FIN.DAL;
using System.Data ;
namespace FIN.BLL.PER
{
    public class NONrecurringElement_BLL
    {
        public static PAY_NR_ELEMENTS_HDR getClassEntity(string str_Id)
        {
            PAY_NR_ELEMENTS_HDR obj_PAY_NR_ELEMENTS_HDR = new PAY_NR_ELEMENTS_HDR();
            using (IRepository<PAY_NR_ELEMENTS_HDR> userCtx = new DataRepository<PAY_NR_ELEMENTS_HDR>())
            {
                obj_PAY_NR_ELEMENTS_HDR = userCtx.Find(r =>
                    (r.PAY_NR_ID == (str_Id.ToString()))
                    ).SingleOrDefault();
            }
            return obj_PAY_NR_ELEMENTS_HDR;
        }
        public static PAY_NR_ELEMENTS_DTL getChildClassEntity(string str_Id)
        {
            PAY_NR_ELEMENTS_DTL obj_PAY_NR_ELEMENTS_DTL = new PAY_NR_ELEMENTS_DTL();
            using (IRepository<PAY_NR_ELEMENTS_DTL> userCtx = new DataRepository<PAY_NR_ELEMENTS_DTL>())
            {
                obj_PAY_NR_ELEMENTS_DTL = userCtx.Find(r =>
                    (r.PAY_NR_DTL_ID == str_Id)
                    ).SingleOrDefault();
            }
            return obj_PAY_NR_ELEMENTS_DTL;
        }
      
        public static DataTable fn_getNonRecEmplist(string str_elementid, string str_PeriodId)
        {
            DataTable dt_data = DBMethod.ExecuteQuery(FIN.DAL.PER.NONRecurringElement_DAL.getNonRecEmplist(str_elementid, str_PeriodId)).Tables[0];
            return dt_data;
        }
    }
}
