﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.PER;
using VMVServices.Web;
using System.Data;


namespace FIN.BLL.PER
{
    public class PayrollGroupElement_BLL
    {
        static DataTable dtDropDownData = new DataTable();

        public static PAY_GROUP_ELEMENT_MAPPING getClassEntity(string str_Id)
        {
            PAY_GROUP_ELEMENT_MAPPING pAY_GROUP_ELEMENT_MAPPING = new PAY_GROUP_ELEMENT_MAPPING();
            using (IRepository<PAY_GROUP_ELEMENT_MAPPING> userCtx = new DataRepository<PAY_GROUP_ELEMENT_MAPPING>())
            {
                pAY_GROUP_ELEMENT_MAPPING = userCtx.Find(r =>
                    (r.PG_ELE_ID == str_Id)
                    ).SingleOrDefault();
            }
            return pAY_GROUP_ELEMENT_MAPPING;
        }
        # region FillCombo


        //public void fn_getLineNo(ref DropDownList ddlList)
        //{
        //    dtDropDownData = DBMethod.ExecuteQuery(WarehouseReceiptItem_DAL.getLineNo()).Tables[0];
        //    CommonUtils.LoadDropDownList(ddlList, "LINE_NUM", "ITEM_ID", dtDropDownData, true, false);
        //}

        public void fn_getElement(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(PayrollGroupElement_DAL.getPayElements()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "PAY_ELEMENT", "PAY_ELEMENT_ID", dtDropDownData, true, false);

        }

        public void fn_getGroup(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(PayrollGroupElement_DAL.getPayGroup()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "PAY_GROUP", "PAY_GROUP_ID", dtDropDownData, true, false);

        }

        # endregion
        public static DataTable getChildEntityDet(string Str_ID)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.PER.PayrollGroupElement_DAL.GetPayPerioddtls(Str_ID)).Tables[0];
            return dt_Det;
        }
    }
}
