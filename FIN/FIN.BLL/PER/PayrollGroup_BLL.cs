﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.PER;
using VMVServices.Web;
using System.Data;
namespace FIN.BLL.PER
{
    public class PayrollGroup_BLL
    {
        static DataTable dtDropDownData = new DataTable();

        # region FillCombo


        //public void fn_getLineNo(ref DropDownList ddlList)
        //{
        //    dtDropDownData = DBMethod.ExecuteQuery(WarehouseReceiptItem_DAL.getLineNo()).Tables[0];
        //    CommonUtils.LoadDropDownList(ddlList, "LINE_NUM", "ITEM_ID", dtDropDownData, true, false);
        //}

        public void fn_getCurrency(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(PayrollGroup_DAL.GetCurrency()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "CURRENCY", "CURRENCY_CODE", dtDropDownData, true, false);
            
        }

        public static void fn_GetPayGroup(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(PayrollGroup_DAL.GetPayGroup()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "PAY_GROUP_DESC", "PAY_GROUP_ID", dtDropDownData, true, false);
            
        }

        
        # endregion

        public static DataTable getChildEntityDet(string Str_ID)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.PER.PayrollGroup_DAL.GetPayPerioddtls(Str_ID)).Tables[0];
            return dt_Det;
        }
    }
}
