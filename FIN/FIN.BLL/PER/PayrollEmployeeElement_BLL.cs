﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.PER;
using VMVServices.Web;
using System.Data;

namespace FIN.BLL.PER
{
    public class PayrollEmployeeElement_BLL
    {
        static DataTable dtDropDownData = new DataTable();


        # region FillCombo

        //public void fn_getLineNo(ref DropDownList ddlList)
        //{
        //    dtDropDownData = DBMethod.ExecuteQuery(WarehouseReceiptItem_DAL.getLineNo()).Tables[0];
        //    CommonUtils.LoadDropDownList(ddlList, "LINE_NUM", "ITEM_ID", dtDropDownData, true, false);
        //}

        public static void fn_getEmployee(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(PayrollEmployeeElement_DAL.getEmployees()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "EMP_FIRST_NAME", "EMP_ID", dtDropDownData, true, false);

        }

        public static  void fn_getGroup(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(PayrollEmployeeElement_DAL.getPayGroup()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "PAY_GROUP", "PAY_GROUP_ID", dtDropDownData, true, false);

        }

        public static void fn_getElement(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(PayrollEmployeeElement_DAL.getPayElements()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "PAY_ELEMENT", "PAY_ELEMENT_ID", dtDropDownData, true, false);

        }
        # endregion

        public static PAY_EMP_ELEMENT_VALUE getClassEntity(string str_Id)
        {
            PAY_EMP_ELEMENT_VALUE pAY_EMP_ELEMENT_VALUE = new PAY_EMP_ELEMENT_VALUE();
            using (IRepository<PAY_EMP_ELEMENT_VALUE> userCtx = new DataRepository<PAY_EMP_ELEMENT_VALUE>())
            {
                pAY_EMP_ELEMENT_VALUE = userCtx.Find(r =>
                    (r.PAY_EMP_ELEMENT_ID == str_Id)
                    ).SingleOrDefault();
            }
            return pAY_EMP_ELEMENT_VALUE;
        }
        
        public static DataTable getChildEntityDet(string Str_ID)
        {
            //PayrollEmployeeElement_DAL
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.PER.PayrollEmployeeElement_DAL.GetPayPerioddtls(Str_ID)).Tables[0];
            return dt_Det;
        }
        public static DataTable getChildEntityDet4EmpId(string Str_ID)
        {
            //PayrollEmployeeElement_DAL
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.PER.PayrollEmployeeElement_DAL.GetPayPerioddtls4EmpId(Str_ID)).Tables[0];
            return dt_Det;
        }

    }
}
