﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.PER;
using VMVServices.Web;
using System.Data;

namespace FIN.BLL.PER
{
    public class PayrollAdvice_BLL
    {
        static DataTable dtDropDownData = new DataTable();
        public static void fn_PayrollAdviceNumber(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(PayrollAdvice_DAL.getPayAdviceNumber()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "PAY_ADVICE_NUMBER", "BANK_ID", dtDropDownData, true, false);

        }
    }
}
