﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.AP;
using VMVServices.Web;
using System.Data;

namespace FIN.BLL.PER
{
    public class PayrollPayDeductionParty_BLL
    {
        static DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();

        # region FillCombo
        public static void fn_getDeductionParty(ref DropDownList ddlList)
        {
            
            dtDropDownData = DBMethod.ExecuteQuery(PayrollPayDeductionParty_DAL.getDedPartyNumber()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.DED_PARTY_CODE, FINColumnConstants.DED_PARTY_CODE, dtDropDownData, true, false);
        }

        public static void fn_getBank(ref DropDownList ddlList, string DED_PARTY_CODE)
        {
            dtDropDownData = DBMethod.ExecuteQuery(PayrollPayDeductionParty_DAL.getBankName(DED_PARTY_CODE)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.BANK_NAME, FINColumnConstants.BANK_CODE, dtDropDownData, true, false);
        }

        public static void fn_getBranch(ref DropDownList ddlList, string DED_PARTY_CODE, string BANK_CODE)
        {
            dtDropDownData = DBMethod.ExecuteQuery(PayrollPayDeductionParty_DAL.getBranchName(DED_PARTY_CODE, BANK_CODE)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.BRANCH_NAME, FINColumnConstants.BRANCH_CODE, dtDropDownData, true, false);
        }

        public static void fn_getAccount(ref DropDownList ddlList, string DED_PARTY_CODE, string BANK_CODE, string BRANCH_CODE)
        {
            dtDropDownData = DBMethod.ExecuteQuery(PayrollPayDeductionParty_DAL.getAccountNumber(DED_PARTY_CODE, BANK_CODE, BRANCH_CODE)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.ACCOUNT_NUMBER, FINColumnConstants.ACCOUNT_NUMBER, dtDropDownData, true, false);
        }

        public static void fn_getChequeNumber(ref DropDownList ddlList, string DED_PARTY_CODE)
        {
            dtDropDownData = DBMethod.ExecuteQuery(PayrollPayDeductionParty_DAL.getChequeNumber(DED_PARTY_CODE)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.CHECK_NUMBER, FINColumnConstants.CHECK_DTL_ID, dtDropDownData, true, false);
        }


        #endregion
    }
}
