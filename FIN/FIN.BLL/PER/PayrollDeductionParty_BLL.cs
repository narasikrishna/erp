﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.AP;
using VMVServices.Web;
using System.Data;

namespace FIN.BLL.PER
{
    public class PayrollDeductionParty_BLL
    {
        static DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();

        # region FillCombo
        public static void fn_getCountry(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Supplier_DAL.getCountry()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.COUNTRY_NAME, FINColumnConstants.COUNTRY_CODE, dtDropDownData, true, false);
        }

        public static void fn_getState(ref DropDownList ddlList, string COUNTRY_CODE)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Supplier_DAL.getState(COUNTRY_CODE)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.STATE_NAME, FINColumnConstants.STATE_CODE, dtDropDownData, true, false);
        }

        public static void fn_getCity(ref DropDownList ddlList, string STATE_CODE)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Supplier_DAL.getCity(STATE_CODE)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.CITY_NAME, FINColumnConstants.CITY_CODE, dtDropDownData, true, false);
        }


        public static void fn_getPayElements(ref DropDownList ddlList)
        {

            dtDropDownData = DBMethod.ExecuteQuery(PayrollDeductionParty_DAL.getPayElements()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.PAY_ELEMENT, FINColumnConstants.PAY_ELEMENT_ID, dtDropDownData, true, false);
        }

        public static void fn_getPayElements_FR_DEDUCTION(ref DropDownList ddlList)
        {

            dtDropDownData = DBMethod.ExecuteQuery(PayrollDeductionParty_DAL.getPayElements_FR_DEDUCTION()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.PAY_ELEMENT, FINColumnConstants.PAY_ELEMENT_ID, dtDropDownData, true, false);
        }


        #endregion
    }
}
