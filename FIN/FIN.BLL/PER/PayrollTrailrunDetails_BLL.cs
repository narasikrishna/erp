﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.PER;
using VMVServices.Web;
using System.Data;

namespace FIN.BLL.PER
{

    public class PayrollTrailrunDetails_BLL
    {
        static DataTable dtDropDownData = new DataTable();
        public static void fn_getApprovedPayrollId(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(PayrollTrailrunDetails_DAL.getApprovedProllId()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "PAYROLL_DET", "PAYROLL_ID", dtDropDownData, true, false);

        }
        public static void fn_getApprovedPayrollId4PayrollPeriod(ref DropDownList ddlList, string str_payPeriod, Boolean bol_Select = true)
        {
            dtDropDownData = DBMethod.ExecuteQuery(PayrollTrailrunDetails_DAL.getApprovedProllId4PayrollPeriod(str_payPeriod)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "PAYROLL_DET", "PAYROLL_ID", dtDropDownData, bol_Select, !bol_Select);

        }

        public static void fn_getFinalPayrollId(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(PayrollTrailrunDetails_DAL.getFinalProllId()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "PAYROLL_DET", "PAYROLL_ID", dtDropDownData, true, false);
        }

        public static void fn_getFinalPayrollDtlId4Payadvice(ref DropDownList ddlList, string str_PayrollId, string str_Mode)
        {
            dtDropDownData = DBMethod.ExecuteQuery(PayrollTrailrunDetails_DAL.getFinalProllDtlId4Payadvice(str_PayrollId, str_Mode)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "PAYROLL_DET", "PAYROLL_ID", dtDropDownData, false, true);
        }
        public static DataSet GetPayAdviceReportData()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.PER.PayrollRun_DAL.getPayAdviceReportData());
        }
    }
}
