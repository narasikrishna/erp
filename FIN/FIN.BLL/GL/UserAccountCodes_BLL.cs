﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FIN.DAL.GL;
using FIN.BLL.CA;
using FIN.DAL;
using FIN.BLL;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;


using VMVServices.Services.Data;
using VMVServices.Services.Business;
using System.Configuration;

using System.Data.EntityClient;
using Oracle.DataAccess.Client;
using System.Data.Common;

namespace FIN.BLL.GL
{
   public class UserAccountCodes_BLL
    {
        DropDownList ddlList = new DropDownList();
        DataTable dtDropDownData = new DataTable();

        # region FillCombo
        //public void fn_GetJobname(ref DropDownList ddlList)
        //{
        //    dtDropDownData = DBMethod.ExecuteQuery(Jobs_DAL.GetJobname()).Tables[0];
        //    CommonUtils.LoadDropDownList(ddlList, "JOB_NAME", "JOB_ID", dtDropDownData, true, false);
        //}
        # endregion

        public static DataTable getChildEntityDet(string Str_ID)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.GL.UserAccountCode_DAL.getUserAcctCodeDtl(Str_ID)).Tables[0];
            return dt_Det;
        }
    }
}
