﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Entities;
using FIN.DAL;
using FIN.BLL;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;


using VMVServices.Services.Data;
using VMVServices.Services.Business;
using System.Configuration;

using System.Data.EntityClient;
using Oracle.DataAccess.Client;
using System.Data.Common;

namespace FIN.BLL.GL
{
    public static class ExchangeRate_BLL
    {

        static DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();

        public static DataTable getExchangeRateDetails(string Str_ID)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.GL.ExchangeRate_DAL.getExchangeRateDetails(Str_ID)).Tables[0];
            return dt_Det;
        }


        public static DataTable getStandardRate(string CURR_ID, string exchngeDate= "")
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.GL.ExchangeRate_DAL.getStandardRate(CURR_ID, exchngeDate)).Tables[0];
            return dt_Det;
        }

        public static void GetExchangeRate(ref DropDownList ddlList,string exchangeDate)
        {
            dtDropDownData = DBMethod.ExecuteQuery(FIN.DAL.GL.ExchangeRate_DAL.GetExchangeRate(exchangeDate)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.CURRENCY_RATE_ID, FINColumnConstants.CURRENCY_RATE_ID, dtDropDownData, true, false);
        }

        public static void SavePCEntity<T, TC>(T entity, List<Tuple<object, string>> tmpChildEntity, TC ChildEntity, bool modifyMode = false)
            where T : class
            where TC : class
        {
            try
            {
                DBMethod.SavePCEntity<T, TC>(entity, tmpChildEntity, ChildEntity, modifyMode);
            }
            catch (Exception ex)
            {
                throw ex.InnerException;
            }

        }

        public static SSM_CURRENCY_EXCHANGE_RATES getClassEntity(string str_Id)
        {
            SSM_CURRENCY_EXCHANGE_RATES obj_SSM_CURRENCY_EXCHANGE_RATES = new SSM_CURRENCY_EXCHANGE_RATES();
            using (IRepository<SSM_CURRENCY_EXCHANGE_RATES> userCtx = new DataRepository<SSM_CURRENCY_EXCHANGE_RATES>())
            {
                obj_SSM_CURRENCY_EXCHANGE_RATES = userCtx.Find(r =>
                    (r.CURRENCY_ID == str_Id)
                    ).SingleOrDefault();
            }


            return obj_SSM_CURRENCY_EXCHANGE_RATES;
        }


        public static SSM_CURRENCY_EXCHANGE_RATES getDetailClassEntity(string str_Id)
        {
            SSM_CURRENCY_EXCHANGE_RATES obj_SSM_CURRENCY_EXCHANGE_RATES = new SSM_CURRENCY_EXCHANGE_RATES();
            using (IRepository<SSM_CURRENCY_EXCHANGE_RATES> userCtx = new DataRepository<SSM_CURRENCY_EXCHANGE_RATES>())
            {
                obj_SSM_CURRENCY_EXCHANGE_RATES = userCtx.Find(r =>
                    (r.CURRENCY_ID == str_Id)
                    ).SingleOrDefault();
            }
            return obj_SSM_CURRENCY_EXCHANGE_RATES;
        }



        public static String ValidateEntity(SSM_CURRENCY_EXCHANGE_RATES obj_SSM_CURRENCY_EXCHANGE_RATES)
        {
            string str_Error = "";
            return str_Error;
        }


        public static String DeleteEntity(int int_PKID)
        {
            string str_Message = "";
            SSM_CURRENCY_EXCHANGE_RATES obj_SSM_CURRENCY_EXCHANGE_RATES = new SSM_CURRENCY_EXCHANGE_RATES();
            obj_SSM_CURRENCY_EXCHANGE_RATES.PK_ID = int_PKID;
            DBMethod.DeleteEntity<SSM_CURRENCY_EXCHANGE_RATES>(obj_SSM_CURRENCY_EXCHANGE_RATES);
            return str_Message;
        }
    }
}
