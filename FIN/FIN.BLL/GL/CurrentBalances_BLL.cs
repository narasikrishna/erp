﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.AP;
using VMVServices.Web;
using System.Data;

namespace FIN.BLL.GL
{
   public class CurrentBalances_BLL
    {
        static DataTable dtDropDownData = new DataTable();
        public static DataSet GetReportData()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.GL.CurrentBalances_DAL.getReportData());
        }
        public static void GetGroupName(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(FIN.DAL.GL.CurrentBalances_DAL.getGlobalSegment()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "SEGMENT_VALUE", "SEGMENT_VALUE", dtDropDownData, false, true);
        }
        public static void GetPeriodName(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(FIN.DAL.GL.CurrentBalances_DAL.getPeriodNumber()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "PERIOD_NAME", "PERIOD_NUMBER", dtDropDownData, false, true);
        }
    }
}
