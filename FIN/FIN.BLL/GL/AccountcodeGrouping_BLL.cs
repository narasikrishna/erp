﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.AP;
using VMVServices.Web;
using System.Data;

namespace FIN.BLL.GL
{
  public class AccountcodeGrouping_BLL
    {
        DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();

        public static DataSet GetAccountcodeGroupingReportData()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.GL.AccountcodeGrouping_DAL.getAccountcodeGroupingReportData());
        }
    }
}
