﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Entities;
using FIN.DAL;
using FIN.BLL;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;


using VMVServices.Services.Data;
using VMVServices.Services.Business;
using System.Configuration;

using System.Data.EntityClient;
using Oracle.DataAccess.Client;
using System.Data.Common;

namespace FIN.BLL.GL
{
    public static class Currency_BLL
    {
        static DataTable dtDropDownData = new DataTable();



        //        # region FillCombo
        public static void getCurrencyDesc(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(FIN.DAL.GL.Currency_DAL.getCurrentyDesc()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.CURRENCY_DESC, FINColumnConstants.CURRENCY_ID, dtDropDownData, true, false);
        }

        public static void getCurrencyCode(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(FIN.DAL.GL.Currency_DAL.getCurrentyCode()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.CURRENCY_CODE, FINColumnConstants.CURRENCY_ID, dtDropDownData, true, false);
        }

        public static void getCurrencyDetails(ref DropDownList ddlList, bool includeAll=false)
        {
            dtDropDownData = DBMethod.ExecuteQuery(FIN.DAL.GL.Currency_DAL.getCurrencyDetails()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.CURRENCY_NAME, FINColumnConstants.CURRENCY_ID, dtDropDownData, !includeAll, includeAll);
        }
        public static void getCurrencyBasedOrg(ref DropDownList ddlList, string orgId)
        {
            dtDropDownData = DBMethod.ExecuteQuery(FIN.DAL.GL.Currency_DAL.getCurrencyBasedOrg(orgId)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.CURRENCY_NAME, FINColumnConstants.CURRENCY_ID, dtDropDownData, true, false);
        }
        public static void GetPaymentCurrency(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(FIN.DAL.GL.Currency_DAL.GetPaymentCurrency()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.CURRENCY_NAME, FINColumnConstants.CURRENCY_ID, dtDropDownData, true, false);
        }
        public static void GetARPaymentCurrency(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(FIN.DAL.GL.Currency_DAL.GetARPaymentCurrency()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.CURRENCY_NAME, FINColumnConstants.CURRENCY_ID, dtDropDownData, true, false);
        }


        //        # endregion  
        public static bool IsBaseCurrency(string selectedCurrency)
        {
            bool exchageActive = false;
            DataTable dtDat = new DataTable();
            dtDat = DBMethod.ExecuteQuery(FIN.DAL.GL.Currency_DAL.getCurrencyBasedOrg(VMVServices.Web.Utils.OrganizationID)).Tables[0];

            if (dtDat != null)
            {
                if (dtDat.Rows.Count > 0)
                {
                    if (selectedCurrency == dtDat.Rows[0][FINColumnConstants.CURRENCY_ID].ToString())
                    {
                        exchageActive = true;
                    }
                }
            }

            return exchageActive;

        }
        public static int GetPrecision(string selectedCurrency)
        {
            int precision = 0;
            string precisionValue = "0";
            SSM_CURRENCIES SSM_CURRENCIES = new SSM_CURRENCIES();

            using (IRepository<SSM_CURRENCIES> ssm = new DataRepository<SSM_CURRENCIES>())
            {
                SSM_CURRENCIES = ssm.Find(x => (x.CURRENCY_ID == selectedCurrency)).SingleOrDefault();
                if (SSM_CURRENCIES != null)
                {
                    precisionValue = SSM_CURRENCIES.STANDARD_PRECISION;
                }
            }
            if (CommonUtils.ConvertStringToDecimal(precisionValue) > 0)
            {
                precision = int.Parse(precisionValue);
            }
            else
            {
                precision = FINAppConstants.defaultPrecision;
            }
            return precision;
        }

    }
}
