﻿using System.Linq;
using System.Text;

using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.AP;

using VMVServices.Web;
using System.Data;

namespace FIN.BLL.GL
{
    public class AccountMonthlySummary_BLL
    {
        static DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();
        public static string GetReportData()
        {
           return  FIN.DAL.GL.AccountMonthlySummary_DAL.getReportData();
        }
        //public static void GetGroupName(ref DropDownList ddlList)
        //{
        //    dtDropDownData = DBMethod.ExecuteQuery(FIN.DAL.GL.Segments_DAL.GetGlobalSegmentvaluesBasedOrg(OrgId)).Tables[0];
        //    CommonUtils.LoadDropDownList(ddlList, "SEGMENT_VALUE", "SEGMENT_ID", dtDropDownData, false, true);
        //}
    }
}
