﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Web.UI.WebControls;
using FIN.DAL;
namespace FIN.BLL.GL
{
   public  class BalanceSheetTemplate_BLL
    {
        public static void fn_getTemplateName(ref DropDownList ddlList, Boolean bol_select = true)
        {
          
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.GL.BalanceSheetTemplate_DAL.getTemplateName()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "BS_TMPL_NAME", "BS_TMPL_NAME", dt_Det, bol_select, !bol_select);
        }

        public static void fn_getTemplateGroupNumber(ref DropDownList ddlList,string str_TempName, Boolean bol_select = true)
        {

            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.GL.BalanceSheetTemplate_DAL.get_TemplateGroupNumber(str_TempName)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "BS_GROUP_NO", "BS_GROUP_NO", dt_Det, bol_select, !bol_select);
        }
    }
}
