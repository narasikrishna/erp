﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Entities;
using FIN.DAL;
using FIN.BLL;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;


using VMVServices.Services.Data;
using VMVServices.Services.Business;
using System.Configuration;

using System.Data.EntityClient;
using Oracle.DataAccess.Client;
using System.Data.Common;

namespace FIN.BLL.GL
{
   public class Balances_BLL
    {


       public static void fn_getPeriod(ref DropDownList ddlList)
        {
            // DataTable dt_Det = new DataTable();
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.GL.Balances_DAL.getPeriod()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.PERIOD_NAME, FINColumnConstants.PERIOD_NAME, dt_Det, true, false);
        }

       public static void fn_getSegment(ref DropDownList ddlList)
       {
           // DataTable dt_Det = new DataTable();
           DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.GL.Balances_DAL.getSegment()).Tables[0];
           CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.SEGMENT_VALUE, FINColumnConstants.SEGMENT_VALUE, dt_Det, true, false);
       }


    }
}
