﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Entities;
using FIN.DAL;
using FIN.BLL;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;


using VMVServices.Services.Data;
using VMVServices.Services.Business;
using System.Configuration;

using System.Data.EntityClient;
using Oracle.DataAccess.Client;
using System.Data.Common;

namespace FIN.BLL.GL
{
   public class AccountStructure_BLL
    {
     

       public static DataTable getChildEntityDet(string Str_ID)
       {
           DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountStructure_DAL.getAcctStructDetails(Str_ID)).Tables[0];
           return dt_Det;
       }


       public static void SavePCEntity<T, TC>(T entity, List<Tuple<object, string>> tmpChildEntity, TC ChildEntity, bool modifyMode = false)
           where T : class
           where TC : class
       {
           try
           {
               DBMethod.SavePCEntity<T, TC>(entity, tmpChildEntity, ChildEntity, modifyMode);
           }
           catch (Exception ex)
           {
               throw ex.InnerException;
           }

       }



       public static GL_ACCT_STRUCTURE getClassEntity(string str_Id)
       {
           GL_ACCT_STRUCTURE obj_GL_ACCT_STRUCTURE = new GL_ACCT_STRUCTURE();
           using (IRepository<GL_ACCT_STRUCTURE> userCtx = new DataRepository<GL_ACCT_STRUCTURE>())
           {
               obj_GL_ACCT_STRUCTURE = userCtx.Find(r =>
                   (r.ACCT_STRUCT_ID == str_Id)
                   ).SingleOrDefault();
           }


           return obj_GL_ACCT_STRUCTURE;
       }
       public static String ValidateEntity(GL_ACCT_STRUCTURE obj_GL_ACCT_STRUCTURE)
       {
           string str_Error = "";
           return str_Error;
       }


     



    }
}
