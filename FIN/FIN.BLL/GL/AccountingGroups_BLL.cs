﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Entities;
using FIN.DAL;
using FIN.BLL;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;


using VMVServices.Services.Data;
using VMVServices.Services.Business;
using System.Configuration;

using System.Data.EntityClient;
using Oracle.DataAccess.Client;
using System.Data.Common;

namespace FIN.BLL.GL
{
    public static class AccountingGroups_BLL
    {
        
        static string sqlQuery = "";



        
        //        # region FillCombo
        public static string getTopAccountGroupSummaryReportData()
        {
            return FIN.DAL.GL.AccountingGroups_DAL.getTopAccountGroupSummaryReportData();
        }
        public static string getTopBalanceAccountGroupSummaryReportData()
        {
            return FIN.DAL.GL.AccountingGroups_DAL.getTopBalanceAccountGroupSummaryReportData();
        }

        public static void getTopBalanceAccountGroups(ref DropDownList ddlList)
        {
            // DataTable dt_Det = new DataTable();
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountingGroups_DAL.getTopBalanceAccountGroups()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.GROUP_NAME, FINColumnConstants.GROUP_ID, dt_Det, true, false);
        }
        public static void fn_getAccountGroup(ref DropDownList ddlList)
        {
            // DataTable dt_Det = new DataTable();
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountingGroups_DAL.getGroupName()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.GROUP_NAME, FINColumnConstants.GROUP_ID, dt_Det, true, false);
        }
        public static void getGroupDetails(ref DropDownList ddlList)
        {
            // DataTable dt_Det = new DataTable();
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountingGroups_DAL.getGroupDetails()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.GROUP_NAME, FINColumnConstants.GROUP_ID, dt_Det, true, false);
        }
        //        # endregion
        

        public static GL_ACCT_GROUPS getClassEntity(string  str_Id)
        {
            GL_ACCT_GROUPS obj_GL_ACCT_GROUPS = new GL_ACCT_GROUPS();
            using (IRepository<GL_ACCT_GROUPS> userCtx = new DataRepository<GL_ACCT_GROUPS>())
            {
                obj_GL_ACCT_GROUPS = userCtx.Find(r =>
                    (r.ACCT_GRP_ID == str_Id)
                    ).SingleOrDefault();
            }
            return obj_GL_ACCT_GROUPS;
        }
        public static String ValidateEntity(GL_ACCT_GROUPS obj_GL_ACCT_GROUPS)
        {
            string str_Error = "";
            return str_Error;
        }

        public static String SaveEntity(GL_ACCT_GROUPS obj_GL_ACCT_GROUPS, string str_Mode, bool GenPKID = true)
        {
            string str_PK = "";
            
            switch (str_Mode)
            {
                case FINAppConstants.Add:
                    {
                        if (GenPKID)
                        {
                            obj_GL_ACCT_GROUPS.PK_ID = DBMethod.GetPrimaryKeyValue(FINSequenceConstant.GL_ACCT_GROUPS_SEQ);
                        }
                        DBMethod.SaveEntity<GL_ACCT_GROUPS>(obj_GL_ACCT_GROUPS);
                        break;
                    }
                case FINAppConstants.Update:
                    {
                        DBMethod.SaveEntity<GL_ACCT_GROUPS>(obj_GL_ACCT_GROUPS, true);
                        break;
                    }
            }
            return str_PK;
        }
        public static String DeleteEntity(string str_ID)
        {
            string str_Message="";
            GL_ACCT_GROUPS obj_GL_ACCT_GROUPS = new GL_ACCT_GROUPS();
            obj_GL_ACCT_GROUPS.ACCT_GRP_ID = str_ID;
            DBMethod.DeleteEntity<GL_ACCT_GROUPS>(obj_GL_ACCT_GROUPS);
            return str_Message;
        }

        public static DataTable getListData(string modifyURL, string deleteURL)
        {
            return DBMethod.ExecuteQuery("select ACCT_GRP_ID," + modifyURL+"," + deleteURL + " from GL_ACCT_GROUPS").Tables[0];
        }
        public static string GetAccountingGroupsSummaryReportData()
        {
            return FIN.DAL.GL.AccountingGroups_DAL.getAccountGroupSummaryReportData();
        }
    }
}
