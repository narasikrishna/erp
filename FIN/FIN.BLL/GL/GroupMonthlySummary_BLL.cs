﻿using System.Linq;
using System.Text;

using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.AP;
using VMVServices.Web;
using System.Data;

namespace FIN.BLL.GL
{
    public class GroupMonthlySummary_BLL
    {
        static DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();
        public static string GetReportData()
        {
            return FIN.DAL.GL.GroupMonthlySummary_DAL.getReportData();
        }
        public static void GetGroupName(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(FIN.DAL.GL.GroupMonthlySummary_DAL.getGlobalSegment()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "SEGMENT_VALUE", "SEGMENT_ID", dtDropDownData, false, true);
        }

        public static void GetAccountGroupName(ref DropDownList ddlList)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountingGroupLinks_DAL.getAccountGroupName()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.GROUP_NAME, FINColumnConstants.GROUP_ID, dt_Det, true, false);
        }




    }
}
