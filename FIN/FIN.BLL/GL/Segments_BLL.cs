﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FIN.DAL.GL;
using FIN.BLL.CA;
using FIN.DAL;
using FIN.BLL;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;


using VMVServices.Services.Data;
using VMVServices.Services.Business;
using System.Configuration;

using System.Data.EntityClient;
using Oracle.DataAccess.Client;
using System.Data.Common;

namespace FIN.BLL.GL
{
    public class Segments_BLL
    {
        static DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();

        public static void GetGlobalSegmentvalues(ref DropDownList ddlList, string orgId)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Segments_DAL.GetGlobalSegmentvalues(orgId)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.SEGMENT_VALUE, FINColumnConstants.SEGMENT_VALUE_ID, dtDropDownData, true, false);
        }
        public static void GetGlobalSegmentvaluesBasedOrg(ref DropDownList ddlList,Boolean bol_SEL=true)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Segments_DAL.GetGlobalSegmentvaluesBasedOrg()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.SEGMENT_VALUE, FINColumnConstants.SEGMENT_VALUE_ID, dtDropDownData, bol_SEL, !bol_SEL);
        }
        public static void GetGlobalSegmentvalues(ref DropDownList ddlList, Boolean bol_Sel = true)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Segments_DAL.GetGlobalSegmentvalues()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.SEGMENT_VALUE, FINColumnConstants.SEGMENT_VALUE_ID, dtDropDownData, bol_Sel, !bol_Sel);
        }

        public static void GetGlobalCostCentreSegmentvalues(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Segments_DAL.GetGlobalCostCentreSegmentvalues()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.SEGMENT_VALUE, FINColumnConstants.SEGMENT_VALUE_ID, dtDropDownData, false, true);
        }
        public static void GetGlobalSegvalues(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Segments_DAL.GetGlobalSegmentvalues()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.SEGMENT_VALUE, FINColumnConstants.SEGMENT_VALUE_ID, dtDropDownData, false, true);
        }
        public static void GetGlobalSegmentvaluesByOrgName(ref DropDownList ddlList, Boolean bol_Sel = true)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Segments_DAL.GetGlobalSegmentvaluesByOrgName()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.SEGMENT_VALUE, FINColumnConstants.SEGMENT_VALUE_ID, dtDropDownData, bol_Sel, !bol_Sel);
        }
        public static void GetGlobalSegmentName(ref DropDownList ddlList,Boolean bol_Sel=true)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Segments_DAL.GetGlobalSegmentName()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.SEGMENT_NAME, FINColumnConstants.SEGMENT_ID, dtDropDownData, bol_Sel, !bol_Sel);
        }

        public static void GetSegmentvaluesBasedAccSegment(ref DropDownList ddlList, string accountCodeId, string segmentId)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Segments_DAL.GetSegmentvaluesBasedAccSegment(accountCodeId, segmentId)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.SEGMENT_VALUE, FINColumnConstants.SEGMENT_VALUE_ID, dtDropDownData, true, false);
        }
        public static void getSegmentValueNotInGlobalSegment(ref DropDownList ddlList, Boolean bol_Sel=true, string segId="")
        {
            dtDropDownData = DBMethod.ExecuteQuery(Segments_DAL.getSegmentValueNotInGlobalSegment(segId)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.SEGMENT_VALUE, FINColumnConstants.SEGMENT_VALUE_ID, dtDropDownData, bol_Sel, !bol_Sel);
        }
        public static void GetSegmentvaluesBasedGroupSegment(ref DropDownList ddlList, string accountGroupId, string segmentId)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Segments_DAL.GetSegmentvaluesBasedGroupSegment(accountGroupId, segmentId)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.SEGMENT_VALUE, FINColumnConstants.SEGMENT_VALUE_ID, dtDropDownData, true, false);
        }

        public static void GetSegmentvaluesBaseDSegment(ref DropDownList ddlList, string segmentId,Boolean bol_Sel=true)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Segments_DAL.getSegmentvalue4Segment(segmentId)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.SEGMENT_VALUE, FINColumnConstants.SEGMENT_VALUE_ID, dtDropDownData, bol_Sel, !bol_Sel);
        }

        public static void getSegmentName4Type(ref DropDownList ddlList, string degSeg, string mode)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Segments_DAL.getSegmentName4Type(degSeg, mode)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.SEGMENT_NAME, FINColumnConstants.SEGMENT_ID, dtDropDownData, true, false);

        }
        public static void GetSegmentvalues(ref DropDownList ddlList, string segmentId)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Segments_DAL.GetSegmentvalues(segmentId)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.SEGMENT_VALUE, FINColumnConstants.SEGMENT_VALUE_ID, dtDropDownData, true, false);
        }

        public static void GetSegmentvalues4ParentSeg(ref DropDownList ddlList, string segmentId, string ParentSegId)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Segments_DAL.GetSegmentvalues4ParentSeg(segmentId, ParentSegId)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.SEGMENT_VALUE, FINColumnConstants.SEGMENT_VALUE_ID, dtDropDownData, true, false);
        }

        public static DataTable getSegmentData(string str_segmentid)
        {
            return DBMethod.ExecuteQuery(Segments_DAL.getSegmentData(str_segmentid)).Tables[0];
        }
    }
}
