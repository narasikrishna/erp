﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Entities;
using FIN.DAL;
using FIN.BLL;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;


using VMVServices.Services.Data;
using VMVServices.Services.Business;
using System.Configuration;

using System.Data.EntityClient;
using Oracle.DataAccess.Client;
using System.Data.Common;
using System.Collections;

namespace FIN.BLL.GL
{
    public static class Organisation_BLL
    {

        public static void getOrganizationDet(ref DropDownList ddlList)
        {
            // DataTable dt_Det = new DataTable();
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.GL.Organisation_DAL.getOrganisationDetails()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.ORG_NAME, FINColumnConstants.ORG_ID, dt_Det, true, false);
        }

        public static void getCompanyNM(ref DropDownList ddlList)
        {
            // DataTable dt_Det = new DataTable();
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.GL.Organisation_DAL.getCompanyNM()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "COMP_INTERNAL_NAME", "COMP_ID", dt_Det, true, false);
        }
        public static void getPeriodCalendarDtl(ref DropDownList ddlList, bool includeAll = false)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.GL.Organisation_DAL.getPeriodCalendarDtls()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "cal_acct_year", "cal_dtl_id", dt_Det, !includeAll, includeAll);
        }
        public static void getPeriodStatusDtl(ref DropDownList ddlList, string calDtlId, bool includeAll = false)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.GL.Organisation_DAL.getPeriodStatus(calDtlId)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "period_name", "period_id", dt_Det, !includeAll, includeAll);
        }

        public static void SavePCEntity<T, TC>(T entity, List<Tuple<object, string>> tmpChildEntity, TC ChildEntity, bool modifyMode = false)
            where T : class
            where TC : class
        {
            try
            {
                DBMethod.SavePCEntity<T, TC>(entity, tmpChildEntity, ChildEntity, modifyMode);
            }
            catch (Exception ex)
            {
                throw ex.InnerException;
            }

        }


        public static GL_COMPANIES_DTL getClassEntity(string str_Id)
        {
            GL_COMPANIES_DTL obj_gL_COMPANIES_DTL = new GL_COMPANIES_DTL();
            using (IRepository<GL_COMPANIES_DTL> userCtx = new DataRepository<GL_COMPANIES_DTL>())
            {
                obj_gL_COMPANIES_DTL = userCtx.Find(r =>
                    (r.COMP_DTL_ID == str_Id)
                    ).SingleOrDefault();
            }


            return obj_gL_COMPANIES_DTL;
        }


        public static GL_COMPANIES_DTL getDetailClassEntity(string str_Id)
        {
            GL_COMPANIES_DTL obj_gL_COMPANIES_DTL = new GL_COMPANIES_DTL();
            using (IRepository<GL_COMPANIES_DTL> userCtx = new DataRepository<GL_COMPANIES_DTL>())
            {
                obj_gL_COMPANIES_DTL = userCtx.Find(r =>
                    (r.COMP_DTL_ID == str_Id)
                    ).SingleOrDefault();
            }
            return obj_gL_COMPANIES_DTL;
        }



        public static String ValidateEntity(GL_COMPANIES_DTL obj_gL_COMPANIES_DTL)
        {
            string str_Error = "";
            return str_Error;
        }


        public static String DeleteEntity(int int_PKID)
        {
            string str_Message = "";
            GL_COMPANIES_DTL obj_gL_COMPANIES_DTL = new GL_COMPANIES_DTL();
            obj_gL_COMPANIES_DTL.PK_ID = int_PKID;
            DBMethod.DeleteEntity<GL_COMPANIES_DTL>(obj_gL_COMPANIES_DTL);
            return str_Message;
        }

        static System.Collections.SortedList ErrorCollection = new System.Collections.SortedList();

        public static SortedList DataDuplication(System.Data.DataTable dtGridData, string strCondition, string strMessage, DateTime startDate, DateTime endDate, string currencyId, string accountStru, string calId)
        {
            SortedList ErrorCollection = new SortedList();


            if (dtGridData != null)
            {
                if (dtGridData.Rows.Count > 0)
                {
                    if (dtGridData.Select(strCondition).Length > 0)
                    {
                        for (int i = 0; i < dtGridData.Rows.Count; i++)
                        {
                            DateTime start_date = DateTime.Parse(dtGridData.Rows[i][FINColumnConstants.EFFECTIVE_START_DT].ToString());
                            DateTime? end_date = null;


                            if (dtGridData.Rows[i][FINColumnConstants.EFFECTIVE_END_DT].ToString() != string.Empty)
                            {
                                end_date = DateTime.Parse(dtGridData.Rows[i][FINColumnConstants.EFFECTIVE_END_DT].ToString());
                            }

                            if (dtGridData.Rows[i][FINColumnConstants.ENABLED_FLAG].ToString() == "TRUE")
                            {
                                if ((startDate) >= start_date && (startDate) <= end_date && dtGridData.Rows[i][FINColumnConstants.CURRENCY_ID].ToString() == currencyId && dtGridData.Rows[i][FINColumnConstants.ACCT_STRUCT_ID].ToString() == accountStru && dtGridData.Rows[i][FINColumnConstants.CAL_ID].ToString() == calId)
                                {
                                    ErrorCollection.Remove("Start");
                                    ErrorCollection.Add("Start", "Currency and Accounting Structure is already exists");
                                    break;
                                }

                                if (endDate != null)
                                {
                                    if ((endDate) >= start_date && (endDate) <= end_date && dtGridData.Rows[i][FINColumnConstants.CURRENCY_ID].ToString() == currencyId && dtGridData.Rows[i][FINColumnConstants.ACCT_STRUCT_ID].ToString() == accountStru && dtGridData.Rows[i][FINColumnConstants.CAL_ID].ToString() == calId)
                                    {
                                        ErrorCollection.Remove("Start");
                                        ErrorCollection.Add("Start", "Currency and Accounting Structure is already exists");
                                        break;
                                    }
                                }

                            }
                        }
                    }
                }
            }
            return ErrorCollection;
        }


        public static DataTable getCompanyCurrency(string comp_id)
        {
            return DBMethod.ExecuteQuery(FIN.DAL.GL.Organisation_DAL.getCompanyCurrency(comp_id)).Tables[0];
        }
    }
}
