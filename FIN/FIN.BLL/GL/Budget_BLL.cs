﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Entities;
using FIN.DAL;
using FIN.BLL;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;


using VMVServices.Services.Data;
using VMVServices.Services.Business;
using System.Configuration;

using System.Data.EntityClient;
using Oracle.DataAccess.Client;
using System.Data.Common;

namespace FIN.BLL.GL
{
    public static class Budget_BLL
    {
        static DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();

        public static DataTable getActualAndProjectedBudgetDetails(string Str_ID, string accCodeId = "", string IsCodeORGroup = "")
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.GL.Budget_DAL.getActualAndProjectedBudgetDetails(Str_ID, accCodeId, IsCodeORGroup)).Tables[0];
            return dt_Det;
        }

        
        public static DataTable getBudgetDetails(string Str_ID, string accCodeId = "", string IsCodeORGroup="")
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.GL.Budget_DAL.getBudgetDetails(Str_ID, accCodeId, IsCodeORGroup)).Tables[0];
            return dt_Det;
        }
        public static DataTable getGroupAcCodeDetails()
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.GL.Budget_DAL.getGroupAcCodeDetails()).Tables[0];
            return dt_Det;
        }
        public static DataTable GetGroupAcCodeAmt(string accGrpId, string prevYrId = "", string IsCodeORGroup = "", string recordId = "")
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.GL.Budget_DAL.GetGroupAcCodeAmt(accGrpId, prevYrId, IsCodeORGroup, recordId)).Tables[0];
            return dt_Det;
        }
        public static DataTable GetGroupAcCodeJournalAmt(string accGrpId, string prevYrId = "", string IsCodeORGroup="")
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.GL.Budget_DAL.GetGroupAcCodeJournalAmt(accGrpId, prevYrId, IsCodeORGroup)).Tables[0];
            return dt_Det;
        }
        public static void GetBudgetCalYear(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(FIN.DAL.GL.Budget_DAL.GetBudgetCalYear()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "CAL_ACCT_YEAR", FINColumnConstants.CAL_DTL_ID, dtDropDownData, true, false);
        }
        public static void getGroupDetails(ref DropDownList ddlList,string calYrId)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.GL.Budget_DAL.getBudgetGroupDetails(calYrId)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.GROUP_NAME, FINColumnConstants.GROUP_ID, dt_Det, true, false);
        }
        public static DataTable getGroupAcCodeDetailsBasedOrg(string orgId, string groupId)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.GL.Budget_DAL.getGroupAcCodeDetailsBasedOrg(orgId, groupId)).Tables[0];
            return dt_Det;
        }
        public static DataTable getGroupDetails()
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.GL.Budget_DAL.getGroupDetails()).Tables[0];
            return dt_Det;
        }
        public static DataTable getGroupAcCodeBASEDGroup(string groupId)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.GL.Budget_DAL.getGroupAcCodeBASEDGroup(groupId)).Tables[0];
            return dt_Det;
        }
        ////        # region FillCombo
        //public static void fn_getAccountGroup(ref DropDownList ddlList)
        //{
        //    // DataTable dt_Det = new DataTable();
        //    DataTable dt_Det = DBMethod.ExecuteQuery(FIN.BLL.GL.AccountingGroups_BLL.fn_getAccountGroup()).Tables[0];
        //    CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.GROUP_NAME, FINColumnConstants.GROUP_ID, dt_Det, true, false);
        //}

        //public static void fn_getAccount(ref DropDownList ddlList)
        //{
        //    // DataTable dt_Det = new DataTable();
        //    DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.GL.Budget_DAL.getAccount()).Tables[0];
        //    CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.GROUP_NAME, FINColumnConstants.GROUP_ID, dt_Det, true, false);
        //}
        
        ////        # endregion


        public static void SavePCEntity<T, TC>(T entity, List<Tuple<object, string>> tmpChildEntity, TC ChildEntity, bool modifyMode = false)
            where T : class
            where TC : class
        {
            try
            {
                DBMethod.SavePCEntity<T, TC>(entity, tmpChildEntity, ChildEntity, modifyMode);
            }
            catch (Exception ex)
            {
                throw ex.InnerException;
            }

        }


        public static GL_ACCT_GROUP_LINK_DTL getClassEntity(string str_Id)
        {
            GL_ACCT_GROUP_LINK_DTL obj_GL_ACCT_GROUP_LINK_DTL = new GL_ACCT_GROUP_LINK_DTL();
            using (IRepository<GL_ACCT_GROUP_LINK_DTL> userCtx = new DataRepository<GL_ACCT_GROUP_LINK_DTL>())
            {
                obj_GL_ACCT_GROUP_LINK_DTL = userCtx.Find(r =>
                    (r.ACCT_GRP_LNK_ID == str_Id)
                    ).SingleOrDefault();
            }


            return obj_GL_ACCT_GROUP_LINK_DTL;
        }


        public static GL_ACCT_GROUP_LINK_DTL getDetailClassEntity(string str_Id)
        {
            GL_ACCT_GROUP_LINK_DTL obj_GL_ACCT_GROUP_LINK_DTL = new GL_ACCT_GROUP_LINK_DTL();
            using (IRepository<GL_ACCT_GROUP_LINK_DTL> userCtx = new DataRepository<GL_ACCT_GROUP_LINK_DTL>())
            {
                obj_GL_ACCT_GROUP_LINK_DTL = userCtx.Find(r =>
                    (r.ACCT_GRP_LNK_DTL_ID == str_Id)
                    ).SingleOrDefault();
            }
            return obj_GL_ACCT_GROUP_LINK_DTL;
        }



        public static String ValidateEntity(GL_ACCT_GROUP_LINK_DTL obj_GL_ACCT_GROUP_LINK_DTL)
        {
            string str_Error = "";
            return str_Error;
        }


        public static String DeleteEntity(int int_PKID)
        {
            string str_Message = "";
            GL_ACCT_GROUP_LINK_DTL obj_GL_ACCT_GROUP_LINK_DTL = new GL_ACCT_GROUP_LINK_DTL();
            obj_GL_ACCT_GROUP_LINK_DTL.PK_ID = int_PKID;
            DBMethod.DeleteEntity<GL_ACCT_GROUP_LINK_DTL>(obj_GL_ACCT_GROUP_LINK_DTL);
            return str_Message;
        }
    }
}
