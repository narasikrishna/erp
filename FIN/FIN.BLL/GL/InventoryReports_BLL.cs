﻿using System.Linq;
using System.Text;

using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.AP;
using VMVServices.Web;
using System.Data;

namespace FIN.BLL.GL
{
    public class InventoryReports_BLL
    {
        static DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();
        public static DataSet GetReportData()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.GL.InventoryReports_DAL.getReportData());
        }
        public static void GetWareHouseName(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(FIN.DAL.GL.InventoryReports_DAL.getWareHouseName()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.INV_WH_NAME, FINColumnConstants.INV_WH_ID, dtDropDownData, false, true);
        }
        public static DataSet GetInventoryCostReportData()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.GL.InventoryReports_DAL.getInventoryCostReportData());
        }
    }
}
