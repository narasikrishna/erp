﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.AP;
using VMVServices.Web;
using System.Data;

namespace FIN.BLL.GL
{
   public class TrailBalance_BLL
    {
       static DataTable dtDropDownData = new DataTable();
        public static string  GetReportData()
        {
            return FIN.DAL.GL.TrailBalance_DAL.getReportData();
        }
        public static void GetGroupName(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(FIN.DAL.GL.TrailBalance_DAL.getGlobalSegment()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "SEGMENT_VALUE","SEGMENT_ID", dtDropDownData, false, true);
        }
        public static string GetTrailBalanceDetailReportData()
        {
            return FIN.DAL.GL.TrailBalance_DAL.getTrailBalanceDetailReportData();
        }
        public static string GetAccountCostSummary()
        {
            return FIN.DAL.GL.TrailBalance_DAL.getAccountCostCenterSummary();
        }
       
    }
}
