﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.AP;
using FIN.DAL.GL;
using VMVServices.Web;
using System.Data;

namespace FIN.BLL.GL
{
    public class JournalEntry_BLL
    {

        static DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();

        public static void getDistinctJERefernce(ref DropDownList ddlList, string str_fromdate,string str_todate, Boolean bol_Sel=true )
        {
            dtDropDownData = DBMethod.ExecuteQuery(FIN.DAL.GL.JournalEntry_DAL.getDistinctJEReference(str_fromdate, str_todate)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "je_REFERENCE", "je_REFERENCE", dtDropDownData, bol_Sel, !bol_Sel);
        }

        public static string CalculateDrAmount(DataTable dtGridData, decimal currentAmount)
        {
            decimal CalculateAmount = currentAmount;

            if (dtGridData != null)
            {
                if (dtGridData.Rows.Count >= 1)
                {
                    for (int i = 0; i < dtGridData.Rows.Count; i++)
                    {
                        if (dtGridData.Rows[i]["JE_CONVERTED_AMT_dr"].ToString() != null)
                        {
                            CalculateAmount = CalculateAmount + CommonUtils.ConvertStringToDecimal(dtGridData.Rows[i]["JE_CONVERTED_AMT_dr"].ToString());
                        }                     
                    }
                }
                else
                {
                    CalculateAmount = currentAmount;
                }
            }
            return CalculateAmount.ToString();
        }
        public static string CalculateCrAmount(DataTable dtGridData, decimal currentAmount)
        {
            decimal CalculateAmount = currentAmount;

            if (dtGridData != null)
            {
                if (dtGridData.Rows.Count >= 1)
                {
                    for (int i = 0; i < dtGridData.Rows.Count; i++)
                    {
                        if (dtGridData.Rows[i]["JE_ACCOUNTED_AMT_CR"].ToString() != null)
                        {
                            CalculateAmount = CalculateAmount + CommonUtils.ConvertStringToDecimal(dtGridData.Rows[i]["JE_ACCOUNTED_AMT_CR"].ToString());
                        }
                    }
                }
                else
                {
                    CalculateAmount = currentAmount;
                }
            }
            return CalculateAmount.ToString();
        }

        public static DataSet GetReportData()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.GL.JournalEntry_DAL.getReportData());
        }
        public static void GetjournalNumber(ref DropDownList ddlList,string Fromdate,string ToDate)
        {
            dtDropDownData = DBMethod.ExecuteQuery(JournalEntry_DAL.getJournalNumber(Fromdate,ToDate)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "je_number", "je_number", dtDropDownData, false, true);
        }
    }
}
