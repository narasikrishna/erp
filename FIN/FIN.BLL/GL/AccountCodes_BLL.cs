﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Entities;
using FIN.DAL;
using FIN.BLL;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;


using VMVServices.Services.Data;
using VMVServices.Services.Business;
using System.Configuration;

using System.Data.EntityClient;
using Oracle.DataAccess.Client;
using System.Data.Common;

namespace FIN.BLL.GL
{
    public static class AccountCodes_BLL
    {

        public static DataTable getChildEntityDet(string Str_ID)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountCodes_DAL.getSegmentDetails(Str_ID)).Tables[0];
            return dt_Det;
        }


        //        # region FillCombo

        public static void fn_getTopLevelAcctGroup(ref DropDownList ddlList, Boolean bol_ALL = true)
        {
            // DataTable dt_Det = new DataTable();
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountCodes_DAL.getTopLevelAcctGroup()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "LEVEL1", "LEVEL1_ID", dt_Det, !bol_ALL, bol_ALL);
        }

        public static void fn_getGroupnameinaccount(ref DropDownList ddlList, Boolean bol_select = true)
        {
            // DataTable dt_Det = new DataTable();
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountCodes_DAL.getGroupnameinaccount()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "Group_Name", "acct_grp_id", dt_Det, bol_select, !bol_select);
        }

        public static void fn_geAccountStartWith(ref DropDownList ddlList, Boolean bol_select = true)
        {
            // DataTable dt_Det = new DataTable();
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountCodes_DAL.getAccountCodeStarts()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "acct_code_start", "acct_code_start", dt_Det, bol_select, !bol_select);
        }



        public static void fn_getAccount(ref DropDownList ddlList)
        {
            // DataTable dt_Det = new DataTable();
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountCodes_DAL.getAccount()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.CODE_NAME, FINColumnConstants.CODE_ID, dt_Det, true, false);
        }

        public static void fn_getAccount_BasedonAcctStruct(ref DropDownList ddlList)
        {
            // DataTable dt_Det = new DataTable();
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountCodes_DAL.getAccount_BasedonAcctStruct()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.CODE_NAME, FINColumnConstants.CODE_ID, dt_Det, true, false);
        }

        public static void getAccountBasedGroup(ref DropDownList ddlList, string accGrpId)
        {
            // DataTable dt_Det = new DataTable();
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountCodes_DAL.getAccountBasedGroup(accGrpId)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.CODE_NAME, FINColumnConstants.CODE_ID, dt_Det, true, false);
        }

        public static void fn_getAccCodeBasedSegment(ref DropDownList ddlList, string segmentValueId)
        {
            // DataTable dt_Det = new DataTable();
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountCodes_DAL.getAccCodeBasedSegment(segmentValueId)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.CODE_NAME, FINColumnConstants.CODE_ID, dt_Det, true, false);
        }
        public static void getAccCodes(ref DropDownList ddlList)
        {
            // DataTable dt_Det = new DataTable();
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountCodes_DAL.getAccCodes()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.CODE_NAME, FINColumnConstants.CODE_ID, dt_Det, true, false);
        }
        public static void getAccCodeswithAll(ref DropDownList ddlList)
        {
            // DataTable dt_Det = new DataTable();
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountCodes_DAL.getAccCodes()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.CODE_NAME, FINColumnConstants.CODE_ID, dt_Det, false, true);
        }
        public static void fn_getAccCodeBasedSegment_frall(ref DropDownList ddlList, string segmentValueId)
        {
            // DataTable dt_Det = new DataTable();
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountCodes_DAL.getAccCodeBasedSegment(segmentValueId)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.CODE_NAME, "ACCT_CODE", dt_Det, false, true);
        }

        public static void getAccCodeBasedOrg(ref DropDownList ddlList, string orgId, string mode = "",Boolean bol_sel=true)
        {
            // DataTable dt_Det = new DataTable();
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountCodes_DAL.getAccCodeBasedOrg(orgId, mode)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.CODE_NAME, FINColumnConstants.CODE_ID, dt_Det, bol_sel, !bol_sel);
        }
        public static void getNonCtrlAccCodeBasedOrg(ref DropDownList ddlList, string orgId, string mode = "", bool fundTrans = false)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountCodes_DAL.getNonCtrlAccCodeBasedOrg(orgId, mode, fundTrans)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.CODE_NAME, FINColumnConstants.CODE_ID, dt_Det, true, false);
        }

        public static void getAccountWithControlAC(ref DropDownList ddlList)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountCodes_DAL.getAccountWithControlAC()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.CODE_NAME, FINColumnConstants.CODE_ID, dt_Det, true, false);
        }

        public static void fn_getUserdtls(ref DropDownList ddlList)
        {
            // DataTable dt_Det = new DataTable();
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountCodes_DAL.getUserdtls()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.USER_CODE, FINColumnConstants.USER_CODE, dt_Det, true, false);
        }

        public static void fn_getAccountdtls(ref DropDownList ddlList)
        {
            // DataTable dt_Det = new DataTable();
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountCodes_DAL.getAccount()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.CODE_NAME, FINColumnConstants.CODE_ID, dt_Det, true, false);
        }


        public static void getAccCodeBasedStartupLetter(ref DropDownList ddlList, string startWith)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountCodes_DAL.getAccCodeBasedStartupLetter(startWith)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.CODE_NAME, FINColumnConstants.ACCT_CODE, dt_Det, false, true);
        }
        public static void getAccCodeBasedOrgRep(ref DropDownList ddlList)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountCodes_DAL.getAccCodeBasedOrgRep()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.CODE_NAME, FINColumnConstants.ACCT_CODE, dt_Det, false, true);
        }
        public static void getAccCodeBasedOrgR(ref DropDownList ddlList)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountCodes_DAL.getAccCodeBasedOrgRepForR()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.CODE_NAME, FINColumnConstants.ACCT_CODE, dt_Det, false, true);
        }
        public static void getRevenueAccCodeBasedOrgRep(ref DropDownList ddlList)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountCodes_DAL.getRevenueAccCodeBasedOrgRep()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.CODE_NAME, FINColumnConstants.ACCT_CODE, dt_Det, false, true);
        }
        public static void getAccCodeBasedOrgExp(ref DropDownList ddlList)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountCodes_DAL.getExpAccCodeBasedOrgRep()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.CODE_NAME, FINColumnConstants.ACCT_CODE, dt_Det, false, true);
        }



        //        # endregion


        public static void SavePCEntity<T, TC>(T entity, List<Tuple<object, string>> tmpChildEntity, TC ChildEntity, bool modifyMode = false)
            where T : class
            where TC : class
        {
            try
            {
                DBMethod.SavePCEntity<T, TC>(entity, tmpChildEntity, ChildEntity, modifyMode);
            }
            catch (Exception ex)
            {
                throw ex.InnerException;
            }

        }
        //public static void SavePCEntity<T, TC>(T entity, List<Tuple<object, string>> tmpChildEntity, TC ChildEntity, bool modifyMode = false)
        //    where T : class
        //    where TC : class
        //{
        //    try
        //    {
        //        FINEntities context = new FINEntities();
        //        DbTransaction transaction = null;

        //        try
        //        {
        //            context.Connection.Open();
        //            transaction = context.Connection.BeginTransaction();

        //            DBMethod.SavePCEntity1<T, TC>(entity, tmpChildEntity, ChildEntity,context, modifyMode);

        //            transaction.Commit();
        //        }
        //        catch (Exception ex)
        //        {
        //            throw ex.InnerException;
        //        }
        //        finally
        //        {
        //            context.Connection.Close();
        //            transaction = null;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex.InnerException;
        //    }
        //}


        public static GL_ACCT_CODES getClassEntity(string str_Id)
        {
            GL_ACCT_CODES obj_GL_ACCT_CODES = new GL_ACCT_CODES();
            using (IRepository<GL_ACCT_CODES> userCtx = new DataRepository<GL_ACCT_CODES>())
            {
                obj_GL_ACCT_CODES = userCtx.Find(r =>
                    (r.ACCT_CODE_ID == str_Id)
                    ).SingleOrDefault();
            }


            return obj_GL_ACCT_CODES;
        }
        public static String ValidateEntity(GL_ACCT_CODES obj_GL_ACCT_CODES)
        {
            string str_Error = "";
            return str_Error;
        }


        public static String DeleteEntity(int int_PKID)
        {
            string str_Message = "";
            GL_ACCT_GROUPS obj_GL_ACCT_GROUPS = new GL_ACCT_GROUPS();
            obj_GL_ACCT_GROUPS.PK_ID = int_PKID;
            DBMethod.DeleteEntity<GL_ACCT_GROUPS>(obj_GL_ACCT_GROUPS);
            return str_Message;
        }

        public static void getSegmentValues(ref DropDownList ddlList1, ref DropDownList ddlList2, ref DropDownList ddlList3, ref DropDownList ddlList4, ref DropDownList ddlList5, ref DropDownList ddlList6, string str_AccCodeId)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountCodes_DAL.getSegmentValues(str_AccCodeId)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList1, FINColumnConstants.SEGMENT_VALUE, FINColumnConstants.SEGMENT_VALUE_ID, dt_Det, true, false);
            CommonUtils.LoadDropDownList(ddlList2, FINColumnConstants.SEGMENT_VALUE, FINColumnConstants.SEGMENT_VALUE_ID, dt_Det, true, false);
            CommonUtils.LoadDropDownList(ddlList3, FINColumnConstants.SEGMENT_VALUE, FINColumnConstants.SEGMENT_VALUE_ID, dt_Det, true, false);
            CommonUtils.LoadDropDownList(ddlList4, FINColumnConstants.SEGMENT_VALUE, FINColumnConstants.SEGMENT_VALUE_ID, dt_Det, true, false);
            CommonUtils.LoadDropDownList(ddlList5, FINColumnConstants.SEGMENT_VALUE, FINColumnConstants.SEGMENT_VALUE_ID, dt_Det, true, false);
            CommonUtils.LoadDropDownList(ddlList6, FINColumnConstants.SEGMENT_VALUE, FINColumnConstants.SEGMENT_VALUE_ID, dt_Det, true, false);
        }

        public static void getAccCodeswithOldAccCode(ref DropDownList ddlList)
        {
            // DataTable dt_Det = new DataTable();
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountCodes_DAL.getAccCodeswithOldAccCode()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.CODE_NAME, FINColumnConstants.CODE_ID, dt_Det, true, false);
        }

        public static void getAccCodeswithR(ref DropDownList ddlList)
        {
            // DataTable dt_Det = new DataTable();
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountCodes_DAL.getAccCodeswithR()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.CODE_NAME, FINColumnConstants.CODE_ID, dt_Det, false, true);
        }
    }
}
