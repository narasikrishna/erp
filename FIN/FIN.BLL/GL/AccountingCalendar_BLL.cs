﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.GL;
using FIN.DAL.HR;
using System.Data;
using System.Collections;

namespace FIN.BLL.GL
{
    public class AccountingCalendar_BLL
    {
        static DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();

        # region FillCombo
        public static void GetFinancialYear(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(AccountingCalendar_DAL.GetFinancialYear()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.CAL_DESC, FINColumnConstants.CAL_ID, dtDropDownData, true, false);
        }

        public static void GetFinancialPeriod4Year(ref DropDownList ddlList,string str_caldtlid,Boolean bol_Sel=true)
        {
            dtDropDownData = DBMethod.ExecuteQuery(AccountingCalendar_DAL.getPerioddtl(str_caldtlid)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.PERIOD_NAME, FINColumnConstants.PERIOD_ID, dtDropDownData, bol_Sel, !bol_Sel);
        }
        public static void GetFinancialPeriod4YearAndActual(ref DropDownList ddlList, string str_caldtlid)
        {
            dtDropDownData = DBMethod.ExecuteQuery(AccountingCalendar_DAL.getPerioddtl4ActualPeriod(str_caldtlid)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.PERIOD_NAME, FINColumnConstants.PERIOD_ID, dtDropDownData, true, false);
        }
        public static void GetFinclYear(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(AccountingCalendar_DAL.GetFinancialYear()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.CAL_DESC, FINColumnConstants.CAL_ID, dtDropDownData, true, false);
        }

        public static void GetCalAcctYear(ref DropDownList ddlList,Boolean bol_sel=true )
        {
            dtDropDownData = DBMethod.ExecuteQuery(AccountingCalendar_DAL.GetCalAcctYear()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "CAL_ACCT_YEAR", FINColumnConstants.CAL_DTL_ID, dtDropDownData, bol_sel, !bol_sel);
        }
        public static void GetPreviousCalAcctYear(ref DropDownList ddlList, string calDtlId = "")
        {
            dtDropDownData = DBMethod.ExecuteQuery(AccountingCalendar_DAL.GetPreviousCalAcctYear(calDtlId)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "CAL_ACCT_YEAR", FINColumnConstants.CAL_DTL_ID, dtDropDownData, true, false);
        }
        public static void GetCalYear(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(AccountingCalendar_DAL.GetCalYear()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "CAL_ACCT_YEAR", FINColumnConstants.CAL_DTL_ID, dtDropDownData, true, false);
        }



        public static void GetAccPeriodBasedOrg(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(AccountingCalendar_DAL.GetAccPeriodBasedOrg()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.PERIOD_NAME, FINColumnConstants.PERIOD_ID, dtDropDownData, true, false);
        }

        public static void GetAccPeriodBasedOrg4NotNOPPeriod(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(AccountingCalendar_DAL.GetAccPeriodBasedOrg4NotNOPPeriod()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.PERIOD_NAME, FINColumnConstants.PERIOD_ID, dtDropDownData, true, false);
        }
        public static void GetAccPeriodBasedOrg4NotNOPPeriods(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(AccountingCalendar_DAL.GetAccPeriodBasedOrg4NotNOPPeriod()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.PERIOD_NAME, FINColumnConstants.PERIOD_ID, dtDropDownData, false, true);
        }
        public static void GetAccPeriodBasedOrg4NotNOPPeriodANDYear(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(AccountingCalendar_DAL.GetAccPeriodBasedOrg4NotNOPPeriodANDYear()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.PERIOD_NAME, FINColumnConstants.PERIOD_ID, dtDropDownData, true, false);
        }
        


        public static void GetAccPeriodBasedOrgJDate(ref DropDownList ddlList, string journalDate)
        {
            dtDropDownData = DBMethod.ExecuteQuery(AccountingCalendar_DAL.GetAccPeriodBasedOrgJDate(journalDate)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.PERIOD_NAME, FINColumnConstants.PERIOD_ID, dtDropDownData, true, false);
        }
        public static void GetCompPeriodBasedOrgJDate(ref DropDownList ddlList, string journalDate)
        {
            dtDropDownData = DBMethod.ExecuteQuery(AccountingCalendar_DAL.GetCompPeriodBasedOrgJDate(journalDate)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.PERIOD_NAME, FINColumnConstants.PERIOD_ID, dtDropDownData, true, false);
        }
        public static void GetAccPeriodBasedNotNOPPeriodSegmentBal(ref DropDownList ddlList, string str_caldtlid)
        {
            dtDropDownData = DBMethod.ExecuteQuery(AccountingCalendar_DAL.GetAccPeriodBasedNotNOPPeriodSegmentBal(str_caldtlid)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.PERIOD_NAME, FINColumnConstants.PERIOD_ID, dtDropDownData, true, false);
        }
        # endregion

        public static SortedList Validate(GL_ACCT_CALENDAR_HDR gL_ACCT_CALENDAR_HDR, string appMode, DateTime periodStartDate, DateTime periodEndDate)
        {
            SortedList errorCollection = new SortedList();


            if (gL_ACCT_CALENDAR_HDR.CAL_EFF_END_DT < periodStartDate)
            {
                errorCollection.Remove("period_validation1");
                errorCollection.Add("period_validation1", "Begin Date cannot be less than the Master section End Date");
            }
            if (gL_ACCT_CALENDAR_HDR.CAL_EFF_START_DT > periodStartDate)
            {
                errorCollection.Remove("period_validation2");
                errorCollection.Add("period_validation2", "Begin Date cannot be less than the Effective date in the master section");
            }
            if (gL_ACCT_CALENDAR_HDR.CAL_EFF_START_DT > periodEndDate)
            {
                errorCollection.Remove("period_validation3");
                errorCollection.Add("period_validation3", "Detail Level End date cannot be less than the Effective date in the master section");
            }
            if (gL_ACCT_CALENDAR_HDR.CAL_EFF_END_DT < periodEndDate)
            {
                errorCollection.Remove("period_validation4");
                errorCollection.Add("period_validation4", "Detail Level End date cannot be greater than the End date in the master section");
            }
            if (gL_ACCT_CALENDAR_HDR.CAL_EFF_START_DT >= gL_ACCT_CALENDAR_HDR.CAL_EFF_END_DT)
            {
                errorCollection.Remove("period_validation5");
                errorCollection.Add("period_validation5", "Effective date cannot be greater than or equal to end date");
            }
            if (periodStartDate >= periodEndDate)
            {
                errorCollection.Remove("period_validation6");
                errorCollection.Add("period_validation6", "Begin date cannot be greater than or equal to end date");
            }

            return errorCollection;
        }

        public static DataTable getFinanceYear4Date(string str_date)
        {
            dtDropDownData = DBMethod.ExecuteQuery(AccountingCalendar_DAL.getFinanicalYear4Date(str_date)).Tables[0];
            return dtDropDownData;
        }
    }
}
