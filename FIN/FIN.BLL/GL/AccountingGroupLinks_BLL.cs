﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Entities;
using FIN.DAL;
using FIN.DAL.GL;
using FIN.BLL;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;


using VMVServices.Services.Data;
using VMVServices.Services.Business;
using System.Configuration;

using System.Data.EntityClient;
using Oracle.DataAccess.Client;
using System.Data.Common;
using System.Collections;
namespace FIN.BLL.GL
{
    public static class AccountingGroupLinks_BLL
    {
        
        static DataTable dtDropDownData = new DataTable();

        public static DataTable getAccountingGroupLinksDetails(string Str_ID)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountingGroupLinks_DAL.getAccountingGroupLinksDetails(Str_ID)).Tables[0];
            return dt_Det;
        }
        public static DataTable getAccountingGroupDetails(string Str_ID)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountingGroupLinks_DAL.getAccountingGroupDetails(Str_ID)).Tables[0];
            return dt_Det;
        }

        # region FillCombo
        public static void fn_getAccountGroupName(ref DropDownList ddlList)
        {
            // DataTable dt_Det = new DataTable();
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountingGroupLinks_DAL.getAccountGroupName()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.GROUP_NAME, FINColumnConstants.GROUP_ID, dt_Det, true, false);
        }
        public static void  fn_getAccountGroupName4SubGroup(ref  DropDownList ddlList,string str_MasterRecId)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountingGroupLinks_DAL.getAccountGroupName4SubGroup(str_MasterRecId)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.GROUP_NAME, FINColumnConstants.GROUP_ID, dt_Det, true, false);
        }
        # endregion


        public static void SavePCEntity<T, TC>(T entity, List<Tuple<object, string>> tmpChildEntity, TC ChildEntity, bool modifyMode = false)
            where T : class
            where TC : class
        {
            try
            {
                DBMethod.SavePCEntity<T, TC>(entity, tmpChildEntity, ChildEntity, modifyMode);
            }
            catch (Exception ex)
            {
                throw ex.InnerException;
            }

        }


        public static GL_ACCT_GROUP_LINK_HDR getClassEntity(string str_Id)
        {
            GL_ACCT_GROUP_LINK_HDR obj_GL_ACCT_GROUP_LINK_HDR = new GL_ACCT_GROUP_LINK_HDR();
            using (IRepository<GL_ACCT_GROUP_LINK_HDR> userCtx = new DataRepository<GL_ACCT_GROUP_LINK_HDR>())
            {
                obj_GL_ACCT_GROUP_LINK_HDR = userCtx.Find(r =>
                    (r.ACCT_GRP_LNK_ID == str_Id)
                    ).SingleOrDefault();
            }


            return obj_GL_ACCT_GROUP_LINK_HDR;
        }


        public static GL_ACCT_GROUP_LINK_DTL getDetailClassEntity(string str_Id)
        {
            GL_ACCT_GROUP_LINK_DTL obj_GL_ACCT_GROUP_LINK_DTL = new GL_ACCT_GROUP_LINK_DTL();
            using (IRepository<GL_ACCT_GROUP_LINK_DTL> userCtx = new DataRepository<GL_ACCT_GROUP_LINK_DTL>())
            {
                obj_GL_ACCT_GROUP_LINK_DTL = userCtx.Find(r =>
                    (r.ACCT_GRP_LNK_DTL_ID == str_Id)
                    ).SingleOrDefault();
            }
            return obj_GL_ACCT_GROUP_LINK_DTL;
        }



        public static String ValidateEntity(GL_ACCT_GROUP_LINK_DTL obj_GL_ACCT_GROUP_LINK_DTL)
        {
            string str_Error = "";
            return str_Error;
        }


        public static String DeleteEntity(int int_PKID)
        {
            string str_Message = "";
            GL_ACCT_GROUP_LINK_DTL obj_GL_ACCT_GROUP_LINK_DTL = new GL_ACCT_GROUP_LINK_DTL();
            obj_GL_ACCT_GROUP_LINK_DTL.PK_ID = int_PKID;
            DBMethod.DeleteEntity<GL_ACCT_GROUP_LINK_DTL>(obj_GL_ACCT_GROUP_LINK_DTL);
            return str_Message;
        }

        public static DataSet GetReportData()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.GL.AccountingGroupLinks_DAL.getReportData());
        }
        public static DataSet getAccountGroupListsBasedLevel()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.GL.AccountingGroupLinks_DAL.getAccountGroupListsBasedLevel());
        }
        public static void GetGroupName(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(AccountingGroupLinks_DAL.getGroupLinkDetails()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.GROUP_NAME, FINColumnConstants.GROUP_ID, dtDropDownData, false,true);
        }

        public static void GetGroupName4Header(ref DropDownList ddlList,string  str_mode)
        {
            dtDropDownData = DBMethod.ExecuteQuery(AccountingGroupLinks_DAL.GetGroupName4Header(str_mode)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.GROUP_NAME, FINColumnConstants.GROUP_ID, dtDropDownData, true, false);
      
        }

        public static void GetGroupName4QuaterlyReport(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(AccountingGroupLinks_DAL.getGroupLinkDetails()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.GROUP_NAME, FINColumnConstants.GROUP_ID, dtDropDownData, false, true);

        }
        public static DataSet GetAccountBalanceReportData()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.GL.AccountingGroupLinks_DAL.getAccountBalanceReportData());
        }


    }
}
