﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Entities;
using FIN.DAL;
using FIN.BLL;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;


using VMVServices.Services.Data;
using VMVServices.Services.Business;
using System.Configuration;

using System.Data.EntityClient;
using Oracle.DataAccess.Client;
using System.Data.Common;

namespace FIN.BLL.GL
{
   public class AccountingGroupDefaultSegment_BLL
    {

        public static DataTable getChildEntityDet(string Str_ID)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountingGroupDefaultSegment_DAL.getAcctGrpDefSegDtl(Str_ID)).Tables[0];
            return dt_Det;
        }

        # region FillCombo
        public static void fn_getGroupName(ref DropDownList ddlList, string mode, bool includeAll=false)
        {
            // DataTable dt_Det = new DataTable();
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountingGroupDefaultSegment_DAL.getGroupName(mode)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.ACCT_GRP_DESC, FINColumnConstants.ACCT_GRP_ID, dt_Det, !includeAll, includeAll);
        }
        # endregion

        //        # region FillCombo
        //public static void fn_getAccount(ref DropDownList ddlList)
        //{
        //    // DataTable dt_Det = new DataTable();
        //    DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountCodes_DAL.getAccount()).Tables[0];
        //    CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.CODE_NAME, FINColumnConstants.CODE_ID, dt_Det, true, false);
        //}
       


        //        # endregion


        public static void SavePCEntity<T, TC>(T entity, List<Tuple<object, string>> tmpChildEntity, TC ChildEntity, bool modifyMode = false)
            where T : class
            where TC : class
        {
            try
            {
                DBMethod.SavePCEntity<T, TC>(entity, tmpChildEntity, ChildEntity, modifyMode);
            }
            catch (Exception ex)
            {
                throw ex.InnerException;
            }

        }
        //public static void SavePCEntity<T, TC>(T entity, List<Tuple<object, string>> tmpChildEntity, TC ChildEntity, bool modifyMode = false)
        //    where T : class
        //    where TC : class
        //{
        //    try
        //    {
        //        FINEntities context = new FINEntities();
        //        DbTransaction transaction = null;

        //        try
        //        {
        //            context.Connection.Open();
        //            transaction = context.Connection.BeginTransaction();

        //            DBMethod.SavePCEntity1<T, TC>(entity, tmpChildEntity, ChildEntity,context, modifyMode);

        //            transaction.Commit();
        //        }
        //        catch (Exception ex)
        //        {
        //            throw ex.InnerException;
        //        }
        //        finally
        //        {
        //            context.Connection.Close();
        //            transaction = null;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex.InnerException;
        //    }
        //}


        public static GL_ACCT_GROUP_DEFAULT_SEGMENTS getClassEntity(string str_Id)
        {
            GL_ACCT_GROUP_DEFAULT_SEGMENTS obj_GL_ACCT_GROUP_DEFAULT_SEGMENTS = new GL_ACCT_GROUP_DEFAULT_SEGMENTS();
            using (IRepository<GL_ACCT_GROUP_DEFAULT_SEGMENTS> userCtx = new DataRepository<GL_ACCT_GROUP_DEFAULT_SEGMENTS>())
            {
                obj_GL_ACCT_GROUP_DEFAULT_SEGMENTS = userCtx.Find(r =>
                    (r.ACCT_DEF_ID == str_Id)
                    ).SingleOrDefault();
            }


            return obj_GL_ACCT_GROUP_DEFAULT_SEGMENTS;
        }
        public static String ValidateEntity(GL_ACCT_GROUP_DEFAULT_SEGMENTS obj_GL_ACCT_GROUP_DEFAULT_SEGMENTS)
        {
            string str_Error = "";
            return str_Error;
        }


        public static String DeleteEntity(int int_PKID)
        {
            string str_Message = "";
            GL_ACCT_GROUP_DEFAULT_SEGMENTS obj_GL_ACCT_GROUP_DEFAULT_SEGMENTS = new GL_ACCT_GROUP_DEFAULT_SEGMENTS();
            obj_GL_ACCT_GROUP_DEFAULT_SEGMENTS.PK_ID = int_PKID;
            DBMethod.DeleteEntity<GL_ACCT_GROUP_DEFAULT_SEGMENTS>(obj_GL_ACCT_GROUP_DEFAULT_SEGMENTS);
            return str_Message;
        }

     
    }
}
