﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.DAL.SSM;
using System.Data;


namespace FIN.BLL.SSM
{
    public class RoleModule_BLL
    {
        public static SSM_ROLE_MODULE_INTERSECTS getClassEntity(int pkId)
        {
            SSM_ROLE_MODULE_INTERSECTS SSM_ROLE_MODULE_INTERSECTS = new SSM_ROLE_MODULE_INTERSECTS();
            using (IRepository<SSM_ROLE_MODULE_INTERSECTS> userCtx = new DataRepository<SSM_ROLE_MODULE_INTERSECTS>())
            {
                SSM_ROLE_MODULE_INTERSECTS = userCtx.Find(r =>
                    (r.PK_ID == int.Parse(pkId.ToString()))
                    ).SingleOrDefault();
            }
            return SSM_ROLE_MODULE_INTERSECTS;
        }
    }
}
