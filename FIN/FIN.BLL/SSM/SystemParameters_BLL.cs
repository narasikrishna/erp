﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.DAL.SSM;
using System.Data;


namespace FIN.BLL.SSM
{
    public class SystemParameters_BLL
    {
        public static SSM_SYSTEM_PARAMETERS getClassEntity(int pkId)
        {
            SSM_SYSTEM_PARAMETERS SSM_SYSTEM_PARAMETERS = new SSM_SYSTEM_PARAMETERS();
            using (IRepository<SSM_SYSTEM_PARAMETERS> userCtx = new DataRepository<SSM_SYSTEM_PARAMETERS>())
            {
                SSM_SYSTEM_PARAMETERS = userCtx.Find(r =>
                    (r.PK_ID == int.Parse(pkId.ToString()))
                    ).SingleOrDefault();
            }
            return SSM_SYSTEM_PARAMETERS;
        }

     
    }
}
