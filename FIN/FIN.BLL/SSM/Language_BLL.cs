﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.SSM;
using VMVServices.Web;
using System.Data;

namespace FIN.BLL.SSM
{
    public class Language_BLL
    {
        DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();

        public static void GetLanguageDetails(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Language_DAL.GetLanguageDetails()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.LANGUAGE_DESCRIPTION, FINColumnConstants.LANGUAGE_CODE, dtDropDownData, true, false);
        }
    }
}
