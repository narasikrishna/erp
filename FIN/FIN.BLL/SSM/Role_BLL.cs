﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.DAL.SSM;
using System.Data;


namespace FIN.BLL.SSM
{
    public class Role_BLL
    {
       
        public static SSM_ROLES getClassEntity(int pkId)
        {
            SSM_ROLES SSM_ROLES = new SSM_ROLES();
            using (IRepository<SSM_ROLES> userCtx = new DataRepository<SSM_ROLES>())
            {
                SSM_ROLES = userCtx.Find(r =>
                    (r.PK_ID == int.Parse(pkId.ToString()))
                    ).SingleOrDefault();
            }
            return SSM_ROLES;
        }
        static DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();


        public static void GetRole(ref DropDownList ddlList, string ROLE_CODE = "")
        {
            dtDropDownData = DBMethod.ExecuteQuery(Role_DAL.GetRole(ROLE_CODE)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.ROLE_CODE, FINColumnConstants.ROLE_CODE, dtDropDownData, true, false);
        }

        public static void GetModuleList4Role(ref DropDownList ddlList, string RoleCode )
        {
            dtDropDownData = DBMethod.ExecuteQuery(Role_DAL.GetModuleList4Role(RoleCode)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.MODULE_CODE, FINColumnConstants.MODULE_CODE, dtDropDownData, true, false);
        }
    }
}
