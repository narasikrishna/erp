﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.DAL.SSM;
using System.Data;


namespace FIN.BLL.SSM
{
    public class WorkflowBLL
    {
        public static WFM_WORKFLOW_CODE_MASTERS getClassEntity(int pkId)
        {
            WFM_WORKFLOW_CODE_MASTERS WFM_WORKFLOW_CODE_MASTERS = new WFM_WORKFLOW_CODE_MASTERS();
            using (IRepository<WFM_WORKFLOW_CODE_MASTERS> userCtx = new DataRepository<WFM_WORKFLOW_CODE_MASTERS>())
            {
                WFM_WORKFLOW_CODE_MASTERS = userCtx.Find(r =>
                    (r.PK_ID == int.Parse(pkId.ToString()))
                    ).SingleOrDefault();
            }
            return WFM_WORKFLOW_CODE_MASTERS;
        }
    }
}
