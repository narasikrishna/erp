﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.DAL.SSM;
using System.Data;


namespace FIN.BLL.SSM
{
    public class Alert_BLL
    {
        static DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();
        static DataTable dtData = new DataTable();

        public static void GetAlertName(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Alerts_DAL.GetAlertNM()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "ALERT_MESSAGE", "ALERT_CODE", dtDropDownData, true, false);
        }
        public static void GenerateEmail(string AlertCode = "")
        {
            string emailParam = string.Empty;
            string email = string.Empty;
            string alertMsg = string.Empty;


            emailParam = DBMethod.GetStringValue(SystemParameters_DAL.GetSysEmailParam());

            if (emailParam != string.Empty)
            {
                if (emailParam.ToUpper() == "YES" || emailParam.ToUpper() == "Y")
                {
                    dtData = DBMethod.ExecuteQuery(Alerts_DAL.GetAlertDetails(AlertCode)).Tables[0];

                    if (dtData != null)
                    {
                        if (dtData.Rows.Count > 0)
                        {
                            email = DBMethod.GetStringValue(Employee_DAL.getEmployeeEmail(dtData.Rows[0]["USER_CODE"].ToString()));
                            alertMsg = (dtData.Rows[0]["ALERT_MESSAGE"].ToString());

                            if (email != string.Empty && alertMsg != string.Empty && email != "0")
                            {
                                CommonUtils.GenerateEmail(email, alertMsg);
                            }
                        }
                    }
                }
            }
        }
    }
}
