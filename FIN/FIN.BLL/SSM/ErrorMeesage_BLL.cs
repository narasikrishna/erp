﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.DAL.SSM;
using System.Data;


namespace FIN.BLL.SSM
{
    public class ErrorMeesage_BLL
    {
        public static SSM_ERROR_MESSAGES getClassEntity(int pkId)
        {
            SSM_ERROR_MESSAGES SSM_ERROR_MESSAGES = new SSM_ERROR_MESSAGES();
            using (IRepository<SSM_ERROR_MESSAGES> userCtx = new DataRepository<SSM_ERROR_MESSAGES>())
            {
                SSM_ERROR_MESSAGES = userCtx.Find(r =>
                    (r.PK_ID == int.Parse(pkId.ToString()))
                    ).SingleOrDefault();
            }
            return SSM_ERROR_MESSAGES;
        }
    }
}
