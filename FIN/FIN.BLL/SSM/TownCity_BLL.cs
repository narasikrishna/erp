﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.DAL.SSM;
using System.Data;

namespace FIN.BLL.SSM
{
    public class TownCity_BLL
    {
        static DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();

        public static SSM_CITIES getClassEntity(int pkId)
        {
            SSM_CITIES SSM_CITIES = new SSM_CITIES();
            using (IRepository<SSM_CITIES> userCtx = new DataRepository<SSM_CITIES>())
            {
                SSM_CITIES = userCtx.Find(r =>
                    (r.PK_ID == int.Parse(pkId.ToString()))
                    ).SingleOrDefault();
            }
            return SSM_CITIES;
        }


        public static void getCity(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(FIN.DAL.SSM.TownCity_DAL.getCity()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.FULL_NAME, FINColumnConstants.CITY_CODE, dtDropDownData, true, false);
        }
    }
}
