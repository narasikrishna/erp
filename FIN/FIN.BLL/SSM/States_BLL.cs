﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.DAL.SSM;
using System.Data;


namespace FIN.BLL.SSM
{
    public class States_BLL
    {
        static DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();


        public static void getStateDetails(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(FIN.DAL.SSM.States_DAL.getStateDetails()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.FULL_NAME, FINColumnConstants.STATE_CODE, dtDropDownData, true, false);
        }
        public static void getStateDetailsBasedCountry(ref DropDownList ddlList,string CountryCode)
        {
            dtDropDownData = DBMethod.ExecuteQuery(FIN.DAL.SSM.States_DAL.getStateDetailsBasedCountry(CountryCode)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.STATE_CODE, FINColumnConstants.STATE_CODE, dtDropDownData, true, false);
        }
      
    }
}
