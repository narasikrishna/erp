﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.DAL.SSM;
using System.Data;

namespace FIN.BLL.SSM
{
    public class TableColumnStruct_BLL
    {
        static DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();


        public static void GetTableName(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(TableColumnStruct_DAL.GetTableName()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "TABLE_NAME", "TABLE_NAME", dtDropDownData, true, false);
        }

        public static void GetColName(ref DropDownList ddlList, string TABLE_NAME)
        {
            dtDropDownData = DBMethod.ExecuteQuery(TableColumnStruct_DAL.GetColName(TABLE_NAME)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "COLUMN_NAME", "COLUMN_NAME", dtDropDownData, true, false);
        }

        public static void GetScreenName(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(TableColumnStruct_DAL.GetScreenName()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "SCREEN_NAME", "SCREEN_CODE", dtDropDownData, true, false);
        }

        public static void GetScreenNameinTSD(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(TableColumnStruct_DAL.getScreenNameinTSD()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "SCREEN_NAME", "SCREEN_CODE", dtDropDownData, true, false);
        }
        public static void GetTableNM_BasedScreencode(ref DropDownList ddlList, string screen_code)
        {
            dtDropDownData = DBMethod.ExecuteQuery(TableColumnStruct_DAL.GetTableName_basedscreencode(screen_code)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "TABLE_NAME", "TABLE_NAME", dtDropDownData, true, false);
        }

        public static void getColName4ScreenCode(ref DropDownList ddlList, string screen_code)
        {
            dtDropDownData = DBMethod.ExecuteQuery(TableColumnStruct_DAL.getColName4Screen(screen_code)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "column_name", "tab_col_struct_id", dtDropDownData, true, false);
        }
    }
}
