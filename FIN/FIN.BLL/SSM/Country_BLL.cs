﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Entities;
using FIN.DAL;
using FIN.BLL;
using FIN.BLL.AP;
using FIN.DAL.AP;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;


using VMVServices.Services.Data;
using VMVServices.Services.Business;
using System.Configuration;

using System.Data.EntityClient;
using Oracle.DataAccess.Client;
using System.Data.Common;

namespace FIN.BLL.SSM
{
    public static class Country_BLL
    {
        static DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();

        public static DataTable getCountryDetails(string Master_id)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.SSM.Country_DAL.getCountryDetails(Master_id)).Tables[0];
            return dt_Det;
        }

        public static void GetCurrencyDetails(ref DropDownList ddlList) 
        {
            dtDropDownData = DBMethod.ExecuteQuery(FIN.DAL.SSM.Country_DAL.getCurrency()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.CURRENCY_NAME, FINColumnConstants.CURRENCY_ID, dtDropDownData, true, false);
        }
        public static void getCountryDetails(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(FIN.DAL.SSM.Country_DAL.getCountryDetails()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "COUNTRY_CODE", FINColumnConstants.COUNTRY_CODE, dtDropDownData, true, false);
        }
        public static void getCountryName(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(FIN.DAL.SSM.Country_DAL.getCountryDetails()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "country_name", FINColumnConstants.COUNTRY_CODE, dtDropDownData, false,true);
        }
        public static void getNationalityName(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(FIN.DAL.SSM.Country_DAL.getCountryDetails()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "Nationality", FINColumnConstants.COUNTRY_CODE, dtDropDownData, true, false);
        }
        public static void getSupplierType(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(FIN.DAL.Supplier_DAL.getSupplierNameType()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "vendor_type", FINColumnConstants.VENDOR_TYPE, dtDropDownData, false, true);
        }
        public static void getCurrencyCode(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(FIN.DAL.SSM.Country_DAL.getCurrencyDetails()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "CURRENCY_CODE", "CURRENCY_ID", dtDropDownData, false, true);
        }
        public static void getCurrencyDescription(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(FIN.DAL.SSM.Country_DAL.getCurrencyDetails()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "currency_desc", "CURRENCY_ID", dtDropDownData, false, true);
        }

        public static DataSet GetReportData()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.Supplier_DAL.getReportData());
        }
        public static DataSet GetCurrencyReportData()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.SSM.Country_DAL.getCurrencyReportData());
        }
     
    }

}