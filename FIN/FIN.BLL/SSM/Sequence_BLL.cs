﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Entities;
using FIN.DAL;
using FIN.BLL;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;


using VMVServices.Services.Data;
using VMVServices.Services.Business;
using System.Configuration;

using System.Data.EntityClient;
using Oracle.DataAccess.Client;
using System.Data.Common;

namespace FIN.BLL.SSM
{
    public static class Sequence_BLL
    {
        static DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();

        public static DataTable getSequenceDetails(int Master_id)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.SSM.Sequence_DAL.getSequenceDetails(Master_id)).Tables[0];
            return dt_Det;
        }

        public static void getScreenCode(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(FIN.DAL.SSM.Sequence_DAL.getScreenCode()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.SCREEN_NAME, FINColumnConstants.SCREEN_CODE, dtDropDownData, true, false);
        }
    }

}