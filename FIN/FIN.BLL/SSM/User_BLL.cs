﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.DAL.SSM;
using System.Data;


namespace FIN.BLL.SSM
{
    public class User_BLL
    {
        static DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();


        public static void GetUserData(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(User_DAL.GetUserData()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.USER_NAME, FINColumnConstants.USER_CODE, dtDropDownData, true, false);
        }
        public static void GetUser(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(User_DAL.GetUserData()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.USER_NAME, FINColumnConstants.USER_CODE, dtDropDownData, false, true);
        }
        public static void GetCreatedby(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(User_DAL.getCreatedBy()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "USER_NAME", "USER_CODE", dtDropDownData, false, true);
        }
    }
}
