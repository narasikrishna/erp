﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FIN.DAL;
using FIN.DAL.SSM;
using VMVServices.Web;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;

namespace FIN.BLL.SSM
{
    public class Lookups_BLL
    {
        DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();

        public static void GetLookUp(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Lookups_DAL.GetLookUp()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.PARENT_CODE, FINColumnConstants.PARENT_CODE, dtDropDownData, true, false);
        }
    }
}
