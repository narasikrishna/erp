﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Entities;
using FIN.DAL;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;

using VMVServices.Services.Data;
using VMVServices.Services.Business;
using System.Configuration;

using System.Data.EntityClient;
using Oracle.DataAccess.Client;
using System.Data.Common;

namespace FIN.BLL.SSM
{
    public class MasterReport_BLL
    {
        static DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();

        public static void GeDetails(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(FIN.DAL.SSM.Country_DAL.getCurrency()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.CURRENCY_NAME, FINColumnConstants.CURRENCY_ID, dtDropDownData, true, false);
        }
        public static void GeReportName(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(FIN.DAL.SSM.MasterReport_DAL.getReportListName()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "REPORT_NAME", "LIST_HDR_ID", dtDropDownData, true, false);
        }

    }
}
