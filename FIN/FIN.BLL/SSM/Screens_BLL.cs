﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.DAL.SSM;
using System.Data;

namespace FIN.BLL.SSM
{
    public class Screens_BLL
    {
        static DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();

        public static void GetModuleData(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Screens_DAL.GetModuleData()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.MODULE_CODE, FINColumnConstants.MODULE_CODE, dtDropDownData, true, false);
        }
    
        public static void GetModuleList(ref DropDownList ddlList,string moduleCode="")
        {
            dtDropDownData = DBMethod.ExecuteQuery(Screens_DAL.GetModuleList(moduleCode)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.MODULE_CODE, FINColumnConstants.MODULE_CODE, dtDropDownData, true, false);
        }

        public static void GetScreenName(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Screens_DAL.GetScreenName()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.SCREEN_NAME, FINColumnConstants.SCREEN_CODE, dtDropDownData, true, false);
        }
        public static void GetScreenName(ref DropDownList ddlList,string moduleCode,string screecode="")
        {
            dtDropDownData = DBMethod.ExecuteQuery(Screens_DAL.GetScreenName(moduleCode, screecode)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.SCREEN_NAME, FINColumnConstants.SCREEN_CODE, dtDropDownData, true, false);
        }


        public static SSM_SCREENS getClassEntity(int pkId)
        {
            SSM_SCREENS SSM_SCREENS = new SSM_SCREENS();
            using (IRepository<SSM_SCREENS> userCtx = new DataRepository<SSM_SCREENS>())
            {
                SSM_SCREENS = userCtx.Find(r =>
                    (r.PK_ID == int.Parse(pkId.ToString()))
                    ).SingleOrDefault();
            }
            return SSM_SCREENS;
        }
    }
}
