﻿using System.Linq;
using System.Text;

using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.AP;
using VMVServices.Web;
using System.Data;

namespace FIN.BLL.AP
{
    public class APAginAnalysis_BLL
    {
        static DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();
        public static DataSet GetReportData()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.AP.APAgingAnalysis_DAL.getReportData());
        }

        public static DataSet GetReservedGuaranteeBalanceReportData()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.AP.APAgingAnalysis_DAL.getReservedGuaranteeBalanceReportData());
        }

        public static DataSet GetSubmitPaymentDtlsReportData()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.AP.APAgingAnalysis_DAL.getSubmitPaymentDtlsReportData());
        }


        public static DataSet GetChequesReportData()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.AP.APAgingAnalysis_DAL.getChequesReportData());
        }
        public static DataSet GetChequesReportDataPaymentReg()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.AP.APAgingAnalysis_DAL.getChequesReportDataPaymnetReg());
        }
        public static DataSet GetInvoiceListReportData()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.AP.APAgingAnalysis_DAL.getInvoiceListReportData());
        }
        public static DataSet getInvoiceRegisterReportData()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.AP.APAgingAnalysis_DAL.getInvoiceRegisterReportData());
        }
        public static DataSet GetStatementofAccountReportData()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.AP.APAgingAnalysis_DAL.getStatementofAccountReportData());
        }

        public static void GetGroupName(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(FIN.DAL.AP.APAgingAnalysis_DAL.getGlobalSegment()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "SEGMENT_VALUE", "SEGMENT_VALUE_ID", dtDropDownData, false, true);
        }
    }
}
