﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.AP;
using VMVServices.Web;
using System.Data;

namespace FIN.BLL.AP
{
   public class ReceivedGoods_BLL
    {
        static DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();

        public static DataSet GetReportData()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.AP.ReceivedGoods_DAL.getReportData());
        }
    }
}
