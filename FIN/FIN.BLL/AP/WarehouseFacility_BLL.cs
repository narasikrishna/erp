﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.AP;
using VMVServices.Web;
using System.Data;

namespace FIN.BLL.AP
{
    public class WarehouseFacility_BLL
    {
        DropDownList ddlList = new DropDownList();
        DataTable dtDropDownData = new DataTable();

        # region FillCombo
        public void fn_getWarehouseName(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(WarehouseFacility_DAL.getWarehouseName()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.INV_WH_NAME, FINColumnConstants.INV_WH_ID, dtDropDownData, true, false);
        }
        # endregion

        public static DataSet GetReportData()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.AP.WarehouseFacility_DAL.getReportData());
        }
    }
}
