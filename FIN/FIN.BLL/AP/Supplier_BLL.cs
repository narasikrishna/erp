﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.AP;
using VMVServices.Web;
using System.Data;

namespace FIN.BLL.AP
{
    public static class Supplier_BLL
    {
        static DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();

        # region FillCombo
        public static void fn_getSupplierType(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Supplier_DAL.getSupplierType()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.LOOKUP_NAME, FINColumnConstants.LOOKUP_ID, dtDropDownData, true, false);
        }
        public static void getCustomerNameType(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Supplier_DAL.getCustomerType()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.LOOKUP_NAME, FINColumnConstants.LOOKUP_ID, dtDropDownData, true, false);
        }
        public static void GetSupplierName_ForArInvoice(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Supplier_DAL.GetSupplierName_ForArInvoice()).Tables[0];
            //CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.VENDOR_CODE, FINColumnConstants.VENDOR_ID, dtDropDownData, true, false);
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.VENDOR_NAME, FINColumnConstants.VENDOR_ID, dtDropDownData, true, false);
        }


        public static void fn_getSupplierStatus(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Supplier_DAL.getSupplierStatus()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.LOOKUP_NAME, FINColumnConstants.LOOKUP_ID, dtDropDownData, true, false);
        }

        public static void fn_getCountry(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Supplier_DAL.getCountry()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.COUNTRY_NAME, FINColumnConstants.COUNTRY_CODE, dtDropDownData, true, false);
        }

        public static void fn_getState(ref DropDownList ddlList, string COUNTRY_CODE)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Supplier_DAL.getState(COUNTRY_CODE)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.STATE_NAME, FINColumnConstants.STATE_CODE, dtDropDownData, true, false);
        }

        public static void fn_getCity(ref DropDownList ddlList, string STATE_CODE)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Supplier_DAL.getCity(STATE_CODE)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.CITY_NAME, FINColumnConstants.CITY_CODE, dtDropDownData, true, false);
        }
       
        public static void fn_getCit(ref DropDownList ddlList, string STATE_CODE,string COUNTRY_CODE)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Supplier_DAL.getCit(STATE_CODE,COUNTRY_CODE)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.CITY_NAME, FINColumnConstants.CITY_CODE, dtDropDownData, true, false);
        }
        public static void GetSupplierName(ref DropDownList ddlList, bool includeAll = false)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Supplier_DAL.GetSupplierName()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.VENDOR_NAME, FINColumnConstants.VENDOR_ID, dtDropDownData, !includeAll, includeAll);
        }
        public static void GetSupplierNamesR(ref DropDownList ddlList, bool includeAll = true)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Supplier_DAL.GetSupplierName()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.VENDOR_NAME, FINColumnConstants.VENDOR_ID, dtDropDownData, !includeAll, includeAll);
        }


        public static void GetSupplierNameDetails(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Supplier_DAL.GetSupplierNameDetails()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.VENDOR_NAME, FINColumnConstants.VENDOR_ID, dtDropDownData, true, false);
        }

        public static void GetCustomerNameDetails(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Supplier_DAL.GetCustomerNameDetails()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.VENDOR_NAME, FINColumnConstants.VENDOR_ID, dtDropDownData, true, false);
        }
        public static void GetCustomerBasedQuote(ref DropDownList ddlList, string quteHdrId)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Supplier_DAL.GetCustomerBasedQuote(quteHdrId)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.VENDOR_NAME, FINColumnConstants.VENDOR_ID, dtDropDownData, true, false);
        }
        public static void GetCustomerNumber_Name(ref DropDownList ddlList, bool includeAll=false)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Supplier_DAL.GetCustomerName()).Tables[0];
            //CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.VENDOR_ID, FINColumnConstants.VENDOR_NAME, dtDropDownData, true, false);
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.VENDOR_NAME, FINColumnConstants.VENDOR_ID, dtDropDownData, !includeAll, includeAll);
        }
       
        public static void GetSupplierNumber_Name(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Supplier_DAL.GetSupplierName()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.VENDOR_ID, FINColumnConstants.VENDOR_NAME, dtDropDownData, true, false);
        }
        public static void GetSupplierNumber(ref DropDownList ddlList, bool includeAll=false)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Supplier_DAL.GetSupplierName()).Tables[0];
            //CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.VENDOR_CODE, FINColumnConstants.VENDOR_ID, dtDropDownData, true, false);
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.VENDOR_NAME, FINColumnConstants.VENDOR_ID, dtDropDownData, !includeAll, includeAll);
        }
        public static void GetSupplierNam(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Supplier_DAL.GetSupplierName()).Tables[0];
            //CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.VENDOR_CODE, FINColumnConstants.VENDOR_ID, dtDropDownData, true, false);
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.VENDOR_NAME, FINColumnConstants.VENDOR_ID, dtDropDownData, true, false);
        }
        public static void GetCustomerNumber(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Supplier_DAL.GetCustomerName()).Tables[0];
            //CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.VENDOR_CODE, FINColumnConstants.VENDOR_ID, dtDropDownData, true, false);
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.VENDOR_NAME, FINColumnConstants.VENDOR_ID, dtDropDownData, true, false);
        }

        public static void GetCustomerAndSupplier(ref DropDownList ddlList, bool includeAll=false)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Supplier_DAL.GetCustomerAndSupplier()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.VENDOR_NAME, FINColumnConstants.VENDOR_ID, dtDropDownData, !includeAll, includeAll);
        }
        public static DataSet GetSupplierSiteReportData(bool customer=false)
        {
            return DBMethod.ExecuteQuery(FIN.DAL.AP.SupplierBranch_DAL.getSupplierSiteReportData(customer));
        }
        public static DataSet GetReportData()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.Supplier_DAL.getPOReportData());
        }
        public static void GetSupplier(ref DropDownList ddlList,Boolean bol_ALL=false )
        {
            dtDropDownData = DBMethod.ExecuteQuery(Supplier_DAL.GetSupplier()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.VENDOR_NAME, FINColumnConstants.VENDOR_ID, dtDropDownData, !bol_ALL, bol_ALL);
        }

        public static void GetSupplierNames(ref DropDownList ddlList, Boolean bol_ALL = false)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Supplier_DAL.GetSupplier()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.VENDOR_NAME, FINColumnConstants.VENDOR_ID, dtDropDownData, !bol_ALL, bol_ALL);
        }

        public static void GetSupplierNams(ref DropDownList ddlList, Boolean bol_ALL = false)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Supplier_DAL.GetSupplier()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.VENDOR_NAME, FINColumnConstants.VENDOR_ID, dtDropDownData, bol_ALL, !bol_ALL);
        }

        public static void GetPaymentIDForSupplier(ref DropDownList ddlList, Boolean bol_ALL = false)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Supplier_DAL.GetPaymentDtlsForSupplier()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.PAY_ID, FINColumnConstants.PAY_ID, dtDropDownData, bol_ALL, !bol_ALL);
        }

        public static void GetPaymentDetails(ref DropDownList ddlList, Boolean bol_ALL = false)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Supplier_DAL.GetPaymentDtls()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.PAY_ID, FINColumnConstants.PAY_ID, dtDropDownData, bol_ALL, !bol_ALL);
        }

        public static DataSet GetVendorReportData()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.Supplier_DAL.getVendorReportData());
        }

        # endregion

    }
}
