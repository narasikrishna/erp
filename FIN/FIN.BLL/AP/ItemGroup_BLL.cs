﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Entities;
using FIN.DAL;
using FIN.BLL;
using FIN.BLL.AP;
using FIN.DAL.AP;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;


using VMVServices.Services.Data;
using VMVServices.Services.Business;
using System.Configuration;

using System.Data.EntityClient;
using Oracle.DataAccess.Client;
using System.Data.Common;

namespace FIN.BLL.AP
{
    public class ItemGroup_BLL
    {
        public static DataSet GetItemGroupReportData()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.AP.ItemGroup_DAL.getItemGroupReportData());
        }
    }
}
