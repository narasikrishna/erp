﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.AP;
using VMVServices.Web;
using System.Data;


namespace FIN.BLL.AP
{
    public class RFQ_BLL
    {
        static DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();

        public static PO_RFQ_ADDITIONAL_DTLS getClassEntity(string str_Id)
        {
            PO_RFQ_ADDITIONAL_DTLS obj_PO_RFQ_ADDITIONAL_DTLS = new PO_RFQ_ADDITIONAL_DTLS();
            using (IRepository<PO_RFQ_ADDITIONAL_DTLS> userCtx = new DataRepository<PO_RFQ_ADDITIONAL_DTLS>())
            {
                obj_PO_RFQ_ADDITIONAL_DTLS = userCtx.Find(r =>
                    (r.RFQ_ADD_ID == str_Id)
                    ).SingleOrDefault();
            }
            return obj_PO_RFQ_ADDITIONAL_DTLS;
        }


        public static void getQution4Supplier(ref DropDownList ddlList,string strSupplierid)
        {
            dtDropDownData = DBMethod.ExecuteQuery(FIN.DAL.AP.RFQ_DAL.getQution4Supplier(strSupplierid)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "RFQ_NUMBER", "RFQ_ID", dtDropDownData, true, false);
        }
        public static void GetRFQQuotationNo(ref DropDownList ddlList, string strSupplierid)
        {
            dtDropDownData = DBMethod.ExecuteQuery(FIN.DAL.AP.RFQ_DAL.GetRFQQuotationNo(strSupplierid)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "quote_number", "RFQ_ID", dtDropDownData, true, false);
        }
        
        public static void getQuotationNumber(ref DropDownList ddlList, string supplierId="")
        {
            dtDropDownData = DBMethod.ExecuteQuery(FIN.DAL.AP.RFQ_DAL.getQuotationNumber(supplierId)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "RFQ_NUMBER", "RFQ_ID", dtDropDownData, true, false);
        }
    
    }
}
