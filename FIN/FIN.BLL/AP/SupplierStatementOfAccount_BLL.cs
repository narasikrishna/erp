﻿using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.AP;
using VMVServices.Web;
using System.Data;

namespace FIN.BLL.AP
{
   public class SupplierStatementOfAccount_BLL
    {
        static DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();
        //public static string GetSupplierStatementOfAccountReportData()
        //{
        //    return FIN.DAL.AP.SupplierStatementOfAccount_DAL.getSupplierStatementOfAccount();
        //}

        public static DataSet GetReportData()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.AP.SupplierStatementOfAccount_DAL.getSupplierStatementOfAccount());
        }

        public static void GetVendorNumber(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(FIN.DAL.AP.SupplierStatementOfAccount_DAL.getVendorNumber()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "VENDOR_ID", "VENDOR_ID", dtDropDownData, false, true);
        }
    }
}