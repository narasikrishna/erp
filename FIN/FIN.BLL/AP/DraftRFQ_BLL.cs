﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.AP;
using VMVServices.Web;
using System.Data;

namespace FIN.BLL.AP
{
    public class DraftRFQ_BLL
    {
        static DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();

        public static void GetSupplierName(ref DropDownList ddlList,string tmpRfqDtlId = "")
        {
            dtDropDownData = DBMethod.ExecuteQuery(DraftRFQ_DAL.GetDraftRFQSupplierDtls(tmpRfqDtlId)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.VENDOR_NAME, FINColumnConstants.VENDOR_ID, dtDropDownData, true, false);
        }
        public static void GetDraftRFQBasedSupplier(ref DropDownList ddlList, string supplierId, string draftRfqId = "", string mode = "")
        {
            dtDropDownData = DBMethod.ExecuteQuery(DraftRFQ_DAL.GetDraftRFQNoBasedSupplier(supplierId, draftRfqId, mode)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "tmp_rfq_num", "tmp_rfq_id", dtDropDownData, true, false);
        }
        public static void GetDraftRFQNo(ref DropDownList ddlList, string supplierId)
        {
            dtDropDownData = DBMethod.ExecuteQuery(DraftRFQ_DAL.GetDraftRFQNo(supplierId)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "tmp_rfq_num", "tmp_rfq_id", dtDropDownData, true, false);
        }
    }
}
