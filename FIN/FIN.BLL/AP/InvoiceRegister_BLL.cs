﻿using System.Linq;
using System.Text;

using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.AP;
using VMVServices.Web;
using System.Data;

namespace FIN.BLL.AP
{
    public class InvoiceRegister_BLL
    {
        static DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();
        public static DataSet GetReportData(string vendorId="")
        {
            return DBMethod.ExecuteQuery(FIN.DAL.AP.InvoiceRegister_DAL.getReportData(vendorId));
        }
        public static DataSet GetSupplierwiseSummaryReportData()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.AP.InvoiceRegister_DAL.getSupplierwiseSummaryReportData());
        }
        public static void GetGroupName(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(FIN.DAL.AP.InvoiceRegister_DAL.getGlobalSegment()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "SEGMENT_VALUE", "SEGMENT_ID", dtDropDownData, false, true);
        }
        public static DataSet GetInvoiceBalancesReport()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.AP.InvoiceRegister_DAL.getInvoiceBalancesReportData());
        }
    }
}
