﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.AP;
using VMVServices.Web;
using System.Data;


namespace FIN.BLL.AP
{
    public class MaterialIssue_BLL
    {
        DropDownList ddlList = new DropDownList();
        static  DataTable dtDropDownData = new DataTable();

        public static INV_MATERIAL_ISSUE_HDR getClassEntity(string str_Id)
        {
            INV_MATERIAL_ISSUE_HDR obj_INV_MATERIAL_ISSUE_HDR = new INV_MATERIAL_ISSUE_HDR();
            using (IRepository<INV_MATERIAL_ISSUE_HDR> userCtx = new DataRepository<INV_MATERIAL_ISSUE_HDR>())
            {
                obj_INV_MATERIAL_ISSUE_HDR = userCtx.Find(r =>
                    (r.INDENT_ID == str_Id)
                    ).SingleOrDefault();
            }
            return obj_INV_MATERIAL_ISSUE_HDR;
        }

        public static INV_MATERIAL_ISSUE_DTL getDetailClassEntity(string str_Id)
        {
            INV_MATERIAL_ISSUE_DTL obj_INV_MATERIAL_ISSUE_DTL = new INV_MATERIAL_ISSUE_DTL();
            using (IRepository<INV_MATERIAL_ISSUE_DTL> userCtx = new DataRepository<INV_MATERIAL_ISSUE_DTL>())
            {
                obj_INV_MATERIAL_ISSUE_DTL = userCtx.Find(r =>
                    (r.INDENT_DTL_ID == str_Id)
                    ).SingleOrDefault();
            }
            return obj_INV_MATERIAL_ISSUE_DTL;
        }

        public static INV_MATERIAL_ISSUE_WH getWHClassEntity(string str_Id)
        {
            INV_MATERIAL_ISSUE_WH obj_INV_MATERIAL_ISSUE_WH = new INV_MATERIAL_ISSUE_WH();
            using (IRepository<INV_MATERIAL_ISSUE_WH> userCtx = new DataRepository<INV_MATERIAL_ISSUE_WH>())
            {
                obj_INV_MATERIAL_ISSUE_WH = userCtx.Find(r =>
                    (r.ISSUE_MAT_WH_ID == str_Id)
                    ).SingleOrDefault();
            }


            return obj_INV_MATERIAL_ISSUE_WH;
        }

        public static void GetLotNumber(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(MaterialIssue_DAL.GetLotDetails()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "lot_Number", "lot_id", dtDropDownData, true, false);
        }

        public static DataTable GetItemLotDtls(string ItemId)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(MaterialIssue_DAL.GetItemLotDtls(ItemId)).Tables[0];
            return dt_Det;
        }

        public static void GetWHDetails(ref DropDownList ddlList, string itemID)
        {
            dtDropDownData = DBMethod.ExecuteQuery(MaterialIssue_DAL.getWHDetails(itemID)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "inv_wh_name", "inv_wh_id", dtDropDownData, true, false);
        }

        public static void getWHDetails_fritem(ref DropDownList ddlList, string itemID)
        {
            dtDropDownData = DBMethod.ExecuteQuery(MaterialIssue_DAL.getWHDetails_fritem(itemID)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "inv_wh_name", "inv_wh_id", dtDropDownData, true, false);
        }

        public static void GetLotDetails(ref DropDownList ddlList, string WhID,string itemID)
        {
            dtDropDownData = DBMethod.ExecuteQuery(MaterialIssue_DAL.GetLotDetails(WhID, itemID)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "lot_number", "lot_id", dtDropDownData, true, false);
        }
    }
}
