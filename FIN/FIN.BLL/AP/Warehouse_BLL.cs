﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.AP;
using VMVServices.Web;
using System.Data;

namespace FIN.BLL.AP
{
   public class Warehouse_BLL
    {
        static DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();
        public static DataSet GetReportData()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.AP.Warehouse_DAL.getReportData());
        }
        public static void GetWareHouseName(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Warehouse_DAL.getWareHouseName()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.INV_WH_NAME, FINColumnConstants.INV_WH_ID, dtDropDownData, false, true);
        }

        public static void GetLotNumber(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Warehouse_DAL.getLotNumber()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "LOT_NUMBER", "LOT_NUMBER", dtDropDownData, false, true);
        }
        public static void GetWareHouseData(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Warehouse_DAL.getWareHouseName()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.INV_WH_NAME, FINColumnConstants.INV_WH_ID, dtDropDownData, true, false);
        }
        public static DataSet GetWarehouseReportData()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.AP.Warehouse_DAL.getWarehouseReportData());
        }
    }
}
