﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.AP;
using VMVServices.Web;
using System.Data;
namespace FIN.BLL.AP
{
   public class WarehouseReceiptItem_BLL
   {
       DropDownList ddlList = new DropDownList();
      static DataTable dtDropDownData = new DataTable();

       # region FillCombo
       

       public void fn_getLineNo(ref DropDownList ddlList)
       {
           dtDropDownData = DBMethod.ExecuteQuery(WarehouseReceiptItem_DAL.getLineNo()).Tables[0];
           CommonUtils.LoadDropDownList(ddlList, "LINE_NUM", "ITEM_ID", dtDropDownData, true, false);
       }

       public void fn_getWarehouse(ref DropDownList ddlList)
       {
           dtDropDownData = DBMethod.ExecuteQuery(WarehouseReceiptItem_DAL.getWarehouse()).Tables[0];
           CommonUtils.LoadDropDownList(ddlList, "INV_WH_ID", "INV_WH_NAME", dtDropDownData, true, false);
       }

       public void fn_getLotNo(ref DropDownList ddlList)
       {
           dtDropDownData = DBMethod.ExecuteQuery(WarehouseReceiptItem_DAL.getLotNo()).Tables[0];
           CommonUtils.LoadDropDownList(ddlList, "LOT_NUMBER", "LOT_ID", dtDropDownData, true, false);
       }
       public void getLotNoBasedReceipt(ref DropDownList ddlList, string receiptDtlId, string mode, string pk_id)
       {
           dtDropDownData = DBMethod.ExecuteQuery(WarehouseReceiptItem_DAL.getLotNoBasedReceipt(receiptDtlId,mode,pk_id)).Tables[0];
           CommonUtils.LoadDropDownList(ddlList, "LOT_NUMBER", "LOT_ID", dtDropDownData, true, false);
       }
      
       # endregion




       public static DataTable getChildEntityDet(string Str_ID)
       {
           DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.AP.WarehouseReceiptItem_DAL.GetWHRecptitemdtls(Str_ID)).Tables[0];
           return dt_Det;
       }
       public static void SavePCEntity<T, TC>(T entity, List<Tuple<object, string>> tmpChildEntity, TC ChildEntity, bool modifyMode = false)
           where T : class
           where TC : class
       {
           try
           {
               DBMethod.SavePCEntity<T, TC>(entity, tmpChildEntity, ChildEntity, modifyMode);
           }
           catch (Exception ex)
           {
               throw ex.InnerException;
           }

       }

       public static INV_RECEIPT_ITEM_WH_HDR getClassEntity(string str_Id)
       {
           INV_RECEIPT_ITEM_WH_HDR obj_INV_RECEIPT_ITEM_WH_HDR = new INV_RECEIPT_ITEM_WH_HDR();
           using (IRepository<INV_RECEIPT_ITEM_WH_HDR> userCtx = new DataRepository<INV_RECEIPT_ITEM_WH_HDR>())
           {
               obj_INV_RECEIPT_ITEM_WH_HDR = userCtx.Find(r =>
                   (r.INV_RECEIPT_ITEM_WH_ID == str_Id)
                   ).SingleOrDefault();
           }


           return obj_INV_RECEIPT_ITEM_WH_HDR;
       }
       public static String ValidateEntity(INV_RECEIPT_ITEM_WH_HDR obj_INV_RECEIPT_ITEM_WH_HDR)
       {
           string str_Error = "";
           return str_Error;
       }

       public static string CalculateAmount(DataTable dtGridData, decimal currentPOAmount)
       {
           decimal CalculateAmount = currentPOAmount;

           if (dtGridData != null)
           {
               if (dtGridData.Rows.Count >= 1)
               {
                   for (int i = 0; i < dtGridData.Rows.Count; i++)
                   {
                       //if (rowIndex != i)
                       //{
                       if (dtGridData.Rows[i]["LOT_QTY"].ToString() != null)
                       {
                           CalculateAmount = CalculateAmount + CommonUtils.ConvertStringToDecimal(dtGridData.Rows[i]["LOT_QTY"].ToString());
                       }
                       // }
                   }
               }
               else
               {
                   CalculateAmount = currentPOAmount;
               }
           }
           return CalculateAmount.ToString();
       }
       //public static String DeleteEntity(int int_PKID)
       //{
       //    string str_Message = "";
       //    GL_ACCT_GROUPS obj_GL_ACCT_GROUPS = new GL_ACCT_GROUPS();
       //    obj_GL_ACCT_GROUPS.PK_ID = int_PKID;
       //    DBMethod.DeleteEntity<GL_ACCT_GROUPS>(obj_GL_ACCT_GROUPS);
       //    return str_Message;
       //}

       public static void fn_getReceipt4Wh(ref DropDownList ddlList, string str_wh_id)
       {
           dtDropDownData = DBMethod.ExecuteQuery(WarehouseReceiptItem_DAL.getReceiptDetails4WareHouse(str_wh_id)).Tables[0];
           CommonUtils.LoadDropDownList(ddlList, "GRN_NUM", "RECEIPT_ID", dtDropDownData, true, false);
       }
       public static void fn_getItem4Recipt(ref DropDownList ddlList, string str_Rec_id)
       {
           dtDropDownData = DBMethod.ExecuteQuery(WarehouseReceiptItem_DAL.getItem4Receipt(str_Rec_id)).Tables[0];
           CommonUtils.LoadDropDownList(ddlList, "ITEM_NAME", "ITEM_ID", dtDropDownData, true, false);
       }
       public static void fn_getItemForRecipt(ref DropDownList ddlList, string str_Rec_id = "")
       {
           dtDropDownData = DBMethod.ExecuteQuery(WarehouseReceiptItem_DAL.getItem4Receipt(str_Rec_id)).Tables[0];
           CommonUtils.LoadDropDownList(ddlList, "ITEM_NAME", "ITEM_ID", dtDropDownData, true, false);
       }
        public static void fn_getItem4Wh(ref DropDownList ddlList,string str_wh_id)
       {
           dtDropDownData = DBMethod.ExecuteQuery(WarehouseReceiptItem_DAL.getItemDetails4WareHouse(str_wh_id)).Tables[0];
           CommonUtils.LoadDropDownList(ddlList, "ITEM_NAME", "ITEM_ID", dtDropDownData, true, false);
       }

        public static void fn_getLot4WHItemRecID(ref DropDownList ddlList, string str_wh_id="",string str_Item_ID="",string str_Rec_id="")
        {
            dtDropDownData = DBMethod.ExecuteQuery(WarehouseReceiptItem_DAL.getLotDetails4WHItemRecID(str_wh_id, str_Item_ID, str_Rec_id)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "LOT_NUMBER", "LOT_ID", dtDropDownData, true, false);
        }


   }
}
