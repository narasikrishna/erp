﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FIN.DAL;
using FIN.DAL.AP;
using VMVServices.Web;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;

namespace FIN.BLL.AP
{
    public class Item_BLL
    {
        DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();

        # region FillCombo
        public static void GetItemName(ref DropDownList ddlList,string itemType)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Item_DAL.getItemName(itemType)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.ITEM_NAME, FINColumnConstants.ITEM_ID, dtDropDownData, true, false);
        }
        public static void GetItemNames(ref DropDownList ddlList, string itemType)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Item_DAL.getItemName(itemType)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.ITEM_NAME, FINColumnConstants.ITEM_ID, dtDropDownData, false, true);
        }
        public static void GetItemName(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Item_DAL.getItemNameDetails()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.ITEM_NAME, FINColumnConstants.ITEM_ID, dtDropDownData, true, false);
        }
        public static void GetItemNam(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Item_DAL.getItemNameDetails()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.ITEM_NAME, FINColumnConstants.ITEM_ID, dtDropDownData, false, true);
        }

        public static void GetItemName_frTransLedger(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Item_DAL.getItemList_Without_Service()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.ITEM_NAME, FINColumnConstants.ITEM_ID, dtDropDownData, false, true);
        }
        public static void GetTransactionType(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Item_DAL.getTransactionType()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "inv_trans_type", "inv_tran_id", dtDropDownData, false, true);
        }

        public static void getItem_RECinWh_notServce(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Item_DAL.getItem_RECinWh_notServce()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.ITEM_NAME, FINColumnConstants.ITEM_ID, dtDropDownData, true, false);
        }

        public static void getItemList_Without_Service(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Item_DAL.getItemList_Without_Service()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.ITEM_NAME, FINColumnConstants.ITEM_ID, dtDropDownData, true, false);
        }
        
        public static void getAreaUOM(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Item_DAL.getAreaUOM()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.UOM_NAME, FINColumnConstants.UOM_ID, dtDropDownData, true, false);
        }


        public static void getUOM(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Item_DAL.getUOM()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.UOM_NAME, FINColumnConstants.UOM_CODE, dtDropDownData, true, false);
        }

        public static void getUOM_frSO(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Item_DAL.getUOM_frSO()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.UOM_NAME, FINColumnConstants.UOM_CODE, dtDropDownData, true, false);
        }
        public static void getServiceUOM(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Item_DAL.getServiceUOM()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.UOM_NAME, FINColumnConstants.UOM_ID, dtDropDownData, true, false);
        }


        # endregion


        public static void getItemNameDetails_frARDeleiverychallan(ref DropDownList ddlList, string order_id)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Item_DAL.getItemNameDetails_frARDeleiverychallan(order_id)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.ITEM_NAME, FINColumnConstants.ITEM_ID, dtDropDownData, true, false);
        }

        public static DataSet GetReportData()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.AP.Item_DAL.getReportData());
        }

        public static DataSet GetDayBookReportData()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.AP.Item_DAL.getDayBookReportData());
        }

        public static void GetItemType(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(FIN.DAL.AP.Item_DAL.getItemType()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "ITEM_TYPE", FINColumnConstants.ITEM_TYPE, dtDropDownData, false, true);
        }
        public static void GetItemCategory(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(FIN.DAL.AP.Item_DAL.getItemCategoryName()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "ITEM_CAT_NAME", "ITEM_CAT_NAME", dtDropDownData, false, true);
        }
        public static void GetItemUom(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(FIN.DAL.AP.Item_DAL.getItemUOM()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "ITEM_UOM", "ITEM_UOM", dtDropDownData, false, true);
        }
        public static void GetItemGroupName(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(FIN.DAL.AP.Item_DAL.getGroupName()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "ITEM_GROUP_NAME", "ITEM_GROUP_NAME", dtDropDownData, false, true);
        }

        public static DataSet getItemCategoryReportData()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.AP.Item_DAL.getItemCategoryReportData());
        }
    }
}
