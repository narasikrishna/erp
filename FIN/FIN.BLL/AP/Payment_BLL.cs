﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Entities;
using FIN.DAL.CA;
using FIN.BLL.CA;
using FIN.DAL.AP;
using FIN.DAL;
using FIN.BLL;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;


using VMVServices.Services.Data;
using VMVServices.Services.Business;
using System.Configuration;

using System.Data.EntityClient;
using Oracle.DataAccess.Client;
using System.Data.Common;


namespace FIN.BLL.AP
{
    public class Payment_BLL
    {

        public static INV_PAYMENTS_HDR getClassEntity(string str_Id)
        {
            INV_PAYMENTS_HDR obj_INV_PAYMENTS_HDR = new INV_PAYMENTS_HDR();
            using (IRepository<INV_PAYMENTS_HDR> userCtx = new DataRepository<INV_PAYMENTS_HDR>())
            {
                obj_INV_PAYMENTS_HDR = userCtx.Find(r =>
                    (r.PAY_ID == str_Id)
                    ).SingleOrDefault();
            }


            return obj_INV_PAYMENTS_HDR;
        }
        public static INV_PAYMENTS_DTL getDetailClassEntity(string str_Id)
        {
            INV_PAYMENTS_DTL obj_INV_PAYMENTS_DTL = new INV_PAYMENTS_DTL();
            using (IRepository<INV_PAYMENTS_DTL> userCtx = new DataRepository<INV_PAYMENTS_DTL>())
            {
                obj_INV_PAYMENTS_DTL = userCtx.Find(r =>
                    (r.PAY_DTL_ID == str_Id)
                    ).SingleOrDefault();
            }
            return obj_INV_PAYMENTS_DTL;
        }
        public static String DeleteEntity(string payId)
        {
            string str_Message = "";
            INV_PAYMENTS_DTL obj_INV_PAYMENTS_DTL = new INV_PAYMENTS_DTL();
            obj_INV_PAYMENTS_DTL.PAY_ID = payId;
            DBMethod.DeleteEntity<INV_PAYMENTS_DTL>(obj_INV_PAYMENTS_DTL);
            return str_Message;
        }
        public static string CalculateAmount(DataTable dtGridData, decimal currentAmount)
        {
            decimal CalculateAmount = currentAmount;

            if (dtGridData != null)
            {
                if (dtGridData.Rows.Count >= 1)
                {
                    for (int i = 0; i < dtGridData.Rows.Count; i++)
                    {
                        if (dtGridData.Rows[i]["PAY_INVOICE_AMT"].ToString() != null)
                        {
                            CalculateAmount = CalculateAmount + CommonUtils.ConvertStringToDecimal(dtGridData.Rows[i]["PAY_INVOICE_AMT"].ToString());
                        }
                    }
                }
                else
                {
                    CalculateAmount = currentAmount;
                }
            }
            return CalculateAmount.ToString();
        }
        static DataTable dtDropDownData = new DataTable();

        public static INV_PAYMENTS_HDR getClassEntityData(string str_Id)
        {
            INV_PAYMENTS_HDR obj_INV_PAYMENTS_HDR = new INV_PAYMENTS_HDR();
            using (IRepository<INV_PAYMENTS_HDR> userCtx = new DataRepository<INV_PAYMENTS_HDR>())
            {
                obj_INV_PAYMENTS_HDR = userCtx.Find(r =>
                    (r.PAY_ID == str_Id)
                    ).SingleOrDefault();
            }
            return obj_INV_PAYMENTS_HDR;
        }
        public static void getInvoiceDetails(ref DropDownList ddlList, string currId, string supplierId="")
        {
            dtDropDownData = DBMethod.ExecuteQuery(Payment_DAL.getInvoiceDetails(currId, supplierId)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.INV_NUM, FINColumnConstants.inv_id, dtDropDownData, true, false);
        }

        public static void getAccCodeDetails(ref DropDownList ddlList, string currId, string supplierId = "")
        {
            dtDropDownData = DBMethod.ExecuteQuery(Payment_DAL.getInvoiceDetails(currId, supplierId)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList,"inv_acct_code_id", "inv_acct_code_id", dtDropDownData, true, false);
        }
        public static DataSet GetPaymentRegisterReportData()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.AP.Payment_DAL.getPaymentRegisterReportData());
        }
        public static DataSet GetSupplierwisePaymentReportData()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.AP.Payment_DAL.getSupplierwisePaymentReportData());
        }

        public static DataSet GetPaymentReportData()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.AP.Payment_DAL.getPaymentReportData());
        }
    }
}
