﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FIN.DAL;
using FIN.DAL.AP;
using VMVServices.Web;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;


namespace FIN.BLL.AP
{
    public class UOMMasters_BLL
    {
        DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();
      
        # region FillCombo
        public static void GetUOMDetails(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(UOMMasters_DAL.getUOMDetails()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.UOM_NAME, FINColumnConstants.UOM_ID, dtDropDownData, true, false);
        }
        public static void getUOMBasedItem(ref DropDownList ddlList, string itemId)
        {
            dtDropDownData = DBMethod.ExecuteQuery(UOMMasters_DAL.getUOMBasedItem(itemId)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.UOM_NAME, FINColumnConstants.UOM_ID, dtDropDownData, true, false);
        }
        # endregion

        public static DataSet fn_get_VW_UOMDetails()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.AP.UOMMasters_DAL.get_VW_UOMDetails());
        }
    }
}
