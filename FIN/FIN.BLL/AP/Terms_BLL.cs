﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.AP;
using VMVServices.Web;
using System.Data;

namespace FIN.BLL.AP
{
    public class Terms_BLL
    {
       static DropDownList ddlList = new DropDownList();
       static DataTable dtDropDownData = new DataTable();
        
        # region FillCombo


        public static void getTermName(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Terms_DAL.getTermName()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.TERM_NAME, FINColumnConstants.TERM_ID, dtDropDownData, true, false);
        }

       # endregion




       public static DataTable getChildEntityDet(string Str_ID)
       {
           DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.AP.Terms_DAL.GetTermdtls(Str_ID)).Tables[0];
           return dt_Det;
       }
       public static void SavePCEntity<T, TC>(T entity, List<Tuple<object, string>> tmpChildEntity, TC ChildEntity, bool modifyMode = false)
           where T : class
           where TC : class
       {
           try
           {
               DBMethod.SavePCEntity<T, TC>(entity, tmpChildEntity, ChildEntity, modifyMode);
           }
           catch (Exception ex)
           {
               throw ex.InnerException;
           }

       }

       public static  INV_PAY_TERMS_HDR getClassEntity(string str_Id)
       {
           INV_PAY_TERMS_HDR obj_INV_PAY_TERMS_HDR = new INV_PAY_TERMS_HDR();
           using (IRepository<INV_PAY_TERMS_HDR> userCtx = new DataRepository<INV_PAY_TERMS_HDR>())
           {
               obj_INV_PAY_TERMS_HDR = userCtx.Find(r =>
                   (r.TERM_ID == str_Id)
                   ).SingleOrDefault();
           }


           return obj_INV_PAY_TERMS_HDR;
       }
       public static String ValidateEntity(INV_PAY_TERMS_HDR obj_AP_PAY_TERMS_HDR)
       {
           string str_Error = "";
           return str_Error;
       }


       //public static String DeleteEntity(int int_PKID)
       //{
       //    string str_Message = "";
       //    GL_ACCT_GROUPS obj_GL_ACCT_GROUPS = new GL_ACCT_GROUPS();
       //    obj_GL_ACCT_GROUPS.PK_ID = int_PKID;
       //    DBMethod.DeleteEntity<GL_ACCT_GROUPS>(obj_GL_ACCT_GROUPS);
       //    return str_Message;
       //}



   }
}
