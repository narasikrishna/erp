﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.AP;
using VMVServices.Web;
using System.Data;


namespace FIN.BLL.AP
{
    public class Invoice_BLL
    {
        DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();

        public static void getInvoice(ref DropDownList ddlList)
        {
            using (IRepository<INV_INVOICES_HDR> userCtx = new DataRepository<INV_INVOICES_HDR>())
            {
                CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.INV_NUM, FINColumnConstants.inv_id, userCtx.Find(r =>
                    (r.ENABLED_FLAG == "1" && r.WORKFLOW_COMPLETION_STATUS == "1" && r.INV_ORG_ID == VMVServices.Web.Utils.OrganizationID.ToString())
                    ).OrderBy(r => r.INV_ID), true, false);
            }
        }
        public static void getInvoice4Payment(ref DropDownList ddlList, string str_supplierid, string str_mode, string currId)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Invoice_DAL.getInvoice4Payemnt(str_supplierid, str_mode, currId)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.INV_NUM, "INV_ID", dtDropDownData, true, false);
        }
        public static INV_INVOICES_HDR getClassEntity(string str_Id)
        {
            INV_INVOICES_HDR obj_INV_INVOICES_HDR = new INV_INVOICES_HDR();
            using (IRepository<INV_INVOICES_HDR> userCtx = new DataRepository<INV_INVOICES_HDR>())
            {
                obj_INV_INVOICES_HDR = userCtx.Find(r =>
                    (r.INV_ID == str_Id)
                    ).SingleOrDefault();
            }
            return obj_INV_INVOICES_HDR;
        }
        public static INV_INVOICES_DTLS getDetailClassEntity(string str_Id)
        {
            INV_INVOICES_DTLS obj_INV_INVOICES_DTLS = new INV_INVOICES_DTLS();
            using (IRepository<INV_INVOICES_DTLS> userCtx = new DataRepository<INV_INVOICES_DTLS>())
            {
                obj_INV_INVOICES_DTLS = userCtx.Find(r =>
                    (r.INV_LINE_ID == str_Id)
                    ).SingleOrDefault();
            }
            return obj_INV_INVOICES_DTLS;
        }

        public static void fn_getInvoice4Perpayment(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Invoice_DAL.getInvoice4Perpayment()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.INV_NUM, "INV_ID", dtDropDownData, true, false);
        }
        public static void fn_getInvoiceNumber(ref DropDownList ddlList, bool includeAll=false)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Invoice_DAL.getInvoiceNumber()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.INV_NUM, "INV_ID", dtDropDownData, !includeAll, includeAll);
        }

        public static void fn_getInvoice4Supplier(ref DropDownList ddlList, string str_supplier)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Invoice_DAL.getInvoiceNumber4Supplier(str_supplier)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.INV_NUM, "INV_ID", dtDropDownData, true, false);
        }

        public static void fn_getInvoiceNumberForSupplier(ref DropDownList ddlList, string str_supplier)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Invoice_DAL.getInvoiceNumberForSupplier(str_supplier)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.INV_NUM, "INV_ID", dtDropDownData, true, false);
        }

        public static DataSet GetInvoiceReportData()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.AP.Invoice_DAL.getInvoiceReportData());
        }

        public static void GetCustomerName(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Invoice_DAL.getCustomerName()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.VENDOR_NAME, FINColumnConstants.VENDOR_NAME, dtDropDownData, false, true);
        }
    }
}
