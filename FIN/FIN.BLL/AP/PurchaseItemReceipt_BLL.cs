﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.AP;
using VMVServices.Web;
using System.Data;


namespace FIN.BLL.AP
{
    public class PurchaseItemReceipt_BLL
    {
        DropDownList ddlList = new DropDownList();
        static  DataTable dtDropDownData = new DataTable();

        public static INV_RECEIPTS_HDR getClassEntity(string str_Id)
        {
            INV_RECEIPTS_HDR obj_INV_RECEIPTS_HDR = new INV_RECEIPTS_HDR();
            using (IRepository<INV_RECEIPTS_HDR> userCtx = new DataRepository<INV_RECEIPTS_HDR>())
            {
                obj_INV_RECEIPTS_HDR = userCtx.Find(r =>
                    (r.RECEIPT_ID == str_Id)
                    ).SingleOrDefault();
            }
            return obj_INV_RECEIPTS_HDR;
        }
   
        public static INV_RECEIPT_DTLS getDetailClassEntity(string str_Id)
        {
            INV_RECEIPT_DTLS obj_INV_RECEIPT_DTLS = new INV_RECEIPT_DTLS();
            using (IRepository<INV_RECEIPT_DTLS> userCtx = new DataRepository<INV_RECEIPT_DTLS>())
            {
                obj_INV_RECEIPT_DTLS = userCtx.Find(r =>
                    (r.RECEIPT_DTL_ID == str_Id)
                    ).SingleOrDefault();
            }
            return obj_INV_RECEIPT_DTLS;
        }

        public static void fn_getGRNNo(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(PurchaseItemReceipt_DAL.getGRNNo()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "GRN_NUM", "RECEIPT_ID", dtDropDownData, true, false);
        }

        public static void fn_GetPOItemLineData(ref DropDownList ddlList, string receiptId)
        {
            dtDropDownData = DBMethod.ExecuteQuery(PurchaseItemReceipt_DAL.GetPOItemLine(receiptId)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.PO_DATA, FINColumnConstants.RECEIPT_DTL_ID, dtDropDownData, true, false);
        }
        public static void fn_GetPOItemLine(ref DropDownList ddlList, string receiptId)
        {
            dtDropDownData = DBMethod.ExecuteQuery(PurchaseItemReceipt_DAL.GetPOItemLine(receiptId)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.PO_DATA, FINColumnConstants.ITEM_ID, dtDropDownData, true, false);
        }
        public static void fn_GetPOItemLines(ref DropDownList ddlList, string receiptId)
        {
            dtDropDownData = DBMethod.ExecuteQuery(PurchaseItemReceipt_DAL.GetPOItemLine(receiptId)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.RECEIPT_DTL_ID, FINColumnConstants.ITEM_ID, dtDropDownData, true, false);
        }
        public static void GetPOItemLineBasedLot(ref DropDownList ddlList, string receiptId)
        {
            dtDropDownData = DBMethod.ExecuteQuery(PurchaseItemReceipt_DAL.GetPOItemLineBasedLot(receiptId)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.PO_DATA, FINColumnConstants.RECEIPT_DTL_ID, dtDropDownData, true, false);
        }
        public static DataSet GetReportData()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.AP.PurchaseItemReceipt_DAL.getReportData());
        }
        public static DataSet GetPOwiseItemreceiptReportData()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.AP.PurchaseItemReceipt_DAL.getPOwiseItemReceiptReportData());
        }
        public static void GetGRNNumber(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(PurchaseItemReceipt_DAL.getGRNNumber()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "GRN_NUM", "GRN_NUM", dtDropDownData, false, true);
        }

        public static DataTable fn_getGRNItemRecDet(string str_grnid, string str_itemid)
        {
            return DBMethod.ExecuteQuery(FIN.DAL.AP.PurchaseItemReceipt_DAL.getGRNItemRecDet(str_grnid, str_itemid)).Tables[0];
        }
    }
}
