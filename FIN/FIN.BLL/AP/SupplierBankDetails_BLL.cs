﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.AP;
using VMVServices.Web;
using System.Data;

namespace FIN.BLL.AP
{
  public  class SupplierBankDetails_BLL
  {
      DropDownList ddlList = new DropDownList();
      static DataTable dtDropDownData = new DataTable();

      # region FillCombo
      public void fn_getSupplierName(ref DropDownList ddlList)
      {
          dtDropDownData = DBMethod.ExecuteQuery(SupplierBankDetails_DAL.getSupplierName()).Tables[0];
          CommonUtils.LoadDropDownList(ddlList, "VENDOR_CODE", "VENDOR_ID", dtDropDownData, true, false);
      }

      public void fn_getfrlookup(ref DropDownList ddlList, string typeName)
      {
          dtDropDownData = DBMethod.ExecuteQuery(SupplierBankDetails_DAL.getAccounttype(typeName)).Tables[0];
          CommonUtils.LoadDropDownList(ddlList, "VALUE_NAME", "VALUE_KEY_ID", dtDropDownData, true, false);
      }
      public void fn_getBankBranchName(ref DropDownList ddlList)
      {
          dtDropDownData = DBMethod.ExecuteQuery(SupplierBankDetails_DAL.getBankBranchName()).Tables[0];
          CommonUtils.LoadDropDownList(ddlList, "BANK_BRANCH_NAME", "BANK_BRANCH_ID", dtDropDownData, true, false);
      }

      public void fn_getBankBranchNameasper_bank(ref DropDownList ddlList, string BANK_ID)
      {
          dtDropDownData = DBMethod.ExecuteQuery(SupplierBankDetails_DAL.getBankBranchNameasper_bank(BANK_ID)).Tables[0];
          CommonUtils.LoadDropDownList(ddlList, "BANK_BRANCH_NAME", "BANK_BRANCH_ID", dtDropDownData, true, false);
      }

      public void fn_getBankName(ref DropDownList ddlList)
      {
          dtDropDownData = DBMethod.ExecuteQuery(SupplierBankDetails_DAL.getBankName()).Tables[0];
          CommonUtils.LoadDropDownList(ddlList, "BANK_NAME", "BANK_ID", dtDropDownData, true, false);
      }

      # endregion

      //public static DataTable getChildEntityDet(string Str_ID)
      //{
      //    DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.AP.SupplierBankDetails_DAL.GetSupplierbankdtls(Str_ID)).Tables[0];
      //    return dt_Det;
      //}

      public static void SavePCEntity<T, TC>(T entity, List<Tuple<object, string>> tmpChildEntity, TC ChildEntity, bool modifyMode = false)
          where T : class
          where TC : class
      {
          try
          {
              DBMethod.SavePCEntity<T, TC>(entity, tmpChildEntity, ChildEntity, modifyMode);
          }
          catch (Exception ex)
          {
              throw ex.InnerException;
          }

      }


      //public static SUPPLIER_CUSTOMER_BANK getClassEntity(string str_Id)
      //{
      //    SUPPLIER_CUSTOMER_BANK obj_SUPPLIER_CUSTOMER_BANK = new SUPPLIER_CUSTOMER_BANK();
      //    using (IRepository<SUPPLIER_CUSTOMER_BANK> userCtx = new DataRepository<SUPPLIER_CUSTOMER_BANK>())
      //    {
      //        obj_SUPPLIER_CUSTOMER_BANK = userCtx.Find(r =>
      //            (r.PK_ID == str_Id)
      //            ).SingleOrDefault();
      //    }


      //    return obj_SUPPLIER_CUSTOMER_BANK;
      //}
      //public static String ValidateEntity(GL_ACCT_CODES obj_SUPPLIER_CUSTOMER_BANK)
      //{
      //    string str_Error = "";
      //    return str_Error;
      //}


      //public static String DeleteEntity(int int_PKID)
      //{
      //    string str_Message = "";
      //    GL_ACCT_GROUPS obj_GL_ACCT_GROUPS = new GL_ACCT_GROUPS();
      //    obj_GL_ACCT_GROUPS.PK_ID = int_PKID;
      //    DBMethod.DeleteEntity<GL_ACCT_GROUPS>(obj_GL_ACCT_GROUPS);
      //    return str_Message;
      //}

      public static DataSet GetSupplierBankDtlsReportData(bool customer=false)
      {
          return DBMethod.ExecuteQuery(FIN.DAL.AP.SupplierBankDetails_DAL.getSupplierBankDtlsReportData(customer));
      }

  }
}
