﻿using System.Linq;
using System.Text;

using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.AP;
using VMVServices.Web;
using System.Data;

namespace FIN.BLL.AP
{
    public class ServiceContract_BLL
    {
        static DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();
        public static DataSet GetServiceContractReportData()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.AP.ServiceContract_DAL.getServiceContractReportData());
        }
        public static DataSet GetServiceContractSummaryReportData()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.AP.ServiceContract_DAL.getServiceContractSummaryReportData());
        }
        public static void GetContractNumber(ref DropDownList ddlList, bool includeAll = true)
        {
            dtDropDownData = DBMethod.ExecuteQuery(ServiceContract_DAL.getContractNumber()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "serv_contr_num", "serv_contr_id", dtDropDownData, !includeAll, includeAll);
        }
    }
}
