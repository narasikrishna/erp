﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.AP;
using VMVServices.Web;
using System.Data;


namespace FIN.BLL.AP
{
    public class PurchaseOrder_BLL
    {
        static DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();


        public static PO_HEADERS getClassEntity(string str_Id)
        {
            PO_HEADERS obj_PO_HEADERS = new PO_HEADERS();
            using (IRepository<PO_HEADERS> userCtx = new DataRepository<PO_HEADERS>())
            {
                obj_PO_HEADERS = userCtx.Find(r =>
                    (r.PO_HEADER_ID == str_Id)
                    ).SingleOrDefault();
            }
            return obj_PO_HEADERS;
        }
        public static PO_LINES getDetailClassEntity(string str_Id)
        {
            PO_LINES obj_PO_LINES = new PO_LINES();
            using (IRepository<PO_LINES> userCtx = new DataRepository<PO_LINES>())
            {
                obj_PO_LINES = userCtx.Find(r =>
                    (r.PO_LINE_ID == str_Id)
                    ).SingleOrDefault();
            }
            return obj_PO_LINES;
        }

        public static HR_EMP_LEAVE_SAL_ELEMENT getDetailSalClassEntity(string str_Id)
        {
            HR_EMP_LEAVE_SAL_ELEMENT obj_SAL_ELEMENT = new HR_EMP_LEAVE_SAL_ELEMENT();
            using (IRepository<HR_EMP_LEAVE_SAL_ELEMENT> userCtx = new DataRepository<HR_EMP_LEAVE_SAL_ELEMENT>())
            {
                obj_SAL_ELEMENT = userCtx.Find(r =>
                    (r.LEAVE_SAL_ELEMENT_ID == str_Id)
                    ).SingleOrDefault();
            }
            return obj_SAL_ELEMENT;
        }

        public static string CalculateAmount(DataTable dtGridData, decimal currentPOAmount)
        {
            decimal CalculateAmount = currentPOAmount;

            if (dtGridData != null)
            {
                if (dtGridData.Rows.Count >= 1)
                {
                    for (int i = 0; i < dtGridData.Rows.Count; i++)
                    {
                        //if (rowIndex != i)
                        //{
                        if (dtGridData.Rows[i]["PO_line_Amount"].ToString() != null)
                        {
                            CalculateAmount = CalculateAmount + CommonUtils.ConvertStringToDecimal(dtGridData.Rows[i]["PO_line_Amount"].ToString());
                        }
                        // }
                    }
                }
                else
                {
                    CalculateAmount = currentPOAmount;
                }
            }
            return CalculateAmount.ToString();
        }
        # region FillCombo
        public static void GetPODetails(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(PurchaseOrder_DAL.GetPODetails()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "po_data", FINColumnConstants.PO_LINE_ID, dtDropDownData, true, false);            
        }
        public static void GetPODetailsBasedSupplier(ref DropDownList ddlList, string vendorId)
        {
            dtDropDownData = DBMethod.ExecuteQuery(PurchaseOrder_DAL.GetPODetailsBasedSupplier(vendorId)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "po_data", FINColumnConstants.PO_LINE_ID, dtDropDownData, true, false);
        }
        public static void GetItemUnitBasedSupplierprice(ref DropDownList ddlList, string ITEM_ID)
        {
            dtDropDownData = DBMethod.ExecuteQuery(PurchaseOrder_DAL.GetItemUnitBasedSupplierprice(ITEM_ID)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "PO_ITEM_UNIT_PRICE", "PO_ITEM_UNIT_PRICE", dtDropDownData, true, false);
        }
        # endregion
        public static DataSet GetReportData()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.AP.PurchaseOrder_DAL.getReportData());
        }
        public static void GetPONumber(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(PurchaseOrder_DAL.getPONumber()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "PO_NUMBER", "PO_HEADER_ID", dtDropDownData, true, false);
        }
        public static void GetPONumber4Supplier(ref DropDownList ddlList,string str_Supplier)
        {
            dtDropDownData = DBMethod.ExecuteQuery(PurchaseOrder_DAL.getPONumber4Supplier(str_Supplier)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "PO_NUMBER", "PO_HEADER_ID", dtDropDownData, true, false);
        }

        public static DataSet GetPONumberReportData(string poNum = "")
        {
            return DBMethod.ExecuteQuery(FIN.DAL.AP.PurchaseOrder_DAL.getPONumberReportData(poNum));
        }
        public static DataSet GetPOLineNumberReportData()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.AP.PurchaseOrder_DAL.getPOLineNumberReportData());
        }

        public static DataSet GetPOLineItem2ReportData()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.AP.PurchaseOrder_DAL.getPOLineItem2ReportData());
        }

        public static void GetPOLineNumber(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(PurchaseOrder_DAL.getPOLineNumber()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.PO_LINE_NUM,FINColumnConstants.PO_LINE_NUM, dtDropDownData, false, true);
        }
        public static void GetPOLineNumberWithSupplier(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(PurchaseOrder_DAL.getPOLineNumber()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.PO_LINE_NUM, FINColumnConstants.PO_LINE_NUM, dtDropDownData, false, true);
        }
        public static void GetItem4PoHeaderid(ref DropDownList ddlList,string str_PoHeaderid)
        {
            dtDropDownData = DBMethod.ExecuteQuery(PurchaseOrder_DAL.GetPODetails(str_PoHeaderid)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.ITEM_NAME, FINColumnConstants.ITEM_ID, dtDropDownData, true, false);
       
        }
        public static void GetItemBasedVendor(ref DropDownList ddlList, string vendorId = "")
        {
            dtDropDownData = DBMethod.ExecuteQuery(PurchaseOrder_DAL.GetItemBasedVendor(vendorId)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.ITEM_NAME, FINColumnConstants.ITEM_ID, dtDropDownData, false, true);
       
        }
        

    }
}
