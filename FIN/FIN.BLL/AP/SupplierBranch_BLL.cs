﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.AP;
using VMVServices.Web;
using System.Data;

namespace FIN.BLL.AP
{
    public class SupplierBranch_BLL
    {
        DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();

        # region FillCombo
        public static  void fn_getSupplierSite4SupplierName(ref DropDownList ddlList,string str_SupplierId,bool includeAll=false)
        {
            dtDropDownData = DBMethod.ExecuteQuery(SupplierBranch_DAL.getSupplierSite4SupplierName(str_SupplierId)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "VENDOR_BRANCH_CODE", "VENDOR_LOC_ID", dtDropDownData, !includeAll, includeAll);
        }
        public static void fn_getSupplierSite(ref DropDownList ddlList, bool includeAll = false)
        {
            dtDropDownData = DBMethod.ExecuteQuery(SupplierBranch_DAL.getSupplierSite4SupplierName()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "VENDOR_BRANCH_CODE", "VENDOR_LOC_ID", dtDropDownData, !includeAll, includeAll);
        }
        public static void GetSupplierNameFromBranch(ref DropDownList ddlList, bool includeAll = false)
        {
            dtDropDownData = DBMethod.ExecuteQuery(SupplierBranch_DAL.GetSupplierNameFromBranch()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "VEND_NAME", "VENDOR_ID", dtDropDownData, !includeAll, includeAll);
        }
        # endregion
    }
}
