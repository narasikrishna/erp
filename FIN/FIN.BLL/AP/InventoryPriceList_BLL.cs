﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FIN.DAL;
using FIN.DAL.AP;
using FIN.DAL.HR;
using VMVServices.Web;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;

namespace FIN.BLL.AP
{
   public class InventoryPriceList_BLL
    {
        DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();
        public static void GetItemCode(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(InventoryPriceList_DAL.GetItemCode()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.ITEM_CODE, FINColumnConstants.ITEM_ID, dtDropDownData, true, false);
        }
        public static void GetPriceListCurrency(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(InventoryPriceList_DAL.GetPricelistCurrency()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.CURRENCY_DESC, FINColumnConstants.CURRENCY_ID, dtDropDownData, true, false);
        }
        public static void GetInventoryPriceList(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(InventoryPriceList_DAL.GetInventoryPriceList()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "price_list_name", "price_hdr_id", dtDropDownData, true, false);
        }
       
        public static void GetInventoryPriceListDtls(ref DropDownList ddlList, string priceListId, string ITEM_ID)
        {
            dtDropDownData = DBMethod.ExecuteQuery(InventoryPriceList_DAL.GetInventoryPriceListDtls(priceListId,ITEM_ID)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "PO_ITEM_UNIT_PRICE", "PO_ITEM_UNIT_PRICE", dtDropDownData, true, false);
        }
        public static DataTable getChildEntityDet(string Str_ID)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.AP.InventoryPriceList_DAL.GetInventoryPriceListDtls(Str_ID)).Tables[0];
            return dt_Det;
        }
        public static DataTable getItemDtls(string Item_ID)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.AP.InventoryPriceList_DAL.GetItem_Dtls(Item_ID)).Tables[0];
            return dt_Det;
        }

        public static DataSet GetInventoryPriceListReport()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.AP.InventoryPriceList_DAL.getInventoryPriceListReport());
        }
    }
}
