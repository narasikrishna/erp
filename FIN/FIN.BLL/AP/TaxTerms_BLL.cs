﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using FIN.DAL;

namespace FIN.BLL.AP
{
    public class TaxTerms_BLL
    {
        public static DataTable getTaxDetails()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.AP.TaxTerms_DAL.getTaxDetails()).Tables[0];
        }
    }
}
