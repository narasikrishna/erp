﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.AP;
using VMVServices.Web;
using System.Data;


namespace FIN.BLL.AP
{
    public class ItemOtherCost_BLL
    {
        public static INV_ITEM_COSTING getClassEntity(string str_Id)
        {
            INV_ITEM_COSTING obj_INV_ITEM_COSTING = new INV_ITEM_COSTING();
            using (IRepository<INV_ITEM_COSTING> userCtx = new DataRepository<INV_ITEM_COSTING>())
            {
                obj_INV_ITEM_COSTING = userCtx.Find(r =>
                    (r.INV_ITEM_COST_ID == str_Id)
                    ).SingleOrDefault();
            }
            return obj_INV_ITEM_COSTING;
        }

    }
}
