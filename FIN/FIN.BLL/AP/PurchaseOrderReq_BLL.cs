﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.AP;
using VMVServices.Web;
using System.Data;


namespace FIN.BLL.AP
{
    public class PurchaseOrderReq_BLL
    {
       static DropDownList ddlList = new DropDownList();
       static DataTable dtDropDownData = new DataTable();

        # region FillCombo

        # endregion

        public static PO_REQUISITION_HDR getClassEntity(string str_Id)
        {
            PO_REQUISITION_HDR obj_PO_REQUISITION_HDR = new PO_REQUISITION_HDR();
            using (IRepository<PO_REQUISITION_HDR> userCtx = new DataRepository<PO_REQUISITION_HDR>())
            {
                obj_PO_REQUISITION_HDR = userCtx.Find(r =>
                    (r.PO_REQ_ID == str_Id)
                    ).SingleOrDefault();
            }
            return obj_PO_REQUISITION_HDR;
        }
        public static PO_REQUISITION_DTL getDetailClassEntity(string str_Id)
        {
            PO_REQUISITION_DTL obj_PO_REQUISITION_DTL = new PO_REQUISITION_DTL();
            using (IRepository<PO_REQUISITION_DTL> userCtx = new DataRepository<PO_REQUISITION_DTL>())
            {
                obj_PO_REQUISITION_DTL = userCtx.Find(r =>
                    (r.PO_REQ_LINE_ID == str_Id)
                    ).SingleOrDefault();
            }
            return obj_PO_REQUISITION_DTL;
        }

        public static string CalculateAmount(DataTable dtGridData, decimal currentPOAmount)
        {
            decimal CalculateAmount = currentPOAmount;

            if (dtGridData != null)
            {
                if (dtGridData.Rows.Count >= 1)
                {
                    for (int i = 0; i < dtGridData.Rows.Count; i++)
                    {
                        //if (rowIndex != i)
                        //{
                        if (dtGridData.Rows[i]["PO_Amount"].ToString() != null)
                        {
                            CalculateAmount = CalculateAmount + CommonUtils.ConvertStringToDecimal(dtGridData.Rows[i]["PO_Amount"].ToString());
                        }
                        // }
                    }
                }
                else
                {
                    CalculateAmount = currentPOAmount;
                }
            }
            return CalculateAmount.ToString();
        }

        # region FillCombo
        public static void GetPORequsitionNo(ref DropDownList ddlList)
        {
            using (IRepository<PO_REQUISITION_HDR> userCtx = new DataRepository<PO_REQUISITION_HDR>())
            {
                CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.PO_REQ_ID, FINColumnConstants.PO_REQ_ID, userCtx.Find(x => x.ENABLED_FLAG == "1" && x.WORKFLOW_COMPLETION_STATUS == "1").OrderBy(x => x.PO_REQ_ID).ThenBy(x => x.PO_REQ_ID), true, false);
            }
        }

        # endregion
        public static DataSet GetReportData()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.AP.PurchaseOrderReq_DAL.getReportData());
        }
        public static void GetRequisitionNumber(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(FIN.DAL.AP.PurchaseOrderReq_DAL.getRequisitionNumber()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "PO_REQ_ID","PO_REQ_ID", dtDropDownData, true, false);
        }


        public static void GetPOReqNumber(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(FIN.DAL.AP.PurchaseOrderReq_DAL.getPOReqNumber()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "PO_REQ_DESC", "PO_REQ_ID", dtDropDownData, true, false);
        }


    }
}
