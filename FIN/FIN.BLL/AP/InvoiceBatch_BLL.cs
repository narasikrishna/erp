﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.AP;
using VMVServices.Web;
using System.Data;

namespace FIN.BLL.AP
{
    public class InvoiceBatch_BLL
    {
        static DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();

        # region FillCombo
        public static void fn_getIvoiceBatch(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(InvoiceBatch_DAL.getInvoiceBatchDetails()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.BATCH_DESC, FINColumnConstants.BATCH_ID, dtDropDownData, true, false);
        }
        # endregion
    }
}
