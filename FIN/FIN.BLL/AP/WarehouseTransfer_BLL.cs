﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.AP;
using VMVServices.Web;
using System.Data;
namespace FIN.BLL.AP
{
   public class WarehouseTransfer_BLL
   {
       DropDownList ddlList = new DropDownList();
       DataTable dtDropDownData = new DataTable();

       # region FillCombo


       //public void fn_getLineNo(ref DropDownList ddlList)
       //{
       //    dtDropDownData = DBMethod.ExecuteQuery(WarehouseReceiptItem_DAL.getLineNo()).Tables[0];
       //    CommonUtils.LoadDropDownList(ddlList, "LINE_NUM", "ITEM_ID", dtDropDownData, true, false);
       //}

       public void fn_getWarehouseFROM(ref DropDownList ddlList)
       {
           dtDropDownData = DBMethod.ExecuteQuery(WarehouseTransfer_DAL.getWarehouseFROM()).Tables[0];
           CommonUtils.LoadDropDownList(ddlList, "INV_WH_ID", "INV_WH_NAME", dtDropDownData, true, false);
       }
       public void fn_getWarehouseTO(ref DropDownList ddlList)
       {
           dtDropDownData = DBMethod.ExecuteQuery(WarehouseTransfer_DAL.getWarehouseTO()).Tables[0];
           CommonUtils.LoadDropDownList(ddlList, "INV_WH_ID", "INV_WH_NAME", dtDropDownData, true, false);
       }
       public void getWarehouseFROMBasedReceipt(ref DropDownList ddlList)
       {
           dtDropDownData = DBMethod.ExecuteQuery(WarehouseTransfer_DAL.getWarehouseFROMBasedReceipt()).Tables[0];
           CommonUtils.LoadDropDownList(ddlList, "INV_WH_ID", "INV_WH_NAME", dtDropDownData, true, false);
       }
       public void fn_getItem(ref DropDownList ddlList)
       {
           dtDropDownData = DBMethod.ExecuteQuery(Item_DAL.getItemNameDetails()).Tables[0];
           CommonUtils.LoadDropDownList(ddlList, "ITEM_NAME", "ITEM_ID", dtDropDownData, true, false);
       }

       public void fn_getLotNo(ref DropDownList ddlList)
       {
           dtDropDownData = DBMethod.ExecuteQuery(WarehouseTransfer_DAL.getLotNo()).Tables[0];
           CommonUtils.LoadDropDownList(ddlList, "LOT_NUMBER", "LOT_ID", dtDropDownData, true, false);
       }


       # endregion




       public static DataTable getChildEntityDet(string Str_ID)
       {
           DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.AP.WarehouseTransfer_DAL.GetWHTransdtls(Str_ID)).Tables[0];
           return dt_Det;
       }
       public static void SavePCEntity<T, TC>(T entity, List<Tuple<object, string>> tmpChildEntity, TC ChildEntity, bool modifyMode = false)
           where T : class
           where TC : class
       {
           try
           {
               DBMethod.SavePCEntity<T, TC>(entity, tmpChildEntity, ChildEntity, modifyMode);
           }
           catch (Exception ex)
           {
               throw ex.InnerException;
           }

       }

       public static INV_TRANSFER_HDR getClassEntity(string str_Id)
       {
           INV_TRANSFER_HDR obj_INV_TRANSFER_HDR = new INV_TRANSFER_HDR();
           using (IRepository<INV_TRANSFER_HDR> userCtx = new DataRepository<INV_TRANSFER_HDR>())
           {
               obj_INV_TRANSFER_HDR = userCtx.Find(r =>
                   (r.INV_TRANS_ID == str_Id)
                   ).SingleOrDefault();
           }


           return obj_INV_TRANSFER_HDR;
       }
       public static String ValidateEntity(INV_TRANSFER_HDR obj_INV_TRANSFER_HDR)
       {
           string str_Error = "";
           return str_Error;
       }


       //public static String DeleteEntity(int int_PKID)
       //{
       //    string str_Message = "";
       //    GL_ACCT_GROUPS obj_GL_ACCT_GROUPS = new GL_ACCT_GROUPS();
       //    obj_GL_ACCT_GROUPS.PK_ID = int_PKID;
       //    DBMethod.DeleteEntity<GL_ACCT_GROUPS>(obj_GL_ACCT_GROUPS);
       //    return str_Message;
       //}



   }
}
