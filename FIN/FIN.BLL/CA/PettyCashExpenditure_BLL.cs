﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FIN.DAL;
using FIN.DAL.AP;
using FIN.DAL.HR;
using VMVServices.Web;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;

namespace FIN.BLL.CA
{
   public class PettyCashExpenditure_BLL
    {
        DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();
        public static DataTable getChildEntityDet(string Str_ID)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.CA.PettyCashExpenditure_DAL.GetPettyCashExpendituredtls(Str_ID)).Tables[0];
            return dt_Det;
        }

        public static void GetAccountdtls(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(FIN.DAL.CA.PettyCashExpenditure_DAL.GetAccountdtls()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.ACCT_CODE, FINColumnConstants.ACCT_CODE_ID, dtDropDownData, true, false);
        }
        public static void GetUserdtls(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(FIN.DAL.CA.PettyCashExpenditure_DAL.GetUserdtls()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.USER_CODE, FINColumnConstants.USER_CODE, dtDropDownData, true, false);
        }

        public static void GetUserEmpDtl(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(FIN.DAL.CA.PettyCashExpenditure_DAL.GetUserEmpDtl()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.USER_NAME, FINColumnConstants.USER_CODE, dtDropDownData, true, false);
        }

        public static void GetBalanceAmt(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(FIN.DAL.CA.PettyCashExpenditure_DAL.GetBalanceAmt()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "GL_PCA_BALANCE_AMOUNT", "GL_PCA_BALANCE_AMOUNT", dtDropDownData, true, false);
        }
    }
}
