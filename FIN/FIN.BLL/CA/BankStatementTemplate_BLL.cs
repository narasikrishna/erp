﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.Data;
using FIN.DAL;

namespace FIN.BLL.CA
{
    public class BankStatementTemplate_BLL
    {
        public static void fn_getTemplateName(ref DropDownList ddlList)
        {            
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.CA.BankStatementTemplate_DAL.getBankStatTemplateName()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "BR_TMPL_NAME", "BR_TMPL_NAME", dt_Det, true, false);
        }
    }
}
