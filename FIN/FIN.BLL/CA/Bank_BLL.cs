﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Entities;
using FIN.DAL;
using FIN.BLL;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;


using VMVServices.Services.Data;
using VMVServices.Services.Business;
using System.Configuration;

using System.Data.EntityClient;
using Oracle.DataAccess.Client;
using System.Data.Common;

namespace FIN.BLL.CA
{
    public static class Bank_BLL
    {

        //        # region FillCombo
        public static void fn_getBankName(ref DropDownList ddlList)
        {
            // DataTable dt_Det = new DataTable();
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.CA.Bank_DAL.getBankName()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.BANK_NAME, FINColumnConstants.BANK_ID, dt_Det, true, false);
        }
        public static void fn_getBankNam(ref DropDownList ddlList)
        {
            // DataTable dt_Det = new DataTable();
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.CA.Bank_DAL.getBankName()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.BANK_NAME, FINColumnConstants.BANK_ID, dt_Det, false, true);
        }
        public static void getBankBranchName(ref DropDownList ddlList)
        {
            // DataTable dt_Det = new DataTable();
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.CA.Bank_DAL.getBankBranchName()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.BANK_NAME, FINColumnConstants.BANK_BRANCH_ID, dt_Det, true, false);
        }

        public static void fn_getBankBranchAcctNo(ref DropDownList ddlList)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.CA.Bank_DAL.getBankBranchAcctNo()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "bank_branch_acctno","ACCOUNT_ID", dt_Det, true, false);
        }

        public static void fn_getPaymentNumber(ref DropDownList ddlList,string Vendor_id)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.CA.Bank_DAL.getPaymentNumber(Vendor_id)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.PAY_ID,FINColumnConstants.PAY_ID, dt_Det, true, false);
        }
        public static void fn_getInvoiceNumber(ref DropDownList ddlList)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.CA.Bank_DAL.getInvoiceNumber()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.INV_NUM, FINColumnConstants.inv_id, dt_Det, true, false);
        }
        public static void fn_getInvoiceNumbers(ref DropDownList ddlList)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.CA.Bank_DAL.getInvoiceNumber()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.INV_NUM, FINColumnConstants.inv_id, dt_Det, false, true);
        }
        public static void fn_getInvoiceNumbersWithVendor(ref DropDownList ddlList,string Vendor_id)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.CA.Bank_DAL.getInvoiceNumberWithSupplier(Vendor_id)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.INV_NUM, FINColumnConstants.inv_id, dt_Det, false, true);
        }
        public static void fn_getCurrency(ref DropDownList ddlList)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.CA.Bank_DAL.getCurrency()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.CURRENCY_DESC, FINColumnConstants.CURRENCY_ID, dt_Det, true, false);
        }


        //        # endregion


        public static DataTable fn_getBankShortName(string StrID)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.CA.Bank_DAL.getBankShortName(StrID)).Tables[0];
            return dt_Det;
        }


    }
}
