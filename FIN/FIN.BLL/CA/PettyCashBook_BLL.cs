﻿using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.AP;
using VMVServices.Web;
using System.Data;

namespace FIN.BLL.CA
{
   public class PettyCashBook_BLL
    {
        static DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();
        public static DataSet GetReportData()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.CA.PettyCashBook_DAL.getReportData());
        }
        public static void GetGroupName(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(FIN.DAL.CA.PettyCashBook_DAL.getGlobalSegment()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "SEGMENT_VALUE", "SEGMENT_VALUE_ID", dtDropDownData, false, true);
        }
        public static void GetUser(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(FIN.DAL.CA.PettyCashBook_DAL.getUser()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "GL_PCA_USER_ID", "GL_PCA_USER_ID", dtDropDownData, false, true);
        }

    }
}
