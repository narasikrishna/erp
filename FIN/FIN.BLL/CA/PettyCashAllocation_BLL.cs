﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Entities;
using FIN.DAL.CA;
using FIN.BLL.CA;
using FIN.DAL;
using FIN.BLL;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;


using VMVServices.Services.Data;
using VMVServices.Services.Business;
using System.Configuration;

using System.Data.EntityClient;
using Oracle.DataAccess.Client;
using System.Data.Common;

namespace FIN.BLL.CA
{
    public class PettyCashAllocation_BLL
    {
        static DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();

        public static GL_PETTY_CASH_ALLOCATE_HDR getClassEntity(string str_Id)
        {
            GL_PETTY_CASH_ALLOCATE_HDR obj_GL_PETTY_CASH_ALLOCATE_HDR = new GL_PETTY_CASH_ALLOCATE_HDR();
            using (IRepository<GL_PETTY_CASH_ALLOCATE_HDR> userCtx = new DataRepository<GL_PETTY_CASH_ALLOCATE_HDR>())
            {
                obj_GL_PETTY_CASH_ALLOCATE_HDR = userCtx.Find(r =>
                    (r.GL_PCA_CHECK_HDR_ID == str_Id)
                    ).SingleOrDefault();
            }


            return obj_GL_PETTY_CASH_ALLOCATE_HDR;
        }

        public static void fn_getChequeNum(ref DropDownList ddlList, string check_bank_id, string check_branch_id, string account_id)
        {
            dtDropDownData = DBMethod.ExecuteQuery(PettyCashAllocation_DAL.GetChequeDetails(check_bank_id, check_branch_id, account_id)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.CHECK_HDR_ID, FINColumnConstants.CHECK_HDR_ID, dtDropDownData, true, false);
        }
        public static void fn_getChequeNum_dtl(ref DropDownList ddlList, string chk_hdr_id)
        {
            dtDropDownData = DBMethod.ExecuteQuery(PettyCashAllocation_DAL.GetChequeDetails_dtls(chk_hdr_id)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.CHECK_NUMBER, FINColumnConstants.CHECK_DTL_ID, dtDropDownData, true, false);
        }
        public static void GetChequeNo(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(PettyCashAllocation_DAL.GetChequeNo()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.CHECK_NUMBER, FINColumnConstants.CHECK_DTL_ID, dtDropDownData, true, false);
        }
        public static void GetChequeNo_R(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(PettyCashAllocation_DAL.GetChequeNo()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.CHECK_NUMBER, FINColumnConstants.CHECK_DTL_ID, dtDropDownData, false, true);
        }

        public static void fn_getUsers(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(PettyCashAllocation_DAL.GetUsers()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.USER_CODE, FINColumnConstants.USER_CODE, dtDropDownData, true, false);
        }
        public static DataSet GetPettyCashAllocationDetData()
        {
            return DBMethod.ExecuteQuery(PettyCashAllocation_DAL.GetPettyCashAllocationRepData());
        }
    }
}
