﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Entities;
using FIN.DAL.CA;
using FIN.BLL.CA;
using FIN.DAL;
using FIN.BLL;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;


using VMVServices.Services.Data;
using VMVServices.Services.Business;
using System.Configuration;

using System.Data.EntityClient;
using Oracle.DataAccess.Client;
using System.Data.Common;

namespace FIN.BLL.CA
{
    public static class Cheque_BLL
    {


        static DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();


        public static DataTable getAccountingGroupLinksDetails(string Str_ID)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.GL.AccountingGroupLinks_DAL.getAccountingGroupLinksDetails(Str_ID)).Tables[0];
            return dt_Det;
        }

        //        # region FillCombo

        public static void fn_getAccountNumber(ref DropDownList ddlList, string BANK_ID, string CHECK_BRANCH_ID)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Cheque_DAL.GetAccountNumberDetails(BANK_ID, CHECK_BRANCH_ID)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.VENDOR_BANK_ACCOUNT_CODE, FINColumnConstants.ACCOUNT_ID, dtDropDownData, true, false);
        }

        public static void fn_getChequeNumber(ref DropDownList ddlList, string bankId = "", string bankBranchId = "", string accountId = "")
        {
            dtDropDownData = DBMethod.ExecuteQuery(Cheque_DAL.GetCheckNumberDetails(bankId, bankBranchId, accountId)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.CHECK_NUMBER, FINColumnConstants.CHECK_DTL_ID, dtDropDownData, true, false);
        }

        public static void GetUnusedCheckNumberDetails(ref DropDownList ddlList, string bankId = "", string bankBranchId = "", string accountId = "",string str_Mode="")
        {
            dtDropDownData = DBMethod.ExecuteQuery(Cheque_DAL.GetUnusedCheckNumberDetails(bankId, bankBranchId, accountId, str_Mode)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.CHECK_NUMBER, FINColumnConstants.CHECK_DTL_ID, dtDropDownData, true, false);
        }


        public static void GetCheckNumberDetail(ref DropDownList ddlList,  string accountId = "")
        {
            dtDropDownData = DBMethod.ExecuteQuery(Cheque_DAL.getCheckNumberDetails(accountId)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.PAY_CHECK_NUMBER, FINColumnConstants.PAY_CHECK_NUMBER, dtDropDownData, true, false);
        }



        public static void GetUnusedCheck(ref DropDownList ddlList, string bankId = "", string bankBranchId = "", string accountId = "", string str_Mode = "")
        {
            dtDropDownData = DBMethod.ExecuteQuery(Cheque_DAL.GetUnusedCheck(bankId, bankBranchId, accountId, str_Mode)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.CHECK_NUMBER, FINColumnConstants.CHECK_DTL_ID, dtDropDownData, true, false);
        }
        public static void fn_getChequeNumber(ref DropDownList ddlList, string chequeNumber = "")
        {
            dtDropDownData = DBMethod.ExecuteQuery(Cheque_DAL.GetCheckNumberDetails(chequeNumber)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.CHECK_NUMBER, FINColumnConstants.CHECK_DTL_ID, dtDropDownData, true, false);
        }
        //        # endregion


        public static void SavePCEntity<T, TC>(T entity, List<Tuple<object, string>> tmpChildEntity, TC ChildEntity, bool modifyMode = false)
            where T : class
            where TC : class
        {
            try
            {
                DBMethod.SavePCEntity<T, TC>(entity, tmpChildEntity, ChildEntity, modifyMode);
            }
            catch (Exception ex)
            {
                throw ex.InnerException;
            }

        }


        public static CA_CHECK_HDR getClassEntity(string str_Id)
        {
            CA_CHECK_HDR obj_CA_CHECK_HDR = new CA_CHECK_HDR();
            using (IRepository<CA_CHECK_HDR> userCtx = new DataRepository<CA_CHECK_HDR>())
            {
                obj_CA_CHECK_HDR = userCtx.Find(r =>
                    (r.CHECK_HDR_ID == str_Id)
                    ).SingleOrDefault();
            }


            return obj_CA_CHECK_HDR;
        }


        public static CA_CHECK_DTL getDetailClassEntity(string str_Id)
        {
            CA_CHECK_DTL obj_CA_CHECK_DTL = new CA_CHECK_DTL();
            using (IRepository<CA_CHECK_DTL> userCtx = new DataRepository<CA_CHECK_DTL>())
            {
                obj_CA_CHECK_DTL = userCtx.Find(r =>
                    (r.CHECK_DTL_ID == str_Id)
                    ).SingleOrDefault();
            }
            return obj_CA_CHECK_DTL;
        }



        public static String ValidateEntity(CA_CHECK_DTL obj_CA_CHECK_DTL)
        {
            string str_Error = "";
            return str_Error;
        }


        public static String DeleteEntity(int int_PKID)
        {
            string str_Message = "";
            CA_CHECK_DTL obj_CA_CHECK_DTL = new CA_CHECK_DTL();
            obj_CA_CHECK_DTL.PK_ID = int_PKID;
            DBMethod.DeleteEntity<CA_CHECK_DTL>(obj_CA_CHECK_DTL);
            return str_Message;
        }


        public static DataSet GetChequesReportData()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.CA.Cheque_DAL.getCheckViewData());
        }
    }
}
