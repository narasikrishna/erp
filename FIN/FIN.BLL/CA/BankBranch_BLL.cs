﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Entities;
using FIN.DAL;
using FIN.BLL;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;


using VMVServices.Services.Data;
using VMVServices.Services.Business;
using System.Configuration;

using System.Data.EntityClient;
using Oracle.DataAccess.Client;
using System.Data.Common;

namespace FIN.BLL.CA
{
    public class BankBranch_BLL
    {
        //        # region FillCombo
        public static void fn_getBranchName(ref DropDownList ddlList, string BANK_ID )
        {
            // DataTable dt_Det = new DataTable();
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.CA.BankBranch_DAL.getBranchName(BANK_ID)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.BANK_BRANCH_NAME, FINColumnConstants.BANK_BRANCH_ID, dt_Det, true, false);
        }
       

        public static void fn_getBankAccountNumber(ref DropDownList ddlList, string BANK_ID)
        {
            // DataTable dt_Det = new DataTable();
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.CA.BankBranch_DAL.getBankAccountNumber(BANK_ID)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.ACCOUNT_ID, FINColumnConstants.BANK_BRANCH_ID, dt_Det, true, false);
        }

        //        # endregion


        public static DataTable fn_getBankBranchShortName(string StrID)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.CA.BankAccounts_DAL.getBankBranchShortName(StrID)).Tables[0];
            return dt_Det;
        }



    }
}
