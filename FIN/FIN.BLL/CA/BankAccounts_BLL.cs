﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Entities;
using FIN.DAL;
using FIN.BLL;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL.CA;

using VMVServices.Services.Data;
using VMVServices.Services.Business;
using System.Configuration;

using System.Data.EntityClient;
using Oracle.DataAccess.Client;
using System.Data.Common;

namespace FIN.BLL.CA
{
    public static class BankAccounts_BLL
    {
        public static void fn_getBankAccount(ref DropDownList ddlList, string bankId, string bankBranchId)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(BankAccounts_DAL.getBankAccount(bankId, bankBranchId)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.VENDOR_BANK_ACCOUNT_CODE, FINColumnConstants.ACCOUNT_ID, dt_Det, true, false);
        }

        public static void fn_getFullDetails(ref DropDownList ddlList)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(BankAccounts_DAL.getBankFullDetails()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "BANK_FULL_NAME", FINColumnConstants.ACCOUNT_ID, dt_Det, true, false);
        }
    }
}
