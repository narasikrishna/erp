﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Entities;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.BLL;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;

using VMVServices.Services.Data;
using VMVServices.Services.Business;
using System.Configuration;

using System.Data.EntityClient;
using Oracle.DataAccess.Client;
using System.Data.Common;

namespace FIN.BLL.HR
{
    public class AppraisalAssesment_BLL
    {
        static DataTable dtDropDownData = new DataTable();
        public static DataTable fn_GetKPI4Apprsial(string Str_ID,string apprasial_id)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.HR.AppraisalAssesment_DAL.GetKPI4Apprsial(Str_ID, apprasial_id)).Tables[0];
            return dt_Det;
        }

        public static void GetEmplName(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(FIN.DAL.HR.AppraisalAssesment_DAL.GetAppAssessmentEmpDetails()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.EMP_NAME, FINColumnConstants.EMP_ID, dtDropDownData, false, true);
        }

        public static void GetTrainingEmployeeDetails(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(FIN.DAL.HR.Employee_DAL.GetTrainingEmployeeDetails()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.EMP_NAME, FINColumnConstants.EMP_ID, dtDropDownData, true, false);
        }

        public static void GetCourseDetails(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(FIN.DAL.HR.AppraisalAssesment_DAL.GetAppAssessmentCourseDetails()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.COURSE_DESC, FINColumnConstants.COURSE_ID, dtDropDownData, true, false);
        }
    
        
    }
}
