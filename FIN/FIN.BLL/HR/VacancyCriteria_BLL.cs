﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.HR;
using System.Data;

namespace FIN.BLL.HR
{
    public class VacancyCriteria_BLL
    {
        static DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();

     
        public static HR_VAC_EVAL_CONDITION getClassEntity(string str_Id)
        {
            HR_VAC_EVAL_CONDITION obj_HR_VAC_EVAL_CONDITION = new HR_VAC_EVAL_CONDITION();
            using (IRepository<HR_VAC_EVAL_CONDITION> userCtx = new DataRepository<HR_VAC_EVAL_CONDITION>())
            {
                obj_HR_VAC_EVAL_CONDITION = userCtx.Find(r =>
                    (r.VAC_EVAL_ID == (str_Id.ToString()))
                    ).SingleOrDefault();
            }
            return obj_HR_VAC_EVAL_CONDITION;
        }


        public static HR_VAC_CRITERIA_MASTER getHdrClassEntity(string str_Id)
        {
            HR_VAC_CRITERIA_MASTER obj_HR_VAC_CRITERIA_MASTER = new HR_VAC_CRITERIA_MASTER();
            using (IRepository<HR_VAC_CRITERIA_MASTER> userCtx = new DataRepository<HR_VAC_CRITERIA_MASTER>())
            {
                obj_HR_VAC_CRITERIA_MASTER = userCtx.Find(r =>
                    (r.VAC_CRIT_ID == (str_Id.ToString()))
                    ).SingleOrDefault();
            }
            return obj_HR_VAC_CRITERIA_MASTER;
        }

       
        public static void getEvalCondition(ref DropDownList ddlList,string str_vacid)
        {
            dtDropDownData = DBMethod.ExecuteQuery(VacancyCriteria_DAL.getEvalCondition(str_vacid)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "VAC_CRIT_NAME", "VAC_EVAL_ID", dtDropDownData, true, false);
        }
    }
}
