﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.HR;
using VMVServices.Web;
using System.Data;
namespace FIN.BLL.HR
{
    public class Reimbursementtype_BLL
    {

        
        static DataTable dtDropDownData = new DataTable();

        public static DataTable getChildEntityDet(string Str_ID)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.HR.Reimbursementtype_DAL.GetReimbursementtype(Str_ID)).Tables[0];
            return dt_Det;
        }

        public static void fn_getCategory(ref DropDownList ddlList,string mode)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Category_DAL.getCategory(mode)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.CATEGORY_DESC, FINColumnConstants.CATEGORY_ID, dtDropDownData, true, false);
        }
        public static void fn_getCategory_R(ref DropDownList ddlList, string mode)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Category_DAL.getCategory(mode)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.CATEGORY_DESC, FINColumnConstants.CATEGORY_ID, dtDropDownData, false, true);
        }

        public static void fn_getReimbtyp(ref DropDownList ddlList, string mode)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Reimbursementtype_DAL.getReimbtype(mode)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.REIMB_TYP_DESC, FINColumnConstants.REIMB_TYP_ID, dtDropDownData, false, true);
        }
       
    }
}
