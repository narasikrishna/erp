﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.HR;

using System.Data;

namespace FIN.BLL.HR
{
    public class InterviewResult_BLL
    {
        public static HR_INTERVIEWS_RESULTS getClassEntity(string str_Id)
        {
            HR_INTERVIEWS_RESULTS obj_HR_INTERVIEWS_RESULTS = new HR_INTERVIEWS_RESULTS();
            using (IRepository<HR_INTERVIEWS_RESULTS> userCtx = new DataRepository<HR_INTERVIEWS_RESULTS>())
            {
                obj_HR_INTERVIEWS_RESULTS = userCtx.Find(r =>
                    (r.RESULT_ID == str_Id)
                    ).SingleOrDefault();
            }
            return obj_HR_INTERVIEWS_RESULTS;
        }

        public static String ValidateEntity(HR_INTERVIEWS_RESULTS obj_HR_INTERVIEWS_RESULTS)
        {
            string str_Error = "";
            return str_Error;
        }

        public static String SaveEntity(HR_INTERVIEWS_RESULTS obj_HR_INTERVIEWS_RESULTS, string str_Mode, bool GenPKID = true)
        {
            string str_PK = "";

            switch (str_Mode)
            {
                case FINAppConstants.Add:
                    {

                        DBMethod.SaveEntity<HR_INTERVIEWS_RESULTS>(obj_HR_INTERVIEWS_RESULTS);
                        break;
                    }
                case FINAppConstants.Update:
                    {
                        DBMethod.SaveEntity<HR_INTERVIEWS_RESULTS>(obj_HR_INTERVIEWS_RESULTS, true);
                        break;
                    }
            }
            return str_PK;
        }
       
    }
}
