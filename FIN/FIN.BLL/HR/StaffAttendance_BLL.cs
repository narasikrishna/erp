﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.HR;

using System.Data;

namespace FIN.BLL.HR
{
    public class StaffAttendance_BLL
    {
        public static HR_STAFF_ATTENDANCE getClassEntity(string str_Id)
        {
            HR_STAFF_ATTENDANCE obj_HR_STAFF_ATTENDANCE = new HR_STAFF_ATTENDANCE();
            using (IRepository<HR_STAFF_ATTENDANCE> userCtx = new DataRepository<HR_STAFF_ATTENDANCE>())
            {
                obj_HR_STAFF_ATTENDANCE = userCtx.Find(r =>
                    (r.STATT_ID == (str_Id.ToString()))
                    ).SingleOrDefault();
            }
            return obj_HR_STAFF_ATTENDANCE;
        }
        public static DataSet getAttendanceRep4Month()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.HR.StaffAttendance_DAL.getAttendanceRep4Month());
        }
    }
}
