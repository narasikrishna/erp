﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.HR;

using System.Data;

namespace FIN.BLL.HR
{
   public class AppraisalTraining_BLL
    {
        public static HR_APPRAISAL_TRAINING getClassEntity(string str_Id)
        {
            HR_APPRAISAL_TRAINING obj_hR_APPRAISAL_TRAINING = new HR_APPRAISAL_TRAINING();
            using (IRepository<HR_APPRAISAL_TRAINING> userCtx = new DataRepository<HR_APPRAISAL_TRAINING>())
            {
                obj_hR_APPRAISAL_TRAINING = userCtx.Find(r =>
                    (r.APPRAISAL_TRAINING_ID == (str_Id.ToString()))
                    ).SingleOrDefault();
            }
            return obj_hR_APPRAISAL_TRAINING;
        }
    }
}
