﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.HR;
using VMVServices.Web;
using System.Data;
namespace FIN.BLL.HR
{
    public class FeedbackTemplateBLL
    {

        DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();

        # region FillCombo
        public static void fn_GetFeedbackTemplateName(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(FeedbackTemplateDAL.getFeedbackTemplateName()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "feedback_temp_name", "feedback_id", dtDropDownData, true, false);
        }
        public static void fn_GetFeedbackTypes(ref DropDownList ddlList, string feedbackID)
        {
            dtDropDownData = DBMethod.ExecuteQuery(FeedbackTemplateDAL.getFeedbackTypes(feedbackID)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "feedback_type", "feedback_id_type", dtDropDownData, true, false);
        }
        public static void fn_GetFeedbackRating(ref DropDownList ddlList, string feedbackID)
        {
            dtDropDownData = DBMethod.ExecuteQuery(FeedbackTemplateDAL.getFeedbackRating(feedbackID)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "feedback_rating_type", "feedback_id_rating", dtDropDownData, true, false);
        }
        public static void fn_GetFeedbackDesc(ref DropDownList ddlList, string feedbackID)
        {
            dtDropDownData = DBMethod.ExecuteQuery(FeedbackTemplateDAL.getFeedbackDesc(feedbackID)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "feedback_desc", "feedback_id_desc", dtDropDownData, true, false);
        }

        public static void fn_GetFeedbackTemplateTypes(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(FeedbackTemplateDAL.getFeedbackTemplateTypes()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "feedback_type_name", "feedback_type_id", dtDropDownData, true, false);
        }
        #endregion

        public static DataTable getChildEntityDet(string Str_ID)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.HR.FeedbackTemplateDAL.GetFeedbackDtls(Str_ID)).Tables[0];
            return dt_Det;
        }

        public static HR_FEEDBACK_DTL getClassEntity(string str_Id)
        {
            HR_FEEDBACK_DTL obj_HR_FEEDBACK_DTL = new HR_FEEDBACK_DTL();
            using (IRepository<HR_FEEDBACK_DTL> userCtx = new DataRepository<HR_FEEDBACK_DTL>())
            {
                obj_HR_FEEDBACK_DTL = userCtx.Find(r =>
                    (r.FEEDBACK_DTL_ID == str_Id)
                    ).SingleOrDefault();
            }

            return obj_HR_FEEDBACK_DTL;
        }

        public static DataTable getEmpChildEntityDetails(string Str_ID)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.HR.FeedbackTemplateDAL.GetEmpFeedbackDtls(Str_ID)).Tables[0];
            return dt_Det;
        }

        public static HR_EMP_FEEDBACK_DTL getEmpClassEntity(string str_Id)
        {
            HR_EMP_FEEDBACK_DTL obj_HR_EMP_FEEDBACK_DTL = new HR_EMP_FEEDBACK_DTL();
            using (IRepository<HR_EMP_FEEDBACK_DTL> userCtx = new DataRepository<HR_EMP_FEEDBACK_DTL>())
            {
                obj_HR_EMP_FEEDBACK_DTL = userCtx.Find(r =>
                    (r.EMP_FEEDBACK_DTL_ID == str_Id)
                    ).SingleOrDefault();
            }

            return obj_HR_EMP_FEEDBACK_DTL;
        }
    }
}
