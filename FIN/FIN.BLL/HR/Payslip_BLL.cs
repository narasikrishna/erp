﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.HR;
using VMVServices.Web;
using System.Data;

namespace FIN.BLL.HR
{
    public class Payslip_BLL
    {
        static DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();
        static string sqlQuery = "";
        public static string get_yearEndLeaveCarryOver(string dt_date)
        {
            sqlQuery = string.Empty;
            sqlQuery = " SELECT * FROM vw_yr_end_lve_carry_over V WHERE ROWNUM > 0 ";
            //sqlQuery += " and v.org_id = '" + VMVServices.Web.Utils.OrganizationID + "'";

            if (VMVServices.Web.Utils.ReportViewFilterParameter != null || VMVServices.Web.Utils.ReportViewFilterParameter.Count != 0)
            {
                //if (VMVServices.Web.Utils.ReportViewFilterParameter["From_Date"] != null)
                //{
                sqlQuery += " AND V.leave_txn_date =  to_date('" + dt_date + "','dd/mm/yyyy')";

                //sqlQuery += " AND V.emp_doj <= to_date('" + VMVServices.Web.Utils.ReportViewFilterParameter["To_Date"].ToString() + "','dd/mm/yyyy')";
                //}
            }
            return sqlQuery;
        }

        public static DataSet GetEarningReportData()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.HR.Payslip_DAL.get_EarningsDeductReportData());
        }

        // public static DataSet GetPayslipReportData()
        //{
        //    DataSet ds = new DataSet();

        //    DataTable EmpDtls = new DataTable();
        //    DataTable EarningDed = new DataTable();

        //    EmpDtls = DBMethod.ExecuteQuery(FIN.DAL.HR.Payslip_DAL.get_EmpDtlsReportData()).Tables[0];
        //    EarningDed = DBMethod.ExecuteQuery(FIN.DAL.HR.Payslip_DAL.get_EarningsReportData()).Tables[0];

        //    DataTable EmpDtlsCopy = new DataTable("Table1");
        //    DataTable EarningDedCopy = new DataTable("Table2");

        //    EmpDtlsCopy = EmpDtls.Copy();
        //    ds.Tables.Add("Table1");          

        //    EarningDedCopy = EarningDed.Copy();
        //    //ds.Tables.Add(EarningDedCopy);
        //    ds.Tables.AddRange(new DataTable[] { EarningDedCopy });

        //    return ds;


        //   }

        public static DataSet GetEmployeeDtlsReportData()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.HR.Payslip_DAL.get_EmpDtlsReportData());
        }

        public static DataSet GetDeductionReportData()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.HR.Payslip_DAL.get_DeductionReportData());
        }

        public static DataSet GetEarninReportData()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.HR.Payslip_DAL.get_EarningsReportData());
        }
        public static DataSet GetAbsentReport()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.HR.LeaveDefinition_DAL.get_AbsentReport());
        }

        public static DataSet GetTerminationReport()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.HR.LeaveDefinition_DAL.get_TerminationReport());
        }

        public static DataSet GetLeaveBalanceReport()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.HR.LeaveDefinition_DAL.get_LeaveBalanceReport());
        }

        public static DataSet GetEmpListDOA()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.HR.LeaveDefinition_DAL.get_EmpListDOA());
        }

        public static DataSet GetEmpListEOS()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.HR.LeaveDefinition_DAL.get_EmpListEOS());
        }

        public static DataSet GetVacationSpecified()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.HR.LeaveDefinition_DAL.get_VacationSpecified());
        }

        public static DataSet GetEmployeeVacation()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.HR.LeaveDefinition_DAL.get_EmployeeVacation());
        }

        public static DataSet GetEmpDetailsReport()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.HR.LeaveDefinition_DAL.get_EmpDetailsReport());
        }

        public static DataSet GetMedInsuranceRequestReport()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.HR.LeaveDefinition_DAL.get_MedicalInsureRequestReport());
        }

        public static DataSet GetEmegrationWPhotoReport()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.HR.LeaveDefinition_DAL.get_EmegrationWPhotoReport());
        }

        public static DataSet GetTrainingRequestReport()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.HR.LeaveDefinition_DAL.get_TrainingRequestReport());
        }

        public static DataSet GetInductionCheckilistReport()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.HR.LeaveDefinition_DAL.get_InductionCheckilistReport());
        }

        public static DataSet GetEmpPermitDetailsReport()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.HR.LeaveDefinition_DAL.get_EmpPermitDetailsReport());
        }

        public static DataSet GetTrainingFeedbackReport()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.HR.TrainingFeedback_DAL.get_TrainingFeedbackReport());
        }

        public static DataSet GetProgramAnalysisLearning()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.HR.TrainingFeedback_DAL.GetProgramLearnAnalysis());
        }

        public static DataSet GetClearanceFormReport()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.HR.TrainingFeedback_DAL.get_ClearanceFormReport());
        }
        public static DataSet GetMinistryofSocialAffairsReport()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.HR.TrainingFeedback_DAL.get_MinistryofSocialAffairsReport());
        }
        public static DataSet GetPermissionduringwrkReport()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.HR.TrainingFeedback_DAL.get_PermissionduringwrkReport());
        }
        public static DataSet GetSocialAffairContractReport()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.HR.TrainingFeedback_DAL.get_SocialAffairContractReport());
        }

        public static DataSet GetFinalClearanceFormReport()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.HR.TrainingFeedback_DAL.get_FinalClearanceFormReport());
        }

        public static DataSet GetEmpBankDetailsReport()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.HR.LeaveDefinition_DAL.get_EmpBankDetailsReport());
        }

        public static DataSet GetApplicantDetailsReport()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.HR.LeaveDefinition_DAL.get_ApplicantDetailsReport());
        }


        public static DataSet GetInterviewAssesment()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.HR.LeaveDefinition_DAL.get_InterviewAssesmentReport());
        }
        public static DataSet GetEmpProbationReport()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.HR.LeaveDefinition_DAL.get_EmpProbationReport());
        }

        public static DataSet GetEmpProbationEvalutionReport()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.HR.LeaveDefinition_DAL.get_EmpProbationEvalutionReport());
        }
        public static DataSet GetEmpLoanDtls()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.HR.LoanRequest_DAL.getEmpLoanDtls());
        }
        public static DataSet GetMonthlySalariesRevealedReport()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.HR.TrainingFeedback_DAL.get_MonthlysalariesRevealedReport());
        }
        public static DataSet GetMobileBillingReport()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.HR.TrainingFeedback_DAL.get_MobileBillingReport());
        }
        public static DataSet GetMonthlySalariesRevealedTrailReport()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.HR.TrainingFeedback_DAL.get_MonthlysalariesRevealedTrailReport());
        }
        public static DataSet GetSocialInsuranceInstallmentReport()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.HR.TrainingFeedback_DAL.get_SocialInsuranceInstallmentReport());
        }
        public static DataSet GetSocialInsuranceInstallmentTrailReport()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.HR.TrainingFeedback_DAL.get_SocialInsuranceInstallmentTrailReport());
        }


        public static DataSet GetYearEndLeaveCarryOver(string txmdate)
        {
            return DBMethod.ExecuteQuery(FIN.DAL.HR.LeaveDefinition_DAL.get_yearEndLeaveCarryOver(txmdate));
        }


        public static DataSet GetSalaryReviewReport()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.HR.LeaveDefinition_DAL.get_SalaryReviewReport());
        }


        public static DataSet GetPositionGradeEmpReport()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.HR.TrainingFeedback_DAL.get_PositionGradeEmpReport());
        }


        public static DataSet GetPositionHierarchyReport()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.HR.LeaveDefinition_DAL.get_PositionHierarchyReport());
        }


        public static DataSet GetNationalityCountReport()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.HR.TrainingFeedback_DAL.get_NationalityCountReport());
        }


        public static DataSet GetEmigrationReport()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.HR.LeaveDefinition_DAL.get_EmigrationReport());
        }



        public static DataSet getEmpPayrollComparsionDet()
        {
            return DBMethod.ExecuteQuery(FIN.DAL.HR.Payslip_DAL.getPayrollComparsionDet());
        }


       



    }
}
