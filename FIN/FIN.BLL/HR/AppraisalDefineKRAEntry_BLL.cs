﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Entities;
using FIN.DAL;
using FIN.DAL.HR;
using FIN.BLL;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;

using VMVServices.Services.Data;
using VMVServices.Services.Business;
using System.Configuration;

using System.Data.EntityClient;
using Oracle.DataAccess.Client;
using System.Data.Common;

namespace FIN.BLL.HR
{
    public class AppraisalDefineKRAEntry_BLL
    {


        DropDownList ddlList = new DropDownList();
        static DataTable dtDropDownData = new DataTable();

        # region FillCombo
        public static void fn_GetType(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(AppraisalDefineKRA_DAL.GetType()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.TYPE_NAME, FINColumnConstants.TYPE_ID, dtDropDownData, true, false);
        }

        public static void fn_GetObjectiveDescription(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(AppraisalDefineKRA_DAL.GetObjectiveDescription()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.PER_OBJ_NAME, FINColumnConstants.PER_OBJ_ID, dtDropDownData, true, false);
        }

        public static void fn_GetObjectiveDescriptionasperTyp(ref DropDownList ddlList, string PER_TYPE)
        {
            dtDropDownData = DBMethod.ExecuteQuery(AppraisalDefineKRA_DAL.GetObjectiveDescriptionasperTyp(PER_TYPE)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.PER_OBJ_NAME, FINColumnConstants.PER_OBJ_ID, dtDropDownData, true, false);
        }

        public static void fn_GetJobDetails(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(AppraisalDefineKRA_DAL.GetJobDetails()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, FINColumnConstants.JOB_NAME, FINColumnConstants.JOB_ID, dtDropDownData, true, false);
        }

        public static void GetKPINumber(ref DropDownList ddlList, string str_deptid, string JOB_ID)
        {
            dtDropDownData = DBMethod.ExecuteQuery(AppraisalDefineKRA_DAL.GetKPINumber(str_deptid, JOB_ID)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "assign_hdr_id", "assign_hdr_id", dtDropDownData, true, false);
        }
        public static void fn_GetAppraisalDefineKPIDetails(ref DropDownList ddlList,string str_deptid, string JOB_ID)
        {
            dtDropDownData = DBMethod.ExecuteQuery(AppraisalDefineKRA_DAL.GetAppraisalDefKPI(str_deptid, JOB_ID)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "APP_DESC", "ASSIGN_HDR_ID", dtDropDownData, true, false);
        }

        public static void fn_GetAppraisal(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(AppraisalDefineKRA_DAL.GetAppraisal()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "Description", "ra_hdr_id", dtDropDownData, true, false);
        }

        public static void fn_GetAppraisalDtlsBasedInitiateAppr(ref DropDownList ddlList, string str_deptid, string JOB_ID)
        {
            dtDropDownData = DBMethod.ExecuteQuery(AppraisalDefineKRA_DAL.GetAppraisalDtlsBasedInitiateAppr(str_deptid, JOB_ID)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "APP_DESC", "ASSIGN_HDR_ID", dtDropDownData, true, false);
        }

        public static void GetAraisal(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(AppraisalDefineKRA_DAL.GetAraisal()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "APP_DESC", "ASSIGN_HDR_ID", dtDropDownData, true, false);
        }

        public static void fn_GetAppraisalDtlsBasedInitiateAppr(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(AppraisalDefineKRA_DAL.GetAppraisalDtlsBasedInitiateAppr()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "APP_DESC", "ASSIGN_HDR_ID", dtDropDownData, true, false);
        }
        public static void GetAppraisalDetails(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(AppraisalDefineKRA_DAL.GetAppraisalDetails()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "APP_DESC", "APP_ID", dtDropDownData, true, false);
        }
        public static void fn_GetAppraisalDescriptionDetails(ref DropDownList ddlList, string str_deptid, string JOB_ID)
        {
            dtDropDownData = DBMethod.ExecuteQuery(AppraisalDefineKRA_DAL.GetAppraisalDescription(str_deptid, JOB_ID)).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "Description", "ra_hdr_id", dtDropDownData, true, false);
        }
        # endregion

        public static void SavePCEntity<T, TC>(T entity, List<Tuple<object, string>> tmpChildEntity, TC ChildEntity, bool modifyMode = false)
            where T : class
            where TC : class
        {
            try
            {
                DBMethod.SavePCEntity<T, TC>(entity, tmpChildEntity, ChildEntity, modifyMode);
            }
            catch (Exception ex)
            {
                throw ex.InnerException;
            }

        }


        public static HR_PER_APP_ASSIGNMENT_DTL getClassEntity(string str_Id)
        {
            HR_PER_APP_ASSIGNMENT_DTL obj_HR_PER_APP_ASSIGNMENT_DTL = new HR_PER_APP_ASSIGNMENT_DTL();
            using (IRepository<HR_PER_APP_ASSIGNMENT_DTL> userCtx = new DataRepository<HR_PER_APP_ASSIGNMENT_DTL>())
            {
                obj_HR_PER_APP_ASSIGNMENT_DTL = userCtx.Find(r =>
                    (r.ASSIGN_DTL_ID == str_Id)
                    ).SingleOrDefault();
            }


            return obj_HR_PER_APP_ASSIGNMENT_DTL;
        }


        public static HR_PER_APP_ASSIGNMENT_DTL getDetailClassEntity(string str_Id)
        {
            HR_PER_APP_ASSIGNMENT_DTL obj_HR_PER_APP_ASSIGNMENT_DTL = new HR_PER_APP_ASSIGNMENT_DTL();
            using (IRepository<HR_PER_APP_ASSIGNMENT_DTL> userCtx = new DataRepository<HR_PER_APP_ASSIGNMENT_DTL>())
            {
                obj_HR_PER_APP_ASSIGNMENT_DTL = userCtx.Find(r =>
                    (r.ASSIGN_DTL_ID == str_Id)
                    ).SingleOrDefault();
            }
            return obj_HR_PER_APP_ASSIGNMENT_DTL;
        }



        public static String ValidateEntity(HR_PER_APP_ASSIGNMENT_DTL obj_HR_PER_APP_ASSIGNMENT_DTL)
        {
            string str_Error = "";
            return str_Error;
        }


        public static String DeleteEntity(int int_PKID)
        {
            string str_Message = "";
            HR_PER_APP_ASSIGNMENT_DTL obj_HR_PER_APP_ASSIGNMENT_DTL = new HR_PER_APP_ASSIGNMENT_DTL();
            obj_HR_PER_APP_ASSIGNMENT_DTL.PK_ID = int_PKID;
            DBMethod.DeleteEntity<HR_PER_APP_ASSIGNMENT_DTL>(obj_HR_PER_APP_ASSIGNMENT_DTL);
            return str_Message;
        }
    }
}
