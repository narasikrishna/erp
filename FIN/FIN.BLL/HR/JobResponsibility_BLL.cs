﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web;
using System.Web.UI.WebControls;
using FIN.DAL;
using FIN.DAL.HR;
using VMVServices.Web;
using System.Data;
namespace FIN.BLL.HR
{
   public class JobResponsibility_BLL
    {

        DropDownList ddlList = new DropDownList();
        DataTable dtDropDownData = new DataTable();

        # region FillCombo
        public void fn_GetJobname(ref DropDownList ddlList)
        {
            dtDropDownData = DBMethod.ExecuteQuery(Jobs_DAL.GetJobname()).Tables[0];
            CommonUtils.LoadDropDownList(ddlList, "JOB_NAME", "JOB_ID", dtDropDownData, true, false);
        }
        # endregion

        public static DataTable getChildEntityDet(string Str_ID)
        {
            DataTable dt_Det = DBMethod.ExecuteQuery(FIN.DAL.HR.JobsResponsibility_DAL.GetJRdtls(Str_ID)).Tables[0];
            return dt_Det;
        }
    


    }
}
